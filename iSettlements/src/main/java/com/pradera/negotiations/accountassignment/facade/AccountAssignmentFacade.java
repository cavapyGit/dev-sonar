
package com.pradera.negotiations.accountassignment.facade;

import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.lang3.SerializationUtils;
import org.beanio.BeanWriter;
import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.massive.type.AssignmentCorrectHeaderLabel;
import com.pradera.commons.massive.type.BbvAssignmentLabel;
import com.pradera.commons.massive.type.BbvOperationLabel;
import com.pradera.commons.massive.type.OperationIncorrectHeaderLabel;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.BeanIOUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.AccountOperationMarketFact;
import com.pradera.model.negotiation.AssignmentProcess;
import com.pradera.model.negotiation.AssignmentProcessOpen;
import com.pradera.model.negotiation.AssignmentProcessOpenDetail;
import com.pradera.model.negotiation.AssignmentProcessOpenFile;
import com.pradera.model.negotiation.AssignmentProcessOpenState;
import com.pradera.model.negotiation.AssignmentRequest;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.InchargeAgreement;
import com.pradera.model.negotiation.InchargeRule;
import com.pradera.model.negotiation.McnProcessFile;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.MechanismOperationOpen;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.ParticipantAssignment;
import com.pradera.model.negotiation.ReassignmentRequest;
import com.pradera.model.negotiation.ReassignmentRequestDetail;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.negotiation.type.AssignmentProcessOpenStateType;
import com.pradera.model.negotiation.type.AssignmentProcessStateType;
import com.pradera.model.negotiation.type.AssignmentRequestStateType;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantAssignmentStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.settlement.ChainedHolderOperation;
import com.pradera.model.settlement.HolderChainDetail;
import com.pradera.model.settlement.SettlementAccountMarketfact;
import com.pradera.model.settlement.type.ReassignmentRequestStateType;
import com.pradera.negotiations.accountassignment.exception.AccountAssignmentException;
import com.pradera.negotiations.accountassignment.service.AccountAssignmentService;
import com.pradera.negotiations.accountassignment.to.AssignmentProcessTO;
import com.pradera.negotiations.accountassignment.to.MechanismOperationTO;
import com.pradera.negotiations.accountassignment.to.OperationWithNoSecuritiesTO;
import com.pradera.negotiations.accountassignment.to.ParticipantAssignmentTO;
import com.pradera.negotiations.accountassignment.to.RegisterAccountAssignmentTO;
import com.pradera.negotiations.accountassignment.to.SearchAccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.AccountAssignmentTO;
import com.pradera.negotiations.assignmentrequest.to.AccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.AssignmentOpenModelTO;
import com.pradera.negotiations.assignmentrequest.to.MechanismOperationForOpenTO;
import com.pradera.negotiations.assignmentrequest.to.RegisterAccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAccountReassignmentTO;
import com.pradera.negotiations.assignmentrequest.to.SearchAssignmentRequestTO;
import com.pradera.negotiations.inchargeagreement.facade.InchargeAgreementFacade;
import com.pradera.negotiations.mcnoperations.massive.to.AssignmentOperationType;
import com.pradera.negotiations.mcnoperations.service.McnOperationAdditionalService;
import com.pradera.negotiations.mcnoperations.service.McnOperationServiceBean;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.settlements.chainedoperation.facade.ChainedOperationsFacade;
import com.pradera.settlements.chainedoperation.to.SearchChainedOperationTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.service.SettlementsComponentSingleton;
import com.pradera.settlements.currencyexchange.service.CurrencySettlementService;
import com.pradera.settlements.utils.NegotiationUtils;
import com.pradera.settlements.utils.PropertiesConstants;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AccountAssignmentFacade.
 *
 * @author PraderaTechnologies
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AccountAssignmentFacade {
	
	/** The account assignment service. */
	@EJB
	private AccountAssignmentService accountAssignmentService;
	
	/** The negotiation service bean. */
	@EJB
	private NegotiationOperationServiceBean negotiationServiceBean;	
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The settlements component singleton. */
	@EJB
	private SettlementsComponentSingleton settlementsComponentSingleton;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The parameter service bean. */
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	/** The mcn massive service facade. */
	@EJB
	private McnOperationAdditionalService mcnMassiveServiceBean;
	
	/** The mcn operation service bean. */
	@EJB
	private McnOperationServiceBean mcnOperationServiceBean;
		
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;	
	
	/** The incharge agreement facade. */
	@EJB
	InchargeAgreementFacade inchargeAgreementFacade;
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;

	/** The currency settlement service. */
	@EJB
	private CurrencySettlementService currencySettlementService;
	
	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;

	/** The batch service bean. */
	@EJB
	private BatchServiceBean batchServiceBean;
	
	@EJB
	ChainedOperationsFacade chainedOperationsFacade;
	
	/**
	 * obtiene una solicutd de Apertura de Cierre de Asignacion completa para mostrarla en la pantalla de view
	 */
	public AssignmentProcessOpen findAssignmentProcessOpenFull(Long idAssignmentProcessOpenPk) throws ServiceException {
		AssignmentProcessOpen apo= accountAssignmentService.find(AssignmentProcessOpen.class, idAssignmentProcessOpenPk).clone();
		apo.setAssignmentProcessOpenFileList(accountAssignmentService.lstAssignmentProcessOpenFiles(idAssignmentProcessOpenPk));
		apo.setMechanismOperationOpenList(accountAssignmentService.lstMechanismOperationOpen(idAssignmentProcessOpenPk));
		return apo;
	}
	
	/**
	 * issue 1161: obtiene las Aperturas de Cierre de Asignacion General de acuerdo a los filtros enviados como parametros
	 */
	public List<AssignmentOpenModelTO> lstAssignmentOpenModelTO(AssignmentOpenModelTO filter) throws ServiceException {
		List<Object[]> lstObj=accountAssignmentService.lstAssignmentOpenModelTO(filter);
		List<AssignmentOpenModelTO> lst=new ArrayList<>();
		AssignmentOpenModelTO assignmentOpenModelTO;
		for(Object [] row:lstObj){
			assignmentOpenModelTO=new AssignmentOpenModelTO();
			assignmentOpenModelTO.setId(Long.parseLong(row[0].toString()));
			assignmentOpenModelTO.setRegistryDate((Date) row[1]);
			assignmentOpenModelTO.setMotiveOpen(row[2].toString());
			assignmentOpenModelTO.setBallotsNumber(row[3] != null ? row[3].toString() : "---");
			assignmentOpenModelTO.setUserRegisterAssignmentOpen(row[4].toString());
			assignmentOpenModelTO.setUserConfirmAssignmentOpen(row[5] != null ? row[5].toString() : "---");
			assignmentOpenModelTO.setStateRequestText(row[6].toString());
			assignmentOpenModelTO.setStateRequest(Integer.parseInt(row[7].toString()));
			
			lst.add(assignmentOpenModelTO);
		}
		return lst;
	}
	
	/**
	 * issue 1161: realizando cambio de estado en ASSIGNMENT_PROCESS PARTICIPANT_ASSIGNMENT, MECHANISM_OPERATION
	 * <br>
	 * ASSIGNMENT_PROCESS: CERRADO -> ABIERTO
	 * <br>
	 * PARTICIPANT_ASSIGNMENT: CERRADO -> ABIERTO
	 * <br>
	 * MECHANISM_OPERATION: ASIGNADA -> REGISTRADA
	 * @param idAssignmentProcessOpenPk
	 */
	private void executeOpenOfClosedAssignmentProcess(Long idAssignmentProcessOpenPk){
		
		List<String> lstSQLExceute=new ArrayList<>();
		
		// cambiando a estado ABIERTO los ProcesosDeAsignacion Cerrados
		StringBuilder sd=new StringBuilder();
		sd.append(" update ASSIGNMENT_PROCESS set ASSIGNMENT_STATE = ");
		sd.append(AssignmentProcessStateType.OPENED.getCode());
		sd.append(" where ID_ASSIGNMENT_PROCESS_PK in (                                            ");
		sd.append("                                   select APOD.ID_ASSIGMENT_PROCESS_FK          ");
		sd.append("                                   from ASSIGNMENT_PROCESS_OPEN_DETAIL apod     ");
		sd.append("                                   where APOD.ID_ASSIGNMENT_PROCESS_OPEN_FK = :idAssignmentProcessOpenPk)");
		lstSQLExceute.add(sd.toString());
		
		// cambiando a estado ABIERTO la asignacion de participantes
		sd=new StringBuilder();
		sd.append(" update PARTICIPANT_ASSIGNMENT set ASSIGNMENT_STATE = ");
		sd.append(ParticipantAssignmentStateType.OPENED.getCode());
		sd.append(" where ID_ASSIGNMENT_PROCESS_FK in (                                            ");
		sd.append("                                   select APOD.ID_ASSIGMENT_PROCESS_FK          ");
		sd.append("                                   from ASSIGNMENT_PROCESS_OPEN_DETAIL apod     ");
		sd.append("                                   where APOD.ID_ASSIGNMENT_PROCESS_OPEN_FK = :idAssignmentProcessOpenPk)");
		lstSQLExceute.add(sd.toString());
		
		// cambiando a estado REGISTRADA las operaciones MCN seleccionadas en la Apertura de Cierre Asignacion General
		sd=new StringBuilder();
		sd.append(" update MECHANISM_OPERATION set OPERATION_STATE = ");
		sd.append(MechanismOperationStateType.REGISTERED_STATE.getCode());
		sd.append(" where ID_MECHANISM_OPERATION_PK in (                                  ");
		sd.append("                                   select MOO.ID_MECHANISM_OPERATION_FK"); 
		sd.append("                                   from MECHANISM_OPERATION_OPEN moo   ");
		sd.append("                                   where MOO.ID_ASSIGNMENT_OPEN_FK = :idAssignmentProcessOpenPk)");
		lstSQLExceute.add(sd.toString());
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idAssignmentProcessOpenPk", idAssignmentProcessOpenPk);
		for(String query:lstSQLExceute){
			accountAssignmentService.executeUpdateNativeQuery(query, parameters);
		}
	}
	
	/**
	 * issue 1161: metodo para cambiar de estado una Solicitud de Apertura de Cierre de Asignacion General
	 * @param modelTO
	 * @return
	 * @throws ServiceException
	 */
	public AssignmentProcessOpen changeStateAssignmentProcessOpen(AssignmentOpenModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			if(modelTO.getStateRequest().equals(AssignmentProcessOpenStateType.CONFIRMADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			} else if(modelTO.getStateRequest().equals(AssignmentProcessOpenStateType.RECHAZADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			}
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo AccountAssignmentFacade.changeStateAssignmentProcessOpen. Se procedera a setear las pistas de auditoria manualemnte");
		}
		
		// si esta cambiando a estado CONFIRMADO entonces se hace el cambio de estado y la reversion de saldos(si corresponde)
		if(modelTO.getStateRequest().equals(AssignmentProcessOpenStateType.CONFIRMADO.getCode())){
			System.out.println("::: realizando cambio de estado en ASSIGNMENT_PROCESS PARTICIPANT_ASSIGNMENT, MECHANISM_OPERATION");
			executeOpenOfClosedAssignmentProcess(modelTO.getId());
			System.out.println("::: realizando reversion de saldos (COMPRA/VENTA) en fecha "+modelTO.getRegistryDate());
			accountAssignmentService.revertSaleBalances(modelTO.getId(), modelTO.getRegistryDate());
			System.out.println("::: Fin derversion de Valores");
		}
		
		AssignmentProcessOpen assignmentProcessOpen = accountAssignmentService.find(AssignmentProcessOpen.class, modelTO.getId());
		assignmentProcessOpen.setStateRequest(modelTO.getStateRequest());
		//pistas de auditoria
		assignmentProcessOpen.setLastModifyUser(modelTO.getAuditUserName());
		assignmentProcessOpen.setLastModifyDate(new Date());
		assignmentProcessOpen.setLastModifyIp(modelTO.getAuditIpAddress());
		assignmentProcessOpen.setLastModifyApp(modelTO.getAuditIdPrivilege());
		
		// PREPARARANDO LAS TABLAS HIJAS
		// ====================================
		// tablas hija historico de estados
		AssignmentProcessOpenState state=new AssignmentProcessOpenState();
		state.setStateRequest(modelTO.getStateRequest());
		//pistas de auditoria
		state.setLastModifyUser(modelTO.getAuditUserName());
		state.setLastModifyDate(new Date());
		state.setLastModifyIp(modelTO.getAuditIpAddress());
		state.setLastModifyApp(modelTO.getAuditIdPrivilege());
		state.setIdAssignmentProcessOpenFk(assignmentProcessOpen);
		// para el registro en cascada
		List<AssignmentProcessOpenState> lst=new ArrayList<>();
		lst.add(state);
		assignmentProcessOpen.setAssignmentProcessOpenStateList(lst);

		return accountAssignmentService.update(assignmentProcessOpen);
	}
	
	/**
	 * issue 1161: registra una solicitud de apertura de Cierre de Asignacion General
	 * @param modelTO
	 * @return
	 * @throws ServiceException
	 */
	public AssignmentProcessOpen regiterAssignmentOpen(AssignmentOpenModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo AccountAssignmentFacade.regiterAssignmentOpen. Se procedera a setear las pistas de auditoria manualemnte");
		}
		AssignmentProcessOpen assignmentProcessOpen = new AssignmentProcessOpen();
		assignmentProcessOpen.setMotiveOpen(modelTO.getMotiveOpen());
		assignmentProcessOpen.setRegistryDate(modelTO.getRegistryDate());
		assignmentProcessOpen.setStateRequest(AssignmentProcessOpenStateType.REGISTRADO.getCode());
		//pistas de auditoria
		assignmentProcessOpen.setLastModifyUser(modelTO.getAuditUserName());
		assignmentProcessOpen.setLastModifyDate(new Date());
		assignmentProcessOpen.setLastModifyIp(modelTO.getAuditIpAddress());
		assignmentProcessOpen.setLastModifyApp(modelTO.getAuditIdPrivilege());
		
		// PREPARARANDO LAS TABLAS HIJAS
		// ====================================
		// tablas hija historico de estados
		AssignmentProcessOpenState state=new AssignmentProcessOpenState();
		state.setStateRequest(AssignmentProcessOpenStateType.REGISTRADO.getCode());
		//pistas de auditoria
		state.setLastModifyUser(modelTO.getAuditUserName());
		state.setLastModifyDate(new Date());
		state.setLastModifyIp(modelTO.getAuditIpAddress());
		state.setLastModifyApp(modelTO.getAuditIdPrivilege());
		state.setIdAssignmentProcessOpenFk(assignmentProcessOpen);
		// para el registro en cascada
		List<AssignmentProcessOpenState> lst=new ArrayList<>();
		lst.add(state);
		assignmentProcessOpen.setAssignmentProcessOpenStateList(lst);
		
		// tabla hija archivos adjuntos
		for(AssignmentProcessOpenFile file:modelTO.getLstAssignmentProcessOpenFile()){
			file.setRegistryDate(new Date());
			//pistas de auditoria
			file.setLastModifyUser(modelTO.getAuditUserName());
			file.setLastModifyDate(new Date());
			file.setLastModifyIp(modelTO.getAuditIpAddress());
			file.setLastModifyApp(modelTO.getAuditIdPrivilege());
			
			file.setIdAssignmentProcessOpenFk(assignmentProcessOpen);
		}
		// para el registro en cascada
		assignmentProcessOpen.setAssignmentProcessOpenFileList(modelTO.getLstAssignmentProcessOpenFile());
		
		// tabla hija MechanismOperationOpen, son las Operaciones MCN que seran cambiadas de estado ASIGNADA -> REGISTRADA
		MechanismOperationOpen openMCN;
		List<MechanismOperationOpen> lstOpenMCN=new ArrayList<>();
		for(MechanismOperationForOpenTO selectedMCN:modelTO.getLstMechanismOperationForOpenTOAdded()){
			openMCN=new MechanismOperationOpen();
			openMCN.setRegistryDate(new Date());
			//pistas de auditoria
			openMCN.setLastModifyUser(modelTO.getAuditUserName());
			openMCN.setLastModifyDate(new Date());
			openMCN.setLastModifyIp(modelTO.getAuditIpAddress());
			openMCN.setLastModifyApp(modelTO.getAuditIdPrivilege());
			
			openMCN.setIdMechanismOperationFk(new MechanismOperation(selectedMCN.getId()));
			openMCN.setIdAssignmentOpenFk(assignmentProcessOpen);
			
			lstOpenMCN.add(openMCN);
		}
		// para el registro en cascada
		assignmentProcessOpen.setMechanismOperationOpenList(lstOpenMCN);
		
		// tabla hija AssignmentProcessOpenDetail, son los Procesos de Asigancion que seran Aperturados
		List<Long> lstIdAssignmentProcesToOpen=accountAssignmentService.lstIdAssignmentProcessByDate(modelTO.getRegistryDate());
		List<AssignmentProcessOpenDetail> lstAssignmentProcessOpenDetails = new ArrayList<>();
		AssignmentProcessOpenDetail assignmentProcesToOpen;
		for(Long idAP:lstIdAssignmentProcesToOpen){
			assignmentProcesToOpen = new AssignmentProcessOpenDetail();
			//pistas de auditoria
			assignmentProcesToOpen.setLastModifyUser(modelTO.getAuditUserName());
			assignmentProcesToOpen.setLastModifyDate(new Date());
			assignmentProcesToOpen.setLastModifyIp(modelTO.getAuditIpAddress());
			assignmentProcesToOpen.setLastModifyApp(modelTO.getAuditIdPrivilege());
			
			assignmentProcesToOpen.setIdAssigmentProcessFk(new AssignmentProcess(idAP));
			assignmentProcesToOpen.setIdAssignmentProcessOpenFk(assignmentProcessOpen);
			
			lstAssignmentProcessOpenDetails.add(assignmentProcesToOpen);
		}
		// para el registro en cascada
		assignmentProcessOpen.setAssignmentProcessOpenDetailList(lstAssignmentProcessOpenDetails);
		
		return accountAssignmentService.createWithFlush(assignmentProcessOpen);
	}

	/**
	 * obtiene las Operaciones MCN que seran cambiadas de estado de ASIGNADO a REGISTRADO
	 * @param dateParameter
	 * @return
	 * @throws ServiceException
	 */
	public List<MechanismOperationForOpenTO> lstMechanismOperationForOpenTO(AssignmentOpenModelTO filter) throws ServiceException{
		return accountAssignmentService.lstMechanismOperationForOpenTO(filter);
	}
	/**
	 * obtiene la cantidad de encadenamientos que tiene una operacio MCN
	 * @param idMechanisOperationPk
	 * @return
	 */
	public Integer quantityChainsOfMcn(Long idMechanisOperationPk){
		return accountAssignmentService.quantityChainsMcn(idMechanisOperationPk);
	}
	
	public boolean existAssigmentProcessClosedByDate(Date dateParameter){
		List<Long> lstIdAssignmentProcess;
		try {
			lstIdAssignmentProcess = accountAssignmentService.lstIdAssignmentProcessByDate(dateParameter);
		} catch (ServiceException e) {
			e.printStackTrace();
			System.out.println("::: error: "+e.getMessage());
			return false;
		}
		if (lstIdAssignmentProcess == null || lstIdAssignmentProcess.isEmpty() || lstIdAssignmentProcess.size()==0){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * issue 1105: metodo que obtiene las operaciones con falta de valores de una determinada fecha y un participante
	 * @param dateOperation
	 * @param idParticipantPk
	 * @return
	 */
	public List<OperationWithNoSecuritiesTO> getLstOperationsWithNoSecurities(String dateOperation, Long idParticipantPk, Long codeModality){
		List<Object[]> lst=accountAssignmentService.getLstOperationsWithNoSecurities(dateOperation, idParticipantPk, codeModality);
		List<OperationWithNoSecuritiesTO> lstOnlyOperationsWithNoSecurities=new ArrayList<>();
		OperationWithNoSecuritiesTO operationWithNoSecurities;
		
		SearchChainedOperationTO searchChainedOperationTO=new SearchChainedOperationTO();
		searchChainedOperationTO.setIdParticipantPk(idParticipantPk);
		searchChainedOperationTO.setInitialDate(CommonsUtilities.convertStringtoDate(dateOperation));
		List<ChainedHolderOperation> lstChained=accountAssignmentService.getChainHolderRequest(searchChainedOperationTO);
		List<HolderChainDetail> lstHolderChainDetail;
		
		//variables para las operaciones con CANTIDAD_FALTANTE = 0
		BigDecimal missingAmount;
		Integer securityClass;
		Integer isInconsistencyMarketFacts=0;
		String ballotNumberSeq;
		//esta variables es para permitir o no la adicion de la operacion con valores faltantes
		boolean allowedOperationWithNoSecurities=false;
		for(Object [] data:lst){
			missingAmount = data[5] != null ? new BigDecimal(data[5].toString()):BigDecimal.ZERO;
			securityClass = data[10] != null ? Integer.parseInt(data[10].toString()):-1;
			isInconsistencyMarketFacts = data[11] != null ? Integer.parseInt(data[11].toString()):-1;
				
			// solo si se trata de DPF,PGS,DPA con inconsistencia de HM, entonces no se carga en la lista lstOnlyOperationsWithNoSecurities
			if ((securityClass.equals(SecurityClassType.DPF.getCode()) 
					|| securityClass.equals(SecurityClassType.PGS.getCode()) 
					|| securityClass.equals(SecurityClassType.DPA.getCode())) 
				&& isInconsistencyMarketFacts.equals(BooleanType.YES.getCode())) {
				continue;
			}
			
			operationWithNoSecurities=new OperationWithNoSecuritiesTO();
			operationWithNoSecurities.setOperationNumber(data[0]!= null ? Integer.parseInt(data[0].toString()): 0);
			operationWithNoSecurities.setDateOperation(data[1]!= null ? CommonsUtilities.convertDateToString((Date) data[1],"dd/MM/yyyy"):"-");
			operationWithNoSecurities.setSequentialBallot(data[2]!= null ? data[2].toString():"-");
			operationWithNoSecurities.setIdSecurityCodePk(data[3]!= null ? data[3].toString():"-");
			operationWithNoSecurities.setQuantityHolder(data[4]!= null ? Integer.parseInt(data[4].toString()): 0);
			operationWithNoSecurities.setQuantityMissing(missingAmount.intValue());
			operationWithNoSecurities.setMnemonicParticipantSell(data[6]!= null ? data[6].toString():"");
			operationWithNoSecurities.setMnemonicParticipantBuy(data[7]!= null ? data[7].toString():"");
			operationWithNoSecurities.setAmountSettlements(data[8] != null ? new BigDecimal(data[8].toString()):BigDecimal.ZERO);
			operationWithNoSecurities.setStateOperation(data[9]!= null ? data[9].toString():"-");
			
			allowedOperationWithNoSecurities=true;
			// si la falta de valor es igual a 0 entonces se evalua si es encadenamiento
			if(missingAmount.equals(BigDecimal.ZERO)){
				allowedOperationWithNoSecurities=false;
				forChain:
				for(ChainedHolderOperation chained:lstChained){
					lstHolderChainDetail = chainedOperationsFacade.getListHolderChainDetail(chained.getIdChainedHolderOperationPk());
					for(HolderChainDetail chaindedDetail:lstHolderChainDetail){
						// validando que papeleta y el secuencial no sean nulos
						if(Validations.validateIsNotNullAndNotEmpty(chaindedDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getBallotNumber())
								&& Validations.validateIsNotNullAndNotEmpty(chaindedDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getSequential()) ){
							
							// aca se genera la papeleta secuencial segun como nos arroja la query
							ballotNumberSeq=chaindedDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getBallotNumber()
							+"-"+
							chaindedDetail.getSettlementAccountMarketfact().getSettlementAccountOperation().getSettlementOperation().getMechanismOperation().getSequential();
							// comparando papeleta generadcon la papeleta q retorna la BBDD
							if(ballotNumberSeq.equals(operationWithNoSecurities.getSequentialBallot())){
								allowedOperationWithNoSecurities=true;
								break forChain;
							}
						}
					}
				}
			}
			
			if(allowedOperationWithNoSecurities)
				lstOnlyOperationsWithNoSecurities.add(operationWithNoSecurities);
				
		}
		
		return lstOnlyOperationsWithNoSecurities;
	}
	
	/**
	 * Update participant assignment.
	 *
	 * @param idParticipantAssignmentPk the id participant assignment pk
	 * @param state the state
	 * @throws ServiceException the service exception
	 */
	public void updateParticipantAssignment(Long idParticipantAssignmentPk, Integer state) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		accountAssignmentService.updateParticipantAssignment(idParticipantAssignmentPk, state, loggerUser);
		return; 
	}
	
	/**
	 * Gets the settlement account marketfacts.
	 *
	 * @param idSettlementAccountOperation the id settlement account operation
	 * @return the settlement account marketfacts
	 */
	public List<SettlementAccountMarketfact> getSettlementAccountMarketfacts(Long idSettlementAccountOperation) {
		return settlementProcessService.getSettlementAccountMarketfacts(idSettlementAccountOperation);
	}

	/**
	 * Gets the holder account operations.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param participantId the participant id
	 * @param lstRoles the lst roles
	 * @param lstStates the lst states
	 * @param blDepositary the bl depositary
	 * @return the holder account operations
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountOperation> getHolderAccountOperations(Long idMechanismOperation,Long participantId, List<Integer> lstRoles,
																		List<Integer> lstStates, boolean blDepositary) throws ServiceException{
		List<HolderAccountOperation> holderAccountOperations = new ArrayList<HolderAccountOperation>();
		List<Object[]> arrObjs = accountAssignmentService.getHolderAccountOperations(idMechanismOperation, participantId, lstRoles, lstStates, ComponentConstant.CASH_PART, blDepositary);
		for (Object[] arrObject : arrObjs) {
			HolderAccountOperation holderAccountOperation = (HolderAccountOperation)arrObject[0];
			holderAccountOperation.setHolderAccount(accountAssignmentService.findHolderAccount((Long)arrObject[1]));
			holderAccountOperation.setAccountOperationMarketFacts(accountAssignmentService.getAccountOperationMarketfacts(holderAccountOperation.getIdHolderAccountOperationPk()));
			holderAccountOperations.add(holderAccountOperation);
		}
		return holderAccountOperations;
	}
	
	/**
	 * Gets the holder account operations repo.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the holder account operations repo
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountOperation> getHolderAccountOperationsRepo(Long idMechanismOperationPk) throws ServiceException{
		Long mechanismOperationRef = accountAssignmentService.getReferenceMechanismOperation(idMechanismOperationPk);
		List<Integer> roles = new ArrayList<Integer>();
		roles.add(ComponentConstant.SALE_ROLE);
		
		List<Integer> states = new ArrayList<Integer>();
		states.add(HolderAccountOperationStateType.CONFIRMED.getCode());
		
		return getHolderAccountOperations(mechanismOperationRef, null, roles, states, false);
	}

	/**
	 * Save assignment.
	 *
	 * @param idAssignmentProcessPk the id assignment process pk
	 * @param idParticipantAssignmentPk the id participant assignment pk
	 * @param registerAccountAssignmentTO the register account assignment to
	 * @throws ServiceException the service exception
	 * @throws AccountAssignmentException 
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_ASSIGN_REGISTER)
	public void saveAssignment(Long idAssignmentProcessPk, Long idParticipantAssignmentPk, RegisterAccountAssignmentTO registerAccountAssignmentTO) throws ServiceException, AccountAssignmentException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		//validate if participant assignment is closed.
		ParticipantAssignment participantAssignment = (ParticipantAssignment)accountAssignmentService.find(ParticipantAssignment.class, idParticipantAssignmentPk);
		if(!ParticipantAssignmentStateType.OPENED.getCode().equals(participantAssignment.getAssignmentState())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_ASSIGNMENT_CLOSED);
		}

		// remove previous registered assignments
		List<HolderAccountOperation> lstHolderAccountOperationRemoved = registerAccountAssignmentTO.getLstHolderAccountOperationRemoved();
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountOperationRemoved)){
			accountAssignmentService.removeHolderAccountOperations(lstHolderAccountOperationRemoved,registerAccountAssignmentTO.getMechanismOperation(), loggerUser);
		}		
			
		//create new assignments if exists
		List<HolderAccountOperation> lstHolderAccountOperation = null;
		if(Validations.validateIsNotNull(registerAccountAssignmentTO.getLstHolderAccountOperation())){
			lstHolderAccountOperation = registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList();
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountOperation)){
			for(HolderAccountOperation holderAccountOperation:lstHolderAccountOperation){			
				//Si no tiene el campo de auditoria, es un nuevo registro
				if(Validations.validateIsNullOrEmpty(holderAccountOperation.getLastModifyUser())){
					validateEntities(holderAccountOperation);
					holderAccountOperation.setMechanismOperation(registerAccountAssignmentTO.getMechanismOperation());
					holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
					holderAccountOperation.setOperationPart(ComponentConstant.CASH_PART);	
					
					List<HolderAccount> lstOriginHolderAccount = new ArrayList<HolderAccount>();
					if(ComponentConstant.ONE.equals(holderAccountOperation.getIndIncharge()) && 
							(Validations.validateIsNotNull(holderAccountOperation.getIndAutomatedIncharge()) && holderAccountOperation.getIndAutomatedIncharge().equals(ComponentConstant.ONE))) {
						lstOriginHolderAccount = checkHolderAccountInParticipant(null, registerAccountAssignmentTO.getParticipantOrigin(), Boolean.FALSE, Boolean.TRUE);
					}					
					
					accountAssignmentService.saveHolderAccountAssignment(holderAccountOperation, idepositarySetup.getIndMarketFact(), loggerUser);
					
					if(ComponentConstant.ONE.equals(holderAccountOperation.getIndIncharge()) &&
							(Validations.validateIsNotNull(holderAccountOperation.getIndAutomatedIncharge()) && holderAccountOperation.getIndAutomatedIncharge().equals(ComponentConstant.ONE))) {
						this.confirmInchargeRequest(holderAccountOperation, lstOriginHolderAccount.get(0));
					}
					
				}
			}
		}		
	}
	
	/**
	 * Validate entities.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @return the error service type
	 */
	private ErrorServiceType validateEntities(HolderAccountOperation holderAccountOperation){
		Map<String, Object> mpEntities = new HashMap<String, Object>();
		mpEntities.put(parameterServiceBean.HOLDER_ACCOUNT_KEY, holderAccountOperation.getHolderAccount().getIdHolderAccountPk());
		mpEntities.put(parameterServiceBean.PARTICIPANT_KEY, holderAccountOperation.getInchargeStockParticipant().getIdParticipantPk());
		return parameterServiceBean.validateEntities(mpEntities);
	}
	
	/**
	 * Confirm assignment.
	 *
	 * @param idAssignmentProcessPk the id assignment process pk
	 * @param idParticipantAssignmentPk the id participant assignment pk
	 * @param registerAccountAssignmentTO the register account assignment to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_ASSIGN_CONFIRM)
	public void confirmAssignment(Long idAssignmentProcessPk, Long idParticipantAssignmentPk, RegisterAccountAssignmentTO registerAccountAssignmentTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		AssignmentRequest buyerAssignmentRequest = null;
		boolean blUpdateBuyerAssignment = false;
		AssignmentRequest sellerAssignmentRequest = null;
		boolean blUpdateSellerAssignment = false;
		ParticipantAssignment participantAssignment = null;
		List<Integer> lstRoles = new ArrayList<Integer>();		
		
		if(registerAccountAssignmentTO.isBlCrossOperation()){
			lstRoles.add(ParticipantRoleType.BUY.getCode());
			lstRoles.add(ParticipantRoleType.SELL.getCode());
		}else{
			lstRoles.add(registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList().get(0).getRole());
		}
		participantAssignment = (ParticipantAssignment)accountAssignmentService.find(ParticipantAssignment.class, idParticipantAssignmentPk);
		if(!ParticipantAssignmentStateType.OPENED.getCode().equals(participantAssignment.getAssignmentState()))
			throw new ServiceException(ErrorServiceType.PARTICIPANT_ASSIGNMENT_CLOSED);

		//Se recupera las solicitudes de asignacion existe 1 por cada rol
		List<AssignmentRequest> lstAssignmentRequests = accountAssignmentService.getListAssignmentRequest(idParticipantAssignmentPk,
																											registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk(),
																											lstRoles);
		if(Validations.validateListIsNotNullAndNotEmpty(lstAssignmentRequests)){
			for (AssignmentRequest assigRequest : lstAssignmentRequests) {
				if(ParticipantRoleType.BUY.getCode().equals(assigRequest.getRole()) 
						&& (AssignmentRequestStateType.PENDING.getCode().equals(assigRequest.getRequestState())
								|| AssignmentRequestStateType.REGISTERED.getCode().equals(assigRequest.getRequestState()))){
					buyerAssignmentRequest = assigRequest;
				}
				if(ParticipantRoleType.SELL.getCode().equals(assigRequest.getRole()) &&
						(AssignmentRequestStateType.PENDING.getCode().equals(assigRequest.getRequestState())
								|| AssignmentRequestStateType.REGISTERED.getCode().equals(assigRequest.getRequestState()))){
					sellerAssignmentRequest = assigRequest;
				} 
			}
		}
		
		for(Integer role:lstRoles){
			if(ParticipantRoleType.BUY.getCode().equals(role)
					&& Validations.validateIsNullOrEmpty(buyerAssignmentRequest)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_REQUEST_CONFIRMED, new Object[]{ParticipantRoleType.BUY.getValue()});
			}else if(ParticipantRoleType.SELL.getCode().equals(role)
					&& Validations.validateIsNullOrEmpty(sellerAssignmentRequest)){
				throw new ServiceException(ErrorServiceType.ASSIGNMENT_REQUEST_CONFIRMED, new Object[]{ParticipantRoleType.SELL.getValue()});
			}				
		}
		
		List<HolderAccountOperation> lstHolderAccountOperation = registerAccountAssignmentTO.getLstHolderAccountOperation().getDataList();		
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountOperation)){
			for(HolderAccountOperation holderAccountOperation:lstHolderAccountOperation){
				validateEntities(holderAccountOperation);
				if(ParticipantRoleType.BUY.getCode().equals(holderAccountOperation.getRole())){
					blUpdateBuyerAssignment = true;
				}else{
					blUpdateSellerAssignment = true;
				}
				holderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());				
				holderAccountOperation.setConfirmDate(CommonsUtilities.currentDateTime());
				holderAccountOperation.setMechanismOperation(registerAccountAssignmentTO.getMechanismOperation());
				accountAssignmentService.update(holderAccountOperation);
				
				if(BooleanType.YES.getCode().equals(registerAccountAssignmentTO.getMechanismOperation().getMechanisnModality().getNegotiationModality().getIndTermSettlement())){
					HolderAccountOperation hActOperation = accountAssignmentService.getHolderAccountOperationTermPart(holderAccountOperation.getIdHolderAccountOperationPk());
					hActOperation.setHolderAccountState(HolderAccountOperationStateType.CONFIRMED.getCode());					
					hActOperation.setConfirmDate(CommonsUtilities.currentDateTime());
					hActOperation.setMechanismOperation(holderAccountOperation.getMechanismOperation());
					
				}
			}
			if(blUpdateBuyerAssignment){
				buyerAssignmentRequest.setRequestState(AssignmentRequestStateType.CONFIRMED.getCode());
				buyerAssignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
				buyerAssignmentRequest.setConfirmUser(loggerUser.getUserName());
			}
			if(blUpdateSellerAssignment){
				sellerAssignmentRequest.setRequestState(AssignmentRequestStateType.CONFIRMED.getCode());
				sellerAssignmentRequest.setConfirmDate(CommonsUtilities.currentDateTime());
				sellerAssignmentRequest.setConfirmUser(loggerUser.getUserName());
			}
		}
		AssignmentRequest sellAssignmentRequest = accountAssignmentService.getAssignmentRequest(registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk()
																							, ParticipantRoleType.SELL.getCode());
		AssignmentRequest buyAssignmentRequest = accountAssignmentService.getAssignmentRequest(registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk()
																							, ParticipantRoleType.BUY.getCode());
		if (AssignmentRequestStateType.CONFIRMED.getCode().equals(sellAssignmentRequest.getRequestState())
				&& AssignmentRequestStateType.CONFIRMED.getCode().equals(buyAssignmentRequest.getRequestState())) {
			//if both participants confirmed their assignment request then we have to change the state of mechanism operation to ASSIGNED
//			manageHolderAccountAssignmentService.updateMechanisOperationAssignedState(registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk(), loggerUser);
		}
		
		// calculate coverage amount if modality has guarantees indicator
		if(AssignmentRequestStateType.CONFIRMED.getCode().equals(sellAssignmentRequest.getRequestState())){
			if(BooleanType.YES.getCode().equals(registerAccountAssignmentTO.getMechanismOperation().getIndMarginGuarantee())){
				accountAssignmentService.calculateCoverageAmount(registerAccountAssignmentTO.getMechanismOperation().getIdMechanismOperationPk(),loggerUser);
			}
		}
	}
	
	/**
	 * To confirm Assignment.
	 *
	 * @param strIdAssignmentProcess the str id assignment process
	 * @throws ServiceException the service exception
	 * @throws AccountAssignmentException 
	 */
	public void closeGeneralAssignmentProcess(String strIdAssignmentProcess) throws ServiceException, AccountAssignmentException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		List<Long> lstIdAssignmentProcess= new ArrayList<Long>();
		if (Validations.validateIsNotNullAndNotEmpty(strIdAssignmentProcess)) {
			String[] strAssignmentProcess= strIdAssignmentProcess.split(GeneralConstants.STR_COMMA_WITHOUT_SPACE);
			for (String idAssignmentProcess: strAssignmentProcess) {
				lstIdAssignmentProcess.add(new Long(idAssignmentProcess));
			}
		} else {
			lstIdAssignmentProcess = accountAssignmentService.getListIdAssignmentProcess(CommonsUtilities.currentDate());
		}
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstIdAssignmentProcess)) {
			for (Long assignmentProcessId: lstIdAssignmentProcess) {
				settlementsComponentSingleton.closeGeneralAssignmentProcess(assignmentProcessId, loggerUser);
			}
		}				
		
		//we send the update sale and purchase balance process
		BusinessProcess objBusinessProcess= new BusinessProcess();
		objBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.BALANCE_UPDATE_BUY_SELL.getCode());
		Map<String, Object> processParameters=new HashMap<String, Object>();
		processParameters.put(ComponentConstant.SETTLEMENT_SCHEMA, SettlementSchemaType.NET.getCode());
		batchProcessServiceFacade.registerBatch(loggerUser.getUserName(), objBusinessProcess, processParameters);
	}
	
	/**
	 * Gets the operations.
	 *
	 * @param filter the filter
	 * @return the operations
	 * @throws ServiceException the service exception
	 */
	public List<MechanismOperationTO> getOperations(RegisterAccountReassignmentTO filter) throws ServiceException{
		return accountAssignmentService.getOperations(filter);
	}

	
	/**
	 * Gets the assignment process.
	 *
	 * @param searchAccountAssignmentTO the search account assignment to
	 * @return the assignment process
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.HOLDER_ACCOUNT_ASSIGNMENT_MONITORING)
	public List<AssignmentProcessTO> getAssignmentProcess(SearchAccountAssignmentTO searchAccountAssignmentTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<Object[]> lstResult = accountAssignmentService.getAssignmentProcess(searchAccountAssignmentTO);
		List<AssignmentProcessTO> lstAssignmentProcess = new ArrayList<AssignmentProcessTO>();
		Long idParticipantPk = searchAccountAssignmentTO.getParticipantSelected();
		Integer participantAssignmentState = searchAccountAssignmentTO.getParticipantAssignmentState();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(Object[] objResult:lstResult){				
				//PROCESO DE ASIGNACION
				BigDecimal pending = accountAssignmentService.getAssignmentRequestsByState(new Long(objResult[0].toString()), null,
																				AssignmentRequestStateType.PENDING.getCode(), idParticipantPk);
				BigDecimal registered = accountAssignmentService.getAssignmentRequestsByState(new Long(objResult[0].toString()), null,
																				AssignmentRequestStateType.REGISTERED.getCode(), idParticipantPk);
				BigDecimal confirmed = accountAssignmentService.getAssignmentRequestsByState(new Long(objResult[0].toString()), null,
																				AssignmentRequestStateType.CONFIRMED.getCode(), idParticipantPk);
				
				if(pending.compareTo(BigDecimal.ZERO) == 0 && registered.compareTo(BigDecimal.ZERO) == 0 && 
						confirmed.compareTo(BigDecimal.ZERO) == 0){
					continue;
				}
				
				AssignmentProcessTO assignmentProcessTO = new AssignmentProcessTO();
				assignmentProcessTO.setIdAssignmentProcessPk(new Long(objResult[0].toString()));
				assignmentProcessTO.setIdNegotiationModalityPk(new Long(objResult[1].toString()));
				assignmentProcessTO.setModalityName(objResult[2].toString());
				assignmentProcessTO.setSettlementDate((Date)objResult[3]);
				assignmentProcessTO.setState((Integer)objResult[4]);
				assignmentProcessTO.setStateDescription(objResult[5].toString());
				assignmentProcessTO.setSettlementSchema((Integer)objResult[6]);
				assignmentProcessTO.setSettlementSchemaName(SettlementSchemaType.get((Integer)objResult[6]).getValue());
				assignmentProcessTO.setPendingAssignment(pending);
				assignmentProcessTO.setRegisteredAssignment(registered);
				assignmentProcessTO.setConfirmedAssignment(confirmed);
				BigDecimal total = assignmentProcessTO.getPendingAssignment().add(assignmentProcessTO.getRegisteredAssignment());
				total = total.add(assignmentProcessTO.getConfirmedAssignment());
				assignmentProcessTO.setTotalAssignment(total);
				//PROCESO DE ASIGNACION DE PARTICIPANTES
				List<Object[]> lstResultTemp = accountAssignmentService.getParticipantAssignment(assignmentProcessTO.getIdAssignmentProcessPk(), idParticipantPk, participantAssignmentState);
				List<ParticipantAssignmentTO> lstParticipantAssignment = new ArrayList<ParticipantAssignmentTO>();
				if(Validations.validateListIsNotNullAndNotEmpty(lstResultTemp)){
					for(Object[] objResultTemp:lstResultTemp){
						ParticipantAssignmentTO participAssignmentTO = new ParticipantAssignmentTO();
						participAssignmentTO.setIdParticipantAssignmentPk(new Long(objResultTemp[0].toString()));
						participAssignmentTO.setIdParticipantPk(new Long(objResultTemp[1].toString()));
						participAssignmentTO.setParticipantDesc(objResultTemp[2].toString() + GeneralConstants.DASH + objResultTemp[1].toString());
						participAssignmentTO.setParticipantFullDesc(objResultTemp[3].toString());
						participAssignmentTO.setState(new Integer(objResultTemp[4].toString()));
						participAssignmentTO.setStateDescription(objResultTemp[5].toString());
						participAssignmentTO.setPendingOperations(accountAssignmentService.getAssignmentRequestsByState(null,
																					participAssignmentTO.getIdParticipantAssignmentPk(), 
																					AssignmentRequestStateType.PENDING.getCode(),
																					participAssignmentTO.getIdParticipantPk()));
						participAssignmentTO.setRegisteredOperations(accountAssignmentService.getAssignmentRequestsByState(null,
																					participAssignmentTO.getIdParticipantAssignmentPk(), 
																					AssignmentRequestStateType.REGISTERED.getCode(),
																					participAssignmentTO.getIdParticipantPk()));
						participAssignmentTO.setConfirmedOperations(accountAssignmentService.getAssignmentRequestsByState(null,
																					participAssignmentTO.getIdParticipantAssignmentPk(), 
																					AssignmentRequestStateType.CONFIRMED.getCode(),
																					participAssignmentTO.getIdParticipantPk()));
						BigDecimal totalTemp = participAssignmentTO.getPendingOperations().add(participAssignmentTO.getRegisteredOperations());
						totalTemp = totalTemp.add(participAssignmentTO.getConfirmedOperations());
						if(totalTemp.compareTo(BigDecimal.ZERO) == 0)
							continue;
						participAssignmentTO.setTotalOperations(totalTemp);
						//OPERACIONES SEGUN PARTICIPANTE
						List<Object[]> lstOperations = null;
						List<MechanismOperationTO> lstMechanismOperation = null;
						//SOLO PENDIENTES
						if(participAssignmentTO.getPendingOperations().compareTo(BigDecimal.ZERO) == 1){
							lstMechanismOperation = new ArrayList<MechanismOperationTO>();
							lstOperations = accountAssignmentService.getOperations(participAssignmentTO.getIdParticipantPk(),
																							assignmentProcessTO.getIdAssignmentProcessPk(),
																							AssignmentRequestStateType.PENDING.getCode());							
							if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
								for(Object[] objects:lstOperations){
									MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
									mechanismOperationTO.setOperationNumber((Long)objects[0]);
									mechanismOperationTO.setIdSecurityCodePk(objects[1].toString());
									mechanismOperationTO.setStockQuantity((BigDecimal)objects[2]);
									mechanismOperationTO.setCashSettlementAmount((BigDecimal)objects[3]);
									mechanismOperationTO.setPurchaseParticipantDescription(objects[4].toString() + GeneralConstants.DASH + objects[6].toString());
									mechanismOperationTO.setSaleParticipantDescription(objects[5].toString() + GeneralConstants.DASH + objects[7].toString());
									mechanismOperationTO.setIdPurchaseParticipant((Long)objects[6]);
									mechanismOperationTO.setIdSaleParticipant((Long)objects[7]);
									mechanismOperationTO.setOperationDate((Date)objects[8]);
									mechanismOperationTO.setIdMechanismOperationPk((Long)objects[9]);
									mechanismOperationTO.setRoleDescription(ParticipantRoleType.get((Integer)objects[10]).getValue());
									mechanismOperationTO.setRole((Integer)objects[10]);
									mechanismOperationTO.setCashSettlementPrice((BigDecimal)objects[11]);
									mechanismOperationTO.setCashSettlementDate((Date)objects[12]);		
									mechanismOperationTO.setBallotNumber((Long)objects[15]);
									mechanismOperationTO.setSequential((Long)objects[16]);
									mechanismOperationTO.setCurrencyDescription((String)objects[17]);
									lstMechanismOperation.add(mechanismOperationTO);
								}
								participAssignmentTO.setLstPendingMechanismOperation(lstMechanismOperation);
							}
						}
						//SOLO REGISTRADAS
						if(participAssignmentTO.getRegisteredOperations().compareTo(BigDecimal.ZERO) == 1){
							lstMechanismOperation = new ArrayList<MechanismOperationTO>();
							lstOperations = accountAssignmentService.getOperations(participAssignmentTO.getIdParticipantPk(),
																							assignmentProcessTO.getIdAssignmentProcessPk(),
																							AssignmentRequestStateType.REGISTERED.getCode());							
							if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
								for(Object[] objects:lstOperations){
									MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
									mechanismOperationTO.setOperationNumber((Long)objects[0]);
									mechanismOperationTO.setIdSecurityCodePk(objects[1].toString());
									mechanismOperationTO.setStockQuantity((BigDecimal)objects[2]);
									mechanismOperationTO.setCashSettlementAmount((BigDecimal)objects[3]);
									mechanismOperationTO.setPurchaseParticipantDescription(objects[4].toString() + GeneralConstants.DASH + objects[6].toString());
									mechanismOperationTO.setSaleParticipantDescription(objects[5].toString() + GeneralConstants.DASH + objects[7].toString());
									mechanismOperationTO.setIdPurchaseParticipant((Long)objects[6]);
									mechanismOperationTO.setIdSaleParticipant((Long)objects[7]);
									mechanismOperationTO.setOperationDate((Date)objects[8]);
									mechanismOperationTO.setIdMechanismOperationPk((Long)objects[9]);
									mechanismOperationTO.setRoleDescription(ParticipantRoleType.get((Integer)objects[10]).getValue());
									mechanismOperationTO.setRole((Integer)objects[10]);
									mechanismOperationTO.setCashSettlementPrice((BigDecimal)objects[11]);
									mechanismOperationTO.setCashSettlementDate((Date)objects[12]);
									mechanismOperationTO.setBallotNumber((Long)objects[15]);
									mechanismOperationTO.setSequential((Long)objects[16]);
									mechanismOperationTO.setCurrencyDescription((String)objects[17]);
									lstMechanismOperation.add(mechanismOperationTO);
								}
								participAssignmentTO.setLstRegisteredMechanismOperation(lstMechanismOperation);
							}
						}
						//SOLO CONFIRMADAS						
						if(participAssignmentTO.getConfirmedOperations().compareTo(BigDecimal.ZERO) == 1){
							lstMechanismOperation = new ArrayList<MechanismOperationTO>();
							lstOperations = accountAssignmentService.getOperations(participAssignmentTO.getIdParticipantPk(),
																							assignmentProcessTO.getIdAssignmentProcessPk(),
																							AssignmentRequestStateType.CONFIRMED.getCode());							
							if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
								for(Object[] objects:lstOperations){
									MechanismOperationTO mechanismOperationTO = new MechanismOperationTO();
									mechanismOperationTO.setOperationNumber((Long)objects[0]);
									mechanismOperationTO.setIdSecurityCodePk(objects[1].toString());
									mechanismOperationTO.setStockQuantity((BigDecimal)objects[2]);
									mechanismOperationTO.setCashSettlementAmount((BigDecimal)objects[3]);
									mechanismOperationTO.setPurchaseParticipantDescription(objects[4].toString() + GeneralConstants.DASH + objects[6].toString());
									mechanismOperationTO.setSaleParticipantDescription(objects[5].toString() + GeneralConstants.DASH + objects[7].toString());
									mechanismOperationTO.setIdPurchaseParticipant((Long)objects[6]);
									mechanismOperationTO.setIdSaleParticipant((Long)objects[7]);
									mechanismOperationTO.setOperationDate((Date)objects[8]);
									mechanismOperationTO.setIdMechanismOperationPk((Long)objects[9]);
									mechanismOperationTO.setRoleDescription(ParticipantRoleType.get((Integer)objects[10]).getValue());
									mechanismOperationTO.setRole((Integer)objects[10]);
									mechanismOperationTO.setCashSettlementPrice((BigDecimal)objects[11]);
									mechanismOperationTO.setCashSettlementDate((Date)objects[12]);
									mechanismOperationTO.setBallotNumber((Long)objects[15]);
									mechanismOperationTO.setSequential((Long)objects[16]);
									mechanismOperationTO.setCurrencyDescription((String)objects[17]);
									lstMechanismOperation.add(mechanismOperationTO);
								}
								participAssignmentTO.setLstConfirmedMechanismOperation(lstMechanismOperation);
							}
						}
						lstParticipantAssignment.add(participAssignmentTO);			
					}
					assignmentProcessTO.setLstParticipantAssignment(lstParticipantAssignment);
				}
				lstAssignmentProcess.add(assignmentProcessTO);
			}
			return lstAssignmentProcess;
		}else
			return null;
	}
	
	/**
	 * Gets the list id assignment process.
	 *
	 * @param processDate the process date
	 * @return the list id assignment process
	 */
	public List<Long> getListIdAssignmentProcess(Date processDate){
		return accountAssignmentService.getListIdAssignmentProcess(processDate);
	}

	/**
	 * Gets the list negotiation mechanism.
	 *
	 * @return the list negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListNegotiationMechanism() throws ServiceException{
		return mcnOperationServiceBean.getLstNegotiationMechanism(ComponentConstant.ONE);
	}

	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 */
	public Participant getParticipant(Long participantCode){
		return accountAssignmentService.find(Participant.class, participantCode);
	}

	/**
	 * Gets the list nego mechanism for participant.
	 *
	 * @param participantCode the participant code
	 * @return the list nego mechanism for participant
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationMechanism> getListNegoMechanismForParticipant(Long participantCode) throws ServiceException{
		return accountAssignmentService.getListNegoMechanismForParticipant(participantCode);
	}

	/**
	 * Gets the list participants for nego mechanism.
	 *
	 * @param negoMechanismSelected the nego mechanism selected
	 * @param accountType the account type
	 * @return the list participants for nego mechanism
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getListParticipantsForNegoMechanism(Long negoMechanismSelected,Integer accountType) throws ServiceException{
		return accountAssignmentService.getListParticipantsForMechanismModality(negoMechanismSelected,null, accountType,null);
	}

	/**
	 * Gets the list negotiation modality.
	 *
	 * @param negoMechanismSelected the nego mechanism selected
	 * @param participantCode the participant code
	 * @param inCharge the in charge
	 * @return the list negotiation modality
	 * @throws ServiceException the service exception
	 */
	public List<NegotiationModality> getListNegotiationModality(Long negoMechanismSelected, Long participantCode, Integer inCharge) throws ServiceException{
		if(Validations.validateIsNullOrEmpty(participantCode)){
			return mcnOperationServiceBean.getListNegotiationModality(negoMechanismSelected, inCharge);
		}else{
			return mcnOperationServiceBean.getLstNegotiationModalityForParticipant(negoMechanismSelected, participantCode, inCharge);
		}
	}

	/**
	 * Gets the list participants for nego modality.
	 *
	 * @param negoMechanismSelected the nego mechanism selected
	 * @param negoModalitySelected the nego modality selected
	 * @param accountType the account type
	 * @param indIncharge the ind incharge
	 * @return the list participants for nego modality
	 */
	public List<Participant> getListParticipantsForNegoModality(Long negoMechanismSelected, Long negoModalitySelected, Integer accountType, Integer indIncharge) {
		return accountAssignmentService.getListParticipantsForMechanismModality(negoMechanismSelected,negoModalitySelected, accountType, indIncharge);
	}

	/**
	 * Gets the mechanism operation.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @return the mechanism operation
	 * @throws ServiceException the service exception
	 */
	public MechanismOperation getMechanismOperation(Long idMechanismOperationPk) throws ServiceException{
		MechanismOperation mechanismOperation = accountAssignmentService.getMechanismOperation(idMechanismOperationPk);
		return mechanismOperation;
	}
	
	/**
	 * Find holder accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccounts(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		return accountAssignmentService.findHolderAccounts(idHolderPk, idParticipantPk);
	}
	
	/**
	 * Find holder accounts for incharge agreement.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccountsForInchargeAgreement(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		return accountAssignmentService.findHolderAccountsForInchargeAgreement(idHolderPk, idParticipantPk);
	}
	
	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		return accountAssignmentService.findHolderAccount(idHolderAccountPk);
	}

	/**
	 * Gets the incharge rules.
	 *
	 * @param role the role
	 * @return the incharge rules
	 */
	public Map<String, String> getInchargeRules(Integer role) {
		Map<String, String> mpInchargeRule = new HashMap<String, String>();
		List<InchargeRule> lstInchargeRule = accountAssignmentService.getInchargeRule(role);
		if(Validations.validateListIsNotNullAndNotEmpty(lstInchargeRule)){
			String keyInchargeRule = "";
			for(InchargeRule inchargeRule:lstInchargeRule){
				keyInchargeRule = inchargeRule.getIndTraderParticipant().toString() + inchargeRule.getIndStockParticipant().toString() + inchargeRule.getIndFundsParticipant().toString();
				mpInchargeRule.put(keyInchargeRule, BooleanType.YES.getCode().toString());
			}
		}
		return mpInchargeRule;
	}
	
	/**
	 * Gets the incharge state.
	 *
	 * @param idHolderAccountOperationPk the id holder account operation pk
	 * @return the incharge state
	 */
	public Long getInchargeState(Long idHolderAccountOperationPk) {
		return accountAssignmentService.getInchargeState(idHolderAccountOperationPk);
	}

	/**
	 * Save assignment massively.
	 *
	 * @param processFileTO the process file to
	 * @param indMarketFact the ind market fact
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=15)
	public void saveAssignmentMassively(ProcessFileTO processFileTO, Integer indMarketFact)  {

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        // #1 separate transaction: save file
        ValidationProcessTO validation = processFileTO.getValidationProcessTO();
        McnProcessFile mcnProcessFile = mcnMassiveServiceBean.saveMcnProcessFileTx(processFileTO,loggerUser);

        try {
			// #2 separate transaction: save operations independently
			for(ValidationOperationType operation : validation.getLstOperations()){
				AssignmentOperationType assignmentOperation = (AssignmentOperationType) operation;
				
				List<RecordValidationType> errs = validateMassiveAccountOperation(assignmentOperation)  ;
				
				if(errs.size() == 0){
					RecordValidationType e = mcnMassiveServiceBean.saveSingleHolderAccountOperation(assignmentOperation,mcnProcessFile,indMarketFact);
					
					if(e != null){
						validation.getLstValidations().add(e);
						operation.setHasError(true);
					}
				}else{
					if(validation.getLstValidations().addAll(errs)){
						operation.setHasError(true);
					}
				}
			}
			
			validation.countOperations();
			
			// save accepted operations file
			if(validation.getAcceptedOps() > 0){
				InputStream mappingFile = Thread.currentThread().getContextClassLoader().
						getResourceAsStream(NegotiationConstant.ACCEPTED_ASSIGNMENT_STREAM);
				StringWriter sw = new StringWriter(); 
				
				//BeanIOUtils.write(val.getAcceptedOperations(),mappingFile, "file", sw);
				BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
				
				out.write("header",new AssignmentCorrectHeaderLabel());
				for (ValidationOperationType object : validation.getAcceptedOperations()) {
					out.write("assignment",(AssignmentOperationType)object);
				}
			    out.flush();
			    out.close();
			    
				mcnProcessFile.setAcceptedFile(sw.toString().getBytes("UTF-8"));
				sw.flush();
				sw.close();
				mappingFile.close();
			}
			
			// save rejected operations file
			if(validation.getRejectedOps() > 0 && Validations.validateListIsNotNullAndNotEmpty(validation.getLstValidations())){
				InputStream mappingFile = Thread.currentThread().getContextClassLoader().
						getResourceAsStream(NegotiationConstant.REJECTED_OPERATIONS_STREAM);
				StringWriter sw = new StringWriter();
				
				//BeanIOUtils.write(val.getLstErrors(),inputFile, "file", sw);
				BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
				out.write("header",new OperationIncorrectHeaderLabel());
				for (RecordValidationType object : validation.getLstValidations()) {
					out.write("error",object);
				}
			    out.flush();
			    out.close();
			    
				mcnProcessFile.setRejectedFile(sw.toString().getBytes("UTF-8"));
				sw.flush();
				sw.close();
				mappingFile.close();
			}
			
			BigDecimal acc = new BigDecimal(validation.getAcceptedOps());
			BigDecimal rej = new BigDecimal(validation.getRejectedOps());
			mcnProcessFile.setAcceptedOperations(acc);
			mcnProcessFile.setRejectedOperations(rej);
			mcnProcessFile.setTotalOperations(acc.add(rej));
			mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.CORRECT.getCode());
			//#3 separate transaction: update file independently
			mcnMassiveServiceBean.updateMcnProcessFileTx(mcnProcessFile,loggerUser);
			
        } catch (Exception e) {
			// TODO Auto-generated catch block
			mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.FAILED.getCode());
			e.printStackTrace();
		} finally {
			mcnMassiveServiceBean.updateMcnProcessFileTx(mcnProcessFile, loggerUser);
		}
	}

	/**
	 * Validate massive account operation.
	 *
	 * @param assignmentOperation the assignment operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	private List<RecordValidationType> validateMassiveAccountOperation(AssignmentOperationType assignmentOperation) throws ServiceException {
		List<RecordValidationType> lstErrors = new ArrayList<RecordValidationType>(0);
		HolderAccountOperation objHolderAccountOperation = new HolderAccountOperation();
		assignmentOperation.setHolderAccountOperation(objHolderAccountOperation);
		objHolderAccountOperation.setStockQuantity(assignmentOperation.getStockQuantity());
		objHolderAccountOperation.setHolderAccountState(HolderAccountOperationStateType.REGISTERED.getCode());
		objHolderAccountOperation.setOperationPart(ComponentConstant.CASH_PART);			
		
		if(!assignmentOperation.getReferenceDate().equals(CommonsUtilities.currentDate())){
			lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.FECHA.toString(),PropertiesConstants.MCN_MASSIVE_TRANS_DATE_INVALID));
		}
		
		ParameterTable operationType = null;
		
		//validate if operation class exists CVT/RPT
		/*ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setIndicator1(assignmentOperation.getOperationClass());
		parameterTableTO.setMasterTableFk(MasterTableType.MASSIVE_OPERATION_CLASS.getCode());
		List<ParameterTable> operationClasses = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		if(operationClasses.size() != 1){
			//La clase de operacion especificada no existe
			lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation, BbvAssignmentLabel.MODALIDAD.toString(),PropertiesConstants.MCN_MASSIVE_OPERATION_CLASS_NOT_FOUND));
		}*/
		
		MechanismModality mm = this.mcnOperationServiceBean.getMechanismModality(assignmentOperation.getIdFileMechanismPk(), assignmentOperation.getOperationClass(), 
																	null, null);

		boolean valid = false;
		if(mm != null ){
			if(mm.getIndMassiveLoad().equals(ComponentConstant.ONE)){
				valid = true;
			}
		}
		if(!valid){
			lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation, BbvAssignmentLabel.MODALIDAD.toString(),PropertiesConstants.MCN_MASSIVE_OPERATION_CLASS_NOT_FOUND));
		}
		
		//validate if operation type exists COM/COR/VEN/VER..
		ParameterTableTO parameterTableTO1 = new ParameterTableTO();
		parameterTableTO1.setIndicator1(assignmentOperation.getOperationType());
		parameterTableTO1.setMasterTableFk(MasterTableType.MASSIVE_OPERATION_TYPE.getCode());
		List<ParameterTable> operationTypes = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO1);
		if(operationTypes.size() != 1){
			lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.OPERACION.toString(),PropertiesConstants.MCN_MASSIVE_OPERATION_TYPE_NOT_FOUND));
		}else{
			operationType = operationTypes.get(0);
			if(operationType.getIndicator4()!=null && (
					operationType.getIndicator4().equals(ComponentConstant.PURCHARSE_ROLE) || operationType.getIndicator4().equals(ComponentConstant.SALE_ROLE))){
				objHolderAccountOperation.setRole(operationType.getIndicator4());
			}
		}

		//validate operationType
		/*if(operationType != null && !operationType.getText1().equals(assignmentOperation.getOperationClass())){
			lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.MODALIDAD.toString(),PropertiesConstants.MCN_MASSIVE_OPERATION_TYPE_INVALID));
		}*/
		
		// validate mechanismOperation
		Long ballotNumber = assignmentOperation.getBallotNumber();
		Long sequential = assignmentOperation.getSequential();
		MechanismOperation mechanismOperation = mcnOperationServiceBean.getMechanismOperation(ballotNumber, sequential);
		
		if(mechanismOperation==null){
			lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.PAPELETA.toString(),PropertiesConstants.MCN_MASSIVE_OPERATION_ASSIGN_NOT_FOUND));
		}else{
			//validate operationClass
			if(!assignmentOperation.getOperationClass().equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getModalityCode())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.MODALIDAD.toString(),PropertiesConstants.MCN_MASSIVE_OPERATION_CLASS_INVALID));
			}else{
				objHolderAccountOperation.setMechanismOperation(mechanismOperation);
			}
		}
		
		if(objHolderAccountOperation.getMechanismOperation()!=null && objHolderAccountOperation.getRole()!=null){
			
			MechanismOperation objMechanismOperation = objHolderAccountOperation.getMechanismOperation();
			
			Object[] securityParams = mcnOperationServiceBean.getSecurityOperationParams(objMechanismOperation.getIdMechanismOperationPk());
			String securityClass = (String)securityParams[0];
			String securityCodeOnly = (String)securityParams[1];
			if(!securityCodeOnly.toString().equals(assignmentOperation.getSecurityCode())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.VALOR_NEGOCIADO.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_CODE_NOT_OPERATION));
			} 
			
			if(!securityClass.toString().equals(assignmentOperation.getSecurityClass())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.TIPO_VALOR_NEGOCIADO.toString(),PropertiesConstants.MCN_MASSIVE_SECURITY_CODE_NOT_OPERATION));
			}
			
			// get AssignmentRequest
			AssignmentRequest assignmentRequest = accountAssignmentService.getAssignmentRequest(objMechanismOperation.getIdMechanismOperationPk(), objHolderAccountOperation.getRole());
			objHolderAccountOperation.setAssignmentRequest(assignmentRequest);
			
			//validate participant assignment
			ParticipantAssignment participantAssignment = objHolderAccountOperation.getAssignmentRequest().getParticipantAssignment();
			if(!ParticipantAssignmentStateType.OPENED.getCode().equals(participantAssignment.getAssignmentState())){
				lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.CTA_MATRIZ.toString(),PropertiesConstants.MCN_MASSIVE_PART_ASSIGNMENT_CLOSED));
			}
			
			//validate participant
			Participant assignParticipant = new Participant();
			assignParticipant.setMnemonic(assignmentOperation.getAssignmentPartMnemonic());
			assignParticipant = participantServiceBean.findParticipantByFilters(assignParticipant);
			
			if(assignParticipant==null){
				lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.CTA_MATRIZ.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_NOT_FOUND));
			}else{
				
				//validate if participant assignment in file is the same as user participant
				if(assignmentOperation.getIdFileParticipantPk()!=null &&  !assignParticipant.getIdParticipantPk().equals(assignmentOperation.getIdFileParticipantPk())){
					lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.CTA_MATRIZ.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_OTC_NOT_VALID));
					return lstErrors; // returns as critical error.
				}
				
				//validate participant is in correct role
				if(objHolderAccountOperation.getRole().equals(ComponentConstant.PURCHARSE_ROLE)){
					if(!assignParticipant.getIdParticipantPk().equals(objMechanismOperation.getBuyerParticipant().getIdParticipantPk())){
						lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.CTA_MATRIZ.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_INVALID_PURCH_ROLE));
						assignParticipant = null;
						return lstErrors;
					}
				}else if(objHolderAccountOperation.getRole().equals(ComponentConstant.SALE_ROLE)){
					if(!assignParticipant.getIdParticipantPk().equals(objMechanismOperation.getSellerParticipant().getIdParticipantPk())){
						lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.CTA_MATRIZ.toString(),PropertiesConstants.MCN_MASSIVE_PARTICIPANT_INVALID_SALE_ROLE));
						assignParticipant = null;
						return lstErrors;
					}
					// validate primary placement in sale role
					if(ComponentConstant.ONE.equals(objMechanismOperation.getIndPrimaryPlacement())){
						lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation, BbvAssignmentLabel.COMITENTE.toString(), PropertiesConstants.MCN_MASSIVE_PRIMARY_PLACEMENT_ROLE_SALE));
						return lstErrors;
					}
					
				}
				
				HolderAccountTO holderAccountTO = new HolderAccountTO();
				holderAccountTO.setHolderAccountNumber(assignmentOperation.getAccountNumber());
				holderAccountTO.setParticipantTO(assignParticipant.getIdParticipantPk());
				HolderAccount holderAccount = accountAssignmentService.getHolderAccountComponentServiceBean(holderAccountTO);
				
				if(holderAccount==null){
					lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.COMITENTE.toString(),PropertiesConstants.MCN_MASSIVE_HOLDER_ACCOUNT_NOT_FOUND));
				}else{
					objHolderAccountOperation.setHolderAccount(holderAccount);
					
					//Validate Incharge Agreement
					
					//verificamos si existe cuenta 
					if(holderAccount!=null && (holderAccount.getAccountType().equals(HolderAccountType.JURIDIC.getCode())
							|| holderAccount.getAccountType().equals(HolderAccountType.NATURAL.getCode()))){
						boolean rolBuy=false;
						boolean rolSell=false;
						if(objHolderAccountOperation.getRole().equals(ParticipantRoleType.BUY.getCode())){
							rolBuy=true;
						}
						if(objHolderAccountOperation.getRole().equals(ParticipantRoleType.SELL.getCode())){
							rolSell=true;
						}
						List<InchargeAgreement>  lstInchargeAgreement = (List<InchargeAgreement>)inchargeAgreementFacade.getInchargeAgreementForFilter
								(objHolderAccountOperation.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationMechanismPk(),
										objHolderAccountOperation.getMechanismOperation().getMechanisnModality().getId().getIdNegotiationModalityPk(),
										rolBuy,rolSell, 
										assignParticipant.getIdParticipantPk(),null,
										holderAccount.getHolderAccountDetails().get(0).getHolder().getIdHolderPk());
						//verificamos si existe contrato de encargo
						if(Validations.validateIsNotNull(lstInchargeAgreement) &&
								lstInchargeAgreement.size() == GeneralConstants.ONE_VALUE_INTEGER){
							objHolderAccountOperation.setIndIncharge(ComponentConstant.ONE);
							InchargeAgreement inchargeAgreement = (InchargeAgreement)lstInchargeAgreement.get(GeneralConstants.ZERO_VALUE_INTEGER);
							//verificamos si existe cuenta en participante negociador
							List<HolderAccount> lstResultAux =findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
									inchargeAgreement.getTraderParticipant().getIdParticipantPk());
							if(Validations.validateListIsNullOrEmpty(lstResultAux)){
								lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.COMITENTE.toString(),PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_TRADE_AGREEMENT));
								return lstErrors; 
							}
							//verificamos si existe cuenta en participante encargado
							List<HolderAccount> lstResult = findHolderAccountsForInchargeAgreement(inchargeAgreement.getHolder().getIdHolderPk(), 
									inchargeAgreement.getInchargeParticipant().getIdParticipantPk());
							if(Validations.validateListIsNullOrEmpty(lstResult)){
								lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.COMITENTE.toString(),PropertiesConstants.ASSIGNMENT_ACCOUNT_HOLDER_NOT_ACCOUNTS_PARTICIPANT_INCHARGE_AGREEMENT));
								return lstErrors; 
							}
							if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)
									&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)){
								objHolderAccountOperation.setInchargeFundsParticipant(inchargeAgreement.getInchargeParticipant());
								objHolderAccountOperation.setInchargeStockParticipant(inchargeAgreement.getInchargeParticipant());
							}
							if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ZERO_VALUE_INTEGER)
									&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)){
								objHolderAccountOperation.setInchargeStockParticipant(inchargeAgreement.getInchargeParticipant());
								objHolderAccountOperation.setInchargeFundsParticipant(inchargeAgreement.getTraderParticipant());
							}
							if(inchargeAgreement.getIndFundsIncharge().equals(GeneralConstants.ONE_VALUE_INTEGER)
									&& inchargeAgreement.getIndStockIncharge().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
								objHolderAccountOperation.setInchargeFundsParticipant(inchargeAgreement.getInchargeParticipant());
								objHolderAccountOperation.setInchargeStockParticipant(inchargeAgreement.getTraderParticipant());
							}
						}
						else {
							objHolderAccountOperation.setIndIncharge(ComponentConstant.ZERO);
							objHolderAccountOperation.setInchargeStockParticipant(assignParticipant);
							objHolderAccountOperation.setInchargeFundsParticipant(assignParticipant);		
						}			
					}else {
						objHolderAccountOperation.setIndIncharge(ComponentConstant.ZERO);
						objHolderAccountOperation.setInchargeStockParticipant(assignParticipant);
						objHolderAccountOperation.setInchargeFundsParticipant(assignParticipant);		
					}
				}
					
			}
					
			BigDecimal stockQuantity = objHolderAccountOperation.getStockQuantity();
			objHolderAccountOperation.setCashAmount(NegotiationUtils.getSettlementAmount(objMechanismOperation.getCashPrice(),stockQuantity));
			objHolderAccountOperation.setSettlementAmount(NegotiationUtils.getSettlementAmount(objMechanismOperation.getCashSettlementPrice(),stockQuantity));
			
			// validate primary placement
			/*
			if(ComponentConstant.ONE.equals(objMechanismOperation.getIndPrimaryPlacement())){
				BigDecimal amountEntered = stockQuantity.multiply(objMechanismOperation.getSecurities().getCurrentNominalValue());
			  	if(objMechanismOperation.getSecurities().getMinimumInvesment().compareTo(amountEntered) == 1){
			  		lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.VALOR_NEGOCIADO.toString(),PropertiesConstants.OTC_OPERATION_VALIDATION_RANGE_AMOUTNS));
			  	}else if(objMechanismOperation.getSecurities().getMaximumInvesment().compareTo(amountEntered) == -1){
			  		lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.VALOR_NEGOCIADO.toString(),PropertiesConstants.OTC_OPERATION_VALIDATION_RANGE_MAX_AMOUTNS));
			    }
			}*/
			
			//validate and add market facts
			if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact()) && 
					ComponentConstant.SALE_ROLE.equals(objHolderAccountOperation.getRole())){
				
				if(assignmentOperation.getMarketDate() == null){
					lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.FECHAUH.toString(),PropertiesConstants.MCN_MASSIVE_ASSIGN_MARKET_DATE_NULL));
				}else{
					
					objHolderAccountOperation.setAccountOperationMarketFacts(new ArrayList<AccountOperationMarketFact>());
					AccountOperationMarketFact objAccountMarketFact = new AccountOperationMarketFact();
					objAccountMarketFact.setHolderAccountOperation(objHolderAccountOperation);
					objAccountMarketFact.setMarketDate(assignmentOperation.getMarketDate());
					objAccountMarketFact.setOperationQuantity(objHolderAccountOperation.getStockQuantity());
					
					Integer instrumentType = objMechanismOperation.getMechanisnModality().getNegotiationModality().getInstrumentType();
					if(instrumentType.equals(InstrumentType.FIXED_INCOME.getCode())){
						//if(assignmentOperation.getMarketRate() == null || assignmentOperation.getMarketRate().compareTo(BigDecimal.ZERO) == 0){
						if(Validations.validateIsNull(assignmentOperation.getMarketRate())) {
							lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.TASAUH.toString(),PropertiesConstants.MCN_MASSIVE_ASSIGN_MARKET_RATE_NULL));
						}else{
							objAccountMarketFact.setMarketRate(assignmentOperation.getMarketRate());
							objHolderAccountOperation.getAccountOperationMarketFacts().add(objAccountMarketFact);
						}
						
					}else{
						if(assignmentOperation.getMarketPrice() == null || assignmentOperation.getMarketPrice().compareTo(BigDecimal.ZERO) <= 0){
							lstErrors.add(CommonsUtilities.populateRecordValidation(assignmentOperation,BbvAssignmentLabel.PRECIOUH.toString(),PropertiesConstants.MCN_MASSIVE_ASSIGN_MARKET_PRICE_NULL));
						}else{
							objAccountMarketFact.setMarketPrice(assignmentOperation.getMarketPrice().setScale(8,RoundingMode.HALF_UP));
							objHolderAccountOperation.getAccountOperationMarketFacts().add(objAccountMarketFact);
						}
						
					}
				}
				
			}
		}
			
		return lstErrors;
	}
	

	/**
	 * Validate assignment conditions.
	 *
	 * @param holderAccountOperation the holder account operation
	 * @param existingAccountOperations the existing account operations
	 * @throws ServiceException the service exception
	 */
	public void validateAssignmentConditions(HolderAccountOperation holderAccountOperation, List<HolderAccountOperation> existingAccountOperations) throws ServiceException {
		accountAssignmentService.validateAssignmentConditions(holderAccountOperation, existingAccountOperations);
	}

	/**
	 * Validate registered market facts.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @throws ServiceException the service exception
	 */
	public void validateRegisteredMarketFacts(Long assignmentProcessId, Long participantAssignmentId) throws ServiceException{
		accountAssignmentService.validateRegisteredMarketFacts(assignmentProcessId,participantAssignmentId);
	}
	
	/**
	 * Validate pending assignments.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @param Role the role
	 * @throws ServiceException the service exception
	 */
	public void validatePendingAssignments(Long assignmentProcessId,Long participantAssignmentId,Integer Role) throws ServiceException{
		 accountAssignmentService.validatePendingAssignments(assignmentProcessId,participantAssignmentId,Role);
	}
	
//	/**
//	 * Validate pending assignments.
//	 *
//	 * @param assignmentProcessId the assignment process id
//	 * @param participantAssignmentId the participant assignment id
//	 * @param Role the role
//	 * @throws ServiceException the service exception
//	 */
//	public String validatePendingAssignments(Long assignmentProcessId,Long participantAssignmentId,Integer Role) throws ServiceException{
//		return accountAssignmentService.validatePendingAssignments(assignmentProcessId,participantAssignmentId,Role);
//	}
	
	
	/**
	 * Validate incomplete securities.
	 *
	 * @param assignmentProcessId the assignment process id
	 * @param participantAssignmentId the participant assignment id
	 * @param idParticipant the participant assignment id
	 * @param Role the role
	 * @throws ServiceException the service exception
	 */
	public String validateIncompleteSecurities(Long negotiationModality,Long participantAssignmentId,Long idParticipant,Integer role, Date dateSelected) throws ServiceException{
		return accountAssignmentService.validateIncompleteSecurities(negotiationModality,participantAssignmentId,idParticipant,role,dateSelected);
	}

	/**
	 * Gets the holder account operations.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the holder account operations
	 */
	public List<HolderAccountOperation> getHolderAccountOperations(Long idMechanismOperation, List<Integer> role, Long idParticipant) {
		return accountAssignmentService.getHolderAccountOperations(idMechanismOperation, role, idParticipant );
	}
	
	/**
	 * Gets the reassignment details.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @return the reassignment details
	 */
	public List<ReassignmentRequestDetail> getReassignmentDetails(Long idReassignmentRequest) {
		return accountAssignmentService.getReassignmentDetails(idReassignmentRequest);
	}
	
	/**
	 * Check previous reassignment request.
	 *
	 * @param idMechanismOperationPk the id mechanism operation pk
	 * @param idParticipantPk the id participant pk
	 * @throws ServiceException the service exception
	 */
	public void checkPreviousReassignmentRequest(Long idMechanismOperationPk, Long idParticipantPk) throws ServiceException {
		accountAssignmentService.checkPreviousReassignmentRequest(idMechanismOperationPk, idParticipantPk);
	}
	
	/**
	 * Check assignment process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param expectedState the expected state
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void checkAssignmentProcess(Long idMechanism, Long idModality, Integer expectedState, Date settlementDate) throws ServiceException {
		accountAssignmentService.checkAssignmentProcess(idMechanism,idModality,expectedState,settlementDate);
	}
	
	/**
	 * Check pending settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @throws ServiceException the service exception
	 */
	public void checkPendingSettlementProcess(Long idMechanism,Long idModality, Integer idCurrency, Date settlementDate) throws ServiceException {
		settlementProcessService.checkPendingSettlementProcess(idMechanism,idModality, idCurrency, settlementDate );
	}
	
	/**
	 * Check exist chained account.
	 *
	 * @param idHolderAccountOperation the id holder account operation
	 * @throws ServiceException the service exception
	 */
	public void checkExistChainedAccount(Long idHolderAccountOperation) throws ServiceException {
		accountAssignmentService.checkExistChainedAccount(idHolderAccountOperation);
	}
	
	/**
	 * Check finished settlement process.
	 *
	 * @param idMechanism the id mechanism
	 * @param idModality the id modality
	 * @param idCurrency the id currency
	 * @param settlementDate the settlement date
	 * @param scheduleType the schedule type
	 * @throws ServiceException the service exception
	 */
	public void checkFinishedSettlementProcess(Long idMechanism, Long idModality, Integer idCurrency, Date settlementDate,Integer scheduleType) throws ServiceException {
		settlementProcessService.checkFinishedSettlementProcess(idMechanism,idModality, idCurrency, settlementDate , scheduleType);
	}
	
	/**
	 * Gets the holder account operation detail.
	 *
	 * @param idSettlementAccount the id settlement account
	 * @return the holder account operation detail
	 * @throws ServiceException the service exception
	 */
	public HolderAccountOperation getHolderAccountOperationDetail(Long idSettlementAccount) throws ServiceException{
		return accountAssignmentService.getHolderAccountOperationDetail(idSettlementAccount);
	}

	/**
	 * Save reassignment request.
	 *
	 * @param reassignmentRequest the reassignment request
	 * @param lstAccountOperations the lst account operations
	 * @param lstDeletedAccounts the lst deleted accounts
	 * @throws ServiceException the service exception
	 */
	public void saveReassignmentRequest( ReassignmentRequest reassignmentRequest,
			List<HolderAccountOperation> lstAccountOperations, List<HolderAccountOperation> lstDeletedAccounts) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		accountAssignmentService.saveReassignmentRequest(reassignmentRequest,lstAccountOperations,lstDeletedAccounts,loggerUser);
	}

	/**
	 * Search reassignment requests.
	 *
	 * @param searchAccountReassignmentTO the search account reassignment to
	 * @return the list
	 */
	public List<AccountReassignmentTO> searchReassignmentRequests(SearchAccountReassignmentTO searchAccountReassignmentTO) {
		return accountAssignmentService.searchReassignmentRequests(searchAccountReassignmentTO);
	}

	/**
	 * Reject reassignment request.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param details the details
	 * @throws ServiceException the service exception
	 */
	public void rejectReassignmentRequest(Long idReassignmentRequest, MechanismOperation mechanismOperation, List<ReassignmentRequestDetail> details) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		accountAssignmentService.updateStateReassignmentRequest(idReassignmentRequest, mechanismOperation, ReassignmentRequestStateType.REJECTED.getCode(), loggerUser, details);
		
	}
	
	/**
	 * Confirm reassignment request.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param details the details
	 * @throws ServiceException the service exception
	 */
	public void confirmReassignmentRequest(Long idReassignmentRequest, MechanismOperation mechanismOperation, List<ReassignmentRequestDetail> details) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		accountAssignmentService.updateStateReassignmentRequest(idReassignmentRequest, mechanismOperation, ReassignmentRequestStateType.CONFIRMED.getCode(), loggerUser, details);
	}
	
	public void authorizeReassignmentRequest(Long idReassignmentRequest, MechanismOperation mechanismOperation, List<ReassignmentRequestDetail> details) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());
		
		accountAssignmentService.updateStateReassignmentRequest(idReassignmentRequest, mechanismOperation, ReassignmentRequestStateType.AUTHORIZED.getCode(), loggerUser, details);
	}
	
	
	/**
	 * Search incharge requests.
	 *
	 * @param searchAssignmentRequestTO the search assignment request to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.INCHARGE_REQUEST_MANAGEMENT)
	public List<AccountAssignmentTO> searchInchargeRequests(SearchAssignmentRequestTO searchAssignmentRequestTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return accountAssignmentService.searchInchargeRequests(searchAssignmentRequestTO);
	}
	
	/**
	 * Confirm incharge request.
	 *
	 * @param tmpAccountOperation the tmp account operation
	 * @param holderAccount the holder account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.INCHARGE_REQUEST_CONFIRM)
	public void confirmInchargeRequest(HolderAccountOperation tmpAccountOperation, HolderAccount holderAccount) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());	   		
		
		accountAssignmentService.confirmInchargeAssignment(tmpAccountOperation, holderAccount, loggerUser);
	}
	
	/**
	 * To reject the In Charge Operation.
	 *
	 * @param tmpAccount the tmp account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.INCHARGE_REQUEST_REJECT)
	public void rejectInchargeRequest(HolderAccountOperation tmpAccount) throws ServiceException{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());	
		
		accountAssignmentService.rejectInchargeAssignment(tmpAccount, loggerUser);
	}


	/**
	 * Confirm incharge massive request.
	 *
	 * @param tmpAccountOperation the tmp account operation
	 * @param holderAccount the holder account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.INCHARGE_REQUEST_CONFIRM)
	public void confirmInchargeMassiveRequest(HolderAccountOperation tmpAccountOperation, HolderAccount holderAccount) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());	   		
		
		accountAssignmentService.confirmInchargeAssignment(tmpAccountOperation, holderAccount, loggerUser);
	}
	
	/**
	 * To reject the In Charge Operation.
	 *
	 * @param tmpAccount the tmp account
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.INCHARGE_REQUEST_REJECT)
	public void rejectInchargeMassiveRequest(HolderAccountOperation tmpAccount) throws ServiceException{	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());	
		
		accountAssignmentService.rejectInchargeAssignment(tmpAccount, loggerUser);
	}
	
	/**
	 * Close participant assignment process.
	 *
	 * @param processDate the process date
	 */
	public void closeParticipantAssignmentProcess(Date processDate) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		List<Long> lstParticipantAssignment= accountAssignmentService.getListParticipantAssignment(processDate);
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstParticipantAssignment)) {
			accountAssignmentService.updateParticipantAssignment(lstParticipantAssignment, ParticipantAssignmentStateType.CLOSED.getCode(), loggerUser);
		}
	}
	
	/**
	 * Synchronize settlement account marketfact.
	 *
	 * @param idAssignmentProcess the id assignment process
	 * @throws ServiceException the service exception
	 */
	public void synchronizeSettlementAccountMarketfact(Long idAssignmentProcess) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		accountAssignmentService.synchronizeSettlementAccountMarketfact(idAssignmentProcess, loggerUser);
	}
	
	
	
	/**
	 * Distribute, market events according to the following rule:
	 * <br>1. If it is fixed income: Sort market events by date
	 * <br>2. If is variable income: Sort the events market for the highest price.
	 * <br>3. Spread the facts according to assigned market operations from highest to lowest according to pk the operation.
	 *
	 * @param listAccountAssignmentTO the list account assignment to
	 * @param listAccountOperationMarketFact the list account operation market fact
	 * @param blFixedIncome Instrument Type; true Fixed, false: Variable
	 * @return the map
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public Map<Long,List<AccountOperationMarketFact>> calculateMarketFact(List<AccountAssignmentTO> listAccountAssignmentTO,
			  List<AccountOperationMarketFact> listAccountOperationMarketFact,
			  Boolean blFixedIncome
				){
		Map<Long,List<AccountOperationMarketFact>> mapMarketFact=new HashMap<Long,List<AccountOperationMarketFact>>();
		Map<Long,BigDecimal> stockQuantity= new HashMap<Long,BigDecimal>();
		
		if(Validations.validateListIsNullOrEmpty(listAccountOperationMarketFact)){
			return mapMarketFact;
		}
		/**
		 * Sort Operation by pk the Operation
		 */
		Collections.sort(listAccountAssignmentTO, new Comparator<AccountAssignmentTO>() {

			@Override
			public int compare(AccountAssignmentTO accountAssignment1, AccountAssignmentTO accountAssignment2) {
				
				Long holderAccountOperation1=accountAssignment1.getIdHolderAccountOperation();
				Long holderAccountOperation2=accountAssignment2.getIdHolderAccountOperation();
				
				return holderAccountOperation1.compareTo(holderAccountOperation2);
				
			}
		});
		for (AccountAssignmentTO accountAssignment : listAccountAssignmentTO) {
			stockQuantity.put(accountAssignment.getIdHolderAccountOperation(), accountAssignment.getAccountStockQuantity());
		}
		
		
		/**
		 * FIXED_INCOME, sort by date
		 */
		if(blFixedIncome){
			Collections.sort(listAccountOperationMarketFact, new Comparator<AccountOperationMarketFact>() {

				@Override
				public int compare(AccountOperationMarketFact o1,
						AccountOperationMarketFact o2) {
					Date dateO1=o1.getMarketDate();
					Date dateO2=o2.getMarketDate();
					return dateO1.compareTo(dateO2);
				}});
			
		}else{
			/**
			 * VARIABLE_INCOME, Order by highest price
			 */
			Collections.sort(listAccountOperationMarketFact, new Comparator<AccountOperationMarketFact>() {

				@Override
				public int compare(AccountOperationMarketFact o1,
						AccountOperationMarketFact o2) {
					
					BigDecimal marketPriceO1=o1.getMarketPrice();
					BigDecimal marketPriceO2=o2.getMarketPrice();
					return marketPriceO1.compareTo(marketPriceO2);
				}});
			
		}
		
		
		Iterator it = stockQuantity.keySet().iterator();
		Boolean blOtherAccountAssignment=Boolean.FALSE;
		while(it.hasNext()){
		  Long key = (Long) it.next();
		  BigDecimal stockQuantityTmp=stockQuantity.get(key);
		  
		  for (AccountOperationMarketFact accountOperationMarketFact : listAccountOperationMarketFact) {
			BigDecimal marketFactTmp=accountOperationMarketFact.getOperationQuantity();
			
			if(Validations.validateIsNull(marketFactTmp)||marketFactTmp.compareTo(BigDecimal.ZERO)<=0){
				continue;
			}
			
			if (stockQuantityTmp.compareTo(marketFactTmp)>=0) {
				//Assign all the amount of MarketFact
				AccountOperationMarketFact marketFactClone=SerializationUtils.clone(accountOperationMarketFact);
				marketFactClone.setOperationQuantity(marketFactTmp);
				accountOperationMarketFact.setOperationQuantity(BigDecimal.ZERO);
				stockQuantityTmp=CommonsUtilities.subtractTwoDecimal(stockQuantityTmp, marketFactTmp, GeneralConstants.ONE_VALUE_INTEGER);
				if(mapMarketFact.containsKey(key)){
					List<AccountOperationMarketFact> listAccOperatioMarketFact=mapMarketFact.get(key);
					listAccOperatioMarketFact.add(marketFactClone);
					mapMarketFact.put(key, listAccOperatioMarketFact);
				}else{
					mapMarketFact.put(key, new ArrayList<>(Arrays.asList(marketFactClone)));
				}
				blOtherAccountAssignment=Boolean.FALSE;

			}else if(stockQuantityTmp.compareTo(marketFactTmp)<0){
				//Assign only the amount
				AccountOperationMarketFact marketFactClone=SerializationUtils.clone(accountOperationMarketFact);
				marketFactClone.setOperationQuantity(stockQuantityTmp);
				accountOperationMarketFact.setOperationQuantity(CommonsUtilities.subtractTwoDecimal(marketFactTmp,stockQuantityTmp , 1));
				
				if(mapMarketFact.containsKey(key)){
					List<AccountOperationMarketFact> listAccOperatioMarketFact=mapMarketFact.get(key);
					listAccOperatioMarketFact.add(marketFactClone);
					mapMarketFact.put(key, listAccOperatioMarketFact);
				}else{
					mapMarketFact.put(key, new ArrayList<>(Arrays.asList(marketFactClone)));
				}
				
				blOtherAccountAssignment=Boolean.TRUE;
				break;
			}
			
			
		}
//			if(blOtherAccountAssignment){
//				break;
//			}
//		  
		}
		
		return mapMarketFact;


	}
	
	/**
	 * Find if The Holder have Market Fact.
	 *
	 * @param marketFactBalance the market fact balance
	 * @return the market fact balance help to
	 */
	public MarketFactBalanceHelpTO findMarketBalance(MarketFactBalanceHelpTO marketFactBalance){
		
		MarketFactBalanceHelpTO marketBalanceFinded=null;
		
		try{
				
			/*ATRIBUTO PARA MANEJAR SI HECHOS DE MERCADO SON LLENADAS ANTES DE USAR EL COMPONENTE*/
			if(marketFactBalance.getIndHandleDetails()!=null && marketFactBalance.getIndHandleDetails().equals(BooleanType.YES.getCode())){
				marketBalanceFinded = marketFactBalance;
			}else{
				marketBalanceFinded = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
			}
		}
		catch(ServiceException e){
			e.printStackTrace();
		}
		
		
		return marketBalanceFinded;
	}
	
	/**
	 * Crear en listas segun el Numero de elementos que debe tener cada lista.
	 *
	 * @param listValidationOperationType the list validation operation type
	 * @return the map
	 */
	public Map<Integer,List<ValidationOperationType>> generateListValidationsByNumber(List<ValidationOperationType> listValidationOperationType){
		
		/**Map with List ValidationOperationType**/
		Map<Integer, List<ValidationOperationType>> mapListValidations=new HashMap<Integer, List<ValidationOperationType>>();		
		/**Variable TO DB**/
		Integer numberThread=null;
		numberThread=200;//valor parametrico
		
		if(Validations.validateListIsNotNullAndNotEmpty(listValidationOperationType)){
			if(listValidationOperationType.size()<=numberThread){
				mapListValidations.put(0, listValidationOperationType);
			}
		}else{
			for (int i = 0; i < listValidationOperationType.size();) {
				int ifinal=i+numberThread-1;
				 if(ifinal>=listValidationOperationType.size()){
					 ifinal=listValidationOperationType.size();
				 }
				 mapListValidations.put(i, listValidationOperationType.subList(i, ifinal));
				 i=i+numberThread;
			}
		}
		
		
		return mapListValidations;
	}
	
	/**
	 * Crear en listas segun el numero de Hilos que se quieren enviar .
	 *
	 * @param listValidationOperationType the list validation operation type
	 * @return the map
	 */
	@SuppressWarnings("unused")
	public Map<Integer,List<ValidationOperationType>> generateListValidationsByThreads(List<ValidationOperationType> listValidationOperationType){
		/**Set List */
		List<ValidationOperationType> listValidationOperationTypeSending=new ArrayList<ValidationOperationType>();
		Map<Integer,List<ValidationOperationType>> listValidations=new HashMap<Integer,List<ValidationOperationType>>();
		/**Variable TO DB**/
		Integer numberThread=null;	
		/**Map with List ValidationOperationType**/
		Map<Integer, List<ValidationOperationType>> mapListValidations=new HashMap<Integer, List<ValidationOperationType>>();
		numberThread=15;//valor parametrico
//		log.debug(" Number the Matrix ::: "+listValidationOperationType.size());

		/**split  quotient**/
		int quotient=listValidationOperationType.size()/numberThread;
		/**Mod Size Matrix List and Number Thread*/
		int residue=listValidationOperationType.size()%numberThread;

		/**
		 * Split the list in numberThread, parameter database
		 * if quotient >0 create list and residue
		 * Ex: listValidationOperationType.size()=10
		 * 
		 * */
		if(quotient>0){
			
			for (int i = 0; i < listValidationOperationType.size()&& listValidationOperationType.size()>=i+numberThread; i=i+numberThread) {
				if(i==0){
					for (int j = 0; j < numberThread; j++) {
						listValidationOperationTypeSending=new ArrayList<ValidationOperationType>();
						listValidationOperationTypeSending.add(listValidationOperationType.get(i+j));
						mapListValidations.put(j, listValidationOperationTypeSending);
						
					}
				}else{
					for (int j = 0; j < numberThread; j++) {
						listValidationOperationTypeSending=mapListValidations.get(j);
						listValidationOperationTypeSending.add(listValidationOperationType.get(i+j));
						mapListValidations.put(j, listValidationOperationTypeSending);
						
					}
				}
				
			}

			for(int i=0;i<residue;i++){
				listValidationOperationTypeSending=mapListValidations.get(i);
				listValidationOperationTypeSending.add(listValidationOperationType.get(i+quotient*numberThread));
				mapListValidations.put(i, listValidationOperationTypeSending);
			}

		}
		else{
			for(int i=0;i<residue;i++){
				listValidationOperationTypeSending=new ArrayList<ValidationOperationType>();
				listValidationOperationTypeSending.add(listValidationOperationType.get(i));
				mapListValidations.put(i, listValidationOperationTypeSending);
			}
		}
		
		return mapListValidations;
	}
	
	/**
	 * Save operation independent by batch.
	 *
	 * @param listValidations the list validations
	 * @param mcnProcessFile the mcn process file
	 * @param indMarketFact the ind market fact
	 * @param validation the validation
	 * @param loggerUser the logger user
	 */
	public void saveOperationIndependentByBatch(List<ValidationOperationType> listValidations,
			McnProcessFile mcnProcessFile, Integer indMarketFact, ValidationProcessTO validation,LoggerUser loggerUser ){


		try {
			// #2 separate transaction: save operations independently
			for(ValidationOperationType operation : listValidations){
			AssignmentOperationType assignmentOperation = (AssignmentOperationType) operation;
			
			List<RecordValidationType> errs = validateMassiveAccountOperation(assignmentOperation)  ;
			
			if(errs.size() == 0){//Si no hay Errores de Validacion
				RecordValidationType e = mcnMassiveServiceBean.saveSingleHolderAccountOperation(assignmentOperation,mcnProcessFile,indMarketFact);
				
				if(e != null){
					validation.getLstValidations().add(e);
					operation.setHasError(true);
				}
			}else{
				if(validation.getLstValidations().addAll(errs)){
				operation.setHasError(true);
				}
			}
			}
			
			validation.countOperations();
			
			// save accepted operations file
			if(validation.getAcceptedOps() > 0){
			InputStream mappingFile = Thread.currentThread().getContextClassLoader().
			getResourceAsStream(NegotiationConstant.ACCEPTED_ASSIGNMENT_STREAM);
			StringWriter sw = new StringWriter(); 
			
			//BeanIOUtils.write(val.getAcceptedOperations(),mappingFile, "file", sw);
			BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
			
			out.write("header",new AssignmentCorrectHeaderLabel());
			for (ValidationOperationType object : validation.getAcceptedOperations()) {
			out.write("assignment",(AssignmentOperationType)object);
			}
			out.flush();
			out.close();
			
			mcnProcessFile.setAcceptedFile(sw.toString().getBytes("UTF-8"));
			sw.flush();
			sw.close();
			mappingFile.close();
			}
			
			// save rejected operations file
			if(validation.getRejectedOps() > 0 && Validations.validateListIsNotNullAndNotEmpty(validation.getLstValidations())){
			InputStream mappingFile = Thread.currentThread().getContextClassLoader().
			getResourceAsStream(NegotiationConstant.REJECTED_OPERATIONS_STREAM);
			StringWriter sw = new StringWriter();
			
			//BeanIOUtils.write(val.getLstErrors(),inputFile, "file", sw);
			BeanWriter out = BeanIOUtils.createWriter(mappingFile, "file", sw);
			out.write("header",new OperationIncorrectHeaderLabel());
			for (RecordValidationType object : validation.getLstValidations()) {
			out.write("error",object);
			}
			out.flush();
			out.close();
			
			mcnProcessFile.setRejectedFile(sw.toString().getBytes("UTF-8"));
			sw.flush();
			sw.close();
			mappingFile.close();
			}
			
			BigDecimal acc = new BigDecimal(validation.getAcceptedOps());
			BigDecimal rej = new BigDecimal(validation.getRejectedOps());
			mcnProcessFile.setAcceptedOperations(acc);
			mcnProcessFile.setRejectedOperations(rej);
			mcnProcessFile.setTotalOperations(acc.add(rej));
			mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.CORRECT.getCode());
			//#3 separate transaction: update file independently
			mcnMassiveServiceBean.updateMcnProcessFileTx(mcnProcessFile,loggerUser);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			mcnProcessFile.setProcessState(ExternalInterfaceReceptionStateType.FAILED.getCode());
			e.printStackTrace();
		} finally {
			mcnMassiveServiceBean.updateMcnProcessFileTx(mcnProcessFile, loggerUser);
		}

	}
	
	/**
	 * Gets the holder account operations market fact.
	 *
	 * @param idMechanismOperation the id mechanism operation
	 * @param role the role
	 * @param idParticipant the id participant
	 * @return the holder account operations market fact
	 */
	public List<SettlementAccountMarketfact> getSettlementAccountMarketfactByReassignment(Long idMechanismOperation, List<Integer> role, Long idParticipant) {
		return accountAssignmentService.getSettlementAccountMarketfactByReassignment(idMechanismOperation, role, idParticipant);
	}
	
	/**
	 * Save reassignment request.
	 *
	 * @param reassignmentRequest the reassignment request
	 * @param lstSettlementAccountMarketfact the lst settlement account marketfact
	 * @param lstSettlementAccountMarketfactInitial the lst settlement account marketfact initial
	 * @param isFixedIncome the is fixed income
	 * @throws ServiceException the service exception
	 */
	public void saveReassignmentRequestMarketFact( ReassignmentRequest reassignmentRequest,	List<SettlementAccountMarketfact> lstSettlementAccountMarketfact, 
			List<SettlementAccountMarketfact> lstSettlementAccountMarketfactInitial, boolean isFixedIncome) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		accountAssignmentService.saveReassignmentRequestMarketFact(reassignmentRequest, lstSettlementAccountMarketfact, 
				lstSettlementAccountMarketfactInitial, isFixedIncome, loggerUser);
	}
	
	/**
	 * Authorize reassignment request market fact.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param details the details
	 * @param isFixedIncome the is fixed income
	 * @throws ServiceException the service exception
	 */
	public void authorizeReassignmentRequestMarketFact(Long idReassignmentRequest, MechanismOperation mechanismOperation, 
			List<ReassignmentRequestDetail> details, boolean isFixedIncome) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());		
		accountAssignmentService.updateStateReassignmentRequestMarketFact(idReassignmentRequest, mechanismOperation, 
				ReassignmentRequestStateType.AUTHORIZED.getCode(), loggerUser, details, isFixedIncome);
	}
	
	/**
	 * Confirm reassignment request market fact.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param details the details
	 * @param isFixedIncome the is fixed income
	 * @throws ServiceException the service exception
	 */
	public void confirmReassignmentRequestMarketFact(Long idReassignmentRequest, MechanismOperation mechanismOperation, 
			List<ReassignmentRequestDetail> details, boolean isFixedIncome) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());		
		accountAssignmentService.updateStateReassignmentRequestMarketFact(idReassignmentRequest, mechanismOperation, 
				ReassignmentRequestStateType.CONFIRMED.getCode(), loggerUser, details, isFixedIncome);
	}
	
	/**
	 * Reject reassignment request market fact.
	 *
	 * @param idReassignmentRequest the id reassignment request
	 * @param mechanismOperation the mechanism operation
	 * @param details the details
	 * @param isFixedIncome the is fixed income
	 * @throws ServiceException the service exception
	 */
	public void rejectReassignmentRequestMarketFact(Long idReassignmentRequest, MechanismOperation mechanismOperation,
			List<ReassignmentRequestDetail> details, boolean isFixedIncome) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());		
		accountAssignmentService.updateStateReassignmentRequestMarketFact(idReassignmentRequest, mechanismOperation, 
				ReassignmentRequestStateType.REJECTED.getCode(), loggerUser, details, isFixedIncome);
		
	}
	
	/**
	 * Gets the list mnemonic participant assignment.
	 *
	 * @param processDate the process date
	 * @return the list mnemonic participant assignment
	 */
	public List<String> getListMnemonicParticipantAssignment(Date processDate) {
		return accountAssignmentService.getListMnemonicParticipantAssignment(processDate);
	}
	
	/**
	 * Llenamos por defecto al participante encargado que se encuentre configurado en la tabla idepositarySetup
	 * Solo si no es un cliente directo
	 * @param registerAccountAssignmentTO
	 * @param idInChargeParticipant
	 * @throws AccountAssignmentException 
	 * @throws ServiceException 
	 */
	public Boolean fillInChargeParticipant(RegisterAccountAssignmentTO registerAccountAssignmentTO, Long idInChargeParticipant, HashMap<Integer, String> mapParameters) throws AccountAssignmentException, ServiceException {
		Participant originParticipant = this.getParticipant(registerAccountAssignmentTO.getParticipantOrigin());
		
		if(Validations.validateIsNull(originParticipant.getIndDepositary()) ||  originParticipant.getIndDepositary().equals(ComponentConstant.ZERO)) {
			Participant inChargeParticipant = this.getParticipant(idInChargeParticipant);
			
			if(Validations.validateIsNull(inChargeParticipant)) {
				throw new AccountAssignmentException(PropertiesConstants.ASSIGNMENT_ACCOUNT_IN_CHARGE_PARTICIPANT_NO_EXISTS);
			} 
		
			registerAccountAssignmentTO.setIsDepositaryClient(Boolean.TRUE);
			registerAccountAssignmentTO.getHolderAccountOperation().setIsSelected(Boolean.TRUE);
			registerAccountAssignmentTO.setBlInchargeAgreementDisabled(Boolean.TRUE);
			registerAccountAssignmentTO.getHolderAccountOperation().setInchargeFundsParticipant(new Participant(idInChargeParticipant));
			registerAccountAssignmentTO.getHolderAccountOperation().setInchargeStockParticipant(new Participant(idInChargeParticipant));
			registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity());
			this.calculateAmount(registerAccountAssignmentTO);
			
			/**
			 * Ubicamos la cuenta titular del depositante, esta cuenta debe estar creada dentro del mismo depositante.
			 * Por ejemplo, el depositante BANVAR debe tener un titular con el mismo nombre y doc y una cuenta titular bajo el depositante BANVAR
			 */
			
			List<HolderAccount> lstOriginHolderAccount = this.checkHolderAccountInParticipant(originParticipant, registerAccountAssignmentTO.getParticipantOrigin(), Boolean.FALSE, Boolean.TRUE);

			HolderAccount holderAccountOrigin = lstOriginHolderAccount.get(0);
			if(Validations.validateIsNotNull(mapParameters)) {
				holderAccountOrigin.setAccountDescription(mapParameters.get(holderAccountOrigin.getAccountType()));
			}
			
			registerAccountAssignmentTO.setHolder(this.accountAssignmentService.getHolderServiceBean(originParticipant.getHolder().getIdHolderPk()));
			registerAccountAssignmentTO.setLstHolderAccount(lstOriginHolderAccount);
			registerAccountAssignmentTO.getHolderAccountOperation().setHolderAccount(holderAccountOrigin);
			
			registerAccountAssignmentTO.getHolderAccountOperation().setIndAutomatedIncharge(ComponentConstant.ONE);
			
			return Boolean.TRUE;
		}
		 return Boolean.FALSE;
	}
	
	/**
	 * 
	 * @param registerAccountAssignmentTO
	 * @throws ServiceException
	 * @throws AccountAssignmentException
	 */
	public List<HolderAccount> checkHolderAccountInParticipant(Participant originParticipant, Long idOriginParticipantPk, Boolean checkOriginParticipant, Boolean checkInChargeParticipant) throws ServiceException, AccountAssignmentException {
		return this.accountAssignmentService.checkHolderAccountInParticipant(originParticipant, idOriginParticipantPk, checkOriginParticipant, checkInChargeParticipant);
	}
	
	public void calculateAmount(RegisterAccountAssignmentTO registerAccountAssignmentTO) throws AccountAssignmentException{
		BigDecimal enteredQuantity = registerAccountAssignmentTO.getHolderAccountOperation().getStockQuantity();
		BigDecimal totalOpQuantity = registerAccountAssignmentTO.getMechanismOperation().getStockQuantity();
		if (Validations.validateIsNotNull(enteredQuantity)){
			
			if(enteredQuantity.compareTo(BigDecimal.ZERO) == 0){
				registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(null);
				return;
			}
			
			if(enteredQuantity.compareTo(totalOpQuantity) > 0){
				registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(null);
				registerAccountAssignmentTO.getHolderAccountOperation().setSettlementAmount(null);
				throw new AccountAssignmentException(PropertiesConstants.ASSIGNMENT_ACCOUNT_QUANTITY_EXCEEDED);				
			}
			
			BigDecimal totalQuantity = BigDecimal.ZERO;
			BigDecimal cashAmount = BigDecimal.ZERO;
			if(registerAccountAssignmentTO.isBlOperationRepo()
					&& Validations.validateIsNotNullAndNotEmpty(registerAccountAssignmentTO.getHolderAccountOperationRepo()) &&
					ComponentConstant.SALE_ROLE.equals(registerAccountAssignmentTO.getHolderAccountOperation().getRole())){
				BigDecimal originStock = registerAccountAssignmentTO.getHolderAccountOperationRepo().getStockQuantity();
				if(registerAccountAssignmentTO.getHolderAccountOperation().getStockQuantity().compareTo(originStock) == 1){
					registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(null);
					registerAccountAssignmentTO.getHolderAccountOperation().setSettlementAmount(null);
					throw new AccountAssignmentException(PropertiesConstants.ASSIGNMENT_ACCOUNT_REPOSEC_QUANTITY_EXCEEDED);				
				}
			}
			if(registerAccountAssignmentTO.isBlCrossOperation()){
				if(ParticipantRoleType.BUY.getCode().equals(registerAccountAssignmentTO.getHolderAccountOperation().getRole())){
					totalQuantity = enteredQuantity.add(registerAccountAssignmentTO.getQuantityAssig());
					if(totalQuantity.compareTo(totalOpQuantity) == 0)
						cashAmount = registerAccountAssignmentTO.getMechanismOperation().getCashSettlementAmount().subtract(registerAccountAssignmentTO.getAmountAssig());
					else
						cashAmount = NegotiationUtils.getSettlementAmount(registerAccountAssignmentTO.getMechanismOperation().getCashSettlementPrice(), enteredQuantity); 
				}else{
					totalQuantity = enteredQuantity.add(registerAccountAssignmentTO.getCrossQuantityAssig());
					if(totalQuantity.compareTo(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity()) == 0)
						cashAmount = registerAccountAssignmentTO.getMechanismOperation().getCashSettlementAmount().subtract(registerAccountAssignmentTO.getCrossAmountAssig());
					else
						cashAmount = NegotiationUtils.getSettlementAmount(registerAccountAssignmentTO.getMechanismOperation().getCashSettlementPrice(), enteredQuantity);
				}					
			}else{
				totalQuantity = enteredQuantity.add(registerAccountAssignmentTO.getQuantityAssig());
				if(totalQuantity.compareTo(registerAccountAssignmentTO.getMechanismOperation().getStockQuantity()) == 0)
					cashAmount = registerAccountAssignmentTO.getMechanismOperation().getCashSettlementAmount().subtract(registerAccountAssignmentTO.getAmountAssig());
				else
					cashAmount = NegotiationUtils.getSettlementAmount(registerAccountAssignmentTO.getMechanismOperation().getCashSettlementPrice(), enteredQuantity);
			}
			registerAccountAssignmentTO.getHolderAccountOperation().setCashAmount(
					NegotiationUtils.getSettlementAmount(registerAccountAssignmentTO.getMechanismOperation().getCashPrice(), enteredQuantity));
			registerAccountAssignmentTO.getHolderAccountOperation().setSettlementAmount(cashAmount);
		}else{
			registerAccountAssignmentTO.getHolderAccountOperation().setStockQuantity(null);
			registerAccountAssignmentTO.getHolderAccountOperation().setSettlementAmount(null);
		}
	}
}