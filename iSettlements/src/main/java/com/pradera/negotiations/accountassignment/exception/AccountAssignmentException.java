package com.pradera.negotiations.accountassignment.exception;

import com.pradera.commons.utils.PropertiesUtilities;

public class AccountAssignmentException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6132972756179794437L;
	
	public final String messageProperty;
	
	public AccountAssignmentException(String messageProperty) {
		super(PropertiesUtilities.getMessage(messageProperty));
		this.messageProperty = messageProperty;
	}

	public String getMessageProperty() {
		return messageProperty;
	}
	
	
}
