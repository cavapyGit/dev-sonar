package com.pradera.negotiations.accountassignment.service;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.massive.type.MechanismFileProcessType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.negotiation.constant.NegotiationConstant;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.model.settlement.constant.SettlementConstant;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveAccountAssignmentBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@BatchProcess(name="MassiveAccountAssignmentBatch")
@RequestScoped
public class MassiveAccountAssignmentBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder account assignment facade. */
	@EJB
	AccountAssignmentFacade holderAccountAssignmentFacade;

	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The notification service facade. */
	@EJB 
	NotificationServiceFacade notificationServiceFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {

		String mappingFile=null;
		String filePath=null;
		Long idMechanismPk = null;
		Long participantId = null;
		String locale=null;
		String fileName=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE)){
				filePath = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM)){
				mappingFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_MECHANISM)){
				idMechanismPk = new Long(detail.getParameterValue());
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_LOCALE)){
				locale = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE_NAME)){
				fileName = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_OTC_PART_ID)){
				participantId = Long.parseLong(detail.getParameterValue());
			}
		}
		
		try {
			
			ProcessFileTO processFileTO = new ProcessFileTO();
			processFileTO.setStreamFileDir(mappingFile);
			processFileTO.setTempProcessFile(new File(filePath));
			processFileTO.setProcessFile(FileUtils.readFileToByteArray(processFileTO.getTempProcessFile()));
			processFileTO.setIdNegotiationMechanismPk(idMechanismPk);
			processFileTO.setLocale(new Locale(locale));
			processFileTO.setFileName(fileName);
			processFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
			processFileTO.setIdParticipantPk(participantId);
			processFileTO.setInterfaceName(ComponentConstant.INTERFACE_BBV_ASSIGNMENTS);
			processFileTO.setProcessType(MechanismFileProcessType.MCN_ASSIGNMENT_UPLOAD.getCode());
			processFileTO.setRootTag(NegotiationConstant.MASSIVE_XML_BBV_ROOT_TAG);
			
			CommonsUtilities.validateFileOperationStruct(processFileTO);
				
			holderAccountAssignmentFacade.saveAssignmentMassively(processFileTO,idepositarySetup.getIndMarketFact());

			//we send the chaining process automatic
			BusinessProcess chainedBusinessProcess= new BusinessProcess();
			chainedBusinessProcess.setIdBusinessProcessPk(BusinessProcessType.GENERATE_CHAIN_OPERATION.getCode());
			Map<String, Object> processParameters=new HashMap<String, Object>();
			if (Validations.validateIsNotNull(participantId)) {
				processParameters.put(SettlementConstant.ID_PARTICIPANT, participantId);
			}
			batchProcessServiceFacade.registerBatch(processLogger.getIdUserAccount(), chainedBusinessProcess, processParameters);
			
			// Send notifications
			sendNotificationProcess(processLogger, participantId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Send notification process.
	 *
	 * @param processLogger the process logger
	 * @param idParticipantPk the id participant Pk
	 */
	public void sendNotificationProcess(ProcessLogger processLogger, Long idParticipantPk) {
		if(Validations.validateIsNotNullAndNotEmpty(idParticipantPk)) {
			Participant objParticipant = holderAccountAssignmentFacade.getParticipant(idParticipantPk);
			notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), 
					processLogger.getBusinessProcess(), null, new Object[]{objParticipant.getMnemonic()});
		}
						
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		return false;
	}

}
