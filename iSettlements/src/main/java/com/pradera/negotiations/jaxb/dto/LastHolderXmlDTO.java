package com.pradera.negotiations.jaxb.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "secc_ultimo_titular")
@XmlType(propOrder = { "accountTypeText", "holderType", "juridicHolder",
		"lstNaturalHolder" })
public class LastHolderXmlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountTypeText;
	private Integer holderType;

	private JuridicHolderXmlDTO juridicHolder;

	private List<NaturalHolderXmlDTO> lstNaturalHolder = new ArrayList<>();

	@XmlElement(name = "tipo_cuenta")
	public String getAccountTypeText() {
		return accountTypeText;
	}

	@XmlElement(name = "tipo_titular")
	public Integer getHolderType() {
		return holderType;
	}

	@XmlElement(name = "secc_titular_juridico", nillable = false)
	public JuridicHolderXmlDTO getJuridicHolder() {
		return juridicHolder;
	}

	@XmlElementWrapper(name = "secc_titulares_naturales", nillable = false)
	@XmlElement(name = "secc_titular_natural", nillable = false)
	public List<NaturalHolderXmlDTO> getLstNaturalHolder() {
		return lstNaturalHolder;
	}

	public void setAccountTypeText(String accountTypeText) {
		this.accountTypeText = accountTypeText;
	}

	public void setHolderType(Integer holderType) {
		this.holderType = holderType;
	}

	public void setJuridicHolder(JuridicHolderXmlDTO juridicHolder) {
		this.juridicHolder = juridicHolder;
	}

	public void setLstNaturalHolder(List<NaturalHolderXmlDTO> lstNaturalHolder) {
		this.lstNaturalHolder = lstNaturalHolder;
	}

}
