package com.pradera.negotiations.jaxb.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "secc_valor")
@XmlType(propOrder = { "securityClassText", "securityOnlyCode", "codeAlternate" })
public class SecurityXmlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String securityClassText;
	private String securityOnlyCode;
	private String codeAlternate;

	@XmlElement(name="inst")
	public String getSecurityClassText() {
		return securityClassText;
	}

	@XmlElement(name="serie")
	public String getSecurityOnlyCode() {
		return securityOnlyCode;
	}

	@XmlElement(name="codigo_alterno")
	public String getCodeAlternate() {
		return codeAlternate;
	}

	public void setSecurityClassText(String securityClassText) {
		this.securityClassText = securityClassText;
	}

	public void setSecurityOnlyCode(String securityOnlyCode) {
		this.securityOnlyCode = securityOnlyCode;
	}

	public void setCodeAlternate(String codeAlternate) {
		this.codeAlternate = codeAlternate;
	}

}
