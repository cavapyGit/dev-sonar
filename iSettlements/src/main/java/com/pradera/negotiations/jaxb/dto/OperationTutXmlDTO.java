package com.pradera.negotiations.jaxb.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "secc_operacion")
@XmlType(propOrder = { "numOperation", "operationName", "dateOPeration", "EEFF", "lstNegotiation" })
public class OperationTutXmlDTO implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long numOperation;
	private String operationName;
	private String dateOPeration;
	private String EEFF;
	private List<NegotiationXmlDTO> lstNegotiation = new ArrayList<>();

	
	@XmlElement(name = "nro_operacion")
	public Long getNumOperation() {
		return numOperation;
	}

	@XmlElement(name = "operacion")
	public String getOperationName() {
		return operationName;
	}

	@XmlElement(name = "fecha_operacion")
	public String getDateOPeration() {
		return dateOPeration;
	}

	@XmlElement(name = "EEFF")
	public String getEEFF() {
		return EEFF;
	}

	@XmlElementWrapper(name = "secc_negociaciones")
	@XmlElement(name = "secc_negociacion")
	public List<NegotiationXmlDTO> getLstNegotiation() {
		return lstNegotiation;
	}

	public void setNumOperation(Long numOperation) {
		this.numOperation = numOperation;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public void setDateOPeration(String dateOPeration) {
		this.dateOPeration = dateOPeration;
	}

	public void setEEFF(String eEFF) {
		EEFF = eEFF;
	}

	public void setLstNegotiation(List<NegotiationXmlDTO> lstNegotiation) {
		this.lstNegotiation = lstNegotiation;
	}
	
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

}
