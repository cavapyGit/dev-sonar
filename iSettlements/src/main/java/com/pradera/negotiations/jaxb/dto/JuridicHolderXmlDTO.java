package com.pradera.negotiations.jaxb.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "secc_titular_juridico")
@XmlType(propOrder = { 	"idHolderPk", 
						//"accountNumber", 
						"documentNumber",
						"businessName" })
public class JuridicHolderXmlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idHolderPk;
	//private Integer accountNumber;
	private String documentNumber;
	private String businessName;

	@XmlElement(name = "razon_social")
	public String getBusinessName() {
		return businessName;
	}

	@XmlElement(name = "cui")
	public Long getIdHolderPk() {
		return idHolderPk;
	}

//	@XmlElement(name = "cuenta_titular")
//	public Integer getAccountNumber() {
//		return accountNumber;
//	}

	@XmlElement(name = "num_documento")
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

//	public void setAccountNumber(Integer accountNumber) {
//		this.accountNumber = accountNumber;
//	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

}
