package com.pradera.negotiations.jaxb.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "secc_negociacion")
@XmlType(propOrder = { "mecanismo", "operTrans", "security", "lastHolder" })
public class NegotiationXmlDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mecanismo;
	private String operTrans;
	private SecurityXmlDTO security;
	private LastHolderXmlDTO lastHolder;

	@XmlElement(name = "mecanismo")
	public String getMecanismo() {
		return mecanismo;
	}

	@XmlElement(name = "oper_trans")
	public String getOperTrans() {
		return operTrans;
	}

	@XmlElement(name = "secc_valor")
	public SecurityXmlDTO getSecurity() {
		return security;
	}

	@XmlElement(name = "secc_ultimo_titular")
	public LastHolderXmlDTO getLastHolder() {
		return lastHolder;
	}

	public void setMecanismo(String mecanismo) {
		this.mecanismo = mecanismo;
	}

	public void setOperTrans(String operTrans) {
		this.operTrans = operTrans;
	}

	public void setSecurity(SecurityXmlDTO security) {
		this.security = security;
	}

	public void setLastHolder(LastHolderXmlDTO lastHolder) {
		this.lastHolder = lastHolder;
	}

}
