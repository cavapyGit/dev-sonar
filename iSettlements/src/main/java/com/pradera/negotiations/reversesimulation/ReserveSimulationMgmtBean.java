package com.pradera.negotiations.reversesimulation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.negotiation.ModalityGroup;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.negotiation.type.SettlementSchemaType;
import com.pradera.negotiations.accountassignment.facade.AccountAssignmentFacade;
import com.pradera.negotiations.operations.view.OperationBusinessBean;
import com.pradera.negotiations.positioncommission.to.PositionModalityGroupTO;
import com.pradera.negotiations.positioncommission.to.PositionParticipantTO;
import com.pradera.negotiations.positioncommission.to.PositionRoleTO;
import com.pradera.negotiations.positioncommission.to.SearchPositionCommissionTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ManagePositionsCommissionsBean.
 *
 * @author : PraderaTechnologies.
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReserveSimulationMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search position commission to. */
	private SearchPositionCommissionTO searchPositionCommissionTO;
	
	private List<PositionParticipantTO>  listCopy;
	
	private List<PositionRoleTO>  selectionLstPositionByRole;
	
	/** The bl participant. */
	private boolean blParticipant;
	
	/** The id mechanism operation. */
	private Long idMechanismOperation;
	
	/** The general pameters facade. */
	@Inject
	private GeneralParametersFacade generalPametersFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;		
	
	/** The monitor participant service facade. */
	@EJB
	private ReserveSimulationServiceFacade reserveSimulationServiceFacade;
	
	/** The manage holder account assignment facade. */
	@Inject
	private AccountAssignmentFacade manageHolderAccountAssignmentFacade;
	
	/** The operation business comp. */
	@Inject
	private OperationBusinessBean operationBusinessComp;
	
	@Inject @Configurable String idParticipantBC;
	
	private Long participantSelectedOfList;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
    public void init(){
		selectionLstPositionByRole = new ArrayList<>();
		searchPositionCommissionTO = new SearchPositionCommissionTO();
		searchPositionCommissionTO.setSettlementDate(getCurrentSystemDate());
		try{
			fillCombos();
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				List<Participant> lstTemp = new ArrayList<Participant>();
				if(!userInfo.getUserAccountSession().getParticipantCode().equals(new Long(idParticipantBC))){
					Participant participant = manageHolderAccountAssignmentFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
					lstTemp.add(participant);
					searchPositionCommissionTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
					searchPositionCommissionTO.setLstParticipants(lstTemp);
					searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegoMechanismForParticipant(userInfo.getUserAccountSession().getParticipantCode()));
					blParticipant = true;
				}else{
					searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
				}
			}else{
				searchPositionCommissionTO.setLstNegotiationMechanism(manageHolderAccountAssignmentFacade.getListNegotiationMechanism());
			}
			searchPositionCommissionTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
			changeNegotiationMechanism();
			searchPositionCommissionTO.setSettlementSchemaSelected(SettlementSchemaType.NET.getCode());
			searchPositionCommissionTO.setCurrencySelected(CurrencyType.PYG.getCode());
		}catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change negotiation mechanism.
	 */
	public void changeNegotiationMechanism(){
		try {
			cleanResults();
			searchPositionCommissionTO.setLstModalityGroup(null);
			searchPositionCommissionTO.setModalityGroupSelected(null);
			if(!blParticipant){
				searchPositionCommissionTO.setLstParticipants(null);
				searchPositionCommissionTO.setParticipantSelected(null);
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchPositionCommissionTO.getNegoMechanismSelected())){				
				if(!blParticipant)
					searchPositionCommissionTO.setLstParticipants(manageHolderAccountAssignmentFacade.getListParticipantsForNegoMechanism(searchPositionCommissionTO.getNegoMechanismSelected(), null));
				searchPositionCommissionTO.setLstModalityGroup(reserveSimulationServiceFacade.getListModalityGroup(searchPositionCommissionTO.getNegoMechanismSelected()));
				List<ModalityGroup> lstModalityGroup = new ArrayList<ModalityGroup>();
				for(ModalityGroup objModalityGroup : searchPositionCommissionTO.getLstModalityGroup()){
					if(!objModalityGroup.getIdModalityGroupPk().equals(GeneralConstants.MODALITY_GROUP_FOP)){
						lstModalityGroup.add(objModalityGroup);
					}
				}
				searchPositionCommissionTO.setLstModalityGroup(lstModalityGroup);
			}		
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		selectionLstPositionByRole=new ArrayList<>();
		searchPositionCommissionTO.setLstPositionByModalityGroup(null);
		searchPositionCommissionTO.setLstPositionByParticipant(null);
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResult(false);
		searchPositionCommissionTO.setBlNoResultByRole(false);
	}
	
	/**
	 * Load mechanism operation.
	 *
	 * @param mechanismOperationId the mechanism operation id
	 */
	public void loadMechanismOperation(Long mechanismOperationId){
		idMechanismOperation = mechanismOperationId;
		operationBusinessComp.setIdMechanismOperationPk(idMechanismOperation);
		operationBusinessComp.setShowAssignments(false);
		operationBusinessComp.searchMechanismOperation();
	}	
	/**
	 * Limpia los filtros busqueda
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchPositionCommissionTO.setCurrencySelected(null);
		searchPositionCommissionTO.setLstModalityGroup(null);
		searchPositionCommissionTO.setModalityGroupSelected(null);
		searchPositionCommissionTO.setNegoMechanismSelected(null);
		searchPositionCommissionTO.setSettlementSchemaSelected(null);		
		if(!blParticipant){
			searchPositionCommissionTO.setParticipantSelected(null);
			searchPositionCommissionTO.setLstParticipants(null);
		}
		searchPositionCommissionTO.setNegoMechanismSelected(NegotiationMechanismType.BOLSA.getCode());
		changeNegotiationMechanism();
		searchPositionCommissionTO.setSettlementSchemaSelected(SettlementSchemaType.NET.getCode());
		searchPositionCommissionTO.setCurrencySelected(CurrencyType.PYG.getCode());
	}
	
	/**
	 *  Gets  Positions by Participant list.
	 */
	@LoggerAuditWeb
	public void searchPositionsParticipant(){
		cleanResults();
		try {
			searchPositionCommissionTO.setTotalPurchase(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalSale(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalDebt(BigDecimal.ZERO);
			searchPositionCommissionTO.setTotalCreditor(BigDecimal.ZERO);
			searchPositionCommissionTO.setBlNoResult(true);
			selectionLstPositionByRole = new ArrayList<>();
			List<PositionParticipantTO> lstResult = reserveSimulationServiceFacade.searchPositionByParticipant(searchPositionCommissionTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(PositionParticipantTO posByParti:lstResult){
					searchPositionCommissionTO.setTotalPurchase(searchPositionCommissionTO.getTotalPurchase().add(posByParti.getBuyPosition()));
					searchPositionCommissionTO.setTotalSale(searchPositionCommissionTO.getTotalSale().add(posByParti.getSellPosition()));
					if(posByParti.getNetPosition().compareTo(BigDecimal.ZERO) < 0){
						searchPositionCommissionTO.setTotalDebt(searchPositionCommissionTO.getTotalDebt().add(posByParti.getNetPosition()));
					}else{
						searchPositionCommissionTO.setTotalCreditor(searchPositionCommissionTO.getTotalCreditor().add(posByParti.getNetPosition()));
					}
				}
				searchPositionCommissionTO.setLstPositionByParticipant(lstResult);
				searchPositionCommissionTO.setBlNoResult(false);
				listCopy = new ArrayList<>();
				listCopy = getCloneList(lstResult);
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}	
	
	/**
	 * Show position modality group.
	 *
	 * @param posByParti the pos by parti
	 */
	//iniciando valores antes de simular en el momento de hacer click en el participante
	public void showPositionModalityGroup(PositionParticipantTO posByParti){
		searchPositionCommissionTO.setBlNoResultByRole(false);
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setParticipantHeader(posByParti.getIdParticipantPk());
		searchPositionCommissionTO.setModalityGroupHeader(posByParti.getIdModalityGroupPk());
		searchPositionCommissionTO.setSettlementSchemaHedaer(posByParti.getSettlementSchema());
		searchPositionCommissionTO.setLstPositionByModalityGroup(posByParti.getLstPositionByModalityGroup());
		listCopy = getCloneList(searchPositionCommissionTO.getLstPositionByParticipant()); 
		selectionLstPositionByRole = new ArrayList<>();
		participantSelectedOfList = posByParti.getIdParticipantPk();
	}
	
	/**
	 * Show position sale.
	 *
	 * @param posByModaGroup the pos by moda group
	 */
	public void showPositionSale(PositionModalityGroupTO posByModaGroup){
		searchPositionCommissionTO.setRole(ParticipantRoleType.SELL.getValue());
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResultByRole(true);
		if(Validations.validateListIsNotNullAndNotEmpty(posByModaGroup.getLstPositionByRoleSell())){
			searchPositionCommissionTO.setLstPositionByRole(posByModaGroup.getLstPositionByRoleSell());
			searchPositionCommissionTO.setBlNoResultByRole(false);
		}
		selectionLstPositionByRole = new ArrayList<>();
	}
	
	/**
	 * Show position purchase.
	 *
	 * @param posByModaGroup the pos by moda group
	 */
	public void showPositionPurchase(PositionModalityGroupTO posByModaGroup){
		searchPositionCommissionTO.setRole(ParticipantRoleType.BUY.getValue());
		searchPositionCommissionTO.setLstPositionByRole(null);
		searchPositionCommissionTO.setBlNoResultByRole(true);
		if(Validations.validateListIsNotNullAndNotEmpty(posByModaGroup.getLstPositionByRoleBuy())){
			searchPositionCommissionTO.setLstPositionByRole(posByModaGroup.getLstPositionByRoleBuy());
			searchPositionCommissionTO.setBlNoResultByRole(false);
		}	
		selectionLstPositionByRole = new ArrayList<>();
	}
	
	/**
	 * Fill combos.re
	 *
	 * @throws ServiceException the service exception
	 */
	
	
	public void reCalculeDates(){
		/*Reseteando valores en caso de no seleccionar ninguno*/
		if(selectionLstPositionByRole.size()==0){	
			cleanSimulate();
			return;
		}
		setPositionParticipantsTO();

	}
	private void printArray(List<PositionRoleTO>  list){
		System.out.println("COMPRADOR---VENDEDOR---MONTO");
		for (PositionRoleTO aux : list) {
			System.out.print(aux.getIdParticipantBuyer()+"-"+aux.getParticipantBuyerMnemonic()+"---"+aux.getIdParticipantSeller()+"-"+aux.getParticipantSellerMnemonic()+"---"+aux.getEffectiveAmount()+"\n");
		}
	}
	private List<PositionPart> getSimplifiedList(){
		//printArray(selectionLstPositionByRole);
		List<PositionRoleTO> listPositions =  orderSelectionLstPositionByRoleList();
		//printArray(listPositions);
		List<PositionPart> simplifiedList = new ArrayList<>();
		PositionPart part;
		List<PositionParticipantTO> listParticipant = getCloneList(listCopy);
		for (PositionParticipantTO mainPart : listParticipant){
			for (PositionRoleTO positionRoleTO : listPositions) {
				/*PARA COMPRAS*/
				if(searchPositionCommissionTO.getRole().equals(ParticipantRoleType.BUY.getValue()))
				if(mainPart.getIdParticipantPk().equals(positionRoleTO.getIdParticipantBuyer())){
					part = new PositionPart();
				    part.setAmount(positionRoleTO.getEffectiveAmount());
				    part.setIdParticipantBuyer(positionRoleTO.getIdParticipantBuyer());
				    part.setIdParticipantSeller(positionRoleTO.getIdParticipantSeller());
				    PositionPart posAuxBuy= isContainsPosition(simplifiedList, part);
				    if(posAuxBuy!=null){
				    	BigDecimal amount = posAuxBuy.getAmount().add(part.getAmount());
				    	//buscar y reemplazar el objeto
				    	for (PositionPart var : simplifiedList) {
							if(part.idParticipantBuyer.equals(var.getIdParticipantBuyer())
									&&part.getIdParticipantSeller().equals(var.getIdParticipantSeller())){
								var.setAmount(amount);
							}
						}
				    	//simplifiedList = replacePosition(simplifiedList,part);
				    }else{
				    	//System.out.println("AGREGANDO : "+part.toString());
				    	simplifiedList.add(part);
				    }
				}
				/*VENTA*/
				if(searchPositionCommissionTO.getRole().equals(ParticipantRoleType.SELL.getValue()))
				if(mainPart.getIdParticipantPk().equals(positionRoleTO.getIdParticipantSeller())){
					part = new PositionPart();
				    part.setAmount(positionRoleTO.getEffectiveAmount());
				    part.setIdParticipantBuyer(positionRoleTO.getIdParticipantBuyer());
				    part.setIdParticipantSeller(positionRoleTO.getIdParticipantSeller());
				    PositionPart posAuxSell= isContainsPosition(simplifiedList, part);
				    if(posAuxSell!=null){
				    	BigDecimal amount = posAuxSell.getAmount().add(part.getAmount());
				    	//buscar y reemplazar el objeto
				    	for (PositionPart var : simplifiedList) {
							if(part.idParticipantBuyer.equals(var.getIdParticipantBuyer())
									&&part.getIdParticipantSeller().equals(var.getIdParticipantSeller())){
								var.setAmount(amount);
							}
				    	}
				    }else{
				    	simplifiedList.add(part);
				    }
				}
			}
		}
//		System.out.println("LISTA SIMPLIFICADA");
//		for (PositionPart parti : simplifiedList) {
//			System.out.print(parti.getIdParticipantBuyer()+"---"+parti.getIdParticipantSeller()+"---"+parti.getAmount()+"\n");			
//		}
		return simplifiedList;
	}
	private List<PositionRoleTO>  orderSelectionLstPositionByRoleList(){
		Long idBuyerPk;
		Long idSellerPk;
		List<PositionRoleTO>  listAux = getCloneListPos(selectionLstPositionByRole);
		for (PositionRoleTO positionRoleTO : listAux) {
			if(searchPositionCommissionTO.getRole().equals(ParticipantRoleType.BUY.getValue())){
				if(participantSelectedOfList.equals(positionRoleTO.getIdParticipantSeller())){
					idBuyerPk = positionRoleTO.getIdParticipantBuyer();
					positionRoleTO.setIdParticipantBuyer(positionRoleTO.getIdParticipantSeller());
					positionRoleTO.setIdParticipantSeller(idBuyerPk);
				}
			}
			if(searchPositionCommissionTO.getRole().equals(ParticipantRoleType.SELL.getValue())){
				if(participantSelectedOfList.equals(positionRoleTO.getIdParticipantBuyer())){
					idSellerPk =  positionRoleTO.getIdParticipantSeller();
					positionRoleTO.setIdParticipantSeller(positionRoleTO.getIdParticipantBuyer());
					positionRoleTO.setIdParticipantBuyer(idSellerPk);
				}
			}
		}
		return listAux;
	}
	private List<PositionPart> replacePosition(List<PositionPart> simplifiedList,PositionPart part){
		BigDecimal position = BigDecimal.ZERO;
		for (PositionPart pos : simplifiedList) {
			if(pos.getIdParticipantBuyer().equals(part.getIdParticipantBuyer())){
			   position = pos.getAmount().add(part.getAmount());
			   pos.setAmount(position);
			}
		}	
		return simplifiedList;
	}
	private PositionPart isContainsPosition(List<PositionPart> simplifiedList,PositionPart part){
		for (PositionPart pos : simplifiedList) {
			if(pos.getIdParticipantBuyer().equals(part.getIdParticipantBuyer())&&pos.getIdParticipantSeller().equals(part.getIdParticipantSeller()))
				return pos;
		}
		return null;
	}
	private void setPositionParticipantsTO(){
		List<PositionPart> simplifiedList = getSimplifiedList(); 
		BigDecimal sellPositionAux;
		BigDecimal buyPositionAux;
		List<PositionParticipantTO> listParticipant = getCloneList(listCopy);
		for (PositionPart posPart : simplifiedList) {
			/*POSISIONES VENTA*/
			for (PositionParticipantTO mainPart : listParticipant){
				if(mainPart.getIdParticipantPk().equals(posPart.getIdParticipantSeller())){
					sellPositionAux = mainPart.getSellPosition().subtract(posPart.getAmount());
					mainPart.setSellPosition(sellPositionAux);
					mainPart.getLstPositionByModalityGroup().get(0).getPositionParticipantTO().setSellPosition(sellPositionAux);
				}
			}
			/*POSISIONES COMPRA*/
			for (PositionParticipantTO mainPart : listParticipant){
				if(mainPart.getIdParticipantPk().equals(posPart.getIdParticipantBuyer())){
					buyPositionAux = mainPart.getBuyPosition().subtract(posPart.getAmount());
					mainPart.setBuyPosition(buyPositionAux);
					mainPart.getLstPositionByModalityGroup().get(0).getPositionParticipantTO().setBuyPosition(buyPositionAux);
				}
			}
		}
		BigDecimal sumPositionBuy = BigDecimal.ZERO;
		BigDecimal sumPositionSell = BigDecimal.ZERO;
		/*Total Posición Neta Deudora y Total Posición Neta Acreedora*/
		BigDecimal sumTotalDebt = BigDecimal.ZERO;
		BigDecimal sumTotalCreditor = BigDecimal.ZERO;
		/*Suma de totales*/
		for (PositionParticipantTO participantTO : listParticipant){
			/*Sumando totales de compra y venta*/
			sumPositionBuy  =  sumPositionBuy.add(participantTO.getBuyPosition());
			sumPositionSell = sumPositionSell.add(participantTO.getSellPosition());
			/*seteando la diferencia*/
			participantTO.setNetPosition(participantTO.getSellPosition().subtract(participantTO.getBuyPosition()));
			/*Suma de posiciones deudoras y accredoras*/
			if(participantTO.getNetPosition().longValue()<0){
				participantTO.setIsNetPositionNegative(true);
				sumTotalDebt = sumTotalDebt.add(participantTO.getNetPosition());
			}else{
				participantTO.setIsNetPositionNegative(false);
				sumTotalCreditor = sumTotalCreditor.add(participantTO.getNetPosition());
			}
		}
		searchPositionCommissionTO.setTotalPurchase(sumPositionBuy);
		searchPositionCommissionTO.setTotalSale(sumPositionSell);
		
		searchPositionCommissionTO.setTotalCreditor(sumTotalCreditor);
		searchPositionCommissionTO.setTotalDebt(sumTotalDebt);
		
		searchPositionCommissionTO.setLstPositionByParticipant(listParticipant);
	}

	private List<PositionParticipantTO> getCloneList(List<PositionParticipantTO> list){
		List<PositionParticipantTO>participantTOs = new ArrayList<>(); 
		PositionParticipantTO aux;
		for (PositionParticipantTO positionParticipantTO : list) {
			aux =new PositionParticipantTO();
			aux.setBuyPosition(positionParticipantTO.getBuyPosition());
			aux.setCurrency(positionParticipantTO.getCurrency());
			aux.setIdModalityGroupPk(positionParticipantTO.getIdModalityGroupPk());
			aux.setIdParticipantPk(positionParticipantTO.getIdParticipantPk());
			aux.setIsNetPositionNegative(positionParticipantTO.getIsNetPositionNegative());
			aux.setLstPositionByModalityGroup(positionParticipantTO.getLstPositionByModalityGroup());
			aux.setModalityGroup(positionParticipantTO.getModalityGroup());
			aux.setNetPosition(positionParticipantTO.getNetPosition());
			aux.setParticipant(positionParticipantTO.getParticipant());
			aux.setParticipantMnemonic(positionParticipantTO.getParticipantMnemonic());
			aux.setSellPosition(positionParticipantTO.getSellPosition());
			aux.setSettlementSchema(positionParticipantTO.getSettlementSchema());
			aux.setSettlementSchemaDesc(positionParticipantTO.getSettlementSchemaDesc());
			participantTOs.add(aux);
		}
		return participantTOs;
	}
	private List<PositionRoleTO> getCloneListPos(List<PositionRoleTO> list){
		List<PositionRoleTO>positionTOs = new ArrayList<>(); 
		PositionRoleTO aux;
		for (PositionRoleTO to : list) {
			aux =new PositionRoleTO();
			aux.setEffectiveAmount(to.getEffectiveAmount());
			aux.setIdParticipantBuyer(to.getIdParticipantBuyer());
			aux.setIdParticipantSeller(to.getIdParticipantSeller());
			positionTOs.add(aux);
		}
		return positionTOs;
	}
	
	public void clearSearch(){
		selectionLstPositionByRole = new ArrayList<>();
		reCalculeDates();
	}
	private void cleanSimulate(){
		searchPositionCommissionTO.setLstPositionByParticipant(getCloneList(listCopy));
		BigDecimal totalPurchase = BigDecimal.ZERO;
		BigDecimal totalSale = BigDecimal.ZERO;
		BigDecimal totalDebt = BigDecimal.ZERO;
		BigDecimal totalCreditor = BigDecimal.ZERO;	
		
		PositionParticipantTO positionParticipantTO = new PositionParticipantTO();
		
		for (PositionParticipantTO  participantTO : searchPositionCommissionTO.getLstPositionByParticipant()) {
			totalPurchase = totalPurchase.add(participantTO.getBuyPosition());
			totalSale = totalSale.add(participantTO.getSellPosition());
			
			if(participantTO.getNetPosition().longValue()>0){
				totalCreditor = totalCreditor.add(participantTO.getNetPosition());
				participantTO.setIsNetPositionNegative(false);
			}
			else{
				totalDebt = totalDebt.add(participantTO.getNetPosition());
				participantTO.setIsNetPositionNegative(true);
			}
			if(participantSelectedOfList.equals(participantTO.getIdParticipantPk()))
				positionParticipantTO = participantTO;
		}
		searchPositionCommissionTO.setTotalPurchase(totalPurchase);
		searchPositionCommissionTO.setTotalSale(totalSale);
		searchPositionCommissionTO.setTotalDebt(totalDebt);
		searchPositionCommissionTO.setTotalCreditor(totalCreditor);
		
		for (PositionModalityGroupTO var : searchPositionCommissionTO.getLstPositionByModalityGroup()) {
			if(var.getPositionParticipantTO().getIdParticipantPk().equals(participantSelectedOfList)){
				var.setPositionParticipantTO(positionParticipantTO);
				return;
			}
		}
	}
	private void fillCombos() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setIndicator2(GeneralConstants.ONE_VALUE_STRING);
		searchPositionCommissionTO.setLstCurrency(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.NET.getCode());
		paramTab.setParameterName(SettlementSchemaType.NET.getValue());
		lstTemp.add(paramTab);
		paramTab = new ParameterTable();
		paramTab.setParameterTablePk(SettlementSchemaType.GROSS.getCode());
		paramTab.setParameterName(SettlementSchemaType.GROSS.getValue());
		lstTemp.add(paramTab);
		searchPositionCommissionTO.setLstSettlementSchema(lstTemp);
	}
	

	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Gets the search position commission to.
	 *
	 * @return the search position commission to
	 */
	public SearchPositionCommissionTO getSearchPositionCommissionTO() {
		return searchPositionCommissionTO;
	}
	
	/**
	 * Sets the search position commission to.
	 *
	 * @param searchPositionCommissionTO the new search position commission to
	 */
	public void setSearchPositionCommissionTO(
			SearchPositionCommissionTO searchPositionCommissionTO) {
		this.searchPositionCommissionTO = searchPositionCommissionTO;
	}

	/**
	 * Gets the id mechanism operation.
	 *
	 * @return the id mechanism operation
	 */
	public Long getIdMechanismOperation() {
		return idMechanismOperation;
	}

	/**
	 * Sets the id mechanism operation.
	 *
	 * @param idMechanismOperation the new id mechanism operation
	 */
	public void setIdMechanismOperation(Long idMechanismOperation) {
		this.idMechanismOperation = idMechanismOperation;
	}

	/**
	 * Gets the id participant bc.
	 *
	 * @return the id participant bc
	 */
	public Long getIdParticipantBC() {
		return new Long(idParticipantBC);
	}

	/**
	 * Sets the id participant bc.
	 *
	 * @param idParticipantBC the new id participant bc
	 */
	public void setIdParticipantBC(String idParticipantBC) {
		this.idParticipantBC = idParticipantBC;
	}

	public List<PositionRoleTO> getSelectionLstPositionByRole() {
		return selectionLstPositionByRole;
	}

	public void setSelectionLstPositionByRole(
			List<PositionRoleTO> selectionLstPositionByRole) {
		this.selectionLstPositionByRole = selectionLstPositionByRole;
	}	
	class PositionPart{
		Long idParticipantBuyer;
		Long idParticipantSeller;
		BigDecimal amount;
		
		public PositionPart() {
			// TODO Auto-generated constructor stub
		}
		
		public Long getIdParticipantBuyer() {
			return idParticipantBuyer;
		}

		public void setIdParticipantBuyer(Long idParticipantBuyer) {
			this.idParticipantBuyer = idParticipantBuyer;
		}

		public Long getIdParticipantSeller() {
			return idParticipantSeller;
		}

		public void setIdParticipantSeller(Long idParticipantSeller) {
			this.idParticipantSeller = idParticipantSeller;
		}

		public BigDecimal getAmount() {
			return amount;
		}
		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
		
		@Override
		public String toString(){
			return "PART-COMPRA : "+idParticipantBuyer+" PART-VENTA : "+idParticipantSeller+" MONTO : "+amount;
		}
	}
}