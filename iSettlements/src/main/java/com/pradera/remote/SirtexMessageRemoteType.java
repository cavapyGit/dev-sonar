package com.pradera.remote;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public enum SirtexMessageRemoteType {
	/** The systems. */
	EXITO("0000","Exito"),
	OPERATION_NUMBER("0024","Numero de Operacion SIRTEX no existe."),
	INVALID_DATE("0025","Fecha de Operacion SIRTEX no valida."),
	INVALID_PARTICIPANT("0026","Codigo de Participante Vendedor no valida."),
	INVALID_SECURITY("0027","Código de Valor no valida."),
	INVALID_ISSUANCE_DATE_SECURITY("0028","Fecha de Emision de el Código de Valor no valida."),
	INVALID_EXPIRATION_DATE_SECURITY("0029","Fecha de Vencimiento de el Código de Valor no valida."),
	INVALID_MODALITY("0030","Modalidad de la Operacion SIRTEX no valida."),
	INVALID_RATE("0031","Tasa de Interes de Código de Valor no valida."),
	INVALID_QUANTITY("0032","Cantidad Ofertada no valida o no coincide."),
	INVALID_QUANTITY_ACCEPTED("0033","Cantidad Aceptada debe ser mayor a cero."),
	INVALID_QUANTITY_ACCEPTED_VS("0034","Cantidad Aceptada no debe ser mayor a la Cantidad Ofertada."),
	INVALID_COUPON_NUMBER("0035","Numero de Cupon no valido."),
	INVALID_COUPON_LIFE("0036","Plazo de Cupon no valido."),
	INVALID_COUPON_QUANTITY("0040","Cantidad de Cupones no puede ser mayor a la cantidad de Cupones de la operacion."),
	INVALID_COUPON_QUANTITY_ZERO("0041","Cantidad de Cupones debe ser mayor a cero"),
	INVALID_COUPON_EXPIRATION("0037","Fecha de Vencimiento del Cupon no Valida."),
	INVALID_SIRTEX_OPERATION_ERROR_REVIEW("0038","Operacion SIRTEX no se encuentra en un estado valido para poder ser Revisada."),
	INVALID_SIRTEX_OPERATION_REVIEW("0039","Operacion SIRTEX ya se encuentra en estado Revisado."),
	
	INVALID_SIRTEX_OPERATION_ERROR_REJECT("0038","Operacion SIRTEX no se encuentra en un estado valido para poder ser Rechazado."),
	INVALID_SIRTEX_OPERATION_REJECT("0039","Operacion SIRTEX ya se encuentra en estado Rechazado."),
	
	INVALID_SIRTEX_OPERATION_ERROR_CONFIRM("0038","Operacion SIRTEX no se encuentra en un estado valido para poder ser Confirmado."),
	INVALID_SIRTEX_OPERATION_CONFIRM("0039","Operacion SIRTEX ya se encuentra en estado Confirmado."),
	
	UNDEFINIED_ERROR("1000","Operacion SIRTEX ya se encuentra en estado Revisado.")
	
	;
	
	/** The code. */
	private String code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<SirtexMessageRemoteType> list = new ArrayList<SirtexMessageRemoteType>();
	
	/** The Constant lookup. */
	public static final Map<String, SirtexMessageRemoteType> lookup = new HashMap<String, SirtexMessageRemoteType>();

	static {
		for (SirtexMessageRemoteType s : EnumSet.allOf(SirtexMessageRemoteType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SirtexMessageRemoteType(String code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return null;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static SirtexMessageRemoteType get(Integer code) {
		return lookup.get(code);
	}
}
