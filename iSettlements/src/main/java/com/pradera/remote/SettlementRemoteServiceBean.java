package com.pradera.remote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectRteTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.component.settlements.service.SettlementRemoteService;
import com.pradera.integration.component.settlements.to.OverTheCounterRegisterTO;
import com.pradera.integration.component.settlements.to.SirtexOperationConsultResponseTO;
import com.pradera.integration.component.settlements.to.SirtexOperationConsultTO;
import com.pradera.integration.component.settlements.to.SirtexOperationCouponTO;
import com.pradera.integration.component.settlements.to.SirtexOperationResponseTO;
import com.pradera.integration.component.settlements.to.SirtexOperationStateResponseTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.CouponTradeRequest;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.TradeRequest;
import com.pradera.model.negotiation.type.SirtexOperationStateType;
import com.pradera.negotiations.operations.service.NegotiationOperationServiceBean;
import com.pradera.negotiations.otcoperations.facade.OtcOperationServiceFacade;
import com.pradera.negotiations.sirtex.facade.SirtexOperationFacade;
import com.pradera.negotiations.sirtex.service.SirtexOperationService;
import com.pradera.negotiations.sirtex.to.SirtexOperationResultTO;
import com.pradera.negotiations.sirtex.view.SirtexOperationTO;
import com.pradera.settlements.core.service.FundsComponentSingleton;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementRemoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Interceptors(RemoteLoggerUserInterceptor.class)
public class SettlementRemoteServiceBean implements SettlementRemoteService{

	/** The otc facade. */
	@EJB private OtcOperationServiceFacade otcFacade;
	
	/** The general parameter facade. */
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	/** The sirtex operation facade. */
	@EJB private SirtexOperationFacade sirtexOperationFacade;
	
	/** The parameter service bean. */
	@EJB private ParameterServiceBean parameterServiceBean;
	
	/** The participant service bean. */
	@EJB private ParticipantServiceBean participantServiceBean;
	
	/** The sirtex operation service. */
	@EJB private SirtexOperationService sirtexOperationService;
	
	/** The negotiation validation. */
	@EJB private NegotiationOperationServiceBean negotiationValidation;
	
	/** The interface component service bean. */
	@Inject private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The funds component singleton. */
	@EJB private FundsComponentSingleton fundsComponentSingleton;
	
	/** The accounts remote service. */
	@Inject
	private Instance<AccountsRemoteService> accountsRemoteService;
	
	@Inject
	private HolderQueryServiceBean holderQueryServiceBean;
	/** The Constant logger. */
	@Inject  
	PraderaLogger logger;
	
	/** The parameter description. */
	Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	
	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.settlements.service.SettlementRemoteService#registerOTCoperation(com.pradera.integration.component.settlements.to.OverTheCounterRegisterTO)
	 */
	@Override
	public RecordValidationType registerOTCoperation(OverTheCounterRegisterTO overTheCounterRegisterTO) throws ServiceException {
		/**Getting the logger user from remote transaction*/
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		/**Put loggerUser into localSession transaction*/
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);		
        try {
        	RecordValidationType recordValidationType = converOperationFromTOTx(overTheCounterRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransaction(overTheCounterRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (ServiceException e) {
			e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(overTheCounterRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}
	
	/**
	 * Metodo para generar el TO que luego se transforma en XML de la consulta de webservice
	 */
	@Override
	public RecordValidationType sirtexOperationConsult(SirtexOperationConsultTO sirtexOperationTO) throws ServiceException{
		
		/**Getting the logger user from remote transaction*/
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		/**Put loggerUser into localSession transaction*/
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);		
		
        Integer transactionState= BooleanType.NO.getCode();
        
        //debemos enviar a la consulta de la pantalla de SIRTEX
		SirtexOperationTO operationTO = new SirtexOperationTO();
		operationTO.setInitialDate(sirtexOperationTO.getInitialDate());
		operationTO.setFinalDate(sirtexOperationTO.getEndDate());
		operationTO.setIdSearchDateType(sirtexOperationTO.getDateType());
		
		if(Validations.validateIsNotNullAndNotEmpty(sirtexOperationTO.getParticipant())){
			Participant participant = new Participant();
			participant.setMnemonic(sirtexOperationTO.getParticipant());
			participant = participantServiceBean.findParticipantByFiltersServiceBean(participant);
			operationTO.setIdParticipantBuyer(participant.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(sirtexOperationTO.getModality())){
			operationTO.setIdModalityNegotiation(sirtexOperationTO.getModality().longValue());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(sirtexOperationTO.getState())){
			operationTO.setOtcOperationState(sirtexOperationTO.getState());
		}
		
		SirtexOperationConsultResponseTO consultResponseTO = null;
		
		//ejecutamos el query con los parametros enviados 
        List<SirtexOperationResultTO> listResult = sirtexOperationService.getSirtexOperationsServiceBean(operationTO);
       
       
        //este es el objeto con todos los datos de la consulta
        consultResponseTO = new SirtexOperationConsultResponseTO();
        consultResponseTO.setQueryNumber(sirtexOperationTO.getQueryNumber());
        consultResponseTO.setOperationType(sirtexOperationTO.getOperationType());
        consultResponseTO.setOperationDate(sirtexOperationTO.getOperationDate());
            
            List<SirtexOperationResponseTO> lstOperationResponseTO = new ArrayList<>();
            
            if(listResult.size() > GeneralConstants.ZERO_VALUE_INTEGER){
            	
            for(SirtexOperationResultTO operation : listResult){
            	SirtexOperationResponseTO operationResponseTO = new SirtexOperationResponseTO();
            	
            	operationResponseTO.setOperationID(operation.getIdMechanismOperationRequestPk().intValue());
            	//con esto ir al metodo y traer todos los cupones y agregarlos tmb al objeto con un for
            	operationResponseTO.setOperationDate(operation.getOperationDate());
            	operationResponseTO.setParticipant(operation.getMnemoPartSeller());
            	operationResponseTO.setSecurity(operation.getSecurityCode());
            	operationResponseTO.setModality(operation.getIdNegotiationModalityPk().intValue());
            	Security security = otcFacade.findSecurityFromSerie(operation.getSecurityCode().substring(4));
            	operationResponseTO.setIssuanceDate(security.getIssuanceDate());
            	operationResponseTO.setExpirationDate(security.getExpirationDate());
            	operationResponseTO.setOperationRate(security.getInterestRate().toString());
            	operationResponseTO.setTimeOfLife(security.getSecurityDaysTerm());
            	operationResponseTO.setQuantity(operation.getStockQuantity().intValue());
            	
            	//con esto ir al metodo y traer todos los cupones y agregarlos tmb al objeto con un for
            	 Map<Integer,String> parameters = new HashMap<Integer, String>();
            	TradeRequest tradeRequest = sirtexOperationService.findSirtexOperationFromId(
            			Long.valueOf(operationResponseTO.getOperationID().toString()),parameters);
            	
            	for (CouponTradeRequest coupon : tradeRequest.getCouponsTradeRequestList()){
            		SirtexOperationCouponTO sirtexCoupon = new SirtexOperationCouponTO();
            		
            		sirtexCoupon.setCouponNumber(coupon.getCouponNumber());
            		sirtexCoupon.setTimeOfLife(coupon.getDaysOfLife().toString());
            		sirtexCoupon.setExpirationDate(coupon.getExpiredDate());
            		sirtexCoupon.setQuantity(coupon.getSuppliedQuantity().intValue());
            		
            		operationResponseTO.getSirCouponTOs().add(sirtexCoupon);
            	}
            	
            	lstOperationResponseTO.add(operationResponseTO);
            }
            
            consultResponseTO.setSirtexOperations(lstOperationResponseTO);
           }
        
        
        
        RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(consultResponseTO,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		interfaceComponentServiceBean.get().saveInterfaceTransactionTx(sirtexOperationTO, BooleanType.YES.getCode(), loggerUser);
		return objRecordValidationType;
	}
	
	/**
	 * Metodo para generar el TO que luego se transforma en XML de la revision de webservice
	 */
	
	@Override
	public RecordValidationType sirtexOperationReview(SirtexOperationConsultResponseTO sirtexOperationTO, String operationType) throws ServiceException{
		
		/**Getting the logger user from remote transaction*/
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		/**Put loggerUser into localSession transaction*/
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);		
		
		//cargamos parametros para generar codigo BCB
		loadSecurityClassBCB();
		Boolean existCoupon = false;
		Boolean timeLifeCoupon = false;
		Boolean dateExpirationCoupon = false;
		
		
		TradeRequest tradeRequest = new TradeRequest();
		TradeRequest mechanismOperation = null;
		
		//esta es la respuesta, dentro lleva la lista de arriba 
		SirtexOperationConsultResponseTO consultResponseTO = new SirtexOperationConsultResponseTO();
				
		List<SirtexOperationStateResponseTO> lstOperationStateSirtex = new ArrayList<>();
		//revisamos operacion por operacion
		for(SirtexOperationResponseTO sirtexOperation : sirtexOperationTO.getSirtexOperations()){
			
			//extraemos toda la informacion para revisar la operacion
			try {
				mechanismOperation = sirtexOperationFacade.findSirtexOperationFromId(Long.valueOf(sirtexOperation.getOperationID().toString()),parameterDescription);
			} catch (Exception e) {
				// TODO: No hay datos con ese ID de operacion
			}
			
			//generamos el objeto de respuesta
			SirtexOperationStateResponseTO operationStateResponseTO = new SirtexOperationStateResponseTO();
			
			operationStateResponseTO.setQueryNumber(sirtexOperation.getOperationID());
			operationStateResponseTO.setParticipant(sirtexOperation.getParticipant());
			operationStateResponseTO.setOperationDate(sirtexOperation.getOperationDate());
			operationStateResponseTO.setSecurity(sirtexOperation.getSecurity());
			operationStateResponseTO.setQuantity(sirtexOperation.getQuantityAccept());
			
			if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation)){
				List<CouponTradeRequest> lstCouponAccepted = new ArrayList<>();
				
				//aplicar la cantidad aceptada solo si es distinta a la cantidad ofertada
				for (CouponTradeRequest couponOperation : mechanismOperation.getCouponsTradeRequestList()){
					
					for (SirtexOperationCouponTO cuponXml:sirtexOperation.getSirCouponTOs()){
						//si el numero de cupon del XML coindice con el nro de cupon de la operacion
						if(cuponXml.getCouponNumber().equals(couponOperation.getCouponNumber())){
							couponOperation.setAcceptedQuantity(BigDecimal.valueOf(cuponXml.getQuantityAccepted()));
							
							lstCouponAccepted.add(couponOperation);
						}
					}
				}
				
				//seteamos la nueva lista con las cantidades del cupon enviadas desde el XML
				mechanismOperation.setCouponsTradeRequestList(lstCouponAccepted);
			}
			
			if(!Validations.validateIsNotNullAndNotEmpty(mechanismOperation)){
				operationStateResponseTO.setCode(SirtexMessageRemoteType.OPERATION_NUMBER.getCode());
				operationStateResponseTO.setDescCode(SirtexMessageRemoteType.OPERATION_NUMBER.getValue());
			}else
				if(!sirtexOperation.getOperationDate().equals(sirtexOperationTO.getOperationDate())){
					operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_DATE.getCode());
					operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_DATE.getValue());
				}else //comenzamos con las validaciones
					if(!sirtexOperation.getParticipant().equals(mechanismOperation.getSellerParticipant().getMnemonic())){
						operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_PARTICIPANT.getCode());
						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_PARTICIPANT.getValue());
					}else //comenzamos con las validaciones
						if(!sirtexOperation.getSecurity().equals(mechanismOperation.getSecurities().getIdSecurityCodePk())){
							operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SECURITY.getCode());
							operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SECURITY.getValue());
						}else //comenzamos con las validaciones
							if(!sirtexOperation.getIssuanceDate().equals(mechanismOperation.getSecurities().getIssuanceDate())){
								operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_ISSUANCE_DATE_SECURITY.getCode());
								operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_ISSUANCE_DATE_SECURITY.getValue());
							}else //comenzamos con las validaciones
								if(!sirtexOperation.getExpirationDate().equals(mechanismOperation.getSecurities().getExpirationDate())){
									operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_EXPIRATION_DATE_SECURITY.getCode());
									operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_EXPIRATION_DATE_SECURITY.getValue());
								}else //comenzamos con las validaciones
									if(!sirtexOperation.getModality().equals(mechanismOperation.getMechanisnModality().getNegotiationModality().getIdNegotiationModalityPk().intValue())){
										operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_MODALITY.getCode());
										operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_MODALITY.getValue());
									}else //comenzamos con las validaciones
										if(!sirtexOperation.getOperationRate().equals(mechanismOperation.getSecurities().getInterestRate().toString())){
											operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_RATE.getCode());
											operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_RATE.getValue());
										}else //comenzamos con las validaciones
											if(!sirtexOperation.getQuantity().equals(mechanismOperation.getStockQuantity().intValue())){
												operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_QUANTITY.getCode());
												operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_QUANTITY.getValue());
											}else //comenzamos con las validaciones
												if(sirtexOperation.getQuantityAccept() < GeneralConstants.ONE_VALUE_INTEGER){
													operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_QUANTITY_ACCEPTED.getCode());
													operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_QUANTITY_ACCEPTED.getValue());
												}else //comenzamos con las validaciones
													if(sirtexOperation.getQuantityAccept() > mechanismOperation.getStockQuantity().intValue()){
														operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_QUANTITY_ACCEPTED_VS.getCode());
														operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_QUANTITY_ACCEPTED_VS.getValue());
													}else //comenzamos con las validaciones
														if(sirtexOperation.getSirCouponTOs().size() > mechanismOperation.getCouponsTradeRequestList().size() ){
															operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_COUPON_QUANTITY.getCode());
															operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_COUPON_QUANTITY.getValue());
														}else	
															if(sirtexOperation.getSirCouponTOs().size() < GeneralConstants.ONE_VALUE_INTEGER){
																operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_COUPON_QUANTITY_ZERO.getCode());
																operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_COUPON_QUANTITY_ZERO.getValue());
															}else
																for (CouponTradeRequest couponOperation : mechanismOperation.getCouponsTradeRequestList()){
																	for (SirtexOperationCouponTO cuponXml:sirtexOperation.getSirCouponTOs()){
																		
																		if(cuponXml.getCouponNumber().equals(couponOperation.getCouponNumber())){
																			existCoupon = true;
																			
																			if(Integer.valueOf(cuponXml.getTimeOfLife()).equals(couponOperation.getDaysOfLife())){
																				timeLifeCoupon = true;
																				
																				if(cuponXml.getExpirationDate().equals(couponOperation.getExpiredDate())){
																					dateExpirationCoupon = true;
																					break;
																				}else{
																					break;
																				}
																			}else {
																				break;
																			}
																		}else{
																			break;
																		}
																	}
																	if(existCoupon.equals(false)||timeLifeCoupon.equals(false)||dateExpirationCoupon.equals(false)){
																		break;
																	}
																}
																if(existCoupon.equals(false)){
																	operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_COUPON_NUMBER.getCode());
																	operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_COUPON_NUMBER.getValue());
																}else
																	if(timeLifeCoupon.equals(false)){
																		operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_COUPON_LIFE.getCode());
																		operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_COUPON_LIFE.getValue());
																	}else
																		if(dateExpirationCoupon.equals(false)){
																			operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_COUPON_EXPIRATION.getCode());
																			operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_COUPON_EXPIRATION.getValue());
																		}else{
																			
																			if(operationType.equals(ComponentConstant.INTERFACE_SIRTEX_OPERATION_REVIEW)){
																				if(SirtexOperationStateType.REVIEWED.getCode().equals(mechanismOperation.getOperationState())){
																					operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_REVIEW.getCode());
																					operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_REVIEW.getValue());
																				}else 
																					if(!SirtexOperationStateType.APROVED.getCode().equals(mechanismOperation.getOperationState())){
																						operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_ERROR_REVIEW.getCode());
																						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_ERROR_REVIEW.getValue());               
																					}else{
																						tradeRequest = sirtexOperationFacade.reviewSirtexOperation(mechanismOperation);
																						operationStateResponseTO.setCode(SirtexMessageRemoteType.EXITO.getCode());
																						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.EXITO.getValue());
																					}
																			}else if(operationType.equals(ComponentConstant.INTERFACE_SIRTEX_OPERATION_REJECT)){
																				if(SirtexOperationStateType.REJECTED.getCode().equals(mechanismOperation.getOperationState())){
																					operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_REJECT.getCode());
																					operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_REJECT.getValue());
																				}else 
																					if(!SirtexOperationStateType.APROVED.getCode().equals(mechanismOperation.getOperationState())){
																						operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_ERROR_REJECT.getCode());
																						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_ERROR_REJECT.getValue());               
																					}else{
																						tradeRequest = sirtexOperationFacade.rejectSirtexOperation(mechanismOperation);
																						operationStateResponseTO.setCode(SirtexMessageRemoteType.EXITO.getCode());
																						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.EXITO.getValue());
																					}
																			}else if(operationType.equals(ComponentConstant.INTERFACE_SIRTEX_OPERATION_CONFIRM)){
																				if(SirtexOperationStateType.CONFIRMED.getCode().equals(mechanismOperation.getOperationState())){
																					operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_CONFIRM.getCode());
																					operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_CONFIRM.getValue());
																				}else if(!SirtexOperationStateType.REVIEWED.getCode().equals(mechanismOperation.getOperationState())){
																						operationStateResponseTO.setCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_ERROR_CONFIRM.getCode());
																						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.INVALID_SIRTEX_OPERATION_ERROR_CONFIRM.getValue());               
																					}else{
																						tradeRequest = sirtexOperationFacade.confirmSirtexOperation(mechanismOperation);
																						operationStateResponseTO.setCode(SirtexMessageRemoteType.EXITO.getCode());
																						operationStateResponseTO.setDescCode(SirtexMessageRemoteType.EXITO.getValue());
																					}
																			}
																		}
			lstOperationStateSirtex.add(operationStateResponseTO);
		}
		
		//si hay datos
		if(lstOperationStateSirtex.size() > GeneralConstants.ZERO_VALUE_INTEGER){
			consultResponseTO.setSirtexStateOperations(lstOperationStateSirtex);
		}
		
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(consultResponseTO,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(sirtexOperationTO, BooleanType.YES.getCode(), loggerUser);
			return objRecordValidationType;
	}
		
	private void loadSecurityClassBCB() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		for(ParameterTable parameter: generalParameterFacade.getListParameterTableServiceBean(parameterTableTO))
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getText3());
	}
	
	
	/**
	 * Conver operation from to tx.
	 *
	 * @param overTheCounterTO the over the counter to
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType converOperationFromTOTx(OverTheCounterRegisterTO overTheCounterTO, LoggerUser loggerUser) throws ServiceException{
		Object objResult = converOperationFromTO(overTheCounterTO, loggerUser);
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * *.
	 *
	 * @param overTheCounterTO the over the counter to
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object converOperationFromTO(OverTheCounterRegisterTO overTheCounterTO, LoggerUser loggerUser) throws ServiceException {		
		
		/** check operation date is today */
		Date operationDate = CommonsUtilities.truncateDateTime(overTheCounterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date)!=0){
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE,null);
		}
		
		/** Getting participant*/
		String partMnemonic = overTheCounterTO.getEntityNemonic();
		Participant participant = new Participant();
		participant.setMnemonic(partMnemonic);
		participant = participantServiceBean.findParticipantByFiltersServiceBean(participant);
		
		if(participant == null ){
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		}
		
		/** Get key of participant*/
		Long participantCod = participant.getIdParticipantPk();
		
		/** check if operation code is correct for trv */
		if(!ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode()) && 
				!ComponentConstant.INTERFACE_OTC_OPERATION_WEB_BCB.equals(overTheCounterTO.getOperationCode())){
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}			
		
		/** check if operation was successfully registered previously */
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
				participantCod, overTheCounterTO.getOperationNumber(), overTheCounterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
				
		/** Get security from serial */
		
		SecurityObjectTO objSecurityObjectTO=null;
		SecurityObjectRteTO objSecurityObjectRteTO=null;
		Security security=null;
		
		if(ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode())){
			objSecurityObjectTO = overTheCounterTO.getSecurityObjectTO();
			security = otcFacade.findSecurityFromSerie(objSecurityObjectTO.getSecuritySerial());
		}else{
			objSecurityObjectRteTO = overTheCounterTO.getSecuritiesTransferObjectTO().getSecurityObjectRteTO();
			security = otcFacade.findSecurityFromSerie(objSecurityObjectRteTO.getSecuritySerial());
		}
	
		if(security == null){
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
		} 
		
		String securityCode = null;
		
		if(ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode())){
			if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode()) 
					|| security.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				securityCode = security.getIdSecurityCodePk();			
				overTheCounterTO.getSecurityObjectTO().setSecurityClass(security.getSecurityClass());
				if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					overTheCounterTO.getSecurityObjectTO().setSecurityClassDesc(SecurityClassType.DPF.getText1());
				} else {
					overTheCounterTO.getSecurityObjectTO().setSecurityClassDesc(SecurityClassType.DPA.getText1());
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			}
		}else{
				if(security.getSecurityClass().equals(SecurityClassType.BTX.getCode()) 
						|| security.getSecurityClass().equals(SecurityClassType.BBX.getCode())){
					securityCode = security.getIdSecurityCodePk();			
					overTheCounterTO.getSecuritiesTransferObjectTO().getSecurityObjectRteTO().setSecurityClass(security.getSecurityClass());
					if(security.getSecurityClass().equals(SecurityClassType.BTX.getCode())){
						overTheCounterTO.getSecuritiesTransferObjectTO().getSecurityObjectRteTO().setSecurityClassDesc(SecurityClassType.BTX.getText1());
					} else {
						overTheCounterTO.getSecuritiesTransferObjectTO().getSecurityObjectRteTO().setSecurityClassDesc(SecurityClassType.BBX.getText1());
					}
				} else {
					return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
				}
			
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(security.getNotTraded())){
			if(GeneralConstants.ONE_VALUE_INTEGER.equals(security.getNotTraded())){
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_NOTTRADED, null);
			}
		}
		
		/** Get holderCode from TO SOURCE HOLDER */
		Long holderPkSrc = null;
		if (ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode())){
			holderPkSrc = overTheCounterTO.getSecuritiesTransferObjectTO().getIdSourceHolder();
		}else{
			holderPkSrc = overTheCounterTO.getSecuritiesTransferObjectTO().getSourceHolderAccount().getIdHolderAccount();
		}
		HolderAccountBalance objHolderAccountBalance = otcFacade.getHolderAccount(participantCod, holderPkSrc, securityCode, ComponentConstant.SALE_ROLE);							
		HolderAccount objHolderAccountSource = new HolderAccount();
		
		//set account number to response rte bcb
		if(ComponentConstant.INTERFACE_OTC_OPERATION_WEB_BCB.equals(overTheCounterTO.getOperationCode())){
			overTheCounterTO.getSecuritiesTransferObjectTO().setIdSourceHolder(holderPkSrc.longValue());
		}
		
		if(objHolderAccountBalance == null){						
			return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
		} else {
		
			// VALIDANDO QUE EL CUI ORIGEN ESTE HABILITADO PARA NEGOCIAR CARTERA PROPIA
			// EN LA MODALIDAD TRANSFERENCIA EXTRABURSATIL DE DPF.
			HolderAccountObjectTO holderAccountObjectTO = new HolderAccountObjectTO();			
			holderAccountObjectTO.setIdHolderAccount(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
			
			List<Holder> listHolders = holderQueryServiceBean.findHolders(holderAccountObjectTO);
			if(listHolders != null && listHolders.size() > 0){
				for(Holder holder : listHolders){
					if(holder.getIndCanNegotiate() == null || holder.getIndCanNegotiate() == 0){
						Object[] params = {participantCod+"-("+holder.getIdHolderPk()+")-"+holderPkSrc};
						return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_ALERT_HOLDER_CANNOT_NEGOTIATE, null, params);
					}
				}
			}			
			
			//VALIDATE BLOCKED OPERATIONS
			if(objHolderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) > 0
					|| objHolderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO) > 0
					|| objHolderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_BLOCKED_OPERATIONS, null);				
			} 
			
			//VALIDATE REPORTO OPERATIONS
			if(objHolderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO) > 0
					|| objHolderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_REPORT_OPERATION_EXISTS, null);				
			}
			
			//VALIDATE AVAILABLE BALANCE
			if(objHolderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0 ){
				objHolderAccountSource = objHolderAccountBalance.getHolderAccount();		
			} else {
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_BALANCE_AVAILABLE, null);	
			}
		
			//IN RTE COMPARE AVAILABLE BALANCE, WITH BALANCE ON XML
			if(ComponentConstant.INTERFACE_OTC_OPERATION_WEB_BCB.equals(overTheCounterTO.getOperationCode())){
					if ((overTheCounterTO.getSecuritiesTransferObjectTO().getSecurityObjectRteTO().getStockQuantity().intValue())
							>(objHolderAccountBalance.getAvailableBalance().intValue())){
						return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
					}
				}
		}
		
		/** Get holderCode from TO TARGET HOLDER */
		
		// validate holder account 
		HolderAccountObjectTO holderAccountObjectTOTarget = overTheCounterTO.getSecuritiesTransferObjectTO().getTargetHolderAccount();
		holderAccountObjectTOTarget.setIdParticipant(participantCod);		
		holderAccountObjectTOTarget.setCreateObject(true);
		holderAccountObjectTOTarget.setDpfInterface(overTheCounterTO.isDpfInterface());		
		// almacenar el numero de cuenta	
		if (ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode())){
			if(Validations.validateIsNotNullAndNotEmpty(overTheCounterTO.getSecuritiesTransferObjectTO().getIdTargetHolder())){
				holderAccountObjectTOTarget.setAccountNumber(overTheCounterTO.getSecuritiesTransferObjectTO().getIdTargetHolder().intValue());
				holderAccountObjectTOTarget.setValidateAccount(BooleanType.YES.getBooleanValue());
			} else {
				holderAccountObjectTOTarget.setValidateAccount(BooleanType.NO.getBooleanValue());
			}
		}else{
			if(Validations.validateIsNotNullAndNotEmpty(overTheCounterTO.getSecuritiesTransferObjectTO().getTargetHolderAccount().getIdHolderAccount())){
				holderAccountObjectTOTarget.setAccountNumber(overTheCounterTO.getSecuritiesTransferObjectTO().getTargetHolderAccount().getIdHolderAccount().intValue());
				holderAccountObjectTOTarget.setValidateAccount(BooleanType.YES.getBooleanValue());
			} else {
				holderAccountObjectTOTarget.setValidateAccount(BooleanType.NO.getBooleanValue());
			}
		}
		
		logger.info(":::: ENVIA DATOS DE LA CUENTA DESTINO  ::::");
		logger.info("Participante: " + holderAccountObjectTOTarget.getIdParticipant());
		logger.info("Participante dpf AccounTypeCode: " + holderAccountObjectTOTarget.getAccountTypeCode()+ " AccountType: " + holderAccountObjectTOTarget.getAccountType());
		logger.info("Participante dpf: " + holderAccountObjectTOTarget.getAccountNumber());
		
		//crea titular o agarra cuenta 
		Object objResponse = accountsRemoteService.get().createHolderRemoteService(loggerUser, holderAccountObjectTOTarget);
		
		if(objResponse instanceof RecordValidationType){
			RecordValidationType validation = (RecordValidationType) objResponse;
			validation.setOperationRef(overTheCounterTO);			
			return validation;
		} else if (objResponse instanceof HolderAccountObjectTO){
			holderAccountObjectTOTarget = (HolderAccountObjectTO) objResponse;
		}
		
		HolderAccount objHolderAccountTarget = null;
		
		if(holderAccountObjectTOTarget.getIdHolderAccount() != null){
			objHolderAccountTarget = otcFacade.getHolderAccount(holderAccountObjectTOTarget.getIdHolderAccount());				
			overTheCounterTO.getSecuritiesTransferObjectTO().setIdTargetHolder(objHolderAccountTarget.getAccountNumber().longValue());
		}
				
		if(security != null && participant != null && objHolderAccountSource != null && objHolderAccountTarget != null){
			
			logger.info(":::: CUENTA OBTENIDA PARA OTC ::::");
			logger.info("Valor: " + security.getIdSecurityCodePk());
			logger.info("Id Cuenta: " + objHolderAccountTarget.getIdHolderAccountPk());
			logger.info("Numero de Cuenta: " + objHolderAccountTarget.getAccountNumber());
			logger.info("Participante: " + objHolderAccountTarget.getParticipant().getIdParticipantPk());
			
			if(!participant.getIdParticipantPk().equals(objHolderAccountTarget.getParticipant().getIdParticipantPk())){
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PARTICIPANT_BALANCE_INSERT, null);
			}
			
			if(objHolderAccountTarget.getIdHolderAccountPk().equals(objHolderAccountSource.getIdHolderAccountPk())){
				return CommonsUtilities.populateRecordCodeValidation(overTheCounterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SETTLEMENT_TRA, null);
			}
			
			/**Assignment SecurityObject into overTheCounter*/		
			if (ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode())){
				objSecurityObjectTO.setSecurityClass(security.getSecurityClass());
				objSecurityObjectTO.setSecuritySerial(security.getIdSecurityCodeOnly());	
			}else{
				objSecurityObjectRteTO.setSecurityClass(security.getSecurityClass());
				objSecurityObjectRteTO.setSecuritySerial(security.getIdSecurityCodeOnly());
			}

			/**Assignment other fields, to main entity*/
			overTheCounterTO.setIdParticipant(participantCod);
			overTheCounterTO.setIdSecurityCode(securityCode);			
			try{				
				/**Create and SettlementOperation*/ 
				
				Object operation=null;
				if (ComponentConstant.INTERFACE_OTC_OPERATION_DPF.equals(overTheCounterTO.getOperationCode())){
					operation = otcFacade.createAndSettlementOperation
							(security, participant, participant, objHolderAccountSource,objHolderAccountTarget, null);
				}else{
					operation = otcFacade.createAndSettlementOperation
							(security, participant, participant, objHolderAccountSource, objHolderAccountTarget,overTheCounterTO.getSecuritiesTransferObjectTO().getSecurityObjectRteTO().getStockQuantity());
				}
				
				
				overTheCounterTO.setIdTradeOperation(((MechanismOperation)operation).getIdMechanismOperationPk());
				
			} catch (Exception e){
				/**If happen error, throw ServiceException*/
				throw e;
			}
		}	
		return overTheCounterTO;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.settlements.service.SettlementRemoteService#depositOfFundsLip(com.pradera.integration.component.funds.to.FundsTransferRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public RecordValidationType depositOfFundsLip(FundsTransferRegisterTO fundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException{
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		try {
			RecordValidationType recordValidationType = fundsComponentSingleton.validateAndSaveDepositOfFundsLipTx(fundsTransferRegisterTO, loggerUser);
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(fundsTransferRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (ServiceException e) {
			e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(fundsTransferRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}

	
}