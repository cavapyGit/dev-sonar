package com.pradera.webclient;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.funds.automaticinterface.lip.service.ManageLipServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.component.settlements.to.SecuritiesTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;
import com.pradera.negotiations.jaxb.dto.OperationTutXmlDTO;
import com.pradera.settlements.core.service.SettlementProcessService;
import com.pradera.settlements.core.to.SettlementOperationTO;

import bo.com.edv.www.ServiciosClienteBCBProxy;
import bo.com.edv.www.ServiciosClienteEDVProxy;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementServiceConsumer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class SettlementServiceConsumer implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The settlement process service. */
	@EJB
	private SettlementProcessService settlementProcessService;
	
	/** The manage process service. */
	@EJB
	private ManageLipServiceFacade manageProcessService;
	
	/** The server configuration. */		
	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;
	
	/** The str services consumer EDV. */
	String strServicesConsumerEDV = GeneralConstants.EMPTY_STRING;
	
	/** The str services consumer EDV. */
	String strServicesConsumerBCB = GeneralConstants.EMPTY_STRING;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strServicesConsumerEDV = applicationConfiguration.getProperty("eif.banks.web.services.consumer");
		strServicesConsumerBCB = applicationConfiguration.getProperty("bcb.lip.web.services.consumer");
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendInformationLastHolderDPFWebClient(OperationTutXmlDTO operationTutXmlDTO, LoggerUser loggerUser) throws Exception {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumerEDV);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		ProcessFileTO objProcessFileTO = null;
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		try {
			// validando estructura de XML
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_TRANSFER_DPF_LAST_HOLDER,
																		null, ComponentConstant.TUT_DPF_NALE, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				// registro en InterfaceProcess, estado: EN PROCESO
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
						ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				
				log.info("::: registrarndo el InterfaceTransaction exitoso");
				lstRecordValidationTypes = settlementProcessService.registerInterfaceTrasaction(operationTutXmlDTO, idInterfaceProcess, loggerUser);
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					
					// Obteniendo el XML en formato String
					StringWriter xmltoSend = new StringWriter();
					JAXBContext contextObj = JAXBContext.newInstance(OperationTutXmlDTO.class);
					Marshaller marshallerObj = contextObj.createMarshaller();
					marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
					marshallerObj.setProperty("com.sun.xml.bind.xmlDeclaration", false);
					marshallerObj.marshal(operationTutXmlDTO, xmltoSend);
					log.info("::: XML para enviar:\n" + xmltoSend.toString());
					String msgXMLSendToBUS=null;
					if(Validations.validateIsNotNull(xmltoSend)
							&& Validations.validateIsNotNullAndNotEmpty(xmltoSend.toString())){
						msgXMLSendToBUS = xmltoSend.toString();
					}else{
						return;
					}
					
					interfaceComponentServiceBean.get().updateInterfaceProcessUploadFile(objProcessFileTO.getIdProcessFilePk(), processState, msgXMLSendToBUS.getBytes(), loggerUser);
					
					log.info(":::::::: ENVIANDO MENSAJE AL BUS ::::::::");
					String xmlResponse = serviciosClienteEDVProxy.enviarTransferenciaUltimoTitular(msgXMLSendToBUS,operationTutXmlDTO.getEEFF());
					//TODO quemado, solo para pruebas
					//String xmlResponse = "<raiz><operacion><nroOperacion></nroOperacion><EEFF></EEFF><inst></inst><serie></serie><codResp>0000</codResp><descResp>Exito</descResp></operacion></raiz>";
					log.info("::: Respuesta del BUS:\n"+xmlResponse);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
				}
			}
			//
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				throw ex;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send securities transfer operation web client.
	 *
	 * @param objSettlementOperationTO the obj settlement operation to
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendSecuritiesTransferOperationWebClient(SettlementOperationTO objSettlementOperationTO, LoggerUser loggerUser) throws Exception {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumerEDV);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SecuritiesTransferRegisterTO securitiesTransferRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_SECURITIES_TRANSFER_DPF, 
																			null, ComponentConstant.TRV_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= settlementProcessService.generateSecuritiesTransferRegisterTO(objSettlementOperationTO, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					securitiesTransferRegisterTO = (SecuritiesTransferRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse= serviciosClienteEDVProxy.enviarTransferencia(xmlUpload.toString(), securitiesTransferRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				throw ex;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send funds lip web client.
	 *
	 * @param fundOperation the fund operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void sendFundsLipWebClient(FundsOperation fundOperation, LoggerUser loggerUser) throws ServiceException{
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy=new ServiciosClienteBCBProxy(strServicesConsumerBCB);				
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		ProcessFileTO objProcessFileTO = null;
		String strXmlReception=null, strXmlSend=null;
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_BCB_LIP_SEND, 
																			null, ComponentConstant.LIP_SEND_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR sendFundsLipWebClient ::::::::");
				// armando objeto que contendra informacion del XML
				FundsTransferRegisterTO objFundsTransferRegisterTO = manageProcessService.createDataSendFunds(fundOperation, loggerUser.getUserName());
				settlementProcessService.generateFundsTransferRegisterTO(objFundsTransferRegisterTO, idInterfaceProcess, loggerUser);

				if (Validations.validateIsNotNull(objFundsTransferRegisterTO)) {
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					// merge objeto xml
					byte [] arrXmlSend = CommonsUtilities.generateInterfaceLipResponseFile(objFundsTransferRegisterTO.getAutomaticSendTypeTO(), objProcessFileTO.getStreamFileDir(),
							ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.INTERFACE_LIP_ROW);
					interfaceComponentServiceBean.get().updateExternalInterfaceState(idInterfaceProcess, processState,arrXmlSend, loggerUser);
					strXmlSend = CommonsUtilities.bytesToString(arrXmlSend);
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					strXmlReception = objServiciosClienteBCBProxy.enviarProcesarTransferencia(strXmlSend);										
					
					// guardando mensaje XML devuelto
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, strXmlReception.getBytes(), loggerUser);
					
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					parameterServiceBean.verifyResponseFile(strXmlReception, objProcessFileTO);
					interfaceComponentServiceBean.get().updateFundsInterfaceTransaction(objFundsTransferRegisterTO.getIdInterfaceTransaction(), 
																						objFundsTransferRegisterTO.getIdFundsOperation());
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send funds extract lip.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AutomaticSendType> sendFundsExtractLip(FundsTransferRegisterTO objFundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException{
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy = new ServiciosClienteBCBProxy(strServicesConsumerBCB);				
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();				
		ProcessFileTO objProcessFileTO = null;
		String strXmlReception=null, strXmlSend=null;
		List<AutomaticSendType> lstAutomaticSendType = null;
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_BCB_LIP_EXTRACT, null, ComponentConstant.LIP_EXTRACT_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				// guardando transaccion del estracto
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");				
				settlementProcessService.generateFundsTransferRegisterTO(objFundsTransferRegisterTO, idInterfaceProcess, loggerUser);

				if (Validations.validateIsNotNull(objFundsTransferRegisterTO)) {
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					byte [] arrXmlSend = CommonsUtilities.generateInterfaceLipResponseFile(objFundsTransferRegisterTO.getAutomaticSendTypeTO(), 
							objProcessFileTO.getStreamFileDir(), ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.EXTRACT_LIP_ROW);
					
					interfaceComponentServiceBean.get().updateExternalInterfaceState(idInterfaceProcess, processState, arrXmlSend, loggerUser);
					
					strXmlSend = CommonsUtilities.bytesToString(arrXmlSend);
					
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					strXmlReception = objServiciosClienteBCBProxy.enviareSolicitarExtracto(strXmlSend);										
					
					// persistiendo archiv enviado al LIP
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, strXmlReception.getBytes(), loggerUser);
					
					if(Validations.validateIsNotNullAndNotEmpty(strXmlReception)){
						log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
						lstAutomaticSendType = new ArrayList<AutomaticSendType>();
						objProcessFileTO.setRootTag(ComponentConstant.EXTRACT_LIP_RESP);
						lstAutomaticSendType = parameterServiceBean.getResponseFileLip(strXmlReception, objProcessFileTO);
					} 
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
		return lstAutomaticSendType;
	}
	
	
}
