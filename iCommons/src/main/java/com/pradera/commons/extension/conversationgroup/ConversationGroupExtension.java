package com.pradera.commons.extension.conversationgroup;

import java.lang.annotation.Annotation;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessAnnotatedType;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationGroup;

import com.pradera.commons.extension.AnnotatedTypeWrapper;
import com.pradera.commons.view.DepositaryGroupConversation;
import com.pradera.commons.view.DepositaryWebBean;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ConversationGroupExtension.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14-may-2015
 */
public class ConversationGroupExtension implements Extension {

	/**
	 * Process annotated type.
	 *
	 * @param <T> the generic type
	 * @param processAnnotatedType the process annotated type
	 */
	public <T> void processAnnotatedType(@Observes ProcessAnnotatedType<T> processAnnotatedType) {
		AnnotatedType<T> annotatedType = processAnnotatedType.getAnnotatedType();
		if(hasDepositaryConversationScope(annotatedType)){
			AnnotatedTypeWrapper<T> annotatedTypeWrapper = new AnnotatedTypeWrapper<>(annotatedType);
			Annotation groupAnnotation = new ConversationGroupLiteral(DepositaryGroupConversation.class);
			annotatedTypeWrapper.getAnnotations().add(groupAnnotation);
			processAnnotatedType.setAnnotatedType(annotatedTypeWrapper);
		}
	}
	
	/**
	 * Checks for depositary conversation scope.
	 *
	 * @param <T> the generic type
	 * @param annotatedType the annotated type
	 * @return true, if successful
	 */
	private <T> boolean hasDepositaryConversationScope(AnnotatedType<T> annotatedType) {
		//Only add default group when is not conversationGroup qualifier  
		if(annotatedType.isAnnotationPresent(DepositaryWebBean.class)
				&& !annotatedType.isAnnotationPresent(ConversationGroup.class)){
			return true;
		} else {
			return false;
		}
    }
}
