package com.pradera.commons.extension.conversationgroup;

import javax.enterprise.util.AnnotationLiteral;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationGroup;

public class ConversationGroupLiteral extends
		AnnotationLiteral<ConversationGroup> implements ConversationGroup {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1337437780658557318L;
	
	private Class<?> value;
	
	public ConversationGroupLiteral(Class<?> value){
		this.value=value;
	}

	@Override
	public Class<?> value() {
		// TODO Auto-generated method stub
		return this.value;
	}

}
