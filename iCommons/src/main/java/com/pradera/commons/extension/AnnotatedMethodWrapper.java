package com.pradera.commons.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedParameter;
import javax.enterprise.inject.spi.AnnotatedType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AnnotatedMethodWrapper.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/06/2014
 * @param <X> the generic type
 */
public class AnnotatedMethodWrapper<X> implements AnnotatedMethod<X> {
	 
    /** The wrapped annotated method. */
    private AnnotatedMethod<X> wrappedAnnotatedMethod;
 
    /** The annotations. */
    private Set<Annotation> annotations;
 
    /**
     * Instantiates a new annotated method wrapper.
     *
     * @param wrappedAnnotatedMethod the wrapped annotated method
     */
    public AnnotatedMethodWrapper(AnnotatedMethod<X> wrappedAnnotatedMethod) {
        this.wrappedAnnotatedMethod = wrappedAnnotatedMethod;
 
        annotations = new HashSet<>(wrappedAnnotatedMethod.getAnnotations());
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedCallable#getParameters()
     */
    @Override
    public List<AnnotatedParameter<X>> getParameters() {
        return wrappedAnnotatedMethod.getParameters();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedMember#getDeclaringType()
     */
    @Override
    public AnnotatedType<X> getDeclaringType() {
        return wrappedAnnotatedMethod.getDeclaringType();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedMember#isStatic()
     */
    @Override
    public boolean isStatic() {
        return wrappedAnnotatedMethod.isStatic();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getAnnotation(java.lang.Class)
     */
    @Override
    public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
        for (Annotation annotation : annotations) {
            if (annotationType.isInstance(annotation)) {
                return annotationType.cast(annotation);
            }
        }
 
        return null;
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getAnnotations()
     */
    @Override
    public Set<Annotation> getAnnotations() {
        return Collections.unmodifiableSet(annotations);
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getBaseType()
     */
    @Override
    public Type getBaseType() {
        return wrappedAnnotatedMethod.getBaseType();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getTypeClosure()
     */
    @Override
    public Set<Type> getTypeClosure() {
        return wrappedAnnotatedMethod.getTypeClosure();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#isAnnotationPresent(java.lang.Class)
     */
    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
        for (Annotation annotation : annotations) {
            if (annotationType.isInstance(annotation)) {
                return true;
            }
        }
 
        return false;
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedMethod#getJavaMember()
     */
    @Override
    public Method getJavaMember() {
        return wrappedAnnotatedMethod.getJavaMember();
    }
 
    /**
     * Adds the annotation.
     *
     * @param annotation the annotation
     */
    public void addAnnotation(Annotation annotation) {
        annotations.add(annotation);
    }
 
    /**
     * Removes the annotation.
     *
     * @param annotation the annotation
     */
    public void removeAnnotation(Annotation annotation) {
        annotations.remove(annotation);
    }
 
    /**
     * Removes the annotation.
     *
     * @param annotationType the annotation type
     */
    public void removeAnnotation(Class<? extends Annotation> annotationType) {
        Annotation annotation = getAnnotation(annotationType);
        if (annotation != null ) {
            removeAnnotation(annotation);
        }
    }
 
}
