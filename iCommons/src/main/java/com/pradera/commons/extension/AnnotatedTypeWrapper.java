package com.pradera.commons.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.inject.spi.AnnotatedConstructor;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AnnotatedTypeWrapper.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/06/2014
 * @param <T> the generic type
 */
public class AnnotatedTypeWrapper<T> implements AnnotatedType<T> {
	 
    /** The wrapped annotated type. */
    private AnnotatedType<T> wrappedAnnotatedType;
 
    /** The annotations. */
    private Set<Annotation> annotations = new HashSet<>();
    
    /** The annotated methods. */
    private Set<AnnotatedMethod<? super T>> annotatedMethods = new HashSet<>();
    
    /** The annotated fields. */
    private Set<AnnotatedField<? super T>> annotatedFields = new HashSet<>();
 
    /**
     * Instantiates a new annotated type wrapper.
     *
     * @param wrappedAnnotatedType the wrapped annotated type
     */
    public AnnotatedTypeWrapper(AnnotatedType<T> wrappedAnnotatedType) {
        this.wrappedAnnotatedType = wrappedAnnotatedType;
 
        annotations.addAll(wrappedAnnotatedType.getAnnotations());
        annotatedMethods.addAll(wrappedAnnotatedType.getMethods());
        annotatedFields.addAll(wrappedAnnotatedType.getFields());
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getAnnotation(java.lang.Class)
     */
    @Override
    public <A extends Annotation> A getAnnotation(Class<A> annotationType) {
        return wrappedAnnotatedType.getAnnotation(annotationType);
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getAnnotations()
     */
    @Override
    public Set<Annotation> getAnnotations() {
        return annotations;
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getBaseType()
     */
    @Override
    public Type getBaseType() {
        return wrappedAnnotatedType.getBaseType();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedType#getConstructors()
     */
    @Override
    public Set<AnnotatedConstructor<T>> getConstructors() {
        return wrappedAnnotatedType.getConstructors();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedType#getFields()
     */
    @Override
    public Set<AnnotatedField<? super T>> getFields() {
        return annotatedFields;
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedType#getJavaClass()
     */
    @Override
    public Class<T> getJavaClass() {
        return wrappedAnnotatedType.getJavaClass();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.AnnotatedType#getMethods()
     */
    @Override
    public Set<AnnotatedMethod<? super T>> getMethods() {
        return annotatedMethods;
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#getTypeClosure()
     */
    @Override
    public Set<Type> getTypeClosure() {
        return wrappedAnnotatedType.getTypeClosure();
    }
 
    /* (non-Javadoc)
     * @see javax.enterprise.inject.spi.Annotated#isAnnotationPresent(java.lang.Class)
     */
    @Override
    public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
        for (Annotation annotation : annotations) {
            if (annotationType.isInstance(annotation)) {
                return true;
            }
        }
 
        return false;
    }
 
}
