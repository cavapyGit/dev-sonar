package com.pradera.commons.resourcehandler;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;

import org.apache.commons.lang3.StringUtils;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IDepositaryResourceHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2013
 */
public class IDepositaryResourceHandler extends ResourceHandlerWrapper {
	
	/** The wrapped. */
	private ResourceHandler wrapped;
	
	private static final String JS_RESOURCE_LIBRARY ="js";
	
	private static final String CSS_RESOURCE_LIBRARY ="css";

	/**
	 * Instantiates a new i depositary resource handler.
	 *
	 * @param wrapped the wrapped
	 */
	public IDepositaryResourceHandler(ResourceHandler wrapped) {
		 this.wrapped = wrapped;
	}

	/* (non-Javadoc)
	 * @see javax.faces.application.ResourceHandlerWrapper#getWrapped()
	 */
	@Override
	public ResourceHandler getWrapped() {
		// TODO Auto-generated method stub
		return this.wrapped;
	}
	
    /* (non-Javadoc)
     * @see javax.faces.application.ResourceHandlerWrapper#createResource(java.lang.String, java.lang.String)
     */
    @Override
    public Resource createResource(String resourceName, String libraryName) {
        Resource resource = super.createResource(resourceName, libraryName);
        // here a check of library name could be necessary, etc.
        if(StringUtils.isNotBlank(libraryName)){
        	if(libraryName.equals(JS_RESOURCE_LIBRARY) || libraryName.equals(CSS_RESOURCE_LIBRARY)){
        		return new IDepositaryResource(resource);
        	} else {
        		return resource;
        	}
        } else {
        	return resource;
        }
       
    }

}
