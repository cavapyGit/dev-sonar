package com.pradera.commons.resourcehandler;

import java.io.IOException;
import java.util.Properties;

import javax.faces.application.Resource;
import javax.faces.application.ResourceWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.exception.global.DefaultExceptionHandler;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IDepositaryResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/10/2013
 */
public class IDepositaryResource extends ResourceWrapper {
	
	/** The log. */
	private Logger log = LoggerFactory.getLogger(IDepositaryResource.class);
	
	/** The resource. */
	private Resource resource;
	
	/** The number revision resources. */
	private String numberRevisionResources;

	/**
	 * Instantiates a new i depositary resource.
	 *
	 * @param resource the resource
	 */
	public IDepositaryResource(Resource  resource) {
		 this.resource = resource;
	}

	/* (non-Javadoc)
	 * @see javax.faces.application.ResourceWrapper#getWrapped()
	 */
	@Override
	public Resource getWrapped() {
		// TODO Auto-generated method stub
		return this.resource;
	}
	
	/* (non-Javadoc)
	 * @see javax.faces.application.ResourceWrapper#getRequestPath()
	 */
	@Override
    public String getRequestPath() {
		Properties fileConfig=new Properties();
		try {
		fileConfig.load(DefaultExceptionHandler.class.getResourceAsStream("/ApplicationConfiguration.properties"));
		numberRevisionResources = fileConfig.getProperty("numberRevisionResources");
		} catch(IOException iox) {
			log.error(iox.getMessage());
		}
        String requestPath = resource.getRequestPath();
        // using current revision
        if(requestPath.contains("?"))
            requestPath = requestPath + "&rv=" + numberRevisionResources;
        else
            requestPath = requestPath + "?rv=" + numberRevisionResources;
 
        return requestPath;
    }
 
    /* (non-Javadoc)
     * @see javax.faces.application.Resource#getContentType()
     */
    @Override
    public String getContentType() {
        return getWrapped().getContentType();
    }
    
    /* (non-Javadoc)
     * @see javax.faces.application.Resource#getLibraryName()
     */
    @Override
    public String getLibraryName() {
    	return resource.getLibraryName();
    }

    /* (non-Javadoc)
     * @see javax.faces.application.Resource#getResourceName()
     */
    @Override
    public String getResourceName() {
    	return resource.getResourceName();
    }

}
