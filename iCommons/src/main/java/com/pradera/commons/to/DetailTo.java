package com.pradera.commons.to;

import java.io.Serializable;
import java.util.List;

public class DetailTo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5384355440861124130L;
	
	private List<RegistryTO> lisRegistryTOs;
	
	/**
	 * 
	 */
	public DetailTo() {
	}
	/**
	 * @param lisRegistryTOs
	 */
	public DetailTo(List<RegistryTO> lisRegistryTOs) {
		this.lisRegistryTOs = lisRegistryTOs;
	}
	public List<RegistryTO> getLisRegistryTOs() {
		return lisRegistryTOs;
	}
	public void setLisRegistryTOs(List<RegistryTO> lisRegistryTOs) {
		this.lisRegistryTOs = lisRegistryTOs;
	}
}
