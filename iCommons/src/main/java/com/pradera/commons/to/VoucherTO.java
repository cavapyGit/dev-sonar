package com.pradera.commons.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class VoucherTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3235832413471047305L;

	@XmlElement(name="NUMCBTE")
	private Integer numVoucher;
	
	@XmlElement(name = "GLOSA")
	private String gloss;
	
	@XmlElement(name = "TIPOCAMBIOCAB")
	private BigDecimal exchangeRate;
	
	@XmlElement(name="FECHA")
	private Date date ;
	
	@XmlElement(name="TIPO")
	private String type;
	
	//@XmlElement(name= "DETALLE")
	private DetailTo detailTo;

	public Integer getNumVoucher() {
		return numVoucher;
	}

	public void setNumVoucher(Integer numVoucher) {
		this.numVoucher = numVoucher;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public DetailTo getDetailTo() {
		return detailTo;
	}

	public void setDetailTo(DetailTo detailTo) {
		this.detailTo = detailTo;
	}
	
}
