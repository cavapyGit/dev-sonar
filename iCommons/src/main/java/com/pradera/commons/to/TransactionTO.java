package com.pradera.commons.to;

import java.io.Serializable;
import java.util.List;

/**
 * @author jquino
 *
 */
public class TransactionTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**Id AccountinProcess */
	private Long idAccountingProcess;
	
	/**Lista de comprobantes*/
	private List<VoucherTO> listVoucherTOs;
	
	
	

	public List<VoucherTO> getListVoucherTOs() {
		return listVoucherTOs;
	}

	public void setListVoucherTOs(List<VoucherTO> listVoucherTOs) {
		this.listVoucherTOs = listVoucherTOs;
	}

	public Long getIdAccountingProcess() {
		return idAccountingProcess;
	}

	public void setIdAccountingProcess(Long idAccountingProcess) {
		this.idAccountingProcess = idAccountingProcess;
	}
	
}
