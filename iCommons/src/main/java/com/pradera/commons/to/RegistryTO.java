package com.pradera.commons.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class RegistryTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6831961725965588668L;
	
	
	private Integer numReg;
	
	private String account;
	
	private BigDecimal amount;
	
	private String operation;
	
	private String gloss;

	public Integer getNumReg() {
		return numReg;
	}

	public void setNumReg(Integer numReg) {
		this.numReg = numReg;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}
	
}
