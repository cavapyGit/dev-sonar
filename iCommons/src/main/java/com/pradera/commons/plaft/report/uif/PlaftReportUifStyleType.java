package com.pradera.commons.plaft.report.uif;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PlaftReportUifStyleType {
	
	/** The detail expiration purchase. */
	SUPERIOR_IZQUIERDO(new Integer(1),"SUPERIOR IZQUIERDO", ""),
	
	/** The purchase force expiration settled. */
	SUPERIOR_CENTRO(new Integer(2),"SUPERIOR CENTRO", ""),

	/** The purchase force expiration settled. */
	SUPERIOR_DERECHA(new Integer(3),"SUPERIOR DERECHA", ""),
	
	/** The purchase force expiration settled. */
	CENTRO_IZQUIERDA(new Integer(4),"CENTRO IZQUIERDA", ""),		

	/** The purchase force expiration settled. */
	CENTRO(new Integer(5),"CENTRO", ""),

	/** The purchase force expiration settled. */
	CENTRO_DERECHA(new Integer(6),"CENTRO DERECHA", ""),

	/** The purchase force expiration settled. */
	INFERIOR_IZQUIERDA(new Integer(7),"INFERIOR IZQUIERDA", ""),

	/** The purchase force expiration settled. */
	INFERIOR_CENTRO(new Integer(8),"INFERIOR CENTRO", ""),

	/** The purchase force expiration settled. */
	INFERIOR_DERECHA(new Integer(9),"INFERIOR DERECHA", ""),
	
	/** The purchase force expiration settled. */
	TEXTO_EN_NEGRITA(new Integer(1),"TEXTO EN NEGRITA", ""),
	
	/** The purchase force expiration settled. */
	TEXTO_NORMAL(new Integer(0),"TEXTO NORMAL", ""),
	;
		
	/** The value. */
	private String value;
	
	/** The code. */
	private Integer code;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The Constant list. */
	public static final List<PlaftReportUifStyleType> list = new ArrayList<PlaftReportUifStyleType>();
	
	/** The Constant listPk. */
	public static final List<Integer> listPk = new ArrayList<Integer>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PlaftReportUifStyleType> lookup = new HashMap<Integer, PlaftReportUifStyleType>();

	static{
		for(PlaftReportUifStyleType type: EnumSet.allOf(PlaftReportUifStyleType.class))
		{
			list.add(type);
			lookup.put(type.getCode(), type);
			listPk.add(type.getCode());
		}

	}

	/**
	 * Instantiates a new accord type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param mnemonic the mnemonic
	 */
	private PlaftReportUifStyleType(Integer code, String value, String mnemonic) {
		this.value = value;
		this.code = code;
		this.mnemonic = mnemonic;
	}


	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the accord type
	 */
	public static PlaftReportUifStyleType get(Integer code)
	{
		return lookup.get(code);
	}
}
