package com.pradera.commons.plaft.report.uif;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PlaftReportUifLabelType {
	
	/** The detail expiration purchase. */
	TITULO_ESTATICO(new Long(1),"TIULO ESTATICO", ""),
	
	/** The purchase force expiration settled. */
	TITULO_DINAMICO(new Long(2),"TITULO DINAMICO", ""),

	/** The purchase force expiration settled. */
	ETIQUETA_ESTATICA(new Long(3),"ETIQUETA ESTATICA", ""),
	
	/** The purchase force expiration settled. */
	ETIQUETA_DINAMICA(new Long(4),"ETIQUETA DINAMICA", ""),
		
	;
		
	/** The value. */
	private String value;
	
	/** The code. */
	private Long code;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The Constant list. */
	public static final List<PlaftReportUifLabelType> list = new ArrayList<PlaftReportUifLabelType>();
	
	/** The Constant listPk. */
	public static final List<Long> listPk = new ArrayList<Long>();
	
	/** The Constant lookup. */
	public static final Map<Long, PlaftReportUifLabelType> lookup = new HashMap<Long, PlaftReportUifLabelType>();

	static{
		for(PlaftReportUifLabelType type: EnumSet.allOf(PlaftReportUifLabelType.class))
		{
			list.add(type);
			lookup.put(type.getCode(), type);
			listPk.add(type.getCode());
		}

	}

	/**
	 * Instantiates a new accord type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param mnemonic the mnemonic
	 */
	private PlaftReportUifLabelType(Long code, String value, String mnemonic) {
		this.value = value;
		this.code = code;
		this.mnemonic = mnemonic;
	}


	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the accord type
	 */
	public static PlaftReportUifLabelType get(Long code)
	{
		return lookup.get(code);
	}
}
