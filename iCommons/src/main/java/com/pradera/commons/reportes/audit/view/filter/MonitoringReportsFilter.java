package com.pradera.commons.reportes.audit.view.filter;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;

/**
 * @author PraderaTechnologies
 *
 */
public class MonitoringReportsFilter implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idSystem;
	private Integer idReport;
	private String reportName;
	private Integer idModule;
	private Integer idOption;
	private Date startdate;
	private Date endDate;
	private Date maxDate;
	private String userName;
	private String userDescription;
	/**
	 * @return the idSystem
	 */
	public Integer getIdSystem() {
		return idSystem;
	}
	/**
	 * @param idSystem the idSystem to set
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}
	/**
	 * @return the idReport
	 */
	public Integer getIdReport() {
		return idReport;
	}
	/**
	 * @param idReport the idReport to set
	 */
	public void setIdReport(Integer idReport) {
		this.idReport = idReport;
	}
	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}
	/**
	 * @param reportName the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	/**
	 * @return the idModule
	 */
	public Integer getIdModule() {
		return idModule;
	}
	/**
	 * @param idModule the idModule to set
	 */
	public void setIdModule(Integer idModule) {
		this.idModule = idModule;
	}
	/**
	 * @return the idOption
	 */
	public Integer getIdOption() {
		return idOption;
	}
	/**
	 * @param idOption the idOption to set
	 */
	public void setIdOption(Integer idOption) {
		this.idOption = idOption;
	}
	/**
	 * @return the startdate
	 */
	public Date getStartdate() {
		return startdate;
	}
	/**
	 * @param startdate the startdate to set
	 */
	public void setStartdate(Date startdate) {
		Date today = CommonsUtilities.currentDate();
		if(startdate != null && today.compareTo(startdate)>=0){
			this.startdate = startdate;
		}else{
			this.startdate = null;
		}
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		Date today = CommonsUtilities.currentDate();
		if(endDate != null && today.compareTo(endDate)>=0 && (startdate==null || startdate.compareTo(endDate)<=0)){
			this.endDate = endDate;
		}else{
			this.endDate = null;
		}
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userDescription
	 */
	public String getUserDescription() {
		return userDescription;
	}
	/**
	 * @param userDescription the userDescription to set
	 */
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}
	/**
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		return CommonsUtilities.currentDate();
	}
	/**
	 * @param maxDate the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	
	
}
