package com.pradera.commons.security;

import java.io.IOException;
import java.util.Set;
import java.util.TimeZone;

import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.audit.UserAuditTrackingTO;
import com.pradera.commons.audit.service.UserAuditServiceBean;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.ConfiguratorParametersApplication;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.menu.PraderaMenu;
import com.pradera.commons.menu.MenuProducer;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.type.RequestMethodType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.usersession.UserAcctions;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
e * </ul>
 * 
 * The Class PraderaLoginFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/11/2012
 */
@WebFilter(urlPatterns = {"/pages/*"},filterName="PraderaLoginFilter",
initParams={@WebInitParam(name="LOGIN_URI",value="/login.xhtml"),
		@WebInitParam(name="SELECT_SYSTEM_URI",value="/pages/selectSystem.xhtml"),
		@WebInitParam(name="WELCOME_URI",value="/pages/welcome.xhtml"),
		@WebInitParam(name="REST_NOT_FOUND_URI",value="/RES_NOT_FOUND"),
		@WebInitParam(name="LOGOUT_URI",value=""),
		@WebInitParam(name="SECURITY",value="true")}) 
public class PraderaLoginFilter implements Filter { 

	/** The usuario info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private MenuProducer menuProducer;
	
	@Inject
	SessionRegistry sessionRegistry;
		
	/** The listado contenido estatico. */
	@Inject
	private Set<String> staticContectList;
	
	/** The client rest service. */
	@Inject
	private ClientRestService clientRestService;
	
	/** The excepcion. */
	@Inject
	transient Event<ExceptionToCatchEvent> excepcion;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@Inject @Configurable
	private String urlMonitoringReports;
	
	@Inject @Configurable
	private String urlInboxNotifications;
	
	@Inject @Configurable
	private String urlInboxProcess;
	
	@Inject @Configurable
	private String urlUserManualFiles;
	
	private String TICKETID = "tikectId";
	
	/** The report param. */
	private final String REPORT_PARAM = "report_id";
	
	/** The url login. */
	private String urlLogin;
	
	/** The url logout. */
	private String urlLogout;
	
	/** The url select system. */
	private String urlSelectSystem;
	
	/** The url welcome. */
	private String urlWelcome;
	
	/** The url pattern rest not found. */
	private String urlPatternRestNotFound;
	
	/** The security path. */
	@Inject @StageDependent
	private String securityPath;
	
	/** The security login full path. */
	@Inject @StageDependent
	private String securityLoginFullPath;
	
	/** The server path. */
	@Inject @StageDependent
	private String serverPath;
	
	@Inject
	StageApplication stageApplication;
	
	@Inject
	private ConfiguratorParametersApplication application;
	
	/** This attibute validate if the filter is enabled or not */
	private boolean validate;
	
	/** The module name application. */
	private String moduleNameApplication;
	
	/** The module url application. */
	private String moduleUrlApplication;
	
	@EJB
	UserAuditServiceBean userAuditService;
	
	private static String URL_CHANGEPASSWORD ="/pages/changepwd.xhtml";
	
	private static String URL_MULTIPLESESSION ="/pages/multipleUserSessions.xhtml";
	
	private static String URL_PASSWORDPOLICY ="/pages/ppolicy/passwordPolicy.xhtml";
	
	/**
	 * Instantiates a new pradera login filter.
	 */
	public PraderaLoginFilter() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		try{
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse)response;
			log.info("request type-----------------"+req.getMethod());
			log.info("server path --------------"+serverPath);
			log.info("validate  boolean value------------------"+validate);
			log.info("URL :"+req.getServletPath());
			String requestIpAddr = req.getRemoteAddr();
			String tikectId = req.getParameter("tikectId"); 
			/*Verificando que las solicitudes vengan de la misma IP*/
			if(!requestIpAddr.equals(application.getUrlLocalVpn()))
            if (requestIpAddr != null&&tikectId!=null) {
                log.info("IP de peticion cliente:"+requestIpAddr);
                String userName = req.getParameter("usuario");
				/**Verificando ip de registro para todas las solicitudes*/
				String registrationIp = clientRestService.getRegistrationIpAudit(userName, tikectId);
				if(!requestIpAddr.equals(registrationIp)){
					redirectLogin(req, res);
					return;
				}
            }
			//If validate attribute is false, then filterChain.doFilter
		    request.setCharacterEncoding("UTF-8");
		    /** * Set the default response content type and encoding */   
	        response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			/* don't allow caching */ 
			res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		    res.setHeader("Pragma", "No-cache");
			// to allow framing of this content only by this site
		    res.setHeader( "X-FRAME-OPTIONS", "SAMEORIGIN" );
		    res.setDateHeader("Expires", 0);
			if (!validate) {
				filterChain.doFilter(request, response);
			} else {
				String strUri = req.getServletPath();
				String reportId = req.getParameter(REPORT_PARAM);
				//concat report id for comparation againts menu
				if (StringUtils.isNotBlank(reportId)) {
					strUri = strUri + "?"+REPORT_PARAM+"="+reportId;
				}
				if (strUri.equals(urlLogout)
						|| strUri.equals(urlLogin)
						|| isStaticResource(strUri)) {
					filterChain.doFilter(request, response);
					if (userInfo != null && userInfo.getUserAccountSession() != null && 
							userInfo.getUserAccountSession().getIdUserAccountPk()!=null) {
						registerAuditInformation(req, strUri);
					}
				} else {
					//Check User login on memory
					if (userInfo != null && userInfo.getUserAccountSession() != null && 
							userInfo.getUserAccountSession().getIdUserAccountPk()!=null) {
						log.info("User have login session with userAccountpk: "+userInfo.getUserAccountSession().getIdUserAccountPk());
						if(strUri.equals(urlSelectSystem)
								|| strUri.equals(urlWelcome) 
								|| strUri.indexOf(urlPatternRestNotFound) != -1){
							if(Validations.validateIsNotNull(req.getSession().getAttribute("userfirstlogin")) &&
									(Boolean)req.getSession().getAttribute("userfirstlogin")){
								res.sendRedirect(req.getContextPath()+"/pages/changepwd.xhtml");
							}else if(Validations.validateIsNotNull(req.getSession().getAttribute("multipleUserSession")) &&
									(Boolean)req.getSession().getAttribute("multipleUserSession")){
								res.sendRedirect(req.getContextPath()+"/pages/multipleUserSessions.xhtml");
							}
							else{
							filterChain.doFilter(request, response);
							if(!strUri.equals(urlSelectSystem))
							registerAuditInformation(req, strUri);
							}
						} else {	
								//If the page was requested by Url (GET)
							processRequest(req, (HttpServletResponse)response, filterChain, strUri);
						}
						
												
					} else {
						if(strUri.equals(urlSelectSystem)
								|| strUri.equals(urlWelcome)) {
							log.info("Redirecting to Login because user not have Session and url is SelectSystem or Welcome ------------------"+strUri);
							redirectLogin(req,res);
						} else if(strUri.indexOf(urlPatternRestNotFound) != -1 ) {
							filterChain.doFilter(request, response);
						} else {
							if(req.getParameter(TICKETID) != null){
								getUserInfoService(req, res, filterChain, strUri);
								processRequest(req, res, filterChain, strUri);
							} else {
								log.info("Redirecting to Login because requets GET not have TICKETID------------------"+strUri);
								redirectLogin(req, res);																										
							}
						}
						
						
					}
				}
			}
			
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		
	}
	
	/**
	 * Redirect login.
	 *
	 * @param req the req
	 * @param resp the resp
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void redirectLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		String contextPath = req.getContextPath();
		if(isAjaxRequest(req)){
			 HttpServletResponse response = (HttpServletResponse) resp;
             response.getWriter().print(xmlPartialRedirectToPage(req, urlLogin));
             response.flushBuffer();
		}else{
			if(contextPath.equals(securityPath)){
				 if (!resp.isCommitted()) {
					 resp.sendRedirect(req.getContextPath()+"/error/redirectLogin.xhtml");
				 } 
			} else {
				StringBuilder parameters = new StringBuilder();
				if(stageApplication != null && stageApplication.equals(StageApplication.Development)) {
					parameters = new StringBuilder("?url_source=");
					parameters.append(moduleUrlApplication)
					.append("&module_source=")
					.append(moduleNameApplication);
					parameters.append("&idleTime_source="+"/idleTime");
				}else{
					parameters.append("?idleTime_source="+"/idleTime");
				}
				 if (!resp.isCommitted()) {
					 resp.sendRedirect(serverPath+securityPath+"/error/redirectLogin.xhtml"+parameters.toString());
				 } 
			}
		}
	}
	
	private String xmlPartialRedirectToPage(HttpServletRequest request, String page) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version='1.0' encoding='UTF-8'?>");
        sb.append("<partial-response><redirect url=\"").append(request.getContextPath()).append(request.getServletPath()).append("/error/redirectLogin.xhtml?page="+securityPath+page).append("\"/></partial-response>");
        return sb.toString();
    }

    private boolean isAjaxRequest(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
	
	/**
	 * This method validate if the uri was visited on some previous request 
	 *
	 * @param uri the uri
	 * @return true, if successful
	 */
	private boolean validateUriAlreadyVisitaded(String uri){
		if(Validations.validateIsNull(userInfo.getVisitedUrls()) || 
				userInfo.getVisitedUrls().isEmpty()){
			log.info("User session have empty getVisitedUrls when validation "+uri);
			return false;
		} else {
			for (String  currentUri : userInfo.getVisitedUrls()) {
				if(currentUri.equals(uri)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * This method validate if the user have access enabled to URL
	 *
	 * @param url the url
	 * @return true, if successful
	 */
	private boolean validateUrlUser(String url){
		if(Validations.validateIsNull(userInfo.getAvailiableOptionsUrl()) || 
				userInfo.getAvailiableOptionsUrl().isEmpty()){
			log.info("User Session  don't have getAvailiableOptionsUrl validation url"+url);
			return false;
		} else {
			for (String  currentUrl : userInfo.getAvailiableOptionsUrl()) {
//				log.info("serverPath-----------------"+serverPath);
				String fullUrlPath = null;
				//Check when url in full in menu
				if (currentUrl.contains(serverPath)) {
					fullUrlPath = currentUrl;
				} else {
					fullUrlPath=serverPath+currentUrl;
				}
//				log.info("url--------------------"+url);
//				log.info("currentUrl--------------------"+currentUrl);
				if(fullUrlPath.equals(url)){
					log.info("full url inside validate user-----------"+fullUrlPath);
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Checks if is static resource.
	 *
	 * @param uri the uri
	 * @return true, if is static resource
	 */
	private boolean isStaticResource(String uri) {
		boolean result = false;
		if(uri.lastIndexOf(".") > 0) {
			if (staticContectList.contains(uri.substring(uri.lastIndexOf(".")))) {
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param req
	 * @param res
	 * @param filterChain
	 * @param strUri
	 * @throws IOException
	 * @throws ServletException
	 */
	private void getUserInfoService(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain, String strUri) throws IOException, ServletException {
		String ticket = req.getParameter(TICKETID);
		UserInfo userInfoRemote = null;
		if (StringUtils.isNotBlank(ticket)) {
			try{
				String ticket_plain= AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(),ticket);
				userInfoRemote = clientRestService.findUserInfoBySessionId(ticket_plain, moduleNameApplication);
			if (userInfoRemote != null) {
				userInfo.setUserAccountSession(userInfoRemote.getUserAccountSession());
				userInfo.setPraderaMenuAll(userInfoRemote.getPraderaMenuAll());
				userInfo.setAvailiableOptionsUrl(userInfoRemote.getAvailiableOptionsUrl());
				userInfo.setCurrentSystem(userInfoRemote.getCurrentSystem());
				userInfo.setCurrentLanguage(userInfoRemote.getCurrentLanguage());
				userInfo.setUserAgencies(userInfoRemote.getUserAgencies());
				//Only get privileges not MenuModel
				menuProducer.createPrivilegeUser(userInfo);
				userInfo.setTimeZone(TimeZone.getDefault());
				//release PraderaMenu
				userInfo.setPraderaMenuAll(new PraderaMenu());
				//is necesary create session pk for remote logout
				req.getSession().setAttribute(GeneralConstants.USER_INFO_ID, userInfoRemote.getUserAccountSession().getIdUserSessionPk());
				if(StringUtils.isNotBlank(userInfoRemote.getCurrentLanguage())){
					req.getSession().setAttribute("locale", userInfoRemote.getCurrentLanguage());
				}
				//Register active sessions
				sessionRegistry.addSession(userInfo.getUserAccountSession().getUserName(), req.getSession());
			} else {
				log.info("Redirecting to Login page because remote services return not user logged------------------"+strUri+ " using token:"+ticket_plain);
				redirectLogin(req, res);
			}
		}catch(Exception e){
				log.error(e.getMessage());
			}
		} else {
			log.info("Redirecting to Login page because request is GET and not have ticket in url ------------------"+strUri);
			redirectLogin(req, res);
		}
	}
	
	/**
	 * Process the request
	 * @param req
	 * @param res
	 * @param filterChain
	 * @param strUri
	 * @throws IOException
	 * @throws ServletException
	 */
	private void processRequest(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain, String strUri) throws IOException, ServletException {
		if(req.getMethod().equals(RequestMethodType.GET.getValue())){
			log.info("Only check privileges in GET request ");
			StringBuilder sbUrl = new StringBuilder();
			sbUrl.append(serverPath).append(req.getContextPath()).append(strUri);
			registerAuditInformation(req, strUri);
			String ticket = req.getParameter(TICKETID);
			if(ticket != null ){
				try{
					ticket = AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(),ticket);
				}catch(Exception e){
					log.error(e.getMessage());
				}
				if(ticket != null && !ticket.equals(userInfo.getUserAccountSession().getTicketSession())){
					log.info("ticket from url and session are diferents so using getUserInfoService for validation");
					getUserInfoService(req, res, filterChain, strUri);
				}
			}
			if(validateUrlUser(sbUrl.toString())){
				if(validateUriAlreadyVisitaded(strUri)){											
//					userInfo.setUserAcctions(userInfo.getOptionPrivileges().get(strUri));
					filterChain.doFilter(req, res);
				} else {
					//check with url request
					UserAcctions userPrivilegios = userInfo.getOptionPrivileges().get(strUri);
					if(userPrivilegios != null) {
						if(Validations.hasPrivilegesOnPage(userPrivilegios)){
 							userInfo.setUserAcctions(userPrivilegios);
							userInfo.getVisitedUrls().add(strUri);
							filterChain.doFilter(req, res);
						} else {
							log.info("Redirecting to Login page because User don't have privilege from ------------------"+strUri);
							redirectLogin(req, res);
						}
					} else {
						log.info("Redirecting to Login page because user  don't have UserAcctions from------------------"+strUri);
						redirectLogin(req, res);
					}
				}
				
			} else {
				//check if url is report
				if(!strUri.contains(REPORT_PARAM)){
					//Check User login on memory
					if (userInfo != null && userInfo.getUserAccountSession() != null && 
							userInfo.getUserAccountSession().getIdUserAccountPk()!=null) {
						log.info("User have session but url is not in options available list-----------------"+strUri);
						if(userInfo.getUserAcctions()!= null || (strUri.equals(URL_MULTIPLESESSION) || strUri.equals(URL_CHANGEPASSWORD) || strUri.equals(URL_PASSWORDPOLICY))){
							log.info("Url have UserAcction from Menu url----"+strUri);
							if(userInfo.getAvailiableOptionsUrl()!=null){
								userInfo.getAvailiableOptionsUrl().add(serverPath+req.getContextPath()+strUri);
							}
							userInfo.getVisitedUrls().add(strUri);
							filterChain.doFilter(req, res);	
						} else if(checkUrlHeader(strUri)){ 
							//if is url from reports,process or messages 
							userInfo.getAvailiableOptionsUrl().add(serverPath+req.getContextPath()+strUri);
							userInfo.getVisitedUrls().add(strUri);
							//create default privilege search
							UserAcctions userPrivileg = new UserAcctions();
							userPrivileg.setSearch(true);
							userPrivileg.setIdPrivilegeSearch(BooleanType.YES.getCode());
							userInfo.getOptionPrivileges().put(strUri, userPrivileg);
							filterChain.doFilter(req, res);
						} else {
							//if put url without login close session
							log.info("User have session but is not in options available list the url-----------------"+strUri);
							closeSession(req, res,strUri);
						}
					} else {
						log.info("Close session because user don't have userAccountPK-----------------"+strUri);
						closeSession(req, res,strUri);
					}
				} else{
					//bab report always close session
					log.info("Close session because user don't have url report available-----------------"+strUri);
					closeSession(req, res,strUri);
				}
			}									
		} else {
			filterChain.doFilter(req, res);
		}
	}
	
	/**
	 * Check url header.
	 *
	 * @param strUri the str uri
	 * @return true, if successful
	 */
	private boolean checkUrlHeader(String strUri){
		if(urlMonitoringReports.contains(strUri) || urlInboxNotifications.contains(strUri) || urlInboxProcess.contains(strUri)
				|| urlUserManualFiles.contains(strUri)){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Close session when url is not configurate in user profile.
	 * 
	 * @param req the req
	 * @param res the res
	 * @param uri the uri
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public void closeSession(HttpServletRequest req,HttpServletResponse res,String uri) throws IOException, ServletException{
		log.info("to close session because url is not available in user session: "+uri);
		HttpSession httpSession = req.getSession(false);
		if(httpSession != null){
			//Check User login on memory
			if (userInfo != null && userInfo.getUserAccountSession() != null && 
					userInfo.getUserAccountSession().getIdUserSessionPk()!=null) {
				UserInfo userCloseSession = new UserInfo();
				userCloseSession.setUserAccountSession(new UserAccountSession());
				userCloseSession.getUserAccountSession().setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
				userCloseSession.setLogoutMotive(LogoutMotiveType.KILLSESSION);
				clientRestService.registerCloseSessionUser(userCloseSession);
			}
			 httpSession.invalidate();
		}
		redirectLogin(req, res);
	}
	
	/**
	 * 
	 * @param req
	 * @param strUri
	 */
	private void registerAuditInformation(HttpServletRequest req, String strUri) {
		try{
			if(!isAjaxRequest(req) && (!(req.getQueryString() != null && req.getQueryString().contains("tikectId") && req.getQueryString().contains("windowId")
					&& userInfo.getUserAccountSession().getIdUserSessionPk() != null))){
				strUri = moduleNameApplication +strUri;				
				UserAuditTrackingTO userAuditTracking = new UserAuditTrackingTO();
				userAuditTracking.setUrl(strUri);
				userAuditTracking.setQueryString(req.getQueryString());
				userAuditTracking.setUserSession(userInfo.getUserAccountSession().getIdUserSessionPk());
				userAuditService.registerUserAuditTracking(userAuditTracking);
			}
		}catch(Exception e){
			log.error(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		urlLogin = filterConfig.getInitParameter("LOGIN_URI");
		urlLogout = filterConfig.getInitParameter("LOGOUT_URI");
		urlSelectSystem = filterConfig.getInitParameter("SELECT_SYSTEM_URI");
		urlWelcome = filterConfig.getInitParameter("WELCOME_URI");
		urlPatternRestNotFound = filterConfig.getInitParameter("REST_NOT_FOUND_URI");
//		securityPath = filterConfig.getInitParameter("SECURITY_PATH_URI");
//		securityLoginFullPath = filterConfig.getInitParameter("SECURITY_LOGIN_FULL_PATH_URI");
//		serverPath = filterConfig.getInitParameter("SERVER_URI");
		String parameter = filterConfig.getInitParameter("SECURITY");
		if (parameter != null) {
			filterConfig.getServletContext().setAttribute("SECURITY", parameter);
			validate = Boolean.valueOf(parameter).booleanValue();
		}
		moduleNameApplication = filterConfig.getServletContext().getContextPath().substring(1);
		moduleUrlApplication = filterConfig.getServletContext().getInitParameter("URL_IDEPOSITARY_MODULE");
		if(!System.getProperty("os.name").substring(0,3).equalsIgnoreCase("win") && filterConfig.getServletContext().getContextPath().equals(securityPath)){
			try{
				userAuditService.closeAllSessions();
			}catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
	}
}
