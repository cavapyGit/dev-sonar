package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;


public enum LoginMessageType {
	LOGIN_SUCCESS(Integer.valueOf(1),"login.success"),
	LOGIN_FAIL(Integer.valueOf(-5),"login.fail"),
	INVALIED_CREDENTIALS(Integer.valueOf(2),"login.invalied.credentials"),
	NO_SUCH_OBJECT(Integer.valueOf(-4),"login.nosuch.object"),
	NO_SUCH_ATTRIBUTE(Integer.valueOf(-3),"login.nosuch.attribute"),
	PWD_MUST_CHANGE_FIRST_LOGIN(Integer.valueOf(3),"login.pwdmust.change.firstlogin"),
	PWD_MUST_CHANGE_GRACE_LOGIN(Integer.valueOf(4),"login.pwdmust.change.gracelogin"),
	PWD_MUST_CHANGE_EXP_WARN(Integer.valueOf(5),"login.pwdmust.change.expwarn"),
	PWD_EXPIRED(Integer.valueOf(-2),"login.pwdexpired"),
	PWD_IN_EXP_WARING(Integer.valueOf(6),"login.pwdinexp.waring"),
	USER_LOCKED(Integer.valueOf(-1),"login.user.locked"),
	USER_DISABLED(Integer.valueOf(-6),"login.user.disabled"),
	MFA_INVALIED(Integer.valueOf(9),"login.mfacode.invalied");
		
	private Integer code;
	private String value;

	public static final List<LoginMessageType> list = new ArrayList<LoginMessageType>();
	public static final Map<Integer, LoginMessageType> lookup = new HashMap<Integer, LoginMessageType>();

	static {
		for (LoginMessageType s : EnumSet.allOf(LoginMessageType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private LoginMessageType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	public static LoginMessageType get(Integer code) {
		return lookup.get(code);
	}
}