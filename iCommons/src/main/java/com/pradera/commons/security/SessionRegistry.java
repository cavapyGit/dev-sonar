package com.pradera.commons.security;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.servlet.http.HttpSession;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SessionRegistry.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20-abr-2015
 */
@ApplicationScoped
public class SessionRegistry implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8525698582081037931L;
	
	/** The sessions. */
	private ConcurrentHashMap<String, HttpSession> sessions;

	/**
	 * Instantiates a new session registry.
	 */
	public SessionRegistry() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		sessions = new ConcurrentHashMap<>(); 
	}
	
	/**
	 * Adds the session.
	 *
	 * @param user the user
	 * @param session the session
	 */
	public void addSession(String user,HttpSession session){
		sessions.put(user, session);
	}
	
	/**
	 * Removes the.
	 *
	 * @param user the user
	 */
	public void remove(String user){
		sessions.remove(user);
	}
	
	public HttpSession getSession(String user){
		return sessions.get(user);
	}
	
	

	/**
	 * Gets the sessions.
	 *
	 * @return the sessions
	 */
	public ConcurrentHashMap<String, HttpSession> getSessions() {
		return sessions;
	}

	/**
	 * Sets the sessions.
	 *
	 * @param sessions the sessions
	 */
	public void setSessions(ConcurrentHashMap<String, HttpSession> sessions) {
		this.sessions = sessions;
	}

}
