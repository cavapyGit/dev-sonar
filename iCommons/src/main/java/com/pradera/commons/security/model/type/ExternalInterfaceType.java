/**
 * 
 */
package com.pradera.commons.security.model.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ExternalInterfaceType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/11/2013
 */
public enum ExternalInterfaceType {
	
	NO(0,"NO"),
	
	YES(1,"YES");
	
	private ExternalInterfaceType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**The code of the External Interface */
	private Integer code;
	
	/**The value of the External Interface */
	private String value;
	
	public static final Map<Integer, ExternalInterfaceType> lookup = new HashMap<Integer, ExternalInterfaceType>();
	
	static {
		for (ExternalInterfaceType interfaceType : EnumSet.allOf(ExternalInterfaceType.class)) {
			lookup.put(interfaceType.getCode(), interfaceType);
		}
	}
	
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**Get the External Interface
	 * 
	 * @param code
	 * @return
	 */
	public static ExternalInterfaceType get(Integer code) {
		return lookup.get(code);
	}
}
