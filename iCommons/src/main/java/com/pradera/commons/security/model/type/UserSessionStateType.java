package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum UserSessionStateType .
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.0
 */
public enum UserSessionStateType {

	
	CONNECTED(Integer.valueOf(1), "CONECTADO","confirmed.png"),
	
	DISCONNECTED(Integer.valueOf(0), "DESCONECTADO","rejected.png"),
	/**Autenticacion no confirmada*/
	NOTLOGGEDCONFIRMED(Integer.valueOf(30),"AUTENTICACION NO CONFIRMADA",""),
	
	NOTLOGGEDREGISTERED(Integer.valueOf(29),"","");
	

	private Integer code;
	private String value;
	private String icon;

	public static final List<UserSessionStateType> list = new ArrayList<UserSessionStateType>();
	public static final Map<Integer, UserSessionStateType> lookup = new HashMap<Integer, UserSessionStateType>();

	static {
		for (UserSessionStateType s : EnumSet.allOf(UserSessionStateType.class)) {
			if(s.getCode().equals(Integer.valueOf(1)) || s.getCode().equals(Integer.valueOf(0)))
				list.add(s);
			lookup.put(s.getCode(), s);
		}
	}	

	private UserSessionStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}	
	
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	public static UserSessionStateType get(Integer code) {
		return lookup.get(code);
	}
}




