/*
 * 
 */
package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * The Enum ScheduleType.
 */
public enum UserSessionAccessType {
	
	/** The systems. */
	NORMAL(Integer.valueOf(1),"NORMAL"),
	
	/** The institutions. */
	SCHEDULEEXCEPTION(Integer.valueOf(2),"EXCEPCION DE HORARIO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<UserSessionAccessType> list = new ArrayList<UserSessionAccessType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, UserSessionAccessType> lookup = new HashMap<Integer, UserSessionAccessType>();

	static {
		for (UserSessionAccessType s : EnumSet.allOf(UserSessionAccessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private UserSessionAccessType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static UserSessionAccessType get(Integer code) {
		return lookup.get(code);
	}
}
