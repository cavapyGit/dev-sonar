package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum LanguageType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum LanguageType {
	
	/** The spanish. */
	SPANISH(Integer.valueOf(1),"es"),
	
	/** The english. */
	ENGLISH(Integer.valueOf(2),"en"),
	
	/** The frances. */
	FRANCES(Integer.valueOf(3),"fr");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<LanguageType> list = new ArrayList<LanguageType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, LanguageType> lookup = new HashMap<Integer, LanguageType>();

	static {
		for (LanguageType s : EnumSet.allOf(LanguageType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new language type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private LanguageType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the language type
	 */
	public static LanguageType get(Integer code) {
		return lookup.get(code);
	}
}

