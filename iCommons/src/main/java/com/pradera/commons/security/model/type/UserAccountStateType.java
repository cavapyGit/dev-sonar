package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * The Enum UserAccountStateType.
 */
public enum UserAccountStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(29),"REGISTRADO","registered.png"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(30),"CONFIRMADO","confirmed.png"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(31),"RECHAZADO","rejected.png"),
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(32),"BLOQUEADO","blocked.png"),
	
	/** The annulled. */
	ANNULLED(Integer.valueOf(158),"DADO DE BAJA","removed.png");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<UserAccountStateType> list = new ArrayList<UserAccountStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, UserAccountStateType> lookup = new HashMap<Integer, UserAccountStateType>();

	static {
		for (UserAccountStateType s : EnumSet.allOf(UserAccountStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * List some elements.
	 *
	 * @param userAccountStateTypeList the user account state type list
	 * @return list
	 */
	public static List<UserAccountStateType> listSomeElements(UserAccountStateType... userAccountStateTypeList){
		List<UserAccountStateType> retorno = new ArrayList<UserAccountStateType>();
		for(UserAccountStateType userAccountStateType: userAccountStateTypeList){
			retorno.add(userAccountStateType);
		}
		return retorno;
	}

	/**
	 * Instantiates a new user account state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private UserAccountStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the icon.
	 *
	 * @return icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return  description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return UserAccountStateType
	 */
	public static UserAccountStateType get(Integer code) {
		return lookup.get(code);
	}
}