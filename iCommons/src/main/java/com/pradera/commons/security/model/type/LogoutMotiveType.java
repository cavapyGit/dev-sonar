package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;


public enum LogoutMotiveType {
	LOGOUTSESSION(Integer.valueOf(128),"DESCONECTAR SESION"),
	KILLSESSION(Integer.valueOf(129),"MATAR SESION"),
	EXPIREDSESSION(Integer.valueOf(130),"SESION EXPIRADA"),
	SERVERRESTART(Integer.valueOf(131),"REINICIAR SERVIDOR"),
	INVALIEDCREDENTIALS(Integer.valueOf(132),"CREDENCIALES INVALIDAS"),
	INVALIEDACCESS(Integer.valueOf(133),"ACCESO INVALIDO"),
	PASSWORDRESET(Integer.valueOf(134),"REINICIAR PASSWORD"),
	KILLCURRENTUSERSESSION(Integer.valueOf(135),"CERRAR SESION ACTUAL"),
	KILLREMOTEUSERSESSION(Integer.valueOf(136),"CERRAR SESION REMOTA");
	private Integer code;
	private String value;

	public static final List<LogoutMotiveType> list = new ArrayList<LogoutMotiveType>();
	public static final Map<Integer, LogoutMotiveType> lookup = new HashMap<Integer, LogoutMotiveType>();

	static {
		for (LogoutMotiveType s : EnumSet.allOf(LogoutMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private LogoutMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	public static LogoutMotiveType get(Integer code) {
		return lookup.get(code);
	}
}