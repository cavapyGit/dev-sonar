package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum UserAccountResponsabilityType .
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.0
 */
public enum UserAccountResponsabilityType{

	TOTAL(Integer.valueOf(27),"TOTAL"),
	SHARED(Integer.valueOf(28),"COMPARTIDA");
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private UserAccountResponsabilityType(int ordinal,String name) {
		this.code = ordinal;
		this.value = name;
	}
	public static final List<UserAccountResponsabilityType> list = new ArrayList<UserAccountResponsabilityType>();
	public static final Map<Integer, UserAccountResponsabilityType> lookup = new HashMap<Integer, UserAccountResponsabilityType>();

	static {
		for (UserAccountResponsabilityType s : EnumSet.allOf(UserAccountResponsabilityType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	public static UserAccountResponsabilityType get(Integer code) {
		return lookup.get(code);
	}
	
}