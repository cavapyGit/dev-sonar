package com.pradera.commons.security;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.pradera.commons.audit.service.UserAuditServiceBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;

/**
 * The listener interface for receiving praderaHttpSession events.
 * The class that is interested in processing a praderaHttpSession
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addPraderaHttpSessionListener<code> method. When
 * the praderaHttpSession event occurs, that object's appropriate
 * method is invoked.
 *
 * @see PraderaHttpSessionEvent
 */
@WebListener
public class PraderaHttpSessionListener implements HttpSessionListener {
	
	/** The user audit service. */
	@EJB
	UserAuditServiceBean userAuditService;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {		

	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession objSession = sessionEvent.getSession();
		ServletContext objContext = objSession.getServletContext();
		boolean enableAuditing = false;
		String enableAudit = (String)objContext.getAttribute("SECURITY");
		if (enableAudit != null && Boolean.valueOf(enableAudit)) {
			enableAuditing = true;
		}
//TODO disabled close session method because this kill session in other wars actives
		enableAuditing = false;
		if(enableAuditing){
			long lnLastAccessTime = objSession.getLastAccessedTime();
			long lnCurrentTime = CommonsUtilities.currentDateTime().getTime();
			long lnInactiveIterval = objSession.getMaxInactiveInterval() * 1000;
			Long lngUserInfo = (Long) objSession.getAttribute(GeneralConstants.USER_INFO_ID);
			LogoutMotiveType motive = (LogoutMotiveType)objSession.getAttribute(GeneralConstants.LOGOUT_MOTIVE);
			if(lngUserInfo != null){
				log.debug("Having User Info");
				UserInfo userInfo = new UserInfo();
				userInfo.setUserAccountSession(new UserAccountSession());
				userInfo.getUserAccountSession().setIdUserSessionPk(lngUserInfo);
				userInfo.setLogoutMotive(motive);
				try{
					if(userInfo.getLogoutMotive() != null){
						log.debug("User Session expired because of "+userInfo.getLogoutMotive().getValue());
					}else if (lnInactiveIterval > 0){
						if (lnCurrentTime - lnLastAccessTime >= lnInactiveIterval) {
							userInfo.setLogoutMotive(LogoutMotiveType.EXPIREDSESSION);
							log.debug("Session expired because of inactive interval");
						}else{
							userInfo.setLogoutMotive(LogoutMotiveType.SERVERRESTART);
							log.debug("Session expired because of server restart");
						}
					}else{
						userInfo.setLogoutMotive(LogoutMotiveType.SERVERRESTART);
						log.debug("Session expired because of server restart");
					}
					userInfo.getUserAccountSession().setTicketSession(objSession.getId());
					userAuditService.closeSessionUser(userInfo);
				}catch(Exception e){
					log.error(e.getMessage());
				}
			}else{
				log.debug("User Info Null");
			}
		}else{
			log.debug("Disabled the Auditing");
		}
	}

}
