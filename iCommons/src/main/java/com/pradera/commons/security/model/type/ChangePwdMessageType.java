package com.pradera.commons.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;


public enum ChangePwdMessageType {
	PWD_CHANGE_FAIL(Integer.valueOf(0),"pwd.change.fail"),
	PWD_AUTH_FAIL(Integer.valueOf(1),"pwd.auth.fail"),
	PWD_NOT_MATCH_CONFIRM(Integer.valueOf(2),"pwd.notmatch.confirm"),
	PWD_SAME_OLD(Integer.valueOf(3),"pwd.same.old"),
	PWD_MIN_LENGH(Integer.valueOf(4),"pwd.min.lengh"),
	PWD_IN_HISTORY(Integer.valueOf(5),"pwd.in.history"),
	PWD_CHANGE_SUCCESS(Integer.valueOf(6),"pwd.change.success"),
	PWD_ALPHA_NUMERIC(Integer.valueOf(7),"pwd.alpha.numeric"),
	PWD_ILLEGAL_VALUE(Integer.valueOf(8),"pwd.illegal.value"),
	PWD_NULL(Integer.valueOf(9),"pwd.null"),
	PWD_NUMERIC(Integer.valueOf(10),"pwd.numeric"),
	PWD_FIRST_CHARACTERS_EQUAL(Integer.valueOf(11),"pwd.first.characters.equals"),
	PWD_NEW_SECURITY_VIOLATION(Integer.valueOf(12),"pwd.new.pass.fails.quality");
		
	private Integer code;
	private String value;

	public static final List<ChangePwdMessageType> list = new ArrayList<ChangePwdMessageType>();
	public static final Map<Integer, ChangePwdMessageType> lookup = new HashMap<Integer, ChangePwdMessageType>();

	static {
		for (ChangePwdMessageType s : EnumSet.allOf(ChangePwdMessageType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	private ChangePwdMessageType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	public static ChangePwdMessageType get(Integer code) {
		return lookup.get(code);
	}
}