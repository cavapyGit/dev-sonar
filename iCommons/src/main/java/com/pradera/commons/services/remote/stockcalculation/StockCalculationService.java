package com.pradera.commons.services.remote.stockcalculation;

import java.util.Date;

import javax.ejb.Remote;

import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface StockCalculationBoundary.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
@Remote
public interface StockCalculationService {
	
	/**
	 * Execute stock calculation monthly.
	 *
	 * @param cutoffDate the cutoff date
	 * @param registryDate the registry date
	 * @param loggerUser the logger user
	 * @return the long
	 */
	public Long executeStockCalculationMonthly(Date cutoffDate,Date registryDate,LoggerUser loggerUser) throws ServiceException;
	
	public void executeStockMarketFact(Long stockCalculationId, LoggerUser loggerUser) throws ServiceException;
	
	public Long executeStockCalculationMarketFactByCutDate(Date cutoffDate,Date registryDate, LoggerUser loggerUser) throws ServiceException;
	
	//public Long executeStockCalculationMarketFactByCutDate(Date cutoffDate,Date registryDate, LoggerUser loggerUser) throws ServiceException;

}
