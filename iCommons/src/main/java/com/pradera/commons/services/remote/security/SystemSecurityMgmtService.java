package com.pradera.commons.services.remote.security;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Remote;

/**
 * <ul>
 * <li>
 * Project PraderaCommons.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The interface ProfileAndOptionsReportRemote.
 *
 * @author PraderaTechnologies.
 * @version 1.0 ,
 */
@Remote
public interface SystemSecurityMgmtService extends Serializable{
	/**
	 * to get systems Profiles and Options
	 * @return List<Object[]>
	 */
	public List<Object[]> getSystemProfileAndOptions(Integer idSystem,String strProfileMn,Integer state) throws  Exception;
}
