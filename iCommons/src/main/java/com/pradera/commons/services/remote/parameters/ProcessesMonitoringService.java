package com.pradera.commons.services.remote.parameters;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import com.pradera.commons.processes.remote.view.filter.MonitorSystemProcessFilter;
import com.pradera.integration.exception.ServiceException;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ProcessesMonitoringService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2014
 */
@Remote
public interface ProcessesMonitoringService extends Serializable{

	/**
	 * The method for getting the Audit process details from the data base.
	 *
	 * @param optionPrevilages the option previlages
	 * @param startDate starting date
	 * @param endDate starting date
	 * @return the List of Process beans
	 * @throws ServiceException the service exception
	 */

	public List<Object[]> getAuditProcessesDetails(
			List<Integer> optionPrevilages, Date startDate, Date endDate)
			throws ServiceException;

	/**
	 * Method for getting the parameters of selected process.
	 *
	 * @param id Integer
	 * @return the values of parameters
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getProcessParameters(Long id) throws ServiceException;

	/**
	 * To get the list of macroProcesses.
	 *
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getMacroProcessList()throws ServiceException;
	
	/**
	 * To get the list of macroProcesses by id System.
	 *
	 * @param idSystem the id system
	 * @return the values of macroProcess
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getMacroProcessListbyidSystem(Integer idSystem)throws ServiceException;

	/**
	 * To get the list of processlevels.
	 *
	 * @param idBusinessProcess the id business process
	 * @return the values of processlevels
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getProcessLevels(List<Long> idBusinessProcess)throws ServiceException;

	/**
	 * To get the ProcessDetails.
	 *
	 * @param monitorSystemProcessFilter the monitor system process filter
	 * @return the values of processDetails
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getProcessDetails(
			MonitorSystemProcessFilter monitorSystemProcessFilter)throws ServiceException;

	/**
	 * Gets the business process details.
	 *
	 * @param refProcess the ref process
	 * @return the values of businessProcessDetails
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getBusinessProcessDetails(Long refProcess)throws ServiceException;
	
	/**
	 * Gets the system list.
	 *
	 * @return the system list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getSystemList() throws ServiceException;

}
