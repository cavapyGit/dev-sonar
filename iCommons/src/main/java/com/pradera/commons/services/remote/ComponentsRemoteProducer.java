package com.pradera.commons.services.remote;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.services.remote.parameters.ProcessesMonitoringService;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.services.remote.security.SystemSecurityMgmtService;
import com.pradera.commons.services.remote.stockcalculation.StockCalculationService;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.component.settlements.service.SettlementRemoteService;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ComponentRemoteProducer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
@ApplicationScoped
public class ComponentsRemoteProducer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5680045761597744676L;
	
	@Inject 
	StageApplication stateApplication;
	
	/**
	 * Instantiates a new component remote producer.
	 */
	public ComponentsRemoteProducer() {
		// TODO Auto-generated constructor stub
	}
	
	@Produces
	public AccountsRemoteService getAccountsInterfaceService(){
		AccountsRemoteService accountsInterfaceService = (AccountsRemoteService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.ACCOUNTS.getValue()).withBeanName(RemoteServicesConstants.ACCOUNT_INTERFACE_SERVICE_IMPL).
				withBusinessInterface(AccountsRemoteService.class).locateRemote(stateApplication.name());
		return accountsInterfaceService;
	}
	
	@Produces
	public CustodyRemoteService getCustodyInterfaceService(){
		CustodyRemoteService custodyInterfaceService = (CustodyRemoteService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.CUSTODY.getValue()).withBeanName(RemoteServicesConstants.CUSTODY_INTERFACE_SERVICE_IMPL).
				withBusinessInterface(CustodyRemoteService.class).locateRemote(stateApplication.name());
		return custodyInterfaceService;
	}
	
	@Produces
	public SecuritiesRemoteService getSecuritiesInterfaceService(){
		SecuritiesRemoteService securitiesInterfaceService = (SecuritiesRemoteService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.SECURITIES.getValue()).withBeanName(RemoteServicesConstants.SECURITIES_INTERFACE_SERVICE_IMPL).
				withBusinessInterface(SecuritiesRemoteService.class).locateRemote(stateApplication.name());
		return securitiesInterfaceService;
	}
	
	@Produces
	public CorporateProcessRemoteService getCorporativesInterfaceService(){
		CorporateProcessRemoteService corporativesInterfaceService = (CorporateProcessRemoteService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.CORPORATE.getValue()).withBeanName(RemoteServicesConstants.CORPORATIVES_INTERFACE_SERVICE_IMPL).
				withBusinessInterface(CorporateProcessRemoteService.class).locateRemote(stateApplication.name());
		return corporativesInterfaceService;
	}
	
	@Produces
	public SettlementRemoteService getSettlementsInterfaceService(){
		SettlementRemoteService settlementsInterfaceService = (SettlementRemoteService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.SETTLEMENT.getValue()).withBeanName(RemoteServicesConstants.SETTLEMENTS_INTERFACE_SERVICE_IMPL).
				withBusinessInterface(SettlementRemoteService.class).locateRemote(stateApplication.name());
		return settlementsInterfaceService;
	}
	
	@Produces
	public StockCalculationService getStockCalculationService(){
		StockCalculationService stockCalculationService =  (StockCalculationService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.CORPORATE.getValue()).withBeanName(RemoteServicesConstants.STOCK_CALCULATION_SERVICE_IMPL).
				withBusinessInterface(StockCalculationService.class).locateRemote(stateApplication.name());
		return stockCalculationService;
	}
	
	@Produces
	public SystemSecurityMgmtService getSystemSecurityMgmtService(){
		SystemSecurityMgmtService systemSecurityMgmtService =  (SystemSecurityMgmtService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.SECURITY.getValue()).withBeanName(RemoteServicesConstants.SYSTEM_SECURITY_SERVICE_IMPL).
				withBusinessInterface(SystemSecurityMgmtService.class).locateRemote(stateApplication.name());
		return systemSecurityMgmtService;
	}
	
	@Produces
	public ProcessesMonitoringService getProcessesMonitoringService(){
		ProcessesMonitoringService processesMonitoringService =  (ProcessesMonitoringService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.PARAMETER.getValue()).withBeanName(RemoteServicesConstants.PROCESS_MONITORING_SERVICE_IMPL).
				withBusinessInterface(ProcessesMonitoringService.class).locateRemote(stateApplication.name());
		return processesMonitoringService;
	}

	@Produces
	public BillingConexionService getBillingConexionService(){
		BillingConexionService billingConexionService =  (BillingConexionService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BILLING.getValue()).withBeanName(RemoteServicesConstants.BILLING_CONEXION_SERVICE_IMPL).
				withBusinessInterface(BillingConexionService.class).locateRemote(stateApplication.name());
		return billingConexionService;
	}
	
	@Produces
	public ReportGenerationService getReportGenerationService(){
		ReportGenerationService reportGenerationService =  (ReportGenerationService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.REPORT.getValue()).withBeanName(RemoteServicesConstants.REPORT_GENERATION_SERVICE_IMPL).
				withBusinessInterface(ReportGenerationService.class).locateRemote(stateApplication.name());
		return reportGenerationService;
	}
	
}

