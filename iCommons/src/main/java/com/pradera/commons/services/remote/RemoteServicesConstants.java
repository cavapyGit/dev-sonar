package com.pradera.commons.services.remote;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface RemoteServicesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/03/2014
 */
public interface RemoteServicesConstants {
	
	/** The initial context factory. */
	String INITIAL_CONTEXT_FACTORY = "remote.connection.initial.context.factory";
	
	/** The url pkg prefixes. */
	String URL_PKG_PREFIXES="remote.connection.url.pkg.prefixes";
	
	/** The default host. */
	String DEFAULT_HOST = "{0}.remote.connection.default.host";
	
	/** The default port. */
	String DEFAULT_PORT = "{0}.remote.connection.default.port";
	
	/** The default user. */
	String DEFAULT_USER = "{0}.remote.connection.default.username";
	
	/** The default password. */
	String DEFAULT_PASSWORD = "{0}.remote.connection.default.password";
	
	/** The module host. */
	String MODULE_HOST = "{0}.remote.connection.{1}.host";
	
	/** The module port. */
	String MODULE_PORT = "{0}.remote.connection.{1}.port";
	
	/** The module user. */
	String MODULE_USER = "{0}.remote.connection.{1}.username";
	
	/** The module password. */
	String MODULE_PASSWORD = "{0}.remote.connection.{1}.password";
	
	/**********************Module Interface Integration******************/
	
	String ACCOUNT_INTERFACE_SERVICE_IMPL ="AccountsRemoteServiceBean";
	
	String SECURITIES_INTERFACE_SERVICE_IMPL="SecuritiesRemoteServiceBean";
	
	String CUSTODY_INTERFACE_SERVICE_IMPL="CustodyRemoteServiceBean";
	
	String CORPORATIVES_INTERFACE_SERVICE_IMPL="CorporateProcessRemoteServiceBean";
	
	String SETTLEMENTS_INTERFACE_SERVICE_IMPL= "SettlementRemoteServiceBean";
	
	/**********************Components Business Integration******************/
	
	String INTERFACE_COMPONENT_SERVICE_IMPL ="InterfaceComponentServiceBean";
	
	String ACCOUNT_COMPONENT_SERVICE_IMPL ="AccountsComponentServiceBean";
	
	String SECURITIES_COMPONENT_SERVICE_IMPL="SecuritiesComponentServiceBean";
	
	String FUNDS_COMPONENT_SERVICE_IMPL="FundsComponentServiceBean";
	
	String SPLIT_COUPON_COMPONENT_SERVICE_IMPL="SplitCouponServiceBean";
	
	String SALE_PURCHASE_UPDATE_COMPONENT_SERVICE_IMPL = "SalePurchaseUpdateServiceBean";
	
	String VAULT_CONTROL_SERVICE_IMPL = "VaultControlServiceBean";
	
	/******************************* Components remote RMI    ********************************************/
	
	String STOCK_CALCULATION_SERVICE_IMPL="StockCalculationRemoteServiceFacade";
	
	String SYSTEM_SECURITY_SERVICE_IMPL="SystemSecurityMgmtServiceFacade";
	
	String PROCESS_MONITORING_SERVICE_IMPL="MonitoringProcessesServiceFacade";
	
	String BILLING_CONEXION_SERVICE_IMPL="ConexionRemoteServiceFacade";
	
	String REPORT_GENERATION_SERVICE_IMPL="ReportGenerationServiceFacade";
	
}
