package com.pradera.commons.services.remote.reports;

import java.io.IOException;
import java.io.Serializable;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import com.pradera.commons.report.ReportFile;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.utils.FileData;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface ReportGenerationService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 30/09/2013
 */
@Remote
public interface ReportGenerationService extends Serializable {

	/**
	 * Save report execution.
	 *
	 * @param reportId the report id
	 * @param parameters the parameters
	 * @param parameterDetails the parameter details
	 * @param reportUser the report user who have grant to see report
	 * @param loggerUser the logger user
	 * @return the id report logger generate
	 * @throws ServiceException the service exception
	 */
    public Long saveReportExecution(Long reportId,Map<String,String> parameters,Map<String,String> parameterDetails,
    		ReportUser reportUser, LoggerUser loggerUser) throws ServiceException;
    
    /**
     * Gets the data report.
     *
     * @param reportLoggerId the report logger id
     * @return the data report
     * @throws ServiceException the service exception
     */
    public byte[] getDataReport(Long reportLoggerId)throws ServiceException;
    
    /**
     * Sets the accreditation certificate.
     *
     * @param reportId the report id
     * @param accreditationId the accreditation id
     * @param username the username
     * @param loggerUser the logger user
     * @throws ServiceException the service exception
     */
    public void setAccreditationCertificate(Long reportId, Long accreditationId, String username, LoggerUser loggerUser) throws ServiceException;
    
    /**
     * Save report file.
     *
     * @param reportId the report id
     * @param reportUser the report user
     * @param parameters the parameters
     * @param parameterDetails the parameter details
     * @param loggerUser the logger user
     * @param files the files
     * @return the long
     * @throws ServiceException the service exception
     */
    public Long saveReportFile(Long reportId,ReportUser reportUser, Map<String,String> parameters,Map<String,String> parameterDetails,
    		LoggerUser loggerUser,List<ReportFile> files) throws ServiceException;
    
    /**
     * Send report participant unification.
     *
     * @param reportId the report id
     * @param idReportUnification the id report unification
     * @param loggerUser the logger user
     * @throws ServiceException the service exception
     */
    public void sendReportParticipantUnification(Long reportId, Long idReportUnification, LoggerUser loggerUser) throws ServiceException;

    /**
     * Send modifying of data.
     *
     * @param reportId the report id
     * @param holderRequestId the holder request id
     * @param username the username
     * @param loggerUser the logger user
     * @param nameParam the name param
     * @throws ServiceException the service exception
     */
    public void sendModifyingOfData(Long reportId, Long holderRequestId, String username, LoggerUser loggerUser, String nameParam) throws ServiceException;
    
    /**
     * Save report interfaces.
     *
     * @param reportId the report id
     * @param parameters the parameters
     * @param parameterDetails the parameter details
     * @param reportUser the report user
     * @param loggerUser the logger user
     * @return the long
     * @throws ServiceException the service exception
     */
    public Long saveReportInterfaces(Long reportId, Map<String,String> parameters, Map<String,String> parameterDetails, 
    		ReportUser reportUser, LoggerUser loggerUser) throws ServiceException ;
    
    /**
     * Gets the data report object.
     *
     * @param reportId the report id
     * @return the data report object
     * @throws ServiceException the service exception
     */
    public Object [] getDataReportObject(Long reportId) throws ServiceException ;
    
    /**
     * Send ftp upload.
     *
     * @param listFileData the list file data
     * @param parameterTablePk the parameter table pk
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public boolean sendFtpUpload(List<FileData> listFileData, Integer parameterTablePk) throws IOException ;
    
    /**
     * 
     * REPORTE DE CARTERA BUSQUEDA DE PERSONAS
     * 
     * */
    public void sendApplicantConsult(Long reportId, Long holderId, Long idApplicantConsultRequest, String holderDesc, String username, LoggerUser loggerUser) throws ServiceException;
    
    public byte[]  signPdfDocument(byte[] pdfDoc, int pagNum, PrivateKey key, Certificate[] chain, int x, int y);
}
