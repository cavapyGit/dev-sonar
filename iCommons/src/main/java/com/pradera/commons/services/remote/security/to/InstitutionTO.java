package com.pradera.commons.services.remote.security.to;

import com.pradera.integration.contextholder.LoggerUser;

public class InstitutionTO {

	private String name;
	private String mnemonic;
	private Integer type;
	private String issuerCode;
	private Long participantCode;
	private Integer state;
	
	private LoggerUser loggerUser;

	public InstitutionTO() {
		
	}
	
	public InstitutionTO(String name, String mnemonic, Integer type, String issuerCode, Long participantCode) {
		this.name = name;
		this.mnemonic = mnemonic;
		this.type = type;
		this.issuerCode = issuerCode;
		this.participantCode = participantCode;
	}
	
	public InstitutionTO(String name, String mnemonic, Integer type, String issuerCode, Long participantCode, Integer state) {
		this.name = name;
		this.mnemonic = mnemonic;
		this.type = type;
		this.issuerCode = issuerCode;
		this.participantCode = participantCode;
		this.state = state;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getIssuerCode() {
		return issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	
	public Long getParticipantCode() {
		return participantCode;
	}

	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}

	public LoggerUser getLoggerUser() {
		return loggerUser;
	}

	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
	
	
}
