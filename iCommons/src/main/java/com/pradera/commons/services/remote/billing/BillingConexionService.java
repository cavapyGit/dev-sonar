package com.pradera.commons.services.remote.billing;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.Remote;

import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface ConexionRemoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2014
 */
@Remote
public interface BillingConexionService extends Serializable {
	
	/**
	 * Find billing service remote.
	 *
	 * @param parameters the parameters
	 * @param loggerUser the logger user
	 * @return the map
	 */
	public Map 	 findBillingServiceRemote(Map parameters ,LoggerUser loggerUser ) throws ServiceException;
	
	/**
	 * Save remote processed service.
	 *
	 * @param parameters the parameters
	 * @param loggerUser the logger user
	 * @throws ServiceException 
	 */
	public void  saveRemoteProcessedService(Map parameters , LoggerUser loggerUser) throws ServiceException;

}
