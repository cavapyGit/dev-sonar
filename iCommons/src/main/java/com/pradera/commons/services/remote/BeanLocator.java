package com.pradera.commons.services.remote;

import java.text.MessageFormat;
import java.util.Properties;

import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BeanLocator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 31/03/2014
 */
public class BeanLocator {
	
	/**
	 * <ul>
	 * <li>
	 * Project iDepositary.
	 * Copyright PraderaTechnologies 2014.</li>
	 * </ul>
	 * 
	 * The Class GlobalJNDIName.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 31/03/2014
	 */
	/** The Constant PROPERTY_FILE. */
	private final static String PROPERTY_FILE = "/ejb-client.properties";
	
	
	/**
	 * <ul>
	 * <li>
	 * Project iDepositary.
	 * Copyright PraderaTechnologies 2015.</li>
	 * </ul>
	 * 
	 * The Class GlobalJNDIName.
	 *
	 * @author PraderaTechnologies.
	 * @version 1.0 , 01-sep-2015
	 */
	public static class GlobalJNDIName {
		
		/** The builder. */
		private StringBuilder builder;
		
		/** The Constant PREFIX. */
		private final static String PREFIX = "java:global";
		
		/** The Constant SEPARATOR. */
		private final static String SEPARATOR = "/";
		
		/** The module name. */
		private String moduleName;
		
		/** The bean name. */
		private String beanName;
		
		/** The business interface name. */
		private String businessInterfaceName;

		/**
		 * Instantiates a new global jndi name.
		 */
		public GlobalJNDIName() {
			this.builder = new StringBuilder();
			
		}

		/**
		 * With module name.
		 *
		 * @param moduleName the module name
		 * @return the global jndi name
		 */
		public GlobalJNDIName withModuleName(String moduleName) {
			this.moduleName = moduleName;
			return this;
		}

		/**
		 * With bean name.
		 *
		 * @param beanName the bean name
		 * @return the global jndi name
		 */
		public GlobalJNDIName withBeanName(String beanName) {
			this.beanName = beanName;
			return this;
		}

		/**
		 * With bean name.
		 *
		 * @param beanClass the bean class
		 * @return the global jndi name
		 */
		public GlobalJNDIName withBeanName(Class beanClass) {
			return withBeanName(computeBeanName(beanClass));
		}

		/**
		 * With business interface.
		 *
		 * @param interfaze the interfaze
		 * @return the global jndi name
		 */
		public GlobalJNDIName withBusinessInterface(Class interfaze) {
			this.businessInterfaceName = interfaze.getName();
			return this;
		}

		/**
		 * Compute bean name.
		 *
		 * @param beanClass the bean class
		 * @return the string
		 */
		String computeBeanName(Class beanClass) {
			Stateless stateless = (Stateless) beanClass.getAnnotation(Stateless.class);
			if (stateless != null && isNotEmpty(stateless.name())) {
				return stateless.name();
			}
			Stateful stateful = (Stateful) beanClass.getAnnotation(Stateful.class);
			if (stateful != null && isNotEmpty(stateful.name())) {
				return stateful.name();
			}
			Singleton singleton = (Singleton) beanClass.getAnnotation(Singleton.class);
			if (singleton != null && isNotEmpty(singleton.name())) {
				return singleton.name();
			}
			return beanClass.getSimpleName();
		}

		/**
		 * Checks if is not empty.
		 *
		 * @param name the name
		 * @return true, if is not empty
		 */
		private boolean isNotEmpty(String name) {
			return (name != null && !name.isEmpty());
		}

		/**
		 * As string.
		 *
		 * @return the string
		 */
		public String asString() {
			this.builder.append(PREFIX).append(SEPARATOR);
			this.builder.append(moduleName).append(SEPARATOR);
			this.builder.append(beanName);
			if (businessInterfaceName != null) {
				this.builder.append("!").append(businessInterfaceName);
			}
			return this.builder.toString();
		}
		
		/**
		 * As string.
		 *
		 * @return the string
		 */
		public String asStringRemote() {
			this.builder.append(moduleName).append(SEPARATOR).append(SEPARATOR);
			this.builder.append(beanName);
			if (businessInterfaceName != null) {
				this.builder.append("!").append(businessInterfaceName);
			}
			return this.builder.toString();
		}

		/**
		 * Locate.
		 *
		 * @param <T> the generic type
		 * @param stage the stage
		 * @param clazz the clazz
		 * @return the t
		 */
		public <T> T locate(String stage,Class<T> clazz) {
			return BeanLocator.lookup(stage,clazz, asString());
		}

		/**
		 * Locate.
		 *
		 * @param stage the stage
		 * @return the object
		 */
		public Object locateRemote(String stage) {
			return BeanLocator.lookup(stage,asStringRemote());
		}
		
		/**
		 * Locate.
		 *
		 * @return the object
		 */
		public Object locate(){
			return BeanLocator.lookup(null, asString());
		}
	}

	/**
	 * Lookup.
	 *
	 * @param <T> the generic type
	 * @param stage the stage
	 * @param clazz the clazz
	 * @param jndiName the jndi name
	 * @return the t
	 */
	public static <T> T lookup(String stage,Class<T> clazz, String jndiName) {
		Object bean = lookup(stage,jndiName);
		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
	}

	/**
	 * Lookup.
	 *
	 * @param stage the stage
	 * @param jndiName the jndi name
	 * @return the object
	 */
	public static Object lookup(String stage,String jndiName) {
		Context context = null;
		Properties globalConfiguration = new Properties();
		
		try {
			globalConfiguration.load(PraderaLogger.class.getResourceAsStream(BeanLocator.PROPERTY_FILE));
		} catch (Exception ex) {
			System.out.println("Cannot load configuration: " + ex);
		}
		
		try {
			if(StringUtils.isBlank(stage)){
				context = new InitialContext();
			}  else {
				//call remote
				String moduleHost = globalConfiguration.getProperty(MessageFormat.format(RemoteServicesConstants.DEFAULT_HOST, stage));
				String modulePort = globalConfiguration.getProperty(MessageFormat.format(RemoteServicesConstants.DEFAULT_PORT, stage));
				String moduleUser = globalConfiguration.getProperty(MessageFormat.format(RemoteServicesConstants.DEFAULT_USER, stage));
				String modulePassword = globalConfiguration.getProperty(MessageFormat.format(RemoteServicesConstants.DEFAULT_PASSWORD, stage));
				Properties props = new Properties(); 
				props.put(Context.INITIAL_CONTEXT_FACTORY, globalConfiguration.getProperty(RemoteServicesConstants.INITIAL_CONTEXT_FACTORY));
				props.put(Context.URL_PKG_PREFIXES, globalConfiguration.getProperty(RemoteServicesConstants.URL_PKG_PREFIXES));
				props.put(Context.PROVIDER_URL, getContextURL(moduleHost.split(","), modulePort.split(","), "", 0));
				props.put(Context.SECURITY_PRINCIPAL, moduleUser);
				props.put(Context.SECURITY_CREDENTIALS, modulePassword);
				context = new InitialContext(props);
			}
			return context.lookup(jndiName);
		} catch (NamingException ex) {
			ex.printStackTrace();
			System.out.println("Cannot connect to bean: "
					+ jndiName + " Reason: " + ex.getCause().toString());
			throw new IllegalStateException("Cannot connect to bean: "
					+ jndiName + " Reason: " + ex, ex.getCause());
		} finally {
			try {
				if(context != null){
					context.close();
				}
			} catch (NamingException ex) {
				throw new IllegalStateException(
						"Cannot close InitialContext. Reason: " + ex,
						ex.getCause());
			}
		}
	}
	
	/**
	 * Gets the context url.
	 *
	 * @param hosts the hosts
	 * @param ports the ports
	 * @param url the url
	 * @param index the index
	 * @return the context url
	 */
	public static String getContextURL(String[] hosts, String[] ports, String url, int index){
		if(hosts != null && hosts.length > index && ports != null && ports.length > index ){
			if(url != null && url.length() > 0){
				url = url+",";
			}
			url += "http-remoting://"+hosts[index]+":"+ports[index];
			//url += "remote://"+hosts[index]+":"+ports[index];
			url = getContextURL(hosts, ports, url, ++index);
		}
		return url;
	}
}
