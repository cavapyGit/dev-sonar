package com.pradera.commons.logging;

import java.io.Serializable;

//import org.apache.log4j.Logger;
//used sl4j to improve logging performance.
import org.slf4j.Logger;

public class SimpleLogger implements PraderaLogger, Serializable{
	
	private Logger logger; 

	public SimpleLogger(Logger logger) {
		this.logger = logger;
	}


	public void info(String mensaje) {
		logger.info(mensaje);

	}
	
	public void error(String mensaje) {
		logger.error(mensaje);
	}

	public Logger getLogger() {
		// TODO Auto-generated method stub
		return this.logger;
	}


	@Override
	public void debug(String message) {
		logger.debug(message);
		
	}


	@Override
	public void warn(String message) {
		logger.warn(message);
		
	}

	//sl4j does not support fatal level

/*	@Override
	public void fatal(String message) {
		logger.fatal(message);
		
	}*/

}
