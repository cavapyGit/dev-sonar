package com.pradera.commons.logging;



import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
//import org.apache.log4j.Logger;
//used sl4j to improve logging performance.
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerProducer {
	@Produces
	public PraderaLogger getLogger(InjectionPoint ip) {
		Class<?> aClass = ip.getMember().getDeclaringClass();
        Logger logger = LoggerFactory.getLogger(aClass.getName());
        return new SimpleLogger(logger);
	}
}
