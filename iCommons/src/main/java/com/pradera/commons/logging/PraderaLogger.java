package com.pradera.commons.logging;

//import org.apache.log4j.Logger;
//used sl4j to improve logging performance.
import org.slf4j.Logger;

public interface PraderaLogger {
	
	public void debug(String string);
	
	public void info(String string);
	
	public void warn(String string);
	//sl4j does not support fatal level
	//public void fatal(String string);
	
	public void error(String string);

    public Logger getLogger();
}
