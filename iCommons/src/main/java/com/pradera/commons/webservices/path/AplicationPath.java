package com.pradera.commons.webservices.path;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class AplicationPath.
 * is useful to will be use in many RestFul WebServices.
 *
 * @author PraderaTechnologies(mmacalupu).
 * @version 1.0 , 10/12/2012
 */
@ApplicationPath("resources")
public class AplicationPath extends Application {

	/**
	 * Instantiates a new aplication path.
	 */
	public AplicationPath() {
		// TODO Auto-generated constructor stub
	}
}
