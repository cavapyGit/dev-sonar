package com.pradera.commons.configuration.type;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum StageApplication.
 * The diferents stages of Idepositary
 * @author PraderaTechnologies.
 * @version 1.0 , 30/04/2013
 */
public enum StageApplication {
	
	/** The Development. */
	Development,
	
	/** The DevelopmentTest. */
	developmenttest,
	
	/** The System test. */
	SystemTest,
	
	/** The Production. */
	Production,
	
	/** The Migration. */
	Migration;
}
