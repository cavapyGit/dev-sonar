package com.pradera.commons.configuration;

import java.io.Serializable;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.type.StageApplication;

@ApplicationScoped
public class ConfiguratorStage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7921641387692845375L;

	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;
	
	@Produces
	public StageApplication getStage() {
		//Default is SystemTest
		return StageApplication.valueOf(applicationConfiguration.getProperty("app.stage.idepositary.key", "SystemTest"));
	}

}
