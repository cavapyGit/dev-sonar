package com.pradera.commons.configuration;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.type.StageApplication;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ControllerResource.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/02/2013
 */
@ApplicationScoped
public class ConfiguratorParametersApplication {
	
	/** The application configuration. */
	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;

	/**
	 * Gets the faces context.
	 * produce FacesContext 
	 * @return the faces context
	 */
	@Produces @ContexJSF
	public FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}
	
	/**
	 * Gets the static resources web.
	 * config static resources web
	 * @return the static resources web
	 */
	@Produces
	public Set<String> getStaticResourcesWeb() {
		HashSet<String> constantesResources = new HashSet<String>();
		String listStaticResources = applicationConfiguration.getProperty("app.static.resource.web.key");
		if (StringUtils.isNotBlank(listStaticResources)) {
			String[] resourceWeb = listStaticResources.split(",");
			if (resourceWeb != null && resourceWeb.length > 0) {
				for(String resource : resourceWeb) {
					constantesResources.add(resource);
				}
			}
		}
		return constantesResources;
	} 
	
	/**
	 * Gets the user admin.
	 * 
	 * @return the user admin
	 */
	@Produces @UserAdmin
	public String getUserAdmin() {
		return applicationConfiguration.getProperty("app.user.admin.key", "admin");
	}
	
	/**
	 * Gets the url local vpn.
	 *
	 * @return the url local vpn
	 */
	@Produces @IpLocalDevelopment
	public String getUrlLocalVpn() {
		return applicationConfiguration.getProperty("app.ip.vpn.local.key");
	}
	
	/**
	 * Gets the parameter stage.
	 *
	 * @param point the point
	 * @param stage the stage
	 * @return the parameter stage
	 */
	@Produces @StageDependent
	public String getParameterStage(InjectionPoint point, StageApplication stage) {
		String fieldName = getNameField(point);
		fieldName = stage.name() + "." + fieldName;
		return getValueForKey(fieldName);
	}
	
	@Produces @Configurable
	public String getParameterString(InjectionPoint point){
		return getConfigurableNameField(point);
	}
	
	@Produces @Configurable
	public Integer getParameterInteger(InjectionPoint point) {
		String value = getConfigurableNameField(point);
		return (value != null) ? Integer.valueOf(value) : null;
	}
	
	@Produces @Configurable
	public Long getParameterLong(InjectionPoint point) {
		String value = getConfigurableNameField(point);
		return (value != null) ? Long.valueOf(value) : null;
	}
	
	@Produces @Configurable
	public boolean getParameterBoolean(InjectionPoint point){
		String value = getConfigurableNameField(point);
		return (value != null) ? Boolean.parseBoolean(value) : false;
	}
	
	private String getConfigurableNameField(InjectionPoint point) {
		AnnotatedField<Configurable> field = (AnnotatedField)point.getAnnotated();
		Configurable configurable = field.getAnnotation(Configurable.class);
		String value = null;
		if (configurable != null) {
			if (StringUtils.isNotBlank(configurable.value())) {
				value = getValueForKey(configurable.value());
			} else {
				value = getValueForKey(getNameField(point));
			}
		}
		return value;
	}
	
	/**
	 * Gets the name field.
	 *
	 * @param point the point
	 * @return the name field
	 */
	private String getNameField(InjectionPoint point) {
		return point.getMember().getName();
	}
	
	/**
	 * Gets the value for key.
	 *
	 * @param fieldName the field name
	 * @return the value for key
	 */
	private String getValueForKey(String fieldName) {
		return applicationConfiguration.getProperty(fieldName);
	}
	
}
