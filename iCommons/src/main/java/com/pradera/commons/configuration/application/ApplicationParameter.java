package com.pradera.commons.configuration.application;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.integration.common.validation.Validations;

@Named
@ApplicationScoped
public class ApplicationParameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7235746428581873534L;
	
	public static final String ENABLE_TOTP = "app.security.totp.enabled";
	
	/** The stage application. */
	@Inject
	StageApplication stageApplication;
	
	@Inject
	@Configurable("app.version.idepositary.key")
	private String versionApplication;

	/** The date application. */
	@Inject
	@Configurable("app.date.idepositary.key")
	private String dateApplication;

	/** The url pradera. */
	@Inject
	@Configurable("app.url.pradera.key")
	private String urlPradera;
	
	/** The url monitoring report. */
	@Inject
	@Configurable
	private String urlMonitoringReports;
	
	/** The url user manual files. */
	@Inject
	@Configurable
	private String urlUserManualFiles;
	
	/** The server path. */
	@Inject
	@StageDependent
	private String serverPath;
	
	/** The security path. */
	@Inject @StageDependent
	private String securityPath;
	
	/** The session idle time. */
	@Inject @Configurable
	private Integer sessionIdleTime;
	
	/** The enabled disclaimer. */
	@Inject
	@Configurable("app.security.disclaimer.enabled")
	private Integer enabledDisclaimer;
	
	/** The total last url menu. */
	@Inject
	@Configurable("app.security.number.last.url.visited")
	private Integer totalLastUrlMenu;
	
	@Inject
	UserInfo userInfo;
	
	/** The application configuration. */
	@Inject
	@InjectableResource(location = "ParametersEmail.properties")
	private Properties emailConfiguration;

	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	private Properties applicationConfiguration;
	
	private String stageApplicationDescription;

	public ApplicationParameter() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void init(){
		if (stageApplication != null){
			if(stageApplication.equals(StageApplication.Production)){
				stageApplicationDescription = PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.LBL_ENVIRONMENT_PRODUCTION);
			}else if(stageApplication.equals(StageApplication.SystemTest)){
				stageApplicationDescription = PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.LBL_ENVIRONMENT_SYSTEMTEST);
			}else if(stageApplication.equals(StageApplication.Migration)){
				stageApplicationDescription = PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.LBL_ENVIRONMENT_MIGRATION);
			}else{
				stageApplicationDescription = PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.LBL_ENVIRONMENT_DEVELOPMENT);
			}
		}
	}
	
	/**
	 * Gets the urlmonitoring reports.
	 * 
	 * @return the urlmonitoring reports
	 */
	public String getUrlmonitoringReports() throws Exception {
		StringBuilder urlBuilder = new StringBuilder(100);
		urlBuilder.append(serverPath + urlMonitoringReports).append("?")
		.append("tikectId="+ AESEncryptUtils.encrypt(AESKeyType.PRADERAKEY.getValue(), userInfo
									.getUserAccountSession().getTicketSession()))
		.append("&" + GeneralConstants.RESET_BEAN_MENU)
		.append("=").append(GeneralConstants.RESET_BEAN_MENU);
		return urlBuilder.toString();
	}
	
	/**
	 * Gets the url back button.
	 *
	 * @return the url back button
	 */
	public String getUrlBackButton(){
		return serverPath + securityPath + "/error/backButtonError.xhtml";
	}
	
	/**
	 * Gets the disclaimer messages.
	 *
	 * @return the disclaimer messages
	 */
	public String getDisclaimerMessages(){
		return emailConfiguration.getProperty("alert.disclaimer");
	}
	
	public Integer getEnableTotp() {
		if(Validations.validateIsNotNull(stageApplication)) {
			return Integer.valueOf(
					this.applicationConfiguration.getProperty(
							stageApplication+"."+ENABLE_TOTP, this.applicationConfiguration.getProperty("app.security.totp.enabled")));
		} else {
			return GeneralConstants.ZERO_VALUE_INTEGER;
		}
		 
	}
	
	/**
	 * Gets the session idle time.
	 *
	 * @return the session idle time
	 */
	public Integer getSessionIdleTime() {
		return sessionIdleTime;
	}
	
	/**
	 * Gets the stage application value.
	 *
	 * @return the stage application value
	 */
	public String getStageApplicationValue() {
		return stageApplicationDescription;
	}

	/**
	 * Gets the version application.
	 * 
	 * @return the version application
	 */
	public String getVersionApplication() {
		return versionApplication;
	}

	/**
	 * Gets the date application.
	 * 
	 * @return the date application
	 */
	public String getDateApplication() {
		return dateApplication;
	}

	/**
	 * Gets the url pradera.
	 * 
	 * @return the url pradera
	 */
	public String getUrlPradera() {
		return urlPradera;
	}
	
	public String getUrlUserManualFiles() {
		return urlUserManualFiles;
	}

	public Integer getEnabledDisclaimer() {
		return enabledDisclaimer;
	}

	public Integer getTotalLastUrlMenu() {
		return totalLastUrlMenu;
	}

}
