package com.pradera.commons.configuration;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Qualifier Configurable.
 * is using to get setup parameters of applications
 * if value is different of empty parameter name is get from value
 * if not parameter is get from name attribute injection point
 * @author PraderaTechnologies.
 * @version 1.0 , 06/06/2013
 */
@Qualifier
@Target({ TYPE, METHOD, PARAMETER, FIELD })
@Retention(RUNTIME)
@Documented
public @interface Configurable {
	
	/**
	 * Value.
	 *
	 * @return the string
	 */
	@Nonbinding
	String value() default "";
}
