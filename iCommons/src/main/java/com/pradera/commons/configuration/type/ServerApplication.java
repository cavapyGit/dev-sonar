package com.pradera.commons.configuration.type;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Enum ServerApplication.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/06/2014
 */
public enum ServerApplication {
	
	/** The jboss. */
	JBOSS,
	
	/** The weblogic. */
	WEBLOGIC,
	
	/** The glassfish. */
	GLASSFISH;
}
