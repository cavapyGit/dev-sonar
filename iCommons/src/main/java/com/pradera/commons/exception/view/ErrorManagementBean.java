package com.pradera.commons.exception.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.cdi.Param;

import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.integration.common.validation.Validations;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ErrorManagementBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07-may-2015
 */
@Named
@RequestScoped
public class ErrorManagementBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11735567120061290L;
	
	/** The security path. */
	@Inject @StageDependent
	private String securityPath;
	
	/** The url login. */
	private String urlLogin ="/login.xhtml";
	
	private String urlError ="error.xhtml";
	
	@Inject @Param
	private String idleTime_source;
	
	@Inject @Param
	private String url_source;
	
	@Inject @Param
	private String module_source;
	
	@Inject
	private StageApplication stageApplication;
	
	private StringBuilder urlParam;
	
	private Boolean isUatOrProd = Boolean.FALSE;
	
	/**
	 * Clean session.
	 */
	public void cleanSession(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if(facesContext != null){
			HttpSession session =(HttpSession) facesContext.getExternalContext().getSession(false);
			if(session != null){
				session.invalidate();
			}
		}
	}

	/**
	 * Instantiates a new error management bean.
	 */
	public ErrorManagementBean() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void init(){
		this.isUatOrProd = checkUatOrProd();
		
		urlParam= new StringBuilder("");
		if(StringUtils.isNotBlank(idleTime_source) || StringUtils.isNotBlank(url_source) || StringUtils.isNotBlank(module_source)){
			urlParam.append("?");
			if(stageApplication != null && stageApplication.equals(StageApplication.Development)){
				urlParam.append("url_source="+url_source);
				urlParam.append("&module_source="+module_source);
				urlParam.append("&idleTime_source=/idleTime");
			} else {
				urlParam.append("idleTime_source=/idleTime");
			}
		}
	}
	
	public Boolean checkUatOrProd() {
		if(Validations.validateIsNotNull(stageApplication) 
				&& (stageApplication.equals(StageApplication.Production) || stageApplication.equals(StageApplication.SystemTest))) {
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}
	
	/**
	 * Gets the url login.
	 *
	 * @return the url login
	 */
	public String getUrlLogin(){
		return securityPath+urlLogin+urlParam.toString();
	}

	public String getUrlError() {
		return urlError;
	}

	public Boolean getIsUatOrProd() {
		return isUatOrProd;
	}

	public void setIsUatOrProd(Boolean isUatOrProd) {
		this.isUatOrProd = isUatOrProd;
	}

}
