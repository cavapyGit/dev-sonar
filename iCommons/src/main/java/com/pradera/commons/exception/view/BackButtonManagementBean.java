package com.pradera.commons.exception.view;

import java.io.Serializable;

import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BackButtonManagementBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/01/2014
 */
@Model
public class BackButtonManagementBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8126691664579755753L;
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/**
	 * Clean session.
	 */
	public void cleanSession(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if(facesContext != null){
			HttpSession session =(HttpSession) facesContext.getExternalContext().getSession(false);
			UserInfo userCloseSession = new UserInfo();
			userCloseSession.setUserAccountSession(new UserAccountSession());
			userCloseSession.getUserAccountSession().setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			userCloseSession.getUserAccountSession().setUserName(userInfo.getUserAccountSession().getUserName());
			userCloseSession.setLogoutMotive(LogoutMotiveType.KILLSESSION);
			clientRestService.registerCloseSessionUser(userCloseSession);
			if(session != null){
				session.invalidate();
			}
			
		}
	}

	/**
	 * Instantiates a new back button management bean.
	 */
	public BackButtonManagementBean() {
		// TODO Auto-generated constructor stub
	}

}
