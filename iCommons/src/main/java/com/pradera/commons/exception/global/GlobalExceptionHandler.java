package com.pradera.commons.exception.global;

import java.io.IOException;
import java.io.PrintWriter;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
//import org.jboss.solder.exception.control.CaughtException;
//import org.jboss.solder.exception.control.Handles;
//import org.jboss.solder.exception.control.HandlesExceptions;
//import org.jboss.solder.exception.control.Precedence;
//import org.jboss.solder.exception.control.TraversalMode;
import org.apache.deltaspike.core.api.exception.control.ExceptionHandler;
import org.apache.deltaspike.core.api.exception.control.Handles;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.ContexJSF;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.JSFUtilities;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GlobalExceptionHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/05/2013
 */
@ExceptionHandler
public class GlobalExceptionHandler {

//	/** The faces context. */
	@Inject @ContexJSF
	FacesContext facesContext;
	
//	@Inject
//	FacesContext facesContext;

	
	/** The server path. */
	@Inject @StageDependent
	private String serverPath;
	
	/** The security path. */
	@Inject @StageDependent
	private String securityPath;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	@Inject
	ClientRestService clientRestService;
	
	@Inject
	UserInfo userInfo;
	
	/**
	 * Instantiates a new global exception handler.
	 */
	public GlobalExceptionHandler() {
		//verificar
		
	}
	
	/**
	 * Log exception.
	 *
	 * @param excepcion the excepcion
	 * during= TraversalMode.BREADTH_FIRST,
	 */
	public void logException(@Handles(ordinal = 100) ExceptionEvent<Throwable> excepcion) {
		if(StringUtils.isNotBlank(excepcion.getException().getMessage())) {
			log.error(" Error registrado : " + excepcion.getException());
		} else if(excepcion.getException().getCause() != null){
			log.error(" Error NPE : " + excepcion.getException().getCause().toString());
		} else {
			log.error("Error registrado sin throwable :" +excepcion.getException().toString());
		}
//		excepcion.getException().printStackTrace();
		for(StackTraceElement stack : excepcion.getException().getStackTrace()) {
			if (stack.getClassName().contains("Facade") || stack.getClassName().contains("Bean")) {
				if(stack.getLineNumber() != -1){
					log.error(" Clase " + stack.getClassName()+"\n"+
							" Metodo " + stack.getMethodName()+"\n"+
							" Linea " + stack.getLineNumber()+"\n");
				}
				//log.error(" Metodo " + stack.getMethodName());
				//log.error(" Linea " + stack.getLineNumber());
			}
		}
		log.error(" Error registrado : " + excepcion.getException());
		log.error(ExceptionUtils.getStackTrace(excepcion.getException()));
	}
	
	/**
	 * Redirect error page.
	 *
	 * @param excepcion the excepcion
	 * @param facesContext the faces context
	 */
	public void redirectErrorPage(@Handles ExceptionEvent<Exception> excepcion) { //, FacesContext facesContext
		try {
			if (facesContext != null) {
				//only redirect for error catch in JFS layer
				log.info(" redireccionando a la pagina de error ");
				log.info(excepcion.toString());
				ExternalContext externalContext = facesContext.getExternalContext();
				HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
				HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
				//close session of DB
				closeSession(userInfo,request);
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				StringBuilder sbJScript = new StringBuilder(""); 
				if (facesContext.getPartialViewContext().isAjaxRequest()) {
					sbJScript.append(serverPath);
					sbJScript.append(securityPath);
					sbJScript.append("/error/redirectError.xhtml");
					sbJScript.append("?page=error.xhtml");
					out.write(JSFUtilities.getXmlAjaxRedirect(sbJScript.toString()));
					facesContext.responseComplete();
				} else {
					//full request
				   sbJScript.append("window.top.location.href='");
				   sbJScript.append(serverPath);
				   sbJScript.append(securityPath);
				   sbJScript.append("/error/error.xhtml'");
				   out.write("<html>");
				   out.write("<script>");
				   out.write(sbJScript.toString());
				   out.write("</script>");
				   out.write("</html>");
				   facesContext.responseComplete();
				}
			}
		} catch (IOException ioex) {
			log.error(ioex.toString());
		}
	}
	
	/**
	 * Close session.
	 *
	 * @param userInfo the user info
	 */
	private void closeSession(UserInfo userInfo,HttpServletRequest request) {
		log.debug("closeSession");
		if(userInfo != null && userInfo.getUserAccountSession().getIdUserSessionPk()!=null){
			UserInfo userCloseSession = new UserInfo();
			userCloseSession.setUserAccountSession(new UserAccountSession());
			userCloseSession.getUserAccountSession().setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
			userCloseSession.getUserAccountSession().setUserName(userInfo.getUserAccountSession().getUserName());
			userCloseSession.setLogoutMotive(LogoutMotiveType.KILLSESSION);
			clientRestService.registerCloseSessionUser(userCloseSession);
			request.getSession(false).invalidate();
		}
	}

}
