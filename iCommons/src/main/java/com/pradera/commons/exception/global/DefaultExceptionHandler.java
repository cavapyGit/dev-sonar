package com.pradera.commons.exception.global;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Properties;

import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.omnifaces.util.Faces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class DefaultExceptionHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/06/2013
 */
public class DefaultExceptionHandler extends ExceptionHandlerWrapper {

	/** The log. */
	private Logger log = LoggerFactory.getLogger(DefaultExceptionHandler.class);
	
	/** key for session scoped message detail. */
	public static final String MESSAGE_DETAIL_KEY = "ip.client.jsftoolkit.messageDetail";

	/** The wrapped. */
	private ExceptionHandler wrapped;

	/** The server path. */
	@Inject
	@StageDependent
	private String serverPath;

	/** The security path. */
	@Inject
	@StageDependent
	private String securityPath;
	

	/**
	 * Instantiates a new default exception handler.
	 *
	 * @param wrapped the wrapped
	 */
	public DefaultExceptionHandler(ExceptionHandler wrapped) {
		this.wrapped = wrapped;
	}

	/* (non-Javadoc)
	 * @see javax.faces.context.ExceptionHandlerWrapper#getWrapped()
	 */
	@Override
	public ExceptionHandler getWrapped() {
		return this.wrapped;
	}

	/* (non-Javadoc)
	 * @see javax.faces.context.ExceptionHandlerWrapper#handle()
	 */
	@Override
	public void handle() throws FacesException {
		//TODO work around for now , because handler not support inject CDI
		String stageDepositary;
		try(InputStream is = DefaultExceptionHandler.class.getResourceAsStream("/ApplicationConfiguration.properties")) {
			Properties fileConfig=new Properties();
			fileConfig.load(is);
			stageDepositary = fileConfig.getProperty("app.stage.idepositary.key");
			serverPath = fileConfig.getProperty(stageDepositary+".serverPath");
			securityPath = fileConfig.getProperty(stageDepositary+".securityPath");
			fileConfig.clear();
		} catch(IOException iox) {
			log.error(iox.getMessage());
		}
		for (Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator(); i.hasNext();) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
			Throwable throwable = context.getException();
//			context.getException().printStackTrace();
			log.error(ExceptionUtils.getStackTrace(throwable));
			FacesContext facesContext = FacesContext.getCurrentInstance();
			//call methods clean session and register in DB
			closeSession(facesContext);
			try {
				if (throwable instanceof ViewExpiredException) {
					ViewExpiredException vee = (ViewExpiredException) throwable;
					log.error("Error expired session in page " + vee.getViewId());
					redirectError(facesContext, "redirectSessionError.xhtml");
				} else {
					// TODO if in future is necesary other way to handler add
					// else if XxxxxException
					// to example ContextNotActiveException
					log.error("exception",throwable);
					if(context.getComponent() != null){
						log.error(context.getComponent().getFamily() +" :"+context.getComponent().getClientId());
					}
					redirectError(facesContext, "redirectError.xhtml");
				}
			} finally {
				i.remove();
			}

		}
		// At this point, the queue will not contain any ViewExpiredEvents.
		// Therefore, let the parent handle them.
		getWrapped().handle();

	}

	/**
	 * Redirect error page.
	 *
	 * @param facesContext the faces context
	 * @param errorPage the error page
	 */
	private void redirectError(FacesContext facesContext, String errorPage){
		StringBuilder sbJScript = new StringBuilder("");
		if (facesContext.getPartialViewContext().isAjaxRequest()) {
			try {
			ExternalContext externalContext = facesContext.getExternalContext();
			HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
			PrintWriter out = response.getWriter();
			sbJScript.append(serverPath);
			sbJScript.append(securityPath);
			sbJScript.append("/error/");
			sbJScript.append(errorPage);
			sbJScript.append("?page=");
			sbJScript.append("error.xhtml");
			out.write(JSFUtilities.getXmlAjaxRedirect(sbJScript.toString()));
			facesContext.responseComplete();
			} catch (IOException ioex) {
				log.error(ioex.getMessage());
			}
		} else {
			try {
			ExternalContext externalContext = facesContext.getExternalContext();
			HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			sbJScript.append("window.top.location.href='");
		    sbJScript.append(serverPath);
		    sbJScript.append(securityPath);
		    sbJScript.append("/error/");
		    sbJScript.append("error.xhtml");
			sbJScript.append("'");
			out.write("<html>");
			out.write("<script>");
			out.write(sbJScript.toString());
			out.write("</script>");
			out.write("</html>");
			facesContext.responseComplete();
			} catch (IOException ioex) {
				log.error(ioex.getMessage());
			}
		}
	}

	/**
	 * Close session.
	 */
	private void closeSession(FacesContext facesContext) {
		Long userSessionPk = (Long)JSFUtilities.getSessionMap(GeneralConstants.USER_INFO_ID);
		if(userSessionPk != null){
			UserInfo userInfo = new UserInfo();
			userInfo.setUserAccountSession(new UserAccountSession());
			userInfo.getUserAccountSession().setIdUserSessionPk(userSessionPk);
			userInfo.setLogoutMotive(LogoutMotiveType.KILLSESSION);
			//Get userInfo from cdi scope
			UserInfo userInfoCDI = Faces.evaluateExpressionGet("#{userInfo}");
			if(userInfoCDI != null){
				userInfo.getUserAccountSession().setUserName(userInfoCDI.getUserAccountSession().getUserName());
			}
			try {
	        	StringBuilder urlResource = new StringBuilder();
	    		urlResource.append(serverPath).append(securityPath).append("/resources/users/closeSession");	
	             Client client = Client.create();
	             String input = new Gson().toJson(userInfo);
	             WebResource webResource = client.resource(urlResource.toString());
	             ClientResponse response = webResource.type("application/json")
	                        .post(ClientResponse.class, input);
	             if (response.getStatus() != 200) {
	                     log.error("error register audit process "+response.getEntity(String.class));

	             } 
	        	} catch (Exception e) {
	            	log.error(e.getMessage());
	            	e.printStackTrace();
	            }
			if(facesContext != null){
				JSFUtilities.killSession();
			}
		}
	}

	/**
	 * Handle unexpected.
	 *
	 * @param facesContext the faces context
	 * @param t the t
	 * @return the string
	 */
	protected String handleUnexpected(FacesContext facesContext,
			final Throwable t) {
		log.error("An unexpected internal error has occurred");
		return "jsftoolkit.exception.UncheckedException";
	}
}
