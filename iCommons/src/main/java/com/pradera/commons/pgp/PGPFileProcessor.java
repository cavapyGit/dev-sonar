
package com.pradera.commons.pgp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.integration.exception.CustomException;
import com.diesgut.simplepgp.core.IMessageEncryptor;
import com.diesgut.simplepgp.crypt.PGPFactory;

@Named
@Dependent
public class PGPFileProcessor {
	
	@Inject
	private transient PraderaLogger log;
	
	IMessageEncryptor messageEncryptor;
	
	private String fileName;

	private String passphrase;

	private String keyFile;

	private File inputFile;
	private byte[] inputByteArray;

	private String outputFilePath;
	private File outputFile;
	private byte[] outputByteArray;

	// is output ASCII or binary
	private boolean asciiArmored = true; //false

    // should integrity check information be added
    // set to true for compatibility with GnuPG 2.2.8+
	private boolean integrityCheck = false; //true
	
	public PGPFileProcessor() {
		messageEncryptor = PGPFactory.getEncyptor();
	}

	
	public boolean encrypt() throws Exception {
        FileInputStream keyIn = new FileInputStream(keyFile);
        FileInputStream in = new FileInputStream(inputFile);
		FileOutputStream out = new FileOutputStream(outputFile);

		messageEncryptor.encrypt( keyIn, this.fileName, in, out);

		in.close();
		out.close();
		keyIn.close();
		return true;
	}
	
	public boolean encryptToByteArray() throws Exception {
        FileInputStream keyIn = new FileInputStream(keyFile);
        log.info("Original file size "+this.inputByteArray.length  );
		ByteArrayOutputStream plainResult = new ByteArrayOutputStream();
		messageEncryptor.encrypt( keyIn, this.fileName, new ByteArrayInputStream(this.inputByteArray), plainResult);
		this.outputByteArray=plainResult.toByteArray();
		log.info("Encrypted file size "+this.outputByteArray.length  );

		keyIn.close();
		return true;
	}

	
//	public boolean encrypt2() throws Exception {
//        File publicCertFile = new File(keyFile);
//        X509Certificate cert = KeyChainFactory.getPublicCertificate(publicCertFile.getAbsolutePath());
//        
//        String filename = inputFile.getAbsolutePath();
//        byte[] data = Encryptor.encryptData(filename, cert);
//        
//        ByteOperator.writeBytesFile(outputFile.getAbsolutePath(), data);
//		return true;
//	} 


	public boolean decrypt() throws CustomException, Exception {
		try {
		
	        FileInputStream in = new FileInputStream(inputFile);
	        FileInputStream keyIn = new FileInputStream(keyFile);
	        FileOutputStream out = new FileOutputStream(outputFile);
	        
	        messageEncryptor.decrypt(passphrase, keyIn, in, out);
	        in.close();
	        out.close();
	        keyIn.close();
        
		} catch (Exception | NoSuchMethodError e) {
			e.printStackTrace();
			String error=PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.PGP_ERROR_DECRYPT);
			throw new CustomException(error);
		}
		 return true;
		
	}
	
	public boolean decryptToByteArray() throws CustomException, Exception {
		try {
			FileInputStream keyIn = new FileInputStream(keyFile);
			
			ByteArrayOutputStream plainResult = new ByteArrayOutputStream();
			messageEncryptor.decrypt(passphrase, keyIn, new ByteArrayInputStream(this.inputByteArray), plainResult);
			this.outputByteArray=plainResult.toByteArray();
			keyIn.close();
			
		} catch (Exception | NoSuchMethodError e) {
			e.printStackTrace();
			String error=PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.PGP_ERROR_DECRYPT);
			throw new CustomException(error);
		}
		return true;
		
	}

	public boolean isAsciiArmored() {
		return asciiArmored;
	}

	public void setAsciiArmored(boolean asciiArmored) {
		this.asciiArmored = asciiArmored;
	}

	public boolean isIntegrityCheck() {
		return integrityCheck;
	}

	public void setIntegrityCheck(boolean integrityCheck) {
		this.integrityCheck = integrityCheck;
	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	public String getKeyFile() {
		return keyFile;
	}

	public void setKeyFile(String keyFile) {
		this.keyFile = keyFile;
	}

	public File getInputFile() {
		return inputFile;
	}

	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	public File getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(File outputFile) {
		this.outputFile = outputFile;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public byte[] getInputByteArray() {
		return inputByteArray;
	}


	public void setInputByteArray(byte[] inputByteArray) {
		this.inputByteArray = inputByteArray;
	}


	public byte[] getOutputByteArray() {
		return outputByteArray;
	}


	public void setOutputByteArray(byte[] outputByteArray) {
		this.outputByteArray = outputByteArray;
	}
	
	

}
