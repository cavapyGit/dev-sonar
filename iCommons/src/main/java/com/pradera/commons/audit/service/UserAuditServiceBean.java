package com.pradera.commons.audit.service;

import java.io.Serializable;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.pradera.commons.audit.UserAuditTrackingTO;
import com.pradera.commons.audit.UserTrackProcess;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserInfo;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class UserAuditServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/03/2014
 */
@Stateless
public class UserAuditServiceBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5113461077892076479L;
	
	/** The client rest service. */
	@Inject
	ClientRestService clientRestService;
	
	/**
	 * Register user track process.
	 *
	 * @param userTrackProcess the user track process
	 */
	@Asynchronous
	public void registerUserTrackProcess(UserTrackProcess userTrackProcess){
		clientRestService.registerUserTrackProcess(userTrackProcess);
	}
	
	/**
	 * Close session user.
	 *
	 * @param userInfo the user info
	 */
	@Asynchronous
	public void closeSessionUser(UserInfo userInfo) {
		clientRestService.registerCloseSessionUser(userInfo);
	}
	
	/**
	 * Close all sessions.
	 */
	@Asynchronous
	public void closeAllSessions(){
		clientRestService.registerCloseAllSessions();
	}
	
	/**
	 * Register user audit tracking.
	 *
	 * @param userAuditTracking the user audit tracking
	 */
	@Asynchronous
	public void registerUserAuditTracking(UserAuditTrackingTO userAuditTracking){
		clientRestService.registerUserAuditTracking(userAuditTracking);
	}
	

	/**
	 * Instantiates a new user audit service bean.
	 */
	public UserAuditServiceBean() {
		// TODO Auto-generated constructor stub
	}

}
