package com.pradera.commons.audit;

import java.io.Serializable;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class UserAuditTrackingTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/03/2014
 */
public class UserAuditTrackingTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5731497471253569266L;
	
	/** The url. */
	private String url;
	
	/** The exception message. */
	private String exceptionMessage;
	
	/** The query string. */
	private String queryString;
	
	/** The user session. */
	private Long userSession;

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the exception message.
	 *
	 * @return the exception message
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	/**
	 * Sets the exception message.
	 *
	 * @param exceptionMessage the new exception message
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryString() {
		return queryString;
	}

	/**
	 * Sets the query string.
	 *
	 * @param queryString the new query string
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * Gets the user session.
	 *
	 * @return the user session
	 */
	public Long getUserSession() {
		return userSession;
	}

	/**
	 * Sets the user session.
	 *
	 * @param userSession the new user session
	 */
	public void setUserSession(Long userSession) {
		this.userSession = userSession;
	}

}
