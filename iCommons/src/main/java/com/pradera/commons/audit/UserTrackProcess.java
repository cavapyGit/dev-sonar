package com.pradera.commons.audit;

import java.io.Serializable;
import java.util.Map;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserTrackProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/11/2013
 */
public class UserTrackProcess implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8631195146813175077L;
	
	/** The id user session pk. */
	private Long idUserSessionPk;
	
	/** The id privilege system. */
	private Integer idPrivilegeSystem;
	
	/** The id business process. */
	private Long idBusinessProcess;
	
	/** The business method. */
	private String businessMethod;
	
	/** The class name. */
	private String className;
	
	/** The id process logger. */
	private Long idProcessLogger;
	
	/** The id process report. */
	private Long idProcessReport;
	
	/** The ind error. */
	private Integer indError;
	
	/** The parameters. */
	private Map<String,String> parameters;

	/**
	 * Instantiates a new user track process.
	 */
	public UserTrackProcess() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the id user session pk.
	 *
	 * @return the id user session pk
	 */
	public Long getIdUserSessionPk() {
		return idUserSessionPk;
	}

	/**
	 * Sets the id user session pk.
	 *
	 * @param idUserSessionPk the new id user session pk
	 */
	public void setIdUserSessionPk(Long idUserSessionPk) {
		this.idUserSessionPk = idUserSessionPk;
	}

	/**
	 * Gets the id privilege system.
	 *
	 * @return the id privilege system
	 */
	public Integer getIdPrivilegeSystem() {
		return idPrivilegeSystem;
	}

	/**
	 * Sets the id privilege system.
	 *
	 * @param idPrivilegeSystem the new id privilege system
	 */
	public void setIdPrivilegeSystem(Integer idPrivilegeSystem) {
		this.idPrivilegeSystem = idPrivilegeSystem;
	}

	/**
	 * Gets the id business process.
	 *
	 * @return the id business process
	 */
	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}

	/**
	 * Sets the id business process.
	 *
	 * @param idBusinessProcess the new id business process
	 */
	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}

	/**
	 * Gets the business method.
	 *
	 * @return the business method
	 */
	public String getBusinessMethod() {
		return businessMethod;
	}

	/**
	 * Sets the business method.
	 *
	 * @param businessMethod the new business method
	 */
	public void setBusinessMethod(String businessMethod) {
		this.businessMethod = businessMethod;
	}

	/**
	 * Gets the id process logger.
	 *
	 * @return the id process logger
	 */
	public Long getIdProcessLogger() {
		return idProcessLogger;
	}

	/**
	 * Sets the id process logger.
	 *
	 * @param idProcessLogger the new id process logger
	 */
	public void setIdProcessLogger(Long idProcessLogger) {
		this.idProcessLogger = idProcessLogger;
	}

	/**
	 * Gets the id process report.
	 *
	 * @return the id process report
	 */
	public Long getIdProcessReport() {
		return idProcessReport;
	}

	/**
	 * Sets the id process report.
	 *
	 * @param idProcessReport the new id process report
	 */
	public void setIdProcessReport(Long idProcessReport) {
		this.idProcessReport = idProcessReport;
	}

	/**
	 * Gets the ind error.
	 *
	 * @return the ind error
	 */
	public Integer getIndError() {
		return indError;
	}

	/**
	 * Sets the ind error.
	 *
	 * @param indError the new ind error
	 */
	public void setIndError(Integer indError) {
		this.indError = indError;
	}

	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Sets the class name.
	 *
	 * @param className the new class name
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public Map<String,String> getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters.
	 *
	 * @param parameters the parameters
	 */
	public void setParameters(Map<String,String> parameters) {
		this.parameters = parameters;
	}

}
