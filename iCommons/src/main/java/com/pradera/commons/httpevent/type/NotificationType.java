package com.pradera.commons.httpevent.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum NotificationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/05/2013
 */
public enum NotificationType {
	
	/** The process. */
	PROCESS(Integer.valueOf(247)),
	
	/** The message. */
	MESSAGE(Integer.valueOf(1440)),
	
	/** The email. */
	EMAIL(Integer.valueOf(1445)),
	
	SMS(Integer.valueOf(666)),
	
	/** The killsession. */
	KILLSESSION(Integer.valueOf(1446)),
	
	
	EMAIL_ADJUNT(Integer.valueOf(2393)),
	
	EMAIL_ADJUNT_CONTENT_HTML(Integer.valueOf(2397)),
	
	SMS_MESSAGE_TO_CELL_PHONE(Integer.valueOf(2397)),
	
	
	/** The report. */
	REPORT(Integer.valueOf(1653));
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	private static final Map<Integer, NotificationType> lookup = new HashMap<Integer, NotificationType>();
	
	/** The Constant list. */
	public static final List<NotificationType> list = new ArrayList<NotificationType>();
    static {
        for (NotificationType notify : NotificationType.values()){
            lookup.put(notify.getCode(), notify);
        	list.add(notify);
        }
    }
    
    /**
     * Gets the.
     *
     * @param codigo the codigo
     * @return the notification type
     */
    public static NotificationType get(Integer codigo) {
        return lookup.get(codigo);
    }
	
	/**
	 * Instantiates a new notification type.
	 *
	 * @param code the code
	 */
	private NotificationType(Integer code) {
		this.setCode(code);
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
}
