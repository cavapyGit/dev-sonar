package com.pradera.commons.httpevent.publish;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.notifications.remote.FileAtach;
import com.pradera.commons.notifications.remote.NotificationMessgaeDetail;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class BrowserWindow.
 * using for send event of notification from IDepositary
 * to Security
 * @author PraderaTechnologies.
 * @version 1.0 , 10/05/2013
 */
public class BrowserWindow implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3502332967418504106L;
	
	/** The subject. */
	private String subject;
	
	/** The message. */
	private String message;
	
	/** The notification type. */
	private Integer notificationType;
	
	/** The user channel. */
	private String userChannel;
	
	/** The read. */
	private boolean read;

	/** The priority. */
	private String priority;
	
	/** The id notification. */
	private Long idNotification;
	
	/** The notificationMessgaeDetail. */
	private NotificationMessgaeDetail notificationMessgaeDetail;
	
	private List<FileAtach> filesAtach;
	
	/**
	 * Instantiates a new browser window.
	 */
	public BrowserWindow() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject the new subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	public Integer getNotificationType() {
		return notificationType;
	}

	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the new notification type
	 */
	public void setNotificationType(Integer notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * Checks if is read.
	 *
	 * @return true, if is read
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * Sets the read.
	 *
	 * @param read the new read
	 */
	public void setRead(boolean read) {
		this.read = read;
	}

	/**
	 * Gets the user channel.
	 *
	 * @return the user channel
	 */
	public String getUserChannel() {
		return userChannel;
	}

	/**
	 * Sets the user channel.
	 *
	 * @param userChannel the new user channel
	 */
	public void setUserChannel(String userChannel) {
		this.userChannel = userChannel;
	}

	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * Sets the priority.
	 *
	 * @param priority the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * Gets the notification messgae detail.
	 *
	 * @return the notificationMessgaeDetail
	 */
	public NotificationMessgaeDetail getNotificationMessgaeDetail() {
		return notificationMessgaeDetail;
	}

	/**
	 * Sets the notification messgae detail.
	 *
	 * @param notificationMessgaeDetail the notificationMessgaeDetail to set
	 */
	public void setNotificationMessgaeDetail(
			NotificationMessgaeDetail notificationMessgaeDetail) {
		this.notificationMessgaeDetail = notificationMessgaeDetail;
	}

	/**
	 * Gets the id notification.
	 *
	 * @return the id notification
	 */
	public Long getIdNotification() {
		return idNotification;
	}

	/**
	 * Sets the id notification.
	 *
	 * @param idNotification the new id notification
	 */
	public void setIdNotification(Long idNotification) {
		this.idNotification = idNotification;
	}

	public List<FileAtach> getFilesAtach() {
		return filesAtach;
	}

	public void setFilesAtach(List<FileAtach> filesAtach) {
		this.filesAtach = filesAtach;
	}
	
	

}
