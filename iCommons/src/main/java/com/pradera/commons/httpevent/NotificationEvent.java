package com.pradera.commons.httpevent;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

import com.pradera.commons.httpevent.type.NotificationType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Qualifier NotificationEvent.
 * is using for differentiate notification type events
 * @author PraderaTechnologies.
 * @version 1.0 , 10/05/2013
 */
@Qualifier
@Target({ TYPE, METHOD, PARAMETER, FIELD })
@Retention(RUNTIME)
@Documented
public @interface NotificationEvent {
	
	/**
	 * Value.
	 *
	 * @return the notification type
	 */
	NotificationType value();
}
