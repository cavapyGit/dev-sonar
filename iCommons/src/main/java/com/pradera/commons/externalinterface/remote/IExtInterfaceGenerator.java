/*
 * 
 */
package com.pradera.commons.externalinterface.remote;

import java.io.Serializable;

import javax.ejb.Remote;

import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Interface IExtInterfaceGenerator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/11/2013
 */
@Remote
public interface IExtInterfaceGenerator extends Serializable{

	/**
	 * Execute external interface.
	 *
	 * @param idInterfacePk the id interface pk
	 * @throws ServiceException the service exception
	 */
	public void executeExternalInterface(Long idInterfacePk) throws ServiceException;
	
	/**
	 * Generate interface file.
	 *
	 * @param idInterfacePk the id interface pk
	 * @throws ServiceException the service exception
	 */
	public void generateInterfaceFile(Long idInterfacePk) throws ServiceException;
}