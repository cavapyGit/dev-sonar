package com.pradera.commons.notifications.remote;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MessageFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/10/2013
 */
public class MessageFilter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2205882981047153083L;

	/** The user receptor. */
	private String userReceptor;
	
	/** The process level. */
	private Integer processLevel;
	
	
	
	/** The id business process. */
	private Long idBusinessProcess;
	
	/** The read. */
	private Integer read;
	
	/** The notification type. */
	private Integer notificationType;
	
	/** The user issuer. */
	private String userIssuer;
	

	/** The beginDate. */
	private Date beginDate;
	
	/** The endDate. */
	private Date endDate;
	
	/** The id notification logger. */
	private Long idNotificationLogger;
	
	private List<String> lstUserNames;
	
	private String processName;

	/**
	 * Gets the begin date.
	 *
	 * @return the beginDate
	 */
	public Date getBeginDate() {
		return beginDate;
	}

	/**
	 * Sets the begin date.
	 *
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the user receptor.
	 *
	 * @return the user receptor
	 */
	public String getUserReceptor() {
		return userReceptor;
	}

	/**
	 * Sets the user receptor.
	 *
	 * @param userReceptor the new user receptor
	 */
	public void setUserReceptor(String userReceptor) {
		this.userReceptor = userReceptor;
	}

	/**
	 * Gets the process level.
	 *
	 * @return the process level
	 */
	public Integer getProcessLevel() {
		return processLevel;
	}

	/**
	 * Sets the process level.
	 *
	 * @param processLevel the new process level
	 */
	public void setProcessLevel(Integer processLevel) {
		this.processLevel = processLevel;
	}

	

	/**
	 * Gets the id business process.
	 *
	 * @return the id business process
	 */
	public Long getIdBusinessProcess() {
		return idBusinessProcess;
	}

	/**
	 * Sets the id business process.
	 *
	 * @param idBusinessProcess the new id business process
	 */
	public void setIdBusinessProcess(Long idBusinessProcess) {
		this.idBusinessProcess = idBusinessProcess;
	}

	
	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	public Integer getNotificationType() {
		return notificationType;
	}

	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the new notification type
	 */
	public void setNotificationType(Integer notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * Gets the user issuer.
	 *
	 * @return the user issuer
	 */
	public String getUserIssuer() {
		return userIssuer;
	}

	/**
	 * Sets the user issuer.
	 *
	 * @param userIssuer the new user issuer
	 */
	public void setUserIssuer(String userIssuer) {
		this.userIssuer = userIssuer;
	}

	/**
	 * Gets the id notification logger.
	 *
	 * @return the id notification logger
	 */
	public Long getIdNotificationLogger() {
		return idNotificationLogger;
	}

	/**
	 * Sets the id notification logger.
	 *
	 * @param idNotificationLogger the new id notification logger
	 */
	public void setIdNotificationLogger(Long idNotificationLogger) {
		this.idNotificationLogger = idNotificationLogger;
	}

	public Integer getRead() {
		return read;
	}

	public void setRead(Integer read) {
		this.read = read;
	}

	public List<String> getLstUserNames() {
		return lstUserNames;
	}

	public void setLstUserNames(List<String> lstUserNames) {
		this.lstUserNames = lstUserNames;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}				
	

}

