package com.pradera.commons.notifications.remote;

import java.io.Serializable;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class Message.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/10/2013
 */
public class Message implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 */
	public Message() {
    }
	
	/** The idMessage. */
	private Long idMessage;

	/** The notification type. */
	private Integer notificationType;
	
	/** The str message date. */
	private String strMessageDate;
	
	/** The messageSubject. */
	private String messageSubject;
	
	/** The messageBody. */
	private String messageBody;
	
	/** The read. */
	private boolean read;
	
	/** The messageTo. */
	private String messageReceptor;
	
	/** The message issuer. */
	private String messageIssuer;
	
	/** The registry date. */
	private Date registryDate;

	/**
	 * Gets the id message.
	 *
	 * @return the id message
	 */
	public Long getIdMessage() {
		return idMessage;
	}

	/**
	 * Sets the id message.
	 *
	 * @param idMessage the new id message
	 */
	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}

	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	public Integer getNotificationType() {
		return notificationType;
	}

	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the new notification type
	 */
	public void setNotificationType(Integer notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * Gets the str message date.
	 *
	 * @return the str message date
	 */
	public String getStrMessageDate() {
		return strMessageDate;
	}

	/**
	 * Sets the str message date.
	 *
	 * @param strMessageDate the new str message date
	 */
	public void setStrMessageDate(String strMessageDate) {
		this.strMessageDate = strMessageDate;
	}

	/**
	 * Gets the message subject.
	 *
	 * @return the message subject
	 */
	public String getMessageSubject() {
		return messageSubject;
	}

	/**
	 * Sets the message subject.
	 *
	 * @param messageSubject the new message subject
	 */
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	/**
	 * Gets the message body.
	 *
	 * @return the message body
	 */
	public String getMessageBody() {
		return messageBody;
	}

	/**
	 * Sets the message body.
	 *
	 * @param messageBody the new message body
	 */
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}

	/**
	 * Checks if is read.
	 *
	 * @return true, if is read
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * Sets the read.
	 *
	 * @param read the new read
	 */
	public void setRead(boolean read) {
		this.read = read;
	}

	/**
	 * Gets the message receptor.
	 *
	 * @return the message receptor
	 */
	public String getMessageReceptor() {
		return messageReceptor;
	}

	/**
	 * Sets the message receptor.
	 *
	 * @param messageReceptor the new message receptor
	 */
	public void setMessageReceptor(String messageReceptor) {
		this.messageReceptor = messageReceptor;
	}

	/**
	 * Gets the message issuer.
	 *
	 * @return the message issuer
	 */
	public String getMessageIssuer() {
		return messageIssuer;
	}

	/**
	 * Sets the message issuer.
	 *
	 * @param messageIssuer the new message issuer
	 */
	public void setMessageIssuer(String messageIssuer) {
		this.messageIssuer = messageIssuer;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	
}
