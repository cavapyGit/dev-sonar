package com.pradera.commons.notifications.remote;

import java.io.Serializable;

import com.pradera.integration.contextholder.LoggerUser;

/**
 * @author Pradera Technologies
 *
 */
public class NotificationMessgaeDetail implements Serializable{
	/**default serialVersionUID*/
	private static final long serialVersionUID = 1L;
	/**the fullName*/
	private String fullName;
	/**the email*/
	private String email;
	/**the phoneNumber*/
	private String phoneNumber;
	/**the userName*/
	private String userName;
	/**the businessProcessId*/
	private Long businessProcessId;
	/**the subject*/
	private String subject;
	/**the message*/
	private String message;
	/**the loggerUser*/
	private LoggerUser loggerUser;
	/**the processNotificationId*/
	private Long processNotificationId;
	
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the businessProcessId
	 */
	public Long getBusinessProcessId() {
		return businessProcessId;
	}
	/**
	 * @param businessProcessId the businessProcessId to set
	 */
	public void setBusinessProcessId(Long businessProcessId) {
		this.businessProcessId = businessProcessId;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the loggerUser
	 */
	public LoggerUser getLoggerUser() {
		return loggerUser;
	}
	/**
	 * @param loggerUser the loggerUser to set
	 */
	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}
	/**
	 * @return the processNotificationId
	 */
	public Long getProcessNotificationId() {
		return processNotificationId;
	}
	/**
	 * @param processNotificationId the processNotificationId to set
	 */
	public void setProcessNotificationId(Long processNotificationId) {
		this.processNotificationId = processNotificationId;
	}
}
