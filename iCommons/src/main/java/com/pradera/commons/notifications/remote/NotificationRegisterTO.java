package com.pradera.commons.notifications.remote;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.integration.contextholder.LoggerUser;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class NotificationRegisterTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 10/03/2014
 */
public class NotificationRegisterTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger user. */
	private LoggerUser loggerUser;
	
	/** The user name. */
	private String userName;
	
	/** The subject. */
	private String subject;
	
	/** The message. */
	private String message;
	
	/** The business process. */
	private Long businessProcess;
	
	/** The users. */
	private List<UserAccountSession> users;
	
	/** The notifications. */
	private List<Long> notifications;
	
	/** The status. */
	private Integer status;
	
	/** The notification type. */
	private Integer notificationType;
	
	/** The institution code. */
	private Object institutionCode;
	
	/** The parameters. */
	private Object[] parameters;

	/**
	 * Gets the logger user.
	 *
	 * @return the logger user
	 */
	public LoggerUser getLoggerUser() {
		return loggerUser;
	}

	/**
	 * Sets the logger user.
	 *
	 * @param loggerUser the new logger user
	 */
	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject the new subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the business process.
	 *
	 * @return the business process
	 */
	public Long getBusinessProcess() {
		return businessProcess;
	}

	/**
	 * Sets the business process.
	 *
	 * @param businessProcess the new business process
	 */
	public void setBusinessProcess(Long businessProcess) {
		this.businessProcess = businessProcess;
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public List<UserAccountSession> getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(List<UserAccountSession> users) {
		this.users = users;
	}

	/**
	 * Gets the notifications.
	 *
	 * @return the notifications
	 */
	public List<Long> getNotifications() {
		return notifications;
	}

	/**
	 * Sets the notifications.
	 *
	 * @param notifications the new notifications
	 */
	public void setNotifications(List<Long> notifications) {
		this.notifications = notifications;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the notification type.
	 *
	 * @return the notification type
	 */
	public Integer getNotificationType() {
		return notificationType;
	}

	/**
	 * Sets the notification type.
	 *
	 * @param notificationType the new notification type
	 */
	public void setNotificationType(Integer notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * Gets the institution code.
	 *
	 * @return the institution code
	 */
	public Object getInstitutionCode() {
		return institutionCode;
	}

	/**
	 * Sets the institution code.
	 *
	 * @param institutionCode the new institution code
	 */
	public void setInstitutionCode(Object institutionCode) {
		this.institutionCode = institutionCode;
	}

	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public Object[] getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters.
	 *
	 * @param parameters the new parameters
	 */
	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}
	
	

}
