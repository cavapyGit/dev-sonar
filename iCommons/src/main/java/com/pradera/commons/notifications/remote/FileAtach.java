package com.pradera.commons.notifications.remote;

import java.io.Serializable;

import com.pradera.commons.type.FileAtachType;

public class FileAtach implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileTitle;
	private String fileName;
	private String extension;
	private String dataFileB64;
	private String fileType;
	private byte[] file;
	
	public FileAtach() {
		super();
	}

	public FileAtach(String fileTitle, String fileName, String extension, String dataFileB64, FileAtachType fileTypeEnum) {
		super();
		this.fileTitle = fileTitle;
		this.fileName = fileName;
		this.extension = extension;
		this.dataFileB64 = dataFileB64;
		this.fileType = fileTypeEnum.name();
	}
	
	public String getFileTitle() {
		return fileTitle;
	}
	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDataFileB64() {
		return dataFileB64;
	}
	public void setDataFileB64(String dataFileB64) {
		this.dataFileB64 = dataFileB64;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public void setFileTypeEnum(FileAtachType fileType) {
		this.fileType = fileType.name();
	}

	public FileAtachType getFileTypeEnum() {
		if(this.fileType==null) {
			return null;
		}
		return FileAtachType.valueOf(this.fileType);
	}
	
	public boolean isFileTypeImg() {
		return FileAtachType.IMG.equals( this.getFileTypeEnum() );
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}
	
}
