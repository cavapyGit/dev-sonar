package com.pradera.commons.processes.remote.view.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MonitorSystemProcessFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/12/2013
 */
public class MonitorSystemProcessFilter implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The System ID. */
	private Integer idSystem;
	
	/** The id business process pk. */
	private Long idBusinessProcessPk;
	
	/** The ID MacroProcess. */
	private Long idMacroProcess;
	
	/** The ID Process Level One. */
	private Long idProcessLevelOne;
	
	/** The ID ProcessLevelTwo. */
	private Long idProcessLevelTwo;
	
	/** The MnemonicProcess. */
	private String mnemonicProcess;
	
	/** The Process Name. */
	private String processName;
	
	/** The selceted ExecutionType. */
	private Integer selectedExecutionType;
	
	/** The UserID. */
	private String idUser;
	
	/** The UserDescription. */
	private String userDescription;
	
	/** The InitialDate. */
	private Date initialDate;
	
	/** The FinalDate. */
	private Date finalDate;
	
	/** The MaxDate. */
	private Date maxDate;
	
	/** The ID ReferenceProcess. */
	private List<Long> idReferenceProcesses;
	
	/** The state. */
	private Integer state;
	
	/** The id process logger. */
	private Long idProcessLogger;

	/**
	 * Gets the id system.
	 *
	 * @return the idSystem
	 */
	public Integer getIdSystem() {
		return idSystem;
	}

	/**
	 * Sets the id system.
	 *
	 * @param idSystem the idSystem to set
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}

	/**
	 * Gets the id macro process.
	 *
	 * @return the idMacroProcess
	 */
	public Long getIdMacroProcess() {
		return idMacroProcess;
	}

	/**
	 * Sets the id macro process.
	 *
	 * @param idMacroProcess the idMacroProcess to set
	 */
	public void setIdMacroProcess(Long idMacroProcess) {
		this.idMacroProcess = idMacroProcess;
	}

	/**
	 * Gets the id process level one.
	 *
	 * @return the idProcessLevelOne
	 */
	public Long getIdProcessLevelOne() {
		return idProcessLevelOne;
	}

	/**
	 * Sets the id process level one.
	 *
	 * @param idProcessLevelOne the idProcessLevelOne to set
	 */
	public void setIdProcessLevelOne(Long idProcessLevelOne) {
		this.idProcessLevelOne = idProcessLevelOne;
	}

	/**
	 * Gets the id process level two.
	 *
	 * @return the idProcessLevelTwo
	 */
	public Long getIdProcessLevelTwo() {
		return idProcessLevelTwo;
	}

	/**
	 * Sets the id process level two.
	 *
	 * @param idProcessLevelTwo the idProcessLevelTwo to set
	 */
	public void setIdProcessLevelTwo(Long idProcessLevelTwo) {
		this.idProcessLevelTwo = idProcessLevelTwo;
	}

	/**
	 * Gets the mnemonic process.
	 *
	 * @return the mnemonicProcess
	 */
	public String getMnemonicProcess() {
		return mnemonicProcess;
	}

	/**
	 * Sets the mnemonic process.
	 *
	 * @param mnemonicProcess the mnemonicProcess to set
	 */
	public void setMnemonicProcess(String mnemonicProcess) {
		this.mnemonicProcess = mnemonicProcess;
	}

	/**
	 * Gets the process name.
	 *
	 * @return the processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * Sets the process name.
	 *
	 * @param processName the processName to set
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * Gets the selected execution type.
	 *
	 * @return the selectedExecutionType
	 */
	public Integer getSelectedExecutionType() {
		return selectedExecutionType;
	}

	/**
	 * Sets the selected execution type.
	 *
	 * @param selectedExecutionType the selectedExecutionType to set
	 */
	public void setSelectedExecutionType(Integer selectedExecutionType) {
		this.selectedExecutionType = selectedExecutionType;
	}

	/**
	 * Gets the id user.
	 *
	 * @return the idUser
	 */
	public String getIdUser() {
		return idUser;
	}

	/**
	 * Sets the id user.
	 *
	 * @param idUser the idUser to set
	 */
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	/**
	 * Gets the user description.
	 *
	 * @return the userDescription
	 */
	public String getUserDescription() {
		return userDescription;
	}

	/**
	 * Sets the user description.
	 *
	 * @param userDescription the userDescription to set
	 */
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		   this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		   this.finalDate = finalDate;
	}

	/**
	 * Gets the id reference processes.
	 *
	 * @return the idReferenceProcesses
	 */
	public List<Long> getIdReferenceProcesses() {
		return idReferenceProcesses;
	}

	/**
	 * Sets the id reference processes.
	 *
	 * @param idReferenceProcesses the idReferenceProcesses to set
	 */
	public void setIdReferenceProcesses(List<Long> idReferenceProcesses) {
		this.idReferenceProcesses = idReferenceProcesses;
	}

	/**
	 * Gets the id business process pk.
	 *
	 * @return the id business process pk
	 */
	public Long getIdBusinessProcessPk() {
		return idBusinessProcessPk;
	}

	/**
	 * Sets the id business process pk.
	 *
	 * @param idBusinessProcessPk the new id business process pk
	 */
	public void setIdBusinessProcessPk(Long idBusinessProcessPk) {
		this.idBusinessProcessPk = idBusinessProcessPk;
	}

	/**
	 * Gets the max date.
	 *
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		maxDate = CommonsUtilities.currentDate();
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the id process logger.
	 *
	 * @return the id process logger
	 */
	public Long getIdProcessLogger() {
		return idProcessLogger;
	}

	/**
	 * Sets the id process logger.
	 *
	 * @param idProcessLogger the new id process logger
	 */
	public void setIdProcessLogger(Long idProcessLogger) {
		this.idProcessLogger = idProcessLogger;
	}
	
	

}
