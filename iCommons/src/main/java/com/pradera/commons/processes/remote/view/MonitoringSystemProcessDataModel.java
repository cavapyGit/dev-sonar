package com.pradera.commons.processes.remote.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.commons.processes.remote.view.beans.ManageSystemProcessBean;

public class MonitoringSystemProcessDataModel extends ListDataModel<ManageSystemProcessBean> implements SelectableDataModel<ManageSystemProcessBean>,Serializable{

		public MonitoringSystemProcessDataModel(){
			
		}
		
		public MonitoringSystemProcessDataModel(List<ManageSystemProcessBean> data){
			super(data);
		}
		
		@Override
		public ManageSystemProcessBean getRowData(String rowKey) {
			List<ManageSystemProcessBean> systemProcesses=(List<ManageSystemProcessBean>)getWrappedData();
	        for(ManageSystemProcessBean systemProcess : systemProcesses) {  
	        	
	            if(String.valueOf(systemProcess.getIdProcessLoggerPk()).equals(rowKey))
	                return systemProcess;  
	        }  
			return null;
		}

		@Override
		public Object getRowKey(ManageSystemProcessBean systemProcess) {
			return systemProcess.getIdProcessLoggerPk();
		}

}
