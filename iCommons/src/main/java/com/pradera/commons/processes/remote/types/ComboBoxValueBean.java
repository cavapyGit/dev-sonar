package com.pradera.commons.processes.remote.types;

import java.io.Serializable;

public class ComboBoxValueBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Object itemLabel;
	private Object itemValue;
	/**
	 * @return the itemLabel
	 */
	public Object getItemLabel() {
		return itemLabel;
	}
	/**
	 * @param itemLabel the itemLabel to set
	 */
	public void setItemLabel(Object itemLabel) {
		this.itemLabel = itemLabel;
	}
	/**
	 * @return the itemValue
	 */
	public Object getItemValue() {
		return itemValue;
	}
	/**
	 * @param itemValue the itemValue to set
	 */
	public void setItemValue(Object itemValue) {
		this.itemValue = itemValue;
	}
	
}
