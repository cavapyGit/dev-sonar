package com.pradera.commons.processes.remote.types;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author PraderaTechnologies
 *
 */
public enum ProcessExecutionType {
	MANUAL(Integer.valueOf(1360),"MANUAL"),
	AUTOMATIC(Integer.valueOf(1361),"AUTOMATIC");
	
	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ProcessExecutionType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	public static final List<ProcessExecutionType> list = new ArrayList<ProcessExecutionType>();
	public static final Map<Integer, ProcessExecutionType> lookup = new HashMap<Integer, ProcessExecutionType>();
	static {
		for (ProcessExecutionType s : EnumSet.allOf(ProcessExecutionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
