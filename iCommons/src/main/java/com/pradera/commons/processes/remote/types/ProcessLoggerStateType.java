package com.pradera.commons.processes.remote.types;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ProcessLoggerStateType{
	
	REGISTERED(Integer.valueOf(1364),"REGISTRADO"),
	PROCESSING(Integer.valueOf(1365),"PROCESANDO"),
	FINISHED(Integer.valueOf(1366),"FINALIZADO"),
	CANCELED(Integer.valueOf(1367),"CANCELADO"),
	ERROR(Integer.valueOf(1368),"ERROR");
	
	private Integer code;
	private String value;		
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
			
	private ProcessLoggerStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<ProcessLoggerStateType> list = new ArrayList<ProcessLoggerStateType>();
	
	public static final Map<Integer, ProcessLoggerStateType> lookup = new HashMap<Integer, ProcessLoggerStateType>();
	static {
		for (ProcessLoggerStateType s : EnumSet.allOf(ProcessLoggerStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	    
}

	
