package com.pradera.commons.processes.scheduler;

import java.io.Serializable;

public class SynchronizeBatchEvent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long eventTypeCode;
	private Long eventLoggerCode;
	private Integer indBegin;
	private Integer indFinish;
	private String beginTime;
	private String finishTime;
	private Integer indHasError;
	private String errorDetail;
	private Long rowQuantity;
	private Integer percentage;
	private String processDate;
	private Integer isAllFinish;
	public Long getEventTypeCode() {
		return eventTypeCode;
	}
	public void setEventTypeCode(Long eventTypeCode) {
		this.eventTypeCode = eventTypeCode;
	}
	public Integer getIndBegin() {
		return indBegin;
	}
	public void setIndBegin(Integer indBegin) {
		this.indBegin = indBegin;
	}
	public Integer getIndFinish() {
		return indFinish;
	}
	public void setIndFinish(Integer indFinish) {
		this.indFinish = indFinish;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getIndHasError() {
		return indHasError;
	}
	public void setIndHasError(Integer indHasError) {
		this.indHasError = indHasError;
	}
	public Long getRowQuantity() {
		return rowQuantity;
	}
	public void setRowQuantity(Long rowQuantity) {
		this.rowQuantity = rowQuantity;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eventTypeCode == null) ? 0 : eventTypeCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SynchronizeBatchEvent other = (SynchronizeBatchEvent) obj;
		if (eventTypeCode == null) {
			if (other.eventTypeCode != null)
				return false;
		} else if (!eventTypeCode.equals(other.eventTypeCode))
			return false;
		return true;
	}
	public String getProcessDate() {
		return processDate;
	}
	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}
	public Long getEventLoggerCode() {
		return eventLoggerCode;
	}
	public void setEventLoggerCode(Long eventLoggerCode) {
		this.eventLoggerCode = eventLoggerCode;
	}
	public String getErrorDetail() {
		return errorDetail;
	}
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}
	public Integer getIsAllFinish() {
		return isAllFinish;
	}
	public void setIsAllFinish(Integer isAllFinish) {
		this.isAllFinish = isAllFinish;
	}
}