package com.pradera.commons.processes.scheduler;

import java.util.HashMap;
import java.util.Map;

public class SynchronizedBatch {

	private String userName;
	private Long businessProcess;
	private Map<String,String> parameters;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getBusinessProcess() {
		return businessProcess;
	}
	public void setBusinessProcess(Long businessProcess) {
		this.businessProcess = businessProcess;
	}
	public Map<String, String> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
	public Map<String, Object> getObjectParameters() {
		if(parameters!=null && !parameters.isEmpty()){
			Map<String, Object> fullData = new HashMap<>();
			for(String data: parameters.keySet()){
				fullData.put(data, parameters.get(data));
			}
			return fullData;
		}
		return null;
	}
}