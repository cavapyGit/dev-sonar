package com.pradera.commons.processes.remote.view.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author PraderaTechnologies
 * 
 */
public class ManageSystemProcessBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The ProcessLoggerId
	 */
	private Integer idProcessLoggerPk;
	/**
	 * The ProcessDate
	 */
	private Date processDate;
	/**
	 * The ProcessLoggerState
	 */
	private Integer state;
	/**
	 * The StateDiscription
	 */
	private String stateDiscription;
	/**
	 * The ProcessType
	 */
	private Integer processType;
	/**
	 * The Mnemonic
	 */
	private String mnemonic;
	/**
	 * The ProcessName
	 */
	private String processName;
	/**
	 * The FinishingDate
	 */
	private Date finishingDate;
	/**
	 * The ProcessDuration
	 */
	private double processDuration;
	/**
	 * The System ID
	 */
	private Integer idSystem;
	/**
	 * The System Name
	 */
	private String system;
	/**
	 * The MacroProcess Name
	 */
	private String macroProcess;
	/**
	 * The ProcessLevelOne
	 */
	private String processLevel1;
	/**
	 * The ProcessLevelTwo
	 */
	private String processLevel2;
	/**
	 * The ExecutionType
	 */
	private String executionType;
	/**
	 * The UserName
	 */
	private String userName;

	/**
	 * @return the idProcessLoggerPk
	 */
	public Integer getIdProcessLoggerPk() {
		return idProcessLoggerPk;
	}

	/**
	 * @param idProcessLoggerPk
	 *            the idProcessLoggerPk to set
	 */
	public void setIdProcessLoggerPk(Integer idProcessLoggerPk) {
		this.idProcessLoggerPk = idProcessLoggerPk;
	}

	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * @param processDate
	 *            the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * @return the processType
	 */
	public Integer getProcessType() {
		return processType;
	}

	/**
	 * @param processType
	 *            the processType to set
	 */
	public void setProcessType(Integer processType) {
		this.processType = processType;
	}

	/**
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * @param mnemonic
	 *            the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * @return the processName
	 */
	public String getProcessName() {
		return processName;
	}

	/**
	 * @param processName
	 *            the processName to set
	 */
	public void setProcessName(String processName) {
		this.processName = processName;
	}

	/**
	 * @return the finishingDate
	 */
	public Date getFinishingDate() {
		return finishingDate;
	}

	/**
	 * @param finishingDate
	 *            the finishingDate to set
	 */
	public void setFinishingDate(Date finishingDate) {
		this.finishingDate = finishingDate;
	}


//	/**
//	 * @return the stateDiscription
//	 */
//	public String getStateDiscription() {
//		stateDiscription = ProcessLoggerStateType.lookup.get(state).getValue();
//		return stateDiscription;
//	}

	/**
	 * @return the idSystem
	 */
	public Integer getIdSystem() {
		return idSystem;
	}

	/**
	 * @param idSystem
	 *            the idSystem to set
	 */
	public void setIdSystem(Integer idSystem) {
		this.idSystem = idSystem;
	}

	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @param system
	 *            the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * @return the macroProcess
	 */
	public String getMacroProcess() {
		return macroProcess;
	}

	/**
	 * @param macroProcess
	 *            the macroProcess to set
	 */
	public void setMacroProcess(String macroProcess) {
		this.macroProcess = macroProcess;
	}

	/**
	 * @return the processLevel1
	 */
	public String getProcessLevel1() {
		return processLevel1;
	}

	/**
	 * @param processLevel1
	 *            the processLevel1 to set
	 */
	public void setProcessLevel1(String processLevel1) {
		this.processLevel1 = processLevel1;
	}

	/**
	 * @return the processLevel2
	 */
	public String getProcessLevel2() {
		return processLevel2;
	}

	/**
	 * @param processLevel2
	 *            the processLevel2 to set
	 */
	public void setProcessLevel2(String processLevel2) {
		this.processLevel2 = processLevel2;
	}

	/**
	 * @return the executionType
	 */
	public String getExecutionType() {
		return executionType;
	}

	/**
	 * @param executionType
	 *            the executionType to set
	 */
	public void setExecutionType(String executionType) {
		this.executionType = executionType;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStateDiscription() {
		return stateDiscription;
	}

	public void setStateDiscription(String stateDiscription) {
		this.stateDiscription = stateDiscription;
	}

	public double getProcessDuration() {
		return processDuration;
	}

	public void setProcessDuration(double processDuration) {
		this.processDuration = processDuration;
	}



}
