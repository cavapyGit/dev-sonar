package com.pradera.commons.processes.remote;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

public class RemoteUtilities {
	private static Object object;
	
	/**
	 * to get the lookup for ReporteDinamicoBeanRemote 
	 * @return ReporteDinamicoBeanRemote
	 */
	public static Object getRemoteObject(String strLookup){
		try{
			final Hashtable jndiProperties = new Hashtable();
	        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	        final Context context = new InitialContext(jndiProperties);
	        object = context.lookup(strLookup);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}
}
