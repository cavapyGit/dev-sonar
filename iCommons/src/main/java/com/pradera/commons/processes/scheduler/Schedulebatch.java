package com.pradera.commons.processes.scheduler;

import java.io.Serializable;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class Schedulebatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24/06/2013
 */
public class Schedulebatch implements  Serializable{
    
    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6716213177596303714L;
	
	/** The id process schedule. */
	private Long idProcessSchedule;

	/** The id business process. */
	private Long idBusinessProcess;
    
    /** The user name. */
    private String userName;
    
    /** The parameters. */
    private Map<String,String> parameters;
    
    /** The privilege code. */
    private Integer privilegeCode;
    
    /** The ip addres. */
    private String ipAddres;

    /**
     * Instantiates a new schedulebatch.
     */
    public Schedulebatch() {
        
    }
    
    

    /**
     * Gets the id business process.
     *
     * @return the id business process
     */
    public Long getIdBusinessProcess() {
        return idBusinessProcess;
    }

    /**
     * Sets the id business process.
     *
     * @param idBusinessProcess the new id business process
     */
    public void setIdBusinessProcess(Long idBusinessProcess) {
        this.idBusinessProcess = idBusinessProcess;
    }

    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     *
     * @param userName the new user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the parameters.
     *
     * @return the parameters
     */
    public Map<String,String> getParameters() {
        return parameters;
    }

    /**
     * Sets the parameters.
     *
     * @param parameters the new parameters
     */
    public void setParameters(Map<String,String> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets the privilege code.
     *
     * @return the privilege code
     */
    public Integer getPrivilegeCode() {
        return privilegeCode;
    }

    /**
     * Sets the privilege code.
     *
     * @param privilegeCode the new privilege code
     */
    public void setPrivilegeCode(Integer privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.idBusinessProcess != null ? this.idBusinessProcess.hashCode() : 0);
        hash = 29 * hash + (this.privilegeCode != null ? this.privilegeCode.hashCode() : 0);
        return hash;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Schedulebatch other = (Schedulebatch) obj;
        if (this.idBusinessProcess != other.idBusinessProcess && (this.idBusinessProcess == null || !this.idBusinessProcess.equals(other.idBusinessProcess))) {
            return false;
        }
        if (this.privilegeCode != other.privilegeCode && (this.privilegeCode == null || !this.privilegeCode.equals(other.privilegeCode))) {
            return false;
        }
        return true;
    }



	/**
	 * Gets the ip addres.
	 *
	 * @return the ip addres
	 */
	public String getIpAddres() {
		return ipAddres;
	}



	/**
	 * Sets the ip addres.
	 *
	 * @param ipAddres the new ip addres
	 */
	public void setIpAddres(String ipAddres) {
		this.ipAddres = ipAddres;
	}



	/**
	 * Gets the id process schedule.
	 *
	 * @return the id process schedule
	 */
	public Long getIdProcessSchedule() {
		return idProcessSchedule;
	}



	/**
	 * Sets the id process schedule.
	 *
	 * @param idProcessSchedule the new id process schedule
	 */
	public void setIdProcessSchedule(Long idProcessSchedule) {
		this.idProcessSchedule = idProcessSchedule;
	}

   
}
