package com.pradera.commons.processes.remote.view.beans;

import java.io.Serializable;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class BeanType
 * 
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */

public class ParametersBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String value;

	public ParametersBean(String text) {
		value = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
