package com.pradera.commons.sessionuser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Timer;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.pradera.commons.audit.UserAuditTrackingTO;
import com.pradera.commons.audit.UserTrackProcess;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.httpevent.publish.BrowserWindow;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.notifications.remote.Message;
import com.pradera.commons.notifications.remote.MessageFilter;
import com.pradera.commons.notifications.remote.NotificationRegisterTO;
import com.pradera.commons.processes.scheduler.Schedulebatch;
import com.pradera.commons.processes.scheduler.SynchronizeBatchEvent;
import com.pradera.commons.processes.scheduler.SynchronizedBatch;
import com.pradera.commons.security.model.type.LoginMessageType;
import com.pradera.commons.security.model.type.UserAccountResponsabilityType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.InterfaceFileTO;
import com.pradera.integration.component.custody.to.XmlRespuestaActivoFinanciero;
import com.pradera.integration.component.custody.to.XmlSolicitudActivoFinanciero;
import com.pradera.integration.exception.ServiceException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class Utils.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/02/2013
 */
@ApplicationScoped
public class ClientRestService {
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The server path. */
	@Inject @StageDependent
	private String serverPath;
	
	/** The security path. */
	@Inject @StageDependent
	private String securityPath;
	
	@Inject
	StageApplication stageApplication;
	
	/** The server configuration. */
	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;
	
	public static final String CERT_FULL_PATH = "certFullPath";
	/**
	 * Creates the client jerser.
	 * using like workaround because ssl self signed
	 * @return the client
	 * @throws NoSuchAlgorithmException the no such algorithm exception
	 * @throws KeyManagementException the key management exception
	 */
	private Client createClientJerser() throws NoSuchAlgorithmException,KeyManagementException{
		/** Este fragmento de código sirve para que el wildfly DEJE de validar el certificado SSL 
		 *  NO SE RECOMIENDA REACTIVAR ESTE CÓDIGO 
		 
		 SSLContext sslcontext = SSLContext.getInstance("TLS");
		    sslcontext.init(null, new TrustManager[]{new TrustManager() {
		        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
		        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
		        	return new java.security.cert.X509Certificate[0];
		        	}
				@Override
				public void checkClientTrusted(
						java.security.cert.X509Certificate[] arg0, String arg1)
						throws java.security.cert.CertificateException {
					// TODO Auto-generated method stub
					
				}
				@Override
				public void checkServerTrusted(
						java.security.cert.X509Certificate[] arg0, String arg1)
						throws java.security.cert.CertificateException {
					// TODO Auto-generated method stub
					
				}

		    }}, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
		ClientConfig config = new DefaultClientConfig();
        config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(
            new HostnameVerifier() {
                @Override
                public boolean verify( String s, SSLSession sslSession ) {
                    return true;
                }
            }
        ,sslcontext));
        return Client.create(config);
        */
		
		/**
		 * Este fragmento de código vaida un certificado SSL cuyo archivo .crt este en la ruta configurada
		 */
		log.info("Iniciando Validacion de certificado");
		log.info(stageApplication.toString());
		if(stageApplication !=null && ( stageApplication.equals(StageApplication.Production) 
				|| stageApplication.equals(StageApplication.SystemTest)) ) {
			
			String certFUllPath = this.applicationConfiguration.getProperty(stageApplication+"."+CERT_FULL_PATH, this.applicationConfiguration.getProperty(CERT_FULL_PATH));
			
			File crtFile = new File(certFUllPath);
			Certificate certificate;
			try {
				certificate = CertificateFactory.getInstance("X.509").generateCertificate(new FileInputStream(crtFile));
		
				// Or if the crt-file is packaged into a jar file:
				// CertificateFactory.getInstance("X.509").generateCertificate(this.class.getClassLoader().getResourceAsStream("server.crt"));
		
				KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				keyStore.load(null, null);
				keyStore.setCertificateEntry("server", certificate);
		
				TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				trustManagerFactory.init(keyStore);
		
				SSLContext sslContext = SSLContext.getInstance("TLS");
				sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
				
				HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
				ClientConfig config = new DefaultClientConfig();
		        config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(
		            new HostnameVerifier() {
		                @Override
		                public boolean verify( String s, SSLSession sslSession ) {
		                    return true;
		                }
		            }
		        ,sslContext));
		        return Client.create(config);
	
			} catch (java.security.cert.CertificateException e) {
				log.info("Error Validacion de certificado");
				log.error(e.getMessage()); 
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				log.info("Error Validacion de certificado");
				log.error(e.getMessage());
				e.printStackTrace();
			} catch (KeyStoreException e) {
				log.info("Error Validacion de certificado");
				log.error(e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				log.info("Error Validacion de certificado");
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		return Client.create();
	}
	

	
	/**
	 * Find user info by session id.
	 * call remote to Security module
	 * @param userSessionId the user session id
	 * @param module the module
	 * @return the user info
	 */
	public UserInfo findUserInfoBySessionId(String userSessionId, String module){
		try{
		StringBuilder urlResource = new StringBuilder();
                urlResource.append(serverPath).
    				append(securityPath).
    				append(serverConfiguration.getProperty("resource.path"));
                
                Client client = createClientJerser();
                WebResource webResource = client.resource(urlResource.toString());
                StringBuilder parameters = new StringBuilder().
                    append(serverConfiguration.getProperty("resource.slash")).
                    append(userSessionId).
                            append(serverConfiguration.getProperty("resource.hyphen")).
                            append(module);
        
                String data = webResource.path(parameters.toString()).type(serverConfiguration.getProperty("resource.type")) .get(String.class);
                data = data.replaceAll(",\"pwdMustChange\":false", "");
                return new Gson().fromJson(data, UserInfo.class);
		} catch (UniformInterfaceException uex){
			log.error(uex.getMessage()+ ": codigo error "+ uex.getResponse().getStatus());
			return null;
		} 
		catch (Exception e) {
		    log.error(e.getMessage());
		    return null;
	    } 
	
	}
	
	/**
	 * Notify event browser window in Security.
	 *
	 * @param browserWindow the browser window
	 */
	public void notifyEventBrowserWindow(BrowserWindow browserWindow) {
		try{
		StringBuilder urlResource = new StringBuilder();
                urlResource.append(serverPath).append(securityPath).
                append(serverConfiguration.getProperty("resource.notification.path"));	
                Client client = createClientJerser();
                String input = new Gson().toJson(browserWindow);
                WebResource webResource = client.resource(urlResource.toString());
                ClientResponse response = webResource.type("application/json")
                           .post(ClientResponse.class, input);
                if (response.getStatus() != 200) {
                        log.error("error trigger notification "+response.getEntity(String.class));

                } 
 		} catch (Exception e) {
		    log.error(e.getMessage());
	    } 
	}
        
    /**
     * Excute remote batch process.
     *
     * @param scheduleBatch the schedule batch
     * @param ModuleName the module name
     */
    public void excuteRemoteBatchProcess(Schedulebatch scheduleBatch,String ModuleName){
	try{
            StringBuilder urlResource = new StringBuilder();
            urlResource.append(serverPath).append("/").append(ModuleName).
//            urlResource.append("http://127.0.0.1:8080").append("/").append(ModuleName).  
            append(serverConfiguration.getProperty("resource.batchprocess.path"));	
            Client client = createClientJerser();
            String input = new Gson().toJson(scheduleBatch); 
            WebResource webResource = client.resource(urlResource.toString()); 
            ClientResponse response = webResource.type("application/json")
                       .post(ClientResponse.class, input);
            if (response.getStatus() != 200) {
                    log.error("error execute remote batchprocess "+response.getEntity(String.class));
            }
        } catch (Exception e) {
        	log.error(e.getMessage()); 
        }
    }
    
    /**
     * Excute sync batch process.
     *
     * @param batch the batch
     * @param ModuleName the module name
     * @throws ServiceException the service exception
     */
    public void excuteSyncBatchProcess(SynchronizedBatch batch,String ModuleName) throws ServiceException{
    	try{
//            Thread.sleep(new Random().nextInt(8)*1000);
    		StringBuilder urlResource = new StringBuilder(); 
//            urlResource.append("http://127.0.0.1:8080").append("/").append(ModuleName).
            urlResource.append(serverPath).append("/").append(ModuleName).
            append(serverConfiguration.getProperty("resource.batchprocess.sync.path"));
            Client client = createClientJerser();
            String input = new Gson().toJson(batch);
            WebResource webResource = client.resource(urlResource.toString());
            ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);
            if (response.getStatus() != 200) {
                    log.error("error execute remote batchprocess "+response.getEntity(String.class));
            }
        } catch (Exception e) {
        	log.error(e.getMessage());
        }
    }
    
    /**
     * Fire synchronized batch event.
     *
     * @param batchEvent the batch event
     * @throws ServiceException the service exception
     */
    public void fireSynchronizedBatchEvent(SynchronizeBatchEvent batchEvent) throws ServiceException{
    	try{
	    	StringBuilder urlResource = new StringBuilder();
//	        urlResource.append("http://127.0.0.1:8080").append("/").append(ModuleWarType.PARAMETER.getValue()).
	        urlResource.append(serverPath).append("/").append(ModuleWarType.PARAMETER.getValue()).
	        append(serverConfiguration.getProperty("resource.batchprocess.sync.event"));
	        String input = new Gson().toJson(batchEvent);
	        createClientJerser().resource(urlResource.toString()).type("application/json").post(ClientResponse.class, input);
    	} catch (Exception e) {
        	log.error(e.getMessage());
        }
    }
    
    /**
     * Start operative daily process.
     *
     * @param userName the user name
     * @param ipAddress the ip address
     * @throws ServiceException the service exception
     */
    public void startOperativeDailyProcess(String userName, String ipAddress) throws ServiceException{
    	try{
	    	StringBuilder urlResource = new StringBuilder();
//	        urlResource.append("http://127.0.0.1:8080").append("/").append(ModuleWarType.BUSINESS.getValue()).
	        urlResource.append(serverPath).append("/").append(ModuleWarType.BUSINESS.getValue()).
	        append(serverConfiguration.getProperty("resource.batchprocess.sync.start")).
	        append("/").append(userName).append("/").append(ipAddress);
	        createClientJerser().resource(urlResource.toString()).type("application/json").get(ClientResponse.class);
    	} catch (Exception e) {
        	log.error(e.getMessage());
        }
    }
    
    /**
     * Gets the users or emails list.
     *
     * @param userState the user state
     * @param getUsers the get users
     * @param institution the institution
     * @param institutionCode the institution code
     * @param responsability the responsability
     * @return the users or emails
     */
    public List<String> getUsersOrEmails(Integer userState,boolean getUsers, InstitutionType institution,Object institutionCode
			,UserAccountResponsabilityType responsability) {
    	ArrayList<String> usersInfo = null;
    	try {
    		UserFilterTO userFilter = new UserFilterTO();
    		userFilter.setState(userState);
    		if(institution != null) {
    			userFilter.setInstitutionType(institution.getCode());
    			if(institutionCode != null){
    				userFilter.setEntityCode(institutionCode);
    			}
    		}
    		if(responsability != null) {
    			userFilter.setResponsibility(responsability.getCode());
    		}
    		StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append(securityPath);
    		//choose remote method
    		if(getUsers) {
    			urlResource.append(serverConfiguration.getProperty("resource.user.logins.path"));
    		} else {
    			urlResource.append(serverConfiguration.getProperty("resource.user.emails.path"));
    		}
    		 Client client = createClientJerser();
    		 WebResource webResource = client.resource(urlResource.toString());
    		 //parameters json
            String input = new Gson().toJson(userFilter);
            String users = webResource.type("application/json").post(String.class, input);
            usersInfo = new Gson().fromJson(users, ArrayList.class);
    		return usersInfo;
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        	return usersInfo;
        }
    }
    
    /**
     * Gets the users information.
     *
     * @param userState the user state
     * @param institution the institution
     * @param institutionCode the institution code
     * @param responsability the responsability
     * @param userName the user name
     * @param indExtInterface the ind ext interface
     * @return the users info
     */
    public List<UserAccountSession> getUsersInformation(Integer userState,InstitutionType institution,Object institutionCode
			,UserAccountResponsabilityType responsability,String userName,Integer indExtInterface){
    	List<UserAccountSession> accounts = null;
    	try {
    		UserFilterTO userFilter = new UserFilterTO();
    		userFilter.setState(userState);
    		if(institution != null) {
    			userFilter.setInstitutionType(institution.getCode());
    			if(institutionCode != null){
    				userFilter.setEntityCode(institutionCode);
    			}
    		}
    		if(responsability != null) {
    			userFilter.setResponsibility(responsability.getCode());
    		}
    		userFilter.setUserName(userName);
    		userFilter.setIndExtInterface(indExtInterface);
    		StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append(securityPath);
    		
    		//choose remote method
    		urlResource.append(serverConfiguration.getProperty("resource.users.information.path"));
    		Client client = createClientJerser();
    		WebResource webResource = client.resource(urlResource.toString());
    		 //parameters json
            String input = new Gson().toJson(userFilter);
            String users = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
            accounts = new Gson().fromJson(users, listOfObject);
    		return accounts;
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        	return accounts;
        }
    	
    }
    
    /**
     * Gets the users information for Billing.
     *
     * @param userState the user state
     * @param institution the institution
     * @param institutionCode the institution code
     * @param indExtInterface the ind ext interface
     * @param registerDate the register date
     * @return the users info
     */
    public List<UserAccountSession> getUsersInformationForBilling(Integer userState,Integer institution,Object institutionCode
			,Integer indExtInterface, String registerDate){
    	List<UserAccountSession> accounts = null;
    	
    	
    	try {
        		UserFilterTO userFilter = new UserFilterTO();
        		userFilter.setState(userState);
        		
        		if(Validations.validateIsNotNull(institution)) {
        			userFilter.setInstitutionType(institution);
        			if(institutionCode != null){
        				userFilter.setEntityCode(institutionCode);
        			}
        		}
        		if(Validations.validateIsNotNull(registerDate)){
        			userFilter.setRegisterDate(registerDate);
        		}
        		
        		userFilter.setIndExtInterface(indExtInterface);
        			        		
        		StringBuilder urlResource = new StringBuilder();
        		urlResource.append(serverPath).append(securityPath);
        		//choose remote method
        		urlResource.append(serverConfiguration.getProperty("resource.users.billing.path"));
        		Client client = createClientJerser();
        		WebResource webResource = client.resource(urlResource.toString());
        		 //parameters json
                String input = new Gson().toJson(userFilter);
                String users = webResource.type("application/json").post(String.class, input);
                Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
                accounts = new Gson().fromJson(users, listOfObject);
                
                return accounts;
    		
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        	return accounts;
        }
    }
    
    /**
     * Gets the users information supervisor.
     *
     * @param userFilter the user filter
     * @return the users information
     */
    public List<UserAccountSession> getUsersInformationSupervisor(UserFilterTO userFilter)
    {
       	List<UserAccountSession> accounts = null;
       	try {
       		StringBuilder urlResource = new StringBuilder();
       		urlResource.append(serverPath).append(securityPath);
       		urlResource.append(serverConfiguration.getProperty("resource.users.supervisor.path"));
       		Client client = createClientJerser();
       		WebResource webResource = client.resource(urlResource.toString());
       		String input = new Gson().toJson(userFilter);
       		String users = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
            accounts = new Gson().fromJson(users, listOfObject);
       		return accounts;
       	} catch (Exception e) {
           	log.error(e.getMessage());
           	e.printStackTrace();
           	return accounts;
        }       	
    }
    
    public List<UserAccountSession> getUsersInformationOperator(UserFilterTO userFilter)
    {
       	List<UserAccountSession> accounts = null;
       	try {
       		StringBuilder urlResource = new StringBuilder();
       		urlResource.append(serverPath).append(securityPath);
       		urlResource.append(serverConfiguration.getProperty("resource.users.operator.path"));
       		Client client = createClientJerser();
       		WebResource webResource = client.resource(urlResource.toString());
       		String input = new Gson().toJson(userFilter);
       		String users = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
            accounts = new Gson().fromJson(users, listOfObject);
       		return accounts;
       	} catch (Exception e) {
           	log.error(e.getMessage());
           	e.printStackTrace();
           	return accounts;
        }       	
    }
    
    public List<UserAccountSession> getUsersInformationSupervisorAndOperator(UserFilterTO userFilter)
    {
       	List<UserAccountSession> accounts = null;
       	try {
       		StringBuilder urlResource = new StringBuilder();
       		urlResource.append(serverPath).append(securityPath);
       		urlResource.append(serverConfiguration.getProperty("resource.users.supervisoroperator.path"));
       		Client client = createClientJerser();
       		WebResource webResource = client.resource(urlResource.toString());
       		String input = new Gson().toJson(userFilter);
       		String users = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
            accounts = new Gson().fromJson(users, listOfObject);
       		return accounts;
       	} catch (Exception e) {
           	log.error(e.getMessage());
           	e.printStackTrace();
           	return accounts;
        }       	
    }

    public List<UserAccountSession> getUsersInformationHolderAccount(UserFilterTO userFilter)
    {
       	List<UserAccountSession> accounts = null;
       	try {
       		StringBuilder urlResource = new StringBuilder();
       		urlResource.append(serverPath).append(securityPath);
       		urlResource.append(serverConfiguration.getProperty("resource.users.holderaccount.path"));
       		Client client = createClientJerser();
       		WebResource webResource = client.resource(urlResource.toString());
       		String input = new Gson().toJson(userFilter);
       		String users = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
            accounts = new Gson().fromJson(users, listOfObject);
       		return accounts;
       	} catch (Exception e) {
           	log.error(e.getMessage());
           	e.printStackTrace();
           	return accounts;
        }       	
    }
    
    
    /**
     * Gets the institution information.
     * @param institution the institution
    
     * @return the list of object array which contain the institution information
     */
    public List<Object[]> getInstitutionInformation(Integer institution){
    	List<Object[]> institutions = null;
    	try {
    		UserFilterTO userFilter = new UserFilterTO();
    		if(institution != null) {
    			userFilter.setInstitutionType(institution);
    	    }
    		StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append(securityPath);
    		//choose remote method
    		urlResource.append(serverConfiguration.getProperty("resource.users.institution.path"));
    		Client client = createClientJerser();
    		WebResource webResource = client.resource(urlResource.toString());
    		 //parameters json
            String input = new Gson().toJson(userFilter);
            String users = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<Object[]>>(){}.getType();
            institutions = new Gson().fromJson(users, listOfObject);
    		return institutions;
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        	return institutions;
        }
    	
    }
    
    /**
     * Register user track process.
     *
     * @param userTrackProcess the user track process
     */
    public void registerUserTrackProcess(UserTrackProcess userTrackProcess){
    	try {
    		StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append(securityPath).
    		 append(serverConfiguration.getProperty("resource.users.track.path"));	
             Client client = createClientJerser();
             String input = new Gson().toJson(userTrackProcess);
             WebResource webResource = client.resource(urlResource.toString());
             ClientResponse response = webResource.type("application/json")
                        .post(ClientResponse.class, input);
             if (response.getStatus() != 200) {
                     log.error("error register audit process "+response.getEntity(String.class));

             } 
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        }
    }
    
    /**
     * Register close session user.
     *
     * @param userInfo the user info
     */
    public void registerCloseSessionUser(UserInfo userInfo){
    	try {
    	StringBuilder urlResource = new StringBuilder();
		urlResource.append(serverPath).append(securityPath).
		 append(serverConfiguration.getProperty("resource.users.session.path"));	
         Client client = createClientJerser();
         String input = new Gson().toJson(userInfo);
         WebResource webResource = client.resource(urlResource.toString());
         ClientResponse response = webResource.type("application/json")
                    .post(ClientResponse.class, input);
         if (response.getStatus() != 200) {
                 log.error("error register audit process "+response.getEntity(String.class));

         } 
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        }
    }
    
    /**
     * Register close all sessions.
     */
    public void registerCloseAllSessions(){
    	try {
    	StringBuilder urlResource = new StringBuilder();
		urlResource.append(serverPath).append(securityPath).
		 append(serverConfiguration.getProperty("resource.audits.closeallsession.path"));	
         Client client = createClientJerser();
         WebResource webResource = client.resource(urlResource.toString());
         ClientResponse response = webResource.post(ClientResponse.class);
         if (response.getStatus() != 200) {
                 log.error("error register audit process "+response.getEntity(String.class));

         } 
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        }
    }
    
    /**
     * Register user audit tracking.
     *
     * @param userAuditTracking the user audit tracking
     */
    public void registerUserAuditTracking(UserAuditTrackingTO userAuditTracking){
    	try {
        	StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append(securityPath).
    		 append(serverConfiguration.getProperty("resource.audits.registeruseraudit.path"));	
             Client client = createClientJerser();
             String input = new Gson().toJson(userAuditTracking);
             WebResource webResource = client.resource(urlResource.toString());
             ClientResponse response = webResource.type("application/json")
                        .post(ClientResponse.class, input);
             if (response.getStatus() != 200) {
                     log.error("error register audit process "+response.getEntity(String.class));

             } 
        	} catch (Exception e) {
            	log.error(e.getMessage());
            	e.printStackTrace();
            }
    }
    
    /**
     * Search notifications.
     *
     * @param messageFilter the message filter
     * @return the list
     */
    public List<Message> searchNotifications(MessageFilter messageFilter) {
    	List<Message> messages = null;
    	try{
    		StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
    		append(serverConfiguration.getProperty("resources.notification.search.path"));
    		Client client = createClientJerser();
    		WebResource webResource = client.resource(urlResource.toString());
    		 //parameters json
            String input = new Gson().toJson(messageFilter);
            String notifications = webResource.type("application/json").post(String.class, input);
            Type listOfObject = new TypeToken<List<Message>>(){}.getType();
         // Creates the json object which will manage the information received 
            GsonBuilder builder = new GsonBuilder(); 
            // Register an adapter to manage the date types as long values 
            builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
               public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                  return new Date(json.getAsJsonPrimitive().getAsLong()); 
               } 
            });
            Gson gson = builder.create();
            messages = gson.fromJson(notifications, listOfObject);
            return messages;
    	} catch (Exception e) {
        	log.error(e.getMessage());
        	e.printStackTrace();
        	return messages;
        }
    }
    
    /**
     * Send notification manual.
     *
     * @param notificationRegisterTO the notification register to
     */
    public void sendNotificationManual(NotificationRegisterTO notificationRegisterTO){
    	try {
        	StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
    		 append(serverConfiguration.getProperty("resources.notification.send.manual.path"));	
             Client client = createClientJerser();
             // Creates the json object which will manage the information received 
             GsonBuilder builder = new GsonBuilder(); 
             builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>(){
            	public JsonElement serialize(Date date, Type typeOfT,JsonSerializationContext context) throws JsonParseException{
            		Calendar cal = Calendar.getInstance();
            		cal.setTime(date);
            		return new JsonPrimitive(cal.getTimeInMillis());
            	}
             });  
             Gson gson = builder.create();
             String input = gson.toJson(notificationRegisterTO);
             WebResource webResource = client.resource(urlResource.toString());
             ClientResponse response = webResource.type("application/json")
                        .post(ClientResponse.class, input);
             if (response.getStatus() != 200) {
                     log.error("error send notification manual "+response.getEntity(String.class));
             } 
        	} catch (Exception e) {
            	log.error(e.getMessage());
            	e.printStackTrace();
            }
    }
    
    /**
     * Update notification status.
     *
     * @param notificationRegisterTO the notification register to
     */
    public void updateNotificationStatus(NotificationRegisterTO notificationRegisterTO){
    	try {
        	StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
    		 append(serverConfiguration.getProperty("resources.notification.update.status.path"));	
             Client client = createClientJerser();
          // Creates the json object which will manage the information received 
             GsonBuilder builder = new GsonBuilder(); 
             builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>(){
            	public JsonElement serialize(Date date, Type typeOfT,JsonSerializationContext context) throws JsonParseException{
            		Calendar cal = Calendar.getInstance();
            		cal.setTime(date);
            		return new JsonPrimitive(cal.getTimeInMillis());
            	}
             }); 
             Gson gson = builder.create();
             String input = gson.toJson(notificationRegisterTO);
             WebResource webResource = client.resource(urlResource.toString());
             ClientResponse response = webResource.type("application/json")
                        .post(ClientResponse.class, input);
             if (response.getStatus() != 200) {
                     log.error("error register audit process "+response.getEntity(String.class));
             } 
        	} catch (Exception e) {
            	log.error(e.getMessage());
            	e.printStackTrace();
            }
    }
    
    /**
     * Send notification configurate.
     *
     * @param notificationRegisterTO the notification register to
     */
    public void sendNotificationConfigurate(NotificationRegisterTO notificationRegisterTO){
    	try {
        	StringBuilder urlResource = new StringBuilder();
    		urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
    		 append(serverConfiguration.getProperty("resources.notification.send.path"));	
             Client client = createClientJerser();
             // Creates the json object which will manage the information received 
             GsonBuilder builder = new GsonBuilder(); 
             builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>(){
            	public JsonElement serialize(Date date, Type typeOfT,JsonSerializationContext context) throws JsonParseException{
            		Calendar cal = Calendar.getInstance();
            		cal.setTime(date);
            		return new JsonPrimitive(cal.getTimeInMillis());
            	}
             });  
             Gson gson = builder.create();
             String input = gson.toJson(notificationRegisterTO);
             WebResource webResource = client.resource(urlResource.toString());
             ClientResponse response = webResource.type("application/json")
                        .post(ClientResponse.class, input);
             if (response.getStatus() != 200) {
                     log.error("error send notification manual "+response.getEntity(String.class));
             } 
        	} catch (Exception e) {
            	log.error(e.getMessage());
            	e.printStackTrace();
            }
    }
    
    /**
     * Gets the users information.
     *
     * @param userFilter the user filter
     * @return the users information
     */
    public List<UserAccountSession> getUsersInformation(UserFilterTO userFilter)
    {
       	List<UserAccountSession> accounts = null;
       	try {
       		StringBuilder urlResource = new StringBuilder();
       		urlResource.append(serverPath).append(securityPath);
       		//choose remote method
       		urlResource.append(serverConfiguration.getProperty("resource.users.information.path"));
       		Client client = createClientJerser();
       		WebResource webResource = client.resource(urlResource.toString());
       		 //parameters json
               String input = new Gson().toJson(userFilter);
               String users = webResource.type("application/json").post(String.class, input);
               Type listOfObject = new TypeToken<List<UserAccountSession>>(){}.getType();
               accounts = new Gson().fromJson(users, listOfObject);
       		return accounts;
       	} catch (Exception e) {
           	log.error(e.getMessage());
           	e.printStackTrace();
           	return accounts;
        }
       	
    }
       
	   /**
	* Gets the profiles system.
	*
	* @param system the system
	* @return the profiles system
	*/
	public List<Object[]> getProfilesSystem(Integer system){
	   	List<Object[]> profiles = null;
	   	try {
	     	StringBuilder urlResource = new StringBuilder();
	   		urlResource.append(serverPath).append(securityPath);
	   		//choose remote method
			urlResource.append(serverConfiguration.getProperty("resource.profile.path"));
			Client client = createClientJerser();
			WebResource webResource = client.resource(urlResource.toString());
			StringBuilder parameters = new StringBuilder().
			append(serverConfiguration.getProperty("resource.slash")).
			append(system);
			//parameters json
			String profilesSystem = webResource.path(parameters.toString()).type("application/json").get(String.class);
	   		if(StringUtils.isNotBlank(profilesSystem)){
	   			Type listOfObject = new TypeToken<List<Object[]>>(){}.getType();
	            profiles = new Gson().fromJson(profilesSystem, listOfObject);
	   		}
	   		return profiles;
	   	} catch (Exception e) {
	       	log.error(e.getMessage());
	       	e.printStackTrace();
	       	return profiles;
	   	}
	}
	
	
	/**
	 * Gets the system list.
	 *
	 * @return the system list
	 */
	public List<Object[]> getSystemList(){
		List<Object[]> systems=null;
		try {
			StringBuilder urlResource = new StringBuilder();
	   		urlResource.append(serverPath).append(securityPath);
	   		urlResource.append("/resources/sessionuserconfiguration/systems");
	   		Client client = createClientJerser();
			WebResource webResource = client.resource(urlResource.toString());
			String listSystem = webResource.type("application/json").get(String.class);
	   		if(StringUtils.isNotBlank(listSystem)){
	   			Type listOfObject = new TypeToken<List<Object[]>>(){}.getType();
	   			systems = new Gson().fromJson(listSystem, listOfObject);
	   		}
			return systems;
		} catch (Exception e) {
	       	log.error(e.getMessage());
	       	e.printStackTrace();
	       	return systems;
	   	}
	}
	   
	   /**
   	 * Update Timer related to ProcessSchedule.
   	 *
   	 * @param schedulerInfoGsonObject the scheduler info gson object
   	 * @return the timer
   	 */
	public Timer updateProcessScheduleTimer(String schedulerInfoGsonObject){
		try{
           StringBuilder urlResource = new StringBuilder();
           urlResource.append(serverPath).append(GeneralConstants.SLASH).append(ModuleWarType.PARAMETER.getValue());
           urlResource.append(serverConfiguration.getProperty("resource.scheduler.update.path"));	
	       
	       Client client = createClientJerser();
	       
	       WebResource webResource = client.resource(urlResource.toString());
	       
	       String data = webResource.type(MediaType.APPLICATION_JSON).post(String.class, schedulerInfoGsonObject);
	       
	       return new Gson().fromJson(data, Timer.class);
		} catch (Exception e) {
	   		log.error(e.getMessage());
	   		log.error(" error in object " + schedulerInfoGsonObject);
	    }
		return null;
	}
      
	/**
	 * Execute depositary web resourse.
	 *
	 * @param interfaceFileTO the interface file to
	 * @return the string
	 */
	public String executeDepositaryWebResourse(InterfaceFileTO interfaceFileTO){
		try{
			String localServerPath= "http://192.168.100.241:8080"; //just for local testing (Development)
			if (Validations.validateIsNotNull(serverPath)) {
				localServerPath= serverPath;
			}
			StringBuilder urlResource = new StringBuilder();
		    urlResource.append(localServerPath).append(GeneralConstants.SLASH).append(ModuleWarType.BUSINESS.getValue());
		    urlResource.append(interfaceFileTO.getRelativeServiceUrl());	
		   
		    String xmlFile= new String(interfaceFileTO.getInterfaceFile(),"UTF-8");
		    Client client = createClientJerser();
		    
		    WebResource webResource = client.resource(urlResource.toString());
		    String response = webResource.type(MediaType.TEXT_XML).post(String.class, xmlFile);
		   
		    return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Close user session module.
	 *
	 * @param userName the user name
	 * @param moduleName the module name
	 */
	public void closeUserSessionModule(String userName,String moduleName){
		try{
            StringBuilder urlResource = new StringBuilder();
            urlResource.append(serverPath).append("/").append(moduleName).
            append(serverConfiguration.getProperty("resource.usersession.close.path"));	
            Client client = createClientJerser();
            WebResource webResource = client.resource(urlResource.toString());
            webResource.type(MediaType.TEXT_PLAIN).entity(userName,MediaType.TEXT_PLAIN_TYPE).post();
        } catch (Exception e) {
        	log.error(e.getMessage());
        }
	} 
	
	/**
	 * Update process schedule execution.
	 *
	 * @param processSchedule the process schedule
	 * @param execution the execution
	 */
	public void updateProcessScheduleExecution(Long processSchedule,Integer execution){
		try{
			StringBuilder urlResource = new StringBuilder();
            urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
            append(serverConfiguration.getProperty("resource.scheduler.path")).append("/execution");
            Client client = createClientJerser();
            WebResource webResource = client.resource(urlResource.toString());
            StringBuilder parameters = new StringBuilder().
                append(serverConfiguration.getProperty("resource.slash")).
                append(processSchedule).append(serverConfiguration.getProperty("resource.hyphen")).append(execution);
            webResource.path(parameters.toString()).type(serverConfiguration.getProperty("resource.type")).post();	
		} catch(Exception e){
			log.error(e.getMessage());
		}
	}
	
	/**
	 * Register scheduler.
	 *
	 * @param schedulerInfo the scheduler info
	 */
	public void registerTimer(String schedulerInfo) {
		try{
		   StringBuilder urlResource = new StringBuilder();
			urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
			append(serverConfiguration.getProperty("resource.scheduler.path")).append("/registerTimer");	
	        Client client = createClientJerser();
	        WebResource webResource = client.resource(urlResource.toString());
	        webResource.type(MediaType.APPLICATION_JSON).entity(schedulerInfo,MediaType.APPLICATION_JSON).post();
		} catch (Exception e) {
        	log.error(e.getMessage());
        }
	   }
	
	/**
	 * Delete timer.
	 *
	 * @param processSchedule the process schedule
	 */
	public void deleteTimer(Long processSchedule){
		try{
		   StringBuilder urlResource = new StringBuilder();
			urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
			append(serverConfiguration.getProperty("resource.scheduler.path")).append("/deleteTimer");	
	        Client client = createClientJerser();
	        WebResource webResource = client.resource(urlResource.toString());
	        StringBuilder parameters = new StringBuilder().
	                append(serverConfiguration.getProperty("resource.slash")).
	                append(processSchedule);
	        webResource.path(parameters.toString()).type(MediaType.APPLICATION_JSON).post();
			} catch (Exception e) {
	        	log.error(e.getMessage());
	        }
	}
	
	/**
	 * Gets the process scheduler.
	 *
	 * @param businessProcess the business process
	 * @return the process scheduler
	 */
	public String getProcessScheduler(Long businessProcess){
		try{
		StringBuilder urlResource = new StringBuilder();
		urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
		append(serverConfiguration.getProperty("resource.scheduler.path")).append("/process");	
        Client client = createClientJerser();
        WebResource webResource = client.resource(urlResource.toString());
        StringBuilder parameters = new StringBuilder().
                append(serverConfiguration.getProperty("resource.slash")).
                append(businessProcess);
        return webResource.path(parameters.toString()).type(MediaType.APPLICATION_JSON).get(String.class);
		} catch (Exception e) {
        	log.error(e.getMessage());
        	return null;
        }
	}
	
	/**
	 * Gets the all scheduler.
	 *
	 * @return the all scheduler
	 */
	public String getAllScheduler(){
		try{
			StringBuilder urlResource = new StringBuilder();
			urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
			append(serverConfiguration.getProperty("resource.scheduler.path"));	
	        Client client = createClientJerser();
	        WebResource webResource = client.resource(urlResource.toString());
	        return webResource.type(MediaType.APPLICATION_JSON).get(String.class);
			} catch (Exception e) {
	        	log.error(e.getMessage());
	        	return null;
	        }
	}
	
	/**
	 * Register batch execution.
	 *
	 * @param processSchedule the process schedule
	 */
	public void registerBatchExecution(Long processSchedule){
		try{
		   StringBuilder urlResource = new StringBuilder();
			urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
			append("/resources/singletons").append("/register");	
	        Client client = createClientJerser();
	        WebResource webResource = client.resource(urlResource.toString());
	        StringBuilder parameters = new StringBuilder().
	                append(serverConfiguration.getProperty("resource.slash")).
	                append(System.getProperty("jboss.node.name")+GeneralConstants.BATCH_CACHE_NAME).append(serverConfiguration.getProperty("resource.slash")).
	                append(processSchedule);
	        webResource.path(parameters.toString()).type(MediaType.APPLICATION_JSON).post();
			} catch (Exception e) {
	        	log.error(e.getMessage());
	        }
	}
	
	
	/**
	 * Removes the batch execution.
	 *
	 * @param processSchedule the process schedule
	 */
	public void removeBatchExecution(Long processSchedule){
		try{
		   StringBuilder urlResource = new StringBuilder();
			urlResource.append(serverPath).append("/"+ModuleWarType.PARAMETER.getValue()).
			append("/resources/singletons").append("/remove");	
	        Client client = createClientJerser();
	        WebResource webResource = client.resource(urlResource.toString());
	        StringBuilder parameters = new StringBuilder().
	                append(serverConfiguration.getProperty("resource.slash")).
	                append(System.getProperty("jboss.node.name")+GeneralConstants.BATCH_CACHE_NAME).append(serverConfiguration.getProperty("resource.slash")).
	                append(processSchedule);
	        webResource.path(parameters.toString()).type(MediaType.APPLICATION_JSON).post();
			} catch (Exception e) {
	        	log.error(e.getMessage());
	        }
	}
	
	/**
	 * Gets the system list.
	 *
	 * @return the system list
	 */
	public Long getParticipantIssuerCode(String strIssuerCode){
		Long participantCode = null;
		try {			
			StringBuilder urlResource = new StringBuilder();
		    urlResource.append(serverPath).append(GeneralConstants.SLASH).append(ModuleWarType.BUSINESS.getValue());
		    urlResource.append("/resources/UserResource/issuercode");						
			Client client = createClientJerser();
			WebResource webResource = client.resource(urlResource.toString());
			StringBuilder parameters = new StringBuilder().
			append(serverConfiguration.getProperty("resource.slash")).
			append(strIssuerCode);
			//parameters json
			String strParticipantCode = webResource.path(parameters.toString()).type("application/json").get(String.class);
	   		if(StringUtils.isNotBlank(strParticipantCode)){
	   			Type listOfObject = new TypeToken<Long>(){}.getType();
	   			participantCode = new Gson().fromJson(strParticipantCode, listOfObject);
	   		}	   		
			return participantCode;
		} catch (Exception e) {
	       	log.error(e.getMessage());
	       	e.printStackTrace();
	       	return participantCode;
	   	}
	}
	/**
	 * Validamos usuario mediante web service
	 * 
	 **/
	public boolean validateUser(String loginName,String pass) throws KeyManagementException, NoSuchAlgorithmException {
		  String URL_VERIFY = "/resources/audits/verifyUsernamePassword";
		  String path = "";
		  StringBuilder urlResource = new StringBuilder();
          urlResource.append(serverPath).append(GeneralConstants.SLASH).append(ModuleWarType.SECURITY.getValue());
		  //urlResource.append("http://localhost:8080").append(GeneralConstants.SLASH).append(ModuleWarType.SECURITY.getValue());
		  urlResource.append(URL_VERIFY+"?userName="+loginName+"&pass="+pass);
		  
		  Client client = createClientJerser();
		  WebResource webResource = client.resource(urlResource.toString());
		  //webResource.queryParam("userName", loginName);
		  //webResource.queryParam("pass", pass);
		  String response = webResource.path(path).type("application/json").get(String.class);//login.success
		  if(LoginMessageType.LOGIN_SUCCESS.getValue().equals(response)
		  ||LoginMessageType.PWD_IN_EXP_WARING.getValue().equals(response))    
			  return true;
		  else
			  return false;
	}
	/**
	 * Verificamos si el usuario tiene perfil de supervidor o gerente
	 * */
	public boolean isSupervisorOrManagerProfile(String loginUser){
		String booleanValue = null;
		try {			
			String URL_SPRING_WS = "/resources/audits/isSupervisorOrManagerProfile";
			StringBuilder urlResource = new StringBuilder();
			//urlResource.append("http://localhost:8080").append(GeneralConstants.SLASH).append(ModuleWarType.SECURITY.getValue());
			urlResource.append(serverPath).append(GeneralConstants.SLASH).append(ModuleWarType.SECURITY.getValue());
			urlResource.append(URL_SPRING_WS);	
			
			Client client = createClientJerser();
			WebResource webResource = client.resource(urlResource.toString());
			StringBuilder parameters = new StringBuilder().
					
			append("/").append(loginUser);
			booleanValue = webResource.path(parameters.toString()).type("application/json").get(String.class);
	   		if(StringUtils.isNotBlank(booleanValue))
	   			if(booleanValue.equalsIgnoreCase(Boolean.TRUE.toString()))
	   				return true;
	   			else
	   				return false;
		} catch (Exception e) {
	       	log.error(e.getMessage());
	       	e.printStackTrace();
	   	}
		return false;
	}
	
	/**
	 * Consulta de ip de registro 
	 * Se desencripta el ticker que viene dentro la solicitud
	 * */
	public String getRegistrationIpAudit(String registryUser,String ticket) throws Exception{
		  String QUERY_URL = "/resources/audits/ipRegistrationAudit";
		  String path = "";
		  StringBuilder urlResource = new StringBuilder();
          urlResource.append(serverPath).append(GeneralConstants.SLASH).append(ModuleWarType.SECURITY.getValue());
		  //urlResource.append("http://localhost:8080").append(GeneralConstants.SLASH).append(ModuleWarType.SECURITY.getValue());
		  urlResource.append(QUERY_URL+"?registryUser="+registryUser+"&ticket="+ticket);
		  Client client = createClientJerser();
		  WebResource webResource = client.resource(urlResource.toString());
		  String response = webResource.path(path).type(MediaType.TEXT_PLAIN).get(String.class);
		  return response;
	}
	
	/**
	 * 
	 */
	public XmlRespuestaActivoFinanciero envioSolicitud(XmlSolicitudActivoFinanciero xmlSolicitudActivoFinanciero, String direccion){
		XmlRespuestaActivoFinanciero responseActivoFinanciero = new XmlRespuestaActivoFinanciero();
		try {
			StringWriter xml = new StringWriter();
			JAXBContext jaxbContext;
			jaxbContext = JAXBContext.newInstance(XmlSolicitudActivoFinanciero.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(xmlSolicitudActivoFinanciero, xml);
			
			System.out.println(xml);
			String xmlEnvio = xml.toString();
			
			URL url = new URL(direccion);
			
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/xml");

            OutputStream os = conn.getOutputStream();
            os.write(xmlEnvio.getBytes());
            os.flush();
            
            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED && conn.getResponseCode() != HttpURLConnection.HTTP_ACCEPTED) {
                //Aquí ya se debe personalizar la excepción en caso de no tener respuesta del servidor …… 
	             throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	        }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
//            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
//                  System.out.println("."+output+".");
                  break;
            }
            
            conn.disconnect();
//            System.out.println(".-"+output+".-");
            try {
				jaxbContext = JAXBContext.newInstance(XmlRespuestaActivoFinanciero.class);
	            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
	            StringReader reader = new StringReader(output);
	            responseActivoFinanciero = (XmlRespuestaActivoFinanciero) unmarshaller.unmarshal(reader);
	            System.out.println(responseActivoFinanciero.toString());
            } catch (JAXBException e) {
				e.printStackTrace();
			}
			
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		return responseActivoFinanciero;
	}
	
	public void callLocalWebService(String warModuleName, String path, Object inputObject) {
		try {
			StringBuilder urlResource = new StringBuilder();
			urlResource.append(serverPath).append("/" + warModuleName)
					.append(path);
			Client client = createClientJerser();
			// Creates the json object which will manage the information received
			String input = new Gson().toJson(inputObject);
			WebResource webResource = client.resource(urlResource.toString());
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input);
			if (response.getStatus() != 200) {
				log.error("error send notification manual " + response.getEntity(String.class));
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
}
