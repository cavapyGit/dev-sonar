package com.pradera.commons.sessionuser;

import java.io.Serializable;

import javax.faces.event.ActionEvent;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/05/2013
 */
public class PrivilegeComponent implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6885342572804537437L;

	/** The register. */
	private boolean btnRegisterView;
	
	/** The modify. */
	private boolean btnModifyView;
	
	/** The confirm. */
	private boolean btnConfirmView;
	
	/** The reject. */
	private boolean btnRejectView;
	
	/** The block. */
	private boolean btnBlockView;
	
	/** The unblock. */
	private boolean btnUnblockView;
	
	/** The delete. */
	private boolean btnDeleteView;
	
	/** The search. */
	private boolean btnSearchView;
	
	/** The add. */
	private boolean btnAddView;
	
	/** The Annular. */
	private boolean btnAnnularView;
	
	/** The approve. */
	private boolean btnApproveView;
	
	/** The btn review. */
	private boolean btnReview;
	
	/** The btn apply. */
	private boolean btnApply;
	
	/** The btn authorize. */
	private boolean btnAuthorize;
	
	/** The btn add. */
	private boolean btnAdd;
	
	/** The btn cancel. */
	private boolean btnCancel;
	
	/** The btn settlement. */
	private boolean btnSettlement;
	
	/** The btn activate. */
	private boolean btnActivate;
	
	/** The btn deactivate. */
	private boolean btnDeactivate;
	
	/** The btn confirm depositary. */
	private boolean btnConfirmDepositary;
	
	/** The btn confirm issuer. */
	private boolean btnConfirmIssuer;
	
	/** The btn preliminary. */
	private boolean btnPreliminary;
	
	/** The btn definitive. */
	private boolean btnDefinitive;
	
	/** The btn adjustments. */
	private boolean btnAdjustments;
	
	/** The btn generate. */
	private boolean btnGenerateView;
	
	/** The btn deliver. */
	private boolean btnDeliverView;
	
	private boolean btnAppeal;
	
	private boolean btnCertifyView;
	
	/**
	 * Checks if is btn apply.
	 *
	 * @return true, if is btn apply
	 */
	public boolean isBtnApply() {
		return btnApply;
	}




	/**
	 * Sets the btn apply.
	 *
	 * @param btnApply the new btn apply
	 */
	public void setBtnApply(boolean btnApply) {
		this.btnApply = btnApply;
	}




	/**
	 * Checks if is btn authorize.
	 *
	 * @return true, if is btn authorize
	 */
	public boolean isBtnAuthorize() {
		return btnAuthorize;
	}




	/**
	 * Sets the btn authorize.
	 *
	 * @param btnAuthorize the new btn authorize
	 */
	public void setBtnAuthorize(boolean btnAuthorize) {
		this.btnAuthorize = btnAuthorize;
	}
			
	/**
     * Instantiates a new privilege component.
     */
    public PrivilegeComponent() {
		// TODO Auto-generated constructor stub
	}

	

	
	/**
	 * Checks if is btn register view.
	 *
	 * @return true, if is btn register view
	 */
	public boolean isBtnRegisterView() {
		return btnRegisterView;
	}




	/**
	 * Sets the btn register view.
	 *
	 * @param btnRegisterView the new btn register view
	 */
	public void setBtnRegisterView(boolean btnRegisterView) {
		this.btnRegisterView = btnRegisterView;
	}




	/**
	 * Checks if is btn modify view.
	 *
	 * @return true, if is btn modify view
	 */
	public boolean isBtnModifyView() {
		return btnModifyView;
	}




	/**
	 * Sets the btn modify view.
	 *
	 * @param btnModifyView the new btn modify view
	 */
	public void setBtnModifyView(boolean btnModifyView) {
		this.btnModifyView = btnModifyView;
	}




	/**
	 * Checks if is btn confirm view.
	 *
	 * @return true, if is btn confirm view
	 */
	public boolean isBtnConfirmView() {
		return btnConfirmView;
	}




	/**
	 * Sets the btn confirm view.
	 *
	 * @param btnConfirmView the new btn confirm view
	 */
	public void setBtnConfirmView(boolean btnConfirmView) {
		this.btnConfirmView = btnConfirmView;
	}




	/**
	 * Checks if is btn reject view.
	 *
	 * @return true, if is btn reject view
	 */
	public boolean isBtnRejectView() {
		return btnRejectView;
	}




	/**
	 * Sets the btn reject view.
	 *
	 * @param btnRejectView the new btn reject view
	 */
	public void setBtnRejectView(boolean btnRejectView) {
		this.btnRejectView = btnRejectView;
	}




	/**
	 * Checks if is btn block view.
	 *
	 * @return true, if is btn block view
	 */
	public boolean isBtnBlockView() {
		return btnBlockView;
	}




	/**
	 * Sets the btn block view.
	 *
	 * @param btnBlockView the new btn block view
	 */
	public void setBtnBlockView(boolean btnBlockView) {
		this.btnBlockView = btnBlockView;
	}




	/**
	 * Checks if is btn unblock view.
	 *
	 * @return true, if is btn unblock view
	 */
	public boolean isBtnUnblockView() {
		return btnUnblockView;
	}




	/**
	 * Sets the btn unblock view.
	 *
	 * @param btnUnblockView the new btn unblock view
	 */
	public void setBtnUnblockView(boolean btnUnblockView) {
		this.btnUnblockView = btnUnblockView;
	}




	/**
	 * Checks if is btn delete view.
	 *
	 * @return true, if is btn delete view
	 */
	public boolean isBtnDeleteView() {
		return btnDeleteView;
	}




	/**
	 * Sets the btn delete view.
	 *
	 * @param btnDeleteView the new btn delete view
	 */
	public void setBtnDeleteView(boolean btnDeleteView) {
		this.btnDeleteView = btnDeleteView;
	}




	/**
	 * Checks if is btn search view.
	 *
	 * @return true, if is btn search view
	 */
	public boolean isBtnSearchView() {
		return btnSearchView;
	}




	/**
	 * Sets the btn search view.
	 *
	 * @param btnSearchView the new btn search view
	 */
	public void setBtnSearchView(boolean btnSearchView) {
		this.btnSearchView = btnSearchView;
	}




	/**
	 * Checks if is btn add view.
	 *
	 * @return true, if is btn add view
	 */
	public boolean isBtnAddView() {
		return btnAddView;
	}




	/**
	 * Sets the btn add view.
	 *
	 * @param btnAddView the new btn add view
	 */
	public void setBtnAddView(boolean btnAddView) {
		this.btnAddView = btnAddView;
	}




	/**
	 * Checks if is btn annular view.
	 *
	 * @return true, if is btn annular view
	 */
	public boolean isBtnAnnularView() {
		return btnAnnularView;
	}




	/**
	 * Sets the btn annular view.
	 *
	 * @param btnAnnularView the new btn annular view
	 */
	public void setBtnAnnularView(boolean btnAnnularView) {
		this.btnAnnularView = btnAnnularView;
	}




	/**
	 * Checks if is btn approve view.
	 *
	 * @return true, if is btn approve view
	 */
	public boolean isBtnApproveView() {
		return btnApproveView;
	}




	/**
	 * Sets the btn approve view.
	 *
	 * @param btnApproveView the new btn approve view
	 */
	public void setBtnApproveView(boolean btnApproveView) {
		this.btnApproveView = btnApproveView;
	}

	
	/**
	 * Method listener default.
	 *
	 * @param event the event
	 */
	public void methodListenerDefault(ActionEvent event){
	 
	}
	
	/**
	 * Method action default.
	 *
	 * @return the string
	 */
	public String methodActionDefault(){
		return "";
	}




	/**
	 * Checks if is btn review.
	 *
	 * @return true, if is btn review
	 */
	public boolean isBtnReview() {
		return btnReview;
	}




	/**
	 * Sets the btn review.
	 *
	 * @param btnReview the new btn review
	 */
	public void setBtnReview(boolean btnReview) {
		this.btnReview = btnReview;
	}




	/**
	 * Checks if is btn add.
	 *
	 * @return the btnAdd
	 */
	public boolean isBtnAdd() {
		return btnAdd;
	}




	/**
	 * Sets the btn add.
	 *
	 * @param btnAdd the btnAdd to set
	 */
	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}


	/**
	 * Checks if is btn cancel.
	 *
	 * @return true, if is btn cancel
	 */
	public boolean isBtnCancel() {
		return btnCancel;
	}


	/**
	 * Sets the btn cancel.
	 *
	 * @param btnCancel the new btn cancel
	 */
	public void setBtnCancel(boolean btnCancel) {
		this.btnCancel = btnCancel;
	}


	/**
	 * Checks if is btn settlement.
	 *
	 * @return true, if is btn settlement
	 */
	public boolean isBtnSettlement() {
		return btnSettlement;
	}


	/**
	 * Sets the btn settlement.
	 *
	 * @param btnSettlement the new btn settlement
	 */
	public void setBtnSettlement(boolean btnSettlement) {
		this.btnSettlement = btnSettlement;
	}
	
	
	/**
	 * Checks if is btn activate.
	 *
	 * @return true, if is btn activate
	 */
	public boolean isBtnActivate() {
		return btnActivate;
	}


	/**
	 * Sets the btn activate.
	 *
	 * @param btnActivate the new btn activate
	 */
	public void setBtnActivate(boolean btnActivate) {
		this.btnActivate = btnActivate;
	}


	/**
	 * Checks if is btn deactivate.
	 *
	 * @return true, if is btn deactivate
	 */
	public boolean isBtnDeactivate() {
		return btnDeactivate;
	}

	/**
	 * Sets the btn deactivate.
	 *
	 * @param btnDeactivate the new btn deactivate
	 */
	public void setBtnDeactivate(boolean btnDeactivate) {
		this.btnDeactivate = btnDeactivate;
	}




	/**
	 * Checks if is btn confirm depositary.
	 *
	 * @return true, if is btn confirm depositary
	 */
	public boolean isBtnConfirmDepositary() {
		return btnConfirmDepositary;
	}




	/**
	 * Sets the btn confirm depositary.
	 *
	 * @param btnConfirmDepositary the new btn confirm depositary
	 */
	public void setBtnConfirmDepositary(boolean btnConfirmDepositary) {
		this.btnConfirmDepositary = btnConfirmDepositary;
	}




	/**
	 * Checks if is btn confirm issuer.
	 *
	 * @return true, if is btn confirm issuer
	 */
	public boolean isBtnConfirmIssuer() {
		return btnConfirmIssuer;
	}




	/**
	 * Sets the btn confirm issuer.
	 *
	 * @param btnConfirmIssuer the new btn confirm issuer
	 */
	public void setBtnConfirmIssuer(boolean btnConfirmIssuer) {
		this.btnConfirmIssuer = btnConfirmIssuer;
	}




	/**
	 * Checks if is btn preliminary.
	 *
	 * @return true, if is btn preliminary
	 */
	public boolean isBtnPreliminary() {
		return btnPreliminary;
	}




	/**
	 * Sets the btn preliminary.
	 *
	 * @param btnPreliminary the new btn preliminary
	 */
	public void setBtnPreliminary(boolean btnPreliminary) {
		this.btnPreliminary = btnPreliminary;
	}




	/**
	 * Checks if is btn definitive.
	 *
	 * @return true, if is btn definitive
	 */
	public boolean isBtnDefinitive() {
		return btnDefinitive;
	}




	/**
	 * Sets the btn definitive.
	 *
	 * @param btnDefinitive the new btn definitive
	 */
	public void setBtnDefinitive(boolean btnDefinitive) {
		this.btnDefinitive = btnDefinitive;
	}




	/**
	 * Checks if is btn adjustments.
	 *
	 * @return true, if is btn adjustments
	 */
	public boolean isBtnAdjustments() {
		return btnAdjustments;
	}




	/**
	 * Sets the btn adjustments.
	 *
	 * @param btnAdjustments the new btn adjustments
	 */
	public void setBtnAdjustments(boolean btnAdjustments) {
		this.btnAdjustments = btnAdjustments;
	}

	public boolean isBtnGenerateView() {
		return btnGenerateView;
	}




	public void setBtnGenerateView(boolean btnGenerateView) {
		this.btnGenerateView = btnGenerateView;
	}




	public boolean isBtnDeliverView() {
		return btnDeliverView;
	}




	public void setBtnDeliverView(boolean btnDeliverView) {
		this.btnDeliverView = btnDeliverView;
	}




	public boolean isBtnAppeal() {
		return btnAppeal;
	}




	public void setBtnAppeal(boolean btnAppeal) {
		this.btnAppeal = btnAppeal;
	}




	public boolean isBtnCertifyView() {
		return btnCertifyView;
	}




	public void setBtnCertifyView(boolean btnCertifyView) {
		this.btnCertifyView = btnCertifyView;
	}
	
	

	
}
