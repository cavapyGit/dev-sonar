package com.pradera.commons.sessionuser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.primefaces.model.menu.MenuModel;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.menu.PraderaMenu;
import com.pradera.commons.menu.MenuProducer;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.integration.usersession.UserAcctions;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserInfo.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/02/2013
 */
@SessionScoped
@Named
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class UserInfo implements Serializable {

	/** The Constant serialVersionUID. */
	@JsonIgnore
	private static final long serialVersionUID = -7681643176529439660L;
	
	/** The usuario cuenta. */
	private UserAccountSession userAccountSession;
	
	/** The privilegios opcion. */
	private Map<String, UserAcctions> optionPrivileges;
	
	/** The user agencies. */
	private Map<String, Integer> userAgencies; 
	
	/** The idioma actual. */
	private String currentLanguage;
	
	/** The pradera menu total. */
	private PraderaMenu praderaMenuAll;
	
	private Integer currentSystemId;
	
	/** The sistema actual. */
	private String currentSystem;
	
	/** The end time connection. */
	private Date endTimeConnection;
	
	/** The last modify priv. */
	private Integer lastModifyPriv = Integer.valueOf(1);
	
	/** The user acctions. */
	private UserAcctions userAcctions;
	
	/** The availiable options url. */
	private List<String> availiableOptionsUrl;
	
	/** The visited urls. */
	private List<String> visitedUrls;	
	
	/** The current year. */
	private Integer currentYear;
	
	/** The time zone. */
	private transient TimeZone timeZone;
	
	/** The logout motive. */
	private LogoutMotiveType logoutMotive;
	
	/** The privilege component. */
	@Inject
	private PrivilegeComponent privilegeComponent;
	
	@JsonIgnore
	@Inject
	private MenuProducer menuProducer;
	
	
	/** The pwd must change. */
	@XmlTransient
	private transient boolean pwdMustChange;
	
	@Inject
	@Configurable
	private Integer systemSAB;
	
	@Inject
	@Configurable
	private Integer systemTITU;
	
	@Inject
	@Configurable
	private Integer systemSAFI;
	
	@Inject
	@Configurable
	private String growlLife;
	
	private MenuModel menu;
	
	
	/**
	 * Instantiates a new usuario info.
	 */
	public UserInfo() {
		userAccountSession = new UserAccountSession();
		visitedUrls = new ArrayList<String>();
		optionPrivileges = new HashMap<String, UserAcctions>();
		userAgencies = new HashMap<String, Integer>();
		currentYear = CommonsUtilities.currentYear();
		timeZone = TimeZone.getDefault();
	}

	/**
	 * Gets the usuario cuenta.
	 *
	 * @return the usuario cuenta
	 */
	public UserAccountSession getUserAccountSession() {
		return userAccountSession;
	}

	/**
	 * Gets the user acctions.
	 *
	 * @return the user acctions
	 */
	public UserAcctions getUserAcctions() {
		return userAcctions;
	}

	/**
	 * Sets the user acctions.
	 *
	 * @param userAcctions the new user acctions
	 */
	public void setUserAcctions(UserAcctions userAcctions) {
		this.userAcctions=userAcctions;
	}

	/**
	 * Sets the usuario cuenta.
	 *
	 * @param usuarioCuenta the new usuario cuenta
	 */
	public void setUserAccountSession(UserAccountSession usuarioCuenta) {
		this.userAccountSession = usuarioCuenta;
	}

	/**
	 * Gets the idioma actual.
	 *
	 * @return the idioma actual
	 */
	public String getCurrentLanguage() {
		return currentLanguage;
	}

	/**
	 * Sets the idioma actual.
	 *
	 * @param idiomaActual the new idioma actual
	 */
	public void setCurrentLanguage(String idiomaActual) {
		this.currentLanguage = idiomaActual;
	}

	/**
	 * Gets the menu.
	 *
	 * @return the menu
	 */
	public MenuModel getMenu() {
		return menuProducer.showMenu(this, this.currentLanguage, FacesContext.getCurrentInstance().getExternalContext()
				.getInitParameter("IDEPOSITARY_MODULE"));
	}

	/**
	 * Gets the pradera menu total.
	 *
	 * @return the pradera menu total
	 */
	public PraderaMenu getPraderaMenuAll() {
		return praderaMenuAll;
	}

	/**
	 * Sets the pradera menu total.
	 *
	 * @param praderaMenuTotal the new pradera menu total
	 */
	public void setPraderaMenuAll(PraderaMenu praderaMenuTotal) {
		this.praderaMenuAll = praderaMenuTotal;
	}

	/**
	 * Gets the sistema actual.
	 *
	 * @return the sistema actual
	 */
	public String getCurrentSystem() {
		return currentSystem;
	}

	/**
	 * Sets the sistema actual.
	 *
	 * @param sistemaActual the new sistema actual
	 */
	public void setCurrentSystem(String sistemaActual) {
		this.currentSystem = sistemaActual;
	}

	/**
	 * Gets the option privileges.
	 *
	 * @return the option privileges
	 */
	public Map<String, UserAcctions> getOptionPrivileges() {
		return optionPrivileges;
	}

	/**
	 * Sets the option privileges.
	 *
	 * @param optionPrivileges the option privileges
	 */
	public void setOptionPrivileges(Map<String, UserAcctions> optionPrivileges) {
		this.optionPrivileges = optionPrivileges;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the end time connection.
	 *
	 * @return the end time connection
	 */
	public Date getEndTimeConnection() {
		return endTimeConnection;
	}

	/**
	 * Sets the end time connection.
	 *
	 * @param endTimeConnection the new end time connection
	 */
	public void setEndTimeConnection(Date endTimeConnection) {
		this.endTimeConnection = endTimeConnection;
	}

	/**
	 * Gets the availiable options url.
	 *
	 * @return the availiable options url
	 */
	public List<String> getAvailiableOptionsUrl() {
		return availiableOptionsUrl;
	}

	/**
	 * Sets the availiable options url.
	 *
	 * @param availiableOptionsUrl the new availiable options url
	 */
	public void setAvailiableOptionsUrl(List<String> availiableOptionsUrl) {
		this.availiableOptionsUrl = availiableOptionsUrl;
	}

	/**
	 * Gets the visited urls.
	 *
	 * @return the visited urls
	 */
	public List<String> getVisitedUrls() {
		return visitedUrls;
	}

	/**
	 * Sets the visited urls.
	 *
	 * @param visitedUrls the new visited urls
	 */
	public void setVisitedUrls(List<String> visitedUrls) {
		this.visitedUrls = visitedUrls;
	}

	/**
	 * Gets the current year.
	 *
	 * @return the current year
	 */
	public Integer getCurrentYear() {
		return currentYear;
	}

	/**
	 * Sets the current year.
	 *
	 * @param currentYear the new current year
	 */
	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}

	/**
	 * Gets the time zone.
	 *
	 * @return the time zone
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}

	/**
	 * Sets the time zone.
	 *
	 * @param timeZone the new time zone
	 */
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}	
	
	/**
	 * Gets the logout motive.
	 *
	 * @return the logout motive
	 */
	public LogoutMotiveType getLogoutMotive() {
		return logoutMotive;
	}

	/**
	 * Sets the logout motive.
	 *
	 * @param logoutMotive the new logout motive
	 */
	public void setLogoutMotive(LogoutMotiveType logoutMotive) {
		this.logoutMotive = logoutMotive;
	}
	

	/**
	 * Gets the pwdMustChange.
	 *
	 * @return the pwdMustChange
	 */
	public boolean isPwdMustChange() {
		return pwdMustChange;
	}

	/**
	 * Sets the pwdMustChange.
	 *
	 * @param pwdMustChange the new pwd must change
	 */
	public void setPwdMustChange(boolean pwdMustChange) {
		this.pwdMustChange = pwdMustChange;
	}

	/**
	 * Gets the privilege component.
	 *
	 * @return the privilege component
	 */
	public PrivilegeComponent getPrivilegeComponent() {
		return privilegeComponent;
	}

	/**
	 * Sets the privilege component.
	 *
	 * @param privilegeComponent the new privilege component
	 */
	public void setPrivilegeComponent(PrivilegeComponent privilegeComponent) {
		this.privilegeComponent = privilegeComponent;
	}

	/**
	 * Session logout.
	 */
	public void sessionLogout(){
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,null, null,GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,GeneralPropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
	}

	/**
	 * Gets the user agencies.
	 *
	 * @return the user agencies
	 */
	public Map<String, Integer> getUserAgencies() {
		return userAgencies;
	}

	/**
	 * Sets the user agencies.
	 *
	 * @param userAgencies the user agencies
	 */
	public void setUserAgencies(Map<String, Integer> userAgencies) {
		this.userAgencies = userAgencies;
	}

	public void setMenu(MenuModel menu) {
		this.menu = menu;
	}

	public Integer getCurrentSystemId() {
		return currentSystemId;
	}

	public void setCurrentSystemId(Integer currentSystemId) {
		this.currentSystemId = currentSystemId;
	}
	
	@JsonIgnore
	public boolean isSystemSAB() {
		if(this.currentSystemId==null) {
			return false;
		}
		return this.systemSAB.equals( this.currentSystemId );
	}
	
	@JsonIgnore
	public boolean isSystemTITU() {
		if(this.currentSystemId==null) {
			return false;
		}
		return this.systemTITU.equals( this.currentSystemId );
	}
	
	@JsonIgnore
	public boolean isSystemSAFI() {
		if(this.currentSystemId==null) {
			return false;
		}
		return this.systemSAFI.equals( this.currentSystemId );
	}

	public String getGrowlLife() {
		return growlLife;
	}

	public void setGrowlLife(String growlLife) {
		this.growlLife = growlLife;
	}
	
	
	
}