package com.pradera.commons.sessionuser;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.security.model.type.UserAccountResponsabilityType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.integration.common.type.BooleanType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserAccountSession.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/02/2013
 */
public class UserAccountSession implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3947536512858828538L;
	
	/** The nombre completo. */
	private String fullName;
	
	/** The nombre usuario. */
	private String userName;
	
	/** The correo electronico. */
	private String email;
	
	/** The estado. */
	private Integer state;
	
	/** The nombre institucion usuario. */
	private String userInstitutionName;
	
	/** The ip address. */
	private String ipAddress;
	
	/** The id user account pk. */
	private Integer idUserAccountPk;
	
	/** The last selected privilege pk. */
	private Integer lastActuatedPrivilegePk;
	
	/** The id user session pk. */
	private Long idUserSessionPk;
	
	/** The ticket session. */
	private String ticketSession;
	
	/** The last sucess access. */
	private Date lastSucessAccess;
	
	/** The institution type. */
	private Integer institutionType; 
	
	/** The issuer code. */
	private String issuerCode;
	
	/** The participant code. */
	private Long participantCode;
	
	private Integer institutionCode;
	
	/** The institution state. handle State usser Like Participant Or Issuer*/
	private Integer institutionState;
	
	/** The regulator agent. */
	private Integer regulatorAgent;
	
	/** The responsability. */
	private Integer responsability;
	
	/** The phone number. */
	private String phoneNumber;
	
	/** The count User. */
	private Integer countUser;
	
	/** The document number. */
	private String documentNumber;

	/** The document type. */
	private Integer documentType;
	
	/** The Initial Date. */
	private Date registerDate;
	
	/** The id subsidiary pk. */
	private Long idSubsidiaryPk;
	
	/** The department. */
	private Integer department;
	
	/** The description institution type. */
	private String descInstitutionType;
	
	/** The participant issuer code. */
	private Long partIssuerCode;
	
	private Integer isOperator;
	
	private String descUserType;
	
	public UserAccountSession() {
		this.institutionType = InstitutionType.PARTICIPANT.getCode();
	}
	
	
	public String getDescUserType() {
		return descUserType;
	}

	public void setDescUserType(String descUserType) {
		this.descUserType = descUserType;
	}

	/**
	 * Checks if is total responsability.
	 *
	 * @return true, if is total responsability
	 */
	public boolean isTotalResponsability() {
		boolean result = false;
		if(UserAccountResponsabilityType.TOTAL.getCode().equals(responsability)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Checks if user is regular agent.
	 *
	 * @return true, if is regular agent
	 */
	public boolean isUserRegulatorAgent(){
		boolean result = false;
		if(BooleanType.YES.getCode().equals(this.regulatorAgent)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Checks if is depositary institution.
	 *
	 * @return true, if is depositary institution
	 */
	public boolean isDepositaryInstitution(){
		return InstitutionType.DEPOSITARY.getCode().equals(this.institutionType);
	}
	
	
	/**
	 * Checks if the user is participant institucion.
	 *
	 * @return true, if is participant institucion
	 */
	public boolean isParticipantInstitucion() {
		boolean result = false;
		if (InstitutionType.PARTICIPANT.getCode().equals(this.institutionType) ||
				InstitutionType.AFP.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Checks if the user is participant investor institucion.
	 *
	 * @return true, if is participant investor institucion
	 */
	public boolean isParticipantInvestorInstitucion() {
		boolean result = false;
		if (InstitutionType.PARTICIPANT_INVESTOR.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Checks if the user is issuer dpf institucion.
	 *
	 * @return true, if is issuer dpf institucion
	 */
	public boolean isIssuerDpfInstitucion() {
		boolean result = false;
		if (InstitutionType.ISSUER_DPF.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	/**
		 * Checks if is participant BCB institution.
	 *
	 * @return true, if is BCB institution
	 */
	public boolean isBcbInstitution(){
		boolean result = false;
		if (participantCode != null && participantCode.equals(new Long(114))) {
			result = true;
		}
		return result;
	}	
	/** 
		 * Checks if is afp institution.
	 *
	 * @return true, if is afp institution
	 */
	public boolean isAfpInstitution(){
		boolean result = false;
		if (InstitutionType.AFP.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	/**
	 * Checks if the user is issuer institucion.
	 *
	 * @return true, if is issuer institucion
	 */
	public boolean isIssuerInstitucion() {
		boolean result = false;
		if (InstitutionType.ISSUER.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	
	public boolean isStockExchange() {
		boolean result = false;
		if (InstitutionType.BOLSA.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	
	public boolean isFinanceMinistry() {
		boolean result = false;
		if (InstitutionType.ASFI.getCode().equals(this.institutionType)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Checks if the user is a institution particular in parameter.
	 *
	 * @param institutionTypeParameter the institution type parameter
	 * @return true, if is institution
	 */
	public boolean isInstitution(InstitutionType institutionTypeParameter) {
		boolean result = false;
		InstitutionType currentInstitution = InstitutionType.get(this.institutionType);
		if (institutionTypeParameter.getCode().equals(currentInstitution.getCode())) {
			result = true;
		}
		return result;
	}
	
	
		/**
	 * Gets the ip address.
	 *
	 * @return the ip address
	 */ 
	public String getIpAddress() {
		return ipAddress;
	} 

	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the new ip address
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * Gets the nombre completo.
	 *
	 * @return the nombre completo
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the nombre completo.
	 *
	 * @param nombreCompleto the new nombre completo
	 */
	public void setFullName(String nombreCompleto) {
		this.fullName = nombreCompleto;
	}

	/**
	 * Gets the nombre usuario.
	 *
	 * @return the nombre usuario
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the nombre usuario.
	 *
	 * @param nombreUsuario the new nombre usuario
	 */
	public void setUserName(String nombreUsuario) {
		this.userName = nombreUsuario;
	}

	/**
	 * Gets the correo electronico.
	 *
	 * @return the correo electronico
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the correo electronico.
	 *
	 * @param correoElectronico the new correo electronico
	 */
	public void setEmail(String correoElectronico) {
		this.email = correoElectronico;
	}

	/**
	 * Gets the estado.
	 *
	 * @return the estado
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the estado.
	 *
	 * @param estado the new estado
	 */
	public void setState(Integer estado) {
		this.state = estado;
	}

	/**
	 * Gets the nombre institucion usuario.
	 *
	 * @return the nombre institucion usuario
	 */
	public String getUserInstitutionName() {
		return userInstitutionName;
	}

	/**
	 * Sets the nombre institucion usuario.
	 *
	 * @param nombreInstitucionUsuario the new nombre institucion usuario
	 */
	public void setUserInstitutionName(String nombreInstitucionUsuario) {
		this.userInstitutionName = nombreInstitucionUsuario;
	}
	
	
	
	/**
	 * Gets the last actuated privilege pk.
	 *
	 * @return the last actuated privilege pk
	 */
	public Integer getLastActuatedPrivilegePk() {
		return lastActuatedPrivilegePk;
	}

	/**
	 * Sets the last actuated privilege pk.
	 *
	 * @param lastActuatedPrivilegePk the new last actuated privilege pk
	 */
	public void setLastActuatedPrivilegePk(Integer lastActuatedPrivilegePk) {
		this.lastActuatedPrivilegePk = lastActuatedPrivilegePk;
	}

	/**
	 * Gets the id user account pk.
	 *
	 * @return the id user account pk
	 */
	public Integer getIdUserAccountPk() {
		return idUserAccountPk;
	}

	/**
	 * Sets the id user account pk.
	 *
	 * @param idUserAccountPk the new id user account pk
	 */
	public void setIdUserAccountPk(Integer idUserAccountPk) {
		this.idUserAccountPk = idUserAccountPk;
	}

	/**
	 * Gets the id user session pk.
	 *
	 * @return the id user session pk
	 */
	public Long getIdUserSessionPk() {
		return idUserSessionPk;
	}

	/**
	 * Sets the id user session pk.
	 *
	 * @param idUserSessionPk the new id user session pk
	 */
	public void setIdUserSessionPk(Long idUserSessionPk) {
		this.idUserSessionPk = idUserSessionPk;
	}

	/**
	 * Gets the ticket session.
	 *
	 * @return the ticket session
	 */
	public String getTicketSession() {
		return ticketSession;
	}

	/**
	 * Sets the ticket session.
	 *
	 * @param ticketSession the new ticket session
	 */
	public void setTicketSession(String ticketSession) {
		this.ticketSession = ticketSession;
	}

	/**
	 * Gets the last sucess access.
	 *
	 * @return the last sucess access
	 */
	public Date getLastSucessAccess() {
		return lastSucessAccess;
	}

	/**
	 * Sets the last sucess access.
	 *
	 * @param lastSucessAccess the new last sucess access
	 */
	public void setLastSucessAccess(Date lastSucessAccess) {
		this.lastSucessAccess = lastSucessAccess;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}

	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	/**
	 * Gets the participant code.
	 *
	 * @return the participant code
	 */
	public Long getParticipantCode() {
		return participantCode;
	}

	/**
	 * Sets the participant code.
	 *
	 * @param participantCode the new participant code
	 */
	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}


	/**
	 * Sets the regulator agent.
	 *
	 * @param regulatorAgent the new regulator agent
	 */
	public void setRegulatorAgent(Integer regulatorAgent) {
		this.regulatorAgent = regulatorAgent;
	}


	/**
	 * Gets the regulator agent.
	 *
	 * @return the regulator agent
	 */
	public Integer getRegulatorAgent() {
		return regulatorAgent;
	}

	/**
	 * Gets the responsability.
	 *
	 * @return the responsability
	 */
	public Integer getResponsability() {
		return responsability;
	}

	/**
	 * Sets the responsability.
	 *
	 * @param responsability the new responsability
	 */
	public void setResponsability(Integer responsability) {
		this.responsability = responsability;
	}

	/**
	 * Gets the institution state.
	 *
	 * @return the institution state
	 */
	public Integer getInstitutionState() {
		return institutionState;
	}

	/**
	 * Sets the institution state.
	 *
	 * @param institutionState the new institution state
	 */
	public void setInstitutionState(Integer institutionState) {
		this.institutionState = institutionState;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAccountSession other = (UserAccountSession) obj;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * Gets the count user.
	 *
	 * @return the count user
	 */
	public Integer getCountUser() {
		return countUser;
	}
	/**
	 * Sets the count user.
	 *
	 * @param countUser the new count user
	 */
	public void setCountUser(Integer countUser) {
		this.countUser = countUser;
	}

	/**
	 * Gets the Document Number.
	 *
	 * @return the Document Number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	/**
	 * Sets the Document Number.
	 *
	 * @param documentNumber the new  Document Number.
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	/**
	 * Gets the  document type
	 *
	 * @return the  document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}
	/**
	 * Sets the document type
	 *
	 * @param documentType the new  document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(Integer institutionCode) {
		this.institutionCode = institutionCode;
	}

	/**
	 * @return the idSubsidiaryPk
	 */
	public Long getIdSubsidiaryPk() {
		return idSubsidiaryPk;
	}

	/**
	 * @param idSubsidiaryPk the idSubsidiaryPk to set
	 */
	public void setIdSubsidiaryPk(Long idSubsidiaryPk) {
		this.idSubsidiaryPk = idSubsidiaryPk;
	}

	/**
	 * @return the descInstitutionType
	 */
	public String getDescInstitutionType() {
		return descInstitutionType;
	}

	/**
	 * @param descInstitutionType the descInstitutionType to set
	 */
	public void setDescInstitutionType(String descInstitutionType) {
		this.descInstitutionType = descInstitutionType;
	}

	/**
	 * @return the partIssuerCode
	 */
	public Long getPartIssuerCode() {
		return partIssuerCode;
	}

	/**
	 * @param partIssuerCode the partIssuerCode to set
	 */
	public void setPartIssuerCode(Long partIssuerCode) {
		this.partIssuerCode = partIssuerCode;
	}

	/**
	 * @return the department
	 */
	public Integer getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Integer department) {
		this.department = department;
	}

	/**
	 * @return the isOperator
	 */
	public Integer getIsOperator() {
		return isOperator;
	}

	/**
	 * @param isOperator the isOperator to set
	 */
	public void setIsOperator(Integer isOperator) {
		this.isOperator = isOperator;
	}

	
	
}