package com.pradera.commons.sessionuser;

import java.io.IOException;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.menu.MenuProducer;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserValidationServlet.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/02/2013
 */
@WebServlet(name="UserValidationServlet",urlPatterns = {"/userValidation"})
public class UserValidationServlet extends HttpServlet {
	
	/** The client rest service. */
	@Inject
	private ClientRestService clientRestService;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private MenuProducer menuProducer;
	
	/** The ticketid. */
	private String TICKETID = "tikectId";
	
	/** The module. */
	private String MODULE = "IDEPOSITARY_MODULE";
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8279656576756737088L;

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * Process request.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*back from Security later login
		 * so when ticket we will validate and return menu of system
		 */
		String ticket = request.getParameter(TICKETID);
		String moduleApp = request.getServletContext().getInitParameter(MODULE);
		if (StringUtils.isNotBlank(ticket)) {
			UserInfo userInfoRemote = clientRestService.findUserInfoBySessionId(ticket, moduleApp);
			if (userInfoRemote != null) {
				userInfo.setUserAccountSession(userInfoRemote.getUserAccountSession());
				userInfo.setPraderaMenuAll(userInfoRemote.getPraderaMenuAll());
				userInfo.setAvailiableOptionsUrl(userInfoRemote.getAvailiableOptionsUrl());
				userInfo.setUserAgencies(userInfoRemote.getUserAgencies());								
				//Only get privileges not MenuModel
				menuProducer.createPrivilegeUser(userInfo);
				userInfo.setCurrentSystem(userInfoRemote.getCurrentSystem());
				userInfo.setCurrentLanguage(userInfoRemote.getCurrentLanguage());
				userInfo.setTimeZone(TimeZone.getDefault());
				//TODO replace hard code in future
				response.sendRedirect("/"+moduleApp+"/pages/welcome.xhtml");
				
			} else {
				//error not match ticket id
				response.sendRedirect("/"+moduleApp+"/error/privilegesError.xhtml");
			}
		} else {
			//error not have ticket
			response.sendRedirect("/"+moduleApp+"/error/privilegesError.xhtml");
		}
	}
}