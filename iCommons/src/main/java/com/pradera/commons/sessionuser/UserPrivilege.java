package com.pradera.commons.sessionuser;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.WindowScoped;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.usersession.UserAcctions;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UserPrivilege.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26-ene-2015
 */
@Named
@WindowScoped
public class UserPrivilege implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	@Inject
	transient PraderaLogger log;
	
	/** The user acctions. */
	private UserAcctions userAcctions;
	
	@Inject
	private PrivilegeComponent privilegeComponent;

	/**
	 * Instantiates a new user privilege.
	 */
	public UserPrivilege() {
		
	}
	
	@PostConstruct
	public void init(){
		this.userAcctions = new UserAcctions();
		log.info("Creando bean en "+CommonsUtilities.currentTime());
	}

	/**
	 * Gets the user acctions.
	 *
	 * @return the user acctions
	 */
	public UserAcctions getUserAcctions() {
		return userAcctions;
	}

	/**
	 * Sets the user acctions.
	 *
	 * @param userAcctions the new user acctions
	 */
	public void setUserAcctions(UserAcctions userAcctions) {
		this.userAcctions = userAcctions;
	}

	public PrivilegeComponent getPrivilegeComponent() {
		return privilegeComponent;
	}

	public void setPrivilegeComponent(PrivilegeComponent privilegeComponent) {
		this.privilegeComponent = privilegeComponent;
	}

}
