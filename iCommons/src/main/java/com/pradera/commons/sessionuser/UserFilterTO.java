package com.pradera.commons.sessionuser;

import java.io.Serializable;

import com.google.gson.Gson;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserFilterTO.
 * using for get user accounts from remote applications
 * @author PraderaTechnologies.
 * @version 1.0 , 26/07/2013
 */
public class UserFilterTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3867452057872674465L;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** Profile user*/
	
	private Integer idSystemProfilePk;
	
	/** The responsibility. */
	private Integer responsibility;
	
	/** The state. */
	private Integer state;
	
	/** The entity code. */
	private Object entityCode;
	
	/** The user name. */
	private String userName;
	
	/** The external interface */
	private Integer indExtInterface;
	
	/** The Register Date. */
	private String registerDate;
	
	/** The profile system. */
	private Integer profileSystem;
	
	/** The institution security. */
	private Integer institutionSecurity;
	
	/** The department. */
	private Integer department;
	
	private Long idHolderAccountPk;
	
	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * From string.
	 *
	 * @param jsonRepresentation the json representation
	 * @return the user filter to
	 */
	public static UserFilterTO fromString(String jsonRepresentation) {
		UserFilterTO filter= null;
		filter =new Gson().fromJson(jsonRepresentation, UserFilterTO.class);
		return filter;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the responsibility.
	 *
	 * @return the responsibility
	 */
	public Integer getResponsibility() {
		return responsibility;
	}

	/**
	 * Sets the responsibility.
	 *
	 * @param responsibility the new responsibility
	 */
	public void setResponsibility(Integer responsibility) {
		this.responsibility = responsibility;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the entity code.
	 *
	 * @return the entity code
	 */
	public Object getEntityCode() {
		return entityCode;
	}

	/**
	 * Sets the entity code.
	 *
	 * @param entityCode the new entity code
	 */
	public void setEntityCode(Object entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * Instantiates a new user filter to.
	 */
	public UserFilterTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the indExtInterface
	 */
	public Integer getIndExtInterface() {
		return indExtInterface;
	}

	/**
	 * @param indExtInterface the indExtInterface to set
	 */
	public void setIndExtInterface(Integer indExtInterface) {
		this.indExtInterface = indExtInterface;
	}

	public Integer getProfileSystem() {
		return profileSystem;
	}
	
	public void setProfileSystem(Integer profileSystem) {
		this.profileSystem = profileSystem;
	}

	/**
	 * @return the institutionSecurity
	 */
	public Integer getInstitutionSecurity() {
		return institutionSecurity;
	}

	/**
	 * @param institutionSecurity the institutionSecurity to set
	 */
	public void setInstitutionSecurity(Integer institutionSecurity) {
		this.institutionSecurity = institutionSecurity;
	}

	/**
	 * @return the department
	 */
	public Integer getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Integer department) {
		this.department = department;
	}

	public Integer getIdSystemProfilePk() {
		return idSystemProfilePk;
	}

	public void setIdSystemProfilePk(Integer idSystemProfilePk) {
		this.idSystemProfilePk = idSystemProfilePk;
	}

	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
}
