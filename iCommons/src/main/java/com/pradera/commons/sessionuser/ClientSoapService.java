package com.pradera.commons.sessionuser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.enterprise.context.ApplicationScoped;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

@ApplicationScoped
public class ClientSoapService {
	public static final String SOAP_ACTION_INVOICE = "http://tempuri.org/IServicioIntegracion/ServFactura";
	public static final String SOAP_ACTION_ACCOUNTING = "http://tempuri.org/IServicioIntegracion/ServAsiento";

	public  SOAPMessage sendSapClient(SOAPMessage soapMessage,String endPointUrl ,String soapAction) {
	 return  callSoapWebService(endPointUrl, soapAction, soapMessage) ;
	}

	private SOAPMessage callSoapWebService(String soapEndpointUrl, String soapAction, SOAPMessage soapMessage) {
		SOAPMessage soapResponse = null;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			soapResponse = soapConnection.call(createSOAPRequest(soapAction, soapMessage),soapEndpointUrl);

			// Print the SOAP Response
			System.out.println("Response SOAP Message:");
			soapResponse.writeTo(System.out);
			System.out.println();

			soapConnection.close();
		} catch (Exception e) {
			System.err
					.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
			e.printStackTrace();
		}
		return soapResponse;
	}

	private SOAPMessage createSOAPRequest(String soapAction, SOAPMessage soapMessage) throws Exception {
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);
		soapMessage.saveChanges();
		System.out.println("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;
	}

	public SOAPMessage sendSapClient(byte[] fileXmlSent, String endPoint) {

		SOAPConnectionFactory soapConnectionFactory;
		try {
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory .createConnection();
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(this.SOAP_ACTION_ACCOUNTING, fileXmlSent),endPoint);
			System.out.println("Response SOAP Message:");
			soapResponse.writeTo(System.out);
			System.out.println();
			return soapResponse;
		} catch (UnsupportedOperationException | SOAPException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static SOAPMessage createSOAPRequest(String soapAction, byte[] fileXmlSent) throws Exception {
		SOAPMessage soapMessage;
		// InputStream is = new ByteArrayInputStream(test.getBytes());
		InputStream is = new ByteArrayInputStream(fileXmlSent);
		soapMessage = MessageFactory.newInstance().createMessage(null, is);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);

		soapMessage.saveChanges();

		System.out.println("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;
	}

}
