package com.pradera.commons.sessionuser;

import java.io.Serializable;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class OperationUserTO.
 * using for get user and operation from diferents objects
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2015
 */
public class OperationUserTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3867452057872674465L;
	
	/** The user name. */
	private String userName;
	
	/** The operation number. */
	private String operNumber;
	
	/**
	 * Instantiates a new user filter to.
	 */
	public OperationUserTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the operNumber
	 */
	public String getOperNumber() {
		return operNumber;
	}

	/**
	 * @param operNumber the operNumber to set
	 */
	public void setOperNumber(String operNumber) {
		this.operNumber = operNumber;
	}
	
	

}
