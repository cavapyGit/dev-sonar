package com.pradera.commons.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tika.Tika;
import org.beanio.BeanIOConfigurationException;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.BeanWriterException;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Years;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.gson.Gson;
import com.pradera.commons.massive.utils.ValidationOperationErrorHandler;
import com.pradera.commons.security.model.type.LanguageType;
import com.pradera.commons.type.InterfaceLabelType;
import com.pradera.commons.type.InterfaceResponseCodeType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.OperationInterfaceTO;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationCupon;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationOperation;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.generalparameters.to.MoneyExchangeTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.CustomException;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.usersession.UserAcctions;
import com.sun.jersey.core.util.Base64;

/** <ul>
 * <li>
 * Project Pradera. Copyright 2012.</li>
 * </ul>
 * 
 * The Class CommonsUtilities.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 17/12/2012 */
public class CommonsUtilities {

	/** The map interface label. */
	public static Map<String, InterfaceLabelType> mapInterfaceLabel = new HashMap<String, InterfaceLabelType>();

	/** The map interface error code. */
	public static Map<String, InterfaceResponseCodeType> mapInterfaceErrorCode = new ConcurrentHashMap<String, InterfaceResponseCodeType>();

	/** The Constant DEFAULT_LOCALE. */
	public static final Locale DEFAULT_LOCALE = new Locale("es");

	/** The Constant DATE_PATTERN. */
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String DATE_PATTERN_EN = "yy/MM/dd";

	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	public static final String DATE_TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/** The Constant DATE_PATTERN. */
	public static final String DATE_PATTERN_OPTIONAL = "ddMMyyyy";

	/** The Constant DATE_PATTERN_MINIMAL. */
	public static final String DATE_PATTERN_MINIMAL = "ddMMyy";

	/** The Constant DATE_PATTERN_MINIMAL_REV. */
	public static final String DATE_PATTERN_MINIMAL_REV = "yyMMdd";

	/** The Constant DATE_FORMAT_DOCUMENT. */
	public static final String DATE_PATTERN_DOCUMENT = "dd ' de ' MMMM ' de ' yyyy";

	/** The Constant DATE_PATTERN_DOCUMENT_WITH_HOURS. */
	public static final String DATE_PATTERN_DOCUMENT_WITH_HOURS = "dd ' de ' MMMM ' de ' yyyy ' hrs. ' HH:mm:ss";

	/** The Constant HOUR_PATTERN_OPTIONAL. */
	public static final String HOUR_PATTERN_OPTIONAL = "HHMMs";

	/** The Constant DATETIME_PATTERN. */
	public static final String DATETIME_PATTERN = "ddMMyyyyHHmmss";

	/** The Constant DATETIME_PATTERN. */
	public static final String DATETIME_HOURS_PATTERN = "yyyyMMddHHmm";

	/** The Constant DATE_PATTERN_BILLING yyyyMMdd*. */
	public static final String DATE_PATTERN_BILLING = "yyyyMMdd";

	/** The Constant HOUR_PATTERN. */
	public static final String HOUR_PATTERN = "HH:mm:ss";

	/** The Constant YEAR_PATTERN. */
	public static final String YEAR_PATTERN = "yyyy";

	/** The Constant STR_BLANK. */
	public static final String STR_BLANK = "";

	/** The Constant STR_ZERO. */
	public static final String STR_ZERO = "0";

	/** The Constant STR_ONE. */
	public static final String STR_ONE = "1";

	/** The Constant NO_UTIL_DAYS. */
	public static final Integer[] NO_UTIL_DAYS = { 7, 1 };

	/** The Constant CHARACTER_TRUNCATE_STRING. */
	public static final String CHARACTER_TRUNCATE_STRING = ".....";

	/** The Constant CHARACTER_TRUNCATE_STRING. */
	public static final String[] CAPITAL_MONTHS = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
			"Septiembre", "Octubre", "Noviembre", "Diciembre" };


	/** The default tika. */
	private static Tika defaultTika = new Tika();
	
	
	private static final String UNIDADES[] = { "cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve" };// 10
	private static final String DECENAS_ESPECIALES[] = { "once", "doce", "trece", "catorce", "quince", "diecis&eacute;is", "diecisiete", "dieciocho", "diecinueve" };// 9

	private static final String DECENA_ESPECIAL = "veinti";

	private static final String DECENAS[] = { "diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa" };// 9

	private static final String CENTENA_ESPECIAL = "ciento";

	private static final String CENTENAS[] = { "cien", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos",
			"novecientos" };// 9

	private static final String UNIDADES_MIL[] = { "mil", "dos mil", "tres mil", "cuatro mil", "cinco mil", "seis mil", "siete mil", "ocho mil", "nueve mil" };// 9

	private static final String DECENAS_MIL_ESPECIALES[] = { "once mil", "doce mil", "trece mil", "catorce mil", "quince mil", "diecis&eacute;is mil", "diecisiete mil",
			"dieciocho mil", "diecinueve mil" };// 9

	private static final String DECENAS_MIL[] = { "diez mil", "veinte mil", "treinta mil", "cuarenta mil", "cincuenta mil", "sesenta mil", "setenta mil", "ochenta mil",
			"noventa mil" };// 9

	private static final String DECENA_UN_MIL_ESPECIAL_VEINTE = "&uacute;n mil";
	private static final String DECENA_UN_MIL_ESPECIAL = "un mil";

	private static final String CENTENA_MIL[] = { "cien mil", "doscientos mil", "trescientos mil", "cuatrocientos mil", "quinientos mil", "seiscientos mil",
			"setecientos mil", "ochocientos mil", "novecientos mil" };// 9

	private static final String UNIDAD_MILLON[] = { "un mill&oacute;n", "dos millones", "tres millones", "cuatro millones", "cinco millones", "seis millones", "siete millones",
			"ocho millones", "nueve millones" };// 9

	private static final String CENTENA_MILLON[] = { "cien millones", "doscientos millones", "trescientos millones", "cuatrocientos millones", "quinientos millones",
			"seiscientos millones", "setecientos millones", "ochocientos millones", "novecientos millones" };// 9

	private static final String MILLONES = "millones";

	/** menores a 100
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess100(int num) {
		String literal = "";
		if (num >= 0 && num < 10) {
			literal = UNIDADES[num];
		} else {
			if (num > 10 && num < 20) {
				literal = DECENAS_ESPECIALES[num - 11];
			} else {
				if (num < 100) {
					int unid = num % 10;
					int dec = num / 10;
					if (unid == 0)
						literal = DECENAS[dec - 1];
					else {
						if (dec == 2) {
							literal = DECENA_ESPECIAL + UNIDADES[unid];
						} else {
							literal = DECENAS[dec - 1] + " y " + UNIDADES[unid];
						}
					}
				}
			}
		}
		return literal;
	}
	
	/** menores a 1000
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess1000(int num) {
		String literal = "";
		String numString = Integer.valueOf(num).toString();
		int centena = Integer.parseInt(numString.substring(0, 1));
		int resto = Integer.parseInt(numString.substring(1));
		if (resto > 0) {
			if (centena == 1) {
				literal = CENTENA_ESPECIAL + " " + literalNumLess100(resto);
			} else {
				literal = CENTENAS[centena - 1] + " " + literalNumLess100(resto);
			}
		} else {
			literal = CENTENAS[centena - 1];
		}
		return literal;
	}

	/** menores a diez mil
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess10000(int num) {
		String literal = "";
		String numString = Integer.valueOf(num).toString();
		int unidadMil = Integer.parseInt(numString.substring(0, 1));
		int resto = Integer.parseInt(numString.substring(1));
		if (resto > 0) {
			if (resto < 100) {
				literal = UNIDADES_MIL[unidadMil - 1] + " " + literalNumLess100(resto);
			} else {
				literal = UNIDADES_MIL[unidadMil - 1] + " " + literalNumLess1000(resto);
			}
		} else {
			literal = UNIDADES_MIL[unidadMil - 1];
		}
		return literal;
	}

	/** menores a cien mil
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess100000(int num) {
		String literal = "";
		if (num >= 1000 && num < 10000) {
			literal = literalNumLess10000(num);
		} else {
			if (num >= 11000 && num < 20000) {
				String numString = Integer.valueOf(num).toString();
				int decenaMil = Integer.parseInt(numString.substring(0, 2));
				int resto = Integer.parseInt(numString.substring(2));
				if (resto > 0) {
					if (resto < 100) {
						literal = DECENAS_MIL_ESPECIALES[decenaMil - 11] + " " + literalNumLess100(resto);
					} else {
						literal = DECENAS_MIL_ESPECIALES[decenaMil - 11] + " " + literalNumLess1000(resto);
					}
				} else {
					literal = DECENAS_MIL_ESPECIALES[decenaMil - 11];
				}
			} else {
				if (num < 100000) {
					String numString = Integer.valueOf(num).toString();
					int decenaMil = Integer.parseInt(numString.substring(0, 1));
					int resto = Integer.parseInt(numString.substring(1));

					if (resto == 0) {
						literal = DECENAS_MIL[decenaMil - 1];
					} else {
						// caso especial para [21 - 29]
						if (decenaMil == 2) {
							if (resto < 1000) {
								if (resto < 100) {
									literal = DECENAS_MIL[decenaMil - 1] + " " + literalNumLess100(resto);
								} else {
									literal = DECENAS_MIL[decenaMil - 1] + " " + literalNumLess1000(resto);
								}
							} else {
								// caso especial para [UN MIL] dentro de [21 - 29]
								String numString2 = Integer.valueOf(resto).toString();
								int unidadMil = Integer.parseInt(numString2.substring(0, 1));
								int restoAux = Integer.parseInt(numString2.substring(1));

								if (unidadMil == 1) {
									if (restoAux == 0) {
										literal = DECENA_ESPECIAL + DECENA_UN_MIL_ESPECIAL_VEINTE;
									} else {
										if (restoAux < 100) {
											literal = DECENA_ESPECIAL + DECENA_UN_MIL_ESPECIAL_VEINTE + " " + literalNumLess100(restoAux);
										} else {
											literal = DECENA_ESPECIAL + DECENA_UN_MIL_ESPECIAL_VEINTE + " " + literalNumLess1000(restoAux);
										}
									}
								} else {
									// final
									literal = DECENA_ESPECIAL + literalNumLess10000(resto);
								}
							}
						} else {
							if (resto < 1000) {
								if (resto < 100) {
									literal = DECENAS_MIL[decenaMil - 1] + " " + literalNumLess100(resto);
								} else {
									literal = DECENAS_MIL[decenaMil - 1] + " " + literalNumLess1000(resto);
								}
							} else {
								// caso especial para [UN MIL]
								String numString2 = Integer.valueOf(resto).toString();
								int unidadMil = Integer.parseInt(numString2.substring(0, 1));
								int restoAux = Integer.parseInt(numString2.substring(1));

								if (unidadMil == 1) {
									if (restoAux > 0) {
										if (restoAux < 100) {
											literal = DECENAS[decenaMil - 1] + " y " + DECENA_UN_MIL_ESPECIAL + " " + literalNumLess100(restoAux);
										} else {
											literal = DECENAS[decenaMil - 1] + " y " + DECENA_UN_MIL_ESPECIAL + " " + literalNumLess1000(restoAux);
										}
									} else {
										literal = DECENAS[decenaMil - 1] + " y " + DECENA_UN_MIL_ESPECIAL;
									}
								} else {
									// final
									literal = DECENAS[decenaMil - 1] + " y " + literalNumLess10000(resto);
								}
							}
						}
					}
				}
			}
		}
		return literal;
	}

	/** menores a un millon
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess1000000(int num) {
		String literal = "";
		String numString = Integer.valueOf(num).toString();
		int centenaMil = Integer.parseInt(numString.substring(0, 1));
		int resto = Integer.parseInt(numString.substring(1));
		if (resto > 0) {
			if (centenaMil == 1) {
				// menor a diez mil?
				if (resto < 10000) {
					// menor a mil?
					if (resto < 1000) {
						// menor a cien?
						if (resto < 100) {
							literal = CENTENA_MIL[centenaMil - 1] + " " + literalNumLess100(resto);
						} else {
							literal = CENTENA_MIL[centenaMil - 1] + " " + literalNumLess1000(resto);
						}
					} else {
						literal = CENTENA_ESPECIAL + " " + literalNumLess10000(resto);
					}
				} else {
					literal = CENTENA_ESPECIAL + " " + literalNumLess100000(resto);
				}
			} else {
				// menor a diez mil?
				if (resto < 10000) {
					// menor a mil?
					if (resto < 1000) {
						// menor a cien?
						if (resto < 100) {
							literal = CENTENA_MIL[centenaMil - 1] + " " + literalNumLess100(resto);
						} else {
							literal = CENTENA_MIL[centenaMil - 1] + " " + literalNumLess1000(resto);
						}
					} else {
						literal = CENTENAS[centenaMil - 1] + " " + literalNumLess10000(resto);
					}
				} else {
					literal = CENTENAS[centenaMil - 1] + " " + literalNumLess100000(resto);
				}
			}
		} else {
			literal = CENTENA_MIL[centenaMil - 1];
		}
		return literal;
	}
	
	/** menores a 10 MILLONES
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess10000000(int num) {
		String literal = "";
		String numString = Integer.valueOf(num).toString();
		int unidadMillon = Integer.parseInt(numString.substring(0, 1));
		int resto = Integer.parseInt(numString.substring(1));
		if (resto > 0) {
			// menor a cien mil?
			if (resto < 100000) {
				// menor a diez mil?
				if (resto < 10000) {
					// menor a mil?
					if (resto < 1000) {
						// menor a cien?
						if (resto < 100) {
							literal = UNIDAD_MILLON[unidadMillon - 1] + " " + literalNumLess100(resto);
						} else {
							literal = UNIDAD_MILLON[unidadMillon - 1] + " " + literalNumLess1000(resto);
						}
					} else {
						literal = UNIDAD_MILLON[unidadMillon - 1] + " " + literalNumLess10000(resto);
					}
				} else {
					literal = UNIDAD_MILLON[unidadMillon - 1] + " " + literalNumLess100000(resto);
				}
			} else {
				literal = UNIDAD_MILLON[unidadMillon - 1] + " " + literalNumLess1000000(resto);
			}
		} else {
			literal = UNIDAD_MILLON[unidadMillon - 1];
		}
		return literal;
	}

	/** menores a cien millones
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess100000000(int num) throws ArrayIndexOutOfBoundsException {
		String literal = "";
		if (num >= 1000000 && num < 10000000) {
			literal = literalNumLess10000000(num);
		} else {
			// estan entre 11 o 20 millomes?
			if (num >= 11000000 && num < 20000000) {
				String numString = Integer.valueOf(num).toString();
				int decenaMillon = Integer.parseInt(numString.substring(0, 2));
				int resto = Integer.parseInt(numString.substring(2));
				if (resto > 0) {
					// es menor a cien mil?
					if (resto < 100000) {
						// es menor a diez mil
						if (resto < 10000) {
							// es menor a mil?
							if (resto < 1000) {
								// es menor a cien
								if (resto < 100) {
									literal = DECENAS_ESPECIALES[decenaMillon - 11] + " " + MILLONES + " " + literalNumLess100(resto);
								} else {
									literal = DECENAS_ESPECIALES[decenaMillon - 11] + " " + MILLONES + " " + literalNumLess1000(resto);
								}
							} else {
								literal = DECENAS_ESPECIALES[decenaMillon - 11] + " " + MILLONES + " " + literalNumLess10000(resto);
							}
						} else {
							literal = DECENAS_ESPECIALES[decenaMillon - 11] + " " + MILLONES + " " + literalNumLess100000(resto);
						}
					} else {
						literal = DECENAS_ESPECIALES[decenaMillon - 11] + " " + MILLONES + " " + literalNumLess1000000(resto);
					}
				} else {
					literal = DECENAS_ESPECIALES[decenaMillon - 11] + " " + MILLONES;
				}
			} else {
				if (num < 100000000) {
					String numString = Integer.valueOf(num).toString();
					int decenaMillon = Integer.parseInt(numString.substring(0, 1));
					int resto = Integer.parseInt(numString.substring(1));

					if (resto == 0) {
						literal = DECENAS[decenaMillon - 1] + " " + MILLONES;
					} else {
						// caso especial para [21 - 29]
						if (decenaMillon == 2) {
							// es menor a un millon?
							if (resto < 1000000) {
								// es menor a cien mil?
								if (resto < 100000) {
									// es menor a diez mil?
									if (resto < 10000) {
										// es menor a mil?
										if (resto < 1000) {
											// es menor a cien?
											if (resto < 100) {
												literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess100(resto);
											} else {
												literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess1000(resto);
											}
										} else {
											literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess10000(resto);
										}
									} else {
										literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess100000(resto);
									}
								} else {
									literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess1000000(resto);
								}
							} else {
								// caso especial para [UN MIL] dentro de [21 - 29]
								String numString2 = Integer.valueOf(resto).toString();
								int unidadMillon = Integer.parseInt(numString2.substring(0, 1));
								int restoAux = Integer.parseInt(numString2.substring(1));

								if (unidadMillon == 1) {
									if (restoAux == 0) {
										literal = DECENA_ESPECIAL + "Ãºn " + MILLONES;
									} else {
										// es menor a cien mil?
										if (restoAux < 100000) {
											// es menor a diez mil?
											if (resto < 10000) {
												// es menor a mil?
												if (resto < 1000) {
													// es menor a cien?
													if (resto < 100) {
														literal = DECENA_ESPECIAL + "Ãºn " + MILLONES + " " + literalNumLess100(restoAux);
													} else {
														literal = DECENA_ESPECIAL + "Ãºn " + MILLONES + " " + literalNumLess1000(restoAux);
													}
												} else {
													literal = DECENA_ESPECIAL + "Ãºn " + MILLONES + " " + literalNumLess10000(restoAux);
												}
											} else {
												literal = DECENA_ESPECIAL + "Ãºn " + MILLONES + " " + literalNumLess100000(restoAux);
											}
										} else {
											literal = DECENA_ESPECIAL + "Ãºn " + MILLONES + " " + literalNumLess1000000(restoAux);
										}
									}
								} else {
									// final
									literal = DECENA_ESPECIAL + literalNumLess10000000(resto);
								}
							}
						} else {
							// es menor a un millon?
							if (resto < 1000000) {
								// es menor a cien mil?
								if (resto < 100000) {
									// es menor a diez mil?
									if (resto < 10000) {
										// es menor a mil?
										if (resto < 1000) {
											// es moenro a 100?
											if (resto < 100) {
												literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess100(resto);
											} else {
												literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess1000(resto);
											}
										} else {
											literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess10000(resto);
										}
									} else {
										literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess100000(resto);
									}
								} else {
									literal = DECENAS[decenaMillon - 1] + " " + MILLONES + " " + literalNumLess1000000(resto);
								}
							} else {
								// caso especial para [UN MIL]
								String numString2 = Integer.valueOf(resto).toString();
								int unidadMillon = Integer.parseInt(numString2.substring(0, 1));
								int restoAux = Integer.parseInt(numString2.substring(1));

								if (unidadMillon == 1) {
									if (restoAux > 0) {
										// es menor cien mil?
										if (restoAux < 100000) {
											// es menor a diez mil?
											if (restoAux < 10000) {
												// es menor a mil?
												if (restoAux < 1000) {
													// es menor a cien?
													if (restoAux < 100) {
														literal = DECENAS[decenaMillon - 1] + " y un " + MILLONES + " " + literalNumLess100(restoAux);
													} else {
														literal = DECENAS[decenaMillon - 1] + " y un " + MILLONES + " " + literalNumLess1000(restoAux);
													}
												} else {
													literal = DECENAS[decenaMillon - 1] + " y un " + MILLONES + " " + literalNumLess10000(restoAux);
												}
											} else {
												literal = DECENAS[decenaMillon - 1] + " y un " + MILLONES + " " + literalNumLess100000(restoAux);
											}
										} else {
											literal = DECENAS[decenaMillon - 1] + " y un " + MILLONES + " " + literalNumLess1000000(restoAux);
										}
									} else {
										literal = DECENAS[decenaMillon - 1] + " y un " + MILLONES;
									}
								} else {
									// final
									literal = DECENAS[decenaMillon - 1] + " y " + literalNumLess10000000(resto);
								}
							}
						}
					}
				}
			}
		}
		return literal;
	}

	/** metodo para retornar la parte literal de un numero menor a 1000 millones
	 * 
	 * @param num
	 * @return */
	private static String literalNumLess1000000000(int num) {
		String literal = "";
		String numString = Integer.valueOf(num).toString();
		int centenaMillon = Integer.parseInt(numString.substring(0, 1));
		int resto = Integer.parseInt(numString.substring(1));
		if (resto > 0) {
			if (centenaMillon == 1) {
				// menor a 10 millones?
				if (resto < 10000000) {
					// menor a 1 millon?
					if (resto < 1000000) {
						// menor a cien mil?
						if (resto < 100000) {
							// menor a 10 mil?
							if (resto < 10000) {
								// menor a mil?
								if (resto < 1000) {
									// menor a cien?
									if (resto < 100) {
										literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess100(resto);
									} else {
										literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess1000(resto);
									}
								} else {
									literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess10000(resto);
								}
							} else {
								literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess100000(resto);
							}
						} else {
							literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess1000000(resto);
						}
					} else {
						literal = CENTENA_ESPECIAL + " " + literalNumLess10000000(resto);
					}
				} else {
					literal = CENTENA_ESPECIAL + " " + literalNumLess100000000(resto);
				}
			} else {
				// de ciento noventa y nueva millones para arriba
				// menor a 10 millones?
				if (resto < 10000000) {
					// menor a 1 millon?
					if (resto < 1000000) {
						// menor a cien mil?
						if (resto < 100000) {
							// menor a 10 mil?
							if (resto < 10000) {
								// menor a mil?
								if (resto < 1000) {
									// menor a cien?
									if (resto < 100) {
										literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess100(resto);
									} else {
										literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess1000(resto);
									}
								} else {
									literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess10000(resto);
								}
							} else {
								literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess100000(resto);
							}
						} else {
							literal = CENTENA_MILLON[centenaMillon - 1] + " " + literalNumLess1000000(resto);
						}
					} else {
						literal = CENTENAS[centenaMillon - 1] + " " + literalNumLess10000000(resto);
					}
				} else {
					literal = CENTENAS[centenaMillon - 1] + " " + literalNumLess100000000(resto);
				}
			}
		} else {
			literal = CENTENA_MILLON[centenaMillon - 1];
		}
		return literal;
	}

	/** method to get a literal number less a 100.000.000
	 * 
	 * @param number
	 * @return */
	public static String getLiteralNumber(int number) {
		try {
			String literal = "";
			// menor a 100
			if (number < 100) {
				literal = literalNumLess100(number);
			} else {
				// menor a 1000
				if (number < 1000) {
					literal = literalNumLess1000(number);
				} else {
					// menor a 10 mil
					if (number < 10000) {
						literal = literalNumLess10000(number);
					} else {
						// menor a 100 mil
						if (number < 100000) {
							literal = literalNumLess100000(number);
						} else {
							// menor a 1 millon
							if (number < 1000000) {
								literal = literalNumLess1000000(number);
							} else {
								// menor a 10 millones
								if (number < 10000000) {
									literal = literalNumLess10000000(number);
								} else {
									// menor a 100 millones
									if (number < 100000000) {
										literal = literalNumLess100000000(number);
									} else {
										// menor a 1000 millones
										if (number < 1000000000) {
											literal = literalNumLess1000000000(number);
										} else {
											System.out.println("::: Fuera de rango");
											/* Fuera de rango */
											literal = number + "";
										}

									}
								}
							}
						}
					}
				}
			}

			// colocando acentos a los numeros acentuados
			literal = literal.replaceAll(" un mill&oacute;n", " un millones");
			literal = literal.replaceAll("veintidos", "veintid&oacute;s");
			literal = literal.replaceAll("veintitres", "veintitr&eacute;s");
			literal = literal.replaceAll("veintiseis", "veintis&eacute;is");

			return literal;
		} catch (Exception e) {
			e.printStackTrace();
			return number + "";
		}
	}

	/** Current date.
	 * 
	 * @return the date */
	public static Date currentDate() {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN);
			Calendar calCurrentDate = Calendar.getInstance();
			dfDate.setTimeZone(calCurrentDate.getTimeZone());
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}

	/** Current date.
	 * 
	 * @return the date */
	public static Date formatDate(Date objDate) {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN);
			String strCurrentDate = dfDate.format(objDate);
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}

	/** Current date with time.
	 * 
	 * @return the date */
	public static Date currentDateWithTime() {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN_DOCUMENT_WITH_HOURS);
			Calendar calCurrentDate = Calendar.getInstance();
			dfDate.setTimeZone(calCurrentDate.getTimeZone());
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}

	/** Current date optional.
	 * 
	 * @return the date */
	public static Date currentDateOptional() {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN_OPTIONAL);
			Calendar calCurrentDate = Calendar.getInstance();
			dfDate.setTimeZone(calCurrentDate.getTimeZone());
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}

	/** Next date.
	 * 
	 * @return the date */
	public static Date nextDate() {
		return addDate(currentDate(), 1);
	}

	/** Adds the date.
	 * 
	 * @param objDate the obj date
	 * @param daysAdd the days add
	 * @return the date */
	public static Date addDate(Date objDate, int daysAdd) {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN);
			Calendar calCurrentDate = Calendar.getInstance();
			calCurrentDate.setTime(objDate);
			calCurrentDate.add(Calendar.DATE, daysAdd);
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}

	public static Date addMinutes(Date objDate, int minutesAdd) {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_TIME_PATTERN);
			Calendar calCurrentDate = Calendar.getInstance();
			calCurrentDate.setTime(objDate);
			calCurrentDate.add(Calendar.MINUTE, minutesAdd);
			String strCurrentDate = dfDate.format(calCurrentDate.getTime());
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}

	/** Current date time.
	 * 
	 * @return the date */
	public static Date currentDateTime() {
		Calendar calCurrentDate = Calendar.getInstance();
		return calCurrentDate.getTime();
	}

	/** Sets the time to date.
	 * 
	 * @param objDate the obj date
	 * @param timeAdd the time add
	 * @return the date */
	public static Date setTimeToDate(Date objDate, Date timeAdd) {
		Calendar calCurrentDate = Calendar.getInstance();
		calCurrentDate.setTime(objDate);

		Calendar calTime = Calendar.getInstance();
		calTime.setTime(timeAdd);

		calCurrentDate.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
		calCurrentDate.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
		calCurrentDate.set(Calendar.SECOND, calTime.get(Calendar.SECOND));
		calCurrentDate.set(Calendar.MILLISECOND, calTime.get(Calendar.MILLISECOND));

		return calCurrentDate.getTime();
	}

	/** Convert string to date with format dd/MM/yyyy.
	 * 
	 * @param templateDate the template date
	 * @return the date */
	public static Date convertStringtoDate(String templateDate) {
		Date newDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN);
		try {
			if (templateDate != null && !templateDate.isEmpty()) {
				newDate = simpleFormat.parse(templateDate);
			}
		} catch (Exception ex) {

		}
		return newDate;
	}

	/** Convert stringto date.
	 * 
	 * @param templateDate the template date
	 * @param pattern the pattern
	 * @return the date */
	public static Date convertStringtoDate(String templateDate, String pattern) {
		Date newDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(pattern);
		try {
			if (templateDate != null && !templateDate.isEmpty()) {
				newDate = simpleFormat.parse(templateDate);
			}
		} catch (Exception ex) {

		}
		return newDate;
	}

	/** Validate date.
	 * 
	 * @param templateDate the template date
	 * @return the date */
	public static Date validateDate(String templateDate) {
		try {
			SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN);
			simpleFormat.setLenient(false);
			simpleFormat.parse(templateDate);
		} catch (ParseException e) {
			return null;
		}
		return convertStringtoDate(templateDate);
	}
	
	public static Date validateDate(String templateDate, String pattern) {
		try {
			SimpleDateFormat simpleFormat = new SimpleDateFormat(pattern);
			simpleFormat.setLenient(false);
			simpleFormat.parse(templateDate);
		} catch (ParseException e) {
			return null;
		}
		return convertStringtoDate(templateDate, pattern);
	}

	/** Generate json from object.
	 * 
	 * @param object the object
	 * @return the string */
	public static String generateJsonFromObject(Object object) {
		Gson objectGson = new Gson();
		return objectGson.toJson(object);
	}

	/** Are string equals.
	 * 
	 * @param o_1 the o_1
	 * @param o_2 the o_2
	 * @return true, if successful */
	public static boolean areStringEquals(Object o_1, Object o_2) {
		return o_1.toString().compareToIgnoreCase(o_2.toString()) == 0;
	}

	/** Round.
	 * 
	 * @param d the d
	 * @param scale the scale
	 * @param roundUp the round up
	 * @return the big decimal */
	public static BigDecimal round(BigDecimal d, int scale, boolean roundUp) {
		int mode = (roundUp) ? BigDecimal.ROUND_UP : BigDecimal.ROUND_DOWN;
		return d.setScale(scale, mode);
	}

	/** Round rate factor.
	 * 
	 * @param factor the factor
	 * @return the big decimal */
	public static BigDecimal roundRateFactor(BigDecimal factor) {
		// return factor.setScale(4, RoundingMode.HALF_UP); a cuatro decimales se pierde exactitud en el monto de cupon
		return factor.setScale(8, RoundingMode.HALF_UP);
	}

	/** Round amount.
	 * 
	 * @param amount the amount
	 * @return the big decimal */
	public static BigDecimal roundAmount(BigDecimal amount) {
		return amount.setScale(2, RoundingMode.HALF_UP);
	}

	/** Round amount Coupon.
	 * 
	 * @param amount the amount Coupon
	 * @return the big decimal */
	public static BigDecimal roundAmountCoupon(BigDecimal amount) {
		return amount.setScale(8, RoundingMode.HALF_UP);
	}

	/** Encripte string for rest full.
	 * 
	 * @param replace the replace
	 * @return the string */
	public static String encripteStringForRestFull(String replace) {
		String data = replace;
		data = data.replaceAll("\"", "OPEN_COMILLA");
		data = data.replaceAll("\\{", "OPEN_KEY");
		data = data.replaceAll("}", "CLOSE_KEY");
		data = data.replaceAll(":", "2_POINT");
		data = data.replaceAll("\\[", "OPEN_CLASP");
		data = data.replaceAll("]", "CLOSE_CLASP");
		return data;
	}

	/** Des encripte string for rest full.
	 * 
	 * @param parameter the parameter
	 * @return the string */
	public static String desEncripteStringForRestFull(String parameter) {
		String data = parameter.replace("OPEN_COMILLA", "\"");
		data = parameter.replace("OPEN_KEY", "{");
		data = parameter.replace("CLOSE_KEY", "}");
		data = parameter.replace("2_POINT", ":");
		data = parameter.replace("OPEN_CLASP", "[");
		data = parameter.replace("CLOSE_CLASP", "[");
		return data;
	}

	/** Current year.
	 * 
	 * @return the integer */
	public static Integer currentYear() {
		return Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR));
	}

	/** Is Util Date.
	 * 
	 * @param date the date
	 * @return the date */
	public static boolean isUtilDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer numDay = cal.get(Calendar.DAY_OF_WEEK);
		for (Integer day : NO_UTIL_DAYS) {
			if (numDay.equals(day)) {
				return false;
			}
		}
		// TODO HoyDays
		return true;
	}

	/** Match custom pattern.
	 * 
	 * @param patternValue the patternValue
	 * @param value true, if successful
	 * @return true, if successful */
	public static boolean matchPattern(String patternValue, String value) {
		Pattern pattern = Pattern.compile(patternValue);
		Matcher matcher = pattern.matcher(value);
		// Match the end of value is an alphanumeric character
		return matcher.matches();
	}

	/** Match address.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchAddress(String value) {
		Pattern pattern = Pattern.compile(GeneralConstants.ADDRESS_PATTERN);
		Matcher matcher = pattern.matcher(value);
		// Match the end of value is an alphanumeric character
		return matcher.matches() && matchAlphanumericAndDot(value.substring(value.length() - 1));
	}

	/** Match observation.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchObservation(String value) {
		// Address is the same pattern of observation
		Pattern pattern = Pattern.compile(GeneralConstants.ADDRESS_PATTERN);
		Matcher matcher = pattern.matcher(value);
		// Match the end of value is an alphanumeric character
		return matcher.matches();
	}

	/** Match email.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchEmail(String value) {
		Pattern pattern = Pattern.compile(GeneralConstants.EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(value);
		// Match the end of value is an alphanumeric character
		return matcher.matches() && matchAlphanumeric(value.substring(value.length() - 1));
	}

	/** Match web page.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchWebPage(String value) {
		Pattern pattern = Pattern.compile(GeneralConstants.WEB_PAGE_PATTERN);
		Matcher matcher = pattern.matcher(value);
		// Match the end of value is an alphanumeric character
		return matcher.matches() && matchAlphanumeric(value.substring(value.length() - 1));
	}

	/** Match alphanumeric.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchAlphanumeric(String value) {
		Pattern pattern = Pattern.compile(GeneralConstants.ALPHANUMERIC_PATTERN);
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}

	/** Match alphanumeric and dot.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchAlphanumericAndDot(String value) {
		Pattern pattern = Pattern.compile(GeneralConstants.ALPHANUMERIC_AND_DOT_PATTERN);
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}

	/** Match numeric.
	 * 
	 * @param value the value
	 * @return true, if successful */
	public static boolean matchNumeric(String value) {
		Pattern pattern = Pattern.compile(GeneralConstants.NUMERIC_PATTERN);
		Matcher matcher = pattern.matcher(value);
		return matcher.matches();
	}

	/** Convert date to String with format dd/MM/yyyy.
	 * 
	 * @param templateDate the template date
	 * @return the date */
	public static String convertDatetoString(Date templateDate) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN);
		try {
			if (templateDate != null) {
				strDate = simpleFormat.format(templateDate);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Convert dateto string optional.
	 * 
	 * @param templateDate the template date
	 * @return the string */
	public static String convertDatetoStringOptional(Date templateDate) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN_OPTIONAL);
		try {
			if (templateDate != null) {
				strDate = simpleFormat.format(templateDate);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Convert dateto string For Template.
	 * 
	 * @param templateDate the template date
	 * @param template the template
	 * @return the string */
	public static String convertDatetoStringForTemplate(Date templateDate, String template) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(template);
		try {
			if (templateDate != null) {
				strDate = simpleFormat.format(templateDate);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Convert dateto time string.
	 * 
	 * @param templateDate the template date
	 * @return the string */
	public static String convertDatetoTimeString(Date templateDate) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(HOUR_PATTERN);
		try {
			if (templateDate != null) {
				strDate = simpleFormat.format(templateDate);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Convert date to string.
	 * 
	 * @param date the date
	 * @param format the format
	 * @return the string */
	public static String convertDateToString(Date date, String format) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format);
		try {
			if (date != null) {
				strDate = simpleFormat.format(date);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Convert date to string locale es.
	 * 
	 * @param date the date
	 * @param format the format
	 * @return the string */
	public static String convertDateToStringLocaleES(Date date, String format) {
		String strDate = null;
		Locale locale = new Locale(LanguageType.SPANISH.getValue());
		DateFormatSymbols symbols = new DateFormatSymbols(locale);
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format, locale);

		symbols.setMonths(CAPITAL_MONTHS);
		simpleFormat.setDateFormatSymbols(symbols);
		try {
			if (date != null) {
				strDate = simpleFormat.format(date);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}
	
	/** Convert date to string locale as param.
	 * 
	 * @param date the date
	 * @param format the format
	 * @return the string */
	public static String convertDateToStringLocale(Date date, String format, Locale locale) {
		String strDate = null;
		DateFormatSymbols symbols = new DateFormatSymbols(locale);
		SimpleDateFormat simpleFormat = new SimpleDateFormat(format, locale);

		symbols.setMonths(CAPITAL_MONTHS);
		simpleFormat.setDateFormatSymbols(symbols);
		try {
			if (date != null) {
				strDate = simpleFormat.format(date);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Adds the days to date.
	 * 
	 * @param date the date
	 * @param days the days
	 * @return the date */
	public static Date addDaysToDate(Date date, Integer days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, days);
		return cal.getTime();
	}

	/** Adds the years to date.
	 * 
	 * @param date the date
	 * @param days the days
	 * @return the date */
	public static Date addYearsToDate(Date date, Integer days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, days);
		return cal.getTime();
	}

	/** Checks if is weekend.
	 * 
	 * @param cal the cal
	 * @return true, if is weekend */
	public static boolean isWeekend(Calendar cal) {
		Integer currentDay = cal.get(Calendar.DAY_OF_WEEK);
		if (currentDay == Calendar.SATURDAY || currentDay == Calendar.SUNDAY)
			return Boolean.TRUE;
		return Boolean.FALSE;
	}

	/** Adds the days habils to date.
	 * 
	 * @param date the date
	 * @param days the days
	 * @return the date */
	public static Date addDaysHabilsToDate(Date date, Integer days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		while (days != 0) {
			cal.add(Calendar.DAY_OF_YEAR, 1);
			if (!isWeekend(cal)) {
				days--;
			}
		}

		return cal.getTime();
	}
	
	public static Date removeDaysHabilsToDate(Date date, Integer days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		while (days != 0) {
			cal.add(Calendar.DAY_OF_YEAR, -1);
			if (!isWeekend(cal)) {
				days--;
			}
		}

		return cal.getTime();
	}

	/** Checks if is multiple of.
	 * 
	 * @param multiple the multiple
	 * @param base the base
	 * @return true, if is multiple of */
	public static boolean isMultipleOf(final BigDecimal multiple, final BigDecimal base) {
		if (multiple.compareTo(base) == 0) {
			return true;
		}
		try {
			multiple.divide(base, 0, BigDecimal.ROUND_UNNECESSARY);
			return true;
		} catch (ArithmeticException e) {
			return false;
		}
	}

	/** Gets the percentage of.
	 * 
	 * @param number the number
	 * @param porcent the porcent
	 * @return the percentage of */
	public static BigDecimal getPercentageOf(BigDecimal number, Double porcent) {
		return number.multiply(new BigDecimal(porcent / 100));
	}

	/** Gets the percent on decimals.
	 * 
	 * @param number the number
	 * @return the percent on decimals */
	public static BigDecimal getPercentOnDecimals(BigDecimal number) {
		return number.divide(new BigDecimal(100), new MathContext(8, RoundingMode.HALF_EVEN));
	}

	/** Adds the months to date.
	 * 
	 * @param date the date
	 * @param months the months
	 * @return the date */
	public static Date addMonthsToDate(Date date, Integer months) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, months);
		return cal.getTime();
	}

	public static Integer getMonths(Date date) {
		Calendar cal = Calendar.getInstance();
		
		return cal.get(Calendar.MONTH) + 1;
	}
	
	public static Integer getYear(Date date) {
		Calendar cal = Calendar.getInstance();
		
		return cal.get(Calendar.YEAR);
	}
	
	/** Working date calculate.
	 * 
	 * @param date the date
	 * @param intervalDays the interval days
	 * @return the date */
	public static Date workingDateCalculate(Date date, Integer intervalDays) {
		Date returnDate = date;
		Calendar calAux = Calendar.getInstance();
		while (!CommonsUtilities.isUtilDate(returnDate)) {
			calAux.setTime(returnDate);
			calAux.add(Calendar.DAY_OF_MONTH, Integer.valueOf(intervalDays));
			returnDate = calAux.getTime();
		}
		return returnDate;
	}

	/** Convert an Object to a DateTime.
	 * 
	 * @param value the value
	 * @return the java.util. date */
	public static java.util.Date convertObjectToDate(Object value) {
		Date objDate = null;
		if (value != null)
			if (value instanceof java.util.Date)
				objDate = (java.util.Date) value;

		return objDate;
	}

	/** Gets the days between.
	 * 
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the days between */
	public static int getDaysBetween(Date startDate, Date endDate) {

		LocalDate firstDate = new LocalDate(startDate);
		LocalDate lastDate = new LocalDate(endDate);
		int days = Days.daysBetween(firstDate, lastDate).getDays();
		return days;
	}

	/** Gets the months between.
	 * 
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the days months */
	public static int getMonthsBetween(Date startDate, Date endDate) {
		LocalDate firstDate = new LocalDate(startDate);
		LocalDate lastDate = new LocalDate(endDate);
		int months = Months.monthsBetween(firstDate, lastDate).getMonths();
		return months;
	}

	/** Gets the years between.
	 * 
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the years between */
	public static int getYearsBetween(Date startDate, Date endDate) {
		LocalDate firstDate = new LocalDate(startDate);
		LocalDate lastDate = new LocalDate(endDate);
		int years = Years.yearsBetween(firstDate, lastDate).getYears();
		return years;
	}

	/** Gets the hours minut seconds between.
	 * 
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the hours minut seconds between */
	public static String getHoursMinutSecondsBetween(Date startDate, Date endDate) {
		LocalTime firstTime = new LocalTime(setTimeToDate(new Date(), startDate));
		LocalTime lastTime = new LocalTime(endDate);
		int seconds = Seconds.secondsBetween(lastTime, firstTime).getSeconds();
		int hours = seconds / 3600;
		int minuts = (seconds % 3600) / 60;
		seconds = (seconds % 3600) % 60;
		String format = "%02d";
		return String.format(format, hours) + ":" + String.format(format, minuts) + ":" + String.format(format, seconds);
	}

	/** Convert list date to string.
	 * 
	 * @param lstDates the lst dates
	 * @return the string */
	public static String convertListDateToString(List<Date> lstDates) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN);

		StringBuilder sbDatesConverted = new StringBuilder();
		for (int i = 0; i < lstDates.size(); i++) {
			sbDatesConverted.append(simpleFormat.format(lstDates.get(i)));
			if (i < (lstDates.size() - 1)) {
				sbDatesConverted.append(",");
			}
		}

		return sbDatesConverted.toString();
	}

	/** To get the default date with current time.
	 * 
	 * @return the date */
	public static Date getDefaultDateWithCurrentTime() {
		Calendar cal = Calendar.getInstance();
		Calendar calCur = Calendar.getInstance();
		cal.clear();
		cal.setTimeZone(calCur.getTimeZone());
		cal.set(Calendar.HOUR_OF_DAY, calCur.get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, calCur.get(Calendar.MINUTE));
		cal.set(Calendar.SECOND, calCur.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, calCur.get(Calendar.MILLISECOND));
		return cal.getTime();
	}

	/** Copy date.
	 * 
	 * @param date the date
	 * @return the date */
	public static Date copyDate(Date date) {
		return new Date(date.getTime());
	}

	/** Sum big decimal list.
	 * 
	 * @param numbers the numbers
	 * @return the big decimal */
	public static BigDecimal sumBigDecimalList(List<BigDecimal> numbers) {

		BigDecimal sum = new BigDecimal(0);
		for (BigDecimal bigDecimal : numbers) {
			if (bigDecimal != null) {
				sum = sum.add(bigDecimal);
			}
		}
		return sum;
	}

	/** Gets the string base64.
	 * 
	 * @param text the text
	 * @return the string base64 */
	public static String getStringBase64(String text) {
		String originText = text;
		try {
			if (StringUtils.isNotBlank(text)) {
				byte b[] = text.getBytes("UTF-8");
				byte b64[] = Base64.encode(b);
				originText = new String(b64);
			}
		} catch (UnsupportedEncodingException uex) {
			return text;
		}
		return originText;
	}

	/** Gets the max date of month.
	 * 
	 * @param objDate the obj date
	 * @return the max date of month */
	public static int getMaxDateOfMonth(Date objDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(objDate);
		cal.set(cal.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime().getDate();
	}

	/** Gets day of Month.
	 * 
	 * @param objDate the object date
	 * @return the max date of month */
	public static Integer getDayOfWeek(Date objDate) {
		Calendar cal = Calendar.getInstance();
		if (objDate != null) {
			cal.setTime(objDate);
			Integer day = cal.get(Calendar.DAY_OF_WEEK);
			return day;
		}
		return null;
	}

	/** Gets the date of the first working day of month (Monday).
	 * 
	 * @param objDate the obj date
	 * @return the date of the first working day of month (Monday). */
	public static Date firstWorkingDayOfMonth(Date objDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(objDate);
		cal.set(Calendar.WEEK_OF_MONTH, 1);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return cal.getTime();
	}

	/** Gets the date of the first working day of month (Monday) Formatted for the Documents Note to External (dd ' de ' MMMM '
	 * de ' yyyy).
	 * 
	 * @return the date of the first working day of month (Monday) */
	public static String firstWorkingDayOfMonthDocument() {
		return convertDateToStringLocaleES(firstWorkingDayOfMonth(currentDate()), DATE_PATTERN_DOCUMENT);
	}

	/** Gets the date of the last working day of month (Friday).
	 * 
	 * @param objDate the obj date
	 * @return the date of the last working day of month */
	public static Date lastWorkingDayOfMonth(Date objDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(objDate);
		cal.set(GregorianCalendar.DAY_OF_WEEK, Calendar.FRIDAY);
		cal.set(GregorianCalendar.DAY_OF_WEEK_IN_MONTH, -1);
		return cal.getTime();
	}

	/** Gets the date of the last working day of month (Friday) * Formatted for the Documents Note to External (dd ' de ' MMMM '
	 * de ' yyyy).
	 * 
	 * @return the date of the last working day of month (Friday) */
	public static String lastWorkingDayOfLastMonthDocument() {
		return convertDateToStringLocaleES(lastWorkingDayOfMonth(addMonthsToDate(currentDate(), -1)), DATE_PATTERN_DOCUMENT);
	}

	/** Gets the list of components for update.
	 * 
	 * @param componentsIdUpdate the components id update
	 * @return the list of components for update */
	public static List<String> getListOfComponentsForUpdate(String componentsIdUpdate) {
		ArrayList<String> updates = new ArrayList<String>();
		if (componentsIdUpdate.contains(",") || componentsIdUpdate.contains(" ")) {
			if (componentsIdUpdate.contains(",")) {
				String[] idsList = StringUtils.split(componentsIdUpdate, ",");
				for (String id : idsList) {
					updates.add(id);
				}
			} else {
				// empty separator
				String[] idsList = StringUtils.split(componentsIdUpdate);
				for (String id : idsList) {
					updates.add(id);
				}
			}
		} else {
			updates.add(componentsIdUpdate);
		}
		return updates;
	}

	/** METHOD TO GET THE CURRENT TIME IN FORMAT HH:MM:SS.
	 * 
	 * @return the string */
	public static String currentTime() {
		String currentTime = StringUtils.EMPTY;
		Calendar calendario = new GregorianCalendar();
		int hora = calendario.get(Calendar.HOUR_OF_DAY);
		int minutos = calendario.get(Calendar.MINUTE);
		int segundos = calendario.get(Calendar.SECOND);
		currentTime = hora + ":" + minutos + ":" + segundos;

		return currentTime;
	}

	/** Gets the date of the issuance note for document * Formatted for the Documents Note to External (dd ' de ' MMMM ' de '
	 * yyyy).
	 * 
	 * @return the date of the issuance note for document */
	public static String issuanceNoteDocument() {
		return convertDateToStringLocaleES(currentDate(), DATE_PATTERN_DOCUMENT);
	}

	/** Round.
	 * 
	 * @param d the d
	 * @param scale the scale
	 * @return the big decimal */
	public static BigDecimal round(BigDecimal d, int scale) {
		int mode = BigDecimal.ROUND_HALF_UP;
		return d.setScale(scale, mode);
	}

	/** Truncate string.
	 * 
	 * @param text the text
	 * @param limit the limit of string size
	 * @param characterEnding the character ending add in string
	 * @return the string */
	public static String truncateString(String text, int limit, String characterEnding) {
		if (StringUtils.isNotBlank(text)) {
			StringBuilder builder = null;
			if (text.length() > limit) {
				builder = new StringBuilder(StringUtils.substring(text, 0, limit));
				String ending = characterEnding != null ? characterEnding : CHARACTER_TRUNCATE_STRING;
				builder.append(ending);
			} else {
				builder = new StringBuilder(text);
			}
			return builder.toString();
		} else {
			return text;
		}
	}

	/** Concat with break line.
	 * 
	 * @param objects the objects
	 * @param trunc the trunc
	 * @return the string */
	public static String concatWithBreakLine(Object[] objects, int trunc) {
		String parameter = StringUtils.join(objects, GeneralConstants.STR_COMMA_WITHOUT_SPACE);

		// int idx = parameter.

		// StringBuilder.
		//
		// for (int i = 0 ; i < objects.length ; i++) {
		//
		// }
		return parameter;
	}

	/** Checks if is is leap year date.
	 * 
	 * @param date the date
	 * @return true, if is leap year */
	public static boolean isLeapYearDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		GregorianCalendar gcal = new GregorianCalendar();
		return gcal.isLeapYear(cal.get(Calendar.YEAR));
	}

	/** Checks if is between date.
	 * 
	 * @param compareDate the compare date
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return true, if is leap year */
	public static boolean isBetweenDate(Date compareDate, Date startDate, Date endDate) {
		compareDate = truncateDateTime(compareDate);
		startDate = truncateDateTime(startDate);
		endDate = truncateDateTime(endDate);

		if (compareDate.equals(startDate) || compareDate.equals(endDate))
			return true;
		if (compareDate.after(startDate) && compareDate.before(endDate))
			return true;

		return false;
	}

	/** Is less or equal date.
	 * 
	 * @param date compareDate
	 * @param compareDate the compare date
	 * @param truncateDate the truncate date
	 * @return true, if Is less or equal date */
	public static boolean isLessOrEqualDate(Date date, Date compareDate, boolean truncateDate) {
		if (truncateDate) {
			date = truncateDateTime(date);
			compareDate = truncateDateTime(compareDate);
		}
		if (date.equals(compareDate) || date.before(compareDate)) {
			return true;
		}
		return false;
	}

	/** Checks if is equal date.
	 * 
	 * @param date the date
	 * @param compareDate the compare date
	 * @return true, if is equal date */
	public static boolean isEqualDate(Date date, Date compareDate) {

		date = truncateDateTime(date);
		compareDate = truncateDateTime(compareDate);

		if (date.equals(compareDate)) {
			return true;
		}
		return false;
	}

	/** Truncate date time.
	 * 
	 * @param truncateDate the truncate date
	 * @return the date */
	public static Date truncateDateTime(Date truncateDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(truncateDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/** Put time to date.
	 * 
	 * @param date the date
	 * @param hour the hour
	 * @param minute the minute
	 * @param second the second
	 * @param milisecond the milisecond
	 * @return the date */
	public static Date putTimeToDate(Date date, int hour, int minute, int second, int milisecond) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, second);
		cal.set(Calendar.MILLISECOND, milisecond);
		return cal.getTime();
	}

	/** Validate bic code.
	 * 
	 * @param bciCode the bci code
	 * @param isSwift the is swift
	 * @return true, if successful */
	public static boolean validateBicCode(String bciCode, boolean isSwift) {
		Pattern pat = null;
		boolean validation = false;

		if (isSwift) {
			pat = Pattern.compile("^([a-zA-Z]{6,6})([a-zA-Z0-9]{1,1})([a-zA-Z]{1,1})([a-zA-Z0-9]{3,3})");
		} else {
			pat = Pattern.compile("^([a-zA-Z]{6,6})([a-zA-Z0-9]{1,1})([1]{1,1})([a-zA-Z0-9]{3,3})");
		}

		Matcher mat = pat.matcher(bciCode);

		if (mat.matches()) {
			validation = true;
		}

		return validation;
	}

	/** Gets the max date by month and year.
	 * 
	 * @param month the month
	 * @param year the year
	 * @return the max date by month and year */
	public static Date getMaxDateByMonthAndYear(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		int date = 1;
		calendar.set(year, month, date);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, month, maxDay);
		return calendar.getTime();
	}

	/** get the positions from initial to end and if there are zeros forward then delete them(flag will indicate that).
	 * 
	 * @param initial the initial
	 * @param end the end
	 * @param strField the str field
	 * @param flag (true=indicates remove zeros)
	 * @return the substring without zeros */
	public static String getSubstringWithoutZeros(int initial, int end, String strField, boolean flag) {
		String fieldCut = "";
		String finalField = "";
		boolean fl = false;

		if (strField.length() > 0) {
			if (strField.length() > initial) {
				fieldCut = strField.substring(initial);
			} else if (strField.length() > end) {
				fieldCut = strField.substring(initial, end);
			}

			if (flag) {
				for (int i = 0; i < fieldCut.length(); i++) {
					if (fieldCut.charAt(i) != '0' && fl == false) {
						fl = true;
					}
					if (fl) {
						finalField = finalField + fieldCut.charAt(i);
					}
				}
			}
			finalField = fieldCut;
		}
		return finalField.trim();
	}

	/** Get format of amount.
	 * 
	 * @param amountBd the amount bd
	 * @param indDecimals the ind decimals
	 * @param maxLength the max length
	 * @param decFormat the dec format
	 * @param blDecimal the bl decimal
	 * @return the string */
	public static String getFormatOfAmount(BigDecimal amountBd, Integer indDecimals, Integer maxLength, String decFormat,
			boolean blDecimal) {
		String amount = StringUtils.EMPTY;
		DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
		simbolo.setDecimalSeparator('.');
		if (blDecimal) {
			simbolo.setGroupingSeparator(',');
		}
		DecimalFormat formateador = new DecimalFormat(decFormat, simbolo);
		if (amountBd != null && amountBd.compareTo(BigDecimal.ZERO) >= 0) {
			amount = formateador.format(amountBd.setScale(indDecimals, RoundingMode.HALF_UP));
			// if we have a maximum of characters, we'll validate it.
			if (maxLength != null && amountBd.toString().length() >= maxLength) {
				amount = amountBd.toString().substring(0, maxLength);
			}
		} else if (amountBd != null && amountBd.compareTo(BigDecimal.ZERO) < 0) {
			amountBd = amountBd.abs();
			amount = formateador.format(amountBd.setScale(indDecimals, RoundingMode.HALF_UP));
			// if we have a maximum of characters, we'll validate it.
			if (maxLength != null && amountBd.toString().length() >= maxLength) {
				amount = amountBd.toString().substring(0, maxLength);
			}
			amount = GeneralConstants.HYPHEN.concat(amount);
		} else {
			amount = "0";
		}

		return amount;
	}

	/** Format decimal.
	 * 
	 * @param data the data
	 * @param format the format
	 * @return the string */
	public static String formatDecimal(Object data, String format) {
		String value = GeneralConstants.ZERO_DECIMAL_STRING;
		if (data != null) {
			DecimalFormat objDf = new DecimalFormat(format);
			value = objDf.format(data);
		}
		return value;
	}

	/** Add two BigDecimal result BigDecimal.
	 * 
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return the big decimal */
	public static BigDecimal addTwoDecimal(BigDecimal numDecimal1, BigDecimal numDecimal2, Integer round) {
		BigDecimal result = new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if (Validations.validateIsNull(numDecimal1)) {
			numDecimal1 = BigDecimal.ZERO;
		}
		if (Validations.validateIsNull(numDecimal2)) {
			numDecimal2 = BigDecimal.ZERO;
		}
		result = numDecimal1.add(numDecimal2).setScale(round, mode);
		return result;
	}

	/** subtract two numbers BigDecimal.
	 * 
	 * @param minuend the minuend
	 * @param subtrahend the subtrahend
	 * @param round the round
	 * @return BigDecimal */
	public static BigDecimal subtractTwoDecimal(BigDecimal minuend, BigDecimal subtrahend, Integer round) {
		if (Validations.validateIsNull(subtrahend)) {
			subtrahend = BigDecimal.ZERO;
		}
		return addTwoDecimal(minuend, subtrahend.negate(), round);
	}

	/** Multiply decimal integer.
	 * 
	 * @param numDecimal the num decimal
	 * @param numEntero the num entero
	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return Multiply BigDecimal and Integer result BigDecimal */
	public static BigDecimal multiplyDecimalInteger(BigDecimal numDecimal, Integer numEntero, Integer round) {
		BigDecimal decimalOfInteger = null;
		BigDecimal result = new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if (Validations.validateIsNull(numDecimal)) {
			numDecimal = BigDecimal.ZERO;
		}
		if (Validations.validateIsNull(numEntero)) {
			decimalOfInteger = BigDecimal.ZERO;
		} else {
			decimalOfInteger = new BigDecimal(numEntero);
		}
		result = numDecimal.multiply(decimalOfInteger).setScale(round, mode);
		return result;
	}

	/** Multiply Number <code>BigDecimal</code> and <code>Long</code>.
	 * 
	 * @param numDecimal Number BigDecimal
	 * @param numEntero Number Long
	 * @param round Ind. Round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return the big decimal */
	public static BigDecimal multiplyDecimalLong(BigDecimal numDecimal, Long numEntero, Integer round) {
		BigDecimal decimalOfLong = null;
		BigDecimal result = new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if (Validations.validateIsNull(numDecimal)) {
			numDecimal = BigDecimal.ZERO;
		}
		if (Validations.validateIsNull(numEntero)) {
			numEntero = Long.parseLong(GeneralConstants.ZERO_VALUE_STRING);
		}
		decimalOfLong = new BigDecimal(numEntero);
		result = numDecimal.multiply(decimalOfLong).setScale(round, mode);
		return result;
	}

	/** * Multiply two BigDecimal result BigDecimal.
	 * 
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return the big decimal */
	public static BigDecimal multiplyDecimals(BigDecimal numDecimal1, BigDecimal numDecimal2, Integer round) {
		BigDecimal result = new BigDecimal(0);
		int mode = BigDecimal.ROUND_HALF_UP;
		if (Validations.validateIsNull(numDecimal1)) {
			numDecimal1 = BigDecimal.ZERO;
		}
		if (Validations.validateIsNull(numDecimal2)) {
			numDecimal2 = BigDecimal.ZERO;
		}

		result = numDecimal1.multiply(numDecimal2).setScale(round, mode);
		return result;
	}

	/** Divide two decimal.
	 * 
	 * @param numDecimal1 the num decimal1
	 * @param numDecimal2 the num decimal2
	 * @param round GeneralConstants.ROUNDING_DIGIT_NUMBER
	 * @return *Divide two Decimal result BigDecimal
	 * @throws ArithmeticException the arithmetic exception */
	public static BigDecimal divideTwoDecimal(BigDecimal numDecimal1, BigDecimal numDecimal2, Integer round)
			throws ArithmeticException {
		BigDecimal result = new BigDecimal(0);

		int mode = BigDecimal.ROUND_HALF_UP;
		if (numDecimal1 == null) {
			numDecimal1 = BigDecimal.ZERO;
		}
		if (numDecimal2 == null) {
			return null;
		}
		result = numDecimal1.divide(numDecimal2, round, mode);
		return result;
	}

	/** * Get first working day of month adquiere el primer dia del mes segun la fecha del sistema en formato simple.
	 * 
	 * @return the first business day of month */
	public static Date getFirstBusinessDayOfMonth() {
		boolean blValidate = false;
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMinimum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY), cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
		Calendar now = cal;
		int day = now.get(Calendar.DAY_OF_WEEK);
		if (day >= Calendar.MONDAY && day <= Calendar.FRIDAY) {
			return now.getTime();
		} else {
			while (!blValidate) {
				now.add(Calendar.DATE, 1);
				day = now.get(Calendar.DAY_OF_WEEK);
				if (day >= Calendar.MONDAY && day <= Calendar.FRIDAY) {
					blValidate = true;
				}
			}
			return now.getTime();
		}
	}

	/** * Get ultimate working day of month past adquiere el ultimo dia del mes pasado segun la fecha del sistema en formato
	 * simple.
	 * 
	 * @return the ultimate business day of month */
	public static Date getUltimateBusinessDayOfMonth() {
		boolean blValidate = false;
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.getActualMaximum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY), cal.getMinimum(Calendar.MINUTE), cal.getMinimum(Calendar.SECOND));
		Calendar now = cal;
		now.add(Calendar.MONTH, -1);
		int day = now.get(Calendar.DAY_OF_WEEK);
		if (day >= Calendar.MONDAY && day <= Calendar.FRIDAY) {
			return now.getTime();
		} else {
			while (!blValidate) {
				now.add(Calendar.DATE, 1);
				day = now.get(Calendar.DAY_OF_WEEK);
				if (day >= Calendar.MONDAY && day <= Calendar.FRIDAY) {
					blValidate = true;
				}
			}
			return now.getTime();
		}
	}

	/** Populate record validation.
	 * 
	 * @param validationOperationType the validation operation type
	 * @param field the field
	 * @param errorKey the error key
	 * @return the record validation type */
	public static RecordValidationType populateRecordValidation(ValidationOperationType validationOperationType, String field,
			String errorKey) {
		RecordValidationType recordValidationType = new RecordValidationType();
		recordValidationType.setField(field);
		recordValidationType.setOperationRef(validationOperationType);
		if (validationOperationType != null) {
			recordValidationType.setLineNumber(validationOperationType.getLineNumber());
			recordValidationType.setErrorDescription(PropertiesUtilities.getMessage(validationOperationType.getLocale(),
					errorKey));
		}
		return recordValidationType;
	}

	/** Validate file operation.
	 * 
	 * @param tmpMcnProcessFile the tmp mcn process file
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws ServiceException the service exception */
	public static void validateFileOperation(ProcessFileTO tmpMcnProcessFile) throws ParserConfigurationException,
		IOException, BeanIOConfigurationException, IllegalArgumentException, ServiceException {
		
		tmpMcnProcessFile.setValidationProcessTO(new ValidationProcessTO());
		//********************************* *************
		validateXMLFileOperation(tmpMcnProcessFile);
		
		if (Validations.validateListIsNullOrEmpty(tmpMcnProcessFile.getValidationProcessTO().getLstValidations())) {
			validateFileOperationStruct(tmpMcnProcessFile);
		}
	
	}

	/** Validate file operation.
	 * 
	 * @param tmpMcnProcessFile the tmp mcn process file
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws ServiceException the service exception */
	public static void validateFileOperation(ProcessFileTO tmpMcnProcessFile, String filePrefix, String fileSuffix) throws ParserConfigurationException,
			IOException, BeanIOConfigurationException, IllegalArgumentException, ServiceException {

		tmpMcnProcessFile.setValidationProcessTO(new ValidationProcessTO());
		/**
		 * Proceso de archivos EXCEL
		 */
		Boolean validName = validateFileOperationName(tmpMcnProcessFile, filePrefix, fileSuffix);

		if(validName) {
			//********************************* *************
			validateXMLFileOperation(tmpMcnProcessFile);
		}
		if (Validations.validateListIsNullOrEmpty(tmpMcnProcessFile.getValidationProcessTO().getLstValidations())) {
			validateFileOperationStruct(tmpMcnProcessFile);
		}

	}
	
	public static Boolean validateFileOperationName(ProcessFileTO tmpMcnProcessFile, String filePrefix, String fileSuffix) {
		String periodFormat = convertDatetoStringForTemplate(currentDate(), "yyyyMMdd");
		String validFileName = filePrefix.concat(periodFormat).concat(fileSuffix);
		
		if(!validFileName.equalsIgnoreCase(tmpMcnProcessFile.getFileName().substring(0, tmpMcnProcessFile.getFileName().indexOf(".")))){
			tmpMcnProcessFile.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail("Nombre de archivo inv\u00e1lido.");
			tmpMcnProcessFile.getValidationProcessTO().getLstValidations().add(fe);
			
			return Boolean.FALSE;
		} else {
			return Boolean.TRUE;
		}
	}
	
	/** Validate xml file operation.
	 * 
	 * @param processFile the process file
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred. */
	public static void validateXMLFileOperation(ProcessFileTO processFile) throws ParserConfigurationException, IOException {
		boolean blStatusFile = Boolean.FALSE;
		try {
			if (processFile.isDpfInterface()) {
				Random random = new Random();
				String nameFile = GeneralConstants.STR_NAME_TEMP_DPF_FILE + (random.nextInt(100) + 1)
						+ CommonsUtilities.convertDateToString(CommonsUtilities.getDefaultDateWithCurrentTime(), "mmssSSS");
				processFile.setTempProcessFile(File.createTempFile(nameFile, ".tmp"));
				FileUtils.writeByteArrayToFile(processFile.getTempProcessFile(), processFile.getProcessFile());
				blStatusFile = XMLUtils.isValidXMLFile(processFile.getTempProcessFile());
			} else {
				blStatusFile = XMLUtils.isValidXMLFile(processFile.getTempProcessFile());
			}
			if (!blStatusFile) {
				processFile.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
				RecordValidationType fe = new RecordValidationType();
				fe.setErrorDescription("Estructura de archivo inv\u00e1lida. ");
				processFile.getValidationProcessTO().getLstValidations().add(fe);
			}
		} catch (SAXException e) {
			processFile.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			if (e instanceof SAXParseException) {
				fe.setLineNumber(((SAXParseException) e).getLineNumber());
			}
			fe.setErrorDescription("Estructura de archivo inv\u00e1lida: <br/> " + e.getLocalizedMessage());
			processFile.getValidationProcessTO().getLstValidations().add(fe);
			e.printStackTrace();
		}
	}

	/** Validate file operation struct.
	 * 
	 * @param processFileTO the process file to
	 * @throws ServiceException the service exception
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception */
	public static void validateFileOperationStruct(ProcessFileTO processFileTO) throws ServiceException,
			BeanIOConfigurationException, IllegalArgumentException, IOException, ParserConfigurationException {
		InputStream inputFile = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(processFileTO.getStreamFileDir());
		String string = null;
		if (!processFileTO.isDpfInterface() && processFileTO.getTempProcessFile() != null) {
			string = FileUtils.readFileToString(processFileTO.getTempProcessFile());
		} else {
			string = new String(processFileTO.getProcessFile(), "UTF-8");
		}

		StringReader stringReader = new StringReader(string);
		BeanReader beanReader = BeanIOUtils.getReader(processFileTO.getRootTag(), inputFile, stringReader, new Locale("es"));

		InterfaceLabelType interfaceLabel = loadInterfaceLabel(processFileTO.getInterfaceName(),
				processFileTO.getStreamFileDir());
		ValidationOperationErrorHandler handler = new ValidationOperationErrorHandler(interfaceLabel);
		beanReader.setErrorHandler(handler);

		if (processFileTO.getValidationProcessTO() == null) {
			processFileTO.setValidationProcessTO(new ValidationProcessTO());
		}

		Object operation;
		// Cada objeto definido en la plantilla o stream, debe extender de ValidationOperationType
		while ((operation = beanReader.read()) != null) {
			ValidationOperationType validationOperation = (ValidationOperationType) operation;
			validationOperation.setIdFileMechanismPk(processFileTO.getIdNegotiationMechanismPk());
			validationOperation.setIdFileParticipantPk(processFileTO.getIdParticipantPk());
			validationOperation.setLocale(processFileTO.getLocale());
			validationOperation.setLineNumber(beanReader.getLineNumber());
			beanReader.getLineNumber();
			processFileTO.getValidationProcessTO().getLstOperations().add(validationOperation);
		}
		beanReader.close();
		stringReader.close();
		inputFile.close();

		processFileTO.getValidationProcessTO().setLstValidations(handler.getTotalErrors());
		processFileTO.getValidationProcessTO().setRejectedOps(handler.getRejectedOperations());
		processFileTO.setRejectedOperations(handler.getRejectedOperations());
		processFileTO.setAcceptedOperations(processFileTO.getValidationProcessTO().getLstOperations().size());
		processFileTO.setTotalOperations(processFileTO.getAcceptedOperations() + processFileTO.getRejectedOperations());
	}

	/** Load interface label.
	 * 
	 * @param interfaceName the interface name
	 * @param streamFileDir the stream file dir
	 * @return the interface label type
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred. */
	private static InterfaceLabelType loadInterfaceLabel(String interfaceName, String streamFileDir)
			throws ParserConfigurationException, IOException {
		InputStream inputFile = Thread.currentThread().getContextClassLoader().getResourceAsStream(streamFileDir);
		InterfaceLabelType interfaceLabel = mapInterfaceLabel.get(interfaceName);
		if (interfaceLabel == null) {
			interfaceLabel = new InterfaceLabelType();
			interfaceLabel.setInterfaceName(interfaceName);
			interfaceLabel.setMapInterfaceLabel(new HashMap<String, String>());

			Document streamDom = null;
			try {
				streamDom = XMLUtils.getDOMFromXMLFile(inputFile, false, false);
				NodeList nodeList = streamDom.getElementsByTagName("*");
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						if (node.hasAttributes()) {
							Attr attrXmlName = (Attr) node.getAttributes().getNamedItem("xmlName");
							Attr attrName = (Attr) node.getAttributes().getNamedItem("name");
							if (attrXmlName != null && attrName != null) {
								interfaceLabel.getMapInterfaceLabel().put(attrName.getNodeValue(), attrXmlName.getNodeValue());
							}
						}
					}
				}
				mapInterfaceLabel.put(interfaceName, interfaceLabel);
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				inputFile.close();
			}

		}

		return interfaceLabel;
	}

	/** Generate interface response file.
	 * 
	 * @param lstOperations the lst operations
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param recordTag the record tag
	 * @return the byte[] */
	public static byte[] generateInterfaceResponseFile(List<RecordValidationType> lstOperations, String mappingResponse,
			String rootTag, String recordTag) {
		byte[] objFile = null;
		try {
			InputStream mappingFile = Thread.currentThread().getContextClassLoader().getResourceAsStream(mappingResponse);
			StringWriter sw = new StringWriter();

			BeanWriter out = BeanIOUtils.createWriter(mappingFile, rootTag, sw);

			for (RecordValidationType object : lstOperations) {
				out.write(recordTag, object);
			}
			out.flush();
			out.close();

			objFile = sw.toString().getBytes("UTF-8");
			sw.flush();
			sw.close();
			mappingFile.close();
		} catch (BeanIOConfigurationException | BeanWriterException | IOException e) {
			e.printStackTrace();
		}

		return objFile;
	}

	/** Generate File to External Interface .
	 * 
	 * @param lstOperations the lst operations
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param rowTag the row tag
	 * @return the byte[] */
	public static byte[] generateInterfaceFile(List<Object> lstOperations, String mappingResponse, String rootTag,
			String rowTag) {
		byte[] objFile = null;
		try {
			InputStream mappingFile = Thread.currentThread().getContextClassLoader().getResourceAsStream(mappingResponse);
			StringWriter sw = new StringWriter();

			BeanWriter out = BeanIOUtils.createWriter(mappingFile, rootTag, sw);

			for (Object object : lstOperations) {
				if (rowTag != null) {
					out.write(rowTag, object);
				} else {
					out.write(object);
				}
			}
			out.flush();
			out.close();

			objFile = sw.toString().getBytes("UTF-8");
			sw.flush();
			sw.close();
			mappingFile.close();
		} catch (BeanIOConfigurationException | BeanWriterException | IOException e) {
			e.printStackTrace();
		}

		return objFile;
	}

	/** Checks for previous errors.
	 * 
	 * @param lstErrors the lst errors
	 * @return true, if successful */
	public static boolean hasPreviousErrors(List<RecordValidationType> lstErrors) {
		if (Validations.validateListIsNullOrEmpty(lstErrors)) {
			return false;
		}
		return true;
	}

	/** Gets the byte arg lenght mb.
	 * 
	 * @param argByte the arg byte
	 * @return the byte arg lenght mb */
	public static BigDecimal getByteArgLenghtMB(byte[] argByte) {
		BigDecimal result = null;
		if (argByte != null && argByte.length > 0) {
			double blResult = Math.round((argByte.length / 1024.0) / 1024.0 * 100.0) / 100.0;
			result = BigDecimal.valueOf(blResult);
		}
		return result;
	}

	/** Gets the logger user.
	 * 
	 * @param strUser the str user
	 * @param strIpAddress the str ip address
	 * @return the logger user */
	public static LoggerUser getLoggerUser(String strUser, String strIpAddress) {
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress(strIpAddress);
		loggerUser.setUserName(strUser);
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		UserAcctions userAcctions = new UserAcctions();
		userAcctions.setIdPrivilegeRegister(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeConfirm(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeSettlement(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeReject(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeAnnular(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeSearch(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeModify(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeActivate(new Integer(STR_ONE));
		userAcctions.setIdPrivilegeExecute(new Integer(STR_ONE));
		loggerUser.setUserAction(userAcctions);
		loggerUser.setIdPrivilegeOfSystem(userAcctions.getIdPrivilegeRegister());
		return loggerUser;
	}

	/** Gets the id security code.
	 * 
	 * @param strSecurityClass the str security class
	 * @param strSecuritySerial the str security serial
	 * @return the id security code */
	public static String getIdSecurityCode(String strSecurityClass, String strSecuritySerial) {
		String strIdSecurityCode = null;
		if (strSecurityClass != null && strSecuritySerial != null) {
			StringBuffer stringB = new StringBuffer();
			stringB.append(strSecurityClass).append(GeneralConstants.HYPHEN).append(strSecuritySerial);
			strIdSecurityCode = stringB.toString();
		}
		return strIdSecurityCode;
	}

	/** Populate record code validation.
	 * 
	 * @param objOperation the obj operation
	 * @param interfaceKey the interface key
	 * @param errorKey the error key
	 * @return the record validation type */
	public static RecordValidationType populateRecordCodeValidation(ValidationOperationType objOperation, String interfaceKey,
			String errorKey) {
		return populateRecordCodeValidation(objOperation, interfaceKey, errorKey, null);
	}

	/** Populate record code validation.
	 * 
	 * @param objOperation the obj operation
	 * @param interfaceKey the interface key
	 * @param errorKey the error key
	 * @param params the params
	 * @return the record validation type */
	public static RecordValidationType populateRecordCodeValidation(ValidationOperationType objOperation, String interfaceKey,
			String errorKey, Object[] params) {
		RecordValidationType recordValidationType = new RecordValidationType();

		if (objOperation instanceof OperationInterfaceTO) {
			recordValidationType.setOperationRef(objOperation);
		}

		if (interfaceKey != null) {
			InterfaceResponseCodeType responseCode = CommonsUtilities.mapInterfaceErrorCode.get(interfaceKey);
			if (responseCode != null) {
				recordValidationType.setErrorCode(responseCode.getResponseCode());
			}
			recordValidationType.setErrorDescription(PropertiesUtilities.getGenericMessage(interfaceKey, params,
					DEFAULT_LOCALE));
		} else if (errorKey != null) {
			recordValidationType.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE, errorKey));
		}
		return recordValidationType;
	}
	
	/** Populate record code validation.
	 * 
	 * @param objOperation the obj operation
	 * @param interfaceKey the interface key
	 * @param errorKey the error key
	 * @param params the params
	 * @return the record validation type */
	public static RecordValidationType populateRecordRowCodeValidation(Integer rowNumber, String interfaceKey,
			String errorKey) {
		RecordValidationType recordValidationType = new RecordValidationType();

		if(Validations.validateIsNotNull(rowNumber)) {
			recordValidationType.setLineNumber(rowNumber);
		}

		if (interfaceKey != null) {
			InterfaceResponseCodeType responseCode = CommonsUtilities.mapInterfaceErrorCode.get(interfaceKey);
			if (responseCode != null) {
				recordValidationType.setErrorCode(responseCode.getResponseCode());
			}
			recordValidationType.setErrorDescription(PropertiesUtilities.getGenericMessage(interfaceKey, null,
					DEFAULT_LOCALE));
		} else if (errorKey != null) {
			recordValidationType.setErrorDescription(PropertiesUtilities.getMessage(DEFAULT_LOCALE, errorKey));
		}
		return recordValidationType;
	}

	/** *.
	 * 
	 * @param listOfAlias the list of alias
	 * @param entityTOClass the entity to class
	 * @param dataList the data list
	 * @return the list
	 * @throws ServiceException the service exception */
	public static List<Object> filledBean(List<String> listOfAlias, Class entityTOClass, List<Object> dataList)
			throws ServiceException {
		List<Object> listClass = new ArrayList<>();
		Map<String, Object> mapObject = null;

		Object objectWithData = null;
		for (Object objectByRow : dataList) {
			if (((Object[]) objectByRow).length == listOfAlias.size()) {
				mapObject = new HashMap<>();
				for (int i = 0; i < listOfAlias.size(); i++) {
					mapObject.put(listOfAlias.get(i), ((Object[]) objectByRow)[i]);
				}
				objectWithData = CommonsUtilities.filledBean(mapObject, entityTOClass);
				listClass.add(objectWithData);
			}
		}

		return listClass;

	}

	/** Filled Bean .
	 * 
	 * @param fieldValueMapping the field value mapping
	 * @param cls the cls
	 * @return the object
	 * @throws ServiceException the service exception */
	public static Object filledBean(Map<String, Object> fieldValueMapping, Class cls) throws ServiceException {
		Object obj = null;
		try {
			obj = cls.newInstance();
			Field[] fields = cls.getDeclaredFields();
			for (Field field : fields) {
				field.setAccessible(true);
				for (Map.Entry<String, Object> entry : fieldValueMapping.entrySet()) {
					if (field.getName().equals(entry.getKey())) {
						if (entry.getValue() == null) {
							field.set(obj, null);
						} else if (field.getType().equals(Integer.class)) {
							field.set(obj, Integer.parseInt(entry.getValue().toString()));
						} else if (field.getType().equals(String.class)) {
							field.set(obj, entry.getValue().toString());
						} else if (field.getType().equals(BigDecimal.class)) {
							field.set(obj, (BigDecimal) entry.getValue());
						} else if (field.getType().equals(Long.class)) {
							field.set(obj, Long.parseLong(entry.getValue().toString()));
						} else if (field.getType().equals(Object.class)) {
							field.set(obj, entry.getValue());
						} else {
							field.set(obj, entry.getValue());
						}

					}
				}
			}
		} catch (InstantiationException ex) {
			throw new ServiceException();
		} catch (IllegalAccessException ex) {
			throw new ServiceException();
		}

		return obj;
	}

	/** Create Name File with Template and Map to parameter.
	 * 
	 * @param nameFile the name file
	 * @param parameters the parameters
	 * @return the string */
	public static String createNameFile(String nameFile, Map<String, Object> parameters) {

		List<String> format = new ArrayList<>();
		List<String> formatNew = new ArrayList<>();
		Map<String, String> mapa = new HashMap<String, String>();
		if (Validations.validateIsNotNullAndNotEmpty(nameFile)) {
			format = Arrays.asList(nameFile.split(ComponentConstant.SIMBOL_TO_REPLACE));
			for (String string : format) {
				int index = string.indexOf(ComponentConstant.CLASP_CLOSURE);
				if (index != -1) {
					formatNew.add(string.substring(0, index));
				}
			}
		}

		String valueReplace = null;
		for (String patron : formatNew) {
			Date hoy = currentDateWithTime();
			if (patron.substring(0, 1).equals(GeneralConstants.HYPHEN)) {
				/** Date or Time the System */
				valueReplace = convertDatetoStringForTemplate(hoy, patron.substring(1, patron.length()));
			} else if (parameters.containsKey(patron)) {
				/** value of Map parameters */
				valueReplace = parameters.get(patron).toString();
			} else {
				/** If not Exists Value ins Map: getXXX */
				valueReplace = "error: no hay data ";
			}
			mapa.put(patron, valueReplace);
		}

		Iterator it = mapa.keySet().iterator();
		while (it.hasNext()) {
			String key = (String) it.next();
			nameFile = StringUtils.replace(nameFile, key, mapa.get(key));
		}
		nameFile = StringUtils.remove(nameFile, ComponentConstant.OPEN_CLASP);
		nameFile = StringUtils.remove(nameFile, ComponentConstant.CLASP_CLOSURE);

		return nameFile;
	}

	/** Generate interface lip response file.
	 * 
	 * @param objAutomaticSendType the obj automatic send type
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param recordTag the record tag
	 * @return the byte[] */
	public static byte[] generateInterfaceLipResponseFile(AutomaticSendType objAutomaticSendType, String mappingResponse,
			String rootTag, String recordTag) {
		byte[] objFile = null;
		try {
			InputStream mappingFile = Thread.currentThread().getContextClassLoader().getResourceAsStream(mappingResponse);
			StringWriter sw = new StringWriter();

			BeanWriter out = BeanIOUtils.createWriter(mappingFile, rootTag, sw);
			out.write(recordTag, objAutomaticSendType);
			out.flush();
			out.close();

			objFile = sw.toString().getBytes("UTF-8");
			sw.flush();
			sw.close();
			mappingFile.close();
		} catch (BeanIOConfigurationException | BeanWriterException | IOException e) {
			e.printStackTrace();
		}

		return objFile;
	}

	/** Generate interface lip response file.
	 * 
	 * @param objAutomaticSendType the obj automatic send type
	 * @param mappingResponse the mapping response
	 * @param rootTag the root tag
	 * @param recordTag the record tag
	 * @return the byte[] */
	public static byte[] generateMoneyExchangeResponseFile(MoneyExchangeTO moneyExchangeTO, String mappingResponse,
			String rootTag, String recordTag) {
		byte[] objFile = null;
		try {
			InputStream mappingFile = Thread.currentThread().getContextClassLoader().getResourceAsStream(mappingResponse);
			StringWriter sw = new StringWriter();

			BeanWriter out = BeanIOUtils.createWriter(mappingFile, rootTag, sw);
			out.write(recordTag, moneyExchangeTO);
			out.flush();
			out.close();

			objFile = sw.toString().getBytes("UTF-8");
			sw.flush();
			sw.close();
			mappingFile.close();
		} catch (BeanIOConfigurationException | BeanWriterException | IOException e) {
			e.printStackTrace();
		}

		return objFile;
	}

	/** Bytes to string.
	 * 
	 * @param _bytes the _bytes
	 * @return the string */
	public static String bytesToString(byte[] _bytes) {
		String file_string = "";

		for (int i = 0; i < _bytes.length; i++) {
			file_string += (char) _bytes[i];
		}

		return file_string;
	}

	/** Generate xml file.
	 * 
	 * @param listObjectValidations the list object validations
	 * @param nameFile the name file
	 * @param schemeName the scheme name
	 * @param rootTag the root tag
	 * @param rowTag the row tag
	 * @return the file
	 * @throws IOException Signals that an I/O exception has occurred. */
	public static File generateXmlFile(List<Object> listObjectValidations, String nameFile, String schemeName, String rootTag,
			String rowTag) throws IOException {

		byte[] responseFile = null;
		if (nameFile.endsWith(GeneralConstants.XML_FORMAT)) {
			responseFile = CommonsUtilities.generateInterfaceFile(listObjectValidations, schemeName, rootTag, rowTag);
		} else {

			responseFile = CommonsUtilities.generateInterfaceFile(listObjectValidations, schemeName, rootTag, null);
		}

		File file = new File(nameFile);
		FileUtils.writeByteArrayToFile(file, responseFile);
		return file;
	}

	/** Complete left zero.
	 * 
	 * @param code the code
	 * @param maxLength the max length
	 * @return the string */
	public static String completeLeftZero(String code, int maxLength) {
		String aux = "";
		String lastCode = "";
		if (code.length() < maxLength) {
			for (int i = 0; i < maxLength - code.length(); i++) {

				aux = aux + "0";
			}
			lastCode = aux + code;

		} else {
			lastCode = code;
		}

		return lastCode;
	}

	/** Truncate decimal.
	 * 
	 * @param bdNumberBigDecimal the bd number big decimal
	 * @param inNumberDecimals the in number decimals
	 * @return the big decimal */
	public static BigDecimal truncateDecimal(BigDecimal bdNumberBigDecimal, int inNumberDecimals) {
		if (bdNumberBigDecimal != null) {
			if (bdNumberBigDecimal.compareTo(BigDecimal.ZERO) > 0) {
				return new BigDecimal(String.valueOf(bdNumberBigDecimal)).setScale(inNumberDecimals, BigDecimal.ROUND_FLOOR);
			} else {
				return new BigDecimal(String.valueOf(bdNumberBigDecimal)).setScale(inNumberDecimals, BigDecimal.ROUND_CEILING);
			}
		}
		return null;
	}

	/** Metodo que permite obtener la representacion de ascci de una cadena, la misma se obtiene caracter a caracter.
	 * 
	 * @param cadenaString the cadena string
	 * @return the string
	 * @throws Exception the exception */
	public static String obtenerAscciCadena(String cadenaString) throws Exception {
		StringBuilder cadenaAscii = new StringBuilder();
		if (cadenaString != null && cadenaString.length() > 0) {
			for (char caracter : cadenaString.toCharArray()) {
				int asciiNumber = caracter;
				if (asciiNumber >= 48 && asciiNumber <= 57) {
					cadenaAscii.append(caracter);
				} else {
					cadenaAscii.append(asciiNumber);
				}
			}
		} else {
			throw new Exception("Cadena nula o vacia, verifique por favor.");
		}
		return cadenaAscii.toString();
	}

	/** Metodo que realiza la obtencion de un codigo verificador en modulo 11.
	 * 
	 * @param cadena the cadena
	 * @return the int */
	public static int obtenerSumaPorDigitos(String cadena) {
		int pivote = 2;
		int longitudCadena = cadena.length();
		int cantidadTotal = 0;
		int b = 1;
		for (int i = 0; i < longitudCadena; i++) {
			if (pivote == 8) {
				pivote = 2;
			}
			int temporal = Integer.parseInt("" + cadena.substring(i, b));
			b++;
			temporal *= pivote;
			pivote++;
			cantidadTotal += temporal;
		}
		cantidadTotal = 11 - cantidadTotal % 11;
		return cantidadTotal;
	}

	/** Metodo que realiza la obtencion del codigo modulo 11 de una cadena.
	 * 
	 * @param cadena the cadena
	 * @return codigo de control modulo 11 */
	public static String obtenerModulo11(String cadena) {
		String codigoModulo11String = "0";
		try {
			cadena = obtenerAscciCadena(cadena);
			int codigoModulo11 = obtenerSumaPorDigitos(invertirCadena(cadena));

			if (codigoModulo11 == 10) {
				codigoModulo11String = "K";
			} else if (codigoModulo11 == 11) {
				// para el caso de resto 11 se devuelve el valor 0
				codigoModulo11String = "0";
			} else {
				codigoModulo11String = String.valueOf(codigoModulo11);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return codigoModulo11String;
	}

	/** Metodo que realiza la obtencion del codigo modulo 11 de un objeto tipo date.
	 * 
	 * @param fecha the fecha
	 * @return the int */
	public static String obtenerModulo11(Date fecha) {
		String codigoModulo11String = "0";
		String cadena = convertDateToString(fecha);
		int codigoModulo11 = obtenerSumaPorDigitos(invertirCadena(cadena));
		// para el caso de resto 10 se devuelve el caracter K
		if (codigoModulo11 == 10) {
			codigoModulo11String = "K";
		} else if (codigoModulo11 == 11) {
			// para el caso de resto 11 se devuelve el valor 0
			codigoModulo11String = "0";
		} else {
			codigoModulo11String = String.valueOf(codigoModulo11);
		}
		return codigoModulo11String;
	}

	/** Metodo que realiza la conversion de un objeto String a su correspondiente representacion en cadena.
	 * 
	 * @param fecha the fecha
	 * @return the string */
	public static String convertDateToString(Date fecha) {
		String fechaCadena = "";
		String pattern = "ddMMyyyy";
		if (fecha != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);

			fechaCadena = sdf.format(fecha);
		}
		return fechaCadena;
	}

	/** Metodo que invierte una cadena.
	 * 
	 * @param cadena the cadena
	 * @return the string */
	public static String invertirCadena(String cadena) {
		String cadenaInvertida = "";
		for (int x = cadena.length() - 1; x >= 0; x--) {
			cadenaInvertida = cadenaInvertida + cadena.charAt(x);
		}
		return cadenaInvertida;
	}

	/** Convert list object to string.
	 * 
	 * @param lstObjects the lst objects
	 * @param separator the separator
	 * @return the string */
	public static String convertListObjectToString(List<Object> lstObjects, String separator) {

		StringBuilder sbString = new StringBuilder();
		for (int i = 0; i < lstObjects.size(); i++) {
			sbString.append(lstObjects.get(i).toString());
			if (i < (lstObjects.size() - 1)) {
				sbString.append(separator);
			}
		}

		return sbString.toString();
	}

	/** Gets the true extension file upload.
	 * 
	 * @param fileUpload the file upload
	 * @return the true extension file upload */
 	public static String getTrueExtensionFileUpload(InputStream fileUpload){
		 String fileType=null;
	      try
	      {
	         fileType = defaultTika.detect(fileUpload);
	         fileType = StringUtils.substringAfter(fileType, "/");
	         
	         if(GeneralConstants.TIKA_FORMAT_2007_OFFICE.equals(fileType)){
	        	return GeneralConstants.EXCEL_FORMAT_2007; 
	         }else if(GeneralConstants.TIKA_FORMAT_2003_OFFICE.equals(fileType)){
	        	 return GeneralConstants.EXCEL_FORMAT_2003;
	         }else if(GeneralConstants.FORMART_CERTIFICATE.equals(fileType)){
	        	 return GeneralConstants.FORMART_CERTIFICATE_PFX;
	         }
	      }
	      catch (IOException ioEx)
	      {
	         fileType = "Unknown";
	      }
	      return fileType;
	 }

	public static int countNumberParametersNotification(String strMessage) {
		int count = 0;
		if (Validations.validateIsNotNullAndNotEmpty(strMessage)) {
			char[] arrayChar = strMessage.toCharArray();
			for (int i = 0; i < arrayChar.length; i++) {
				if (arrayChar[i] == '%') {
					count++;
				}
			}
		}
		return count;
	}

	/** Convert dateto string optional.
	 * 
	 * @param templateDate the template date
	 * @return the string */
	public static String convertDateToStringReport(Date templateDate) {
		String strDate = null;
		SimpleDateFormat simpleFormat = new SimpleDateFormat(DATE_PATTERN_BILLING);
		try {
			if (templateDate != null) {
				strDate = simpleFormat.format(templateDate);
			}
		} catch (Exception ex) {

		}
		return strDate;
	}

	/** Current date.
	 * 
	 * @return the date */
	public static Date formatDateFromObject(Object objDate) {
		try {
			DateFormat dfDate = new SimpleDateFormat(DATE_PATTERN);
			String strCurrentDate = dfDate.format(objDate);
			Date currentDate = dfDate.parse(strCurrentDate);
			return currentDate;
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	/**
	 * Obtiene la extencion de un file
	 * @param nombreArchivo
	 * @return
	 */
	public static String getExtension(String nombreArchivo) {
		if (Validations.validateIsNullOrEmpty(nombreArchivo)) {
			return null;
		}
		nombreArchivo = nombreArchivo.trim();
		String[] strArr = nombreArchivo.split("\\.");
		if (strArr.length == 0) {
			return null;
		}
		return strArr[(strArr.length - 1)];
	}
	
	/**
	 * Obtiene el tamanio de de un file
	 * @param fileSize
	 * @return
	 */
	public static String sizeFileString(Long fileSize) {
		Long kilo = new Long("1024");
		StringBuilder sb = new StringBuilder();
		if (fileSize == null || fileSize == 0) {
			fileSize = new Long("0");
		}
		if (fileSize > kilo) {
			if (fileSize > (kilo * kilo)) {
				sb.append((fileSize / (kilo * kilo)));
				sb.append("MB");
			} else {
				sb.append((fileSize / kilo));
				sb.append("KB");
			}
		} else {
			sb.append((fileSize));
			sb.append("Bytes");
		}
		return sb.toString();
	}
  
	/**
	 * Obtiene los tipos de files permitidos
	 * @param tiposArchivoPermitidos
	 * @return
	 */
	public static String allowedFileString(String tiposArchivoPermitidos) {
		if (Validations.validateIsNullOrEmpty(tiposArchivoPermitidos)) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		String[] parts = tiposArchivoPermitidos.split(",");
		boolean primero = true;
		for (int i = 0; i < parts.length; i++) {
			if (primero) {
				primero = false;
			} else {
				sb.append(", ");
			}
			sb.append(parts[i]);
		}
		return sb.toString();
	}
  
	/**
	 * Obtiene la dimension de una imagen alto por ancho
	 * @param dimensionImage
	 * @return
	 */
	public static String dimensionImageString(String dimensionImage) {
		if (Validations.validateIsNullOrEmpty(dimensionImage)) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		String[] parts = dimensionImage.split(",");
		sb.append(parts[0]);
		sb.append(" x ");
		sb.append(parts[1]);
		sb.append(" pixeles");
		return sb.toString();
	}
  
	/**
	 * Convierte un listado en cadena a un array
	 * @param listString
	 * @return
	 */
	public static List<String> toList(String listString) {
		if (Validations.validateIsNullOrEmpty(listString)) {
			return new ArrayList<>();
		}
		listString = listString.trim();
		String[] strArr = listString.split(",");
		return Arrays.asList(strArr);
	}
	
	public void generateXMLToXLSX(byte[] processFile, Integer sheet, Map<Integer, String> mapCellNumberWithDataType) throws IOException, ParseException {
		
 		XSSFWorkbook book = new XSSFWorkbook(new ByteArrayInputStream(processFile));
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size();//cellFirst = 0, 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
					
					cell = row.getCell(cellPosition);
					String dataType = mapCellNumberWithDataType.get(cellPosition);
					if(dataType == "String") {
						getCellString(cell);
					}else if(dataType == "Date") {
						getCellDate(cell,DATE_PATTERN);
					}else if(dataType == "Integer") {
						getCellInteger(cell);
					}else if(dataType == "BigDecimal") {
						getCellBigDecimal(cell);
					}
					
				}
			}
		}
		
	}
	
	public static String getCellGenericInString(XSSFCell cell, String dataType) throws ParseException {
		String cellValue = null;
		try {
		
			if(cell!=null) {
				if(dataType.equals("String")) {
					cellValue = getCellString(cell);
				}else if(dataType.equals("Date")) {
					Date cellDate = getCellDate(cell,DATE_PATTERN);
					if(cellDate!=null) {
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonsUtilities.DATE_PATTERN);
						cellValue = simpleDateFormat.format(cellDate);
					}
				}else if(dataType.equals("Integer")) {
					Integer cellInteger = getCellInteger(cell);
					cellValue = (cellInteger != null)?cellInteger.toString():null;
				}else if(dataType.equals("BigDecimal")) {
					BigDecimal cellBigDecimal = getCellBigDecimal(cell);
					cellValue = (cellBigDecimal != null)?""+cellBigDecimal.intValue():null;
				}
			}
			
		}catch (Exception e) {
			System.out.println("CELL: "+cell+" - DATATYPE: "+dataType);
		}
		
		return cellValue;
	}

	public static List<ValidationAccountAnnotationCupon> setXLSXToEntityAccountAnnotationCupon(String certificateNumber, String serialNumber,InputStream processFile) throws IOException, ParseException {
		
		Integer sheet = 0;
		Map<Integer, String> mapCellNumberWithDataType = new HashedMap<Integer, String>();
 		XSSFWorkbook book = new XSSFWorkbook(processFile);
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size(); 
		
		mapCellNumberWithDataType = new HashedMap<Integer, String>();
		/*mapCellNumberWithDataType.put(0, "String");//CERTIFICADO
		mapCellNumberWithDataType.put(1, "String");//SERIE
		*/
		mapCellNumberWithDataType.put(0, "Integer");//NRO CUPON
		mapCellNumberWithDataType.put(1, "Date");//F INICIO
		mapCellNumberWithDataType.put(2, "Date");//F VENCIMIENTO
		mapCellNumberWithDataType.put(3, "Date");//F PAGO
		mapCellNumberWithDataType.put(4, "BigDecimal");//TASA
		mapCellNumberWithDataType.put(5, "BigDecimal");//MONTO
		
		ValidationAccountAnnotationCupon validationAccountAnnotationCupon = null;
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon = new ArrayList<ValidationAccountAnnotationCupon>();
		
		sheet = 0;
		sheetOperation = book.getSheetAt(sheet);
		lastRow = sheetOperation.getLastRowNum();
		lastCell = mapCellNumberWithDataType.size(); 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationCupon = new ValidationAccountAnnotationCupon();
					validationAccountAnnotationCupon.setCertificateNumber(certificateNumber);
					validationAccountAnnotationCupon.setSerialNumber(serialNumber);
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);

						if(cellPosition == 0) {
							validationAccountAnnotationCupon.setNroCupon(cellValue);
						}else if(cellPosition == 1) {
							validationAccountAnnotationCupon.setBeginingDate(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationCupon.setExpirationDate(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationCupon.setPaymentDate(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationCupon.setInterestRate(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationCupon.setCuponAmount(cellValue);
						}
						
					}
					lstValidationAccountAnnotationCupon.add(validationAccountAnnotationCupon);
				}
			}
		}
		
		return lstValidationAccountAnnotationCupon;
		
	}
	
	
	public static List<ValidationAccountAnnotationOperation> setXLSXToEntityAccountAnnotation(InputStream processFile,Long idParticipantPk) throws IOException, ParseException, Exception {
		
		List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations = new ArrayList<ValidationAccountAnnotationOperation>();
		ValidationAccountAnnotationOperation validationAccountAnnotationOperation = null;
		
		Integer sheet = 0;
		Map<Integer, String> mapCellNumberWithDataType = new HashedMap<Integer, String>();
		//	mapCellNumberWithDataType.put(0, "String");//NEMONICO DEPOSITANTE	//0
		mapCellNumberWithDataType.put(0, "String");//NUMERO CUENTA				//1
		mapCellNumberWithDataType.put(1, "String");//SERIE						//2
		mapCellNumberWithDataType.put(2, "String");//CERTIFICADO				//3
		mapCellNumberWithDataType.put(3, "String");//NEMONICO EMISOR			//4
		mapCellNumberWithDataType.put(4, "Integer");//VALOR NOMINAL				//5
		mapCellNumberWithDataType.put(5, "String");//MONEDA						//6
		mapCellNumberWithDataType.put(6, "BigDecimal");//TASA ANUAL				//7
		mapCellNumberWithDataType.put(7, "Date");//FECHA EMISION				//8
		mapCellNumberWithDataType.put(8, "Date");//FECHA VENCIMIENTO			//9
		mapCellNumberWithDataType.put(9, "String");//PERIDIOCIDAD				//10
		mapCellNumberWithDataType.put(10, "String");//TIENE CUPONES ELECTRONICO //11
		
 		XSSFWorkbook book = new XSSFWorkbook(processFile);
		XSSFSheet sheetOperation = book.getSheetAt(sheet);
		XSSFRow row; XSSFCell cell;
		int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
		int lastCell = mapCellNumberWithDataType.size();//cellFirst = 0, 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationOperation = new ValidationAccountAnnotationOperation();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);

						validationAccountAnnotationOperation.setParticipant(idParticipantPk.toString());	
						
						if(cellPosition == 0) {
							validationAccountAnnotationOperation.setHolderAccountNumber(cellValue);
						}else if(cellPosition == 1) {
							validationAccountAnnotationOperation.setSerialNumber(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationOperation.setCertificateNumber(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationOperation.setMnemonicIssuer(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationOperation.setSecurityNominalValue(cellValue);
						}else if(cellPosition == 5) {
							validationAccountAnnotationOperation.setSecurityCurrency(cellValue);
						}else if(cellPosition == 6) {
							validationAccountAnnotationOperation.setSecurityInterestRate(cellValue);
						}else if(cellPosition == 7) {
							validationAccountAnnotationOperation.setExpeditionDate(cellValue);
						}else if(cellPosition == 8) {
							validationAccountAnnotationOperation.setSecurityExpirationDate(cellValue);
						}else if(cellPosition == 9) {
							validationAccountAnnotationOperation.setPeriodicity(cellValue);
						}else if(cellPosition == 10) {
							validationAccountAnnotationOperation.setIndElectronicCupon(cellValue);
						}
					}
					lstAccountAnnotationOperations.add(validationAccountAnnotationOperation);
				}
			}
			
		}
		
		//**************
		mapCellNumberWithDataType = new HashedMap<Integer, String>();
		mapCellNumberWithDataType.put(0, "String");//SERIE			0
		mapCellNumberWithDataType.put(1, "String");//CERTIFICADO	1
		mapCellNumberWithDataType.put(2, "Integer");//NRO CUPON		2
		//mapCellNumberWithDataType.put(3, "Date");//F INICIO		3
		mapCellNumberWithDataType.put(3, "Date");//F VENCIMIENTO	4
		//mapCellNumberWithDataType.put(5, "Date");//F PAGO			5
		//mapCellNumberWithDataType.put(6, "BigDecimal");//TASA		6
		mapCellNumberWithDataType.put(4, "BigDecimal");//MONTO		7
		
		ValidationAccountAnnotationCupon validationAccountAnnotationCupon = null;
		List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon = new ArrayList<ValidationAccountAnnotationCupon>();
		
		sheet = 1;
		sheetOperation = book.getSheetAt(sheet);
		lastRow = sheetOperation.getLastRowNum();
		lastCell = mapCellNumberWithDataType.size(); 
		
		for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
			
			row = sheetOperation.getRow(filaExtrac);
			if (Validations.validateIsNotNull(row)) {
				if(!isRowEmpty(row)) {
					validationAccountAnnotationCupon = new ValidationAccountAnnotationCupon();
					for(int cellPosition=0; cellPosition<= lastCell; cellPosition++) {
						
						cell = row.getCell(cellPosition);
						String dataType = mapCellNumberWithDataType.get(cellPosition);
						String cellValue = getCellGenericInString(cell, dataType);
	
						if(cellPosition == 0) {
							validationAccountAnnotationCupon.setSerialNumber(cellValue);
						}else if(cellPosition == 1) {
							validationAccountAnnotationCupon.setCertificateNumber(cellValue);
						}else if(cellPosition == 2) {
							validationAccountAnnotationCupon.setNroCupon(cellValue);
						}else if(cellPosition == 3) {
							validationAccountAnnotationCupon.setExpirationDate(cellValue);
						}else if(cellPosition == 4) {
							validationAccountAnnotationCupon.setCuponAmount(cellValue);
						}
						Date paymentDate = CommonsUtilities.addDaysHabilsToDate( CommonsUtilities.convertStringtoDate(validationAccountAnnotationCupon.getExpirationDate(), DATE_PATTERN), 1);
						
						validationAccountAnnotationCupon.setBeginingDate(cellValue);
						validationAccountAnnotationCupon.setPaymentDate(cellValue);
						validationAccountAnnotationCupon.setInterestRate(cellValue);
						
					}
					lstValidationAccountAnnotationCupon.add(validationAccountAnnotationCupon);
				}
			}
		}
		
		return reordenValidationAccountAnnotationOperation(lstAccountAnnotationOperations, lstValidationAccountAnnotationCupon);
		
	}
	

	
	public static List<ValidationAccountAnnotationOperation> reordenValidationAccountAnnotationOperation(List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperations, 
																										 List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon){
		
		List<ValidationAccountAnnotationOperation> lstOperationWithCuponReturn = null;
		if(lstAccountAnnotationOperations != null) {
			lstOperationWithCuponReturn = new ArrayList<ValidationAccountAnnotationOperation>();
			 
			for(ValidationAccountAnnotationOperation operation: lstAccountAnnotationOperations) {
				for(ValidationAccountAnnotationCupon cupon: lstValidationAccountAnnotationCupon) {
					if(isCuponInAccountAnnotationOperation(operation, cupon)) {
						if(operation.getValidationAccountAnnotationCupons() == null) {
							operation.setValidationAccountAnnotationCupons(new ArrayList<ValidationAccountAnnotationCupon>());
						}
						operation.getValidationAccountAnnotationCupons().add(cupon);
					}
				}
				lstOperationWithCuponReturn.add(operation);
			}
		}
		
		return lstOperationWithCuponReturn;
		
	}
	
	public static Boolean isCuponInAccountAnnotationOperation(ValidationAccountAnnotationOperation operation, ValidationAccountAnnotationCupon cupon) {
		if(	   operation.getCertificateNumber()!=null && !operation.getCertificateNumber().equals("")
			&& operation.getSerialNumber()!=null && !operation.getSerialNumber().equals("")
			&& cupon.getCertificateNumber()!=null && !operation.getCertificateNumber().equals("")
			&& cupon.getSerialNumber()!=null && !operation.getSerialNumber().equals("")
		) {
			if( operation.getCertificateNumber().equalsIgnoreCase(cupon.getCertificateNumber()) &&
				operation.getSerialNumber().equalsIgnoreCase(cupon.getSerialNumber())	
			) {
				return true;
			}
		}
		return false;
	}
	
	 private static boolean isRowEmpty(Row row) {
		    if (row == null) {
		        return true;
		    }
		    if (row.getLastCellNum() <= 0) {
		        return true;
		    }
		    for (int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++) {
		        Cell cell = row.getCell(cellNum);
		        if (cell != null && cell.getCellType() != XSSFCell.CELL_TYPE_BLANK && StringUtils.isNotBlank(cell.toString())) {
		            return false;
		        }
		    }
		    return true;
	}
	 
	public static String getCellString(XSSFCell cell) {
		String returnStringCell = "";
		Object returnValue = null;
		BigDecimal bigDecimal = null;
		switch (cell.getCellType()) {
			case XSSFCell.CELL_TYPE_NUMERIC:
				returnValue = cell.getNumericCellValue();
				bigDecimal = new BigDecimal(returnValue.toString());
				returnValue = bigDecimal.intValue();
				break;
			case XSSFCell.CELL_TYPE_STRING:
				returnValue = cell.getStringCellValue();
				break;
			case XSSFCell.CELL_TYPE_BLANK:
				returnValue = "";
				break;	
			case XSSFCell.CELL_TYPE_FORMULA:
				returnValue = cell.getNumericCellValue();
				bigDecimal = new BigDecimal(returnValue.toString());
				returnValue = bigDecimal.intValue();
				break;
			default:
				returnStringCell = cell.getStringCellValue();
				break;
		}
		
		if(returnValue!=null) {
			returnStringCell = returnValue.toString();
		}
		  
		return returnStringCell;
	}
	
	public static Date getCellDate(XSSFCell cell, String dateFormat) throws ParseException {
		dateFormat = (dateFormat ==null)? DATE_PATTERN : dateFormat;// : DATE_PATTERN_EN
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Object cellObject = null;
		Date date = null;
		
		switch (cell.getCellType()) {
			case XSSFCell.CELL_TYPE_NUMERIC:
				date = cell.getDateCellValue();
				break;
			case XSSFCell.CELL_TYPE_FORMULA:
				date = cell.getDateCellValue();
				break;	
			case XSSFCell.CELL_TYPE_STRING:
				cellObject = cell.getStringCellValue();
				String tmpValue = ""+cellObject;
				if(!tmpValue.matches("[a-zA-Z]+")) {
					date = (cellObject != null) ? simpleDateFormat.parse(tmpValue) : null;
				}
				break;
		}
		
		return date; 
	}
	
	public static Integer getCellInteger(XSSFCell cell) {
		Integer returnIntegerCell = null;
		Object returnValue = null;
		switch (cell.getCellType()) {
			case XSSFCell.CELL_TYPE_NUMERIC:
				returnValue = cell.getNumericCellValue();
				BigDecimal bigDecimal = new BigDecimal(returnValue.toString());
				returnValue = bigDecimal.intValue();
				break;
			case XSSFCell.CELL_TYPE_STRING:
				returnValue = cell.getStringCellValue();
				break;
			case XSSFCell.CELL_TYPE_FORMULA:
				returnValue = cell.getNumericCellValue();
				break;
		}
		
		if(returnValue!=null && !returnValue.equals("")) {
			String tmpValue = returnValue.toString();
			if(!tmpValue.matches("[a-zA-Z]+")) {
				BigDecimal currentBD = new BigDecimal(returnValue.toString());
				returnIntegerCell = currentBD.intValue();
			}
		}
		  
		return returnIntegerCell;
	}

	
	public static BigDecimal getCellBigDecimal(XSSFCell cell) {
		BigDecimal returnBigDecimalCell = null;
		Object returnValue = null;
		switch (cell.getCellType()) {
			case XSSFCell.CELL_TYPE_NUMERIC:
				returnValue = cell.getNumericCellValue();
				break;
			case XSSFCell.CELL_TYPE_STRING:
				returnValue = cell.getStringCellValue();
				break;
			case XSSFCell.CELL_TYPE_FORMULA:
				returnValue = cell.getNumericCellValue();
				break;
		}
		
		if(returnValue!=null && !returnValue.equals("")) {
			String tmpValue = returnValue.toString();
			if(!tmpValue.matches("[a-zA-Z]+")) {
				returnBigDecimalCell = new BigDecimal(returnValue.toString());
			}
		}
		  
		return returnBigDecimalCell;
	}
	
	public static void convertAccountAnnotationOperationXLStoXML(ProcessFileTO processFileTO){
 		try {
		 		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder builder = factory.newDocumentBuilder();
		        DOMImplementation implementation = builder.getDOMImplementation();

		 		File fileTemp = File.createTempFile("tempMcnfile", ".tmp");
		 		
		 		
		 		XSSFWorkbook book = new XSSFWorkbook(new ByteArrayInputStream(processFileTO.getProcessFile()));
				XSSFSheet sheetOperation = book.getSheetAt(0);
				XSSFRow row; XSSFCell cell;
				int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
				int cellFirst = 0, lastCell = 15;
				
				for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
					row = sheetOperation.getRow(filaExtrac);
					if (Validations.validateIsNotNull(row)) {
						
						//cell = row.getCell(col0_Cui);
						
					}
				}
				
				
				/*
		 		HSSFWorkbook workBookExcel = new HSSFWorkbook(new ByteArrayInputStream(processFileTO.getProcessFile()));
	            HSSFSheet workSheetAccountAnnotationOperation = workBookExcel.getSheetAt(0);
	            Iterator<Row> rowIterator = workSheetAccountAnnotationOperation.iterator();
	            
	            while(rowIterator.hasNext()) {
                    Row row = rowIterator.next(); 
                    Iterator<Cell> cellIterator = row.cellIterator();
                            while(cellIterator.hasNext()) {
                            	
                                    Cell cell = cellIterator.next(); //Fetch CELL
                                    switch(cell.getCellType()) { //Identify CELL type
                                            
                                    case Cell.CELL_TYPE_STRING:
                                            
                                            break;
                                    }
                                    
                            }
	            }
	            */
	            
	            
	            
				//Document documento = implementation.createDocument(null, "ROWSET", null);
		        //documento.setXmlVersion("1.0");
			        
		        //if( processFileTO.getValidationProcessTO() != null && processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation() != null && processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation().size() > 0 ) {
		       /* if( processFileTO.getProcessFile() != null ) {
		        	for( int i=0; i< processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation().size() ; i++) {
		        		
			        		ValidationAccountAnnotationOperation mcnObject = processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation().get(i);
							Element operation = documento.createElement("ROW");
							operation.setAttribute("NUM", (i+1)+"" );
							createTagXML(operation, documento, mcnObject.getCertificateNumber(), "CERTIFICATE_NUMBER");
							createTagXML(operation, documento, mcnObject.getSerialNumber(), "SERIAL_NUMBER");
							createTagXML(operation, documento, mcnObject.getMnemonicIssuer(), "MENOMIC_ISSUER");
							createTagXML(operation, documento, mcnObject.getSecurityClass(), "SECURITY_CLASS");
							createTagXML(operation, documento, mcnObject.getSecurityNominalValue(), "NOMINAL_VALUE");
							createTagXML(operation, documento, mcnObject.getTotalBalance(), "TOTAL_BALANCE");//CANTIDAD DE VALORES
							createTagXML(operation, documento, mcnObject.getSecurityCurrency(), "CURRENCY");
							createTagXML(operation, documento, mcnObject.getMotive(), "MOTIVO");
							//createTagXML(operation, documento, mcnObject.getBranchOffice(), "BRANCH_OFFICE");
							createTagXML(operation, documento, mcnObject.getSecurityInterestType(), "INTEREST_TYPE");
							createTagXML(operation, documento, mcnObject.getSecurityInterestRate(), "INTEREST_RATE");
							createTagXML(operation, documento, mcnObject.getExpeditionDate(), "EXPEDITION_DATE");
							createTagXML(operation, documento, mcnObject.getSecurityExpirationDate(), "EXPIRATION_DATE");
							createTagXML(operation, documento, mcnObject.getPeriodicity(), "PERIDIOCITY");
							createTagXML(operation, documento, mcnObject.getIndElectronicCupon(), "IND_ELECTRONIC_CUPON");
							createTagXML(operation, documento, mcnObject.getMnemonicParticipant(), "MNEMONIC_PARTICIPANT");
							createTagXML(operation, documento, mcnObject.getHolderAccountNumber(), "ACCOUNT_NUMBER");
							documento.getDocumentElement().appendChild(operation);
							
		        	}
		        	
		        }else {
					processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
					RecordValidationType fe = new RecordValidationType();
					fe.setErrorSimpleDetail("Estructura de archivo inv\\u00e1lida.");
					processFileTO.getValidationProcessTO().getLstValidations().add(fe);
				}
				*/
				// Asocio el source con el Document
				//Source source = new DOMSource(documento);
				// Creo el Result, indicado que fichero se va a crear
				Result result = new StreamResult(fileTemp);
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        //transformer.transform(source, result);
				if(!Validations.validateListIsNullOrEmpty(processFileTO.getValidationProcessTO().getLstValidations())){
					FileUtils.deleteDirectory(fileTemp);
				}else {
					processFileTO.setTempProcessFile(fileTemp);
				}
				
 		}catch(IOException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		}catch(TransformerException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		}catch(ParserConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		}
 	}
	
	public static void createTagXML(Element element, Document document, String fieldValue, String tagXML){
		if(fieldValue != null)
			fieldValue = fieldValue.replace("\"", "'");
		Element tempElement = document.createElement(tagXML);
		tempElement.appendChild(document.createTextNode(fieldValue));
		element.appendChild(tempElement);
	}
	
	public static Integer diffDatesInDays(Date firstDate, Date secondDate) {

        long diff = secondDate.getTime() - firstDate.getTime();

        TimeUnit time = TimeUnit.DAYS; 
        long diffrence = time.convert(diff, TimeUnit.MILLISECONDS);
        if(  !Objects.isNull(diffrence) ) {
        	return new Integer(diffrence+"");
        }        
        return 0;

    }
	

	public static void convertOperationMCNXLStoXML(ProcessFileTO processFileTO){
 		try {
		 		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder builder = factory.newDocumentBuilder();
		        DOMImplementation implementation = builder.getDOMImplementation();

		 		File fileTemp = File.createTempFile("tempMcnfile", ".tmp");
		 		
		 		
		 		XSSFWorkbook book = new XSSFWorkbook(new ByteArrayInputStream(processFileTO.getProcessFile()));
				XSSFSheet sheetOperation = book.getSheetAt(0);
				XSSFRow row; XSSFCell cell;
				int rowFirst = 1, lastRow = sheetOperation.getLastRowNum();
				int cellFirst = 0, lastCell = 15;
				
				for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
					row = sheetOperation.getRow(filaExtrac);
					if (Validations.validateIsNotNull(row)) {
						
						//cell = row.getCell(col0_Cui);
						
					}
				}
				
				
				/*
		 		HSSFWorkbook workBookExcel = new HSSFWorkbook(new ByteArrayInputStream(processFileTO.getProcessFile()));
	            HSSFSheet workSheetAccountAnnotationOperation = workBookExcel.getSheetAt(0);
	            Iterator<Row> rowIterator = workSheetAccountAnnotationOperation.iterator();
	            
	            while(rowIterator.hasNext()) {
                    Row row = rowIterator.next(); 
                    Iterator<Cell> cellIterator = row.cellIterator();
                            while(cellIterator.hasNext()) {
                            	
                                    Cell cell = cellIterator.next(); //Fetch CELL
                                    switch(cell.getCellType()) { //Identify CELL type
                                            
                                    case Cell.CELL_TYPE_STRING:
                                            
                                            break;
                                    }
                                    
                            }
	            }
	            */
	            
	            
	            
				//Document documento = implementation.createDocument(null, "ROWSET", null);
		        //documento.setXmlVersion("1.0");
			        
		        //if( processFileTO.getValidationProcessTO() != null && processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation() != null && processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation().size() > 0 ) {
		       /* if( processFileTO.getProcessFile() != null ) {
		        	for( int i=0; i< processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation().size() ; i++) {
		        		
			        		ValidationAccountAnnotationOperation mcnObject = processFileTO.getValidationProcessTO().getLstAccountAnnotationOperation().get(i);
							Element operation = documento.createElement("ROW");
							operation.setAttribute("NUM", (i+1)+"" );
							createTagXML(operation, documento, mcnObject.getCertificateNumber(), "CERTIFICATE_NUMBER");
							createTagXML(operation, documento, mcnObject.getSerialNumber(), "SERIAL_NUMBER");
							createTagXML(operation, documento, mcnObject.getMnemonicIssuer(), "MENOMIC_ISSUER");
							createTagXML(operation, documento, mcnObject.getSecurityClass(), "SECURITY_CLASS");
							createTagXML(operation, documento, mcnObject.getSecurityNominalValue(), "NOMINAL_VALUE");
							createTagXML(operation, documento, mcnObject.getTotalBalance(), "TOTAL_BALANCE");//CANTIDAD DE VALORES
							createTagXML(operation, documento, mcnObject.getSecurityCurrency(), "CURRENCY");
							createTagXML(operation, documento, mcnObject.getMotive(), "MOTIVO");
							//createTagXML(operation, documento, mcnObject.getBranchOffice(), "BRANCH_OFFICE");
							createTagXML(operation, documento, mcnObject.getSecurityInterestType(), "INTEREST_TYPE");
							createTagXML(operation, documento, mcnObject.getSecurityInterestRate(), "INTEREST_RATE");
							createTagXML(operation, documento, mcnObject.getExpeditionDate(), "EXPEDITION_DATE");
							createTagXML(operation, documento, mcnObject.getSecurityExpirationDate(), "EXPIRATION_DATE");
							createTagXML(operation, documento, mcnObject.getPeriodicity(), "PERIDIOCITY");
							createTagXML(operation, documento, mcnObject.getIndElectronicCupon(), "IND_ELECTRONIC_CUPON");
							createTagXML(operation, documento, mcnObject.getMnemonicParticipant(), "MNEMONIC_PARTICIPANT");
							createTagXML(operation, documento, mcnObject.getHolderAccountNumber(), "ACCOUNT_NUMBER");
							documento.getDocumentElement().appendChild(operation);
							
		        	}
		        	
		        }else {
					processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
					RecordValidationType fe = new RecordValidationType();
					fe.setErrorSimpleDetail("Estructura de archivo inv\\u00e1lida.");
					processFileTO.getValidationProcessTO().getLstValidations().add(fe);
				}
				*/
				// Asocio el source con el Document
				//Source source = new DOMSource(documento);
				// Creo el Result, indicado que fichero se va a crear
				Result result = new StreamResult(fileTemp);
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        //transformer.transform(source, result);
				if(!Validations.validateListIsNullOrEmpty(processFileTO.getValidationProcessTO().getLstValidations())){
					FileUtils.deleteDirectory(fileTemp);
				}else {
					processFileTO.setTempProcessFile(fileTemp);
				}
				
 		}catch(IOException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		}catch(TransformerException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		}catch(ParserConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		}
 	}
	

	public static void operationMCNconvertXLStoXML(ProcessFileTO processFileTO){
		
 		
 		try {
 			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
 	        DocumentBuilder builder = factory.newDocumentBuilder();
 	        DOMImplementation implementation = builder.getDOMImplementation();
 	 		File fileTemp = null;
 	 		Integer pos = 0;
 	 		
 			FileInputStream fileInputStream = new FileInputStream(processFileTO.getTempProcessFile().getPath());
 			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
 			XSSFSheet datatypeSheet = workbook.getSheetAt(0);
 			Iterator<Row> iterator = datatypeSheet.iterator();

			Document documento = implementation.createDocument(null, "ROWSET", null);
	        documento.setXmlVersion("1.0");
			

	        if(processFileTO.getValidationProcessTO() == null){
	        	processFileTO.setValidationProcessTO(new ValidationProcessTO());
	        }	
	        
	        Boolean cellValid = true;
			
 			while (iterator.hasNext()) {
 				Row currentRow = iterator.next();
 				Element operation = documento.createElement("ROW");
                if(currentRow.getRowNum() > 0) {
                    Iterator<Cell> cellIterator = currentRow.iterator();

    				pos++;
        			cellValid = true;
        			
	                while (cellIterator.hasNext()) {
	                    Cell currentCell = cellIterator.next();
	                    Object cellStringValue = cellStringValue(currentCell);
	                    
	                    if(currentCell.getColumnIndex() == 0) {	                    	
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "NRO_REGISTRO");
	                    } else if(currentCell.getColumnIndex() == 1) {
	                    	createTagXML(operation, documento, cellStringValue.toString(), "FECHA");
	                    } else if(currentCell.getColumnIndex() == 2) {
	                    	createTagXML(operation, documento, cellStringValue.toString(), "MODALIDAD_NEGOCIACION");
	                    } else if(currentCell.getColumnIndex() == 3) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "PAPELETA");
	                    } else if(currentCell.getColumnIndex() == 4) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "SECUENCIA");
	                    } else if(currentCell.getColumnIndex() == 5) {
							createTagXML(operation, documento, cellStringValue.toString(), "COMPRADOR");
	                    } else if(currentCell.getColumnIndex() == 6) {
							createTagXML(operation, documento, cellStringValue.toString(), "VENDEDOR");
	                    } else if(currentCell.getColumnIndex() == 7) {
							createTagXML(operation, documento, cellStringValue.toString(), "INSTRUMENTO");
	                    } else if(currentCell.getColumnIndex() == 8) {
	                    	if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
	                    		createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "SERIE");
	                    	} else {
	                    		createTagXML(operation, documento, cellStringValue.toString(), "SERIE");
	                    	}							
	                    } else if(currentCell.getColumnIndex() == 9) {
	                    	createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "PLAZO_CONTADO");
	                    } else if(currentCell.getColumnIndex() == 10) {
							createTagXML(operation, documento, cellStringValue.toString(), "FECHA_LIQUIDACION_CONTADO");
	                    } else if(currentCell.getColumnIndex() == 11) {
	                    	if(Validations.validateIsNotNull(cellNumericToLong(currentCell))) {
	                    		createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "PLAZO_REPORTO");
	                    	} 
	                    } else if(currentCell.getColumnIndex() == 12) {
							createTagXML(operation, documento, cellStringValue.toString(), "FECHA_LIQUIDACION_PLAZO");
	                    } else if(currentCell.getColumnIndex() == 13) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "CANTIDAD");
	                    } else if(currentCell.getColumnIndex() == 14) {
							createTagXML(operation, documento, cellStringValue.toString(), "TASA");
	                    } else if(currentCell.getColumnIndex() == 15) {
							createTagXML(operation, documento, cellStringValue.toString(), "PRECIO_CONTADO_LIMPIO");
	                    } else if(currentCell.getColumnIndex() == 16) {
	                    	createTagXML(operation, documento, cellStringValue.toString(), "PRECIO_CONTADO_SUCIO");
	                    } else if(currentCell.getColumnIndex() == 17) {
							createTagXML(operation, documento, cellStringValue.toString(), "PRECIO_PLAZO");
	                    } else if(currentCell.getColumnIndex() == 18) {
							createTagXML(operation, documento, cellStringValue.toString(), "MONEDA");
	                    } else if(currentCell.getColumnIndex() == 19) {
							createTagXML(operation, documento, cellStringValue.toString(), "LUGAR_LIQ");
	                    } else if(currentCell.getColumnIndex() == 20) {
							createTagXML(operation, documento, cellStringValue.toString(), "MONTO_CONTADO_SUCIO");
	                    } else if(currentCell.getColumnIndex() == 21) {
							createTagXML(operation, documento, cellStringValue.toString(), "MONTO_PLAZO_SUCIO");
	                    } else if(currentCell.getColumnIndex() == 22) {
	                    	createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "PAPELETA_ORIG");
	                    } else if(currentCell.getColumnIndex() == 23) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "SECUENCIA_ORIG");
	                    } else if(currentCell.getColumnIndex() == 24) {
							createTagXML(operation, documento, cellStringValue.toString(), "VEND_ORIG");
	                    } else if(currentCell.getColumnIndex() == 25) {
							createTagXML(operation, documento, cellStringValue.toString(), "TASA_ORIG");
	                    } else if(currentCell.getColumnIndex() == 26) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "PLAZO_ORIG");
	                    } else if(currentCell.getColumnIndex() == 27) {
							createTagXML(operation, documento, cellStringValue.toString(), "PRECIO_ORIG");
	                    } else if(currentCell.getColumnIndex() == 28) {
							createTagXML(operation, documento, cellStringValue.toString(), "FECHA_ORIG");
	                    }
	                }
	                
                    documento.getDocumentElement().appendChild(operation);                    
                }
 			}
 			
 			// Asocio el source con el Document
			Source source = new DOMSource(documento);
			// Creo el Result, indicado que fichero se va a crear
 			fileTemp = File.createTempFile("tempMcnfile", ".tmp");			
			Result result = new StreamResult(fileTemp);
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
	        transformer.transform(source, result);
	        
			processFileTO.setOriginalTempProcessFile(processFileTO.getTempProcessFile());
			processFileTO.setTempProcessFile(fileTemp);
 		} catch(IOException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch(TransformerException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch(ParserConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch (TransformerFactoryConfigurationError e) {
 			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
		}
 	}
	

	public static void operationAssignmentconvertXLStoXML(ProcessFileTO processFileTO){
		
 		try {
 			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
 	        DocumentBuilder builder = factory.newDocumentBuilder();
 	        DOMImplementation implementation = builder.getDOMImplementation();
 	 		File fileTemp = null;
 	 		Integer pos = 0;
 	 		
 			FileInputStream fileInputStream = new FileInputStream(processFileTO.getTempProcessFile().getPath());
 			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
 			XSSFSheet datatypeSheet = workbook.getSheetAt(0);
 			Iterator<Row> iterator = datatypeSheet.iterator();

			Document documento = implementation.createDocument(null, "ROWSET", null);
	        documento.setXmlVersion("1.0");
			

	        if(processFileTO.getValidationProcessTO() == null){
	        	processFileTO.setValidationProcessTO(new ValidationProcessTO());
	        }	
	        
	        Boolean cellValid = true;
			
 			while (iterator.hasNext()) {
 				Row currentRow = iterator.next();
 				Element operation = documento.createElement("ROW");
                if(currentRow.getRowNum() > 0) {
                    Iterator<Cell> cellIterator = currentRow.iterator();

    				pos++;
        			cellValid = true;
        			
	                while (cellIterator.hasNext()) {
	                    Cell currentCell = cellIterator.next();
	                    Object cellStringValue = cellStringValue(currentCell);
	                    
	                    if(currentCell.getColumnIndex() == 0) {	                    	
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "NRO_REGISTRO");
	                    } else if(currentCell.getColumnIndex() == 1) {
	                    	createTagXML(operation, documento, cellStringValue.toString(), "FECHA");
	                    } else if(currentCell.getColumnIndex() == 2) {
	                    	createTagXML(operation, documento, cellStringValue.toString(), "MODALIDAD");
	                    } else if(currentCell.getColumnIndex() == 3) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "PAPELETA");
	                    } else if(currentCell.getColumnIndex() == 4) {
							createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "SECUENCIA");
	                    } else if(currentCell.getColumnIndex() == 5) {
							createTagXML(operation, documento, cellStringValue.toString(), "DEPOSITANTE_ASIGNADOR");
	                    } else if(currentCell.getColumnIndex() == 6) {
							createTagXML(operation, documento, cellStringValue.toString(), "TIPO_OPERACION");
	                    } else if(currentCell.getColumnIndex() == 7) {
							createTagXML(operation, documento,  cellNumericToLong(currentCell).toString(), "COMITENTE");
	                    } else if(currentCell.getColumnIndex() == 8) {
	                        createTagXML(operation, documento, cellNumericToLong(currentCell).toString(), "CANTIDAD");					
	                    } else if(currentCell.getColumnIndex() == 9) {
	                    	createTagXML(operation, documento, cellStringValue.toString(), "INSTRUMENTO");
	                    } else if(currentCell.getColumnIndex() == 10) {
							createTagXML(operation, documento, cellStringValue.toString(), "VALOR_NEGOCIADO");
	                    } 
	                }
	                
                    documento.getDocumentElement().appendChild(operation);                    
                }
 			}
 			
 			// Asocio el source con el Document
			Source source = new DOMSource(documento);
			// Creo el Result, indicado que fichero se va a crear
 			fileTemp = File.createTempFile("tempMcnfile", ".tmp");			
			Result result = new StreamResult(fileTemp);
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
	        transformer.transform(source, result);
	        
			processFileTO.setOriginalTempProcessFile(processFileTO.getTempProcessFile());
			processFileTO.setTempProcessFile(fileTemp);
 		} catch(IOException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch(TransformerException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch(ParserConfigurationException e) {
			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
 		} catch (TransformerFactoryConfigurationError e) {
 			processFileTO.getValidationProcessTO().setLstValidations(new ArrayList<RecordValidationType>());
			RecordValidationType fe = new RecordValidationType();
			fe.setErrorSimpleDetail(e.getMessage());
			processFileTO.getValidationProcessTO().getLstValidations().add(fe);
		}
 	}
	
	public static Object cellStringValue(Cell currentCell) {
		if(Validations.validateIsNotNull(currentCell) ) {
	 		if (currentCell.getCellType() == Cell.CELL_TYPE_STRING) {
	            return currentCell.getStringCellValue();
	        } else if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC && HSSFDateUtil.isCellDateFormatted(currentCell)) {
	            return currentCell.getDateCellValue();
	        } else if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
	            return currentCell.getNumericCellValue()+"";
	        }
		}
 		return "";
 	}
	
	public static Long cellNumericToLong(Cell currentCell) {
		if (Validations.validateIsNotNull(currentCell) &&  currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			Double doubleValue = currentCell.getNumericCellValue();
	    	BigDecimal bd = new BigDecimal(doubleValue.toString());
	        return bd.longValue();
		} else {
			return null;
		}
	}
	
	public static String convertAmount(String realAmount, Integer scale){
		BigDecimal bdAmount = null;
		try {
			if(realAmount.endsWith("000000000000.0000000000") || 
					realAmount.endsWith("0.0") || 
					realAmount.endsWith("0.0000000000")|| 
					realAmount.endsWith("00.0000000000")|| 
					realAmount.endsWith("00.00") || 
					realAmount.endsWith("000000000000.0") || 
					realAmount.endsWith("000000000000.00") || 
					realAmount.endsWith("0.0000000000")		
			) {
				return BigDecimal.ZERO.toString();
			}
			bdAmount = new BigDecimal(realAmount);
			return bdAmount.toString();							
		} catch (Exception e) {
			return GeneralConstants.BLANK_SPACE;
		}
	}
	
	
	public static Integer factorPeriodicityType(Integer tipoPeridiocidad) {

		Integer factorPeriodicity = null;
		
		if(tipoPeridiocidad == 147) {//MENSUAL
			factorPeriodicity = 12; //12 / 1
		}else if(tipoPeridiocidad == 536) {//BIMESTRAL
			factorPeriodicity = 6; //12/2
		}else if(tipoPeridiocidad == 537) {///TRIMESTRAL
			factorPeriodicity = 4; //12/3
		}else if(tipoPeridiocidad == 538) {//CUATRIMESTRAL
			factorPeriodicity = 3; //12/4
		}else if(tipoPeridiocidad == 539) {//SEMESTRAL
			factorPeriodicity = 2; //12/6
		}else if(tipoPeridiocidad == 540) {//ANUAL
			factorPeriodicity = 1; //12/12
		}else {
			factorPeriodicity = 1;
		}
		
		return factorPeriodicity;
	}
	
	public static Integer daysDefaultWithFixedPeridiocity(String peridiocidad) {
		
		if(peridiocidad == "MENSUAL") {//MENSUAL
			return 30;
		}else if(peridiocidad == "BIMESTRAL") {//BIMESTRAL
			return 60; 
		}else if(peridiocidad == "TRIMESTRAL") {///TRIMESTRAL
			return 90; 
		}else if(peridiocidad == "CUATRIMESTRAL") {//CUATRIMESTRAL
			return 120; 
		}else if(peridiocidad == "SEMESTRAL") {//SEMESTRAL
			return 180; 
		}else if(peridiocidad == "ANUAL") {//ANUAL
			return 360; 
		}else {
			return 0;
		}
		
	}

	
	public static Integer daysDefaultWithFixedPeridiocity(Integer peridiocidad) {
		
		if(peridiocidad == 147) {//MENSUAL
			return 30;
		}else if(peridiocidad == 536) {//BIMESTRAL
			return 60; 
		}else if(peridiocidad == 537) {///TRIMESTRAL
			return 90; 
		}else if(peridiocidad == 538) {//CUATRIMESTRAL
			return 120; 
		}else if(peridiocidad == 539) {//SEMESTRAL
			return 180; 
		}else if(peridiocidad == 540) {//ANUAL
			return 360; 
		}else {
			return 0;
		}
		
	}

	
	public static Integer cuponesWithFixedPeridiocity(String peridiocidad) {
		
		if(peridiocidad == "MENSUAL") {//MENSUAL
			return 12;
		}else if(peridiocidad == "BIMESTRAL") {//BIMESTRAL
			return 6; 
		}else if(peridiocidad == "TRIMESTRAL") {///TRIMESTRAL
			return 4; 
		}else if(peridiocidad == "CUATRIMESTRAL") {//CUATRIMESTRAL
			return 3; 
		}else if(peridiocidad == "SEMESTRAL") {//SEMESTRAL
			return 2; 
		}else if(peridiocidad == "ANUAL") {//ANUAL
			return 1; 
		}else {
			return 1;
		}
		
	}

	public static Integer representationWithPeridiocity(String peridiocidad) {
		
		if(peridiocidad == "MENSUAL") {//MENSUAL
			return 1;
		}else if(peridiocidad == "BIMESTRAL") {//BIMESTRAL
			return 2; 
		}else if(peridiocidad == "TRIMESTRAL") {///TRIMESTRAL
			return 3; 
		}else if(peridiocidad == "CUATRIMESTRAL") {//CUATRIMESTRAL
			return 4; 
		}else if(peridiocidad == "SEMESTRAL") {//SEMESTRAL
			return 6; 
		}else if(peridiocidad == "ANUAL") {//ANUAL
			return 12; 
		}else {
			return 0;
		}
		
	}
	
	/**
	 * Put time to date to end of current day
	 * @param date
	 * @return
	 */
	public static Date putTimeToDateToEndOfDay(Date date) {
		return putTimeToDate(date, 23, 59, 59, 99);
	}

	public static byte[] readFile(File file) throws IOException{
		return FileUtils.readFileToByteArray(file);
	}
	
	public static String getCellValue(int pos, Row row) {
		Cell cell = row.getCell(pos);
		if (cell == null) {
			return null;
		}
		cell.setCellType(Cell.CELL_TYPE_STRING);
		String value= cell.toString();
//		if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC) {
//			Double cellNumericValue= Double.valueOf( cell.getNumericCellValue()  );
//			value=cellNumericValue.toString();
//		}
		if(value.endsWith(".0")) {
			value=value.substring(0, value.lastIndexOf("0")-1);
		}
		return value;
	}
	
	public static String getCellForceStringValue(int pos, Row row) {
		Cell cell = row.getCell(pos);
		if (cell == null) {
			return null;
		}
		String value= cell.toString();
		if(value.endsWith(".0")) {
			value=value.substring(0, value.lastIndexOf("0")-1);
		}
		return value;
	}
	
	public static void assertCustomException(boolean indicator, String message) throws CustomException {
		if(indicator) {
			throw new CustomException(message);
		}
	}
	
	public static int calculateAge(Date birthDate) {
		java.time.LocalDate currentDate =  java.time.LocalDate.now();
		java.time.LocalDate birthLocalDate = new java.sql.Date(birthDate.getTime()).toLocalDate();
        int age = Period.between(birthLocalDate, currentDate).getYears();
        return age;
    }
	
	public static boolean compareString(String str1, String str2) {
		if(Validations.validateIsNullOrEmpty(str1) && Validations.validateIsNullOrEmpty(str2)) {
			return true;
		}
		if(Validations.validateIsNullOrEmpty(str1) && !Validations.validateIsNullOrEmpty(str2)) {
			return false;
		}
		if(!Validations.validateIsNullOrEmpty(str1) && Validations.validateIsNullOrEmpty(str2)) {
			return false;
		}
		return str1.equals(str2);
	}
}