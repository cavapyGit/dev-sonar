
package com.pradera.commons.utils;

import java.math.BigDecimal;
import java.security.Key;

import org.apache.commons.lang3.RandomStringUtils;

import io.jsonwebtoken.impl.crypto.MacProvider;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeneralConstants.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 20/02/2013
 */
public class GeneralConstants {

	/**
	 * Instantiates a new general constants.
	 */
	private GeneralConstants() {
		// TODO Auto-generated constructor stub
	}
	
	public static String SCHEMA="acres_simvest";
	
	public static String NUMERIC_STYLE_CLASS="n-align";
	
	public static  Key JWT_KEY = MacProvider.generateKey();
	
//	public static  Long HOLDER_MANAGER= new Long(1);
	
	public static  Long ACRES_PARTICIPANT= new Long(1);
	
	public static final String JWT_KEY_STR =RandomStringUtils.randomNumeric(10);
	
	public static final String JWT_KEY_STR_2 ="xxx";

	/** The Constant BATCH_CACHE_NAME. */
	public static final String BATCH_CACHE_NAME="|batch-cache-container";

	/** The Constant LBL_HEADER_ALERT_ACTIVE. */
	public static final String LBL_HEADER_ALERT_ACTIVE = "lbl.header.alert.active";
	
	/** The Constant LBL_HEADER_ALERT_DEACTIVE. */
	public static final String LBL_HEADER_ALERT_DEACTIVE = "lbl.header.alert.deactive";
	
	/** The Constant LBL_HEADER_ALERT_CONFIRM. */
	public static final String LBL_HEADER_ALERT_CONFIRM = "lbl.header.alert.confirm";
	
	/** The Constant LBL_HEADER_ALERT_REJECT. */
	public static final String LBL_HEADER_ALERT_REJECT = "lbl.header.alert.reject";
	
	/** The Constant LBL_HEADER_ALERT_APPROVE. */
	public static final String LBL_HEADER_ALERT_APPROVE = "lbl.header.alert.approve";
	
	/** The Constant LBL_HEADER_ALERT_ANNUL. */
	public static final String LBL_HEADER_ALERT_ANNUL = "lbl.header.alert.annul";
	
	/** The Constant LBL_HEADER_ALERT_REGISTER. */
	public static final String LBL_HEADER_ALERT_REGISTER = "lbl.header.alert.register";
	
	/** The Constant LBL_HEADER_ALERT_APPLY. */
	public static final String LBL_HEADER_ALERT_APPLY = "lbl.header.alert.apply";
	
	/** The Constant LBL_HEADER_ALERT_AUTHORIZE. */
	public static final String LBL_HEADER_ALERT_AUTHORIZE = "lbl.header.alert.authorize";
	
	/** The Constant LBL_HEADER_ALERT_SUCCESS. */
	public static final String LBL_HEADER_ALERT_SUCCESS= "lbl.header.alert.success";
	
	/** The Constant LBL_HEADER_ALERT_MOTIVE_REJECT. */
	public static final String LBL_HEADER_ALERT_MOTIVE_REJECT = "lbl.header.alert.motiveReject";
	
	/** The Constant LBL_HEADER_ALERT_REVIEW. */
	public static final String LBL_HEADER_ALERT_REVIEW = "lbl.header.alert.review";
	
	/** The Constant LBL_HEADER_ALERT_CERTIFY. */
	public static final String LBL_HEADER_ALERT_CERTIFY = "lbl.header.alert.certify";
	
	/** The Constant LBL_HEADER_ALERT_ERROR. */
	public static final String LBL_HEADER_ALERT_ERROR = "lbl.header.alert.error";
	
	/** The Constant LBL_HEADER_ALERT_WARNING. */
	public static final String LBL_HEADER_ALERT_WARNING = "lbl.header.alert.warning";

	/** The Constant LBL_HEADER_ALERT_WARNING. */
	public static final String LBL_HEADER_ALERT_CANCEL = "lbl.header.alert.cancel";
	
	/** The Constant LBL_HEADER_ALERT. */
	public static final String LBL_HEADER_ALERT = "lbl.header.alert";
	
	/** The Constant LBL_HEADER_ALERT_MODIFY. */
	public static final String LBL_HEADER_ALERT_MODIFY = "lbl.header.alert.modify";
	
	/** The Constant LBL_HEADER_ALERT_MOTIVE_ANNUL. */
	public static final String LBL_HEADER_ALERT_MOTIVE_ANNUL = "lbl.header.alert.motiveAnnul";
	
	/** The Constant LBL_BODY_ALERT_MANDATORY_DATA. */
	public static final String LBL_BODY_ALERT_MANDATORY_DATA = "lbl.body.alert.mandatory.data";

	public static final String LBL_BODY_ALERT_SELECT_ONE_DATA = "lbl.body.alert.select.one.data";

	/** The Constant LBL_HEADER_ALERT_DELETE. */
	public static final String LBL_HEADER_ALERT_DELETE = "lbl.header.alert.delete";
	
	/** The Constant LBL_HEADER_ALERT_APPEAL. */
	public static final String LBL_HEADER_ALERT_APPEAL = "lbl.header.alert.appeal";
	
	/** The Constant LBL_ALERT_DOCUMENT_ALREADY_LOADED. */
	public static final String LBL_ALERT_DOCUMENT_ALREADY_LOADED = "lbl.alert.document.already.loaded";
	
	/** The Constant ERROR_MULTIPLO. */
	public final static String ERROR_MULTIPLO="error.multiplo";
	
	/** The Constant ERROR_NEW_NOMINAL_VALUE. */
	public final static String ERROR_NEW_NOMINAL_VALUE="error.new.nominal.value";
	
	/** The Constant HELPER_ISSUER_NOT_FOUND. */
	public static final String HELPER_ISSUER_NOT_FOUND = "lbl.helper.issuer.notFound";
	
	/** The Constant HELPER_ISSUER_STATE_PENDING. */
	public static final String HELPER_ISSUER_STATE_PENDING = "issuer.helper.alert.statepending";
	
	/** The Constant HELPER_HOLDER_NOT_FOUND. */
	public static final String HELPER_HOLDER_NOT_FOUND = "lbl.helper.holder.notFound";
	
	/** The Constant HELPER_HOLDER_NOT_FOUND. */
	public static final String HELPER_HOLDER_NOT_FOUND2 = "lbl.helper.holder.notFound2";
	
	/** The Constant HELPER_SECURITY_NOT_FOUND. */
	public static final String HELPER_SECURITY_NOT_FOUND = "lbl.helper.security.notFound";
	
	/** The Constant HELPER_ACCOUNT_NOT_FOUND. */
	public static final String HELPER_ACCOUNT_NOT_FOUND = "lbl.helper.account.notFound";
	
	/** The Constant HELPER_ACCOUNT_NOT_RELATED_WITH_HOLDER. */
	public static final String HELPER_ACCOUNT_NOT_RELATED_WITH_HOLDER = "lbl.helper.account.notRelated.with.holder";
	
	/** The Constant HELPER_ACCOUNT_PARTICIPANT_REQUIRED. */
	public static final String HELPER_ACCOUNT_PARTICIPANT_REQUIRED = "lbl.helper.account.partNotSelected";
	
	/** The Constant HELPER_ACCOUNT_HOLDER_REQUIRED. */
	public static final String HELPER_ACCOUNT_HOLDER_REQUIRED = "lbl.helper.account.holdNotSelected";
	
	/** The Constant ERROR_ACCOUNT_NOT_RELATED_WITH_HOLDER. */
	public static final String ERROR_ACCOUNT_NOT_RELATED_WITH_HOLDER = "error.report.accountN.holder";
	
	/** The Constant HELPER_SECURITY_NOT_FOUND. */
	public static final String HELPER_BILLING_NOT_FOUND = "lbl.helper.billing.notFound";
	
	/** The Constant RESET_BEAN_MENU. */
	public static final String RESET_BEAN_MENU = "resetmenu";
	
	/** The Constant MESSAGE_ID. */
	public static final String MESSAGE_ID="message_id";
	
	/** The Constant PROCESS_ID. */
	public static final String PROCESS_ID="process_id";
	
	/** The Constant DASH. */
	public static final String DASH = " - ";

	/** The Constant HYPHEN. */
	public static final String HYPHEN = "-";

	/** The Constant HYPHEN. */
	public static final String DOT = ".";
	
	public static final String DOT_REG_EXP  = "\\.";

	/** The Constant EMPTY_STRING. */
	public static final String EMPTY_STRING = "";

	/** The Constant BLANK_SPACE. */
	public static final String BLANK_SPACE = " ";
	
	/** The Constant BRACKET_OPEN. */
	public static final String BRACKET_OPEN = "(";
	
	/** The Constant BRACKET_CLOSE. */
	public static final String BRACKET_CLOSE = ")";
	
	/** The Constant ZERO_VALUE_INTEGER. */
	public static final int ZERO_VALUE_INT = 0;

	/** The Constant ZERO_VALUE_INTEGER. */
	public static final Integer ZERO_VALUE_INTEGER = Integer.valueOf(0);

	/** The Constant ZERO_VALUE_LONG. */
	public static final Long ZERO_VALUE_LONG = Long.valueOf(0);

	/** The Constant ZERO_VALUE_DOUBLE. */
	public static final Double ZERO_VALUE_DOUBLE = Double.valueOf(0.0);

	/** The Constant ONE_VALUE_INTEGER. */
	public static final Integer ONE_VALUE_INTEGER = Integer.valueOf(1);

	/** The Constant TWO_VALUE_INTEGER. */
	public static final Integer TWO_VALUE_INTEGER = Integer.valueOf(2);

	/** The Constant THREE_VALUE_INTEGER. */
	public static final Integer THREE_VALUE_INTEGER = Integer.valueOf(3);
	
	/** The Constant THIRTY_ONE_VALUE_INTEGER. */
	public static final Integer THIRTY_ONE_VALUE_INTEGER = Integer.valueOf(31);
	
	/** The Constant THIRTY_TWO_VALUE_INTEGER. */
	public static final Integer THIRTY_TWO_VALUE_INTEGER = Integer.valueOf(32);
	
	/** The Constant THIRTY_THREE_VALUE_INTEGER. */
	public static final Integer THIRTY_THREE_VALUE_INTEGER = Integer.valueOf(33);
	
	/** The Constant THOUSAND_VALUE_INTEGER. */
	public static final Integer THOUSAND_VALUE_INTEGER=Integer.valueOf(1000);
	
	/** The Constant HUNDRED_VALUE_INTEGER. */
	public static final BigDecimal HUNDRED_VALUE_BIGDECIMAL=BigDecimal.valueOf(100);

	/** The Constant THREE_HUNDRED_SIXTY_FIVE. */
	public static final BigDecimal THREE_HUNDRED_SIXTY_FIVE=BigDecimal.valueOf(365);
	
	/** The Constant THOUSAND_VALUE_BIGDECIMAL. */
	public static final BigDecimal THOUSAND_VALUE_BIGDECIMAL=BigDecimal.valueOf(1000);
	
	/** The Constant ONE_VALUE_BIGDECIMAL. */
	public static final BigDecimal ONE_VALUE_BIGDECIMAL = BigDecimal.valueOf(1);

	public static final Integer SURVEY_QUESTION_PARAM = Integer.valueOf(4);
	
	/** The Constant SIX_VALUE_INTEGER. */
	public static final Integer SIX_VALUE_INTEGER=Integer.valueOf(6);
	
	/** The Constant SEVEN_VALUE_INTEGER. */
	public static final Integer SEVEN_VALUE_INTEGER=Integer.valueOf(7);
	
	/** The Constant NINE_VALUE_INTEGER. */
	public static final Integer NINE_VALUE_INTEGER=Integer.valueOf(9);

	/** The Constant PROPERTY_FILE_VALIDATION_MESSAGES. */
	public static final String PROPERTY_FILE_VALIDATION_MESSAGES = "ValidationMessages";
	
	/** The Constant PROPERTY_FILE_MESSAGES. */
	public static final String PROPERTY_FILE_MESSAGES = "Messages";

	/** The Constant PROPERTY_FILE_CONSTANTS. */
	public static final String PROPERTY_FILE_CONSTANTS = "Constants";

	/** The Constant PROPERTY_FILE_EXCEPTIONS. */
	public static final String PROPERTY_FILE_EXCEPTIONS = "ExceptionMessages";
	
	/** The Constant PROPERTY_FILE_GENERIC_MESSAGES. */
	public static final String PROPERTY_FILE_GENERIC_MESSAGES = "GenericMessages";
	
	/** The Constant PROPERTY_FILE_GENERIC_CONSTANTS. */
	public static final String PROPERTY_FILE_GENERIC_CONSTANTS="GenericConstants";

	/** The Constant NEGATIVE_ONE_INTEGER. */
	public static final Integer NEGATIVE_ONE_INTEGER = Integer.valueOf(-1);

	/** The Constant NEGATIVE_ONE_LONG. */
	public static final Long NEGATIVE_ONE_LONG = Long.valueOf(-1);

	/** The Constant TAMANIO_FILE_KBS. */
	public static final Long TAMANIO_FILE_KBS = 200L;

	/** The Constant TAMANIO_FILE_BITES. */

	/** The Constant ASTERISK. */
	public static final String ASTERISK_STRING = "*";
	
	/** The Constant ASTERISK. */
	public static final String SCHEDULE_WORK_DAYS_STRING = "1-5";
	
	/** The Constant PERCENTAGE. */
	public static final String PERCENTAGE_STRING = "%";

	/** The Constant ASTERISK_CHAR. */
	public static final char ASTERISK_CHAR = '*';

	/** The Constant PERCENTAGE_CHAR. */
	public static final char PERCENTAGE_CHAR = '%';

	/** The Constant PARTICIPANT_EDB_CODE. */
	public static final Long PARTICIPANT_EDB_CODE = new Long(133);
	
	public static final Long PARTICIPANT_ACRES_CODE = new Long(1);

	/** The Constant EMAIL_PATTERN. */
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/** The Constant WEB_PAGE_PATTERN. */
	public static final String WEB_PAGE_PATTERN = "^(http(s{0,1})://){0,1}[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%\\#\\/\\.\\_\\!\\@]*";

	/** The Constant ALPHANUMERIC_PATTERN. */
	public static final String ALPHANUMERIC_PATTERN = "^([a-z]|[A-Z]|[Ã±Ã‘]|[0-9])*$";

	/** The Constant ADDRESS_PATTERN. */
	public static final String ADDRESS_PATTERN = "^([a-z]|[A-Z]|[Ã±Ã‘]|[\\d]|[\\s]|[(]|[)]|[-]|[,]|[.]|[:]|[;]|[#]|[\\u00F1\\u00D1]|[/])*";

	/** The Constant ALPHANUMERIC_AND_DOT_PATTERN. */
	public static final String ALPHANUMERIC_AND_DOT_PATTERN = "^([a-z]|[A-Z]|[Ã±Ã‘]|[0-9]|[.])*$";

	/** The Constant NUMERIC_PATTERN. */
	public static final String NUMERIC_PATTERN = "^([\\d])*$";
	
	/** The Constant LOGOUT_MOTIVE. */
	public static final String LOGOUT_MOTIVE = "logoutMotive";

	/** The Constant USER_INFO_ID. */
	public static final String USER_INFO_ID = "userInfoId";

	/** The Constant ZERO_VALUE_STRING. */
	public static final String ZERO_VALUE_STRING = "0";
	
	/** The Constant ZERO_DECIMAL_STRING. */
	public static final String ZERO_DECIMAL_STRING ="0.00";	

	/** The Constant ZERO_DECIMAL_STRING. */
	public static final String ZERO_DECIMAL_1_STRING ="0.0";

	/** The Constant ONE_VALUE_STRING. */
	public static final String ONE_VALUE_STRING = "1";

	/** The Constant ONE_VALUE_LONG. */
	public static final Long ONE_VALUE_LONG = Long.valueOf(1);
	
	/** The Constant TWO_VALUE_LONG. */
	public static final Long TWO_VALUE_LONG = Long.valueOf(2);
	
	/** The Constant FIVE_VALUE_LONG. */
	public static final Long FIVE_VALUE_LONG = Long.valueOf(5);

	/** The Constant DATE_FORMAT_PATTERN. */
	public static final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";
	
	/** The Constant DATE_FORMAT_PATTERN_TXT. */
	public static final String DATE_FORMAT_PATTERN_TXT = "yyyy-MM-dd";
	
	/** The Constant DATE_FORMAT_PATTERN. */
	public static final String DATE_FORMAT_PATTERN_ALT_01 = "dd-MMM-yyyy";
	
	/** The Constant SLASH. */
	public static final String SLASH = "/";
	
	/** The Constant SLASH_SPACE. */
	public static final String SLASH_SPACE = " / ";
	
	/** Constant for the 0 value as the Big Decimal. */
	public static final BigDecimal ZERO_VALUE_BIGDECIMAL = new BigDecimal(0);

	/** The Constant STR_BR. */
	public static final String STR_BR = "</br>";
	
	/** The Constant STR_COMMA. */
	public static final String STR_COMMA = ", ";
	
	public static final String STR_PNG=".png";
	
	public static final String STR_PDF=".pdf";

	/** The Constant STR_COMMA_WITHOUT_SPACE. */
	public static final String STR_COMMA_WITHOUT_SPACE = ",";
	
	/** The Constant STR_COMMA_WITHOUT_SPACE. */
	public static final String STR_SEMI_COLON_WITHOUT_SPACE = ";";
	
	/** The Constant STR_HASH. */
	public static final String STR_HASH = "#";
	
	/** The Constant TWO_POINTS. */
	public static final String TWO_POINTS = ":";
	
	/** The Constant UNDERLINE_SEPARATOR. */
	public static final String UNDERLINE_SEPARATOR = "_";
	
	/** The Constant STR_END_LINE_TXT. */
	public static final String STR_END_LINE_TXT="\r\n";
	
	public static final String FORMAT_FILE_TXT = "txt";
	
	public static final String FORMAT_FILE_CAVALI = "CAVALI";

	/** The Constant XML_FORMAT. */
	public static final String XML_FORMAT = ".xml";
	
	
	public static final Integer NEED_REFINITIV_PROCESS = Integer.valueOf(1445);

	/** The Constant APPROVE_OPERATION_CONSTANT. */
	public static final Integer APPROVE_OPERATION_CONSTANT = Integer.valueOf(1);
	
	/** The Constant CANCEL_OPERATION_CONSTANT. */
	public static final Integer CANCEL_OPERATION_CONSTANT = Integer.valueOf(2);
	
	/** The Constant CANCEL_DEPOSIT_OIPERATION. */
	public static final Integer CANCEL_DEPOSIT_OIPERATION = Integer.valueOf(7);
	
	/** The Constant REJECT_OPERATION_CONSTANT. */
	public static final Integer REJECT_OPERATION_CONSTANT = Integer.valueOf(4);
	
	/** The Constant CONFIRM_OPERATION_CONSTANT. */
	public static final Integer CONFIRM_OPERATION_CONSTANT = Integer.valueOf(3);
	
	/** The Constant APPLY_OPERATION_CONSTANT. */
	public static final Integer APPLY_OPERATION_CONSTANT = Integer.valueOf(5);
	
	public static final Integer FIVE_VALUE_INTEGER = Integer.valueOf(5);

	/** The Constant AUTHORIZE_OPERATION_CONSTANT. */
	public static final Integer AUTHORIZE_OPERATION_CONSTANT = Integer.valueOf(6);
												
	/** The Constant IMPORTANCE_EVENT_DETAIL_PAGE. */
	public static final String IMPORTANCE_EVENT_DETAIL_PAGE="/PraderaCorporateEvents/pages/importanceEvent/importanceEventDetail.xhtml";
	
	/** The Constant IMPORTANCE_EVENT_QUERY_STRING. */
	public static final String IMPORTANCE_EVENT_QUERY_STRING="remoteParam";
	
	// This section describes Business Process Parameters for BATCH processing marketfact File. 
	/** The Constant REGISTER_DATE. */
	public static final String REGISTER_DATE = "REGISTER_DATE";
	
	/** The Constant PROCESS_DATE. */
	public static final String PROCESS_DATE = "PROCESS_DATE";
	
	
	/** The Constant FILE_SOURCE. */
	public static final String FILE_SOURCE="FILE_SOURCE";
	
	public static final String ATTACHED_DEFAULT_NAME = "Adjunto";
	
	public static final String ATTACHED_DEFAULT_SUBJECT = "Default_subject";

	public static final String ATTACHED_DEFAULT_MESSAGE = "Default_message";

	
	/** END SECTION MARKETFACT FILE. */
	
	// This section describes Business Process Parameters for BATCH processing. 
	// Usage: PARAMETER_<NAME>
	
	/** The Constant PARAMETER_DATE. */
	public static final String PARAMETER_DATE = "FECHA_INICIAL";
	
	
	/** The Report By Process. */
	public static String REPORT_BY_PROCESS="reportByProcess";
	
	/** The Constant PARAMETER_REVERSION_DAYS. */
	public static final String PARAMETER_REVERSION_DAYS= "REVERSION_DAYS";	
	
	/** The Constant PARAMETER_FILE. */
	public static final String PARAMETER_FILE= "FILE";
	
	/** The Constant PARAMETER_FILE_NAME. */
	public static final String PARAMETER_FILE_NAME= "FILE_NAME";
	
	/** The Constant PARAMETER_MECHANISM. */
	public static final String PARAMETER_MECHANISM = "MECHANISM";
	
	/** The Constant PARAMETER_STREAM. */
	public static final String PARAMETER_STREAM= "STREAM";
	
	/** The Constant PARAMETER_STREAM_RESPONSE. */
	public static final String PARAMETER_STREAM_RESPONSE= "STREAM_RESPONSE";
	
	/** The Constant PARAMETER_ROOT_TAG. */
	public static final String PARAMETER_ROOT_TAG= "ROOT_TAG";
	
	/** The Constant PARAMETER_LOCALE. */
	public static final String PARAMETER_LOCALE= "LOCALE";
	
	/** The Constant PARAMETER_REPO. */
	public static final String PARAMETER_REPO = "REPO";
	
	/** The Constant PARAMETER_INCHARGE. */
	public static final String PARAMETER_INCHARGE= "INCHARGE";
	
	/** The Constant PARAMETER_OTC_PART_ID. */
	public static final String PARAMETER_OTC_PART_ID= "OTC_PART_ID";
	
	/** The Constant PARAMETER_OTC_PART_ID. */
	public static final String PARAMETER_AUDIT_RULE_PK= "AUDIT_RULE_PK";
	
	/** The Constant PARAMETER_SCHEDULE_ID. */
	public static final String PARAMETER_SCHEDULE_ID= "SCHEDULE_ID";

	/** The Constant N_A. */
	public static final String N_A = "N/A";
	
	/** The Constant PAGOS_UP. */
	public static final String PAGOS_UP = "PAGOSUP";
	
	/** The Constant ALLOCATION_HOLDER_PAYMENT_FORMAT. */
	public static final String ALLOCATION_HOLDER_PAYMENT_FORMAT = "schema/holderFile.xml";

	/** The Constant ZERO. */
	public static final BigDecimal ZERO = new BigDecimal("0.00");
	
	/** The Regular Express for User Password. */
	public static final String PASSWORD_PATTERN= "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\_\\W])(?=^[^@\\^\\.\\(\\#\\s]+$)";
	
	/** The Constant REGEX_CHAR. */
	public static final String REGEX_CHAR = "[a-zA-Z]";
	
	/** The Constant REGEX_NUMBER. */
	public static final String REGEX_NUMBER = "[0-9]";
	
	/** PARAMETER PARAMETER NAME DB FIELD idBillingServicePk. **/
	public static final String 		FIELD_ID_BILLING_SERVICE_PK		= "idBillingServicePk";
	
	/** PARAMETER PARAMETER NAME DB FIELD idProcessedServicePk. **/
	public static final String 		FIELD_ID_PROCESSED_SERVICE_PK	= "idProcessedServicePk";
	
	/** PARAMETER PARAMETER NAME DB FIELD calculationDate. **/
	public static final String 		FIELD_CALCULATION_DATE			= "calculationDate";
	
	/** PARAMETER PARAMETER NAME DB FIELD entityCollection. **/
	public static final String 		FIELD_ENTITY_COLLECTION			= "entityCollection";
	
	/** PARAMETER PARAMETER NAME DB FIELD collectionPeriod. **/
	public static final String 		FIELD_COLLECTION_PERIOD			= "collectionPeriod";
	
	
	/** PARAMETER PARAMETER NAME DB FIELD year. **/
	public static final String 		FIELD_YEAR						= "year";
	
	/** PARAMETER PARAMETER NAME DB FIELD month. **/
	public static final String 		FIELD_MONTH						= "month";
	
	/** PARAMETER PARAMETER NAME DB FIELD INITIAL DATE. **/
	public static final String 		FIELD_INITIAL_DATE				= "initialDate";
	
	/** PARAMETER PARAMETER NAME DB FIELD FINAL DATE. **/
	public static final String 		FIELD_FINAL_DATE				= "finalDate";
	
	/** PARAMETER PARAMETER NAME DB FIELD serviceCode. **/
	public static final String 		FIELD_SERVICE_CODE				= "serviceCode";
	
	/** PARAMETER PARAMETER NAME DB FIELD serviceName. **/
	public static final String 		FIELD_SERVICE_NAME				= "serviceName";
	
	/** PARAMETER PARAMETER NAME DB FIELD baseCollection. **/
	public static final String 		FIELD_BASE_COLLECTION			= "baseCollection";
	
	/** PARAMETER PARAMETER NAME DB FIELD calculationPeriod. **/
	public static final String 		FIELD_CALCULATION_PERIOD		= "calculationPeriod";
	
	/**  PARAMETER NAME TO PROCESS BILLING DEFINITIVE. */
	/** PARAMETER NAME ID BILLING CALCULATION PROCESS */
	public static final String 		FIELD_CALCULATION_PROCESS		= "billingCalculationProcessId";
	
	/**  PARAMETER NAME DATE BECOMING. */
	public static final String 		FIELD_DATE_BECOMING				= "dateBecoming";
	
	/** PARAMETER  KEY FOR BILLING*. */	 
	public static final String 		ID_BILLING_SERVICE_PK		= "ID_BILLING_SERVICE_PK";
	
	/** The Constant ID_SERVICE_RATE_PK. */
	public static final String 		ID_SERVICE_RATE_PK			= "ID_SERVICE_RATE_PK";
	
	/** PARAMETER   BILLING_SERVICE ENTITY*. */	 
	public static final String 		BILLING_SERVICE				= "BILLING_SERVICE";	
	
	/** PARAMETER SERVICE_RATE ENTITY. */
	public static final String 		SERVICE_RATE				= "SERVICE_RATE";
	
	/** PARAMETER   BILLING_SERVICE ENTITY*. */	 
	public static final String 		ECONOMIC_ACTIVITY_LIST_INT	= "ECONOMIC_ACTIVITY_LIST_INT";	
	
	/** The Constant AMOUNT. */
	public static final String 		AMOUNT						= "AMOUNT";
	
	/** The Constant AMOUNT. */
	public static final String 		CURRENCY					= "CURRENCY";
	
	/** The Constant CALCULATION_DATE.
	 * FORMAT :  dd/MM/yyyy . **/
	public static final String 		CALCULATION_DATE			= "CALCULATION_DATE";
	
	/** The Constant OPERATION_DATE.
	 * FORMAT :  dd/MM/yyyy  .**/
	public static final String 		OPERATION_DATE				= "OPERATION_DATE";
	
	/** The Constant ID_SECURITY_CODE. */
	public static final String  	ID_SECURITY_CODE			= "ID_SECURITY_CODE";
	
	/** The Constant ID_HOLDER_PK. */
	public static final String		ID_HOLDER_PK				="ID_HOLDER_PK";
	
	/** The Constant ID_HOLDER_ACCOUNT_PK. */
	public static final String		ID_HOLDER_ACCOUNT_PK		="ID_HOLDER_ACCOUNT_PK";
	
	/** The Class HOLDER_ACCOUNT_MOVEMENT_TO. */
	public static final String		HOLDER_ACCOUNT_MOVEMENT_TO	="HOLDER_ACCOUNT_MOVEMENT_TO";
	
	/** The Constant ID_PARTICIPANT_PK. */
	public static final String		ID_PARTICIPANT_PK			="ID_PARTICIPANT_PK";
	
	/** The Constant ID_ISSUER_PK. */
	public static final String		ID_ISSUER_PK				="ID_ISSUER_PK";
	
	/** The Constant BASE_COLLECTION. */
	public static final String		BASE_COLLECTION				="BASE_COLLECTION";
	
	/** The Constant BILLING_SERVICE_LIST. */
	public static final String		BILLING_SERVICE_LIST		="BILLING_SERVICE_LIST";
	
	/** The Constant COLLECTION_RECORD_TO_LIST. */
	public static final String		COLLECTION_RECORD_TO_LIST	="COLLECTION_RECORD_TO_LIST";
	
	/** The Constant COLLECTION_RECORD_TO_LIST BUT WITH MOVEMENT. */
	public static final String		COLLECTION_RECORD_MOVEMENT	="COLLECTION_RECORD_MOVEMENT";
	
	/** The Constant USER_ACCOUNT_LIST. */
	public static final String		USER_ACCOUNT_LIST			="USER_ACCOUNT_LIST";
	
	/** The Constant TYPE_DOCUMENT. */
	public static final String		TYPE_DOCUMENT				="TYPE_DOCUMENT";
	
	/** The Constant OTHER_ENTITY_DOCUMENT. */
	public static final String		OTHER_ENTITY_DOCUMENT		="OTHER_ENTITY_DOCUMENT";
	
	/** The Constant OTHER_ENTITY_DESCRIPTION. */
	public static final String		OTHER_ENTITY_DESCRIPTION 	="OTHER_ENTITY_DESCRIPTION";
	
	/** The Constant RATE_EXCEPTION_EXCLUSION_LIST. */
	public static final String		RATE_EXCEPTION_EXCLUSION_LIST ="RATE_EXCEPTION_EXCLUSION_LIST";
	
	/** The Constant MATRIX_EXCHANGE_RATE_BUY. */
	public static final String		MATRIX_EXCHANGE_RATE_BUY 	="MATRIX_EXCHANGE_RATE_BUY";
	
	/** The Constant MATRIX_EXCHANGE_RATE_SELL. */
	public static final String		MATRIX_EXCHANGE_RATE_SELL 	="MATRIX_EXCHANGE_RATE_SELL";
	
	/** The Constant MATRIX_EXCHANGE_RATE_RANGE. */
	public static final String		MATRIX_EXCHANGE_RATE_RANGE 	="MATRIX_EXCHANGE_RATE_RANGE";
	
	/** The Constant HOLDER_EXCEPTION_ALL_LIST. */
	public static final String		HOLDER_EXCEPTION_ALL_LIST 	="HOLDER_EXCEPTION_ALL_LIST";
	
	/** The Constant PARTICIPANT_EXCEPTION_ALL_LIST. */
	public static final String		PARTICIPANT_EXCEPTION_ALL_LIST 	="PARTICIPANT_EXCEPTION_ALL_LIST";

	/** The Constant SECURITY_EXCEPTION_ALL_LIST. */
	public static final String		SECURITY_EXCEPTION_ALL_LIST ="SECURITY_EXCEPTION_ALL_LIST";
	
	/** The Constant SECURITY_CLASS_EXCEPTION_ALL_LIST. */
	public static final String		SECURITY_CLASS_EXCEPTION_ALL_LIST ="SECURITY_CLASS_EXCEPTION_ALL_LIST";
	
	/** The Constant ISSUER_EXCEPTION_ALL_LIST. */
	public static final String		ISSUER_EXCEPTION_ALL_LIST ="ISSUER_EXCEPTION_ALL_LIST";
	
	/**  The Amount Minimun To Billing. */
	public static final String		MINIMUM_BILLING 			="MINIMUM_BILLING";
	
	/** The Constant ID_CUSTODY_PK. */
	public static final String  	ID_CUSTODY_PK				= "ID_CUSTODY_PK";
	
	/** The Constant ID_BLOCK_REQUEST_PK. */
	public static final String  	ID_BLOCK_REQUEST_PK			= "ID_BLOCK_REQUEST_PK";
	
	/** The Constant ID_BLOCK_OPERATION_PK. */
	public static final String  	ID_BLOCK_OPERATION_PK		= "ID_BLOCK_OPERATION_PK";
	
	/** The Constant CHANGE_OWNERSHIP_SECURITY. */
	public static final String  	CHANGE_OWNERSHIP_SECURITY	= "CHANGE_OWNERSHIP_SECURITY";
	
	/** The Constant AFFECTATION_LIST. */
	public static final String  	AFFECTATION_LIST			= "AFFECTATION_LIST";
	
	/** The Constant ACCREDITATION_CERTIFICATE. */
	public static final String  	ACCREDITATION_CERTIFICATE	= "ACCREDITATION_CERTIFICATE";
	
	/** Custody Number Request. */
	public static final String  	NUMBER_REQUEST				= "NUMBER_REQUEST";
	
	/**  Tipo de Bloqueo. */
	public static final String  	BLOCK_TYPE					= "BLOCK_TYPE";
	
	/** Counter to Exceptions To Billing. */
	public static final String  	COUNTER_EXCEPTIONS			= "COUNTER_EXCEPTIONS";
	
	/** Limit Max To Exceptions Allowed. */
	public static final Integer  	CAP_LIMIT_EXCEPTIONS		= Integer.valueOf(32000);
	
	/** The Constant ROUNDING_DIGIT_NUMBER_TWO. */
	public static final Integer 	ROUNDING_DIGIT_NUMBER_TWO	= Integer.valueOf(2);
	
	/** The Constant ROUNDING_DIGIT_NUMBER_FOUR. */
	public static final Integer 	ROUNDING_DIGIT_NUMBER_FOUR	= Integer.valueOf(4);
	
	/** The Constant ROUNDING_DIGIT_NUMBER_FIVE. */
	public static final Integer 	ROUNDING_DIGIT_NUMBER_FIVE	= Integer.valueOf(5);
	
	/** The Constant ROUNDING_DIGIT_NUMBER. */
	public static final Integer 	ROUNDING_DIGIT_NUMBER		= Integer.valueOf(8);
	
	/** The Constant ROUNDING_DIGIT_NUMBER_TEN. */
	public static final Integer 	ROUNDING_DIGIT_NUMBER_TEN	= Integer.valueOf(10);
	
	/** The Constant ROUNDING_DIGIT_NUMBER_FIFTY. */
	public static final Integer 	ROUNDING_DIGIT_NUMBER_FIFTY	= Integer.valueOf(50);
	
	/** Parameters. */
	public static final String  	COLLECTION_PERIOD				= "COLLECTION_PERIOD";
	
	/** The Constant ENTITY_COLLECTION. */
	public static final String  	ENTITY_COLLECTION				= "ENTITY_COLLECTION";
	
	/** The Constant EXCEPTION. */
	public static final String  	EXCEPTION						= "EXCEPTION";
	
	/** The Constant EXCEPTION_STATUS. */
	public static final String  	EXCEPTION_STATUS				= "EXCEPTION_STATUS";
	
	/** End Billing Service. */
	
	
	/***
	 * Init Accounting Constants
	 */

	public static final String  	DESMATERIALIZADO		= "DESMATERIALIZADO";
	
	/** The Constant REPORTADO. */
	public static final String  	REPORTADO				= "REPORTADO";
	
	/** The Constant REPORTANTE. */
	public static final String  	REPORTANTE				= "REPORTANTE";
	
	/** The Constant BLOQUEOS. */
	public static final String  	BLOQUEOS				= "BLOQUEOS";
	
	/** The Constant BLOQUEO_OTROS. */
	public static final String  	BLOQUEO_OTROS			= "BLOQUEO_OTROS";
	
	/** The Constant FISICOS. */
	public static final String  	FISICOS					= "FISICOS";
	
	/** The Constant NOT_PLACED. */
	public static final String  	NOT_PLACED				= "NO_COLOCADOS";
	
	/** The Constant SQUARING_OPERATIONS. */
	public static final String  	SQUARING_OPERATIONS		= "SQUARING_OPERATIONS";
	
	/** The Constant SQUARING_ACCOUNTING. */
	public static final String  	SQUARING_ACCOUNTING 	= "SQUARING_ACCOUNTING";
	
	/** The Constant ACCOUNTING_PROCESS. */
	public static final String  	ACCOUNTING_PROCESS 		= "ACCOUNTING_PROCESS";
	
	/** The Constant FORMAT_AMOUNT_SECURITIES. */
	public static final String  	FORMAT_AMOUNT_SECURITIES= "###,###.00";
	
	/** The Constant FORMAT_QUANTITY. */
	public static final String  	FORMAT_QUANTITY = "###,###.##";
	
	
	/** End  Accounting Constants. */
	
	/** The Constant MODALITY_GROUP_FOP. */
	public static final Long MODALITY_GROUP_FOP = new Long(4);
		
	/** The Constant FOUR_VALUE_INTEGER. */
	public static final Integer 	FOUR_VALUE_INTEGER = Integer.valueOf(4);
	
	/** The Constant PAY_SHEDULE_MODIFICATION_PAGE. */
	public static final String PAY_SHEDULE_MODIFICATION_PAGE="M";
	
	/** The Constant PAY_SHEDULE_RESTRUCTURATION_PAGE. */
	public static final String PAY_SHEDULE_RESTRUCTURATION_PAGE="R";
	
		/** The Constant LBL_HEADER_ALERT_MESSAGES. */
	public static final String LBL_HEADER_ALERT_MESSAGES="messages.alert";
	
	/** The Constant CONF_MSG_CLEAN. */
	public static final String CONF_MSG_CLEAN ="conf.msg.clean";
	
	/** The Constant NOTIFICATION_DEFAULT_HEADER. */
	public static final String NOTIFICATION_DEFAULT_HEADER ="Notificación de Sistema";
	
	/** The Constant ASSET_LAUNDRY_NAMES_CONTANTS. */
	public static final String[] ASSET_LAUNDRY_NAMES_CONTANTS= {"TIPO PERSONA","NOMBRE", "PAIS","CARGO","COMMENTS","Fecha Inicio","Fecha fin","Motivo","Fuente Origen"};

	/** The Constant ASSSET_LAUNDRY_AKALIST. */
	public static final String ASSSET_LAUNDRY_AKALIST = "akaList";
	
	/** The Constant ASSSET_LAUNDRY_UID. */
	public static final String ASSSET_LAUNDRY_UID = "uid";

	/** The Constant ASSSET_LAUNDRY_LASTNAME. */
	public static final String ASSSET_LAUNDRY_LASTNAME = "lastName";
	
	/** The Constant ASSSET_LAUNDRY_FIRSTNAME. */
	public static final String ASSSET_LAUNDRY_FIRSTNAME = "firstName";
	
	/** The Constant ASSET_LAUNDRY_SDN_TYPE. */
	public static final String ASSET_LAUNDRY_SDN_TYPE= "sdnType";
	
	/** The Constant ASSET_LAUNDRY_ENTITY_TYPE. */
	public static final String ASSET_LAUNDRY_ENTITY_TYPE= "Entity";
	
	/** The Constant ASSET_LAUNDRY_INDIVIDUAL. */
	public static final String ASSET_LAUNDRY_INDIVIDUAL = "Individual";

	/** The Constant ASSSET_LAUNDRY_SDNENTRY. */
	public static final String ASSSET_LAUNDRY_SDNENTRY = "sdnEntry";
	
	/** The Constant ASSSET_LAUNDRY_ADDRESS1. */
	public static final String ASSSET_LAUNDRY_ADDRESS1 = "address1";
	
	/** The Constant ASSSET_LAUNDRY_CITY. */
	public static final String ASSSET_LAUNDRY_CITY = "city";
	
	/** The Constant ASSSET_LAUNDRY_COUNTRY. */
	public static final String ASSSET_LAUNDRY_COUNTRY = "country";
	
	/** The Constant ASSSET_LAUNDRY_TYPE. */
	public static final String ASSSET_LAUNDRY_TYPE = "type";
	
	/** The Constant ASSSET_LAUNDRY_CATEGORY. */
	public static final String ASSSET_LAUNDRY_CATEGORY = "category";

	/** The Constant ASSSET_LAUNDRY_ADDRESSLIST. */
	public static final String ASSSET_LAUNDRY_ADDRESSLIST = "addressList";
	
	/** The Constant ASSET_LAUNDRY_COUNTRY. */
	public static final String ASSET_LAUNDRY_COUNTRY = "country";
	
	/** The Constant ASSET_LAUNDRY_VESSEL. */
	public static final String ASSET_LAUNDRY_VESSEL = "Vessel";
	
	/** The Constant HEADER_MESSAGE_EXCEPTION. */
	public static final String HEADER_MESSAGE_EXCEPTION="messages.alert";
	
	/** The Constant SKIP_FILTER_PROPERTY. */
	public static final String SKIP_FILTER_PROPERTY="skipFilterProperty";
	
	/** The Constant SEARCH_TYPE_FILTER. */
	public static final String SEARCH_TYPE_FILTER="searchTypeFilter";
	
	/** The Constant SEARCH_TYPE_FILTER_DEFAULT. */
	public static final String SEARCH_TYPE_FILTER_DEFAULT="startsWith";
	
	/**  Constant Module type. */
	public static final Integer MODULE_TYPE= Integer.valueOf(215);
	
	/** The Constant MOVEMENT_TYPE_PROPERTIES. */
	public static final String  MOVEMENT_TYPE_PROPERTIES="MovementType";
	
	/** The Constant SECURITY_LBL_CODE_ONLY. */
	public static final String SECURITY_LBL_CODE_ONLY="security.lbl.code.only";
	
	/** The Constant REPORT_GENERATED_SUCCESSFULLY. */
	public static final String REPORT_GENERATED_SUCCESSFULLY="report.msge.generation.success";
	
	/** The Constant IMAGE_EXPORT_PDF. */
	public static final String IMAGE_EXPORT_PDF="/META-INF/resources/img/logo-export.jpg";
	
	public static final String IMAGE_EXPORT_PDF_SAB = "/META-INF/resources/img/logoSab.png";

	public static final String IMAGE_EXPORT_PDF_SAFI = "/META-INF/resources/img/logoSafi.png";

	public static final String IMAGE_EXPORT_PDF_TITU = "/META-INF/resources/img/logoTitu.png";
	/** The Constant REPORT_HELPER_HOLDER_NOTEXIST. */
	public static final String REPORT_HELPER_HOLDER_NOTEXIST="holder.helper.alert.notexist";
	
	/** The Constant REPORT_HELPER_HOLDER_NOTEXIST. */
	public static final String REPORT_HELPER_HOLDER_NOTEXIST_ISSUER="holder.helper.alert.notexist.issuer"; 
	
	/** The Constant REPORT_SIZE_MB. */
	public static final String REPORT_SIZE_MB=".mb";
	
	/** The Constant CSDCORE_PREFIX_CODE. */
	public static final String CSDCORE_PREFIX_CODE= "CSDCORE";
	
	/** The Constant RECEPTION_DESCRIPTION. */
	public static final String RECEPTION_DESCRIPTION= "RECEPCION";
	
	/** The Constant SENDING_DESCRIPTION. */
	public static final String SENDING_DESCRIPTION= "ENVIO";
	
	/** The Constant DPA_DPF_MIN_DAYS_TERM. */
	public static final Integer DPA_DPF_MIN_DAYS_TERM=Integer.valueOf(1);
	
	/** The Constant DPA_DPF_MAX_DAYS_TERM_RENEWAL. */
	public static final Integer DPA_DPF_MAX_DAYS_TERM_RENEWAL = Integer.valueOf(35);
	
	/** The Constant RESPONSE_FILE_SUFIX. */
	public static final String RESPONSE_FILE_SUFIX= "_response";
	
	/** The Constant HOLDER_ACCOUNT_TYPE_SINGLE_DPF. */
	public static final String HOLDER_ACCOUNT_TYPE_SINGLE_DPF= "IND";
	
	/** The Constant HOLDER_ACCOUNT_TYPE_CO_OWNERSHIP_DPF. */
	public static final String HOLDER_ACCOUNT_TYPE_CO_OWNERSHIP_DPF= "MAN";
	
	/** The Constant DPF_BLOCK_TYPE_CAT. */
	public static final String DPF_BLOCK_TYPE_CAT= "ECA";
	
	/** The Constant DPF_BLOCK_TYPE_BAN. */
	public static final String DPF_BLOCK_TYPE_BAN= "REJ";
	
	/** The Constant DPF_BLOCK_TYPE_PAWN. */
	public static final String DPF_BLOCK_TYPE_PAWN= "PRE";
	
	/** The Constant DPF_BLOCK_TYPE_PAWN. */
	public static final String AFFECTATION_CODE_DPF_RAC= "RAC";
	
	/** The Constant DPF_BLOCK_TYPE_PAWN. */
	public static final String AFFECTATION_CODE_DPF_PAL= "PAL";

	/** The Constant LEGAL_DEPARTMENT_CODE_OTHER. */
	public static final String LEGAL_DEPARTMENT_CODE_OTHER= "OT";
	
	/** The Constant VALUE_STRING_MECHANISM_CODE. */
	public static final String VALUE_STRING_MECHANISM_CODE = "05";
	
	/** The Constant VALUE_TRADED_STRING. */
	public static final String VALUE_TRADED_STRING = "N";

	/** The Constant PARTICIPANT_CODE. */
	public static final String PARTICIPANT_CODE = "participantCode";
	
	/** The Constant TEMPLATE_NAME_SECURITY_TR_CR. */
	public static final String TEMPLATE_NAME_SECURITY_TR_CR = "TCR[participantCode][-YYYYMMdd].XML";
	
	/** The Constant STOCK_EXCHANGE_OPERATION_CLASS_CVT. */
	public static final String STOCK_EXCHANGE_OPERATION_CLASS_CVT = "CVT";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_CONFIRM. */
	public static final String ERROR_MESSAGE_INVALID_USER_CONFIRM="error.message.invalid.user.confirm";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_ANNUL. */
	public static final String ERROR_MESSAGE_INVALID_USER_ANNUL="error.message.invalid.user.annul";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_APPROVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_APPROVE="error.message.invalid.user.approve";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_REJECT. */
	public static final String ERROR_MESSAGE_INVALID_USER_REJECT="error.message.invalid.user.reject";
	
	/** The Constant ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT. */
	public static final String ERROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT="error.message.issuer.dpf.not.have.participant";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_MODIFY. */
	public static final String ERROR_MESSAGE_INVALID_USER_MODIFY="error.message.invalid.user.modify";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_UNBLOCK. */
	public static final String ERROR_MESSAGE_INVALID_USER_UNBLOCK="error.message.invalid.user.unblock";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_DELIVER. */
	public static final String ERROR_MESSAGE_INVALID_USER_DELIVER="error.message.invalid.user.deliver";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_GENERATE. */
	public static final String ERROR_MESSAGE_INVALID_USER_GENERATE="error.message.invalid.user.generate";
	
	/** The Constant ERROR_MESSAGE_RECORD_NOT_SELECTED. */
	public static final String ERROR_MESSAGE_RECORD_NOT_SELECTED = "error.message.record.not.selected";

	public static final String ERROR_MESSAGE_RECORD_JUSTONLY_SELECTED = "error.message.record.justonly.selected";
	
	public static final String ERROR_MESSAGE_RECORD_INVREQ_JUSTONLY_STATE_REGISTER = "error.message.record.invreq.justonly.state.register";

	/** The Constant ERROR_MESSAGE_INVALID_USER_MODIFY_SECURITY. */
	public static final String ERROR_MESSAGE_INVALID_USER_MODIFY_SECURITY="error.message.invalid.user.modify.security";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_BLOCK_SECURITY. */
	public static final String ERROR_MESSAGE_INVALID_USER_BLOCK_SECURITY="error.message.invalid.user.block.security";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_UNBLOCK_SECURITY. */
	public static final String ERROR_MESSAGE_INVALID_USER_UNBLOCK_SECURITY="error.message.invalid.user.unblock.security";

	/** The Constant STRING_ERROR_WHRITEEN. */
	public static final String STRING_ERROR_WHRITEEN="No se pudo leer completamente el archivo ";
	
	/** The Constant STR_DOCUMENT_PDF. */
	public static final String STR_DOCUMENT_PDF ="document-report.pdf";
	
	/** The Constant STR_EVIDENCY_OWNERSHIP. */
	public static final String STR_EVIDENCY_OWNERSHIP="CONSTANCIA DE TITULARIDAD";  
	
    public static final String STR_INFO_HOLDER="DATOS DEL TITULAR";
	
	/** The Constant STR_NEW_LINEA. */
	public static final String STR_NEW_LINEA ="\n";
	private static final String TAB2="\t\t";
	public static final String STR_COMILLA ="\"";
	
	/** The Constant STR_HOLDER_ACCOUNT. */
	public static final String STR_HOLDER_ACCOUNT = "Cuenta Titular:             "+TAB2;
	
	/** The Constant STR_COMPONENT. */
	public static final String STR_COMPONENT="Componentes:            ";
	
	/** The Constant STR_TABULATOR. */
	public static final String STR_TABULATOR="\t";
	
	
	/** The Constant STR_CUI. */
	public static final String STR_CUI="RUT: ";
	
	/** The Constant STR_INFO_SECURITY. */
	public static final String STR_INFO_SECURITY = "DATOS DEL VALOR ";
	
	
	/** The Constant STR_SECURITY_CLASS. */
	public static final String STR_SECURITY_CLASS = "Clase del Valor:           "+TAB2;
	
	/** The Constant STR_SECURITY_CODE. */
	public static final String STR_SECURITY_CODE = "Código de Valor:           "+TAB2;
	
	/** The Constant STR_NOMINAL_VALUE. */
	public static final String STR_NOMINAL_VALUE = "Valor Nominal:             "+TAB2;
	
	/** The Constant STR_CURRENCY. */
	public static final String STR_CURRENCY = "Moneda:                       "+TAB2;
	
	/** The Constant STR_SERIE_ALTERNATIVE. */
	public static final String STR_SERIE_ALTERNATIVE = "Serie Alterna:                "+TAB2;
	
	/** The Constant STR_ISSUE_DATE. */
	public static final String STR_ISSUE_DATE = "Fecha de Emisi\u00f3n:       "+TAB2;
	
	/** The Constant STR_LAST_TERM. */
	public static final String STR_LAST_TERM = "Plazo de Vida:              "+TAB2;
	
	/** The Constant STR_EXPIRATE_DATE. */
	public static final String STR_EXPIRATE_DATE = "Fecha de Vencimiento:"+TAB2 ;
	
	/** The Constant STR_ISSUE_RATE. */
	public static final String STR_ISSUE_RATE = "Tasa de Emisi\u00f3n:         "+TAB2;
	
	/** The Constant STR_CONDITION. */
	public static final String STR_CONDITION = "Condici\u00f3n:                    "+TAB2;
	
	/** The Constant STR_BALANCE. */
	public static final String STR_BALANCE = "Saldo:                           "+TAB2;
	
	/** The Constant STR_NAME_TEMP_DPF_FILE. */
	public static final String STR_NAME_TEMP_DPF_FILE = "tempDpffile";
	
	/** The Constant STR_NAME_TEMP_FTP_FILE. */
	public static final String STR_NAME_TEMP_FTP_FILE = "tempFTPfile";
	
	
	/** The Constant STR_SQ_ISSUANCE_PK. */
	public static final String STR_SQ_ISSUANCE_PK = "SQ_ID_ISSUANCE_";
	
	/** The Constant STR_SQ_PARTICIPANT_PK. */
	public static final String STR_SQ_PARTICIPANT_PK = "SQ_ID_ACCNUMBER_";
	
	/** The Constant SQ_SEND_LIP_CORP_PK. */
	public static final String SQ_SEND_LIP_CORP_PK = "SQ_SEND_LIP_CORP";
	
	/** The Constant TIKA_FORMAT_2007_OFFICE. */
	public static final String TIKA_FORMAT_2007_OFFICE = "x-tika-ooxml";
	
	/** The Constant TIKA_FORMAT_2003_OFFICE. */
	public static final String TIKA_FORMAT_2003_OFFICE = "x-tika-msoffice";
	
	/** The Constant DOC_FORMAT_2007. */
	public static final String DOC_FORMAT_2007 = "docx";
	
	/** The Constant DOC_FORMAT_2003. */
	public static final String DOC_FORMAT_2003 = "doc";

	/** The Constant DOC_FORMAT_2007. */
	public static final String EXCEL_FORMAT_2007 = "xlsx";

	/** The Constant DOC_FORMAT_2003. */
	public static final String EXCEL_FORMAT_2003 = "xls";
	
	/** The Constant FORMART_CERTIFICATE. */
	public static final String FORMART_CERTIFICATE = "octet-stream";
	
	/** The Constant FORMART_CERTIFICATE_PFX. */
	public static final String FORMART_CERTIFICATE_PFX = "pfx";
	
	/** The Constant FORMART_CERTIFICATE_STORE */
	public static final String FORMART_STORE_CERTIFICATE = "p12";
	
	public static final String FORMAT_CER_TEXT_PLAIN = "plain";
	
	public static final String FORMAT_CER_CERTIFICATE = "cer";
	
	/** The Constant WEBSERVICES_ERROR_1000. */
	public static final String WEBSERVICES_ERROR_1000 = "0000";
	
	/** The Constant WEBSERVICES_CSDCORE. */
	public static final String WEBSERVICES_CSDCORE = "CSDCORE";
	
	/** The Constant WEBSERVICES_BCB. */
	public static final String WEBSERVICES_BCB = "BCB";
	
	/** The Constant STR_UNBLOCK_TAG. */
	public static final String STR_UNBLOCK_TAG = "T";
	
	/** The Constant STR_UNBLOCK_TAG. */
	public static final String STR_UNBLOCK_TAG_FALSE = "F";
	
	/** The Constant STR_SING_TYPE. */
	public static final String STR_SING_TYPE = "IN";
		
	/** The Constant STR_IND_CHAINED. */
	public static final String STR_IND_CHAINED = "(C)";
	
	/** The Constant STR_FOR_COMPLETE. */
	public static final String STR_FOR_COMPLETE = "Por Completar";
	
	/** The Constant STR_MESSAGE_ERROR_NET_SETTLEMENT_01. */
	public static final String STR_MESSAGE_ERROR_NET_SETTLEMENT_01 = "Error durante Liquidacion Neta: Mensaje de Error :  ";
	
	/** The Constant STR_MESSAGE_ERROR_NET_SETTLEMENT_02. */
	public static final String STR_MESSAGE_ERROR_NET_SETTLEMENT_02 = "Parametros :  ";
	
	/** The name report deafult. */
	public static String NAME_REPORT_DEAFULT = "CSDCORERPT";
	

	/** The Constant EXTERNAL INTERFACE TR CR. */
	public static final Long EXTERNAL_INTERFACE_TR_CR = 1040L;
	
	public static final Long EXTERNAL_INTERFACE_CFI_FSIN = 1021L;
	
	/** The Constant PK_INFORMES_REGULADOR. */
	public static final Integer PK_INFORMES_REGULADOR = 2135;
	
	/** The Constant PK_INFORMES_ASFI. */
	public static final Integer PK_INFORMES_ASFI = 2334;
	
	/** The Constant PK_BLOQUEOS_VIGENTES. */
	public static final Integer PK_BLOQUEOS_VIGENTES = 2338;
	
	/** The Constant PK_SEND_INTERFACES_BCB. */
	public static final Integer PK_SEND_INTERFACES_BCB = 2341;
	
	/** The Constant PK_SEND_CLOSE_DAY_REPORTS. */
	public static final Integer PK_SEND_CLOSE_DAY_REPORTS = 2346;
	
	public static final Integer PK_GEOGRAPHIC_COUNTRY_USA = 64;
	
	public static final Integer COUNTRY_PUERTO_RICO = 183;

	public static final Integer COUNTRY_ISLAS_MARIANAS_NORTE = 110;

	public static final Integer COUNTRY_ISLAS_VIRGENES_ESTADOS_UNIDOS = 117;

	public static final Integer COUNTRY_GUAM = 80;

	public static final Integer COUNTRY_SAMOA_AMERICANA = 196;

	public static final Integer PK_GEOGRAPHIC_COUNTRY_PERU = 179;

	public static final Integer PK_GEOGRAPHIC_DEPARTMENT_LIMA = 1666;

	public static final Integer PK_GEOGRAPHIC_PROVINCE_LIMA = 9578;

	public static final Integer PK_GEOGRAPHIC_DISTRICT_LIMA= 234607;

	public static final Integer WORK_ACTIVITY_PERU = 281;

	public static final Integer INTERNAL = 33;
	
	public static final Integer EXTERNAL = 34;
	
	public static String CONTRATO_PN = "CONTRATO_DE_INTERMEDIACION_SAB_PN";

	public static String CONTRATO_PJ = "CONTRATO_DE_INTERMEDIACION_SAB_PJ";

	public static String CONTRATO_PA = "CONTRATO_DE_INTERMEDIACION_SAB_PA";

	/** The max rows. */
	public static Integer MAX_ROWS = 1000;
	
	/** The Constant ONE_HUNDRED. */
	public static final Double ONE_HUNDRED = 100D;
	
	/** The Constant ONE_HUNDRED. */
	public static Integer IND_REPROCESS_NOMINAL_VALUE = 2356;

	public static final String STR_DPF_SECURITIES_LIST= "STR_DPF_SECURITIES_LIST"; 
	
	/** The Constant DAYS_OF_LIFE_COUPON_SIRTEX. */
	public static Integer DAYS_OF_LIFE_COUPON_SIRTEX = 1824;
	
	/**PARA BUSQUEDA DE PERSONAS.*/
	public static final String LBL_ALERT_APPLICANT_NUM_MISSING="alert.applicant.num.missing";
	/***/
	public static final String LBL_ALERT_APPLICANT_TYPE_MISSING="alert.applicant.type.missing";
	/***/
	public static final String LBL_ALERT_APPLICANT_IS_ALREADY_REGISTERED="alert.applicant.is.already.registered";
	/***/
	public static final String LBL_SUCCESS_APPLICANT_REGISTERED="success.applicant.registered";
	/***/
	public static final String LBL_ALERT_NO_APPLICANT_REGISTERED="alert.no.applicant.registered";
	/***/
	public static final String LBL_ALERT_SPECIFY_DATA_OF_SEARCH="alert.specify.data.of.search";
	/***/
	public static final String LBL_ALERT_MUST_ENTER_DOCUMENT_NUMBER="alert.must.enter.document.number";
	/***/
	public static final String LBL_ALERT_MUST_ENTER_DOCUMENT_TYPE="alert.must.enter.document.type";
	/***/
	public static final String LBL_ALERT_YOU_MUST_END_SEARCH="alert.you.must.end.search";
	/***/
	public static final String LBL_ALERT_EXCHANGE_RATE_REQUIRED="alert.exchange.rate.required";
	/***/
	public static final String THERE_ARE_TOO_MANY_REPORTS_THERE_COULD_BE_A_PROBLEM="there.are.too.many.reports.there.could.be.a.problem";
	/***/
	public static final String REPORT_WERE_GENERATED_CORRECTLY="report.were.generated.correctly";
	
	public static String REDEMTION_TYPE_PARTIAL = "P";
	public static String REDEMTION_TYPE_TOTAL = "T";
	
		public static String STRING_TWO_ZERO_STR_DEFAULT = "00";

	public static String STRING_SEND_OTHER_BY_STR_DEFAULT = "0000";

	public static String STRING_FUNDS_SOURCE_STR_DEFAULT = "0000000000";

	public static String MODIFY_OR_VIEW_DETAIL = "modifyOrViewDetail";
	
	public static String PERSON_TYPE = "personType";

	public static String ORIGIN_DETAIL_COSTUMER = "originDetailCostumer";
	
	public static String LONG_ID_HOLDER_PK = "idHolderPk";

	public static String ACRES_INSOLUTION_PE_SEARCH_DOC = "//acres.insolutions.pe/busqueda?cod_doc=";

	public static String BASE_64_IMG_DEMO="/9j/4AAQSkZJRgABAQEAeAB4AAD/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAEAAAAAAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAIAAcDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9evC3iX4uRftxahYXmm6lcfDeaCdRcSQxx2tsojjaF0bdlm8zKEA7j5hJQBMqUUUAf//Z";
	
	public static String SUBJECT_NOTIFICATION_UPDATE = "MENSAJE ACTUALIZACION DE DATOS";

	public static int ADULT = 18;
	
	public static String QRSIGN = "qrsign";
	
	public static String PGP_EXTENSION = ".pgp";
	
	public static String OPEN_ACCOUNT_USER = "OPEN_USER";
	
	public static String CONTRATO_PN_SAB  = "PN_SAB_FICHA_REG_CLIENTE";
	
	public static String CONTRATO_PN_SAFI = "PN_SAFI_FICHA_REG_CLIENTE";

	public static String CONTRATO_PN_TITU = "PN_TITU_FICHA_REG_CLIENTE";

	public static String CONTRATO_PJ_SAB  = "PJ_SAB_FICHA_REG_CLIENTE";
	
	public static String CONTRATO_PJ_SAFI = "PJ_SAFI_FICHA_REG_CLIENTE";

	public static String CONTRATO_PJ_TITU = "PJ_TITU_FICHA_REG_CLIENTE";

	public static String CONTRATO_PA_SAB  = "PA_SAB_FICHA_REG_CLIENTE";
	
	public static String CONTRATO_PA_SAFI = "PA_SAFI_FICHA_REG_CLIENTE";

	public static String CONTRATO_PA_TITU = "PA_TITU_FICHA_REG_CLIENTE";
	
	public static String ISSUER_CODE_INIT = "ACR";
	
	public static String SETUP_PARTICIPANT_CODE_INIT = "SACR";
	
	public static  Long  PATRIMONY_MANAGED_ACRES_FUNDS= new Long(6);
	
	public static  Long  PATRIMONY_MANAGED_ACRES_TITULIZADORA= new Long(4);
	
	public static Integer REQUEST_SOCIAL_OBJECT_FUNDS = 643;
	
	public static String USER_INFO_PRM = "user_info_prm";
	
	public static String SYSTEM_SAB  = "SYSTEM_SAB";
	
	public static String SYSTEM_SAFI = "SYSTEM_SAFI";

	public static String SYSTEM_TITU = "SYSTEM_TITU";

	public static String ID_ACCOUNT_ANNOTATION_PK = "ID_ACCOUNT_ANNOTATION_PK";
	
	public static String LIST_ID_ACCOUNT_ANNOTATION_PK = "LIST_ID_ACCOUNT_ANNOTATION_PK";
	
	public static String PLAFT_MASSIVE_PROCESS_COUNT = "PLAFT_RISK_PEOPLE_S";
	
	public static String PLAFT_MASSIVE_PROCESS_REJECTED = "R";
			
	public static String PLAFT_MASSIVE_PROCESS_ACCEPTED = "A";
	
	public static Integer PARAMETER_IGV = 2299;
	
	public static Integer PARAMETER_RPJ_SAB = 2303;
	
	public static final String CORRELATIVE_SAB = "ACRES";
	
	public static final String CORRELATIVE_BVL = "BVL";
	
	public static final String ARRAY_ASSIGNMENT_PROCESS = "ArrAssignmentProcessPk";
	
	public static final String SPANISH_YES = "SI?";
	
	public static final String SPANISH_NO = "NO";

	public static final String PAYROLL_HEADER_PK= "PAYROLL_HEADER_PK";

	public static final String PAYROLL_DETAIL_PK= "PAYROLL_DETAIL_PK";

	public static final String LIST_PAYROLL_HEADER_PK= "LIST_PAYROLL_HEADER_PK";

	public static final String LIST_PAYROLL_DETAIL_PK= "LIST_PAYROLL_DETAIL_PK";

	public static final String USER_NAME= "USER_NAME";
	
	public static final String GENERAL_NAME_REGEX_PATTERN="exp.regular.alphabetic.name.lastname";
	
	public static final String HOLDER_ACCOUNT_MODIFY_REPORT="CVPRPT125";
	
	public static final Integer NEGO_PERCENTAGE_DECIMAL =6;

	public static final Integer SHOW_PERCENTAGE_DECIMAL =4;
	
	public static final String MIN_HACIENDA_DOC = "80024627-6";
	
	public static final String MIN_HACIENDA_NAME = "MINISTERIO DE HACIENDA";
}
