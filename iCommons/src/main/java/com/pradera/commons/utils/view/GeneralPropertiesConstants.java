package com.pradera.commons.utils.view;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/05/2013
 */
public class GeneralPropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	private GeneralPropertiesConstants() {
		super();
	}
	
	/** The Constant ERROR_LESS_THAN_OR_EQUAL_TO. */
	public final static String ERROR_LESS_THAN_OR_EQUAL_TO="error.less.than.or.equal.to";
	
	/** The Constant ERROR_GREATER_THAN_OR_EQUAL_TO. */
	public final static String ERROR_GREATER_THAN_OR_EQUAL_TO="error.greater.than.or.equal.to";
	
	/** The Constant ERROR_GREATER_THAN. */
	public final static String ERROR_GREATER_THAN="error.greater.than";
	
	/** The Constant ERROR_LESS_THAN. */
	public final static String ERROR_LESS_THAN="error.less.than";
	
	/** The Constant ERROR_COMBO_REQUIRED. */
	public final static String ERROR_COMBO_REQUIRED="error.combo.required";
	
	/** The Constant ERROR_FIELD_REQUIRED. */
	public final static String ERROR_FIELD_REQUIRED="error.field.required";
	
	/** The Constant ERROR_MULTIPLO. */
	public final static String ERROR_MULTIPLO="error.multiplo";
	
	/** The Constant LBL_MOTIVE_BLOCK. */
	public final static String LBL_MOTIVE_BLOCK="lbl.motive.block";
	
	/** The Constant LBL_MOTIVE_UNBLOCK. */
	public final static String LBL_MOTIVE_UNBLOCK="lbl.motive.unblock";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_SUPPORTED. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_NOT_SUPPORTED = "error.validator.document.type.not.supported";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_ALPHANUMERIC. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_ALPHANUMERIC = "error.validator.document.type.alphanumeric";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_NUMERIC. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_NUMERIC = "error.validator.document.type.numeric";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_INVALID_FORMAT. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_INVALID_FORMAT  = "error.validator.document.type.invalid.format";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_INVALID_FORMAT_S. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_INVALID_FORMAT_S  = "error.validator.document.type.invalid.format.s";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_INVALID_VERIFIER_DIGIT. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_INVALID_VERIFIER_DIGIT = "error.validator.document.type.invalid.verifier.digit";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_1_FIRST_ZERO. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_1_FIRST_ZERO= "error.validator.document.type.1.first.zero";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_2_FIRSTS_ZERO. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_2_FIRSTS_ZERO= "error.validator.document.type.2.firsts.zero";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_11_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_11_DIGITS = "error.validator.document.type.11.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_9_11_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_9_11_DIGITS = "error.validator.document.type.9.11.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_16_18_21_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_16_18_21_DIGITS = "error.validator.document.type.16.18.21.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_AN_LESS_THAN_1900. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_AN_LESS_THAN_1900 = "error.validator.document.type.an.less.than.1900";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_CANNOT_TAKE_SYMBOLS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_CANNOT_TAKE_SYMBOLS= "error.validator.document.type.cannot.take.symbols";
	
	/** The Constant QUOTATION_REGISTER_SUCCESS. */
	public static final String QUOTATION_REGISTER_SUCCESS = "quotation.msg.register.success";
	
	/** The Constant QUOTATION_MODIFY_SUCCESS. */
	public static final String QUOTATION_MODIFY_SUCCESS = "quotation.msg.modify.success";
	
	/** The Constant QUOTATION_FILE_INCORRECTNAME. */
	public static final String QUOTATION_FILE_INCORRECTNAME = "quotation.error.msg.filename";
	
	/** The Constant QUOTATION_FILE_REGISTER_SUCCESS. */
	public static final String QUOTATION_FILE_REGISTER_SUCCESS = "quotation.msg.register.file.success";
	
	/** The Constant QUOTATION_FILE_UPLOADED_SUCCESS. */
	public static final String QUOTATION_FILE_UPLOADED_SUCCESS = "quotation.msg.upload.success";
	
	/** The Constant QUOTATION_FILE_STATUS. */
	public static final String QUOTATION_FILE_STATUS = "quotation.msg.file.status";
	
	/** The Constant TIPO_CAMBIO__REGISTER_SUCCESS. */
	public static final String TIPO_CAMBIO__REGISTER_SUCCESS = "tipo.msg.status.success";
	
	/** The Constant TYPE_SHIFT_REGISTER_FAIL. */
	public static final String TYPE_SHIFT_REGISTER_FAIL = "tipo.msg.status.falure";
	
	/** The Constant REGISTER_TYPE_CHANGE_SUCCESS. */
	public static final String REGISTER_TYPE_CHANGE_SUCCESS = "tipo.msg.register.success";
	
	/** The Constant REGISTER_TYPE_CHANGE_FAILURE. */
	public static final String REGISTER_TYPE_CHANGE_FAILURE = "tipo.msg.register.failure";
	
	/** The Constant MODIFY_TYPE_CHANGE_SUCCESS. */
	public static final String MODIFY_TYPE_CHANGE_SUCCESS = "tipo.msg.modify.success";
	
	/** The Constant MODIFY_TYPE_CHANGE_FAILURE. */
	public static final String MODIFY_TYPE_CHANGE_FAILURE = "tipo.msg.modfiy.failure";
	
	/** The Constant TYPE_EXCHANGE_LOSS_VALIDATION. */
	public static final String TYPE_EXCHANGE_LOSS_VALIDATION = "tipo.msg.negative.numbers";
	
	/** The Constant GEOGRAPHICLOCATION_REGISTER_SUCCESS. */
	public static final String GEOGRAPHICLOCATION_REGISTER_SUCCESS = "geolocation.msg.register.success";
	
	/** The Constant GEOGRAPHICLOCATION_MODIFY_SUCCESS. */
	public static final String GEOGRAPHICLOCATION_MODIFY_SUCCESS = "geolocation.msg.modify.success";
	
	/** The Constant GEOGRAPHICLOCATION_BLOCKED_SUCCESS. */
	public static final String GEOGRAPHICLOCATION_BLOCKED_SUCCESS = "geolocation.msg.block.success";
	
	/** The Constant GEOGRAPHICLOCATION_BLOCKED_FAIL. */
	public static final String GEOGRAPHICLOCATION_BLOCKED_FAIL = "geolocation.msg.block.fail";
	
	/** The Constant GEOGRAPHICLOCATION_LOCALCOUNTRY_REGISTERED. */
	public static final String GEOGRAPHICLOCATION_LOCALCOUNTRY_REGISTERED = "geolocation.error.localcountry.registered";
	
	/** The Constant GEOGRAPHICLOCATION_CNF_REGISTER. */
	public static final String GEOGRAPHICLOCATION_CNF_REGISTER="geolocation.msg.confirm.register";
	
	/** The Constant GEOGRAPHICLOCATION_CNF_MODIFY. */
	public static final String GEOGRAPHICLOCATION_CNF_MODIFY="geolocation.msg.confirm.modify";
	
	/** The Constant GEOGRAPHICLOCATION_REQUIRED_LOCATIONTYPE. */
	public static final String GEOGRAPHICLOCATION_REQUIRED_LOCATIONTYPE="geolocation.msg.required.locationtype";
	
	/** The Constant GEOGRAPHICLOCATION_REQUIRED_COUNTRY. */
	public static final String GEOGRAPHICLOCATION_REQUIRED_COUNTRY="geolocation.msg.required.country";
	
	/** The Constant GEOGRAPHICLOCATION_REQUIRED_PROVINCE. */
	public static final String GEOGRAPHICLOCATION_REQUIRED_PROVINCE="geolocation.msg.required.province";
	
	/** The Constant MASTER_DEFINITION_SAVE_SUCCESS. */
	public static final String MASTER_DEFINITION_SAVE_SUCCESS = "generalparameters.master.lbl.savedefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_MODIFY_SUCCESS. */
	public static final String MASTER_DEFINITION_MODIFY_SUCCESS = "generalparameters.master.lbl.modifydefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_MODIFY_FAIL. */
	public static final String MASTER_DEFINITION_MODIFY_FAIL = "generalparameters.master.lbl.modifydefinitionfail";
	
	/** The Constant MASTER_DEFINITION_CONFIRM_SUCCESS. */
	public static final String MASTER_DEFINITION_CONFIRM_SUCCESS = "generalparameters.master.lbl.confirmdefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_CONFIRM_FAIL. */
	public static final String MASTER_DEFINITION_CONFIRM_FAIL = "generalparameters.master.lbl.confirmdefinitionfail";
	
	/** The Constant MASTER_DEFINITION_REJECT_SUCCESS. */
	public static final String MASTER_DEFINITION_REJECT_SUCCESS = "generalparameters.master.lbl.rejectdefinitionsucess";
	
	/** The Constant MASTER_DEFINITION_REJECT_FAIL. */
	public static final String MASTER_DEFINITION_REJECT_FAIL = "generalparameters.master.lbl.rejectdefinitionfail";
	
	/** The Constant MASTER_DEFINITION_SAVE_FAILURE. */
	public static final String MASTER_DEFINITION_SAVE_FAILURE = "generalparameters.master.lbl.savedefinitionfail";
	
	/** The Constant MASTER_ELEMENT_SAVE_SUCCESS. */
	public static final String MASTER_ELEMENT_SAVE_SUCCESS = "generalparameters.master.lbl.saveelementsucess";
	
	/** The Constant MASTER_ELEMENT_SAVE_FAILURE. */
	public static final String MASTER_ELEMENT_SAVE_FAILURE = "generalparameters.master.lbl.savedelementfail";
	
	/** The Constant MASTER_ELEMENT_MODIFY_SUCCESS. */
	public static final String MASTER_ELEMENT_MODIFY_SUCCESS = "generalparameters.master.lbl.modifyelementsucess";
	
	/** The Constant MASTER_ELEMENT_MODIFY_FAILURE. */
	public static final String MASTER_ELEMENT_MODIFY_FAILURE = "generalparameters.master.lbl.modifyelementfail";
	
	/** The Constant MASTER_ELEMENT_CONFIRM_SUCCESS. */
	public static final String MASTER_ELEMENT_CONFIRM_SUCCESS = "generalparameters.master.lbl.confirmelementsucess";
	
	/** The Constant MASTER_ELEMENT_CONFIRM_FAILURE. */
	public static final String MASTER_ELEMENT_CONFIRM_FAILURE = "generalparameters.master.lbl.confirmelementfail";
	
	/** The Constant MASTER_ELEMENT_REJECT_SUCCESS. */
	public static final String MASTER_ELEMENT_REJECT_SUCCESS = "generalparameters.master.lbl.rejectlementsucess";
	
	/** The Constant MASTER_ELEMENT_REJECT_FAILURE. */
	public static final String MASTER_ELEMENT_REJECT_FAILURE = "generalparameters.master.lbl.rejectelementfail";
	
	/** The Constant MASTER_SEARCH_MESSAGE. */
	public static final String MASTER_SEARCH_MESSAGE = "generalparameters.master.msg.searchtype";
	
	/** The Constant MASTER_CONFIRM. */
	public static final String MASTER_CONFIRM = "generalparameters.master.msg.searchconfirmmaster";
	
	/** The Constant MASTER_REJECT. */
	public static final String MASTER_REJECT = "generalparameters.master.msg.searchrejectmaster";
	
	/** The Constant MASTER_DEFINITION_FOR_ELEMENT. */
	public static final String MASTER_DEFINITION_FOR_ELEMENT = "generalparameters.master.msg.selectelementdef";
	
	/** The Constant MASTER_DEFINITION_NOT_FOUND. */
	public static final String MASTER_DEFINITION_NOT_FOUND = "generalparameters.master.msg.selecteledef";
	
	/** The Constant MASTER_NO_RECORDS_FOUND. */
	public static final String MASTER_NO_RECORDS_FOUND = "generalparameters.master.msg.norecords";
	
	/** The Constant MASTER_EXCEPTION. */
	public static final String MASTER_EXCEPTION = "generalparameters.master.msg.exception";
	
	/** The Constant MASTER_SEARCH_FECHA. */
	public static final String MASTER_SEARCH_FECHA = "generalparameters.master.msg.searchfecha";
	
	/** The Constant MASTER_SEARCH_SELECT_NORECORDS. */
	public static final String MASTER_SEARCH_SELECT_NORECORDS = "generalparameters.master.msg.selrec";
	
	/** The Constant HOLIDAY_REGISTER_SUCCESS. */
	public static final String HOLIDAY_REGISTER_SUCCESS="holiday.msg.register.success";
	
	/** The Constant HOLIDAY_MODIFY_SUCCESS. */
	public static final String HOLIDAY_MODIFY_SUCCESS="holiday.msg.modify.success";
	
	/** The Constant CODE_USER_EMPTY. */
	public static final String CODE_USER_EMPTY="generalparameters.msg.search.empty";
	
	/** The Constant GENERALPARAMETERS_REGISTER_SUCCESS. */
	public static final String GENERALPARAMETERS_REGISTER_SUCCESS = "general.msg.register.success";
	
	/** The Constant GENERALPARAMETERS_MODIFY_SUCCESS. */
	public static final String GENERALPARAMETERS_MODIFY_SUCCESS = "general.msg.modify.success";
	
	/** The Constant QUOTATION_VALOR_VALUE_REQUIRED. */
	public static final String QUOTATION_VALOR_VALUE_REQUIRED = "quotation.msg.required.valor";
	
	/** The Constant QUOTATION_VALOR_VALUE_INVALID. */
	public static final String QUOTATION_VALOR_VALUE_INVALID = "quotation.msg.invalid.valor";
	
	/** The Constant INDEX_REGISTERED_SUCCESS. */
	public static final String INDEX_REGISTERED_SUCCESS ="general.msg.register.success";
	
	/** The Constant INDEX_MODIFIED_SUCCESS. */
	public static final String INDEX_MODIFIED_SUCCESS="general.msg.modify.success";
	
	/** The Constant USER_FOUND. */
	public static final String USER_FOUND = "notifications.msg.user.exist";
	
	/** The Constant INITIAL_FINAL_FAIL. */
	public static final String INITIAL_FINAL_FAIL = "notifications.msg.initial.final.fail.exist";
	
	/** The Constant USER_NOT_FOUND. */
	public static final String USER_NOT_FOUND = "notifications.msg.user.didNotExist"; 
	
	/** The Constant CHANGE_MONEY_TYPE_BUYPRICE. */
	public static final String CHANGE_MONEY_TYPE_BUYPRICE="tipo.msg.purchaseprice.required";
	
	/** The Constant CHANGE_MONEY_TYPE_SELLPRICE. */
	public static final String CHANGE_MONEY_TYPE_SELLPRICE="tipo.msg.saleprice.required";
	
	/** The Constant CHANGE_MONEY_TYPE_AVGPRICE. */
	public static final String CHANGE_MONEY_TYPE_AVGPRICE="tipo.msg.avgprice.required";
	
	/** The Constant CHANGE_MONEY_TYPE_BUYPRICE_ZERO. */
	public static final String CHANGE_MONEY_TYPE_BUYPRICE_ZERO="tipo.msg.purchaseprice.zero";
	
	/** The Constant CHANGE_MONEY_TYPE_SELLPRICE_ZERO. */
	public static final String CHANGE_MONEY_TYPE_SELLPRICE_ZERO="tipo.msg.saleprice.zero";
	
	/** The Constant CHANGE_MONEY_TYPE_AVGPRICE_ZERO. */
	public static final String CHANGE_MONEY_TYPE_AVGPRICE_ZERO="tipo.msg.avgprice.zero";
	
	/** The Constant SAVE_SUCCESS_MSG. */
	public static final String SAVE_SUCCESS_MSG = "business.save.success";
	
	/** The Constant USER_ID. */
	public static final String USER_ID = "notifications.id";
	
	/** The Constant USER_EMAIL. */
	public static final String USER_EMAIL= "opc.generales.email";
	
	/** The Constant USER_MOBILE. */
	public static final String USER_MOBILE="notifications.mobile";
	
	/** The Constant MASTER_MGMT_DEFINITION_EXISTS. */
	public static final String MASTER_MGMT_DEFINITION_EXISTS = "generalparameters.master.msg.defexists";
	
	/** The Constant MASTER_MGMT_ELEMENT_EXISTS. */
	public static final String MASTER_MGMT_ELEMENT_EXISTS = "generalparameters.master.msg.elementexists";
	
	/** The Constant MASTER_SERACH_DEF_MODIFY. */
	public static final String MASTER_SERACH_DEF_MODIFY = "generalparameters.master.msg.searchmodifymaster";
	
	/** The Constant INTERVAL_NOT_POSSIABLE. */
	public static final String INTERVAL_NOT_POSSIABLE = "notfications.msg.interval.fail";
	
	/** The Constant ENDDATE_NOT_BEFORE_CURRENTDATE. */
	public static final String ENDDATE_NOT_BEFORE_CURRENTDATE="indexrate.error.msg.datevalidation.fail";
	
	/** The Constant INDEX_REGISTER_SUCCESS. */
	public static final String INDEX_REGISTER_SUCCESS="indexrate.index.register.success";
	
	/** The Constant RATE_REGISTER_SUCCESS. */
	public static final String RATE_REGISTER_SUCCESS="indexrate.rate.register.success";
	
	/** The Constant INDEX_RATE_MODIFY_SUCCESS. */
	public static final String INDEX_RATE_MODIFY_SUCCESS="indexrate.modify.success";
	
	/** The Constant INDEX_RATE_UPDATE_SUCCESS. */
	public static final String INDEX_RATE_UPDATE_SUCCESS="indexrate.update.success";
	
	/** The Constant ERROR_COUNTRY_REQUIRED. */
	public static final String ERROR_COUNTRY_REQUIRED="holiday.error.msg.country.required";
	
	/** The Constant ERROR_COMPORTEMENTAL_REQUIRED. */
	public static final String ERROR_COMPORTEMENTAL_REQUIRED="holiday.error.msg.behaviour.required";
	
	/** The Constant CNF_REGISTER_HOLIDAY. */
	public static final String CNF_REGISTER_HOLIDAY="holidayMgmt.mesg.dateResistration";
	
	//masterTable
	/** The Constant INDICATOR_REQUIRED. */
	public static final String INDICATOR_REQUIRED="mstrtbl.indicator.required";
	
	/** The Constant INTEGER_REQUIRED. */
	public static final String INTEGER_REQUIRED="mstrtbl.NumeroEntero.required";
	
	/** The Constant DATE_REQUIRED. */
	public static final String DATE_REQUIRED="mstrtbl.date.required";
	
	/** The Constant DECIMAL_AMOUNT_REQUIRED. */
	public static final String DECIMAL_AMOUNT_REQUIRED="mstrtbl.decimalamount.required";
	
	/** The Constant STRING_REQUIRED. */
	public static final String STRING_REQUIRED="mstrtbl.string.required";
	
	/** The Constant SUBLOCATION_REQUIRED. */
	public static final String SUBLOCATION_REQUIRED="mstrtbl.sublocation.required";
	
	/** The Constant STATE_REQUIRED. */
	public static final String STATE_REQUIRED="mstrtbl.state.required";
	
	/** The Constant OBSERVATION_REQUIRED. */
	public static final String OBSERVATION_REQUIRED="mstrtbl.observation.required";
	
	/** The Constant SELECT_RECORD_TOACTIVE. */
	public static final String SELECT_RECORD_TOACTIVE="process.err.msg.select.record.active";
	
	/** The Constant SELECT_RECORD_TODEACTIVE. */
	public static final String SELECT_RECORD_TODEACTIVE="process.err.msg.select.record.deactive";
	
	/** The Constant SCHEDULE_PROCESS_ALREADY_EXISTS. */
	public static final String 	SCHEDULE_PROCESS_ALREADY_EXISTS="process.err.msg.scheduleprocess.exist";
	
	/** Constants Commons *. */
	
	public static final String ERROR_INPUT_TEXT = "error.input.text";
	
	/** The Constant ERROR_INPUT_NUMBER. */
	public static final String ERROR_INPUT_NUMBER = "error.input.number";
	
	/** The Constant ERROR_COMBO_SELECT. */
	public static final String ERROR_COMBO_SELECT= "error.combo.select";
	
	/** The Constant PROPERTYFILE. */
	public static final String PROPERTYFILE = "ValidationMessages";
	
	/** The Constant ENTER_LEAST_ONE. */
	public static final String ENTER_LEAST_ONE = "enter.least.least.one";
	

	/** The Constant ERROR_IN_ACCREDITATION_BALANCE. */
	public static final String ERROR_IN_ACCREDITATION_BALANCE = "error.accreditation.certificate.accreditation_balance";
	
	/** The Constant ERROR_ON_ACCOUNT_AND_PARTICIPANT. */
	public static final String ERROR_ON_ACCOUNT_AND_PARTICIPANT = "error.accreditation.certificate.holder_participant";
	
	/** The Constant CONFIRM_ACCREDITATION_SAVE. */
	public static final String CONFIRM_ACCREDITATION_SAVE ="confirm.accreditation.certificate.save_confirm";
	
	/** The Constant ERROR_FINDING_HOLDER. */
	public static final String ERROR_FINDING_HOLDER = "error.accreditation.certificate.finding_holder";
	
	/** The Constant ERROR_CONCURRENCY_UPDATE_GENERAL. */
	public static final String ERROR_CONCURRENCY_UPDATE_GENERAL= "persistence.concurrency.lock.general.message";
	
	/** The Constant ERROR_CONCURRENCY_UPDATE_REQUEST. */
	public static final String ERROR_CONCURRENCY_UPDATE_REQUEST = "persistence.concurrency.lock.request.message";	
	
	/** The Constant LBTR_FILE_ERROR. */
	public final static String LBTR_FILE_ERROR = "lbtr.file.error";
	
	/** The Constant LBTR_REQUIRED_FILE. */
	public final static String LBTR_REQUIRED_FILE = "lbl.upload.lbtr.requiredData";
	
	/** The Constant LBTR_REQUIRED_LOAD_FILE. */
	public final static String LBTR_REQUIRED_LOAD_FILE = "lbl.upload.lbtr.requiredLoadFile";
	
	/** The Constant LBTR_CONFIRM_MESSAGE. */
	public final static String LBTR_CONFIRM_MESSAGE="lbl.upload.lbtr.confirmationMessage";
	
	/** The Constant LBTR_SUCCESSFULLY_SAVED. */
	public final static String LBTR_SUCCESSFULLY_SAVED="lbl.upload.lbtr.saveOperations";
	
	/** The Constant FUPL_ERROR_INVALID_FILE_TYPE. */
	public final static String FUPL_ERROR_INVALID_FILE_TYPE="fupl.error.invalid.file.type";
	
	/** The Constant FUPL_ERROR_INVALID_FILE_SIZE. */
	public final static String FUPL_ERROR_INVALID_FILE_SIZE="fupl.error.invalid.file.size";
	
	/** The Constant LBTR_SUCCESSFULL_LOADED. */
	public final static String LBTR_SUCCESSFULL_LOADED="lbl.upload.lbtr.loadedOperation";
	
	/** The Constant ERROR_DOC_COUNT_ONE_ZERO_VALID. */
	public final static String ERROR_DOC_COUNT_ONE_ZERO_VALID="error.validator.document.type.count.one.zero.valid";
	
	/** The Constant ERROR_DOC_COUNT_THREE_ZERO_VALID. */
	public final static String ERROR_DOC_COUNT_THREE_ZERO_VALID="error.validator.document.type.count.three.zero.valid";
	
	/** The Constant ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS. */
	public final static String ERROR_DOCUMENTS_TYPE_AND_NUMBER_EQUALS="error.documents.type.and.number.equals";
	
	/** The Constant EXIST_REPRESENTATIVE_INFORMATION_EXPORT. */
	public final static String EXIST_REPRESENTATIVE_INFORMATION_EXPORT="exist.representative.information.export";
	
	/** The Constant EXIST_HOLDER_INFORMATION_EXPORT. */
	public final static String EXIST_HOLDER_INFORMATION_EXPORT="exist.holder.information.export";
	
	/** The Constant ERROR_SAME_TYPE_DOCUMENT. */
	public final static String ERROR_SAME_TYPE_DOCUMENT="error.same.type.document";
	
	/** The Constant WARNING_SAME_NATIONALITY. */
	public static final String WARNING_SAME_NATIONALITY = "warning.same.nationality";
	
	/** The Constant WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE. */
	public static final String WARNING_FILE_REQUEST_REPRESENTATIVE_NO_COMPLETE = "warning.file.representative.no.complete";
	
	/** The Constant ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO. */
	public static final String ERROR_ADM_HOLDER_REQUEST_DATE_BIRTHDAY_DATE_ONENINEZEROZEO="error.adm.holder.request.date.birthday.date.oneninezerozero";
	
	/** The Constant ERROR_REPRESENTATIVE_AGE_NO_MAJOR. */
	public static final String ERROR_REPRESENTATIVE_AGE_NO_MAJOR = "error.representative.age.no.major";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_6_20_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_6_20_DIGITS = "error.validator.document.type.6.20.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_6_15_DIGITS = "error.validator.document.type.6.15.digits";

	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_21_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_21_DIGITS = "error.validator.document.type.21.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_12_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_12_DIGITS = "error.validator.document.type.12.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_15_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_15_DIGITS = "error.validator.document.type.15.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_10_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_10_DIGITS = "error.validator.document.type.10.digits";
	
	/** The Constant ERROR_VALIDATOR_DOCUMENT_TYPE_17_DIGITS. */
	public static final String ERROR_VALIDATOR_DOCUMENT_TYPE_17_DIGITS = "error.validator.document.type.17.digits";
	
	/** The Constant IDLE_TIME_EXPIRED. */
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
	
	/** The Constant ERROR_DOC_COUNT_ONE_ZERO_FIRST. */
	public static final String ERROR_DOC_COUNT_ONE_ZERO_FIRST = "error.doc.count.one.zero.firts";
	
	/** The Constant ERROR_DOC_COUNT_THREE_ZERO_FIRST. */
	public static final String ERROR_DOC_COUNT_THREE_ZERO_FIRST = "error.doc.count.three.zero.firts";
	
	/** The Constant WARNING_NECESSARY_TWO_FATHER_REPRESENTATIVE_CLASS. */
	public static final String WARNING_NECESSARY_TWO_FATHER_REPRESENTATIVE_CLASS = "legal.representative.warning.necessary.two.father.representative.class";
    
	/** The Constant WARNING_NECESSARY_TWO_TUTOR_LEGAL_REPRESENTATIVE_CLASS. */
	public static final String WARNING_NECESSARY_TWO_TUTOR_LEGAL_REPRESENTATIVE_CLASS = "legal.representative.warning.necessary.two.legal.tutor.representative.class";
	
	/** The Constant WARNING_FATHER_AND_TUTOR_LEGAL_REPRESENTATIVE_CLASS. */
	public static final String WARNING_FATHER_AND_TUTOR_LEGAL_REPRESENTATIVE_CLASS = "legal.representantive.warning.father.and.tutor.representative.class";
	
	/** The Constant LBL_HELPER_SECURITY_NOT_FOUND. */
	public static final String LBL_HELPER_SECURITY_NOT_FOUND = "lbl.helper.security.notFound";
	
	/** The Constant ERROR_NIT_NO_EXIST. */
	public static final String ERROR_NIT_NO_EXIST = "error.nit.no.exist";
	
	/** The Constant ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE. */
	public static final String ERROR_DOCUMENT_TYPE_NUMBER_HOLDER_REPRESENTATIVE="error.document.type.number.holder.representative";

	/** The Constant ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE. */
	public static final String ERROR_DOCUMENT_TYPE_NUMBER_EQUAL_REPRESENTATIVE="error.document.type.number.equal.representative";
	
	/** The Constant ERROR_PEP_DATE_TO_AGE_REPRESENTATIVE. */
	public static final String ERROR_PEP_DATE_TO_AGE_REPRESENTATIVE="error.pep.date.to.age.representative";
	
	/** The Constant ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN. */
	public static final String ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN="error.validator.number.character.hyphen";
	
	/** The Constant ERROR_VALIDATOR_EXIST_CHARACTER_HYPHEN_CEDULA_PARAGUAYA. */
	public static final String ERROR_VALIDATOR_NUMBER_CHARACTER_HYPHEN_CI="error.validator.number.character.hyphen.ci";
	
	/** The Constant ERROR_VALIDATOR_NO_EXIST_CHARACTER_HYPHEN_CEDULA_PARAGUAYA. */
	public static final String ERROR_VALIDATOR_NUMBER_CHARACTER_NO_HYPHEN="error.validator.number.character.no.hyphen";
	
	/** The Constant ERROR_VALIDATOR_NUMBER_MAX_LENGTH. */
	public static final String ERROR_VALIDATOR_NUMBER_MAX_LENGTH="error.validator.number.max.length";
	
	/** The Constant ERROR_VALIDATOR_NUMBER_MAX_LENGTH_CEDULA_IDENTIDAD_PARAGUAYA. */
	public static final String ERROR_VALIDATOR_NUMBER_MAX_LENGTH_CI="error.validator.number.max.length.ci";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MAX_LENGTH. */
	public static final String ERROR_VALIDATOR_EXTENSION_MAX_LENGTH="error.validator.extension.max.length";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MIN_LENGTH. */
	public static final String ERROR_VALIDATOR_EXTENSION_MIN_LENGTH="error.validator.extension.min.length";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MAX_LENGTH_RUC. */
	public static final String ERROR_VALIDATOR_EXTENSION_MAX_LENGTH_RUC = "error.validator.extension.max.length.ruc";
	
	/** The Constant ERROR_VALIDATOR_EXTENSION_MIN_LENGTH_RUC. */
	public static final String ERROR_VALIDATOR_EXTENSION_MIN_LENGTH_RUC = "error.validator.extension.min.length.ruc";
	
	/** The Constant ERROR_VALIDATOR_IS_NUMERIC. */
	public static final String ERROR_VALIDATOR_IS_NUMERIC = "error.validator.is.numeric";
			
	/** The Constant ERROR_VALIDATOR_IS_NOT_ALL_ZERO. */
	public static final String ERROR_VALIDATOR_IS_NOT_ALL_ZERO = "error.validator.is.not.all.zero";
	
	/** The Constant ERROR_DOC_EMISOR_VALID. */
	public static final String ERROR_DOC_EMISOR_MNEMONIC_VALID = "error.doc.emisor.mnemonic.valid";
	/*Holder start*/
	
	/** The Constant HOLDER_ERROR_STATE_NOT_REGISTERED. */
	public static final String HOLDER_ERROR_STATE_NOT_REGISTERED="holder.error.state.not.registered";
	
	/** The Constant EMAIL_FORMAT. */
	public static final String EMAIL_FORMAT = "email.format";
	
	/** The Constant LBL_ENVIRONMENT_PRODUCTION. */
	public static final String LBL_ENVIRONMENT_PRODUCTION = "lbl.environment.production";
	
	/** The Constant LBL_ENVIRONMENT_SYSTEMTEST. */
	public static final String LBL_ENVIRONMENT_SYSTEMTEST = "lbl.environment.systemtest";
	
	/** The Constant LBL_ENVIRONMENT_DEVELOPMENT. */
	public static final String LBL_ENVIRONMENT_DEVELOPMENT = "lbl.environment.development";
	
	/** The Constant LBL_ENVIRONMENT_MIGRATION. */
	public static final String LBL_ENVIRONMENT_MIGRATION = "lbl.environment.migration";
	
	public static final String ERROR_CLASS_REPRESENTATIVE_NULL = "error.class.representative.null";
	
	public static final String PGP_ERROR_DECRYPT = "error.class.pgp.decrypt";
	
}
