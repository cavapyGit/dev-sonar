package com.pradera.commons.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class ImageUtilities. Utility class to handle showing images on the UI.
 *
 * @author PraderaTechnologies.
 */
public class ImageUtilities implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Converts byte array object into StreamedContent Object
	 * @param image
	 * @return StreamedContent
	 */
	public static StreamedContent getStreamedImage(byte[] image){
		StreamedContent streamedImagen=null;
		if(image!=null){
			InputStream input = new ByteArrayInputStream(image);
			streamedImagen=new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return streamedImagen;
	}
}
