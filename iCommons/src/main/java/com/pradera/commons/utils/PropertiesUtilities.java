package com.pradera.commons.utils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PropertiesUtilities.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 23/02/2013
 */
public class PropertiesUtilities {
	
		/**
		 * Gets the message from constants.
		 *
		 * @param key the key
		 * @return the message from constants
		 */
		public static String getConstantsMessage(String key) {
			ResourceBundle bundle = ResourceBundle.getBundle(GeneralConstants.PROPERTY_FILE_CONSTANTS);
			String value = bundle.getString(key);
			return value;
		} 

		/**
		 * Gets the message.
		 *
		 * @param locale the locale
		 * @param key the key
		 * @return the message
		 */
		public static String getMessage(Locale locale, String key) {
			return getBundle(locale).getString(key);
		} 
		

		/**
		 * Gets the message.
		 *
		 * @param key the key
		 * @return the message
		 */
		public static String getMessage(String key){
			ResourceBundle bundle = ResourceBundle.getBundle(GeneralConstants.PROPERTY_FILE_MESSAGES, JSFUtilities.getCurrentLocale());
			String value = bundle.getString(key);
			return value;
		}
		
		public static String getGenericMessage(String key){
			return getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, JSFUtilities.getCurrentLocale(), key);
		}
		
		public static String getGenericMessage(String key,Object ... params) {
			return getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, JSFUtilities.getCurrentLocale(), key,params);
		}
		
		public static String getGenericMessage(String key,Object [] params, Locale locale) {
			return getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, locale, key,params);
		}
		
		public static String getExceptionMessage(String key){
			return getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, JSFUtilities.getCurrentLocale(), key);
		}
		
		public static String getExceptionMessage(String key,Object [] params) {
			return getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, JSFUtilities.getCurrentLocale(), key,params);
		}
		
		public static String getExceptionMessage(String key,Object [] params, Locale locale) {
			return getMessage(GeneralConstants.PROPERTY_FILE_EXCEPTIONS, locale, key,params);
		}
		
		public static String getValidationMessage(String key,Object [] params){
			ResourceBundle bundle = ResourceBundle.getBundle(GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, JSFUtilities.getCurrentLocale());
			return MessageFormat.format(bundle.getString(key), params);			
		}
		
		/**
		 * Gets the bundle.
		 *
		 * @param locale the locale
		 * @return the bundle
		 */
		public static ResourceBundle getBundle(Locale locale) {
			return ResourceBundle.getBundle(GeneralConstants.PROPERTY_FILE_MESSAGES,locale);
		}
		
		/**
		 * Gets the bundle.
		 *
		 * @param propertyFile the property file
		 * @param locale the locale
		 * @return the bundle
		 */
		public static ResourceBundle getBundle(String propertyFile,Locale locale) {
			return ResourceBundle.getBundle(propertyFile,locale);
		}
		
		/**
		 * Gets the message from parameter file property.
		 *
		 * @param propertyFile the property file
		 * @param locale the locale
		 * @param key the key
		 * @return the message
		 */
		public static String getMessage(String propertyFile,Locale locale, String key) {
			ResourceBundle bundle = ResourceBundle.getBundle(propertyFile,locale);
			return bundle.getString(key);
		}
		
		/**
		 * Gets the message from parameter file property.
		 *
		 * @param propertyFile the property file
		 * @param locale the locale
		 * @param key the key
		 * @param params the params
		 * @return the message
		 */
		public static String getMessage(String propertyFile,Locale locale, String key, Object... params) {
			return MessageFormat.format(getBundle(propertyFile,locale).getString(key), params);
		}
		

		/**
		 * Gets the message from parameter file property.
		 *
		 * @param propertyFile the property file
		 * @param key the key
		 * @param params the params
		 * @return the message
		 */
		 public static String getMessage(String propertyFile, String key, Object[] params) {
		   return getMessage(propertyFile,JSFUtilities.getCurrentLocale(), key, params);
		  }
		
		/**
		 * Gets the message.
		 *
		 * @param locale the locale
		 * @param key the key
		 * @param params the params
		 * @return the message
		 */
		public static String getMessage(Locale locale, String key, Object... params) {
			return MessageFormat.format(getBundle(locale).getString(key), params);
		}
		
		/**
		 * Gets the message.
		 *
		 * @param key the key
		 * @param params the params
		 * @return the message
		 */
		public static String getMessage(String key, Object... params) {
			Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
			return MessageFormat.format(getBundle(locale).getString(key), params);
		}
		
}
