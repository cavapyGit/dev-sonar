package com.pradera.commons.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.Locale;

import org.beanio.BeanIOConfigurationException;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;


public class BeanIOUtils {
	
	private static StreamFactory factory;
	
	public static StreamFactory getInstance(){
		if(factory==null)
		{	
			factory = StreamFactory.newInstance();
		}
		return factory;
	}
	
	public static BeanReader getReader(String stream, InputStream mappingFile, File xmlFile,Locale locale) 
			throws IllegalArgumentException, BeanIOConfigurationException, IOException{
		getInstance().load(mappingFile);
		BeanReader beanReader = getInstance().createReader(stream, new FileReader(xmlFile),locale);
		return beanReader;
	}
	
	public static BeanReader getReader(String stream, InputStream mappingFile, Reader reader,Locale locale) 
			throws IllegalArgumentException, BeanIOConfigurationException, IOException{
		getInstance().load(mappingFile);
		BeanReader beanReader = getInstance().createReader(stream, reader,locale);
		return beanReader;
	}
	
	public static <T> void writeToFile(List<T> objects,InputStream mappingFile, String stream, File xmlResultFile) 
			throws BeanIOConfigurationException, IOException{
		BeanWriter out = BeanIOUtils.createWriter(mappingFile, stream, xmlResultFile);
		for (T object : objects) {
			out.write(object);
		}
        out.flush();
        out.close();
	}
	
	public static <T> void write(List<T> objects,InputStream mappingFile, String stream, Writer xmlWriter) 
			throws BeanIOConfigurationException, IOException{
		BeanWriter out = BeanIOUtils.createWriter(mappingFile, stream, xmlWriter);
		for (T object : objects) {
			out.write(object);
		}
        out.flush();
        out.close();
	}
	
	public static BeanWriter createWriter(InputStream mappingFile, String stream, File xmlResultFile) 
			throws BeanIOConfigurationException, IOException{
		getInstance().load(mappingFile);
		return getInstance().createWriter(stream, xmlResultFile);
	}
	
	public static BeanWriter createWriter(InputStream mappingFile, String stream, Writer xmlWriter) 
			throws BeanIOConfigurationException, IOException{
		getInstance().load(mappingFile);
		return getInstance().createWriter(stream, xmlWriter);
	}
	
}
