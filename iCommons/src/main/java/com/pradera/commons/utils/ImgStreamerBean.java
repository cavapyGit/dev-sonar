package com.pradera.commons.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.imageio.ImageIO;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ImageStreamerBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */
@Named
@RequestScoped
public class ImgStreamerBean implements Serializable{  

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private StreamedContent fileContent;
	private StreamedContent filePhotoImage;
	private StreamedContent fileSigningImage;
	
	private StreamedContent filePhotoImageModify;
	private StreamedContent fileSigningImageModify;
	
	private StreamedContent fileSignatureImage;
	
	/**
	 * @return fileContent
	 */
	public StreamedContent getFileContent() {
		return fileContent;
	}
	
	
	/**
	 * @param fileContent
	 */
	public void setFileContent(StreamedContent fileContent) {
		this.fileContent = fileContent;
	}


	/**
	 * @return the filePhotoImage
	 */
	public StreamedContent getFilePhotoImage() {
		if(JSFUtilities.getSessionMap("sessStreamedPhoto") != null){
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedPhoto");
			InputStream input = new ByteArrayInputStream(systemImage);
			filePhotoImage = new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return filePhotoImage;
	}


	/**
	 * @param filePhotoImage the filePhotoImage to set
	 */
	public void setFilePhotoImage(StreamedContent filePhotoImage) {
		this.filePhotoImage = filePhotoImage;
	}


	/**
	 * @return the fileSigningImage
	 */
	public StreamedContent getFileSigningImage() {
		if(JSFUtilities.getSessionMap("sessStreamedSigning") != null){
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigning");
			BufferedImage image = null;
			try {
				image = ImageIO.read(new ByteArrayInputStream(systemImage));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(image == null){
				return null;
			} 
			byte [] systemImageAux = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigning");
			InputStream input = new ByteArrayInputStream(systemImageAux);
			fileSigningImage = new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return fileSigningImage;
	}

	/**
	 * Gets the streamed content file detail.
	 *
	 * @return the streamed content file detail
	 */
	public StreamedContent getStreamedContentFileSigning(){
		StreamedContent streamedContentFile = null;
		try {
			byte [] systemImageVal = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigning");
			BufferedImage image = null;
			try {
				image = ImageIO.read(new ByteArrayInputStream(systemImageVal));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(image != null){
				return null;
			}
			
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigning");
			InputStream input = new ByteArrayInputStream(systemImage);				
			streamedContentFile = new DefaultStreamedContent(input, "application/pdf", "fileSignin.pdf");
			input.close();
		} catch (Exception e) {
			
		}
		return streamedContentFile;
	}

	/**
	 * @param fileSigningImage the fileSigningImage to set
	 */
	public void setFileSigningImage(StreamedContent fileSigningImage) {
		this.fileSigningImage = fileSigningImage;
	}


	public StreamedContent getFilePhotoImageModify() {
		if(JSFUtilities.getSessionMap("sessStreamedPhotoModify") != null){
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedPhotoModify");
			InputStream input = new ByteArrayInputStream(systemImage);
			filePhotoImageModify = new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return filePhotoImageModify;
	}


	public void setFilePhotoImageModify(StreamedContent filePhotoImageModify) {
		this.filePhotoImageModify = filePhotoImageModify;
	}


	public StreamedContent getFileSigningImageModify() {
		if(JSFUtilities.getSessionMap("sessStreamedSigningModify") != null){
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigningModify");
			InputStream input = new ByteArrayInputStream(systemImage);
			fileSigningImageModify = new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return fileSigningImageModify;
	}


	public void setFileSigningImageModify(StreamedContent fileSigningImageModify) {
		this.fileSigningImageModify = fileSigningImageModify;
	}
	
	
	/**
	 * Gets the streamed content file detail.
	 *
	 * @return the streamed content file detail
	 */
	public StreamedContent getStreamedContentFileSigningModify(){
		StreamedContent streamedContentFile = null;
		try {
			byte [] systemImageVal = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigningModify");
			BufferedImage image = null;
			try {
				image = ImageIO.read(new ByteArrayInputStream(systemImageVal));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(image != null){
				return null;
			}
			
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedSigningModify");
			InputStream input = new ByteArrayInputStream(systemImage);				
			streamedContentFile = new DefaultStreamedContent(input, "application/pdf", "fileSignin.pdf");
			input.close();
		} catch (Exception e) {
			
		}
		return streamedContentFile;
	}


	/**
	 * @return the fileSignatureImage
	 */
	public StreamedContent getFileSignatureImage() {		
		if(JSFUtilities.getSessionMap("sessStreamedSignature") != null){
			byte [] systemImage = (byte[]) JSFUtilities.getSessionMap("sessStreamedSignature");
			InputStream input = new ByteArrayInputStream(systemImage);
			fileSignatureImage = new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return fileSignatureImage;		
	}


	/**
	 * @param fileSignatureImage the fileSignatureImage to set
	 */
	public void setFileSignatureImage(StreamedContent fileSignatureImage) {
		this.fileSignatureImage = fileSignatureImage;
	}
	
}
