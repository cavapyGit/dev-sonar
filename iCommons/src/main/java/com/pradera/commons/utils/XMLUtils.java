package com.pradera.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.pradera.commons.massive.utils.ValidationXMLErrorHandler;

public class XMLUtils {

	public static boolean isValidXMLFile(File xmlFile) throws ParserConfigurationException, IOException, SAXException {
		boolean valid = false;

		Document dom = getDOMFromXMLFile(xmlFile, false, false);
		if(dom!=null){
			valid = true;
		}
		return valid;
	}
	
	public static <T> Document getDOMFromXMLFile (T xmlFile, boolean validate, boolean namespaceAware) 
			throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(validate);
		factory.setNamespaceAware(namespaceAware);
		Document document = null;
		DocumentBuilder builder = factory.newDocumentBuilder();
		//builder.setErrorHandler(new ValidationXMLErrorHandler());
		if(xmlFile instanceof File){
			document = builder.parse((File)xmlFile);
		}
		if(xmlFile instanceof InputStream){
			document = builder.parse((InputStream)xmlFile);
		}
		return document;
	}
	
	public static byte[] getBytesFromFile(File file) throws IOException {        
        // Get the size of the file
        Long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
            throw new IOException("File is too large!");
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[length.intValue()];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;

        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                   && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
        return bytes;
    }
}