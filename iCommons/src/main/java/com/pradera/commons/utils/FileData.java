package com.pradera.commons.utils;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class FileData.
 */
public class FileData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The report data. */
	private byte[] reportData;
	
	/** The report name. */
	private String reportName;
	
	/** The file path. */
	private String filePath;
	
	/**
	 * Gets the report data.
	 *
	 * @return the reportData
	 */
	public byte[] getReportData() {
		return reportData;
	}
	
	/**
	 * Sets the report data.
	 *
	 * @param reportData the reportData to set
	 */
	public void setReportData(byte[] reportData) {
		this.reportData = reportData;
	}
	
	/**
	 * Gets the report name.
	 *
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}
	
	/**
	 * Sets the report name.
	 *
	 * @param reportName the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * Gets the file path.
	 *
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Sets the file path.
	 *
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}		
	
}
