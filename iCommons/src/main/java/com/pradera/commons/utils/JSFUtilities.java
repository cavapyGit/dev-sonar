package com.pradera.commons.utils;

import java.util.Locale;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIParameter;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.context.RequestContext;

import com.pradera.integration.common.validation.Validations;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class JSFUtilities.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26/11/2012
 */
public class JSFUtilities{	
	
	/**
	 * Put request map.
	 *
	 * @param name the name
	 * @param obj the obj
	 */
	public static void putRequestMap(String name,Object obj){
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(name, obj);
	}
	
	/**
	 * Gets the request map.
	 *
	 * @param name the name
	 * @return the request map
	 */
	public static Object getRequestMap(String name){
		return FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(name);
	}
	
	/**
	 * Removes the request map.
	 *
	 * @param name the name
	 */
	public static void removeRequestMap(String name){
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().remove(name);
	}
	
	/**
	 * Put view map.
	 *
	 * @param name the name
	 * @param obj the obj
	 */
	public static void putViewMap(String name, Object obj){
		FacesContext.getCurrentInstance().getViewRoot().getViewMap().put(name, obj);
	}
	
	/**
	 * Gets the view map.
	 *
	 * @param name the name
	 * @return the view map
	 */
	public static Object getViewMap(String name){
		return FacesContext.getCurrentInstance().getViewRoot().getViewMap().get(name);
	}	
	
	/**
	 * Removes the view map.
	 *
	 * @param name the name
	 */
	public static void removeViewMap(String name){
		FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(name);
	}	
	
	/**
	 * Put session map.
	 *
	 * @param name the name
	 * @param obj the obj
	 */
	public static void putSessionMap(String name,Object obj){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(name, obj);
//		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//		HttpSession request.getSession();
	}	
	
	/**
	 * Gets the session map.
	 *
	 * @param name the name
	 * @return the session map
	 */
	public static Object getSessionMap(String name){
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(name);
	}	
	
	/**
	 * Removes the session map.
	 *
	 * @param name the name
	 */
	public static void removeSessionMap(String name){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(name);
	}
	
	/**
	 * Find view component.
	 *
	 * @param key the key
	 * @return the object
	 */
	public static Object findViewComponent(String key){
		return (Object)FacesContext.getCurrentInstance().getViewRoot().findComponent(key);
	}
	
	/**
	 * Gets the request parameter map.
	 *
	 * @param key the key
	 * @return the request parameter map
	 */
	public static String getRequestParameterMap(String key){
		return (String)FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
	}
	
	
	/**
	 * Show end transaction dialog.
	 */
	public static void showEndTransactionDialog(){
		showComponent("cnfEndTransaction");
	}
	
	/**
	 * Show end transaction same page dialog.
	 */
	public static void showEndTransactionSamePageDialog(){
		showComponent("cnfEndTransactionSamePage");
	}
	
	/**
	 * Show validation dialog.
	 */
	public static void showValidationDialog(){
		showComponent("cnfEndTransactionSamePage");
	}
	
	/**
	 * Show simple validation dialog.
	 */
	public static void showSimpleValidationDialog(){
		updateComponent("cnfMsgCustomValidationSec");
		executeJavascriptFunction("PF('cnfwMsgCustomValidationSec').show()");
	}
	
	public static void showSimpleValidationAltDialog(){
		updateComponent("cnfMsgCustomValidationAlt");
		executeJavascriptFunction("PF('cnfwMsgCustomValidationAlt').show()");
	}
	
	public static void showRequiredValidationDialog(){
		updateComponent("cnfMsgRequiredValidationSec");
		executeJavascriptFunction("PF('cnfwMsgRequiredValidationSec').show()");
	}
	
	/**
	 * Show simple validation dialog.
	 */
	public static void showSimpleEndTransactionDialog(String navigationRule){
		CommandButton button=(CommandButton)findViewComponent("frmEndTransactionSec:cmbConfirmTransactionOk");
		button.setActionExpression( createMethosExpression(navigationRule));
		updateComponent("cnfEndTransactionSec");
		executeJavascriptFunction("PF('cnfwEndTransactionSec').show()");
	}
	
	/**
	 * Show required dialog. cnfwMsgRequiredValidation
	 */
	public static void showRequiredDialog(){
		executeJavascriptFunction("PF('cnfwMsgRequiredValidationSec').show()");
		
		//showComponent("cnfMsgRequiredValidation");
	}
	
	/**
	 * Show search required dialog.
	 */
	public static void showSearchRequiredDialog(){
		showComponent("cnfMsgSearchValidation");
	}
	
	/**
	 * Show component.
	 *
	 * @param idComponente the id componente
	 */
	public static void showComponent(String idComponente){
		UIComponent componente = FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponente);
		if(componente != null){
			componente.setRendered(Boolean.TRUE);
		}			
	}
	
	/**
	 * Hide component.
	 *
	 * @param idComponente the id componente
	 */
	public static void hideComponent(String idComponente){
		UIComponent componente = FacesContext.getCurrentInstance().getViewRoot().findComponent(idComponente);
		
		if(componente != null){
			componente.setRendered(Boolean.FALSE);
		}	
	}
	
	/**
	 * Hide general dialogues.
	 */
	public static void hideGeneralDialogues(){
		hideComponent("cnfEndTransaction");
		hideComponent("cnfEndTransactionSamePage");
		hideComponent("cnfMsgCustomValidation");
		hideComponent("cnfMsgRequiredValidation");
		hideComponent("cnfCloseSession");
		hideComponent("cnfCloseSystem");
		hideComponent("idCnfSamePage");
//		hideComponent("cnfGeneralMsgBusinessValidation");
		hideComponent("cnfBackAdvanced");
		hideComponent("cnfMsgSearchValidation");
		hideComponent("idcnfclosecurrentpage");
	}
	
	/**
	 * Adds the context message.
	 * This method add faces message in the context.
	 *
	 * @param controlId the control id, can be null for global messages
	 * @param severity the severity
	 * @param summary the summary
	 * @param detail the detail
	 */
	public static void addContextMessage(String controlId,Severity severity,String summary,String detail){
		  FacesMessage fMessage=new FacesMessage(severity, summary, detail);
		  FacesContext.getCurrentInstance().addMessage(controlId, fMessage);
		  if(controlId!=null && !controlId.isEmpty()){
		   setInvalidViewComponent(controlId);
		  }
     }
		 
     /**
      * Adds the context message.
      * This method add faces message in the context.
      *
      * @param controlId the control id, can be null for global messages
      * @param summary the summary
      * @param detail the detail
      */
     public static void addContextMessage(String controlId, String summary,String detail){
		  FacesMessage fMessage=new FacesMessage(summary, detail);
		  FacesContext.getCurrentInstance().addMessage(controlId,fMessage);
		  if(controlId!=null && !controlId.isEmpty()){
		   setInvalidViewComponent(controlId);
		  }
     }
		 
	 /**
 	 * Adds the context message.
 	 * This method add faces message in the context.
 	 *
 	 * @param controlId the control id, can be null for global messages
 	 * @param summary the summary
 	 */
 	public static void addContextMessage(String controlId, String summary){
		  FacesMessage fMessage=new FacesMessage(summary);
		  FacesContext.getCurrentInstance().addMessage(controlId,fMessage);
		  if(controlId!=null && !controlId.isEmpty()){
		   setInvalidViewComponent(controlId);
		  }
	 }

     /**
      * Sets the invalid view component.
      * This method set a component as invalid.
      * Decorate with a red border to the component.
      *
      * @param key the new invalid view component
      */
     public static void setInvalidViewComponent(String key){
		  Object component=findViewComponent(key);
		  if(component instanceof UIInput){
		   UIInput input=(UIInput)component;
		   input.setValid(false);
		  }
	 }
     
     /**
      * Sets the valid view component.
      * This method set a component as valid.
      * Remove a red border to the component.
      *
      * @param key the new invalid view component
      */
     public static void setValidViewComponent(String key){
    	 Object component=findViewComponent(key);
		  if(component instanceof UIInput){
			   UIInput input=(UIInput)component;
			   input.setValid(true);
		   }
     }
     
     /**
      * Gets the remote ip address.
      *
      * @return the remote ip address
      */
     public static String getRemoteIpAddress(){
    	 HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	 String strIp = request.getHeader("ClientIP");
	 		if (strIp == null) {
	 			strIp = request.getHeader("x-forwarded-for");
	 			if (strIp == null) {
	 				strIp = request.getRemoteAddr();
	 			}
	 		}
	 	return strIp;
     }
     
     /**
      * Creates the methos expression.
      *
      * @param methodName the method name
      * @return the method expression
      */
     public static MethodExpression createMethosExpression(String methodName){
    	 FacesContext context = FacesContext.getCurrentInstance();
		 return context.getApplication().getExpressionFactory()
			    .createMethodExpression(context.getELContext(), methodName, null, new Class[] {});			
     }
     
     
     /**
      * Creates the methos expression action listener.
      *
      * @param methodName the method name
      * @return the method expression action listener
      */
     public static MethodExpressionActionListener createMethosExpressionActionListener(String methodName){
    	 FacesContext context = FacesContext.getCurrentInstance();
    	 MethodExpression actionListener = context.getApplication().getExpressionFactory()
		    .createMethodExpression(context.getELContext(), methodName, null, new Class[] {ActionEvent.class});
    	 return new MethodExpressionActionListener(actionListener);
     }
     
     /**
      * Creates the value expression.
      *
      * @param strValueExpression the str value expression
      * @param className the class name
      * @return the value expression
      */
     public static ValueExpression createValueExpression(String strValueExpression, Class className){
    	 Application app = FacesContext.getCurrentInstance().getApplication();
		 ExpressionFactory exprFactory = app.getExpressionFactory();
		 ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		 return exprFactory.createValueExpression(elContext, strValueExpression, className);
     }
     
     public static Object getValueExpression(String strValueExpression,Class className){
		 ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		 return (Object)createValueExpression(strValueExpression,className).getValue(elContext);
     }
     
     /**
      * Creates the ui parameter.
      *
      * @param paramName the param name
      * @param valueExpression the value expression
      * @return the uI parameter
      */
     public static UIParameter createUIParameter(String paramName, ValueExpression valueExpression){
    	 UIParameter uiParameter = new UIParameter();
    	 uiParameter.setName(paramName);
    	 uiParameter.setValueExpression("value", valueExpression);
		 return uiParameter;
     }
     
     /**
      * Creates the ui parameter.
      *
      * @param paramName the param name
      * @param value the value
      * @return the uI parameter
      */
     public static UIParameter createUIParameter(String paramName, String value){
    	 UIParameter uiParameter = new UIParameter();
    	 uiParameter.setName(paramName);
    	 uiParameter.setValue(value);
		 return uiParameter;
     }
     
     /**
      * Execute javascript function.
      *
      * @param function the function
      */
     public static void executeJavascriptFunction(String function){
    	 RequestContext requestContext = RequestContext.getCurrentInstance();  
 		 requestContext.execute(function.toString());
     }
     
     /**
      * Kill session.
      */
     public static void killSession(){
    	 HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		 if(httpSession != null)
			 httpSession.invalidate();
     }
     
     /**
      * Gets the current locale.
      *
      * @return the current locale
      */
     public static Locale getCurrentLocale(){
    	 return FacesContext.getCurrentInstance().getViewRoot().getLocale();
     }
	
 	/**
	  * Show message on dialog.
	  *
	  * @param keyHeaderProperty the key header property
	  * @param headerData the header data
	  * @param keyBodyProperty the key body property
	  * @param bodyData the body data
	  */
	 public static void showMessageOnDialog(String keyHeaderProperty,Object[] headerData,String keyBodyProperty, Object[] bodyData){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty,headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty,bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	 
	 public static void showMessageOnDialog(String propertyFileHeader, String keyHeaderProperty,Object[] headerData,
			 								String propertyFileBody, String keyBodyProperty, Object[] bodyData){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(propertyFileHeader,locale, keyHeaderProperty,headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(propertyFileBody,locale, keyBodyProperty,bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	 
	 
	 
	 /**
 	 * Sets the valid view component and childrens.
 	 *
 	 * @param componentId the new valid view component and childrens
 	 */
 	public static void setValidViewComponentAndChildrens(String componentId){
			UIComponent uiComponent = (UIComponent)JSFUtilities.findViewComponent(componentId);
			if(Validations.validateIsNotNull(uiComponent)){
				for (UIComponent uiComponentChild : uiComponent.getChildren()) {
					setValidViewComponentRecursive(uiComponentChild);			
				}
			}			
	}
		
	 /**
 	 * Sets the valid view component recursive.
 	 *
 	 * @param uiComponent the new valid view component recursive
 	 */
 	public static void setValidViewComponentRecursive(UIComponent uiComponent){
			setValidViewComponent(uiComponent);
			if(uiComponent.getChildCount()>0){
				for (UIComponent uiComponentChild : uiComponent.getChildren()) {
					setValidViewComponentRecursive(uiComponentChild);
				}
			}
	}
		
	 /**
 	 * Sets the valid view component.
 	 *
 	 * @param uiComponent the new valid view component
 	 */
 	public static void setValidViewComponent(UIComponent uiComponent){
			  if(uiComponent instanceof UIInput){
				   UIInput input=(UIInput)uiComponent;
				   input.setValid(true);
			   }
	}
 	
 	/**
	  * Gets the current view id.
	  *
	  * @return the current view id
	  */
	 public static String getCurrentViewId(){
 		return FacesContext.getCurrentInstance().getViewRoot().getViewId();
 	}
	 
	 /**
	 * Put http session attribute.
	 *
	 * @param name the name
	 * @param obj the obj
	 */
	public static void setHttpSessionAttribute(String name, Object obj){		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().setAttribute(name, obj);
	}	
	
	/**
	 * Remove http session attribute.
	 *
	 * @param name the name
	 * @param obj the obj
	 */
	public static void removeHttpSessionAttribute(String name){		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().removeAttribute(name);
	}
	
	 /**
	 * Reset view root.
	 * Clean all viesRoot
	 */
	public static void resetViewRoot(){
        FacesContext context = FacesContext.getCurrentInstance(); 
        Application application = context.getApplication(); 
        ViewHandler viewHandler = application.getViewHandler(); 
        UIViewRoot viewRoot = viewHandler.createView(context, 
        			context.getViewRoot().getViewId()); 
        context.setViewRoot(viewRoot); 
        context.renderResponse();
	 }
	 
	 /**
	 * Reset component.
	 *
	 * @param componentId the component id
	 * Reset component, clean border and invalid message
	 */
	public static void resetComponent(String componentId){
		try {
			RequestContext.getCurrentInstance().reset(componentId); 
		} catch (FacesException e) {
			e.getMessage();
		}
	 }
	
	/**
	 * Gets the xml ajax redirect.
	 *
	 * @param fullUrl the full url
	 * @return the xml ajax redirect
	 */
	public static String getXmlAjaxRedirect(String fullUrl) {
		StringBuilder sb = new StringBuilder();
        sb.append("<?xml version='1.0' encoding='UTF-8'?>");
        sb.append("<partial-response><redirect url=\"").append(fullUrl).append("\"/></partial-response>");
        return sb.toString();
	}
	
	/*
	 * Update component
	 *This method is used to do update some control from managebean, for the puntual cases
	 * */
	public static void updateComponent(String componentId){
		RequestContext.getCurrentInstance().update(componentId);		
	}
	
	/*
	 * Scroll to component
	 *This method is used to do scroll to some control from managebean.
	 * */
	public static void scrollToComponent(String componentId){
		RequestContext.getCurrentInstance().scrollTo(componentId);
	}
	
	public static void showMessageOnDialog(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
}