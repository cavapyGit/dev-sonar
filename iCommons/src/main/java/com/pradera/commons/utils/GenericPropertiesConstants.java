/*
 * 
 */
package com.pradera.commons.utils;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
public class GenericPropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	private GenericPropertiesConstants() {
		super();
	}
	
	public static final String EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE="external.interface.error.downloading.file";
	
	public static final String EXTERNAL_INTERFACE_FILE_IS_NULL = "external.interface.file.notExists";
	
	public static final String EXTERNAL_INTERFACE_UPLOAD_CONFIRM = "external.interface.upload.confirm";
	
	public static final String DEPOSIT_RECEPTION_LIP_CONFIRM = "deposit.reception.lip.funds.confirm";
	
	public static final String DEPOSIT_RECEPTION_LIP_BS_CONFIRM = "deposit.reception.lip.funds.bs.confirm";
	
	public static final String DEPOSIT_RECEPTION_LIP_SUS_CONFIRM = "deposit.reception.lip.funds.sus.confirm";
	
	public static final String EXTERNAL_INTERFACE_UPLOAD_SUCCESS = "external.interface.upload.success";
	
	public static final String DEPOSIT_RECPTION_LIP_SUCCESS = "deposit.reception.lip.funds.success";
	
	public static final String MSG_INTERFACE_TRANSACTION_SUCCESS_CODE = "0000";
		
	public static final String MSG_INTERFACE_TRANSACTION_SUCCESS = "msg.interface.transaction.success";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION = "msg.interface.invalid.operation";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_CLASS= "msg.interface.invalid.security.class";
	
	public static final String MSG_INTERFACE_INVALID_SIGN= "msg.interface.invalid.sign";
	
	public static final String MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE= "msg.interface.invalid.passive.interest.rate";
	
	public static final String MSG_INTERFACE_INVALID_INTEREST_PAYMENT_MODALITY= "msg.interface.invalid.interest.payment.modality";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_ALT_CODE= "msg.interface.invalid.security.alternative.code";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_PLACE= "msg.interface.invalid.issuance.place";
	
	public static final String MSG_INTERFACE_INVALID_ENTITY_CODE= "msg.interface.invalid.entity.code";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION_DATE= "msg.interface.invalid.operation.date";
	
	public static final String MSG_INTERFACE_INVALID_ISSUANCE_DATE_MUST_BE_LESS_OPERATION_DATE= "msg.interface.invalid.issuance.date.must.be.less.operation.date";

	public static final String MSG_INTERFACE_INVALID_ACCOUNT_TYPE= "msg.interface.invalid.account.type";
	
	public static final String MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE= "msg.interface.invalid.blocked.securitycode";
	
	public static final String MSG_INTERFACE_INVALID_DOCUMENT_TYPE= "msg.interface.invalid.document.type";
	
	public static final String MSG_VALIDATION_EXCEEDED_HOURALLOWED="msg.validation.exceeded.hourAllowed";
	
	public static final String MSG_DAYS_EXCEEDED_AFTER_DATE_ISSUANCE= "msg.days.exceeded.after.date.issuance";
	
	public static final String MSG_INTERFACE_INVALID_DOCUMENT_SOURCE= "msg.interface.invalid.document.source";
	
	public static final String MSG_INTERFACE_INVALID_SECOND_DOCUMENT_TYPE= "msg.interface.invalid.second.document.type";
	
	public static final String MSG_INTERFACE_INVALID_DOCUMENT_NUMBER= "msg.interface.invalid.document.number";
	
	public static final String MSG_INTERFACE_INVALID_SECOND_DOCUMENT_NUMBER= "msg.interface.invalid.second.document.number";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_CITY= "msg.interface.invalid.residence.city";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_PROVINCE= "msg.interface.invalid.residence.province";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_DISTRICT= "msg.interface.invalid.residence.district";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_INDICATOR= "msg.interface.invalid.residence.indicator";
	
	public static final String MSG_INTERFACE_INVALID_NATIONALITY= "msg.interface.invalid.nationality";
	
	public static final String MSG_INTERFACE_INVALID_SECOND_NATIONALITY= "msg.interface.invalid.second.nationality";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_COUNTRY= "msg.interface.invalid.residence.country";
	
	public static final String MSG_INTERFACE_INVALID_ECONOMIC_SECTOR= "msg.interface.invalid.economic.sector";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_CITY_JURIDIC= "msg.interface.invalid.residence.city.juridic";
	
	public static final String MSG_INTERFACE_INVALID_BIRTHDAY= "msg.interface.invalid.birthday";
	
	public static final String MSG_INTERFACE_INVALID_SEX= "msg.interface.invalid.sex";
	
	public static final String MSG_INTERFACE_INVALID_NATIONALITY_JURIDIC= "msg.interface.invalid.nationality.juridic";
	
	public static final String MSG_INTERFACE_INVALID_RESIDENCE_COUNTRY_JURIDIC= "msg.interface.invalid.residence.country.juridic";
	
	public static final String MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE= "msg.interface.invalid.instrument.type";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_SERIAL= "msg.interface.invalid.security.serial";
	
	public static final String MSG_INTERFACE_INVALID_ISSUANCE_DATE= "msg.interface.invalid.issuance.date";
	
	public static final String MSG_INTERFACE_INVALID_EXPIRATION_DAYS= "msg.interface.invalid.expiration.days";
	
	public static final String MSG_INTERFACE_INVALID_EXPIRATION_DATE= "msg.interface.invalid.expiration.date";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_CURRENCY= "msg.interface.invalid.security.currency";
	
	public static final String MSG_INTERFACE_INVALID_INTEREST_PAYMENT_PERIODICITY= "msg.interface.invalid.interest.payment.periodicity";
	
	public static final String MSG_INTERFACE_INVALID_NOMINAL_VALUE= "msg.interface.invalid.nominal.value";
	
	public static final String MSG_INTERFACE_INVALID_ISSUER_CODE= "msg.interface.invalid.issuer.code";
	
	public static final String MSG_INTERFACE_INVALID_INTEREST_RATE= "msg.interface.invalid.interest.rate";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_NUMBER= "msg.interface.invalid.coupon.number";
	
	public static final String MSG_INTERFACE_INVALID_seccOperation= "msg.interface.invalid.section.seccOperation";
	
	public static final String MSG_INTERFACE_INVALID_seccValor= "msg.interface.invalid.section.seccValor";
	
	public static final String MSG_INTERFACE_INVALID_seccTitularNatural= "msg.interface.invalid.section.seccTitularNatural";
	
	public static final String MSG_INTERFACE_INVALID_seccTitularJuridico= "msg.interface.invalid.section.seccTitularJuridico";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_ELEMENT= "msg.interface.invalid.section.coupon";
	
	public static final String MSG_INTERFACE_OPERATION_DUPLICATE_TAG_SERIAL= "msg.interface.operation.duplicate.tag.serial";
	
	public static final String MSG_INTERFACE_SECURITY_CODE_REPORT_OPERATION_EXISTS= "msg.interface.operation.report.exists";
	
	public static final String MSG_INTERFACE_UNDEFINED_ERROR= "msg.interface.undefined.error";
	
	public static final String MSG_INTERFACE_INVALID_XML_STRUCTURE= "msg.interface.invalid.xml.structure";
	
	public static final String MSG_INTERFACE_ENTITY_INVALID_SIGN= "msg.interface.entity.invalid.sign";
	
	public static final String MSG_INTERFACE_ERROR_ON_SIGN= "msg.interface.error.on.sign";
	
	public static final String MSG_INTERFACE_ENTITY_PUBLIC_KEY_NOT_EXISTS= "msg.interface.public.key.not.exists";
	
	public static final String MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION= "msg.interface.duplicate.operation.tag.operation";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION_CODE_TAG_OPERATION= "msg.interface.invalid.operation.code.tag.operation";
	
	public static final String MSG_INTERFACE_SOURCE_CUI_NOT_REGISTERED= "msg.interface.cui.source.not.registered";
	
	public static final String MSG_INTERFACE_TARGET_CUI_NOT_REGISTERED= "msg.interface.cui.target.not.registered";
	
	public static final String MSG_INTERFACE_COUPON_NUMBER_NOT_EXISTS= "msg.interface.coupon.number.not.exists";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE = "msg.interface.invalid.coupon.expiration.date";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_ALREADY_PAYED = "msg.interface.invalid.coupon.already.payed";
	
	public static final String MSG_INTERFACE_INVALID_REVERSE_PAYMENT_DATE = "msg.interface.invalid.reverse.payment.date";
	
	public static final String MSG_INTERFACE_INTEREST_ALREADY_REVERSED = "msg.interface.interest.already.reversed";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE= "msg.interface.invalid.security.alternative.code";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_ACCOUNT = "msg.interface.invalid.holderaccount";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE = "msg.interface.invalid.holderaccountbalance";
	
	public static final String MSG_INTERFACE_INVALID_BALANCE_AVAILABLE = "msg.interface.invalid.balance.available";
	
	public static final String MSG_INTERFACE_INVALID_MULTIPLE_MARKETFACTS = "msg.interface.invalid.multiple.marketfacts";
	
	public static final String MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES = "msg.interface.invalid.insufficient.balances";
	
	public static final String MSG_INTERFACE_INVALID_NO_MKTFACT_BALANCES = "msg.interface.invalid.no.marketfact.balances";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_EXCEED_BALANCES = "msg.interface.invalid.security.exceed.balances";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT = "msg.interface.invalid.operation.retirement.recurrent";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_SOURCE= "msg.interface.invalid.security.issuance.source";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_INTEREST_TYPE = "msg.interface.invalid.security.interest.type";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY = "msg.interface.invalid.security.interest.payment.modality";
	
	public static final String MSG_INTERFACE_INVALID_BLOCKED_OPERATIONS = "msg.interface.invalid.blocked.operations";
	
	public static final String MSG_INTERFACE_ALERT_HOLDER_CANNOT_NEGOTIATE = "msg.interface.alert.holder.cannot.negotiate";	
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_MOVEMENTS_EXISTS = "msg.interface.invalid.security.movements.exists";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION_RENEW_RECURRENT = "msg.interface.invalid.operation.renew.recurrent";
	
	public static final String MSG_INTERFACE_INVALID_QUERY_SERVICE_STATUS_WEBSERVICES = "msg.interface.invalid.query.service.status.webservices";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_CODE_REPEATED = "msg.interface.invalid.security.code.repeated";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS = "msg.interface.invalid.security.code.incorrect.instrument.class";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_CURRENCY_SOURCE= "msg.interface.invalid.security.currency.source";
		
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_ISSUANCE_DATE_NOT_EQUALS= "msg.interface.invalid.security.fractionable.issuance.date.notequals";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_EXPIRATION_DATE_NOT_EQUALS= "msg.interface.invalid.security.fractionable.expitration.date.notequals";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_INTERES_RATE_NOT_EQUALS= "msg.interface.invalid.security.fractionable.interes.rate.notequals";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_CURRENCY_NOT_EQUALS= "msg.interface.invalid.security.fractionable.currency.notequals";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_STATE_NOT_EQUALS= "msg.interface.invalid.security.fractionable.state.notvalid";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_NOT_EXIST= "msg.interface.invalid.security.fractionable.notexist";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_TOTAL_NOMINAL_VALUE= "msg.interface.invalid.security.fractionable.error.total.nominal.value";	                                                                                             
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_INVALID_DATE_RA= "msg.interface.invalid.security.fractionable.invalid.date";	
	
	public static final String MSG_INTERFACE_INVALID_REVERSE_SECURITY_FATHER_FRACTION= "msg.interface.invalid.reverse.security.father.fraction";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_FRACTIONS= "msg.interface.invalid.holder.fraction";	
	
	//amortization
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_MODALITY= "msg.interface.invalid.security.amortization.modality";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_TYPE= "msg.interface.invalid.security.amortization.type";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PERIODICITY= "msg.interface.invalid.security.amortization.periodicity";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PAYMENT_FACTOR= "msg.interface.invalid.security.amortization.payment.factor";
	
	public static final String MSG_INTERFACE_INVALID_SECC_PAGOS= "msg.interface.invalid.section.seccPagos";
	
	public static final String MSG_INTERFACE_INVALID_REVERSE_OPERATION_RENEWAL_DATE = "msg.interface.invalid.reverse.operation.renewal.date";
	
	public static final String MSG_INTERFACE_INVALID_REVERSE_OPERATION_RETIREMENT_DATE = "msg.interface.invalid.reverse.operation.retirement.date";
	
	public static final String MSG_INTERFACE_INVALID_REVERSE_OPERATION_ACCOUNTENTRY_DATE = "msg.interface.invalid.reverse.operation.accountentry.date";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION_RENEWAL_RECURRENT = "msg.interface.invalid.operation.renewal.recurrent";
	
	public static final String MSG_INTERFACE_INVALID_OPERATION_ACCOUNTRY_RECURRENT = "msg.interface.invalid.operation.accountry.recurrent";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_ACCOUNTNUMBER = "msg.interface.invalid.holderaccount.accountnumber";
	
	public static final String MSG_INTERFACE_INVALID_OBLIGATION_NUMBER_ORIGIN = "msg.interface.invalid.obligation.number.origin";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_NECCESARY_LEGAL_REPRESENTATIVE= "msg.interface.invalid.holder.necessary.legal.representative";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_NOT_NECCESARY_LEGAL_REPRESENTATIVE= "msg.interface.invalid.holder.not.necessary.legal.representative";
	
	public static final String MSG_INTERFACE_REPRESENTATIVE_MINOR_ERROR= "msg.interface.representative.minor.error";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_ISSUANCE_DATE = "msg.interface.invalid.coupon.expiration.date.issuance.date";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_EXPIRATION_DATE = "msg.interface.invalid.coupon.expiration.date.expiration.date";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_HOLDERS = "msg.interface.invalid.holderaccount.size.holders";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_HOLDERS_NOTEXITS = "msg.interface.invalid.holderaccount.holders.notexits";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_REVERSE_SIZE_ONE = "msg.interface.invalid.coupon.reverse.size.one";
	
	public static final String MSG_INTERFACE_INVALID_COUPON_NUMBER_NOT_IS_LAST = "msg.interface.invalid.coupon.number.not.is.last";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_JURIDIC_CLASS= "msg.interface.invalid.holder.juridic.class";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_SOURCE = "msg.interface.invalid.holder.document.source";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_DOCUMENT_DUPLICATE_CODE = "msg.interface.invalid.holder.document.duplicate.code";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_OWNERSHIP = "msg.interface.invalid.holderaccount.size.ownership";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_INDEXED_CURRENCY = "msg.interface.invalid.security.indexed.currency";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE_PARTICIPANT = "msg.interface.invalid.holderaccountbalance.participant";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_RENEW_EXITS = "msg.interface.invalid.security.renew.exits";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_RENEW_ISINCODE_EXITS = "msg.interface.invalid.security.renew.isincode.exits";
	
	public static final String MSG_INTERFACE_INVALID_PARTICIPANT_BALANCE_INSERT = "msg.interface.invalid.participant.balance.insert";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_DIDEICOMISO_NOTEXITS = "msg.interface.invalid.holder.fideicomiso.notexits";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_DIDEICOMISO_ISNECESARIO_CUI = "msg.interface.invalid.holder.fideicomiso.isnecesario.cui";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_STATUS_NOT_REGISTERED = "msg.interface.invalid.holder.status.not.registered";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_STATUS_NOT_REGISTERED = "msg.interface.invalid.holderaccount.status.not.registered";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_NOTEXITS_ACCOUNT_NUMBER = "msg.interface.invalid.holderaccount.notexits.account.number";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_EXITS_ACCOUNT_NUMBER = "msg.interface.invalid.holderaccount.exits.account.number";
	
	public static final String MSG_INTERFACE_INVALID_EXITS_AVAILABLE_BALANCE_SECURITY_EXPIRED = "msg.interface.invalid.exits.available.balance.security.expired";
	
	public static final String MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY = "msg.interface.invalid.stock.balances.early";
	
	public static final String MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_TOTAL = "msg.interface.invalid.stock.balances.early.total";
	
	public static final String MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_TOTAL_ZERO = "msg.interface.invalid.stock.balances.early.zero";
	
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT_SIZE_NATURAL = "msg.interface.invalid.holderaccount.size.natural";	
	
	public static final String MSG_INTERFACE_INVALID_SETTLEMENT_TRA = "msg.interface.invalid.settlement.tra";
	
	public static final String MSG_INTERFACE_INVALID_EXPIRATION_DATE_SOURCE= "msg.interface.invalid.expiration.date.source";
	
	public static final String MSG_INTERFACE_INVALID_EXPIRATION_DATE_RETIREMENT = "msg.interface.invalid.expiration.date.retirement";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_SERIAL_PREVIUOS= "msg.interface.invalid.security.serial.previuos";
	
	public static final String MSG_INTERFACE_INVALID_SECURITY_NOTTRADED = "msg.interface.invalid.security.nottraded";
	
	public static final String MSG_INTERFACE_SECURITY_CODE_ACCREDITATION_OPERATION_EXISTS= "msg.interface.operation.accreditation.exists";
	
	public static final String MSG_INTERFACE_INVALID_ENTITY_STATUS_CODE= "msg.interface.invalid.entity.status.code";
	
	public static final String MSG_INTERFACE_INVALID_INDICATOR_EARLY_PAYMENT = "msg.interface.invalid.indicator.early.payment";

	public static final String MSG_INTERFACE_INVALID_INSUFFICIENT_LEGAL_REPRESANTIVE = "msg.interface.invalid.insufficient.legal.representative";
	
	public static final String MSG_INTERFACE_INVALID_HOLDER_BIRTH_DATE_INCORRECT = "msg.interface.invalid.holder.birth.date.incorrect";
	
	//para el web service de consulta de dpf,s
	public static final String MSG_DISPLAY_LBL_ISSUER_NO_VALIDE="msg.display.lbl.issuer.no.valide";	
	public static final String MSG_DISPLAY_LBL_ISSUER_NO_VALIDE_CODE="msg.display.lbl.issuer.no.valide.code";

	public static final String MSG_DISPLAY_LBL_INSTRUMENT_NO_VALIDE="msg.display.lbl.instrument.no.valide";
	public static final String MSG_DISPLAY_LBL_INSTRUMENT_NO_VALIDE_CODE="msg.display.lbl.instrument.no.valide.code";
	
	public static final String MSG_DISPLAY_LBL_SERIE_NO_VALIDE="msg.display.lbl.serie.no.valide";
	public static final String MSG_DISPLAY_LBL_SERIE_NO_VALIDE_CODE="msg.display.lbl.serie.no.valide.code";
	
	public static final String MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION_DINAMIC="msg.interface.duplicate.operation.tag.operation.dinamic";
	
	//Issue 1239 RCO
	public static final String MSG_INTERFACE_INVALID_ENTITY_CODE_PLACEMENT= "msg.interface.invalid.entity.code.placement";
	public static final String MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE_HM= "msg.interface.invalid.interest.rate.hm";
	public static final String MSG_INTERFACE_INVALID_PASSIVE_INTEREST_PRICE_HM= "msg.interface.invalid.interest.price.hm";
	public static final String MSG_INTERFACE_INVALID_DOCUMENT_SOURCE_SIZE= "msg.interface.invalid.document.source.size";
	public static final String MSG_INTERFACE_INVALID_DOCUMENT_SOURCE_TYPE_FACT= "msg.interface.invalid.document.source.type.fact";
	public static final String MSG_INTERFACE_INVALID_DOCUMENT_SOURCE_MANAGEMENT= "msg.interface.invalid.document.source.management";
	public static final String MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OBLIGATION_NUMBER= "msg.interface.duplicate.operation.tag.obligation.number";
	public static final String MSG_INTERFACE_INVALID_SECURITY_STATE= "msg.interface.invalid.security.state";
	public static final String MSG_INTERFACE_INVALID_INTEREST_DATE_HM= "msg.interface.invalid.interest.date.hm";
	
	//Issue 1239 RRC
	public static final String MSG_INTERFACE_INVALID_ENVIO_DATE= "msg.interface.invalid.envio.date";
	public static final String MSG_INTERFACE_INVALID_OPERATION_DATE_PLACEMENT= "msg.interface.invalid.operation.date.placement";
	public static final String MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION_ORIG= "msg.interface.duplicate.operation.tag.operation.orig";
	public static final String MSG_INTERFACE_INVALID_SECURITY_CODE= "msg.interface.invalid.security.code";
	public static final String MSG_INTERFACE_INVALID_ACCOUNT_NUMBER= "msg.interface.invalid.account.number";
	public static final String MSG_INTERFACE_INVALID_SECURITY_MOVEMENTS_EXISTS_REVERSE = "msg.interface.invalid.security.movements.exists.reverse";
	public static final String MSG_INTERFACE_INVALID_EXPIRATION_DATE_PLACEMENT = "msg.interface.invalid.expiration.date.placement";
	public static final String MSG_INTERFACE_INVALID_COUPON_REVERSE_PLACEMENT = "msg.interface.invalid.coupon.reverse.placement";
	public static final String MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT = "msg.interface.invalid.operation.placement.recurrent";

	// issue 1239
	// RVA
	public static final String MSG_INTERFACE_INVALID_MUST_HAVE_NULL_TAG_NROCUPONES = "msg.interface.invalid.must.have.null.tag.nrocupones";
	public static final String MSG_INTERFACE_INVALID_MUST_HAVE_NULLFECHAVCTOPRIMERUPON = "msg.interface.invalid.must.have.null.fechavctoprimerupon";
	public static final String MSG_INTERFACE_INVALID_MUST_HAVE_NULL_PERIODICIDAD = "msg.interface.invalid.must.have.null.periodicidad";
	public static final String MSG_INTERFACE_INVALID_MUST_HAVE_NULL_TIPOPAGO = "msg.interface.invalid.must.have.null.tipopago";
	public static final String MSG_INTERFACE_INVALID_MUST_HAVE_NULL_PERIODICIDADPAGO = "msg.interface.invalid.must.have.null.periodicidadpago";
	public static final String MSG_INTERFACE_INVALID_SEND_DATE= "msg.interface.invalid.send.date";
	public static final String MSG_INTERFACE_INVALID_ISSUANCE_DATE_SECURITY="msg.interface.invalid.issuance.date.security";
	public static final String MSG_INTERFACE_INVALID_SECURITY_CODE_REPEATED_MASSIVE="msg.interface.invalid.security.code.repeated.massive";
	public static final String MSG_INTERFACE_INVALID_SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS_MASSIVE="msg.interface.invalid.security.code.incorrect.instrument.class.massive";
	public static final String MSG_INTERFACE_INVALID_SECTION_COUPON_MASSIVE="msg.interface.invalid.section.coupon.massive";
	public static final String MSG_INTERFACE_INVALID_COUPON_NUMBER_MASSIVE="msg.interface.invalid.coupon.number.massive";
	public static final String MSG_INTERFACE_INVALID_INTEREST_PAYMENT_PERIODICITY_MASSIVE="msg.interface.invalid.interest.payment.periodicity.massive";
	public static final String MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_ISSUANCE_DATE_MASSIVE="msg.interface.invalid.coupon.expiration.date.issuance.date.massive";
	public static final String MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_EXPIRATION_DATE_MASSIVE="msg.interface.invalid.coupon.expiration.date.expiration.date.massive";
	public static final String MSG_INTERFACE_INVALID_COUPON_EXPIRATION_DATE_MASSIVE="msg.interface.invalid.coupon.expiration.date.massive";
	public static final String MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY_MASSIVE="msg.interface.invalid.security.interest.payment.modality.massive";
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_TYPE_MASSIVE="msg.interface.invalid.security.amortization.type.massive";
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PERIODICITY_MASSIVE="msg.interface.invalid.security.amortization.periodicity.massive";
	public static final String MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PAYMENT_FACTOR_MASSIVE="msg.interface.invalid.security.amortization.payment.factor.massive";
	public static final String MSG_INTERFACE_INVALID_SECTION_SECCPAGOS_MASSIVE="msg.interface.invalid.section.seccPagos.massive";
	
	//RCA
	public static final String MSG_INTERFACE_INVALID_EXPIRATION_DATE_RETIREMENT_MASSIVE="msg.interface.invalid.expiration.date.retirement.massive";
	public static final String MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_ZERO_RCA="msg.interface.invalid.stock.balances.early.zero.rca";
	public static final String MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_TOTAL_RCA="msg.interface.invalid.stock.balances.early.total.rca";
	public static final String MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_RCA="msg.interface.invalid.stock.balances.early.rca";
	
	// RPA
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT_RPA="msg.interface.invalid.operation.retirement.recurrent.rpa";
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_REVERT_RPA="msg.interface.invalid.operation.retirement.revert.rpa";
}