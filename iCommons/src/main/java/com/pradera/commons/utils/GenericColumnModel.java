package com.pradera.commons.utils;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class GenericColumnModel.
 */
public class GenericColumnModel implements Serializable{

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The header. */
    private String header;
    
    /** The property. */
    private String property;
    
    /** The id. */
    private String id;

    /** The header value bol. */
    private Boolean headerValueBol;
    
    /**
     * Instantiates a new generic column model.
     *
     * @param header the header
     * @param property the property
     * @param id the id
     */
    public GenericColumnModel(String header, String property, String id) {
        this.header = header;
        this.property = property;
        this.id=id;
    }

    /**
     * Instantiates a new generic column model.
     *
     * @param header the header
     * @param property the property
     * @param id the id
     * @param headerValueBol the header value bol
     */
    public GenericColumnModel(String header, String property, String id,
			Boolean headerValueBol) {
		super();
		this.header = header;
		this.property = property;
		this.id = id;
		this.headerValueBol = headerValueBol;
	}

	/**
     * Gets the header.
     *
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * Gets the property.
     *
     * @return the property
     */
    public String getProperty() {
        return property;
    }

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the header value bol.
	 *
	 * @return the header value bol
	 */
	public Boolean getHeaderValueBol() {
		return headerValueBol;
	}

	/**
	 * Sets the header value bol.
	 *
	 * @param headerValueBol the new header value bol
	 */
	public void setHeaderValueBol(Boolean headerValueBol) {
		this.headerValueBol = headerValueBol;
	}
	
    
    
}
