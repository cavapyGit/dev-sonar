package com.pradera.commons.utils;

import java.io.Serializable;

import com.pradera.integration.common.validation.Validations;

// TODO: Auto-generated Javadoc
/*
 * 
 */
/**
 * <ul>
 * <li>
 * Project Pradera.
 * This class is useful to handle Modal if you want to back
 * at the search.xhtml, When one changes was realized. If
 * there isn't any change on the form the modal is not show.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class backDialogController.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/03/2013
 */
public class BackDialogController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The object before. */
	private StringBuilder objectBefore;
	
	/** The object after. */
	private StringBuilder objectAfter;
	
	/** The Constant SEARCH_ACTION. */
	public static final String SEARCH_ACTION = "search"; 
	
	/**
	 * Gets the object before.
	 *
	 * @return the object before
	 */
	public StringBuilder getObjectBefore() {
		return objectBefore;
	}
	
	/**
	 * Sets the object before.
	 *
	 * @param objectBefore the new object before
	 */
	public void setObjectBefore(StringBuilder objectBefore) {
		this.objectBefore = objectBefore;
	}
	
	/**
	 * Gets the object after.
	 *
	 * @return the object after
	 */
	public StringBuilder getObjectAfter() {
		return objectAfter;
	}
	
	/**
	 * Sets the object after.
	 *
	 * @param objectAfter the new object after
	 */
	public void setObjectAfter(StringBuilder objectAfter) {
		this.objectAfter = objectAfter;
	}
	
	/**
	 * Evualuate changes on ajax.
	 */
	public boolean isAjaxValue(){
		if(Validations.validateIsNull(this.getObjectBefore()) || Validations.validateIsNull(this.getObjectAfter())){
			return Boolean.TRUE;
		}
		if(this.isShowModal()){
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	/**
	 * Instantiates a new back dialog controller.
	 */
	public BackDialogController() {
		super();
	}
	
	/**
	 * Checks if is show modal.
	 *
	 * @return true, if is show modal
	 */
	public boolean isShowModal(){
		if(!this.getObjectBefore().toString().equals(this.getObjectAfter().toString())){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}