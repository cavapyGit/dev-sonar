package com.pradera.commons.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class JAXBUtils {
	
	public static <T> T unmarshall(Source xmlFile, Class<T> t) throws JAXBException, FileNotFoundException {
		// Creating context by class
		JAXBContext jaxbContext = JAXBContext.newInstance(t);
		// Creating a Unmarshaller
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		T jaxbObject = (T) jaxbUnmarshaller.unmarshal(xmlFile,t);
		return jaxbObject;
	}
	
	public static <T> T unmarshall(Source xmlFile, Class<T> t, Source schemaFile, ValidationEventHandler handler) 
			throws JAXBException, FileNotFoundException, SAXException {
		//Set schema props
		SchemaFactory sf = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		//Set schema
		Schema schema = sf.newSchema(schemaFile);
		// Creating context by class
		JAXBContext jaxbContext = JAXBContext.newInstance(t);
		//jaxbContext.`
		// Creating a Unmarshaller
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		//jaxbUnmarshaller.set
		jaxbUnmarshaller.setSchema(schema);
		jaxbUnmarshaller.setEventHandler(handler);
		//jaxbUnmarshaller.set
		//jaxbUnmarshaller.set
		JAXBElement<T> jaxbObject = (JAXBElement<T>) jaxbUnmarshaller.unmarshal(xmlFile,t);
		return jaxbObject.getValue();
	}
	
	public static <T> String marshall(T t) throws JAXBException {
		// Creating context by object class
	    JAXBContext jaxbContext = JAXBContext.newInstance(t.getClass());
	    // Creating a Marshaller
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	 
	    StringWriter result = new StringWriter();
	    jaxbMarshaller.marshal(t, result);
	 
	    String xml = result.toString();
	    
	    return xml;
	}

	public static <T> void generateSchema(T t) throws JAXBException, IOException {
		JAXBContext jaxbContext = JAXBContext.newInstance(t.getClass());
		//SchemaOutputResolver sor = new MySchemaOutputResolver();
		//jaxbContext.generateSchema(sor);
	}
	
}