package com.pradera.commons.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.integration.common.validation.Validations;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class GenericDataModel.
 *
 * @param <T> the generic type
 * @author PraderaTechnologies.
 * @version 1.0 , 04/03/2013
 */
public class GenericDataModel<T> extends ListDataModel<T> implements SelectableDataModel<T>,Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new generic data model.
	 */
	public GenericDataModel() {
		super();
	}

	/**
	 * Instantiates a new generic data model.
	 *
	 * @param list the list
	 */
	public GenericDataModel(List<T> list) {
		super(list);
	}

	/**
	 * Gets the data list.
	 *
	 * @return the data list
	 */
	@SuppressWarnings("unchecked")
	public List<T> getDataList(){
		if(Validations.validateIsNullOrEmpty(((List<T>) this.getWrappedData()))){
			this.setWrappedData(new ArrayList<Object>());
		}
		return (List<T>) this.getWrappedData();
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public T getRowData(String arg0) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(T arg0) {
		return arg0;
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public int getSize(){
		if(Validations.validateIsNotNullAndNotEmpty(((List<T>) this.getWrappedData()))){
			return ((List<T>) this.getWrappedData()).size();
		}
		return 0;
	}
}
