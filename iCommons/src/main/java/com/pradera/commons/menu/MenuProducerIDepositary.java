package com.pradera.commons.menu;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.GeneralConstants;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MenuProducerIDepositary.
 * production generation of Menu
 * @author PraderaTechnologies.
 * @version 1.0 , 29/04/2013
 */
@ApplicationScoped
public class MenuProducerIDepositary implements Serializable,MenuProducer {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4419136885131091646L;
	
	/** The excepcion. */
	@Inject
	private Event<ExceptionToCatchEvent> excepcion;
	
	/** The menu separator. */
	private String menuSeparator;
	
	/** The module name. */
	@Resource(lookup="java:module/ModuleName")
	private String moduleName;

	/**
	 * Instantiates a new menu producer i depositary.
	 */
	public MenuProducerIDepositary() {
		menuSeparator ="-";
	}
	

	/* (non-Javadoc)
	 * @see com.pradera.commons.menu.MenuProducer#createPrivilegeUser(com.pradera.commons.sessionuser.UserInfo)
	 */
	@Override
	public void createPrivilegeUser(UserInfo userInfo) {
		PraderaMenu praderaMenu = userInfo.getPraderaMenuAll();
		if (praderaMenu != null) {
			for(PraderaSubMenu subMenuPradera : praderaMenu.getSubMenus()) {
				for (PraderaMenuOption menuOption : subMenuPradera.getOptions()) {
					String url = menuOption.getUrl();
					if(!url.equals(menuSeparator)) {
						//add privilegio to options
						userInfo.getOptionPrivileges().put(getAfterModule(url), menuOption.getUserAcctions());
					}
				}
				if (subMenuPradera.getSubMenus() != null && subMenuPradera.getSubMenus().size() > 0) {
					for(PraderaSubMenu subMenuPraderaChildren: subMenuPradera.getSubMenus()) {
						//recursive Childrens
						createRecursivePrivilegeUser(userInfo,subMenuPraderaChildren);
					}
				}
			}
		}
	}
	
	/**
	 * Creates the recursive privilege user.
	 *
	 * @param userInfo the user info
	 * @param subMenuPraderaChildren the sub menu pradera children
	 */
	private void createRecursivePrivilegeUser(UserInfo userInfo,PraderaSubMenu subMenuPraderaChildren){
		for(PraderaMenuOption opcionChildren : subMenuPraderaChildren.getOptions()) {
			String url = opcionChildren.getUrl();
			if(!url.equals(menuSeparator)) {
				userInfo.getOptionPrivileges().put(getAfterModule(url), opcionChildren.getUserAcctions());
			}
		}
		/* workaround subMenus in the last order */
		if (subMenuPraderaChildren.getSubMenus() != null) {
			for(PraderaSubMenu menuChildrenPradera : subMenuPraderaChildren.getSubMenus()) {
				createRecursivePrivilegeUser(userInfo,menuChildrenPradera);
			}
		}
	}
	
	/**
	 * Show menu.
	 * to generate menu primefaces 
	 * @param userInfo the user info
	 * @param language the language
	 * @param moduleApplication the module application
	 * @return the menu model
	 */
	public MenuModel showMenu(UserInfo userInfo,String language,String moduleApplication) {
		try{
			moduleApplication = moduleName;
			PraderaMenu praderaMenu = userInfo.getPraderaMenuAll();
			MenuModel menuPrincipal = null;
			if (praderaMenu != null) {
				menuPrincipal = new DefaultMenuModel();
				for(PraderaSubMenu subMenuPradera : praderaMenu.getSubMenus()) {
					DefaultSubMenu subMenuPrime = new DefaultSubMenu();
					subMenuPrime.setLabel(getLabelLanguage(subMenuPradera.getName(), language));
					if(subMenuPradera.getIdMenuOptionPk() != null){
						subMenuPrime.setId("idSubMenu" + subMenuPradera.getIdMenuOptionPk());
					}
					//submenu style color ugly blue
				//	subMenuPrime.setStyleClass("submenu-san");
					menuPrincipal.addElement(subMenuPrime);
					//Childrens
					//submenus
					/* workaround empty list to null jaxb/json */
					if (subMenuPradera.getSubMenus() != null && subMenuPradera.getSubMenus().size() > 0) {
						for(PraderaSubMenu subMenuPraderaChildren: subMenuPradera.getSubMenus()) {
							DefaultSubMenu subMenuChildren = new DefaultSubMenu();
							subMenuChildren.setLabel(getLabelLanguage(subMenuPraderaChildren.getName(), language));
							if(subMenuPraderaChildren.getIdMenuOptionPk() != null){
								subMenuChildren.setId("idSubMenuChild" + subMenuPraderaChildren.getIdMenuOptionPk());
							}
							subMenuPrime.addElement(subMenuChildren);
							//recursive Childrens
							getChildren(userInfo,subMenuChildren, subMenuPraderaChildren, language,moduleApplication);
						}
					}
					//options
//					if (userInfo.getOptionPrivileges() == null) {
//						userInfo.setOptionPrivileges(new HashMap<String, UserAcctions>());
//					}
					for (PraderaMenuOption menuOption : subMenuPradera.getOptions()) {
						DefaultMenuItem menuItem = new DefaultMenuItem(getLabelLanguage(menuOption.getName(), language));
						//menuItem.setValue(getLabelLanguage(menuOption.getName(), language));
						if(menuOption.getIdMenuOptionPk() != null){
							menuItem.setId("idMenuItem" + menuOption.getIdMenuOptionPk());
						}
						String url = menuOption.getUrl();
						if(!url.equals(menuSeparator)) {
							//Privilege is created in other method
//							userInfo.getOptionPrivileges().put(getAfterModule(url), menuOption.getUserAcctions());
							//outcome according if is local url or remote
							StringBuilder urlFinal;
							menuItem.setTarget("gameFrame");
							urlFinal = new StringBuilder(url);
							if (isUrlLocal(moduleApplication,url)) {								
								menuItem.setOutcome(StringUtils.substringAfter(url, moduleApplication).replaceAll(".xhtml",""));
								menuItem.setParam(GeneralConstants.RESET_BEAN_MENU, GeneralConstants.RESET_BEAN_MENU);
							} else{
								//remote
								if(url.contains("?")){
									urlFinal.append("&");
								}else{
									urlFinal.append("?");
								}
								urlFinal.append("tikectId="+AESEncryptUtils.encrypt(
										AESKeyType.PRADERAKEY.getValue(),
									userInfo.getUserAccountSession().getTicketSession()));
								urlFinal.append("&"+GeneralConstants.RESET_BEAN_MENU).
								append("=")
								.append(GeneralConstants.RESET_BEAN_MENU);
								if (!userInfo.getCurrentSystem().isEmpty() ){
									urlFinal.append("&usuario=" + userInfo.getUserAccountSession().getUserName());									
								}
								menuItem.setUrl(urlFinal.toString());
							}
							menuItem.setOnclick("$(this).attr('target',window.frames[0].name);addDirectAccess('"+menuItem.getValue()+"','"+urlFinal.toString()+"')");
							menuItem.setIcon("ui-icon-seek-next");
						}else {
							menuItem.setIcon("ui-icon-folder-collapsed");
						}
						menuItem.setAjax(false);						
						subMenuPrime.addElement(menuItem);
					}
				}
			}
			return menuPrincipal;//model for vertical menu
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return null;
		}
	}
	
	/**
	 * return the relative url after the module from give url.
	 *
	 * @param url complete url
	 * @return relative url without module
	 */
	public String getAfterModule(String url){
		String module = "";
		if(url != null){
			String[] strArr = url.split("/");
			for(int i=0; i<strArr.length; i++){
				if(strArr[i] != null && strArr[i].trim().length() > 0 && !strArr[i].contains(":")&& !strArr[i].contains(".")){
					module = strArr[i];
					break;
				}
			}
		}
		return StringUtils.substringAfter(url, module);
	}
	
	/**
	 * Gets the children.
	 *
	 * @param userInfo the user info
	 * @param subMenu the sub menu
	 * @param praderaSubMenu the pradera sub menu
	 * @param language the language
	 * @param moduleApplication the module application
	 * @return the children
	 * @throws Exception the exception
	 */
	private void getChildren(UserInfo userInfo,DefaultSubMenu subMenu,PraderaSubMenu praderaSubMenu,String language,String moduleApplication) throws Exception {
		//options
		for(PraderaMenuOption opcionChildren : praderaSubMenu.getOptions()) {
			DefaultMenuItem menuItem = new DefaultMenuItem();
			menuItem.setValue(getLabelLanguage(opcionChildren.getName(), language));
			if(opcionChildren.getIdMenuOptionPk() != null){
				menuItem.setId("idMenuItem" + opcionChildren.getIdMenuOptionPk());
			}
			String url = opcionChildren.getUrl();
			if(!url.equals(menuSeparator)) {
//				Privilege is created in other method
//				userInfo.getOptionPrivileges().put(getAfterModule(url), opcionChildren.getUserAcctions());
				//outcome according if is local url or remote
				menuItem.setTarget("gameFrame");
				StringBuilder urlFinal;
				urlFinal = new StringBuilder(url);
				if (isUrlLocal(moduleApplication,url)) {
					menuItem.setOutcome(StringUtils.substringAfter(url, moduleApplication).replaceAll(".xhtml",""));
					menuItem.setParam(GeneralConstants.RESET_BEAN_MENU, GeneralConstants.RESET_BEAN_MENU);
				} else{
					//remote
					if(url.contains("?")){
						urlFinal.append("&");
					}else{
						urlFinal.append("?");
					}
					urlFinal.append("tikectId="+AESEncryptUtils.encrypt(
							AESKeyType.PRADERAKEY.getValue(),
						userInfo.getUserAccountSession().getTicketSession()));
					urlFinal.append("&"+GeneralConstants.RESET_BEAN_MENU).
					append("=")
					.append(GeneralConstants.RESET_BEAN_MENU);
					if (!userInfo.getCurrentSystem().isEmpty() ){
						urlFinal.append("&usuario=" + userInfo.getUserAccountSession().getUserName());									
					}
					menuItem.setUrl(urlFinal.toString());
				}
				//add privilegio to options
				menuItem.setOnclick("$(this).attr('target',window.frames[0].name);addDirectAccess('"+menuItem.getValue()+"','"+urlFinal.toString()+"')");
				menuItem.setIcon("ui-icon-seek-next");
			}else{
				menuItem.setIcon("ui-icon-folder-collapsed");
			}
			
			menuItem.setAjax(false);						
			subMenu.addElement(menuItem);
		}
		
		/* workaround subMenus in the last order */
		if (praderaSubMenu.getSubMenus() != null) {
			for(PraderaSubMenu menuChildrenPradera : praderaSubMenu.getSubMenus()) {
				//submenus
				DefaultSubMenu subMenuChildren = new DefaultSubMenu();
				subMenuChildren.setLabel(getLabelLanguage(menuChildrenPradera.getName(), language));
				if(menuChildrenPradera.getIdMenuOptionPk() != null){
					subMenuChildren.setId("idSubMenuChild" + menuChildrenPradera.getIdMenuOptionPk());
				}
				subMenu.addElement(subMenuChildren);
				getChildren(userInfo,subMenuChildren, menuChildrenPradera, language,moduleApplication);
			}
		}
	}
	
	/**
	 * Gets the label language.
	 *
	 * @param names the names
	 * @param language the language
	 * @return the label language
	 */
	private String getLabelLanguage(Map<String,String> names, String language) {
		if (names.get(language) != null) {
			return names.get(language);
		} else {
			//TODO replace call Type Idioma when refactory IdiomaType to PraderaCommons
			return names.get("es");
		}
	}
	
	/**
	 * Checks if is url local.
	 *
	 * @param module the module
	 * @param urlOption the url option
	 * @return true, if is url local
	 */
	private boolean isUrlLocal(String module,String urlOption) {
		boolean result = false;
		int index = urlOption.indexOf(module);
		if (index > 0) {
			result = true;
		}
		return result;
	}


}
