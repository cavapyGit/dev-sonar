package com.pradera.commons.menu;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PraderaSubMenu.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
public class PraderaSubMenu implements Serializable, Comparable<PraderaSubMenu> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3310441062187354859L;

	/** The opciones. */
	private Set<PraderaMenuOption> options;
	
	/** The sub menus. */
	private Set<PraderaSubMenu> subMenus;
	
	/** The nombre. */
	private Map<String,String> name;
	
	/** The order option. */
	private Integer orderOption;
	
	/** The menu option pk */
	private Integer idMenuOptionPk;
	
	/**
	 * Instantiates a new pradera sub menu.
	 */
	public PraderaSubMenu() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the options.
	 *
	 * @return the options
	 */
	public Set<PraderaMenuOption> getOptions() {
		return options;
	}

	/**
	 * Sets the options.
	 *
	 * @param options the new options
	 */
	public void setOptions(Set<PraderaMenuOption> options) {
		this.options = options;
	}

	/**
	 * Gets the sub menus.
	 *
	 * @return the sub menus
	 */
	public Set<PraderaSubMenu> getSubMenus() {
		return subMenus;
	}

	/**
	 * Sets the sub menus.
	 *
	 * @param subMenus the new sub menus
	 */
	public void setSubMenus(Set<PraderaSubMenu> subMenus) {
		this.subMenus = subMenus;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public Map<String, String> getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(Map<String, String> name) {
		this.name = name;
	}

	/**
	 * Gets the order option.
	 *
	 * @return the order option
	 */
	public Integer getOrderOption() {
		return orderOption;
	}

	/**
	 * Sets the order option.
	 *
	 * @param orderOption the new order option
	 */
	public void setOrderOption(Integer orderOption) {
		this.orderOption = orderOption;
	}

	/**
	 * @return the idMenuOptionPk
	 */
	public Integer getIdMenuOptionPk() {
		return idMenuOptionPk;
	}

	/**
	 * @param idMenuOptionPk the idMenuOptionPk to set
	 */
	public void setIdMenuOptionPk(Integer idMenuOptionPk) {
		this.idMenuOptionPk = idMenuOptionPk;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PraderaSubMenu objPraderaSubMenu) {
		return this.getOrderOption().compareTo(objPraderaSubMenu.getOrderOption());
	}
	
	
}