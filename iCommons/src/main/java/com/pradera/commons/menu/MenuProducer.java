package com.pradera.commons.menu;

import org.primefaces.model.menu.MenuModel;

import com.pradera.commons.sessionuser.UserInfo;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Interface MenuProducer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/11/2012
 */

public interface MenuProducer {
	
	/**
	 * Generate menu in xhtml pages from primefaces Model.
	 *
	 * @param userInfo the user info
	 * @param language the language
	 * @param moduleApplication the module application
	 * @return the menu model
	 */
	public MenuModel showMenu(UserInfo userInfo,String language,String moduleApplication); 
	
	/**
	 * Creates the privilege user.
	 *
	 * @param userInfo the user info
	 */
	public void createPrivilegeUser(UserInfo userInfo);
}
