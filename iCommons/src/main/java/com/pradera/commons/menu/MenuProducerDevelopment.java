package com.pradera.commons.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.pradera.commons.configuration.IpLocalDevelopment;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.usersession.UserAcctions;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class MenuProducerDevelopment.
 * Development enviroment generation of Menu
 * @author PraderaTechnologies.
 * @version 1.0 , 29/04/2013
 */
@ApplicationScoped
@Alternative
public class MenuProducerDevelopment implements Serializable,MenuProducer {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5393353183193902959L;
	
	/** The excepcion. */
	@Inject
	private Event<ExceptionToCatchEvent> excepcion;
	
	@Inject @IpLocalDevelopment
	private String ipLocalVpn;
	
	/** The menu separator. */
	private String menuSeparator;

	/**
	 * Instantiates a new menu producer development.
	 */
	public MenuProducerDevelopment() {
		menuSeparator = "-";
	}
	
	/**
	 * Show menu.
	 * to generate menu primefaces 
	 * @param userInfo the user info
	 * @param language the language
	 * @param moduleApplication the module application
	 * @return the menu model
	 */
	public MenuModel showMenu(UserInfo userInfo,String language,String moduleApplication) {
		try{
			PraderaMenu praderaMenu = userInfo.getPraderaMenuAll();
			MenuModel menuPrincipal = null;
			//tiered Menu
			List<TieredMenuModel> tieredModelList = new ArrayList<TieredMenuModel>();
			//END - tiered Menu
			if (praderaMenu != null) {
				menuPrincipal = new DefaultMenuModel();				
				for(PraderaSubMenu subMenuPradera : praderaMenu.getSubMenus()) {
					//tiered Menu
					TieredMenuModel tieredModel = new TieredMenuModel();
					MenuModel tieredMenuModel = new DefaultMenuModel();
					tieredModel.setMenuModel(tieredMenuModel);
					//END - tiered Menu
					
					DefaultSubMenu subMenuPrime = new DefaultSubMenu();
					subMenuPrime.setLabel(getLabelLanguage(subMenuPradera.getName(), language));
					tieredModel.setParentName(getLabelLanguage(subMenuPradera.getName(), language));
					if(subMenuPradera.getIdMenuOptionPk() != null){
						subMenuPrime.setId("idSubMenu" + subMenuPradera.getIdMenuOptionPk());
						//tiered Menu
						tieredModel.setId("idSubMenu" + subMenuPradera.getIdMenuOptionPk());
						//END - tiered Menu
					}
					menuPrincipal.addElement(subMenuPrime);
					//Childrens
					//submenus
					/* workaround empty list to null jaxb/json */
					if (subMenuPradera.getSubMenus() != null) {
						for(PraderaSubMenu subMenuPraderaChildren: subMenuPradera.getSubMenus()) {
							DefaultSubMenu subMenuChildren = new DefaultSubMenu();
							subMenuChildren.setLabel(getLabelLanguage(subMenuPraderaChildren.getName(), language));
							if(subMenuPraderaChildren.getIdMenuOptionPk() != null){
								subMenuChildren.setId("idSubMenuChild" + subMenuPraderaChildren.getIdMenuOptionPk());
							}
							subMenuPrime.addElement(subMenuChildren);
							//tiered Menu
							tieredMenuModel.addElement(subMenuChildren);
							//END - tiered Menu
							//recursive Childrens
							getChildren(userInfo,subMenuChildren, subMenuPraderaChildren, language,moduleApplication);
						}
					}
					//options
					if (userInfo.getOptionPrivileges() == null) {
						userInfo.setOptionPrivileges(new HashMap<String, UserAcctions>());
					}
					for (PraderaMenuOption menuOption : subMenuPradera.getOptions()) {
						DefaultMenuItem menuItem = new DefaultMenuItem();
						menuItem.setValue(getLabelLanguage(menuOption.getName(), language));
						if(menuOption.getIdMenuOptionPk() != null){
							menuItem.setId("idMenuItem" + menuOption.getIdMenuOptionPk());
						}
						String url = menuOption.getUrl();
						if(!url.equals(menuSeparator)) {
							//outcome according if is local url or remote
							if (isUrlLocal(moduleApplication,url)) {
								menuItem.setTarget("gameFrame");
								StringBuilder urlFinal = new StringBuilder(StringUtils.substringAfter(url, moduleApplication).replaceAll(".xhtml",""));
								menuItem.setOutcome(urlFinal.toString());
								menuItem.setParam(GeneralConstants.RESET_BEAN_MENU, GeneralConstants.RESET_BEAN_MENU);
							} else{
								//remote
								menuItem.setTarget("_top");
								//Concat ip local machine vpn
								String nameModule = getNameModule(url);
								StringBuilder urlFinal = new StringBuilder("http://"+ipLocalVpn+":8080/"+nameModule);
								urlFinal.append(getAfterModule(url));
								if(url.contains("?")){
									urlFinal.append("&");
								}else{
									urlFinal.append("?");
								}
								urlFinal.append("tikectId="+AESEncryptUtils.encrypt(
										AESKeyType.PRADERAKEY.getValue(),
									userInfo.getUserAccountSession().getTicketSession()));
								urlFinal.append("&"+GeneralConstants.RESET_BEAN_MENU).
								append("=")
								.append(GeneralConstants.RESET_BEAN_MENU);
								menuItem.setUrl(urlFinal.toString());
							}
							//add privilegio to options
							userInfo.getOptionPrivileges().put(getAfterModule(url), menuOption.getUserAcctions());
						}
						menuItem.setAjax(false);
						subMenuPrime.addElement(menuItem);
						//tiered Menu
						tieredMenuModel.addElement(menuItem);
						//END - tiered Menu
					}
					//tiered Menu
					tieredModelList.add(tieredModel);
					//END - tiered Menu
				}
			}
			return menuPrincipal;
		
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return null;
		}
	}
	
	/**
	 * return the relative url after the module from give url.
	 *
	 * @param url complete url
	 * @return relative url without module
	 */
	public String getAfterModule(String url){
		String module = getNameModule(url);
		return StringUtils.substringAfter(url, module);
	}
	
	/**
	 * Gets the name module.
	 *
	 * @param url the url
	 * @return the name module
	 */
	private String getNameModule(String url) {
		String module = "";
		if(url != null){
			String[] strArr = url.split("/");
			for(int i=0; i<strArr.length; i++){
				if(strArr[i] != null && strArr[i].trim().length() > 0 && !strArr[i].contains(":")&& !strArr[i].contains(".")){
					module = strArr[i];
					break;
				}
			}
		}
		return module;
	}
	
	/**
	 * Gets the children.
	 *
	 * @param userInfo the user info
	 * @param subMenu the sub menu
	 * @param praderaSubMenu the pradera sub menu
	 * @param language the language
	 * @param moduleApplication the module application
	 * @return the children
	 * @throws Exception the exception
	 */
	private void getChildren(UserInfo userInfo,DefaultSubMenu subMenu,PraderaSubMenu praderaSubMenu,String language,String moduleApplication) throws Exception {
		/* workaround empty list to null jaxb/json */
		if (praderaSubMenu.getSubMenus() != null) {
			for(PraderaSubMenu menuChildrenPradera : praderaSubMenu.getSubMenus()) {
				//submenus
				DefaultSubMenu subMenuChildren = new DefaultSubMenu();
				subMenuChildren.setLabel(getLabelLanguage(menuChildrenPradera.getName(), language));
				subMenu.addElement(subMenuChildren);
				getChildren(userInfo,subMenuChildren, menuChildrenPradera, language,moduleApplication);
			}
		}
		
		//options
		for(PraderaMenuOption opcionChildren : praderaSubMenu.getOptions()) {
			DefaultMenuItem menuItem = new DefaultMenuItem();
			menuItem.setValue(getLabelLanguage(opcionChildren.getName(), language));
			if(opcionChildren.getIdMenuOptionPk() != null){
				menuItem.setId("idMenuItem" + opcionChildren.getIdMenuOptionPk());
		    }
			String url = opcionChildren.getUrl();
			if(!url.equals(menuSeparator)) {
				//outcome according if is local url or remote
				if (isUrlLocal(moduleApplication,url)) {
					menuItem.setTarget("gameFrame");
					StringBuilder urlFinal = new StringBuilder(StringUtils.substringAfter(url, moduleApplication).replaceAll(".xhtml",""));
					menuItem.setOutcome(urlFinal.toString());
					menuItem.setParam(GeneralConstants.RESET_BEAN_MENU, GeneralConstants.RESET_BEAN_MENU);
				} else{
					//remote
					menuItem.setTarget("_top");
					//Concat ip local machine vpn
					String nameModule = getNameModule(url);
					StringBuilder urlFinal = new StringBuilder("http://"+ipLocalVpn+":8080/"+nameModule);
					urlFinal.append(getAfterModule(url));
					if(url.contains("?")){
						urlFinal.append("&");
					}else{
						urlFinal.append("?");
					}
					urlFinal.append("tikectId="+AESEncryptUtils.encrypt(
							AESKeyType.PRADERAKEY.getValue(),
						userInfo.getUserAccountSession().getTicketSession()));
					urlFinal.append("&"+GeneralConstants.RESET_BEAN_MENU).
					append("=")
					.append(GeneralConstants.RESET_BEAN_MENU);
					menuItem.setUrl(urlFinal.toString());
				}
				//add privilegio to options
				userInfo.getOptionPrivileges().put(getAfterModule(url), opcionChildren.getUserAcctions());
			}
			menuItem.setAjax(false);						
			subMenu.addElement(menuItem);
		}
	}
	
	/**
	 * Gets the label language.
	 *
	 * @param names the names
	 * @param language the language
	 * @return the label language
	 */
	private String getLabelLanguage(Map<String,String> names, String language) {
		if (names.get(language) != null) {
			return names.get(language);
		} else {
			//TODO replace call Type Idioma when refactory IdiomaType to PraderaCommons
			return names.get("es");
		}
	}
	
	/**
	 * Checks if is url local.
	 *
	 * @param module the module
	 * @param urlOption the url option
	 * @return true, if is url local
	 */
	private boolean isUrlLocal(String module,String urlOption) {
		boolean result = false;
		int index = urlOption.indexOf(module);
		if (index > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public void createPrivilegeUser(UserInfo userInfo) {
		// TODO Auto-generated method stub
		
	}

}
