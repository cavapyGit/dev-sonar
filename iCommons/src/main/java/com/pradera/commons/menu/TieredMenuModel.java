package com.pradera.commons.menu;

import java.io.Serializable;

import org.primefaces.model.menu.MenuModel;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Interface MenuProducer.
 *x
 * @author PraderaTechnologies.
 * @version 1.0 , 20/11/2012
 */

public class TieredMenuModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MenuModel menuModel;
	
	private String parentName;
	
	private String id;
	
	public MenuModel getMenuModel() {
		return menuModel;
	}

	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public TieredMenuModel() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}