package com.pradera.commons.menu;

import java.io.Serializable;
import java.util.Map;

import com.pradera.integration.usersession.UserAcctions;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PraderaMenuOption.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
public class PraderaMenuOption implements Serializable, Comparable<PraderaMenuOption> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8007894220419150238L;

	/** The url. */
	private String url;
	
	/** The nombre. */
	private Map<String,String> name;
	
	/** The id menu option pk. */
	private Integer idMenuOptionPk;
	
	/** The id module pk. */
	private Integer idModulePk;
	
	/** The user acctions. */
	private UserAcctions userAcctions;
	
	/** The order option. */
	private Integer orderOption;
	
	/**
	 * Instantiates a new pradera opcion menu.
	 */
	public PraderaMenuOption() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the user acctions.
	 *
	 * @return the user acctions
	 */
	public UserAcctions getUserAcctions() {
		return userAcctions;
	}

	/**
	 * Sets the user acctions.
	 *
	 * @param userAcctions the new user acctions
	 */
	public void setUserAcctions(UserAcctions userAcctions) {
		this.userAcctions = userAcctions;
	}

	/**
	 * Gets the id menu option pk.
	 *
	 * @return the id menu option pk
	 */
	public Integer getIdMenuOptionPk() {
		return idMenuOptionPk;
	}

	/**
	 * Sets the id menu option pk.
	 *
	 * @param idMenuOptionPk the new id menu option pk
	 */
	public void setIdMenuOptionPk(Integer idMenuOptionPk) {
		this.idMenuOptionPk = idMenuOptionPk;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public Map<String, String> getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(Map<String, String> name) {
		this.name = name;
	}

	/**
	 * Gets the id module pk.
	 *
	 * @return the id module pk
	 */
	public Integer getIdModulePk() {
		return idModulePk;
	}

	/**
	 * Sets the id module pk.
	 *
	 * @param idModulePk the new id module pk
	 */
	public void setIdModulePk(Integer idModulePk) {
		this.idModulePk = idModulePk;
	}

	/**
	 * Gets the order option.
	 *
	 * @return the order option
	 */
	public Integer getOrderOption() {
		return orderOption;
	}

	/**
	 * Sets the order option.
	 *
	 * @param orderOption the new order option
	 */
	public void setOrderOption(Integer orderOption) {
		this.orderOption = orderOption;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PraderaMenuOption objPraderaMenuOption) {
		return this.getOrderOption().compareTo(objPraderaMenuOption.getOrderOption());
	}

}
