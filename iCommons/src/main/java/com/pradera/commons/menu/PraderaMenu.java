package com.pradera.commons.menu;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Class PraderaMenu.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/11/2012
 */

public class PraderaMenu implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1103068699556113296L;
	
	/** The sub menus. */
	private Set<PraderaSubMenu> subMenus;
	
	/** The nombre. */
	private Map<String,String> nombre;
	
	/**
	 * Instantiates a new pradera menu.
	 */
	public PraderaMenu() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the sub menus.
	 *
	 * @return the sub menus
	 */
	public Set<PraderaSubMenu> getSubMenus() {
		return subMenus;
	}

	/**
	 * Sets the sub menus.
	 *
	 * @param subMenus the new sub menus
	 */
	public void setSubMenus(Set<PraderaSubMenu> subMenus) {
		this.subMenus = subMenus;
	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public Map<String,String> getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the nombre
	 */
	public void setNombre(Map<String,String> nombre) {
		this.nombre = nombre;
	}

}