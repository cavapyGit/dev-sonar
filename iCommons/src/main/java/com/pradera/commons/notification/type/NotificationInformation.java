package com.pradera.commons.notification.type;

import java.util.List;
import java.util.Map;

import com.pradera.integration.contextholder.LoggerUser;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum AlertType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public class NotificationInformation {

	private List<Long> participantCodes;
	private List<String> issuerCodes;
	private Map<String,Object> parameters;
	private LoggerUser loggerUser;
	private Long businessProcess;
	public List<Long> getParticipantCodes() {
		return participantCodes;
	}
	public void setParticipantCodes(List<Long> participantCodes) {
		this.participantCodes = participantCodes;
	}
	public List<String> getIssuerCodes() {
		return issuerCodes;
	}
	public void setIssuerCodes(List<String> issuerCodes) {
		this.issuerCodes = issuerCodes;
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	public LoggerUser getLoggerUser() {
		return loggerUser;
	}
	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}
	public Long getBusinessProcess() {
		return businessProcess;
	}
	public void setBusinessProcess(Long businessProcess) {
		this.businessProcess = businessProcess;
	}
	@Override
	public String toString() {
		return "NotificationInformation [participantCodes=" + participantCodes
				+ ", issuerCodes=" + issuerCodes + ", parameters=" + parameters
				+ ", loggerUser=" + loggerUser + ", businessProcess="
				+ businessProcess + "]";
	}
}