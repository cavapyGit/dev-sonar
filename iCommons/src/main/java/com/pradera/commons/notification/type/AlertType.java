package com.pradera.commons.notification.type;

import java.util.HashMap;
import java.util.Map;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum AlertType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public enum AlertType {

	/** The message. */
	MESSAGE(Integer.valueOf(1)),/** The email. */
	EMAIL(Integer.valueOf(2)),/** The both. */
	BOTH(Integer.valueOf(3));
	
	/** The code. */
	private Integer code;
	
	/**
	 * Instantiates a new alert type.
	 *
	 * @param codigo the codigo
	 */
	private AlertType(Integer codigo) {
		this.code = codigo;
	}
	
	/** The Constant lookup. */
	private static final Map<Integer, AlertType> lookup = new HashMap<Integer, AlertType>();
    static {
        for (AlertType d : AlertType.values())
            lookup.put(d.getCode(), d);
    }
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}
    
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the alert type
	 */
	public static AlertType get(Integer codigo) {
        return lookup.get(codigo);
    }
}
