package com.pradera.commons.notification.type;

import java.util.HashMap;
import java.util.Map;



/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum AlertType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 29/11/2012
 */
public enum NotificationParamType {

	/** The message. */
	PARAM_1(Integer.valueOf(1),"param_1"),/** The email. */
	PARAM_2(Integer.valueOf(2),"param_2"),/** The both. */
	PARAM_3(Integer.valueOf(3),"param_3"),
	PARAM_4(Integer.valueOf(4),"param_4"),
	PARAM_5(Integer.valueOf(5),"param_5"),
	PARAM_6(Integer.valueOf(6),"param_6"),
	PARAM_7(Integer.valueOf(7),"param_7"),
	PARAM_8(Integer.valueOf(8),"param_8"),
	PARAM_9(Integer.valueOf(9),"param_9"),
	PARAM_10(Integer.valueOf(10),"param_10")
	;
	
	/** The code. */
	private Integer code;
	
	private String value;
	
	/**
	 * Instantiates a new alert type.
	 *
	 * @param codigo the codigo
	 */
	private NotificationParamType(Integer codigo, String value) {
		this.code = codigo;
		this.value = value;
	}
	
	/** The Constant lookup. */
	private static final Map<Integer, NotificationParamType> lookup = new HashMap<Integer, NotificationParamType>();
    static {
        for (NotificationParamType d : NotificationParamType.values())
            lookup.put(d.getCode(), d);
    }
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return this.code;
	}

	
	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public void setCode(Integer code) {
		this.code = code;
	}


	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the alert type
	 */
	public static NotificationParamType get(Integer codigo) {
        return lookup.get(codigo);
    }
}
