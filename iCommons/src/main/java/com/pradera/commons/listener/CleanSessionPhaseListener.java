/*
 * 
 */
package com.pradera.commons.listener;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.WindowContext;
import org.apache.myfaces.extensions.cdi.jsf.api.listener.phase.BeforePhase;
import org.apache.myfaces.extensions.cdi.jsf.api.listener.phase.JsfPhaseId;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.view.DepositaryGroupConversation;
import com.pradera.integration.usersession.UserAcctions;




/**
 * Clean conversation scoped after click in menu
 * 
 * The listener interface for receiving cleanSessionPhase events.
 * The class that is interested in processing a cleanSessionPhase
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addCleanSessionPhaseListener<code> method. When
 * the cleanSessionPhase event occurs, that object's appropriate
 * method is invoked.
 *
 * @see CleanSessionPhaseEvent
 */
@ApplicationScoped
public class CleanSessionPhaseListener implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9128555000855588237L;
	
	/** The windo context. */
	@Inject 
	WindowContext windowContext;
	
	@Inject
	UserInfo userInfo;
	
	@Inject
	Instance<UserPrivilege> userPrivilege;
	
	/**
	 * Clean conversation restore view.
	 *
	 * @param event the event
	 */
	public void cleanConversationRestoreView(@Observes @BeforePhase(JsfPhaseId.RESTORE_VIEW)  PhaseEvent event) {
		 FacesContext facesContext = event.getFacesContext();
			if(facesContext.getExternalContext().getRequestParameterMap().get(GeneralConstants.RESET_BEAN_MENU) != null 
					&& facesContext.getExternalContext().getRequestParameterMap().get(GeneralConstants.RESET_BEAN_MENU).equals(GeneralConstants.RESET_BEAN_MENU)){
				//Clean conversations CDI
				windowContext.getId();
				windowContext.closeConversations();
				//set privilege tab
				HttpServletRequest req =(HttpServletRequest)facesContext.getExternalContext().getRequest();
				String strUri = req.getServletPath();
				String reportId = req.getParameter("report_id");
				//concat report id for comparation againts menu
				if (StringUtils.isNotBlank(reportId)) {
					strUri = strUri + "?report_id="+reportId;
				}
				UserAcctions userPrivilegios = userInfo.getOptionPrivileges().get(strUri);
				userPrivilege.get().setUserAcctions(userPrivilegios);
			}
	}

}
