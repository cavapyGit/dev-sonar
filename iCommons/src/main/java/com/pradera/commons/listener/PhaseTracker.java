package com.pradera.commons.listener;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.utils.JSFUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class PhaseTracker.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
public class PhaseTracker implements PhaseListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
	 */
	@Override
	public void beforePhase(PhaseEvent event) {	
		FacesContext context = event.getFacesContext();		
		if(!context.getMessageList().isEmpty()){
			boolean hadErrors = false;
			List<FacesMessage> listaMensajes = context.getMessageList();
			for (FacesMessage facesMessage : listaMensajes) {
				if(facesMessage.getSeverity()==FacesMessage.SEVERITY_ERROR){
					hadErrors = true;
					break;
				}
			}
			 if(hadErrors){
			//validations for search select one menu
			 if(JSFUtilities.getRequestMap("searchValidation") != null && 
					 JSFUtilities.getRequestMap("searchValidation")==Boolean.TRUE){
					JSFUtilities.hideGeneralDialogues();
					JSFUtilities.removeRequestMap("searchValidation");
					JSFUtilities.showSearchRequiredDialog();
				} else if(StringUtils.isNotBlank(JSFUtilities.getRequestParameterMap("searchValidation"))) {
					JSFUtilities.hideGeneralDialogues();
					JSFUtilities.showSearchRequiredDialog();
				}
				else if(JSFUtilities.getRequestMap("executeAction") == null){
					JSFUtilities.hideGeneralDialogues();
					JSFUtilities.showRequiredDialog();
				}else{
					JSFUtilities.hideComponent("cnfMsgRequiredValidation");
					JSFUtilities.hideComponent("cnfMsgSearchValidation");
				}
			}
			
		}else{
			JSFUtilities.hideComponent("cnfMsgRequiredValidation");
			JSFUtilities.hideComponent("cnfMsgSearchValidation");
		}
		
		FacesContext facesContext = event.getFacesContext();
	    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
	    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");  
	    response.addHeader("Pragma", "no-cache");
	    response.setDateHeader("Expires", 0);  
	}
	
	/* (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
	 */
	@Override
	public void afterPhase(PhaseEvent event) {
		
	}

	/* (non-Javadoc)
	 * @see javax.faces.event.PhaseListener#getPhaseId()
	 */
	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}
}
