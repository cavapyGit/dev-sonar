package com.pradera.commons.interceptores;



import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PerformanceInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Performance
@Interceptor
public class PerformanceInterceptor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6043462320127602881L;
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Instantiates a new performance interceptor.
	 */
	public PerformanceInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Manage.
	 *
	 * @param context the context
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object manage(InvocationContext context) throws Exception {
		 String methodName = context.getMethod().toString();
	        long start = System.currentTimeMillis();
	        try {
	        	log.info("Enter Method "+CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HH:mm:ss.SSS"));
	            return context.proceed();
	        } catch (Exception e) {
	            log.error(String.format("!!!During invocation of: %s exception occured: %s", new Object[]{methodName, e}));
	            //registrar error en db
	            throw e;
	        } finally {
	            long duration = System.currentTimeMillis() - start;
	            /*log.info("class "+context.getClass().getSimpleName()+": Method "+methodName);
	            log.info(String.format(" duracion milisegundos : %s", new Object[]{duration}));*/
	            log.info("$$$$$$ class "+context.getClass().getSimpleName()+": Method "+methodName +String.format(" duration of  milliseconds to excecute method : %s", new Object[]{duration}));
	            log.info("End Method "+CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(),"HH:mm:ss.SSS"));
	        }
	}

}
