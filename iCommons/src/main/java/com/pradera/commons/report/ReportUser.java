package com.pradera.commons.report;

import java.io.Serializable;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ReportUser.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/07/2013
 */
public class ReportUser implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2354268847413435809L;
	
	/** The user name. */
	private String userName;
	
	/** The partcipant code. */
	private Long partcipantCode;
	
	/** The Issuer code. */
	private String IssuerCode;
	
	/** The show notification. */
	private Integer showNotification = Integer.valueOf(0);
	
	/** The ind ext interface. */
	private Integer indExtInterface;
	
	/** The institution type. */
	private Integer institutionType;
	
	/** The report format. */
	private Integer reportFormat;

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the partcipant code.
	 *
	 * @return the partcipant code
	 */
	public Long getPartcipantCode() {
		return partcipantCode;
	}

	/**
	 * Sets the partcipant code.
	 *
	 * @param partcipantCode the new partcipant code
	 */
	public void setPartcipantCode(Long partcipantCode) {
		this.partcipantCode = partcipantCode;
	}

	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return IssuerCode;
	}

	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		IssuerCode = issuerCode;
	}

	/**
	 * Instantiates a new report user.
	 */
	public ReportUser() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the show notification.
	 *
	 * @return the show notification
	 */
	public Integer getShowNotification() {
		return showNotification;
	}

	/**
	 * Sets the show notification.
	 *
	 * @param showNotification the new show notification
	 */
	public void setShowNotification(Integer showNotification) {
		this.showNotification = showNotification;
	}

	/**
	 * Gets the ind ext interface.
	 *
	 * @return the ind ext interface
	 */
	public Integer getIndExtInterface() {
		return indExtInterface;
	}

	/**
	 * Sets the ind ext interface.
	 *
	 * @param indExtInterface the new ind ext interface
	 */
	public void setIndExtInterface(Integer indExtInterface) {
		this.indExtInterface = indExtInterface;
	}

	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	public Integer getReportFormat() {
		return reportFormat;
	}

	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}
}