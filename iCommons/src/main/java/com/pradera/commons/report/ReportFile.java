package com.pradera.commons.report;

import java.io.Serializable;

public class ReportFile implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 733778526957938053L;

	private byte[] file;
	
	private String physicalName;
	
	private Integer reportType;

	/**
	 * @return the file
	 */
	public byte[] getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(byte[] file) {
		this.file = file;
	}

	/**
	 * @return the physicalName
	 */
	public String getPhysicalName() {
		return physicalName;
	}

	/**
	 * @param physicalName the physicalName to set
	 */
	public void setPhysicalName(String physicalName) {
		this.physicalName = physicalName;
	}

	/**
	 * @return the reportType
	 */
	public Integer getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}

}
