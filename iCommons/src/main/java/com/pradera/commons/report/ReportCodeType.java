package com.pradera.commons.report;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum ReportCodeType.
 */
public enum ReportCodeType {
	
	/** The detail expiration purchase. */
	DETAIL_EXPIRATION_REPURCHASE(new Long(178),"REPORTE DE DETALLE DE VENCIMIENTO DE REPORTOS", "_VTO_"),
	
	/** The purchase force expiration settled. */
	PURCHASE_FORCE_EXPIRATION_SETTLED(new Long(164),"REPORTE DE LIQUIDACIONES DE COMPRAS FORZOSAS, SELVE Y/O CAMBIO DE TIPO DE LIQUIDACION ESPECIAL", "_VTOCF_"),
	
	/** The operations detail with pending deliv. */
	OPERATIONS_DETAIL_WITH_PENDING_DELIV(new Long(165),"REPORTE DETALLE DE OPERACIONES PENDIENTES DE ENTREGA DE VALORES", "_PEND_ENT_VAL_"),
	
	/** The trading record BBV. */
	TRADING_RECORD_BBV(new Long(146),"REPORTE DE REGISTRO DE OPERACIONES BBV", "_OP_BBV_"),
	
	/** The prepaid settlement. */
	PREPAID_SETTLEMENT(new Long(131),"REPORTE DE SOLICITUDES DE LIQUIDACION ANTICIPADA", "_SOL_ANT_"),
	
	/** The settlement type exchange. */
	SETTLEMENT_TYPE_EXCHANGE(new Long(132),"REPORTE DE SOLICITUDES DE CAMBIO DE TIPO DE LIQUIDACION (SELVE)", "_SOL_SEL_"),
	
	/** The net positions operations bag pre. */
	NET_POSITIONS_OPERATIONS_BAG_PRE(new Long(161),"REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA (PRELIMINAR)", "_PRE_"),
	
	/** The details settled operations. */
	DETAILS_SETTLED_OPERATIONS(new Long(163),"REPORTE DE DETALLE DE OPERACIONES LIQUIDADAS", "_LIQ_"),
	
	/** The exlack security oper fund bag. */
	EXLACK_SECURITY_OPER_FUND_BAG(new Long(176),"REPORTE DE EXCLUSION POR FALTA DE VALORES Y/O FONDOS DE OPERACION DE BOLSA", "_EXC_"),
	
	/** The net settlement positions pre. */
	NET_SETTLEMENT_POSITIONS_PRE(new Long(115),"REPORTE DE RESUMEN DE POSICIONES NETAS (PRELIMINAR)", "_POS_PRE_"),
	
	/** REPORTS FOR AUTOMATIC SEND*. */
	NET_POSITIONS_SETTLED(new Long(129),"POSICIONES NETAS - LIQUIDADAS", "_POS_LIQ_"),
	
	/** The net positions withdrawals. */
	NET_POSITIONS_WITHDRAWALS(new Long(130),"POSICIONES NETAS - RETIRADAS", "_POS_EXC_"),
	
	/** The unfulfilled operations. */
	UNFULFILLED_OPERATIONS(new Long(128),"REPORTE DE OPERACIONES INCUMPLIDAS", "_INC_"),
	
	/** The settled operations. */
	SETTLED_OPERATIONS(new Long(145),"REPORTE DE OPERACIONES LIQUIDADAS", "_OP_LIQ_"),
	
	/** The settled advance operations. */
	SETTLED_ADVANCE_OPERATIONS(new Long(159),"REPORTE DE OPERACIONES LIQUIDADAS ANTICIPADAMENTE", "_LIQ_ANT_"),
	
	/** The gross scheme settlement. */
	GROSS_SCHEME_SETTLEMENT(new Long(149),"REPORTE LIQUIDADAS POR ESQUEMA BRUTO", "_LIQ_BRUT_"),
	
	/** The net positions operations def ex reg. */
	NET_POSITIONS_OPERATIONS_DEF_EX_REG(new Long(199),"REPORTE DE POSICIONES NETAS DE OPERACIONES EN BOLSA REGISTRO DE OPERACIONES DIFERIDAS", "_PREDIF_"),
	
	/** The transfers sent to participants. */
	TRANSFERS_SENT_TO_PARTICIPANTS(new Long(203), "TRANSFERENCIAS ENVIADAS A PARTICIPANTES", "_TRF_FON_"),
	
	/** The transfers sent received type transaction account . */
	TRANSFERS_SENT_RECEIVED_TYPE_TRANSACTION_ACCOUNT_CORE(new Long(204), "TRANSFERENCIAS ENVIADAS O RECIBIDAS POR TIPO DE OPERACION CUENTA CSDCORE", "_TRF_ENV_REC_FO_"),
	
	/** The daily balance transactions made participants. */
	DAILY_BALANCE_TRANSACTIONS_MADE_PARTICIPANTS(new Long(195), "BALANCE DIARIO DE TRANSACCIONES EFECTUADAS POR LOS PARTICIPANTES", "_BAL_DIR_TRA_FO_"),
	
	/** The assignment holder account. */
	ASSIGNMENT_HOLDER_ACCOUNT(new Long(140), "ASIGNACION CUENTA TITULAR", "_ASG_CTA_"),
	
	/** The holders assigned buy sell. */
	HOLDERS_ASSIGNED_BUY_SELL(new Long(150), "TITULARES ASIGNADOS COMPRA/VENTA", "_TIT_ASG_CV_"),
	
	/** The outstanding securities transactions block. */
	OUTSTANDING_SECURITIES_TRANSACTIONS_BLOCK(new Long(139), "OPERACIONES PENDIENTES DE BLOQUEAR VALORES", "_OPE_PEN_BLO_"),
	
	/** The negotiated transactions total. */
	NEGOTIATED_TRANSACTIONS_TOTAL(new Long(143), "TOTAL OPERACIONES NEGOCIADAS", "_TOT_OPE_NEG_"),
	
	/** The total amounts settled. */
	TOTAL_AMOUNTS_SETTLED(new Long(142), "TOTAL MONTOS LIQUIDADOS", "_TOT_MON_LIQ_") ,
	
	/** The record generation chains. */
	RECORD_GENERATION_CHAINS(new Long(258), "REGISTRO DE GENERACION DE ENCADENAMIENTOS", "_REG_ENC_"),
		
	/** The cuadre daily operations. */
	CUADRE_DAILY_OPERATIONS(new Long(136), "CUADRE DIARIO DE OPERACIONES", "_CUA_OP_"),
	
	/** The expiration securities by issuer. */
	EXPIRATION_SECURITIES_BY_ISSUER(new Long(64),"REPORTE DE VENCIMIENTO DE VALORES POR EMISOR", "_VENC_VAL_EMI_"),
	
	/** The report details pending expiration securities. */
	REPORT_DETAILS_PENDING_EXPIRATION_SECURITIES(new Long(184),"REPORTE DE DETALLE DE TITULOS PENDIENTES DE VENCIMIENTO","_DET_TIT_PEND_VENC_"),
	
	/** The summary reconciliation report daily. */
	SUMMARY_RECONCILIATION_REPORT_DAILY(new Long(194),"REPORTE DE RESUMEN DE CONCILIACION DIARIA", "_RES_CONC_DIA_"),
	
	/** The report dematerialized total portfolio clients. */
	REPORT_DEMATERIALIZED_TOTAL_PORTFOLIO_CLIENTS(new Long(277),"REPORTE DE CARTERA DE CLIENTES - DESMATERIALIZADA TOTAL", "_CARTERA_TOT_")
	
	;
		
	/** The value. */
	private String value;
	
	/** The code. */
	private Long code;
	
	/** The mnemonic. */
	private String mnemonic;
	
	/** The Constant list. */
	public static final List<ReportCodeType> list = new ArrayList<ReportCodeType>();
	
	/** The Constant listPk. */
	public static final List<Long> listPk = new ArrayList<Long>();
	
	/** The Constant lookup. */
	public static final Map<Long, ReportCodeType> lookup = new HashMap<Long, ReportCodeType>();

	static{
		for(ReportCodeType type: EnumSet.allOf(ReportCodeType.class))
		{
			list.add(type);
			lookup.put(type.getCode(), type);
			listPk.add(type.getCode());
		}

	}

	/**
	 * Instantiates a new accord type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param mnemonic the mnemonic
	 */
	private ReportCodeType(Long code, String value, String mnemonic) {
		this.value = value;
		this.code = code;
		this.mnemonic = mnemonic;
	}


	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}


	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}
	
	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the accord type
	 */
	public static ReportCodeType get(Long code)
	{
		return lookup.get(code);
	}
}
