package com.pradera.commons.report.remote;

import com.pradera.integration.contextholder.LoggerUser;

public class ReportRegisterTO {

	private LoggerUser loggerUser;
	
	private Long reportId;
	
	private Long relatedId;
	
	private String relatedParam;
	
	private String stringInitDate;
	
	private String institutionName;
	
	private Integer institutionType;
	
	private Long participantCode;

	public LoggerUser getLoggerUser() {
		return loggerUser;
	}

	public void setLoggerUser(LoggerUser loggerUser) {
		this.loggerUser = loggerUser;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public String getRelatedParam() {
		return relatedParam;
	}

	public void setRelatedParam(String relatedParam) {
		this.relatedParam = relatedParam;
	}

	public String getStringInitDate() {
		return stringInitDate;
	}

	public void setStringInitDate(String stringInitDate) {
		this.stringInitDate = stringInitDate;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	public Long getParticipantCode() {
		return participantCode;
	}

	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}
	
	
}
