package com.pradera.commons.contextholder.interceptor;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;


public class ContextHolderInterceptor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4151167249392893436L;
	
	@Resource
	private TransactionSynchronizationRegistry registry;

	public ContextHolderInterceptor() {
		// TODO Auto-generated constructor stub
	}

	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		//Read contextHolder from jsf view to ejb layer
		if (ThreadLocalContextHolder.checkInitialited()) {
			registry.putResource(RegistryContextHolderType.LOGGER_USER,
					ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name()));
			registry.putResource(RegistryContextHolderType.LOGGER_USER.name(),
					ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name()));
		}
		//path normal
		return ic.proceed();
	}

}
