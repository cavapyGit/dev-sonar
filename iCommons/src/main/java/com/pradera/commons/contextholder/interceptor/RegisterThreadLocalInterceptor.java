package com.pradera.commons.contextholder.interceptor;

import java.io.Serializable;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class RegisterThreadLocalInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
@LoggerAuditWeb
@Interceptor
public class RegisterThreadLocalInterceptor implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5114899327070760847L;
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	Instance<UserPrivilege> userPrivilege;

	/**
	 * Instantiates a new register thread local interceptor.
	 */
	public RegisterThreadLocalInterceptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Around invoke.
	 *
	 * @param ic the ic
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		//Register logger user info
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress(JSFUtilities.getRemoteIpAddress());
		loggerUser.setUserName(userInfo.getUserAccountSession().getUserName());
		loggerUser.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
		loggerUser.setSessionId(userInfo.getUserAccountSession().getTicketSession());
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		//Praderafilter in each request get current UserAcction from WindowContext
		loggerUser.setUserAction(userPrivilege.get().getUserAcctions());
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		return ic.proceed();
	}
	
	

}
