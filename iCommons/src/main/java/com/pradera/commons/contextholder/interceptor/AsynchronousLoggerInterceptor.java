package com.pradera.commons.contextholder.interceptor;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AsynchronousLoggerInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 16/09/2015
 */
public class AsynchronousLoggerInterceptor implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2426845420990300692L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The Constant INT_PRIVILEGE. */
	public static final Integer INT_PRIVILEGE = new Integer(69);
	
	/** The Constant strIpAddress. */
	public static final String strIpAddress = "192.168.100.41";
	
	/** The Constant strUserName. */
	public static final String strUserName = "ADMIN";
	
	/**
	 * Around invoke.
	 *
	 * @param context the context
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object aroundInvoke(InvocationContext context) throws Exception {
		log.info(" ::::::::::::Inicio Interceptor AsynchronousLoggerInterceptor::::::::::: ");
		//Read first parameters always should be Logger user
		LoggerUser loggerUser = (LoggerUser) context.getParameters()[0];
		if(loggerUser == null){
			log.info(" ::::::::::::AsynchronousLoggerInterceptor loggerUser is null (ERRINTERC) ::::::::::: ");
		} else {
			log.info(" ::::::::::::AsynchronousLoggerInterceptor loggerUser is not null (ERRINTERC) ::::::::::: ");			
			if(Validations.validateIsNullOrEmpty(loggerUser.getIdPrivilegeOfSystem())) {
				loggerUser.setIdPrivilegeOfSystem(INT_PRIVILEGE);
			}
			if(Validations.validateIsNullOrEmpty(loggerUser.getIpAddress())) {
				loggerUser.setIpAddress(strIpAddress);
			}
			if(Validations.validateIsNullOrEmpty(loggerUser.getUserName())){
				loggerUser.setUserName(strUserName);
			}			
			loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		}		
		registry.putResource(RegistryContextHolderType.LOGGER_USER, loggerUser);
		//path normal
		return context.proceed();
	}

	/**
	 * Instantiates a new asynchronous logger interceptor.
	 */
	public AsynchronousLoggerInterceptor() {
		// TODO Auto-generated constructor stub
	}

}
