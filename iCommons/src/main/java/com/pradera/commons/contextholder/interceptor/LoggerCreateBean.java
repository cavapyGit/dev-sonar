package com.pradera.commons.contextholder.interceptor;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface LoggerCreateBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/04/2013
 */
@InterceptorBinding
@Inherited
@Target({ TYPE })
@Retention(RUNTIME)
@Documented
public @interface LoggerCreateBean {

}
