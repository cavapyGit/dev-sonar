package com.pradera.commons.contextholder.interceptor;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.integration.contextholder.LoggerUser;

@LoggerCreateBean
@Interceptor
public class RegisterThreadConstructorInterceptor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7752920031925691077L;
	
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	Instance<UserPrivilege> userPrivilege;

	/**
	 * Around create bean.
	 *
	 * @param ic the ic
	 * @return the object
	 * @throws Exception the exception
	 */
	@PostConstruct
	public void aroundCreateBean(InvocationContext ic) throws Exception {
		//Register logger user info
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress(JSFUtilities.getRemoteIpAddress());
		loggerUser.setUserName(userInfo.getUserAccountSession().getUserName());
		loggerUser.setIdUserSessionPk(userInfo.getUserAccountSession().getIdUserSessionPk());
		loggerUser.setSessionId(userInfo.getUserAccountSession().getTicketSession());
		loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		//Praderafilter in each request get current UserAcction from WindowContext
		loggerUser.setUserAction(userPrivilege.get().getUserAcctions());
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
		ic.proceed();
	}

}
