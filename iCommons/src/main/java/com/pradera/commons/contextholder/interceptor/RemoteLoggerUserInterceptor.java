package com.pradera.commons.contextholder.interceptor;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.remote.ThreadRemoteContextHolder;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RemoteLoggerUserInterceptor.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 03/04/2014
 */
public class RemoteLoggerUserInterceptor implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4956848620975946723L;
	
	/** The registry. */
	@Resource
	private TransactionSynchronizationRegistry registry;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Around invoke.
	 *
	 * @param context the context
	 * @return the object
	 * @throws Exception the exception
	 */
	@AroundInvoke
	public Object aroundInvoke(InvocationContext context) throws Exception {
		log.info(" ::::::::::::RemoteLoggerUserInterceptor Component (RLUIC) ::::::::::: ");
		if(ThreadRemoteContextHolder.checkInitialited() && ThreadRemoteContextHolder.get(registry.getTransactionKey().toString())!=null){
			log.info(" ::::::::::::Instacia si null (RLUIC) ::::::::::: ");
			registry.putResource(RegistryContextHolderType.LOGGER_USER.name(), ThreadRemoteContextHolder.get(registry.getTransactionKey().toString()));
		}
		//Read first paremeters always should be Logger user
		LoggerUser loggerUser =(LoggerUser)registry.getResource(RegistryContextHolderType.LOGGER_USER.name());
		log.info(" ::::::::::::Obtener loggerUser (RLUIC) ::::::::::: ");
		if(loggerUser!=null){
			log.info(" ::::::::::::loggerUser diferente de null (RLUIC) ::::::::::: ");
			if(loggerUser.getIdPrivilegeOfSystem() == null) {
				loggerUser.setIdPrivilegeOfSystem(new Integer(69));
				log.info(" :::::::::::: set IdPrivilegeOfSystem (RLUIC) ::::::::::: ");
			}
			registry.putResource(RegistryContextHolderType.LOGGER_USER,loggerUser);
		}
		
		return context.proceed();
	}

}
