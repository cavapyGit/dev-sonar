package com.pradera.commons.contextholder.type;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum RegistryContextHolderType.
 * kEYS for context holder
 * @author PraderaTechnologies.
 * @version 1.0 , 21/03/2013
 */
public enum RegistryContextHolderType {
	
	/** The logger user. */
	LOGGER_USER
}
