package com.pradera.commons.bmt.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class is used to handle bean managed transactions.
 * Advantage of this class is, it can be used to measure the transaction timing
 * and to take advantage of any customisation.
 * Persistent unit name has to be configured to use this service.
 * @author Mahesh
 *
 * @param <T>
 */
public class BMTransactionServiceBean<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String PRESISTANCE_UNIT ="cevaldom";//// Configure the persistance unit name of the JPA
	private static final  Logger logger = LoggerFactory.getLogger(BMTransactionServiceBean.class);
	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory(PRESISTANCE_UNIT);
	private EntityManager em;
	
	private Class<T> entityClass;

	/**
	 * method used to start the transaction
	 */
	
	public void beginTransaction() {
		em = emf.createEntityManager();
		em.getTransaction().begin();
		logger.info("transaction begin");
	}
	
	/**
	 * method used to commit the transaction
	 */
	public void commit() {
		em.getTransaction().commit();
		logger.info("transaction commit");
	}

	/**
	 * method used to rollback the transaction
	 */
	public void rollback() {
		em.getTransaction().rollback();
		logger.info("transaction roolback");
	}

	/**
	 * method used to close the transaction
	 */
	public void closeTransaction() {
		em.close();
		logger.info("transaction close");
	}

	/**
	 * method used to commitAndClose the transaction
	 */
	public void commitAndCloseTransaction() {
		commit();
		closeTransaction();
		logger.info("transaction commit and close transaction");
	}

	/**
	 * method used to flush the transaction
	 */
	public void flush() {
		em.flush();
		logger.info("transaction flush");
	}

	/**
	 * method used to join the transaction
	 */
	public void joinTransaction() {
		em = emf.createEntityManager();
		em.joinTransaction();
	}

	/**
	 * getter method of entity 
	 */
	public BMTransactionServiceBean(Class<T> entityClass) {
		this.entityClass = entityClass;
		
	}

	/**
	 * method used to save the transaction
	 */
	public void save(T entity) {
		em.persist(entity);
		//loger.info("transaction save");
	}

	/**
	 * method used to delete the transaction
	 */
	public void delete(T entity) {
		T entityToBeRemoved = em.merge(entity);

		em.remove(entityToBeRemoved);
		//loger.info("transaction remove");
	}

	/**
	 * method used to update the transaction
	 */
	public T update(T entity) {
		//loger.info("transaction update");
		return em.merge(entity);
		
	}
	
	/**
	 * used to find the entity
	 */
	public T find(int entityID) {
		return em.find(entityClass, entityID);
	}

	/**
	 * used to findReferenceOnly  of the entity
	 */
	public T findReferenceOnly(int entityID) {
		return em.getReference(entityClass, entityID);
	}

	/**
	 * used to get the data that satisfies the given criteria
	 */
	// Using the unchecked because JPA does not have a
	// em.getCriteriaBuilder().createQuery()<T> method
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> findAll() {
		CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return em.createQuery(cq).getResultList();
	}

	// Using the unchecked because JPA does not have a
	// query.getSingleResult()<T> method
	@SuppressWarnings("unchecked")
	protected T findOneResult(String namedQuery, Map<String, Object> parameters) {
		T result = null;

		try {
			Query query = em.createNamedQuery(namedQuery);

			// Method that will populate parameters if they are passed not null and empty
			if (parameters != null && !parameters.isEmpty()) {
				populateQueryParameters(query, parameters);
			}

			result = (T) query.getSingleResult();

		} catch (NoResultException e) {
			logger.info("No result found for named query: " + namedQuery);
		} catch (Exception e) {
			logger.info("Error while running query: " + e.getMessage());
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * used to get the data that satisfies the given criteria
	 */
	private void populateQueryParameters(Query query, Map<String, Object> parameters) {
		for (Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
	}
}