package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum ModuleWarType
 * this enum belongs the distints
 * war deployed in the server. .
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/12/2012
 */
public enum AESKeyType {
	
	/** The security. */
	PRADERAKEY("AESArkinKey");
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new module war type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private AESKeyType(String value) {
		this.value = value;
	}
	
	/** The Constant list. */
	public static final List<AESKeyType> list = new ArrayList<AESKeyType>();
	
	/** The Constant lookup. */
	public static final Map<String, AESKeyType> lookup = new HashMap<String, AESKeyType>();
	static {
        for (AESKeyType d : AESKeyType.values()){
            lookup.put(d.getValue(), d);
        	list.add(d);
        }
    }
}