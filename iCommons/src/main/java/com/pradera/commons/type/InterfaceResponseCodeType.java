package com.pradera.commons.type;

public class InterfaceResponseCodeType {
	
	private Long idResponseCode;
	
	private String responseCode;
	
	private String responseCodeDpf;

	public Long getIdResponseCode() {
		return idResponseCode;
	}

	public void setIdResponseCode(Long idResponseCode) {
		this.idResponseCode = idResponseCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseCodeDpf() {
		return responseCodeDpf;
	}

	public void setResponseCodeDpf(String responseCodeDpf) {
		this.responseCodeDpf = responseCodeDpf;
	}
	
	

}
