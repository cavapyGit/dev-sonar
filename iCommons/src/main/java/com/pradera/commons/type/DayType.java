package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum Daytype.
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.0
 */
public enum DayType{

	MONDAY(Integer.valueOf(2),"LUNES"),
	TUESDAY(Integer.valueOf(3),"MARTES"),
	WEDNESDAY(Integer.valueOf(4),"MIERCOLES"),
	THURSDAY(Integer.valueOf(5),"JUEVES"),
	FRIDAY(Integer.valueOf(6),"VIERNES"),
	SATURDAY(Integer.valueOf(7),"SABADO"),
	SUNDAY(Integer.valueOf(1),"DOMINGO");
	
	private Integer code;
	private String value;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private DayType(int ordinal,String name) {
		this.code = ordinal;
		this.value = name;
	}
	public static final List<DayType> list = new ArrayList<DayType>();
	public static final Map<Integer, DayType> lookup = new HashMap<Integer, DayType>();

	static {
		for (DayType s : EnumSet.allOf(DayType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}