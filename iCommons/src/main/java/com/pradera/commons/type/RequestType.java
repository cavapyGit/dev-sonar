package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum InstitutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/02/2013
 */
public enum RequestType {
	/** The deposit. */
	DEPOSITEO(Integer.valueOf(673),"DEPOSITO"),
	/** The retiro. */
	RETIRO(Integer.valueOf(674),"RETIRO");
	
	/** The code. */
	private Integer code;
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	* Instantiates a new request type.
	 * @param code Integer
	 * @param value String
	 */
	private RequestType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/** The Constant list. */
	public static final List<RequestType> list = new ArrayList<RequestType>();
	/** The Constant lookup. */
	public static final Map<Integer, RequestType> lookup = new HashMap<Integer, RequestType>();
	static {
        for (RequestType d : RequestType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
	
	/**
	 * Gets the description
	 * @param locale Locale
	 * @return the description value
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	* Gets the.
	 * @param code Integer
	 * @return the RequestType object
	 */
	public static RequestType get(Integer code) {
		return lookup.get(code);
	}
}