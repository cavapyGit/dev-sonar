package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum InstitutionType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15/02/2013
 */
public enum InstitutionType {
	
	/** The depositary. */
	DEPOSITARY(Integer.valueOf(20),"CSDCORE"),
	
	/** The afp. */
	AFP(Integer.valueOf(47),"AFP"),
	
	/** The bcr. */
	BCR(Integer.valueOf(49),"BANCO CENTRAL"),

	/** The bolsa. */
	BOLSA(Integer.valueOf(50),"BOLSA DE VALORES"),
	
	/** The issuer. */
	ISSUER(Integer.valueOf(22),"EMISOR"),

	/** The min hac. */
	ASFI(Integer.valueOf(51),"ASFI"),
	
	/** The participant. */
	PARTICIPANT(Integer.valueOf(21),"DEPOSITANTE"),
	
	/** The issuer dpf. */
	ISSUER_DPF(Integer.valueOf(186),"EMISOR CDA"),
	
	/** The participant investor. */
	PARTICIPANT_INVESTOR(Integer.valueOf(187),"DEPOSITANTE INVERSIONISTA"),
	
	SUPERVISOR_FINANCIAL_AUTHORITY(Integer.valueOf(215),"AUTORIDAD DE SUPERVISION DEL SISTEMA FINANCIERO");
	
	/** The Constant list. */
	public static final List<InstitutionType> list = new ArrayList<InstitutionType>();
	
	/** The Constant listExternalInstitutions. */
	public static final List<InstitutionType> listExternalInstitutions = new ArrayList<InstitutionType>();
	
	/** The Constant listInternalInstitutions. */
	public static final List<InstitutionType> listInternalInstitutions = new ArrayList<InstitutionType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InstitutionType> lookup = new HashMap<Integer, InstitutionType>();
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InstitutionType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	static {
        for (InstitutionType d : InstitutionType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        	if(d.getCode().equals(DEPOSITARY.getCode())){
        		listInternalInstitutions.add(d);
        	}else{
        		listExternalInstitutions.add(d);
        	}	
        }
    }
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the institution type
	 */
	public static InstitutionType get(Integer code) {
		return lookup.get(code);
	}
	
}