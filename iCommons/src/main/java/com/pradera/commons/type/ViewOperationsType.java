/**@author mmacalupu
 * This Enum is useful to diferents actions in the view(*.xhtml)
 * the element of each register below to the actions in the priviligies
 * for example(Register,Modify,Delete,.. etc) and will put in all "GenericBaseBean.java"   
 */
package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ViewOperationsType {	
	
	REGISTER (1,"REGISTRAR"),
	MODIFY (2,"MODIFICAR"),
	CONSULT (3,"CONSULTAR"),
	BLOCK(4,"BLOQUEAR"),
	UNBLOCK (5,"DESBLOQUEAR"),
	DELETE(6,"ELIMINAR"), 
	DETAIL(7,"DETALLE"),
	CONFIRM(8,"CONFIRMAR"),
	REJECT(9,"RECHAZAR"),
	ANULATE(10,"ANULAR"),
	APPROVE(11,"APROBAR"),
	SUSPEND(12,"SUSPENDER"),
	OPENED(13,"APERTURADO"),
	SUSPEND_UP(14,"LEV SUSPENSION"),
	DEACTIVATE(15,"DEACTIVATE"),
	REVIEW(16,"REVISAR"),
	ACTIVATE(17,"ACTIVAR"), 
	UPDATE(18,"ACTUALIZAR"),
	CANCEL(19,"CANCELAR"),
	SETTLEMENT(20,"LIQUIDAR"),
	APPEAL(21,"APELAR"),
	AUTHORIZE(26,"AUTORIZAR");
	
	
	private Integer code;
	private String value;

	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private ViewOperationsType(int code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<ViewOperationsType> list = new ArrayList<ViewOperationsType>();
	public static final Map<Integer, ViewOperationsType> lookup = new HashMap<Integer, ViewOperationsType>();

	static {
		for (ViewOperationsType s : EnumSet.allOf(ViewOperationsType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static ViewOperationsType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * List some elements.
	 *
	 * @param transferSecuritiesTypeParams the transfer securities type params
	 * @return the list
	 */
	public static List<ViewOperationsType> listSomeElements(ViewOperationsType... transferSecuritiesTypeParams){
		List<ViewOperationsType> retorno = new ArrayList<ViewOperationsType>();
		for(ViewOperationsType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
}