package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum ModuleWarType
 * this enum belongs the distints
 * war deployed in the server. .
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/12/2012
 */
public enum ModuleWarType {
	
	/** The security. */
	SECURITY(1,"iSecurity"),
	
	/** The accounts. */
	ACCOUNTS(2,"iAccounts"),
	
	/** The custody. */
	CUSTODY(3,"iCustody"),
	
	/** The securities. */
	SECURITIES(4,"iSecurities"),
	
	/** The parameter. */
	PARAMETER(5,"iGeneralParameters"),
	
	/** The fund. */
//	FUND(6,"iFunds"),
	
	/** The corporate. */
	CORPORATE(7,"iCorporateEvents"),
	
	/** The negotiation. */
//	NEGOTIATION(8,"iNegotiations"),
	
	/** The settlement. */
	SETTLEMENT(9,"iSettlements"),
	
	/** The report. */
	REPORT(10,"iReports"),
	 
	 /** The Billing. */
	 BILLING(11,"iBilling"),
	 
	/** The business. */
	BUSINESS(12,"iBusinessIntegration"),
	
	COLLECTION_ECONOMIC(13,"iCollectionEconomic"),
	
	AUDIT(6,"iAuditProcess"),
	
	ACCOUNTING(8,"iAccounting");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new module war type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private ModuleWarType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}
	
	/** The Constant list. */
	public static final List<ModuleWarType> list = new ArrayList<ModuleWarType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ModuleWarType> lookup = new HashMap<Integer, ModuleWarType>();
	static {
        for (ModuleWarType d : ModuleWarType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
}