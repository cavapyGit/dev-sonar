package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2012.</li>
 * </ul>
 * 
 * The Enum ModuleWarType
 * this enum belongs the distints
 * war deployed in the server. .
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/12/2012
 */
public enum RequestMethodType {
	
	GET("GET"),
	POST("POST");
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new module war type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private RequestMethodType(String value) {
		this.value = value;
	}
	
	/** The Constant list. */
	public static final List<RequestMethodType> list = new ArrayList<RequestMethodType>();
	
	/** The Constant lookup. */
	public static final Map<String, RequestMethodType> lookup = new HashMap<String, RequestMethodType>();
	static {
        for (RequestMethodType d : RequestMethodType.values()){
            lookup.put(d.getValue(), d);
        	list.add(d);
        }
    }
}