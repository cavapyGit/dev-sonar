package com.pradera.commons.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum RegistryType {

	ORIGIN_OF_THE_REQUEST(Integer.valueOf(1095),"ORIGEN"),
	HISTORICAL_COPY(Integer.valueOf(1096),"HISTORIAL_COPIA");
	
	
	private Integer code;
	private String value;
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private RegistryType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public static final List<RegistryType> list = new ArrayList<RegistryType>();
	public static final Map<Integer, RegistryType> lookup = new HashMap<Integer, RegistryType>();
	static {
        for (RegistryType d : RegistryType.values()){
            lookup.put(d.getCode(), d);
        	list.add(d);
        }
    }
}