package com.pradera.commons.type;

import java.util.HashMap;
import java.util.Map;

public class InterfaceLabelType {

	private String interfaceName;
	
	private Map<String,String> mapInterfaceLabel = new HashMap<String, String>();

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public Map<String, String> getMapInterfaceLabel() {
		return mapInterfaceLabel;
	}

	public void setMapInterfaceLabel(Map<String, String> mapInterfaceLabel) {
		this.mapInterfaceLabel = mapInterfaceLabel;
	}
	
	
	
}
