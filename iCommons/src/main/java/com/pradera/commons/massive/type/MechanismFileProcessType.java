package com.pradera.commons.massive.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum MechanismFileProcessType {
	
	MCN_OPERATIONS_UPLOAD (new Integer(1401), "REGISTRO MASIVO DE MCN"),
	OTC_REGISTRATION_UPLOAD (new Integer(1402), "REGISTRO MASIVO DE OTC"),
	OTC_REVISION_UPLOAD (new Integer(1403), "REVISION MASIVA DE OTC"),
	OTC_REVISION_GENERATION (new Integer(1404), "GENERACION DE REVISION MASIVA DE OTC"),
	MCN_ASSIGNMENT_UPLOAD (new Integer(1897), "ASIGNACION MASIVA DE MCN");
			
	private Integer code;
	private String descripcion;
	
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private MechanismFileProcessType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public static final List<MechanismFileProcessType> list = new ArrayList<MechanismFileProcessType>();
	
	public static final Map<Integer, MechanismFileProcessType> lookup = new HashMap<Integer, MechanismFileProcessType>();
	
	static {
		for (MechanismFileProcessType s : EnumSet.allOf(MechanismFileProcessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
