package com.pradera.commons.massive.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum HolderMassiveFileProcessType {
	
	HOLDER_MASSIVE_UPLOAD (new Integer(2850), "REGISTRO MASIVO DE TITULARES");
			
	private Integer code;
	private String descripcion;
	
	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private HolderMassiveFileProcessType(Integer code, String descripcion) {
		this.code = code;
		this.descripcion = descripcion;
	}

	public static final List<HolderMassiveFileProcessType> list = new ArrayList<HolderMassiveFileProcessType>();
	
	public static final Map<Integer, HolderMassiveFileProcessType> lookup = new HashMap<Integer, HolderMassiveFileProcessType>();
	
	static {
		for (HolderMassiveFileProcessType s : EnumSet.allOf(HolderMassiveFileProcessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
