package com.pradera.commons.massive.type;

import java.io.Serializable;

public class OperationCorrectHeaderLabel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String referenceNumber = "OPERACI\u00d3N REF";
	private String referenceDate = "FECHA REF";
	private String operationNumber  = "OPERACI\u00d3N";
	private String isinCode  = "VALOR REGISTRADO";
	private String rowNumberRef = "NÚMERO DE LINEA";
	private String holderAccount = "NÚMERO DE CUENTA EN DEPOSITANTE";
	private String documentNumber = "DOCUMENTO DE TITULAR";
	private String holderAccountType="TIPO DE CUENTA TITULAR";
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	/**
	 * @return the referenceDate
	 */
	public String getReferenceDate() {
		return referenceDate;
	}
	/**
	 * @param referenceDate the referenceDate to set
	 */
	public void setReferenceDate(String referenceDate) {
		this.referenceDate = referenceDate;
	}
	/**
	 * @return the operationNumber
	 */
	public String getOperationNumber() {
		return operationNumber;
	}
	/**
	 * @param operationNumber the operationNumber to set
	 */
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	/**
	 * @return the isinCode
	 */
	public String getIsinCode() {
		return isinCode;
	}
	/**
	 * @param isinCode the isinCode to set
	 */
	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}
	public String getRowNumberRef() {
		return rowNumberRef;
	}
	public void setRowNumberRef(String rowNumberRef) {
		this.rowNumberRef = rowNumberRef;
	}
	public String getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getHolderAccountType() {
		return holderAccountType;
	}
	public void setHolderAccountType(String holderAccountType) {
		this.holderAccountType = holderAccountType;
	}

	
	
}
