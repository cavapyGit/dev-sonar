package com.pradera.commons.massive.type;

import java.io.Serializable;

public class OperationIncorrectHeaderLabel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String referenceNumber = "OPERACION REF";
	private String referenceDate = "FECHA REF";
	private String lineNumberRef = "LINEA OP";
	private String field  = "CAMPO";
	private String errorDescription  = "DESCRIPCION";
	private String rowNumberRef = "NÚMERO DE LINEA";
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	/**
	 * @return the referenceDate
	 */
	public String getReferenceDate() {
		return referenceDate;
	}
	/**
	 * @param referenceDate the referenceDate to set
	 */
	public void setReferenceDate(String referenceDate) {
		this.referenceDate = referenceDate;
	}
	/**
	 * @return the lineNumberRef
	 */
	public String getLineNumberRef() {
		return lineNumberRef;
	}
	/**
	 * @param lineNumberRef the lineNumberRef to set
	 */
	public void setLineNumberRef(String lineNumberRef) {
		this.lineNumberRef = lineNumberRef;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public String getRowNumberRef() {
		return rowNumberRef;
	}
	public void setRowNumberRef(String rowNumberRef) {
		this.rowNumberRef = rowNumberRef;
	}
	
	
	
}
