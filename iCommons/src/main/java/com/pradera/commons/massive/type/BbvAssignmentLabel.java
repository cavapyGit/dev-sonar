package com.pradera.commons.massive.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BbvAssignmentLabel {
	
	NUM("index"),	
	NRO_REGISTRO("referenceNumber"),
	FECHA("referenceDate"),
	MODALIDAD("operationClass"),
	PAPELETA("ballotNumber"),
	SECUENCIA("sequential"),
	CTA_MATRIZ("assignmentPartMnemonic"),
	OPERACION("operationType"),
	COMITENTE("accountNumber"),
	CANTIDAD("stockQuantity"),
	FECHAUH("marketDate"),
	TASAUH("marketRate"),
	PRECIOUH("marketPrice"),
	TIPO_VALOR_NEGOCIADO("securityClass"),
	VALOR_NEGOCIADO("securityCode"),
	VALOR_ORIGEN("sourceSecurityCode"),
	VALOR_RESULTANTE("targetSecurityCode")
	;
	
	BbvAssignmentLabel(String name){
		this.name=name;
	}
	
	String name;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static final List<BbvAssignmentLabel> list = new ArrayList<BbvAssignmentLabel>();
	
	/** The Constant lookup. */
	public static final Map<String, BbvAssignmentLabel> lookup = new HashMap<String, BbvAssignmentLabel>();
	static {
		for (BbvAssignmentLabel s : EnumSet.allOf(BbvAssignmentLabel.class)) {
			list.add(s);
			lookup.put(s.getName(), s);
		}
	}
	
	public static BbvAssignmentLabel get(String name) {
		return lookup.get(name);
	}
}