package com.pradera.commons.massive.type;

import java.io.Serializable;

public class AssignmentCorrectHeaderLabel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String operationNumber  = "OPERACION";
	private String ballotNumber  = "PAPELETA";
	private String sequential  = "SECUENCIA";
	private String accountNumber = "N\u00b0 CUENTA";
	private String quantity  = "CANTIDAD";
	
	public String getOperationNumber() {
		return operationNumber;
	}
	public void setOperationNumber(String operationNumber) {
		this.operationNumber = operationNumber;
	}
	public String getBallotNumber() {
		return ballotNumber;
	}
	public void setBallotNumber(String ballotNumber) {
		this.ballotNumber = ballotNumber;
	}
	public String getSequential() {
		return sequential;
	}
	public void setSequential(String sequential) {
		this.sequential = sequential;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
