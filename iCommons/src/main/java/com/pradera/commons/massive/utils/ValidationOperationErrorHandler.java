package com.pradera.commons.massive.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.beanio.BeanReaderErrorHandler;
import org.beanio.BeanReaderException;
import org.beanio.RecordContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.type.InterfaceLabelType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;

public class ValidationOperationErrorHandler implements BeanReaderErrorHandler, Serializable {
	
	private static final  Logger logger = LoggerFactory.getLogger(ValidationOperationErrorHandler.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<RecordValidationType> totalErrors;
	private Integer rejectedOperations = 0;
	private InterfaceLabelType interfaceLabel;

	public ValidationOperationErrorHandler(InterfaceLabelType interfaceLabel) {
		this.totalErrors = new ArrayList<RecordValidationType>();
		this.interfaceLabel = interfaceLabel;
	}
	
	@Override
	public void handleError(BeanReaderException e) {
		rejectedOperations++;
		RecordContext context = e.getRecordContext();
		if (context.hasRecordErrors()) {
			for (String error : context.getRecordErrors()) {
				RecordValidationType ref = new RecordValidationType();
				ref.setErrorDescription(error);
				ref.setLineNumber(context.getLineNumber());
				totalErrors.add(ref);
			}
		}
		if (context.hasFieldErrors()) {
			outerloop:
			for (String field : context.getFieldErrors().keySet()) {
				for (String error : context.getFieldErrors(field)) {
					
					Object label = null;
					if(Validations.validateIsNotNull(interfaceLabel) && Validations.validateIsNotNull(interfaceLabel.getMapInterfaceLabel())){
						label = interfaceLabel.getMapInterfaceLabel().get(field);
					}
					
					RecordValidationType fe = new RecordValidationType();
					fe.setField(label==null?GeneralConstants.EMPTY_STRING:label.toString());
					fe.setErrorSimpleDetail(error);
					fe.setLineNumber(context.getLineNumber());
					
					StringBuffer sbuffer = new StringBuffer();
					sbuffer.append("Linea: ").append(fe.getLineNumber());
					sbuffer.append(" Campo: ").append(fe.getField());
					sbuffer.append(" Error: ").append(fe.getErrorSimpleDetail());
					fe.setErrorDescription(sbuffer.toString());
					
					logger.error("validation error: " + fe.getErrorDescription());
					totalErrors.add(fe);
					break outerloop;
				}
			}
		}
	}
	
	/**
	 * @return the totalErrors
	 */
	public List<RecordValidationType> getTotalErrors() {
		return totalErrors;
	}

	/**
	 * @param totalErrors the totalErrors to set
	 */
	public void setTotalErrors(List<RecordValidationType> totalErrors) {
		this.totalErrors = totalErrors;
	}

	/**
	 * @return the rejectedOperations
	 */
	public int getRejectedOperations() {
		return rejectedOperations;
	}

	/**
	 * @param rejectedOperations the rejectedOperations to set
	 */
	public void setRejectedOperations(int rejectedOperations) {
		this.rejectedOperations = rejectedOperations;
	}


	public void setRejectedOperations(Integer rejectedOperations) {
		this.rejectedOperations = rejectedOperations;
	}

}
