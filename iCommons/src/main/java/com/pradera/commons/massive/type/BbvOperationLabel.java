package com.pradera.commons.massive.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum BbvOperationLabel {
	
	NUM("index"),	
	NRO_REGISTRO("referenceNumber"),
	FECHA("referenceDate"),
	CLASE("operationClass"),
	PAPELETA("ballotNumber"),
	SECUENCIA("sequential"),
	COMPRADOR("buyerPartMnemonic"),
	VENDEDOR("sellerPartMnemonic"),
	INSTRUMENTO("securityClassCode"),
	SERIE("securityCode"),
	PLAZO_RPT("termSettlementDays"),
	CANTIDAD("stockQuantity"),
	TASA("rate"),
	PRECIO("cashPrice"),
	PLAZO_LIQ("cashSettlementDays"),
	MONEDA("currencyCode"),
	LUGAR_LIQ("settlementPlace"),
	PAPELETA_ORIG("ballotNumberOri"),
	SECUENCIA_ORIG("sequentialOri"),
	VEND_ORIG("partMnemonicOri"),
	TASA_ORIG("amountRateOri"),
	PLAZO_ORIG("termSettlementDaysOri"),
	PRECIO_ORIG("cashPriceOri"),
	FLAG_SUBASTA("sellFlagStr"),
	;
	
	BbvOperationLabel(String name){
		this.name=name;
	}
	
	String name;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static final List<BbvOperationLabel> list = new ArrayList<BbvOperationLabel>();
	
	/** The Constant lookup. */
	public static final Map<String, BbvOperationLabel> lookup = new HashMap<String, BbvOperationLabel>();
	static {
		for (BbvOperationLabel s : EnumSet.allOf(BbvOperationLabel.class)) {
			list.add(s);
			lookup.put(s.getName(), s);
		}
	}
	
	public static BbvOperationLabel get(String name) {
		return lookup.get(name);
	}
}