package com.pradera.commons.view.validators;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
@FacesValidator("com.pradera.commons.view.validators.EmailValidator")
public class EmailValidator implements Validator{

	private Pattern pattern;
	private Matcher matcher;
	public static final String EMAIL_PATTERN="^[_A-Za-zñÑ0-9-\\+]+(\\.[_A-Za-zñÑ0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
	public EmailValidator(){
		pattern = Pattern.compile(EMAIL_PATTERN);
	}
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		Boolean skipEmailRequired=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipEmailRequired"));
		
		if(context.getPartialViewContext().isExecuteAll() || skipEmailRequired.equals(Boolean.TRUE)){
		
		if(value == null) {			
		
			Locale locale = context.getViewRoot().getLocale();

			Object[] parametros = null;
			if(component instanceof InputText){
				parametros = new Object[]{((InputText) component).getLabel()};
			}else if (component instanceof InputNumber){
				parametros = new Object[]{((InputNumber) component).getLabel()};
			}else if (component instanceof InputTextarea){
				parametros = new Object[]{((InputTextarea) component).getLabel()};
			}
			
			
			String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
					GeneralPropertiesConstants.ERROR_INPUT_TEXT, parametros);
			
			FacesMessage msg = 
					new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showRequiredValidationDialog();
				throw new ValidatorException(msg);
			
				
		 }
		}
		else{
			if(value!=null && !validate(value.toString())){
				
				Locale locale = context.getViewRoot().getLocale();

				Object[] parametros = null;
				
				String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
						GeneralPropertiesConstants.EMAIL_FORMAT, parametros);
				
				FacesMessage msg = 
						new FacesMessage(strMsg);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getValidationMessage(GeneralPropertiesConstants.EMAIL_FORMAT,null));
					JSFUtilities.showRequiredValidationDialog();
					throw new ValidatorException(msg);

				
			}
		}
		
	}
	
	public boolean validate(String value) {
		 
		matcher = pattern.matcher(value);
		return matcher.matches();
 
	}

}
