package com.pradera.commons.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
@FacesValidator("com.pradera.commons.view.validators.InputTextValidator")
public class InputTextValidator implements Validator{

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
	
	Boolean skipValidation=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipValidation"));		
			
		
	if(context.getPartialViewContext().isExecuteAll() || skipValidation.equals(Boolean.TRUE)){
		if(value == null) {			
		
			Locale locale = context.getViewRoot().getLocale();

			Object[] parametros = null;
			if(component instanceof InputText){
				parametros = new Object[]{((InputText) component).getLabel()};
			}else if (component instanceof InputNumber){
				parametros = new Object[]{((InputNumber) component).getLabel()};
			}else if (component instanceof InputTextarea){
				parametros = new Object[]{((InputTextarea) component).getLabel()};
			}else if (component instanceof InputTextarea){
				InputTextarea inputTextarea=(InputTextarea) component;
				parametros = new Object[]{((InputTextarea) component).getLabel()};
				inputTextarea.resetValue();
			}
			
			
			String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
					GeneralPropertiesConstants.ERROR_INPUT_TEXT, parametros);
			
			FacesMessage msg = 
					new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				JSFUtilities.showRequiredValidationDialog();
				throw new ValidatorException(msg);
			
				
		}
		
	}
	}

}
