package com.pradera.commons.view.config;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Specializes;

import org.apache.myfaces.extensions.cdi.jsf.api.config.ClientConfig;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DepositaryClientConfig.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-may-2015
 */
@Specializes
@SessionScoped
public class DepositaryClientConfig extends ClientConfig {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6237536378741121380L;

	/**
	 * Instantiates a new depositary client config.
	 */
	public DepositaryClientConfig() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.apache.myfaces.extensions.cdi.jsf.api.config.ClientConfig#getWindowHandlerResourceLocation()
	 */
	@Override
	public String getWindowHandlerResourceLocation() {
		// TODO Auto-generated method stub
		return "codi/windowhandler.html";
	}
}
