package com.pradera.commons.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.calendar.Calendar;

import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

@FacesValidator("calendarRequiredValidator")
public class CalendarRequiredValidator implements Validator{

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		Boolean skipValidation=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipValidation"));
		
		if(value == null && Boolean.FALSE.equals(skipValidation)) {			
		
			Locale locale = context.getViewRoot().getLocale();
			
			Calendar calendarDate = (Calendar) component;
			Object[] parametros = {calendarDate.getLabel() };
			
			String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
					GeneralPropertiesConstants.ERROR_FIELD_REQUIRED, parametros);
			
			FacesMessage msg = 
					new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
		
		}
	}

}
