package com.pradera.commons.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;

import javax.enterprise.event.Event;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.StageDependent;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.ui.UICompositeEvent;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.integration.common.validation.Validations;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GenericBaseBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/02/2013
 */
public abstract class GenericBaseBean implements Serializable {

	protected String allHolidays;
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The excepcion. */
	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;
	
	@Inject
	@UICompositeEvent(UICompositeType.MARKETFACT_BALANCE)
	private Event<Object> marketFactUIEvent;
	
	@Inject
	@UICompositeEvent(UICompositeType.SECURITY_FINANCIAL_DATA)
	private Event<Object> securityDataUIEvent;
	
	/** The server path. */
	@Inject @StageDependent
	protected String serverPath;
	
	/** The view operation type. */
	private Integer viewOperationType;
	
	/** The firs register data table. */
	protected int firsRegisterDataTable;
	/** The rows quantity data table. */
	protected int rowsQuantityDataTable;
	
	private String fUploadFileSizeDisplayWithComma="2,000";
	private String fUploadFileSize="3000000";
	private String fUploadFileDocumentsSize="3145728";
	private String fUploadFileLargeDocumentsSize="10485760";
	private String fUploadFileTypes="gif|jpg|jpeg|pdf|xml|png";
	private String fUploadFileDocumentsTypes="xls|xlsx|doc|docx|pdf";
	//protected Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	///protected Pattern fUploadFileTypesPattern=Pattern.compile("(?i)^[\\w\\s0-9.-]+\\.("+ fUploadFileTypes+")$");
	
	private String fUploadFileSizeDocumentsDisplay="3000";
	private String fUploadFileTypesDocumentsDisplay="*.xls, *.xlsx, *.docx, *.doc, *.pdf";	
	
	private String fUploadFileSizeDisplay="3000";
	private String fUploadFileTypesDisplay="*.gif, *.jpg, *.jpeg, *.pdf, *.xml";	
	
	private String pdfFileUploadFiltTypes="pdf";
	private String pdfFileUploadFileTypesDisplay="*.pdf";
	
	private String xmlFileUploadSize="10000000";
	private String xmlFileUploadFiltTypes="xml";
	private String xmlFileUploadFileTypesDisplay="*.xml";
	
	private String txtFileUploadSize="10000000";
	private String txtFileUploadTypes="txt";
	private String txtFileUploadSizeDisplay="10000";
	private String txtFileUploadTypesDisplay="*.txt";
	
	private String xlsFileUploadSize="10000000";
	private String xlsFileUploadFiltTypes="xlsx";
	private String xlsFileUploadFileTypesDisplay="*.xlsx";
	
	private String fUploadFileTypesImgTypes = "jpg|png|jpeg|pdf";
	private String fUploadFileTypesImgDisplay = "*.jpg, *.jpeg, *.png, .*pdf";
	
	private String fUploadFileTypesSignatureTypes = "pfx|p12";
	private String fUploadFileTypesSignatureDisplay = "*.pfx,*.p12";
	
	private String fUploadFileTypesImgPdfTypes = "jpg|png|jpeg|pdf|tiff";
	
	private String fUploadFileTypesImgPdfXlsTypes = "jpg|png|jpeg|pdf|tiff|xls|xlsx";

	private String fUploadFileTypesImgPdfDisplay = "*.jpg, *.jpeg, *.png, *.pdf";
	
	private String fUploadFileSizeImgDisplay = "512";
	private String fUploadFileSizeImg = "512000";
	
	private String localeEnUS="en-US";
	/** The Session idle Time. */
	@Inject
	@Configurable
	private Integer sessionIdleTime;
	
	/** The parameters table map. */
	private Map<Integer,Object> parametersTableMap;
	
	/**
	 * Begin conversation.	
	 */
	public void beginConversation() {
		//TODO remove in future
	}
	
	/**
	 * End conversation.
	 */
	public void endConversation() {
		//TODO remove in future
	}
	
	/**
	 * Gets the conversation id.
	 *
	 * @return the conversation id
	 */
	public String getConversationId() {
		return null;
	}
	
	/**
	 * Gets the conversation timeout.
	 *
	 * @return the conversation timeout
	 */
	public long getConversationTimeout() {
		return  0;
	}

	/**
	 * Instancia un nuevo conversacion bean.
	 */
	public GenericBaseBean() {
		// TODO Auto-generated constructor stub
		parametersTableMap = new HashMap<Integer,Object>();
	}
	
	/**
	 * Show message on dialog.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param headerData the header data
	 * @param keyBodyProperty the key body property
	 * @param bodyData the body data
	 */
	public static void showMessageOnDialog(String keyHeaderProperty,Object[] headerData,String keyBodyProperty, Object[] bodyData){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty,headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty,bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Show exception message.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param keyBodyProperty the key body property
	 */
	public static void showExceptionMessage(String keyHeaderProperty, Object[] headerData, String keyBodyProperty, Object[] bodyData){
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getExceptionMessage(keyHeaderProperty, headerData);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getExceptionMessage(keyBodyProperty, bodyData);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Show exception message.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param keyBodyProperty the key body property
	 */
	public static void showExceptionMessage(String keyHeaderProperty, String keyBodyProperty){
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getExceptionMessage(keyHeaderProperty);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getExceptionMessage(keyBodyProperty);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Show message on dialog only key.
	 *
	 * @param keyHeaderProperty the key header property
	 * @param keyBodyProperty the key body property
	 */
	public static void showMessageOnDialogOnlyKey(String keyHeaderProperty,String keyBodyProperty){
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();	
		String headerMessageConfirmation = "";
		if(keyHeaderProperty!=null && !keyHeaderProperty.isEmpty()){
			headerMessageConfirmation = PropertiesUtilities.getMessage(locale, keyHeaderProperty);
		}
		String bodyMessageConfirmation = "";
		if(keyBodyProperty!=null && !keyBodyProperty.isEmpty()){
			bodyMessageConfirmation = PropertiesUtilities.getMessage(locale, keyBodyProperty);
		}		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Execute js function and show message.
	 * method to execute javascript function and put message on Dialog
	 * @param keyHeaderProperty the key header property
	 * @param keyBodyProperty the key body property
	 * @param jsFunction the js function
	 */
	public static void executeJsFunctionAndShowMessage(String keyHeaderProperty,String keyBodyProperty, String jsFunction){
		showMessageOnDialogOnlyKey(keyHeaderProperty, keyBodyProperty);
		JSFUtilities.executeJavascriptFunction(jsFunction);
	}
	
	/**
	 * Show message on dialog.
	 *
	 * @param headerMessageConfirmation the header message confirmation
	 * @param bodyMessageConfirmation the body message confirmation
	 */
	public static void showMessageOnDialog(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	/**
	 * Change page datatable.
	 *
	 * @param pageEvent the page event
	 */
	public void changePageDatatable (PageEvent pageEvent) {
		DataTable dtb = (DataTable) pageEvent.getSource();
		rowsQuantityDataTable = dtb.getRows();
		firsRegisterDataTable = pageEvent.getPage() * dtb.getRows(); 	      
	}
	
	public String fUploadValidateFile(UploadedFile fileUploaded, Matcher matcher, String maxSize){
		return fUploadValidateFile(fileUploaded, matcher, maxSize,null);
	}
	
	public String fUploadValidate(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize, String fUploadFileTypes){
		return fUploadValidateFile(fileUploaded, matcher, maxSize,fUploadFileTypes);
	}
	
	
	public String fUploadValidateFile(UploadedFile fileUploaded, Matcher matcher, String maxSize,String fUploadFileTypes){
		String displayName=null;
		String extensionFile=null;
		try{
			extensionFile= CommonsUtilities.getTrueExtensionFileUpload(fileUploaded.getInputstream());
		}catch(IOException ioex){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
//		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().lastIndexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getfUploadFileSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		if(fUploadFileTypes == null){
			fUploadFileTypes = this.fUploadFileTypes;
		}
		if (!fUploadFileTypes.contains(extensionFile) && matcherToUser==null) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	public String fUploadValidateFileNameLength(String fileName){
		return fUploadValidateFileNameLength(fileName, null);
	}
	
	public String fUploadValidateFileNameLength(String fileName, Integer maxLength){
		String fileNameToUse = null;
		
		int maxLengthToUse = 30;
		
		if(Validations.validateIsNotNull(maxLength)){
			maxLengthToUse = maxLength;
		}
		
		if(fileName.length() > maxLengthToUse) {
			String extensionFile = fileName.toLowerCase().substring(fileName.lastIndexOf(".")+1,fileName.length());
			
			//get the name without extension
			StringBuilder sbName = new StringBuilder(fileName.substring(0,maxLengthToUse-4));
			//concatenate dot and extensionFile
			sbName.append(".").append(extensionFile);
			
			fileNameToUse = sbName.toString();
		} else {
			fileNameToUse = fileName;
		}
		
		return fileNameToUse;
	}
	
	public String fUploadValidateFileXml(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		return fUploadValidateXmlFile(fileUploaded,dialog, matcher, maxSize);
	}
	public String fUploadValidateXmlFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().lastIndexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getXmlFileUploadSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			JSFUtilities.showMessageOnDialog(null, null,
					GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (!xmlFileUploadFiltTypes.contains(extensionFile) && matcherToUser==null) {
			JSFUtilities.showMessageOnDialog(null, null,GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			JSFUtilities.showMessageOnDialog(null, null,
					GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	public String fUploadValidateXlsFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().lastIndexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getXlsFileUploadSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			JSFUtilities.showMessageOnDialog(null, null,
					GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (!xlsFileUploadFiltTypes.contains(extensionFile) && matcherToUser==null) {
			JSFUtilities.showMessageOnDialog(null, null,GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			JSFUtilities.showMessageOnDialog(null, null,
					GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	public String fUploadValidateTxtFile(UploadedFile fileUploaded,String dialog, Matcher matcher, String maxSize){
		String displayName=null;
		String extensionFile = fileUploaded.getFileName().toLowerCase().substring(fileUploaded.getFileName().lastIndexOf(".")+1,fileUploaded.getFileName().length());
		Matcher matcherToUser= null;
		String maxSizeToUse=getTxtFileUploadSize();
		
		if(matcher!=null){
			matcherToUser=matcher;
		}
		if(maxSize!=null){
			maxSizeToUse=maxSize;
		}
		
		if (matcherToUser!=null && !matcherToUser.matches()) {
			JSFUtilities.showMessageOnDialog(null, null,
					GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (!txtFileUploadTypes.contains(extensionFile) && matcherToUser==null) {
			JSFUtilities.showMessageOnDialog(null, null,GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE, null);
			JSFUtilities.executeJavascriptFunction(dialog);
			return null;
		}
		
		if (fileUploaded.getSize() > Integer.valueOf(maxSizeToUse)) {
			JSFUtilities.showMessageOnDialog(null, null,
					GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_SIZE, null);
			JSFUtilities
					.executeJavascriptFunction(dialog);
			return null;
		}
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			displayName=sbFileName.toString();
		}else{
			displayName=fileUploaded.getFileName();
		}
		return displayName;
	}
	
	
	public StreamedContent getStreamedContentFromFile(byte[] file,String fileType, String fileName) throws IOException {
		
		if(file!=null){
			InputStream inputStream = new ByteArrayInputStream(file);
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, fileType, fileName);
			inputStream.close();

			return streamedContentFile;
		} else {
			return null;
		}
		
	}
	
	public BigDecimal getByteArgLenghtKB(byte[] argByte){
		BigDecimal result=null;
		if(argByte!=null && argByte.length>0){
			result=BigDecimal.valueOf(argByte.length/1024);
		}
		return result;
	}
	
	public boolean isViewOperationUpdate(){
		return this.getViewOperationType().equals(ViewOperationsType.UPDATE.getCode());
	}
	
	/**
	 * Checks if is view operation register.
	 *
	 * @return true, if is view operation register
	 */
	public boolean isViewOperationRegister(){
		return this.getViewOperationType().equals(ViewOperationsType.REGISTER.getCode());
	}	
		
	
	/**
	 * Checks if is view operation modify.
	 *
	 * @return true, if is view operation modify
	 */
	public boolean isViewOperationModify(){
		return this.getViewOperationType().equals(ViewOperationsType.MODIFY.getCode());
	}
	
	public boolean isViewOperationActive(){
		return this.getViewOperationType().equals(ViewOperationsType.ACTIVATE.getCode());
	}
	
	public boolean isViewOperationDeactive(){
		return this.getViewOperationType().equals(ViewOperationsType.DEACTIVATE.getCode());
	}
	
	/**
	 * Checks if is view operation detail.
	 *
	 * @return true, if is view operation detail
	 */
	public boolean isViewOperationDetail(){
		return this.getViewOperationType().equals(ViewOperationsType.DETAIL.getCode());
	}
	
	/**
	 * Checks if is view operation block.
	 *
	 * @return true, if is view operation block
	 */
	public boolean isViewOperationBlock(){
		return this.getViewOperationType().equals(ViewOperationsType.BLOCK.getCode());
	}	
	
	/**
	 * Checks if is view operation un block.
	 *
	 * @return true, if is view operation un block
	 */
	public boolean isViewOperationUnBlock(){
		return this.getViewOperationType().equals(ViewOperationsType.UNBLOCK.getCode());
	}
	
	/**
	 * Checks if is view operation consult.
	 *
	 * @return true, if is view operation consult
	 */
	public boolean isViewOperationConsult(){
		return this.getViewOperationType().equals(ViewOperationsType.CONSULT.getCode());
	}
	
	/**
	 * Checks if is view operation delete.
	 *
	 * @return true, if is view operation delete
	 */
	public boolean isViewOperationDelete(){
		return this.getViewOperationType().equals(ViewOperationsType.DELETE.getCode());
	}
	
	/**
	 * Checks if is view operation settlement.
	 *
	 * @return true, if is view operation settlement
	 */
	public boolean isViewOperationSettlement(){
		return this.getViewOperationType().equals(ViewOperationsType.SETTLEMENT.getCode());
	}
	
	/**
	 * Checks if is view operation cancel.
	 *
	 * @return true, if is view operation cancel
	 */
	public boolean isViewOperationCancel(){
		return this.getViewOperationType().equals(ViewOperationsType.CANCEL.getCode());
	}
	
	/**
	 * Checks if is view operation approve.
	 *
	 * @return true, if is view operation approve
	 */
	public boolean isViewOperationApprove(){
		return this.getViewOperationType().equals(ViewOperationsType.APPROVE.getCode());
	}
	
	/**
	 * Checks if is view operation review.
	 *
	 * @return true, if is view operation review
	 */
	public boolean isViewOperationReview(){
		return this.getViewOperationType().equals(ViewOperationsType.REVIEW.getCode());
	}
	
	/**
	 * Checks if is view operation confirm.
	 *
	 * @return true, if is view operation confirm
	 */
	public boolean isViewOperationConfirm(){
		return this.getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode());
	}
	
	/**
	 * Checks if is reject.
	 *
	 * @return true, if is reject
	 */
	public boolean isViewOperationReject(){
		return this.getViewOperationType().equals(ViewOperationsType.REJECT.getCode());
	}
	
	/**
	 * Checks if is annul.
	 *
	 * @return true, if is annul
	 */
	public boolean isViewOperationAnnul(){
		return this.getViewOperationType().equals(ViewOperationsType.ANULATE.getCode());
	}
	
	public boolean isViewOperationAppeal(){
		return this.getViewOperationType().equals(ViewOperationsType.APPEAL.getCode());
	}
	
	public boolean isViewOperationAuthorize(){
		return this.getViewOperationType().equals(ViewOperationsType.AUTHORIZE.getCode());
	}

	/**
	 * Gets the view operation type.
	 *
	 * @return the view operation type
	 */
	public Integer getViewOperationType() {
		return viewOperationType;
	}

	/**
	 * Sets the view operation type.
	 *
	 * @param viewOperationType the new view operation type
	 */
	public void setViewOperationType(Integer viewOperationType) {
		this.viewOperationType = viewOperationType;
	}
	
	/**
	 * Execute action.
	 */
	public void executeAction(){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
	}
	
	/**
	 * Gets the max enabled date.
	 *
	 * @return the max enabled date
	 */
	public Date getCurrentSystemDate(){
		return CommonsUtilities.currentDate();
	}
	
	public Date getTomorrowSystemtDate(){
		return CommonsUtilities.addDate(CommonsUtilities.currentDate(), 1) ;
	}
	
	public Date getYesterdaySystemDate(){
		return CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1) ;
	}
	
	public Date getAlterDateDays(Date objDate, int daysAdd) {
		if(objDate!=null){
			return CommonsUtilities.addDate(objDate,daysAdd);
		}else{
			return null;
		}
		
	}
	
	public Integer getDaysBetween(Date startDate, Date endDate){
		Integer days=CommonsUtilities.getDaysBetween(startDate, endDate);
		return days;
	}
	
	public List<String> getIsSubsidiary(UserInfo userInfo,List<OperationUserTO> lstOperationUser){
		List<String> lstOperations = new ArrayList<String>();
		if(userInfo.getUserAgencies().size() > 0){
			for(OperationUserTO objOperationUser : lstOperationUser){
				if(!userInfo.getUserAgencies().containsKey(objOperationUser.getUserName())){
					lstOperations.add(objOperationUser.getOperNumber());
				}
			}
		}
		return lstOperations;
	}
	
	/**
	 * Validate is user Operator Institution.
	 *
	 * @return true, if successful
	 */
	public boolean getIsOperator(UserInfo userInfo){
		boolean flagOperator = false;
		if(Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIsOperator()) && 
				userInfo.getUserAccountSession().getIsOperator().equals(GeneralConstants.ONE_VALUE_INTEGER)){
			flagOperator = true;
		}
		return flagOperator;
	}
	
	/**
	 * Validate is issuer DPF Institution.
	 *
	 * @return true, if successful
	 */
	public Long getIssuerDpfInstitution(UserInfo userInfo) {
		Long idParticipantCode = null;	
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			return userInfo.getUserAccountSession().getPartIssuerCode();						
		}		
		return idParticipantCode;
	}
	

	/**
	 * Gets the firs register data table.
	 *
	 * @return the firs register data table
	 */
	public int getFirsRegisterDataTable() {
		return firsRegisterDataTable;
	}

	/**
	 * Sets the firs register data table.
	 *
	 * @param firsRegisterDataTable the new firs register data table
	 */
	public void setFirsRegisterDataTable(int firsRegisterDataTable) {
		this.firsRegisterDataTable = firsRegisterDataTable;
	}

	public String getfUploadFileSize() {
		return fUploadFileSize;
	}

	public void setfUploadFileSize(String fUploadFileSize) {
		this.fUploadFileSize = fUploadFileSize;
	}

	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	public String getfUploadFileSizeDisplay() {
		return fUploadFileSizeDisplay;
	}

	public void setfUploadFileSizeDisplay(String fUploadFileSizeDisplay) {
		this.fUploadFileSizeDisplay = fUploadFileSizeDisplay;
	}

	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}
	
	public String getXmlFileUploadSize() {
		return xmlFileUploadSize;
	}

	public void setXmlFileUploadSize(String xmlFileUploadSize) {
		this.xmlFileUploadSize = xmlFileUploadSize;
	}

	public String getXmlFileUploadFiltTypes() {
		return xmlFileUploadFiltTypes;
	}

	public void setXmlFileUploadFiltTypes(String xmlFileUploadFiltTypes) {
		this.xmlFileUploadFiltTypes = xmlFileUploadFiltTypes;
	}

	public String getXmlFileUploadFileTypesDisplay() {
		return xmlFileUploadFileTypesDisplay;
	}

	public void setXmlFileUploadFileTypesDisplay(
			String xmlFileUploadFileTypesDisplay) {
		this.xmlFileUploadFileTypesDisplay = xmlFileUploadFileTypesDisplay;
	}
	
	
	public String getXlsFileUploadSize() {
		return xlsFileUploadSize;
	}

	public void setXlsFileUploadSize(String xlsFileUploadSize) {
		this.xlsFileUploadSize = xlsFileUploadSize;
	}

	public String getXlsFileUploadFiltTypes() {
		return xlsFileUploadFiltTypes;
	}

	public void setXlsFileUploadFiltTypes(String xlsFileUploadFiltTypes) {
		this.xlsFileUploadFiltTypes = xlsFileUploadFiltTypes;
	}

	public String getXlsFileUploadFileTypesDisplay() {
		return xlsFileUploadFileTypesDisplay;
	}

	public void setXlsFileUploadFileTypesDisplay(
			String xlsFileUploadFileTypesDisplay) {
		this.xlsFileUploadFileTypesDisplay = xlsFileUploadFileTypesDisplay;
	}
	
	public String getTxtFileUploadSize() {
		return txtFileUploadSize;
	}

	public void setTxtFileUploadSize(String txtFileUploadSize) {
		this.txtFileUploadSize = txtFileUploadSize;
	}

	public String getTxtFileUploadTypes() {
		return txtFileUploadTypes;
	}

	public void setTxtFileUploadTypes(String txtFileUploadTypes) {
		this.txtFileUploadTypes = txtFileUploadTypes;
	}

	public String getTxtFileUploadSizeDisplay() {
		return txtFileUploadSizeDisplay;
	}

	public void setTxtFileUploadSizeDisplay(String txtFileUploadSizeDisplay) {
		this.txtFileUploadSizeDisplay = txtFileUploadSizeDisplay;
	}

	public String getTxtFileUploadTypesDisplay() {
		return txtFileUploadTypesDisplay;
	}

	public void setTxtFileUploadTypesDisplay(String txtFileUploadTypesDisplay) {
		this.txtFileUploadTypesDisplay = txtFileUploadTypesDisplay;
	}

	public String getAllHolidays() {
		return allHolidays;
	}

	public void setAllHolidays(String allHolidays) {
		this.allHolidays = allHolidays;
	}

	public int getRowsQuantityDataTable() {
		return rowsQuantityDataTable;
	}

	public void setRowsQuantityDataTable(int rowsQuantityDataTable) {
		this.rowsQuantityDataTable = rowsQuantityDataTable;
	}

	/**
	 * @return the sessionIdleTime
	 */
	public Integer getSessionIdleTime() {
		return sessionIdleTime;
	}

	/**
	 * @param sessionIdleTime the sessionIdleTime to set
	 */
	public void setSessionIdleTime(Integer sessionIdleTime) {
		this.sessionIdleTime = sessionIdleTime;
	}

	/**
	 * Gets the locale en us.
	 *
	 * @return the locale en us
	 */
	public String getLocaleEnUS() {
		return localeEnUS;
	}

	/**
	 * Sets the locale en us.
	 *
	 * @param localeEnUS the new locale en us
	 */
	public void setLocaleEnUS(String localeEnUS) {
		this.localeEnUS = localeEnUS;
	}

	public String getfUploadFileSizeDisplayWithComma() {
		return fUploadFileSizeDisplayWithComma;
	}

	public void setfUploadFileSizeDisplayWithComma(
			String fUploadFileSizeDisplayWithComma) {
		this.fUploadFileSizeDisplayWithComma = fUploadFileSizeDisplayWithComma;
	}

	public void showUIComponent(Integer uiComposityType, Object parameter){
		switch(UICompositeType.get(uiComposityType)){
			case MARKETFACT_BALANCE: marketFactUIEvent.fire(parameter);break;
			case SECURITY_FINANCIAL_DATA: securityDataUIEvent.fire(parameter);break;
			default: return;
		}
	}

	public String getPdfFileUploadFiltTypes() {
		return pdfFileUploadFiltTypes;
	}

	public void setPdfFileUploadFiltTypes(String pdfFileUploadFiltTypes) {
		this.pdfFileUploadFiltTypes = pdfFileUploadFiltTypes;
	}

	public String getPdfFileUploadFileTypesDisplay() {
		return pdfFileUploadFileTypesDisplay;
	}

	public void setPdfFileUploadFileTypesDisplay(
			String pdfFileUploadFileTypesDisplay) {
		this.pdfFileUploadFileTypesDisplay = pdfFileUploadFileTypesDisplay;
	}

	public String getfUploadFileSizeDocumentsDisplay() {
		return fUploadFileSizeDocumentsDisplay;
	}

	public void setfUploadFileSizeDocumentsDisplay(
			String fUploadFileSizeDocumentsDisplay) {
		this.fUploadFileSizeDocumentsDisplay = fUploadFileSizeDocumentsDisplay;
	}

	public String getfUploadFileTypesDocumentsDisplay() {
		return fUploadFileTypesDocumentsDisplay;
	}

	public void setfUploadFileTypesDocumentsDisplay(
			String fUploadFileTypesDocumentsDisplay) {
		this.fUploadFileTypesDocumentsDisplay = fUploadFileTypesDocumentsDisplay;
	}

	public String getfUploadFileDocumentsTypes() {
		return fUploadFileDocumentsTypes;
	}

	public void setfUploadFileDocumentsTypes(String fUploadFileDocumentsTypes) {
		this.fUploadFileDocumentsTypes = fUploadFileDocumentsTypes;
	}

	public String getfUploadFileDocumentsSize() {
		return fUploadFileDocumentsSize;
	}

	public void setfUploadFileDocumentsSize(String fUploadFileDocumentsSize) {
		this.fUploadFileDocumentsSize = fUploadFileDocumentsSize;
	}

	public Map<Integer, Object> getParametersTableMap() {
		return parametersTableMap;
	}

	public void setParametersTableMap(Map<Integer, Object> parametersTableMap) {
		this.parametersTableMap = parametersTableMap;
	}

	/**
	 * @return the fUploadFileTypesImgDisplay
	 */
	public String getfUploadFileTypesImgDisplay() {
		return fUploadFileTypesImgDisplay;
	}

	/**
	 * @param fUploadFileTypesImgDisplay the fUploadFileTypesImgDisplay to set
	 */
	public void setfUploadFileTypesImgDisplay(String fUploadFileTypesImgDisplay) {
		this.fUploadFileTypesImgDisplay = fUploadFileTypesImgDisplay;
	}

	/**
	 * @return the fUploadFileTypesImgTypes
	 */
	public String getfUploadFileTypesImgTypes() {
		return fUploadFileTypesImgTypes;
	}

	/**
	 * @param fUploadFileTypesImgTypes the fUploadFileTypesImgTypes to set
	 */
	public void setfUploadFileTypesImgTypes(String fUploadFileTypesImgTypes) {
		this.fUploadFileTypesImgTypes = fUploadFileTypesImgTypes;
	}

	/**
	 * @return the fUploadFileSizeImgDisplay
	 */
	public String getfUploadFileSizeImgDisplay() {
		return fUploadFileSizeImgDisplay;
	}

	/**
	 * @param fUploadFileSizeImgDisplay the fUploadFileSizeImgDisplay to set
	 */
	public void setfUploadFileSizeImgDisplay(String fUploadFileSizeImgDisplay) {
		this.fUploadFileSizeImgDisplay = fUploadFileSizeImgDisplay;
	}

	/**
	 * @return the fUploadFileSizeImg
	 */
	public String getfUploadFileSizeImg() {
		return fUploadFileSizeImg;
	}

	/**
	 * @param fUploadFileSizeImg the fUploadFileSizeImg to set
	 */
	public void setfUploadFileSizeImg(String fUploadFileSizeImg) {
		this.fUploadFileSizeImg = fUploadFileSizeImg;
	}

	/**
	 * @return the fUploadFileTypesImgPdfTypes
	 */
	public String getfUploadFileTypesImgPdfTypes() {
		return fUploadFileTypesImgPdfTypes;
	}

	/**
	 * @param fUploadFileTypesImgPdfTypes the fUploadFileTypesImgPdfTypes to set
	 */
	public void setfUploadFileTypesImgPdfTypes(String fUploadFileTypesImgPdfTypes) {
		this.fUploadFileTypesImgPdfTypes = fUploadFileTypesImgPdfTypes;
	}

	/**
	 * @return the fUploadFileTypesImgPdfDisplay
	 */
	public String getfUploadFileTypesImgPdfDisplay() {
		return fUploadFileTypesImgPdfDisplay;
	}

	/**
	 * @param fUploadFileTypesImgPdfDisplay the fUploadFileTypesImgPdfDisplay to set
	 */
	public void setfUploadFileTypesImgPdfDisplay(
			String fUploadFileTypesImgPdfDisplay) {
		this.fUploadFileTypesImgPdfDisplay = fUploadFileTypesImgPdfDisplay;
	}

	public String getfUploadFileTypesSignatureTypes() {
		return fUploadFileTypesSignatureTypes;
	}

	public void setfUploadFileTypesSignatureTypes(
			String fUploadFileTypesSignatureTypes) {
		this.fUploadFileTypesSignatureTypes = fUploadFileTypesSignatureTypes;
	}

	public String getfUploadFileTypesSignatureDisplay() {
		return fUploadFileTypesSignatureDisplay;
	}

	public void setfUploadFileTypesSignatureDisplay(
			String fUploadFileTypesSignatureDisplay) {
		this.fUploadFileTypesSignatureDisplay = fUploadFileTypesSignatureDisplay;
	}

	public String getfUploadFileLargeDocumentsSize() {
		return fUploadFileLargeDocumentsSize;
	}

	public void setfUploadFileLargeDocumentsSize(String fUploadFileLargeDocumentsSize) {
		this.fUploadFileLargeDocumentsSize = fUploadFileLargeDocumentsSize;
	}
	
	public String getfUploadFileTypesImgPdfXlsTypes() {
		return fUploadFileTypesImgPdfXlsTypes;
	}

	public void setfUploadFileTypesImgPdfXlsTypes(String fUploadFileTypesImgPdfXlsTypes) {
		this.fUploadFileTypesImgPdfXlsTypes = fUploadFileTypesImgPdfXlsTypes;
	}
	
}