package com.pradera.commons.view.component.exporter;

import java.awt.Color;
import java.io.FileOutputStream;

import com.pradera.commons.utils.GeneralConstants;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

public class HeaderPdfPageEvent extends PdfPageEventHelper {

	private String title;

	private String userName;

	private String date;

	private PdfPTable table;
	/** A template that will hold the total number of pages. */
	private PdfTemplate tpl;
	
	private Font fontTitleBold;
	
	private Font fontHeaderNormal;
	
	private Image imagenLogo;

	public HeaderPdfPageEvent(String title, String userName, String date) {
		super();
		this.title = title;
		this.userName = userName;
		this.date = date;
	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {
		try{
		tpl.setFontAndSize(BaseFont.createFont(BaseFont.COURIER, BaseFont.WINANSI, false), 8);
		ColumnText.showTextAligned(tpl, Element.ALIGN_LEFT,
				new Phrase(String.valueOf(writer.getPageNumber() - 1),fontHeaderNormal), 2, 8, 0);
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		// Table 3 columns
		table = new PdfPTable(3);
		try {
			float[] widths = { 0.5f, 4f, 0.7f };
			table.setWidths(widths);
			table.setTotalWidth(765);
			table.setLockedWidth(true);
			table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			// Title
			Paragraph titleHeader = new Paragraph(title, fontTitleBold);
			titleHeader.setAlignment(Element.ALIGN_CENTER);
			titleHeader.setSpacingBefore(0);
			titleHeader.setSpacingAfter(0);
			// Date
			Paragraph dateHeader = new Paragraph(date, fontHeaderNormal);
			titleHeader.setAlignment(Element.ALIGN_LEFT);
			titleHeader.setSpacingBefore(0);
			titleHeader.setSpacingAfter(0);
			// userName
			Paragraph userHeader = new Paragraph(userName, fontHeaderNormal);
			titleHeader.setAlignment(Element.ALIGN_LEFT);
			titleHeader.setSpacingBefore(0);
			titleHeader.setSpacingAfter(0);
			// Confidencial
			Paragraph clasificationHeader = new Paragraph("CONFIDENCIAL", fontHeaderNormal);
			titleHeader.setAlignment(Element.ALIGN_LEFT);
			titleHeader.setSpacingBefore(0);
			titleHeader.setSpacingAfter(0);
			// pagination
			Paragraph paginationHeader = new Paragraph("PÁGINA ", fontHeaderNormal);
			titleHeader.setAlignment(Element.ALIGN_RIGHT);
			titleHeader.setSpacingBefore(0);
			titleHeader.setSpacingAfter(0);
			//current page
			Paragraph paginationCurrent = new Paragraph(String.format("%d/", writer.getPageNumber()), fontHeaderNormal);
			titleHeader.setAlignment(Element.ALIGN_RIGHT);
			titleHeader.setSpacingBefore(0);
			titleHeader.setSpacingAfter(0);
			
			// First column
			PdfPTable tableLogo = new PdfPTable(1);
			tableLogo.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			PdfPCell cellLogo = null;
			// tableLogo = new PdfPCell(imagenLogo,true);
			cellLogo = new PdfPCell(imagenLogo,true);
			cellLogo.setBorder(0);
			cellLogo.setPadding(0);
			tableLogo.addCell(cellLogo);
			table.addCell(tableLogo);
			// Second column
			PdfPTable tableTitle = new PdfPTable(1);
			tableTitle.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			PdfPCell cellHeader = null;
			cellHeader = new PdfPCell(new Phrase("  ",
					new Font(Font.COURIER, 8)));
			cellHeader.setBorder(0);
			cellHeader.setPadding(0);
			tableTitle.addCell(cellHeader);
			cellHeader = new PdfPCell(titleHeader);
			cellHeader.setBorder(Rectangle.NO_BORDER);
			cellHeader.setPadding(0);
			cellHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellHeader.setVerticalAlignment(Element.ALIGN_BOTTOM);
			tableTitle.addCell(cellHeader);
			table.addCell(tableTitle);
			// Third column
			PdfPTable tableInfoReport = new PdfPTable(1);
			tableInfoReport.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cellHeader = new PdfPCell(dateHeader);
			cellHeader.setBorder(0);
			cellHeader.setPadding(0);
			tableInfoReport.addCell(cellHeader);
			cellHeader = new PdfPCell(userHeader);
			cellHeader.setBorder(0);
			cellHeader.setPadding(0);
			tableInfoReport.addCell(cellHeader);
			//clasification
			cellHeader = new PdfPCell(clasificationHeader);
			cellHeader.setBorder(0);
			cellHeader.setPadding(0);
			tableInfoReport.addCell(cellHeader);
			PdfPTable tablePagination = new PdfPTable(3);
			tablePagination.setWidths(new int[]{4,3,3});
//			tablePagination.setTotalWidth(30);
			tablePagination.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			cellHeader = new PdfPCell(paginationHeader);
			cellHeader.setBorder(Rectangle.NO_BORDER);
			cellHeader.setPadding(0);
			cellHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
			tablePagination.addCell(cellHeader);
			//current page
			cellHeader = new PdfPCell(paginationCurrent);
			cellHeader.setBorder(Rectangle.NO_BORDER);
			cellHeader.setPadding(0);
			cellHeader.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tablePagination.addCell(cellHeader);
			//TOTAL PAGES
			cellHeader = new PdfPCell(Image.getInstance(tpl));
			cellHeader.setBorder(Rectangle.NO_BORDER);
			cellHeader.setVerticalAlignment(Element.ALIGN_TOP);
			tablePagination.addCell(cellHeader);
			tableInfoReport.addCell(tablePagination);
			table.addCell(tableInfoReport);
			table.writeSelectedRows(0, -1,20, document.getPageSize().getHeight() - 15, writer.getDirectContent());
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {
		try {
			// initialization of the template
			tpl = writer.getDirectContent().createTemplate(30,16);
//			tpl.setBoundingBox(new Rectangle(-20, -20, 100, 100));
			fontTitleBold = new Font(Font.HELVETICA, 14, Font.BOLD,
					Color.BLACK);
			fontHeaderNormal = new Font(Font.COURIER, 8, Font.NORMAL,
					Color.BLACK);
			
			imagenLogo = Image.getInstance(this.getClass().getResource(GeneralConstants.IMAGE_EXPORT_PDF));
		} catch (Exception e) {
			throw new ExceptionConverter(e);
		}
	}

	public static void main(String[] args) {
		try {
			// step 1: creating the document
			Document doc = new Document(PageSize.LETTER.rotate(), 50, 50, 50,
					72);
			// step 2: creating the writer
			PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(
					"cabecera.pdf"));
			// step 3: initialisations + opening the document
			writer.setPageEvent(new HeaderPdfPageEvent(
					"REPORTE DE PRUEBA DE LA CABECERA", "CSDCORERC",
					"15/01/2015 19:45 pm"));
			doc.open();
			// step 4: adding content
			String text = "some padding text ";
			StringBuilder sb = new StringBuilder(1000);
			for (int k = 0; k < 300; ++k)
				sb.append(text);

			Paragraph p = new Paragraph(sb.toString());
			p.setAlignment(Element.ALIGN_JUSTIFIED);
			doc.add(p);
			// step 5: closing the document
			doc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
