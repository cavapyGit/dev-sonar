package com.pradera.commons.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SelectOneRequiredValidator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/03/2014
 */
@FacesValidator("selectOneRequiredValidator")
public class SelectOneRequiredValidator implements Validator {

	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
	
		Boolean skipValidation=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipValidation"));
		
		if(Boolean.FALSE.equals(skipValidation) && (value == null)) {			
			SelectOneMenu combo = (SelectOneMenu) component;			
			Locale locale = context.getViewRoot().getLocale();
			Object[] parameters = {combo.getLabel() };
			String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale,
					GeneralPropertiesConstants.ERROR_COMBO_REQUIRED, parameters);
			FacesMessage msg = new FacesMessage(strMsg);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
			
			combo.setValue(null);
			combo.setSubmittedValue(null);
			combo.setLocalValueSet(false);
			combo.setValid(true);
			
			throw new ValidatorException(msg);			
		}		
	}
}