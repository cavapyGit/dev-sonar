package com.pradera.commons.view;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.inject.Stereotype;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Interface ViewDepositaryBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06/08/2013
 */
@Stereotype
@Inherited
@Named
@ViewAccessScoped
@Target({TYPE})
@Retention(RUNTIME)
@Documented
public @interface ViewDepositaryBean {

}
