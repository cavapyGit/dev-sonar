package com.pradera.commons.view.component;

import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.pradera.commons.utils.GeneralConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class SelectOneMenuAdvance.
 */
public class SelectOneMenuAdvance extends SelectOneMenu{

	/* (non-Javadoc)
	 * @see org.primefaces.component.selectonemenu.SelectOneMenu#isFilter()
	 */
	@Override
	public boolean isFilter() {
		boolean flag=true;
		if(getAttributes().get(GeneralConstants.SKIP_FILTER_PROPERTY)!=null){
			flag=Boolean.parseBoolean(getAttributes().get(GeneralConstants.SKIP_FILTER_PROPERTY).toString());
		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.component.selectonemenu.SelectOneMenu#getEffect()
	 */
	@Override
	public String getEffect() {
		// TODO Auto-generated method stub
		return "fade";
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.component.selectonemenu.SelectOneMenu#getFilterMatchMode()
	 */
	@Override
	public String getFilterMatchMode() {
		String filterMode=GeneralConstants.SEARCH_TYPE_FILTER_DEFAULT;
		if(getAttributes().get(GeneralConstants.SEARCH_TYPE_FILTER)!=null){
			filterMode = getAttributes().get(GeneralConstants.SEARCH_TYPE_FILTER).toString();
		}
		return filterMode;
	}
	
}
