package com.pradera.commons.view.component;

import org.primefaces.component.keyfilter.KeyFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class PeKeyFilterAdvance.
 */
public class PeKeyFilterAdvance extends KeyFilter{


	@Override
	public boolean isPreventPaste() {
		return (java.lang.Boolean) getStateHelper().eval(PropertyKeys.preventPaste, false);
	}
}
