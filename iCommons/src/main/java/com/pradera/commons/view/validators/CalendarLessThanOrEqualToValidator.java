package com.pradera.commons.view.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.dialog.Dialog;

import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarValidateMinDate.
 * Not implemented yet
 */
@FacesValidator("calendarLessThanOrEqualToValidator")
public class CalendarLessThanOrEqualToValidator implements Validator{

	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Date curDate=(Date)value;
		Date compareDate=(Date)component.getAttributes().get("calLteCompareValue");
		Date compareDate2=(Date)component.getAttributes().get("calLteCompareValue2");
		Calendar calendar=(Calendar)component;
		
		if(value!=null && compareDate!=null){
//			Dialog dlgReqValidation=(Dialog)JSFUtilities.findViewComponent("dlgMsgRequiredValidation");
			String compareLabel=(String)component.getAttributes().get("calLteCompareLabel");
			String compareLabel2=(String)component.getAttributes().get("calLteCompareLabel2");
			String comboLabel=calendar.getLabel();
			Object[] parameters=new Object[2];
			parameters[0] = comboLabel;
			Locale locale = context.getViewRoot().getLocale();
			
			if(curDate.getTime() <= compareDate.getTime()){
				parameters[1]=compareLabel;
				String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale,
						GeneralPropertiesConstants.ERROR_GREATER_THAN, parameters);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				
//				if(!dlgReqValidation.isRendered()){
//				//	JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.ERROR_GREATER_THAN, parameters);
//				//	JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show();");				
//				}
				throw new ValidatorException(msg);
			}
			if(compareDate2!=null){
				if(curDate.getTime() <= compareDate2.getTime()){					
					parameters[1]=compareLabel2;
					String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale,
							GeneralPropertiesConstants.ERROR_GREATER_THAN, parameters);
					FacesMessage msg = new FacesMessage(strMsg);
					msg.setSeverity(FacesMessage.SEVERITY_ERROR);
					
//					if(!dlgReqValidation.isRendered()){
////						JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.ERROR_GREATER_THAN, parameters);
////						JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show();");				
//					}
					throw new ValidatorException(msg);
					
				}				
			}

			
		}
	}
}
