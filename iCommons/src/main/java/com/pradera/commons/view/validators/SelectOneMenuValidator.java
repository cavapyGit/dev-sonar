package com.pradera.commons.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SelectOneMenuValidator.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/03/2014
 */
@FacesValidator("com.pradera.commons.view.validators.SelectOneMenuValidator")
public class SelectOneMenuValidator implements Validator{

	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		
		Boolean skipValidation=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipValidation"));		
		
		if(context.getPartialViewContext().isExecuteAll() || skipValidation.equals(Boolean.TRUE)){		
			if(value == null) {
				SelectOneMenu combo = (SelectOneMenu) component;			
				Locale locale = context.getViewRoot().getLocale();
				Object[] parametros = {combo.getLabel() };
				String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale,GeneralPropertiesConstants.ERROR_COMBO_SELECT, parametros);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
				//JSFUtilities.showRequiredValidationDialog();
				throw new ValidatorException(msg);
			}
		
		}
	}
}