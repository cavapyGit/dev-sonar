package com.pradera.commons.view.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.dialog.Dialog;

import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarValidateMinDate.
 * Not implemented yet
 */
@FacesValidator("calendarGreaterThanOrEqualValidator")
public class CalendarGreaterThanOrEqualValidator implements Validator{

	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Date curDate=(Date)value;
		Date compareDate=(Date)component.getAttributes().get("calGteCompareValue");
		if(value!=null && compareDate!=null){
			String compareLabel=(String)component.getAttributes().get("calGtecompareLabel");
			Locale locale = context.getViewRoot().getLocale();
			
			if(curDate.getTime() >= compareDate.getTime()){
				Calendar calendar=(Calendar)component;
				Object[] parameters = {calendar.getLabel(),compareLabel };
				
				String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale,
						GeneralPropertiesConstants.ERROR_LESS_THAN, parameters);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				
//				Dialog dlgReqValidation=(Dialog)JSFUtilities.findViewComponent("dlgMsgRequiredValidation");
//				if(!dlgReqValidation.isRendered()){
//					JSFUtilities.showMessageOnDialog(null, null, PropertiesConstants.ERROR_LESS_THAN, parameters);
//				//	JSFUtilities.executeJavascriptFunction("PF('cnfwMsgCustomValidation').show();");
//				}
				throw new ValidatorException(msg);
			}
		}
	}
}
