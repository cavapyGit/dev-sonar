package com.pradera.commons.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

//import org.primefaces.PrimeFaces;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.password.Password;
import org.primefaces.component.spinner.Spinner;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

@FacesValidator("inputRequiredValidator")
public class InputRequiredValidator implements Validator{

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		Boolean skipValidation=Boolean.parseBoolean(JSFUtilities.getRequestParameterMap("skipValidation"));
		
		if(value == null && Boolean.FALSE.equals(skipValidation)) {			
		
			Locale locale = context.getViewRoot().getLocale();
						
			Object[] parametros = null;
			
			if(component instanceof InputText){
				InputText inputText=(InputText) component;
				parametros = new Object[]{inputText.getLabel()};
				inputText.resetValue();
			}else if (component instanceof InputNumber){
				InputNumber inputNumber=(InputNumber) component;
				parametros = new Object[]{inputNumber.getLabel()};
				inputNumber.resetValue();
			}else if (component instanceof InputTextarea){
				InputTextarea inputTextarea=(InputTextarea) component;
				parametros = new Object[]{((InputTextarea) component).getLabel()};
				inputTextarea.resetValue();
			} else if(component instanceof Spinner) {
				Spinner spinner=(Spinner) component;
				parametros = new Object[]{((Spinner) component).getLabel()};
				spinner.resetValue();
			} else if(component instanceof Password) {
				Password pasword=(Password) component;
				parametros = new Object[]{((Password) component).getLabel()};
				pasword.resetValue();
			}
			
			String strMsg =  PropertiesUtilities.getMessage(GeneralPropertiesConstants.PROPERTYFILE,locale, 
					GeneralPropertiesConstants.ERROR_FIELD_REQUIRED, parametros);
			
			FacesMessage msg = 
					new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_BODY_ALERT_MANDATORY_DATA));
			
			//PrimeFaces.current().resetInputs(component.getId());
		
		//	JSFUtilities.showRequiredValidationDialog();
				throw new ValidatorException(msg);
		
		}
	}

}
