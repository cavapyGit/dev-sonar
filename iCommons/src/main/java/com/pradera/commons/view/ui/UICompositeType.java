package com.pradera.commons.view.ui;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum Daytype.
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.0
 */
public enum UICompositeType{

	MARKETFACT_BALANCE(Integer.valueOf(1),"COMPONENTE DE SUBSALDOS"),
	
	SECURITY_FINANCIAL_DATA(Integer.valueOf(2),"COMPONENTE QUE MUESTRA LOS DATOS FINANCIEROS DEL VALOR");
	
	private Integer code;
	private String value;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	private UICompositeType(int ordinal,String name) {
		this.code = ordinal;
		this.value = name;
	}
	public static final List<UICompositeType> list = new ArrayList<UICompositeType>();
	public static final Map<Integer, UICompositeType> lookup = new HashMap<Integer, UICompositeType>();

	static {
		for (UICompositeType s : EnumSet.allOf(UICompositeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	public static UICompositeType get(Integer codigo) {
		return lookup.get(codigo);
	}
}