package com.pradera.commons.view;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.inject.Stereotype;
import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ConversationScoped;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Qualifier DepositaryWebBean.
 * is using for identificate web beans in Idepositary
 * in scope conversation
 * @author PraderaTechnologies.
 * @version 1.0 , 08/05/2013
 */
@Stereotype
@Inherited
@Named
@ConversationScoped
@Target({ TYPE, METHOD, FIELD })
@Retention(RUNTIME)
@Documented
public @interface DepositaryWebBean {

}
