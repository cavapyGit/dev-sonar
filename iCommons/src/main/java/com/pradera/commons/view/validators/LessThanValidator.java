package com.pradera.commons.view.validators;

import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class LessThanValidator.
 * Not implemented yet
 */
@FacesValidator("lessThanValidator")
public class LessThanValidator implements Validator{

	private FacesMessage fMessage=null;
	
	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		
			Double currentValue=value !=null ? Double.valueOf( value.toString() ) : null;
			Double currentExceptionValue=null;
			Double compareExceptionValue=null;
			
			if(currentValue==null) {
				return;
			}
			
			if(component.getAttributes().get("ltCurrentExceptionValue")!=null){
				currentExceptionValue=Double.parseDouble(component.getAttributes().get("ltCurrentExceptionValue").toString());
				if(currentValue.compareTo( currentExceptionValue)==0){
					return;
				}
			}
			
			Double compareValue= null;
			String compareLabel=(String)component.getAttributes().get("ltCompareLabel");
			if(component.getAttributes().get("ltCompareValue")!=null){
				compareValue=Double.parseDouble( component.getAttributes().get("ltCompareValue").toString() );
			}
						
			Double compareValue2=null;			
			String compareLabel2=(String)component.getAttributes().get("ltCompareLabel2");
			if(component.getAttributes().get("ltCompareValue2")!=null){
				compareValue2= Double.parseDouble( component.getAttributes().get("ltCompareValue2").toString() );
			}
			
			if(currentValue!=null){
				if(compareValue!=null){
					
					if(component.getAttributes().get("ltCompareExceptionValue")!=null){
						compareExceptionValue=Double.parseDouble(component.getAttributes().get("ltCompareExceptionValue").toString());
						if(compareValue.compareTo( compareExceptionValue)==0){
							return;
						}
					}
					
					if(isError(context,component,currentValue,compareValue,compareLabel)){				
						throw new ValidatorException(fMessage);
					}
				}
				if(compareValue2!=null){
					if(isError(context,component,currentValue,compareValue2,compareLabel2)){				
						throw new ValidatorException(fMessage);
					}
				}
			}
	}
	
	public boolean isError(FacesContext context,UIComponent component,Double currentValue, Double compareValue, String compareLabel){
		Locale locale = context.getViewRoot().getLocale();
		if(Double.valueOf( currentValue ).doubleValue() < compareValue.doubleValue()){
			UIInput input=(UIInput)component;
			String label=null;
			if(input instanceof InputText){
				InputText inputText=(InputText)component;
				label=inputText.getLabel();
			}else if(input instanceof InputNumber){
				InputNumber inputNumber=(InputNumber)component;
				label=inputNumber.getLabel();
			}
			//Delete wrong value
			JSFUtilities.resetComponent(component.getClientId());
			
			Object[] parameters = {label,compareLabel };
			String strMsg =  PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES,locale,
					GeneralPropertiesConstants.ERROR_GREATER_THAN_OR_EQUAL_TO, parameters);
			fMessage= new FacesMessage(strMsg);
			fMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			
//			ConfirmDialog dlgReqValidation=(ConfirmDialog)JSFUtilities.findViewComponent("dlgMsgRequiredValidation");
//			if(!dlgReqValidation.isRendered()){
//				JSFUtilities.showMessageOnDialog(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,PropertiesConstants.LBL_HEADER_ALERT_ERROR, null, 
//												 GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES, PropertiesConstants.ERROR_GREATER_THAN_OR_EQUAL_TO, parameters);
//				JSFUtilities.executeJavascriptFunction("cnfwMsgCustomValidationSec.show()");
//			}
			return true;
		}
		return false;
	}
}
