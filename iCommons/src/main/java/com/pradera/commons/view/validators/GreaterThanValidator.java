package com.pradera.commons.view.validators;

import java.math.BigDecimal;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarValidateMinDate.
 * Not implemented yet
 */
@FacesValidator("greaterThanValidator")
public class GreaterThanValidator implements Validator{

	private FacesMessage fMessage=null;
	
	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
	
 		BigDecimal currentValue=value !=null ? new BigDecimal(value.toString() ) : null;
		BigDecimal exceptionValue=null;
		BigDecimal compareExceptionValue=null;
		
		if(component.getAttributes().get("gtCurrentExceptionValue")!=null){
			exceptionValue=new BigDecimal(component.getAttributes().get("gtCurrentExceptionValue").toString() );
			if(currentValue.compareTo( exceptionValue )==0){
				return;
			}
		}
		
		BigDecimal compareValue= null;
		String compareLabel=(String)component.getAttributes().get("gtCompareLabel");
		if(component.getAttributes().get("gtCompareValue")!=null){
			compareValue=new BigDecimal(component.getAttributes().get("gtCompareValue").toString() );
		}
		
		BigDecimal compareValue2=null;			
		String compareLabel2=(String)component.getAttributes().get("gtCompareLabel2");
		if(component.getAttributes().get("gtCompareValue2")!=null){
			compareValue2=new BigDecimal(component.getAttributes().get("gtCompareValue2").toString() );
		}
		
		BigDecimal compareValue3=null;			
		String compareLabel3=(String)component.getAttributes().get("gtCompareLabel3");
		if(component.getAttributes().get("gtCompareValue3")!=null){
			compareValue3=new BigDecimal(component.getAttributes().get("gtCompareValue3").toString() );
		}
		
		if(currentValue!=null){
			if(compareValue!=null){
				
				if(component.getAttributes().get("gtCompareExceptionValue")!=null){
					compareExceptionValue=new BigDecimal(component.getAttributes().get("gtCompareExceptionValue").toString() );
					if(compareValue.compareTo( compareExceptionValue)==0){
						return;
					}
				}
				
				if(isError(context,component,currentValue,compareValue,compareLabel)){				
					throw new ValidatorException(fMessage);
				}
			}
			if(compareValue2!=null){
				if(component.getAttributes().get("gtCompareValueTotal2")!=null && new BigDecimal(component.getAttributes().get("gtCompareValueTotal2").toString()).compareTo(BigDecimal.ZERO) != 0){
					currentValue = new BigDecimal(component.getAttributes().get("gtCompareValueTotal2").toString() ).add(currentValue);
				}
				if(isError(context,component,currentValue,compareValue2,compareLabel2)){				
					throw new ValidatorException(fMessage);
				}
			}
			if(compareValue3!=null){
				if(isError(context,component,currentValue,compareValue3,compareLabel3)){				
					throw new ValidatorException(fMessage);
				}
			}
		}
		

	}
	
	
	private boolean isError(FacesContext context,UIComponent component,BigDecimal currentValue, BigDecimal compareValue, String compareLabel){
		Locale locale = context.getViewRoot().getLocale();
		if(currentValue.compareTo(compareValue)==1){
			UIInput input=(UIInput)component;
			String label=null;
			if(input instanceof InputText){
				InputText inputText=(InputText)component;
				label=inputText.getLabel();
				JSFUtilities.resetComponent(component.getClientId());
			}else if(input instanceof InputNumber){
				InputNumber inputNumber=(InputNumber)component;
				label=inputNumber.getLabel();
				JSFUtilities.resetComponent(component.getClientId());
			}else if (input instanceof HtmlInputText){
				HtmlInputText inputText = (HtmlInputText)component;
				label=inputText.getLabel();
			}
			
			Object[] parameters = {label,compareLabel };
			String strMsg =  PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES,locale,
					GeneralPropertiesConstants.ERROR_LESS_THAN_OR_EQUAL_TO, parameters);
			fMessage= new FacesMessage(strMsg);
			fMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			
			return true;
		}
		return false;
	}
	
}
