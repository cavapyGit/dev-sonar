package com.pradera.commons.view.component.exporter;

import java.io.Serializable;
import java.util.Map;

import javax.inject.Named;

import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.ViewAccessScoped;

@ViewAccessScoped
@Named
public class ExporterController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3711000727780995577L;

	private Boolean customExporter;
	
	private String userName;
	
	private Map<String,Object> parameters;
	
	private String reportTile;


	public ExporterController() {
             customExporter=false;
	}

    public Boolean getCustomExporter() {
        return customExporter;
    }

    public void setCustomExporter(Boolean customExporter) {
        this.customExporter = customExporter;
    }

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Map<String,Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String,Object> parameters) {
		this.parameters = parameters;
	}

	public String getReportTile() {
		return reportTile;
	}

	public void setReportTile(String reportTile) {
		this.reportTile = reportTile;
	}

}
