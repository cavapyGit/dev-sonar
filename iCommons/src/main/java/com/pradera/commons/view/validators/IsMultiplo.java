package com.pradera.commons.view.validators;

import java.math.BigDecimal;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class CalendarValidateMinDate.
 * Not implemented yet
 */
@FacesValidator("isMultiplo")
public class IsMultiplo implements Validator{

	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
	
		BigDecimal compareValue=  (BigDecimal) component.getAttributes().get("mlpCompareValue");
		BigDecimal currentValue;
		if(value instanceof Double){
			currentValue=BigDecimal.valueOf( Double.parseDouble(value.toString()) );
		}else{
			currentValue=(BigDecimal)value;
		}		
		
		if(value!=null && compareValue != null){						
			String compareLabel=(String)component.getAttributes().get("mlpCompareLabel");
			Locale locale = context.getViewRoot().getLocale();
			if(currentValue.remainder( compareValue ).compareTo(BigDecimal.ZERO)!=0){
				UIInput input=(UIInput)component;
				String label=null;
				if(input instanceof InputText){
					InputText inputText=(InputText)component;
					label=inputText.getLabel();
				}else if(input instanceof InputNumber){
					InputNumber inputNumber=(InputNumber)component;
					label=inputNumber.getLabel();
				}
				
				JSFUtilities.resetComponent(component.getClientId());
				input.resetValue();
				Object[] parameters = {label,compareLabel };
				String strMsg =  PropertiesUtilities.getMessage( GeneralConstants.PROPERTY_FILE_VALIDATION_MESSAGES,locale,
						GeneralPropertiesConstants.ERROR_MULTIPLO, parameters);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				
				throw new ValidatorException(msg);
			}
		}
	}
}
