package com.pradera.commons.view.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Specializes;

import org.apache.myfaces.extensions.cdi.core.api.config.ConfigEntry;
import org.apache.myfaces.extensions.cdi.core.api.scope.conversation.config.ConversationConfig;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DepositoryConversationConfig.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08-sep-2015
 */

@ApplicationScoped
@Specializes
public class DepositoryConversationConfig extends ConversationConfig {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2579818557890096212L;

	/**
	 * Instantiates a new depository conversation config.
	 */
	public DepositoryConversationConfig() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.apache.myfaces.extensions.cdi.core.api.scope.conversation.config.ConversationConfig#getConversationTimeoutInMinutes()
	 */
	@ConfigEntry
	public int getConversationTimeoutInMinutes() {
		return 60;

	}

}
