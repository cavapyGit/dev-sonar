package com.pradera.commons.view.component.exporter;

import javax.faces.FacesException;
import javax.faces.context.FacesContext;

import org.primefaces.extensions.component.exporter.Exporter;
import org.primefaces.extensions.component.exporter.ExporterFactory;

import com.pradera.commons.utils.CommonsUtilities;

/**
 * Accessor for objects stored in several scopes via faces context {@link javax.faces.context.FacesContext}.
 *
 * @author  Sudheer Jonna / last modified by $Author$
 * @version $Revision$
 */
public class CustomExporterFactory implements ExporterFactory {

    static public enum ExporterType {
        PDF,
        XLSX
    }

    public Exporter getExporterForType(String type) {

        Exporter exporter = null;

        FacesContext context = FacesContext.getCurrentInstance();
        ExporterController bean = (ExporterController) context.getApplication().evaluateExpressionGet(context, "#{exporterController}", ExporterController.class);
 
        try {
            ExporterType exporterType = ExporterType.valueOf(type.toUpperCase());

            switch (exporterType) {

                case PDF:
                	exporter = new PDFCustomExporter(bean.getReportTile(),
                			CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(), "dd/MM/yyyy hh:mm a"),
                			bean.getUserName(),bean.getParameters());
                	break;
                case XLSX:
                    exporter = new ExcelCustomExporter();
                    break;
                default: {
                    exporter = new PDFCustomExporter(bean.getReportTile(),
                			CommonsUtilities.convertDateToString(CommonsUtilities.currentDateTime(), "dd/MM/yyyy hh:mm a"),
                			bean.getUserName(),bean.getParameters());
                    break;
                }

            }
        } catch (IllegalArgumentException e) {
            throw new FacesException(e);
        }

        return exporter;
    }

}