package com.pradera.commons.monitoringReports.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @author PraderaTechnologies
 *
 */
public class ReportBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idReportPk;

	private Integer idSystemOptionFk;

	private String idepositaryModule;

	private Integer inPdfFormat;

	private Integer inPriority;

	private Integer inQueue;

	private Integer inTxtFormat;

	private Integer lastModifyApplic;

	private Date lastModifyDate;

	private String lastModifyIp;

	private String lastModifyUser;

	private String mnemonico;

	private String name;

	private Integer nrColumns;

	private String reportBeanName;

	private String reportClasification;

	private String title;

	private Integer userSecurityType;

	private List<ReportFilterBean> reportFilters;

	private List<ReportLoggerBean> reportLoggers;

    public ReportBean() {
    }

	/**
	 * @return the idReportPk
	 */
	public long getIdReportPk() {
		return idReportPk;
	}

	/**
	 * @param idReportPk the idReportPk to set
	 */
	public void setIdReportPk(long idReportPk) {
		this.idReportPk = idReportPk;
	}

	/**
	 * @return the idSystemOptionFk
	 */
	public Integer getIdSystemOptionFk() {
		return idSystemOptionFk;
	}

	/**
	 * @param idSystemOptionFk the idSystemOptionFk to set
	 */
	public void setIdSystemOptionFk(Integer idSystemOptionFk) {
		this.idSystemOptionFk = idSystemOptionFk;
	}

	/**
	 * @return the idepositaryModule
	 */
	public String getIdepositaryModule() {
		return idepositaryModule;
	}

	/**
	 * @param idepositaryModule the idepositaryModule to set
	 */
	public void setIdepositaryModule(String idepositaryModule) {
		this.idepositaryModule = idepositaryModule;
	}

	/**
	 * @return the inPdfFormat
	 */
	public Integer getInPdfFormat() {
		return inPdfFormat;
	}

	/**
	 * @param inPdfFormat the inPdfFormat to set
	 */
	public void setInPdfFormat(Integer inPdfFormat) {
		this.inPdfFormat = inPdfFormat;
	}

	/**
	 * @return the inPriority
	 */
	public Integer getInPriority() {
		return inPriority;
	}

	/**
	 * @param inPriority the inPriority to set
	 */
	public void setInPriority(Integer inPriority) {
		this.inPriority = inPriority;
	}

	/**
	 * @return the inQueue
	 */
	public Integer getInQueue() {
		return inQueue;
	}

	/**
	 * @param inQueue the inQueue to set
	 */
	public void setInQueue(Integer inQueue) {
		this.inQueue = inQueue;
	}

	/**
	 * @return the inTxtFormat
	 */
	public Integer getInTxtFormat() {
		return inTxtFormat;
	}

	/**
	 * @param inTxtFormat the inTxtFormat to set
	 */
	public void setInTxtFormat(Integer inTxtFormat) {
		this.inTxtFormat = inTxtFormat;
	}

	/**
	 * @return the lastModifyApplic
	 */
	public Integer getLastModifyApplic() {
		return lastModifyApplic;
	}

	/**
	 * @param lastModifyApplic the lastModifyApplic to set
	 */
	public void setLastModifyApplic(Integer lastModifyApplic) {
		this.lastModifyApplic = lastModifyApplic;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the mnemonico
	 */
	public String getMnemonico() {
		return mnemonico;
	}

	/**
	 * @param mnemonico the mnemonico to set
	 */
	public void setMnemonico(String mnemonico) {
		this.mnemonico = mnemonico;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the nrColumns
	 */
	public Integer getNrColumns() {
		return nrColumns;
	}

	/**
	 * @param nrColumns the nrColumns to set
	 */
	public void setNrColumns(Integer nrColumns) {
		this.nrColumns = nrColumns;
	}

	/**
	 * @return the reportBeanName
	 */
	public String getReportBeanName() {
		return reportBeanName;
	}

	/**
	 * @param reportBeanName the reportBeanName to set
	 */
	public void setReportBeanName(String reportBeanName) {
		this.reportBeanName = reportBeanName;
	}

	/**
	 * @return the reportClasification
	 */
	public String getReportClasification() {
		return reportClasification;
	}

	/**
	 * @param reportClasification the reportClasification to set
	 */
	public void setReportClasification(String reportClasification) {
		this.reportClasification = reportClasification;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the userSecurityType
	 */
	public Integer getUserSecurityType() {
		return userSecurityType;
	}

	/**
	 * @param userSecurityType the userSecurityType to set
	 */
	public void setUserSecurityType(Integer userSecurityType) {
		this.userSecurityType = userSecurityType;
	}

	/**
	 * @return the reportFilters
	 */
	public List<ReportFilterBean> getReportFilters() {
		return reportFilters;
	}

	/**
	 * @param reportFilters the reportFilters to set
	 */
	public void setReportFilters(List<ReportFilterBean> reportFilters) {
		this.reportFilters = reportFilters;
	}

	/**
	 * @return the reportLoggers
	 */
	public List<ReportLoggerBean> getReportLoggers() {
		return reportLoggers;
	}

	/**
	 * @param reportLoggers the reportLoggers to set
	 */
	public void setReportLoggers(List<ReportLoggerBean> reportLoggers) {
		this.reportLoggers = reportLoggers;
	}

	
	
}