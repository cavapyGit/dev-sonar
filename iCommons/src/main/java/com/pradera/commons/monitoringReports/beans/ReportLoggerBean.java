package com.pradera.commons.monitoringReports.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author PraderaTechnologies
 *
 */
public class ReportLoggerBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idReportLoggerPk;

	private Date creationDate;
	
	private Date finishDate;
	
	private String creationUser;

	private String errorDetail;

	private byte[] fileXml;

	private Integer inError;

	private Integer lastModifyApplic;

	private Date lastModifyDate;

	private String lastModifyIp;

	private String lastModifyUser;

	private String physicalName;

	private Integer reportState;

	private String state;
	
	private ReportBean report;

	private List<ReportLoggerDetailBean> reportLoggerDetails;

	private List<ReportLoggerFileBean> reportLoggerFiles;

	private List<ReportLoggerUserBean> reportLoggerUsers;

    public ReportLoggerBean() {
    }

	/**
	 * @return the idReportLoggerPk
	 */
	public long getIdReportLoggerPk() {
		return idReportLoggerPk;
	}

	/**
	 * @param idReportLoggerPk the idReportLoggerPk to set
	 */
	public void setIdReportLoggerPk(long idReportLoggerPk) {
		this.idReportLoggerPk = idReportLoggerPk;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the finishDate
	 */
	public Date getFinishDate() {
		return finishDate;
	}

	/**
	 * @param finishDate the finishDate to set
	 */
	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	/**
	 * @return the creationUser
	 */
	public String getCreationUser() {
		return creationUser;
	}

	/**
	 * @param creationUser the creationUser to set
	 */
	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}

	/**
	 * @return the errorDetail
	 */
	public String getErrorDetail() {
		return errorDetail;
	}

	/**
	 * @param errorDetail the errorDetail to set
	 */
	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	/**
	 * @return the fileXml
	 */
	public byte[] getFileXml() {
		return fileXml;
	}

	/**
	 * @param fileXml the fileXml to set
	 */
	public void setFileXml(byte[] fileXml) {
		this.fileXml = fileXml;
	}

	/**
	 * @return the inError
	 */
	public Integer getInError() {
		return inError;
	}

	/**
	 * @param inError the inError to set
	 */
	public void setInError(Integer inError) {
		this.inError = inError;
	}

	/**
	 * @return the lastModifyApplic
	 */
	public Integer getLastModifyApplic() {
		return lastModifyApplic;
	}

	/**
	 * @param lastModifyApplic the lastModifyApplic to set
	 */
	public void setLastModifyApplic(Integer lastModifyApplic) {
		this.lastModifyApplic = lastModifyApplic;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the physicalName
	 */
	public String getPhysicalName() {
		return physicalName;
	}

	/**
	 * @param physicalName the physicalName to set
	 */
	public void setPhysicalName(String physicalName) {
		this.physicalName = physicalName;
	}

	/**
	 * @return the reportState
	 */
	public Integer getReportState() {
		return reportState;
	}

	/**
	 * @param reportState the reportState to set
	 */
	public void setReportState(Integer reportState) {
		this.reportState = reportState;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the report
	 */
	public ReportBean getReport() {
		return report;
	}

	/**
	 * @param report the report to set
	 */
	public void setReport(ReportBean report) {
		this.report = report;
	}

	/**
	 * @return the reportLoggerDetails
	 */
	public List<ReportLoggerDetailBean> getReportLoggerDetails() {
		return reportLoggerDetails;
	}

	/**
	 * @param reportLoggerDetails the reportLoggerDetails to set
	 */
	public void setReportLoggerDetails(
			List<ReportLoggerDetailBean> reportLoggerDetails) {
		this.reportLoggerDetails = reportLoggerDetails;
	}

	/**
	 * @return the reportLoggerFiles
	 */
	public List<ReportLoggerFileBean> getReportLoggerFiles() {
		return reportLoggerFiles;
	}

	/**
	 * @param reportLoggerFiles the reportLoggerFiles to set
	 */
	public void setReportLoggerFiles(List<ReportLoggerFileBean> reportLoggerFiles) {
		this.reportLoggerFiles = reportLoggerFiles;
	}

	/**
	 * @return the reportLoggerUsers
	 */
	public List<ReportLoggerUserBean> getReportLoggerUsers() {
		return reportLoggerUsers;
	}

	/**
	 * @param reportLoggerUsers the reportLoggerUsers to set
	 */
	public void setReportLoggerUsers(List<ReportLoggerUserBean> reportLoggerUsers) {
		this.reportLoggerUsers = reportLoggerUsers;
	}

	
}