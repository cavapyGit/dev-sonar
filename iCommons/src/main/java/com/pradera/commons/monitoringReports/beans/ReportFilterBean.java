package com.pradera.commons.monitoringReports.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author PraderaTechnologies
 *
 */
public class ReportFilterBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idReportFilterPk;

	private String eventOnblur;

	private String eventOnchange;

	private Integer filterType;

	private Integer inRequired;

	private Integer inValidationUser;

	private Integer inViewer;

	private String labelFilter;

	private Integer lastModifyApplic;

	private Date lastModifyDate;

	private String lastModifyIp;

	private String lastModifyUser;

	private Integer maxLenght;

	private String nameValue;

	private Integer screenOrder;

	private Integer sizeInput;

	private String sqlListQuery;

	private ReportBean report;

    public ReportFilterBean() {
    }

	/**
	 * @return the idReportFilterPk
	 */
	public long getIdReportFilterPk() {
		return idReportFilterPk;
	}

	/**
	 * @param idReportFilterPk the idReportFilterPk to set
	 */
	public void setIdReportFilterPk(long idReportFilterPk) {
		this.idReportFilterPk = idReportFilterPk;
	}

	/**
	 * @return the eventOnblur
	 */
	public String getEventOnblur() {
		return eventOnblur;
	}

	/**
	 * @param eventOnblur the eventOnblur to set
	 */
	public void setEventOnblur(String eventOnblur) {
		this.eventOnblur = eventOnblur;
	}

	/**
	 * @return the eventOnchange
	 */
	public String getEventOnchange() {
		return eventOnchange;
	}

	/**
	 * @param eventOnchange the eventOnchange to set
	 */
	public void setEventOnchange(String eventOnchange) {
		this.eventOnchange = eventOnchange;
	}

	/**
	 * @return the filterType
	 */
	public Integer getFilterType() {
		return filterType;
	}

	/**
	 * @param filterType the filterType to set
	 */
	public void setFilterType(Integer filterType) {
		this.filterType = filterType;
	}

	/**
	 * @return the inRequired
	 */
	public Integer getInRequired() {
		return inRequired;
	}

	/**
	 * @param inRequired the inRequired to set
	 */
	public void setInRequired(Integer inRequired) {
		this.inRequired = inRequired;
	}

	/**
	 * @return the inValidationUser
	 */
	public Integer getInValidationUser() {
		return inValidationUser;
	}

	/**
	 * @param inValidationUser the inValidationUser to set
	 */
	public void setInValidationUser(Integer inValidationUser) {
		this.inValidationUser = inValidationUser;
	}

	/**
	 * @return the inViewer
	 */
	public Integer getInViewer() {
		return inViewer;
	}

	/**
	 * @param inViewer the inViewer to set
	 */
	public void setInViewer(Integer inViewer) {
		this.inViewer = inViewer;
	}

	/**
	 * @return the labelFilter
	 */
	public String getLabelFilter() {
		return labelFilter;
	}

	/**
	 * @param labelFilter the labelFilter to set
	 */
	public void setLabelFilter(String labelFilter) {
		this.labelFilter = labelFilter;
	}

	/**
	 * @return the lastModifyApplic
	 */
	public Integer getLastModifyApplic() {
		return lastModifyApplic;
	}

	/**
	 * @param lastModifyApplic the lastModifyApplic to set
	 */
	public void setLastModifyApplic(Integer lastModifyApplic) {
		this.lastModifyApplic = lastModifyApplic;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the maxLenght
	 */
	public Integer getMaxLenght() {
		return maxLenght;
	}

	/**
	 * @param maxLenght the maxLenght to set
	 */
	public void setMaxLenght(Integer maxLenght) {
		this.maxLenght = maxLenght;
	}

	/**
	 * @return the nameValue
	 */
	public String getNameValue() {
		return nameValue;
	}

	/**
	 * @param nameValue the nameValue to set
	 */
	public void setNameValue(String nameValue) {
		this.nameValue = nameValue;
	}

	/**
	 * @return the screenOrder
	 */
	public Integer getScreenOrder() {
		return screenOrder;
	}

	/**
	 * @param screenOrder the screenOrder to set
	 */
	public void setScreenOrder(Integer screenOrder) {
		this.screenOrder = screenOrder;
	}

	/**
	 * @return the sizeInput
	 */
	public Integer getSizeInput() {
		return sizeInput;
	}

	/**
	 * @param sizeInput the sizeInput to set
	 */
	public void setSizeInput(Integer sizeInput) {
		this.sizeInput = sizeInput;
	}

	/**
	 * @return the sqlListQuery
	 */
	public String getSqlListQuery() {
		return sqlListQuery;
	}

	/**
	 * @param sqlListQuery the sqlListQuery to set
	 */
	public void setSqlListQuery(String sqlListQuery) {
		this.sqlListQuery = sqlListQuery;
	}

	/**
	 * @return the report
	 */
	public ReportBean getReport() {
		return report;
	}

	/**
	 * @param report the report to set
	 */
	public void setReport(ReportBean report) {
		this.report = report;
	}

	
}