package com.pradera.commons.monitoringReports.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.report.types.ComboBoxValueBean;


/**
 * The bean class 
 * 
 */
public class ReportLoggerDetailBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idLoggerDetailPk;

	private String filterDescription;

	private String filterValue;

	private Integer inViewer;

	private String labelFilter;

	private Integer lastModifyApplic;

	private Date lastModifyDate;

	private String lastModifyIp;

	private String lastModifyUser;

	private ReportLoggerBean reportLogger;
	
	private boolean requerido;
	
	private Object valorDinamico;
	
	private String eventOnChange;
	
	private String eventOnBlur;
	
	private List<ComboBoxValueBean> lstComboBoxValues;
    
	private String keyFilterExpession;
	
	/**
	 * @return the keyFilterExpession
	 */
	public String getKeyFilterExpession() {
		return keyFilterExpession;
	}

	/**
	 * @param keyFilterExpession the keyFilterExpession to set
	 */
	public void setKeyFilterExpession(String keyFilterExpession) {
		this.keyFilterExpession = keyFilterExpession;
	}

	/**
	 * @return the lstComboBoxValues
	 */
	public List<ComboBoxValueBean> getLstComboBoxValues() {
		return lstComboBoxValues;
	}

	/**
	 * @param lstComboBoxValues the lstComboBoxValues to set
	 */
	public void setLstComboBoxValues(List<ComboBoxValueBean> lstComboBoxValues) {
		this.lstComboBoxValues = lstComboBoxValues;
	}

	/**
	 * @return the requerido
	 */
	public boolean isRequerido() {
		return requerido;
	}

	/**
	 * @param requerido the requerido to set
	 */
	public void setRequerido(boolean requerido) {
		this.requerido = requerido;
	}

	/**
	 * @return the valorDinamico
	 */
	public Object getValorDinamico() {
		return valorDinamico;
	}

	/**
	 * @param valorDinamico the valorDinamico to set
	 */
	public void setValorDinamico(Object valorDinamico) {
		this.valorDinamico = valorDinamico;
	}

	/**
	 * @return the eventOnChange
	 */
	public String getEventOnChange() {
		return eventOnChange;
	}

	/**
	 * @param eventOnChange the eventOnChange to set
	 */
	public void setEventOnChange(String eventOnChange) {
		this.eventOnChange = eventOnChange;
	}

	/**
	 * @return the eventOnBlur
	 */
	public String getEventOnBlur() {
		return eventOnBlur;
	}

	/**
	 * @param eventOnBlur the eventOnBlur to set
	 */
	public void setEventOnBlur(String eventOnBlur) {
		this.eventOnBlur = eventOnBlur;
	}

	public ReportLoggerDetailBean() {
    }

	/**
	 * @return the idLoggerDetailPk
	 */
	public long getIdLoggerDetailPk() {
		return idLoggerDetailPk;
	}

	/**
	 * @param idLoggerDetailPk the idLoggerDetailPk to set
	 */
	public void setIdLoggerDetailPk(long idLoggerDetailPk) {
		this.idLoggerDetailPk = idLoggerDetailPk;
	}

	/**
	 * @return the filterDescription
	 */
	public String getFilterDescription() {
		return filterDescription;
	}

	/**
	 * @param filterDescription the filterDescription to set
	 */
	public void setFilterDescription(String filterDescription) {
		this.filterDescription = filterDescription;
	}

	/**
	 * @return the filterValue
	 */
	public String getFilterValue() {
		return filterValue;
	}

	/**
	 * @param filterValue the filterValue to set
	 */
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}

	/**
	 * @return the inViewer
	 */
	public Integer getInViewer() {
		return inViewer;
	}

	/**
	 * @param inViewer the inViewer to set
	 */
	public void setInViewer(Integer inViewer) {
		this.inViewer = inViewer;
	}

	/**
	 * @return the labelFilter
	 */
	public String getLabelFilter() {
		return labelFilter;
	}

	/**
	 * @param labelFilter the labelFilter to set
	 */
	public void setLabelFilter(String labelFilter) {
		this.labelFilter = labelFilter;
	}

	/**
	 * @return the lastModifyApplic
	 */
	public Integer getLastModifyApplic() {
		return lastModifyApplic;
	}

	/**
	 * @param lastModifyApplic the lastModifyApplic to set
	 */
	public void setLastModifyApplic(Integer lastModifyApplic) {
		this.lastModifyApplic = lastModifyApplic;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the reportLogger
	 */
	public ReportLoggerBean getReportLogger() {
		return reportLogger;
	}

	/**
	 * @param reportLogger the reportLogger to set
	 */
	public void setReportLogger(ReportLoggerBean reportLogger) {
		this.reportLogger = reportLogger;
	}

	
}