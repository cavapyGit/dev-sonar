package com.pradera.commons.monitoringReports.beans;

import java.io.Serializable;
import java.util.Date;


/**
 * @author PraderaTechnologies
 *
 */
public class ReportLoggerUserBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idLoggerUser;

	private String accessUser;

	private Date assignedDate;

	private Integer lastModifyApplic;

	private Date lastModifyDate;

	private String lastModifyIp;

	private String lastModifyUser;

	private ReportLoggerBean reportLogger;

    public ReportLoggerUserBean() {
    }

	/**
	 * @return the idLoggerUser
	 */
	public long getIdLoggerUser() {
		return idLoggerUser;
	}

	/**
	 * @param idLoggerUser the idLoggerUser to set
	 */
	public void setIdLoggerUser(long idLoggerUser) {
		this.idLoggerUser = idLoggerUser;
	}

	/**
	 * @return the accessUser
	 */
	public String getAccessUser() {
		return accessUser;
	}

	/**
	 * @param accessUser the accessUser to set
	 */
	public void setAccessUser(String accessUser) {
		this.accessUser = accessUser;
	}

	/**
	 * @return the assignedDate
	 */
	public Date getAssignedDate() {
		return assignedDate;
	}

	/**
	 * @param assignedDate the assignedDate to set
	 */
	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	/**
	 * @return the lastModifyApplic
	 */
	public Integer getLastModifyApplic() {
		return lastModifyApplic;
	}

	/**
	 * @param lastModifyApplic the lastModifyApplic to set
	 */
	public void setLastModifyApplic(Integer lastModifyApplic) {
		this.lastModifyApplic = lastModifyApplic;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the reportLogger
	 */
	public ReportLoggerBean getReportLogger() {
		return reportLogger;
	}

	/**
	 * @param reportLogger the reportLogger to set
	 */
	public void setReportLogger(ReportLoggerBean reportLogger) {
		this.reportLogger = reportLogger;
	}
	
}