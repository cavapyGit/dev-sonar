package com.pradera.commons.monitoringReports.beans;

import java.io.Serializable;
import java.util.Date;


/**
 * @author PraderaTechnologies
 *
 */
public class ReportLoggerFileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idLoggerFilePk;

	private byte[] dataReport;

	private Integer fileType;
	
	private String type;

	private Integer lastModifyApplic;

	private Date lastModifyDate;

	private String lastModifyIp;

	private String lastModifyUser;

	private String nameFile;

	private Integer numberPages;

	private ReportLoggerBean reportLogger;

    public ReportLoggerFileBean() {
    }

	/**
	 * @return the idLoggerFilePk
	 */
	public long getIdLoggerFilePk() {
		return idLoggerFilePk;
	}

	/**
	 * @param idLoggerFilePk the idLoggerFilePk to set
	 */
	public void setIdLoggerFilePk(long idLoggerFilePk) {
		this.idLoggerFilePk = idLoggerFilePk;
	}

	/**
	 * @return the dataReport
	 */
	public byte[] getDataReport() {
		return dataReport;
	}

	/**
	 * @param dataReport the dataReport to set
	 */
	public void setDataReport(byte[] dataReport) {
		this.dataReport = dataReport;
	}

	/**
	 * @return the fileType
	 */
	public Integer getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the lastModifyApplic
	 */
	public Integer getLastModifyApplic() {
		return lastModifyApplic;
	}

	/**
	 * @param lastModifyApplic the lastModifyApplic to set
	 */
	public void setLastModifyApplic(Integer lastModifyApplic) {
		this.lastModifyApplic = lastModifyApplic;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the nameFile
	 */
	public String getNameFile() {
		return nameFile;
	}

	/**
	 * @param nameFile the nameFile to set
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * @return the numberPages
	 */
	public Integer getNumberPages() {
		return numberPages;
	}

	/**
	 * @param numberPages the numberPages to set
	 */
	public void setNumberPages(Integer numberPages) {
		this.numberPages = numberPages;
	}

	/**
	 * @return the reportLogger
	 */
	public ReportLoggerBean getReportLogger() {
		return reportLogger;
	}

	/**
	 * @param reportLogger the reportLogger to set
	 */
	public void setReportLogger(ReportLoggerBean reportLogger) {
		this.reportLogger = reportLogger;
	}

}