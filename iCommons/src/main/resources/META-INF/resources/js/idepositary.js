var initialFormState;
var currentFormState;
var initialFormStateDialog;
var currentFormStateDialog;
var indInputAmount;
var oldIntputValue;
var buttonBack;
var isCtrlPresset;

var EXP_REG_CASH_AMOUNT = /^[0-9]{0,18}(\.([0-9]{1,2})?)?$/;
var EXP_REG_RATE_FACTOR= /^[0-9]{0,3}(\.([0-9]{1,4})?)?$/; /* /^[0-9]{0,4}(\.([0-9]{1,8})?)?$/; */
var EXP_REG_NOMINAL_AMOUNT= /^[0-9]{0,10}(\.([0-9]{1,2})?)?$/;
var EXP_REG_PERCENTAGE= /^[0-9]{0,2}(\.([0-9]{1,2})?)?$/;
var EXP_REG_EXCHANGE_MONEY= /^[0-9]{0,11}(\.([0-9]{1,8})?)?$/;

$(document).ready(function(){
   $('#general-content').on('keydown', "input[type='text'], textarea", function(event) {
	   if(this.className.indexOf("sensitive-inputField")==-1){	   	   
		   $(this).css("text-transform","uppercase");
	   }
   });

   $('#general-content').on('keyup', "input[type='text'], textarea", function(event) {	   
	   indInputAmount=this.className.indexOf("cashAmount-inputField")!=-1 || 
	   				  this.className.indexOf("rateFactor-inputField")!=-1 ||
	   				  this.className.indexOf("nominalAmount-inputField")!=-1 ||
	   				  this.className.indexOf("percentage-inputField")!=-1 || 
	   				  this.className.indexOf("exchangeMoney-inputField")!=-1;
	   var inputAmounValue=this.value.replace(/,/g,'');
	   var currentRegEx;
	   var keyCode = (event.keyCode ? event.keyCode : event.which);
	   var charCode = String.fromCharCode(keyCode);
	   
	 //Don't do it nothing if key pressed was directional (right, left, up, down) or backspace or shift, control or alt	
	   if(keyCode==37 || keyCode==38 || keyCode==39 ||  keyCode==40 || keyCode==16 || keyCode==17 || keyCode==18){
		   return true;
	   }
	   if( keyCode!=8){
		   if(this.className.indexOf("sensitive-inputField")==-1){	   	   
			   this.value = this.value.toUpperCase();
		   }
	   }
	   if(this.className.indexOf("cashAmount-inputField")!=-1){
		   currentRegEx=EXP_REG_CASH_AMOUNT;
	   }else if(this.className.indexOf("rateFactor-inputField")!=-1){
		   currentRegEx=EXP_REG_RATE_FACTOR;
	   }else if(this.className.indexOf("nominalAmount-inputField")!=-1){
		   currentRegEx=EXP_REG_NOMINAL_AMOUNT;
	   }else if(this.className.indexOf("percentage-inputField")!=-1){
		   currentRegEx=EXP_REG_PERCENTAGE;
	   }else if(this.className.indexOf("exchangeMoney-inputField")!=-1){
		   currentRegEx=EXP_REG_EXCHANGE_MONEY;
	   }
	   
	   if(indInputAmount){
		  
		  if(this.value.length==1){
			   if(inputAmounValue=='.'){
				   this.value=oldIntputValue;
			   }
		  }
	   	 	if(inputAmounValue.match(currentRegEx)){
	   	 		oldIntputValue=this.value;
	   	 	}else{
	   	 	    this.value=oldIntputValue;
	   	 	}
	
	   	 	this.value=inputAmountConvert(this.value);

	   }
    });
   
   $('#general-content').on('blur', "input[type='text'], textarea", function() {
	   this.value =  trim(this.value);
	   
	   var integerValue;
	   var decimalValue;
	   var newValue;
	   var originalNumber;
	   var fixedTo=2;
	   if(this.className.indexOf('rateFactor-inputField')!=-1)
		   fixedTo=4;

	   if(!isNaN(this.value) && this.value!=''){
		  originalNumber=this.value; 
		  var decimalValue = originalNumber % 1;
		  decimalValue = new String(decimalValue.toFixed(fixedTo));
		  decimalValue=decimalValue.substring(decimalValue.indexOf('.'),decimalValue.length  );
		  
		  if(originalNumber.indexOf('.')!=-1){
		  	integerValue=originalNumber.substring(0, originalNumber.lastIndexOf('.'));
		  }else{ 
			 integerValue=originalNumber;
	 	  }
		 newValue=integerValue+decimalValue;
	
		   if(this.className.indexOf("cashAmount-inputField")!=-1){
			   this.value=newValue;
		   }else if(this.className.indexOf("rateFactor-inputField")!=-1){
			   this.value=newValue;
		   }else if(this.className.indexOf("nominalAmount-inputField")!=-1){
			   this.value=newValue;
		   }else if(this.className.indexOf("percentage-inputField")!=-1){
			   this.value=newValue;
		   }else if(this.className.indexOf("exchangeMoney-inputField")!=-1){
			   this.value=newValue;
		   }
	   }
    });
   
    $(document).ready(function() {
	   $(window).keydown(function(event){
	     if(event.keyCode == 13) {
	       event.preventDefault();
	       return false;
	     }
	   });
	 });
	
	$("input[type='text'], textarea").keypress(function(event) { 
		var keyCode = (event.keyCode ? event.keyCode : event.which);
		if(keyCode!=37 && keyCode!=38 && keyCode!=39 &&  keyCode!=40 && keyCode!=16 && keyCode!=17 && keyCode!=18 && keyCode!=8 && keyCode!=9){
			if(this.className.indexOf("novalidate-length-inputField")==-1){
				if(this.value.length >= 200){
					return false;
				}
			}
			
		}
	    return event.keyCode != 13;
	});
   
   $('#general-content').on('keypress', "input[type='text'], textarea", function(event) {
	   indInputAmount=this.className.indexOf("cashAmount-inputField")!=-1 || 
	   				  this.className.indexOf("rateFactor-inputField")!=-1 ||
	   				  this.className.indexOf("nominalAmount-inputField")!=-1 ||
	   				  this.className.indexOf("percentage-inputField")!=-1 || 
	   				  this.className.indexOf("exchangeMoney-inputField")!=-1;
	   indScapeAllValidations=this.className.indexOf("scapeAll-inputField")!=-1;
	   var keyCode = (event.keyCode ? event.keyCode : event.which);
	   var charCode = String.fromCharCode(keyCode);
		 //Don't do it nothing if key pressed was directional (right, left, up, down) or backspace or shift, control or alt	
	   if(keyCode==37 || keyCode==38 || keyCode==39 ||  keyCode==40 || keyCode==16 || keyCode==17 || keyCode==18 || keyCode==8 || keyCode==9){
		   return true;
	   }
	   if(indScapeAllValidations){
		   return true;
	   }
	   if(!indInputAmount){
		   return allowOnlyValidCharacters(event,this);
	   }  
	  
	 });
	 
	 $(document).on('keydown', function (event) {
		    var doPrevent = false;
		    if (event.keyCode === 8) {
		        var d = event.srcElement || event.target;
		        if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD')) 
		             || d.tagName.toUpperCase() === 'TEXTAREA') {
		            doPrevent = d.readOnly || d.disabled;
		        }
		        else {
		            doPrevent = true;
		        }
		    }

		    if (doPrevent) {
		        event.preventDefault();
		    }
	 });
	 
	   $('#general-content').on('focus', "input[type='text'], textarea", function() {
		   $(this).attr('autocomplete', 'off');
		   oldIntputValue=this.value;
	    });
});

function validateInputNumber(input, regEx){
	if(input.value.match(regEx)){
		oldValueAmount=input.value;
	}else{
		input.value=oldValueAmount;
	}
}

function validateZero(input){
	if(input.value.substring(0,1)==0 || input.value.substring(0,1)=='0'){
		if(input.value.length>1)
			input.value = input.value.substring(1,input.value.length);
		else
			input.value = '';
	}
}

function trim(str){
	  var cad=str;
	  if(!cad || typeof cad != 'string')
	    return '';

	  cad = cad.replace(/^[\s]+/,'').replace(/[\s]+$/,'');

	  while(cad.indexOf('  ')!=-1){
	    cad=cad.replace(/[\s]{2,}/,' ');
	  }

	  return cad;
}

PrimeFaces.locales['es'] = {
	    closeText: 'Aceptar',
	    prevText: 'Anterior',
	    nextText: 'Siguiente',
	    monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
	    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
	    dayNamesShort: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
	    dayNamesMin: ['D','L','M','M','J','V','S'], 
	    weekHeader: 'Semana',
	    firstDay: 1,
	    isRTL: false,
	    showMonthAfterYear: false,
	    yearSuffix: '',
	    timeOnlyTitle: 'Sólo hora',
	    timeText: 'Tiempo',
	    hourText: 'Hora',
	    minuteText: 'Minuto',
	    secondText: 'Segundo',
	    currentText: 'Ahora',
	    ampm: false,
	    month: 'Mes',
	    week: 'Semana',
	    day: 'Día',
	    allDayText : 'Todo el día'
	};	

PrimeFaces.locales ['en'] = {
	    closeText: 'Close',
	    prevText: 'Previous',
	    nextText: 'Next',
	    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ],
	    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
	    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	    dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Tue', 'Fri', 'Sat'],
	    dayNamesMin: ['S', 'M', 'T', 'W ', 'T', 'F ', 'S'],
	    weekHeader: 'Week',
	    firstDay: 1,
	    isRTL: false,
	    showMonthAfterYear: false,
	    yearSuffix:'',
	    timeOnlyTitle: 'Only Time',
	    timeText: 'Time',
	    hourText: 'Hour',
	    minuteText: 'Minute',
	    secondText: 'Second',
	    currentText: 'Now',
	    ampm: false,
	    month: 'Month',
	    week: 'week',
	    day: 'Day',
	    allDayText: 'All Day'
	};

PrimeFaces.locales ['fr'] = {
	    closeText: 'Fermer', 
	    prevText: 'Précédent',
	    nextText: 'Suivant',
	    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
	    monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc' ],
	    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
	    dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
	    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
	    weekHeader: 'Semaine',
	    firstDay: 1,
	    isRTL: false,
	    showMonthAfterYear: false,
	    yearSuffix:'',
	    timeOnlyTitle: 'Choisir l\'heure',
	    timeText: 'Heure',
	    hourText: 'Heures',
	    minuteText: 'Minutes',
	    secondText: 'Secondes',
	    currentText: 'Maintenant',
	    ampm: false,
	    month: 'Mois',
	    week: 'Semaine',
	    day: 'Jour',
	    allDayText: 'Toute la journée'
	};

$(document).bind("contextmenu",function(e){
    return false;
});

function startStatus() {  
	PF('statusDialog').show();
}  
  
function stopStatus() {  
	PF('statusDialog').hide();
} 

function allowOnlyValidCharacters(evt, fieldObj) {
	
	var regexValidCharacters=/^[0-9A-Za-z]|[ñÑ]|[\s]|[*]|[(]|[)]|[-]|[,]|[']|[´]|[¨]|[á]|[é]|[í]|[ó]|[ú]|[Á]|[É]|[Í]|[Ó]|[Ú]|[.]|[\:]|[;]|[\#]|[@]|[_]|[ü]|[Ü]|[\/]|[\:]|[?]|[!]+$/;		
	
	evt = (evt) ? evt : event;
	
	//code for allow only Alphanumeric
	var code=(evt.keyCode ? evt.keyCode : evt.which);
	var charCode = String.fromCharCode(code);		
	
	var posicion = findCursorPosition(fieldObj);
	
	if(regexValidCharacters.test(charCode)){
		if(code==32  && ((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)==" ") ||
				fieldObj.value.length==0 || posicion==0 || fieldObj.value.charAt(posicion-1)==" " ||
				fieldObj.value.charAt(posicion)==" " ||
				(fieldObj.value.length>(posicion+1) && fieldObj.value.charAt(posicion+1)==" ")  ) ) { 
					evt.returnValue = false;
					return false;
				}
					
				
				if ((code == 32 || code == 45 || code == 46)&& fieldObj.value.length==0) {
					evt.returnValue = false;
					return false;
				}
				
				if((code==209) || ((code==241))) {
					code=209;
					evt.returnValue = true;
					return true;
				}
				
				if(code==220 || code==252) {		
					evt.returnValue = true;
					return true;
				}
				
				//Invalid Characters
				if(code==94 || code==96 || code==161 || code==191 || code==126 || code==123 || code==125 || code==43 || code==92 || code==180 || code==124 || code==172 || code==176 || code==171 || code==187 || code==168  ){
					evt.returnValue = false;
					return false;
				}
					
				
				if(code==46  &&  ((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)==".") ||
				fieldObj.value.length==0)) {
					evt.returnValue = false;
					return false;
				}
				
				if(code==45  &&  ((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="-") ||
				fieldObj.value.length==0)) {
					evt.returnValue = false;
					return false;
				}

				if(code==47  &&  ((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="/") ||
				fieldObj.value.length==0)) {
					if(fieldObj.className.indexOf("webpage-inputField")==-1){
						if(fieldObj.value.toUpperCase()=="HTTP:/" || fieldObj.value.toUpperCase()=="HTTPS:/"){
							evt.returnValue = true;
							return true;
						}else{
							evt.returnValue = false;
							return false;
						}
					}
				}
				
				if(code==40) {
					if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="(") ||
							fieldObj.value.length==0)){
						evt.returnValue = false;
						return false;
					} else {
						evt.returnValue = true;
						return true;
					}				
				}
				
				if(code==41) {
					if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)==")") ||
							fieldObj.value.length==0)){
						evt.returnValue = false;
						return false;
					} else {
						evt.returnValue = true;
						return true;
					}				
				}
				
				if(code==44) {
					if(fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)==",") ||
								fieldObj.value.length==0)){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}	
					}					
				}
				
				if(code==59) {
					if(fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)==";") ||
								fieldObj.value.length==0)){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}	
					}					
				}
				
				if(code==35) {
					if(fieldObj.className.indexOf("observation-address-inputField")==-1 && fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="#") ||
								fieldObj.value.length==0)){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}
					}								
				}
				
				if(code==39) {
					if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="'") ||
							fieldObj.value.length==0)){
						evt.returnValue = false;
						return false;
					} else {
						evt.returnValue = true;
						return true;
					}				
				}
				
				if(code==64) {
					if(fieldObj.className.indexOf("email-inputField")==-1 && fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="@") ||
								fieldObj.value.length==0)){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}
					}					
				}
				
				if(code==95) {
					if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="_") ||
							fieldObj.value.length==0)){
						evt.returnValue = false;
						return false;
					} else {
						evt.returnValue = true;
						return true;
					}
				}
				
				
				if(code==47) {
					evt.returnValue = true;
					return true;
				}

				if(code==58) {
					if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)==":") ||
							fieldObj.value.length==0)){
						evt.returnValue = false;
						return false;
					} else {
						evt.returnValue = true;
						return true;
					}
				}
				
				if(code==42) {
					if(fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="*")){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}
					}	
				}
				
				if(code==63) {
					if(fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="?") ||
								fieldObj.value.length==0)){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}
					}								
				}

				if(code==33) {
					if(fieldObj.className.indexOf("webpage-inputField")==-1){
						evt.returnValue = false;
						return false;
					} else {
						if(((fieldObj.value.length>0 && fieldObj.value.charAt((fieldObj.value.length)-1)=="!") ||
								fieldObj.value.length==0)){
							evt.returnValue = false;
							return false;
						} else {
							evt.returnValue = true;
							return true;
						}
					}								
				}
				
				if ((code < 48 || code > 57) && (code < 65 || code > 90) && (code < 97 || code > 122)) {
					if (code != 32 && code != 45 && code != 46) {
						evt.returnValue = false;
					} else{
						evt.returnValue = true;
					}
				} else {
					evt.returnValue = true;
				}
				
				//Code for Converting to Uppercase
				if (code > 96 && code < 123) {
					code = code - 32;
					evt.returnValue = true;
				}
	} else {
		evt.returnValue = false;
		return false;
	}


 }

function findCursorPosition(fieldObj) {
        var cursor = -1;
       
        // IE
        if (document.selection && (document.selection != 'undefined')){
            var _range = document.selection.createRange();
            var contador = 0;
            while (_range.move('character', -1))
                contador++;
            cursor = contador;
        } else if (fieldObj.selectionStart >= 0) {        // FF
        	cursor = fieldObj.selectionStart;
        }
            
   
       return cursor;
}

function isModifiedFormForSave(form,vieOperationwModify){ 
	 setCurrentFormState(form); 
	 
	 var initialFormStateAux=$.extend(true, {}, initialFormState);	
	 
	 if(arguments.length > 2){
		 for(var i=2;i < arguments.length;i++){
			 var element=$("[id='"+arguments[i]+"']");
			 if($(element).prop("tagName").toLowerCase()=='div' || $(element).prop("tagName").toLowerCase()=='span'){
				 $(element).find('input, select').each(function(i, obj) {
					 initialFormStateAux[obj['id']]='';
					 currentFormState[obj['id']]='';
				 });
			 }else{
				 initialFormStateAux[arguments[i]]='';
				 currentFormState[arguments[i]]='';				 
			 }
		 }
	 }
	 if((JSON.stringify(initialFormStateAux) == JSON.stringify(currentFormState)) && vieOperationwModify==true){	 
		 PF('cnfwModifiedFormSave').show();
	     return false;
	 }
	 return true;
}

function isModifiedFormForBack(element){ 
	 setCurrentFormState(element.form.id);
	 buttonBack=element.id;
	 
	 var initialFormStateAux=$.extend(true, {}, initialFormState);	
	 
	 if(arguments.length > 1){
		 for(var i=1;i < arguments.length;i++){
			 var element=$("[id='"+arguments[i]+"']");
			 if($(element).prop("tagName").toLowerCase()=='div' || $(element).prop("tagName").toLowerCase()=='span'){
				 $(element).find('input, select').each(function(i, obj) {
					 initialFormStateAux[obj['id']]='';
					 currentFormState[obj['id']]='';
				 });
			 }else{
				 initialFormStateAux[arguments[i]]='';
				 currentFormState[arguments[i]]='';				 
			 }
		 }
	 }
	 if(JSON.stringify(initialFormStateAux) != JSON.stringify(currentFormState)){

	  if(typeof cnfwBackCustom != 'undefined' && cnfwBackCustom != null){
		  PF('cnfwBackCustom').show();
		  return true;  
	  }	 

	  PF('cnfwBack').show();
	  return true;
	 }
	 return false;
}

function confirmBackDialog(){
	
	$("[id='"+buttonBack+"']").removeAttr('onclick');
	$("[id='"+buttonBack+"']").click();
} 

function setInitialFormState(form){
	initialFormState=$(form).serializeObject();
}

function setCurrentFormState(form){
	if($(form).length){
		currentFormState=$(form).serializeObject();
	}else{
		currentFormState=$("[id='"+form+"']").serializeObject();
	}	
}

function setInitialFormStateDialog(form){
	initialFormStateDialog=$(form).serializeObject();
}

function inputAmountConvert(str) {
    var originalNumber = new String(str);
    originalNumber=originalNumber.replace(/,/g,'');
    
    var formatNumber;
    var outputNumber='';
    var decimalNumber='';
    if(originalNumber.indexOf('.')!=-1){
    	formatNumber=originalNumber.split(".");
    	decimalNumber='.'+formatNumber[1];
    	formatNumber=formatNumber[0].split("").reverse();
    }else{
    	formatNumber = originalNumber.split("").reverse();
    }
   
    for ( var i = 0; i <= formatNumber.length-1; i++ ){
    	outputNumber = formatNumber[i] + outputNumber;
        if ((i+1) % 3 == 0 && (formatNumber.length-1) !== i && i!=',')
        	outputNumber = ',' + outputNumber;
    }
    return outputNumber+decimalNumber;
}

function getCaretPosition(ctrl) {
	var CaretPos = 0;	// IE Support
	if (document.selection) {
	ctrl.focus ();
		var Sel = document.selection.createRange ();
		Sel.moveStart ('character', -ctrl.value.length);
		CaretPos = Sel.text.length;
	}
	// Firefox support
	else if (ctrl.selectionStart || ctrl.selectionStart == '0')
		CaretPos = ctrl.selectionStart;
	return (CaretPos);
}
function setCaretPosition(ctrl, pos){
	if(ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(pos,pos);
	}
	else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}
function clearInvalidFileMsgFupl(fuplWidgetVar) {
	// fuplWidgetVar.uploadContent.find("tr.ui-state-error").remove();
//	fuplWidgetVar.content.find("ul li:last-child").remove();
	$("[id$='" + fuplWidgetVar + "']").find("ul li").remove();
}

/*
function clearInvalidFileMsgFupl(fuplWidgetVar){
//	fuplWidgetVar.uploadContent.find("tr.ui-state-error").remove();
	fuplWidgetVar.content.find("ul li:last-child").remove();	
}*/

/*
function fileuploadValidate(fUploadId,fUploadWidgerVar){
	$( "[id='"+fUploadId+"']" ).on( "change", function( e ) {
		var invalidFile=fUploadWidgerVar.content.find("ul li:last-child").html();
		//var invalidFile=fUploadWidgerVar.uploadContent.find("tr.ui-state-error:last").html();
		if(invalidFile!=undefined && invalidFile!=null){ 
			PF('dlgwFileUploadInvFile').show();
			$('#rowFuplContent').html(invalidFile);			
		} else {
			PF('dlgwFileUploadInvFile').hide();
			fuplWidgetVar.content.find("ul li:last-child").remove();
		}
	}); 
} 
*/

function fileuploadValidate(fUploadId) {
	$("[id$='" + fUploadId + "_input']").on(
			"change",
			function(e) {
				var invalidFile = $("[id$='" + fUploadId + "']").find("li").html();
				console.dir(invalidFile);
				// invalidFile=fUploadWidgerVar.uploadContent.find("tr.ui-state-error:last").html();
				if (invalidFile != undefined && invalidFile != null) {
					PF('dlgwFileUploadInvFile').show();
					$('#rowFuplContent').html(invalidFile);
				} else {
					PF('dlgwFileUploadInvFile').hide();
					// fuplWidgetVar.content.find("ul li:last-child").remove();
					$("[id$='" + fUploadId + "']").find("ul li").remove();
				}
			});
}


$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function validateSocialCapital(obj) {
	var originalNumber = new String(obj.value);
    originalNumber=originalNumber.replace(/,/g,'');            
    
    if(originalNumber.indexOf('.')==-1){    	
    	if(originalNumber.length>15){    	
    		obj.value = inputAmountConvert(originalNumber.substring(0, 15));    		
    	}
    }        
}

function validateAmmount(obj,quantity) {
	var originalNumber = new String(obj.value);
    originalNumber=originalNumber.replace(/,/g,'');            
    
    if(originalNumber.indexOf('.')==-1){    	
    	if(originalNumber.length>13){    	
    		obj.value = inputAmountConvert(originalNumber.substring(0, quantity));    		
    	}
    }        
}

var allHolidays;

function setAllHolidays(str){
	allHolidays = str;
}

function enableAlldays(date) {
    return [true];
}

function disableAllHolydays(date) {
	 // primefaces js widget day is from [0 - 6] - [Sunday - Monday]
	var weekDay = date.getDay();
	
	//If Weekend (Saturday or Sunday)
	if(weekDay == 0 || weekDay == 6) {
		return [false];
	}
	
	var disabledDays = allHolidays?.toString().split(",");
    var m = date.getMonth();
    m = m + 1;	    
    var d = date.getDate();
    var y = date.getFullYear();

    var strMonth;
    if(m < 10) {
    	strMonth = "0" + m;
	} else {
		strMonth = m;
	}

	var strDay;
	if(d < 10) {
		strDay = "0" + d;
	} else {
		strDay = d;
	}
    
    for (i = 0; i < disabledDays?.length; i++) { 
        if($.inArray(strDay + '/' + strMonth + '/' + y,disabledDays) != -1) {
            return [false];
        }
    }
    return [true];
}

function disableAllBusinessDay(date) {
	 // primefaces js widget day is from [0 - 6] - [Sunday - Monday]
	var weekDay = date.getDay();
	
	//If Weekend (Saturday or Sunday)
	if(weekDay == 0 || weekDay == 6) {
		return [true];
	} 
	
	var disabledDays = allHolidays.toString().split(",");
   var m = date.getMonth();
   m = m + 1;	    
   var d = date.getDate();
   var y = date.getFullYear();

   var strMonth;
   if(m < 10) {
   	strMonth = "0" + m;
	} else {
		strMonth = m;
	} 

	var strDay;
	if(d < 10) {
		strDay = "0" + d;
	} else {
		strDay = d;
	}
   
   for (i = 0; i < disabledDays.length; i++) { 
       if($.inArray(strDay + '/' + strMonth + '/' + y,disabledDays) != -1) {
           return [true];
       }
   }
   return [false];
}

function disableOnlyHolidaysNoWeekend(date) {
	
	var weekDay = date.getDay();
	
	var disabledDays = allHolidays.toString().split(",");
   var m = date.getMonth();
   m = m + 1;	    
   var d = date.getDate();
   var y = date.getFullYear();

   var strMonth;
   if(m < 10) {
   	strMonth = "0" + m;
	} else {
		strMonth = m;
	}

	var strDay;
	if(d < 10) {
		strDay = "0" + d;
	} else {
		strDay = d;
	}
   
   for (i = 0; i < disabledDays.length; i++) {
       if($.inArray(strDay + '/' + strMonth + '/' + y,disabledDays) != -1) {
           return [false];
       }
   }
   return [true];
}


var initialRepresentativeFormState;
var currentRepresentativeFormState;

function isModifiedRepresentativeFormForBack(form){
	setCurrentRepresentativeFormState(form);
	
	if(arguments.length > 1){
		 for(var i=1;i < arguments.length;i++){
			 var element=$("[id='"+arguments[i]+"']");
			 if($(element).prop("tagName").toLowerCase()=='div' || $(element).prop("tagName").toLowerCase()=='span'){
				 $(element).find('input, select').each(function(i, obj) {
					 initialRepresentativeFormState[obj['id']]='';
					 currentRepresentativeFormState[obj['id']]='';
				 });
			 }else{
				 initialRepresentativeFormState[arguments[i]]='';
				 currentRepresentativeFormState[arguments[i]]='';				 
			 }
		 }
	 }
	
	
	if(JSON.stringify(initialRepresentativeFormState) != JSON.stringify(currentRepresentativeFormState)){	 
		PF('cnfwBackRepresentative').show();
		return true;
	}
		 return false;
}


function setInitialRepresentativeFormState(form){
	initialRepresentativeFormState=$(form).serializeObject();	
}

function setCurrentRepresentativeFormState(form){
	currentRepresentativeFormState=$(form).serializeObject();
}

function showCleanConfirmDialog(element){
	setCurrentFormState(element.form.id);
	
	var actionArr= $(element).attr("onclick").split(";");
	actionArr.shift();
	actionArr.shift();
	$("[id='frmClean:cmbYes']").attr("onclick",actionArr.join(";"));
	
	 var initialFormStateAux=$.extend(true, {}, initialFormState);	
	 
	 if(arguments.length > 1){
		 for(var i=1;i < arguments.length;i++){
			 var element=$("[id='"+arguments[i]+"']");
			 if($(element).prop("tagName").toLowerCase()=='div' || $(element).prop("tagName").toLowerCase()=='span'){
				 $(element).find('input, select').each(function(i, obj) {
					 initialFormStateAux[obj['id']]='';
					 currentFormState[obj['id']]='';
				 });
			 }else{
				 initialFormStateAux[arguments[i]]='';
				 currentFormState[arguments[i]]='';				 
			 }
		 }
	 }
	
	if(JSON.stringify(initialFormStateAux) != JSON.stringify(currentFormState)){
		PF('cnfwClean').show();
		return false;
	}else{
		return true;
	}
}

function validateFirstCharacter(input){
	var regEx = /^[0-9]?$/;
	if(input.value.substring(0,1).match(regEx)){
		if(input.value.length>1)
			input.value = input.value.substring(1,input.value.length);
		else
			input.value = '';
	}
}


/* parent can be form, table, etc. */
function checkAllCheckboxById(selAllChk,parent,id){
	//:checkbox 
    var checkboxes = $(selAllChk).closest(parent).find("[id*='"+id+"']:not(:disabled)").not(selAllChk);
	
    if($(selAllChk).is(':checked')) {
        checkboxes.prop('checked', true);
        $(checkboxes).parent().parent().find(".ui-chkbox-icon").removeClass("ui-icon ui-icon-blank");  
        $(checkboxes).parent().parent().find(".ui-chkbox-box").addClass("ui-state-active");
        $(checkboxes).parent().parent().find(".ui-chkbox-icon").addClass("ui-icon ui-icon-check");
		    
    } else {
        checkboxes.prop('checked', false);
        $(checkboxes).parent().parent().find(".ui-chkbox-box").removeClass("ui-state-active");
        $(checkboxes).parent().parent().find(".ui-chkbox-icon").removeClass("ui-icon ui-icon-check");
        $(checkboxes).parent().parent().find(".ui-chkbox-icon").addClass("ui-icon ui-icon-blank");  
    }
}

function checkAllCheckboxByStyleClass(selAllChk,parent,styleClass){
	//:checkbox 
    var checkboxes = $(selAllChk).closest(parent).find("[class*='"+styleClass+"']").find("[type='checkbox']:not(:disabled)").not(selAllChk);
	
    if($(selAllChk).is(':checked')) {
        checkboxes.prop('checked', true);
        $(checkboxes).parent().parent().find(".ui-chkbox-box").addClass("ui-state-active");
        $(checkboxes).parent().parent().find(".ui-chkbox-icon").addClass("ui-icon ui-icon-check");
    } else {
        checkboxes.prop('checked', false);
        $(checkboxes).parent().parent().find(".ui-chkbox-box").removeClass("ui-state-active");
        $(checkboxes).parent().parent().find(".ui-chkbox-icon").removeClass("ui-icon ui-icon-check");
    }
}


function ctrlPress(evt){
	//code 17 --> ctrl
	evt = (evt) ? evt : event;	
	var code=(evt.keyCode ? evt.keyCode : evt.which);
	if(code == 17){
		isCtrlPresset = false;
	}
}
function validateCtrlSpace(evt,input){
	//code 17 --> ctrl
	//code 32 --> blank space
	evt = (evt) ? evt : event;	
	var code=(evt.keyCode ? evt.keyCode : evt.which);
	if(code == 17){
		isCtrlPresset = true;
	}
	if((code != 17 && isCtrlPresset == true) 
			|| (input.value.length == 0 && code == 32 ) 
			|| (input.value.length > $(input).caret().begin && input.value.charAt($(input).caret().begin)==" " && code == 32)
			|| ($(input).caret().begin > 0 && input.value.charAt(($(input).caret().begin)-1)==" " && code == 32)){
		return false;
	}
}
function isModifiedFormForBackMotive(cnfwCboMotive,idButon){		
	if(cnfwCboMotive.getSelectedValue()=="" || cnfwCboMotive.getSelectedValue()==undefined || cnfwCboMotive.getSelectedValue()==null){
		 document.getElementById(idButon).click();
	} 
	else {	
		PF('cnfwBackMotive').show();
		return false;	
	}
}
function validateHyphenSlash(event,input){
 	
    var code = (event.which) ? event.which : event.keyCode;
    var charCode = String.fromCharCode(code);
    
    var code = (event.which) ? event.which : event.keyCode;
    var charCode = String.fromCharCode(code);
    
    if(charCode=='-'){
     if(input.value.indexOf("-")!= -1){
	  event.preventDefault();
	  return false;
	 }
    }
    if(input.value.indexOf("//")>=0){
	   var position = input.value.indexOf("//");				  
	   var cadena = input.value;
	   input.value=cadena.slice(0, position) + cadena.slice(position+1, cadena.length);				   
	   return false;
	}
	
    
     var length=input.value.length;

	 if(event.type=='blur'){
		 if(input.value.charAt(length-1)=='/' ){
			var position = length - 1;
	    	var str = input.value;
	    	input.value=str.slice(0, position) + str.slice(position+1, str.length);
			return false;
		 }
         if(input.value.charAt(length-1)=='-' ){
	    	var position = length - 1;
	    	var str = input.value;
	    	input.value=str.slice(0, position) + str.slice(position+1, str.length);
	    	return false;
      	}
	 }
     return true;
}
function fixCloseSelectCheckBoxMenu(){
	$(".ui-selectcheckboxmenu-close").attr("href",'#forward');
}
