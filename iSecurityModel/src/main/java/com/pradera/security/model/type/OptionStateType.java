package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum OptionStateType.
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum OptionStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(0),"REGISTRADO","added.png"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(1),"CONFIRMADO","confirmed.png"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(2),"RECHAZADO","denied.png"),
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(3),"BLOQUEADO","blocked.png"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(4),"ELIMINADO","");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<OptionStateType> list = new ArrayList<OptionStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OptionStateType> lookup = new HashMap<Integer, OptionStateType>();

	static {
		for (OptionStateType s : EnumSet.allOf(OptionStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new option state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private OptionStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the option state type
	 */
	public static OptionStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
}
