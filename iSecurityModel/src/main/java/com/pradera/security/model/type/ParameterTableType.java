package com.pradera.security.model.type;

// TODO: Auto-generated Javadoc
/**
 * The Enum ParameterTableType.
 *
 * @author PraderaTechnologies
 */
public enum ParameterTableType {

	/** The insitutions state. */
	INSITUTIONS_STATE(Integer.valueOf(5)),
	
	/** The user code. */
	USER_CODE(Integer.valueOf(11)),
	
	/** The user responsibilities. */
	USER_RESPONSIBILITIES(Integer.valueOf(9)),
	
	/** The user states. */
	USER_STATES(Integer.valueOf(10));

	/** The code. */
	private Integer code;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code            the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Instantiates a new master table type.
	 * 
	 * @param code
	 *            the code
	 */
	private ParameterTableType(Integer code) {
		this.code = code;
	}

}
