package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum Daytype.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum AuditProcessState {

	/** The finalizado. */
	FINALIZADO(Integer.valueOf(1),"FINALIZADO"),
	
	/** The noiniciado. */
	NOINICIADO(Integer.valueOf(2),"NO INICIADO"),
	
	/** The terminado. */
	TERMINADO(Integer.valueOf(3),"TERMINADO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new audit process state.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AuditProcessState(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AuditProcessState> list = new ArrayList<AuditProcessState>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AuditProcessState> lookup = new HashMap<Integer, AuditProcessState>();
	static {
		for (AuditProcessState s : EnumSet.allOf(AuditProcessState.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
