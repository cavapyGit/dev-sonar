package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the SCHEDULE_EXCEPTION_DETAIL database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="SCHEDULE_EXCEPTION_DETAIL")
public class ScheduleExceptionDetail implements Serializable,Auditable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The id schedule exeption detail pk. */
	@Id
	@SequenceGenerator(name="SCHEDULE_EXCEPTION_DE_PK_GENERATOR", sequenceName="SQ_ID_SCHEDULE_EXCEPTION_DE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SCHEDULE_EXCEPTION_DE_PK_GENERATOR")
	@Column(name="ID_SCHEDULE_EXEPTION_DETAIL_PK")
	@NotNull
	private long idScheduleExeptionDetailPk;

	/** The date register. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="DATE_REGISTER")
    @NotNull
	private Date dateRegister;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;

	//bi-directional many-to-one association to ScheduleException
	/** The schedule exception. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_SCHEDULE_EXCEPTION_FK", referencedColumnName = "ID_SCHEDULE_EXCEPTION_PK")
	private ScheduleException scheduleException;

	//bi-directional many-to-one association to UserAccount
    /** The user account. */
	@ManyToOne
	@JoinColumn(name="ID_USER_FK")
	private UserAccount userAccount;
    
    /**
     * The selected.
     *
     * @author mmacalupu
     * this atribute has relationship with checkbox
     * in datatable(.xhtml) and show if that register were 
     * selected o No.
     */
	@Transient
	private boolean selected;

    /**
     * Instantiates a new schedule exception detail.
     */
    public ScheduleExceptionDetail() {
    }

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the id schedule exeption detail pk.
	 *
	 * @return the id schedule exeption detail pk
	 */
	public long getIdScheduleExeptionDetailPk() {
		return this.idScheduleExeptionDetailPk;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the id schedule exeption detail pk.
	 *
	 * @param idScheduleExeptionDetailPk the new id schedule exeption detail pk
	 */
	public void setIdScheduleExeptionDetailPk(long idScheduleExeptionDetailPk) {
		this.idScheduleExeptionDetailPk = idScheduleExeptionDetailPk;
	}

	/**
	 * Gets the date register.
	 *
	 * @return the date register
	 */
	public Date getDateRegister() {
		return this.dateRegister;
	}

	/**
	 * Sets the date register.
	 *
	 * @param dateRegister the new date register
	 */
	public void setDateRegister(Date dateRegister) {
		this.dateRegister = dateRegister;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the schedule exception.
	 *
	 * @return the schedule exception
	 */
	public ScheduleException getScheduleException() {
		return this.scheduleException;
	}

	/**
	 * Sets the schedule exception.
	 *
	 * @param scheduleException the new schedule exception
	 */
	public void setScheduleException(ScheduleException scheduleException) {
		this.scheduleException = scheduleException;
	}
	
	/**
	 * Gets the user account.
	 *
	 * @return the user account
	 */
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	/**
	 * Sets the user account.
	 *
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		String descripcion = null;
		if (this.getState() != null) {
			descripcion = ScheduleExceptionStateType.lookup.get(this.getState()).getIcon();
		}
		return descripcion;
	}
	
	/**
	 * Gets the state information.
	 *
	 * @return the state information
	 */
	public String getStateInformation() {
		String descripcion = null;
		if (this.getState() != null) {
			descripcion = ScheduleExceptionStateType.lookup.get(this.getState()).getValue();
		}
		return descripcion;
	}
	
	/**
	 * Gets the state icon.
	 *
	 * @return the state icon
	 */
	public String getStateIcon() {
		return ScheduleStateType.lookup.get(this.getState()).getIcon();
	}
	
	/**
	 * Gets the state description selected.
	 *
	 * @return the state description selected
	 */
	public String getStateDescriptionSelected(){
		return ScheduleExceptionDetailStateType.lookup.get(BooleanType.lookup.get(this.selected).getCode()).getValue();
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
}