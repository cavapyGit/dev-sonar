package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the TICKETS_DATAMART database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="TICKETS_DATAMART")
public class TicketsDatamart implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_TICKETS_DATAMART_PK")
	private String idTicketsDatamartPk;

	@Column(name="SYSTEM_CODE")
	private Integer systemCode;

	@Column(name="USERACCOUNT_CODE")
	private Integer useraccountCode;

	@Column(name="PROFILE_CODE")
	private Integer profileCode;
	
	@Column(name="INSTITUTION_TYPE_CODE")
	private Integer institutionTypeCode;

	@Column(name="INSTITUTION_TYPE_NAME")
	private String institutionTypeName;

	@Column(name="INSTITUTION_CODE")
	private Integer institutionCode;

	@Column(name="INSTITUTION_NAME")
	private String institutionName;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="START_DATE")
	private Date startDate;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_DATE")
	private Date lastDate;
	
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="EXPIRATION_DATE")
	private Date expirationDate;

    @Transient
	private Map<String, List<HashMap<String, Object>>> menuOptions;

	public String getIdTicketsDatamartPk() {
		return idTicketsDatamartPk;
	}

	public void setIdTicketsDatamartPk(String idTicketsDatamartPk) {
		this.idTicketsDatamartPk = idTicketsDatamartPk;
	}

	public Integer getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(Integer systemCode) {
		this.systemCode = systemCode;
	}

	public Integer getUseraccountCode() {
		return useraccountCode;
	}

	public void setUseraccountCode(Integer useraccountCode) {
		this.useraccountCode = useraccountCode;
	}

	public Integer getProfileCode() {
		return profileCode;
	}

	public void setProfileCode(Integer profileCode) {
		this.profileCode = profileCode;
	}

	public Integer getInstitutionTypeCode() {
		return institutionTypeCode;
	}

	public void setInstitutionTypeCode(Integer institutionTypeCode) {
		this.institutionTypeCode = institutionTypeCode;
	}

	public String getInstitutionTypeName() {
		return institutionTypeName;
	}

	public void setInstitutionTypeName(String institutionTypeName) {
		this.institutionTypeName = institutionTypeName;
	}

	public Integer getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(Integer institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Map<String, List<HashMap<String, Object>>> getMenuOptions() {
		return menuOptions;
	}

	public void setMenuOptions(
			Map<String, List<HashMap<String, Object>>> menuOptions) {
		this.menuOptions = menuOptions;
	}

	
}