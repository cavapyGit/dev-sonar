package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SystemProfileSituationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18-mar-2015
 */
public enum SystemProfileSituationType {

	/** The active. */
	ACTIVE(Integer.valueOf(145), "ACTIVO", "active.png"),
	 
	/** The pending lock. */
	PENDING_LOCK(Integer.valueOf(146), "BLOQUEO PENDIENTE", "pending_lock.png"),
	
	/** The pending unlock. */
	PENDING_UNLOCK(Integer.valueOf(147),"DESBLOQUEO PENDIENTE", "pending_unlock.png"),
	
	/** The pending rejected. */
	PENDING_REJECTED(Integer.valueOf(179),"PENDIENTE DE RECHAZAR", "pending_rejected.png"),
	
	/** The pending delete. */
	PENDING_DELETE(Integer.valueOf(166),"PENDIENTE DE ELIMINAR", "pending_delete.png"),
	
	/** The pending modificate. */
	PENDING_MODIFICATE(Integer.valueOf(180),"PENDIENTE DE MODIFICAR","pending_modificate.png");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<SystemProfileSituationType> list = new ArrayList<SystemProfileSituationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemProfileSituationType> lookup = new HashMap<Integer, SystemProfileSituationType>();

	static {
		for (SystemProfileSituationType s : EnumSet.allOf(SystemProfileSituationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new system profile situation type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private SystemProfileSituationType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the system profile situation type
	 */
	public static SystemProfileSituationType get(Integer code) {
		return lookup.get(code);
	}
}
