package com.pradera.security.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.security.model.type.ExternalInterfaceType;
import com.pradera.commons.security.model.type.UserAccountResponsabilityType;
import com.pradera.commons.security.model.type.UserAccountStateType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.ScheduleExceptionDetailStateType;
import com.pradera.security.model.type.UserAccountSituationType;
import com.pradera.security.model.type.UserAccountType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The persistent class for the USER_ACCOUNT database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 18/02/2013
 */
@Entity
@Table(name="USER_ACCOUNT")
@Cacheable(true)
public class UserAccount implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id user pk. */
	@Id
	@SequenceGenerator(name="USER_IDUSERPK_GENERATOR", sequenceName="SQ_ID_USER_ACCOUNT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_IDUSERPK_GENERATOR")
	@Column(name="ID_USER_PK")
	private Integer idUserPk;
	
	/** The login user. */
	@NotNull(message="{user.login.required}")
	@Column(name="LOGIN_USER")
	private String loginUser;
	
	/** The full name. */
	@NotNull(message="{user.name.required}")
	@Column(name="FULL_NAME")
	private String fullName;
	
	/** The firts last name. */
	@NotNull(message="{user.first.name.required}")
	@Column(name="FIRTS_LAST_NAME")
	private String firtsLastName;
	
	/** The second last name. */
	//@NotNull(message="{user.second.name.required}")
	@Column(name="SECOND_LAST_NAME")
	private String secondLastName;

	/** The change password. */
	@NotNull
	@Column(name="CHANGE_PASSWORD")
	private Integer changePassword;
	
	/** The mac restriction. */
	//@NotNull
	@Column(name="MAC_RESTRICTION")
	private Integer macRestriction;

	/** The mac address. */
	@Column(name="MAC_ADDRESS")
	private String macAddress;

    /** The confirmation date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="CONFIRMATION_DATE")
	private Date confirmationDate;

	/** The confirmation user. */
	@Column(name="CONFIRMATION_USER")
	private String confirmationUser;

	/** The document number. */
	@NotNull(message="{user.nro.document.required}")
	@Column(name="DOCUMENT_NUMBER")
	private String documentNumber;

	/** The document type. */
	@NotNull(message="{user.type.document.required}")
	@Column(name="DOCUMENT_TYPE")
	private Integer documentType;

	/** The id email. */
	@NotNull(message="{user.email.required}")
	@Pattern(message="{user.email.format}",regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
	@Column(name="ID_EMAIL")
	private String idEmail;

	/** The id issuer fk. */
	@Column(name="ID_ISSUER_FK")
	private String idIssuerFk;

	/** The id participant fk. */
	@Column(name="ID_PARTICIPANT_FK")
	private Long idParticipantFk;

	/** The last modify date. */
	@NotNull
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@NotNull
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@NotNull
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;
	
	/** The last modify user. */
	@NotNull
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The phone number. */
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

	/** The registry date. */
	@NotNull
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@NotNull
	@Column(name="REGISTRY_USER")
	private String registryUser;

    /** The reject date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REJECT_DATE")
	private Date rejectDate;

	/** The reject user. */
	@Column(name="REJECT_USER")
	private String rejectUser;

	/** The responsability. */
	@NotNull
	@Column(name="RESPONSABILITY")
	private Integer responsability;

	/** The state. */
	@NotNull
	@Column(name="\"STATE\"")
	private Integer state;
	
    /** The situation. */
    @Column(name="SITUATION")
	private Integer situation;

	/** The type. */
	@NotNull
	@Column(name="\"TYPE\"")
	private Integer type;
	
	/** The institution type fk. */
	@NotNull
	@Column(name="INSTITUTION_TYPE_FK")
	private Integer institutionTypeFk;
	
	/** The regulator agent. */
	@Column(name="REGULATOR_AGENT")
	private Integer regulatorAgent;
	
	/** The is selected. */
	@Transient
	private boolean isSelected;

	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/**  The RejectMotive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/** The annular motive. */
	@Column(name="CANCEL_MOTIVE")
	private Integer annularMotive;
	
	/** The annular other motive. */
	@Column(name="CANCEL_OTHER_MOTIVE")
	private String annularOtherMotive;
	
	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	/**  The Reject other Motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	/**  The ind external interface. */
	@Column(name="IND_EXT_INTERFACE")
	private Integer indExtInterface;

	//bi-directional many-to-one association to ExceptionUserPrivilege
	/** The exception user privileges. */
	@OneToMany(mappedBy="userAccount",fetch=FetchType.LAZY)
	private List<ExceptionUserPrivilege> exceptionUserPrivileges;

	//bi-directional many-to-one association to InstitutionTypeDetail
	/** The institution type details. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="userAccount",fetch=FetchType.LAZY)
	private List<InstitutionTypeDetail> institutionTypeDetails;

	//bi-directional many-to-one association to ScheduleExceptionDetail
	/** The schedule exception details. */
	@OneToMany(mappedBy="userAccount")
	private List<ScheduleExceptionDetail> scheduleExceptionDetails;

	//bi-directional many-to-one association to InstitutionsSecurity
    /** The institutions security. */
	@ManyToOne
	@JoinColumn(name="ID_SECURITY_INSTITUTION_FK")
    @NotNull
	private InstitutionsSecurity institutionsSecurity;

	//bi-directional many-to-one association to UserProfile
	/** The user profiles. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="userAccount",fetch=FetchType.LAZY)
	private List<UserProfile> userProfiles;

	//bi-directional many-to-one association to UserSession
	/** The user sessions. */
	@OneToMany(mappedBy="userAccount")
	private List<UserSession> userSessions;
	
	@OneToMany(cascade=CascadeType.PERSIST,mappedBy="userAccount")
	private List<UserProfileAllowed> userProfilesAllowed;
	
	/** The document file. */
	@Lob
    //@Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;
	
	/** The unregister file. */
	@Lob
    //@Basic(fetch=FetchType.LAZY)
	@Column(name="UNREGISTER_FILE")
	private byte[] unregisterFile;
	
	/** The indicator is recorder. */
	@Column(name="IND_IS_RECORDER")
	private Integer indIsRecorder;
	
	/** The indicator is committer. */
	@Column(name="IND_IS_COMMITTER")
	private Integer indIsCommitter;
	
	@Column(name="LAST_ACCESS_DATE")
	private Date lastAccessDate;
	
	@Column(name="LAST_PASSWORD_CHANGE")
	private Date lastPasswordChange;
	
	@Column(name="COUNT_REQUEST_PASSWORD")
	private Integer countRequestPassword;
	
	/** The subsidiary. */
	@ManyToOne
	@JoinColumn(name="ID_SUBSIDIARY_FK")    
	private Subsidiary subsidiary;
	
	/** The office user. */
	@Column(name="OFFICE")
	private String office;
	
	/** The office user. */
	@Column(name="NAME")
	private String name;
	
	@Column(name="IND_MFA_CODE")
	private Integer indMfaCode;
	
	@Column(name="MFA_SECRET")
	private String mfaSecret;
	
	/** The selected. */
	@Transient
	public boolean selected;
	
	/** The mac restriction bool. */
	@Transient
	private boolean macRestrictionBool;
	
	/** The regulator agent bool. */
	@Transient
	private boolean regulatorAgentBool;
	
	/** The External Interface boolean value. */
	@Transient
	private boolean extInterfaceBool;
	
	/** The internal user. */
	@Transient
	private boolean internalUser;
	
	/** The user type description. */
	@Transient
	private String userTypeDescription;
	
	/** The check box selected. */
	@Transient
	public boolean chkSelected;
	
	/** The user counter. */
	@Transient
	public Integer userCount;
	
	@Transient
	private String fDisplayName;
	
	@Transient
	private String institutionName;
	
	
	
    /**
     * Instantiates a new user account.
     */
    public UserAccount() {
    }

	/**
	 * Gets the id user pk.
	 *
	 * @return the id user pk
	 */
	public Integer getIdUserPk() {
		return this.idUserPk;
	}

	/**
	 * Sets the id user pk.
	 *
	 * @param idUserPk the new id user pk
	 */
	public void setIdUserPk(Integer idUserPk) {
		this.idUserPk = idUserPk;
	}

	/**
	 * Gets the change password.
	 *
	 * @return the change password
	 */
	public Integer getChangePassword() {
		return this.changePassword;
	}

	/**
	 * Sets the change password.
	 *
	 * @param changePassword the new change password
	 */
	public void setChangePassword(Integer changePassword) {
		this.changePassword = changePassword;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param isSelected the new selected
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	/**
	 * Gets the confirmation date.
	 *
	 * @return the confirmation date
	 */
	public Date getConfirmationDate() {
		return this.confirmationDate;
	}

	/**
	 * Sets the confirmation date.
	 *
	 * @param confirmationDate the new confirmation date
	 */
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return this.confirmationUser;
	}

	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return this.documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		if(documentNumber!=null){
			documentNumber=documentNumber.toUpperCase();
		}
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return this.documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the firts last name.
	 *
	 * @return the firts last name
	 */
	public String getFirtsLastName() {
		return this.firtsLastName;
	}

	/**
	 * Sets the firts last name.
	 *
	 * @param firtsLastName the new firts last name
	 */
	public void setFirtsLastName(String firtsLastName) {
		if(firtsLastName!=null){
			firtsLastName=firtsLastName.toUpperCase();
		}
		this.firtsLastName = firtsLastName;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return this.fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		if(fullName!=null){
			fullName=fullName.toUpperCase();
		}
		this.fullName = fullName;
	}

	/**
	 * Gets the id email.
	 *
	 * @return the id email
	 */
	public String getIdEmail() {
		return this.idEmail;
	}

	/**
	 * Sets the id email.
	 *
	 * @param idEmail the new id email
	 */
	public void setIdEmail(String idEmail) {
		if(idEmail!=null){
			idEmail=idEmail.toUpperCase();
		}
		this.idEmail = idEmail;
	}

	/**
	 * Gets the id issuer fk.
	 *
	 * @return the id issuer fk
	 */
	public String getIdIssuerFk() {
		return this.idIssuerFk;
	}

	/**
	 * Sets the id issuer fk.
	 *
	 * @param idIssuerFk the new id issuer fk
	 */
	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}

	/**
	 * Gets the id participant fk.
	 *
	 * @return the id participant fk
	 */
	public Long getIdParticipantFk() {
		return this.idParticipantFk;
	}

	/**
	 * Sets the id participant fk.
	 *
	 * @param idParticipantFk the new id participant fk
	 */
	public void setIdParticipantFk(Long idParticipantFk) {
		this.idParticipantFk = idParticipantFk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the login user.
	 *
	 * @return the login user
	 */
	public String getLoginUser() {
		return this.loginUser;
	}

	/**
	 * Sets the login user.
	 *
	 * @param loginUser the new login user
	 */
	public void setLoginUser(String loginUser) {
		if(loginUser!=null){
			loginUser=loginUser.toUpperCase();
		}
		this.loginUser = loginUser;
	}

	/**
	 * Gets the phone number.
	 *
	 * @return the phone number
	 */
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Sets the phone number.
	 *
	 * @param phoneNumber the new phone number
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return this.rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return this.rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the responsability.
	 *
	 * @return the responsability
	 */
	public Integer getResponsability() {
		return this.responsability;
	}

	/**
	 * Sets the responsability.
	 *
	 * @param responsability the new responsability
	 */
	public void setResponsability(Integer responsability) {
		this.responsability = responsability;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return this.secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		if(secondLastName!=null){
			secondLastName=secondLastName.toUpperCase();
		}
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public Integer getType() {
		return this.type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * Gets the regulator agent.
	 *
	 * @return the regulator agent
	 */
	public Integer getRegulatorAgent() {
		return regulatorAgent;
	}

	/**
	 * Sets the regulator agent.
	 *
	 * @param regulatorAgent the new regulator agent
	 */
	public void setRegulatorAgent(Integer regulatorAgent) {
		this.regulatorAgent = regulatorAgent;
	}

	/**
	 * Gets the exception user privileges.
	 *
	 * @return the exception user privileges
	 */
	public List<ExceptionUserPrivilege> getExceptionUserPrivileges() {
		return this.exceptionUserPrivileges;
	}

	/**
	 * Sets the exception user privileges.
	 *
	 * @param exceptionUserPrivileges the new exception user privileges
	 */
	public void setExceptionUserPrivileges(List<ExceptionUserPrivilege> exceptionUserPrivileges) {
		this.exceptionUserPrivileges = exceptionUserPrivileges;
	}
	
	/**
	 * Gets the institution type details.
	 *
	 * @return the institution type details
	 */
	public List<InstitutionTypeDetail> getInstitutionTypeDetails() {
		return this.institutionTypeDetails;
	}

	/**
	 * Sets the institution type details.
	 *
	 * @param institutionTypeDetails the new institution type details
	 */
	public void setInstitutionTypeDetails(List<InstitutionTypeDetail> institutionTypeDetails) {
		this.institutionTypeDetails = institutionTypeDetails;
	}
	
	/**
	 * Gets the schedule exception details.
	 *
	 * @return the schedule exception details
	 */
	public List<ScheduleExceptionDetail> getScheduleExceptionDetails() {
		return this.scheduleExceptionDetails;
	}

	/**
	 * Sets the schedule exception details.
	 *
	 * @param scheduleExceptionDetails the new schedule exception details
	 */
	public void setScheduleExceptionDetails(List<ScheduleExceptionDetail> scheduleExceptionDetails) {
		this.scheduleExceptionDetails = scheduleExceptionDetails;
	}
	
	/**
	 * Gets the institutions security.
	 *
	 * @return the institutions security
	 */
	public InstitutionsSecurity getInstitutionsSecurity() {
		return this.institutionsSecurity;
	}

	/**
	 * Sets the institutions security.
	 *
	 * @param institutionsSecurity the new institutions security
	 */
	public void setInstitutionsSecurity(InstitutionsSecurity institutionsSecurity) {
		this.institutionsSecurity = institutionsSecurity;
	}
	
	/**
	 * Gets the user profiles.
	 *
	 * @return the user profiles
	 */
	public List<UserProfile> getUserProfiles() {
		return this.userProfiles;
	}

	/**
	 * Sets the user profiles.
	 *
	 * @param userProfiles the new user profiles
	 */
	public void setUserProfiles(List<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}
	
	/**
	 * Gets the user sessions.
	 *
	 * @return the user sessions
	 */
	public List<UserSession> getUserSessions() {
		return this.userSessions;
	}

	/**
	 * Sets the user sessions.
	 *
	 * @param userSessions the new user sessions
	 */
	public void setUserSessions(List<UserSession> userSessions) {
		this.userSessions = userSessions;
	}

	/**
	 * Gets the institution type fk.
	 *
	 * @return the institution type fk
	 */
	public Integer getInstitutionTypeFk() {
		return institutionTypeFk;
	}

	/**
	 * Sets the institution type fk.
	 *
	 * @param institutionTypeFk the new institution type fk
	 */
	public void setInstitutionTypeFk(Integer institutionTypeFk) {
		this.institutionTypeFk = institutionTypeFk;
	}

	/**
	 * Checks if is regulator agent bool.
	 *
	 * @return true, if is regulator agent bool
	 */
	public boolean isRegulatorAgentBool() {
		if(regulatorAgent==null || regulatorAgent.intValue()==0){
			regulatorAgentBool=false;
		}else{
			regulatorAgentBool=true;
		}
		return regulatorAgentBool;
	}

	/**
	 * Sets the regulator agent bool.
	 *
	 * @param regulatorAgentBool the new regulator agent bool
	 */
	public void setRegulatorAgentBool(boolean regulatorAgentBool) {
		if(regulatorAgentBool){
			regulatorAgent=1;
		}else{
			regulatorAgent=0;
		}
		this.regulatorAgentBool = regulatorAgentBool;
	}

	/**
	 * Checks if is internal user.
	 *
	 * @return true, if is internal user
	 */
	public boolean isInternalUser() {
//		if(type!=null){
//			if(type.intValue()==UserAccountType.INTERNAL.getCode().intValue()){
//				internalUser=true;	  
//			}else if(type.intValue()==UserAccountType.EXTERNAL.getCode().intValue()){
//				internalUser=false;
//			}	
//		}
		return internalUser;
	}

	/**
	 * Sets the internal user.
	 *
	 * @param internalUser the new internal user
	 */
	public void setInternalUser(boolean internalUser) {
		this.internalUser = internalUser;
	}

	/**
	 * Checks if is mac restriction bool.
	 *
	 * @return true, if is mac restriction bool
	 */
	public boolean isMacRestrictionBool() {
		if(macRestriction==null || macRestriction.intValue()==0){
			macRestrictionBool=false;
		}else{
			macRestrictionBool=true;
		}
		return macRestrictionBool;
	}

	/**
	 * Sets the mac restriction bool.
	 *
	 * @param macRestrictionBool the new mac restriction bool
	 */
	public void setMacRestrictionBool(boolean macRestrictionBool) {
		if(macRestrictionBool){
			macRestriction=BooleanType.YES.getBinaryValue() ;
		}else{
			macRestriction=BooleanType.NO.getBinaryValue();
		}
		this.macRestrictionBool = macRestrictionBool;
	}

	/**
	 * Gets the mac restriction.
	 *
	 * @return the mac restriction
	 */
	public Integer getMacRestriction() {
		return macRestriction;
	}
	
	

	/**
	 * Gets the mac address.
	 *
	 * @return the mac address
	 */
	public String getMacAddress() {
		return macAddress;
	}

	/**
	 * Sets the mac address.
	 *
	 * @param macAddress the new mac address
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	/**
	 * Sets the mac restriction.
	 *
	 * @param macRestriction the new mac restriction
	 */
	public void setMacRestriction(Integer macRestriction) {
		this.macRestriction = macRestriction;
	}
	
	  /**
  	 * Gets the state icon.
  	 *
  	 * @return the state icon
  	 */
  	public String getStateIcon() {
			String icono = null;
			if (state != null) {
				icono = UserAccountStateType.get(state).getIcon();
			}
			return icono;
		}
		  
	    /**
    	 * Gets the state description.
    	 *
    	 * @return the state description
    	 */
    	public String getStateDescription() {
			String description = null;
			if (state != null) {
				description = UserAccountStateType.get(state).getValue();
			}
			return description;
		}
    	
    	/**
	     * Gets the situation description.
	     *
	     * @return the situation description
	     */
	    public String getSituationDescription() {
			String description = null;
			if (situation != null) {
				description = UserAccountSituationType.get(situation).getValue();
			}
			return description;
		}
	    
	    /**
    	 * Gets the responsability description.
    	 *
    	 * @return the responsability description
    	 */
    	public String getResponsabilityDescription(){
	    	String description=null;
	    	if(responsability!=null){
	    		description=UserAccountResponsabilityType.get(responsability).getValue();
	    	}
	    	return description;
	    }
	    
		/**
		 * Gets the user type description.
		 *
		 * @return the user type description
		 */
		public String getUserTypeDescription(){
	    	String description=null;
	    	if(type!=null){
	    		description=UserAccountType.get(type).getValue();
	    	}
	    	return description;
	    }
		
		/**
		 * Gets the institution type description.
		 *
		 * @return the institution type description
		 */
		public String getInstitutionTypeDescription(){
			String description=null;
			if(institutionTypeFk!=null){
				description=InstitutionType.get(institutionTypeFk).getValue();
			}
			return description;
		}

		/**
		 * Sets the user type description.
		 *
		 * @param userTypeDescription the new user type description
		 */
		public void setUserTypeDescription(String userTypeDescription) {
			this.userTypeDescription = userTypeDescription;
		}
		
		/**
		 * Gets the user account type description.
		 *
		 * @return the user account type description
		 */
		public String getUserAccountTypeDescription(){
			if(Validations.validateIsNotNull(this.getInstitutionTypeFk())){
				if(this.getInstitutionTypeFk().equals(InstitutionType.DEPOSITARY.getCode())){
					return UserAccountType.INTERNAL.getValue();
				}else{
					return UserAccountType.EXTERNAL.getValue();
				}
			}
			return "";
		}
		
	
		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((idUserPk == null) ? 0 : idUserPk.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UserAccount other = (UserAccount) obj;
			if (idUserPk == null) {
				if (other.idUserPk != null)
					return false;
			} else if (!idUserPk.equals(other.idUserPk))
				return false;
			return true;
		}

		/**
		 * Checks if is have schedule exception by system.
		 *
		 * @param idSystemParam the id system param
		 * @return true, if is have schedule exception by system
		 */
		public boolean isHaveScheduleExceptionBySystem(Integer idSystemParam){
	    	if(Validations.validateIsNotNullAndNotEmpty(this.getScheduleExceptionDetails()) && this.getScheduleExceptionDetails().size()!=0){
	    		for(ScheduleExceptionDetail scheduleExceptionDetail: this.getScheduleExceptionDetails()){
	    			if(idSystemParam.equals(scheduleExceptionDetail.getScheduleException().getSystem().getIdSystemPk()) && ScheduleExceptionDetailStateType.REGISTERED.getCode().equals(scheduleExceptionDetail.getState())){
	    				return Boolean.TRUE;
	    			}
	    		}
	    	}
	    	return Boolean.FALSE;
	    }

		/**
		 * Gets the block motive.
		 *
		 * @return the blockMotive
		 */
		public Integer getBlockMotive() {
			return blockMotive;
		}

		/**
		 * Sets the block motive.
		 *
		 * @param blockMotive the blockMotive to set
		 */
		public void setBlockMotive(Integer blockMotive) {
			this.blockMotive = blockMotive;
		}

		/**
		 * Gets the unblock motive.
		 *
		 * @return the unblockMotive
		 */
		public Integer getUnblockMotive() {
			return unblockMotive;
		}

		/**
		 * Sets the unblock motive.
		 *
		 * @param unblockMotive the unblockMotive to set
		 */
		public void setUnblockMotive(Integer unblockMotive) {
			this.unblockMotive = unblockMotive;
		}
		
		/**
		 * Gets the block other motive.
		 *
		 * @return the blockOtherMotive
		 */
		public String getBlockOtherMotive() {
			return blockOtherMotive;
		}

		/**
		 * Sets the block other motive.
		 *
		 * @param blockOtherMotive the blockOtherMotive to set
		 */
		public void setBlockOtherMotive(String blockOtherMotive) {
			this.blockOtherMotive = blockOtherMotive;
		}

		/**
		 * Gets the unblock other motive.
		 *
		 * @return the unblockOtherMotive
		 */
		public String getUnblockOtherMotive() {
			return unblockOtherMotive;
		}

		/**
		 * Sets the unblock other motive.
		 *
		 * @param unblockOtherMotive the unblockOtherMotive to set
		 */
		public void setUnblockOtherMotive(String unblockOtherMotive) {
			this.unblockOtherMotive = unblockOtherMotive;
		}
		
		/**
		 * Gets the reject motive.
		 *
		 * @return the rejectMotive
		 */
		public Integer getRejectMotive() {
			return rejectMotive;
		}

		public List<UserProfileAllowed> getUserProfilesAllowed() {
			return userProfilesAllowed;
		}

		public void setUserProfilesAllowed(List<UserProfileAllowed> userProfilesAllowed) {
			this.userProfilesAllowed = userProfilesAllowed;
		}

		/**
		 * Sets the reject motive.
		 *
		 * @param rejectMotive the rejectMotive to set
		 */
		public void setRejectMotive(Integer rejectMotive) {
			this.rejectMotive = rejectMotive;
		}

		/**
		 * Gets the reject other motive.
		 *
		 * @return the rejectOtherMotive
		 */
		public String getRejectOtherMotive() {
			return rejectOtherMotive;
		}

		/**
		 * Sets the reject other motive.
		 *
		 * @param rejectOtherMotive the rejectOtherMotive to set
		 */
		public void setRejectOtherMotive(String rejectOtherMotive) {
			this.rejectOtherMotive = rejectOtherMotive;
		}
		
		
		/**
		 * Checks if is chk selected.
		 *
		 * @return the chkSelected
		 */
		public boolean isChkSelected() {
			return chkSelected;
		}

		/**
		 * Sets the chk selected.
		 *
		 * @param chkSelected the chkSelected to set
		 */
		public void setChkSelected(boolean chkSelected) {
			this.chkSelected = chkSelected;
		}
		
		/**
		 * Checks if is ext interface bool.
		 *
		 * @return the extInterfaceBool
		 */
		public boolean isExtInterfaceBool() {
			if(indExtInterface==null || indExtInterface.equals(ExternalInterfaceType.NO.getCode())){
				extInterfaceBool=false;
			}else{
				extInterfaceBool=true;
			}
			return extInterfaceBool;
		}

		/**
		 * Sets the ext interface bool.
		 *
		 * @param extInterfaceBool the extInterfaceBool to set
		 */
		public void setExtInterfaceBool(boolean extInterfaceBool) {
			if(extInterfaceBool){
				indExtInterface=ExternalInterfaceType.YES.getCode();
			}else{
				indExtInterface=ExternalInterfaceType.NO.getCode();
			}
			this.extInterfaceBool = extInterfaceBool;
		}
		
		/**
		 * Gets the ind ext interface.
		 *
		 * @return the indExtInterface
		 */
		public Integer getIndExtInterface() {
			return indExtInterface;
		}


		/**
		 * Sets the ind ext interface.
		 *
		 * @param indExtInterface the indExtInterface to set
		 */
		public void setIndExtInterface(Integer indExtInterface) {
			this.indExtInterface = indExtInterface;
		}

		/**
		 * Gets the situation.
		 *
		 * @return the situation
		 */
		public Integer getSituation() {
			return situation;
		}

		/**
		 * Sets the situation.
		 *
		 * @param situation the new situation
		 */
		public void setSituation(Integer situation) {
			this.situation = situation;
		}

		/**
		 * Gets the user count.
		 *
		 * @return the userCount
		 */
		public Integer getUserCount() {
			return userCount;
		}
		
		/**
		 * Sets the user count.
		 *
		 * @param userCount the userCount to set
		 */
		public void setUserCount(Integer userCount) {
			this.userCount = userCount;
		}
		
		/**
		 * Gets the annular motive.
		 *
		 * @return the annular motive
		 */
		public Integer getAnnularMotive() {
			return annularMotive;
		}

		/**
		 * Sets the annular motive.
		 *
		 * @param annularMotive the new annular motive
		 */
		public void setAnnularMotive(Integer annularMotive) {
			this.annularMotive = annularMotive;
		}

		/**
		 * Gets the annular other motive.
		 *
		 * @return the annular other motive
		 */
		public String getAnnularOtherMotive() {
			return annularOtherMotive;
		}

		/**
		 * Sets the annular other motive.
		 *
		 * @param annularOtherMotive the new annular other motive
		 */
		public void setAnnularOtherMotive(String annularOtherMotive) {
			this.annularOtherMotive = annularOtherMotive;
		}

		/**
		 * Gets the document file.
		 *
		 * @return the document file
		 */
		public byte[] getDocumentFile() {
			return documentFile;
		}
		
		/**
		 * Sets the document file.
		 *
		 * @param documentFile the new document file
		 */
		public void setDocumentFile(byte[] documentFile) {
			this.documentFile = documentFile;
		}
		
		/**
		 * Gets the stream document file.
		 *
		 * @return the stream document file
		 */
		public StreamedContent getStreamDocumentFile(){
			InputStream is = null;
			if(documentFile!=null){
				is = new ByteArrayInputStream(documentFile);
				return new DefaultStreamedContent(is,"application/pdf","document");
			}
			return null;
		}
		
		/**
		 * Gets the unregister file.
		 *
		 * @return the unregister file
		 */
		public byte[] getUnregisterFile() {
			return unregisterFile;
		}
		
		/**
		 * Sets the unregister file.
		 *
		 * @param unregisterFile the new unregister file
		 */
		public void setUnregisterFile(byte[] unregisterFile) {
			this.unregisterFile = unregisterFile;
		}
		
		/**
		 * Gets the have unregister doc.
		 *
		 * @return the have unregister doc
		 */
		public boolean getHaveUnregisterDoc(){
			return unregisterFile!=null;
		}
		
		/**
		 * Gets the stream unregister doc.
		 *
		 * @return the stream unregister doc
		 */
		public StreamedContent getStreamUnregisterDoc(){
			InputStream is = null;
			if(unregisterFile!=null){
				is = new ByteArrayInputStream(unregisterFile);
				return new DefaultStreamedContent(is,"application/pdf","document");
			}
			return null;
		}
		/* (non-Javadoc)
		 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
		 */
		@Override
	    public void setAudit(LoggerUser loggerUser) {
	        // TODO review casuisticas no contempladas para mejorar
	        if (loggerUser != null) {
	        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
	            lastModifyDate = loggerUser.getAuditTime();
	            lastModifyIp = loggerUser.getIpAddress();
	            lastModifyUser = loggerUser.getUserName();
	        }
	    }
		/* (non-Javadoc)
		 * @see com.pradera.commons.audit.Auditable#getListForAudit()
		 */
		@Override
		public Map<String, List<? extends Auditable>> getListForAudit() {
			HashMap<String,List<? extends Auditable>> detailsMap = 
	                new HashMap<String, List<? extends Auditable>>();  
			detailsMap.put("exceptionUserPrivileges", exceptionUserPrivileges);
			detailsMap.put("institutionTypeDetails", institutionTypeDetails);
			detailsMap.put("scheduleExceptionDetails", scheduleExceptionDetails);
			detailsMap.put("userProfiles", userProfiles);
			detailsMap.put("userSessions", userSessions);
			detailsMap.put("userProfilesAllowed", userProfilesAllowed);
	        return detailsMap;
		}

		public String getfDisplayName() {
			return fDisplayName;
		}

		public void setfDisplayName(String fDisplayName) {
			this.fDisplayName = fDisplayName;
		}

		public Integer getIndIsRecorder() {
			return indIsRecorder;
		}

		public void setIndIsRecorder(Integer indIsRecorder) {
			this.indIsRecorder = indIsRecorder;
		}

		public Integer getIndIsCommitter() {
			return indIsCommitter;
		}

		public void setIndIsCommitter(Integer indIsCommitter) {
			this.indIsCommitter = indIsCommitter;
		}

		public Date getLastAccessDate() {
			return lastAccessDate;
		}

		public void setLastAccessDate(Date lastAccessDate) {
			this.lastAccessDate = lastAccessDate;
		}

		public Integer getCountRequestPassword() {
			return countRequestPassword;
		}

		public void setCountRequestPassword(Integer countRequestPassword) {
			this.countRequestPassword = countRequestPassword;
		}

		public Date getLastPasswordChange() {
			return lastPasswordChange;
		}

		public void setLastPasswordChange(Date lastPasswordChange) {
			this.lastPasswordChange = lastPasswordChange;
		}

		/**
		 * @return the subsidiary
		 */
		public Subsidiary getSubsidiary() {
			return subsidiary;
		}

		/**
		 * @param subsidiary the subsidiary to set
		 */
		public void setSubsidiary(Subsidiary subsidiary) {
			this.subsidiary = subsidiary;
		}

		/**
		 * @return the office
		 */
		public String getOffice() {
			return office;
		}

		/**
		 * @param office the office to set
		 */
		public void setOffice(String office) {
			this.office = office;
		}

		/**
		 * @return the institutionName
		 */
		public String getInstitutionName() {
			return institutionName;
		}

		/**
		 * @param institutionName the institutionName to set
		 */
		public void setInstitutionName(String institutionName) {
			this.institutionName = institutionName;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the indMfaCode
		 */
		public Integer getIndMfaCode() {
			return indMfaCode;
		}

		/**
		 * @param indMfaCode the indMfaCode to set
		 */
		public void setIndMfaCode(Integer indMfaCode) {
			this.indMfaCode = indMfaCode;
		}

		/**
		 * @return the mfaSecret
		 */
		public String getMfaSecret() {
			return mfaSecret;
		}

		/**
		 * @param mfaSecret the mfaSecret to set
		 */
		public void setMfaSecret(String mfaSecret) {
			this.mfaSecret = mfaSecret;
		}
		
		
		
}