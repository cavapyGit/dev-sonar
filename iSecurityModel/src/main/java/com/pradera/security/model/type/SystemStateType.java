package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum SystemStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum SystemStateType {

	
	/** The registered. */
	REGISTERED(Integer.valueOf(23), "REGISTRADO", "registered.png"),
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(24), "BLOQUEADO", "blocked.png"),
	
//	REGISTERED(Integer.valueOf(37), "REGISTRADO", "registered.png"),
//	 
//	BLOCKED(Integer.valueOf(38), "BLOQUEADO", "blocked.png"),
//	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(137),"CONFIRMADO","confirmed.png"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(138),"RECHAZADO","rejected.png");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<SystemStateType> list = new ArrayList<SystemStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemStateType> lookup = new HashMap<Integer, SystemStateType>();

	static {
		for (SystemStateType s : EnumSet.allOf(SystemStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new system state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private SystemStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the system state type
	 */
	public static SystemStateType get(Integer code) {
		return lookup.get(code);
	}

}




