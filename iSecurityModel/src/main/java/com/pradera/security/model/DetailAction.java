package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the DETAIL_ACTION database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name = DetailAction.DETAIL_ACTION_TABLE_SEARCH_BY_ACTION_TABLE_PK, query = "select da.fieldName,da.isInsertable,da.isUpdatable From DetailAction da Where da.actionTable.idActionTablesPk = :idActionTablesPk",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") })
})

@Entity
@Table(name = "DETAIL_ACTION")
public class DetailAction implements Serializable,Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant DETAIL_ACTION_TABLE_SEARCH_BY_ACTION_TABLE_PK. */
	public static final String DETAIL_ACTION_TABLE_SEARCH_BY_ACTION_TABLE_PK = "DetailAction.searchByActionTablePK";
	
	/** The id detail action pk. */
	@Id
	@SequenceGenerator(name="DETAIL_ACTION_GENERATOR", sequenceName="SQ_ID_DETAIL_ACTION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DETAIL_ACTION_GENERATOR")
	@Column(name="ID_DETAIL_ACTION_PK")
	private long idDetailActionPk;
	
	/** The field name. */
	@Column(name = "FIELD_NAME")
	private String fieldName;
	
	/** The is insertable. */
	@Column(name = "IS_INSERTABLE")
	private long isInsertable;
	
	/** The is updatable. */
	@Column(name ="IS_UPDATABLE")
	private long isUpdatable;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The action table. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ACTION_TABLES_FK", referencedColumnName="ID_ACTION_TABLES_PK")
	private ActionTable actionTable;

	/**
	 * Gets the id detail action pk.
	 *
	 * @return the idDetailActionPk
	 */
	public long getIdDetailActionPk() {
		return idDetailActionPk;
	}

	/**
	 * Sets the id detail action pk.
	 *
	 * @param idDetailActionPk the idDetailActionPk to set
	 */
	public void setIdDetailActionPk(long idDetailActionPk) {
		this.idDetailActionPk = idDetailActionPk;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the checks if is insertable.
	 *
	 * @return the isInsertable
	 */
	public long getIsInsertable() {
		return isInsertable;
	}

	/**
	 * Sets the checks if is insertable.
	 *
	 * @param isInsertable the isInsertable to set
	 */
	public void setIsInsertable(long isInsertable) {
		this.isInsertable = isInsertable;
	}

	/**
	 * Gets the checks if is updatable.
	 *
	 * @return the isUpdatable
	 */
	public long getIsUpdatable() {
		return isUpdatable;
	}

	/**
	 * Sets the checks if is updatable.
	 *
	 * @param isUpdatable the isUpdatable to set
	 */
	public void setIsUpdatable(long isUpdatable) {
		this.isUpdatable = isUpdatable;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the action table.
	 *
	 * @return the actionTable
	 */
	public ActionTable getActionTable() {
		return actionTable;
	}

	/**
	 * Sets the action table.
	 *
	 * @param actionTable the actionTable to set
	 */
	public void setActionTable(ActionTable actionTable) {
		this.actionTable = actionTable;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	
}
