package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum UserAccountType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum UserAccountType {
	
	/** The internal. */
	INTERNAL(Integer.valueOf(33),"INTERNO"),
	
	/** The external. */
	EXTERNAL(Integer.valueOf(34),"EXTERNO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<UserAccountType> list = new ArrayList<UserAccountType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, UserAccountType> lookup = new HashMap<Integer, UserAccountType>();

	static {
		for (UserAccountType s : EnumSet.allOf(UserAccountType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new user account type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private UserAccountType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the user account type
	 */
	public static UserAccountType get(Integer code) {
		return lookup.get(code);
	}
	
}