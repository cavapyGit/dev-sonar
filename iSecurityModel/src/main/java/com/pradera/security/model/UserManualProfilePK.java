package com.pradera.security.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the USER_MANUAL_PROFILE database table.
 * 
 */
@Embeddable
public class UserManualProfilePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_SYSTEM_OPTION_PK")
	private Integer idSystemOptionPk;

	@Column(name="ID_SYSTEM_PROFILE_PK")
	private Integer idSystemProfilePk;

	public UserManualProfilePK() {
	}
	public Integer getIdSystemOptionPk() {
		return this.idSystemOptionPk;
	}
	public void setIdSystemOptionPk(Integer idSystemOptionPk) {
		this.idSystemOptionPk = idSystemOptionPk;
	}
	public Integer getIdSystemProfilePk() {
		return this.idSystemProfilePk;
	}
	public void setIdSystemProfilePk(Integer idSystemProfilePk) {
		this.idSystemProfilePk = idSystemProfilePk;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserManualProfilePK)) {
			return false;
		}
		UserManualProfilePK castOther = (UserManualProfilePK)other;
		return 
			(this.idSystemOptionPk.equals(castOther.idSystemOptionPk))
			&& (this.idSystemProfilePk.equals(castOther.idSystemProfilePk));
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idSystemOptionPk ^ (this.idSystemOptionPk >>> 20)));
		return hash;
	}
}