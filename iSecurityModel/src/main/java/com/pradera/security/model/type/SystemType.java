package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

public enum SystemType {
	
	SECURITY(Integer.valueOf(1),"SEGURIDAD"),
	
	CSDCORE(Integer.valueOf(2),"CSDCORE");
	
	private Integer code;
	
	private String value;

	/** The Constant list. */
	public static final List<SystemType> list = new ArrayList<SystemType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemType> lookup = new HashMap<Integer, SystemType>();

	static {
		for (SystemType s : EnumSet.allOf(SystemType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private SystemType(Integer code, String value) {
		this.code = code;
		this.value = value;	
	}
	

	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}	

	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the state type
	 */
	public static SystemType get(Integer code) {
		return lookup.get(code);
	}
	
	public static List<StateType> listSomeElements(StateType... transferSecuritiesTypeParams){
		List<StateType> retorno = new ArrayList<StateType>();
		for(StateType TransferSecuritiesType: transferSecuritiesTypeParams){
			retorno.add(TransferSecuritiesType);
		}
		return retorno;
	}
	
}
