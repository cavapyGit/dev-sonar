package com.pradera.security.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.SystemSituationType;
import com.pradera.security.model.type.SystemStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class System.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 11/03/2014
 */
@NamedQueries({
	@NamedQuery(name = System.SYSTEM_SEARCH_BY_STATE, query = " Select S From System S Where S.state=:state order by S.name",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") }),
	@NamedQuery(name = System.SYSTEM_SEARCH_ALL, query = " Select S From System S order by S.name",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") }),
})
@Entity
@Table(name="\"SYSTEM\"")
@Cacheable(true)
public class System implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant SYSTEM_SEARCH_BY_STATE. */
	public static final String SYSTEM_SEARCH_BY_STATE = "System.searchByState";
	
	/** The Constant SYSTEM_SEARCH_ALL. */
	public static final String SYSTEM_SEARCH_ALL = "System.searchAll";
	
	/** The id system pk. */
	@Id
	@SequenceGenerator(name="SYSTEM_IDSYSTEMPK_GENERATOR", sequenceName="SQ_ID_SYSTEM_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SYSTEM_IDSYSTEMPK_GENERATOR")
	@Column(name="ID_SYSTEM_PK")
	@NotNull
	private Integer idSystemPk;
	
//	@Version
	private Long version;

	/** The description. */
	@NotNull(message="{system.description.required}")
	private String description;

	/** The mnemonic. */
	@NotNull(message="{system.mnemonic.required}")
	private String mnemonic;

	/** The name. */
	@NotNull(message="{system.name.required}")
	private String name;

    /** The state. */
    @NotNull
	private Integer state;

    /** The system image. */
    @NotNull
    @Lob
    //@Basic(fetch=FetchType.LAZY)
	@Column(name="SYSTEM_IMAGE")
	private byte[] systemImage;

	/** The system url. */
	@NotNull(message="{system.url.required}")
	//@Pattern(message="{system.url.format}" , regexp="^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
	@Column(name="SYSTEM_URL")
	private String systemUrl;
	
	/** The registry date. */
	@NotNull
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@NotNull
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The last modify date. */
	@NotNull
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@NotNull
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@NotNull
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@NotNull
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** Campo que indica si el icono del sistema se mostrara en la pantalla de seleccionar sistema */
	@NotNull
	@Column(name="IND_DISPLAY_ICON")
	private Integer indDisplayIcon;

	//bi-directional many-to-one association to ScheduleException
	/** The schedule exceptions. */
	@OneToMany(mappedBy="system")
	private List<ScheduleException> scheduleExceptions;

	//bi-directional many-to-one association to Schedule
	/** The schedule. */
	@ManyToOne(fetch = FetchType.LAZY,cascade=CascadeType.DETACH)
	@JoinColumn(name="ID_SCHEDULE_FK")
	private Schedule schedule;

	//bi-directional many-to-one association to SystemOption
	/** The system options. */
	@OneToMany(mappedBy="system")
	private List<SystemOption> systemOptions;

	//bi-directional many-to-one association to SystemProfile
	/** The system profiles. */
	@OneToMany(mappedBy="system")
	private List<SystemProfile> systemProfiles;

	//bi-directional many-to-one association to UserSession
	/** The user sessions. */
	@OneToMany(mappedBy="system")
	private List<UserSession> userSessions;
	
	/** The option url. */
	@Transient
	private String optionUrl; 
	
	/** The schema db. */
	@NotNull
	@Column(name = "SCHEMA_DB")
	private String schemaDb;
	
	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;

	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;

	/** The reject other motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	

    /** The situation. */
    @Column(name="SITUATION")
	private Integer situation;
    
    /** The system production file. */
    @NotNull
    @Lob
	@Column(name="DOCUMENT_FILE")
	private byte[] systemProductionFile;
    
	
	/**            
	 * Instantiates a new system.
	 */
	public System() {
	
    }
	
	/**
	 * Gets the schema db.
	 *
	 * @return the schemaDb
	 */
	public String getSchemaDb() {
		return schemaDb;
	}

	/**
	 * Sets the schema db.
	 *
	 * @param schemaDb the schemaDb to set
	 */
	public void setSchemaDb(String schemaDb) {
		this.schemaDb = schemaDb;
	}

	/**
	 * Gets the id system pk.
	 *
	 * @return the id system pk
	 */
	public Integer getIdSystemPk() {
		return idSystemPk;
	}

	/**
	 * Sets the id system pk.
	 *
	 * @param idSystemPk the new id system pk
	 */
	public void setIdSystemPk(Integer idSystemPk) {
		this.idSystemPk = idSystemPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null){
			description=description.toUpperCase();
		}
		this.description = description;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		if(mnemonic!=null){
			mnemonic=mnemonic.toUpperCase();
		}
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		if(name!=null){
			name=name.toUpperCase();
		}
		this.name = name;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the system image.
	 *
	 * @return the system image
	 */
	public byte[] getSystemImage() {
		return systemImage;
	}

	/**
	 * Sets the system image.
	 *
	 * @param systemImage the new system image
	 */
	public void setSystemImage(byte[] systemImage) {
		this.systemImage = systemImage;
	}

	/**
	 * Gets the system url.
	 *
	 * @return the system url
	 */
	public String getSystemUrl() {
		return systemUrl;
	}

	/**
	 * Sets the system url.
	 *
	 * @param systemUrl the new system url
	 */
	public void setSystemUrl(String systemUrl) {
		this.systemUrl = systemUrl;
	}

	/**
	 * Gets the schedule exceptions.
	 *
	 * @return the schedule exceptions
	 */
	public List<ScheduleException> getScheduleExceptions() {
		return scheduleExceptions;
	}

	/**
	 * Sets the schedule exceptions.
	 *
	 * @param scheduleExceptions the new schedule exceptions
	 */
	public void setScheduleExceptions(List<ScheduleException> scheduleExceptions) {
		this.scheduleExceptions = scheduleExceptions;
	}

	/**
	 * Gets the schedule.
	 *
	 * @return the schedule
	 */
	public Schedule getSchedule() {
		return schedule;
	}

	/**
	 * Sets the schedule.
	 *
	 * @param schedule the new schedule
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/**
	 * Gets the system options.
	 *
	 * @return the system options
	 */
	public List<SystemOption> getSystemOptions() {
		return systemOptions;
	}

	/**
	 * Sets the system options.
	 *
	 * @param systemOptions the new system options
	 */
	public void setSystemOptions(List<SystemOption> systemOptions) {
		this.systemOptions = systemOptions;
	}

	/**
	 * Gets the system profiles.
	 *
	 * @return the system profiles
	 */
	public List<SystemProfile> getSystemProfiles() {
		return systemProfiles;
	}

	/**
	 * Sets the system profiles.
	 *
	 * @param systemProfiles the new system profiles
	 */
	public void setSystemProfiles(List<SystemProfile> systemProfiles) {
		this.systemProfiles = systemProfiles;
	}

	/**
	 * Gets the user sessions.
	 *
	 * @return the user sessions
	 */
	public List<UserSession> getUserSessions() {
		return userSessions;
	}

	/**
	 * Sets the user sessions.
	 *
	 * @param userSessions the new user sessions
	 */
	public void setUserSessions(List<UserSession> userSessions) {
		this.userSessions = userSessions;
	}	

	/**
	 * Gets the option url.
	 *
	 * @return the option url
	 */
	public String getOptionUrl() {
		return optionUrl;
	}

	/**
	 * Sets the option url.
	 *
	 * @param optionUrl the new option url
	 */
	public void setOptionUrl(String optionUrl) {
		this.optionUrl = optionUrl;
	}
	
    /**
     * Gets the state icon.
     *
     * @return the state icon
     */
    public String getStateIcon() {
		String icon = null;
		if (state != null) {
			if(SystemStateType.get(state) != null)
			icon = SystemStateType.get(state).getIcon();
		}
		return icon;
	}
    
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		  String descripcion = null;
		  if (state != null) {
			  if(SystemStateType.get(state) != null)
		      descripcion = SystemStateType.get(state).getValue();
		  }
		  return descripcion;
	}
	
	/**
	 * Gets the situation description.
	 *
	 * @return the situation description
	 */
	public String getSituationDescription() {
		  String descripcion = null;
		  if (situation != null) {
			  if(SystemSituationType.get(situation) != null)
		      descripcion = SystemSituationType.get(situation).getValue();
		  }
		  return descripcion;
	}
	
	/**
	 * Gets the streamed image.
	 *
	 * @return the streamed image
	 */
	public StreamedContent getStreamedImage(){
		StreamedContent streamedImagen=null;
		if(systemImage!=null){
			InputStream input = new ByteArrayInputStream(systemImage);
			streamedImagen=new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return streamedImagen;
	}
	
	/**
	 * Gets the streamed file.
	 *
	 * @return the streamed file
	 */
	public StreamedContent getStreamedFile(){
		StreamedContent streamedFile=null;
		if(systemProductionFile!=null){
			InputStream input = new ByteArrayInputStream(systemProductionFile);
			try{
				streamedFile=new DefaultStreamedContent(input,"application/pdf","document");
			}
			catch(Exception ex){
				
			}
			
		}
		return streamedFile;
	}

	/**
	 * Gets the system production file.
	 *
	 * @return the system production file
	 */
	public byte[] getSystemProductionFile() {
		return systemProductionFile;
	}

	/**
	 * Sets the system production file.
	 *
	 * @param systemProductionFile the new system production file
	 */
	public void setSystemProductionFile(byte[] systemProductionFile) {
		this.systemProductionFile = systemProductionFile;
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}
	
	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	/**
	 * Sets the block other motive.
	 *
	 * @param blockOtherMotive the blockOtherMotive to set
	 */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	/**
	 * Sets the unblock other motive.
	 *
	 * @param unblockOtherMotive the unblockOtherMotive to set
	 */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}

	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("scheduleExceptions", scheduleExceptions);
		detailsMap.put("systemOptions", systemOptions);
		detailsMap.put("systemProfiles", systemProfiles);
		detailsMap.put("userSessions", userSessions);
        return detailsMap;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getIndDisplayIcon() {
		return indDisplayIcon;
	}

	public void setIndDisplayIcon(Integer indDisplayIcon) {
		this.indDisplayIcon = indDisplayIcon;
	}
	
}