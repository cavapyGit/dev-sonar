package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum Daytype.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum AuditProcessType {

	/** The opciones. */
	OPCIONES(Integer.valueOf(1),"OPCIONES"),
	
	/** The reportes. */
	REPORTES(Integer.valueOf(2),"REPORTES");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;		
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
			
	/**
	 * Instantiates a new audit process type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private AuditProcessType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/** The Constant list. */
	public static final List<AuditProcessType> list = new ArrayList<AuditProcessType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, AuditProcessType> lookup = new HashMap<Integer, AuditProcessType>();
	static {
		for (AuditProcessType s : EnumSet.allOf(AuditProcessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
