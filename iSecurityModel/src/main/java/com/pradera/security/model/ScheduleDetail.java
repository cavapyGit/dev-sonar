package com.pradera.security.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.commons.type.DayType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the SCHEDULE_DETAIL database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="SCHEDULE_DETAIL")
public class ScheduleDetail implements Serializable,Cloneable,Auditable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id schedule detail pk. */
	@Id
	@SequenceGenerator(name = "SCHEDULE_DETAIL_PK_GENERATOR", sequenceName = "SQ_ID_SCHEDULE_DETAIL_PK", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCHEDULE_DETAIL_PK_GENERATOR")
	@Column(name="ID_SCHEDULE_DETAIL_PK")
	@NotNull
	private long idScheduleDetailPk;

	/** The day. */
	@Column(name="\"DAY\"")
	@NotNull
	private Integer day;

    /** The end date. */
    @Temporal( TemporalType.TIME)
	@Column(name="END_DATE")
    @NotNull
	private Date endDate;

    /** The initial date. */
    @Temporal( TemporalType.TIME)
	@Column(name="INITIAL_DATE")
    @NotNull
	private Date initialDate;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;
	
	/**
	 * The selected.
	 *
	 * @author mmacalupu
	 * this atribute has relationship with checkbox
	 * in datatable(.xhtml) and show if that register were 
	 * selected o No.
	 */
	@Transient
	private boolean selected;

	//bi-directional many-to-one association to Schedule
    /** The schedule. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SCHEDULE_FK", referencedColumnName = "ID_SCHEDULE_PK")
	private Schedule schedule;

    /**
     * Instantiates a new schedule detail.
     */
    public ScheduleDetail() {
    }

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Gets the id schedule detail pk.
	 *
	 * @return the id schedule detail pk
	 */
	public long getIdScheduleDetailPk() {
		return this.idScheduleDetailPk;
	}

	/**
	 * Sets the id schedule detail pk.
	 *
	 * @param idScheduleDetailPk the new id schedule detail pk
	 */
	public void setIdScheduleDetailPk(long idScheduleDetailPk) {
		this.idScheduleDetailPk = idScheduleDetailPk;
	}

	/**
	 * Gets the day.
	 *
	 * @return the day
	 */
	public Integer getDay() {
		return day;
	}

	/**
	 * Sets the day.
	 *
	 * @param day the new day
	 */
	public void setDay(Integer day) {
		this.day = day;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return this.initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the schedule.
	 *
	 * @return the schedule
	 */
	public Schedule getSchedule() {
		return this.schedule;
	}

	/**
	 * Sets the schedule.
	 *
	 * @param schedule the new schedule
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public ScheduleDetail clone(){
		try {
			return (ScheduleDetail)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the day name.
	 *
	 * @return the day name
	 */
	public String getDayName(){
		return DayType.lookup.get(this.day).getValue();
	}
	
	/**
	 * Gets the minute minimun.
	 *
	 * @author mmacalupu
	 * this method is useful to get the first minute available on "endDate" atributte,
	 * for example if initialDate is 10:23 then the fist Hour available for finalDate 
	 * should be 10:24
	 * @return the minute minimun
	 */
	public Integer getMinuteMinimun(){
		if(Validations.validateIsNull(this.getInitialDate()) || Validations.validateIsNull(this.getEndDate())){
			return 0;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(this.getInitialDate());
		int minuteInitialDate = calendar.get(Calendar.MINUTE);
		int hourInitialDate = calendar.get(Calendar.HOUR_OF_DAY);
		
		calendar.setTime(this.getEndDate());
		int hourEndDate = calendar.get(Calendar.HOUR_OF_DAY);
		if(hourInitialDate!=hourEndDate){
			return 0;
		}
		return minuteInitialDate;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
}