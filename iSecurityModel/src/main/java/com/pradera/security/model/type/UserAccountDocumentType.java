package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum UserAccountDocumentType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum UserAccountDocumentType{

	/** The ci. */
	CI(Integer.valueOf(86),"CEDULA DE IDENTIDAD"), 
	
	/** The nit. */
	RUC(Integer.valueOf(386),"REGISTRO UNICO DEL CONTRIBUYENTE (RUC)"),
	
	/** The cie. */
	CIE(Integer.valueOf(387),"CEDULA DE IDENTIDAD DE EXTRANJEROS"),
	
	/** The cdn. */
	CDN(Integer.valueOf(388),"CERTIFICADO DE NACIMIENTO"),	
	
	/** The pas. */
	PAS(Integer.valueOf(389),"PASAPORTE"),
	
	/** The dio. */
	DIO(Integer.valueOf(390),"DOCUMENTO DE IDENTIDAD DE PAIS ORIGEN");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new user account document type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private UserAccountDocumentType(int ordinal,String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/** The Constant list. */
	public static final List<UserAccountDocumentType> list = new ArrayList<UserAccountDocumentType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, UserAccountDocumentType> lookup = new HashMap<Integer, UserAccountDocumentType>();

	static {
		for (UserAccountDocumentType s : EnumSet.allOf(UserAccountDocumentType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the user account document type
	 */
	public static UserAccountDocumentType get(Integer code) {
		return lookup.get(code);
	}
}