package com.pradera.security.model.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum UserExceptionSituationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 06-mar-2015
 */
public enum UserExceptionSituationType {
	
	/** The active. */
	ACTIVE(Integer.valueOf(167),"ACTIVO"),
	
	/** The pending delete. */
	PENDING_DELETE(Integer.valueOf(166),"PENDIENTE DE ELIMINAR");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant lookup. */
	public static final Map<Integer, UserExceptionSituationType> lookup = new HashMap<Integer, UserExceptionSituationType>();

	static {
		for (UserExceptionSituationType s : EnumSet.allOf(UserExceptionSituationType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the user exception situation type
	 */
	public static UserExceptionSituationType get(Integer code) {
		return lookup.get(code);
	}
	
	/**
	 * Instantiates a new user exception situation type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private UserExceptionSituationType(Integer code,String value){
		this.code=code;
		this.value=value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
