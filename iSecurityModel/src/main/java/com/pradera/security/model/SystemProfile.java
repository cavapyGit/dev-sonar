package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.SystemProfileSituationType;
import com.pradera.security.model.type.SystemProfileStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the SYSTEM_PROFILE database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date : 27/11/2012
 */
@Entity
@Table(name="SYSTEM_PROFILE")
@Cacheable(true)
public class SystemProfile implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id system profile pk. */
	@Id
	@SequenceGenerator(name="SYSTEM_PROFILE_IDSYSTEMPROFILEPK_GENERATOR", sequenceName="SQ_ID_SYSTEM_PROFILE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SYSTEM_PROFILE_IDSYSTEMPROFILEPK_GENERATOR")	
	@Column(name="ID_SYSTEM_PROFILE_PK")
	private Integer idSystemProfilePk;

    /** The last modify date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
    @NotNull
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	@NotNull
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	@NotNull
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	@NotNull
	private String lastModifyUser;

	/** The mnemonic profile. */
	@Column(name="MNEMONIC")
	//@NotNull(message = "{profile.mnemonic}")
	private String mnemonic;

	/** The profile description. */
	@Column(name="DESCRIPTION")
	//@NotNull(message = "{profile.description}")
	private String description;

	/** The profile name. */
	@Column(name="NAME")
	//@NotNull(message = "{profile.name}")
	private String name;

    /** The registry date. */
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;
	
    /** The situation. */
    @Column(name="SITUATION")
	private Integer situation;
	
	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/** The reject motive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	/** The reject other motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	//bi-directional many-to-one association to ProfilePrivilege
	/** The profile privileges. */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="systemProfile")
	private List<ProfilePrivilege> profilePrivileges;

	//bi-directional many-to-one association to System
    /** The system. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SYSTEM_FK")
    @NotNull
	private System system;

	//bi-directional many-to-one association to UserProfile
	/** The user profiles. */
	@OneToMany(mappedBy="systemProfile")
	private List<UserProfile> userProfiles;
	
	/** The indicator user type. */
	@Column(name="IND_USER_TYPE")
	private Integer indUserType;
	
	/** The institution type. */
	@Column(name="INSTITUTION_TYPE")
	private Integer institutionType;
	
	@Column(name="DELETE_OTHER_MOTIVE")
	private String deleteOtherMotive;
	
	/** The profile type. */
    @Column(name="PROFILE_TYPE")
	private Integer profileType;
	
	@Transient
	private boolean isExtern;
	@Transient
	private boolean isIntern;
	
	@Transient
	private boolean rejectProfile =false;
	
	@Transient
	private String institutionName;

    /**
     * Instantiates a new system profile.
     */
    public SystemProfile() {
    }
    
    /**
     * Gets the profile state description.
     *
     * @return the profile state description
     */
    public String getProfileStateDescription() {
		String description = null;
		if (state != null) {
			description = SystemProfileStateType.get(state).getValue();
		}
		return description;
	}
    
    /**
     * Gets the profile situation description.
     *
     * @return the profile situation description
     */
    public String getProfileSituationDescription() {
		String description = null;
		if (situation != null) {
			description = SystemProfileSituationType.get(situation).getValue();
		}
		return description;
	}
    
    /**
     * Gets the icono estado.
     *
     * @return the icono estado
     */
    public String getStateIcon() {
		String icon = null;
		if (state != null) {
			icon = SystemProfileStateType.get(state).getIcon();
		}
		return icon;
	}

	/**
	 * Gets the id system profile pk.
	 *
	 * @return the id system profile pk
	 */
	public Integer getIdSystemProfilePk() {
		return idSystemProfilePk;
	}

	/**
	 * Sets the id system profile pk.
	 *
	 * @param idSystemProfilePk the new id system profile pk
	 */
	public void setIdSystemProfilePk(Integer idSystemProfilePk) {
		this.idSystemProfilePk = idSystemProfilePk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the new mnemonic
	 */
	public void setMnemonic(String mnemonic) {
		if(mnemonic!=null){
			mnemonic = mnemonic.toUpperCase();
		}
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null){
			description = description.toUpperCase();
		}
		this.description = description;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		if(name!=null){
			name = name.toUpperCase();
		}
		this.name = name;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the profile privileges.
	 *
	 * @return the profile privileges
	 */
	public List<ProfilePrivilege> getProfilePrivileges() {
		return profilePrivileges;
	}

	/**
	 * Sets the profile privileges.
	 *
	 * @param profilePrivileges the new profile privileges
	 */
	public void setProfilePrivileges(List<ProfilePrivilege> profilePrivileges) {
		this.profilePrivileges = profilePrivileges;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the new system
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * Gets the user profiles.
	 *
	 * @return the user profiles
	 */
	public List<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	/**
	 * Sets the user profiles.
	 *
	 * @param userProfiles the new user profiles
	 */
	public void setUserProfiles(List<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}

	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	/**
	 * Sets the block other motive.
	 *
	 * @param blockOtherMotive the blockOtherMotive to set
	 */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	/**
	 * Sets the unblock other motive.
	 *
	 * @param unblockOtherMotive the unblockOtherMotive to set
	 */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}
	
	
	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	
	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}
	
	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("profilePrivileges", profilePrivileges);
		detailsMap.put("userProfiles", userProfiles);
        return detailsMap;
	}

	public Integer getIndUserType() {
		return indUserType;
	}

	public void setIndUserType(Integer indUserType) {
		this.indUserType = indUserType;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	public boolean isExtern() {
		return isExtern;
	}

	public void setExtern(boolean isExtern) {
		this.isExtern = isExtern;
	}

	public boolean isIntern() {
		return isIntern;
	}

	public void setIntern(boolean isIntern) {
		this.isIntern = isIntern;
	}

	public String getDeleteOtherMotive() {
		return deleteOtherMotive;
	}

	public void setDeleteOtherMotive(String deleteOtherMotive) {
		this.deleteOtherMotive = deleteOtherMotive;
	}

	public boolean isRejectProfile() {
		return rejectProfile;
	}

	public void setRejectProfile(boolean rejectProfile) {
		this.rejectProfile = rejectProfile;
	}

	/**
	 * @return the profileType
	 */
	public Integer getProfileType() {
		return profileType;
	}

	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(Integer profileType) {
		this.profileType = profileType;
	}
	
	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	@Override
	public String toString() {
		return "SystemProfile [idSystemProfilePk=" + idSystemProfilePk + ", lastModifyDate=" + lastModifyDate
				+ ", lastModifyIp=" + lastModifyIp + ", lastModifyPriv=" + lastModifyPriv + ", lastModifyUser="
				+ lastModifyUser + ", mnemonic=" + mnemonic + ", description=" + description + ", name=" + name
				+ ", registryDate=" + registryDate + ", registryUser=" + registryUser + ", state=" + state
				+ ", situation=" + situation + ", blockMotive=" + blockMotive + ", unblockMotive=" + unblockMotive
				+ ", rejectMotive=" + rejectMotive + ", blockOtherMotive=" + blockOtherMotive + ", unblockOtherMotive="
				+ unblockOtherMotive + ", rejectOtherMotive=" + rejectOtherMotive + ", indUserType="
				+ indUserType + ", institutionType=" + institutionType + ", deleteOtherMotive=" + deleteOtherMotive
				+ ", profileType=" + profileType + ", isExtern=" + isExtern + ", isIntern=" + isIntern
				+ ", rejectProfile=" + rejectProfile + "]";
	}
	
	
}