package com.pradera.security.model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the USER_AUDIT_TRACKING database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="USER_AUDIT_TRACKING")
public class UserAuditTracking implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id user audit pk. */
	@Id
	@SequenceGenerator(name="USER_AUDIT_TRACKING_IDUSERAUDITPK_GENERATOR", sequenceName="SQ_ID_USER_AUDIT_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_AUDIT_TRACKING_IDUSERAUDITPK_GENERATOR")
	@Column(name="ID_USER_AUDIT_PK")
	private Integer idUserAuditPk;

	/** The exception message. */
	@Column(name="EXCEPTION_MESSAGE")
	private String exceptionMessage;

	/** The query string. */
	@Column(name="QUERY_STRING")
	private String queryString;

    /** The time access. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="TIME_ACCESS")
	private Date timeAccess;

    /** The url. */
    @Column(name="URL")
	private String url;

	//bi-directional many-to-one association to UserSession
    /** The user session. */
	@ManyToOne
	@JoinColumn(name="ID_USER_SESSION_FK")
	private UserSession userSession;
    
    
  //bi-directional many-to-one association to UserAuditTrackingDetail
  	/** The user audit tracking details. */
  @OneToMany(mappedBy="userAuditTracking")
  	private List<UserAuditTrackingDetail> userAuditTrackingDetails;

    /**
     * Instantiates a new user audit tracking.
     */
    public UserAuditTracking() {
    }

	/**
	 * Gets the id user audit pk.
	 *
	 * @return the id user audit pk
	 */
	public Integer getIdUserAuditPk() {
		return this.idUserAuditPk;
	}

	/**
	 * Sets the id user audit pk.
	 *
	 * @param idUserAuditPk the new id user audit pk
	 */
	public void setIdUserAuditPk(Integer idUserAuditPk) {
		this.idUserAuditPk = idUserAuditPk;
	}

	/**
	 * Gets the exception message.
	 *
	 * @return the exception message
	 */
	public String getExceptionMessage() {
		return this.exceptionMessage;
	}

	/**
	 * Sets the exception message.
	 *
	 * @param exceptionMessage the new exception message
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * Gets the query string.
	 *
	 * @return the query string
	 */
	public String getQueryString() {
		return this.queryString;
	}

	/**
	 * Sets the query string.
	 *
	 * @param queryString the new query string
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * Gets the time access.
	 *
	 * @return the time access
	 */
	public Date getTimeAccess() {
		return this.timeAccess;
	}

	/**
	 * Sets the time access.
	 *
	 * @param timeAccess the new time access
	 */
	public void setTimeAccess(Date timeAccess) {
		this.timeAccess = timeAccess;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the user session.
	 *
	 * @return the user session
	 */
	public UserSession getUserSession() {
		return this.userSession;
	}

	/**
	 * Sets the user session.
	 *
	 * @param userSession the new user session
	 */
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}


	/**
	 * Gets the user audit tracking details.
	 *
	 * @return the user audit tracking details
	 */
	public List<UserAuditTrackingDetail> getUserAuditTrackingDetails() {
		return userAuditTrackingDetails;
	}

	
	/**
	 * Sets the user audit tracking details.
	 *
	 * @param userAuditTrackingDetails the new user audit tracking details
	 */
	public void setUserAuditTrackingDetails(
			List<UserAuditTrackingDetail> userAuditTrackingDetails) {
		this.userAuditTrackingDetails = userAuditTrackingDetails;
	}
	
}