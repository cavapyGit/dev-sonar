package com.pradera.security.model;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the SYSTEM_PROFILE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-mar-2015
 */
@Entity
@Table(name="SYSTEM_PROFILE_REQUEST")
@NamedQuery(name="SystemProfileRequest.findAll", query="SELECT s FROM SystemProfileRequest s")
public class SystemProfileRequest implements Serializable, com.pradera.integration.audit.Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id system prof request pk. */
	@Id
	@SequenceGenerator(name="SYSTEM_PROFILE_REQUEST_IDSYSTEMPROFREQUESTPK_GENERATOR", sequenceName="SQ_ID_SYSTEM_PROF_REQUEST_PK",allocationSize=1,initialValue=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SYSTEM_PROFILE_REQUEST_IDSYSTEMPROFREQUESTPK_GENERATOR")
	@Column(name="ID_SYSTEM_PROF_REQUEST_PK")
	private Long idSystemProfRequestPk;

	/** The confirmation date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CONFIRMATION_DATE")
	private Date confirmationDate;

	/** The confirmation user. */
	@Column(name="CONFIRMATION_USER")
	private String confirmationUser;

	/** The system profile. */
	@ManyToOne
	@JoinColumn(name="ID_SYSTEM_PROFILE_FK")
	private SystemProfile systemProfile;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The profile description. */
	@Column(name="PROFILE_DESCRIPTION")
	private String profileDescription;

	/** The profile name. */
	@Column(name="PROFILE_NAME")
	private String profileName;

	/** The registry date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The reject other motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;

	/** The request state. */
	@Column(name="REQUEST_STATE")
	private Integer requestState;
	
	/** The profile type. */
    @Column(name="PROFILE_TYPE")
	private Integer profileType;

	/** The version. */
	@Version
	private Long version;

	//bi-directional many-to-one association to ProfilePrivilegeRequest
	/** The profile privilege requests. */
	@OneToMany(mappedBy="systemProfileRequest", cascade={CascadeType.PERSIST})
	private List<ProfilePrivilegeRequest> profilePrivilegeRequests;

	/**
	 * Instantiates a new system profile request.
	 */
	public SystemProfileRequest() {
	}

	/**
	 * Gets the id system prof request pk.
	 *
	 * @return the id system prof request pk
	 */
	public Long getIdSystemProfRequestPk() {
		return this.idSystemProfRequestPk;
	}

	/**
	 * Sets the id system prof request pk.
	 *
	 * @param idSystemProfRequestPk the new id system prof request pk
	 */
	public void setIdSystemProfRequestPk(Long idSystemProfRequestPk) {
		this.idSystemProfRequestPk = idSystemProfRequestPk;
	}

	/**
	 * Gets the confirmation date.
	 *
	 * @return the confirmation date
	 */
	public Date getConfirmationDate() {
		return this.confirmationDate;
	}

	/**
	 * Sets the confirmation date.
	 *
	 * @param confirmationDate the new confirmation date
	 */
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return this.confirmationUser;
	}

	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the profile description.
	 *
	 * @return the profile description
	 */
	public String getProfileDescription() {
		return this.profileDescription;
	}

	/**
	 * Sets the profile description.
	 *
	 * @param profileDescription the new profile description
	 */
	public void setProfileDescription(String profileDescription) {
		this.profileDescription = profileDescription;
	}

	/**
	 * Gets the profile name.
	 *
	 * @return the profile name
	 */
	public String getProfileName() {
		return this.profileName;
	}

	/**
	 * Sets the profile name.
	 *
	 * @param profileName the new profile name
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return this.rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return this.requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return this.version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the profile privilege requests.
	 *
	 * @return the profile privilege requests
	 */
	public List<ProfilePrivilegeRequest> getProfilePrivilegeRequests() {
		return this.profilePrivilegeRequests;
	}

	/**
	 * Sets the profile privilege requests.
	 *
	 * @param profilePrivilegeRequests the new profile privilege requests
	 */
	public void setProfilePrivilegeRequests(List<ProfilePrivilegeRequest> profilePrivilegeRequests) {
		this.profilePrivilegeRequests = profilePrivilegeRequests;
	}

	/**
	 * Adds the profile privilege request.
	 *
	 * @param profilePrivilegeRequest the profile privilege request
	 * @return the profile privilege request
	 */
	public ProfilePrivilegeRequest addProfilePrivilegeRequest(ProfilePrivilegeRequest profilePrivilegeRequest) {
		getProfilePrivilegeRequests().add(profilePrivilegeRequest);
		profilePrivilegeRequest.setSystemProfileRequest(this);

		return profilePrivilegeRequest;
	}

	/**
	 * Removes the profile privilege request.
	 *
	 * @param profilePrivilegeRequest the profile privilege request
	 * @return the profile privilege request
	 */
	public ProfilePrivilegeRequest removeProfilePrivilegeRequest(ProfilePrivilegeRequest profilePrivilegeRequest) {
		getProfilePrivilegeRequests().remove(profilePrivilegeRequest);
		profilePrivilegeRequest.setSystemProfileRequest(null);

		return profilePrivilegeRequest;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("profilePrivilegeRequests", profilePrivilegeRequests);
        return detailsMap;
	}

	/**
	 * Gets the system profile.
	 *
	 * @return the system profile
	 */
	public SystemProfile getSystemProfile() {
		return systemProfile;
	}

	/**
	 * Sets the system profile.
	 *
	 * @param systemProfile the new system profile
	 */
	public void setSystemProfile(SystemProfile systemProfile) {
		this.systemProfile = systemProfile;
	}

	/**
	 * @return the profileType
	 */
	public Integer getProfileType() {
		return profileType;
	}

	/**
	 * @param profileType the profileType to set
	 */
	public void setProfileType(Integer profileType) {
		this.profileType = profileType;
	}

	@Override
	public String toString() {
		return "SystemProfileRequest [idSystemProfRequestPk=" + idSystemProfRequestPk + ", confirmationDate="
				+ confirmationDate + ", confirmationUser=" + confirmationUser 
				+ ", lastModifyDate=" + lastModifyDate + ", lastModifyIp=" + lastModifyIp + ", lastModifyPriv="
				+ lastModifyPriv + ", lastModifyUser=" + lastModifyUser + ", profileDescription=" + profileDescription
				+ ", profileName=" + profileName + ", registryDate=" + registryDate + ", registryUser=" + registryUser
				+ ", rejectOtherMotive=" + rejectOtherMotive + ", requestState=" + requestState + ", profileType="
				+ profileType + ", version=" + version + "]";
	}
	
	

}