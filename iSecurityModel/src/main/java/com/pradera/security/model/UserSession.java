package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the USER_SESSION database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :05/12/2012
 */
@Entity
@Table(name="USER_SESSION")
public class UserSession implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id user session pk. */
	@Id
	@SequenceGenerator(name="USER_SESSION_IDUSERSESSIONPK_GENERATOR", sequenceName="SQ_ID_USER_SESSION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_SESSION_IDUSERSESSIONPK_GENERATOR")
	@Column(name="ID_USER_SESSION_PK")
	private Long idUserSessionPk;

    /** The final date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="FINAL_DATE")    
	private Date finalDate;

    /** The initial date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="INITIAL_DATE")
    @NotNull
	private Date initialDate;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
    @NotNull
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	@NotNull
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	@NotNull
	private String lastModifyUser;

	/** The logout motive fk. */
	@Column(name="LOGOUT_MOTIVE_FK")
	private Integer logoutMotiveFk;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The schedule access type. */
	@Column(name="SCHEDULE_ACCESS_TYPE")
	@NotNull
	private Integer scheduleAccessType;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;
	
	/** The state. */
	@Column(name="TICKET")
	@NotNull
	private String ticket;

	//bi-directional many-to-one association to System
    /** The system. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CURRENT_SYSTEM_FK")
	private System system;

	//bi-directional many-to-one association to UserAccount
    /** The user account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USER_FK")
	@NotNull
	private UserAccount userAccount;
    
    //bi-directional many-to-one association to System
    /** The option privilege. */
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="LAST_SELECTED_PRIVILEGE_FK")
	private OptionPrivilege optionPrivilege;
    
    /** The locale. */
    @Column(name="LOCALE")
	private String locale;
    
    @Column(name="REGISTRY_IP")
    private String registryIp;

    /**
     * Instantiates a new user session.
     */
    public UserSession() {
    }

	/**
	 * Gets the id user session pk.
	 *
	 * @return the id user session pk
	 */
	public Long getIdUserSessionPk() {
		return idUserSessionPk;
	}

	/**
	 * Sets the id user session pk.
	 *
	 * @param idUserSessionPk the new id user session pk
	 */
	public void setIdUserSessionPk(Long idUserSessionPk) {
		this.idUserSessionPk = idUserSessionPk;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}


	/**
	 * Gets the ticket.
	 *
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * Sets the ticket.
	 *
	 * @param ticket the new ticket
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the logout motive fk.
	 *
	 * @return the logout motive fk
	 */
	public Integer getLogoutMotiveFk() {
		return logoutMotiveFk;
	}

	/**
	 * Sets the logout motive fk.
	 *
	 * @param logoutMotiveFk the new logout motive fk
	 */
	public void setLogoutMotiveFk(Integer logoutMotiveFk) {
		this.logoutMotiveFk = logoutMotiveFk;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the schedule access type.
	 *
	 * @return the schedule access type
	 */
	public Integer getScheduleAccessType() {
		return scheduleAccessType;
	}

	/**
	 * Sets the schedule access type.
	 *
	 * @param scheduleAccessType the new schedule access type
	 */
	public void setScheduleAccessType(Integer scheduleAccessType) {
		this.scheduleAccessType = scheduleAccessType;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the new system
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * Gets the user account.
	 *
	 * @return the user account
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}

	/**
	 * Sets the user account.
	 *
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	/**
	 * Gets the option privilege.
	 *
	 * @return the option privilege
	 */
	public OptionPrivilege getOptionPrivilege() {
		return optionPrivilege;
	}

	/**
	 * Sets the option privilege.
	 *
	 * @param optionPrivilege the new option privilege
	 */
	public void setOptionPrivilege(OptionPrivilege optionPrivilege) {
		this.optionPrivilege = optionPrivilege;
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}       
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}

	public String getRegistryIp() {
		return registryIp;
	}

	public void setRegistryIp(String registryIp) {
		this.registryIp = registryIp;
	}
	
}