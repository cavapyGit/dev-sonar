package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the HISTORICAL_STATE database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="HISTORICAL_STATE")
public class HistoricalState implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id historical state pk. */
	@Id
	@SequenceGenerator(name="HISTORICAL_STATE_GENERATOR", sequenceName="SQ_ID_HISTORIAL_STATE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HISTORICAL_STATE_GENERATOR")
	@Column(name="ID_HISTORICAL_STATE_PK")
	private Long idHistoricalStatePk;

	/** The before state. */
	@Column(name="BEFORE_STATE")
	private Integer beforeState;

	/** The current state. */
	@Column(name="CURRENT_STATE")
	private Integer currentState;

	/** The id register. */
	@Column(name="ID_REGISTER")
	private Long idRegister;

	/** The id table. */
	@Column(name="TABLE_NAME")
	private String tableName;

    /** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The motive. */
	private Integer motive;

	/** The motive description. */
	@Column(name="MOTIVE_DESCRIPTION")
	private String motiveDescription;

    /** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

    /**
     * Instantiates a new historical state.
     */
    public HistoricalState() {
    }

	/**
	 * Gets the id historical state pk.
	 *
	 * @return the id historical state pk
	 */
	public Long getIdHistoricalStatePk() {
		return idHistoricalStatePk;
	}

	/**
	 * Sets the id historical state pk.
	 *
	 * @param idHistoricalStatePk the new id historical state pk
	 */
	public void setIdHistoricalStatePk(Long idHistoricalStatePk) {
		this.idHistoricalStatePk = idHistoricalStatePk;
	}

	/**
	 * Gets the before state.
	 *
	 * @return the before state
	 */
	public Integer getBeforeState() {
		return beforeState;
	}

	/**
	 * Sets the before state.
	 *
	 * @param beforeState the new before state
	 */
	public void setBeforeState(Integer beforeState) {
		this.beforeState = beforeState;
	}

	/**
	 * Gets the current state.
	 *
	 * @return the current state
	 */
	public Integer getCurrentState() {
		return currentState;
	}

	/**
	 * Sets the current state.
	 *
	 * @param currentState the new current state
	 */
	public void setCurrentState(Integer currentState) {
		this.currentState = currentState;
	}

	/**
	 * Gets the id register.
	 *
	 * @return the id register
	 */
	public Long getIdRegister() {
		return idRegister;
	}

	/**
	 * Sets the id register.
	 *
	 * @param idRegister the new id register
	 */
	public void setIdRegister(Long idRegister) {
		this.idRegister = idRegister;
	}

	/**
	 * Gets the id table.
	 *
	 * @return the id table
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the id table.
	 *
	 * @param idTable the new id table
	 */
	public void setTableName(String idTable) {
		this.tableName = idTable;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	/**
	 * Gets the motive description.
	 *
	 * @return the motive description
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}

	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the new motive description
	 */
	public void setMotiveDescription(String motiveDescription) {
		if(motiveDescription!=null){
			motiveDescription = motiveDescription.toUpperCase();
		}
		this.motiveDescription = motiveDescription;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}       
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	if(loggerUser.getIdPrivilegeOfSystem()!= null){
        		lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
        	}
        	if(loggerUser.getAuditTime()!=null){
        		lastModifyDate = loggerUser.getAuditTime();
        	}
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}

}