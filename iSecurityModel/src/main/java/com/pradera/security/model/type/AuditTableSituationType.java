package com.pradera.security.model.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum AuditTableSituationType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05-mar-2015
 */
public enum AuditTableSituationType {
	
	/** The pending confirm. */
	PENDING_CONFIRM(Integer.valueOf(165)),
	
	/** The pending delete. */
	PENDING_DELETE(Integer.valueOf(166)),
	
	/** The active. */
	ACTIVE(Integer.valueOf(167)), 	

	/** The active. */
	PENDING_EXECUTION(Integer.valueOf(218));
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, AuditTableSituationType> lookup = new HashMap<Integer, AuditTableSituationType>();

	static {
		for (AuditTableSituationType s : EnumSet.allOf(AuditTableSituationType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new audit table state type.
	 *
	 * @param code the code
	 */
	private AuditTableSituationType(Integer code){
		this.code=code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the audit table state type
	 */
	public static AuditTableSituationType get(Integer code){
		return lookup.get(code);
	}
}
