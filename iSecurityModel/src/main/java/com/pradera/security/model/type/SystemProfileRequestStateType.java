package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum SystemProfileRequestStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-mar-2015
 */
public enum SystemProfileRequestStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(181)),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(182)),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(183));
	
	/** The code. */
	private Integer code;
	
	/** The Constant list. */
	public static final List<SystemProfileRequestStateType> list = new ArrayList<SystemProfileRequestStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemProfileRequestStateType> lookup = new HashMap<Integer, SystemProfileRequestStateType>();

	static {
		for (SystemProfileRequestStateType s : EnumSet.allOf(SystemProfileRequestStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new system profile request state type.
	 *
	 * @param code the code
	 */
	private SystemProfileRequestStateType(Integer code){
		this.code=code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	public static SystemProfileRequestStateType get(Integer code) {
		return lookup.get(code);
	}
}
