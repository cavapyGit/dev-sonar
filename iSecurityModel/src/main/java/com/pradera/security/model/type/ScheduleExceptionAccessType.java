/*
 * 
 */
package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum ScheduleExceptionType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum ScheduleExceptionAccessType {
	
	/** The access acept. */
	NORMAL_EXCEPTION(Integer.valueOf(1),"HORARIO NORMAL",Integer.valueOf(9),Integer.valueOf(18)),
	
	/** The access refuse. */
	SPECIAL_EXCEPTION(Integer.valueOf(2),"HORARIO ESPECIAL",Integer.valueOf(18),Integer.valueOf(9));
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The init hour. */
	private Integer initHour;
	
	/** The end hour. */
	private Integer endHour;

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 * @param initHour the init hour
	 * @param endHour the end hour
	 */
	private ScheduleExceptionAccessType(int code, String value, int initHour, int endHour) {
		this.code = code;
		this.value = value;
		this.initHour = initHour;
		this.endHour = endHour;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	
	/**
	 * Gets the inits the hour.
	 *
	 * @return the inits the hour
	 */
	public Integer getInitHour() {
		return initHour;
	}

	/**
	 * Sets the inits the hour.
	 *
	 * @param initHour the new inits the hour
	 */
	public void setInitHour(Integer initHour) {
		this.initHour = initHour;
	}

	/**
	 * Gets the end hour.
	 *
	 * @return the end hour
	 */
	public Integer getEndHour() {
		return endHour;
	}

	/**
	 * Sets the end hour.
	 *
	 * @param endHour the new end hour
	 */
	public void setEndHour(Integer endHour) {
		this.endHour = endHour;
	}


	/** The Constant list. */
	public static final List<ScheduleExceptionAccessType> list = new ArrayList<ScheduleExceptionAccessType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ScheduleExceptionAccessType> lookup = new HashMap<Integer, ScheduleExceptionAccessType>();
	static {
		for (ScheduleExceptionAccessType s : EnumSet.allOf(ScheduleExceptionAccessType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
