package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserAuditProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/11/2013
 */
@Entity
@Table(name="USER_AUDIT_PROCESS")
public class UserAuditProcess implements Serializable {

	   
	/** The id user audit process pk. */
	@Id
	@SequenceGenerator(name="USER_AUDIT_PROCESS_GENERATOR", sequenceName="SQ_ID_USER_AUDIT_PROCESS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_AUDIT_PROCESS_GENERATOR")
	@Column(name="ID_USER_AUDIT_PROCESS_PK")
	private Long idUserAuditProcessPk;
	
	/** The registry date. */
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;
	
	/** The id business process fk. */
	@Column(name="ID_BUSINESS_PROCESS_FK")
	private Long idBusinessProcessFk;
	
	/** The business method. */
	@Column(name="BUSINESS_METHOD")
	private String businessMethod;
	
	/** The class name. */
	@Column(name="CLASS_NAME")
	private String className;
	
	/** The ind error. */
	@Column(name="IND_ERROR")
	private Integer indError;
	
	/** The id process logger fk. */
	@Column(name="ID_PROCESS_LOGGER_FK")
	private Long idProcessLoggerFk;
	
	/** The id report logger fk. */
	@Column(name="ID_REPORT_LOGGER_FK")
	private Long idReportLoggerFk;
	
	/** The user session. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USER_SESSION_FK")
	private UserSession userSession;
	
	/** The user audit details. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="userAuditProcess")
	private List<UserAuditDetail> userAuditDetails;
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new user audit process.
	 */
	public UserAuditProcess() {
		super();
	}   
	
	/**
	 * Gets the id user audit process pk.
	 *
	 * @return the id user audit process pk
	 */
	public Long getIdUserAuditProcessPk() {
		return this.idUserAuditProcessPk;
	}

	/**
	 * Sets the id user audit process pk.
	 *
	 * @param idUserAuditProcessPk the new id user audit process pk
	 */
	public void setIdUserAuditProcessPk(Long idUserAuditProcessPk) {
		this.idUserAuditProcessPk = idUserAuditProcessPk;
	}   
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}   
	
	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}
	
	/**
	 * Gets the id business process fk.
	 *
	 * @return the id business process fk
	 */
	public Long getIdBusinessProcessFk() {
		return idBusinessProcessFk;
	}
	
	/**
	 * Sets the id business process fk.
	 *
	 * @param idBusinessProcessFk the new id business process fk
	 */
	public void setIdBusinessProcessFk(Long idBusinessProcessFk) {
		this.idBusinessProcessFk = idBusinessProcessFk;
	}
	
	/**
	 * Gets the business method.
	 *
	 * @return the business method
	 */
	public String getBusinessMethod() {
		return businessMethod;
	}
	
	/**
	 * Sets the business method.
	 *
	 * @param businessMethod the new business method
	 */
	public void setBusinessMethod(String businessMethod) {
		this.businessMethod = businessMethod;
	}
	
	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * Sets the class name.
	 *
	 * @param className the new class name
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	
	/**
	 * Gets the ind error.
	 *
	 * @return the ind error
	 */
	public Integer getIndError() {
		return indError;
	}
	
	/**
	 * Sets the ind error.
	 *
	 * @param indError the new ind error
	 */
	public void setIndError(Integer indError) {
		this.indError = indError;
	}
	
	/**
	 * Gets the id process logger fk.
	 *
	 * @return the id process logger fk
	 */
	public Long getIdProcessLoggerFk() {
		return idProcessLoggerFk;
	}
	
	/**
	 * Sets the id process logger fk.
	 *
	 * @param idProcessLoggerFk the new id process logger fk
	 */
	public void setIdProcessLoggerFk(Long idProcessLoggerFk) {
		this.idProcessLoggerFk = idProcessLoggerFk;
	}
	
	/**
	 * Gets the id report logger fk.
	 *
	 * @return the id report logger fk
	 */
	public Long getIdReportLoggerFk() {
		return idReportLoggerFk;
	}
	
	/**
	 * Sets the id report logger fk.
	 *
	 * @param idReportLoggerFk the new id report logger fk
	 */
	public void setIdReportLoggerFk(Long idReportLoggerFk) {
		this.idReportLoggerFk = idReportLoggerFk;
	}

	/**
	 * Gets the user session.
	 *
	 * @return the user session
	 */
	public UserSession getUserSession() {
		return userSession;
	}

	/**
	 * Sets the user session.
	 *
	 * @param userSession the new user session
	 */
	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

	/**
	 * Gets the user audit details.
	 *
	 * @return the user audit details
	 */
	public List<UserAuditDetail> getUserAuditDetails() {
		return userAuditDetails;
	}

	/**
	 * Sets the user audit details.
	 *
	 * @param userAuditDetails the new user audit details
	 */
	public void setUserAuditDetails(List<UserAuditDetail> userAuditDetails) {
		this.userAuditDetails = userAuditDetails;
	}
   
}
