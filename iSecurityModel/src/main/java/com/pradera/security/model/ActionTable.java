package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.StateType;


/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the ACTION_TABLE database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name = ActionTable.ACTION_TABLE_SEARCH_BY_SYSTEM, query = "select at From ActionTable at Where at.system.idSystemPk = :idSystemPk order by at.registryDate desc",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") })
})

@Entity
@Table(name ="ACTION_TABLE")
public class ActionTable implements Serializable,Auditable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant ACTION_TABLE_SEARCH_BY_SYSTEM. */
	public static final String ACTION_TABLE_SEARCH_BY_SYSTEM = "ActionTable.searchBySystem";

	/** The id action tables pk. */
	@Id
	@SequenceGenerator(name="ACTION_TABLE_GENERATOR", sequenceName="SQ_ID_ACTION_TABLE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACTION_TABLE_GENERATOR")
	@Column(name="ID_ACTION_TABLES_PK")
	private long idActionTablesPk;
	
	@Version
	private Long version;
	
	/** The table name. */
	@Column(name="TABLE_NAME")
	private String tableName;
	
	/** The rule name. */
	@Column(name="RULE_NAME")
	private String ruleName;
	
	/** The system. */
	@ManyToOne
	@JoinColumn(name="ID_SYSTEM_FK",referencedColumnName="ID_SYSTEM_PK")
	private System system;
	
	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The initial date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="INITIAL_DATE")
	private Date initialDate;
	
	/** The end date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="END_DATE")
	private Date endDate;
	
	/** The state. */
	@Column(name="STATE")
	private Integer state;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The trigger rule. */
	@Column(name="TRIGGER_RULE")
	private String triggerRule;
	
	/** The situation. */
    @Column(name="SITUATION")
	private Integer situation;

	/** The detail actions. */
	@OneToMany(mappedBy="actionTable",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<DetailAction> detailActions;
	
	/** The detail actions. */
	@OneToMany(mappedBy="actionTable",fetch=FetchType.LAZY,cascade=CascadeType.PERSIST)
	private List<ActionTableSql> actionTableSqls;
	
	/** The state type. */
	@Transient
	private String stateType;
	
	/** The old value. */
	@Transient
	private String oldValue;
	
	/** The new value. */
	@Transient
	private String newValue;
	
	/** The field name. */
	@Transient
	private String fieldName;
	
	/** The key. */
	@Transient
	private Integer key;
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public Integer getKey() {
		return key;
	}

	/**
	 * Sets the key.
	 *
	 * @param key the key to set
	 */
	public void setKey(Integer key) {
		this.key = key;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 *
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the id action tables pk.
	 *
	 * @return the idActionTablesPk
	 */
	public long getIdActionTablesPk() {
		return idActionTablesPk;
	}

	/**
	 * Sets the id action tables pk.
	 *
	 * @param idActionTablesPk the idActionTablesPk to set
	 */
	public void setIdActionTablesPk(long idActionTablesPk) {
		this.idActionTablesPk = idActionTablesPk;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the rule name.
	 *
	 * @return the ruleName
	 */
	public String getRuleName() {
		return ruleName;
	}

	/**
	 * Sets the rule name.
	 *
	 * @param ruleName the ruleName to set
	 */
	public void setRuleName(String ruleName) {
		if(ruleName != null){
			ruleName = ruleName.toUpperCase();
		}
		this.ruleName = ruleName;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the system to set
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the trigger rule.
	 *
	 * @return the triggerRule
	 */
	public String getTriggerRule() {
		return triggerRule;
	}

	/**
	 * Sets the trigger rule.
	 *
	 * @param triggerRule the triggerRule to set
	 */
	public void setTriggerRule(String triggerRule) {
		this.triggerRule = triggerRule;
	}

	/**
	 * Gets the detail actions.
	 *
	 * @return the detailActions
	 */
	public List<DetailAction> getDetailActions() {
		return detailActions;
	}

	/**
	 * Sets the detail actions.
	 *
	 * @param detailActions the detailActions to set
	 */
	public void setDetailActions(List<DetailAction> detailActions) {
		this.detailActions = detailActions;
	}

	public List<ActionTableSql> getActionTableSqls() {
		return actionTableSqls;
	}

	public void setActionTableSqls(List<ActionTableSql> actionTableSqls) {
		this.actionTableSqls = actionTableSqls;
	}

	/**
	 * Gets the state type.
	 *
	 * @return the stateType
	 */
	public String getStateType() {
		String stateType = null;
		  if (state != null) {
			  if(StateType.get(state.intValue()) != null)
				  stateType = StateType.get(state.intValue()).getValue();
		  }
		return stateType;
	}

	/**
	 * Sets the state type.
	 *
	 * @param stateType the stateType to set
	 */
	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	/**
	 * Gets the old value.
	 *
	 * @return the oldValue
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * Sets the old value.
	 *
	 * @param oldValue the oldValue to set
	 */
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * Gets the new value.
	 *
	 * @return the newValue
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * Sets the new value.
	 *
	 * @param newValue the newValue to set
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("detailActions", detailActions);
		detailsMap.put("actionTableSqls", actionTableSqls);
        return detailsMap;
	}

	public Integer getSituation() {
		return situation;
	}

	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
