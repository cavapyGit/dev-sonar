package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum OptionType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum OptionType {
	
	/** The module. */
	MODULE(Integer.valueOf(1),"option.lbl.module"),
	
	/** The option. */
	OPTION(Integer.valueOf(2),"option.lbl.option");	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<OptionType> list = new ArrayList<OptionType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OptionType> lookup = new HashMap<Integer, OptionType>();

	static {
		for (OptionType s : EnumSet.allOf(OptionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new option type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private OptionType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		return getDescription(locale);
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the option type
	 */
	public static OptionType get(Integer code) {
		return lookup.get(code);
	}
}

