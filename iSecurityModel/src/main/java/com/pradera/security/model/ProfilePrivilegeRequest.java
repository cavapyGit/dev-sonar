package com.pradera.security.model;

import java.io.Serializable;

import javax.persistence.*;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The persistent class for the PROFILE_PRIVILEGE_REQUEST database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 24-mar-2015
 */
@Entity
@Table(name="PROFILE_PRIVILEGE_REQUEST")
@NamedQuery(name="ProfilePrivilegeRequest.findAll", query="SELECT p FROM ProfilePrivilegeRequest p")
public class ProfilePrivilegeRequest implements Serializable, com.pradera.integration.audit.Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id prof priv request pk. */
	@Id
	@SequenceGenerator(name="PROFILE_PRIVILEGE_REQUEST_IDPROFPRIVREQUESTPK_GENERATOR", sequenceName="SQ_ID_PROF_PRIV_REQUEST_PK",allocationSize=1,initialValue=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PROFILE_PRIVILEGE_REQUEST_IDPROFPRIVREQUESTPK_GENERATOR")
	@Column(name="ID_PROF_PRIV_REQUEST_PK")
	private Long idProfPrivRequestPk;

	/** The option privilege. */
	@ManyToOne
	@JoinColumn(name="ID_OPTION_PRIVILEGE_FK")
	private OptionPrivilege optionPrivilege;

	/** The last modify date. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	//bi-directional many-to-one association to SystemProfileRequest
	/** The system profile request. */
	@ManyToOne
	@JoinColumn(name="ID_SYSTEM_PROF_REQUEST_FK")
	private SystemProfileRequest systemProfileRequest;

	/**
	 * Instantiates a new profile privilege request.
	 */
	public ProfilePrivilegeRequest() {
	}

	/**
	 * Gets the id prof priv request pk.
	 *
	 * @return the id prof priv request pk
	 */
	public Long getIdProfPrivRequestPk() {
		return this.idProfPrivRequestPk;
	}

	/**
	 * Sets the id prof priv request pk.
	 *
	 * @param idProfPrivRequestPk the new id prof priv request pk
	 */
	public void setIdProfPrivRequestPk(Long idProfPrivRequestPk) {
		this.idProfPrivRequestPk = idProfPrivRequestPk;
	}

	

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the system profile request.
	 *
	 * @return the system profile request
	 */
	public SystemProfileRequest getSystemProfileRequest() {
		return this.systemProfileRequest;
	}

	/**
	 * Sets the system profile request.
	 *
	 * @param systemProfileRequest the new system profile request
	 */
	public void setSystemProfileRequest(SystemProfileRequest systemProfileRequest) {
		this.systemProfileRequest = systemProfileRequest;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the option privilege.
	 *
	 * @return the option privilege
	 */
	public OptionPrivilege getOptionPrivilege() {
		return optionPrivilege;
	}

	/**
	 * Sets the option privilege.
	 *
	 * @param optionPrivilege the new option privilege
	 */
	public void setOptionPrivilege(OptionPrivilege optionPrivilege) {
		this.optionPrivilege = optionPrivilege;
	}

	@Override
	public String toString() {
		return "ProfilePrivilegeRequest [idProfPrivRequestPk=" + idProfPrivRequestPk + ", lastModifyDate=" + lastModifyDate + ", lastModifyIp=" + lastModifyIp
				+ ", lastModifyPriv=" + lastModifyPriv + ", lastModifyUser=" + lastModifyUser+ "]";
	}

}