package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum InstitutionTypeDetailStateType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum InstitutionTypeDetailStateType{

	/** The registered. */
	REGISTERED(Integer.valueOf(1),"REGISTERED"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(2),"DELETED");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Instantiates a new institution type detail state type.
	 *
	 * @param ordinal the ordinal
	 * @param name the name
	 */
	private InstitutionTypeDetailStateType(int ordinal,String name) {
		this.code = ordinal;
		this.value = name;
	}
	
	/** The Constant list. */
	public static final List<InstitutionTypeDetailStateType> list = new ArrayList<InstitutionTypeDetailStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InstitutionTypeDetailStateType> lookup = new HashMap<Integer, InstitutionTypeDetailStateType>();

	static {
		for (InstitutionTypeDetailStateType s : EnumSet.allOf(InstitutionTypeDetailStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}