package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class Holiday
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name = Holiday.HOLIDAY_SEARCH_BY_STATE, query = " Select H From Holiday H Where H.state=:state ",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") })
})
@Entity
@Cacheable(true)
public class Holiday implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant HOLIDAY_SEARCH_BY_STATE. */
	public static final String HOLIDAY_SEARCH_BY_STATE = "Holiday.searchByState";
	
	/** The id holiday pk. */
	@Id
	@SequenceGenerator(name="HOLIDAY_IDHOLIDAYPK_GENERATOR", sequenceName="SQ_ID_SEC_HOLIDAY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HOLIDAY_IDHOLIDAYPK_GENERATOR")
	@Column(name="ID_HOLIDAY_PK")
	private Integer idHolidayPk;

    /** The date holiday. */
    @Temporal( TemporalType.DATE)
	@Column(name="DATE_HOLIDAY")
    @NotNull
	private Date dateHoliday;

    /** The description. */
    @NotNull(message = "{holiday.description}")
	private String description;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
    @NotNull
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	@NotNull
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	@NotNull
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	@NotNull
	private String lastModifyUser;

    /** The registry date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;
	
	/**  The deleteMotive. */
	@Column(name="DELETE_MOTIVE")
	private Integer deleteMotive;
	
	
	/**  The delete other Motive. */
	@Column(name="DELETE_OTHER_MOTIVE")
	private String deleteOtherMotive;

    /**
     * Instantiates a new holiday.
     */
    public Holiday() {
    }

	/**
	 * Gets the id holiday pk.
	 *
	 * @return the id holiday pk
	 */
	public Integer getIdHolidayPk() {
		return idHolidayPk;
	}

	/**
	 * Sets the id holiday pk.
	 *
	 * @param idHolidayPk the new id holiday pk
	 */
	public void setIdHolidayPk(Integer idHolidayPk) {
		this.idHolidayPk = idHolidayPk;
	}

	/**
	 * Gets the date holiday.
	 *
	 * @return the date holiday
	 */
	public Date getDateHoliday() {
		return dateHoliday;
	}

	/**
	 * Sets the date holiday.
	 *
	 * @param dateHoliday the new date holiday
	 */
	public void setDateHoliday(Date dateHoliday) {
		this.dateHoliday = dateHoliday;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		if(description!=null){
			description = description.toUpperCase();
		}
		this.description = description;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}       
	

	/**
	 * Gets the delete motive.
	 *
	 * @return the deleteMotive
	 */
	public Integer getDeleteMotive() {
		return deleteMotive;
	}

	/**
	 * Sets the delete motive.
	 *
	 * @param deleteMotive the deleteMotive to set
	 */
	public void setDeleteMotive(Integer deleteMotive) {
		this.deleteMotive = deleteMotive;
	}

	/**
	 * Gets the delete other motive.
	 *
	 * @return the deleteOtherMotive
	 */
	public String getDeleteOtherMotive() {
		return deleteOtherMotive;
	}

	/**
	 * Sets the delete other motive.
	 *
	 * @param deleteOtherMotive the deleteOtherMotive to set
	 */
	public void setDeleteOtherMotive(String deleteOtherMotive) {
		this.deleteOtherMotive = deleteOtherMotive;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}

}