package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the CATALOGUE_LANGUAGE database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name = CatalogueLanguage.LANGUAGE_CATALOGUE, query = "Select c From CatalogueLanguage c WHERE c.idCatalogueLanguagePk = :idCatalogueLanguage",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") })
})
@Entity
@Table(name="CATALOGUE_LANGUAGE")
public class CatalogueLanguage implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant LANGUAGE_CATALOGUE. */
	public static final String LANGUAGE_CATALOGUE = "CatalogueLanguage.searchCatalogueLanguageById";
	
	/** The id catalogue language pk. */
	@Id
	@SequenceGenerator(name="CATALOGUE_LANGUAGE_IDCATALOGUELANGUAGEPK_GENERATOR", sequenceName="SQ_ID_CATALOGUE_LANGUAGE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CATALOGUE_LANGUAGE_IDCATALOGUELANGUAGEPK_GENERATOR")
	@Column(name="ID_CATALOGUE_LANGUAGE_PK")
	private Integer idCatalogueLanguagePk;
	
	/** The catalogue name. */
	@Column(name="CATALOGUE_NAME")
	private String catalogueName;

	/** The description. */
	private String description;

	/** The language. */
	@Column(name="\"LANGUAGE\"")
	private Integer language;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIP;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to SystemOption
   
	/** The system option. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SYSTEM_OPTION_FK",referencedColumnName="ID_SYSTEM_OPTION_PK")
	private SystemOption systemOption;

    /**
     * Instantiates a new catalogue language.
     */
    public CatalogueLanguage() {
    }

	/**
	 * Gets the catalogue name.
	 *
	 * @return the catalogue name
	 */
	public String getCatalogueName() {
		return this.catalogueName;
	}

	/**
	 * Sets the catalogue name.
	 *
	 * @param catalogueName the new catalogue name
	 */
	public void setCatalogueName(String catalogueName) {
		this.catalogueName = catalogueName;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the id catalogue language pk.
	 *
	 * @return the id catalogue language pk
	 */
	public Integer getIdCatalogueLanguagePk() {
		return this.idCatalogueLanguagePk;
	}

	/**
	 * Sets the id catalogue language pk.
	 *
	 * @param idCatalogueLanguagePk the new id catalogue language pk
	 */
	public void setIdCatalogueLanguagePk(Integer idCatalogueLanguagePk) {
		this.idCatalogueLanguagePk = idCatalogueLanguagePk;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public Integer getLanguage() {
		return this.language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(Integer language) {
		this.language = language;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIP() {
		return this.lastModifyIP;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIP the new last modify ip
	 */
	public void setLastModifyIP(String lastModifyIP) {
		this.lastModifyIP = lastModifyIP;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the system option.
	 *
	 * @return the system option
	 */
	public SystemOption getSystemOption() {
		return this.systemOption;
	}

	/**
	 * Sets the system option.
	 *
	 * @param systemOption the new system option
	 */
	public void setSystemOption(SystemOption systemOption) {
		this.systemOption = systemOption;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIP = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	
}