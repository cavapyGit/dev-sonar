package com.pradera.security.model.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum AuditTableStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05-mar-2015
 */
public enum ActionTableSqlStateType {

	/** The registered. */
	NOT_EXECUTED(Integer.valueOf(216)),
	
	/** The confirmed. */
	EXECUTED(Integer.valueOf(217));
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, ActionTableSqlStateType> lookup = new HashMap<Integer, ActionTableSqlStateType>();

	static {
		for (ActionTableSqlStateType s : EnumSet.allOf(ActionTableSqlStateType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new audit table state type.
	 *
	 * @param code the code
	 */
	private ActionTableSqlStateType(Integer code){
		this.code=code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the audit table state type
	 */
	public static ActionTableSqlStateType get(Integer code){
		return lookup.get(code);
	}
}
