package com.pradera.security.model.type;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  class PermissionReportResultBean bean.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class PermissionReportResultBean {
	
	/** The profile. */
	private String profile;
	
	/** The menu name. */
	private String menuName;
	
	/** The url option. */
	private String urlOption;
	
	/** The type of client. */
	private String typeOfClient;
	
	/** The privilege. */
	private String privilege;
	
	/** The attribute. */
	private String attribute;
	
	/** The value. */
	private String value;
	
	/**
	 * Gets the profile.
	 *
	 * @return the profile
	 */
	public String getProfile() {
		return profile;
	}
	
	/**
	 * Sets the profile.
	 *
	 * @param profile the profile to set
	 */
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	/**
	 * Gets the menu name.
	 *
	 * @return the menuName
	 */
	public String getMenuName() {
		return menuName;
	}
	
	/**
	 * Sets the menu name.
	 *
	 * @param menuName the menuName to set
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	
	/**
	 * Gets the url option.
	 *
	 * @return the urlOption
	 */
	public String getUrlOption() {
		return urlOption;
	}
	
	/**
	 * Sets the url option.
	 *
	 * @param urlOption the urlOption to set
	 */
	public void setUrlOption(String urlOption) {
		this.urlOption = urlOption;
	}
	
	/**
	 * Gets the type of client.
	 *
	 * @return the typeOfClient
	 */
	public String getTypeOfClient() {
		return typeOfClient;
	}
	
	/**
	 * Sets the type of client.
	 *
	 * @param typeOfClient the typeOfClient to set
	 */
	public void setTypeOfClient(String typeOfClient) {
		this.typeOfClient = typeOfClient;
	}
	
	/**
	 * Gets the privilege.
	 *
	 * @return the component
	 */
	public String getPrivilege() {
		return privilege;
	}
	
	/**
	 * Sets the privilege.
	 *
	 * @param privilege the new privilege
	 */
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	
	/**
	 * Gets the attribute.
	 *
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}
	
	/**
	 * Sets the attribute.
	 *
	 * @param attribute the attribute to set
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
