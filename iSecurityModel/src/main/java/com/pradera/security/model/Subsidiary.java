package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name="SUBSIDIARY")
public class Subsidiary implements Serializable, Auditable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="SUBSIDIARY_GENERATOR", sequenceName="SQ_ID_SUBSIDIARY", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SUBSIDIARY_GENERATOR")
	@Column(name="ID_SUBSIDIARY_PK")
	private Long idSubsidiaryPk;
	
	//bi-directional many-to-one association to InstitutionsSecurity
    /** The institutions security. */
	@ManyToOne
	@JoinColumn(name="ID_INSTITUTION_SECURITY_FK")
    @NotNull
	private InstitutionsSecurity institutionsSecurity;	
	
	/** The institution type fk. */
	@NotNull
	@Column(name="INSTITUTION_TYPE_FK")
	private Integer institutionTypeFk;
	
	@Column(name="SUBSIDIARY_DESCRIPTION")
	private String subsidiaryDescription;
	
	@Column(name="DEPARTMENT")
	private Integer departament;
	
	@Column(name="LEGAL_ADDRESS")
	private String legalAddress;
	
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;
	
	@Column(name="FAX_NUMBER")
	private String faxNumber;
	
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	@Column(name="DEPARTMENT_CENTRAL")
	private Integer departamentCentral;
	
	/** The description location. */
	@Transient
	private String descDepartament;

	/**
	 * Construct
	 */
	public Subsidiary() {
		super();
	}

	/**
	 * @return the idSubsidiaryPk
	 */
	public Long getIdSubsidiaryPk() {
		return idSubsidiaryPk;
	}

	/**
	 * @param idSubsidiaryPk the idSubsidiaryPk to set
	 */
	public void setIdSubsidiaryPk(Long idSubsidiaryPk) {
		this.idSubsidiaryPk = idSubsidiaryPk;
	}

	/**
	 * @return the institutionsSecurity
	 */
	public InstitutionsSecurity getInstitutionsSecurity() {
		return institutionsSecurity;
	}

	/**
	 * @param institutionsSecurity the institutionsSecurity to set
	 */
	public void setInstitutionsSecurity(InstitutionsSecurity institutionsSecurity) {
		this.institutionsSecurity = institutionsSecurity;
	}

	/**
	 * @return the subsidiaryDescription
	 */
	public String getSubsidiaryDescription() {
		return subsidiaryDescription;
	}

	/**
	 * @param subsidiaryDescription the subsidiaryDescription to set
	 */
	public void setSubsidiaryDescription(String subsidiaryDescription) {
		this.subsidiaryDescription = subsidiaryDescription;
	}

	/**
	 * @return the departament
	 */
	public Integer getDepartament() {
		return departament;
	}

	/**
	 * @param departament the departament to set
	 */
	public void setDepartament(Integer departament) {
		this.departament = departament;
	}

	/**
	 * @return the legalAddress
	 */
	public String getLegalAddress() {
		return legalAddress;
	}

	/**
	 * @param legalAddress the legalAddress to set
	 */
	public void setLegalAddress(String legalAddress) {
		this.legalAddress = legalAddress;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * @return the descDepartament
	 */
	public String getDescDepartament() {
		return descDepartament;
	}

	/**
	 * @param descDepartament the descDepartament to set
	 */
	public void setDescDepartament(String descDepartament) {
		this.descDepartament = descDepartament;
	}

	/**
	 * @return the institutionTypeFk
	 */
	public Integer getInstitutionTypeFk() {
		return institutionTypeFk;
	}

	/**
	 * @param institutionTypeFk the institutionTypeFk to set
	 */
	public void setInstitutionTypeFk(Integer institutionTypeFk) {
		this.institutionTypeFk = institutionTypeFk;
	}

	

	/**
	 * @return the departamentCentral
	 */
	public Integer getDepartamentCentral() {
		return departamentCentral;
	}

	/**
	 * @param departamentCentral the departamentCentral to set
	 */
	public void setDepartamentCentral(Integer departamentCentral) {
		this.departamentCentral = departamentCentral;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		// TODO Auto-generated method stub
		if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		// TODO Auto-generated method stub
		return null;
	}

}
