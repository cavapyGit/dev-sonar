package com.pradera.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UserAuditDetail.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 07/11/2013
 */
@Entity
@Table(name="USER_AUDIT_DETAIL")
public class UserAuditDetail implements Serializable {

	   
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id user audit detail pk. */
	@Id
	@SequenceGenerator(name="USER_AUDIT_DETAIL_GENERATOR", sequenceName="SQ_ID_USER_AUDIT_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_AUDIT_DETAIL_GENERATOR")
	@Column(name="ID_USER_AUDIT_DETAIL_PK")
	private Long idUserAuditDetailPk;
	
	/** The param name. */
	@Column(name="PARAM_NAME")
	private String paramName;
	
	/** The param value. */
	@Column(name="PARAM_VALUE")
	private String paramValue;
	
	/** The user audit process. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USER_AUDIT_PROCESS_FK")
	private UserAuditProcess userAuditProcess;
	
	
	

	/**
	 * Instantiates a new user audit detail.
	 */
	public UserAuditDetail() {
		super();
	}   
	
	/**
	 * Gets the id user audit detail pk.
	 *
	 * @return the id user audit detail pk
	 */
	public Long getIdUserAuditDetailPk() {
		return this.idUserAuditDetailPk;
	}

	/**
	 * Sets the id user audit detail pk.
	 *
	 * @param idUserAuditDetailPk the new id user audit detail pk
	 */
	public void setIdUserAuditDetailPk(Long idUserAuditDetailPk) {
		this.idUserAuditDetailPk = idUserAuditDetailPk;
	}   
	
	/**
	 * Gets the param name.
	 *
	 * @return the param name
	 */
	public String getParamName() {
		return this.paramName;
	}

	/**
	 * Sets the param name.
	 *
	 * @param paramName the new param name
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}   
	
	/**
	 * Gets the param value.
	 *
	 * @return the param value
	 */
	public String getParamValue() {
		return this.paramValue;
	}

	/**
	 * Sets the param value.
	 *
	 * @param paramValue the new param value
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}   
	
	/**
	 * Gets the user audit process.
	 *
	 * @return the user audit process
	 */
	public UserAuditProcess getUserAuditProcess() {
		return this.userAuditProcess;
	}

	/**
	 * Sets the user audit process.
	 *
	 * @param userAuditProcess the new user audit process
	 */
	public void setUserAuditProcess(UserAuditProcess userAuditProcess) {
		this.userAuditProcess = userAuditProcess;
	}
   
}
