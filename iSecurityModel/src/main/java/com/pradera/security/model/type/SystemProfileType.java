package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

public enum SystemProfileType {
	
	/** The operator. */
	OPERATOR(Integer.valueOf(197), "OPERADOR"),
	
	/** The supervisor. */
	SUPERVISOR(Integer.valueOf(198), "SUPERVISOR"),
	
	/** The consultation. */
	CONSULTATION(Integer.valueOf(199), "CONSULTA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<SystemProfileType> list = new ArrayList<SystemProfileType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemProfileType> lookup = new HashMap<Integer, SystemProfileType>();
	
	static {
		for (SystemProfileType s : EnumSet.allOf(SystemProfileType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new location type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private SystemProfileType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the motive type
	 */
	public static SystemProfileType get(Integer code) {
		return lookup.get(code);
	}
}
