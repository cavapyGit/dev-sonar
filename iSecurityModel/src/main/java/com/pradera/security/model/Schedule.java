package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.pradera.commons.type.InstitutionType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.ScheduleSituationType;
import com.pradera.security.model.type.ScheduleStateType;
import com.pradera.security.model.type.ScheduleType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the SCHEDULE database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="\"SCHEDULE\"")
public class Schedule implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id schedule pk. */
	@Id
	@SequenceGenerator(name="SCHEDULE_PK_GENERATOR", sequenceName="SQ_ID_SCHEDULE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SCHEDULE_PK_GENERATOR")
	@Column(name="ID_SCHEDULE_PK")
	@NotNull
	private long idSchedulePk;

	/** The description. */
	@NotNull
	private String description;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The schedule type. */
	@Column(name="SCHEDULE_TYPE")
	@NotNull(message="{schedule.type}")
	private Integer scheduleType;
	
	/** The institution type. */
	@Column(name="INSTITUTION_TYPE")
	private Integer institutionType;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;
	
    /** The situation. */
    @Column(name="SITUATION")
	private Integer situation;

	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	/**  The rejectMotive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/**  The reject other Motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	//bi-directional many-to-one association to ScheduleDetail
	/** The schedule details. */
	@OrderBy("idScheduleDetailPk ASC")
	@OneToMany(cascade=CascadeType.ALL,mappedBy="schedule",fetch=FetchType.LAZY)
	private List<ScheduleDetail> scheduleDetails;

	//bi-directional many-to-one association to System
	/** The systems. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="schedule",fetch=FetchType.LAZY)
	private List<System> systems;

    /**
     * Instantiates a new schedule.
     */
    public Schedule() {
    }

	/**
	 * Gets the id schedule pk.
	 *
	 * @return the id schedule pk
	 */
	public long getIdSchedulePk() {
		return this.idSchedulePk;
	}

	/**
	 * Sets the id schedule pk.
	 *
	 * @param idSchedulePk the new id schedule pk
	 */
	public void setIdSchedulePk(long idSchedulePk) {
		this.idSchedulePk = idSchedulePk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description.toUpperCase();
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the schedule type.
	 *
	 * @return the schedule type
	 */
	public Integer getScheduleType() {
		return scheduleType;
	}

	/**
	 * Sets the schedule type.
	 *
	 * @param scheduleType the new schedule type
	 */
	public void setScheduleType(Integer scheduleType) {
		this.scheduleType = scheduleType;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the schedule details.
	 *
	 * @return the schedule details
	 */
	public List<ScheduleDetail> getScheduleDetails() {
		return this.scheduleDetails;
	}

	/**
	 * Sets the schedule details.
	 *
	 * @param scheduleDetails the new schedule details
	 */
	public void setScheduleDetails(List<ScheduleDetail> scheduleDetails) {
		this.scheduleDetails = scheduleDetails;
	}
	
	/**
	 * Gets the systems.
	 *
	 * @return the systems
	 */
	public List<System> getSystems() {
		return this.systems;
	}

	/**
	 * Sets the systems.
	 *
	 * @param systems the new systems
	 */
	public void setSystems(List<System> systems) {
		this.systems = systems;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription(){
		return ScheduleStateType.lookup.get(this.getState()).getValue();
	}
	
	/**
	 * Gets the situation description.
	 *
	 * @return the situation description
	 */
	public String getSituationDescription(){
		
		return this.getSituation() == null ? "" : ScheduleSituationType.lookup.get(this.getSituation()).getValue();
	}
	
	/**
	 * Gets the type description.
	 *
	 * @return the type description
	 */
	public String getTypeDescription(){
		return ScheduleType.lookup.get(this.getScheduleType()).getValue();
	}
	
	/**
	 * This method is useful to know if this schedule is
	 * for System Or Not. Important for the view(*.xhtml)
	 *
	 * @return true, if is schedule type system
	 */
	public boolean isScheduleTypeSystem(){
		if(Validations.validateIsNotNull(this.getScheduleType()))
			return this.getScheduleType().equals(ScheduleType.SYSTEMS.getCode());
		return false;
	}
	
	/**
	 * This method is useful to know if this schedule is
	 * for Institutions Or Not. Important for the view(*.xhtml)
	 *
	 * @return true, if is schedule type institution
	 */
	public boolean isScheduleTypeInstitution(){
		if(Validations.validateIsNotNull(this.getScheduleType()))
			return this.getScheduleType().equals(ScheduleType.INSTITUTIONS.getCode());
		return false;
	}
	
	/**
	 * Gets the state icon.
	 *
	 * @return the state icon
	 */
	public String getStateIcon() {
			return ScheduleStateType.lookup.get(this.getState()).getIcon();
	}
	
	/**
	 * Gets the institution type.
	 *
	 * @return the institution type
	 */
	public Integer getInstitutionType() {
		return institutionType;
	}
	
	/**
	 * Gets the institution type description.
	 *
	 * @return the institution type description
	 */
	public String getInstitutionTypeDescription() {
		return InstitutionType.lookup.get(this.getInstitutionType()).getValue();
	}

	/**
	 * Sets the institution type.
	 *
	 * @param institutionType the new institution type
	 */
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	/**
	 * Gets the schedule type description.
	 *
	 * @return the schedule type description
	 */
	public String getScheduleTypeDescription(){
		return ScheduleType.lookup.get(this.getScheduleType()).getValue();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
	    return (idSchedulePk + description + lastModifyDate + lastModifyIp + lastModifyPriv 
	    		+ lastModifyUser + registryDate + registryUser +scheduleType + state).hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(obj!=null){
			return ( hashCode() == obj.hashCode() );
		} else {
			return false;
		}
	    
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}
	
	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	/**
	 * Sets the block other motive.
	 *
	 * @param blockOtherMotive the blockOtherMotive to set
	 */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	/**
	 * Sets the unblock other motive.
	 *
	 * @param unblockOtherMotive the unblockOtherMotive to set
	 */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}
	
	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("scheduleDetails", scheduleDetails);
		detailsMap.put("systems", systems);
        return detailsMap;
	}
}