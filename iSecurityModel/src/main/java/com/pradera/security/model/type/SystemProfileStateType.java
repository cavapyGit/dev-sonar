package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum SystemProfileStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum SystemProfileStateType {

	
	/** The registered. */
	REGISTERED(Integer.valueOf(1), "REGISTRADO", "registered.png"),
	
	/** The blocked. */
	BLOCKED(Integer.valueOf(2), "BLOQUEADO", "blocked.png"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(3), "CONFIRMADO", "confirm.png"),
	
	/** The rejected. */
	REJECTED(Integer.valueOf(4), "RECHAZADO", "reject.png"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(5), "ELIMINADO", "reject.png");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<SystemProfileStateType> list = new ArrayList<SystemProfileStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemProfileStateType> lookup = new HashMap<Integer, SystemProfileStateType>();

	static {
		for (SystemProfileStateType s : EnumSet.allOf(SystemProfileStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new system profile state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private SystemProfileStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}		
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the system profile state type
	 */
	public static SystemProfileStateType get(Integer code) {
		return lookup.get(code);
	}

}




