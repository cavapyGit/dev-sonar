package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum OptionPrivilegeStateType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum OptionPrivilegeStateType {
	
	/** The deleted. */
	DELETED(Integer.valueOf(0),"ELIMINADO"),
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1),"REGISTRADO");	
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<OptionPrivilegeStateType> list = new ArrayList<OptionPrivilegeStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OptionPrivilegeStateType> lookup = new HashMap<Integer, OptionPrivilegeStateType>();

	static {
		for (OptionPrivilegeStateType s : EnumSet.allOf(OptionPrivilegeStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new option privilege state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private OptionPrivilegeStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the option privilege state type
	 */
	public static OptionPrivilegeStateType get(Integer code) {
		return lookup.get(code);
	}
}
