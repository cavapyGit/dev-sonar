/*
 * 
 */
package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum ScheduleStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum ScheduleStateType {
	
	/** The registred. */
	REGISTERED(Integer.valueOf(1),"REGISTRADO","confirmed.png"),
	
	/** The locked. */
	BLOCKED(Integer.valueOf(2),"BLOQUEADO","blocked.png"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(4),"CONFIRMADO","confirmed.png"),
	
	/** The reject. */
	REJECT(Integer.valueOf(5),"RECHAZADO","reject.png");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private ScheduleStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}	
	
	/**
	 * Gets the some elements.
	 *
	 * @param scheduleStateTypeArray the schedule state type array
	 * @return the some elements
	 */
	public static List<ScheduleStateType> getSomeElements(ScheduleStateType... scheduleStateTypeArray){
		List<ScheduleStateType> returnList = new ArrayList<ScheduleStateType>();
		for(ScheduleStateType scheduleStateType: scheduleStateTypeArray){
			returnList.add(scheduleStateType);
		}
		return returnList;
	}
	
	/** The Constant lookup. */
	public static final Map<Integer, ScheduleStateType> lookup = new HashMap<Integer, ScheduleStateType>();
	
	/** The Constant list. */
	public static final List<ScheduleStateType> list = new ArrayList<ScheduleStateType>();
	static{
		for (ScheduleStateType d : EnumSet.allOf(ScheduleStateType.class)){
            lookup.put(d.getCode(), d);
			list.add(d);
		}
	}	
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule state type
	 */
	public static ScheduleStateType get(Integer code) {
        return lookup.get(code);
    }
}
