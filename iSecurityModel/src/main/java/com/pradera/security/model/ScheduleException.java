package com.pradera.security.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.pradera.commons.type.InstitutionType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.ScheduleExceptionAccessType;
import com.pradera.security.model.type.ScheduleExceptionSituationType;
import com.pradera.security.model.type.ScheduleExceptionStateType;
import com.pradera.security.model.type.ScheduleExceptionType;
import com.pradera.security.model.type.UserAccountType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the SCHEDULE_EXCEPTION database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="SCHEDULE_EXCEPTION")
public class ScheduleException implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id schedule exception pk. */
	@Id
	@SequenceGenerator(name="SCHEDULE_EXCEPTION_PK_GENERATOR", sequenceName="SQ_ID_SCHEDULE_EXCEPTION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SCHEDULE_EXCEPTION_PK_GENERATOR")
	@Column(name="ID_SCHEDULE_EXCEPTION_PK")
	@NotNull
	private long idScheduleExceptionPk;

	/** The description. */
	@NotNull(message="{scheduleException.description.required}")
	private String description;

    /** The end date. */
    @Temporal( TemporalType.DATE)
	@Column(name="END_DATE")
    @NotNull(message="{scheduleException.finalDate.required}")
	private Date endDate;

	/** The end hour. */
    @Temporal( TemporalType.TIME)
	@Column(name="END_HOUR")
	@NotNull(message="{scheduleException.finalHour.required}")
	private Date endHour;

	/** The exception type fk. */
	@Column(name="EXCEPTION_TYPE_FK")
	@NotNull
	private Integer exceptionTypeFk;
	
	/** The exception access type. */
	@Column(name="EXCEPTION_ACCESS_TYPE")
	@NotNull
	private Integer exceptionAccessType;

    /** The initial date. */
    @Temporal( TemporalType.DATE)
	@Column(name="INITIAL_DATE")
    @NotNull(message="{scheduleException.initialDate.required}")
	private Date initialDate;

	/** The initial hour. */
    @Temporal( TemporalType.TIME)
	@Column(name="INITIAL_HOUR")
	@NotNull(message="{scheduleException.initialHour.required}")
	private Date initialHour;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;
	
    /** The situation. */
    @Column(name="SITUATION")
	private Integer situation;

	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/**  The removeMotive. */
	@Column(name="REMOVE_MOTIVE")
	private Integer removeMotive;
	
	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	/**  The rejectMotive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/**  The reject other Motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;
	
	/**  The clientType. */
	@Column(name="CLIENT_TYPE")
	private Integer clientType;
	
	/**  The institutionTypeFk. */
	@Column(name="INSTITUTION_TYPE_FK")
	private Integer institutionTypeFk;
		
	/**  The idInstitutionFk. */
	@Column(name="ID_INSTITUTION_FK")
	private Integer idInstitutionFk;
	
	
	//bi-directional many-to-one association to System
    /** The system. */
	@ManyToOne
	@JoinColumn(name="ID_SYSTEM_FK")
	private System system;

	//bi-directional many-to-one association to ScheduleExceptionDetail
	/** The schedule exception details. */
	@OneToMany(mappedBy="scheduleException",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<ScheduleExceptionDetail> scheduleExceptionDetails;

    /**
     * The Constructor.
     */
    public ScheduleException() {
    	system = new System();
    }
    
    /**
     * Gets the state icon.
     *
     * @return the state icon
     */
    public String getStateIcon() {
		return ScheduleExceptionStateType.lookup.get(this.getState()).getValue();
    }
    
    /**
     * Gets the state description.
     *
     * @return the state description
     */
    public String getStateDescription() {
		return ScheduleExceptionStateType.lookup.get(this.getState()).getValue();
    }
    
    /**
     * Gets the situation description.
     *
     * @return the situation description
     */
    public String getSituationDescription() {
		return this.getSituation() == null ? "" : ScheduleExceptionSituationType.lookup.get(this.getSituation()).getValue();
    }
    
    /**
     * Gets the ExcepcionType description.
     *
     * @return the type execdescription
     */
    public String getTypeExecdescription(){
    	String desc = null;
    	if(this.getExceptionTypeFk() != null){
    		desc = ScheduleExceptionType.lookup.get(this.getExceptionTypeFk()).getValue();
    	}
    	return  desc;
    }
    
    /**
     * Gets the ExceptionAccessType description.
     *
     * @return the exception accessdescrpicion
     */
    public String getExceptionAccessdescrpicion(){
    	String desc = null;
    	if(this.getExceptionAccessType() != null){
    		desc = ScheduleExceptionAccessType.lookup.get(this.getExceptionAccessType()).getValue();
    	}
    	return  desc;
    }
    
    /**
     * Gets the InstitutionTypeFkdescrpicion description.
     *
     * @return the institution type fkdescrpicion
     */
    public String getInstitutionTypeFkdescrpicion(){
    	String desc = null;
    	if(this.getInstitutionTypeFk() != null && this.getInstitutionTypeFk() > 0 ){
    		desc = InstitutionType.lookup.get(this.getInstitutionTypeFk()).getValue();
    	}else if(this.getInstitutionTypeFk() != null && this.getInstitutionTypeFk() <= 0){
    		desc = "TODOS";
    	}
    	return  desc;
    }
    
    /**
     * Gets the ExceptionAccessType description.
     *
     * @return the tipo clientedescrpicion
     */
    public String getTipoClientedescrpicion(){
    	String desc = null;
    	if(this.getClientType() != null && this.getClientType() > 0){
    		desc = UserAccountType.lookup.get(this.getClientType()).getValue();
    	}else if(this.getClientType() != null && this.getClientType() <= 0){
    		desc = "AMBOS";
    	}
    	return  desc;
    }
 
	/**
	 * Gets the end hour.
	 *
	 * @return the end hour
	 */
	public Date getEndHour() {
		return endHour;
	}

	/**
	 * Sets the end hour.
	 *
	 * @param endHour the end hour
	 */
	public void setEndHour(Date endHour) {
		this.endHour = endHour;
	}

	/**
	 * Gets the initial hour.
	 *
	 * @return the initial hour
	 */
	public Date getInitialHour() {
		return initialHour;
	}

	/**
	 * Sets the initial hour.
	 *
	 * @param initialHour the initial hour
	 */
	public void setInitialHour(Date initialHour) {
		this.initialHour = initialHour;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Gets the exception type fk.
	 *
	 * @return the exception type fk
	 */
	public Integer getExceptionTypeFk() {
		return exceptionTypeFk;
	}

	/**
	 * Sets the exception type fk.
	 *
	 * @param exceptionTypeFk the exception type fk
	 */
	public void setExceptionTypeFk(Integer exceptionTypeFk) {
		this.exceptionTypeFk = exceptionTypeFk;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the id schedule exception pk.
	 *
	 * @return the id schedule exception pk
	 */
	public long getIdScheduleExceptionPk() {
		return this.idScheduleExceptionPk;
	}

	/**
	 * Sets the id schedule exception pk.
	 *
	 * @param idScheduleExceptionPk the id schedule exception pk
	 */
	public void setIdScheduleExceptionPk(long idScheduleExceptionPk) {
		this.idScheduleExceptionPk = idScheduleExceptionPk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description
	 */
	public void setDescription(String description) {
		this.description = description.toUpperCase();
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Sets the end hour.
	 *
	 * @param endHour the end hour
	 */
	public void setEndHour(Timestamp endHour) {
		this.endHour = endHour;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return this.initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Sets the initial hour.
	 *
	 * @param initialHour the initial hour
	 */
	public void setInitialHour(Timestamp initialHour) {
		this.initialHour = initialHour;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return this.system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the system
	 */
	public void setSystem(System system) {
		this.system = system;
	}
	
	/**
	 * Gets the schedule exception details.
	 *
	 * @return the schedule exception details
	 */
	public List<ScheduleExceptionDetail> getScheduleExceptionDetails() {
		return this.scheduleExceptionDetails;
	}

	/**
	 * Sets the schedule exception details.
	 *
	 * @param scheduleExceptionDetails the schedule exception details
	 */
	public void setScheduleExceptionDetails(List<ScheduleExceptionDetail> scheduleExceptionDetails) {
		this.scheduleExceptionDetails = scheduleExceptionDetails;
	}
	
	/**
	 * This method is useful to send the exceptionScheduleType's name
	 * from his code, in this case we going to call his Enum.
	 *
	 * @return the exception type description
	 */
	public String getExceptionTypeDescription(){
		return ScheduleExceptionType.lookup.get(this.getExceptionTypeFk()).getValue();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public ScheduleException clone(){
		try {
			return (ScheduleException) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ScheduleException();
		}
	}

	/**
	 * Gets the exception access type.
	 *
	 * @return the exception access type
	 */
	public Integer getExceptionAccessType() {
		return exceptionAccessType;
	}

	/**
	 * Sets the exception access type.
	 *
	 * @param exceptionAccessType the exception access type
	 */
	public void setExceptionAccessType(Integer exceptionAccessType) {
		this.exceptionAccessType = exceptionAccessType;
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}

	/**
	 * Gets the removes the motive.
	 *
	 * @return the removeMotive
	 */
	public Integer getRemoveMotive() {
		return removeMotive;
	}

	/**
	 * Sets the removes the motive.
	 *
	 * @param removeMotive the removeMotive to set
	 */
	public void setRemoveMotive(Integer removeMotive) {
		this.removeMotive = removeMotive;
	}
	
	/**
	 * Gets the client type.
	 *
	 * @return the clientType
	 */
	public Integer getClientType() {
		return clientType;
	}

	/**
	 * Sets the client type.
	 *
	 * @param clientType the clientType to set
	 */
	public void setClientType(Integer clientType) {
		this.clientType = clientType;
	}

	/**
	 * Gets the institution type fk.
	 *
	 * @return the institutionTypeFk
	 */
	public Integer getInstitutionTypeFk() {
		return institutionTypeFk;
	}

	/**
	 * Sets the institution type fk.
	 *
	 * @param institutionTypeFk the institutionTypeFk to set
	 */
	public void setInstitutionTypeFk(Integer institutionTypeFk) {
		this.institutionTypeFk = institutionTypeFk;
	}

	/**
	 * Gets the id institution fk.
	 *
	 * @return the idInstitutionFk
	 */
	public Integer getIdInstitutionFk() {
		return idInstitutionFk;
	}

	/**
	 * Sets the id institution fk.
	 *
	 * @param idInstitutionFk the idInstitutionFk to set
	 */
	public void setIdInstitutionFk(Integer idInstitutionFk) {
		this.idInstitutionFk = idInstitutionFk;
	}

	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	/**
	 * Sets the block other motive.
	 *
	 * @param blockOtherMotive the blockOtherMotive to set
	 */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	/**
	 * Sets the unblock other motive.
	 *
	 * @param unblockOtherMotive the unblockOtherMotive to set
	 */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}
		
	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("scheduleExceptionDetails", scheduleExceptionDetails);
        return detailsMap;
	}
}