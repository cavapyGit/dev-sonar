package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum MotiveType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum MotiveType {
	
	/** The MOTIV e1. */
	MOTIVE1(Integer.valueOf(14),"MOTIVO1"),
	
	/** The MOTIV e2. */
	MOTIVE2(Integer.valueOf(15),"MOTIVO2"),
	
	/** The MOTIV e3. */
	MOTIVE3(Integer.valueOf(16),"MOTIVO3"),
	
	/** The other motive. */
	OTHER_MOTIVE(Integer.valueOf(17),"OTRO MOTIVO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<MotiveType> list = new ArrayList<MotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, MotiveType> lookup = new HashMap<Integer, MotiveType>();

	static {
		for (MotiveType s : EnumSet.allOf(MotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new motive type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private MotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the motive type
	 */
	public static MotiveType get(Integer code) {
		return lookup.get(code);
	}
}