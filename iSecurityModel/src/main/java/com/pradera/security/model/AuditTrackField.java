package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the AUDIT_TRACK_FIELD database table.
 * 
 */
@Entity
@Table(name="AUDIT_TRACK_FIELD")
public class AuditTrackField implements Serializable,Auditable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id audit track field pk. */
	@Id
	@SequenceGenerator(name="AUDIT_TRACK_FIELD_GENERATOR", sequenceName="SQ_ID_AUDIT_TRACK_FIELD_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDIT_TRACK_FIELD_GENERATOR")
	@Column(name="ID_AUDIT_TRACK_FIELD_PK")
	private Long idAuditTrackFieldPk;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The name field. */
	@Column(name="NAME_FIELD")
	private String nameField;

	/** The new value. */
	@Column(name="NEW_VALUE")
	private String newValue;

	/** The old value. */
	@Column(name="OLD_VALUE")
	private String oldValue;

	//bi-directional many-to-one association to AuditTrackLog
	/** The audit track log. */
	@ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="ID_AUDIT_TRACK_FK")
	private AuditTrackLog auditTrackLog;

	/**
	 * Gets the id audit track field pk.
	 *
	 * @return the idAuditTrackFieldPk
	 */
	public Long getIdAuditTrackFieldPk() {
		return idAuditTrackFieldPk;
	}

	/**
	 * Sets the id audit track field pk.
	 *
	 * @param idAuditTrackFieldPk the idAuditTrackFieldPk to set
	 */
	public void setIdAuditTrackFieldPk(Long idAuditTrackFieldPk) {
		this.idAuditTrackFieldPk = idAuditTrackFieldPk;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the name field.
	 *
	 * @return the nameField
	 */
	public String getNameField() {
		return nameField;
	}

	/**
	 * Sets the name field.
	 *
	 * @param nameField the nameField to set
	 */
	public void setNameField(String nameField) {
		this.nameField = nameField;
	}

	/**
	 * Gets the new value.
	 *
	 * @return the newValue
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * Sets the new value.
	 *
	 * @param newValue the newValue to set
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	/**
	 * Gets the old value.
	 *
	 * @return the oldValue
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * Sets the old value.
	 *
	 * @param oldValue the oldValue to set
	 */
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * Gets the audit track log.
	 *
	 * @return the auditTrackLog
	 */
	public AuditTrackLog getAuditTrackLog() {
		return auditTrackLog;
	}

	/**
	 * Sets the audit track log.
	 *
	 * @param auditTrackLog the auditTrackLog to set
	 */
	public void setAuditTrackLog(AuditTrackLog auditTrackLog) {
		this.auditTrackLog = auditTrackLog;
	}
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	
}