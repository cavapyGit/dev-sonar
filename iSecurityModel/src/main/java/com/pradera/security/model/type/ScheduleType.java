/*
 * 
 */
package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;

import org.primefaces.extensions.util.MessageUtils;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum ScheduleType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum ScheduleType {
	
	/** The systems. */
	SYSTEMS(Integer.valueOf(1),"PARA SISTEMAS"),
	
	/** The institutions. */
	INSTITUTIONS(Integer.valueOf(2),"PARA TIPO DE INSTITUCI\u00d3N");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<ScheduleType> list = new ArrayList<ScheduleType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ScheduleType> lookup = new HashMap<Integer, ScheduleType>();

	static {
		for (ScheduleType s : EnumSet.allOf(ScheduleType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ScheduleType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public FacesMessage getDescription(Locale locale) {
		return MessageUtils.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the schedule type
	 */
	public static ScheduleType get(Integer code) {
		return lookup.get(code);
	}
}
