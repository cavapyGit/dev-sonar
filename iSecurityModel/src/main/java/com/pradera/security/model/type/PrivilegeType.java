package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;

import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.integration.common.validation.Validations;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Enum PrivilegeType.
 * have code abouts privileges in system
 * @author PraderaTechnologies.
 * @version 1.0 , 13/06/2013
 */
public enum PrivilegeType {
	
	/** The register. */
	REGISTER(Integer.valueOf(3),"btn.register"),
	
	/** The modify. */
	MODIFY(Integer.valueOf(4),"btn.modify"),
	
	/** The confirm. */
	CONFIRM(Integer.valueOf(5),"btn.confirm"),
	
	/** The delete. */
	DELETE(Integer.valueOf(6),"btn.delete"),
	
	/** The block. */
	BLOCK(Integer.valueOf(7), "btn.block"),
	
	/** The unblock. */
	UNBLOCK(Integer.valueOf(8),"btn.unblock"),			
	
	/** The reject. */
	REJECT(Integer.valueOf(9),"btn.reject"),	
	
	/** The search. */
	SEARCH(Integer.valueOf(10),"btn.search"),
	
	/** The add. */
	ADD(Integer.valueOf(11),"btn.add"),
	
	/** The annular. */
	APPROVE(Integer.valueOf(45),"btn.aprove"),
	
	/** The approve. */
	ANNULAR(Integer.valueOf(46),"btn.annular"),
	
	/** The review. */
	REVIEW(Integer.valueOf(48),"btn.review"),
	
	/** The apply. */
	APPLY(Integer.valueOf(105),"btn.apply"),
	
	/** The authorize. */
	AUTHORIZE(Integer.valueOf(106),"btn.authorize"),
	
	/** The cancel. */
	CANCEL(Integer.valueOf(114),"btn.cancel"),
	
	/** The settlement. */
	SETTLEMENT(Integer.valueOf(115),"btn.settlement"),
	
	/** The activate. */
	ACTIVATE(Integer.valueOf(119),"btn.activate"),
	

	/** The deactivate. */
	DEACTIVATE(Integer.valueOf(120),"btn.deactivate"),
	
	/** The confirm depositary. */
	CONFIRM_DEPOSITARY(Integer.valueOf(121),"btn.confirm.depositary"),
	
	/** The confirm issuer. */
	CONFIRM_ISSUER(Integer.valueOf(122),"btn.confirm.issuer"),
	
	/** The preliminary. */
	PRELIMINARY(Integer.valueOf(123),"btn.preliminary"),
	
	/** The definitive. */
	DEFINITIVE(Integer.valueOf(124),"btn.definitive"),
	
	/** The adjustsments. */
	ADJUSTSMENTS(Integer.valueOf(125),"btn.adjustments"),
	
	/** The generate. */
	GENERATE(Integer.valueOf(126),"btn.generate"),
	
	/** The execute. */
	EXECUTE(Integer.valueOf(127),"btn.execute"),
	
	/** The update. */
	UPDATE(Integer.valueOf(137),"btn.update"),
	
	/** The deliver. */
	DELIVER(Integer.valueOf(159),"btn.deliver"),
	
	APPEAL(Integer.valueOf(160),"btn.appeal"),
	
	CERTIFY(Integer.valueOf(161),"btn.certify"),
	
	CONFIRM_BLOCK(Integer.valueOf(171),"btn.confirm.block"),
	
	REJECT_BLOCK(Integer.valueOf(172),"btn.reject.block"),
	
	CONFIRM_UNBLOCK(Integer.valueOf(173),"btn.confirm.unblock"),
	
	REJECT_UNBLOCK(Integer.valueOf(174),"btn.reject.unblock"),
	
	CONFIRM_DELETE(Integer.valueOf(175),"btn.confirm.delete"),
	
	REJECT_DELETE(Integer.valueOf(176),"btn.reject.delete"),
	
	CONFIRM_ANNULAR(Integer.valueOf(177),"btn.confirm.annular"),
	
	REJECT_ANNULAR(Integer.valueOf(178),"btn.reject.annular"),
	
	CONFIRM_MODIFY(Integer.valueOf(184),"btn.confirm.modify"),
	
	REJECT_MODIFY(Integer.valueOf(185),"btn.reject.modify");

	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<PrivilegeType> list = new ArrayList<PrivilegeType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, PrivilegeType> lookup = new HashMap<Integer, PrivilegeType>();

	static {
		for (PrivilegeType s : EnumSet.allOf(PrivilegeType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new privilege type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private PrivilegeType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		Locale locale = null;
		if(Validations.validateIsNotNull(FacesContext.getCurrentInstance()))
			locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		else
			locale = new Locale("es");
		return getDescription(locale);
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the privilege type
	 */
	public static PrivilegeType get(Integer code) {
		return lookup.get(code);
	}
}
