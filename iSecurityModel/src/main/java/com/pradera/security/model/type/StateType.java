package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum StateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
	public enum StateType {
				
		
		/** The activated. */
		ACTIVATED(Integer.valueOf(5),PropertiesUtilities.getMessage("status.lbl.activated"),"added.png"),
		
		/** The deactivated. */
		DEACTIVATED(Integer.valueOf(6),PropertiesUtilities.getMessage("status.lbl.deactivated"),"added.png"),
		
		/** The registered. */
		REGISTERED(Integer.valueOf(0),PropertiesUtilities.getMessage("status.lbl.registered"),"added.png"),
		
		/** The confirmed. */
		CONFIRMED(Integer.valueOf(1),PropertiesUtilities.getMessage("status.lbl.confirmed"),"confirmed.png"),
		
		/** The rejected. */
		REJECTED(Integer.valueOf(2),PropertiesUtilities.getMessage("status.lbl.rejected"),"denied.png"),
		
		/** The annulled. */
		ANNULLED(Integer.valueOf(3),PropertiesUtilities.getMessage("status.lbl.annulled"),"annulled.png"),
		
		/** The blocked. */
		BLOCKED(Integer.valueOf(4),PropertiesUtilities.getMessage("status.lbl.blocked"),"blocked.png");
		

		/** The code. */
		private Integer code;
		
		/** The value. */
		private String value;
		
		/** The icon. */
		private String icon;

		/** The Constant list. */
		public static final List<StateType> list = new ArrayList<StateType>();
		
		/** The Constant lookup. */
		public static final Map<Integer, StateType> lookup = new HashMap<Integer, StateType>();

		static {
			for (StateType s : EnumSet.allOf(StateType.class)) {
				list.add(s);
				lookup.put(s.getCode(), s);
			}
		}

		/**
		 * Instantiates a new state type.
		 *
		 * @param code the code
		 * @param value the value
		 * @param icon the icon
		 */
		private StateType(Integer code, String value, String icon) {
			this.code = code;
			this.value = value;	
		    this.icon = icon;
		}
		
		/**
		 * Gets the code.
		 *
		 * @return the code
		 */
		public Integer getCode() {
			return code;
		}
		
		/**
		 * Gets the value.
		 *
		 * @return the value
		 */
		public String getValue() {
			return value;
		}	
		
		/**
		 * Gets the icon.
		 *
		 * @return the icon
		 */
		public String getIcon() {
			return icon;
		}

		/**
		 * Sets the icon.
		 *
		 * @param icon the new icon
		 */
		public void setIcon(String icon) {
			this.icon = icon;
		}

		/**
		 * Gets the description.
		 *
		 * @param locale the locale
		 * @return the description
		 */
		public String getDescription(Locale locale) {
			return PropertiesUtilities.getMessage(locale, this.getValue());
		}
		
		/**
		 * Gets the.
		 *
		 * @param code the code
		 * @return the state type
		 */
		public static StateType get(Integer code) {
			return lookup.get(code);
		}
		
		public static List<StateType> listSomeElements(StateType... transferSecuritiesTypeParams){
			List<StateType> retorno = new ArrayList<StateType>();
			for(StateType TransferSecuritiesType: transferSecuritiesTypeParams){
				retorno.add(TransferSecuritiesType);
			}
			return retorno;
		}
		
}
