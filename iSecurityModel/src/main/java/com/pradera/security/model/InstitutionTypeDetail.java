package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the INSTITUTION_TYPE_DETAIL database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name=InstitutionTypeDetail.SEARCH_BY_USER,
			query="Select DTI from InstitutionTypeDetail DTI where DTI.userAccount.idUserPk=:idUserPkPrm and DTI.state=:statePrm",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") })
})
@Entity
@Table(name="INSTITUTION_TYPE_DETAIL")
public class InstitutionTypeDetail implements Serializable,Auditable {
	
	/** The Constant SEARCH_BY_USER. */
	public static final String SEARCH_BY_USER="DetTipoInstitucion.buscarPorUsuario";
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id institution type detail pk. */
	@Id						 
	@SequenceGenerator(name="INSTITUTION_TYPE_DETAIL_IDINSTITUTION_TYPE_PK_GENERATOR", sequenceName="SQ_ID_INSTITUTION_TYPE_DET_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INSTITUTION_TYPE_DETAIL_IDINSTITUTION_TYPE_PK_GENERATOR")	
	@Column(name="ID_INSTITUTION_TYPE_DETAIL_PK")
	private Integer idInstitutionTypeDetailPk;

	/** The id institution type fk. */
	@Column(name="ID_INSTITUTION_TYPE_FK")
	@NotNull
	private Integer idInstitutionTypeFk;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
    @NotNull
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	@NotNull
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	@NotNull
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	@NotNull
	private String lastModifyUser;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
    @NotNull
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	@NotNull
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	@NotNull
	private Integer state;

	//bi-directional many-to-one association to UserAccount
	/** The user account. */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_USER_FK",referencedColumnName="ID_USER_PK")
    @NotNull
	private UserAccount userAccount;

    /**
     * Instantiates a new institution type detail.
     */
    public InstitutionTypeDetail() {
    }

	/**
	 * Gets the id institution type detail pk.
	 *
	 * @return the id institution type detail pk
	 */
	public Integer getIdInstitutionTypeDetailPk() {
		return this.idInstitutionTypeDetailPk;
	}

	/**
	 * Sets the id institution type detail pk.
	 *
	 * @param idInstitutionTypeDetailPk the new id institution type detail pk
	 */
	public void setIdInstitutionTypeDetailPk(Integer idInstitutionTypeDetailPk) {
		this.idInstitutionTypeDetailPk = idInstitutionTypeDetailPk;
	}

	/**
	 * Gets the id institution type fk.
	 *
	 * @return the id institution type fk
	 */
	public Integer getIdInstitutionTypeFk() {
		return this.idInstitutionTypeFk;
	}

	/**
	 * Sets the id institution type fk.
	 *
	 * @param idInstitutionTypeFk the new id institution type fk
	 */
	public void setIdInstitutionTypeFk(Integer idInstitutionTypeFk) {
		this.idInstitutionTypeFk = idInstitutionTypeFk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the user account.
	 *
	 * @return the user account
	 */
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	/**
	 * Sets the user account.
	 *
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	
}