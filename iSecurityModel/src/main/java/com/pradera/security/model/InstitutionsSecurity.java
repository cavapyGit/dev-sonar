package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.pradera.commons.type.InstitutionType;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class InstitutionsSecurity.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/02/2013
 */
@Entity
@Cacheable(true)
@Table(name="INSTITUTIONS_SECURITY")
public class InstitutionsSecurity implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id institution security pk. */
	@Id
	@SequenceGenerator(name="ID_INSTITUTION_SECURITYPK_GENERATOR", sequenceName="SQ_ID_INSTITUTIONS_SECURITY_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_INSTITUTION_SECURITYPK_GENERATOR")
	@Column(name="ID_INSTITUTION_SECURITY_PK")
	private Integer idInstitutionSecurityPk;

	/** The id institution type fk. */
	@Column(name="ID_INSTITUTION_TYPE_Fk")
	private Integer idInstitutionTypeFk;
	
	/** The state. */
	@Column(name="STATE")
	private Integer state;
	
	/** The id participante. */
	@Column(name="ID_PARTICIPANT_FK")
	private Long idParticipante;
	
	/** The id issuer. */
	@Column(name="ID_ISSUER_FK")
	private String idIssuer;

	/** The institution name. */
	@Column(name="INSTITUTION_NAME")
	private String institutionName;

	/** The mnemonic institution. */
	@Column(name="MNEMONIC_INSTITUTION")
	private String mnemonicInstitution;

	//bi-directional many-to-one association to MacAddressDetail
	/** The mac address details. */
	@OneToMany(mappedBy="institutionsSecurity")
	private List<MacAddressDetail> macAddressDetails;

	//bi-directional many-to-one association to UserAccount
	/** The user accounts. */
	@OneToMany(mappedBy="institutionsSecurity")
	private List<UserAccount> userAccounts;
	
	/** The registry date. */
	@NotNull
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@NotNull
	@Column(name="REGISTRY_USER")
	private String registryUser;
	
	/** The last modify user. */
	@NotNull
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
	@NotNull
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@NotNull
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@NotNull
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The is selected. @author mmacalupu this atributte is useful to checkbox in datatable because in some cases we will need selected some institutions in one datatable */
	@Transient
	private boolean isSelected;

    /**
     * Checks if is selected.
     *
     * @return true, if is selected
     */
    public boolean isSelected() {
		return isSelected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param isSelected the new selected
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	/** The have schedule. @author mmacalupu This atributte is useful to know if this institution has o Not some schedule asignated */
	@Transient
	private boolean haveSchedule;

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime
//				* result
//				+ (int) (idInstitutionSecurityPk ^ (idInstitutionSecurityPk >>> 32));
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		InstitutionsSecurity other = (InstitutionsSecurity) obj;
//		if (idInstitutionSecurityPk != other.idInstitutionSecurityPk)
//			return false;
//		return true;
//	}

	/**
 * Instantiates a new institutions security.
 */
public InstitutionsSecurity() {
    }

	/**
	 * Gets the id institution security pk.
	 *
	 * @return the id institution security pk
	 */
	public Integer getIdInstitutionSecurityPk() {
		return this.idInstitutionSecurityPk;
	}

	/**
	 * Sets the id institution security pk.
	 *
	 * @param idInstitutionSecurityPk the new id institution security pk
	 */
	public void setIdInstitutionSecurityPk(Integer idInstitutionSecurityPk) {
		this.idInstitutionSecurityPk = idInstitutionSecurityPk;
	}

	/**
	 * Gets the id institution type fk.
	 *
	 * @return the id institution type fk
	 */
	public Integer getIdInstitutionTypeFk() {
		return this.idInstitutionTypeFk;
	}

	/**
	 * Sets the id institution type fk.
	 *
	 * @param idInstitutionTypeFk the new id institution type fk
	 */
	public void setIdInstitutionTypeFk(Integer idInstitutionTypeFk) {
		this.idInstitutionTypeFk = idInstitutionTypeFk;
	}

	/**
	 * Gets the institution name.
	 *
	 * @return the institution name
	 */
	public String getInstitutionName() {
		return this.institutionName;
	}

	/**
	 * Sets the institution name.
	 *
	 * @param institutionName the new institution name
	 */
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	/**
	 * Gets the mnemonic institution.
	 *
	 * @return the mnemonic institution
	 */
	public String getMnemonicInstitution() {
		return this.mnemonicInstitution;
	}

	/**
	 * Sets the mnemonic institution.
	 *
	 * @param mnemonicInstitution the new mnemonic institution
	 */
	public void setMnemonicInstitution(String mnemonicInstitution) {
		this.mnemonicInstitution = mnemonicInstitution;
	}

	/**
	 * Gets the mac address details.
	 *
	 * @return the mac address details
	 */
	public List<MacAddressDetail> getMacAddressDetails() {
		return this.macAddressDetails;
	}

	/**
	 * Sets the mac address details.
	 *
	 * @param macAddressDetails the new mac address details
	 */
	public void setMacAddressDetails(List<MacAddressDetail> macAddressDetails) {
		this.macAddressDetails = macAddressDetails;
	}
	
	/**
	 * Gets the user accounts.
	 *
	 * @return the user accounts
	 */
	public List<UserAccount> getUserAccounts() {
		return this.userAccounts;
	}

	/**
	 * Sets the user accounts.
	 *
	 * @param userAccounts the new user accounts
	 */
	public void setUserAccounts(List<UserAccount> userAccounts) {
		this.userAccounts = userAccounts;
	}
	
	/**
	 * Checks if is have schedule.
	 *
	 * @author mmacalupu
	 * this method is useful to know if this institution has a schedule
	 * available
	 * @param haveSchedule the new have schedule
	 * @return true, if is have schedule
	 */
//	public boolean isHaveSchedule(){
//		if(Validations.validateIsNull(this.getSchedules()) || this.getSchedules().size()<1){
//			return false;
//		}
//		return true;
//	}
	
	/**
	 * Sets the have schedule.
	 *
	 * @param haveSchedule the new have schedule
	 */
	public void setHaveSchedule(boolean haveSchedule) {
		this.haveSchedule = haveSchedule;
	}
	
	/**
	 * Gets the institution type name.
	 *
	 * @return the institution type name
	 */
	public String getInstitutionTypeName(){
		if(Validations.validateIsNotNull(this.getIdInstitutionTypeFk())){
			return InstitutionType.lookup.get(this.getIdInstitutionTypeFk()).getValue();
		}
		return null;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the id participante.
	 *
	 * @return the id participante
	 */
	public Long getIdParticipante() {
		return idParticipante;
	}

	/**
	 * Sets the id participante.
	 *
	 * @param idParticipante the new id participante
	 */
	public void setIdParticipante(Long idParticipante) {
		this.idParticipante = idParticipante;
	}

	/**
	 * Gets the id issuer.
	 *
	 * @return the id issuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}

	/**
	 * Sets the id issuer.
	 *
	 * @param idIssuer the new id issuer
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idInstitutionSecurityPk == null) ? 0
						: idInstitutionSecurityPk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstitutionsSecurity other = (InstitutionsSecurity) obj;
		if (idInstitutionSecurityPk == null) {
			if (other.idInstitutionSecurityPk != null)
				return false;
		} else if (!idInstitutionSecurityPk
				.equals(other.idInstitutionSecurityPk))
			return false;
		return true;
	}
	
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("userAccounts", userAccounts);
		detailsMap.put("macAddressDetails", macAddressDetails);
        return detailsMap;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
	
	
}