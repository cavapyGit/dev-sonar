package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Enum SystemSituationType.
 */
public enum SystemSituationType {

	/** The active. */
	ACTIVE(Integer.valueOf(145), "ACTIVO", "active.png"),
	 
	/** The pending lock. */
	PENDING_LOCK(Integer.valueOf(146), "BLOQUEO PENDIENTE", "pending_lock.png"),
	
	/** The pending unlock. */
	PENDING_UNLOCK(Integer.valueOf(147),"DESBLOQUEO PENDIENTE", "pending_unlock.png");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<SystemSituationType> list = new ArrayList<SystemSituationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, SystemSituationType> lookup = new HashMap<Integer, SystemSituationType>();

	static {
		for (SystemSituationType s : EnumSet.allOf(SystemSituationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new system situation type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private SystemSituationType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the system situation type
	 */
	public static SystemSituationType get(Integer code) {
		return lookup.get(code);
	}
}
