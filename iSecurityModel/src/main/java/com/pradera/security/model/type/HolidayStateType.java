package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum HolidayStateType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum HolidayStateType {

	
	/** The registered. */
	REGISTERED(Integer.valueOf(1), "REGISTRADO"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(0), "ELIMINADO");

	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<HolidayStateType> list = new ArrayList<HolidayStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, HolidayStateType> lookup = new HashMap<Integer, HolidayStateType>();

	static {
		for (HolidayStateType s : EnumSet.allOf(HolidayStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}		

	/**
	 * Instantiates a new holiday state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private HolidayStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}		

}




