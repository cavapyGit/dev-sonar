package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum UserProfileStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum UserProfileStateType {
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1),"REGISTERED"),
	
	/** The deleted. */
	DELETED(Integer.valueOf(2),"DELETED");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<UserProfileStateType> list = new ArrayList<UserProfileStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, UserProfileStateType> lookup = new HashMap<Integer, UserProfileStateType>();

	static {
		for (UserProfileStateType s : EnumSet.allOf(UserProfileStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new user profile state type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private UserProfileStateType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}	

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the valor.
	 *
	 * @param value the new valor
	 */
	public void setValor(String value) {
		this.value = value;
	}

	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param key the key
	 * @return the user profile state type
	 */
	public static UserProfileStateType get(Integer key) {
		return lookup.get(key);
	}
}
