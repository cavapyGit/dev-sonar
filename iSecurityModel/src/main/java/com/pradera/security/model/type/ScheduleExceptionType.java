/*
 * 
 */
package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum ScheduleExceptionType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum ScheduleExceptionType {
	
	/** The access acept. */
	ACCESS_ACEPT(Integer.valueOf(1),"PERMITIR ACCESO"),
	
	/** The access refuse. */
	ACCESS_REFUSE(Integer.valueOf(2),"DENEGAR ACCESO");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private ScheduleExceptionType(int code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/** The Constant list. */
	public static final List<ScheduleExceptionType> list = new ArrayList<ScheduleExceptionType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ScheduleExceptionType> lookup = new HashMap<Integer, ScheduleExceptionType>();
	static {
		for (ScheduleExceptionType s : EnumSet.allOf(ScheduleExceptionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

}
