package com.pradera.security.model.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Enum AuditTableStateType.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05-mar-2015
 */
public enum AuditTableStateType {

	/** The registered. */
	REGISTERED(Integer.valueOf(162)),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(163)),
	
	/** The disabled. */
	DISABLED(Integer.valueOf(164)),
	
	REJECTED(Integer.valueOf(168));
	
	/** The code. */
	private Integer code;
	
	/** The Constant lookup. */
	public static final Map<Integer, AuditTableStateType> lookup = new HashMap<Integer, AuditTableStateType>();

	static {
		for (AuditTableStateType s : EnumSet.allOf(AuditTableStateType.class)) {
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new audit table state type.
	 *
	 * @param code the code
	 */
	private AuditTableStateType(Integer code){
		this.code=code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the audit table state type
	 */
	public static AuditTableStateType get(Integer code){
		return lookup.get(code);
	}
}
