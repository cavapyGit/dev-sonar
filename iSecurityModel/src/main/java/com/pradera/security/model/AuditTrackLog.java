package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the AUDIT_TRACK_LOG database table.
 * 
 */
@Entity
@Table(name="AUDIT_TRACK_LOG")
public class AuditTrackLog implements Serializable,Auditable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id audit track pk. */
	@Id
	@SequenceGenerator(name="AUDIT_TRACK_LOG_IDAUDITTRACKPK_GENERATOR", sequenceName="SQ_ID_AUDIT_TRACK_PK")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AUDIT_TRACK_LOG_IDAUDITTRACKPK_GENERATOR")
	@Column(name="ID_AUDIT_TRACK_PK")
	private Long idAuditTrackPk;

	/** The system. */
	@ManyToOne
	@JoinColumn(name="ID_SYSTEM_FK",referencedColumnName="ID_SYSTEM_PK")
	private System system;

	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The name table. */
	@Column(name="NAME_TABLE")
	private String nameTable;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

    /** The user register. */
    @Column(name="USER_REGISTER")
	private String userRegister;

	//bi-directional many-to-one association to AuditTrackField
	/** The audit track fields. */
	@OneToMany(mappedBy="auditTrackLog")
	private List<AuditTrackField> auditTrackFields;
	
	/**
	 * Gets the id audit track pk.
	 *
	 * @return the idAuditTrackPk
	 */
	public Long getIdAuditTrackPk() {
		return idAuditTrackPk;
	}

	/**
	 * Sets the id audit track pk.
	 *
	 * @param idAuditTrackPk the idAuditTrackPk to set
	 */
	public void setIdAuditTrackPk(Long idAuditTrackPk) {
		this.idAuditTrackPk = idAuditTrackPk;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the system to set
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the name table.
	 *
	 * @return the nameTable
	 */
	public String getNameTable() {
		return nameTable;
	}

	/**
	 * Sets the name table.
	 *
	 * @param nameTable the nameTable to set
	 */
	public void setNameTable(String nameTable) {
		this.nameTable = nameTable;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the user register.
	 *
	 * @return the userRegister
	 */
	public String getUserRegister() {
		return userRegister;
	}

	/**
	 * Sets the user register.
	 *
	 * @param userRegister the userRegister to set
	 */
	public void setUserRegister(String userRegister) {
		this.userRegister = userRegister;
	}

	/**
	 * Gets the audit track fields.
	 *
	 * @return the auditTrackFields
	 */
	public List<AuditTrackField> getAuditTrackFields() {
		return auditTrackFields;
	}

	/**
	 * Sets the audit track fields.
	 *
	 * @param auditTrackFields the auditTrackFields to set
	 */
	public void setAuditTrackFields(List<AuditTrackField> auditTrackFields) {
		this.auditTrackFields = auditTrackFields;
	}

	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("auditTrackFields", auditTrackFields);
        return detailsMap;
	}
	
}