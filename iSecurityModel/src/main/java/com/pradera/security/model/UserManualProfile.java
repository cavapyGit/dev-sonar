package com.pradera.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the USER_MANUAL_PROFILE database table.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 15-abr-2015
 */
@Entity
@Table(name="USER_MANUAL_PROFILE")
@NamedQuery(name="UserManualProfile.getUserFile", query="SELECT u FROM UserManualProfile u where u.id.idSystemOptionPk = :option and u.id.idSystemProfilePk= :profile")
public class UserManualProfile implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public static final String GET_USER_MANUAL="UserManualProfile.getUserFile";

	/** The id. */
	@EmbeddedId
	private UserManualProfilePK id;

	/** The file name. */
	@Column(name="FILE_NAME")
	private String fileName;

	/** The url directory. */
	@Column(name="URL_DIRECTORY")
	private String urlDirectory;

	//uni-directional many-to-one association to SystemOption1
	/** The system option. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SYSTEM_OPTION_PK",insertable=false,updatable=false)
	private SystemOption systemOption;

	//uni-directional many-to-one association to SystemProfile1
	/** The system profile. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SYSTEM_PROFILE_PK",insertable=false,updatable=false)
	private SystemProfile systemProfile;

	/**
	 * Instantiates a new user manual profile.
	 */
	public UserManualProfile() {
		this.id = new UserManualProfilePK();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public UserManualProfilePK getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(UserManualProfilePK id) {
		this.id = id;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return this.fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the url directory.
	 *
	 * @return the url directory
	 */
	public String getUrlDirectory() {
		return this.urlDirectory;
	}

	/**
	 * Sets the url directory.
	 *
	 * @param urlDirectory the new url directory
	 */
	public void setUrlDirectory(String urlDirectory) {
		this.urlDirectory = urlDirectory;
	}

	/**
	 * Gets the system option.
	 *
	 * @return the system option
	 */
	public SystemOption getSystemOption() {
		return this.systemOption;
	}

	/**
	 * Sets the system option.
	 *
	 * @param systemOption the new system option
	 */
	public void setSystemOption(SystemOption systemOption) {
		this.systemOption = systemOption;
	}

	/**
	 * Gets the system profile.
	 *
	 * @return the system profile
	 */
	public SystemProfile getSystemProfile() {
		return this.systemProfile;
	}

	/**
	 * Sets the system profile.
	 *
	 * @param systemProfile the new system profile
	 */
	public void setSystemProfile(SystemProfile systemProfile) {
		this.systemProfile = systemProfile;
	}

}