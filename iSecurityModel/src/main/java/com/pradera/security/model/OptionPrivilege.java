package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the OPTION_PRIVILEGE database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name = OptionPrivilege.OPTION_PRIVILEGE, query = "Select p From OptionPrivilege p WHERE p.idPrivilege = :idPrivilege and p.systemOption.idSystemOptionPk =:idOption",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") }),
	@NamedQuery(name= OptionPrivilege.OPTION_PRIVILEGE_LOGIN, query="Select OP.idOptionPrivilegePk,OP.idPrivilege,OP.state,SO.idSystemOptionPk,SO.urlOption,SO.optionType,SO.orderOption "
			+ " From UserAccount UA Join UA.userProfiles UP Join UP.systemProfile SP "
			+ " Join SP.profilePrivileges PP Join PP.optionPrivilege OP Join OP.systemOption SO Join SO.system SYS Where UA.state = :userStatePrm And UA.idUserPk = :idUserPrm "
			+ " And UP.state = :userProfileStatePrm And SP.state = :systemProfilePrm And PP.state = :profilePrivilegeStatePrm And OP.state = :optionPrivilegeStatePrm "
			+ " And SO.state = :systemOptionStatePrm And SYS.state = :systemStatePrm And SYS.idSystemPk = :idSystemPrm ",hints ={@QueryHint(name="org.hibernate.cacheable", value="true")})
})
@Entity
@Table(name="OPTION_PRIVILEGE")
@Cacheable(true)
public class OptionPrivilege implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant OPTION_PRIVILEGE. */
	public static final String OPTION_PRIVILEGE = "OptionPrivilege.seachPrivilegeById";	
	
	/** The Constant OPTION_PRIVILEGE_LOGIN. */
	public static final String OPTION_PRIVILEGE_LOGIN="OptionPrivilege.searchBySystemPrivileges";
	
	/** The id option privilege pk. */
	@Id
	@SequenceGenerator(name="OPTION_PRIVILEGE_IDOPTIONPRIVILEGEPK_GENERATOR", sequenceName="SQ_ID_OPTION_PRIVILEGE_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OPTION_PRIVILEGE_IDOPTIONPRIVILEGEPK_GENERATOR")
	@Column(name="ID_OPTION_PRIVILEGE_PK")
	private Integer idOptionPrivilegePk;

	/** The id privilege. */
	@Column(name="ID_PRIVILEGE")
	private Integer idPrivilege;

    /** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

    /** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to SystemOption
    /** The system option. */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_SYSTEM_OPTION_FK",referencedColumnName="ID_SYSTEM_OPTION_PK")
	private SystemOption systemOption;

	//bi-directional many-to-one association to ProfilePrivilege
	/** The profile privileges. */
	@OneToMany(mappedBy="optionPrivilege")
	private List<ProfilePrivilege> profilePrivileges;

	//bi-directional many-to-one association to UserPrivilegeDetail
	/** The user privilege details. */
	@OneToMany(mappedBy="optionPrivilege")
	private List<UserPrivilegeDetail> userPrivilegeDetails;
	
	/** The state. */
	@Column(name="\"STATE\"")
	private Integer state;

    /**
     * Instantiates a new option privilege.
     */
    public OptionPrivilege() {
    }

	/**
	 * Gets the id option privilege pk.
	 *
	 * @return the id option privilege pk
	 */
	public Integer getIdOptionPrivilegePk() {
		return idOptionPrivilegePk;
	}

	/**
	 * Sets the id option privilege pk.
	 *
	 * @param idOptionPrivilegePk the new id option privilege pk
	 */
	public void setIdOptionPrivilegePk(Integer idOptionPrivilegePk) {
		this.idOptionPrivilegePk = idOptionPrivilegePk;
	}

	/**
	 * Gets the id privilege.
	 *
	 * @return the id privilege
	 */
	public Integer getIdPrivilege() {
		return idPrivilege;

	}


	/**
	 * Sets the id privilege.
	 *
	 * @param idPrivilege the new id privilege
	 */
	public void setIdPrivilege(Integer idPrivilege) {

		this.idPrivilege = idPrivilege;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}


	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;

	}


	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the system option.
	 *
	 * @return the system option
	 */
	public SystemOption getSystemOption() {
		return systemOption;
	}

	/**
	 * Sets the system option.
	 *
	 * @param systemOption the new system option
	 */
	public void setSystemOption(SystemOption systemOption) {
		this.systemOption = systemOption;
	}

	/**
	 * Gets the profile privileges.
	 *
	 * @return the profile privileges
	 */
	public List<ProfilePrivilege> getProfilePrivileges() {
		return profilePrivileges;
	}

	/**
	 * Sets the profile privileges.
	 *
	 * @param profilePrivileges the new profile privileges
	 */
	public void setProfilePrivileges(List<ProfilePrivilege> profilePrivileges) {
		this.profilePrivileges = profilePrivileges;
	}

	/**
	 * Gets the user privilege details.
	 *
	 * @return the user privilege details
	 */
	public List<UserPrivilegeDetail> getUserPrivilegeDetails() {
		return userPrivilegeDetails;
	}

	/**
	 * Sets the user privilege details.
	 *
	 * @param userPrivilegeDetails the new user privilege details
	 */
	public void setUserPrivilegeDetails(
			List<UserPrivilegeDetail> userPrivilegeDetails) {
		this.userPrivilegeDetails = userPrivilegeDetails;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}        
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("userPrivilegeDetails", userPrivilegeDetails);
		detailsMap.put("profilePrivileges", profilePrivileges);
        return detailsMap;
	}

	@Override
	public String toString() {
		return "OptionPrivilege [idOptionPrivilegePk=" + idOptionPrivilegePk + ", idPrivilege=" + idPrivilege
				+ ", lastModifyDate=" + lastModifyDate + ", lastModifyIp=" + lastModifyIp + ", lastModifyPriv="
				+ lastModifyPriv + ", lastModifyUser=" + lastModifyUser + ", registryDate=" + registryDate
				+ ", registryUser=" + registryUser + ", state=" + state + "]";
	}

	
}