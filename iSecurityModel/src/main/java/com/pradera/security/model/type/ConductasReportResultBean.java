package com.pradera.security.model.type;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  class ConductasReportResultBean bean.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class ConductasReportResultBean {
	
	/** The conducta. */
	private String conducta;
	
	/** The menu name. */
	private String menuName;
	
	/** The level. */
	private Integer level;
	
	/** The url option. */
	private String urlOption;
	
	/** The status. */
	private String status;
	
	/**
	 * Gets the conducta.
	 *
	 * @return the conducta
	 */
	public String getConducta() {
		return conducta;
	}
	
	/**
	 * Sets the conducta.
	 *
	 * @param conducta the conducta to set
	 */
	public void setConducta(String conducta) {
		this.conducta = conducta;
	}
	
	/**
	 * Gets the menu name.
	 *
	 * @return the menuName
	 */
	public String getMenuName() {
		return menuName;
	}
	
	/**
	 * Sets the menu name.
	 *
	 * @param menuName the menuName to set
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	
	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}
	
	/**
	 * Sets the level.
	 *
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	/**
	 * Gets the url option.
	 *
	 * @return the urlOption
	 */
	public String getUrlOption() {
		return urlOption;
	}
	
	/**
	 * Sets the url option.
	 *
	 * @param urlOption the urlOption to set
	 */
	public void setUrlOption(String urlOption) {
		this.urlOption = urlOption;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
