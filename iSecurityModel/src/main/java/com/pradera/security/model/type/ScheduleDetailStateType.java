/*
 * 
 */
package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum ScheduleDetailStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum ScheduleDetailStateType{
	
	
	/** The registred. */
	REGISTERED(Integer.valueOf(1),"REGISTRADO","registred.png"),
	
	/** The locked. */
	BLOCKED(Integer.valueOf(2),"BLOQUEADO","locked.png");
		
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}
	
	/**
	 * Sets the icon.
	 *
	 * @param icon the icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private ScheduleDetailStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}
	
	/** The Constant list. */
	public static final List<ScheduleDetailStateType> list = new ArrayList<ScheduleDetailStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ScheduleDetailStateType> lookup = new HashMap<Integer, ScheduleDetailStateType>();
	static {
		for (ScheduleDetailStateType s : EnumSet.allOf(ScheduleDetailStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
}
