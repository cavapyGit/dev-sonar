package com.pradera.security.model.type;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  class AuditoriaReportResultBean bean.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class AuditoriaReportResultBean {
	
	/** The user name. */
	private String userName;
	
	/** The ip address. */
	private String ipAddress;
	
	/** The acc date. */
	private String accDate;
	
	/** The menu name. */
	private String menuName;
	
	/** The id user audit. */
	private Integer idUserAudit;
	
	/** The system name. */
	private String systemName;
	
	/**
	 * Gets the system name.
	 *
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}
	
	/**
	 * Sets the system name.
	 *
	 * @param systemName the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
	/**
	 * Gets the user name.
	 *
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Sets the user name.
	 *
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Gets the ip address.
	 *
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}
	
	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * Gets the acc date.
	 *
	 * @return the accDate
	 */
	public String getAccDate() {
		return accDate;
	}
	
	/**
	 * Sets the acc date.
	 *
	 * @param accDate the accDate to set
	 */
	public void setAccDate(String accDate) {
		this.accDate = accDate;
	}
	
	/**
	 * Gets the menu name.
	 *
	 * @return the parameterName
	 */
	public String getMenuName() {
		return menuName;
	}
	
	/**
	 * Sets the menu name.
	 *
	 * @param menuName the new menu name
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	
	/**
	 * Gets the id user audit.
	 *
	 * @return the idUserAudit
	 */
	public Integer getIdUserAudit() {
		return idUserAudit;
	}
	
	/**
	 * Sets the id user audit.
	 *
	 * @param idUserAudit the idUserAudit to set
	 */
	public void setIdUserAudit(Integer idUserAudit) {
		this.idUserAudit = idUserAudit;
	}
}
