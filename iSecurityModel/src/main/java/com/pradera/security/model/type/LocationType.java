package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
* </ul>
* 
* The  enum LocationType.
*
* @author : PraderaTechnologies.
* @version 1.0
* @Project : PraderaSecurity
* @Creation_Date :
*/

public enum LocationType {
	
	/** The location BE. */
	BE(Integer.valueOf(188),"BENI"),
	
	/** The location BE. */
	CB(Integer.valueOf(189),"COCHABAMBA"),
	
	/** The location BE. */
	LP(Integer.valueOf(190),"LA PAZ"),
	
	/** The location BE. */
	OR(Integer.valueOf(191),"ORURO"),
	
	/** The location BE. */
	PA(Integer.valueOf(192),"PANDO"),
	
	/** The location BE. */
	PO(Integer.valueOf(193),"POTOSI"),
	
	/** The location BE. */
	CH(Integer.valueOf(194),"SUCRE"),
	
	/** The location BE. */
	SC(Integer.valueOf(195),"SANTA CRUZ"),
	
	/** The location BE. */
	TJ(Integer.valueOf(196),"TARIJA");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<LocationType> list = new ArrayList<LocationType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, LocationType> lookup = new HashMap<Integer, LocationType>();
	
	static {
		for (LocationType s : EnumSet.allOf(LocationType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new location type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private LocationType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the motive type
	 */
	public static LocationType get(Integer code) {
		return lookup.get(code);
	}
	
}
