/*
 * 
 */
package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum ScheduleExceptionStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum ScheduleExceptionStateType{
	
	/** The registred. */
	REGISTERED(Integer.valueOf(1),"REGISTRADO","registered.png"),
	
	/** The locked. */
	LOCKED(Integer.valueOf(2),"BLOQUEADO","locked.png"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(4),"CONFIRMADO","confirmed.png"),
	
	/** The confirmed. */
	REVOCATE(Integer.valueOf(0),"REVOCADO","remove.png"),
	
	/** The registred. */
	DELETED(Integer.valueOf(3),"ELIMINADO","rejected.png"),
	
	/** The reject. */
	REJECT(Integer.valueOf(5),"RECHAZADO","rejected.png");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 *
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}
	
	/**
	 * Sets the icon.
	 *
	 * @param icon the icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	/**
	 * The Constructor.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private ScheduleExceptionStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}
	
	/** The Constant list. */
	public static final List<ScheduleExceptionStateType> list = new ArrayList<ScheduleExceptionStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, ScheduleExceptionStateType> lookup = new HashMap<Integer, ScheduleExceptionStateType>();
	static {
		for (ScheduleExceptionStateType s : EnumSet.allOf(ScheduleExceptionStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Gets the some elements.
	 *
	 * @param scheduleStateTypeArray the schedule state type array
	 * @return the some elements
	 */
	public static List<ScheduleExceptionStateType> getSomeElements(ScheduleExceptionStateType... scheduleStateTypeArray){
		List<ScheduleExceptionStateType> returnList = new ArrayList<ScheduleExceptionStateType>();
		for(ScheduleExceptionStateType scheduleStateType: scheduleStateTypeArray){
			returnList.add(scheduleStateType);
		}
		return returnList;
	}
}
