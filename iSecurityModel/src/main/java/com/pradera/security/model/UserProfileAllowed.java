package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UserProfileAllowed.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 26-feb-2015
 */
@Entity
@Table(name="USER_PROFILE_ALLOWED")
public class UserProfileAllowed implements Serializable, Auditable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8332054827386335586L;
	
	@Id
	@SequenceGenerator(name="USER_PROFILE_ALLOWED_IDUSERPROFILEPK_GENERATOR", sequenceName="SQ_ID_USER_PROFILE_ALLOWED_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_PROFILE_ALLOWED_IDUSERPROFILEPK_GENERATOR")
	@Column(name="ID_USER_PROFILE_ALLOWED_PK")
	/** The id user profile allowed pk. */
	private Integer idUserProfileAllowedPk;
	
	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state. */
	@Column(name="PROFILE_STATE")
	private Integer profileState;

	//bi-directional many-to-one association to SystemProfile
    /** The system profile. */
	@ManyToOne
	@JoinColumn(name="ID_SYSTEM_PROFILE_FK")
	private SystemProfile systemProfile;

	//bi-directional many-to-one association to UserAccount
    /** The user account. */
	@ManyToOne
	@JoinColumn(name="ID_USER_FK")
	private UserAccount userAccount;

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#setAudit(com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	public void setAudit(LoggerUser loggerUser) {
		if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}

	/**
	 * Gets the id user profile allowed pk.
	 *
	 * @return the id user profile allowed pk
	 */
	public Integer getIdUserProfileAllowedPk() {
		return idUserProfileAllowedPk;
	}

	/**
	 * Sets the id user profile allowed pk.
	 *
	 * @param idUserProfileAllowedPk the new id user profile allowed pk
	 */
	public void setIdUserProfileAllowedPk(Integer idUserProfileAllowedPk) {
		this.idUserProfileAllowedPk = idUserProfileAllowedPk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the profile state.
	 *
	 * @return the profile state
	 */
	public Integer getProfileState() {
		return profileState;
	}

	/**
	 * Sets the profile state.
	 *
	 * @param profileState the new profile state
	 */
	public void setProfileState(Integer profileState) {
		this.profileState = profileState;
	}

	/**
	 * Gets the system profile.
	 *
	 * @return the system profile
	 */
	public SystemProfile getSystemProfile() {
		return systemProfile;
	}

	/**
	 * Sets the system profile.
	 *
	 * @param systemProfile the new system profile
	 */
	public void setSystemProfile(SystemProfile systemProfile) {
		this.systemProfile = systemProfile;
	}

	/**
	 * Gets the user account.
	 *
	 * @return the user account
	 */
	public UserAccount getUserAccount() {
		return userAccount;
	}

	/**
	 * Sets the user account.
	 *
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

}
