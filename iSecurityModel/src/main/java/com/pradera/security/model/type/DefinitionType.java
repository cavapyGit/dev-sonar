package com.pradera.security.model.type;


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum DefinitionType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum DefinitionType {
	
	/** The system states. */
	SYSTEM_STATES(Integer.valueOf(1),"ESTADOS_SISTEMA"),
	
	/** The privileges option. */
	PRIVILEGES_OPTION(Integer.valueOf(6),"PRIVILEGIOS_OPCION"),
	
	/** The user types. */
	USER_TYPES(Integer.valueOf(11),"TIPOS_USUARIO"),
	
	/** The action motives. */
	ACTION_MOTIVES(Integer.valueOf(4),"MOTIVOS_ACCION"),
	
	/** The block motives. */
	BLOCK_MOTIVES(Integer.valueOf(13),"MOTIVOS_BLOCK"),
	
	/** The desblock motives. */
	DESBLOCK_MOTIVES(Integer.valueOf(14),"MOTIVOS_UNBLOCK"),
	
	/** The system block motives. */
	SYSTEM_BLOCK_MOTIVES(Integer.valueOf(15),"SYSTEM_MOTIVOS_BLOCK"),
	
	/** The system desblock motives. */
	SYSTEM_DESBLOCK_MOTIVES(Integer.valueOf(16),"SYSTEM_MOTIVOS_UNBLOCK"),
	
	/** The sys opt block motives. */
	SYS_OPT_BLOCK_MOTIVES(Integer.valueOf(17),"SYS_OPT_MOTIVOS_BLOCK"),
	
	/** The sys opt desblock motives. */
	SYS_OPT_DESBLOCK_MOTIVES(Integer.valueOf(18),"SYS_OPT_MOTIVOS_UNBLOCK"),
	
	/** The sys profile block motives. */
	SYS_PROFILE_BLOCK_MOTIVES(Integer.valueOf(19),"SYS_PROFILE_MOTIVOS_BLOCK"),
	
	/** The sys profile desblock motives. */
	SYS_PROFILE_DESBLOCK_MOTIVES(Integer.valueOf(20),"SYS_PROFILE_MOTIVOS_UNBLOCK"),
	
	/** The sys schedule block motives. */
	SYS_SCHEDULE_BLOCK_MOTIVES(Integer.valueOf(21),"SYS_SCHEDULE_MOTIVOS_BLOCK"),
	
	/** The sys schedule desblock motives. */
	SYS_SCHEDULE_DESBLOCK_MOTIVES(Integer.valueOf(22),"SYS_SCHEDULE_MOTIVOS_UNBLOCK"),
	
	/** The schedule exception delete motives. */
	SCHEDULE_EXCEPTION_DELETE_MOTIVES(Integer.valueOf(23),"SCHEDULE_EXCEPTION_MOTIVOS_DELETE"),
	
	/** The holiday delete motives. */
	HOLIDAY_DELETE_MOTIVES(Integer.valueOf(24),"HOLIDAY_MOTIVOS_DELETE"),
	
	/** The user exception block motives. */
	USER_EXCEPTION_BLOCK_MOTIVES(Integer.valueOf(25),"USER_EXCEPTION_MOTIVOS_BLOCK"),
	
	/** The user exception desblock motives. */
	USER_EXCEPTION_DESBLOCK_MOTIVES(Integer.valueOf(26),"USER_EXCEPTION_MOTIVOS_UNBLOCK"),
	
	/** The user exception delete motives. */
	USER_EXCEPTION_DELETE_MOTIVES(Integer.valueOf(41),"USER_EXCEPTION_MOTIVOS_DELETE"),
	
	USER_EXCEPTION_REJECTED_MOTIVES(Integer.valueOf(30),"USER_EXCEPTION_MOTIVOS_DELETE"),
	
	/** The sys opt reject motives. */
	SYS_OPT_REJECT_MOTIVES(Integer.valueOf(27),"SYS_OPT_MOTIVOS_REJECT"),
	
	/** The sys opt delete motives. */
	SYS_OPT_DELETE_MOTIVES(Integer.valueOf(28),"SYS_OPT_MOTIVOS_DELETE"),
	
	/** The user reject motives. */
	USER_REJECT_MOTIVES(Integer.valueOf(29),"USER_MOTIVOS_REJECT"),
	
	/** The system reject register motives. */
	SYSTEM_REJECT_REGISTER_MOTIVES(Integer.valueOf(32),"SYSTEM_REJECT_REGISTER_MOTIVES"),
	
	/** The system reject block motives. */
	SYSTEM_REJECT_BLOCK_MOTIVES(Integer.valueOf(33),"SYSTEM_REJECT_REGISTER_MOTIVES"),
	
	/** The sys profile reject motives. */
	SYS_PROFILE_REJECT_MOTIVES(Integer.valueOf(35),"SYS_PROFILE_MOTIVOS_REJECT"),
	
	/** The sys schedule reject motives. */
	SYS_SCHEDULE_REJECT_MOTIVES(Integer.valueOf(36),"SYS_SCHEDULE_MOTIVOS_REJECT"),
	
	/** The schedule exception reject motives. */
	SCHEDULE_EXCEPTION_REJECT_MOTIVES(Integer.valueOf(37),"SCHEDULE_EXCEPTION_MOTIVOS_REJECT"),
	
	/** The user unregister motives. */
	USER_UNREGISTER_MOTIVES(Integer.valueOf(38),"USER_UNREGISTER_MOTIVES"),
	
	AUDIT_TABLES_RULE_STITUATION(Integer.valueOf(39),"AUDIT_TABLES_SITUATION"),
	
	AUDIT_TABLES_RULE_STATE(Integer.valueOf(40),"AUDIT_TABLES_STATE"),
	
	/** The location. */
	DEPARTAMENT(Integer.valueOf(44),"DEPARTAMENTOS"),
	
	/** The profile type. */
	PROFILE_TYPE(Integer.valueOf(45),"TIPOS DE PERFIL");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<DefinitionType> list = new ArrayList<DefinitionType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, DefinitionType> lookup = new HashMap<Integer, DefinitionType>();

	static {
		for (DefinitionType s : EnumSet.allOf(DefinitionType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new definition type.
	 *
	 * @param codigo the codigo
	 * @param valor the valor
	 */
	private DefinitionType(Integer codigo, String valor) {
		this.code = codigo;
		this.value = valor;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param codigo the codigo
	 * @return the definition type
	 */
	public static DefinitionType get(Integer codigo) {
		return lookup.get(codigo);
	}
}
