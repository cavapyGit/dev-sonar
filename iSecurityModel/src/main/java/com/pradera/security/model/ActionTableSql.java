package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

@Entity
@Table(name ="ACTION_TABLE_SQL")
public class ActionTableSql implements Serializable,Auditable {

	/** The id action tables pk. */
	@Id
	@SequenceGenerator(name="ACTION_TABLE_SQL_GENERATOR", sequenceName="SQ_ID_ACTION_TABLE_SQL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACTION_TABLE_SQL_GENERATOR")
	@Column(name="ID_ACTION_TABLES_SQL_PK")
	private Long idActionTableSqlPk;
	
	@Column(name="VERSION")
	private Long version;
		
	/** The system. */
	@ManyToOne
	@JoinColumn(name="ID_ACTION_TABLES_FK",referencedColumnName="ID_ACTION_TABLES_PK")
	private ActionTable actionTable;
	
	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;
	
	/** The state. */
	@Column(name="STATE")
	private Integer state;
	
	/** The state. */
	@Column(name="ORDER_SQL")
	private Integer orderSql;
	
	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;
	
	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;
	
	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;
	
	/** The last modify app. */
	@Column(name="LAST_MODIFY_APP")
	private Integer lastModifyApp;
	
	/** The trigger rule. */
	@Column(name="TRIGGER_RULE")
	private String triggerRule;

	/** The trigger rule. */
	@Column(name="NUMBER_SQL")
	private Integer sqlNumber;

	public Long getIdActionTableSqlPk() {
		return idActionTableSqlPk;
	}

	public void setIdActionTableSqlPk(Long idActionTableSqlPk) {
		this.idActionTableSqlPk = idActionTableSqlPk;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public ActionTable getActionTable() {
		return actionTable;
	}

	public void setActionTable(ActionTable actionTable) {
		this.actionTable = actionTable;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public String getTriggerRule() {
		return triggerRule;
	}

	public void setTriggerRule(String triggerRule) {
		this.triggerRule = triggerRule;
	}

	public Integer getOrderSql() {
		return orderSql;
	}

	public void setOrderSql(Integer orderSql) {
		this.orderSql = orderSql;
	}

	public Integer getSqlNumber() {
		return sqlNumber;
	}

	public void setSqlNumber(Integer sqlNumber) {
		this.sqlNumber = sqlNumber;
	}

	@Override
	public void setAudit(LoggerUser loggerUser) {
		 // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
            lastModifyApp = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }		
	}

	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	
	
}
