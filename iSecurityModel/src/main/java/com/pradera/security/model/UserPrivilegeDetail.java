package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the USER_PRIVILEGE_DETAIL database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="USER_PRIVILEGE_DETAIL")
public class UserPrivilegeDetail implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id user privilege detail pk. */
	@Id
	@SequenceGenerator(name="USER_PRIVILEGE_DETAIL_IDUSERPRIVILEGEDETAILPK_GENERATOR", sequenceName="SQ_ID_USER_PRIVILEGE_DETAIL_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_PRIVILEGE_DETAIL_IDUSERPRIVILEGEDETAILPK_GENERATOR")
	@Column(name="ID_USER_PRIVILEGE_DETAIL_PK")
	private Integer idUserPrivilegeDetailPk;

	/** The last modify date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	//bi-directional many-to-one association to ExceptionUserPrivilege
	/** The exception user privilege. */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_EXCEPTION_USER_PRIVILEGE_FK",referencedColumnName="ID_EXCEPTION_USER_PRIVILEGE_PK")
	private ExceptionUserPrivilege exceptionUserPrivilege;

	//bi-directional many-to-one association to OptionPrivilege
    /** The option privilege. */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PRIVILEGE_FK")
	private OptionPrivilege optionPrivilege;

    /**
     * Instantiates a new user privilege detail.
     */
    public UserPrivilegeDetail() {
    }

	/**
	 * Gets the id user privilege detail pk.
	 *
	 * @return the id user privilege detail pk
	 */
	public Integer getIdUserPrivilegeDetailPk() {
		return this.idUserPrivilegeDetailPk;
	}

	/**
	 * Sets the id user privilege detail pk.
	 *
	 * @param idUserPrivilegeDetailPk the new id user privilege detail pk
	 */
	public void setIdUserPrivilegeDetailPk(Integer idUserPrivilegeDetailPk) {
		this.idUserPrivilegeDetailPk = idUserPrivilegeDetailPk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the exception user privilege.
	 *
	 * @return the exception user privilege
	 */
	public ExceptionUserPrivilege getExceptionUserPrivilege() {
		return this.exceptionUserPrivilege;
	}

	/**
	 * Sets the exception user privilege.
	 *
	 * @param exceptionUserPrivilege the new exception user privilege
	 */
	public void setExceptionUserPrivilege(ExceptionUserPrivilege exceptionUserPrivilege) {
		this.exceptionUserPrivilege = exceptionUserPrivilege;
	}
	
	/**
	 * Gets the option privilege.
	 *
	 * @return the option privilege
	 */
	public OptionPrivilege getOptionPrivilege() {
		return this.optionPrivilege;
	}

	/**
	 * Sets the option privilege.
	 *
	 * @param optionPrivilege the new option privilege
	 */
	public void setOptionPrivilege(OptionPrivilege optionPrivilege) {
		this.optionPrivilege = optionPrivilege;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
        return detailsMap;
	}
	
}