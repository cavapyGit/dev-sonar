package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.security.model.type.UserExceptionStateType;


/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class EXCEPTION_USER_PRIVILEGE
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="EXCEPTION_USER_PRIVILEGE")
public class ExceptionUserPrivilege implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id exception user privilege pk. */
	@Id
	@SequenceGenerator(name="ID_EXCEPTIONUSERPRIVILEGEPK_GENERATOR", sequenceName="SQ_ID_EXCEPTION_USER_PRIV_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ID_EXCEPTIONUSERPRIVILEGEPK_GENERATOR")
	@Column(name="ID_EXCEPTION_USER_PRIVILEGE_PK")
	private Integer idExceptionUserPrivilegePk;
	
	/** The version. */
	@Version
	private Long version;

	/** The description. */
	private String description;

	/** The end date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="END_DATE")
	private Date endDate;

    /** The initial date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="INITIAL_DATE")
	private Date initialDate;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The name. */
	private String name;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	private Integer state;

	//bi-directional many-to-one association to UserAccount
	/** The user account. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USER_FK")
	private UserAccount userAccount;
	
	/** The system. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SYSTEM_FK")
	private System system;

	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	private Integer blockMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;

	/** The document file. */
	@Lob
//    @Basic(fetch=FetchType.LAZY)
	@Column(name="DOCUMENT_FILE")
	private byte[] documentFile;
	
	/** The profile name. */
	@Column(name="NAME_FILE")
	private String nameFile;
	
	
	/** The situation. */
	private Integer situation;
	
	//bi-directional many-to-one association to UserPrivilegeDetail
	/** The user privilege details. */
	@OneToMany(cascade=CascadeType.PERSIST,mappedBy="exceptionUserPrivilege")
	private List<UserPrivilegeDetail> userPrivilegeDetails;
	
	@PreUpdate
	public void loadFilePreUpdate(){
		getDocumentFile();
	}

	/**
	 * Gets the icon state.
	 *
	 * @return the icon state
	 */
	public String getIconState() {
		String icon = null;
		if (state != null) {
			icon = UserExceptionStateType.get(state).getIcon();
		}
		return icon;
     }
 
    /**
     * Gets the description state option.
     *
     * @return the description state option
     */
    public String getDescriptionStateOption() {
		String descripcion = null;
		if (state != null) {
			descripcion = UserExceptionStateType.get(state).getValue();
		}
		return descripcion;
    }
	
    /**
     * Instantiates a new exception user privilege.
     */
    public ExceptionUserPrivilege() {
    }

	/**
	 * Gets the id exception user privilege pk.
	 *
	 * @return the id exception user privilege pk
	 */
	public Integer getIdExceptionUserPrivilegePk() {
		return this.idExceptionUserPrivilegePk;
	}

	/**
	 * Sets the id exception user privilege pk.
	 *
	 * @param idExceptionUserPrivilegePk the new id exception user privilege pk
	 */
	public void setIdExceptionUserPrivilegePk(Integer idExceptionUserPrivilegePk) {
		this.idExceptionUserPrivilegePk = idExceptionUserPrivilegePk;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return this.initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the user account.
	 *
	 * @return the user account
	 */
	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	/**
	 * Sets the user account.
	 *
	 * @param userAccount the new user account
	 */
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	/**
	 * Gets the user privilege details.
	 *
	 * @return the user privilege details
	 */
	public List<UserPrivilegeDetail> getUserPrivilegeDetails() {
		return this.userPrivilegeDetails;
	}

	/**
	 * Sets the user privilege details.
	 *
	 * @param userPrivilegeDetails the new user privilege details
	 */
	public void setUserPrivilegeDetails(List<UserPrivilegeDetail> userPrivilegeDetails) {
		this.userPrivilegeDetails = userPrivilegeDetails;
	}

	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the new system
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}
	
	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	/**
	 * Sets the block other motive.
	 *
	 * @param blockOtherMotive the blockOtherMotive to set
	 */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	/**
	 * Sets the unblock other motive.
	 *
	 * @param unblockOtherMotive the unblockOtherMotive to set
	 */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("userPrivilegeDetails", userPrivilegeDetails);
        return detailsMap;
	}

	/**
	 * Gets the document file.
	 *
	 * @return the document file
	 */
	public byte[] getDocumentFile() {
		return documentFile;
	}

	/**
	 * Sets the document file.
	 *
	 * @param documentFile the new document file
	 */
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	/**
	 * Gets the name file.
	 *
	 * @return the name file
	 */
	public String getNameFile() {
		return nameFile;
	}

	/**
	 * Sets the name file.
	 *
	 * @param nameFile the new name file
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}