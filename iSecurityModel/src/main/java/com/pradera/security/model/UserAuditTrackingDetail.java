package com.pradera.security.model;

import java.io.Serializable;
import javax.persistence.*;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the USER_AUDIT_TRACKING_DETAILS database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@Entity
@Table(name="USER_AUDIT_TRACKING_DETAILS")
public class UserAuditTrackingDetail implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id user audit detials pk. */
	@Id
	@SequenceGenerator(name="USER_AUDIT_TRACKING_DETAILS_IDUSERAUDITDETIALSPK_GENERATOR", sequenceName="SQ_ID_USER_AUDIT_DETIALS_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_AUDIT_TRACKING_DETAILS_IDUSERAUDITDETIALSPK_GENERATOR")
	@Column(name="ID_USER_AUDIT_DETIALS_PK")
	private Integer idUserAuditDetialsPk;

	/** The parameter name. */
	@Column(name="PARAMETER_NAME")
	private String parameterName;

	/** The parameter value. */
	@Column(name="PARAMETER_VALUE")
	private String parameterValue;

	//bi-directional many-to-one association to UserAuditTracking
    /** The user audit tracking. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_USER_AUDIT_FK")
	private UserAuditTracking userAuditTracking;

    /**
     * Instantiates a new user audit tracking detail.
     */
    public UserAuditTrackingDetail() {
    }

	/**
	 * Gets the id user audit detials pk.
	 *
	 * @return the id user audit detials pk
	 */
	public Integer getIdUserAuditDetialsPk() {
		return this.idUserAuditDetialsPk;
	}

	/**
	 * Sets the id user audit detials pk.
	 *
	 * @param idUserAuditDetialsPk the new id user audit detials pk
	 */
	public void setIdUserAuditDetialsPk(Integer idUserAuditDetialsPk) {
		this.idUserAuditDetialsPk = idUserAuditDetialsPk;
	}

	/**
	 * Gets the parameter name.
	 *
	 * @return the parameter name
	 */
	public String getParameterName() {
		return this.parameterName;
	}

	/**
	 * Sets the parameter name.
	 *
	 * @param parameterName the new parameter name
	 */
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	/**
	 * Gets the parameter value.
	 *
	 * @return the parameter value
	 */
	public String getParameterValue() {
		return this.parameterValue;
	}

	/**
	 * Sets the parameter value.
	 *
	 * @param parameterValue the new parameter value
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}

	/**
	 * Gets the user audit tracking.
	 *
	 * @return the user audit tracking
	 */
	public UserAuditTracking getUserAuditTracking() {
		return this.userAuditTracking;
	}

	/**
	 * Sets the user audit tracking.
	 *
	 * @param userAuditTracking the new user audit tracking
	 */
	public void setUserAuditTracking(UserAuditTracking userAuditTracking) {
		this.userAuditTracking = userAuditTracking;
	}
	
}