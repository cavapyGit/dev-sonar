package com.pradera.security.model.type;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum MotiveType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum OtherMotiveType {
    
    /** The sys block other motive. */
    SYS_BLOCK_OTHER_MOTIVE(Integer.valueOf(55),"SYS BLOCK OTHER MOTIVE"),
    
    /** The sys unblock other motive. */
    SYS_UNBLOCK_OTHER_MOTIVE(Integer.valueOf(59),"SYS UNBLOCK OTHER MOTIVE"),
    
    /** The sys opt block other motive. */
    SYS_OPT_BLOCK_OTHER_MOTIVE(Integer.valueOf(63),"SYS OPT BLOCK OTHER MOTIVE"),
    
    /** The sys opt unblock other motive. */
    SYS_OPT_UNBLOCK_OTHER_MOTIVE(Integer.valueOf(67),"SYS OPT UNBLOCK OTHER MOTIVE"),
    
    /** The sys profile block other motive. */
    SYS_PROFILE_BLOCK_OTHER_MOTIVE(Integer.valueOf(71),"SYS PROFLIE BLOCK OTHER MOTIVE"),
    
    /** The sys profile unblock other motive. */
    SYS_PROFILE_UNBLOCK_OTHER_MOTIVE(Integer.valueOf(75),"SYS PROFILE UNBLOCK OTHER MOTIVE"),
    
    /** The sys schedule block other motive. */
    SYS_SCHEDULE_BLOCK_OTHER_MOTIVE(Integer.valueOf(80),"SYS SCHEDULE BLOCK OTHER MOTIVE"),
    
    /** The sys schedule unblock other motive. */
    SYS_SCHEDULE_UNBLOCK_OTHER_MOTIVE(Integer.valueOf(83),"SYS SCHEDULE UNBLOCK OTHER MOTIVE"),
    
    SYS_SCHEDULE_REJECT_OTHER_MOTIVE(Integer.valueOf(153),"SYS SCHEDULE REJECT OTHER MOTIVE"),
    
    /** The schedule exception delete other motive. */
    SCHEDULE_EXCEPTION_DELETE_OTHER_MOTIVE(Integer.valueOf(87),"SCHEDULE EXCEPTION DELETE OTHER MOTIVE"),
    
    /** The holiday delete other motive. */
    HOLIDAY_DELETE_OTHER_MOTIVE(Integer.valueOf(91),"HOLIDAY DELETE OTHER MOTIVE"),
    
    /** The user exception block other motive. */
    USER_EXCEPTION_BLOCK_OTHER_MOTIVE(Integer.valueOf(95),"USER EXCEPTION BLOCK OTHER MOTIVE"),
    
    /** The user exception unblock other motive. */
    USER_EXCEPTION_UNBLOCK_OTHER_MOTIVE(Integer.valueOf(99),"USER EXCEPTION UNBLOCK OTHER MOTIVE"),
    
    /** The user exception delete other motive. */
    USER_EXCEPTION_DELETE_OTHER_MOTIVE(Integer.valueOf(170),"USER EXCEPTION DELETE OTHER MOTIVE"),
    
    /** The user exception reject other motive. */
    USER_EXCEPTION_REJECT_OTHER_MOTIVE(Integer.valueOf(118),"USER EXCEPTION REJECTED OTHER MOTIVE"),
    
    /** The sys opt reject other motive. */
    SYS_OPT_REJECT_OTHER_MOTIVE(Integer.valueOf(101),"SYS OPT REJECT OTHER MOTIVE"),
    
    /** The sys opt delete other motive. */
    SYS_OPT_DELETE_OTHER_MOTIVE(Integer.valueOf(104),"SYS OPT DELETE OTHER MOTIVE"),
    
    /** The user reject other motive. */
    USER_REJECT_OTHER_MOTIVE(Integer.valueOf(111),"USER REJECT OTHER MOTIVE"),
    
    /** The user block other motive. */
    USER_BLOCK_OTHER_MOTIVE(Integer.valueOf(112),"USER BLOCK OTHER MOTIVE"),
    
    /** The user unblock other motive. */
    USER_UNBLOCK_OTHER_MOTIVE(Integer.valueOf(113),"USER UNBLOCK OTHER MOTIVE"),
    
    /** The sys reject register other motive. */
    SYS_REJECT_REGISTER_OTHER_MOTIVE(Integer.valueOf(140),"SYS REJECT REGISTER OTHER MOTIVE"),
    
    /** The sys reject block other motive. */
    SYS_REJECT_BLOCK_OTHER_MOTIVE(Integer.valueOf(144),"SYS REJECT BLOCK OTHER MOTIVE"),
    
    /** The sys profile reject other motive. */
    SYS_PROFILE_REJECT_OTHER_MOTIVE(Integer.valueOf(151),"SYS PROFILE REJECT REGISTRATION OTHER MOTIVE"),
    
    /** The schedule exception reject other motive. */
    SCHEDULE_EXCEPTION_REJECT_OTHER_MOTIVE(Integer.valueOf(155),"SCHEDULE EXCEPTION REJECT OTHER MOTIVE"),
    
    /** The user annular other motive. */
    USER_ANNULAR_OTHER_MOTIVE(Integer.valueOf(157),"USER ANNULAR OTHER MOTIVE");
    
    /** The code. */
    private Integer code;
	
	/** The value. */
	private String value;
	
	/** The Constant list. */
	public static final List<OtherMotiveType> list = new ArrayList<OtherMotiveType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, OtherMotiveType> lookup = new HashMap<Integer, OtherMotiveType>();
	
	static {
		for (OtherMotiveType s : EnumSet.allOf(OtherMotiveType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}
	
	/**
	 * Instantiates a new other motive type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private OtherMotiveType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the other motive type
	 */
	public static OtherMotiveType get(Integer code) {
		return lookup.get(code);
	}
}
