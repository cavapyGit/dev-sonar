package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum InstanceType.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum InstanceType {
	
	/** The system. */
	SYSTEM(Integer.valueOf(1),"SYSTEM"),
	
	/** The option. */
	OPTION(Integer.valueOf(2),"OPTION");
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;

	/** The Constant list. */
	public static final List<InstanceType> list = new ArrayList<InstanceType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, InstanceType> lookup = new HashMap<Integer, InstanceType>();

	static {
		for (InstanceType s : EnumSet.allOf(InstanceType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new instance type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private InstanceType(Integer code, String value) {
		this.code = code;
		this.value = value;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the instance type
	 */
	public static InstanceType get(Integer code) {
		return lookup.get(code);
	}
}

