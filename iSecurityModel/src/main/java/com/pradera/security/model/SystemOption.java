package com.pradera.security.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The persistent class for the SYSTEM_OPTION database table.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
@NamedQueries({
	@NamedQuery(name = SystemOption.SYSTEMOPTION_SEARCH_BY_ID, query = "Select o From SystemOption o inner join fetch o.system WHERE o.idSystemOptionPk = :idOptionPrm ",hints = { @QueryHint(name = "org.hibernate.cacheable", value ="true") })
})
@Entity
@Table(name="SYSTEM_OPTION")
@Cacheable(true)
public class SystemOption implements Serializable,Auditable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant SYSTEMOPTION_SEARCH_BY_ID. */
	public static final String SYSTEMOPTION_SEARCH_BY_ID = "SystemOption.searchOptionById";
	
	/** The id system option pk. */
	@Id
	@SequenceGenerator(name="SYSTEM_OPTION_IDSYSTEMOPTIONPK_GENERATOR", sequenceName="SQ_ID_SYSTEM_OPTION_PK",initialValue=1,allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SYSTEM_OPTION_IDSYSTEMOPTIONPK_GENERATOR")
	@Column(name="ID_SYSTEM_OPTION_PK")
	private Integer idSystemOptionPk;

    /** The last modify date. */
    @Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_MODIFY_DATE")
	private Date lastModifyDate;

	/** The last modify ip. */
	@Column(name="LAST_MODIFY_IP")
	private String lastModifyIp;

	/** The last modify priv. */
	@Column(name="LAST_MODIFY_PRIV")
	private Integer lastModifyPriv;

	/** The last modify user. */
	@Column(name="LAST_MODIFY_USER")
	private String lastModifyUser;

	/** The module. */
	@Column(name="\"MODULE\"")
	private Integer module;

	/** The option type. */
	@Column(name="OPTION_TYPE")
	private Integer optionType;

	/** The order option. */
	@Column(name="ORDER_OPTION")
	private Integer orderOption;

	/** The registry date. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="REGISTRY_DATE")
	private Date registryDate;

	/** The registry user. */
	@Column(name="REGISTRY_USER")
	private String registryUser;

	/** The state. */
	@Column(name="\"STATE\"")
	private Integer state;
	
    /** The situation. */
    @Column(name="SITUATION")
	private Integer situation;

	/** The url option. */
	@Column(name="URL_OPTION")
	private String urlOption;
	
	/**  The blockMotive. */
	@Column(name="BLOCK_MOTIVE")
	
	private Integer blockMotive;
	
	/**  The unblockMotive. */
	@Column(name="UNBLOCK_MOTIVE")
	private Integer unblockMotive;
	
	/**  The removeMotive. */
	@Column(name="REMOVE_MOTIVE")
	private Integer removeMotive;
	
	/**  The remove other Motive. */
	@Column(name="REMOVE_OTHER_MOTIVE")
	private String removeOtherMotive;
	
	/**  The rejectMotive. */
	@Column(name="REJECT_MOTIVE")
	private Integer rejectMotive;
	
	/**  The reject other Motive. */
	@Column(name="REJECT_OTHER_MOTIVE")
	private String rejectOtherMotive;

	/**  The block other Motive. */
	@Column(name="BLOCK_OTHER_MOTIVE")
	private String blockOtherMotive;
	
	/**  The unblock other Motive. */
	@Column(name="UNBLOCK_OTHER_MOTIVE")
	private String unblockOtherMotive;
	
	//bi-directional many-to-one association to CatalogueLanguage
	/** The catalogue languages. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="systemOption",fetch=FetchType.LAZY)
	private List<CatalogueLanguage> catalogueLanguages;

	//bi-directional many-to-one association to OptionPrivilege
	/** The option privileges. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="systemOption")
	private List<OptionPrivilege> optionPrivileges;

	//bi-directional many-to-one association to System
    /** The system. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_SYSTEM_FK")
	private System system;

	//bi-directional many-to-one association to SystemOption
    /** The parent system option. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OPTION_FATHER",referencedColumnName="ID_SYSTEM_OPTION_PK")
	private SystemOption parentSystemOption;

	//bi-directional many-to-one association to SystemOption
	/** The system options. */
	@OneToMany(cascade=CascadeType.ALL,mappedBy="parentSystemOption")
	private List<SystemOption> systemOptions;
	

	@Column(name="DOCUMENT_FILE_NAME")
	private String documentFileName;
	
	@Column(name="DOCUMENT_FILE_PATH")
	private String documentFilePath;
	
	@Lob()
	@Transient
	private byte[] documentFile;
	
    /**
     * Instantiates a new system option.
     */
    public SystemOption() {
    }

	/**
	 * Gets the id system option pk.
	 *
	 * @return the id system option pk
	 */
	public Integer getIdSystemOptionPk() {
		return this.idSystemOptionPk;
	}

	/**
	 * Sets the id system option pk.
	 *
	 * @param idSystemOptionPk the new id system option pk
	 */
	public void setIdSystemOptionPk(Integer idSystemOptionPk) {
		this.idSystemOptionPk = idSystemOptionPk;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify priv.
	 *
	 * @return the last modify priv
	 */
	public Integer getLastModifyPriv() {
		return this.lastModifyPriv;
	}

	/**
	 * Sets the last modify priv.
	 *
	 * @param lastModifyPriv the new last modify priv
	 */
	public void setLastModifyPriv(Integer lastModifyPriv) {
		this.lastModifyPriv = lastModifyPriv;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the module.
	 *
	 * @return the module
	 */
	public Integer getModule() {
		return this.module;
	}

	/**
	 * Sets the module.
	 *
	 * @param module the new module
	 */
	public void setModule(Integer module) {
		this.module = module;
	}

	/**
	 * Gets the option type.
	 *
	 * @return the option type
	 */
	public Integer getOptionType() {
		return this.optionType;
	}

	/**
	 * Sets the option type.
	 *
	 * @param optionType the new option type
	 */
	public void setOptionType(Integer optionType) {
		this.optionType = optionType;
	}

	/**
	 * Gets the order option.
	 *
	 * @return the order option
	 */
	public Integer getOrderOption() {
		return this.orderOption;
	}

	/**
	 * Sets the order option.
	 *
	 * @param orderOption the new order option
	 */
	public void setOrderOption(Integer orderOption) {
		this.orderOption = orderOption;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the url option.
	 *
	 * @return the url option
	 */
	public String getUrlOption() {
		return this.urlOption;
	}

	/**
	 * Sets the url option.
	 *
	 * @param urlOption the new url option
	 */
	public void setUrlOption(String urlOption) {
		this.urlOption = urlOption;
	}

	
	
	/**
	 * Gets the catalogue languages.
	 *
	 * @return the catalogue languages
	 */
	public List<CatalogueLanguage> getCatalogueLanguages() {
		return catalogueLanguages;
	}

	/**
	 * Sets the catalogue languages.
	 *
	 * @param catalogueLanguages the new catalogue languages
	 */
	public void setCatalogueLanguages(List<CatalogueLanguage> catalogueLanguages) {
		this.catalogueLanguages = catalogueLanguages;
	}

	/**
	 * Gets the option privileges.
	 *
	 * @return the option privileges
	 */
	public List<OptionPrivilege> getOptionPrivileges() {
		return this.optionPrivileges;
	}

	/**
	 * Sets the option privileges.
	 *
	 * @param optionPrivileges the new option privileges
	 */
	public void setOptionPrivileges(List<OptionPrivilege> optionPrivileges) {
		this.optionPrivileges = optionPrivileges;
	}
	
	/**
	 * Gets the system.
	 *
	 * @return the system
	 */
	public System getSystem() {
		return this.system;
	}

	/**
	 * Sets the system.
	 *
	 * @param system the new system
	 */
	public void setSystem(System system) {
		this.system = system;
	}

	/**
	 * Gets the parent system option.
	 *
	 * @return the parent system option
	 */
	public SystemOption getParentSystemOption() {
		return parentSystemOption;
	}

	/**
	 * Sets the parent system option.
	 *
	 * @param parentSystemOption the new parent system option
	 */
	public void setParentSystemOption(SystemOption parentSystemOption) {
		this.parentSystemOption = parentSystemOption;
	}

	/**
	 * Gets the system options.
	 *
	 * @return the system options
	 */
	public List<SystemOption> getSystemOptions() {
		return this.systemOptions;
	}

	/**
	 * Sets the system options.
	 *
	 * @param systemOptions the new system options
	 */
	public void setSystemOptions(List<SystemOption> systemOptions) {
		this.systemOptions = systemOptions;
	}

	/**
	 * Gets the block motive.
	 *
	 * @return the blockMotive
	 */
	public Integer getBlockMotive() {
		return blockMotive;
	}

	/**
	 * Sets the block motive.
	 *
	 * @param blockMotive the blockMotive to set
	 */
	public void setBlockMotive(Integer blockMotive) {
		this.blockMotive = blockMotive;
	}

	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblockMotive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the unblockMotive to set
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}

	/**
	 * Gets the removes the motive.
	 *
	 * @return the removeMotive
	 */
	public Integer getRemoveMotive() {
		return removeMotive;
	}

	/**
	 * Sets the removes the motive.
	 *
	 * @param removeMotive the removeMotive to set
	 */
	public void setRemoveMotive(Integer removeMotive) {
		this.removeMotive = removeMotive;
	}
	
	/**
	 * Gets the block other motive.
	 *
	 * @return the blockOtherMotive
	 */
	public String getBlockOtherMotive() {
		return blockOtherMotive;
	}

	/**
	 * Sets the block other motive.
	 *
	 * @param blockOtherMotive the blockOtherMotive to set
	 */
	public void setBlockOtherMotive(String blockOtherMotive) {
		this.blockOtherMotive = blockOtherMotive;
	}

	/**
	 * Gets the unblock other motive.
	 *
	 * @return the unblockOtherMotive
	 */
	public String getUnblockOtherMotive() {
		return unblockOtherMotive;
	}

	/**
	 * Sets the unblock other motive.
	 *
	 * @param unblockOtherMotive the unblockOtherMotive to set
	 */
	public void setUnblockOtherMotive(String unblockOtherMotive) {
		this.unblockOtherMotive = unblockOtherMotive;
	}
	
	/**
	 * Gets the removes the other motive.
	 *
	 * @return the removeOtherMotive
	 */
	public String getRemoveOtherMotive() {
		return removeOtherMotive;
	}

	/**
	 * Sets the removes the other motive.
	 *
	 * @param removeOtherMotive the removeOtherMotive to set
	 */
	public void setRemoveOtherMotive(String removeOtherMotive) {
		this.removeOtherMotive = removeOtherMotive;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the rejectMotive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the rejectMotive to set
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject other motive.
	 *
	 * @return the rejectOtherMotive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}

	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the rejectOtherMotive to set
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}
	
	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#setAudit(com.pradera.commons.contextholder.LoggerUser)
	 */
	@Override
    public void setAudit(LoggerUser loggerUser) {
        // TODO review casuisticas no contempladas para mejorar
        if (loggerUser != null) {
        	lastModifyPriv = loggerUser.getIdPrivilegeOfSystem();
            lastModifyDate = loggerUser.getAuditTime();
            lastModifyIp = loggerUser.getIpAddress();
            lastModifyUser = loggerUser.getUserName();
        }
    }
	/* (non-Javadoc)
	 * @see com.pradera.commons.audit.Auditable#getListForAudit()
	 */
	@Override
	public Map<String, List<? extends Auditable>> getListForAudit() {
		HashMap<String,List<? extends Auditable>> detailsMap = 
                new HashMap<String, List<? extends Auditable>>();  
		detailsMap.put("catalogueLanguages", catalogueLanguages);
		detailsMap.put("optionPrivileges", optionPrivileges);
		detailsMap.put("systemOptions", systemOptions);
        return detailsMap;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	public String getDocumentFilePath() {
		return documentFilePath;
	}

	public void setDocumentFilePath(String documentFilePath) {
		this.documentFilePath = documentFilePath;
	}
}