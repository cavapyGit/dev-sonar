package com.pradera.security.model.type;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  class UserReportResultBean bean.
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public class UserReportResultBean {

	/** The full name. */
	private String fullName;
	
	/** The id email. */
	private String idEmail;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The last modify ip. */
	private String lastModifyIp;
	
	/** The last modify user. */
	private String lastModifyUser;
	
	/** The type of client. */
	private String typeOfClient;
	
	/** The state. */
	private String state;
	
	/**
	 * Gets the full name.
	 *
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * Sets the full name.
	 *
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	/**
	 * Gets the id email.
	 *
	 * @return the idEmail
	 */
	public String getIdEmail() {
		return idEmail;
	}
	
	/**
	 * Sets the id email.
	 *
	 * @param idEmail the idEmail to set
	 */
	public void setIdEmail(String idEmail) {
		this.idEmail = idEmail;
	}
	
	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the type of client.
	 *
	 * @return the typeOfClient
	 */
	public String getTypeOfClient() {
		return typeOfClient;
	}
	
	/**
	 * Sets the type of client.
	 *
	 * @param typeOfClient the typeOfClient to set
	 */
	public void setTypeOfClient(String typeOfClient) {
		this.typeOfClient = typeOfClient;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
}
