package com.pradera.security.model.type;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import com.pradera.commons.utils.PropertiesUtilities;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The  enum UserExceptionStateType .
 *
 * @author : PraderaTechnologies.
 * @version 1.0
 * @Project : PraderaSecurity
 * @Creation_Date :
 */
public enum UserExceptionStateType {
	
	/** The deleted. */
	DELETED(Integer.valueOf(0),"ELIMINADO","remove.png"),
	
	/** The registered. */
	REGISTERED(Integer.valueOf(1),"REGISTRADO","registered.png"),
	
	/** The annulled. */
	ANNULLED(Integer.valueOf(2),"REVOCADO","blocked.png"),
	
	/** The confirmed. */
	CONFIRMED(Integer.valueOf(3),"CONFIRMADO","confirmed.png"),
	
	REJECT(Integer.valueOf(4),"RECHAZADO","confirmed.png"),;
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	
	/** The icon. */
	private String icon;

	/** The Constant list. */
	public static final List<UserExceptionStateType> list = new ArrayList<UserExceptionStateType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, UserExceptionStateType> lookup = new HashMap<Integer, UserExceptionStateType>();

	static {
		for (UserExceptionStateType s : EnumSet.allOf(UserExceptionStateType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new user exception state type.
	 *
	 * @param code the code
	 * @param value the value
	 * @param icon the icon
	 */
	private UserExceptionStateType(Integer code, String value, String icon) {
		this.code = code;
		this.value = value;
		this.icon = icon;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the user exception state type
	 */
	public static UserExceptionStateType get(Integer code) {
		return lookup.get(code);
	}

	/**
	 * Gets the icon.
	 *
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
}
