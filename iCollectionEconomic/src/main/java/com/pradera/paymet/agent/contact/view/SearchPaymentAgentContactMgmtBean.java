package com.pradera.paymet.agent.contact.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;

import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.collection.right.util.SituationConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;
import com.pradera.paymet.agent.facade.PaymetAgentFacade;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;
import com.edv.collection.economic.rigth.PaymentAgent;
import com.edv.collection.economic.rigth.ReqModifyPaymentAgent;

@DepositaryWebBean
@LoggerCreateBean
public class SearchPaymentAgentContactMgmtBean extends GenericBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PayerAgentRegistreTO searchPaymentAgent,viewPaymentAgent;
	/**TODO PARA MODICACION DE UN USURIO/CONTACTO AGENTE PAGADOR*/
	private PayerAgentRegistreTO agentModifiedTO;
	
	private List<ParameterTable>  listTypePayingAgent,listStatePayingAgent,listSituationPayingAgent,listSecurityClass;
	
	private List<PaymentAgent> listPaymentAgents;
	
	private GenericDataModel<PaymentAgent> listPaymentAgentContacts;
	
	private PaymentAgent paymentAgentSelected;
	
	@Inject
	private PaymetAgentFacade agentFacade;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private CollectionRigthFacade collectionRigthFacade; 
	
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private NotificationServiceFacade notificationServiceFacade; 
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	private boolean visibleResult;
	
	@PostConstruct
	public void init(){
		searchPaymentAgent = new PayerAgentRegistreTO();
		listTypePayingAgent = agentFacade.getListEconomicActivities();
		listSecurityClass = collectionRigthFacade.getSecurityClassListAll();
		searchPaymentAgent.setParameterTablePk(45);//Agente pagador BANCOS
		/**Cargamos los contactos agente pagador*/
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(searchPaymentAgent.getParameterTablePk());
		listStatePayingAgent = agentFacade.getListStatePayingAgent();
	}
	/**
	 * Cargamos el detalle del contacto agente pagador
	 * */
	public void loadAgentInfo(ActionEvent event){
		PaymentAgent paymentAgentClicked= new PaymentAgent();
		paymentAgentClicked = (PaymentAgent) event.getComponent().getAttributes().get("paymentAgentView");
		listSituationPayingAgent = agentFacade.getListSituationPayingAgent();
		viewPaymentAgent = new PayerAgentRegistreTO();
		viewPaymentAgent.setParameterTablePk(paymentAgentClicked.getTypePaymentAgent());
		/**Lista de agentes pagadores*/
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(paymentAgentClicked.getTypePaymentAgent());
		/**Agente pagador*/
		viewPaymentAgent.setIdPaymentAgentPk(paymentAgentClicked.getIdPaymentAgentFk());
		/**Datos del contacto agente pagador*/
		viewPaymentAgent.setName(paymentAgentClicked.getName());
		viewPaymentAgent.setFirstName(paymentAgentClicked.getFirstName());
		viewPaymentAgent.setLastName(paymentAgentClicked.getLastName());
		viewPaymentAgent.setCellPhone(paymentAgentClicked.getCellPhone());
		viewPaymentAgent.setPhone(paymentAgentClicked.getPhone());
		viewPaymentAgent.setInternPhone(paymentAgentClicked.getIntern());
		viewPaymentAgent.setEmail(paymentAgentClicked.getEmail());
		viewPaymentAgent.setJob(paymentAgentClicked.getJob());
		viewPaymentAgent.setState(paymentAgentClicked.getState());
		viewPaymentAgent.setSituation(paymentAgentClicked.getSituation());
		
		viewPaymentAgent.setPaymentWithLip(paymentAgentClicked.getIndLip().equals(BooleanType.YES.getCode())?true:false);
		
		viewPaymentAgent.setIndSendUrlPayment(paymentAgentClicked.getIndSendUrlPayment().equals(BooleanType.YES.getCode())?true:false);
		
		List<ParameterTable> listSecurityClass = new ArrayList<>();
		listSecurityClass =  collectionRigthFacade.getSecurityClassList(paymentAgentClicked.getSecurityClassList());
		viewPaymentAgent.setListSecurityClassSelected(listSecurityClass);
		viewPaymentAgent.setRenderedRejected(false);
		viewPaymentAgent.setRenderedConfirm(false);
		
		
		if(paymentAgentClicked.getEncryp()!=null)
			viewPaymentAgent.setEncryptMail(paymentAgentClicked.getEncryp().equals(BooleanType.YES.getCode()));
		else
			viewPaymentAgent.setEncryptMail(false);
		
		/**Carga de certificados*/
		if(paymentAgentClicked.getCertificateName()!=null){
			viewPaymentAgent.setCertificateName(paymentAgentClicked.getCertificateName());
			viewPaymentAgent.setCertificate(paymentAgentClicked.getCertificate());
		}
			
	}
	/**
	 * Evento de cuando se cambia el tipo de agente pagador
	 * */
	public void changeTypePaymentAgent(){
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(searchPaymentAgent.getParameterTablePk());
		JSFUtilities.updateComponent("idHolderPkFilter");
	}
	
	/**
	 * Buscamos contactos agente pagador
	 * 
	 * */
	public void searchFromFilters(){
		paymentAgentSelected = null;
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		if(listPaymentAgentContacts!=null&&listPaymentAgentContacts.getRowCount()>0)
			visibleResult=false;
		else
			visibleResult =true;
	}
	/**Limpiamos las pantalla*/
	public void clrearFilters(){
		searchPaymentAgent = new PayerAgentRegistreTO();
		searchPaymentAgent.setParameterTablePk(45);//Agente pagador BANCOS
		listPaymentAgentContacts = new GenericDataModel<>();
		visibleResult = false;
	}
	/**
	 * Usado en la pantalla de busqueda
	 * Verifica los estados de un usuario/contacto para ser modificados*/
	public String beforeModify(){
		if(isSelectedPaymentAgent()){
			if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
					paymentAgentSelected.getSituation().equals(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT)){
				
				/*Variable para modificar el contacto agente pagador*/
				agentModifiedTO = new PayerAgentRegistreTO();
				listSituationPayingAgent = agentFacade.getListSituationPayingAgent();
				agentModifiedTO.setParameterTablePk(paymentAgentSelected.getTypePaymentAgent());
				
				/*Lista de agentes pagadores*/
				listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(paymentAgentSelected.getTypePaymentAgent());
				/**Agente pagador*/
				agentModifiedTO.setIdPaymentAgentPk(paymentAgentSelected.getIdPaymentAgentFk());
				
				agentModifiedTO.setName(paymentAgentSelected.getName());
				agentModifiedTO.setFirstName(paymentAgentSelected.getFirstName());
				agentModifiedTO.setLastName(paymentAgentSelected.getLastName());
				agentModifiedTO.setCellPhone(paymentAgentSelected.getCellPhone());
				agentModifiedTO.setPhone(paymentAgentSelected.getPhone());
				agentModifiedTO.setInternPhone(paymentAgentSelected.getIntern());
				agentModifiedTO.setEmail(paymentAgentSelected.getEmail());
				agentModifiedTO.setJob(paymentAgentSelected.getJob());
				agentModifiedTO.setState(paymentAgentSelected.getState());
				agentModifiedTO.setSituation(paymentAgentSelected.getSituation());
				
				agentModifiedTO.setPaymentWithLip(paymentAgentSelected.getIndLip().equals(BooleanType.YES.getCode())?true:false);
				
				agentModifiedTO.setIndSendUrlPayment(paymentAgentSelected.getIndSendUrlPayment().equals(BooleanType.YES.getCode())?true:false);
				
				/*Cifrar y adjuntar archivo*/
				if(paymentAgentSelected.getEncryp()==null)
					paymentAgentSelected.setEncryp(BooleanType.NO.getCode());
				
				agentModifiedTO.setEncryptMail(paymentAgentSelected.getEncryp().equals(BooleanType.YES.getCode())?true:false);
				
				if(paymentAgentSelected.getCertificateName()!=null&&paymentAgentSelected.getCertificate()!=null){
					agentModifiedTO.setCertificateName(paymentAgentSelected.getCertificateName());
					agentModifiedTO.setCertificate(paymentAgentSelected.getCertificate());
				}		
				
				List<ParameterTable> listSecurityClass = new ArrayList<>();
				listSecurityClass =  collectionRigthFacade.getSecurityClassList(paymentAgentSelected.getSecurityClassList());
				agentModifiedTO.setListSecurityClassSelected(listSecurityClass);
				return "modifyPagePaymentAgentContact";
			}else{
				/*Mostrar el detalle de la modificacion y confirmar la modificacion*/
				if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
						paymentAgentSelected.getSituation().equals(SituationConstants.PENDING_TO_MODIFY_CONTACT_PAYMET)){
					ReqModifyPaymentAgent agentModify = null;
					try {
						agentModify = agentFacade.searchLastModificationContactPayment(paymentAgentSelected.getIdPaymentAgentPk());
					} catch (Exception e) {
						return null;
					}
					viewPaymentAgent = new PayerAgentRegistreTO();
					viewPaymentAgent.setParameterTablePk(agentModify.getTypePaymentAgent());
					//viewPaymentAgent.setIdHolderPk(agentModify.getIdHolderFk());
					viewPaymentAgent.setName(agentModify.getName());
					if(!paymentAgentSelected.getName().equalsIgnoreCase(viewPaymentAgent.getName()))
						viewPaymentAgent.setRenderedNameAsterisk(true);
					viewPaymentAgent.setFirstName(agentModify.getFirstName());
					if(!paymentAgentSelected.getFirstName().equalsIgnoreCase(viewPaymentAgent.getFirstName()))
						viewPaymentAgent.setRenderedFirstNameAsterisk(true);
					
					
					viewPaymentAgent.setLastName(agentModify.getLastName()==null?"":agentModify.getLastName());
					paymentAgentSelected.setLastName(paymentAgentSelected.getLastName()==null?"":paymentAgentSelected.getLastName());
					if(!paymentAgentSelected.getLastName().equalsIgnoreCase(viewPaymentAgent.getLastName()))
						viewPaymentAgent.setRenderedLastNameAsterisk(true);
					
					viewPaymentAgent.setCellPhone(agentModify.getCellPhone()==null?"":agentModify.getCellPhone());
					paymentAgentSelected.setCellPhone(paymentAgentSelected.getCellPhone()==null?"":paymentAgentSelected.getCellPhone());
					if(!paymentAgentSelected.getCellPhone().equalsIgnoreCase(viewPaymentAgent.getCellPhone()))
						viewPaymentAgent.setRenderedCellPhoneAsterisk(true);
					
					viewPaymentAgent.setPhone(agentModify.getPhone()==null?"":agentModify.getPhone());
					paymentAgentSelected.setPhone(paymentAgentSelected.getPhone()==null?"":paymentAgentSelected.getPhone());
					if(!paymentAgentSelected.getPhone().equalsIgnoreCase(viewPaymentAgent.getPhone()))
						viewPaymentAgent.setRenderedPhoneAsterisk(true);
					
					viewPaymentAgent.setInternPhone(agentModify.getIntern()==null?"":agentModify.getIntern());
					paymentAgentSelected.setIntern(paymentAgentSelected.getIntern()==null?"":paymentAgentSelected.getIntern());
					if(!paymentAgentSelected.getIntern().equalsIgnoreCase(viewPaymentAgent.getInternPhone()))
						viewPaymentAgent.setRenderedInternPhoneAsterisk(true);
					
					viewPaymentAgent.setEmail(agentModify.getEmail());
					if(paymentAgentSelected.getEmail()!=null)
					if(!paymentAgentSelected.getEmail().equalsIgnoreCase(viewPaymentAgent.getEmail()))
						viewPaymentAgent.setRenderedEmailAsterisk(true);
					
					viewPaymentAgent.setPaymentWithLip(agentModify.getIndLip().equals(BooleanType.YES.getCode())?true:false);
					//evaluando si se coloca (*)de modificacion en el campo [IndLip: indicador si paga con LIP]
					if (paymentAgentSelected.getIndLip().equals(BooleanType.YES.getCode()))
						if (!viewPaymentAgent.isPaymentWithLip())
							viewPaymentAgent.setRenderedLipAsterisk(true);
						else
							viewPaymentAgent.setRenderedLipAsterisk(false);
					else if (viewPaymentAgent.isPaymentWithLip())
						viewPaymentAgent.setRenderedLipAsterisk(true);
					else
						viewPaymentAgent.setRenderedLipAsterisk(false);
					
					
					/**Indicador de envio de URL*/
					if(agentModify.getIndSendUrlPayment()==null)
						agentModify.setIndSendUrlPayment(BooleanType.NO.getCode());
					viewPaymentAgent.setIndSendUrlPayment(agentModify.getIndSendUrlPayment().equals(BooleanType.YES.getCode())?true:false);
					//evaluando si se coloca (*)de modificacion en el campo [IndLip: indicador si paga con LIP]
					if (paymentAgentSelected.getIndSendUrlPayment().equals(BooleanType.YES.getCode()))
						if (!viewPaymentAgent.isIndSendUrlPayment())
							viewPaymentAgent.setRenderedIndUrl(true);
						else
							viewPaymentAgent.setRenderedIndUrl(false);
					else if (viewPaymentAgent.isIndSendUrlPayment())
						viewPaymentAgent.setRenderedIndUrl(true);
					else
						viewPaymentAgent.setRenderedIndUrl(false);
						
					
					viewPaymentAgent.setJob(agentModify.getJob()==null?"":agentModify.getJob());
					paymentAgentSelected.setJob(paymentAgentSelected.getJob()==null?"":paymentAgentSelected.getJob());
					if(!paymentAgentSelected.getJob().equalsIgnoreCase(viewPaymentAgent.getJob()))
						viewPaymentAgent.setRenderedJobAsterisk(true);
					viewPaymentAgent.setState(paymentAgentSelected.getState());
					viewPaymentAgent.setSituation(paymentAgentSelected.getSituation());
					
					if(!agentModify.getEncryp().equals(viewPaymentAgent.isEncryptMail()==true?1:0))
						viewPaymentAgent.setEncryptMailAsterisk(true);
					
					viewPaymentAgent.setEncryptMail(agentModify.getEncryp().equals(BooleanType.YES.getCode())?true:false);
					
					viewPaymentAgent.setCertificate(agentModify.getCertificate());
					viewPaymentAgent.setCertificateName(agentModify.getCertificateName());
					
					List<ParameterTable> listOrigen = collectionRigthFacade.getSecurityClassList(paymentAgentSelected.getSecurityClassList());
					List<ParameterTable> listModf = collectionRigthFacade.getSecurityClassList(agentModify.getSecurityClassList());
					
					viewPaymentAgent.setListSecurityClassSelected(listModf);
					
					
					
					for (ParameterTable parameterTable1 : listModf) 
						for (ParameterTable parameterTable2 : listOrigen) 
							if (!parameterTable1.getParameterTablePk().equals(parameterTable2.getParameterTablePk())) {
								viewPaymentAgent.setRenderedSecurityClassAsterisk(false);
								break;
							}
					viewPaymentAgent.setRenderedRejected(false);
					viewPaymentAgent.setRenderedConfirm(false);
					viewPaymentAgent.setRenderedConfirmModfy(true);
					viewPaymentAgent.setRenderedRejectModify(true);
					
					return "paymentAgentContactLoadInfo";
				}else{
					String msg = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NOTIFY_CONTACT_NO_ENABLE);
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return "";
	}
	public String beforeReject(){
		if(isSelectedPaymentAgent()){
			if(isRegisteredState()){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(true);
				viewPaymentAgent.setRenderedBlock(false);
				viewPaymentAgent.setRenderedUnBlock(false);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(false);
				return "paymentAgentContactLoadInfo";
			}
		}
		return "";
	}  
	public String beforeConfirm(){
		if(isSelectedPaymentAgent()){
			if(isRegisteredState()){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(false);
				viewPaymentAgent.setRenderedBlock(false);
				viewPaymentAgent.setRenderedUnBlock(false);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(true);
				return "paymentAgentContactLoadInfo";
			}
		}
		return "";
	} 
	/**
	 * Carga el detalle del contacto agente pagador
	 * */
	private void loadDetailView(){
		listSituationPayingAgent = agentFacade.getListSituationPayingAgent();
		viewPaymentAgent = new PayerAgentRegistreTO();
		viewPaymentAgent.setParameterTablePk(paymentAgentSelected.getTypePaymentAgent());
		
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(paymentAgentSelected.getTypePaymentAgent());
		/**Agente pagador*/
		viewPaymentAgent.setIdPaymentAgentPk(paymentAgentSelected.getIdPaymentAgentFk());
		
		viewPaymentAgent.setName(paymentAgentSelected.getName());
		viewPaymentAgent.setFirstName(paymentAgentSelected.getFirstName());
		viewPaymentAgent.setLastName(paymentAgentSelected.getLastName());
		viewPaymentAgent.setCellPhone(paymentAgentSelected.getCellPhone());
		viewPaymentAgent.setPhone(paymentAgentSelected.getPhone());
		viewPaymentAgent.setEmail(paymentAgentSelected.getEmail());
		viewPaymentAgent.setInternPhone(paymentAgentSelected.getIntern());
		viewPaymentAgent.setJob(paymentAgentSelected.getJob());
		viewPaymentAgent.setState(paymentAgentSelected.getState());
		viewPaymentAgent.setSituation(paymentAgentSelected.getSituation());
		
		/*Cifrar y adjuntar archivo*/
		if(paymentAgentSelected.getEncryp()==null)
			paymentAgentSelected.setEncryp(BooleanType.NO.getCode());
		viewPaymentAgent.setEncryptMail(paymentAgentSelected.getEncryp().equals(BooleanType.YES.getCode())?true:false);
		
		if(paymentAgentSelected.getCertificateName()!=null&&paymentAgentSelected.getCertificate()!=null){
			viewPaymentAgent.setCertificateName(paymentAgentSelected.getCertificateName());
			viewPaymentAgent.setCertificate(paymentAgentSelected.getCertificate());
		}	
		
		viewPaymentAgent.setIndSendUrlPayment(paymentAgentSelected.getIndSendUrlPayment().equals(BooleanType.YES.getCode())?true:false);
		
		viewPaymentAgent.setPaymentWithLip(paymentAgentSelected.getIndLip().equals(BooleanType.YES.getCode())?true:false);
		
		List<ParameterTable> listSecurityClass = new ArrayList<>();
		listSecurityClass =  collectionRigthFacade.getSecurityClassList(paymentAgentSelected.getSecurityClassList());
		viewPaymentAgent.setListSecurityClassSelected(listSecurityClass);
	}
	/** Usado en la pantalla de detalle
	 *  Dialogo de registro de bloqueo Dialogo de confirmacion de bloqueo
	 * */
	public void blockPayerAgentDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BLOCK_PAYER_AGENT,paymentAgentSelected.getPaymentAgentFullName());
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgBlockState').show();");
	}
	/** Usado en la pantalla de detalle
	 *  Dialogo para registrar el desbloqueo
	 * 
	 * */
	public void unBlockPayerAgentDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_UNLOOK_PAYER_AGENT,paymentAgentSelected.getPaymentAgentFullName());
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgUnBlockState').show();");
	}
	/**
	 * Usado en la pantalla de detalle
	 * Se registra el debloqueo y retornando a la pantalla de busqueda
	 * @return ""
	 * */
	public void unBlockPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.BLOCKED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.UNLOCKING_PENDING_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("label.registre.un.block.payment.contact",paymentAgentSelected.getPaymentAgentFullName());
		JSFUtilities.putViewMap(bodyMessage, headerMessage);
		JSFUtilities.updateComponent("opnlDialogs"); 
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogUnBlockYes').show();");
	}
	
	/** Usado en la busqueda 
	 * 	Se confirma el bloqueo del usuario/contacto agente pagador
	 * 
	 **/
	public void blockPayerAgentDialogConfirm(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.BLOCKED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		JSFUtilities.updateComponent("frmSearchPaymentAgent");
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("alert.correct.block.paymet.agent",paymentAgentSelected.getPaymentAgentFullName());
		JSFUtilities.putViewMap(bodyMessage, headerMessage);
		JSFUtilities.updateComponent("opnlDialogs"); 
    	JSFUtilities.executeJavascriptFunction("PF('dlgAlertMessagePaymentAgent').show();");
    	
    	/**Notificacion de bloqueo de contacto agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_BLOCK_UNBLOCK_CONTACT_AGENT_PAYMENT.getCode());
		
		String nameContact = paymentAgentSelected.getName()+" "+ paymentAgentSelected.getFirstName()+" "+paymentAgentSelected.getLastName()==null?"":paymentAgentSelected.getLastName();
		String agentPayment = paymentAgentSelected.getPaymentAgentFullName();
		Object[] parameters = {"Bloqueo",nameContact,agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	
	/**
	 * Metodo usado en la pantalla de busqueda 
	 * Dialogo para confirmar el desblqueo
	 * @return 
	 * */
	public void unBlockPayerAgentDialogConfirm(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		JSFUtilities.updateComponent("frmSearchPaymentAgent");
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("alert.correct.un.block.paymet.agent",paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName());
		JSFUtilities.putViewMap(bodyMessage, headerMessage);
		JSFUtilities.updateComponent("opnlDialogs"); 
    	JSFUtilities.executeJavascriptFunction("PF('dlgAlertMessagePaymentAgent').show();");
    	
    	/**Notificacion de bloqueo de contacto agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_BLOCK_UNBLOCK_CONTACT_AGENT_PAYMENT.getCode());
		
		String nameContact = paymentAgentSelected.getName()+" "+ paymentAgentSelected.getFirstName()+" "+paymentAgentSelected.getLastName()==null?"":paymentAgentSelected.getLastName();
		String agentPayment = paymentAgentSelected.getPaymentAgentFullName();
		Object[] parameters = {"Desbloqueo",nameContact,agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,null, parameters);
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialogo de rechazo de usuario/contacto agente pagador
	 * */
	public String blockPayerAgentDialogReject(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		JSFUtilities.updateComponent("fldResult");
		return null;
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo para confirmar el registro de un usuario/contacto agente pagador
	 * */
	public void confirmPayerAgentDialog(){
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_NOTIFICATION,paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirm').show();");
	}
	public void confirmModifyPayerAgentDialog(){
		String name = paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName();
		Object[] parameters = {name,paymentAgentSelected.getPaymentAgentFullName() };
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.modify.contact.agent",parameters);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogConfirmModifyContact').show();");
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo de notificacion de usuario/contacto agente pagador confirmado correctamente
	 * **/
	public void confirmPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected, StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);

		/**Notificacion de confirmacion de contacto agente pagador*/
		BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CONFIRM_REJECT_CONTACT_AGENT_PAYMENT.getCode());
		
		String agentPayment = paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName();
		Object[] parameters = {"Confirmado",agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
		
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_NOTIFICATION_CONFIRM,paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirmYes').show();");
	}
	
	public void confirmModifyPayerAgentDialogYes(){
		agentFacade.confirmModifyContactAgent(paymentAgentSelected.getIdPaymentAgentPk(),viewPaymentAgent);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		String name = viewPaymentAgent.getName()+" "+viewPaymentAgent.getFirstName();
		Object[] parameters = {name,paymentAgentSelected.getPaymentAgentFullName() };
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.successfully.modify",parameters);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirmYes').show();");
	}

	/**
	 * Usado en la pantalla de detalle
	 * Dialogo de rechazo de usuario/contaco agente pagador
	 * 
	 * */
	public void rejectPayerAgentDialog(){
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT_PAYER_AGENT,paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgRejectState').show();");
	}
	/**
	 * Rechazar la modificacion
	 * */
	public void rejectModifyPayerAgentDialog(){
		Object[] parameters = {viewPaymentAgent.getName()+" "+viewPaymentAgent.getFirstName() +" "+viewPaymentAgent.getLastName(),paymentAgentSelected.getPaymentAgentFullName() };
		String bodyMessage = PropertiesUtilities.getMessage("alert.reject.confim.modify.contact.agent",parameters);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvRejectModifyState').show();");
	}
	
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo de notificacion de usuario/contacto rechazado correctamente
	 * */
	public void rejectPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.REJECTED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		JSFUtilities.updateComponent("fldResult");
		
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT_NOTIFICATION,paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogRejectYes').show();");
    	
    	/**Notificacion de rechazo de contacto agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CONFIRM_REJECT_CONTACT_AGENT_PAYMENT.getCode());
		
		String agentPayment = paymentAgentSelected.getPaymentAgentFullName();
		Object[] parameters = {"Rechazo",agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	/**
	 * rechazar la modificacion
	 * */
	public void rejectModifyPayerAgentDialogYes(){
		agentFacade.rejecyModifyContactAgent(paymentAgentSelected);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		Object[] parameters = {viewPaymentAgent.getName()+" "+viewPaymentAgent.getFirstName(),paymentAgentSelected.getPaymentAgentFullName() };
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.reject.modify.successfully.modify",parameters);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogRejectYes').show();");
	}
	/** Usado en la pantalla de busqueda
	 *  Verifica el estado del usuario/contacto agente pagador
	 *  Se abre pantalla de detalle en caso de tener los estados correctosS
	 * 	@return paymentAgentContactLoadInfo
	 * 
	 * **/
	public String beforeBlocking(){
		if(isSelectedPaymentAgent()){
			if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
					paymentAgentSelected.getSituation().equals(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT)){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(false);
				viewPaymentAgent.setRenderedBlock(true);
				viewPaymentAgent.setRenderedUnBlock(false);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(false);
				return "paymentAgentContactLoadInfo";
			}else{
				/*Privilegio de confirmar bloqueo*/
				if(userPrivilege.getUserAcctions().isConfirmBlock()){
					if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
							paymentAgentSelected.getSituation().equals(SituationConstants.PENDING_LOCK_CONTACT_PAYMENT)){
						String headerMessageView = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
						Object[] bodyData = new Object[2];
						bodyData[0] = paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName();
						bodyData[1] =  paymentAgentSelected.getPaymentAgentFullName();// HOLDER
						String bodyMessageView = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_LOOK_PAYER_AGENT,bodyData);
						showMessageOnDialogPayment(headerMessageView,bodyMessageView); 
						JSFUtilities.updateComponent("opnlDialogs");
				    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgBlockStateConfirm').show();");
					}else{
						String msg = PropertiesUtilities.getMessage("lbl.alert.no.state.no.situation");
						showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
						JSFUtilities.showSimpleValidationDialog();
					}
				}
				else{//No tiene el permiso
					String msg = PropertiesUtilities.getMessage("lbl.not.privilege");
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return null;
	}
	
	/** Metodo usado en la pantalla de detalle
	 *  Registro de un desbloqueo de usuario/contacto agente pagador
	 * 	@return paymentAgentContactLoadInfo
	 * 
	 * **/
	public String beforeUnBlocking(){
		if(isSelectedPaymentAgent()){
			if(paymentAgentSelected.getState().equals(StatesConstants.BLOCKED_PAYER_AGENT)&&
					paymentAgentSelected.getSituation().equals(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT)){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(false);
				viewPaymentAgent.setRenderedBlock(false);
				viewPaymentAgent.setRenderedUnBlock(true);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(false);
				return "paymentAgentContactLoadInfo";
			}else{
				/*Privelegio de confirmar desbloqueo*/
				if(userPrivilege.getUserAcctions().isConfirmUnblock()){
				if(paymentAgentSelected.getState().equals(StatesConstants.BLOCKED_PAYER_AGENT)&&
						paymentAgentSelected.getSituation().equals(SituationConstants.UNLOCKING_PENDING_CONTACT_PAYMENT)){
					String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					Object[] bodyData = new Object[2];
					bodyData[0] = paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName();
					bodyData[1] =  paymentAgentSelected.getPaymentAgentFullName();
					String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.unlook.contact.payer.agent",bodyData);
					showMessageOnDialogPayment(headerMessage,bodyMessage); 
					JSFUtilities.updateComponent("opnlDialogs");
			    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgUnBlockStateConfirm').show();");
				}else{
					String msg = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NOTIFY_CONTACT_NO_ENABLE);
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
				}else{
					String msg = PropertiesUtilities.getMessage("lbl.not.privilege");
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return "";
	}
	
	
	//Registrando el bloqueo del agente pagador
	public String blockPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.PENDING_LOCK_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_REGISTRE_BLOCK_AGENT,paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogBlockYes').show();");
    	return "";
	}
	public boolean isSelectedPaymentAgent(){
		if(paymentAgentSelected==null){
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED);
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}else
			return true;
	}
	private boolean isRegisteredState(){
		if(paymentAgentSelected.getState().equals(StatesConstants.REGISTERED_PAYER_AGENT)){
			return true;
		}else{
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_STATE_REGISTERED);
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}
	}
	private void showMessageOnDialogPayment(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	/**
	 * Usado en la pantalla de modificacion de usuario/contacto agente pagador
	 * Verificamos los cambios antes de registrar la modificacion
	 * */
	public void preModifyPaymetAgent(){
		if(isModifiedPayment()){
			if(agentModifiedTO.getListSecurityClassSelected().size()<1){
				String message = PropertiesUtilities.getMessage("lbl.request.security.class.requiered");
		        showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
			    JSFUtilities.showSimpleValidationDialog();
			}else{
				JSFUtilities.executeJavascriptFunction("PF('wVconfirmModifyPaymet').show();");
			}
		}else{
			String message = PropertiesUtilities.getMessage("lbl.modification.was.made.verify");
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),message);
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	public void executeSaveModify(){
		agentFacade.preSaveMofifyPaymetAgent(agentModifiedTO,paymentAgentSelected);
		Object[] bodyData = new Object[2];
		bodyData[0] = paymentAgentSelected.getName()+" "+paymentAgentSelected.getFirstName();
		bodyData[1] =  paymentAgentSelected.getPaymentAgentFullName();
		String bodyMessageView = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REGISTERED_MODIFICATION_AGENT,bodyData);
		agentModifiedTO= new PayerAgentRegistreTO();
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVModifySuccessfullPaymet').show();");
        showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), bodyMessageView);
	}
	public String modifySuccessfullPaymet(){
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		JSFUtilities.updateComponent("frmModifyPaymentAgent");
		return "backPageModifyPagePaymentAgentContact";
	}
	
	/**
	 * Usado en la pantalla de modificacion
	 * Verificamos si el usuario/contacto agente pagador fue modificado
	 * */
	private boolean isModifiedPayment(){
//		if(!agentModifiedTO.getParameterTablePk().equals(paymentAgentSelected.getTypePaymentAgent()))
//			return true;
//		if(agentModifiedTO.getIdHolderPk()!=null)
//			return true;
		if(!agentModifiedTO.getName().equalsIgnoreCase(paymentAgentSelected.getName()))
			return true;
		if(!agentModifiedTO.getFirstName().equalsIgnoreCase(paymentAgentSelected.getFirstName()))
			return true;
		if(!compareStrings(agentModifiedTO.getLastName(),paymentAgentSelected.getLastName()))
			return true;
		if(!agentModifiedTO.getEmail().equalsIgnoreCase(paymentAgentSelected.getEmail()))
			return true;
		if(!agentModifiedTO.getCellPhone().equalsIgnoreCase(paymentAgentSelected.getCellPhone()))
			return true;
		if(!compareStrings(agentModifiedTO.getPhone(),paymentAgentSelected.getPhone()))
			return true;
		if(!compareStrings(agentModifiedTO.getInternPhone(),paymentAgentSelected.getIntern()))
			return true;
		if(!compareStrings(agentModifiedTO.getJob(),paymentAgentSelected.getJob()))
			return true;
		if(paymentAgentSelected.getEncryp()==null)
			paymentAgentSelected.setEncryp(0);
		
		if(agentModifiedTO.isEncryptMail()!=paymentAgentSelected.getEncryp().equals(BooleanType.YES.getCode())?true:false)
			return true;
		
		/**Comparando certificados*/
		/*Certificado diferente de null y se carga nuevo certificado*/
		if(paymentAgentSelected.getCertificateName()!=null)
			if(agentModifiedTO.getCertificate()==null)
				return true;
	
		/*Sin certificado*/
		if(paymentAgentSelected.getCertificate()==null)
			if(agentModifiedTO.getCertificate()!=null)
				return true;
		
		if(agentModifiedTO.isPaymentWithLip()!=paymentAgentSelected.getIndLip().equals(BooleanType.YES.getCode())?true:false)
			return true;
		
		if(agentModifiedTO.isIndSendUrlPayment()!=paymentAgentSelected.getIndSendUrlPayment().equals(BooleanType.YES.getCode())?true:false)
			return true;
		
		if(agentModifiedTO.getListSecurityClassSelected().size()>0){
			StringBuilder builder = new StringBuilder();
			for (ParameterTable parameterTable : agentModifiedTO.getListSecurityClassSelected()){
				builder.append(parameterTable.getParameterTablePk().toString());
				builder.append(",");
			}
			String classListString = builder.toString().substring(0, builder.toString().length()-1);
			if(!classListString.matches(paymentAgentSelected.getSecurityClassList()))
				return true;
			else 
				return false;
		}
		return true;
	}
	private boolean compareStrings(Object object1, Object object2){
		try {
			object1.toString();
			if(object2!=null)
				return object1.toString().equalsIgnoreCase(object2.toString());
			else 
				return false;
		} catch (Exception e) {
			if(object2==null)
				return true;
			else
				return false;
		}
	}
	/**
	 * Usado en la pantalla de registro
	 * Dialo para regresar a la pantalla de bsuqueda
	 * */
	public String backModifyPaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogPayment(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVBackModifyScreen').show();");
	    	return null;
		}
		else
			return "backPageModifyPagePaymentAgentContact";
	}
	
	/**
	 * Rechzando el desbloqueo*/
	public void blockPayerAgentDialogConfirmReject(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.BLOCKED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgentContacts = agentFacade.searchPaymentAgentContacts(searchPaymentAgent);
		
		String message = PropertiesUtilities.getMessage("label.alert.reject.block.contact.payment");
		showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),message);
		JSFUtilities.showSimpleValidationDialog();
	}
	
	
	/**Elimina el certificado cargado*/
	public String deleteCertificateLoaded(){
		agentModifiedTO.setCertificateName(null);
		agentModifiedTO.setCertificate(null);
		return null;
	}
	
	/**
	 * Carga de certificado*/
	/**
	 * Usado en la pantalla de registro de cuantas bancarias
	 * */
	public void documentAttachFile(FileUploadEvent event){
		String fDisplayName = event.getFile().getFileName(); //fUploadValidateFile(event.getFile(),null, "5000","cer");
		if(fDisplayName!=null){
			agentModifiedTO.setCertificateName(fDisplayName);
			agentModifiedTO.setCertificate(event.getFile().getContents());
			JSFUtilities.updateComponent("idDeleteFile");
		}else{
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	public PayerAgentRegistreTO getAgentRegistreTO() {
		return searchPaymentAgent;
	}
	public List<ParameterTable> getListTypePayingAgent() {
		return listTypePayingAgent;
	}
	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}
	public List<ParameterTable> getListStatePayingAgent() {
		return listStatePayingAgent;
	}
	
	public PaymentAgent getPaymentAgentSelected() {
		return paymentAgentSelected;
	}
	public void setPaymentAgentSelected(PaymentAgent paymentAgentSelected) {
		this.paymentAgentSelected = paymentAgentSelected;
	}

	public PayerAgentRegistreTO getSearchPaymentAgent() {
		return searchPaymentAgent;
	}

	public PayerAgentRegistreTO getViewPaymentAgent() {
		return viewPaymentAgent;
	}

	public List<ParameterTable> getListSituationPayingAgent() {
		return listSituationPayingAgent;
	}
	public PayerAgentRegistreTO getAgentModifiedTO() {
		return agentModifiedTO;
	}
	public void setAgentModifiedTO(PayerAgentRegistreTO agentModifiedTO) {
		this.agentModifiedTO = agentModifiedTO;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public void setSearchPaymentAgent(PayerAgentRegistreTO searchPaymentAgent) {
		this.searchPaymentAgent = searchPaymentAgent;
	}
	public void setViewPaymentAgent(PayerAgentRegistreTO viewPaymentAgent) {
		this.viewPaymentAgent = viewPaymentAgent;
	}
	public void setListTypePayingAgent(List<ParameterTable> listTypePayingAgent) {
		this.listTypePayingAgent = listTypePayingAgent;
	}
	public void setListStatePayingAgent(List<ParameterTable> listStatePayingAgent) {
		this.listStatePayingAgent = listStatePayingAgent;
	}
	public void setListSituationPayingAgent(
			List<ParameterTable> listSituationPayingAgent) {
		this.listSituationPayingAgent = listSituationPayingAgent;
	}
	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}
	
	public List<PaymentAgent> getListPaymentAgents() {
		return listPaymentAgents;
	}
	public void setListPaymentAgents(List<PaymentAgent> listPaymentAgents) {
		this.listPaymentAgents = listPaymentAgents;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
	public GenericDataModel<PaymentAgent> getListPaymentAgentContacts() {
		return listPaymentAgentContacts;
	}
	public void setListPaymentAgentContacts(
			GenericDataModel<PaymentAgent> listPaymentAgentContacts) {
		this.listPaymentAgentContacts = listPaymentAgentContacts;
	}
}
