package com.pradera.paymet.agent.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;


public class PayerAgentRegistreTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7434466060074239614L;

	private Integer parameterTablePk;//este es el tipo de agente
	
	private boolean renderedParameterTablePk;
	
	private Long idPaymentAgentPk; //agente pagador
	
	private String name;
	
	private boolean renderedNameAsterisk;
	
	private String mnemonic;
	
	private boolean renderedMnemonicAsterisk;
	
	private String description;
	
	private boolean renderedDescriptionAsterisk;
	
	private String firstName;
	
	private boolean renderedFirstNameAsterisk;
	
	private String lastName;
	
	private boolean renderedLastNameAsterisk;
	
	private String email;
	
	private boolean renderedEmailAsterisk;
	
	private String cellPhone;
	
	private boolean renderedCellPhoneAsterisk;
	
	private String phone;
	
	private boolean renderedPhoneAsterisk;
	
	private String internPhone;
	
	private boolean renderedInternPhoneAsterisk;
	
	private String job;
	
	private boolean renderedJobAsterisk;
	
	private boolean renderedSecurityClassAsterisk;
	
	private Integer state;
	
	private Integer situation;
	
	private List<ParameterTable> listSecurityClassSelected;
	
	private Integer securityClass;
	
	private boolean renderedRejected;
	
	private boolean renderedConfirm;
	
	private boolean renderedBlock;
	
	private boolean renderedUnBlock;
	
	private boolean renderdModify;
	
	private boolean renderedConfirmModfy;
	
	private boolean renderedRejectModify;
	
	private boolean paymentWithLip;
	
	private boolean indSendUrlPayment;
	
	private boolean renderedLipAsterisk;
	
	private boolean renderedIndUrl;
	
	private String certificateName;
	
   	private byte[] certificate;
   	
   	private boolean encryptMail;
   	
   	private boolean encryptMailAsterisk;
	
	public PayerAgentRegistreTO() {
		listSecurityClassSelected = new ArrayList<>();
		renderedRejected = false;
		setParameterTablePk(45);//Por defecto bancos
		listSecurityClassSelected = new ArrayList<>();
		
		//por defecto paga con lip
		paymentWithLip=true;
		encryptMail = false;
	}
	public Integer getParameterTablePk() {
		return parameterTablePk;
	}
	public void setParameterTablePk(Integer parameterTablePk) {
		this.parameterTablePk = parameterTablePk;
	}
	
	public Long getIdPaymentAgentPk() {
		return idPaymentAgentPk;
	}
	public void setIdPaymentAgentPk(Long idPaymentAgentPk) {
		this.idPaymentAgentPk = idPaymentAgentPk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isRenderedLipAsterisk() {
		return renderedLipAsterisk;
	}
	public void setRenderedLipAsterisk(boolean renderedLipAsterisk) {
		this.renderedLipAsterisk = renderedLipAsterisk;
	}
	public boolean isPaymentWithLip() {
		return paymentWithLip;
	}
	public void setPaymentWithLip(boolean paymentWithLip) {
		this.paymentWithLip = paymentWithLip;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getInternPhone() {
		return internPhone;
	}
	public void setInternPhone(String internPhone) {
		this.internPhone = internPhone;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public List<ParameterTable> getListSecurityClassSelected() {
		return listSecurityClassSelected;
	}
	public void setListSecurityClassSelected(
			List<ParameterTable> listSecurityClassSelected) {
		this.listSecurityClassSelected = listSecurityClassSelected;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getSituation() {
		return situation;
	}
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	public boolean isRenderedRejected() {
		return renderedRejected;
	}
	public void setRenderedRejected(boolean renderedRejected) {
		this.renderedRejected = renderedRejected;
	}
	public boolean isRenderedConfirm() {
		return renderedConfirm;
	}
	public void setRenderedConfirm(boolean renderedConfirm) {
		this.renderedConfirm = renderedConfirm;
	}
	public boolean isRenderedBlock() {
		return renderedBlock;
	}
	public void setRenderedBlock(boolean renderedBlock) {
		this.renderedBlock = renderedBlock;
	}
	public boolean isRenderedUnBlock() {
		return renderedUnBlock;
	}
	public void setRenderedUnBlock(boolean renderedUnBlock) {
		this.renderedUnBlock = renderedUnBlock;
	}
	public boolean isRenderdModify() {
		return renderdModify;
	}
	public void setRenderdModify(boolean renderdModify) {
		this.renderdModify = renderdModify;
	}
	public boolean isRenderedConfirmModfy() {
		return renderedConfirmModfy;
	}
	public void setRenderedConfirmModfy(boolean renderedConfirmModfy) {
		this.renderedConfirmModfy = renderedConfirmModfy;
	}
	public boolean isRenderedRejectModify() {
		return renderedRejectModify;
	}
	public void setRenderedRejectModify(boolean renderedRejectModify) {
		this.renderedRejectModify = renderedRejectModify;
	}
	public boolean isRenderedNameAsterisk() {
		return renderedNameAsterisk;
	}
	public void setRenderedNameAsterisk(boolean renderedNameAsterisk) {
		this.renderedNameAsterisk = renderedNameAsterisk;
	}
	public boolean isRenderedFirstNameAsterisk() {
		return renderedFirstNameAsterisk;
	}
	public void setRenderedFirstNameAsterisk(boolean renderedFirstNameAsterisk) {
		this.renderedFirstNameAsterisk = renderedFirstNameAsterisk;
	}
	public boolean isRenderedLastNameAsterisk() {
		return renderedLastNameAsterisk;
	}
	public void setRenderedLastNameAsterisk(boolean renderedLastNameAsterisk) {
		this.renderedLastNameAsterisk = renderedLastNameAsterisk;
	}
	public boolean isRenderedEmailAsterisk() {
		return renderedEmailAsterisk;
	}
	public void setRenderedEmailAsterisk(boolean renderedEmailAsterisk) {
		this.renderedEmailAsterisk = renderedEmailAsterisk;
	}
	public boolean isRenderedCellPhoneAsterisk() {
		return renderedCellPhoneAsterisk;
	}
	public void setRenderedCellPhoneAsterisk(boolean renderedCellPhoneAsterisk) {
		this.renderedCellPhoneAsterisk = renderedCellPhoneAsterisk;
	}
	public boolean isRenderedPhoneAsterisk() {
		return renderedPhoneAsterisk;
	}
	public void setRenderedPhoneAsterisk(boolean renderedPhoneAsterisk) {
		this.renderedPhoneAsterisk = renderedPhoneAsterisk;
	}
	public boolean isRenderedInternPhoneAsterisk() {
		return renderedInternPhoneAsterisk;
	}
	public void setRenderedInternPhoneAsterisk(boolean renderedInternPhoneAsterisk) {
		this.renderedInternPhoneAsterisk = renderedInternPhoneAsterisk;
	}
	public boolean isRenderedJobAsterisk() {
		return renderedJobAsterisk;
	}
	public void setRenderedJobAsterisk(boolean renderedJobAsterisk) {
		this.renderedJobAsterisk = renderedJobAsterisk;
	}
	public boolean isRenderedSecurityClassAsterisk() {
		return renderedSecurityClassAsterisk;
	}
	public void setRenderedSecurityClassAsterisk(
			boolean renderedSecurityClassAsterisk) {
		this.renderedSecurityClassAsterisk = renderedSecurityClassAsterisk;
	}
	public boolean isIndSendUrlPayment() {
		return indSendUrlPayment;
	}
	public void setIndSendUrlPayment(boolean indSendUrlPayment) {
		this.indSendUrlPayment = indSendUrlPayment;
	}
	public boolean isRenderedIndUrl() {
		return renderedIndUrl;
	}
	public void setRenderedIndUrl(boolean renderedIndUrl) {
		this.renderedIndUrl = renderedIndUrl;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public byte[] getCertificate() {
		return certificate;
	}
	public void setCertificate(byte[] certificate) {
		this.certificate = certificate;
	}
	public boolean isEncryptMail() {
		return encryptMail;
	}
	public void setEncryptMail(boolean encryptMail) {
		this.encryptMail = encryptMail;
	}
	public boolean isEncryptMailAsterisk() {
		return encryptMailAsterisk;
	}
	public void setEncryptMailAsterisk(boolean encryptMailAsterisk) {
		this.encryptMailAsterisk = encryptMailAsterisk;
	}
	public String getMnemonic() {
		return mnemonic;
	}
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isRenderedMnemonicAsterisk() {
		return renderedMnemonicAsterisk;
	}
	public void setRenderedMnemonicAsterisk(boolean renderedMnemonicAsterisk) {
		this.renderedMnemonicAsterisk = renderedMnemonicAsterisk;
	}
	public boolean isRenderedDescriptionAsterisk() {
		return renderedDescriptionAsterisk;
	}
	public void setRenderedDescriptionAsterisk(boolean renderedDescriptionAsterisk) {
		this.renderedDescriptionAsterisk = renderedDescriptionAsterisk;
	}
	public boolean isRenderedParameterTablePk() {
		return renderedParameterTablePk;
	}
	public void setRenderedParameterTablePk(boolean renderedParameterTablePk) {
		this.renderedParameterTablePk = renderedParameterTablePk;
	}
	
}
