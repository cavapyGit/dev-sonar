package com.pradera.paymet.agent.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.paymet.agent.facade.PaymetAgentFacade;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;

@DepositaryWebBean
@LoggerCreateBean
public class RegisterPaymetAgentMgmtBean extends GenericBaseBean{

	private static final long serialVersionUID = 1L;
	
	private PayerAgentRegistreTO agentRegistreTO;
	
	private List<ParameterTable>  listTypePayingAgent;
	
	
	@Inject
	private PaymetAgentFacade agentFacade;
	
	@Inject
	private CollectionRigthFacade collectionRigthFacade; 
	
	private List<ParameterTable> listSecurityClass;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@PostConstruct
	public void init(){
		agentRegistreTO = new PayerAgentRegistreTO();
		agentRegistreTO.setParameterTablePk(null);
		listTypePayingAgent = agentFacade.getListEconomicActivities();
		listSecurityClass = collectionRigthFacade.getSecurityClassListAll();
		JSFUtilities.updateComponent("frmRegisterPaymentAgent");
	}
	
	/**
	 * Guardamos el agente pagador
	 * */
	public void preSavePaymetAgentContact(){
		//validando el mnemonico
		if(verifyMnemonic()){
			JSFUtilities.updateComponent("idconfirmSavePaymet");
			JSFUtilities.executeJavascriptFunction("wVconfirmSavePaymet.show()");
		}
	}
	private void showMessageOnDialogRegPaymet(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessagView", headerMessageConfirmation);
	}
	@LoggerAuditWeb
	public void executeSavePaymentAgentContact(){
		/*verificar si ya existe contacto agente pagador*/
		if(agentFacade.existsContactPaymentAgent(agentRegistreTO)){
			JSFUtilities.updateComponent("frmRegisterPaymentAgent");
			String message = PropertiesUtilities.getMessage("label.exists.registre.contact.payment.alert",agentRegistreTO.getEmail());
	        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
		    JSFUtilities.showSimpleValidationDialog();
		    return;
		}
		String description = agentRegistreTO.getDescription();
		agentFacade.preSavePaymetAgent(agentRegistreTO);
		agentRegistreTO= new PayerAgentRegistreTO();
		agentRegistreTO.setParameterTablePk(null);
		String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SUCCESFULL_REGISTRE_PAYMENT_AGENT,description);
		//JSFUtilities.updateComponent("frmRegisterPaymentAgent");
		
        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
	   // JSFUtilities.showSimpleValidationDialog();
        JSFUtilities.showSimpleEndTransactionDialog("backPageSearchPaymentAgent");
	}
	
	public boolean verifyMnemonic(){
		if(agentRegistreTO.getMnemonic() != null){
			if(agentFacade.existsPaymentAgentMnemonic(agentRegistreTO.getMnemonic(), StatesConstants.REGISTERED_PAYER_AGENT)){
				agentRegistreTO.setMnemonic(null);
				JSFUtilities.updateComponent("frmRegisterPaymentAgent:idMnemonic");
				String message = "Ya existe un Agente Pagador registrado con ese mnemonico";
		        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
			    JSFUtilities.showSimpleValidationDialog();
			    return false;
			}
			if(agentFacade.existsPaymentAgentMnemonic(agentRegistreTO.getMnemonic(), StatesConstants.CONFIRMED_PAYER_AGENT)){
				 agentRegistreTO.setMnemonic(null);
				 JSFUtilities.updateComponent("frmRegisterPaymentAgent:idMnemonic");
				String message = "Ya existe un Agente Pagador confirmado con ese mnemonico";
		        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
			    JSFUtilities.showSimpleValidationDialog();
			    return false;
			}
		}
		return true;
	}
	
	private boolean isModifiedPayment(){
		if(agentRegistreTO.getParameterTablePk()!=null)
			return true;
		if(agentRegistreTO.getIdPaymentAgentPk()!=null)
			return true;
		if(agentRegistreTO.getDescription()!=null)
			return true;
		if(agentRegistreTO.getMnemonic()!=null)
			return true;
		if(agentRegistreTO.getEmail()!=null)
			return true;
		if(agentRegistreTO.getCellPhone()!=null)
			return true;
		if(agentRegistreTO.getListSecurityClassSelected().size()>0)
			return true;
		else 
			return false;
		
	}
	/**
	 * Usado en la pantalla de registro de agente pagador
	 * Funcion limipar pantalla
	 * @return null
	 * */
	public String cleanRegistrePaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			showMessageOnDialogRegPaymet(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVCleanRegistreScreen').show();");
		}
		return null;
	}
	/**
	 * Usado en la pantalla de registro
	 * Limpiamos la pantalla de registro
	 * */
	public void exeCleanRegistrePaymentScreen(){
		agentRegistreTO = new PayerAgentRegistreTO();
		JSFUtilities.updateComponent("frmRegisterPaymentAgent");
	}
	
	/**
	 * Usado en la pantalla de registro
	 * Dialo para regresar a la pantalla de bsuqueda
	 * */
	public String backRegistrePaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogRegPaymet(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVBackRegistreScreen').show();");
	    	return null;
		}
		else
			return "backPageSearchPaymentAgent";
	}
	
	public String exeBackRegistrePaymentScreen(){
		agentRegistreTO= new PayerAgentRegistreTO();
		return "backPageSearchPaymentAgent";
	}
	public PayerAgentRegistreTO getAgentRegistreTO() {
		return agentRegistreTO;
	}
	public void setAgentRegistreTO(PayerAgentRegistreTO agentRegistreTO) {
		this.agentRegistreTO = agentRegistreTO;
	}
	public List<ParameterTable> getListTypePayingAgent() {
		return listTypePayingAgent;
	}
	public void setListTypePayingAgent(List<ParameterTable> listTypePayingAgent) {
		this.listTypePayingAgent = listTypePayingAgent;
	}
	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}
	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
}
