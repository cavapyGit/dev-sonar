package com.pradera.paymet.agent.contact.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;

import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.paymet.agent.facade.PaymetAgentFacade;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;
import com.edv.collection.economic.rigth.PaymentAgent;

@DepositaryWebBean
@LoggerCreateBean
public class RegisterPaymentAgentContactMgmtBean extends GenericBaseBean{

	private static final long serialVersionUID = 1L;
	
	private PayerAgentRegistreTO agentRegistreTO;
	
	private List<ParameterTable>  listTypePayingAgent;
	
	private List<PaymentAgent> listPaymentAgents;
	
	@Inject
	private PaymetAgentFacade agentFacade;
	
	@Inject
	private CollectionRigthFacade collectionRigthFacade; 
	
	private List<ParameterTable> listSecurityClass;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;

	@PostConstruct
	public void init(){
		agentRegistreTO = new PayerAgentRegistreTO();
		listTypePayingAgent = agentFacade.getListEconomicActivities();
		listSecurityClass = collectionRigthFacade.getSecurityClassListAll();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
	}
	/**
	 * Actualizamos la lista de agentes pagadores
	 * 
	 * */
	public void changeTypePaymentAgent(){
		listPaymentAgents = null;
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
	}
	
	/**
	 * Guardamos el contacto agente pagador
	 * */
	public void preSavePaymetAgentContact(){
		if(agentRegistreTO.getListSecurityClassSelected().size()<1){
			String message = "Debe seleccionar al menos una clase de valor";
	        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
		    JSFUtilities.showSimpleValidationDialog();
		}else{
			JSFUtilities.executeJavascriptFunction("wVconfirmSavePaymet.show()");		
		}
	}
	private void showMessageOnDialogRegPaymet(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	@LoggerAuditWeb
	public void executeSavePaymentAgentContact(){
		/*verificar si ya existe contacto agente pagador*/
		if(agentFacade.existsContactPaymentAgent(agentRegistreTO)){
			JSFUtilities.updateComponent("frmRegisterPaymentAgent");
			String message = PropertiesUtilities.getMessage("label.exists.registre.contact.payment.alert",agentRegistreTO.getEmail());
	        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
		    JSFUtilities.showSimpleValidationDialog();
		    return;
		}
		agentFacade.preSavePaymetAgentContact(agentRegistreTO);
		String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SUCCESFULL_REGISTRE_AGENT);
		agentRegistreTO= new PayerAgentRegistreTO();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
		JSFUtilities.updateComponent("frmRegisterPaymentAgent");
        showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
	    JSFUtilities.showSimpleValidationDialog();
	}
	
	private boolean isModifiedPayment(){
		if(agentRegistreTO.getParameterTablePk()!=45L)
			return true;
		if(agentRegistreTO.getIdPaymentAgentPk()!=null)
			return true;
		if(agentRegistreTO.getName()!=null)
			return true;
		if(agentRegistreTO.getFirstName()!=null)
			return true;
		if(agentRegistreTO.getLastName()!=null)
			return true;
		if(agentRegistreTO.getEmail()!=null)
			return true;
		if(agentRegistreTO.getCellPhone()!=null)
			return true;
		if(agentRegistreTO.getPhone()!=null)
			return true;
		if(agentRegistreTO.getInternPhone()!=null)
			return true;
		if(agentRegistreTO.getJob()!=null)
			return true;
		if(agentRegistreTO.getListSecurityClassSelected().size()>0)
			return true;
		else 
			return false;
	}
	/**
	 * Usado en la pantalla de registro de agente pagador
	 * Funcion limipar pantalla
	 * @return null
	 * */
	public String cleanRegistrePaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			showMessageOnDialogRegPaymet(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVCleanRegistreScreen').show();");
		}
		return null;
	}
	/**
	 * Usado en la pantalla de registro
	 * Limpiamos la pantalla de registro
	 * */
	public void exeCleanRegistrePaymentScreen(){
		agentRegistreTO= new PayerAgentRegistreTO();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
		JSFUtilities.updateComponent("frmRegisterPaymentAgent");
	}
	
	/**
	 * Usado en la pantalla de registro
	 * Dialo para regresar a la pantalla de bsuqueda
	 * */
	public String backRegistrePaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogRegPaymet(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVBackRegistreScreen').show();");
	    	return null;
		}
		else
			return "backPageSearchPaymentAgentContact";
	}
	
	/**
	 * Carga de certificado*/
	/**
	 * Usado en la pantalla de registro de cuantas bancarias
	 * */
	public void documentAttachFile(FileUploadEvent event){
		String fDisplayName=fUploadValidateFile(event.getFile(),null, "5000","cer");
		if(fDisplayName!=null){
			agentRegistreTO.setCertificateName(fDisplayName);
			agentRegistreTO.setCertificate(event.getFile().getContents());
			JSFUtilities.updateComponent("idDeleteFile");
		}else{
			showMessageOnDialogRegPaymet(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**Elimina el certificado cargado*/
	public String deleteCertificateLoaded(){
		agentRegistreTO.setCertificateName(null);
		agentRegistreTO.setCertificate(null);
		return null;
	}
	public String exeBackRegistrePaymentScreen(){
		agentRegistreTO= new PayerAgentRegistreTO();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
		return "backPageSearchPaymentAgentContact";
	}
	public PayerAgentRegistreTO getAgentRegistreTO() {
		return agentRegistreTO;
	}
	public void setAgentRegistreTO(PayerAgentRegistreTO agentRegistreTO) {
		this.agentRegistreTO = agentRegistreTO;
	}
	public List<ParameterTable> getListTypePayingAgent() {
		return listTypePayingAgent;
	}
	public void setListTypePayingAgent(List<ParameterTable> listTypePayingAgent) {
		this.listTypePayingAgent = listTypePayingAgent;
	}
	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}
	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public List<PaymentAgent> getListPaymentAgents() {
		return listPaymentAgents;
	}
	public void setListPaymentAgents(List<PaymentAgent> listPaymentAgents) {
		this.listPaymentAgents = listPaymentAgents;
	}
}
