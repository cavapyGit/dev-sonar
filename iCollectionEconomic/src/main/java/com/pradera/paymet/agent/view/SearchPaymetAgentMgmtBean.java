package com.pradera.paymet.agent.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.collection.right.util.SituationConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.BusinessProcess;
import com.pradera.paymet.agent.facade.PaymetAgentFacade;
import com.pradera.paymet.agent.service.PaymentAgentServiceBean;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;
import com.edv.collection.economic.rigth.PaymentAgent;
import com.edv.collection.economic.rigth.ReqModifyPaymentAgent;

@DepositaryWebBean
@LoggerCreateBean
public class SearchPaymetAgentMgmtBean extends GenericBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PayerAgentRegistreTO searchPaymentAgent,viewPaymentAgent;
	/**TODO PARA MODICACION DE UN USURIO/CONTACTO AGENTE PAGADOR*/
	private PayerAgentRegistreTO agentModifiedTO;
	
	private List<ParameterTable>  listTypePayingAgent,listStatePayingAgent,listSituationPayingAgent;
	
	private GenericDataModel<PaymentAgent> listPaymentAgent;
	
	private PaymentAgent paymentAgentSelected;
	
	@Inject
	private PaymetAgentFacade agentFacade;
	
	@Inject
	private PaymentAgentServiceBean  paymentAgentServiceBean;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private NotificationServiceFacade notificationServiceFacade; 
	
	private boolean visibleResult;
	
	@PostConstruct
	public void init(){
		searchPaymentAgent = new PayerAgentRegistreTO();
		listTypePayingAgent = agentFacade.getListEconomicActivities();
		listStatePayingAgent = agentFacade.getListStatePaymentAgent();
		searchPaymentAgent.setParameterTablePk(null);
		searchFromFilters();
	}
	/**
	 * Cargamos el detalle del contacto agente pagador
	 * */
	public void loadAgentInfo(ActionEvent event){
		PaymentAgent paymentAgentClicked= new PaymentAgent();
		paymentAgentClicked = (PaymentAgent) event.getComponent().getAttributes().get("paymentAgentView");
		listSituationPayingAgent = agentFacade.getListStatePaymentAgent();
		viewPaymentAgent = new PayerAgentRegistreTO();
		viewPaymentAgent.setParameterTablePk(paymentAgentClicked.getTypePaymentAgent());
		/**Datos del agente pagador*/
		viewPaymentAgent.setDescription(paymentAgentClicked.getDescription());
		viewPaymentAgent.setMnemonic(paymentAgentClicked.getMnemonic());
		viewPaymentAgent.setCellPhone(paymentAgentClicked.getCellPhone());
		viewPaymentAgent.setEmail(paymentAgentClicked.getEmail());
		viewPaymentAgent.setState(paymentAgentClicked.getState());
		viewPaymentAgent.setSituation(paymentAgentClicked.getSituation());
		viewPaymentAgent.setRenderedRejected(false);
		viewPaymentAgent.setRenderedConfirm(false);
	}
	
	/**
	 * Metodo que verifica el mnemonico del agente pagador
	 * @return
	 */
	public boolean verifyMnemonic(){
		if(agentModifiedTO.getMnemonic() != null){
			if(agentFacade.existsPaymentAgentMnemonicAndId(agentModifiedTO.getMnemonic(), StatesConstants.REGISTERED_PAYER_AGENT,paymentAgentSelected.getIdPaymentAgentPk())){
				agentModifiedTO.setMnemonic(null);
				JSFUtilities.updateComponent("frmModifyPaymentAgent:idMnemonic");
				String message = "Ya existe un Agente Pagador registrado con ese mnemonico";
				showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
			    JSFUtilities.showSimpleValidationDialog();
			    return false;
			}
			if(agentFacade.existsPaymentAgentMnemonicAndId(agentModifiedTO.getMnemonic(), StatesConstants.CONFIRMED_PAYER_AGENT,paymentAgentSelected.getIdPaymentAgentPk())){
				agentModifiedTO.setMnemonic(null);
				JSFUtilities.updateComponent("frmModifyPaymentAgent:idMnemonic");
				String message = "Ya existe un Agente Pagador confirmado con ese mnemonico";
				showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
			    JSFUtilities.showSimpleValidationDialog();
			    return false;
			}
		}
		return true;
	}
	
	/**
	 * Buscamos agente pagador
	 * 
	 * */
	public void searchFromFilters(){
		paymentAgentSelected = null;
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		if(listPaymentAgent!=null&&listPaymentAgent.getRowCount()>0)
			visibleResult=false;
		else
			visibleResult =true;
	}
	
	/**
	 * Limpiamos las pantalla
	 */
	public void clrearFilters(){
		searchPaymentAgent = new PayerAgentRegistreTO();
		searchPaymentAgent.setParameterTablePk(null);
		listPaymentAgent = new GenericDataModel<>();
		visibleResult = false;
	}
	
	/**
	 * Usado en la pantalla de busqueda
	 * Verifica los estados de un usuario/contacto para ser modificados*/
	public String beforeModify(){
		if(isSelectedPaymentAgent()){
			if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
					paymentAgentSelected.getSituation().equals(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT)){
				
				/*Variable para modificar el agente pagador*/
				agentModifiedTO = new PayerAgentRegistreTO();
				listSituationPayingAgent = agentFacade.getListStatePaymentAgent();
				agentModifiedTO.setParameterTablePk(paymentAgentSelected.getTypePaymentAgent());
				agentModifiedTO.setDescription(paymentAgentSelected.getDescription());
				agentModifiedTO.setMnemonic(paymentAgentSelected.getMnemonic());
				agentModifiedTO.setCellPhone(paymentAgentSelected.getCellPhone());
				agentModifiedTO.setPhone(paymentAgentSelected.getPhone());
				agentModifiedTO.setEmail(paymentAgentSelected.getEmail());
				agentModifiedTO.setState(paymentAgentSelected.getState());
				agentModifiedTO.setSituation(paymentAgentSelected.getSituation());
				
				return "modifyPagePaymentAgent";
			}else{
				/*Mostrar el detalle de la modificacion y confirmar la modificacion*/
				if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
						paymentAgentSelected.getSituation().equals(SituationConstants.PENDING_TO_MODIFY_CONTACT_PAYMET)){
					ReqModifyPaymentAgent agentModify = null;
					try {
						agentModify = agentFacade.searchLastModificationContactPayment(paymentAgentSelected.getIdPaymentAgentPk());
					} catch (Exception e) {
						return null;
					}
					viewPaymentAgent = new PayerAgentRegistreTO();
					
					viewPaymentAgent.setParameterTablePk(agentModify.getTypePaymentAgent());
					if(!paymentAgentSelected.getTypePaymentAgent().equals(viewPaymentAgent.getParameterTablePk()))
						viewPaymentAgent.setRenderedParameterTablePk(true);
					
					viewPaymentAgent.setDescription(agentModify.getDescription());
					if(!paymentAgentSelected.getDescription().equalsIgnoreCase(viewPaymentAgent.getDescription()))
						viewPaymentAgent.setRenderedDescriptionAsterisk(true);
					
					viewPaymentAgent.setMnemonic(agentModify.getMnemonic());
					if(!paymentAgentSelected.getMnemonic().equalsIgnoreCase(viewPaymentAgent.getMnemonic()))
						viewPaymentAgent.setRenderedMnemonicAsterisk(true);
					
					viewPaymentAgent.setCellPhone(agentModify.getCellPhone()==null?"":agentModify.getCellPhone());
					paymentAgentSelected.setCellPhone(paymentAgentSelected.getCellPhone()==null?"":paymentAgentSelected.getCellPhone());
					if(!paymentAgentSelected.getCellPhone().equalsIgnoreCase(viewPaymentAgent.getCellPhone()))
						viewPaymentAgent.setRenderedCellPhoneAsterisk(true);
					
					viewPaymentAgent.setEmail(agentModify.getEmail());
					if(paymentAgentSelected.getEmail()!=null)
					if(!paymentAgentSelected.getEmail().equalsIgnoreCase(viewPaymentAgent.getEmail()))
						viewPaymentAgent.setRenderedEmailAsterisk(true);
					
					viewPaymentAgent.setState(paymentAgentSelected.getState());
					viewPaymentAgent.setSituation(paymentAgentSelected.getSituation());
					
					viewPaymentAgent.setRenderedRejected(false);
					viewPaymentAgent.setRenderedConfirm(false);
					viewPaymentAgent.setRenderedConfirmModfy(true);
					viewPaymentAgent.setRenderedRejectModify(true);
					
					return "paymentAgentLoadInfo";
				}else{
					String msg = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NOTIFY_CONTACT_NO_ENABLE);
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return "";
	}
	public String beforeReject(){
		if(isSelectedPaymentAgent()){
			if(isRegisteredState()){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(true);
				viewPaymentAgent.setRenderedBlock(false);
				viewPaymentAgent.setRenderedUnBlock(false);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(false);
				return "paymentAgentLoadInfo";
			}
		}
		return "";
	}  
	public String beforeConfirm(){
		if(isSelectedPaymentAgent()){
			if(isRegisteredState()){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(false);
				viewPaymentAgent.setRenderedBlock(false);
				viewPaymentAgent.setRenderedUnBlock(false);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(true);
				return "paymentAgentLoadInfo";
			}
		}
		return "";
	} 
	/**
	 * Carga el detalle del contacto agente pagador
	 * */
	private void loadDetailView(){
		listSituationPayingAgent = agentFacade.getListStatePaymentAgent();
		viewPaymentAgent = new PayerAgentRegistreTO();
		viewPaymentAgent.setParameterTablePk(paymentAgentSelected.getTypePaymentAgent());
		viewPaymentAgent.setDescription(paymentAgentSelected.getDescription());
		viewPaymentAgent.setMnemonic(paymentAgentSelected.getMnemonic());
		viewPaymentAgent.setCellPhone(paymentAgentSelected.getCellPhone());
		viewPaymentAgent.setEmail(paymentAgentSelected.getEmail());
		viewPaymentAgent.setState(paymentAgentSelected.getState());
		viewPaymentAgent.setSituation(paymentAgentSelected.getSituation());
	}
	/** Usado en la pantalla de detalle
	 *  Dialogo de registro de bloqueo Dialogo de confirmacion de bloqueo
	 * */
	public void blockPayerAgentDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BLOCK_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgBlockState').show();");
	}
	/** Usado en la pantalla de detalle
	 *  Dialogo para registrar el desbloqueo
	 * 
	 * */
	public void unBlockPayerAgentDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_UNLOOK_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgUnBlockState').show();");
	}
	/**
	 * Usado en la pantalla de detalle
	 * Se registra el debloqueo y retornando a la pantalla de busqueda
	 * @return ""
	 * */
	public void unBlockPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.BLOCKED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.UNLOCKING_PENDING_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_RGISTRY_UNLOOK_PAYER_AGENT,paymentAgentSelected.getMnemonic());
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		//JSFUtilities.putViewMap(bodyMessage, headerMessage);
		JSFUtilities.updateComponent("opnlDialogs"); 
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogUnBlockYes').show();");
    	
	}
	
	/** Usado en la busqueda 
	 * 	Se confirma el bloqueo del usuario/contacto agente pagador
	 * 
	 **/
	public void blockPayerAgentDialogConfirm(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.BLOCKED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		JSFUtilities.updateComponent("frmSearchPaymentAgent");
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("alert.correct.block.paymet.agent",paymentAgentSelected.getMnemonic());
		JSFUtilities.putViewMap(bodyMessage, headerMessage);
		JSFUtilities.updateComponent("opnlDialogs"); 
    	JSFUtilities.executeJavascriptFunction("PF('dlgAlertMessagePaymentAgent').show();");
    	
    	/**Notificacion de BLOQUEO agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess = paymentAgentServiceBean.find(BusinessProcess.class, BusinessProcessType.AGENT_PAYMENT_BLOCK.getCode());
		String agentPayment = paymentAgentSelected.getMnemonic() + " - " + paymentAgentSelected.getDescription();
		Object[] parameters = {agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
    	
    	
	}
	
	/**
	 * Metodo usado en la pantalla de busqueda 
	 * Dialogo para confirmar el desblqueo
	 * @return 
	 * */
	public void unBlockPayerAgentDialogConfirm(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		JSFUtilities.updateComponent("frmSearchPaymentAgent");
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("alert.correct.un.block.payment.agent",paymentAgentSelected.getMnemonic());
		JSFUtilities.putViewMap(bodyMessage, headerMessage);
		JSFUtilities.updateComponent("opnlDialogs"); 
    	JSFUtilities.executeJavascriptFunction("PF('dlgAlertMessagePaymentAgent').show();");
    	
    	/**Notificacion de DESBLOQUEO agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess = paymentAgentServiceBean.find(BusinessProcess.class, BusinessProcessType.AGENT_PAYMENT_UNBLOCK.getCode());
		String agentPayment = paymentAgentSelected.getMnemonic() + " - " + paymentAgentSelected.getDescription();
		Object[] parameters = {agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialogo de rechazo de usuario/contacto agente pagador
	 * */
	public String blockPayerAgentDialogReject(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		JSFUtilities.updateComponent("frmSearchPaymentAgent");
		return null;
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo para confirmar el registro de un usuario/contacto agente pagador
	 * */
	public void confirmPayerAgentDialog(){
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_NOTIFICATION_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirm').show();");
	}
	
	public void confirmModifyPayerAgentDialog(){
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.modify.contact.agent.payment",paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogConfirmModifyContact').show();");
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo de notificacion de usuario/contacto agente pagador confirmado correctamente
	 * **/
	public void confirmPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected, StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);

		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_NOTIFICATION_CONFIRM_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirmYes').show();");
    	
    	/**Notificacion de confirmacion de agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess = paymentAgentServiceBean.find(BusinessProcess.class, BusinessProcessType.AGENT_PAYMENT_CREATION_CONFIRM.getCode());
		String agentPayment = paymentAgentSelected.getMnemonic() + " - " + paymentAgentSelected.getDescription();
		Object[] parameters = {agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
		
	}
	
	public void confirmModifyPayerAgentDialogYes(){
		agentFacade.confirmModifyContactAgent(paymentAgentSelected.getIdPaymentAgentPk(),viewPaymentAgent);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.successfully.modify.payment.agent",paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirmYes').show();");
    	
    	/**Notificacion de confirmacion de modificacion agente pagador*/
    	BusinessProcess  businessProcess = new BusinessProcess();
		businessProcess = paymentAgentServiceBean.find(BusinessProcess.class, BusinessProcessType.AGENT_PAYMENT_MODIFY_CONFIRM.getCode());
		String agentPayment = paymentAgentSelected.getMnemonic() + " - " + paymentAgentSelected.getDescription();
		Object[] parameters = {agentPayment};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
		
	}

	/**
	 * Usado en la pantalla de detalle
	 * Dialogo de rechazo de usuario/contaco agente pagador
	 * 
	 * */
	public void rejectPayerAgentDialog(){
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgRejectState').show();");
	}
	/**
	 * Rechazar la modificacion
	 * */
	public void rejectModifyPayerAgentDialog(){
		String bodyMessage = PropertiesUtilities.getMessage("alert.reject.confim.modify.payment.agent",paymentAgentSelected.getMnemonic());//paymentAgentSelected
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvRejectModifyState').show();");
	}
	
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo de notificacion de usuario/contacto rechazado correctamente
	 * */
	public void rejectPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.REJECTED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		JSFUtilities.updateComponent("fldResult");
		
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT_NOTIFICATION_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogRejectYes').show();");
    	
	}
	/**
	 * rechazar la modificacion
	 * */
	public void rejectModifyPayerAgentDialogYes(){
		agentFacade.rejecyModifyContactAgent(paymentAgentSelected);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.reject.modify.successfully.payment.agent",paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogRejectYes').show();");
	}
	/** Usado en la pantalla de busqueda
	 *  Verifica el estado del usuario/contacto agente pagador
	 *  Se abre pantalla de detalle en caso de tener los estados correctosS
	 * 	@return paymentAgentLoadInfo
	 * 
	 * **/
	public String beforeBlocking(){
		if(isSelectedPaymentAgent()){
			if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
					paymentAgentSelected.getSituation().equals(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT)){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(false);
				viewPaymentAgent.setRenderedBlock(true);
				viewPaymentAgent.setRenderedUnBlock(false);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(false);
				return "paymentAgentLoadInfo";
			}else{
				/*Privilegio de confirmar bloqueo*/
				if(userPrivilege.getUserAcctions().isConfirmBlock()){
					if(paymentAgentSelected.getState().equals(StatesConstants.CONFIRMED_PAYER_AGENT)&&
							paymentAgentSelected.getSituation().equals(SituationConstants.PENDING_LOCK_CONTACT_PAYMENT)){
						String headerMessageView = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
						String bodyMessageView = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_LOOK_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
						showMessageOnDialogPayment(headerMessageView,bodyMessageView); 
						JSFUtilities.updateComponent("opnlDialogs");
				    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgBlockStateConfirm').show();");
					}else{
						String msg = PropertiesUtilities.getMessage("lbl.alert.no.state.no.situation");
						showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
						JSFUtilities.showSimpleValidationDialog();
					}
				}
				else{//No tiene el permiso
					String msg = PropertiesUtilities.getMessage("lbl.not.privilege");
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return null;
	}
	
	/** Metodo usado en la pantalla de detalle
	 *  Registro de un desbloqueo de usuario/contacto agente pagador
	 * 	@return paymentAgentLoadInfo
	 * 
	 * **/
	public String beforeUnBlocking(){
		if(isSelectedPaymentAgent()){
			if(paymentAgentSelected.getState().equals(StatesConstants.BLOCKED_PAYER_AGENT)&&
					paymentAgentSelected.getSituation().equals(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT)){
				loadDetailView();
				viewPaymentAgent.setRenderedRejected(false);
				viewPaymentAgent.setRenderedBlock(false);
				viewPaymentAgent.setRenderedUnBlock(true);
				viewPaymentAgent.setRenderdModify(false);
				viewPaymentAgent.setRenderedConfirm(false);
				return "paymentAgentLoadInfo";
			}else{
				/*Privelegio de confirmar desbloqueo*/
				if(userPrivilege.getUserAcctions().isConfirmUnblock()){
				if(paymentAgentSelected.getState().equals(StatesConstants.BLOCKED_PAYER_AGENT)&&
						paymentAgentSelected.getSituation().equals(SituationConstants.UNLOCKING_PENDING_CONTACT_PAYMENT)){
					String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.unlook.payment.agent.confirm",paymentAgentSelected.getMnemonic());
					showMessageOnDialogPayment(headerMessage,bodyMessage); 
					JSFUtilities.updateComponent("opnlDialogs");
			    	JSFUtilities.executeJavascriptFunction("PF('cnfDlgUnBlockStateConfirm').show();");
				}else{
					String msg = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NOTIFY_CONTACT_NO_ENABLE);
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
				}else{
					String msg = PropertiesUtilities.getMessage("lbl.not.privilege");
					showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return "";
	}
	
	
	//Registrando el bloqueo del agente pagador
	public String blockPayerAgentDialogYes(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.CONFIRMED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.PENDING_LOCK_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_REGISTRE_BLOCK_AGENT_PAYMENT,paymentAgentSelected.getMnemonic());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogPayment(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogBlockYes').show();");
    	return "";
	}
	public boolean isSelectedPaymentAgent(){
		if(paymentAgentSelected==null){
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED);
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}else
			return true;
	}
	private boolean isRegisteredState(){
		if(paymentAgentSelected.getState().equals(StatesConstants.REGISTERED_PAYER_AGENT)){
			return true;
		}else{
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_STATE_REGISTERED);
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}
	}
	private void showMessageOnDialogPayment(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	/**
	 * Usado en la pantalla de modificacion de usuario/contacto agente pagador
	 * Verificamos los cambios antes de registrar la modificacion
	 * */
	public void preModifyPaymetAgent(){
		if(isModifiedPayment()){
			if(verifyMnemonic()){
				JSFUtilities.executeJavascriptFunction("PF('wVconfirmModifyPaymet').show();");
			}
		}else{
			String message = PropertiesUtilities.getMessage("lbl.modification.was.made.verify");
			showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),message);
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	public void executeSaveModify(){
		agentFacade.preSaveMofifyPaymetAgent(agentModifiedTO,paymentAgentSelected);
		String bodyMessageView = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REGISTERED_MODIFICATION_PAYMENT_AGENT,paymentAgentSelected.getMnemonic());
		agentModifiedTO= new PayerAgentRegistreTO();
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVModifySuccessfullPaymet').show();");
        showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), bodyMessageView);
	}
	public String modifySuccessfullPaymet(){
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		JSFUtilities.updateComponent("frmModifyPaymentAgent");
		return "backPageModifyPagePaymentAgent";
	}
	
	/**
	 * Usado en la pantalla de modificacion
	 * Verificamos si el usuario/contacto agente pagador fue modificado
	 * */
	private boolean isModifiedPayment(){
		if(!agentModifiedTO.getParameterTablePk().equals(paymentAgentSelected.getTypePaymentAgent()))
			return true;
		if(!agentModifiedTO.getDescription().equalsIgnoreCase(paymentAgentSelected.getDescription()))
			return true;
		if(!agentModifiedTO.getMnemonic().equalsIgnoreCase(paymentAgentSelected.getMnemonic()))
			return true;
		if(!agentModifiedTO.getEmail().equalsIgnoreCase(paymentAgentSelected.getEmail()))
			return true;
		if(!agentModifiedTO.getCellPhone().equalsIgnoreCase(paymentAgentSelected.getCellPhone()))
			return true;
		return true;
	}
	/**
	 * Usado en la pantalla de registro
	 * Dialo para regresar a la pantalla de bsuqueda
	 * */
	public String backModifyPaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogPayment(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVBackModifyScreen').show();");
	    	return null;
		}
		else
			return "backPageModifyPagePaymentAgent";
	}
	
	/**
	 * Rechzando el desbloqueo*/
	public void blockPayerAgentDialogConfirmReject(){
		agentFacade.changeStatePayerAgent(paymentAgentSelected,StatesConstants.BLOCKED_PAYER_AGENT);
		agentFacade.changeSituationPayerAgent(paymentAgentSelected,SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		listPaymentAgent = agentFacade.searchPaymentAgent(searchPaymentAgent);
		
		String message = PropertiesUtilities.getMessage("label.alert.reject.block.contact.payment");
		showMessageOnDialogPayment(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),message);
		JSFUtilities.showSimpleValidationDialog();
	}
	
	public PayerAgentRegistreTO getAgentRegistreTO() {
		return searchPaymentAgent;
	}
	public List<ParameterTable> getListTypePayingAgent() {
		return listTypePayingAgent;
	}
	public List<ParameterTable> getListStatePayingAgent() {
		return listStatePayingAgent;
	}
	
	public PaymentAgent getPaymentAgentSelected() {
		return paymentAgentSelected;
	}
	public void setPaymentAgentSelected(PaymentAgent paymentAgentSelected) {
		this.paymentAgentSelected = paymentAgentSelected;
	}

	public PayerAgentRegistreTO getSearchPaymentAgent() {
		return searchPaymentAgent;
	}

	public PayerAgentRegistreTO getViewPaymentAgent() {
		return viewPaymentAgent;
	}

	public List<ParameterTable> getListSituationPayingAgent() {
		return listSituationPayingAgent;
	}
	public PayerAgentRegistreTO getAgentModifiedTO() {
		return agentModifiedTO;
	}
	public void setAgentModifiedTO(PayerAgentRegistreTO agentModifiedTO) {
		this.agentModifiedTO = agentModifiedTO;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public void setSearchPaymentAgent(PayerAgentRegistreTO searchPaymentAgent) {
		this.searchPaymentAgent = searchPaymentAgent;
	}
	public void setViewPaymentAgent(PayerAgentRegistreTO viewPaymentAgent) {
		this.viewPaymentAgent = viewPaymentAgent;
	}
	public void setListTypePayingAgent(List<ParameterTable> listTypePayingAgent) {
		this.listTypePayingAgent = listTypePayingAgent;
	}
	public void setListStatePayingAgent(List<ParameterTable> listStatePayingAgent) {
		this.listStatePayingAgent = listStatePayingAgent;
	}
	public void setListSituationPayingAgent(
			List<ParameterTable> listSituationPayingAgent) {
		this.listSituationPayingAgent = listSituationPayingAgent;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
	public GenericDataModel<PaymentAgent> getlistPaymentAgent() {
		return listPaymentAgent;
	}
	public void setlistPaymentAgent(
			GenericDataModel<PaymentAgent> listPaymentAgent) {
		this.listPaymentAgent = listPaymentAgent;
	}
}
