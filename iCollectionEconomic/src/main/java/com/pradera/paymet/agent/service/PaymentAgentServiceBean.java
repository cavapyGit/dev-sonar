package com.pradera.paymet.agent.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.collection.right.util.SituationConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;
import com.edv.collection.economic.rigth.PaymentAgent;
import com.edv.collection.economic.rigth.ReqModifyPaymentAgent;

@Stateless
@Performance
public class PaymentAgentServiceBean extends CrudDaoServiceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9026385936786338949L;
	
	@Inject
	private UserInfo userInfo;

	public PaymentAgentServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getListEconomicActivities(){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT DISTINCT PT.PARAMETER_TABLE_PK, PT.DESCRIPTION ");
		builder.append(" FROM PARAMETER_TABLE PT ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND PT.PARAMETER_TABLE_PK IN ( SELECT DISTINCT(TYPE_PAYMENT_AGENT) FROM PAYMENT_AGENT WHERE 1 = 1 AND ID_PAYMENT_AGENT_FK IS NULL )");
		builder.append(" ORDER BY PT.PARAMETER_TABLE_PK ASC ");
		
		Query query = em.createNativeQuery(builder.toString());
		
		List<Object[]> list = query.getResultList();
		List<ParameterTable> listParameterTables = new ArrayList<>();
		ParameterTable parameterTable;
		for (Object[] objects : list) {
			parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk(Integer.parseInt(objects[0].toString()));
			parameterTable.setDescription(objects[1].toString());
			listParameterTables.add(parameterTable);
		}
		return listParameterTables;
	}
	
	/**
	 * Buscamos contactos agente pagador distintos de rechazado
	 * @param agentRegistreTO 
	 * @return {@link List} lista de contactos agente pagador*/
	public List<PaymentAgent> searchContactPaymentAgentReject(PayerAgentRegistreTO agentRegistreTO){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p from PaymentAgent p ");
		builder.append(" where 1 = 1 ");
		builder.append(" and p.idPaymentAgentFk is not null ");
		builder.append(" and p.idPaymentAgentFk = :idPaymentAgentFk ");
		builder.append(" and p.state = :state ");
		builder.append(" and p.situation = :situation ");
		builder.append(" and p.email = :email ");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("idPaymentAgentFk", agentRegistreTO.getIdPaymentAgentPk());
		query.setParameter("state", StatesConstants.CONFIRMED_PAYER_AGENT);
		query.setParameter("situation", SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		query.setParameter("email", agentRegistreTO.getEmail());
		
		@SuppressWarnings("unchecked")
		List<PaymentAgent> list = query.getResultList();
		return list;
	}
	/**
	 * Buscamos agente pagador distintos de rechazado
	 * @param agentRegistreTO 
	 * @return {@link List} lista de contactos agente pagador*/
	public List<PaymentAgent> searchPaymentAgentReject(PayerAgentRegistreTO agentRegistreTO){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p from PaymentAgent p ");
		builder.append(" where 1 = 1 ");
		builder.append(" and p.idPaymentAgentFk is null ");
		builder.append(" and p.state = :state ");
		builder.append(" and p.mnemonic = :mnemonic ");
		builder.append(" and p.email = :email ");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("state", StatesConstants.CONFIRMED_PAYER_AGENT);
		query.setParameter("mnemonic", agentRegistreTO.getMnemonic());
		query.setParameter("email", agentRegistreTO.getEmail());
		
		@SuppressWarnings("unchecked")
		List<PaymentAgent> list = query.getResultList();
		return list;
	}
	/**
	 * Busqueda de Contactos agente pagador
	 * @param paymentSearch Filtros de busqueda*/
	@SuppressWarnings("unchecked")
	public GenericDataModel<PaymentAgent> searchPaymentAgentsContact(PayerAgentRegistreTO paymentSearch){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p from PaymentAgent p ");
		builder.append(" where 1 = 1 ");
		builder.append(" and p.idPaymentAgentFk is not null ");
		if(paymentSearch.getParameterTablePk()!=null)//tipo de agente
			builder.append(" and p.typePaymentAgent = "+paymentSearch.getParameterTablePk());
		//Agente pagador
		if(paymentSearch.getIdPaymentAgentPk()!=null)
			builder.append(" and p.idPaymentAgentFk = "+paymentSearch.getIdPaymentAgentPk());
		
		if(paymentSearch.getName()!=null)
			builder.append(" and p.name like '%"+paymentSearch.getName()+"%'");
		
		if(paymentSearch.getFirstName()!=null)
			builder.append(" and p.firstName like '%"+paymentSearch.getFirstName()+"%'");
		
		if(paymentSearch.getLastName()!=null)
			builder.append(" and p.lastName like '%"+paymentSearch.getLastName()+"%'");
		
		if(paymentSearch.getEmail()!=null)
			builder.append(" and p.email = '"+paymentSearch.getEmail()+"'");
		
		if(paymentSearch.getCellPhone()!=null)
			builder.append(" and p.cellPhone = '"+paymentSearch.getCellPhone()+"'");
		
		if(paymentSearch.getPhone()!=null)
			builder.append(" and p.phone = '"+paymentSearch.getPhone()+"'");
		
		if(paymentSearch.getInternPhone()!=null)
			builder.append(" and p.intern = '"+paymentSearch.getInternPhone()+"'");
		
		if(paymentSearch.getJob()!=null)
			builder.append(" and p.job like  '%"+paymentSearch.getJob()+"%'");
		
		if(paymentSearch.getState()!=null)
			builder.append(" and p.state = "+paymentSearch.getState());
		
		if(paymentSearch.getSecurityClass()!=null&&paymentSearch.getSecurityClass()>0)
			builder.append(" and p.securityClassList like '%"+paymentSearch.getSecurityClass()+"%' ");
		
		Query query = em.createQuery(builder.toString());
		List<PaymentAgent> agents = query.getResultList();
		return new GenericDataModel<>(agents);
	}
	
	/**
	 * Busqueda de Agentes Pagadores
	 * @param paymentSearch Filtros de busqueda*/
	@SuppressWarnings("unchecked")
	public GenericDataModel<PaymentAgent> searchPaymentAgents(PayerAgentRegistreTO paymentSearch){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p from PaymentAgent p ");
		builder.append(" where 1 = 1 ");
		builder.append(" and p.idPaymentAgentFk is null ");
		//tipo de agente
		if(paymentSearch.getParameterTablePk() != null)
			builder.append(" and p.typePaymentAgent = "+paymentSearch.getParameterTablePk());
		
		if(paymentSearch.getDescription()!=null)
			builder.append(" and p.description like upper('%"+paymentSearch.getDescription()+"%')");
		
		if(paymentSearch.getMnemonic()!=null)
			builder.append(" and p.mnemonic like upper('%"+paymentSearch.getMnemonic()+"%')");
		
		if(paymentSearch.getState()!=null)
			builder.append(" and p.state = "+paymentSearch.getState());
		
		builder.append(" order by p.registreDate desc, p.mnemonic asc ");
		
		Query query = em.createQuery(builder.toString());
		List<PaymentAgent> agents = query.getResultList();
		return new GenericDataModel<>(agents);
	}
	
	/**
	 * Buscamos la ultima modificacion realizada al contacto agente pagador
	 * @param idPaymentAgentPk id del contacto agente pagador a modificar
	 * @return id de la solicitud de modificacion
	 * */
	public Long searchLastModificationContactPaymentPk(Long idPaymentAgentPk){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" Select max(empa.idReqModPayAgentPk) from ReqModifyPaymentAgent empa ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and empa.idPaymentAgentFk = :idPaymentAgentFk ");
		queryBuilder.append(" and empa.state is null ");
		queryBuilder.append(" and empa.situation =  :situation ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idPaymentAgentFk", idPaymentAgentPk);
		query.setParameter("situation", SituationConstants.PENDING_TO_MODIFY_CONTACT_PAYMET);
		Long pk = (Long) query.getSingleResult();
		return pk; 
	}
	
	/**
	 * Busqueda de agente pagador por mnemonico
	 * @param mnemonic 
	 * @return {@link List} lista de agentes pagadores
	 * */
	public List<PaymentAgent> searchPaymentAgentByMnemonic(String mnemonic, Integer state){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p from PaymentAgent p ");
		builder.append(" where 1 = 1 ");
		builder.append(" and p.idPaymentAgentFk is null ");
		builder.append(" and p.mnemonic like upper(:mnemonic) ");
		builder.append(" and p.state = :state ");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("mnemonic", mnemonic);
		query.setParameter("state", state);
		
		@SuppressWarnings("unchecked")
		List<PaymentAgent> list = query.getResultList();
		return list;
	}
	
	/**
	 * Busqueda de agente pagador por mnemonico e id de agente pagador
	 * @param mnemonic 
	 * @return {@link List} lista de agentes pagadores
	 * */
	public List<PaymentAgent> searchPaymentAgentByMnemonicAndId(String mnemonic, Integer state, Long idPaymentAgent){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p from PaymentAgent p ");
		builder.append(" where 1 = 1 ");
		builder.append(" and p.idPaymentAgentFk is null ");
		builder.append(" and p.mnemonic like upper(:mnemonic) ");
		builder.append(" and p.state = :state ");
		builder.append(" and p.idPaymentAgentPk != :idPaymentAgent ");
		Query query = em.createQuery(builder.toString());
		query.setParameter("mnemonic", mnemonic);
		query.setParameter("state", state);
		query.setParameter("idPaymentAgent", idPaymentAgent);
		
		@SuppressWarnings("unchecked")
		List<PaymentAgent> list = query.getResultList();
		return list;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void savePaymetAgent(PaymentAgent paymentAgent){
		em.persist(paymentAgent);
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveRequestModifyPaymetAgent(ReqModifyPaymentAgent reqModifyPaymentAgent,PaymentAgent paymentAgent){
		paymentAgent.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		paymentAgent.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		paymentAgent.setLastModifyDate(CommonsUtilities.currentDate());
		
		em.persist(reqModifyPaymentAgent);
		
		try {
			update(paymentAgent);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
