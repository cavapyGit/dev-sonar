package com.pradera.paymet.agent.contact.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.paymet.agent.facade.PaymetAgentFacade;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;
import com.edv.collection.economic.rigth.PaymentAgent;

@DepositaryWebBean
@LoggerCreateBean
public class ModifyPaymentAgentContactMgmtBean extends GenericBaseBean{

	private static final long serialVersionUID = 1L;
	
	private PayerAgentRegistreTO agentRegistreTO;
	
	private List<ParameterTable>  listTypePayingAgent;
	
	private List<PaymentAgent> listPaymentAgents;
	
	@Inject
	private PaymetAgentFacade agentFacade;
	
	@Inject
	private CollectionRigthFacade collectionRigthFacade; 
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	private List<ParameterTable> listSecurityClass;

	@PostConstruct
	public void init(){
		agentRegistreTO = new PayerAgentRegistreTO();
		listTypePayingAgent = agentFacade.getListEconomicActivities();
		listSecurityClass = collectionRigthFacade.getSecurityClassListAll();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
	}
	public void changeTypePaymentAgent(){
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
	}
	
	public void preSavePaymetAgent(){
		if(agentRegistreTO.getListSecurityClassSelected().size()<1){
			String message = "Debe seleccionar al menos una clase de valor";
	        showMessageOnDialogModify(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
		    JSFUtilities.showSimpleValidationDialog();
		}else{
			JSFUtilities.executeJavascriptFunction("wVconfirmSavePaymet.show()");		
		}
	}
	@LoggerAuditWeb
	public void executeSave(){
		agentFacade.preSavePaymetAgentContact(agentRegistreTO);
		String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SUCCESFULL_REGISTRE_AGENT);
		agentRegistreTO= new PayerAgentRegistreTO();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
		JSFUtilities.updateComponent("frmRegisterPaymentAgent");
        showMessageOnDialogModify(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
	    JSFUtilities.showSimpleValidationDialog();
	}
	
	private boolean isModifiedPayment(){
		if(agentRegistreTO.getParameterTablePk()!=45L)
			return true;
		//if(agentRegistreTO.getIdHolderPk()!=null)
		//	return true;
		if(agentRegistreTO.getName()!=null)
			return true;
		if(agentRegistreTO.getFirstName()!=null)
			return true;
		if(agentRegistreTO.getLastName()!=null)
			return true;
		if(agentRegistreTO.getEmail()!=null)
			return true;
		if(agentRegistreTO.getCellPhone()!=null)
			return true;
		if(agentRegistreTO.getPhone()!=null)
			return true;
		if(agentRegistreTO.getInternPhone()!=null)
			return true;
		if(agentRegistreTO.getJob()!=null)
			return true;
		if(agentRegistreTO.getListSecurityClassSelected().size()>0)
			return true;
		else 
			return false;
	}
	/**
	 * Usado en la pantalla de registro de agente pagador
	 * Funcion limipar pantalla
	 * @return null
	 * */
	public String cleanRegistrePaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			showMessageOnDialogModify(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVCleanRegistreScreen').show();");
		}
		return null;
	}
	/**
	 * Usado en la pantalla de registro
	 * Limpiamos la pantalla de registro
	 * */
	public void exeCleanRegistrePaymentScreen(){
		agentRegistreTO= new PayerAgentRegistreTO();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
		JSFUtilities.updateComponent("frmRegisterPaymentAgent");
	}
	
	/**
	 * Usado en la pantalla de registro
	 * Dialo para regresar a la pantalla de bsuqueda
	 * */
	public String backRegistrePaymentScreen(){
		if(isModifiedPayment()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogModify(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVBackRegistreScreen').show();");
	    	return null;
		}
		else
			return "backPageSearchPaymentAgentContact";
	}
	private void showMessageOnDialogModify(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	public String exeBackRegistrePaymentScreen(){
		agentRegistreTO= new PayerAgentRegistreTO();
		listPaymentAgents = parameterServiceBean.getListPaymentAgentContacsByEcomicActivity(agentRegistreTO.getParameterTablePk());
		return "backPageSearchPaymentAgentContact";
	}
	public PayerAgentRegistreTO getAgentRegistreTO() {
		return agentRegistreTO;
	}
	public void setAgentRegistreTO(PayerAgentRegistreTO agentRegistreTO) {
		this.agentRegistreTO = agentRegistreTO;
	}
	public List<ParameterTable> getListTypePayingAgent() {
		return listTypePayingAgent;
	}
	public void setListTypePayingAgent(List<ParameterTable> listTypePayingAgent) {
		this.listTypePayingAgent = listTypePayingAgent;
	}
	
	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}
	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}
	public List<PaymentAgent> getListPaymentAgents() {
		return listPaymentAgents;
	}
	public void setListPaymentAgents(List<PaymentAgent> listPaymentAgents) {
		this.listPaymentAgents = listPaymentAgents;
	}
}
