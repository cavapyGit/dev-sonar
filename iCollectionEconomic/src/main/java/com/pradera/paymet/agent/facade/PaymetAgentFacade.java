package com.pradera.paymet.agent.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.collection.right.util.SituationConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.paymet.agent.service.PaymentAgentServiceBean;
import com.pradera.paymet.agent.to.PayerAgentRegistreTO;
import com.edv.collection.economic.rigth.PaymentAgent;
import com.edv.collection.economic.rigth.ReqModifyPaymentAgent;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class PaymetAgentFacade {

	@Inject
	private PaymentAgentServiceBean  bean;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private PraderaLogger log;
	
	private static final Integer PAYMENT_LIP=Integer.valueOf(1);
	private static final Integer PAYMENT_NO_LIP=Integer.valueOf(0);
	
	public PaymetAgentFacade() {
	}
	

	public List<ParameterTable> getListStatePayingAgent(){
		try {
			return parameterServiceBean.getListParameterTableServiceBean(1180);
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ParameterTable> getListStatePaymentAgent(){
		try {
			List<ParameterTable> newList = new ArrayList<ParameterTable>();
			for(ParameterTable parameter: parameterServiceBean.getListParameterTableServiceBean(1180)){
				if(parameter.getParameterTablePk().compareTo(StatesConstants.BLOCKED_PAYER_AGENT) != 0){
					newList.add(parameter);
				}
			}
			return newList;
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		}
	}
	public void changeSituationPayerAgent(PaymentAgent agent,Integer situation){
		agent.setSituation(situation);
		try {
			bean.update(agent);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Rechazo de la solicitud de modificacion
	 * */
	public void rejecyModifyContactAgent(PaymentAgent paymentAgentSelected){
		Long pk = bean.searchLastModificationContactPaymentPk(paymentAgentSelected.getIdPaymentAgentPk());
		ReqModifyPaymentAgent modfPaymet = bean.find(ReqModifyPaymentAgent.class, pk);
		PaymentAgent payment = bean.find(PaymentAgent.class, paymentAgentSelected.getIdPaymentAgentPk());
		
		payment.setState(StatesConstants.CONFIRMED_PAYER_AGENT);
		payment.setSituation(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		payment.setLastModifyDate(new Date());
		payment.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		
		modfPaymet.setState(StatesConstants.CONFIRMED_PAYER_AGENT);
		modfPaymet.setSituation(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		modfPaymet.setLastModifyDate(CommonsUtilities.currentDate());
		modfPaymet.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		try {
			bean.update(payment);
			bean.update(modfPaymet);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void confirmModifyContactAgent(Long idPaymentAgentPk,PayerAgentRegistreTO viewPaymentAgent){
		/*Buscamos la ultima modificacion del contacto agente pagador*/
		Long pk = bean.searchLastModificationContactPaymentPk(idPaymentAgentPk);
		ReqModifyPaymentAgent modfPaymet = bean.find(ReqModifyPaymentAgent.class, pk);
		if(pk == null){
			log.info("CDDE ::: No se encontro la solicitud de modificacion del registroS :"+idPaymentAgentPk);
			return;
		}
		PaymentAgent payment = bean.find(PaymentAgent.class, idPaymentAgentPk);
		
		payment.setName(viewPaymentAgent.getName());
		payment.setDescription(viewPaymentAgent.getDescription());
		payment.setMnemonic(viewPaymentAgent.getMnemonic());
		payment.setFirstName(viewPaymentAgent.getFirstName());
		payment.setLastName(viewPaymentAgent.getLastName());
		payment.setCellPhone(viewPaymentAgent.getCellPhone());
		payment.setPhone(viewPaymentAgent.getPhone());
		payment.setIntern(viewPaymentAgent.getInternPhone());
		payment.setJob(viewPaymentAgent.getJob());
		payment.setEmail(viewPaymentAgent.getEmail());
		if(payment.getIdPaymentAgentFk()!=null)
		payment.setIndSendUrlPayment(viewPaymentAgent.isIndSendUrlPayment()?BooleanType.YES.getCode():BooleanType.NO.getCode());
		if(payment.getIdPaymentAgentFk()!=null)
		payment.setIndLip(viewPaymentAgent.isPaymentWithLip()?PAYMENT_LIP:PAYMENT_NO_LIP);
		if(payment.getIdPaymentAgentFk()==null)
		payment.setTypePaymentAgent(viewPaymentAgent.getParameterTablePk());
		
		if(viewPaymentAgent.getListSecurityClassSelected() != null && viewPaymentAgent.getListSecurityClassSelected().size()>1){
			StringBuilder builder = new StringBuilder();
			for (ParameterTable parameterTable : viewPaymentAgent.getListSecurityClassSelected()){
				builder.append(parameterTable.getParameterTablePk().toString());
				builder.append(",");
			}
			String string = builder.toString().substring(0, builder.toString().length()-1);
			payment.setSecurityClassList(string);
		}
		
		payment.setState(StatesConstants.CONFIRMED_PAYER_AGENT);
		payment.setSituation(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		payment.setLastModifyDate(CommonsUtilities.currentDate());
		/**Encriptar y archivo*/
		if(payment.getIdPaymentAgentFk()!=null)
		payment.setEncryp(viewPaymentAgent.isEncryptMail()==true?BooleanType.YES.getCode():BooleanType.NO.getCode());
		payment.setCertificate(viewPaymentAgent.getCertificate());
		payment.setCertificateName(viewPaymentAgent.getCertificateName());
		
		modfPaymet.setState(StatesConstants.CONFIRMED_PAYER_AGENT);
		modfPaymet.setSituation(SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		modfPaymet.setLastModifyDate(CommonsUtilities.currentDate());
		modfPaymet.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			try {
				bean.update(payment);
				bean.flushTransaction();
			} catch (ServiceException e) {
				e.printStackTrace();
			} 
			try {
				bean.update(modfPaymet);
				bean.flushTransaction();
			} catch (ServiceException e) {
				e.printStackTrace();
			}
	}
	
	public void changeStatePayerAgent(PaymentAgent agent,Integer state){
		agent.setState(state);
		agent.setLastModifyApp(0);
		agent.setLastModifyDate(CommonsUtilities.currentDate());
		agent.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		agent.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		try {
			bean.update(agent);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	public List<ParameterTable> getListSituationPayingAgent(){
		try {
			return parameterServiceBean.getListParameterTableServiceBean(1181);
		} catch (ServiceException e) {
			return null;
		}
	}
	public List<ParameterTable> getListEconomicActivities(){
		return bean.getListEconomicActivities();
	}
	/**
	 * Busqueda de contactos agentes pagadores
	 * */
	public GenericDataModel<PaymentAgent> searchPaymentAgentContacts(PayerAgentRegistreTO paymentSearch){
		GenericDataModel<PaymentAgent> agentsContacts =  bean.searchPaymentAgentsContact(paymentSearch);
		ParameterTable parameterTable;
		for (PaymentAgent paymentAgentContact : agentsContacts) {
			parameterTable = new ParameterTable();
			paymentAgentContact.setPaymentAgentFullName(bean.find(PaymentAgent.class, paymentAgentContact.getIdPaymentAgentFk()).getDescription());
			parameterTable = bean.find(ParameterTable.class, paymentAgentContact.getState());
			paymentAgentContact.setStateDesc(parameterTable.getDescription());
			if(paymentAgentContact.getSituation()!=null){
				parameterTable = bean.find(ParameterTable.class, paymentAgentContact.getSituation());
				paymentAgentContact.setSituationDesc(parameterTable.getDescription());
			}
		}
		return agentsContacts;
	}
	/**
	 * Busqueda de contactos agentes pagadores
	 * */
	public GenericDataModel<PaymentAgent> searchPaymentAgent(PayerAgentRegistreTO paymentSearch){
		GenericDataModel<PaymentAgent> agentsContacts =  bean.searchPaymentAgents(paymentSearch);
		ParameterTable parameterTable;
		for (PaymentAgent paymentAgentContact : agentsContacts) {
			parameterTable = new ParameterTable();
			parameterTable = bean.find(ParameterTable.class, paymentAgentContact.getState());
			paymentAgentContact.setStateDesc(parameterTable.getDescription());
			if(paymentAgentContact.getSituation()!=null){
				parameterTable = bean.find(ParameterTable.class, paymentAgentContact.getSituation());
				paymentAgentContact.setSituationDesc(parameterTable.getDescription());
			}
		}
		return agentsContacts;
	}
	
	/**
	 * Busqueda de la ultima modificacion del contacto agente pagador
	 * 
	 * */
	
	public ReqModifyPaymentAgent searchLastModificationContactPayment(Long idPaymentAgentPk){
		return bean.find(ReqModifyPaymentAgent.class, bean.searchLastModificationContactPaymentPk(idPaymentAgentPk));
	}
	@ProcessAuditLogger(process=BusinessProcessType.AUDIT_PROCESS_LOG)
	public void preSavePaymetAgentContact(PayerAgentRegistreTO agentRegistreTO){
		StringBuilder builder = new StringBuilder();
		for (ParameterTable parameterTable : agentRegistreTO.getListSecurityClassSelected()){
			builder.append(parameterTable.getParameterTablePk().toString());
			builder.append(",");
		}
		String string = builder.toString().substring(0, builder.toString().length()-1);
		PaymentAgent paymet = new PaymentAgent();
		if(agentRegistreTO.getParameterTablePk()!=null)
			paymet.setTypePaymentAgent(agentRegistreTO.getParameterTablePk());
		if(agentRegistreTO.getIdPaymentAgentPk()!=null)
			paymet.setIdPaymentAgentFk(agentRegistreTO.getIdPaymentAgentPk());
		if(agentRegistreTO.getName()!=null&&agentRegistreTO.getName().length()>0)
			paymet.setName(agentRegistreTO.getName());
		if(agentRegistreTO.getFirstName()!=null&&agentRegistreTO.getFirstName().length()>0)
			paymet.setFirstName(agentRegistreTO.getFirstName());
		if(agentRegistreTO.getLastName()!=null&&agentRegistreTO.getLastName().length()>0)
			paymet.setLastName(agentRegistreTO.getLastName());
		if(agentRegistreTO.getCellPhone()!=null&&agentRegistreTO.getCellPhone().length()>0)
			paymet.setCellPhone(agentRegistreTO.getCellPhone());
		if(agentRegistreTO.getPhone()!=null&&agentRegistreTO.getPhone().length()>0)
			paymet.setPhone(agentRegistreTO.getPhone());
		if(agentRegistreTO.getInternPhone()!=null&&agentRegistreTO.getInternPhone().length()>0)
			paymet.setIntern(agentRegistreTO.getInternPhone());
		if(agentRegistreTO.getEmail()!=null)
			paymet.setEmail(agentRegistreTO.getEmail());
		if(agentRegistreTO.getJob()!=null&&agentRegistreTO.getJob().length()>0)
			paymet.setJob(agentRegistreTO.getJob());
		if(agentRegistreTO.getListSecurityClassSelected().size()>0)
			paymet.setSecurityClassList(string);
		
		if(agentRegistreTO.isEncryptMail())
			paymet.setEncryp(BooleanType.YES.getCode());
		
		/**Certificados para cifrar email*/
		if(agentRegistreTO.getCertificate()!=null&agentRegistreTO.getCertificateName()!=null){
			paymet.setCertificateName(agentRegistreTO.getCertificateName());
			paymet.setCertificate(agentRegistreTO.getCertificate());
		}
		
		paymet.setIndLip(PAYMENT_NO_LIP);
		if(agentRegistreTO.isPaymentWithLip())
			paymet.setIndLip(PAYMENT_LIP);
		
		if(agentRegistreTO.isIndSendUrlPayment())
			paymet.setIndSendUrlPayment(BooleanType.YES.getCode());
		else
			paymet.setIndSendUrlPayment(BooleanType.NO.getCode());
		
		paymet.setRegistreDate(CommonsUtilities.currentDate());
		paymet.setState(StatesConstants.REGISTERED_PAYER_AGENT);
		paymet.setLastModifyApp(0);
		paymet.setLastModifyDate(CommonsUtilities.currentDate());
		paymet.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
	    paymet.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		bean.savePaymetAgent(paymet);
	}
	
	/**
	 * 
	 * @param agentRegistreTO
	 */
	@ProcessAuditLogger(process=BusinessProcessType.AUDIT_PROCESS_LOG)
	public void preSavePaymetAgent(PayerAgentRegistreTO agentRegistreTO){
		PaymentAgent paymet = new PaymentAgent();
		if(agentRegistreTO.getParameterTablePk()!=null)
			paymet.setTypePaymentAgent(agentRegistreTO.getParameterTablePk());
		if(agentRegistreTO.getDescription()!=null&&agentRegistreTO.getDescription().length()>0)
			paymet.setDescription(agentRegistreTO.getDescription());
		if(agentRegistreTO.getMnemonic()!=null&&agentRegistreTO.getMnemonic().length()>0)
			paymet.setMnemonic(agentRegistreTO.getMnemonic());
		if(agentRegistreTO.getCellPhone()!=null&&agentRegistreTO.getCellPhone().length()>0)
			paymet.setCellPhone(agentRegistreTO.getCellPhone());
		if(agentRegistreTO.getEmail()!=null)
			paymet.setEmail(agentRegistreTO.getEmail());
		
		paymet.setRegistreDate(CommonsUtilities.currentDate());
		paymet.setState(StatesConstants.REGISTERED_PAYER_AGENT);
		paymet.setLastModifyApp(0);
		paymet.setLastModifyDate(CommonsUtilities.currentDate());
		paymet.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
	    paymet.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		bean.savePaymetAgent(paymet);
	}
	
	/**
	 * Buscamos contactos agentes pagador distintos de rechazado
	 * @see PayerAgentRegistreTO Parametro
	 * @param agentRegistreTO 
	 * @return {@link Boolean} 
	 * */
	public boolean existsContactPaymentAgent(PayerAgentRegistreTO agentRegistreTO){
		List<PaymentAgent> list = bean.searchContactPaymentAgentReject(agentRegistreTO);
		
		if(list.size()>0)
			return true;
		else
			return false;
	}
	/**
	 * Buscamos agentes pagador distintos de rechazado
	 * @see PayerAgentRegistreTO Parametro
	 * @param agentRegistreTO 
	 * @return {@link Boolean} 
	 * */
	public boolean existsPaymentAgent(PayerAgentRegistreTO agentRegistreTO){
		List<PaymentAgent> list = bean.searchPaymentAgentReject(agentRegistreTO);
		
		if(list.size()>0)
			return true;
		else
			return false;
	}
	/**
	 * Confirmamos la solictud de modificacion
	 * @agentRegistreTO Objeto con los nuevos datos de modificacion
	 * @paymentAgent contacto agente pagador que se va a modificar*/
	@ProcessAuditLogger(process=BusinessProcessType.AUDIT_PROCESS_LOG)
	@LoggerAuditWeb
	public void preSaveMofifyPaymetAgent(PayerAgentRegistreTO agentRegistreTO,PaymentAgent paymentAgent){
		
		ReqModifyPaymentAgent paymetModify = new ReqModifyPaymentAgent();
		
		if (agentRegistreTO.getListSecurityClassSelected() != null && agentRegistreTO.getListSecurityClassSelected().size()>0){
			StringBuilder builder = new StringBuilder();
			for (ParameterTable parameterTable : agentRegistreTO.getListSecurityClassSelected()){
				builder.append(parameterTable.getParameterTablePk().toString());
				builder.append(",");
			}
			String string = builder.toString().substring(0, builder.toString().length()-1);
			paymetModify.setSecurityClassList(string);
		}
		
		if(paymentAgent.getIdPaymentAgentPk()!=null)
			paymetModify.setIdPaymentAgentFk(paymentAgent.getIdPaymentAgentPk());
		if(agentRegistreTO.getParameterTablePk()!=null)
			paymetModify.setTypePaymentAgent(agentRegistreTO.getParameterTablePk());
		if(agentRegistreTO.getName()!=null&&agentRegistreTO.getName().length()>0)
			paymetModify.setName(agentRegistreTO.getName());
		if(agentRegistreTO.getFirstName()!=null&&agentRegistreTO.getFirstName().length()>0)
			paymetModify.setFirstName(agentRegistreTO.getFirstName());
		if(agentRegistreTO.getLastName()!=null&&agentRegistreTO.getLastName().length()>0)
			paymetModify.setLastName(agentRegistreTO.getLastName());
		if(agentRegistreTO.getCellPhone()!=null&&agentRegistreTO.getCellPhone().length()>0)
			paymetModify.setCellPhone(agentRegistreTO.getCellPhone());
		if(agentRegistreTO.getPhone()!=null&&agentRegistreTO.getPhone().length()>0)
			paymetModify.setPhone(agentRegistreTO.getPhone());
		if(agentRegistreTO.getInternPhone()!=null&&agentRegistreTO.getInternPhone().length()>0)
			paymetModify.setIntern(agentRegistreTO.getInternPhone());
		if(agentRegistreTO.getEmail()!=null)
			paymetModify.setEmail(agentRegistreTO.getEmail());
		if(agentRegistreTO.getJob()!=null&&agentRegistreTO.getJob().length()>0)
			paymetModify.setJob(agentRegistreTO.getJob());
		if(agentRegistreTO.getDescription()!=null&&agentRegistreTO.getDescription().length()>0)
			paymetModify.setDescription(agentRegistreTO.getDescription()+"");
		if(agentRegistreTO.getMnemonic()!=null&&agentRegistreTO.getMnemonic().length()>0)
			paymetModify.setMnemonic(agentRegistreTO.getMnemonic()+"");
		
		paymetModify.setIndLip(PAYMENT_NO_LIP);
		if(agentRegistreTO.isPaymentWithLip())
			paymetModify.setIndLip(PAYMENT_LIP);
		
		if(agentRegistreTO.isIndSendUrlPayment())
			paymetModify.setIndSendUrlPayment(BooleanType.YES.getCode());
		else
			paymetModify.setIndSendUrlPayment(BooleanType.NO.getCode());
		
		if(agentRegistreTO.getCertificate()!=null){
			paymetModify.setCertificate(agentRegistreTO.getCertificate());
			paymetModify.setCertificateName(agentRegistreTO.getCertificateName());
		}else{
			paymetModify.setCertificate(null);
			paymetModify.setCertificateName(null);
		}
		paymetModify.setRegistreDate(CommonsUtilities.currentDate());
		paymetModify.setSituation(SituationConstants.PENDING_TO_MODIFY_CONTACT_PAYMET);//2377L);//Pendiente de modificar
		paymetModify.setLastModifyApp(28338);
		paymetModify.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		paymetModify.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		paymetModify.setLastModifyDate(CommonsUtilities.currentDate());
		
		paymetModify.setEncryp(agentRegistreTO.isEncryptMail()==true?BooleanType.YES.getCode():BooleanType.NO.getCode());
		//al confirmar no se guarda el ecnrypt
		paymentAgent.setSituation(SituationConstants.PENDING_TO_MODIFY_CONTACT_PAYMET); //Pendiente de modificar
		bean.saveRequestModifyPaymetAgent(paymetModify,paymentAgent);
	}
	
	/**
	 * Busqueda de Agentes Pagadores por mnemonico
	 * @param mnemonic 
	 * @return {@link Boolean} 
	 * */
	public boolean existsPaymentAgentMnemonic(String mnemonic, Integer state){
		List<PaymentAgent> list = bean.searchPaymentAgentByMnemonic(mnemonic, state);
		if(list.size()>0)
			return true;
		else
			return false;
	}
	
	/**
	 * Busqueda de Agentes Pagadores por mnemonico e id
	 * @param mnemonic 
	 * @return {@link Boolean} 
	 * */
	public boolean existsPaymentAgentMnemonicAndId(String mnemonic, Integer state, Long idPaymentAgent){
		List<PaymentAgent> list = bean.searchPaymentAgentByMnemonicAndId(mnemonic, state, idPaymentAgent);
		if(list.size()>0)
			return true;
		else
			return false;
	}
}
