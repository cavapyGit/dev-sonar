package com.pradera.alert.facade;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.alert.service.InstitutionCashAccountService;
import com.pradera.alert.service.ManageBankAccountsService;
import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.ObjectFunCollectionTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
//import com.pradera.core.component.collectionoperations.CollectionProcessServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsConsultRegisterTO;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.constant.FundsAutomaticConstants;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.edv.collection.economic.rigth.DetailCollectionGloss;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageLipServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageLipServiceFacade {
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The manage bank accounts service. */
	@Inject
	ManageBankAccountsService manageBankAccountsService;
	
	
	/** The participant service bean. */
	@Inject
	ParticipantServiceBean participantServiceBean;	
	
	/** The mnemonic participant edb. */
	@Inject 
	@Configurable 
	String mnemonicParticipantEDB;
	
	/**Numero de cuenta en la BCB 1424*/
	@Inject 
	@Configurable 
	String accountNumberCollection;
	
	/** The account number lip. */
	@Inject 
	@Configurable 
	String accountNumberLip;
	
	/** The manage cash account service. */
	@Inject
	private InstitutionCashAccountService manageCashAccountService;
	
	@Inject
	private CollectionRigthServiceBean bean;
	
	/**
	 * Busqueda para cobro de derechos economicos
	 * */
	
	public FundsConsultRegisterTO createDataExtractCollectionEconomic(Integer currency) throws ServiceException{
		FundsConsultRegisterTO objFundsTransferRegisterTO = new FundsConsultRegisterTO();	
		AutomaticSendType objAutomaticSendType = new AutomaticSendType();
		String [] parametersEdb = getEDBInformationCollectionEconomic(currency); 		
		if(parametersEdb != null){
			objAutomaticSendType.setOriginParticipantCode(parametersEdb[0]);
			objAutomaticSendType.setOriginBankAccountCode(parametersEdb[1]);
			objAutomaticSendType.setOriginCurrency(parametersEdb[2]);
			objAutomaticSendType.setOriginDepartment(FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB);
		}
		objFundsTransferRegisterTO.setAutomaticSendTypeTO(objAutomaticSendType);
		if(parametersEdb == null || parametersEdb.length <= 0 ){
			throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_NOTEXITS_BANK_ACCOUNT_EDB);
		} else {
			return objFundsTransferRegisterTO;
		}		        
	}
	/**
	 * Obtener datos de informacion basica
	 * @param currency
	 * @return */
	public String[] getEDBInformationCollectionEconomic(Integer currency) throws ServiceException{
		String [] strParameters = null;		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(mnemonicParticipantEDB);
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		strParameters = new String[6];
		
		strParameters[0] = objParticipant.getIdBcbCode().toString();
		strParameters[1] = accountNumberCollection;
		ParameterTable objParameterTable = participantServiceBean.find(currency, ParameterTable.class);
		strParameters[2] = objParameterTable.getShortInteger().toString();
		strParameters[3] = objParticipant.getIdParticipantPk().toString();
		strParameters[4] = objParticipant.getMnemonic();
		strParameters[5] = objParameterTable.getParameterTablePk().toString();
		return strParameters;
	}
	
	/**
	 * Creates the data send funds.
	 *
	 * @param fundOperation the fund operation
	 * @param userName the user name
	 * @return the funds transfer register to
	 * @throws ServiceException the service exception
	 */
	public FundsTransferRegisterTO createDataSendFunds(FundsOperation fundOperation,String userName) throws ServiceException{
		Long idSecuenceSendLip = getSecuenceSendFunds();
		if(Validations.validateIsNullOrEmpty(idSecuenceSendLip)){
			throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_NOTEXITS_CORRELATIVE);
		}		
		
		AutomaticSendType objAutomaticSendType = new AutomaticSendType();
		FundsTransferRegisterTO objFundsTransferRegisterTO = new FundsTransferRegisterTO();
		//cod_tipo_operacion
//		objAutomaticSendType.setCodTypeOperation(FundsLIPProcessType.ENVIO.getCode());
		//fecha
		objAutomaticSendType.setDate(CommonsUtilities.currentDateOptional());		
		// TARGET PARTICIPANT BCB ACCOUNT
		// armando informacion a enviar al LIP
		String [] parameters = getBCBInformation(fundOperation.getInstitutionCashAccount().getIdInstitutionCashAccountPk());
		//cod_tipo_operacion [6]
		objAutomaticSendType.setCodTypeOperation(parameters[6]);
		//cod_part_destino [0]
		objAutomaticSendType.setTargetParticipantCode(parameters[0]);
		//cod_cuenta_destino [1]
		objAutomaticSendType.setTargetBankAccountCode(parameters[1]);
		//cod_moneda_destino [2]
		objAutomaticSendType.setTargetCurrency(parameters[2]);
		//cod_plaza_destino
		objAutomaticSendType.setTargetDepartment(FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB);
		String idCurrencyPk = parameters[5];									
		//monto [5]
		objAutomaticSendType.setAmount(fundOperation.getOperationAmount());
		//tipo_liquidacion
		objAutomaticSendType.setSettlementType(FundsAutomaticConstants.SETTLEMENT_TYPE_GROSS);
		// prioridad
		objAutomaticSendType.setPriority(GeneralConstants.ONE_VALUE_STRING);
		//autoriza_credito
		objAutomaticSendType.setCreditEntity(FundsAutomaticConstants.CREDIT_ENTITY);
		//glosa [4]
		objAutomaticSendType.setTextDescription(parameters[4]);
		//cod_usuario
		objAutomaticSendType.setUserCode(userName);
		/*List<AditionalInformationSendType> lstAditionalInformationSendType= new ArrayList<AditionalInformationSendType>();
		AditionalInformationSendType objAditionalInformationSendType=new AditionalInformationSendType();
		objAditionalInformationSendType.setCode(FundsAutomaticConstants.PARTICIPANT_ADITIONAL_INFORMATION_CODE);
		objAditionalInformationSendType.setValue(parameters[3]);
		lstAditionalInformationSendType.add(objAditionalInformationSendType);
		objAutomaticSendType.setLstAditionalInformationSendType(lstAditionalInformationSendType);*/
		//filling correlative format
		String correlative=CommonsUtilities.completeLeftZero(idSecuenceSendLip.toString(),3);
		String operationDateFormat=CommonsUtilities.convertDatetoStringOptional(objAutomaticSendType.getDate());
		String finalCorrelative=objAutomaticSendType.getTargetParticipantCode()+
								objAutomaticSendType.getTargetDepartment()+
								operationDateFormat+correlative;
		//nro_correlativo
		objAutomaticSendType.setCorrelativeNumber(finalCorrelative);
		
		objFundsTransferRegisterTO.setAutomaticSendTypeTO(objAutomaticSendType);
		objFundsTransferRegisterTO.setIdFundsOperation(fundOperation.getIdFundsOperationPk());
		objFundsTransferRegisterTO.setIdParticipant(new Long(parameters[3].toString()));
		objFundsTransferRegisterTO.setOperationNumber(idSecuenceSendLip);
		
		// ORIGEN EDV BCB ACCOUNT
		String [] parametersEdb = getEDBInformation(idCurrencyPk); 					
		
		if(parametersEdb != null){
			objAutomaticSendType.setOriginParticipantCode(parametersEdb[0]);
			objAutomaticSendType.setOriginBankAccountCode(parametersEdb[1]);
			objAutomaticSendType.setOriginCurrency(parametersEdb[2]);
			objAutomaticSendType.setOriginDepartment(FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB);
		}
		
		if(parametersEdb == null || parametersEdb.length <= 0 ){
			throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_NOTEXITS_BANK_ACCOUNT_EDB);
		} else {
			return objFundsTransferRegisterTO;
		}		        
	}
	/**
	 * Gets the EDB information.
	 *
	 * @param idCurrencyPk the id currency pk
	 * @return the EDB information
	 * @throws ServiceException the service exception
	 */
	public String[] getEDBInformation(String idCurrencyPk) throws ServiceException{
		String [] strParameters = null;		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(mnemonicParticipantEDB);
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if(objParticipant != null && objParticipant.getIdParticipantPk()!=null){
			InstitutionBankAccount objInstitutionBankAccount = manageBankAccountsService.getInstitutionBankAccountForLip(
					InstitutionBankAccountsType.PARTICIPANT.getCode(), objParticipant.getIdParticipantPk(), 
					new Integer(idCurrencyPk), accountNumberLip);		
			if(objInstitutionBankAccount != null){
				strParameters = new String[6];
				strParameters[0] = objParticipant.getIdBcbCode().toString();
				strParameters[1] = objInstitutionBankAccount.getAccountNumber().toString();
				ParameterTable objParameterTable = manageCashAccountService.find(objInstitutionBankAccount.getCurrency(), ParameterTable.class);
				strParameters[2] = objParameterTable.getShortInteger().toString();
				strParameters[3] = objParticipant.getIdParticipantPk().toString();
				strParameters[4] = objParticipant.getMnemonic();
				strParameters[5] = objParameterTable.getParameterTablePk().toString();
			}		
		}		
		return strParameters;
	}
	/**
	 * Gets the secuence send funds.
	 *
	 * @return the correlative send funds
	 * @throws ServiceException the service exception
	 */
	public Long getSecuenceSendFunds() throws ServiceException {						
		return manageBankAccountsService.getNextCorrelativeOperations(GeneralConstants.SQ_SEND_LIP_CORP_PK);
	}
	/**
	 * Gets the BCB information.
	 * [0]  cod_part_destino
	 * [1]  cod_cuenta_destino
	 * [2]  cod_moneda_destino
	 * [3]
	 * [4]  glosa
	 * [5]  monto
	 * [6]  operacion   
	 * 
	 * @param institutionCashAccountPk the institution cash account pk
	 * @return the BCB information
	 * @throws ServiceException the service exception
	 */
	public String[] getBCBInformation(Long institutionCashAccountPk) throws ServiceException{
		String[] strParameters= new String[7];
		List<InstitutionCashAccount> lstInstitutionCashAccount = manageCashAccountService.getParticipantSendBCBInformation(institutionCashAccountPk);
		if(Validations.validateListIsNullOrEmpty(lstInstitutionCashAccount)){
			throw new ServiceException(ErrorServiceType.CASH_ACCOUNT_NOTEXISTS);
		}		
		InstitutionCashAccount objInstitutionCashAccount=lstInstitutionCashAccount.get(0);
		//cod_part_destino
		if(objInstitutionCashAccount.getParticipant().getIdBcbCode() != null){
			// Operaciones E74 // Solo se manajera la operacion E03
			// cuenta propia de la agencia de bolsa
			strParameters[0] = objInstitutionCashAccount.getParticipant().getIdBcbCode().toString();
			// cod_operacion
			strParameters[6] = "E03";
			//cod_cuenta_destino
			strParameters[1] = objInstitutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber().toString();
		} else {
			// Operaciones E03
			// cuenta de terceros Banco
			strParameters[0] = objInstitutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBcbCode().toString();
			// cod_operacion
			strParameters[6] = "E03";
			//cod_cuenta_destino
			strParameters[1] = objInstitutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getAccountNumber().toString();
		}
		
		
		/*if(MasterTableType.PARAMETER_TABLE_BANK_ACCOUNT_CLASS_THIRDS.getCode().equals(objInstitutionCashAccount.getCashAccountDetailResults().get(0).getBankAccountClass())) {
			strParameters[0] = objInstitutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getBank().getIdBcbCode().toString();
		} else {
			strParameters[0] = objInstitutionCashAccount.getParticipant().getIdBcbCode().toString();
		}*/		
		
		ParameterTable objParameterTable = manageCashAccountService.find(objInstitutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getCurrency(), ParameterTable.class);
		//
		strParameters[2] = objParameterTable.getShortInteger().toString();
		if(objInstitutionCashAccount.getParticipant()!=null){
			strParameters[3] = objInstitutionCashAccount.getParticipant().getIdParticipantPk().toString();
			strParameters[4] = objInstitutionCashAccount.getParticipant().getMnemonic();
		}
		strParameters[5] = objInstitutionCashAccount.getCashAccountDetailResults().get(0).getInstitutionBankAccount().getCurrency().toString();
		return strParameters;
	}
	
	/**
	 * Busqueda de glosas que no sean pagado 
	 * @throws ServiceException 
	 * 
	 * */	
	public void updateFundsCollectionAndCollection(Date operationDate) throws ServiceException{
		//Date operationDate = CommonsUtilities.currentDate();
		List<Object[]> listDetailCollection = bean.getListDetailCollection(operationDate);
		List<Object[]> listFundCollection   = bean.getListFundCollection(CommonsUtilities.currentDate());
		for (Object[] objects_1 : listDetailCollection) {
			ObjectFunCollectionTO fundGloss = getObjectFunCollectionTO(objects_1);
			//System.out.println("CDE :::::: Buscando glosa :"+fundGloss.getGloss()+" con fecha :"+operationDate);
			for (Object[] objects_2 : listFundCollection){
				ObjectFunCollectionTO fundCollecton = getObjectFunCollectionTO(objects_2);
				//System.out.println("CDE:::::::::::::::::::::::::: Comparando "+fundGloss.getGloss()+" con "+fundCollecton.getGloss());
				if(fundGloss.getGloss().equals(fundCollecton.getGloss()))
					if(fundCollecton.getAmount().compareTo(fundGloss.getAmount())==1||fundCollecton.getAmount().compareTo(fundGloss.getAmount())==0){
						DetailCollectionGloss collectionGloss = bean.searchDepositNotification(fundGloss.getGloss(), operationDate);
						if(collectionGloss!=null){
							collectionGloss.setAmountDeposited(fundCollecton.getAmount());
							collectionGloss.setDepositStatus(DepositStateType.CONFIRMED.getCode());
							collectionGloss.setDepositDate(CommonsUtilities.currentDate());
							collectionGloss.setLastModifyDate(CommonsUtilities.currentDate());
							bean.update(collectionGloss);
							bean.updateCollections(collectionGloss.getGlosaNotClassified(),StatesConstants.CONFIRMED_MATCH_GLOSSES);
						}
					}else{
						DetailCollectionGloss collectionGloss = bean.searchDepositNotification(fundGloss.getGloss(), operationDate);
						if(collectionGloss!=null){
							collectionGloss.setAmountDeposited(fundCollecton.getAmount());
							collectionGloss.setLastModifyDate(CommonsUtilities.currentDate());
							bean.update(collectionGloss);
							bean.updateCollections(collectionGloss.getGlosaNotClassified(),null);
						}
				}
			}
		}
	}
	
	
	/**
	 * Cast por error*/
	private ObjectFunCollectionTO getObjectFunCollectionTO(Object[] objects){
		ObjectFunCollectionTO collectionTO = new ObjectFunCollectionTO(objects[0].toString(),new BigDecimal(objects[1].toString()));
		return collectionTO;
	}
}
