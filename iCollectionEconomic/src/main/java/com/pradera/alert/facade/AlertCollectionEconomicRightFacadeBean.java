package com.pradera.alert.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.pradera.alert.service.AlertCollectionEconomicRightServiceBean;
import com.pradera.alertt.bath.to.SecuritiesNotifyTO;
import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.RegisterCollectionRightTO;
import com.pradera.collection.right.to.SearchRegisterResultTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.process.ProcessLogger;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.ReqCollectionEconomicRight;

public class AlertCollectionEconomicRightFacadeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;

	@Inject
	private AlertCollectionEconomicRightServiceBean alertServiceBean;
	
	@Inject
	private CollectionRigthServiceBean collectionRigthServiceBean;
	
	// TODO cambiar los nombres de los tipos de saldo seg�n corresponda
	private static final String TRANSIT_BALANCE = "TRANSIT_BALANCE";
	private static final String PAWN_BALANCE = "PAWN_BALANCE";
	private static final String BAN_BALANCE = "BAN_BALANCE";
	private static final String OTHER_BLOCK_BALANCE = "OTHER_BLOCK_BALANCE";
	private static final String SALE_BALANCE = "SALE_BALANCE";
	private static final String PURCHASE_BALANCE = "PURCHASE_BALANCE";
	private static final String REPORTED_BALANCE = "REPORTED_BALANCE";
	private static final String REPORTING_BALANCE = "REPORTING_BALANCE";
	private static final String MARGIN_BALANCE = "MARGIN_BALANCE";
	private static final String ACCREDITATION_BALANCE = "ACCREDITATION_BALANCE";
	private static final String BORROWER_BALANCE = "BORROWER_BALANCE";
	private static final String LENDER_BALANCE = "LENDER_BALANCE";
	private static final String RESERVE_BALANCE = "RESERVE_BALANCE";
	private static final String LOANABLE_BALANCE = "LOANABLE_BALANCE";
	private static final String OPPOSITION_BALANCE = "OPPOSITION_BALANCE";
	private static final String GRANTED_BALANCE = "GRANTED_BALANCE";
	private static final String GRANT_BALANCE = "GRANT_BALANCE";

	
	/**
	 * Devuelve la description del saldo
	 * */
	public String getDescriptionHolderAccountBalance(HolderAccountBalance accountBalance){
		if(accountBalance.getTransitBalance().compareTo(BigDecimal.ZERO)>0)
			return TRANSIT_BALANCE;
		if(accountBalance.getPawnBalance().compareTo(BigDecimal.ZERO)>0)
			return PAWN_BALANCE;
		if(accountBalance.getBanBalance().compareTo(BigDecimal.ZERO)>0)
			return BAN_BALANCE;
		if(accountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO)>0)
			return OTHER_BLOCK_BALANCE;
		if(accountBalance.getSaleBalance().compareTo(BigDecimal.ZERO)>0)
			return SALE_BALANCE;
		if(accountBalance.getPurchaseBalance().compareTo(BigDecimal.ZERO)>0)
			return PURCHASE_BALANCE;
		if(accountBalance.getReportedBalance().compareTo(BigDecimal.ZERO)>0)
			return REPORTED_BALANCE;
		if(accountBalance.getReportingBalance().compareTo(BigDecimal.ZERO)>0)
			return REPORTING_BALANCE;
		if(accountBalance.getMarginBalance().compareTo(BigDecimal.ZERO)>0)
			return MARGIN_BALANCE;
		if(accountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO)>0)
			return ACCREDITATION_BALANCE;
		if(accountBalance.getBorrowerBalance().compareTo(BigDecimal.ZERO)>0)
			return BORROWER_BALANCE;
		if(accountBalance.getLenderBalance().compareTo(BigDecimal.ZERO)>0)
			return LENDER_BALANCE;
		if(accountBalance.getReserveBalance().compareTo(BigDecimal.ZERO)>0)
			return RESERVE_BALANCE;
		if(accountBalance.getLoanableBalance().compareTo(BigDecimal.ZERO)>0)
			return LOANABLE_BALANCE;
		if(accountBalance.getOppositionBalance().compareTo(BigDecimal.ZERO)>0)
			return OPPOSITION_BALANCE;
		else
			return null;
	}
	
	/**Metodo para alerta de cobro de derechos economicos
	 * Buscar los valores que esten venciendo y que no se encuentren en algun registro
	 * de cobro de derechos economicos*/
	public List<String> notifySecuritiesNotRegisteredInCollections(ProcessLogger processLogger,Date paymentDate,Date nextBusinessDay){
		List<SearchRegisterResultTO>listSecuritiesToNotify = new ArrayList<>();
		List<String> listSecurities = new ArrayList<>();
		try {
			/**Buscamos la lista de valores en cartera*/
			RegisterCollectionRightTO collectionRightTO = new RegisterCollectionRightTO();
			collectionRightTO.setInitialDate(paymentDate);
			collectionRightTO.setFinalDate(nextBusinessDay);
			listSecuritiesToNotify = collectionRigthServiceBean.getListSecuritiesToPay(collectionRightTO);
			
			for (SearchRegisterResultTO searchRegisterResultTO : listSecuritiesToNotify)
				if(!collectionRigthServiceBean.isExistsEconomicRight(searchRegisterResultTO))
					listSecurities.add(searchRegisterResultTO.getIdSecurityCodePk());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listSecurities;
	}
	/**Valores que se encuentran registrados en cobros, pero en cartera ya no estan
	 */
	public List<SecuritiesNotifyTO> notifySecuritiesWithUnavailableBalance(ProcessLogger processLogger,Date paymentDate,Date nextBusinessDay){
		
		List<SecuritiesNotifyTO> listSecuritiesNotifyTOs = new ArrayList<>();
		List<Participant> listParticipants = parameterServiceBean.getListParticipantForCollection();
		SecuritiesNotifyTO securitiesNotifyTO = new SecuritiesNotifyTO();
		for (Participant participant : listParticipants) {
			securitiesNotifyTO = getObtjectSecuritiesNotifyAlert(paymentDate, nextBusinessDay, participant);
			listSecuritiesNotifyTOs.add(securitiesNotifyTO);
		}
		return listSecuritiesNotifyTOs;
	}
	/**Busqueda de valores que se encuntren con saldo 0 o que ya no esten en cartera*/
	public SecuritiesNotifyTO getObtjectSecuritiesNotifyAlert(Date paymentDate,Date nextBusinessDay,Participant participant){
		List<CollectionEconomicRigth> list= collectionRigthServiceBean.getListSecuritiesNotify
				(paymentDate, nextBusinessDay,participant.getIdParticipantPk(), null, null, null, null,null,null,null,null,null);
		
		List<String> listSecuritiesDifferentBalance = new ArrayList<>();
		/*Verificamos que los datos esten en cartera*/
		HolderAccountBalance holderAccountBalance = null;
		HolderAccountBalancePK holderAccountBalancePK =  null;
		
		for (CollectionEconomicRigth collectionEconomicRigth : list) {
			
			holderAccountBalancePK = new HolderAccountBalancePK();
			
			/**Buscamos el participante del cobro registrado*/
			//idParticipantPk = alertServiceBean.searchIdParticipantPk(collectionEconomicRigth.getIdReqCollectionEconomicFk());
			
			holderAccountBalancePK.setIdParticipantPk(participant.getIdParticipantPk());
			holderAccountBalancePK.setIdSecurityCodePk(collectionEconomicRigth.getIdSecurityCodeFk());
			holderAccountBalancePK.setIdHolderAccountPk(collectionEconomicRigth.getIdHolderAccountPk());
			
			holderAccountBalance = collectionRigthServiceBean.find(HolderAccountBalance.class, holderAccountBalancePK);
			if(holderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO)==0)
				listSecuritiesDifferentBalance.add(collectionEconomicRigth.getIdSecurityCodeFk());
		}
		SecuritiesNotifyTO securitiesNotifyTO = new SecuritiesNotifyTO();
		securitiesNotifyTO.setListSecuritiesDifferentBalance(listSecuritiesDifferentBalance);
		securitiesNotifyTO.setParticipant(participant);
		
		return securitiesNotifyTO;
	}


	/** method to update a not submit in {@link ReqCollectionEconomicRight}
	 * 
	 * @param listIdRequestForUpdate */
	public void updateNoValidForSubmit(List<Long> listIdRequestForUpdate) {
		alertServiceBean.updateNoValidForSubmit(listIdRequestForUpdate);
	}
	
}
