package com.pradera.alert.facade;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.alert.service.FundsComponentSingleton;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.funds.to.FundsConsultRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.type.CurrencyType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ManageDepositServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ManageDepositServiceFacade {
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	

	/** The funds component singleton. */
	@Inject
	private FundsComponentSingleton fundsComponentSingleton;
	
	/** The manage lip service facade. */
	@Inject
	private ManageLipServiceFacade manageLipServiceFacade;
	
	@Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	
	@ProcessAuditLogger(process=BusinessProcessType.FUNDS_DEPOSIT_MANUAL_REGISTER)
	public void sendRequestExtract(List<FundsConsultRegisterTO> lstFundsTransferRegisterTO) throws ServiceException {	
		Date operationDate = CommonsUtilities.currentDate();
		//Date operationDate = CommonsUtilities.convertStringtoDate("11/06/2019", CommonsUtilities.DATE_PATTERN);
		
		/*Se hace lectura y registro de fondos por cobro de derechos economicos*/
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		if(Validations.validateListIsNotNullAndNotEmpty(lstFundsTransferRegisterTO)){
				for(FundsConsultRegisterTO objFundsTransferRegisterTO : lstFundsTransferRegisterTO){				
					objFundsTransferRegisterTO.getAutomaticSendTypeTO().setCodTypeOperation(ComponentConstant.INTERFACE_BCB_LIP_EXTRACT);
					objFundsTransferRegisterTO.getAutomaticSendTypeTO().setDate(operationDate);
					fundsComponentSingleton.searchExtractByCurrency(objFundsTransferRegisterTO, loggerUser);					
				}
			}
		/**Actualizamos todos los depositos, si los depositos suman, actualizamos el deposito solicitado*/
		Date paymentDate;
		for (int i = 0 ; i<= 3;i++ ) {
			paymentDate =  null;
			paymentDate = holidayQueryServiceBean.subtractBusinessDays(CommonsUtilities.currentDate(), i);
			manageLipServiceFacade.updateFundsCollectionAndCollection(paymentDate);
		}
	}
	
	public FundsConsultRegisterTO prepareBob() throws ServiceException{
		return manageLipServiceFacade.createDataExtractCollectionEconomic(CurrencyType.PYG.getCode());
	}
	
	public FundsConsultRegisterTO prepareUsd() throws ServiceException{
		return manageLipServiceFacade.createDataExtractCollectionEconomic(CurrencyType.USD.getCode());
	}
}
