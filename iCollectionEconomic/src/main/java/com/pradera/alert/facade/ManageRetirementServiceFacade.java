package com.pradera.alert.facade;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.alert.webclient.CollectionServiceConsumer;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;


@Stateless
@Performance
public class ManageRetirementServiceFacade implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2555119325841173133L;
	

	@Inject
	private CollectionServiceConsumer collectionServiceConsumer;
	
	@Inject
	private ParticipantServiceBean participantServiceBean;
	
	@Inject 
	@Configurable 
	private String mnemonicParticipantEDB;
	
	
	@Inject 
	@Configurable 
	private String accountNumberCollection;
	
	public Long getSecuenceSendFunds() throws ServiceException {						
		return collectionServiceConsumer.getNextCorrelativeOperations(GeneralConstants.SQ_SEND_LIP_CORP_PK);
	}
	
	/**
	 * Informacion de datos de retiro*/
	public String[] getEDBInformationCollectionEconomic(Integer currency) throws ServiceException{
		String [] strParameters = null;		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(mnemonicParticipantEDB);
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		strParameters = new String[6];
		
		strParameters[0] = objParticipant.getIdBcbCode().toString();
		strParameters[1] = accountNumberCollection;
		ParameterTable objParameterTable = participantServiceBean.find(currency, ParameterTable.class);
		strParameters[2] = objParameterTable.getShortInteger().toString();
		strParameters[3] = objParticipant.getIdParticipantPk().toString();
		strParameters[4] = objParticipant.getMnemonic();
		strParameters[5] = objParameterTable.getParameterTablePk().toString();
		return strParameters;
	}
}
