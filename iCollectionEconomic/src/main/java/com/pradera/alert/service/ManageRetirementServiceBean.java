package com.pradera.alert.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.alertt.bath.to.InstitutionCashBankAccountTO;
import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.AccountCashFundsType;
import com.pradera.model.funds.type.BankStateType;
import com.pradera.model.funds.type.CashAccountStateType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.edv.collection.economic.rigth.DetailCollectionGloss;

@Stateless
@Performance
public class ManageRetirementServiceBean extends CrudDaoServiceBean {

	public FundsTransferRegisterTO generateFundsTransferRegisterTO(FundsTransferRegisterTO objFundsTransferRegisterTO,
			Long idInterfaceProcess, LoggerUser loggerUser) throws ServiceException {
		objFundsTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		Long idInterfaceTransaction = 123L;// interfaceComponentServiceBean.get().saveInterfaceTransactionTx(objFundsTransferRegisterTO,
											// BooleanType.YES.getCode(), loggerUser);
		objFundsTransferRegisterTO.setIdInterfaceTransaction(idInterfaceTransaction);
		return objFundsTransferRegisterTO;
	}

	/**
	 * Busqueda de institucion bancaraia de pago para cobro de derechos economicos
	 * 
	 * @param idParticipantPk id del participante al cual se va a pagar
	 * @param idBankPk        Banco al cual se va a pagar
	 * @param currency        Moneda de cobro
	 */
	public InstitutionCashAccount getInstitutionCashAccount(RetirementObjetTO retirementObjetTO)
			throws ServiceException {
		InstitutionCashAccount objInstitutionCashAccount = null;
		StringBuffer stringBuffer = new StringBuffer();
		try {
			stringBuffer.append(" SELECT ICA FROM InstitutionCashAccount ICA ");
			stringBuffer.append(" WHERE 1 = 1 ");
			stringBuffer.append(" and ICA.accountType = :idAccountType ");
			if (retirementObjetTO.getIdParticipantPk() != null)
				stringBuffer.append(" and ICA.participant.idParticipantPk = :idParticipant ");
			stringBuffer.append(" and ICA.currency = :currency ");
			stringBuffer.append(" and ICA.accountState = :idAccountState ");

			Query query = em.createQuery(stringBuffer.toString());
			query.setParameter("idParticipant", retirementObjetTO.getCurrency());
			// usado temporalmente el de liquidacion
			query.setParameter("idAccountType", AccountCashFundsType.BENEFIT.getCode());
			// Usar el de para cobro de derechos economicos
			// query.setParameter("idAccountType", AccountCashFundsType.BENEFIT.getCode());
			query.setParameter("currency", retirementObjetTO.getCurrency());
			query.setParameter("idAccountState", CashAccountStateType.ACTIVATE.getCode());

			objInstitutionCashAccount = (InstitutionCashAccount) query.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objInstitutionCashAccount;
	}

	/**
	 * Gets the next correlative request operations.
	 *
	 * @param requestOperation the request operation
	 * @return the next correlative operations
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Long getNextCorrelativeOperations(String nameSequence) {
		Object value = null;
		Long nextVal = null;
		try {
			Query query = em.createNativeQuery("select " + nameSequence + ".nextval from dual");
			value = query.getSingleResult();
			if (value != null) {
				nextVal = Long.valueOf(value.toString());
			}
		} catch (Exception e) {
			boolean foundException = false;
			while (e.getCause() != null) {
				if (e.getCause().getMessage().toUpperCase().contains("NEXTVAL EXCEEDS MAXVALUE")) {
					foundException = true;
					break;
				} else {
					e = (Exception) e.getCause();
				}
			}
			if (foundException) {
				nextVal = new Long(-1);
			}

		}
		return nextVal;
	}

	/**
	 * Obtenemos las lista de glosas generadas en una determinada fecha
	 **/
	public List<String> getListGlossCollection(Date registreDate, Long idParticipantPk, Integer transferStatus) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select dcg.glosaNotClassified  from DetailCollectionGloss dcg ");
		sbQuery.append(" where 1 = 1 ");
		if (idParticipantPk != null)
			sbQuery.append(" and dcg.idParticipantPk = :idParticipantPk ");
		if (transferStatus != null)
			sbQuery.append(" and dcg.transferStatus = :transferStatus ");
		if (registreDate != null)
			sbQuery.append(" and trunc(dcg.registreDate) = :registreDate ");

		Query query = em.createQuery(sbQuery.toString());
		if (idParticipantPk != null)
			query.setParameter("idParticipantPk", idParticipantPk);
		if (transferStatus != null)
			query.setParameter("transferStatus", transferStatus);
		if (registreDate != null)
			query.setParameter("registreDate", registreDate);

		@SuppressWarnings("unchecked")
		List<String> listGloss = query.getResultList();
		return listGloss;
	}

	/**
	 * Obtenemos las lista de glosas generadas en una determinada fecha
	 **/
	public List<String> getListGlossCollection(Date registreDate, Long idParticipantPk, Integer depositStatus,
			Integer transferStatus) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select dcg.glosaNotClassified  from DetailCollectionGloss dcg ");

		sbQuery.append(" where 1 = 1 ");

		if (idParticipantPk != null)
			sbQuery.append(" and dcg.idParticipantPk = :idParticipantPk ");

		if (depositStatus != null)
			sbQuery.append(" and dcg.depositStatus = :depositStatus ");

		if (transferStatus != null)
			sbQuery.append(" and dcg.transferStatus = :transferStatus ");

		if (registreDate != null)
			sbQuery.append(" and trunc(dcg.paymentDate) = :registreDate ");

		Query query = em.createQuery(sbQuery.toString());
		if (idParticipantPk != null)
			query.setParameter("idParticipantPk", idParticipantPk);

		if (depositStatus != null)
			query.setParameter("depositStatus", depositStatus);

		if (transferStatus != null)
			query.setParameter("transferStatus", transferStatus);

		if (registreDate != null)
			query.setParameter("registreDate", registreDate);

		@SuppressWarnings("unchecked")
		List<String> listGloss = query.getResultList();
		return listGloss;
	}

	/**
	 * Obtenemos las lista de glosas generadas en una determinada fecha
	 **/
	public List<String> getListGlossCollectionForDeposit(Date registreDate, Long idParticipantPk, Long idBank,
			boolean depositBoolean) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select dcg.glosaNotClassified  from DetailCollectionGloss dcg ");
		sbQuery.append(" where 1 = 1 ");
		if (idParticipantPk != null)
			sbQuery.append(" and dcg.idParticipantPk = :idParticipantPk ");

		// evaluando si se trata de un deposito o retiro
		if (depositBoolean) {
			sbQuery.append(" and dcg.depositStatus is null ");
			
			sbQuery.append(" and dcg.transferStatus is null ");
		}
		else {
			sbQuery.append(" and dcg.transferStatus is null ");

			sbQuery.append(" and dcg.depositStatus = "+DepositStateType.CONFIRMED.getCode());
		}
		if (registreDate != null)
			sbQuery.append(" and trunc(dcg.paymentDate) = :registreDate ");

		if (idBank != null)
			sbQuery.append(" and  dcg.idBankPk = :idBank");

		Query query = em.createQuery(sbQuery.toString());
		if (idParticipantPk != null)
			query.setParameter("idParticipantPk", idParticipantPk);

		if (registreDate != null)
			query.setParameter("registreDate", registreDate);

		if (idBank != null)
			query.setParameter("idBank", idBank);

		@SuppressWarnings("unchecked")
		List<String> listGloss = query.getResultList();
		return listGloss;
	}

	/**
	 * Obtenemos las lista de glosas generadas en una determinada fecha
	 **/
	public DetailCollectionGloss getNotificationCurrency(String gloss) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select dcg from DetailCollectionGloss dcg ");
		sbQuery.append(" where 1 = 1 ");
		sbQuery.append(" and dcg.glosaNotClassified = :gloss ");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("gloss", gloss);

		@SuppressWarnings("unchecked")
		DetailCollectionGloss detailCollectionGloss = (DetailCollectionGloss) query.getSingleResult();
		return detailCollectionGloss;
	}

	/**
	 * Buscamos la cuenta bancaria LIP a la cual se hara el deposito
	 * 
	 * @param obj Parametros de busqueda
	 * @return {@link InstitutionCashAccount}
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public InstitutionCashAccount getActivatedCashAccount(InstitutionCashAccount obj) {

		StringBuilder builder = new StringBuilder();

		builder.append(" select ica from InstitutionCashAccount ica ");
		builder.append(" where 1 = 1 ");
		builder.append(" and ica.participant.idParticipantPk = :idParticipantPk ");
		builder.append(" and ica.currency     = :currency ");
		builder.append(" and ica.accountType  = :accountType ");
		builder.append(" and ica.accountClass = :accountClass ");
		builder.append(" and ica.accountState = :accountState ");
		builder.append(" and ica.indRelatedBcrd = :indRelatedBcrd ");

		Query query = em.createQuery(builder.toString());
		query.setParameter("idParticipantPk", obj.getParticipant().getIdParticipantPk());
		query.setParameter("currency", obj.getCurrency());
		query.setParameter("accountType", 650);
		query.setParameter("accountClass", 681);
		query.setParameter("accountState", CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("indRelatedBcrd", 0);

		@SuppressWarnings("unchecked")
		List<InstitutionCashAccount> listaccountActivated = query.getResultList();

		if (listaccountActivated == null || listaccountActivated.size() == 0)
			return null;

		return listaccountActivated.get(0);
	}

	/**
	 * Buscamos el detalle del registro de cuentas
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long searhIdInstitutionBankAccountPk(Long idInstitutionCashAccountPk) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer
				.append(" select cad.institutionBankAccount.idInstitutionBankAccountPk FROM CashAccountDetail cad ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer
				.append(" and cad.institutionCashAccount.idInstitutionCashAccountPk = :idInstitutionCashAccountPk ");
		stringBuffer.append(" and cad.indSending = :indSending ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idInstitutionCashAccountPk", idInstitutionCashAccountPk);
		query.setParameter("indSending", 1);

		return (Long) query.getSingleResult();
	}

	/**
	 * Buscamo el id de banco
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String getCodBcbForbank(Long idInstitutionBankAccountPk) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT B.ID_BCB_CODE FROM INSTITUTION_BANK_ACCOUNT IBA ");
		stringBuffer.append(" INNER JOIN BANK B ON IBA.ID_BANK_FK =  B.ID_BANK_PK ");
		stringBuffer.append(" WHERE 1 = 1 ");
		stringBuffer.append(" AND IBA.ID_INSTITUTION_BANK_ACCOUNT_PK = :idInstitutionBankAccountPk ");
		stringBuffer.append(" AND B.STATE = :state ");

		Query query = em.createNativeQuery(stringBuffer.toString());
		query.setParameter("idInstitutionBankAccountPk", idInstitutionBankAccountPk);
		query.setParameter("state", BankStateType.REGISTERED.getCode());
		BigDecimal bigDecimal = (BigDecimal) query.getSingleResult();
		return bigDecimal.toString();
	}

	/**
	 * Buscamo el id de banco
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long getIdBankPkByInstitutionBankAccount(Long idInstitutionBankAccountPk) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" SELECT IBA.ID_BANK_FK FROM INSTITUTION_BANK_ACCOUNT IBA ");
		stringBuffer.append(" INNER JOIN BANK B ON IBA.ID_BANK_FK =  B.ID_BANK_PK ");
		stringBuffer.append(" WHERE 1 = 1 ");
		stringBuffer.append(" AND IBA.ID_INSTITUTION_BANK_ACCOUNT_PK = :idInstitutionBankAccountPk ");
		stringBuffer.append(" AND B.STATE = :state ");

		Query query = em.createNativeQuery(stringBuffer.toString());
		query.setParameter("idInstitutionBankAccountPk", idInstitutionBankAccountPk);
		query.setParameter("state", BankStateType.REGISTERED.getCode());
		BigDecimal bigDecimal = (BigDecimal) query.getSingleResult();
		return bigDecimal.longValue();
	}

	/**
	 * Obtenemos las lista de glosas generadas en una determinada fecha
	 **/
	public List<String> getListGlossCollectionForDeposit(Date registreDate, Long idParticipantPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select dcg.glosaNotClassified  from DetailCollectionGloss dcg ");
		sbQuery.append(" where 1 = 1 ");
		if (idParticipantPk != null)
			sbQuery.append(" and dcg.idParticipantPk = :idParticipantPk ");

		sbQuery.append(" and dcg.depositStatus is null ");

		if (registreDate != null)
			sbQuery.append(" and trunc(dcg.paymentDate) = :registreDate ");

		Query query = em.createQuery(sbQuery.toString());
		if (idParticipantPk != null)
			query.setParameter("idParticipantPk", idParticipantPk);

		if (registreDate != null)
			query.setParameter("registreDate", registreDate);

		@SuppressWarnings("unchecked")
		List<String> listGloss = query.getResultList();
		return listGloss;
	}

	/**
	 * Buscamos el registro y actualizamos el monto del retiro
	 * 
	 * @throws ServiceException
	 */
	public void registerPaymentTransfer(RetirementObjetTO retirementObjetTO) throws ServiceException {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select dcg FROM DetailCollectionGloss dcg ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and dcg.idParticipantPk = :idParticipantPk ");
		stringBuffer.append(" and dcg.currency = :currency ");
		stringBuffer.append(" and dcg.glosaNotClassified = :glosaNotClassified ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idParticipantPk", retirementObjetTO.getIdParticipantPk());
		query.setParameter("currency", retirementObjetTO.getCurrency());
		query.setParameter("glosaNotClassified", retirementObjetTO.getGloss());

		DetailCollectionGloss collectionGloss = (DetailCollectionGloss) query.getSingleResult();

		if (collectionGloss == null) {
			throw new ServiceException();
			// return;
		}
		collectionGloss.setRetirementDate(CommonsUtilities.currentDate());// bandera de deposito
		collectionGloss.setTransferStatus(DepositStateType.CONFIRMED.getCode());
		super.update(collectionGloss);
	}

	/**
	 * Buscamos el detalle de retiro/deposito
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<DetailCollectionGloss> searchDetailCollectionGlossList(RetirementObjetTO retirementObjetTO) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select dcg FROM DetailCollectionGloss dcg ");
		stringBuffer.append(" where 1 = 1 ");

		if (retirementObjetTO.getIdParticipantPk() != null)
			stringBuffer.append(" and dcg.idParticipantPk = :idParticipantPk ");
		if (retirementObjetTO.getCurrency() != null)
			stringBuffer.append(" and dcg.currency = :currency ");
		if (retirementObjetTO.getGloss() != null)
			stringBuffer.append(" and dcg.glosaNotClassified = '" + retirementObjetTO.getGloss().trim() + "' ");

		if (retirementObjetTO.getState() != null)
			if (retirementObjetTO.getState().equals(Integer.valueOf(0)))
				stringBuffer.append(" and dcg.depositStatus is not null ");
			else
				stringBuffer.append(" and dcg.depositStatus = :state ");

		if (retirementObjetTO.getStateRetirement() != null)
			if (retirementObjetTO.getStateRetirement().equals(Integer.valueOf(0)))
				stringBuffer.append(" and dcg.transferStatus is not null ");
			else
				stringBuffer.append(" and dcg.transferStatus = :stateRetirement ");

		if (retirementObjetTO.getPaymentDate() != null)
			stringBuffer.append(" and trunc(dcg.paymentDate) = :paymentDate ");

		Query query = em.createQuery(stringBuffer.toString());

		if (retirementObjetTO.getIdParticipantPk() != null)
			query.setParameter("idParticipantPk", retirementObjetTO.getIdParticipantPk());
		if (retirementObjetTO.getCurrency() != null)
			query.setParameter("currency", retirementObjetTO.getCurrency());
		if (retirementObjetTO.getState() != null)
			if (!retirementObjetTO.getState().equals(Integer.valueOf(0)))
				query.setParameter("state", retirementObjetTO.getState());
		if (retirementObjetTO.getStateRetirement() != null)
			if (!retirementObjetTO.getStateRetirement().equals(Integer.valueOf(0)))
				query.setParameter("stateRetirement", retirementObjetTO.getStateRetirement());
		if (retirementObjetTO.getPaymentDate() != null)
			query.setParameter("paymentDate", retirementObjetTO.getPaymentDate());

		try {
			@SuppressWarnings("unchecked")
			List<DetailCollectionGloss> collectionGlossList = query.getResultList();
			return collectionGlossList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Buscando la glosa del participante del banco central
	 */
	public String getBcbCodeByIdParticipantPk(Long idParticipantPk) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select p.idBcbCode from Participant p ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and p.idParticipantPk = :idParticipantPk ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idParticipantPk", idParticipantPk);

		return (String) query.getSingleResult();
	}

	/**
	 * Buscando el participante segun codigo BCB
	 */
	public Participant getParticipantByCodeBcb(String idBcbCode) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select p from Participant p ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and p.idBcbCode = :idBcbCode ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idBcbCode", idBcbCode);

		try {
			return (Participant) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * Buscando el banco segun codigo BCB
	 */
	public Bank getBankByCodeBcb(String idBcbCode) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select b from Bank b ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and b.IdBcbCode = :idBcbCode ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idBcbCode", Long.parseLong(idBcbCode));

		try {
			return (Bank) query.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * Buscando el equibalente de la moneda
	 */
	public Integer getEquivalentCurrency(Integer currencyBcb) {
		StringBuilder stringBuffer = new StringBuilder();
		stringBuffer.append(" select pt.parameterTablePk from ParameterTable pt ");
		stringBuffer.append(" where 1 = 1 ");
		stringBuffer.append(" and pt.shortInteger = :shortInteger ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("shortInteger", currencyBcb);

		return (Integer) query.getSingleResult();

	}

}