package com.pradera.alert.service;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.edv.collection.economic.rigth.ReqCollectionEconomicRight;

public class AlertCollectionEconomicRightServiceBean extends CrudDaoServiceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** method to update a not submit in {@link ReqCollectionEconomicRight}
	 * 
	 * @param listIdRequestForUpdate */
	public void updateNoValidForSubmit(List<Long> listIdRequestForUpdate) {

		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("update ReqCollectionEconomicRight rcer set ");
		queryBuilder.append("rcer.submit = 0 ");
		queryBuilder.append("where rcer.idReqCollectionEconomicPk in(:listIdRequestForUpdate)");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("listIdRequestForUpdate", listIdRequestForUpdate);
		query.executeUpdate();
	}
	
	
	/**
	 * Busqueda del id del participante que hizo el registro
	 * */
	
	public Long searchIdParticipantPk(Long idReqCollectionEconomicPk){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select rce.participant.idParticipantPk ");
		queryBuilder.append(" from  ReqCollectionEconomicRight rce ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and rce.idReqCollectionEconomicPk = :idReqCollectionEconomicPk ");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idReqCollectionEconomicPk", idReqCollectionEconomicPk);
		Long idParticipantPk = (Long) query.getSingleResult();
		return idParticipantPk;
	}
}
