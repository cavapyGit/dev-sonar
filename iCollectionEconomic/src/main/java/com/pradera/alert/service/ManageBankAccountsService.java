package com.pradera.alert.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.BankAccountType;
import com.pradera.model.funds.type.BankAccountsStateType;

@Stateless
@Performance
public class ManageBankAccountsService extends CrudDaoServiceBean {
	
	
	/**
	 * Gets the institution bank account for lip.
	 *
	 * @param institution the institution
	 * @param idInstitution the id institution
	 * @param idCurrency the id currency
	 * @param accountNumber the account number
	 * @return the institution bank account for lip
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public InstitutionBankAccount getInstitutionBankAccountForLip(Integer institution, 
			Long idInstitution, Integer idCurrency, String accountNumber) throws ServiceException{
		InstitutionBankAccount institutionBankAccount = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT cs FROM InstitutionBankAccount cs WHERE cs.currency = :currency AND cs.state = :state ");
			sbQuery.append("  AND cs.participant.idParticipantPk = :idParticipantPk AND cs.bankAcountType = :idBankAcountType ");
			if(Validations.validateIsNotNullAndNotEmpty(accountNumber)){
				sbQuery.append("  AND cs.accountNumber = :accountNumber ");
			}			
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",  idCurrency);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("idParticipantPk", idInstitution);
			query.setParameter("idBankAcountType", new Long(BankAccountType.CURRENT.getCode()));
			if(Validations.validateIsNotNullAndNotEmpty(accountNumber)){
				query.setParameter("accountNumber", accountNumber);
			}			
			List<InstitutionBankAccount> lstInstitutionBankAccount = (ArrayList<InstitutionBankAccount>) query.getResultList();	
			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccount)){
				institutionBankAccount = new InstitutionBankAccount();
				institutionBankAccount = lstInstitutionBankAccount.get(0);
			}		
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
	@SuppressWarnings("unchecked")
	public InstitutionBankAccount getInstitutionBankAccountForTransfer(Long idBankPk,Integer currency) throws ServiceException{
		InstitutionBankAccount institutionBankAccount = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT cs FROM InstitutionBankAccount cs ");
			sbQuery.append(" where 1 = 1 ");
			sbQuery.append(" and cs.bank.idBankPk = :idBankPk ");
			sbQuery.append(" and cs.currency = :currency ");
			sbQuery.append(" and cs.state = :state ");
			
			Query query = em.createQuery(sbQuery.toString()); 
			query.setParameter("currency",currency);
			query.setParameter("state",  BankAccountsStateType.REGISTERED.getCode());
			query.setParameter("idBankPk", idBankPk);
			List<InstitutionBankAccount> lstInstitutionBankAccount = (ArrayList<InstitutionBankAccount>) query.getResultList();	
			if(Validations.validateListIsNotNullAndNotEmpty(lstInstitutionBankAccount)){
				institutionBankAccount = new InstitutionBankAccount();
				institutionBankAccount = lstInstitutionBankAccount.get(0);
			}		
			return institutionBankAccount;
		}catch(NoResultException ex){
			   return null;
		} catch(NonUniqueResultException ex){
			   return null;
		}	
	}
}
