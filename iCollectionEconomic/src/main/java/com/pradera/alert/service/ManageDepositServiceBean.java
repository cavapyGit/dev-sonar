package com.pradera.alert.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.business.to.LipMessageHistoryTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.LipMessageHistory;
import com.edv.collection.economic.rigth.FundsCollection;

//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2015.</li>
* </ul>
* 
* The Class ManageDepositServiceBean.
*
* @author PraderaTechnologies.
* @version 1.0 , 21-ago-2015
*/
@Stateless
@Performance
public class ManageDepositServiceBean extends CrudDaoServiceBean{
	/**
	 * Find Participant By Filters 
	 * @param filter
	 * @return
	 * @throws ServiceException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Participant findParticipantByFiltersCollection(Participant filter){
		Participant participant=new Participant();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(	" select new Participant( " +
							"	pa.idParticipantPk,	pa.description,	" + //0 1
							"	pa.mnemonic, pa.state)	" + // 2 3
							" from	Participant pa  where 1=1	" );
		if (Validations.validateIsNotNullAndPositive(filter.getState())) {
			sbQuery.append(" and pa.state = :state ");
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())) {
			sbQuery.append(" and pa.idParticipantPk = :partCode ");
		}
		if (StringUtils.isNotBlank(filter.getDescription())) {
			sbQuery.append(" and pa.description like :description ");
		}
		if (StringUtils.isNotBlank(filter.getMnemonic())) {
			sbQuery.append(" and pa.mnemonic like :mnemonic ");
		}
		if (StringUtils.isNotBlank(filter.getIdBcbCode())) {
			sbQuery.append(" and pa.idBcbCode = :idBcbCode ");
		}
		
		sbQuery.append(" order by pa.mnemonic ");
	
		Query query = em.createQuery(sbQuery.toString());
		if (Validations.validateIsNotNullAndPositive(filter.getState())) {
			query.setParameter("state", filter.getState());
		}
		if (Validations.validateIsNotNullAndPositive(filter.getIdParticipantPk())) {
			query.setParameter("partCode", filter.getIdParticipantPk());
		}
		if (StringUtils.isNotBlank(filter.getDescription())) {
			query.setParameter("description", "%"+filter.getDescription()+"%");
		}
		if (StringUtils.isNotBlank(filter.getMnemonic())) {
			query.setParameter("mnemonic", "%"+filter.getMnemonic()+"%");
		}			
		if (StringUtils.isNotBlank(filter.getIdBcbCode())) {
			query.setParameter("idBcbCode", filter.getIdBcbCode());
		}
		try {
			participant=(Participant) query.getSingleResult();
			return participant;
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * Buscamos a quien pagar
	 * */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Bank findBankByFiltersCollection (Participant filter) {
		//Buscamos un banco
		StringBuilder builder = new StringBuilder();
		builder.append(" select b  from Bank b ");
		builder.append(" where 1 = 1 ");
		builder.append(" and b.IdBcbCode = :idBcbCode ");
		//builder.append(" and b.description = :description ");
		
		Query query = em.createQuery(builder.toString());
		
		query.setParameter("idBcbCode", Long.parseLong(filter.getIdBcbCode()));
		//query.setParameter("description",filter.getDescription());
		
		Bank bank = (Bank) query.getSingleResult();
		
		return  bank;
	}
	
	
	public boolean isExistOperationNumber(Long numberRequest){
		StringBuilder builder = new StringBuilder();
		builder.append(" select f.idFundsCollectionPk  from FundsCollection f ");
		builder.append(" where 1 = 1 ");
		builder.append(" and f.numberRequest = :numberRequest ");
		Query query = em.createQuery(builder.toString());
		query.setParameter("numberRequest", numberRequest);
		List<Long>list = query.getResultList();
		if(list!=null&&list.size()>0)
			return true;
		else
			 return false;
					 
	}
	/**
	 * Gets the lip message history validate.
	 *
	 * @param objLipMessageHistoryTO the obj lip message history to
	 * @return the lip message history validate
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<LipMessageHistory> getLipMessageHistoryValidate(LipMessageHistoryTO objLipMessageHistoryTO) throws ServiceException{
		List<LipMessageHistory> lstLipMessageHistory = new ArrayList<LipMessageHistory>();		
		try {			
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" Select lmh ");
			stringBuilder.append(" from LipMessageHistory lmh ");			
			stringBuilder.append(" where 1=1 ");			
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getRequestNumber())){
				stringBuilder.append(" and lmh.requestNumber = :requestNumber " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getAccountNumber())){
				stringBuilder.append(" and lmh.accountNumber = :accountNumber " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationType())){
				stringBuilder.append(" and lmh.operationType = :opertionType " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getCurrency())){
				stringBuilder.append(" and lmh.currency = :currency " );
			}			
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getDescription())){
				stringBuilder.append(" and lmh.description = :description " );
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationDate())){
				stringBuilder.append(" and trunc(lmh.operationDate) = trunc(:operationDate) " );
			}			
			Query query = em.createQuery(stringBuilder.toString());	
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getRequestNumber())){
				query.setParameter("requestNumber", objLipMessageHistoryTO.getRequestNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getAccountNumber())){
				query.setParameter("accountNumber", objLipMessageHistoryTO.getAccountNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationType())){
				query.setParameter("opertionType", objLipMessageHistoryTO.getOperationType());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getCurrency())){
				query.setParameter("currency", objLipMessageHistoryTO.getCurrency());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getDescription())){
				query.setParameter("description", objLipMessageHistoryTO.getDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objLipMessageHistoryTO.getOperationDate())){
				query.setParameter("operationDate", objLipMessageHistoryTO.getOperationDate());
			}			
			lstLipMessageHistory = query.getResultList();
			return lstLipMessageHistory;			
		} catch(NoResultException ex){
			   return null;
		}
	}
}
