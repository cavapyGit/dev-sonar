package com.pradera.alert.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.commons.lang3.StringUtils;

import com.pradera.alert.webclient.SettlementServiceConsumer;
import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.funds.to.AditionalInformationSendType;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsConsultRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.LipMessageHistory;
import com.pradera.model.funds.type.OperationClassType;
import com.edv.collection.economic.rigth.FundsCollection;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class FundsComponentSingleton.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class FundsComponentSingleton extends CrudDaoServiceBean {

	@Inject
	private ManageDepositServiceBean manageDepositServiceBean;

	private final String FILTER_LABEL_COLLECTION = "COB_DER_ECO";

	/** The settlement service consumer. */
	@Inject
	private SettlementServiceConsumer settlementServiceConsumer;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Se obtiene el extracto mediante web service del banco central, los registros de todos los depositos o retiros se registran 
	 * en la tabla de funds_collection, al mismo tiempo se registra en la tabla de detail_collection_gloss.
	 * 
	 * Solo se confirman aquellos depositos que tienen la misma glosa y que el monto sea mayor o igual al monto requerido
	 * derechos economicos
	 * 
	 * @param objFundsTransferRegisterTO
	 *            Objeto de conulta o extracto par el LIP
	 * @param loggerUser
	 *            Datos de usuario
	 * */
	@Interceptors(ContextHolderInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void searchExtractByCurrency (
			FundsConsultRegisterTO objFundsTransferRegisterTO,
			LoggerUser loggerUser) throws ServiceException {
		/**Obtener objeto que tiene todos los movimientos*/
		List<AutomaticSendType> lstAutomaticSendType = settlementServiceConsumer.sendFundsExtractLip(objFundsTransferRegisterTO, loggerUser);
		if (Validations.validateListIsNotNullAndNotEmpty(lstAutomaticSendType)) {
			for (AutomaticSendType objAutomaticSendType : lstAutomaticSendType) {
				try {
					/*Se valida y retorna el objeto para registrar*/
					FundsCollection collection = new FundsCollection();
					collection.setOriginParticipantCode(objAutomaticSendType.getOriginParticipantCode());
					collection.setOriginParticipantDesc(objAutomaticSendType.getOriginParticipantDesc());
					collection.setOriginDepartment(objAutomaticSendType.getOriginDepartment());
					collection.setTargetParticipantCode(objAutomaticSendType.getTargetParticipantCode());
					collection.setTargetParticipantDesc(objAutomaticSendType.getTargetParticipantDesc());
					collection.setOriginBankAccountCode(objAutomaticSendType.getOriginBankAccountCode());
					collection.setOriginCurrency(Short.parseShort(objAutomaticSendType.getOriginCurrency()));
					collection.setCodTypeOperation(objAutomaticSendType.getCodTypeOperation());
					collection.setDescTypeOperation(objAutomaticSendType.getDescTypeOperation());
					collection.setNumberRequest(objAutomaticSendType.getNumberRequest());
					collection.setAmount(objAutomaticSendType.getAmount());
					collection.setRetirementAmount(objAutomaticSendType.getRetirementAmount());
					collection.setActualBalance(objAutomaticSendType.getActualBalance());
					collection.setPreviousBalance(objAutomaticSendType.getPreviousBalance());
					collection.setDateOperation(objAutomaticSendType.getDate());
					collection.setHourOperation(objAutomaticSendType.getHour());
					collection.setTextDescription(objAutomaticSendType.getTextDescription());
					//collection.setOperationState(StatesConstants.CONFIRMED_MATCH_GLOSSES);
					collection.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem().longValue());
					collection.setLastModifyDate(loggerUser.getAuditTime());
					collection.setLastModifyIp(loggerUser.getIpAddress());
					collection.setLastModifyUser(loggerUser.getUserName());
					for (AditionalInformationSendType aditionalInformationSendType : objAutomaticSendType.getLstAditionalInformationSendType()) {
						if (aditionalInformationSendType.getCode().equals(FILTER_LABEL_COLLECTION))
							collection.setGloss(aditionalInformationSendType.getValue());
						else
							collection.setGloss(aditionalInformationSendType.getValue());
						log.info("CDE::::"+aditionalInformationSendType.getCode()+" - "+aditionalInformationSendType.getValue());
					}
					/*Solo en caso de ser deposito COIN1 actualizamos la glosa*/
					String strOperationType = objAutomaticSendType.getCodTypeOperation();
					if (StringUtils.equalsIgnoreCase(ComponentConstant.TRANSFER_ACCOUNTING_FUNDS,strOperationType)) {
						String glossDeposit = searchGlossByDeposit(objAutomaticSendType.getTextDescription());
						collection.setGloss(glossDeposit);
						if(manageDepositServiceBean.isExistOperationNumber(collection.getNumberRequest()))
							continue;
						em.persist(collection);
						em.flush();
					}
					if (StringUtils.equalsIgnoreCase(ComponentConstant.TRANSFERS_WITH_OPEN_GLOSS_DEPOSIT,strOperationType)) {
						if(manageDepositServiceBean.isExistOperationNumber(collection.getNumberRequest()))
							continue;
						em.persist(collection);
						em.flush();
					}
				} catch (Exception e) {
					e.printStackTrace();
					log.info("CDE::::: No se pudo persistir los depositos con glosa");
				}
			}
		}
	}
	/**
	 * Persistencia del cobro o retiro obtenido mediante web services
	 * 
	 * @param objAutomaticSendType
	 *            Objeto que contiene los datos de retiro o deposito de la
	 *            consulta de extracto
	 * @param loggerUser
	 *            Datos de usuario
	 * @see #searchExtractByCurrency
	 **/
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Interceptors(ContextHolderInterceptor.class)
	private RetirementObjetTO getOperationDepositCollectionEconomic(AutomaticSendType objAutomaticSendType, LoggerUser loggerUser)
			throws ServiceException {
		/* En caso de que se un participante el que nos deposite */
		Participant participantSearch = null;
		/* En caso de que sea un banco el que nos deposite */
		Bank bankSearch = null;
		String strOperationType = objAutomaticSendType.getCodTypeOperation();
		Participant participantFilter = new Participant();
		participantFilter.setIdBcbCode(objAutomaticSendType.getOriginParticipantCode());
		participantSearch = manageDepositServiceBean.findParticipantByFiltersCollection(participantFilter);// ObjectByNamedQuery(Participant.PARTICIPANT_ID_BCB_CODE,
		// verificando la existencia del participante con el Codigo BCB
		if (participantSearch != null&&objAutomaticSendType.getOriginParticipantCode()!=null) {
			objAutomaticSendType.setOriginParticipantCode(participantSearch.getMnemonic());
			objAutomaticSendType.setMnemonicParticipant(participantSearch.getMnemonic());
		} else {
			try {
				if(Validations.validateIsNotNullAndNotEmpty(participantFilter.getIdBcbCode())){
					bankSearch = manageDepositServiceBean.findBankByFiltersCollection(participantFilter);
					objAutomaticSendType.setMnemonicParticipant(bankSearch.getMnemonic());
				}
			} catch (Exception e) {
				log.info("CDE ::::  No se encontro banco/participante en el sistema.");
			}
		}
		objAutomaticSendType.setIndRecepFunds(ComponentConstant.ZERO);
		Integer currency = null;
		// OBTENER LA MONEDA DE LA TRANSFERENCIA
		if (Validations.validateIsNotNullAndNotEmpty(objAutomaticSendType.getOriginCurrency())) {
			if(objAutomaticSendType.getOriginCurrency().equals("69"))
				currency = 127;
			if(objAutomaticSendType.getOriginCurrency().equals("34"))
				currency = 430;
		}
		// GENERAR OPERACIONES DE DEPOSITO SI ES OPERACION : E77
		if (StringUtils.equalsIgnoreCase(ComponentConstant.TRANSFERS_WITH_OPEN_GLOSS_DEPOSIT,strOperationType)) {
		
			RetirementObjetTO depositObjet = new RetirementObjetTO();
			depositObjet.setCurrency(currency);
			for (AditionalInformationSendType aditionalInformationSendType : objAutomaticSendType.getLstAditionalInformationSendType()) {
				if (aditionalInformationSendType.getCode().equals(FILTER_LABEL_COLLECTION)) {
					depositObjet.setAmount(objAutomaticSendType.getAmount());
					depositObjet.setGloss(aditionalInformationSendType.getValue());
				}
				log.info("CDE::::"+aditionalInformationSendType.getCode()+" - "+aditionalInformationSendType.getValue());
			}
			return depositObjet;
		}
		/**
		 * Transferencia de fondos por parte del banco central*/
		if (StringUtils.equalsIgnoreCase(ComponentConstant.TRANSFER_ACCOUNTING_FUNDS,strOperationType)) {
			RetirementObjetTO depositObjet = new RetirementObjetTO();
			depositObjet.setCurrency(currency);
			depositObjet.setAmount(objAutomaticSendType.getAmount());
			String glossDeposit = searchGlossByDeposit(objAutomaticSendType.getTextDescription());
			log.info("CDE::: Glosa encontrada:"+glossDeposit);
			AditionalInformationSendType aditionalInformationSendType = new AditionalInformationSendType();
			aditionalInformationSendType.setCode(FILTER_LABEL_COLLECTION);
			aditionalInformationSendType.setValue(glossDeposit);
			depositObjet.setGloss(glossDeposit);
			
			List<AditionalInformationSendType> list = new ArrayList<>();
			list.add(aditionalInformationSendType);
			objAutomaticSendType.setLstAditionalInformationSendType(list);
			
			log.info("CDE::::"+aditionalInformationSendType.getCode()+" - "+aditionalInformationSendType.getValue());
			return depositObjet;
		}
		return null;
	}
	/**
	 * Buscamos la glosa de deposito en la descripsion
	 * */
	private  String searchGlossByDeposit(String description){
		StringBuffer refSearchPRE = new StringBuffer();
		refSearchPRE.append("(PRE");
		
		StringBuffer refSearchFTB = new StringBuffer();
		refSearchFTB.append("(FTB");
		
		StringBuffer refSearchGPS = new StringBuffer();
		refSearchGPS.append("(GPS");
		
		String gloss = null;
		
		if(description.contains(refSearchPRE.toString())){
			log.info("CDE::: Parametros de busqueda encontrada para PRE");
			/**buscando la poscicion de la glosa*/
			int pos1 = description.indexOf(refSearchPRE.toString());
			int pos2 = description.indexOf(")",pos1);
			gloss = description.substring(pos1+1, pos2);
		}
		if(description.contains(refSearchFTB.toString())){
			log.info("CDE::: Parametros de busqueda encontrada para FTB");
			/**buscando la poscicion de la glosa*/
			int pos1 = description.indexOf(refSearchFTB.toString());
			int pos2 = description.indexOf(")",pos1);
			gloss = description.substring(pos1+1, pos2);
		}
		if(description.contains(refSearchGPS.toString())){
			log.info("CDE::: Parametros de busqueda encontrada para GPS");
			/**buscando la poscicion de la glosa*/
			int pos1 = description.indexOf(refSearchGPS.toString());
			int pos2 = description.indexOf(")",pos1);
			gloss = description.substring(pos1+1, pos2);
		}
		/**Buscamos nuevamente en caso de no encontrar la glosa*/
		if(gloss==null){
			refSearchPRE = new StringBuffer();
			refSearchPRE.append("PRE");
			
			refSearchFTB = new StringBuffer();
			refSearchFTB.append("FTB");
			
			refSearchGPS = new StringBuffer();
			refSearchGPS.append("GPS");
			
			if(description.contains(refSearchPRE.toString())){
				log.info("CDE::: Parametros de busqueda encontrada para PRE");
				/**buscando la poscicion de la glosa*/
				int pos1 = description.indexOf(refSearchPRE.toString());
				int pos2 = description.indexOf("-",pos1);
				String stringGloss = description.substring(pos2+1,pos2+10);
				stringGloss = StringUtils.deleteWhitespace(stringGloss);
				return searchNumberGloss(stringGloss, refSearchPRE);
			}
			if(description.contains(refSearchFTB.toString())){
				log.info("CDE::: Parametros de busqueda encontrada para FTB");
				/**buscando la poscicion de la glosa*/
				int pos1 = description.indexOf(refSearchFTB.toString());
				int pos2 = description.indexOf("-",pos1);
				String stringGloss = description.substring(pos2+1,pos2+10);
				stringGloss = StringUtils.deleteWhitespace(stringGloss);
				return searchNumberGloss(stringGloss, refSearchFTB);
			}
			if(description.contains(refSearchGPS.toString())){
				log.info("CDE::: Parametros de busqueda encontrada para GPS");
				/**buscando la poscicion de la glosa*/
				int pos1 = description.indexOf(refSearchGPS.toString());
				int pos2 = description.indexOf("-",pos1);
				String stringGloss = description.substring(pos2+1,pos2+10);
				stringGloss = StringUtils.deleteWhitespace(stringGloss);
				return searchNumberGloss(stringGloss, refSearchGPS);
			}
		}
		return gloss;
	}
	
	/**
	 * Busqueda de glosa
	 * **/
	private String searchNumberGloss(String stringGloss,StringBuffer glossParticipant){
		StringBuilder glossNumber = new StringBuilder();
		char c;
		for (int i = 0; i < stringGloss.length(); i++) {
			c = stringGloss.charAt(i);
			log.info("CDE::: Caracter descompuesto:"+c);
			try {
				Integer.parseInt(c+"");
				glossNumber.append(c);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		/*Buscar glosa en texto restante*/
		String gloss = glossParticipant.append("-"+glossNumber).toString();
		return gloss;
	}
	/**
	 * Busqueda de los depositos
	 * 
	 * @see FundsOperation
	 * @param searchFilterMonitorTO
	 *            Filtros de busqueda
	 * @return {@link List}
	 **/
	public List<FundsOperation> getDepositsOfCollections(String glosaNotClassified, Integer currency, Date paymentDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select fo from FundsOperation fo ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and fo.depositGloss = :depositGloss ");
		// queryBuilder.append(" and fo.operationAmount =  :amount ");
		queryBuilder
				.append(" and fo.fundsOperationType = :fundsOperationType ");
		queryBuilder
				.append(" and fo.fundsOperationClass = :fundsOperationClass ");

		queryBuilder.append(" and fo.currency = :currency ");
		// queryBuilder.append(" and trunc(fo.registryDate) = :registryDate ");
		queryBuilder.append(" order by fo.idFundsOperationPk desc ");

		javax.persistence.Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("depositGloss", glosaNotClassified);
		// query.setParameter("amount", amount);
		query.setParameter("fundsOperationType",
				ParameterFundsOperationType.OPERATIONS_COLLECTION_DEPOSIT
						.getCode().longValue());
		query.setParameter("fundsOperationClass",
				OperationClassType.RECEPTION_FUND.getCode());

		query.setParameter("currency", currency);
		// query.setParameter("registryDate", paymentDate);

		@SuppressWarnings("unchecked")
		List<FundsOperation> fundsOperationList = query.getResultList();
		return fundsOperationList;
	}

	/**
	 * Save lip message history.
	 * 
	 * @param objAutomaticSendType
	 *            the obj automatic send type
	 * @param currency
	 *            the currency
	 * @throws ServiceException
	 *             the service exception
	 */
	@LoggerAuditWeb
	public void saveLipMessageHistory(AutomaticSendType objAutomaticSendType,
			Integer currency) throws ServiceException {
		LipMessageHistory objLipMessageHistory = new LipMessageHistory();
		objLipMessageHistory.setAccountNumber(objAutomaticSendType
				.getOriginBankAccountCode());
		objLipMessageHistory.setCurrency(currency);

		Long id = null;
		try {
			id = new Long(objAutomaticSendType.getOriginParticipantCode());
			objLipMessageHistory.setParticipantSourceCode(id);
		} catch (Exception e) {
			Participant filter = new Participant();
			filter.setMnemonic(objAutomaticSendType.getOriginParticipantCode());
			Participant participant = manageDepositServiceBean
					.findParticipantByFiltersCollection(filter);
			if (participant != null)
				objLipMessageHistory.setParticipantSourceCode(participant.getIdParticipantPk());
		}
		objLipMessageHistory.setDepartamentCodeSource(objAutomaticSendType.getOriginDepartment());
		objLipMessageHistory.setPartSourceDescription(objAutomaticSendType.getOriginParticipantDesc());
		objLipMessageHistory.setParticipantTargetCode(new Long(objAutomaticSendType.getTargetParticipantCode()));
		objLipMessageHistory.setDepartamentCodeTarget(objAutomaticSendType.getTargetDepartment());
		objLipMessageHistory.setPartTargetDescription(objAutomaticSendType.getTargetParticipantDesc());
		objLipMessageHistory.setOperationDate(objAutomaticSendType.getDate());
		objLipMessageHistory.setOperationType(objAutomaticSendType.getCodTypeOperation());
		objLipMessageHistory.setOperTypeDescription(objAutomaticSendType.getDescTypeOperation());
		objLipMessageHistory.setRequestNumber(objAutomaticSendType.getNumberRequest());
		objLipMessageHistory.setIndRecepFunds(objAutomaticSendType.getIndRecepFunds());
		objLipMessageHistory.setOperationHour(objAutomaticSendType.getHour());
		if (ComponentConstant.LIP_BCB_OPERATION_TYPE_E02
				.compareTo(objLipMessageHistory.getOperationType()) == 0
				|| ComponentConstant.LIP_BCB_OPERATION_TYPE_E73
						.compareTo(objLipMessageHistory.getOperationType()) == 0) {
			objLipMessageHistory.setTransactionAmount(objAutomaticSendType
					.getAmount());
			objLipMessageHistory.setDescription(objAutomaticSendType
					.getMnemonicParticipant());
		} else if (ComponentConstant.LIP_BCB_OPERATION_TYPE_E03
				.compareTo(objLipMessageHistory.getOperationType()) == 0) {
			objLipMessageHistory.setTransactionAmount(objAutomaticSendType.getRetirementAmount());
			objLipMessageHistory.setDescription(objAutomaticSendType.getTextDescription());
		}
		manageDepositServiceBean.create(objLipMessageHistory);
	}
}
