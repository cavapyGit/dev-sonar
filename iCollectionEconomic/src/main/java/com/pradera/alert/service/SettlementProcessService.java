package com.pradera.alert.service;

import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.funds.to.FundsConsultRegisterTO;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

@Stateless
public class SettlementProcessService extends CrudDaoServiceBean{
	
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/**
	 * Generate funds transfer register to.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the funds transfer register to
	 * @throws ServiceException the service exception
	 */
	public FundsTransferRegisterTO generateFundsTransferRegisterTO(FundsTransferRegisterTO objFundsTransferRegisterTO, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
		objFundsTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		Long idInterfaceTransaction = interfaceComponentServiceBean.get().saveInterfaceTransactionTx(objFundsTransferRegisterTO, BooleanType.YES.getCode(), loggerUser);
		objFundsTransferRegisterTO.setIdInterfaceTransaction(idInterfaceTransaction);
		return objFundsTransferRegisterTO;
	}
	
	/**
	 * Para cobro de derechos economicos
	 * */
	public FundsConsultRegisterTO generateFundsTransferRegisterTO(FundsConsultRegisterTO objFundsTransferRegisterTO, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
		objFundsTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		Long idInterfaceTransaction = interfaceComponentServiceBean.get().saveInterfaceTransactionTx(objFundsTransferRegisterTO, BooleanType.YES.getCode(), loggerUser);
		objFundsTransferRegisterTO.setIdInterfaceTransaction(idInterfaceTransaction);
		return objFundsTransferRegisterTO;
	}
}
