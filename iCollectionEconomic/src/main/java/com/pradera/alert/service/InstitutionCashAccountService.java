package com.pradera.alert.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.type.CashAccountStateType;

@Stateless
@Performance
public class InstitutionCashAccountService extends CrudDaoServiceBean{
	
	/**
	 * Gets the participant send bcb information.
	 *
	 * @param institutionCashAccountPk the institution cash account pk
	 * @return the participant send bcb information
	 */
	@SuppressWarnings("unchecked")
	public List<InstitutionCashAccount> getParticipantSendBCBInformation(Long institutionCashAccountPk){
		List<InstitutionCashAccount> lstSettlementAccounts = new ArrayList<InstitutionCashAccount>();		
		String queryStr = "Select ica"
				+ " from InstitutionCashAccount ica "
				+ " left join fetch ica.cashAccountDetailResults cad "
				+ " left join fetch cad.institutionBankAccount iba "
				+ " left join fetch ica.issuer isu "
				+ " left join fetch ica.participant part "
				+ " left join fetch iba.bank pba "
				+ " where "
				+ " ica.idInstitutionCashAccountPk = :idInstitutionCashAccount " 
				+ " and ica.accountState = :state "
				+ " and cad.indSending = :indSending";
		Query query = em.createQuery(queryStr);
		query.setParameter("state",CashAccountStateType.ACTIVATE.getCode());
		query.setParameter("idInstitutionCashAccount",institutionCashAccountPk);
		query.setParameter("indSending",GeneralConstants.ONE_VALUE_INTEGER);
		lstSettlementAccounts = query.getResultList();

		return lstSettlementAccounts;
	}

}
