package com.pradera.alert.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.alert.facade.ManageDepositServiceFacade;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.component.funds.to.FundsConsultRegisterTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;


@RequestScoped
@BatchProcess(name="ReceiptCollectionEconomicRightBatch")
public class ReceiptCollectionEconomicRightBatch implements Serializable, JobExecution{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7358505159506291113L;
	@Inject
	ManageDepositServiceFacade manageDepositServiceFacade;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		List<FundsConsultRegisterTO> list = new ArrayList<>();
		
		FundsConsultRegisterTO consultRegisterTO_BOB = null,consultRegisterTO_USD = null;
		try {
			/*Preparar archivos de envio*/
			consultRegisterTO_BOB = manageDepositServiceFacade.prepareBob();
		} catch (Exception e) {
			//e.printStackTrace();
			processLogger.setLoggerState(1368);
			processLogger.setErrorDetail("No existe deposito en moneda BOB");
		}
		try {
			list.add(consultRegisterTO_BOB);
			manageDepositServiceFacade.sendRequestExtract(list);
		} catch (ServiceException e) {
			//e.printStackTrace();
			processLogger.setLoggerState(1368);
			processLogger.setErrorDetail("No existe deposito en moneda BOB");
		}
		////////////////USD///////////////////////////
		try {
			consultRegisterTO_USD = manageDepositServiceFacade.prepareUsd();
		} catch (Exception e) {
			//e.printStackTrace();
			processLogger.setLoggerState(1368);
			processLogger.setErrorDetail("No existe deposito en moneda USD");
		}
		try {
			list = new ArrayList<>();
			list.add(consultRegisterTO_USD);
			manageDepositServiceFacade.sendRequestExtract(list);
		} catch (ServiceException e) {
			//e.printStackTrace();
			processLogger.setLoggerState(1368);
			processLogger.setErrorDetail("No existe deposito en moneda USD");
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
