package com.pradera.alert.batch;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;


@RequestScoped
@BatchProcess(name="RetirementCollectionEconomicRightBatch")
public class RetirementCollectionEconomicRightBatch  implements Serializable, JobExecution{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2556597264407980508L;
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		/*
		Long idParticipantPk = null;
		Date paymentDate = null;
		Integer currency =  null;
		BigDecimal amount =  null;
		String gloss = null;
		String userName = null;
		
		List<ProcessLoggerDetail> lstProcessLoggerDetails = processLogger.getProcessLoggerDetails();
		for (ProcessLoggerDetail processLoggerDetail : lstProcessLoggerDetails) {
			if(processLoggerDetail.getParameterName().equals(PropertiesConstants.PARAM_ID_PARTICIPANT_PK))
				idParticipantPk = Long.parseLong(processLoggerDetail.getParameterValue());
			if(processLoggerDetail.getParameterName().equals(PropertiesConstants.PARAM_PAYMENT_DATE))
				paymentDate = CommonsUtilities.convertStringtoDate(processLoggerDetail.getParameterValue());
			if(processLoggerDetail.getParameterName().equals(PropertiesConstants.PARAM_CURRENCY))
				currency = Integer.parseInt(processLoggerDetail.getParameterValue());
			if(processLoggerDetail.getParameterName().equals(PropertiesConstants.PARAM_AMOUNT))
				amount = new BigDecimal(processLoggerDetail.getParameterValue());
			if(processLoggerDetail.getParameterName().equals(PropertiesConstants.PARAM_GLOSS))
				gloss = processLoggerDetail.getParameterValue();
			if(processLoggerDetail.getParameterName().equals(PropertiesConstants.PARAM_USER_NAME))
				userName = processLoggerDetail.getParameterValue();
		}
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setIdParticipantPk(idParticipantPk);
		retirementObjetTO.setPaymentDate(paymentDate);
		retirementObjetTO.setCurrency(currency);
		retirementObjetTO.setAmount(amount);
		retirementObjetTO.setGloss(gloss);
		retirementObjetTO.setUserName(userName);
		
		try {
			manageDepositServiceFacade.registerTransferOperation(retirementObjetTO);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
