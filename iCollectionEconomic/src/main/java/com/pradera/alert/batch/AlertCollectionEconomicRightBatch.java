package com.pradera.alert.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.alert.facade.AlertCollectionEconomicRightFacadeBean;
import com.pradera.alertt.bath.to.SecuritiesNotifyTO;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;

@BatchProcess(name = "AlertCollectionEconomicRightBatch")
@RequestScoped
public class AlertCollectionEconomicRightBatch implements JobExecution, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private PraderaLogger log;

	@Inject
	private AlertCollectionEconomicRightFacadeBean alertFacadeBean;

	@Inject
	private NotificationServiceFacade notificationServiceFacade;
	
	/**
	 * Batchero de alerta de cobro de derechos economicos
	 * */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info("::: Iniciando batchero para Alerta de Cobro de Derechos Economicos Pendientes");
		
		Date paymentDate  = CommonsUtilities.currentDate();
		String paymentDateString = CommonsUtilities.convertDatetoString(paymentDate);
		
		/**Alerta de valores que no esten registrados en ninguna solicitud*/
		 List<String> listSecuritiesNotRegistre = alertFacadeBean.notifySecuritiesNotRegisteredInCollections(processLogger, paymentDate, paymentDate);
		 BusinessProcess businessProcess = new BusinessProcess();
		 businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_UNREGISTERED_SECURITIES.getCode());
		 Object[] parameters = {listSecuritiesNotRegistre.toString(),paymentDateString};
		 notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null, parameters);
		 //processLogger.setErrorDetail(message,);
		 /**Vencimientos que estan registrados en cobro pero que ya no esten en cartera*/
		 List<SecuritiesNotifyTO> securitiesNotifyTOs = alertFacadeBean.notifySecuritiesWithUnavailableBalance(processLogger, paymentDate, paymentDate);
		 businessProcess = new BusinessProcess();
		 businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_NOT_AVALAILABLE_BALANCE.getCode());
		 for (SecuritiesNotifyTO securitiesNotifyTO : securitiesNotifyTOs) {
			 Object[] parametersNotAvailable = {securitiesNotifyTO.getListSecuritiesDifferentBalance().toString(),paymentDateString,securitiesNotifyTO.getParticipant().getDescription()};
			 notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null, parametersNotAvailable);
		}
		 /**Notificacion de valores con saldo disponible menor al registrado en las solicitudes de cobros
		  * Todo ya se encuentran en la notificacion de cobro de derechos economicos*/
		 /***/
		 log.info("::: Fin batchero para Alerta de Cobro de Derechos Economicos Pendientes");
	}
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	@Override
	public boolean sendNotification() {
		return false;
	}

}
