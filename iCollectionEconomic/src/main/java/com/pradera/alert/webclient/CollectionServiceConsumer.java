package com.pradera.alert.webclient;

import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

//import org.jboss.solder.resourceLoader.Resource;

import bo.com.edv.www.ServiciosClienteBCBProxy;
//import lombok.NonNull;

import com.pradera.alert.service.ManageBankAccountsService;
import com.pradera.alert.service.ManageRetirementServiceBean;
import com.pradera.alertt.bath.to.InstitutionCashBankAccountTO;
import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.alertt.bath.to.XmlObjectResponse;
import com.pradera.collection.monitor.to.BalanceReponseXmlTO;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.funds.to.AditionalInformationSendType;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.constant.FundsAutomaticConstants;

@Stateless
@Performance
public class CollectionServiceConsumer implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3949738705400944466L;

	@Inject
	private PraderaLogger log;
		
	//@Inject
	//@Resource("ApplicationConfiguration.properties")
	//private Properties applicationConfiguration;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	@Inject
	private ManageRetirementServiceBean manageRetirementServiceBean;
	
	@Inject
	private ManageBankAccountsService accountsService;
	

	private String strServicesConsumerBCB = GeneralConstants.EMPTY_STRING;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strServicesConsumerBCB = null;// applicationConfiguration.getProperty("bcb.lip.web.services.consumer");
	}
	
	/**
	 * Construyendo el objeto de tranferencia/retiro
	 * @param objFundsTransferRegisterTO Obejeto de tranferencia para depositos LIP
	 * @return {@link AutomaticSendType} Obejto de retiro o deposito
	 * */
	public synchronized XmlObjectResponse sendTransferForCollections(FundsTransferRegisterTO objFundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException{
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy = new ServiciosClienteBCBProxy(strServicesConsumerBCB);				
		ProcessFileTO objProcessFileTO = null;
		String strXmlReception=null, strXmlSend=null;
		XmlObjectResponse lipResponseCollection = null;
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.TRANSFERS_WITH_OPEN_GLOSS, null, "retirement_send_collection.xml", Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");				
				if (Validations.validateIsNotNull(objFundsTransferRegisterTO)) {
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					objFundsTransferRegisterTO.getAutomaticSendTypeTO().setCodTypeOperation(ComponentConstant.TRANSFERS_WITH_OPEN_GLOSS);
					byte [] arrXmlSend = CommonsUtilities.generateInterfaceLipResponseFile(objFundsTransferRegisterTO.getAutomaticSendTypeTO(),objProcessFileTO.getStreamFileDir(), ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.INTERFACE_LIP_ROW);
					strXmlSend = CommonsUtilities.bytesToString(arrXmlSend);
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					strXmlReception = objServiciosClienteBCBProxy.enviarProcesarTransferencia(strXmlSend);
					log.info("CDE ::::: XML DE RESPUESTA:"+strXmlReception);
					if(Validations.validateIsNotNullAndNotEmpty(strXmlReception)){
						log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
						objProcessFileTO.setRootTag(ComponentConstant.EXTRACT_LIP_RESP);
						lipResponseCollection = new XmlObjectResponse();
						JAXBContext jaxbContext = JAXBContext.newInstance(XmlObjectResponse.class);
						Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
						StringReader reader = new StringReader(strXmlReception);
						lipResponseCollection = (XmlObjectResponse) unmarshaller.unmarshal(reader);
					} 
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
		return lipResponseCollection;
	}
	//@NonNull 
	public BigDecimal verifyBalance(Integer currency) throws RemoteException {
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy = new ServiciosClienteBCBProxy(strServicesConsumerBCB);
		String xml = "<saldo>" + 
				"				<cod_tipo_operacion>"+ComponentConstant.INTERFACE_BCB_LIP_BALANCE+"</cod_tipo_operacion>" + 
				"				<cod_participante>1424</cod_participante>" + 
				"				<cod_cuenta>8046</cod_cuenta>" + 
				"				<cod_moneda>"+currency+"</cod_moneda>" + 
				"				<fecha>"+CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),CommonsUtilities.DATE_PATTERN)+"</fecha>" + 
				"		 </saldo>";
		BalanceReponseXmlTO balanceReponseXmlTO = new BalanceReponseXmlTO();
		
		String xmlResponse = objServiciosClienteBCBProxy.enviarSolicitarSaldo(xml);
		
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(BalanceReponseXmlTO.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xmlResponse);
			balanceReponseXmlTO = (BalanceReponseXmlTO) unmarshaller.unmarshal(reader);
			BigDecimal s = new BigDecimal(balanceReponseXmlTO.getSaldo());
			return s;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**Llenamos datos de operacion
	 * @throws ServiceException */
	public AutomaticSendType getObjectRetirement(AutomaticSendType automaticSendType ,RetirementObjetTO retirementObjetTO) throws ServiceException{
		automaticSendType.setCodeParticipant(FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB);
		
		InstitutionBankAccount bankAccount = accountsService.getInstitutionBankAccountForTransfer(retirementObjetTO.getIdBankPk(), retirementObjetTO.getCurrency());
		Bank bank = accountsService.find(Bank.class,retirementObjetTO.getIdBankPk());
		
		automaticSendType.setTargetParticipantCode(bank.getIdBcbCode().toString());  //institutionBankAccountSearch
		automaticSendType.setTargetBankAccountCode(bankAccount.getAccountNumber());
		automaticSendType.setTargetDepartment(FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB);
		automaticSendType.setAmount(retirementObjetTO.getAmount());
		automaticSendType.setSettlementType(FundsAutomaticConstants.SETTLEMENT_TYPE_GROSS);
		automaticSendType.setPriority("1");
		automaticSendType.setCreditEntity(FundsAutomaticConstants.CREDIT_ENTITY);
		automaticSendType.setTextDescription(FundsAutomaticConstants.RETIREMENT_COLLECTION_ECONOMIC);
		automaticSendType.setUserCode(retirementObjetTO.getUserName());
		
		//Llenado de datos adicionales
		List<AditionalInformationSendType> lstAditionalInformationSendType = new ArrayList<>();
		AditionalInformationSendType aditionalInformationSendType = null;
		
		aditionalInformationSendType = new AditionalInformationSendType();
		aditionalInformationSendType.setCode("BENEFICIARIO");
		Holder holder = accountsService.find(Holder.class, retirementObjetTO.getIdHolderPk());
		aditionalInformationSendType.setValue(holder.getFullName());
		lstAditionalInformationSendType.add(aditionalInformationSendType);
		
		
		aditionalInformationSendType = new AditionalInformationSendType();
		aditionalInformationSendType.setCode("NRO_CUENTA");
		aditionalInformationSendType.setValue(retirementObjetTO.getAccountNumber());
		lstAditionalInformationSendType.add(aditionalInformationSendType);
		
		aditionalInformationSendType = new AditionalInformationSendType();
		aditionalInformationSendType.setCode("CONCEPTO");
		aditionalInformationSendType.setValue("PAGO POR DERECHOS ECONOMICOS");
		lstAditionalInformationSendType.add(aditionalInformationSendType);
		
		aditionalInformationSendType = new AditionalInformationSendType();
		aditionalInformationSendType.setCode("SIGLA_FONDO");
		aditionalInformationSendType.setValue("");
		lstAditionalInformationSendType.add(aditionalInformationSendType);
		
		automaticSendType.setLstAditionalInformationSendType(lstAditionalInformationSendType);
		return automaticSendType;
	}
	
	/**
	 * Busqueda de la cuenta bancaria en cuentas del LIP BCB
	 * @param retirementObjetTO Objeto de retiro
	 * @return Retornamos la cuenta LIP y su codigo BCB**/
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public InstitutionCashBankAccountTO searchInstitutionCashBankAccount(RetirementObjetTO retirementObjetTO){
		InstitutionCashAccount cashAccount = null;
		
		cashAccount = new InstitutionCashAccount();
		cashAccount.setParticipant(new Participant(retirementObjetTO.getIdParticipantPk()));
		cashAccount.setCurrency(retirementObjetTO.getCurrency());
		
		cashAccount = manageRetirementServiceBean.getActivatedCashAccount(cashAccount);
		
		if(cashAccount==null){
			return null;
		}
		InstitutionCashBankAccountTO cashBankAccountTO = new InstitutionCashBankAccountTO();
		try {
			Long idInstitutionBankAccountPk = manageRetirementServiceBean.searhIdInstitutionBankAccountPk(cashAccount.getIdInstitutionCashAccountPk());
			
			InstitutionBankAccount institutionBankAccount = manageRetirementServiceBean.find(InstitutionBankAccount.class, idInstitutionBankAccountPk);
			
			cashBankAccountTO.setAccountNumberLIp(institutionBankAccount.getAccountNumber());
			
			if(institutionBankAccount.getIdBcbCode()==null){
				/*Busqueda de banco*/
				cashBankAccountTO.setBcbCode(manageRetirementServiceBean.getCodBcbForbank(idInstitutionBankAccountPk));
			}else
				cashBankAccountTO.setBcbCode(institutionBankAccount.getIdBcbCode().toString());
		} catch (NoResultException e) {
			e.printStackTrace();
		} catch (EJBException e) {
			e.printStackTrace();
		} 
		return cashBankAccountTO;
	}
	
	/**Numero correlativo de registro
	 * @param string id de proceso
	 * @return numero correlativo de proceso*/
	public Long getNextCorrelativeOperations(String string){
		return manageRetirementServiceBean.getNextCorrelativeOperations(string);
	}
}
