package com.pradera.alert.webclient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import bo.com.edv.www.ServiciosClienteBCBProxy;

import com.pradera.alert.facade.ManageLipServiceFacade;
import com.pradera.alert.service.SettlementProcessService;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsConsultRegisterTO;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SettlementServiceConsumer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */
@Stateless
public class SettlementServiceConsumer implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	private PraderaLogger log;
	
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	/** The server configuration. */		
	//@Inject
	//@Resource("ApplicationConfiguration.properties")
	//Properties applicationConfiguration;
	
	/** The str services consumer EDV. */
	String strServicesConsumerEDV = GeneralConstants.EMPTY_STRING;
	
	/** The str services consumer EDV. */
	String strServicesConsumerBCB = GeneralConstants.EMPTY_STRING;
	
	@Inject
	private SettlementProcessService settlementProcessService;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strServicesConsumerEDV = null;//applicationConfiguration.getProperty("eif.banks.web.services.consumer");
		strServicesConsumerBCB = null;//applicationConfiguration.getProperty("bcb.lip.web.services.consumer");
	}
	
	
	/**
	 * Para cobro de derechos economicos
	 * */
	public List<AutomaticSendType> sendFundsExtractLip(FundsConsultRegisterTO objFundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException{
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy = new ServiciosClienteBCBProxy(strServicesConsumerBCB);				
		ProcessFileTO objProcessFileTO = null;
		String strXmlReception=null, strXmlSend=null;
		List<AutomaticSendType> lstAutomaticSendType = null;
		try {
			//validacion de archivo
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_BCB_LIP_EXTRACT, null, "funds_extract_collection.xml", Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");				

				if (Validations.validateIsNotNull(objFundsTransferRegisterTO)) {
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					byte [] arrXmlSend = CommonsUtilities.generateInterfaceLipResponseFile(objFundsTransferRegisterTO.getAutomaticSendTypeTO(), 
							objProcessFileTO.getStreamFileDir(), ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.EXTRACT_LIP_ROW);
					
					strXmlSend = CommonsUtilities.bytesToString(arrXmlSend);
					
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					strXmlReception = objServiciosClienteBCBProxy.enviareSolicitarExtracto(strXmlSend);

					/*----------------------------------*/
					/*
					BufferedReader buf = new BufferedReader(new InputStreamReader(is));
					String line = buf.readLine();
					StringBuilder sb = new StringBuilder();
					
					while(line != null){
						   sb.append(line).append("\n");
						   line = buf.readLine();
						}
					strXmlReception = sb.toString();
					*/
					/*----------------------------------*/
					
					if(Validations.validateIsNotNullAndNotEmpty(strXmlReception)){
						log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
						lstAutomaticSendType = new ArrayList<AutomaticSendType>();
						objProcessFileTO.setRootTag(ComponentConstant.EXTRACT_LIP_RESP);
						
						objProcessFileTO.setStreamFileDir("schema/edv/funds_extract_collection.xml");
						objProcessFileTO.setStreamResponseFileDir("schema/edvresponse/funds_extract_collection.xml");
						
						lstAutomaticSendType = parameterServiceBean.getResponseFileLip(strXmlReception, objProcessFileTO);
					} 
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
		return lstAutomaticSendType;
	}
	/**
	 * Send funds extract lip.
	 *
	 * @param objFundsTransferRegisterTO the obj funds transfer register to
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AutomaticSendType> sendFundsExtractLip(FundsTransferRegisterTO objFundsTransferRegisterTO, LoggerUser loggerUser) throws ServiceException{
		ServiciosClienteBCBProxy objServiciosClienteBCBProxy = new ServiciosClienteBCBProxy(strServicesConsumerBCB);				
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();				
		ProcessFileTO objProcessFileTO = null;
		String strXmlReception=null, strXmlSend=null;
		List<AutomaticSendType> lstAutomaticSendType = null;
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_BCB_LIP_EXTRACT, null, ComponentConstant.LIP_EXTRACT_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				
				// guardando transaccion del estracto
				Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");				
				settlementProcessService.generateFundsTransferRegisterTO(objFundsTransferRegisterTO, idInterfaceProcess, loggerUser);

				if (Validations.validateIsNotNull(objFundsTransferRegisterTO)) {
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					byte [] arrXmlSend = CommonsUtilities.generateInterfaceLipResponseFile(objFundsTransferRegisterTO.getAutomaticSendTypeTO(), 
							objProcessFileTO.getStreamFileDir(), ComponentConstant.INTERFACE_TAG_ROWSET, ComponentConstant.EXTRACT_LIP_ROW);
					
					interfaceComponentServiceBean.get().updateExternalInterfaceState(idInterfaceProcess, processState, arrXmlSend, loggerUser);
					
					strXmlSend = CommonsUtilities.bytesToString(arrXmlSend);
					
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					strXmlReception = objServiciosClienteBCBProxy.enviareSolicitarExtracto(strXmlSend);										
					
					// persistiendo archiv enviado al LIP
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, strXmlReception.getBytes(), loggerUser);
					
					if(Validations.validateIsNotNullAndNotEmpty(strXmlReception)){
						log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
						lstAutomaticSendType = new ArrayList<AutomaticSendType>();
						objProcessFileTO.setRootTag(ComponentConstant.EXTRACT_LIP_RESP);
						lstAutomaticSendType = parameterServiceBean.getResponseFileLip(strXmlReception, objProcessFileTO);
					} 
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
		return lstAutomaticSendType;
	}
}
