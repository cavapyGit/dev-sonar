package com.pradera.bank.account.facade;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.bank.account.service.BankAccountServiceBean;
import com.pradera.bank.account.to.BankAccountRegistreTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.edv.collection.economic.rigth.BeneficiaryBankAccount;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BankAccountFacade {

	@Inject
	private BankAccountServiceBean  accountServiceBean;
	
	@Inject
	private UserInfo userInfo;
	
	public List<Bank> getListBank(){
		return accountServiceBean.getListAllBank();
	}
	
	public boolean verifyExistBankAccount(BankAccountRegistreTO  bankSearchRegistreTO){
		bankSearchRegistreTO.setState(StatesConstants.REGISTERED_BANK_ACCOUNT);
		bankSearchRegistreTO.setInitialDate(null);
		bankSearchRegistreTO.setFinalDate(null);
		List<BeneficiaryBankAccount> list = accountServiceBean.searchBankAccounts(bankSearchRegistreTO);
		if(list.size()>0)
			return true;
		else{
			bankSearchRegistreTO.setState(StatesConstants.CONFIRMED_BANK_ACCOUNT);
			list = accountServiceBean.searchBankAccounts(bankSearchRegistreTO);
			if(list.size()>0)
				return true;
			else return false;
		}
	}
	
	@LoggerAuditWeb
	public void saveAccountBank(BankAccountRegistreTO  accountRegistreTO){
		BeneficiaryBankAccount account = new BeneficiaryBankAccount();
		account.setIdParticipantFk(accountRegistreTO.getIdParticipantPk());
		account.setIdHolderFk(accountRegistreTO.getHolder().getIdHolderPk());
		account.setAccountNumber(accountRegistreTO.getSelectedAccountTO().getAccountNumber());
		account.setIdBankFk(accountRegistreTO.getIdBankPk());
		account.setAccountType(accountRegistreTO.getAccountType());
		account.setCurrency(accountRegistreTO.getCurrency());
		account.setDepAccountNumber(accountRegistreTO.getDepAccountNumber());
		account.setRegistreDate(CommonsUtilities.currentDate());
		if(accountRegistreTO.getFileName()!=null){
			account.setDocumentFile(accountRegistreTO.getDocumentFile());
			account.setFileName(accountRegistreTO.getFileName());
		}
		account.setState(StatesConstants.REGISTERED_BANK_ACCOUNT);//2381
		account.setRegistreDate(CommonsUtilities.currentDate());
		account.setLastModifyApp(userInfo.getLastModifyPriv());
		account.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		account.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		account.setLastModifyDate(CommonsUtilities.currentDate());
		
		accountServiceBean.saveAccountBank(account);
	}
	
	public List<BeneficiaryBankAccount> searchBankAccounts(BankAccountRegistreTO bankSearchRegistreTO){
		List<BeneficiaryBankAccount> beneficiaryBankAccounts  = accountServiceBean.searchBankAccounts(bankSearchRegistreTO);
		Participant participant;
		ParameterTable parameterTable;
		Bank bank;
		Holder holder;
		List<BeneficiaryBankAccount> bankAccounts = new ArrayList<BeneficiaryBankAccount>();
		for (BeneficiaryBankAccount beneficiaryBankAccount : beneficiaryBankAccounts) {
			participant = accountServiceBean.find(Participant.class, bankSearchRegistreTO.getIdParticipantPk());
			beneficiaryBankAccount.setParticipantDesc(participant.getIdParticipantPk()+" - "+participant.getDescription());
			holder = accountServiceBean.find(Holder.class, beneficiaryBankAccount.getIdHolderFk());
			beneficiaryBankAccount.setFullName(holder.getFullName());
			bank = accountServiceBean.find(Bank.class,beneficiaryBankAccount.getIdBankFk());
			beneficiaryBankAccount.setBankDesc(bank.getDescription());
			parameterTable = accountServiceBean.find(ParameterTable.class,beneficiaryBankAccount.getState());
			beneficiaryBankAccount.setStateDesc(parameterTable.getDescription());
			parameterTable = accountServiceBean.find(ParameterTable.class,beneficiaryBankAccount.getAccountType());
			beneficiaryBankAccount.setAccountTypeDesc(parameterTable.getDescription());
			bankAccounts.add(beneficiaryBankAccount);
		}
		return bankAccounts;
	}
	
	
	/**
	 * Confirmando el registro
	 * */
	public void confirmBankAccount(BeneficiaryBankAccount bankAccountSelected){
		accountServiceBean.confirmRequestBeneficiaryBankAccount(bankAccountSelected);
	}
	/**
	 * Rechazando registro de cuenta bancaria
	 * */
	public void rejectRequestBeneficiaryBankAccount(BeneficiaryBankAccount bankAccountSelected){
		accountServiceBean.rejectRequestBeneficiaryBankAccount(bankAccountSelected);
	}
	
	/**
	 * Registrando Bloqueo cuenta*/
	public void blockBankAccount(BeneficiaryBankAccount bankAccountSelected){
		accountServiceBean.registreBlockBankAccount(bankAccountSelected);
	}
	/**Desbloqueo*/
	public void unBlockBankAccount(BeneficiaryBankAccount bankAccount){
		accountServiceBean.registreUnBlockBankAccount(bankAccount);
	}
}
