package com.pradera.bank.account.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.bank.account.to.BankAccountRegistreTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.type.BankStateType;
import com.edv.collection.economic.rigth.BeneficiaryBankAccount;

@Stateless
@Performance
public class BankAccountServiceBean extends CrudDaoServiceBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5600509252549454346L;

	public List<Bank> getListAllBank(){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" Select b from Bank b ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and b.state = "+BankStateType.REGISTERED.getCode());
		Query query = em.createQuery(queryBuilder.toString());
		@SuppressWarnings("unchecked")
		List<Bank> lstResult = query.getResultList();
		return lstResult;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveAccountBank(BeneficiaryBankAccount beneficiaryBankAccount){
		em.persist(beneficiaryBankAccount);
	}
	
	public List<BeneficiaryBankAccount> searchBankAccounts(BankAccountRegistreTO bankSearchRegistreTO){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" Select bba from BeneficiaryBankAccount bba ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and bba.idParticipantFk = :idParticipantFk ");
		if(bankSearchRegistreTO.getIdBankPk()!=null)
			queryBuilder.append(" and bba.idBankFk = :idBankFk ");
		if(bankSearchRegistreTO.getHolder().getIdHolderPk()!=null)
			queryBuilder.append(" and bba.idHolderFk = :idHolderFk ");
		if(bankSearchRegistreTO.getAccountNumber()!=null)
			queryBuilder.append(" and bba.accountNumber = :accountNumber ");
		if(bankSearchRegistreTO.getAccountType()!=null)
			queryBuilder.append(" and bba.accountType = :accountType ");
		if(bankSearchRegistreTO.getCurrency()!=null)
			queryBuilder.append(" and bba.currency =  :currency ");
		if(bankSearchRegistreTO.getDepAccountNumber()!=null)
			queryBuilder.append(" and bba.depAccountNumber = :depAccountNumber ");
		if(bankSearchRegistreTO.getState()!=null)
			queryBuilder.append(" and bba.state = :state ");
		if(bankSearchRegistreTO.getRequestNumber()!=null)
			queryBuilder.append(" and bba.idBenBankAccountPk = :requestNumber ");
		if(bankSearchRegistreTO.getInitialDate()!=null||bankSearchRegistreTO.getFinalDate()!=null)
			queryBuilder.append(" and trunc(bba.registreDate) between :initialDate and :finalDate ");
		Query query = em.createQuery(queryBuilder.toString());
		if(bankSearchRegistreTO.getIdParticipantPk()!=null)
			query.setParameter("idParticipantFk", bankSearchRegistreTO.getIdParticipantPk());
		else return null;
		if(bankSearchRegistreTO.getIdBankPk()!=null)
			query.setParameter("idBankFk", bankSearchRegistreTO.getIdBankPk());
		if(bankSearchRegistreTO.getHolder().getIdHolderPk()!=null)
			query.setParameter("idHolderFk", bankSearchRegistreTO.getHolder().getIdHolderPk());
		if(bankSearchRegistreTO.getAccountNumber()!=null)
			query.setParameter("accountNumber",bankSearchRegistreTO.getAccountNumber());
		if(bankSearchRegistreTO.getAccountType()!=null)
			query.setParameter("accountType", bankSearchRegistreTO.getAccountType());
		if(bankSearchRegistreTO.getCurrency()!=null)
			query.setParameter("currency", bankSearchRegistreTO.getCurrency());
		if(bankSearchRegistreTO.getDepAccountNumber()!=null)
			query.setParameter("depAccountNumber", bankSearchRegistreTO.getDepAccountNumber());
		if(bankSearchRegistreTO.getState()!=null)
			query.setParameter("state", bankSearchRegistreTO.getState());
		if(bankSearchRegistreTO.getRequestNumber()!=null)
			query.setParameter("requestNumber", bankSearchRegistreTO.getRequestNumber());
		if(bankSearchRegistreTO.getInitialDate()!=null||bankSearchRegistreTO.getFinalDate()!=null){
			query.setParameter("initialDate", bankSearchRegistreTO.getInitialDate());
			query.setParameter("finalDate", bankSearchRegistreTO.getFinalDate());
		}
		@SuppressWarnings("unchecked")
		List<BeneficiaryBankAccount> lstResult = query.getResultList();
		return lstResult;
	}
	//registrar
	public void confirmRequestBeneficiaryBankAccount(BeneficiaryBankAccount bankAccount){
		bankAccount.setState(StatesConstants.CONFIRMED_BANK_ACCOUNT);
		try {
			update(bankAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	//rechazar
	public void rejectRequestBeneficiaryBankAccount(BeneficiaryBankAccount bankAccount){
		bankAccount.setState(StatesConstants.REJECTED_BANK_ACCOUNT);
		try {
			update(bankAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	//desbloquear
	public void registreUnBlockBankAccount(BeneficiaryBankAccount bankAccount){
		bankAccount.setState(StatesConstants.CONFIRMED_BANK_ACCOUNT);
		try {
			update(bankAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	//bloquear
	public void registreBlockBankAccount(BeneficiaryBankAccount bankAccount){
		bankAccount.setState(StatesConstants.BLOCKED_BANK_ACCOUNT);
		try {
			update(bankAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
