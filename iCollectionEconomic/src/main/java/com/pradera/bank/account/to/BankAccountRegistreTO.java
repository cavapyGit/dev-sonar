package com.pradera.bank.account.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.model.accounts.Holder;


public class BankAccountRegistreTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1842079076765608351L;

	private Long idParticipantPk;
	
	private Holder holder; //agente pagador
	
	private Long idHolderAccountPk;
	
	private HolderAccountHelperResultTO selectedAccountTO;
	
	private Long idBankPk;
	
	private Integer accountType;
	
	private Integer currency;
	
	private String depAccountNumber;//numero de cuenta para deposito
	
	private Integer accountNumber;
	
	private byte[] documentFile;
	
	private String fileName;
	
	private Long requestNumber; //id
	
	private Integer state;
	
	private Integer situation;

    private Date initialDate;
    
    private Date finalDate;
	
	private boolean renderedRejected;
	
	private boolean renderedConfirm;
	
	private boolean renderedBlock;
	
	private boolean renderedUnBlock;
	
	private boolean renderdModify;
	
	public BankAccountRegistreTO() {
		holder = new Holder();
		initialDate = null;
		finalDate = null;
		selectedAccountTO = new HolderAccountHelperResultTO();
	}
	public BankAccountRegistreTO(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
		holder = new Holder();
		initialDate = null;
		finalDate = null;
		selectedAccountTO = new HolderAccountHelperResultTO();
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}

	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}

	public Long getIdBankPk() {
		return idBankPk;
	}

	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getDepAccountNumber() {
		return depAccountNumber;
	}

	public void setDepAccountNumber(String depAccountNumber) {
		this.depAccountNumber = depAccountNumber;
	}

	public byte[] getDocumentFile() {
		return documentFile;
	}

	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getSituation() {
		return situation;
	}

	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public boolean isRenderedRejected() {
		return renderedRejected;
	}

	public void setRenderedRejected(boolean renderedRejected) {
		this.renderedRejected = renderedRejected;
	}

	public boolean isRenderedConfirm() {
		return renderedConfirm;
	}

	public void setRenderedConfirm(boolean renderedConfirm) {
		this.renderedConfirm = renderedConfirm;
	}

	public boolean isRenderedBlock() {
		return renderedBlock;
	}

	public void setRenderedBlock(boolean renderedBlock) {
		this.renderedBlock = renderedBlock;
	}

	public boolean isRenderedUnBlock() {
		return renderedUnBlock;
	}

	public void setRenderedUnBlock(boolean renderedUnBlock) {
		this.renderedUnBlock = renderedUnBlock;
	}

	public boolean isRenderdModify() {
		return renderdModify;
	}

	public void setRenderdModify(boolean renderdModify) {
		this.renderdModify = renderdModify;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
}