package com.pradera.bank.account;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import com.pradera.bank.account.facade.BankAccountFacade;
import com.pradera.bank.account.to.BankAccountRegistreTO;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;
import com.edv.collection.economic.rigth.BeneficiaryBankAccount;

@DepositaryWebBean
@LoggerCreateBean
public class SearchBankAccountMgmtBean extends GenericBaseBean {
	private static final long serialVersionUID = 1L;
	
	private BankAccountRegistreTO bankSearchRegistreTO,bankDetailRegistreTO;
	
	private List<Participant> listParticipant;
	
	private List<Bank> listBanks;
	@Inject
	private ParameterServiceBean parameterServiceBean;
	@Inject
	private BankAccountFacade accountFacade;
	@Inject
	private HelperComponentFacade helperComponentFacade;
	
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	private List<ParameterTable>  listAccontType,listCurrency,listBankStates;
	
	private GenericDataModel<BeneficiaryBankAccount> listBankAccount;
	
	private BeneficiaryBankAccount bankAccountSelected;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private UserInfo userInfo;
	
	private boolean visibleResult;
	
	private boolean visibleBlockBankDetail;
	
	@Inject
	private NotificationServiceFacade notificationServiceFacade;

	

	@PostConstruct
	public void init(){
		bankSearchRegistreTO = new BankAccountRegistreTO();
		try {
			listParticipant =  parameterServiceBean.getListParticipantForCollection();
			listAccontType = generalParametersFacade.getListParameterTableServiceBean(216);
			listCurrency = generalParametersFacade.getListParameterTableServiceBean(53);
			listBankStates = generalParametersFacade.getListParameterTableServiceBean(1182);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		lstAccountTo = null;
		listBanks = accountFacade.getListBank();
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			bankSearchRegistreTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
	}
	/**
	 * Bloqueando el registro de cuenta bancaria
	 * Solo bloqueamos cuentas activas y confirmadas
	 * */
	public String beforeBlocking(){
		if(isSelectedBeneficiary()){
			if(bankAccountSelected.getState().equals(StatesConstants.CONFIRMED_BANK_ACCOUNT)
					){
				loadDetailView();
				bankDetailRegistreTO.setRenderedRejected(false);
				bankDetailRegistreTO.setRenderedBlock(true);
				bankDetailRegistreTO.setRenderedUnBlock(false);
				bankDetailRegistreTO.setRenderdModify(false);
				bankDetailRegistreTO.setRenderedConfirm(false);
				visibleBlockBankDetail = true;
				return "loadBankAccountDetail";
			}else{
				showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("lbl.no.correct.state.bank.account"));
				JSFUtilities.showSimpleValidationDialog();		
				}
		}
		return null;
	}
	
	public void participantChangeEvent(){
		bankSearchRegistreTO.setHolder(new Holder());
		bankSearchRegistreTO.setSelectedAccountTO(null);
		lstAccountTo = null;
		JSFUtilities.updateComponent("tblMainSearchBankAccount");
	}
	/**
	 **/
	public void holderChangeEvent(){	
	    	try {
				Holder holder = bankSearchRegistreTO.getHolder();		
				if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {

				} else {
					HolderTO holderTO = new HolderTO();
					holderTO.setHolderId(holder.getIdHolderPk());
					holderTO.setFlagAllHolders(true);
					holderTO.setParticipantFk(bankSearchRegistreTO.getIdParticipantPk());
					List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
					
					if (lista==null || lista.isEmpty()){
						lstAccountTo = new GenericDataModel<>();
					}else{
						lstAccountTo = new GenericDataModel<>(lista);
						if(lista.size() == ComponentConstant.ONE){
							bankSearchRegistreTO.setSelectedAccountTO(lista.get(0)); 
							JSFUtilities.updateComponent("frmSearchBankAccount:tblResultHelperAccountSourceSearchSearch");
						}
					}
					if(lstAccountTo.getSize()<0){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getGenericMessage("Participante incorrecto"));//PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
						bankSearchRegistreTO.setHolder(new Holder());
						JSFUtilities.showSimpleValidationDialog();
					} else {
						List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
						for (HolderAccountHelperResultTO objAccount : lstAccountTo) 
							for (ParameterTable parameterTable : lstHolderAccountState) 
								objAccount.setAccountStatusDescription(parameterTable.getParameterName());
					}
				}
				if (lstAccountTo!=null){
					JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
				}
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	
	/**
	 * confirmando el registro de desbloqueo
	 * */	
	public void blockBankAccountConfirmYes(){
		accountFacade.blockBankAccount(bankAccountSelected);
		List<BeneficiaryBankAccount> list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		listBankAccount = new GenericDataModel<>(list);
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.correct.bank.account",bankAccountSelected.getParticipantDesc());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('dlgConfirmBlockCorrect').show();");
    	
    	/**Notificacion para configuracion de cuenta bancaria*/
    	BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICACION_BENEFICIARY_BANK_ACCOUNTS.getCode());
		String id = bankAccountSelected.getIdBenBankAccountPk().toString();
		
		Object[] parameters = {id};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	
	/**
	 * Registrando el desblqueo de la cuenta bancaria
	 * */
	@LoggerAuditWeb
	public StreamedContent getStreamedContent() throws IOException{
		byte[] fileContent = bankAccountSelected.getDocumentFile();
		return getStreamedContentFromFile(fileContent, null, bankAccountSelected.getFileName());
	}
	
	@LoggerAuditWeb
	public StreamedContent getStreamedContentLoockFile() throws IOException{
		byte[] fileContent = bankAccountSelected.getDocumentLockFile();
		return getStreamedContentFromFile(fileContent, null, bankAccountSelected.getLockFileName());
	}
	
	public void unLockBankAccountConfirmYes(){
		accountFacade.unBlockBankAccount(bankAccountSelected);
		List<BeneficiaryBankAccount> list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		listBankAccount = new GenericDataModel<>(list);
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.correct.un.lock.bank.account",bankAccountSelected.getParticipantDesc());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('dlgConfirmUnBlockCorrect').show();");
	}
	
	/**
	 * Desbloquear cuenta bancaria
	 * **/
	public String beforeUnBlockBankAccount(){
		bankAccountSelected.setLockFileName(null);
		bankAccountSelected.setBlockingReason(null);
		bankAccountSelected.setDocumentLockFile(null);
		if(isSelectedBeneficiary()){
			if(bankAccountSelected.getState().equals(StatesConstants.BLOCKED_BANK_ACCOUNT)
				//&&bankAccountSelected.getSituation().equals(SituationConstants.ACTIVE_BANK_ACCOUNT)
				){
				loadDetailView();
				bankDetailRegistreTO.setRenderedRejected(false);
				bankDetailRegistreTO.setRenderedBlock(false);
				bankDetailRegistreTO.setRenderedUnBlock(true);
				bankDetailRegistreTO.setRenderdModify(false);
				bankDetailRegistreTO.setRenderedConfirm(false);
				visibleBlockBankDetail = true;
				return "loadBankAccountDetail";
			}else{
				if(bankAccountSelected.getState().equals(StatesConstants.BLOCKED_BANK_ACCOUNT)
						//&&bankAccountSelected.getSituation().equals(SituationConstants.UNLOCKING_PENDING_BANK_ACCOUNT)
						){
						if(!userPrivilege.getUserAcctions().isConfirmBlock()){
							String msg = PropertiesUtilities.getMessage("lbl.not.privilege");
							showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
						String headerMessageView = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
						Object[] bodyData = new Object[2];
						bodyData[0] = bankAccountSelected.getIdBenBankAccountPk();
						String bodyMessageView = PropertiesUtilities.getMessage("lbl.alert.confirm.un.lock.bank.account",bodyData);
						showMessageOnDialogBank(headerMessageView,bodyMessageView); 
						JSFUtilities.updateComponent("opnlDialogs");
				    	JSFUtilities.executeJavascriptFunction("PF('dlgConfirmUnBlockBankAccount').show();");
					}else{
						showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("lbl.no.correct.state.bank.account"));
						JSFUtilities.showSimpleValidationDialog();	
					}
				}
			}
		return null;
		}
	/**
	 * Dialogo para la solicitud de bloqueo
	 * 
	 * */
	public void blockRegistreBankAccountDialog(){
		if(bankAccountSelected.getLockFileName()==null||bankAccountSelected.getBlockingReason()==null){
			showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("lbl.title.collection.data.block.complete"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("alert.confirm.block.bank.account",bankAccountSelected.getParticipantDesc());
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('vWBlockrRegistreBankAccount').show();");
    	return;
	}
	/**
	 * confirmando el registro de bloqueo
	 * **/
	public String blockBankRegistreAccountDialogYes(){  
		
		accountFacade.blockBankAccount(bankAccountSelected);
		List<BeneficiaryBankAccount> list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		
		
    	/**Notificacion para configuracion de cuenta bancaria*/
    	BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICACION_BENEFICIARY_BANK_ACCOUNTS.getCode());
		String id = bankAccountSelected.getIdBenBankAccountPk().toString();
		
		Object[] parameters = {id};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
    	
		
		listBankAccount = new GenericDataModel<>(list);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SUCCESFULL_LOOK_BANK_ACCOUNT,bankAccountSelected.getParticipantDesc());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('dlgCorrectRegistreBlockBankAccount').show();");
    	return "";
	}
	
	/**
	 * Registrando en desbloqueo de la cuenta bancaria
	 * 
	 * */
	public void unBlockRegistreBankAccountDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DIALOG_UN_BLOCK_BANK_ACCOUNT,bankAccountSelected.getParticipantDesc());
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('vWUnBlockRegistreBankAccount').show();");
	}
	/**
	 * Registrando el desbloqueo de la cuenta bancaria
	 * */
	public String unBlockRegistreBankAccountDialogYes(){
		bankAccountSelected.setBlockingReason(null);
		bankAccountSelected.setDocumentLockFile(null);
		bankAccountSelected.setLockFileName(null);
		accountFacade.unBlockBankAccount(bankAccountSelected);
		List<BeneficiaryBankAccount> list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		listBankAccount = new GenericDataModel<>(list);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DIALOG_UN_BLOCK_SUCCESFULL,bankAccountSelected.getParticipantDesc());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('dlgConfirmUnBlockCorrect').show();");
    	
    	/**Notificacion para configuracion de cuenta bancaria*/
    	BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICACION_BENEFICIARY_BANK_ACCOUNTS.getCode());
		String id = bankAccountSelected.getIdBenBankAccountPk().toString();
		
		Object[] parameters = {id};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
    	
    	return "";
    	//return "backLoadBankAccountDetail";
	}
	/**
	 * Usado en la pantalla de detalle
	 * Dialgo de notificacion de usuario/contacto agente pagador confirmado correctamente
	 * **/
	public void confirmBankAccountDialogYes(){
		accountFacade.confirmBankAccount(bankAccountSelected);
		List<BeneficiaryBankAccount> list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		/*Notificacion de confirmacion exitoso*/
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICACION_BENEFICIARY_BANK_ACCOUNTS.getCode());
		//String id = "Se confirmo satisfactoriamente la Solicitud de Cuenta Bancaria Beneficiario "+bankAccountSelected.getIdBenBankAccountPk()+" de "+bankAccountSelected.getParticipantDesc();
		String id = bankAccountSelected.getIdBenBankAccountPk().toString();
		
		Object[] parameters = {id};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
		
		listBankAccount = new GenericDataModel<>(list);
		
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SUCCESFULL_BANK_ACCOUNT,bankAccountSelected.getIdBenBankAccountPk());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirmBankAccountYes').show();");
	}
	/**
	 * Dialogo para confirmar si se va rechazar o no el registro
	 * 
	 * */
	public void rejectBankAccountDialog(){
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_REJECT_DIALOG_BANK_ACCOUNT,bankAccountSelected.getIdBenBankAccountPk());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDlgRejectBankAccount').show();");
	}
	/**
	 * Rechazar una solicitud de registro de cuenta bancaria
	 *Estado = rechazado y situasion = activo
	 * */
	public void rejectBankAccountDialogYes(){
		accountFacade.rejectRequestBeneficiaryBankAccount(bankAccountSelected);
		List<BeneficiaryBankAccount> list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		listBankAccount = new GenericDataModel<>(list);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_DIALOG_REJECT_SUCCESFULL,bankAccountSelected.getIdBenBankAccountPk());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogRejectBankAccountYes').show();");
	}
	
	/**
	 * Busqueda de cuentas bancarias beneficiario 
	 * Usado en la pantalla de busqueda de cuentas/beneficiario*/
	public String searchBankAccounts(){
		if(bankSearchRegistreTO.getIdParticipantPk()==null){
			showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage("message.search.at.least.one.filter"));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		List<BeneficiaryBankAccount> list = new ArrayList<BeneficiaryBankAccount>();
		list = accountFacade.searchBankAccounts(bankSearchRegistreTO);
		if(list!=null&&list.size()>0)
			visibleResult=false;
		else
			visibleResult =true;
		JSFUtilities.updateComponent("idResultBankAccount");
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		listBankAccount = new GenericDataModel<>(list);
		if(listBankAccount.getSize()>0){
			if(userPrivilege.getUserAcctions().isConfirm()){
				privilegeComponent.setBtnConfirmView(true);
			}
			if(userPrivilege.getUserAcctions().isReject()){
				privilegeComponent.setBtnRejectView(true);
			}
			if(userPrivilege.getUserAcctions().isUnblock()){
				privilegeComponent.setBtnUnblockView(true);
			}
			if(userPrivilege.getUserAcctions().isBlock()){
				privilegeComponent.setBtnBlockView(true);
			}
			if(userPrivilege.getUserAcctions().isModify()){
				privilegeComponent.setBtnModifyView(true);
			}
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		return null;
	}
	/**
	 * Usado en la pantalla de registro de banco o beneficiario
	 * Funcion limipar pantalla
	 * @return null
	 * */
	public String cleanRegBankAccountScreen(){
		if(isModifiedAccountBank()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			showMessageOnDialogBank(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVCleanRegistreAccountScreen').show();");
		}else{
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
					||userInfo.getUserAccountSession().isParticipantInstitucion())
				bankSearchRegistreTO = new BankAccountRegistreTO(userInfo.getUserAccountSession().getParticipantCode());
			else
				bankSearchRegistreTO = new BankAccountRegistreTO();
		}
		JSFUtilities.updateComponent("frmRegisterBankAccount");
		return null;
	}
	/**
	 * Usado en la pantalla de registro de cuentas bancarias
	 * Limpiamos la pantalla de registro
	 * */
	public void exeCleanRegistreAcountScreen(){
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			bankSearchRegistreTO= new BankAccountRegistreTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			bankSearchRegistreTO= new BankAccountRegistreTO();
		lstAccountTo = null;
		JSFUtilities.updateComponent("frmSearchBankAccount");
		listBankAccount = null;
		JSFUtilities.updateComponent("idResultBankAccount");
		visibleResult = false;
		JSFUtilities.updateComponent("dtbResultResult");
	}
	/**
	 * Usado en la pantalla de detalle de cuentas bancarias beneficiario
	 * Dialgo para confirmar el registro de un usuario/beneficiario cuenta banacaria
	 * */
	public void confirmBankAccountDialog(){
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_BANK_ACCOUNT,bankAccountSelected.getIdBenBankAccountPk());
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogBank(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('cnfDialogConfirmBankAccount').show();");
	}
	/**
	 * Confirmando una solicitud de registro de cuenta bancaria
	 * */
	public String beforeConfirm(){
		if(isSelectedBeneficiary()){
			if(bankAccountSelected.getState().equals(StatesConstants.REGISTERED_BANK_ACCOUNT)
					){
				loadDetailView();
				bankDetailRegistreTO.setRenderedRejected(false);
				bankDetailRegistreTO.setRenderedBlock(false);
				bankDetailRegistreTO.setRenderedUnBlock(false);
				bankDetailRegistreTO.setRenderdModify(false);
				bankDetailRegistreTO.setRenderedConfirm(true);
				visibleBlockBankDetail = false;
				return "loadBankAccountDetail";
			}else{
				showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("lbl.message.alert.no.registered"));
				JSFUtilities.showSimpleValidationDialog();	
			}
		}
		return "";
	} 
	
	private void loadDetailView(){
		bankDetailRegistreTO = new BankAccountRegistreTO();
		bankDetailRegistreTO.setIdParticipantPk(bankAccountSelected.getIdParticipantFk());
		bankDetailRegistreTO.setHolder(new Holder(bankAccountSelected.getIdHolderFk()));
		bankDetailRegistreTO.setIdBankPk(bankAccountSelected.getIdBankFk());
		bankDetailRegistreTO.setAccountNumber(bankAccountSelected.getAccountNumber());
		bankDetailRegistreTO.setAccountType(Integer.parseInt(""+bankAccountSelected.getAccountType()));
		bankDetailRegistreTO.setCurrency(bankAccountSelected.getCurrency());
		bankDetailRegistreTO.setDepAccountNumber(bankAccountSelected.getDepAccountNumber());
		bankDetailRegistreTO.setFileName(bankAccountSelected.getFileName());
		visibleBlockBankDetail = true;
	}
	private boolean isSelectedBeneficiary(){
		if(bankAccountSelected==null){
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MESSAGE_NO_SELECT);
			showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}else
			return true;
	}
	/**
	 * Rechazo de un registro de una cuenta bancaria
	 * */
	public String beforeReject(){
		if(isSelectedBeneficiary()){
			if(bankAccountSelected.getState().equals(StatesConstants.REGISTERED_BANK_ACCOUNT)
					//&&bankAccountSelected.getSituation()==null
					){
				loadDetailView();
				bankDetailRegistreTO.setRenderedRejected(true);
				bankDetailRegistreTO.setRenderedBlock(false);
				bankDetailRegistreTO.setRenderedUnBlock(false);
				bankDetailRegistreTO.setRenderdModify(false);
				bankDetailRegistreTO.setRenderedConfirm(false);
				visibleBlockBankDetail = false;
				return "loadBankAccountDetail";
			}
			else{
				showMessageOnDialogBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("lbl.message.alert.no.registered"));
				JSFUtilities.showSimpleValidationDialog();	
			}
		}
		return "";
	} 
	
	/**
	 * Usado en la pantalla de registro de cuantas bancarias
	 * */
	public void documentAttachFile(FileUploadEvent event){
		String fDisplayName=fUploadValidateFile(event.getFile(),null, "5000000","pdf|doc|docx|jpg|jpeg|png|MSG");
		if(fDisplayName!=null){
			bankAccountSelected.setLockFileName(fDisplayName);
			bankAccountSelected.setDocumentLockFile(event.getFile().getContents());
			JSFUtilities.updateComponent("idDeleteFile");
			JSFUtilities.updateComponent("lblFuplDocumentFileBlock");
		}else{
			showMessageOnDialogRegBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	private void showMessageOnDialogRegBank(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	/**
	 * Verificamos si fue modificado el 
	 * */
	private boolean isModifiedAccountBank(){
		if(!userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||!userInfo.getUserAccountSession().isParticipantInstitucion())
		if(bankSearchRegistreTO.getIdParticipantPk()!=null)
			return true;
		if(bankSearchRegistreTO.getHolder().getIdHolderPk()!=null)
			return true;
		if(bankSearchRegistreTO.getSelectedAccountTO()!=null)
			return true;
		if(bankSearchRegistreTO.getIdBankPk()!=null)
			return true;
		if(bankSearchRegistreTO.getAccountType()!=null)
			return true;
		if(bankSearchRegistreTO.getCurrency()!=null)
			return true;
		if(bankSearchRegistreTO.getDepAccountNumber()!=null)
			return true;
		if(bankSearchRegistreTO.getDocumentFile()!=null)
			return true;
		else 
			return false;
	}
	public void loadBankAccountDetail(ActionEvent event){
		bankAccountSelected = new BeneficiaryBankAccount();
		bankAccountSelected = (BeneficiaryBankAccount) event.getComponent().getAttributes().get("bankAccountView");
		loadDetailView();
	}
	/**
	 * Verificamos el registro tiene el estado correcto
	 * 
	 * **/

	private void showMessageOnDialogBank(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	public List<Participant> getListParticipant() {
		return listParticipant;
	}
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}
	public List<Bank> getListBanks() {
		return listBanks;
	}
	public void setListBanks(List<Bank> listBanks) {
		this.listBanks = listBanks;
	}
	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}
	public void setLstAccountTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}
	public List<ParameterTable> getListAccontType() {
		return listAccontType;
	}
	public void setListAccontType(List<ParameterTable> listAccontType) {
		this.listAccontType = listAccontType;
	}
	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}
	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}
	public BankAccountRegistreTO getBankSearchRegistreTO() {
		return bankSearchRegistreTO;
	}
	public void setBankSearchRegistreTO(BankAccountRegistreTO bankSearchRegistreTO) {
		this.bankSearchRegistreTO = bankSearchRegistreTO;
	}

	public List<ParameterTable> getListBankStates() {
		return listBankStates;
	}

	public void setListBankStates(List<ParameterTable> listBankStates) {
		this.listBankStates = listBankStates;
	}

	public GenericDataModel<BeneficiaryBankAccount> getListBankAccount() {
		return listBankAccount;
	}

	public void setListBankAccount(
			GenericDataModel<BeneficiaryBankAccount> listBankAccount) {
		this.listBankAccount = listBankAccount;
	}

	public BeneficiaryBankAccount getBankAccountSelected() {
		return bankAccountSelected;
	}

	public void setBankAccountSelected(BeneficiaryBankAccount bankAccountSelected) {
		this.bankAccountSelected = bankAccountSelected;
	}

	public BankAccountRegistreTO getBankDetailRegistreTO() {
		return bankDetailRegistreTO;
	}

	public void setBankDetailRegistreTO(BankAccountRegistreTO bankDetailRegistreTO) {
		this.bankDetailRegistreTO = bankDetailRegistreTO;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
	public boolean isVisibleBlockBankDetail() {
		return visibleBlockBankDetail;
	}
	public void setVisibleBlockBankDetail(boolean visibleBlockBankDetail) {
		this.visibleBlockBankDetail = visibleBlockBankDetail;
	}
}
