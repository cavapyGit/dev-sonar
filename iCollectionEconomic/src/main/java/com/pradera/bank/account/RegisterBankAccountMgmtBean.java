package com.pradera.bank.account;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;

import com.pradera.bank.account.facade.BankAccountFacade;
import com.pradera.bank.account.to.BankAccountRegistreTO;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

@DepositaryWebBean
@LoggerCreateBean
public class RegisterBankAccountMgmtBean extends GenericBaseBean{

	private static final long serialVersionUID = 1L;
	
	private BankAccountRegistreTO bankRegistreTO; 
	
	private List<Participant> listParticipant;
	
	private List<Bank> listBanks;
	@Inject
	private BankAccountFacade accountFacade;
	@Inject
	private HelperComponentFacade helperComponentFacade;
	
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;

	
	private List<ParameterTable>  listAccontType,listCurrency;
	
	@Inject
	private UserInfo userInfo;

	@PostConstruct
	public void init(){
		bankRegistreTO = new BankAccountRegistreTO();
		try {
			
			listParticipant =   parameterServiceBean.getListParticipantForCollection();
			listAccontType = generalParametersFacade.getListParameterTableServiceBean(216);
			listCurrency = generalParametersFacade.getListParameterTableServiceBean(53);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		listBanks = accountFacade.getListBank();
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			bankRegistreTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
	}
	/**
	 * Usado en la pantalla de registro de cuantas bancarias
	 * */
	public void documentAttachFile(FileUploadEvent event){
		String fDisplayName=fUploadValidateFile(event.getFile(),null, "5000000","pdf|doc|docx|jpg|jpeg|png");
		if(fDisplayName!=null){
			bankRegistreTO.setFileName(fDisplayName);
			bankRegistreTO.setDocumentFile(event.getFile().getContents());
			JSFUtilities.updateComponent("idDeleteFile");
		}else{
			showMessageOnDialogRegBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	private void showMessageOnDialogRegBank(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	 /**
     * Cui change.
     */
    public void holderChangeEvent(){	
    	try {
			Holder holder = bankRegistreTO.getHolder();		
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {

			} else {
				HolderTO holderTO = new HolderTO();
				holderTO.setHolderId(holder.getIdHolderPk());
				holderTO.setFlagAllHolders(true);
				holderTO.setParticipantFk(bankRegistreTO.getIdParticipantPk());
				List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if (lista==null || lista.isEmpty()){
					lstAccountTo = new GenericDataModel<>();
				}else{
					lstAccountTo = new GenericDataModel<>(lista);
					if(lista.size() == ComponentConstant.ONE){
						bankRegistreTO.setSelectedAccountTO(lista.get(0)); 
						JSFUtilities.updateComponent("frmRegisterDeposit:tblResultHelperAccountSource");
					}
				}
				if(lstAccountTo.getSize()<0){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getGenericMessage("Participante incorrecto"));//PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
					bankRegistreTO.setHolder(new Holder());
					JSFUtilities.showSimpleValidationDialog();
				} else {
					List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
					for (HolderAccountHelperResultTO objAccount : lstAccountTo) 
						for (ParameterTable parameterTable : lstHolderAccountState) 
							objAccount.setAccountStatusDescription(parameterTable.getParameterName());
				}
			}
			if (lstAccountTo!=null){
				JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
    /** Usado en la pantalla de registro de cuentas/beneficiario */
	public void preSaveAccountBank(){
		JSFUtilities.executeJavascriptFunction("PF('wVconfirmSaveBankAccount').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	@LoggerAuditWeb
	public void executeSave(){
		if(bankRegistreTO.getSelectedAccountTO()==null){
			showMessageOnDialogRegBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("lbl.request.account.number"));
		    JSFUtilities.showSimpleValidationDialog();
		    return;
		}
		/**Verificar si ya existe una cuenta registrada con el mismo numero de cuenta*/
		if(accountFacade.verifyExistBankAccount(bankRegistreTO)){
			showMessageOnDialogRegBank(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("label.exists.registre.bank.account.alert"));
		    JSFUtilities.showSimpleValidationDialog();
		    return;
		}
		accountFacade.saveAccountBank(bankRegistreTO);
		String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MESSAGE_SAVE_CONFIRM_NOTIFY);
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			bankRegistreTO= new BankAccountRegistreTO(bankRegistreTO.getIdParticipantPk());
		else
			bankRegistreTO= new BankAccountRegistreTO();
		lstAccountTo = null;
		JSFUtilities.updateComponent("frmRegisterBankAccount");
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		showMessageOnDialogRegBank(headerMessage,message); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVidRegistreOkAccountScreen').show();");
	}
	
	private boolean isModifiedAccountBank(){
		if(!userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||!userInfo.getUserAccountSession().isParticipantInstitucion())
		if(bankRegistreTO.getIdParticipantPk()!=null)
			return true;
		if(bankRegistreTO.getHolder().getIdHolderPk()!=null)
			return true;
		if(bankRegistreTO.getSelectedAccountTO()!=null)
			return true;
		if(bankRegistreTO.getIdBankPk()!=null)
			return true;
		if(bankRegistreTO.getAccountType()!=null)
			return true;
		if(bankRegistreTO.getCurrency()!=null)
			return true;
		if(bankRegistreTO.getDepAccountNumber()!=null)
			return true;
		if(bankRegistreTO.getDocumentFile()!=null)
			return true;
		else 
			return false;
	}
	/**
	 * Usado en la pantalla de registro de banco o beneficiario
	 * Funcion limipar pantalla
	 * @return null
	 * */
	public String cleanRegBankAccountScreen(){
		if(isModifiedAccountBank()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			showMessageOnDialogRegBank(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVCleanRegistreAccountScreen').show();");
		}else{
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
					||userInfo.getUserAccountSession().isParticipantInstitucion())
				bankRegistreTO = new BankAccountRegistreTO(userInfo.getUserAccountSession().getParticipantCode());
			else
				bankRegistreTO = new BankAccountRegistreTO();
			lstAccountTo = null;
			JSFUtilities.updateComponent("frmRegisterBankAccount");
		}
		return null;
	}
	public void participantChangeEvent(){
		bankRegistreTO.setHolder(new Holder());
		bankRegistreTO.setSelectedAccountTO(null);
		lstAccountTo = null;
		JSFUtilities.updateComponent("frmRegisterBankAccount");
	}
	
	/**
	 * Usado en la pantalla de registro de cuentas bancarias
	 * Limpiamos la pantalla de registro
	 * */
	public void exeCleanRegistreAcountScreen(){
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			bankRegistreTO= new BankAccountRegistreTO(bankRegistreTO.getIdParticipantPk());
		else
			bankRegistreTO = new BankAccountRegistreTO();
		lstAccountTo = null;
		JSFUtilities.updateComponent("frmRegisterBankAccount");
	}
	
	/**
	 * Usado en la pantalla de registro de cuentas bancarias
	 * Dialogo para regresar a la pantalla de busqueda
	 * */
	public String backRegistrePaymentScreen(){
		if(isModifiedAccountBank()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogRegBank(headerMessage,bodyMessage); 
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVBackRegistreBankScreen').show();");
	    	return null;
		}
		else
			return "backPageSearchBankAccount";
	}
	public String deleteFileAdjunt(){
		bankRegistreTO.setFileName(null);
		bankRegistreTO.setDocumentFile(null);
		return null;
	}
	/**
	 * Usado en la pantalla de registro de cuenta/beneficiario
	 * 
	 * */
	public String exeBackRegistreBankScreen(){
		bankRegistreTO= new BankAccountRegistreTO();
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			bankRegistreTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		return "backPageSearchBankAccount";
	}
	
	public List<Participant> getListParticipant() {
		return listParticipant;
	}
	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}
	public BankAccountRegistreTO getBankRegistreTO() {
		return bankRegistreTO;
	}
	public void setBankRegistreTO(BankAccountRegistreTO bankRegistreTO) {
		this.bankRegistreTO = bankRegistreTO;
	}
	public List<Bank> getListBanks() {
		return listBanks;
	}
	public void setListBanks(List<Bank> listBanks) {
		this.listBanks = listBanks;
	}
	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}
	public void setLstAccountTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}
	public List<ParameterTable> getListAccontType() {
		return listAccontType;
	}
	public void setListAccontType(List<ParameterTable> listAccontType) {
		this.listAccontType = listAccontType;
	}
	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}
	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}
}
