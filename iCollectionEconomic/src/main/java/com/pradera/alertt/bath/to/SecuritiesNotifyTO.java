package com.pradera.alertt.bath.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.Participant;

public class SecuritiesNotifyTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6709639996494928477L;
	
	private List<String> listSecuritiesDifferentBalance;

	private Participant participant;

	public List<String> getListSecuritiesDifferentBalance() {
		return listSecuritiesDifferentBalance;
	}

	public void setListSecuritiesDifferentBalance(
			List<String> listSecuritiesDifferentBalance) {
		this.listSecuritiesDifferentBalance = listSecuritiesDifferentBalance;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
}
