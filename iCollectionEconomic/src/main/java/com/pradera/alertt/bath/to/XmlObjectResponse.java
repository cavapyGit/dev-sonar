package com.pradera.alertt.bath.to;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "respuesta")
@XmlAccessorType(XmlAccessType.FIELD) 
public class XmlObjectResponse implements Serializable{
	
	 /**
	 * 
	 */
	 private static final long serialVersionUID = -1172039459477423190L;

	 @XmlElement(name = "nro_solicitud")
	 private String requestNumber;
	 
	 @XmlElement(name = "cod_retorno")
	 private String returnCode;
	 
	 @XmlElement(name = "descripcion")
	 private String description;
	 
	 @XmlElement(name = "mensaje_recibido")
	 private String messageReceived;

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessageReceived() {
		return messageReceived;
	}

	public void setMessageReceived(String messageReceived) {
		this.messageReceived = messageReceived;
	}
}
