


package com.pradera.alertt.bath.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RetirementObjetTO implements Serializable {
       
       /**
       * 
        */
       private static final long serialVersionUID = -1702025674472447375L;

       private Long idParticipantPk;
       
       private Date paymentDate;
       
       private Integer currency;
       
       private BigDecimal amount;
       
       private Long idBankPk;
       
       private Long idHolderPk;
       
       private String accountNumber;
       
       private String gloss;
       
       private Integer state;
       
       private Integer stateRetirement;
       
       private String userName;

       
       
       
       public RetirementObjetTO() {
             // TODO Auto-generated constructor stub
       }

       public Long getIdParticipantPk() {
             return idParticipantPk;
       }

       public void setIdParticipantPk(Long idParticipantPk) {
             this.idParticipantPk = idParticipantPk;
       }

       public Date getPaymentDate() {
             return paymentDate;
       }

       public void setPaymentDate(Date paymentDate) {
             this.paymentDate = paymentDate;
       }

       public Integer getCurrency() {
             return currency;
       }

       public void setCurrency(Integer currency) {
             this.currency = currency;
       }

       public BigDecimal getAmount() {
             return amount;
       }

       public void setAmount(BigDecimal amount) {
             this.amount = amount;
       }

       public String getGloss() {
             return gloss;
       }

       public void setGloss(String gloss) {
             this.gloss = gloss;
       }

       public Long getIdBankPk() {
             return idBankPk;
       }

       public void setIdBankPk(Long idBankPk) {
             this.idBankPk = idBankPk;
       }

       public String getAccountNumber() {
             return accountNumber;
       }

       public void setAccountNumber(String accountNumber) {
             this.accountNumber = accountNumber;
       }

       public String getUserName() {
             return userName;
       }

       public void setUserName(String userName) {
             this.userName = userName;
       }

       public Integer getState() {
             return state;
       }

       public void setState(Integer state) {
             this.state = state;
       }

       public Integer getStateRetirement() {
             return stateRetirement;
       }

       public void setStateRetirement(Integer stateRetirement) {
             this.stateRetirement = stateRetirement;
       }

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
}