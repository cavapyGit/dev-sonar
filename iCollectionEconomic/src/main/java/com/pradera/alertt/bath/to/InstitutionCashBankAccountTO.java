package com.pradera.alertt.bath.to;

import java.io.Serializable;

public class InstitutionCashBankAccountTO  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2499256001391773174L;
	
	private String accountNumberLIp;
	
	private String bcbCode;

	public String getAccountNumberLIp() {
		return accountNumberLIp;
	}

	public void setAccountNumberLIp(String accountNumberLIp) {
		this.accountNumberLIp = accountNumberLIp;
	}

	public String getBcbCode() {
		return bcbCode;
	}

	public void setBcbCode(String bcbCode) {
		this.bcbCode = bcbCode;
	}
}
