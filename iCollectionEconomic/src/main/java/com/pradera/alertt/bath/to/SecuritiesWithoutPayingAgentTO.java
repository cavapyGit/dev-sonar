package com.pradera.alertt.bath.to;

import java.io.Serializable;

public class SecuritiesWithoutPayingAgentTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1615342873399834304L;
	
	private String idSecurityCodePk;
	
	private String mNemonicPaymentAgent;

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public String getmNemonicPaymentAgent() {
		return mNemonicPaymentAgent;
	}

	public void setmNemonicPaymentAgent(String mNemonicPaymentAgent) {
		this.mNemonicPaymentAgent = mNemonicPaymentAgent;
	}
}
