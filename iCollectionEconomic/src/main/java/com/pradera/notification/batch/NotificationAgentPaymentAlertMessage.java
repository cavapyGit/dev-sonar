package com.pradera.notification.batch;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;

import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;

//Bean generico para envio de notificaciones
@BatchProcess(name = "NotificationAgentPaymentAlertMessage")
@RequestScoped
public class NotificationAgentPaymentAlertMessage implements JobExecution, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1197108809256596733L;

	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
