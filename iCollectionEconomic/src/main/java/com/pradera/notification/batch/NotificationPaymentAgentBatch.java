package com.pradera.notification.batch;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.collection.right.to.DetailCollectionGlossTO;
import com.pradera.collection.right.to.SecuritiesAlmostObjectTO;
import com.pradera.collection.right.util.GenerateAccreditationGertificateUtil;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.collection.right.util.NotificationConstants;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.paymentagent.type.PaymentAgentType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
import com.pradera.notification.facade.NotificationPaymentAgentFacadeBean;
import com.pradera.notification.service.NotificationPaymentAgentServiceBean;
import com.pradera.notification.to.SecuritiesAlmostToExpireTO;
import com.pradera.notification.to.SecuritiesNotPaymentAgentTO;
import com.pradera.notification.to.SecurityDetailObjectTO;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.GlossNoticationDetail;
import com.edv.collection.economic.rigth.PaymentAgent;

@RequestScoped
@BatchProcess(name = "NotificationPaymentAgentBatch")
public class NotificationPaymentAgentBatch implements JobExecution, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger log;

	@Inject
	NotificationServiceFacade notificationServiceFacade;

	@Inject
	NotificationPaymentAgentFacadeBean notificationFacadeBean;

	@Inject
	private GenerateAccreditationGertificateUtil accreditationGertificateUtil;

	@Inject
	private NotificationPaymentAgentServiceBean notificationPaymentAgentServiceBean;

	@Inject
	private GenerateUrlNotification generateUrlNotification;

	@Inject
	private ParameterServiceBean parameterServiceBean;

	@Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	// clasificar en grupos los valores por participante, instrumento y moneda
	private HashMap<Long, PaymentAgent> listPaymentAgents;
	
	private String glosa;

	/** batch for sending notifications */
	@Override
	public void startJob(ProcessLogger processLogger) {
		/*PaymentAgent paymentAgent = new PaymentAgent();
		//Buscar y cargar contacto agente pagador de prueba
		paymentAgent = parameterServiceBean.find(PaymentAgent.class, 2L );
		//paymentAgent.setSign(BooleanType.YES.getCode());
		paymentAgent.setSign(BooleanType.NO.getCode());
		paymentAgent.setEncryp(BooleanType.NO.getCode());
		List<UserAccountSession> lstUserAccount;
		
		UserAccountSession accountSession = new UserAccountSession();
		accountSession.setUserName(paymentAgent.getName());
		accountSession.setEmail(paymentAgent.getEmail());
		accountSession.setPhoneNumber(paymentAgent.getCellPhone());
		accountSession.setFullName(paymentAgent.getName() + " "+ paymentAgent.getFirstName());
		lstUserAccount = new ArrayList<UserAccountSession>();
		lstUserAccount.add(accountSession);
		
		
		//File file = new File("D:\\EXCEL_ISSUES\\680\\certificados\\BUReimpresion.pdf");
		
		List<File> listFile = new ArrayList<File>();
		//listFile.add(file);
		
		
		String htmlMessage = "<p>Test cifrar,firmar email</p>";
		notificationServiceFacade.sendNotificationManualEmailAdjuntHtmlContent(processLogger.getIdUserAccount(),paymentAgent, processLogger.getBusinessProcess(),lstUserAccount, NotificationConstants.SUBJECT_EMAIL_PAYMENT_AGENT, htmlMessage, null, listFile);
		if(true)
			return;
		
		*/
		
		boolean onlyCurrentDate = false;
		
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		if (Validations.validateListIsNotNullAndNotEmpty(processLoggerDetails)){
			for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
				if(processLoggerDetail.getParameterName().equals("onlyCurrentDate"))
					   onlyCurrentDate =  Boolean.parseBoolean(processLoggerDetail.getParameterValue());				
			}
		}
		
		
		log.info("CDDE :::  Inicio de: Batchero para el envio de notificaciones a los agente pagadores");
		/**Siempre se ejecuta a fecha actual*/
		Date paymentDate = CommonsUtilities.currentDate();
		
		Date nextBusinessDay = null;
		
		if(onlyCurrentDate)
			nextBusinessDay = CommonsUtilities.currentDate();
		else
			nextBusinessDay = holidayQueryServiceBean.getNextWorkingDayServiceBean(paymentDate);
				
		/**Verificamos que todos los agentes pagadores existentes en los valores registrados sean correctos*/
		List<SecuritiesNotPaymentAgentTO>  listPaymentAgentNotEnable = notificationPaymentAgentServiceBean.searchListSecuritiesNotPayingAgent(paymentDate, nextBusinessDay);
		if(listPaymentAgentNotEnable!=null&&listPaymentAgentNotEnable.size()>0) {
			List<String> strings = new ArrayList<>();
			for (SecuritiesNotPaymentAgentTO securitiesNotPaymentAgentTO : listPaymentAgentNotEnable) 
				strings.add(securitiesNotPaymentAgentTO.getIdSecurityCodePk());
			
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_NOT_PAYMENT_AGENT_CORRECT.getCode());
			log.info(":::CDE   Los siguientes valores no tienen contacto/agente pagador:"+strings);
			Object[] parameters = {strings};
			notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null, parameters);
			return;
		}
		
		List<Participant> listParticipants = parameterServiceBean.getListParticipantForCollection();
		/** PRIMERO RENTA FIJA*/
		List<Date> listDates = getListOfBusinessDays(paymentDate, nextBusinessDay);
		List<String> listNnemonics = null;
		List<Long>  listIdHolders =  null;
		List<String> lstIssuerPks = notificationPaymentAgentServiceBean.getListIdIssuersPks(paymentDate, nextBusinessDay);
		List<BigDecimal> listSecurityClass = notificationPaymentAgentServiceBean.getListSecurityClassRegistreds(paymentDate, nextBusinessDay);
		List<Long> idBanks = null;
		List<String> depNumbers =  null;
		/** Recorrido por Agente pagador, instrumento, fecha, emisor, moneda, participante, clase de valor y cui */
		for (Date date : listDates) {
			listNnemonics = notificationPaymentAgentServiceBean.getListMnemonicPaymentAgents(date);
			listIdHolders = notificationPaymentAgentServiceBean.getListIdHoldersByCollection(date);
			idBanks = notificationPaymentAgentServiceBean.getIdBanksRegistereds(date);
			depNumbers = notificationPaymentAgentServiceBean.getAccountNumberDeposits(date);
			for (String idIssuerPk : lstIssuerPks){
				for (String mNemonic : listNnemonics){
					/* Verificamos si existe registros para notificar en moneda BOB*/
					if (notificationPaymentAgentServiceBean.checkRecordsForCurrency(CurrencyType.PYG.getCode(), date, date)) {
						for (Participant participant : listParticipants) 
							for (BigDecimal securityClass : listSecurityClass)
								for (Long idHolderPk : listIdHolders) 	
									for (Long idBankPk : idBanks) 
										for (String depAccountNumber : depNumbers)
											sendNotification(processLogger, participant, idIssuerPk,mNemonic, securityClass.intValue(), CurrencyType.PYG.getCode(),idHolderPk, date,date,idBankPk,depAccountNumber);
					}
					/* Verificamos si existe registros para notificar en moneda USD */
					if (notificationPaymentAgentServiceBean.checkRecordsForCurrency(CurrencyType.USD.getCode(), date, date)) {
						for (Participant participant : listParticipants) 
							for (BigDecimal securityClass : listSecurityClass)
								for (Long idHolderPk : listIdHolders) 
									for (Long idBankPk : idBanks) 
										for (String depAccountNumber : depNumbers)
											sendNotification(processLogger, participant, idIssuerPk,mNemonic,securityClass.intValue(), CurrencyType.USD.getCode(),idHolderPk,date,date,idBankPk,depAccountNumber);
					}
					/* Verificamos si existe registros para notificar en moneda- UFV */
					if (notificationPaymentAgentServiceBean.checkRecordsForCurrency(CurrencyType.UFV.getCode(), date, date)) {
						for (Participant participant : listParticipants) 
							for (BigDecimal securityClass : listSecurityClass)
								for (Long idHolderPk : listIdHolders) 
									for (Long idBankPk : idBanks) 
										for (String depAccountNumber : depNumbers)
											sendNotification(processLogger, participant, idIssuerPk,mNemonic, securityClass.intValue(), CurrencyType.UFV.getCode(),idHolderPk, date, date,idBankPk,depAccountNumber);
					}
				}
			}
		}
		/** RENTA VARIABLE */

		log.info("CDDE :::  Fin de: Batchero para el envio de notificaciones a los agente pagadores");
	}
	/**
	 * Lista de fechas validas dentro de un rango
	 * */
	private List<Date> getListOfBusinessDays(Date paymentDate,Date nextBusinessDay){
		Date nextDate = paymentDate;
		List<Date> listDates = new ArrayList<>();
		listDates.add(paymentDate);
		Calendar cal = null;
		while(nextDate.before(nextBusinessDay)) {
			cal = Calendar.getInstance();
            cal.setTime(nextDate);
            cal.add(Calendar.DATE, 1);
            nextDate = cal.getTime();
            listDates.add(nextDate);
		};
		//listDates.add(nextBusinessDay);
		return listDates;
	}

	/** method to get the content of email for {@link PaymentAgent}
	 * 
	 * @param participant
	 * @param listSecurityDetailObjectTOs
	 * @param glosa
	 * @param paymentAgentJuridical
	 * @param UrlNotification
	 * @param currency
	 * @param descBank
	 * @param currentAccountNumberBank
	 * @param paymentWithLip
	 * @return */
	private DetailCollectionGlossTO getDetailNotification(Participant participant, List<SecurityDetailObjectTO> listSecurityDetailObjectTOs, String glosa,
			String paymentAgentJuridical, String urlNotification, Integer currency, String descBank, String currentAccountNumberBank, Integer paymentWithLip,
			Holder holderCui) {
		descBank=getTextHtmlFormat(descBank);
		StringBuilder emailContent = new StringBuilder();
		// email
		emailContent.append("<html>");
		emailContent.append("<head></head>");
		emailContent.append("<body>");
		emailContent.append("<p>");
		if(urlNotification!=null&&urlNotification.length()>0){
			emailContent.append("<p>Ingrese a la URL para confirmar la notificaci&oacute;n.</p>");
			emailContent.append("<br/>");
			emailContent.append("<p>");
			emailContent.append(urlNotification);
		}
		emailContent.append("</p>");
		emailContent.append("<br/>");
		emailContent.append("Estimados Se&ntilde;ores:");
		emailContent.append("<h3>");
		// agente pagador juridico
		emailContent.append(getTextHtmlFormat(paymentAgentJuridical));
		emailContent.append("</h3>");
		emailContent.append("<br/>");
		emailContent.append("<p>");
		emailContent.append("Por medio de la presente y en nuestra calidad de apoderados de ");
		// participante
		//emailContent.append(participant.getMnemonic());
		//emailContent.append(" ");
		emailContent.append(getTextHtmlFormat(participant.getDescription()));
		emailContent.append(", en virtud al poder Nro. ");
		// Adenda de contrato
		if (participant.getIdParticipantPk().equals(112L)) {
			emailContent.append("690/2004");
		} else {
			emailContent.append("002/05");
		}
		String notariaFePublica = "";
		StringBuilder parrafoFinal = new StringBuilder();
		parrafoFinal.append("<br/><p>As&iacute; mismo, informamos a usted que el origen y destino de los fondos proviene de la siguiente manera:</p>");
		if (participant.getIdParticipantPk() == 112L) {
			// notaria de fe bublica para FTB
			notariaFePublica = NotificationConstants.DRA_FUTURO;
			parrafoFinal.append("<p><b>Origen: </b>El origen de los recursos proviene de la administraci&oacute;n del FCI (Fondo de Capitalizaci&oacute;n Individual) administrado por Futuro de Bolivia S.A. AFP.</p>");
			parrafoFinal.append("<p><b>Destino: </b>El destino de los recursos ser&aacute;n empleados para la liquidaci&oacute;n de operaciones de inversi&oacute;n.</p>");
		} 
		if (participant.getIdParticipantPk() == 113L) {
			// notaria de fe publica para PRE
			notariaFePublica = NotificationConstants.DRA_PREVISION;
			parrafoFinal.append("<p><b>Origen: </b>Aportes de los afiliados al sistema integral de pensiones.</p>");
			parrafoFinal.append("<p><b>Destino: </b>Pago de pensiones.</p>");
		}
		if (participant.getIdParticipantPk() == 163L) {
			// notaria de fe publica para GPS
			notariaFePublica = "-----";
			parrafoFinal.append("<p><b>Origen: </b>Aportes de los afiliados al sistema integral de pensiones.</p>");
			parrafoFinal.append("<p><b>Destino: </b>Pago de pensiones.</p>");
		}
		

		String securitiesSingularOrPlural = "";
		if (listSecurityDetailObjectTOs.size() == 1) {
			securitiesSingularOrPlural = NotificationConstants.SECURITIES_SINGULAR;
		} else {
			if (listSecurityDetailObjectTOs.size() > 1) {
				securitiesSingularOrPlural = NotificationConstants.SECURITIES_PLURAL;
			}
		}
		emailContent.append(" de la Notaria de Fe P&uacute;blica ");
		emailContent.append(notariaFePublica);
		emailContent.append(", solicitamos a ustedes efectuar el pago ");
		emailContent.append(securitiesSingularOrPlural);
		emailContent.append(" a continuaci&oacute;n:");
		emailContent.append("</p>");
		emailContent.append("<div align='center'>");
		emailContent.append("<style>");
		emailContent.append("table { width: 80%;  border-collapse: separate; border-spacing: 1px; background: #000 bottom left repeat-x; color: #fff; }");
		emailContent.append("td, th { background: #fff; color: #000; }");
		emailContent.append("th { background: #aabbcc; }");
		emailContent.append("</style>");
		emailContent.append("<table>");
		emailContent.append("<tr>");
		emailContent.append("<th>Clase - Clave de Valor</th>");
		emailContent.append("<th>Nro. Cup&oacute;n</th>");
		emailContent.append("<th>Cantidad de Valores</th>");
		emailContent.append("<th>Fecha de Vencimiento</th>");
		emailContent.append("<th>Fecha de Cobro</th>");
		emailContent.append("<th>Moneda Origen</th>");
		emailContent.append("<th>Monto al Vencimiento</th>");
		emailContent.append("<th>Moneda de Cobro</th>");
		emailContent.append("<th>Monto a Cobrar</th>");
		emailContent.append("</tr>");
		String listCertificateNumbers = "";
		String currencyDescOrigen = null;
		String currencyDescCollection = null;
		BigDecimal sumAll = BigDecimal.ZERO;
		BigDecimal sumAllAux = BigDecimal.ZERO;
		boolean sw = true;
		for (SecurityDetailObjectTO securityDetailObjectTO : listSecurityDetailObjectTOs) {
			emailContent.append("<tr>");
			emailContent.append("<td align='center'>");
			/** En caso de que el agente pagador sea el BCB se debera poner el codigo BCB en el valor */
			Map<String,Object> parameter = new HashMap<String, Object>();
			parameter.put("idSecurityCodePkParam", securityDetailObjectTO.getIdSecurityCodePkOriginal());
			Security security = parameterServiceBean.findEntityByNamedQuery(Security.class, Security.SECURITIE_SINGLE_DATA,parameter);
			if (!security.getSecurityClass().equals(SecurityClassType.DPF.getCode())) {
				
				if (security.getPaymentAgent().equalsIgnoreCase(PaymentAgentType.PAYMNET_AGENT_BCB.getDescription())||
						security.getPaymentAgent().equalsIgnoreCase(PaymentAgentType.PAYMNET_AGENT_SOMA.getDescription())||
						security.getPaymentAgent().equalsIgnoreCase(PaymentAgentType.PAYMNET_AGENT_SOSP.getDescription())) {
				
					String codeSecurityBcb = security.getIdSecurityBcbCode() == null ? security.getIdSecurityCodePk() : security.getIdSecurityBcbCode();
					emailContent.append(codeSecurityBcb);
					// tambien el nro de cupon se modifica segun indique los ultimos caracteres del formato BCB
					try {
						String[] codeBcbSplit = codeSecurityBcb.split("-");
						int tam = codeBcbSplit.length;
						if (tam > 0) {
							int numberCouponObtain = Integer.parseInt(codeBcbSplit[tam - 1]);
							if (numberCouponObtain != 0)
								securityDetailObjectTO.setNumberCoupon(Integer.parseInt(codeBcbSplit[tam - 1]));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else
					emailContent.append(securityDetailObjectTO.getIdSecurityCodePk());
			} else
				emailContent.append(securityDetailObjectTO.getIdSecurityCodePk());

			emailContent.append("</td>");
			emailContent.append("<td align='center'>");
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
				emailContent.append(securityDetailObjectTO.getNumberCoupon());
			if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				emailContent.append("---");
			emailContent.append("</td>");
			emailContent.append("<td align='center'>");
			emailContent.append(securityDetailObjectTO.getAvailableBalance());
			emailContent.append("</td>");
			emailContent.append("<td align='center'>");
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
				emailContent.append(CommonsUtilities.convertDatetoString(securityDetailObjectTO.getDateExpired()));
			if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				emailContent.append("---");
			emailContent.append("</td>");
			emailContent.append("<td align='center'>");
			emailContent.append(CommonsUtilities.convertDatetoString(securityDetailObjectTO.getDatePayment()));
			emailContent.append("</td>");
			/** Moneda origen */
			emailContent.append("<td align='center'>");
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.PYG.getCode()))
				currencyDescOrigen = CurrencyType.PYG.getCodeIso();
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.UFV.getCode()))
				currencyDescOrigen = CurrencyType.UFV.getCodeIso();
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.DMV.getCode()))
				currencyDescOrigen = CurrencyType.DMV.getCodeIso();
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.USD.getCode()))
				currencyDescOrigen = CurrencyType.USD.getCodeIso();
			emailContent.append(currencyDescOrigen);
			emailContent.append("</td>");
			/** Monto al vencimiento */
			emailContent.append("<td align='center'>");
			BigDecimal origenAmount =CommonsUtilities.truncateDecimal(securityDetailObjectTO.getOriginAmount(), 2);//securityDetailObjectTO.getOriginAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode()))
				emailContent.append(accreditationGertificateUtil.getFormatCollection().format(origenAmount));
			if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				emailContent.append("---");
			emailContent.append("</td>");
			/** Otra moneda de cobro */
			emailContent.append("<td align='center'>");
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.PYG.getCode()))
				currencyDescCollection = CurrencyType.PYG.getCodeIso();
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.UFV.getCode()))
				currencyDescCollection = CurrencyType.UFV.getCodeIso();
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.DMV.getCode()))
				currencyDescCollection = CurrencyType.DMV.getCodeIso();
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.USD.getCode()))
				currencyDescCollection = CurrencyType.USD.getCodeIso();
			emailContent.append(currencyDescCollection);
			emailContent.append("</td>");
			/** Otro monto de cobro acordado */
			emailContent.append("<td align='center'>");
			BigDecimal anotherCollectionAmount = securityDetailObjectTO.getAnotherAmountReceivable().setScale(2, BigDecimal.ROUND_HALF_UP);
			emailContent.append(accreditationGertificateUtil.getFormatCollection().format(anotherCollectionAmount));
			emailContent.append("</td>");
			emailContent.append("</tr>");
			if (sw == true) {
				sw = false;
				listCertificateNumbers = securityDetailObjectTO.getCertificateNumber().toString();
			} else {
				listCertificateNumbers = listCertificateNumbers + ", " + securityDetailObjectTO.getCertificateNumber().toString();
			}
			sumAll = sumAll.add(anotherCollectionAmount);
			sumAllAux = sumAllAux.add(anotherCollectionAmount);
		}
		emailContent.append("</table>");
		emailContent.append("</div>");
		emailContent.append("<br/>");
		emailContent.append("<p>");
		emailContent.append("Adjuntamos certificado(s) de acreditaci&oacute;n de titularidad (CAT) Nro. ");
		// numero de CAT
		// listCertificateNumbers = listCertificateNumbers.substring(0, listCertificateNumbers.length()-1);
		emailContent.append(listCertificateNumbers);
		emailContent.append(", para el respectivo cobro.");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("En tal sentido, solicitamos a ustedes efectuar una transferencia electr&oacute;nica por un monto de ");
		// monto a cobrar
		emailContent.append("<strong>");
		emailContent.append(currencyDescCollection);
		emailContent.append(" ");
		sumAll =CommonsUtilities.truncateDecimal(sumAll, 2); 
		String descriptionCurrency="";
		if(currencyDescCollection.equals(CurrencyType.PYG.getCodeIso()))
			descriptionCurrency="Bolivianos";
		if(currencyDescCollection.equals(CurrencyType.USD.getCodeIso()))
			descriptionCurrency="D&oacute;lares Americanos";

		String sumAllFormated = accreditationGertificateUtil.getFormatCollection().format(sumAll);
		emailContent.append(sumAllFormated);
		// monto literal
		int amountReceivableInt = sumAll.intValue();
		BigDecimal fraccion = sumAll.remainder(BigDecimal.ONE);
		fraccion = fraccion.multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
		fraccion = fraccion.setScale(0);
		emailContent.append(" (");
		String literalNumber = CommonsUtilities.getLiteralNumber(amountReceivableInt);
		// primera letra en mayuscula
		literalNumber = literalNumber.substring(0, 1).toUpperCase() + literalNumber.substring(1);
		emailContent.append(literalNumber);
		emailContent.append(" ");
		// si la fraccion es menor a 10 concatenar un cero a la izquierda
		if (fraccion.compareTo(BigDecimal.TEN) == -1) {
			emailContent.append("0");
		}
		emailContent.append(fraccion);
		emailContent.append("/100 ");
		emailContent.append(descriptionCurrency);
		emailContent.append(")</strong>");
		emailContent.append(", ");
		// verificando si paga con LIP
		if (paymentWithLip.equals(PaymentAgentType.PAYMENT_LIP.getCode())) {
			emailContent.append("al <b>Banco Central de Bolivia (LIP)</b> para ser depositado en la <b>cuenta Nro. 8046 de la Entidad de Dep&oacute;sito de Valores de Bolivia S.A.</b>");
//			emailContent.append("la <strong>cuenta LIP de la EDV en el BCB Nro. de cuenta corriente: 254125884669.</strong>");
		} else {
			// cuando no es lip se setea el nombre del banco y su numero de cuneta corriente
			emailContent.append("al <b>");
			emailContent.append(descBank);
			emailContent.append("</b>");
			emailContent.append("para ser depositado en la <b>cuenta Nro. ");
			emailContent.append(currentAccountNumberBank);
			emailContent.append(" de ");
			emailContent.append(holderCui.getDescriptionHolder());
			emailContent.append(".</b>");
		}
		emailContent.append("</p>");
		
		if (paymentWithLip.equals(PaymentAgentType.PAYMENT_LIP.getCode())) {
			emailContent.append("<div align='center'>");
			emailContent.append("<p>");
			emailContent.append("<b>");
			emailContent.append("<font size='4'>");
			emailContent.append("\"LIP C&oacute;digo Cobro Derechos Econ&oacute;micos\"&nbsp;&nbsp;&nbsp;");
			emailContent.append(glosa);
			emailContent.append("</font>");
			emailContent.append("</b>");
			emailContent.append("</p>");
			emailContent.append("</div>");
		}
		if (holderCui.getIdHolderPk() == 451L || holderCui.getIdHolderPk() == 452L)
			emailContent.append(parrafoFinal.toString());
		emailContent.append("<br/><p>Sin otro particular, saludamos a usted muy atentamente.</p>");
		emailContent.append("</body>");
		emailContent.append("</html>");

		DetailCollectionGlossTO collectionGlossTO = new DetailCollectionGlossTO();
		collectionGlossTO.setIdParticipantPk(participant.getIdParticipantPk());
		collectionGlossTO.setCurrency(currency);
		collectionGlossTO.setGlosaNotClassified(glosa);
		collectionGlossTO.setAmountReceivable(sumAllAux);
		collectionGlossTO.setHtmlMessage(emailContent.toString());

		return collectionGlossTO;
	}

	/** Buscamos registro de cobros para la fecha de cobro y un dia siguiente habil
	 * 
	 * @param processLogger log de batchero para las notificaciones
	 * @param participant Participante con el cual se buscara los registros
	 * 
	 * @param processLogger */
	private void sendNotification(ProcessLogger processLogger, Participant participant, String idIssuerPk, String mNemonicPaymentAgent,Integer securityClass, Integer currency,Long idHolderPk,
			Date paymentDate, Date nextBusinessDay,Long idBankPk,String depAccountNumber) {
		
		List<SecurityDetailObjectTO> listSecurityDetailObjectTOs = null;
		/** Vencimientos registrados correctamente para ser notificados */
		SecuritiesAlmostObjectTO almostObjectTO = new SecuritiesAlmostObjectTO();
		/** Valores que no se encuentran registrados y que deben ser notificados */
		try {
			/** Genera glosa para el deposito */
			/* Vencimientos clasificados por participante, emisor,clase de valor y moneda */
			almostObjectTO = notificationFacadeBean.getListSecuritiesToNotifyCurrency(processLogger,participant.getIdParticipantPk(), idIssuerPk, mNemonicPaymentAgent, currency, idHolderPk,paymentDate,nextBusinessDay, securityClass,idBankPk,depAccountNumber);
			if (almostObjectTO == null) {
				// String message =
				// "No existen vencimientos registrados para el Participante:"+participant.getMnemonic()+" - "+participant.getDescription();
				//
				// log.info("CDDE   :::   "+message);
				//
				// BusinessProcess businessProcess = new BusinessProcess();
				// businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_COLLECTION_ECONOMIC_RIGTH.getCode());
				// Object[] parameters = {message};
				// notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null,
				// parameters);
				return;
			}

			if (almostObjectTO != null && almostObjectTO.getListSecurities() != null && almostObjectTO.getListSecurities().size() > 0) {
				String message = "Los siguientes valores: " + almostObjectTO.getListSecurities().toString() + " no tienen asignado el Agente/Contacto Pagador";

				log.info("CDDE   :::   " + message);

				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_COLLECTION_ECONOMIC_RIGTH.getCode());
				Object[] parameters = { message };
				notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null, parameters);
				processLogger.setErrorDetail(message);
				return;
			}
			List<SecuritiesAlmostToExpireTO> listSecuritiesToNotifyPaymentAgent = null;

			listSecuritiesToNotifyPaymentAgent = almostObjectTO.getAlmostToExpireTOs();

			/** Generando glosa */
			glosa = null;
			glosa = generateUrlNotification.generateGlosaNotClassified(participant.getIdParticipantPk(), participant.getMnemonic());
			listSecurityDetailObjectTOs = new ArrayList<SecurityDetailObjectTO>();
			listPaymentAgents = new HashMap<Long, PaymentAgent>();
			List<File> listFiles = new ArrayList<>();
			BigDecimal amountCollectionToGlossPdf = BigDecimal.ZERO;
			SecurityDetailObjectTO detailObjectTO = null;
			for (SecuritiesAlmostToExpireTO securityToNotify : listSecuritiesToNotifyPaymentAgent) {
				listPaymentAgents.putAll(securityToNotify.getMapContactPaymentAgent());
				File fileAdjunt = null;
				try {
					/**Enviando solo archivo con cat*/
					fileAdjunt = accreditationGertificateUtil.generateCertificateAccreditation(securityToNotify.getIdAccreditationOperationPk());
				} catch (Exception e) {
					e.printStackTrace();
				}
				AccreditationOperation accreOperation = notificationPaymentAgentServiceBean.findAccreditationOperation(securityToNotify.getIdAccreditationOperationPk());
				detailObjectTO = new SecurityDetailObjectTO();

				CollectionEconomicRigth cer = securityToNotify.getCollectionEconomicRigth();
				detailObjectTO.setInstrumentType(securityToNotify.getInstrumentType());
				detailObjectTO.setIdSecurityCodePk(cer.getIdSecurityCodeFk());
				detailObjectTO.setIdCollectionEconomicPk(cer.getIdCollectionEconomicPk());
				detailObjectTO.setIdGeneralPurposePk(cer.getIdGeneralPurposePk());
				detailObjectTO.setIdHolderAccountPk(cer.getIdHolderAccountPk());
				detailObjectTO.setNumberCoupon(cer.getCouponNumber());
				detailObjectTO.setCheckOther(cer.getCheckOther());
				/* Monto origen */
				detailObjectTO.setOriginAmount(cer.getAmountReceivable());
				/* Monto acordado */
				detailObjectTO.setAnotherAmountReceivable(cer.getAnotherAmountReceivable());
				/* Moneda Origen */
				detailObjectTO.setOriginCurrency(cer.getCurrency());
				/* Moneda acordada */
				detailObjectTO.setCollectionCurrency(cer.getAnotherCurency());
				/* Numero de certificado */
				detailObjectTO.setCertificateNumber(accreOperation.getCertificateNumber());
				/* Cantidad de valores */
				detailObjectTO.setAvailableBalance(cer.getAmountSecurities());
				/* Fecha de pago */
				detailObjectTO.setDatePayment(cer.getPaymentDate());

				detailObjectTO.setDateExpired(cer.getDueDate());

				// correccion en la glosa PDF que se envia adjunta
				amountCollectionToGlossPdf =   CommonsUtilities.truncateDecimal(detailObjectTO.getAnotherAmountReceivable(), 2);
				detailObjectTO.setCollectionAmount(amountCollectionToGlossPdf);
				
				detailObjectTO.setIdSecurityCodePkOriginal(cer.getIdSecurityCodeFk());
				if(fileAdjunt!=null)
					listFiles.add(fileAdjunt);

				// asignando los bancos correnpondientes a los agentes pagadores, para saber el banco destino
				Bank bank;
				for (PaymentAgent contactPaymen : listPaymentAgents.values()) {
					bank = notificationPaymentAgentServiceBean.findBank(cer.getIdBankFk());
					contactPaymen.setBankDescription(bank.getDescription());
					contactPaymen.setCurrentAccountNumber(cer.getDepAccountNumber());
					contactPaymen.setIdBank(cer.getIdBankFk());
				}

				/* actualizamos el cobro con notificacion enviada */
				listSecurityDetailObjectTOs.add(detailObjectTO);

				/* actualizamos el cobro con notificacion enviada */
				generateUrlNotification.updatNotification(cer.getIdCollectionEconomicPk(), securityToNotify.getCollectionEconomicRigth().getPaymentDate(), 0);// Enviado
				generateUrlNotification.updateGlosa(cer.getIdCollectionEconomicPk(), securityToNotify.getCollectionEconomicRigth().getPaymentDate(), glosa);

			}
			/* Email de todos los valores */
			DetailCollectionGlossTO detailCollectionGlossTO = null;
			String urlNotificacion;
			List<UserAccountSession> lstUserAccount;
			UserAccountSession accountSession;

			File fileGlosaPdf = null;
			
			List<File> lstFileSendNotification;
			
			PaymentAgent paymentAgent;
			String paymentAgentJurudical;
			BigDecimal totalAmountPaymet = BigDecimal.ZERO;
			BigDecimal anotherCollectionAmoun=BigDecimal.ZERO;
			Holder holder=null;
			boolean isNoLip;
			//mascara para la url generada, es un token de desarrollo
			//generateUrlNotification
//			BitlyClient client = null;
//			Response<ShortenResponse> resp;
//			String tokenBitly = generateUrlNotification.getTokenBitly();
//			if (tokenBitly != null)
//				client = new BitlyClient(tokenBitly);
			/* Envio de los CAT(s) a los contactos agente pagador */
			for (PaymentAgent contactPaymen : listPaymentAgents.values()) {
				accountSession = new UserAccountSession();
				accountSession.setUserName(contactPaymen.getFirstName());
				accountSession.setEmail(contactPaymen.getEmail());
				accountSession.setPhoneNumber(contactPaymen.getCellPhone());
				accountSession.setFullName(contactPaymen.getFirstName());
				lstUserAccount = new ArrayList<UserAccountSession>();
				lstUserAccount.add(accountSession);
				
				paymentAgentJurudical="Agente pagador";
				paymentAgent=notificationPaymentAgentServiceBean.find(PaymentAgent.class,contactPaymen.getIdPaymentAgentFk());
				if(paymentAgent!=null){
					paymentAgentJurudical=paymentAgent.getDescription()==null?"":paymentAgent.getDescription();
				}
				
				// verificando si ya se ha calculado el montoTotalCobro
				if (totalAmountPaymet.compareTo(BigDecimal.ZERO) == 0){
					Long idHolderAccountPk=0L;
					boolean firstLoop=true;
					//obtenidndo el monto total de cobro 
					for (SecurityDetailObjectTO securityDetailObjectTO : listSecurityDetailObjectTOs) {
						anotherCollectionAmoun = CommonsUtilities.truncateDecimal(securityDetailObjectTO.getAnotherAmountReceivable(), 2);
						totalAmountPaymet = totalAmountPaymet.add(anotherCollectionAmoun);
						//capturando el idHolderAccountPk para luego obtener la descripcion del CUI
						if(firstLoop){
							idHolderAccountPk=securityDetailObjectTO.getIdHolderAccountPk();
							firstLoop=false;
						}
					}
					holder=notificationPaymentAgentServiceBean.getHolderFromHolderAccount(idHolderAccountPk);
				}
				
				isNoLip = false;
				if (contactPaymen.getIndLip().equals(PaymentAgentType.PAYMENT_NO_LIP.getCode()))
					//verificando si se trata de una agente pagador SOMA-BCB
					if (!contactPaymen.getIdPaymentAgentFk().toString().equals(PaymentAgentType.PAYMNET_AGENT_SOMA.getCode().toString()))
						isNoLip = true;
				// se ingresan los parametros para la url de acuse de pago
				urlNotificacion = generateUrlNotification.generateUrlNotification(contactPaymen, listSecurityDetailObjectTOs,
						participant.getIdParticipantPk(), currency, totalAmountPaymet, glosa, paymentAgentJurudical, holder.getDescriptionHolder(), isNoLip);
				//cifrando la url con bit.ly solo para los NO LIP 
//				if (client != null)
//					if (isNoLip) {
//						resp = client.shorten().setLongUrl(urlNotificacion).call();
//						urlNotificacion = resp.data.url;
//					}
				
				//System.out.println("### url obtenida: "+urlNotificacion);

				/* Construye el mensaje HTML y retorna del detalle del mensaje */
				detailCollectionGlossTO = getDetailNotification(participant, listSecurityDetailObjectTOs, glosa, paymentAgentJurudical, urlNotificacion, currency,
						contactPaymen.getBankDescription(), contactPaymen.getCurrentAccountNumber(), contactPaymen.getIndLip(), holder);
				fileGlosaPdf = buildNoticeNote(processLogger, participant, listSecurityDetailObjectTOs, glosa, paymentAgentJurudical, contactPaymen.getIndLip(),
						contactPaymen.getBankDescription(), contactPaymen.getCurrentAccountNumber(),holder);
				
				//guardando en un transient el archivo y el contenido de correo
				contactPaymen.setGlossPdf(Files.readAllBytes(fileGlosaPdf.toPath()));
				contactPaymen.setHtmlMessage(detailCollectionGlossTO.getHtmlMessage());
				
				lstFileSendNotification=new ArrayList<>();
				lstFileSendNotification.add(fileGlosaPdf);
				lstFileSendNotification.addAll(listFiles);

				notificationServiceFacade.sendNotificationManualEmailAdjuntHtmlContent(processLogger.getIdUserAccount(), processLogger.getBusinessProcess(),lstUserAccount, NotificationConstants.SUBJECT_EMAIL_PAYMENT_AGENT, detailCollectionGlossTO.getHtmlMessage(), null, lstFileSendNotification);
				//notificationServiceFacade.sendNotificationManualEmailAdjuntHtmlContent(processLogger.getIdUserAccount(),paymentAgent, processLogger.getBusinessProcess(),lstUserAccount, NotificationConstants.SUBJECT_EMAIL_PAYMENT_AGENT, detailCollectionGlossTO.getHtmlMessage(), null, lstFileSendNotification);
				generateUrlNotification.registreNotificationReadRecord(contactPaymen.getIdPaymentAgentPk(), listSecurityDetailObjectTOs);
			}
			
			if (detailCollectionGlossTO != null){
				Long idPaymentAgentPk = generateUrlNotification.searchPaymenAgentByNotificationId(mNemonicPaymentAgent);
				// obteniendo todos los numeros de CATS han sido generados
				StringBuilder lstIdCatsSent=new StringBuilder();
				boolean firstLoop=true;
				for (SecuritiesAlmostToExpireTO securityToNotify : listSecuritiesToNotifyPaymentAgent) {
					if (firstLoop) {
						firstLoop = false;
						lstIdCatsSent.append(securityToNotify.getIdAccreditationOperationPk());
					} else {
						lstIdCatsSent.append(",");
						lstIdCatsSent.append(securityToNotify.getIdAccreditationOperationPk());
					}
				}
				
				List<GlossNoticationDetail> lstGlossNoticationDetail=new ArrayList<>();
				GlossNoticationDetail glossNoticationDetail;
				/*lista de contactos agente pagador*/
				for (PaymentAgent contactPaymen : listPaymentAgents.values()) {
					glossNoticationDetail=new GlossNoticationDetail();
					//seteando los datos, excepto el idFkGlossCode que sera seteado mas adelante
					glossNoticationDetail.setIdFkPaymentAgent(new PaymentAgent(contactPaymen.getIdPaymentAgentPk()));
					glossNoticationDetail.setHtmlMessage(contactPaymen.getHtmlMessage());
					glossNoticationDetail.setGlossPdf(contactPaymen.getGlossPdf());
					//0 notificado, 1:reenviado. Siempre sera cero en el envio de notificacion
					glossNoticationDetail.setNotificationSent(BooleanType.NO.getBinaryValue());
					lstGlossNoticationDetail.add(glossNoticationDetail);
				}
				
				detailCollectionGlossTO.setIdIssuerPk(idIssuerPk);
				detailCollectionGlossTO.setPaymentDate(paymentDate);
				detailCollectionGlossTO.setArrayStringIdCatSender(lstIdCatsSent.toString());
				detailCollectionGlossTO.setLstGlossNoticationDetail(lstGlossNoticationDetail);
				/*Id del agente pagador*/
				detailCollectionGlossTO.setIdPaymentAgentFk(idPaymentAgentPk);
				/*Clase de Valor*/
				detailCollectionGlossTO.setSecurityClass(securityClass);
				/**cui-titular*/
				detailCollectionGlossTO.setIdHolderPk(idHolderPk);
				notificationPaymentAgentServiceBean.registerNotificationDetail(detailCollectionGlossTO,processLogger);
			}
				
		} catch (Exception e) {
			log.info("CDE::::: error el poceso de (la obtencion de valores a notificar y registro o confirmacion de CAT)");
			e.printStackTrace();
		}
	}

	/**
	 * metodo para formatear un texto a formato html
	 * @param text
	 * @return
	 */
	public String getTextHtmlFormat(String text) {
		StringBuilder newText=new StringBuilder();
		String caracterSpecial;
		int letra;
		if(text!=null){
			for (int i = 0; i < text.length(); i++) {
				letra = text.charAt(i);
				caracterSpecial="";
				if (letra == 225)
					caracterSpecial="&aacute;";
				if (letra == 233)
					caracterSpecial="&eacute;";
				if (letra == 237)
					caracterSpecial="&iacute;";
				if (letra == 243)
					caracterSpecial="&oacute;";
				if (letra == 250)
					caracterSpecial="&uacute;";
				if (letra == 241)
					caracterSpecial="&ntilde;";
				if (letra == 193)
					caracterSpecial="&Aacute;";
				if (letra == 201)
					caracterSpecial="&Eacute;";
				if (letra == 205)
					caracterSpecial="&Iacute;";
				if (letra == 211)
					caracterSpecial="&Oacute;";
				if (letra == 218)
					caracterSpecial="&Uacute;";
				if (letra == 209)
					caracterSpecial="&Ntilde;";
				if(caracterSpecial.length()>0){
					newText.append(caracterSpecial);
				}else{
					newText.append(text.substring(i,i+1));
				}
			}
		}else{
			newText.append("");
		}
		
		return newText.toString();
	}

	/** metodo para obetener la 1ra parte de la glosa
	 * 
	 * @return */
	private String getFirstText(String descParticipant, String numberAdenda, String notariaFePublica, String securitiesSingularOrPlural) {
		return "<p>De nuestra consideraci&oacute;n.</p><p>Por medio de la Presente y en nuestra calidad de apoderados de " + descParticipant + ", en virtud al poder Nro. " + numberAdenda
				+ " de la Notaria de Fe P&uacute;blica " + notariaFePublica + ", solicitamos a ustedes efectuar el pago " + securitiesSingularOrPlural
				+ " a continuaci&oacute;n:</p>";
	}

	/** metodo para obtener la segunda parte de la glosa
	 * 
	 * @param listNumberCats
	 * @param descAmountPayment
	 * @param descBank
	 * @param currentAccountNumberBank
	 * @param paymentLip
	 * @return */
	private String getSecondText(String listNumberCats, String descAmountPayment, String descBank, String currentAccountNumberBank, Integer indLip, String parrafoFinal, String codeGloss, String descParticipant,
			Holder holderCui) {
		descBank=getTextHtmlFormat(descBank);
		StringBuilder text = new StringBuilder();
		text.append("<p>Adjuntamos Certificado(s) de Acreditaci&oacute;n de Titularidad (CAT) Nro. ");
		text.append(listNumberCats);
		text.append(" , para el respectivo cobro.</p>");

		text.append("<br/><p>En tal sentido, solicitamos a ustedes efectuar una transferencia electr&oacute;nica por un monto de <b>");
		text.append(descAmountPayment);
		text.append("</b> ");
		// verificando si paga o no paga con LIP
		if (indLip.equals(PaymentAgentType.PAYMENT_LIP.getCode())) {
			text.append("al <b>Banco Central de Bolivia (LIP)</b> para ser depositado en la <b>cuenta Nro. 8046 de la Entidad de Dep&oacute;sito de Valores de Bolivia S.A.</b></p>");
			//text.append("la <b>cuenta LIP de la EDV en el BCB Nro. de cuenta corriente: 254125884669.</b></p>");
		} else {
			// cuando no es lip se setea el nombre del banco y su numero de cuneta corriente
			text.append("al <b>");
			text.append(descBank);
			text.append("</b>");
			text.append(" para ser depositado en la <b>cuenta Nro. ");
			text.append(currentAccountNumberBank);
			text.append(" de ");
			text.append(holderCui.getDescriptionHolder());
			text.append("</b></p>");
		}
		
		if (indLip.equals(PaymentAgentType.PAYMENT_LIP.getCode())) {
			text.append("<p>");
			text.append("<b>");
			text.append("<font size='4'>");
			text.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"LIP C&oacute;digo Cobro Derechos Econ&oacute;micos\"&nbsp;&nbsp;&nbsp;");
			text.append(codeGloss);
			text.append("</font>");
			text.append("</b>");
			text.append("</p>");
		}

		// parrafo final, solo se muestra para los cuis 451,452
		if(holderCui.getIdHolderPk() == 451L || holderCui.getIdHolderPk() == 452L){
			text.append("<br/><p>As&iacute; mismo, informamos a usted que el origen y destino de los fondos proviene de la siguiente manera:</p>");
			text.append(parrafoFinal);
		}
		
		text.append("<br/><p>Sin otro particular, saludamos a usted muy atentamente.</p>");
		return text.toString();
	}

	/** metodo para cosntruir la glosa de notificacion
	 * 
	 * @param processLogger
	 * @param participant
	 * @param listSecurityDetailObjectTOs
	 * @param glosa
	 * @param paymentAgentJuridical
	 * @return */
	public File buildNoticeNote(ProcessLogger processLogger, Participant participant, List<SecurityDetailObjectTO> listSecurityDetailObjectTOs, String glosa,
			String paymentAgentJuridical, Integer indLip, String decBank, String currentNumberBank, Holder holderCui) {
		List<File> listFiles = new ArrayList<>();

		BigDecimal collectionAmount;
		BigDecimal sumAll = BigDecimal.ZERO;
		String currencyDescOrigen = CommonsUtilities.STR_BLANK;
		String currencyDescCollection = CommonsUtilities.STR_BLANK;
		String listCertificateNumbers = CommonsUtilities.STR_BLANK;
		boolean sw = true;
		Date paymentDateToGloss=null;
		for (SecurityDetailObjectTO securityDetailObjectTO : listSecurityDetailObjectTOs) {
			/** En caso de que el agente pagador sea el BCB se debera poner el codigo BCB en el valor */
			//Security security = parameterServiceBean.find(Security.class, securityDetailObjectTO.getIdSecurityCodePkOriginal());
			Map<String,Object> parameter = new HashMap<String, Object>();
			parameter.put("idSecurityCodePkParam", securityDetailObjectTO.getIdSecurityCodePkOriginal());
			Security security = parameterServiceBean.findEntityByNamedQuery(Security.class, Security.SECURITIE_SINGLE_DATA,parameter);
			securityDetailObjectTO.setInstrumentType(security.getInstrumentType());
			if (!security.getSecurityClass().equals(SecurityClassType.DPF.getCode())) {
				if (security.getPaymentAgent().equalsIgnoreCase(PaymentAgentType.PAYMNET_AGENT_BCB.getDescription())||
					security.getPaymentAgent().equalsIgnoreCase(PaymentAgentType.PAYMNET_AGENT_SOMA.getDescription())||
					security.getPaymentAgent().equalsIgnoreCase(PaymentAgentType.PAYMNET_AGENT_SOSP.getDescription())) {
					String codeSecurityBcb=security.getIdSecurityBcbCode()==null?security.getIdSecurityCodePk():security.getIdSecurityBcbCode();
					securityDetailObjectTO.setIdSecurityCodePk(codeSecurityBcb);
					try {
						// tambien el nro de cupon se modifica segun indique los ultimos caracteres del formato BCB
						String[] codeBcbSplit = codeSecurityBcb.split("-");
						int tam = codeBcbSplit.length;
						if (tam > 0) {
							int numberCouponObtain = Integer.parseInt(codeBcbSplit[tam - 1]);
							if (numberCouponObtain != 0)
								securityDetailObjectTO.setNumberCoupon(Integer.parseInt(codeBcbSplit[tam - 1]));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			collectionAmount = CommonsUtilities.truncateDecimal( securityDetailObjectTO.getAnotherAmountReceivable(), 2);
			sumAll = sumAll.add(collectionAmount);
			securityDetailObjectTO.setDateExpiredText(CommonsUtilities.convertDatetoString(securityDetailObjectTO.getDateExpired()));
			securityDetailObjectTO.setDatePaymentText(CommonsUtilities.convertDatetoString(securityDetailObjectTO.getDatePayment()));
			if(paymentDateToGloss==null){
				paymentDateToGloss=securityDetailObjectTO.getDatePayment();
			}

			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.PYG.getCode()))
				currencyDescOrigen = CurrencyType.PYG.getCodeIso();
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.UFV.getCode()))
				currencyDescOrigen = CurrencyType.UFV.getCodeIso();
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.DMV.getCode()))
				currencyDescOrigen = CurrencyType.DMV.getCodeIso();
			if (securityDetailObjectTO.getOriginCurrency().equals(CurrencyType.USD.getCode()))
				currencyDescOrigen = CurrencyType.USD.getCodeIso();
			securityDetailObjectTO.setOriginCurrencyText(currencyDescOrigen);

			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.PYG.getCode()))
				currencyDescCollection = CurrencyType.PYG.getCodeIso();
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.UFV.getCode()))
				currencyDescCollection = CurrencyType.UFV.getCodeIso();
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.DMV.getCode()))
				currencyDescCollection = CurrencyType.DMV.getCodeIso();
			if (securityDetailObjectTO.getCollectionCurrency().equals(CurrencyType.USD.getCode()))
				currencyDescCollection = CurrencyType.USD.getCodeIso();
			securityDetailObjectTO.setCollectionCurrencyText(currencyDescCollection);

			if (sw == true) {
				sw = false;
				listCertificateNumbers = securityDetailObjectTO.getCertificateNumber().toString();
			} else {
				listCertificateNumbers = listCertificateNumbers + ", " + securityDetailObjectTO.getCertificateNumber().toString();
			}
		}

		// obteniendo descripcion del monto total a pagar
		// parte fraccionaria
		BigDecimal fraccion = sumAll.remainder(BigDecimal.ONE);
		fraccion = fraccion.multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
		fraccion = fraccion.setScale(0);
		String fractionAmount = "" + fraccion;
		if (fraccion.compareTo(BigDecimal.TEN) == -1) {
			fractionAmount = "0" + fraccion;
		}
		// monto literal
		String montoLiteral = CommonsUtilities.getLiteralNumber(sumAll.intValue());
		// primera letra en mayuscula
		montoLiteral = montoLiteral.substring(0, 1).toUpperCase() + montoLiteral.substring(1);
		// Monto a pagar literal
		String descriptionCurrency="";
		if(currencyDescCollection.equals(CurrencyType.PYG.getCodeIso()))
			descriptionCurrency="Bolivianos";
		if(currencyDescCollection.equals(CurrencyType.USD.getCodeIso()))
			descriptionCurrency="D&oacute;lares Americanos";
		String paymentAmountParamenter = currencyDescCollection+" " + accreditationGertificateUtil.getFormatCollection().format(sumAll) + " (" + montoLiteral + " " + fractionAmount + "/100 "+descriptionCurrency+")";

		// primer parrafo
		String notariaFePublica = "";
		StringBuilder parrafoFinal = new StringBuilder();
		if (participant.getIdParticipantPk() == 112L) {
			// notaria de fe bublica para FTB
			notariaFePublica = NotificationConstants.DRA_FUTURO;
			parrafoFinal
					.append("<p><b>Origen: </b>El origen de los recursos proviene de la administraci&oacute;n del FCI (Fondo de Capitalizaci&oacute;n Individual) administrado por Futuro de Bolivia S.A. AFP.</p>");
			parrafoFinal
					.append("<p><b>Destino: </b>El destino de los recursos ser&aacute;n empleados para la liquidaci&oacute;n de operaciones de inversi&oacute;n.</p>");
		} else {
			// notaria de fe publica para PRE
			notariaFePublica = NotificationConstants.DRA_PREVISION;
			parrafoFinal.append("<p><b>Origen: </b>Aportes de los afiliados al sistema integral de pensiones.</p>");
			parrafoFinal.append("<p><b>Destino: </b>Pago de pensiones.</p>");
		}
		String securitiesSingularOrPlural = "";
		if (listSecurityDetailObjectTOs.size() == 1) {
			securitiesSingularOrPlural = NotificationConstants.SECURITIES_SINGULAR;
		} else {
			if (listSecurityDetailObjectTOs.size() > 1) {
				securitiesSingularOrPlural = NotificationConstants.SECURITIES_PLURAL;
			}
		}

		String textHtml1 = getFirstText( participant.getDescription(), participant.getIdParticipantPk().equals(112L) ? "690/2004"
				: "002/05", notariaFePublica, securitiesSingularOrPlural);
		String textHtml2 = getSecondText(listCertificateNumbers, paymentAmountParamenter, decBank, currentNumberBank, indLip, parrafoFinal.toString(),glosa,participant.getDescription(),holderCui);

		Map<String, Object> reportParameters = new HashMap<String, Object>();
		reportParameters.put("logo", this.getClass().getResourceAsStream("/logo/logoEdv.PNG"));
		reportParameters.put("p_first_text", textHtml1);
		reportParameters.put("p_second_text", textHtml2);
		reportParameters.put("p_glosa", glosa);
		reportParameters.put("p_payment_agent_juridical", paymentAgentJuridical);
		reportParameters.put("p_date_glosa", getDateToGloss(paymentDateToGloss==null?new Date():paymentDateToGloss));

		File fileGlosaPdf = null;
		try {
			fileGlosaPdf = accreditationGertificateUtil.generateGlosaPdf(reportParameters, listSecurityDetailObjectTOs);
			listFiles.add(fileGlosaPdf);
		} catch (Exception e) {
			fileGlosaPdf = null;
		}
		return fileGlosaPdf;
	}
	
	private String getMonthText(int month){
		switch (month) {
		case 0: return "Enero";
		case 1: return "Febrero";
		case 2: return "Marzo";
		case 3: return "Abril";
		case 4: return "Mayo";
		case 5: return "Junio";
		case 6: return "Julio";
		case 7: return "Agosto";
		case 8: return "Septiembre";
		case 9: return "Octubre";
		case 10: return "Noviembre";
		case 11: return "Diciembre";
		}
		return "";
	}
	
	public String getDateToGloss(Date paymentDate){
		StringBuilder dGloss=new StringBuilder();
		Calendar fechaActual=Calendar.getInstance();
		fechaActual.setTime(paymentDate);
		fechaActual.get(Calendar.YEAR);
		dGloss.append("La Paz, ");
		dGloss.append(fechaActual.get(Calendar.DAY_OF_MONTH)>9?fechaActual.get(Calendar.DAY_OF_MONTH):"0"+fechaActual.get(Calendar.DAY_OF_MONTH));
		dGloss.append(" de ");
		dGloss.append(getMonthText(fechaActual.get(Calendar.MONTH)));
		dGloss.append(" de ");
		dGloss.append(fechaActual.get(Calendar.YEAR));
		return dGloss.toString();
	}

	/** Inicio de dia */
	public boolean isDaysIsOpen() throws ServiceException {
		BeginEndDay beginEndDay = notificationPaymentAgentServiceBean.getBeginEndDay();
		if (Validations.validateIsNotNullAndNotEmpty(beginEndDay))
			return true;
		else
			return false;
	}

	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	@Override
	public boolean sendNotification() {
		return false;
	}
}