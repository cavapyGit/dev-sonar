package com.pradera.notification.batch;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;

@RequestScoped
@BatchProcess(name = "NotificationPaymentAgentCurrentBatch")
public class NotificationPaymentAgentCurrentBatch implements JobExecution, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5018405500822466669L;
	@EJB
	BatchServiceBean batchServiceBean;

	@Override
	public void startJob(ProcessLogger processLogger) {
		Map<String, Object> param = new HashMap<String, Object>();
		String onlyCurrentDate = Boolean.TRUE.toString();
		param.put("onlyCurrentDate", onlyCurrentDate);
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CERTIFICATE_ACCREDITATION_PAYMENT_AGENT.getCode());
		try {
			this.batchServiceBean.registerBatchTx(processLogger.getIdUserAccount(), businessProcess, param);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
}
