package com.pradera.notification.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.right.to.ResultUrlAccusePaymentTO;
import com.pradera.collection.right.to.ResultoWsNotification;
import com.pradera.collection.right.util.GenerateAccreditationGertificateUtil;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.collection.right.util.NotificationConstants;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.notification.facade.ElectronicCatWebFacade;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.PaymentAgent;

@BatchProcess(name = "CatWebCollectionEconomicBatch")
@RequestScoped
public class CatWebCollectionEconomicBatch implements Serializable, JobExecution {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	@DepositaryDataBase
	private EntityManager em;

	/** The security service facade. */
	@Inject
	ElectronicCatWebFacade electronicCatWebFacade;

	@Inject
	private GenerateUrlNotification generateUrlNotification;

	@Inject
	private GenerateAccreditationGertificateUtil accreditationGertificateUtil;

	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;

	@Inject
	NotificationServiceFacade notificationServiceFacade;

	@Inject
	private PraderaLogger log;

	@Override
	public void startJob(ProcessLogger processLogger) {
		/*log.info("*********** BATCH CONFIRMACION DE RECIBO DE NOTIFICACIONES DE COBRO DE DERECHOS   *************");
		try {
			List<ResultoWsNotification> lstResultoWsNotification = electronicCatWebFacade.getInfoNotificationCollectionEconomic();

			// enviando notidicacion de acuse de pago a los contacto agente pagador NO LIP correspondientes
			List<UserAccountSession> lstUserAccount;
			PaymentAgent paymentAgent;
			String urlAccusePayment = "";
			Bank bank;
			// mascara para la url generada, es un token de desarrollo
			BitlyClient client = null;
			Response<ShortenResponse> resp;
			String tokenBitly = generateUrlNotification.getTokenBitly();
			if (tokenBitly != null)
				client = new BitlyClient(tokenBitly);
			for (ResultoWsNotification result : lstResultoWsNotification) {

				// actualizando la tabla en la cual se informa la confirmacion de recibo de notificacion
				for (Long l : result.getIdCollectionEconomicPks())
					generateUrlNotification.updatNotification(l, result.getGenerationDate(), 1);

				paymentAgent = em.find(PaymentAgent.class, result.getIdPaymentAgent());
				UserAccountSession accountSession = new UserAccountSession();
				accountSession.setUserName(paymentAgent.getFirstName());
				accountSession.setEmail(paymentAgent.getEmail());
				accountSession.setPhoneNumber(paymentAgent.getCellPhone());
				accountSession.setFullName(paymentAgent.getFirstName());
				lstUserAccount = new ArrayList<UserAccountSession>();
				lstUserAccount.add(accountSession);

				urlAccusePayment = generateUrlNotification.generateUrlAcknowledgmentPayment(result.getIdPaymentAgent(), result.getIdParticiapntPk(),
						result.getCurrency(), result.getAmountPayment(), result.getGlossCode(), result.getIdBank());
				if (client != null) {
					resp = client.shorten().setLongUrl(urlAccusePayment).call();
					urlAccusePayment = resp.data.url;
				}
				
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_COLLECTION_ECONOMIC_RIGTH.getCode());

				String descriptionTotalAmount = "";
				try {
					String currencyDescCollection = "";
					if (result.getCurrency().equals(CurrencyType.PYG.getCode()))
						currencyDescCollection = CurrencyType.PYG.getCodeIso();
					if (result.getCurrency().equals(CurrencyType.UFV.getCode()))
						currencyDescCollection = CurrencyType.UFV.getCodeIso();
					if (result.getCurrency().equals(CurrencyType.DMV.getCode()))
						currencyDescCollection = CurrencyType.DMV.getCodeIso();
					if (result.getCurrency().equals(CurrencyType.USD.getCode()))
						currencyDescCollection = CurrencyType.USD.getCodeIso();

					String descriptionCurrency = "";
					if (currencyDescCollection.equals(CurrencyType.PYG.getCodeIso()))
						descriptionCurrency = "Bolivianos";
					if (currencyDescCollection.equals(CurrencyType.USD.getCodeIso()))
						descriptionCurrency = "D&oacute;lares Americanos";

					// obteniendo descripcion del monto total a pagar
					// parte fraccionaria
					BigDecimal fraccion = result.getAmountPayment().remainder(BigDecimal.ONE);
					fraccion = fraccion.multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
					fraccion = fraccion.setScale(0);
					String fractionAmount = "" + fraccion;
					if (fraccion.compareTo(BigDecimal.TEN) == -1) {
						fractionAmount = "0" + fraccion;
					}
					// monto literal
					String montoLiteral = CommonsUtilities.getLiteralNumber(result.getAmountPayment().intValue());
					// primera letra en mayuscula
					montoLiteral = montoLiteral.substring(0, 1).toUpperCase() + montoLiteral.substring(1);
					descriptionTotalAmount = currencyDescCollection + " " + accreditationGertificateUtil.getFormatCollection().format(result.getAmountPayment()) + " ("
							+ montoLiteral + " " + fractionAmount + "/100 " + descriptionCurrency + ")";
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					bank = em.find(Bank.class, result.getIdBank());
				} catch (Exception e) {
					e.printStackTrace();
					bank = new Bank();
					bank.setDescription("---");
				}

				notificationServiceFacade.sendNotificationManualEmailAdjuntHtmlContent(
						"ADMIN",
						businessProcess,
						lstUserAccount,
						NotificationConstants.SUBJECT_EMAIL_ACCUSE_PAYMENT,
						getContentEmailAccusePayment(urlAccusePayment, result.getDescPaymentAgentJuridical(), descriptionTotalAmount,
								getTextHtmlFormat(bank.getDescription()), result.getAccountBankNumber(), result.getDescCuiHolder()), null, null);
				System.out.println(":::: ENVIO DE ACUSE DE PAGO AL AGENTE PAGADOR " + paymentAgent.getIdPaymentAgentPk() + "-" + paymentAgent.getDescription()
						+ " :::::: " + new Date().toString());

				// guardar el contenido del correo de acuse de pago en la tabla GLOSS_NOTIFICATION_DETAIL
				generateUrlNotification.updateNotificationPayment(
						result.getIdPaymentAgent(),
						result.getGlossCode(),
						getContentEmailAccusePayment(urlAccusePayment, result.getDescPaymentAgentJuridical(), descriptionTotalAmount,
								getTextHtmlFormat(bank.getDescription()), result.getAccountBankNumber(), result.getDescCuiHolder()));
			}

			// realizando el pago manual con los contacto agente pagador que NO pagan con LIP
			List<ResultUrlAccusePaymentTO> lstResultoWsPayment = electronicCatWebFacade.getInfoPaymentCollectionEconomic();
			for (ResultUrlAccusePaymentTO result : lstResultoWsPayment) {
				
				RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
				retirementObjetTO.setGloss(result.getGlossCode());
				retirementObjetTO.setIdParticipantPk(result.getIdParticiapntPk());
				List<DetailCollectionGloss> listDetailCollection = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
				DetailCollectionGloss collectionGloss = listDetailCollection.get(0);
				if(collectionGloss.getDepositStatus().equals(DepositStateType.CONFIRMED.getCode())||
						collectionGloss.getSourceDeposit().equals(BooleanType.YES.getCode())){
					log.info("CDE::::: El deposito ya fue ejecutado de la glosa:"+result.getGlossCode());
				}
				if(collectionGloss.getTransferStatus().equals(DepositStateType.REGISTERED.getCode())){
					collectionGloss.setTransferStatus(DepositStateType.CONFIRMED.getCode());
					collectionGloss.setAmountDeposited(collectionGloss.getAmountReceivable());
					if(result.getIdBank()!=null)
						collectionGloss.setIdBankPk(result.getIdBank());
					collectionGloss.setDepositDate(CommonsUtilities.currentDate());
					em.merge(collectionGloss);
					log.info("CDE :::::Se realizO el pago de la glosa : " + result.getGlossCode() + " mediante url de Acuse de Pago (NO LIP)");
				}
			}
			System.out.println("********** FIN BATCH CONFIRMACION DE RECIBO DE NOTIFICACIONES DE COBRO DE DERECHOS ************");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: "+e.getMessage());
		}*/
	}
	
	/** armando el contenido de correro para le url de acuse de pago
	 * 
	 * @param urlAccusePayment
	 * @return */
	private String getContentEmailAccusePayment(String urlAccusePayment, String paymentAgentJuridical, String descriptionTotalAmount, String descBank,
			String accountNumber, String descriptionCuiHolder) {
		StringBuilder emailContent = new StringBuilder();
		emailContent.append("<html>");
		emailContent.append("<head></head>");
		emailContent.append("<body>");
		emailContent.append("<br/>");
		emailContent.append("<p>Se&ntilde;ores:");
		emailContent.append("<br/>");
		emailContent.append("<b>");
		emailContent.append(paymentAgentJuridical);
		emailContent.append("</b>");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("Solicitamos su confirmaci&oacute;n de pago del Vencimiento de Valores por un monto de ");
		emailContent.append("<b>");
		emailContent.append(descriptionTotalAmount);
		emailContent.append("</b>");
		emailContent.append(" al ");
		emailContent.append("<b>");
		emailContent.append(descBank);
		emailContent.append("</b>");
		emailContent.append(", a la cuenta Nro.: ");
		emailContent.append("<b>");
		emailContent.append(accountNumber);
		emailContent.append("</b>");
		emailContent.append(" a nombre de ");
		emailContent.append("<b>");
		emailContent.append(descriptionCuiHolder);
		emailContent.append("</b>");
		emailContent.append(" que administra la AFP al cual corresponde los valores del vencimiento.");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("Para tal efecto debe ingresar al siguiente link de acceso:");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append(urlAccusePayment);
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("Muchas Gracias.");
		emailContent.append("</p>");
		emailContent.append("</body>");
		emailContent.append("</html>");
		return emailContent.toString();
	}

	/** metodo para formatear un texto a formato html
	 * 
	 * @param text
	 * @return */
	private String getTextHtmlFormat(String text) {
		StringBuilder newText = new StringBuilder();
		String caracterSpecial;
		int letra;
		if (text != null) {
			for (int i = 0; i < text.length(); i++) {
				letra = text.charAt(i);
				caracterSpecial = "";
				if (letra == 225)
					caracterSpecial = "&aacute;";
				if (letra == 233)
					caracterSpecial = "&eacute;";
				if (letra == 237)
					caracterSpecial = "&iacute;";
				if (letra == 243)
					caracterSpecial = "&oacute;";
				if (letra == 250)
					caracterSpecial = "&uacute;";
				if (letra == 241)
					caracterSpecial = "&ntilde;";
				if (letra == 193)
					caracterSpecial = "&Aacute;";
				if (letra == 201)
					caracterSpecial = "&Eacute;";
				if (letra == 205)
					caracterSpecial = "&Iacute;";
				if (letra == 211)
					caracterSpecial = "&Oacute;";
				if (letra == 218)
					caracterSpecial = "&Uacute;";
				if (letra == 209)
					caracterSpecial = "&Ntilde;";
				if (caracterSpecial.length() > 0) {
					newText.append(caracterSpecial);
				} else {
					newText.append(text.substring(i, i + 1));
				}
			}
		} else {
			newText.append("");
		}

		return newText.toString();
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}

}
