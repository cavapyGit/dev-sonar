package com.pradera.notification.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.pradera.collection.right.to.ResultoWsNotification;
import com.pradera.collection.right.util.GenerateAccreditationGertificateUtil;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.collection.right.util.NotificationConstants;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.paymentagent.type.PaymentAgentType;
import com.pradera.model.process.BusinessProcess;
import com.edv.collection.economic.rigth.PaymentAgent;

/** @author hcoarite */
@Path("/loginServiceToAccuseReceived")
@Stateless
public class LoginServicio implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8221810245298043593L;

	@Inject
	@DepositaryDataBase
	private EntityManager em;

	@Inject
	private GenerateUrlNotification generateUrlNotification;
	
	@Inject
	NotificationServiceFacade notificationServiceFacade;
	
	@Inject
	private GenerateAccreditationGertificateUtil accreditationGertificateUtil;

	@GET
	@Path("/admission")
	@Produces(value = MediaType.TEXT_HTML)
	public String admission(@QueryParam("id") String parameter) {
		ResultoWsNotification result = generateUrlNotification.getResutWsNotification(parameter);
		int iLowAcute = 237;
		try {
			// actualizando recepcion
			for (Long l : result.getIdCollectionEconomicPks())
				generateUrlNotification.updatNotification(l, result.getGenerationDate(), 1);
			
			//verificando si el agewnte pagador es de SOMA-BCB, si lo fuera enviar el email de acuse de pago
			// al ser SOMA-BCB se asume que NO paga con LIP
			PaymentAgent paymentAgent = em.find(PaymentAgent.class, result.getIdPaymentAgent());
			if(paymentAgent.getIdPaymentAgentFk().toString().equals(PaymentAgentType.PAYMNET_AGENT_SOMA.getCode().toString())){
				List<UserAccountSession> lstUserAccount;
				UserAccountSession accountSession = new UserAccountSession();
				accountSession.setUserName(paymentAgent.getFirstName());
				accountSession.setEmail(paymentAgent.getEmail());
				accountSession.setPhoneNumber(paymentAgent.getCellPhone());
				accountSession.setFullName(paymentAgent.getFirstName());
				lstUserAccount = new ArrayList<UserAccountSession>();
				lstUserAccount.add(accountSession);
				
				// generar la url para el acuse de pago
				String urlAccusePayment = "";
				
				urlAccusePayment = generateUrlNotification.generateUrlAcknowledgmentPayment(result.getIdPaymentAgent(), result.getIdParticiapntPk(),
						result.getCurrency(), result.getAmountPayment(), result.getGlossCode(), result.getIdBank());
				
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_COLLECTION_ECONOMIC_RIGTH.getCode());
				
				String descriptionTotalAmount = "";
				try {
					String currencyDescCollection = "";
					if (result.getCurrency().equals(CurrencyType.PYG.getCode()))
						currencyDescCollection = CurrencyType.PYG.getCodeIso();
					if (result.getCurrency().equals(CurrencyType.UFV.getCode()))
						currencyDescCollection = CurrencyType.UFV.getCodeIso();
					if (result.getCurrency().equals(CurrencyType.DMV.getCode()))
						currencyDescCollection = CurrencyType.DMV.getCodeIso();
					if (result.getCurrency().equals(CurrencyType.USD.getCode()))
						currencyDescCollection = CurrencyType.USD.getCodeIso();
					
					String descriptionCurrency="";
					if(currencyDescCollection.equals(CurrencyType.PYG.getCodeIso()))
						descriptionCurrency="Bolivianos";
					if(currencyDescCollection.equals(CurrencyType.USD.getCodeIso()))
						descriptionCurrency="D&oacute;lares Americanos";
					
					// obteniendo descripcion del monto total a pagar
					// parte fraccionaria
					BigDecimal fraccion = result.getAmountPayment().remainder(BigDecimal.ONE);
					fraccion = fraccion.multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
					fraccion = fraccion.setScale(0);
					String fractionAmount = "" + fraccion;
					if (fraccion.compareTo(BigDecimal.TEN) == -1) {
						fractionAmount = "0" + fraccion;
					}
					String montoLiteral = CommonsUtilities.getLiteralNumber(result.getAmountPayment().intValue());
					// primera letra en mayuscula
					montoLiteral = montoLiteral.substring(0, 1).toUpperCase() + montoLiteral.substring(1);
					descriptionTotalAmount = currencyDescCollection + " " + accreditationGertificateUtil.getFormatCollection().format(result.getAmountPayment()) + " ("
							+ montoLiteral + " " + fractionAmount + "/100 "+descriptionCurrency+")";
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Bank bank;
				try {
					bank = em.find(Bank.class, result.getIdBank());
				} catch (Exception e) {
					e.printStackTrace();
					bank = new Bank();
					bank.setDescription("");
				}
				
				notificationServiceFacade.sendNotificationManualEmailAdjuntHtmlContent(
						"ADMIN",
						businessProcess,
						lstUserAccount,
						NotificationConstants.SUBJECT_EMAIL_ACCUSE_PAYMENT,
						getContentEmailAccusePayment(urlAccusePayment, result.getDescPaymentAgentJuridical(), descriptionTotalAmount,
								getTextHtmlFormat(bank.getDescription()), result.getAccountBankNumber(), result.getDescCuiHolder()), null, null);

				
			}
			return "<script>alert('Usted, ha sido notificado correctamente!');</script>";
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: error en la clase LoginServicio de CollectionEconomic: " + e.getMessage());
			return "<script>alert('Error del Servicio Web. Comun" + (char) (iLowAcute) + "quese con el Administrador.');</script>";
		}
	}
	
	/**
	 * metodo para formatear un texto a formato html
	 * @param text
	 * @return
	 */
	private String getTextHtmlFormat(String text) {
		StringBuilder newText=new StringBuilder();
		String caracterSpecial;
		int letra;
		if(text!=null){
			for (int i = 0; i < text.length(); i++) {
				letra = text.charAt(i);
				caracterSpecial="";
				if (letra == 225)
					caracterSpecial="&aacute;";
				if (letra == 233)
					caracterSpecial="&eacute;";
				if (letra == 237)
					caracterSpecial="&iacute;";
				if (letra == 243)
					caracterSpecial="&oacute;";
				if (letra == 250)
					caracterSpecial="&uacute;";
				if (letra == 241)
					caracterSpecial="&ntilde;";
				if (letra == 193)
					caracterSpecial="&Aacute;";
				if (letra == 201)
					caracterSpecial="&Eacute;";
				if (letra == 205)
					caracterSpecial="&Iacute;";
				if (letra == 211)
					caracterSpecial="&Oacute;";
				if (letra == 218)
					caracterSpecial="&Uacute;";
				if (letra == 209)
					caracterSpecial="&Ntilde;";
				if(caracterSpecial.length()>0){
					newText.append(caracterSpecial);
				}else{
					newText.append(text.substring(i,i+1));
				}
			}
		}else{
			newText.append("");
		}
		
		return newText.toString();
	}
	
	/** armando el contenido de correro para le url de acuse de pago
	 * 
	 * @param urlAccusePayment
	 * @return */
	private String getContentEmailAccusePayment(String urlAccusePayment, String paymentAgentJuridical, String descriptionTotalAmount, String descBank,
			String accountNumber, String descriptionCuiHolder) {
		StringBuilder emailContent = new StringBuilder();
		emailContent.append("<html>");
		emailContent.append("<head></head>");
		emailContent.append("<body>");
		emailContent.append("<br/>");
		emailContent.append("<p>Se&ntilde;ores:");
		emailContent.append("<br/>");
		emailContent.append("<b>");
		emailContent.append(paymentAgentJuridical);
		emailContent.append("</b>");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("Solicitamos su confirmaci&oacute;n de pago del Vencimiento de Valores por un monto de ");
		emailContent.append("<b>");
		emailContent.append(descriptionTotalAmount);
		emailContent.append("</b>");
		emailContent.append(" al ");
		emailContent.append("<b>");
		emailContent.append(descBank);
		emailContent.append("</b>");
		emailContent.append(", a la cuenta Nro.: ");
		emailContent.append("<b>");
		emailContent.append(accountNumber);
		emailContent.append("</b>");
		emailContent.append(" a nombre de ");
		emailContent.append("<b>");
		emailContent.append(descriptionCuiHolder);
		emailContent.append("</b>");
		emailContent.append(" que administra la AFP al cual corresponde los valores del vencimiento.");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("Para tal efecto debe ingresar al siguiente link de acceso:");
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append(urlAccusePayment);
		emailContent.append("</p>");
		emailContent.append("<p>");
		emailContent.append("Muchas Gracias.");
		emailContent.append("</p>");
		emailContent.append("</body>");
		emailContent.append("</html>");
		return emailContent.toString();
	}
}
