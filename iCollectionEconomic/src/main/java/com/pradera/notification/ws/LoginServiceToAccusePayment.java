package com.pradera.notification.ws;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.right.to.ResultUrlAccusePaymentTO;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.edv.collection.economic.rigth.DetailCollectionGloss;


/**
 * este servicio rest solo se ejecuta en caso de que el agente pagador sea SOMA-BCB
 * @author rlarico
 *
 */
@Path("/loginServiceToAccusePayment")
@Stateless
public class LoginServiceToAccusePayment implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	@DepositaryDataBase
	private EntityManager em;
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	private GenerateUrlNotification generateUrlNotification;
	
	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;
	
	@GET
    @Path("/admission")
	@Produces(value = MediaType.TEXT_HTML)
	public String admission(@QueryParam("id") String parameter) {
		ResultUrlAccusePaymentTO result = generateUrlNotification.getResutWsNotificationToAccusePayment(parameter);
		int iLowAcute = 237;
		int eLowAcute = 233;
		try {
			if (result != null) {
				System.out.println("::: datos obtenidos de acuse de pago\n" + result.toString());
				
				RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
				retirementObjetTO.setGloss(result.getGlossCode());
				retirementObjetTO.setIdParticipantPk(result.getIdParticiapntPk());
				
				List<DetailCollectionGloss> listDetailCollection = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
				DetailCollectionGloss collectionGloss = listDetailCollection.get(0);
				if(collectionGloss.getSourceDeposit()!=null)
				if(collectionGloss.getDepositStatus().equals(DepositStateType.CONFIRMED.getCode())){
					log.info("CDE :::::El deposito ya fue ejutado, glosa :"+result.getGlossCode());
					return "<script>alert('Esta URL ya fue utilizada');</script>";
				}
				if(collectionGloss.getTransferStatus()==null){
					collectionGloss.setDepositStatus(DepositStateType.REGISTERED.getCode());
					collectionGloss.setSourceDeposit(BooleanType.NO.getCode());
					collectionGloss.setAmountDeposited(collectionGloss.getAmountReceivable());
					if(result.getIdBank()!=null)
						collectionGloss.setIdBankPk(result.getIdBank());
					collectionGloss.setDepositDate(CommonsUtilities.currentDate());
					em.merge(collectionGloss);
					return "<script>alert('Pago realizado con " + (char) (eLowAcute) + "xito');</script>";
				}else
					return "<script>alert('Esta URL ya fue utilizada');</script>";
				
			} else
				return "<script>alert('Se ha producido un error. Comun" + (char) (iLowAcute) + "quese con el Administrador.');</script>";
		} catch (Exception e) {
			e.printStackTrace();
			return "<script>alert('Error al descifrar URL. Comun" + (char) (iLowAcute) + "quese con el Administrador.');</script>";
		}
	}
}
