package com.pradera.notification.ws;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.right.to.ResultUrlAccusePaymentTO;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.edv.collection.economic.rigth.DetailCollectionGloss;

@Path("/loginServiceAcknowledgmentPayment")
@Stateless
public class LoginServiceAcknowledgmentPayment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2038023653160657398L;
	
	@Inject
	@DepositaryDataBase
	private EntityManager em;
	
	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;
	@Inject
	private GenerateUrlNotification generateUrlNotification;
	
	@Inject
	private PraderaLogger log;
	
	@GET
    @Path("/admission")
    @Produces(value = MediaType.TEXT_PLAIN)
	public String admission(@QueryParam("id") String parameter) {
		//remplazando los (_) por (%) para recuperar el el cuerpo original del parametro
		parameter=parameter.replace("_", "%");
		ResultUrlAccusePaymentTO result = generateUrlNotification.getResutWsNotificationToAccusePayment(parameter);
		try {
			if (result != null) {
				log.info("CDE::: datos obtenidos de acuse de pago\n" + result.toString());
				// verificando si ya se ha realizado el pago mediante esta url
				RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
				retirementObjetTO.setGloss(result.getGlossCode());
				retirementObjetTO.setIdParticipantPk(result.getIdParticiapntPk());
				List<DetailCollectionGloss> listDetailCollection = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
				DetailCollectionGloss collectionGloss = listDetailCollection.get(0);
				if(collectionGloss.getTransferStatus().equals(DepositStateType.CONFIRMED.getCode())){
					log.info("CDE :::::El deposito ya fue ejutado, glosa :"+result.getGlossCode());
					return "URL YA FUE EJECUTADA";
				}
				if(collectionGloss.getTransferStatus().equals(DepositStateType.REGISTERED.getCode())){
					collectionGloss.setTransferStatus(DepositStateType.CONFIRMED.getCode());
					collectionGloss.setAmountDeposited(collectionGloss.getAmountReceivable());
					if(result.getIdBank()!=null)
						collectionGloss.setIdBankPk(result.getIdBank());
					collectionGloss.setDepositDate(CommonsUtilities.currentDate());
					em.merge(collectionGloss);
					log.info("CDE ::::: Se actualizo el deposito mediante acuse de pago de la glosa :"+result.getGlossCode());
				}
				return MediaType.TEXT_PLAIN;
			}
		} catch (Exception e) {
			e.printStackTrace();;
		}
		return null;
	}
}
