package com.pradera.notification.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.component.HolderAccountBalancePK;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;

public class SecuritiesNotifiedCollectionTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7737685017062302581L;
	
	private List<CollectionEconomicRigth> listCollectionEconomics;
	
	private List<HolderAccountBalancePK> securitiesLockedBalance;
	
	public SecuritiesNotifiedCollectionTO() {
		// TODO Auto-generated constructor stub
	}

	public List<CollectionEconomicRigth> getListCollectionEconomics() {
		return listCollectionEconomics;
	}

	public void setListCollectionEconomics(
			List<CollectionEconomicRigth> listCollectionEconomics) {
		this.listCollectionEconomics = listCollectionEconomics;
	}

	public List<HolderAccountBalancePK> getSecuritiesLockedBalance() {
		return securitiesLockedBalance;
	}

	public void setSecuritiesLockedBalance(
			List<HolderAccountBalancePK> securitiesLockedBalance) {
		this.securitiesLockedBalance = securitiesLockedBalance;
	}
}
