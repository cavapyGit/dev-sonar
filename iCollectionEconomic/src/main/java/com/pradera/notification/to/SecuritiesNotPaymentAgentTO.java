package com.pradera.notification.to;

import java.io.Serializable;

public class SecuritiesNotPaymentAgentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8801130498172171713L;
	
	private String mNemonicPaymentAgent;
	
	private String idSecurityCodePk;

	public String getmNemonicPaymentAgent() {
		return mNemonicPaymentAgent;
	}

	public void setmNemonicPaymentAgent(String mNemonicPaymentAgent) {
		this.mNemonicPaymentAgent = mNemonicPaymentAgent;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
}
