package com.pradera.notification.to;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SecurityDetailObjectTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5542269820019539229L;
	private Long idCollectionEconomicPk;
	private Long idGeneralPurposePk;
	private String idSecurityCodePk;
	private Long idHolderAccountPk;
	private Integer numberCoupon;
	private BigDecimal availableBalance;
	private Date dateExpired;
	private Date datePayment; 
	private Integer checkOther;
	private Integer originCurrency;
	private Integer collectionCurrency;
	private BigDecimal anotherAmountReceivable;
	private BigDecimal originAmount;
	private BigDecimal collectionAmount;
	private Integer certificateNumber;
	private List<File> fileAdjunts;
	private String dateExpiredText;
	private String datePaymentText;
	private String originCurrencyText;
	private String collectionCurrencyText;
	private String idSecurityCodePkOriginal;
	private Integer instrumentType;
	
	public SecurityDetailObjectTO() {
		fileAdjunts = new ArrayList<>();
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public Integer getNumberCoupon() {
		return numberCoupon;
	}
	public void setNumberCoupon(Integer numberCoupon) {
		this.numberCoupon = numberCoupon;
	}
	public String getOriginCurrencyText() {
		return originCurrencyText;
	}
	public void setOriginCurrencyText(String originCurrencyText) {
		this.originCurrencyText = originCurrencyText;
	}
	public String getCollectionCurrencyText() {
		return collectionCurrencyText;
	}
	public void setCollectionCurrencyText(String collectionCurrencyText) {
		this.collectionCurrencyText = collectionCurrencyText;
	}
	public Date getDateExpired() {
		return dateExpired;
	}
	public void setDateExpired(Date dateExpired) {
		this.dateExpired = dateExpired;
	}
	public Date getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(Date datePayment) {
		this.datePayment = datePayment;
	}
	public String getDateExpiredText() {
		return dateExpiredText;
	}
	public void setDateExpiredText(String dateExpiredText) {
		this.dateExpiredText = dateExpiredText;
	}
	public String getDatePaymentText() {
		return datePaymentText;
	}
	public void setDatePaymentText(String datePaymentText) {
		this.datePaymentText = datePaymentText;
	}
	public Integer getOriginCurrency() {
		return originCurrency;
	}
	public void setOriginCurrency(Integer originCurrency) {
		this.originCurrency = originCurrency;
	}
	public Integer getCollectionCurrency() {
		return collectionCurrency;
	}
	public void setCollectionCurrency(Integer collectionCurrency) {
		this.collectionCurrency = collectionCurrency;
	}
	
	public BigDecimal getAnotherAmountReceivable() {
		return anotherAmountReceivable;
	}
	public void setAnotherAmountReceivable(BigDecimal anotherAmountReceivable) {
		this.anotherAmountReceivable = anotherAmountReceivable;
	}
	public Integer getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(Integer certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public Long getIdCollectionEconomicPk() {
		return idCollectionEconomicPk;
	}
	public void setIdCollectionEconomicPk(Long idCollectionEconomicPk) {
		this.idCollectionEconomicPk = idCollectionEconomicPk;
	}
	public Long getIdGeneralPurposePk() {
		return idGeneralPurposePk;
	}
	public void setIdGeneralPurposePk(Long idGeneralPurposePk) {
		this.idGeneralPurposePk = idGeneralPurposePk;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public List<File> getFileAdjunts() {
		return fileAdjunts;
	}
	public void setFileAdjunts(List<File> fileAdjunts) {
		this.fileAdjunts = fileAdjunts;
	}
	public Integer getCheckOther() {
		return checkOther;
	}
	public void setCheckOther(Integer checkOther) {
		this.checkOther = checkOther;
	}
	public BigDecimal getOriginAmount() {
		return originAmount;
	}
	public String getIdSecurityCodePkOriginal() {
		return idSecurityCodePkOriginal;
	}
	public void setIdSecurityCodePkOriginal(String idSecurityCodePkOriginal) {
		this.idSecurityCodePkOriginal = idSecurityCodePkOriginal;
	}
	public void setOriginAmount(BigDecimal originAmount) {
		this.originAmount = originAmount;
	}
	public BigDecimal getCollectionAmount() {
		return collectionAmount;
	}
	public void setCollectionAmount(BigDecimal collectionAmount) {
		this.collectionAmount = collectionAmount;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
}
