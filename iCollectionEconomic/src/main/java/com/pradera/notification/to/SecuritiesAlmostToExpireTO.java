package com.pradera.notification.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.PaymentAgent;

public class SecuritiesAlmostToExpireTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4684194826041839863L;
	private Long idAccreditationOperationPk;
	private Long idParticipantPk;
	private String idIssuerPk;
	private Integer securityClass;
	private Integer instrumentType;
	
	private BigDecimal totalBalance = BigDecimal.ZERO;
	private BigDecimal availableBalance = BigDecimal.ZERO;
	private BigDecimal transitBalance = BigDecimal.ZERO;
	private BigDecimal pawnBalance = BigDecimal.ZERO;
	private BigDecimal banBalance = BigDecimal.ZERO;
	private BigDecimal otherBlockBalance = BigDecimal.ZERO;
	private BigDecimal reserveBalance = BigDecimal.ZERO;
	private BigDecimal oppositionBalance = BigDecimal.ZERO;
	private BigDecimal accreditationBalance = BigDecimal.ZERO;
	private BigDecimal purchaseBalance = BigDecimal.ZERO;
	private BigDecimal saleBalance = BigDecimal.ZERO;
	private BigDecimal reportingBalance = BigDecimal.ZERO;
	private BigDecimal reportedBalance = BigDecimal.ZERO;
	private BigDecimal marginBalance = BigDecimal.ZERO;
	private BigDecimal borrowerBalance = BigDecimal.ZERO;
	private BigDecimal lenderBalance = BigDecimal.ZERO;
	private BigDecimal loanableBalance = BigDecimal.ZERO;
	
	private CollectionEconomicRigth collectionEconomicRigth;
	/**Lista de resumen de todos los saldos*/
	private List<String> listBalanceType;
	/**Lista de contactos agentes pagadores a los cuales se le hara la notificacion*/
	private HashMap<Long,PaymentAgent> mapContactPaymentAgent;

	public SecuritiesAlmostToExpireTO() {
		// TODO Auto-generated constructor stub
	}
	
	

	public SecuritiesAlmostToExpireTO(Long idCorporativeOperationPk,
			Long idParticipantPk, Date deliveryDate, String idSecurityCodePk,
			Date experitationDate, Date paymentDate, BigDecimal couponAmount,
			Integer couponNumber, Long idHolderAccountPk,
			BigDecimal totalBalance, BigDecimal availableBalance,
			BigDecimal transitBalance, BigDecimal pawnBalance,
			BigDecimal banBalance, BigDecimal otherBlockBalance,
			BigDecimal reserveBalance, BigDecimal oppositionBalance,
			BigDecimal accreditationBalance, BigDecimal purchaseBalance,
			BigDecimal saleBalance, BigDecimal reportingBalance,
			BigDecimal reportedBalance, BigDecimal marginBalance,
			BigDecimal borrowerBalance, BigDecimal lenderBalance,
			BigDecimal loanableBalance) {
		super();
		this.idParticipantPk = idParticipantPk;
		this.totalBalance = totalBalance;
		this.availableBalance = availableBalance;
		this.transitBalance = transitBalance;
		this.pawnBalance = pawnBalance;
		this.banBalance = banBalance;
		this.otherBlockBalance = otherBlockBalance;
		this.reserveBalance = reserveBalance;
		this.oppositionBalance = oppositionBalance;
		this.accreditationBalance = accreditationBalance;
		this.purchaseBalance = purchaseBalance;
		this.saleBalance = saleBalance;
		this.reportingBalance = reportingBalance;
		this.reportedBalance = reportedBalance;
		this.marginBalance = marginBalance;
		this.borrowerBalance = borrowerBalance;
		this.lenderBalance = lenderBalance;
		this.loanableBalance = loanableBalance;
	}



	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	
	/*
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
*/
	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	public BigDecimal getAccreditationBalance() {
		return accreditationBalance;
	}

	public void setAccreditationBalance(BigDecimal accreditationBalance) {
		this.accreditationBalance = accreditationBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}


	public HashMap<Long, PaymentAgent> getMapContactPaymentAgent() {
		return mapContactPaymentAgent;
	}



	public void setMapContactPaymentAgent(
			HashMap<Long, PaymentAgent> mapContactPaymentAgent) {
		this.mapContactPaymentAgent = mapContactPaymentAgent;
	}


	public List<String> getListBalanceType() {
		return listBalanceType;
	}

	public void setListBalanceType(List<String> listBalanceType) {
		this.listBalanceType = listBalanceType;
	}


	public Long getIdAccreditationOperationPk() {
		return idAccreditationOperationPk;
	}

	public void setIdAccreditationOperationPk(Long idAccreditationOperationPk) {
		this.idAccreditationOperationPk = idAccreditationOperationPk;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}



	public Integer getSecurityClass() {
		return securityClass;
	}



	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}



	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public CollectionEconomicRigth getCollectionEconomicRigth() {
		return collectionEconomicRigth;
	}

	public void setCollectionEconomicRigth(
			CollectionEconomicRigth collectionEconomicRigth) {
		this.collectionEconomicRigth = collectionEconomicRigth;
	}

}