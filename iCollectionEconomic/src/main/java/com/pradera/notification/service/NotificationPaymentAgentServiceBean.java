package com.pradera.notification.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.exception.ConstraintViolationException;

import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.DetailCollectionGlossTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.funds.BeginEndDay;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.notification.to.SecuritiesNotPaymentAgentTO;
import com.pradera.notification.to.SecuritiesNotifiedCollectionTO;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.PaymentAgent;

public class NotificationPaymentAgentServiceBean extends CrudDaoServiceBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	private CollectionRigthServiceBean collectionRigthServiceBean;
	
	public Holder getHolderFromHolderAccount(Long idHolderAccountPk){
		List<HolderAccountDetail> lst=new ArrayList<>();
		Holder holderResult=null;
		StringBuilder sd=new StringBuilder();
		sd.append(" select had");
		sd.append(" from HolderAccountDetail had");
		sd.append(" inner join fetch had.holder h");
		sd.append(" where 0 = 0");
		sd.append(" and had.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sd.append(" and rownum = 1");
		try {
			Query q=em.createQuery(sd.toString());
			q.setParameter("idHolderAccountPk", idHolderAccountPk);
			lst=q.getResultList();
			if(!lst.isEmpty()){
				holderResult=lst.get(0).getHolder();
			}
		} catch (Exception e) {
			e.printStackTrace();
			holderResult=new Holder();
			holderResult.setIdHolderPk(0L);
			holderResult.setDescriptionHolder("");
		}
		return holderResult;
	}
	
	/**
	 * metodo para hallar un banco
	 * @param idBank
	 * @return
	 */
	public Bank findBank(long idBank){
		Bank bank=em.find(Bank.class,idBank);
		return bank;
	}
	
	public CollectionEconomicRigth getOneCollectionEconomicRigth(Long idCollectionEcoRightPk){
		CollectionEconomicRigth collectionEconomicRigth=null;
		try {
			collectionEconomicRigth = em.find(CollectionEconomicRigth.class, idCollectionEcoRightPk);
		} catch (Exception e) {
			System.out.println("::: Error al recuperar CollectionEconomicRight");
		}
		return collectionEconomicRigth;
	}

	/** method to find a one {@link AccreditationOperation}
	 * 
	 * @param idAccrOperation
	 * @return */
	public AccreditationOperation findAccreditationOperation(long idAccrOperation) {
		AccreditationOperation accreditationOperation = em.find(AccreditationOperation.class, idAccrOperation);
		return accreditationOperation;
	}

	/** method to find a one {@link Participant}
	 * 
	 * @param idParticipantPk
	 * @return */
	public Participant findParticipant(long idParticipantPk) {
		Participant pa = em.find(Participant.class, idParticipantPk);
		return pa;
	}
	
	/**Busqueda de clase de valor registradas
	 * @param idParticipantPk id de participante de filtro
	 * @param paymentDate fecha de cobro, tiene que ser un dia habial siguiente, el batchero se corre antes de un dia
	 * @return retornamos de todas las clases de valor registradas en los cobros
	 * **/
	@SuppressWarnings("unchecked")
	public List<BigDecimal> getListSecurityClassRegistreds(Date paymentDate,Date nextBusinessDay){
		StringBuilder builder= new StringBuilder();
		
		builder.append(" SELECT DISTINCT(SE.SECURITY_CLASS) FROM COLLECTION_ECONOMIC_RIGHT CER, SECURITY SE ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND SE.ID_SECURITY_CODE_PK = CER.ID_SECURITY_CODE_FK ");
		builder.append(" AND SE.INSTRUMENT_TYPE  IN( "+InstrumentType.FIXED_INCOME.getCode()+","+InstrumentType.VARIABLE_INCOME.getCode()+")");
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN :paymentDate AND :nextBusinessDay ");

		Query query = em.createNativeQuery(builder.toString());
		
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> getListIdHoldersByCollection(Date paymentDate){
		StringBuilder builder= new StringBuilder();
		
		builder.append(" select distinct(cer.idHolderPk) from CollectionEconomicRigth cer");
		builder.append(" where 1 = 1 ");
		builder.append(" and cer.glosaNotClassified is null ");
		builder.append(" and trunc(cer.paymentDate) = :paymentDate ");

		Query query = em.createQuery(builder.toString());
		
		query.setParameter("paymentDate", paymentDate);
		
		List<Long> listIdHolders = query.getResultList();
		return listIdHolders;
	}
	
	
	/**
	 * Buscamos todos los mnemonics registrados
	 *  en la solicitud de cobros de derechos economicos**/
	
	@SuppressWarnings("unchecked")
	public List<String> getListMnemonicPaymentAgents(Date paymentDate){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT DISTINCT(SE.PAYMENT_AGENT) FROM COLLECTION_ECONOMIC_RIGHT CER ");
		builder.append(" INNER JOIN SECURITY SE ON CER.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		builder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON RCE.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND RCE.STATE = "+StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);
		builder.append(" AND SE.SECURITY_CLASS <> "+SecurityClassType.DPF.getCode());//diferente de dpf
		builder.append(" AND SE.PAYMENT_AGENT IS NOT NULL ");
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) = :paymentDate ");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("paymentDate", paymentDate);
		
		List<String> mNemonics = query.getResultList();
		
		/**
		 * Para DPF*/
		builder = new StringBuilder();
		builder.append(" SELECT CASE WHEN SE.PAYMENT_AGENT ");
	    builder.append(" IS NULL THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK = SE.ID_ISSUER_FK)  ");
	    builder.append(" ELSE SE.PAYMENT_AGENT ");
	    builder.append(" END ");
	    builder.append(" FROM COLLECTION_ECONOMIC_RIGHT CER ");
		builder.append(" INNER JOIN SECURITY SE ON CER.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		builder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON RCE.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND RCE.STATE = "+StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);
		builder.append(" AND SE.SECURITY_CLASS = "+SecurityClassType.DPF.getCode());//DPF
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) = :paymentDate ");
		
		Query query1 = em.createNativeQuery(builder.toString());
		query1.setParameter("paymentDate", paymentDate);
		
		List<String> mNemonicsDpf = query1.getResultList();
		mNemonics.addAll(mNemonicsDpf);
		
		HashMap<String, String> listFinal = new HashMap<>();
		for (String string : mNemonics)
			listFinal.put(string, string);
		mNemonics = new ArrayList<>();
		mNemonics.addAll(listFinal.values());
		return mNemonics;
	}
	/**
	 * Obtenemos la lista de agentes pagadores dentro de un rango de fechas de pago
	 * */
	@SuppressWarnings("unchecked")
	private List<SecuritiesNotPaymentAgentTO> getListMnemonicPaymentAgents(Date paymentDate,Date nextBusinessDay){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT DISTINCT(SE.PAYMENT_AGENT),CER.ID_SECURITY_CODE_FK FROM COLLECTION_ECONOMIC_RIGHT CER ");
		builder.append(" INNER JOIN SECURITY SE ON CER.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		builder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON  RCE.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		builder.append(" WHERE 1 = 1 ");	
		builder.append(" AND RCE.STATE =  "+StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);	
		builder.append(" AND SE.SECURITY_CLASS <> "+SecurityClassType.DPF.getCode()); //diferente de dpf
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN :paymentDate AND :nextBusinessDay  ");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);
		
		List<Object[]> listResult = query.getResultList();
		List<SecuritiesNotPaymentAgentTO> list = new ArrayList<>();
		SecuritiesNotPaymentAgentTO paymentAgentTO = null;
		for (Object[] objects : listResult) {
			paymentAgentTO = new SecuritiesNotPaymentAgentTO();
			paymentAgentTO.setmNemonicPaymentAgent(objects[0]==null?null:objects[0].toString());
			paymentAgentTO.setIdSecurityCodePk(objects[1].toString());
			list.add(paymentAgentTO);
		}
		/**
		 * Para DPF*/
		builder = new StringBuilder();
		builder.append(" SELECT CASE WHEN SE.PAYMENT_AGENT ");
		builder.append(" IS NULL THEN (SELECT ISS.MNEMONIC FROM ISSUER ISS WHERE ISS.ID_ISSUER_PK = SE.ID_ISSUER_FK) ");
	    builder.append(" ELSE SE.PAYMENT_AGENT ");
	    builder.append(" END,SE.ID_SECURITY_CODE_PK ");
		builder.append(" FROM COLLECTION_ECONOMIC_RIGHT CER ");
		builder.append(" INNER JOIN SECURITY SE ON CER.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		builder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON  RCE.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		builder.append(" WHERE 1 = 1 ");	
		builder.append(" AND RCE.STATE = "+StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);//CONFIRMADO 2387
		builder.append(" AND SE.SECURITY_CLASS = "+SecurityClassType.DPF.getCode());//DPF
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN :paymentDate AND :nextBusinessDay  ");
		
		Query query1 = em.createNativeQuery(builder.toString());
		query1.setParameter("paymentDate", paymentDate);
		query1.setParameter("nextBusinessDay", nextBusinessDay);
		
		listResult = null;
		listResult = query1.getResultList();
		List<SecuritiesNotPaymentAgentTO> list1 = new ArrayList<SecuritiesNotPaymentAgentTO>();
		for (Object[] objects : listResult) {
			paymentAgentTO = new SecuritiesNotPaymentAgentTO();
			paymentAgentTO.setmNemonicPaymentAgent(objects[0]==null?null:objects[0].toString());
			paymentAgentTO.setIdSecurityCodePk(objects[1].toString());
			list1.add(paymentAgentTO);
		}
		list.addAll(list1);
		return list;
	}
	
	/**
	 * Retornamos los valores que no tienen un agente pagador correcto asigando
	 * */
	public List<SecuritiesNotPaymentAgentTO> searchListSecuritiesNotPayingAgent(Date paymentDate,	Date nextBusinessDay){
		List<SecuritiesNotPaymentAgentTO> list =  getListMnemonicPaymentAgents(paymentDate, nextBusinessDay);
		List<SecuritiesNotPaymentAgentTO> listFinal = new ArrayList<>();
		for (SecuritiesNotPaymentAgentTO securitiesNotPaymentAgentTO : list) 
			if(searhPaymentAgentByMnemonic(securitiesNotPaymentAgentTO.getmNemonicPaymentAgent())==null)
				listFinal.add(securitiesNotPaymentAgentTO);
		return listFinal;
	}
	
	/**
	 * Busqueda de el id de agente pagador segun mnemonico
	 * */
	public PaymentAgent searhPaymentAgentByMnemonic(String mNemonicPaymentAgent){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select new com.edv.collection.economic.rigth.PaymentAgent(pa.idPaymentAgentPk) ");
		queryBuilder.append(" from PaymentAgent pa ");
		queryBuilder.append(" where  1 = 1 ");
		queryBuilder.append(" and pa.idPaymentAgentFk is null ");
		queryBuilder.append(" and pa.mnemonic = :mNemonicPaymentAgent");
		
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("mNemonicPaymentAgent", mNemonicPaymentAgent);
		
		PaymentAgent paymentAgent = null;
		try {
			paymentAgent = (PaymentAgent) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		return paymentAgent;
	}
	
	
	/**
	 * Buscamo la lsita de emisores que tienen registrados vencimientos 
	 * Se  buscan valores en cobro de derechos economicos en RF y RV
	 * @param */
	@SuppressWarnings("unchecked")
	public List<String> getListIdIssuersPks(Date paymentDate,Date nextBusinessDay){
		
		StringBuilder builder= new StringBuilder();
		builder.append(" SELECT DISTINCT(SE.ID_ISSUER_FK) FROM COLLECTION_ECONOMIC_RIGHT CER, SECURITY SE, ISSUER ISS ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND SE.ID_SECURITY_CODE_PK = CER.ID_SECURITY_CODE_FK ");
		builder.append(" AND SE.INSTRUMENT_TYPE  IN( "+InstrumentType.FIXED_INCOME.getCode()+","+InstrumentType.VARIABLE_INCOME.getCode()+")");
		builder.append(" AND ISS.ID_ISSUER_PK = SE.ID_ISSUER_FK ");
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN :paymentDate AND :nextBusinessDay ");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);
		return query.getResultList();
	}

	/** 
	 * Buscamos los datos de los valores que tenga fecha de pago
	 * Y que estos deeben de estar registrados en cartera
	 * @return
	 * @throws Exception */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SecuritiesNotifiedCollectionTO getSecuritiesToNotify(Long idParticipantPk,String idIssuerPk,String mNemonicPaymentAgent, Integer currencyType,Long idHolderPk,Date paymentDate,Date nextBusinessDay,Integer securityClass,Long idBankPk,String depAccountNumber)  {
		try {
			//Buscamos los valores registrados para cobro de derechos economicos
			List<CollectionEconomicRigth> list= collectionRigthServiceBean.getListSecuritiesNotify(paymentDate, nextBusinessDay, idParticipantPk, idIssuerPk,mNemonicPaymentAgent, currencyType,idHolderPk,securityClass,StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC,StatesConstants.REGISTERED_COLLECTION_ECONOMIC,idBankPk,depAccountNumber);
			if(list!=null&&list.size()==0){
				log.info("CDDE ::: No existen vencimientos registrados en Cobro de Derechos Economicos de Part:"+idParticipantPk+", Imisor: "+idIssuerPk+", Clase:"+securityClass);
				return null;
			}
			
			//return list;
			/*Verificamos que los datos esten en cartera*/
			HolderAccountBalance holderAccountBalance = null;
			HolderAccountBalancePK holderAccountBalancePK =  null;
			
			List<CollectionEconomicRigth> definitiveList = new ArrayList<>();
			List<HolderAccountBalancePK> securitiesLockedBalance = new ArrayList<>();
			
			for (CollectionEconomicRigth collectionEconomicRigth : list) {
				
				holderAccountBalancePK = new HolderAccountBalancePK();
				
				holderAccountBalancePK.setIdParticipantPk(idParticipantPk);
				holderAccountBalancePK.setIdSecurityCodePk(collectionEconomicRigth.getIdSecurityCodeFk());
				holderAccountBalancePK.setIdHolderAccountPk(collectionEconomicRigth.getIdHolderAccountPk());
				holderAccountBalance = collectionRigthServiceBean.find(HolderAccountBalance.class, holderAccountBalancePK);
				if(holderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO)==0){
					log.info("CDE:::: El valor :"+collectionEconomicRigth.getIdSecurityCodeFk()+" ya no tiene saldo en cartera.");
					securitiesLockedBalance.add(holderAccountBalancePK);
				}else{
					definitiveList.add(collectionEconomicRigth);
				}
			}
			SecuritiesNotifiedCollectionTO collectionTO = new SecuritiesNotifiedCollectionTO();
			collectionTO.setListCollectionEconomics(definitiveList);
			collectionTO.setSecuritiesLockedBalance(securitiesLockedBalance);
			return collectionTO;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * obtener un holder a aprtir de un paymentagent
	 * @param idPaymentAgent
	 * @return
	 */
	public PaymentAgent getPaymentAgenntById(Long idPaymentAgent){
		try {
			PaymentAgent pa=em.find(PaymentAgent.class, idPaymentAgent);
			return pa;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/** method for checking if the currency is registered or Confirmed
	 * 
	 * @param idCurrency
	 * @return
	 * @throws Exception */
	@SuppressWarnings("unchecked")
	public boolean checkCurrencyIsRegisterOrConfirm(int idCurrency, Date dateQuery) throws Exception {

		boolean isRegisterOrConfirm = false;

		List<Long> lst = new ArrayList<Long>();

		StringBuilder queryBuilder = new StringBuilder();
		
		queryBuilder.append(" select Der.Id_Daily_Exchange_Pk from Daily_Exchange_Rates  der ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and trunc(der.DATE_RATE) = :dateQuery ");
		queryBuilder.append(" and der.ID_CURRENCY = :idCurrency ");
		
		Query query = em.createNativeQuery(queryBuilder.toString());
		
		query.setParameter("dateQuery", dateQuery);
		query.setParameter("idCurrency", idCurrency);

		lst = query.getResultList();

		if (lst.size() > 0) {
			isRegisterOrConfirm = true;
		}

		return isRegisterOrConfirm;
	}

	/**
	 * Verificando si existe registros en moneda 
	 * En los registros de cobro de derechos economicos*/
	public boolean checkRecordsForCurrency(Integer currency,Date paymentDate,Date nextBusinessDay){
		
		StringBuilder  builder = new StringBuilder();
		builder.append(" select distinct(cer.idCollectionEconomicPk) from CollectionEconomicRigth cer ");
		builder.append(" where 1 = 1 ");
		builder.append(" and cer.currency = :currency ");
		builder.append(" and cer.glosaNotClassified is null ");
		builder.append(" and cer.checkOther = 0 ");
		builder.append(" and trunc(cer.paymentDate) between :paymentDate and :nextBusinessDay ");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("currency", currency);
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);
		
		@SuppressWarnings("unchecked")
		List<Long> listCurrency = query.getResultList();
		
		builder = new StringBuilder();
		builder.append(" select distinct(cer.idCollectionEconomicPk) from CollectionEconomicRigth cer ");
		builder.append(" where 1 = 1 ");
		builder.append(" and cer.anotherCurency = :anotherCurrency ");
		builder.append(" and cer.glosaNotClassified is null ");
		builder.append(" and cer.checkOther = 1 ");
		builder.append(" and trunc(cer.paymentDate) between :paymentDate and :nextBusinessDay ");
		
		Query query2 = em.createQuery(builder.toString());
		query2.setParameter("anotherCurrency", currency);
		query2.setParameter("paymentDate", paymentDate);
		query2.setParameter("nextBusinessDay", nextBusinessDay);
		
		@SuppressWarnings("unchecked")
		List<Long> listCurrencyOther = query2.getResultList();
		
		listCurrency.addAll(listCurrencyOther);
		if(listCurrency!=null&&listCurrency.size()>0)
			return true;
		else
			return false;
	}
	
	public Long searchAccreditationOperation(Long idParticipantPk,Long idHolderAccountPk, String idSecurityCodePk){
		/***/
		StringBuilder builder = new StringBuilder();
		builder.append(" select ID_ACCREDITATION_OPERATION_FK from ACCREDITATION_DETAIL ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND ID_SECURITY_CODE_FK = :idSecurityCodePk ");
		builder.append(" AND ID_PARTICIPANT_FK = :idParticipantPK ");
		builder.append(" AND ID_HOLDER_ACCOUNT_FK = :idHolderAccountPk ");
		builder.append(" ORDER BY ID_ACCREDITATION_DETAIL_PK DESC ");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		try {
			@SuppressWarnings("unchecked")
			List<Long> list = query.getResultList();
			
			Long long1 = list.get(0);
			return long1;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	/** method to checking if a issuer is TGN
	 * 
	 * @param idIssuer
	 * @return */
	public boolean checkingIfIssuerIsTgn(String idIssuer) {
		boolean isTgn = false;
		Issuer issuer = em.find(Issuer.class, idIssuer);
		if (issuer.getMnemonic().equalsIgnoreCase("TGN")) {
			isTgn = true;
		}
		return isTgn;
	}
	/**
	 * Inicio de dia
	 * @return Dia iniciado
	 * */
	@SuppressWarnings("unchecked")
	public BeginEndDay getBeginEndDay() throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT bed");
			strQuery.append("	FROM BeginEndDay bed");
			strQuery.append("	WHERE TRUNC(bed.processDay) = :day");
			strQuery.append("	AND bed.indSituation = :yes");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("day", CommonsUtilities.currentDate());
			query.setParameter("yes", BooleanType.YES.getCode());
			
			List<BeginEndDay> beginEndDay = null;
			try {
				beginEndDay = query.getResultList();
				return (BeginEndDay) beginEndDay.get(0);
			} catch (Exception e) {
				return null;
			}
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**Guardamos los datos de monto, glosa, participante*/
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void registerNotificationDetail(DetailCollectionGlossTO detailCollectionGlossTO,ProcessLogger processLogger){
		try {
			collectionRigthServiceBean.registerNotificationDetail(detailCollectionGlossTO,processLogger);
		}
		catch (ConstraintViolationException e) {
			e.printStackTrace();
			log.info("CDE :::: Ya existe el registro para la glosa : "+detailCollectionGlossTO.getGlosaNotClassified());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}/**
	 * Busqueda de los depositos
	 * @see FundsOperation
	 * @param searchFilterMonitorTO Filtros de busqueda
	 * @return {@link List} */
	public List<DetailCollectionGloss> getDepositsOfCollections(Long idParticipantPk,String glosaNotClassified,Integer currency){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select dcg from DetailCollectionGloss dcg ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and dcg.idParticipantPk = :idParticipantPk ");
		queryBuilder.append(" and dcg.glosaNotClassified = :glosaNotClassified ");
		queryBuilder.append(" and dcg.currency = :currency ");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("glosaNotClassified", glosaNotClassified);
		query.setParameter("currency", currency);
		
		@SuppressWarnings("unchecked")
		List<DetailCollectionGloss> list = query.getResultList();
		return list;
	}
	
	/**
	 * Buscamos loa id de los bancos registrados en cobros
	 * */
	@SuppressWarnings("unchecked")
	public List<Long> getIdBanksRegistereds(Date paymentDate){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT DISTINCT(CER.ID_BANK_FK) FROM COLLECTION_ECONOMIC_RIGHT CER ");
		builder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON  RCE.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		builder.append(" WHERE 1 = 1 ");	
		builder.append(" AND RCE.STATE = "+StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);	//2387 
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) = :paymentDate ");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("paymentDate", paymentDate);
		
		List<BigDecimal> listResult = query.getResultList();
		List<Long> listFinal = new ArrayList<>();
		for (BigDecimal bigDecimal : listResult) {
			listFinal.add(bigDecimal.longValue());
		}
		return listFinal;
	}
	
	/**Buscamos todas las cuentas registradas 
	 * para la agrupacion por numero de cuenta de desposito*/
	@SuppressWarnings("unchecked")
	public List<String> getAccountNumberDeposits(Date paymentDate){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT DISTINCT(CER.DEP_ACCOUNT_NUMBER) FROM COLLECTION_ECONOMIC_RIGHT CER ");
		builder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCE ON  RCE.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		builder.append(" WHERE 1 = 1 ");	
		builder.append(" AND RCE.STATE = "+StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);	//2387 
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND TRUNC(CER.PAYMENT_DATE) = :paymentDate ");
		
		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("paymentDate", paymentDate);
		
		List<String> listResult = query.getResultList();
		return listResult;
	}
}
