package com.pradera.notification.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.notification.to.SecurityDetailObjectTO;

public class BuildReportGlosaServiceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;

	private static JasperReport report;
	private static JasperPrint reportFilled;

	// cosntructor
	public BuildReportGlosaServiceBean() {
	}

	public byte[] buildReportFromHtml(Map<String, Object> parameters, List<SecurityDetailObjectTO> lstSecurityDetailObjectTO) {
		byte[] reportContent = null;
		try {

			//TODO ========================comentar begin
//			lstSecurityDetailObjectTO = new ArrayList<SecurityDetailObjectTO>();
//			SecurityDetailObjectTO asd;
//			for(int i = 0 ; i<120; i++){
//				asd = new SecurityDetailObjectTO();
//				asd.setAvailableBalance(BigDecimal.ZERO);
//				asd.setIdSecurityCodePk("cod sec");
//				asd.setNumberCoupon(1);
//				asd.setAvailableBalance(BigDecimal.ZERO);
//				asd.setDateExpiredText("fec ven");
//				asd.setDatePaymentText("fec cobro");
//				lstSecurityDetailObjectTO.add(asd);
//			}
			//TODO ========================comentar end

			// seteando la ruta del jasper para crear Reporte
			report = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream("/jasperReport/reportGlosa.jasper"));
			// jabeanDataSources para el reporte
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lstSecurityDetailObjectTO);
			reportFilled = JasperFillManager.fillReport(report, parameters, ds);
			// obteniendo el reporte en bytes
			reportContent = JasperExportManager.exportReportToPdf(reportFilled);
			//System.out.println("::: Reporte generado con exito");
		} catch (JRException e) {
			e.printStackTrace();
			//System.out.println("::: Error de tipo: JRException: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("::: Error de tipo: Exception: " + e.getMessage());
		}
		return reportContent;
	}

}
