package com.pradera.notification.service;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.pradera.collection.right.to.ResultUrlAccusePaymentTO;
import com.pradera.collection.right.to.ResultoWsNotification;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;

@Stateless
public class ElectronicCatWebServiceBean extends CrudDaoServiceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	PraderaLogger log;

	@Inject
	private GenerateUrlNotification generateUrlNotification;

	 //@Resource(name = "java:jboss/datasources/iDepositaryDS")
	 //javax.sql.DataSource dataSourceBBV;

//	@Resource(name = "java:jboss/datasources/catElectronicoDS")
//	javax.sql.DataSource dataSourceBBV;

	public List<ResultUrlAccusePaymentTO> getInfoPaymentCollectionEconomic() throws ServiceException {
		List<ResultUrlAccusePaymentTO> lstResult = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs;

		// condicion es que se listen las glosas SIN PAGO(IND_PAYMENT = 0) pero con RECIBO CONFIRMADO(IND_RECEIVED = 1)
		// y que tengan URL de pago (PARAMETER_URL_PAYMENT is not null)
		StringBuilder sql = new StringBuilder();
		sql.append(" select gc.ID_GLOSS_PK, gc.PARAMETER_URL_RECEIVED_PK, gc.PARAMETER_URL_PAYMENT ");
		sql.append(" from CAEDV.GLOSS_COLLECTION_ECONOMIC@cat.world gc");
		sql.append(" where gc.IND_PAYMENT = 0");
		sql.append(" and gc.IND_RECEIVED = 1");
		sql.append(" and gc.PARAMETER_URL_PAYMENT is not null");

		// query para actualizar los indicadores de confirmacion de pago de glosa (cobro de derechos)
		StringBuilder sqlUpdateIndReceived = new StringBuilder();
		sqlUpdateIndReceived = new StringBuilder();
		sqlUpdateIndReceived.append(" update CAEDV.GLOSS_COLLECTION_ECONOMIC@cat.world gc set");
		// 1: pago confirmado
		sqlUpdateIndReceived.append(" gc.IND_PAYMENT = 1");
		sqlUpdateIndReceived.append(" ,gc.DATE_PAYMNET = ?");
		sqlUpdateIndReceived.append(" where gc.ID_GLOSS_PK = ?");
		sqlUpdateIndReceived.append(" and gc.PARAMETER_URL_RECEIVED_PK = ?");
		
		try {
			ResultUrlAccusePaymentTO resultDecryptParamUrl;

			connection =  null; //dataSourceBBV.getConnection();
			preparedStatement = connection.prepareStatement(sql.toString());
			rs = preparedStatement.executeQuery();
			while (rs.next()) {
				resultDecryptParamUrl = generateUrlNotification.getResutWsNotificationToAccusePayment(rs.getString(3));

				// actualizando el indicador de confirmacion de recibo de notificacion
				preparedStatement = connection.prepareStatement(sqlUpdateIndReceived.toString());
				preparedStatement.setTimestamp(1, new Timestamp(new Date().getTime()));
				preparedStatement.setString(2, rs.getString(1));
				preparedStatement.setString(3, rs.getString(2));
				preparedStatement.executeUpdate();

				lstResult.add(resultDecryptParamUrl);
			}
			preparedStatement.close();
			connection.close();
			return lstResult;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new EJBException(e);
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					log.error(e.getMessage());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.error(e.getMessage());
				}
			}
		}
	}

	/** metodo para obtener informacion de las glosas que fueron notificadas por concepto de cobro de derechos economicos desde
	 * el CAT Electronico
	 * 
	 * @throws ServiceException */
	public List<ResultoWsNotification> getInfoNotificationCollectionEconomic() throws ServiceException {
		List<ResultoWsNotification> lstResult = new ArrayList<>();

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs;

		StringBuilder sql = new StringBuilder();
		sql.append(" select gc.ID_GLOSS_PK, gc.PARAMETER_URL_RECEIVED_PK");
		sql.append(" from CAEDV.GLOSS_COLLECTION_ECONOMIC@cat.world gc");
		sql.append(" where gc.IND_RECEIVED = 0");

		// query para actualizar los indicadores de confirmacion de recibo de notificacion (cobro de derechos)
		StringBuilder sqlUpdateIndReceived = new StringBuilder();
		sqlUpdateIndReceived = new StringBuilder();
		sqlUpdateIndReceived.append(" update CAEDV.GLOSS_COLLECTION_ECONOMIC@cat.world gc set");
		// 1: recibo confirmado
		sqlUpdateIndReceived.append(" gc.IND_RECEIVED = 1");
		sqlUpdateIndReceived.append(" ,gc.DATE_RECEIVED = ?");
		sqlUpdateIndReceived.append(" where gc.ID_GLOSS_PK = ?");
		sqlUpdateIndReceived.append(" and gc.PARAMETER_URL_RECEIVED_PK = ?");

		try {
			ResultoWsNotification resultDecryptParamUrl;

			connection = null;// dataSourceBBV.getConnection();
			preparedStatement = connection.prepareStatement(sql.toString());
			rs = preparedStatement.executeQuery();
			while (rs.next()) {
				resultDecryptParamUrl = generateUrlNotification.getResutWsNotification(rs.getString(2));

				// actualizando el indicador de confirmacion de recibo de notificacion
				preparedStatement = connection.prepareStatement(sqlUpdateIndReceived.toString());
				preparedStatement.setTimestamp(1, new Timestamp(new Date().getTime()));
				preparedStatement.setString(2, rs.getString(1));
				preparedStatement.setString(3, rs.getString(2));
				preparedStatement.executeUpdate();

				resultDecryptParamUrl.setParamUrlReceived(rs.getString(2));
				lstResult.add(resultDecryptParamUrl);
			}
			preparedStatement.close();
			connection.close();
			return lstResult;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new EJBException(e);
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					log.error(e.getMessage());
				}
			}

			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.error(e.getMessage());
				}
			}
		}
	}

}
