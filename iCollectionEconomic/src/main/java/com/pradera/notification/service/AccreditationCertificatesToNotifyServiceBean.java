package com.pradera.notification.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.CorrelativeServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.ReportAccreditation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.CatType;
import com.pradera.model.custody.type.CertificationType;
import com.pradera.model.generalparameter.Correlative;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.negotiation.HolderAccountOperation;

@Stateless
public class AccreditationCertificatesToNotifyServiceBean extends CrudDaoServiceBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3156222100328464029L;

	@Inject
	private GeneralParametersFacade generalParameterFacade;

	@Inject
	CorrelativeServiceBean correlativeServiceBean;

	/** Search accreditation detail by operation.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception */
	@SuppressWarnings("unchecked")
	public List<AccreditationDetail> searchAccreditationDetailByOperation(Long accreditationOperationId)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	Select DISTINCT AD	");
		sbQuery.append("	FROM AccreditationDetail AD	");
		sbQuery.append("	JOIN AD.accreditationOperation AO	");
		sbQuery.append("	Inner Join Fetch AD.accreditationMarketFactList	AML ");
		sbQuery.append("	where AO.custodyOperation.idCustodyOperationPk=:accreditationOperationId	");
		sbQuery.append("	and AML.indActive=:parOne	");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("accreditationOperationId", accreditationOperationId);
		query.setParameter("parOne", GeneralConstants.ONE_VALUE_INTEGER);

		return (List<AccreditationDetail>) query.getResultList();
	}

	/** Search holder account balance.
	 * 
	 * @param idAccreditationPk the id accreditation pk
	 * @return the list
	 * @throws ServiceException the service exception */
	@SuppressWarnings("unchecked")
	public List<BlockOperation> searchBlockOperations(Long idAccreditationPk) throws ServiceException {
		// Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select BOC.blockOperation.idBlockOperationPk ");
		sbQuery.append("from BlockOperationCertification BOC ");
		sbQuery.append("where  BOC.accreditationDetail.accreditationOperation.idAccreditationOperationPk =:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAccreditationPk", idAccreditationPk);

		List<BlockOperation> blockOperations = new ArrayList<BlockOperation>();

		List<Object> objectList = query.getResultList();
		for (int i = 0; i < objectList.size(); ++i) {
			// Setting new Holder Account balance
			BlockOperation objBlockOperation = new BlockOperation();
			// Getting Results from data base
			objBlockOperation.setIdBlockOperationPk((Long) objectList.get(i));
			blockOperations.add(objBlockOperation);
		}
		return blockOperations;
	}

	/** Search holder account operation to update.
	 * 
	 * @param objAO the obj ao
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception */
	@SuppressWarnings("unchecked")
	public List<HolderAccountOperation> searchHolderAccountOperationToUpdate(AccreditationOperation objAO,
			LoggerUser loggerUser) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		List<HolderAccountOperation> lstHao = null;
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("Select mixto.idReportAccreditationPk, ");
		sbQuery.append(" hao.idHolderAccountOperationPk ");
		sbQuery.append(" from AccreditationDetail ad ");
		sbQuery.append(" join ad.accreditationReported mixto join mixto.holderAccountOperationFk hao ");
		sbQuery.append(" WHERE ad.accreditationOperation.idAccreditationOperationPk =:idOperation");
		mapParam.put("idOperation", objAO.getCustodyOperation().getIdCustodyOperationPk());

		Query query = em.createQuery(sbQuery.toString());

		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}

		List<Object> objectList = query.getResultList();
		if (objectList.size() > 0) {
			lstHao = new ArrayList<HolderAccountOperation>();
			for (int i = 0; i < objectList.size(); ++i) {
				HolderAccountOperation objHao = new HolderAccountOperation();
				// Getting Results from data base
				Object[] sResults = (Object[]) objectList.get(i);
				if (sResults[1] != null) {
					objHao.setIdHolderAccountOperationPk((Long) sResults[1]);
				}
				if (loggerUser.getIdPrivilegeOfSystem() != null) {
					objHao.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
				}
				if (loggerUser.getAuditTime() != null) {
					objHao.setLastModifyDate(loggerUser.getAuditTime());
				}
				if (loggerUser.getIpAddress() != null) {
					objHao.setLastModifyIp(loggerUser.getIpAddress());
				}
				if (loggerUser.getUserName() != null) {
					objHao.setLastModifyUser(loggerUser.getUserName());
				}
				lstHao.add(objHao);
			}
		}
		return lstHao;
	}

	/** Update reported operation.
	 * 
	 * @param lstHAO the lst hao
	 * @param state the state
	 * @throws ServiceException the service exception */
	public void updateReportedOperation(List<HolderAccountOperation> lstHAO, Integer state) throws ServiceException {
		// Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE HolderAccountOperation hao");
		sbQuery.append(" SET hao.indAccreditation = :indAccreditation, ");
		sbQuery.append(" hao.lastModifyApp = :lastModifyApp, ");
		sbQuery.append(" hao.lastModifyDate = :lastModifyDate, ");
		sbQuery.append(" hao.lastModifyIp = :lastModifyIp, ");
		sbQuery.append(" hao.lastModifyUser = :lastModifyUser ");
		sbQuery.append(" WHERE  hao.idHolderAccountOperationPk in :lstPks");
		Query query = em.createQuery(sbQuery.toString());
		List<Long> lstIds = new ArrayList<Long>();
		for (int i = 0; i < lstHAO.size(); i++) {
			Long id = lstHAO.get(i).getIdHolderAccountOperationPk();
			lstIds.add(id);
		}
		query.setParameter("lstPks", lstIds);
		if (AccreditationOperationStateType.REGISTRED.getCode().equals(state)) {
			query.setParameter("indAccreditation", BooleanType.YES.getCode());
		} else if (AccreditationOperationStateType.CONFIRMED.getCode().equals(state)) {
			query.setParameter("indAccreditation", BooleanType.NO.getCode());
		}
		query.setParameter("lastModifyApp", lstHAO.get(0).getLastModifyApp());
		query.setParameter("lastModifyDate", lstHAO.get(0).getLastModifyDate());
		query.setParameter("lastModifyIp", lstHAO.get(0).getLastModifyIp());
		query.setParameter("lastModifyUser", lstHAO.get(0).getLastModifyUser());
		query.executeUpdate();
	}

	/** Update block operation.
	 * 
	 * @param blockOperation the block operation
	 * @throws ServiceException the service exception */
	public void updateBlockOperation(BlockOperation blockOperation) throws ServiceException {
		// Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE BlockOperation BO");
		sbQuery.append("   SET BO.indAccreditation = :indAccreditation");
		sbQuery.append(" WHERE  BO.idBlockOperationPk=:blockOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("blockOperationPk", blockOperation.getIdBlockOperationPk());
		query.setParameter("indAccreditation", blockOperation.getIndAccreditation());
		query.executeUpdate();
	}

	/** Metodo que devuelve el correlativo de un determinado tipo de CAT
	 * 
	 * @param correlativeFilter
	 * @param catType Tipo de CAT: CAT o CATB
	 * @return
	 * @throws ServiceException */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Correlative correlativeServiceBean(Correlative correlativeFilter, CatType catType) throws ServiceException {
		Correlative correlative = correlativeServiceBean.getCorrelativeServiceBean(correlativeFilter);

		StringBuilder sql = null;

		if (catType.getCode().equals(CatType.CAT.getCode())) {
			sql = new StringBuilder(" SELECT SQ_CORRELATIVECAT.NEXTVAL FROM DUAL ");
		}

		if (catType.getCode().equals(CatType.CATB.getCode())) {
			sql = new StringBuilder(" SELECT SQ_CORRELATIVECATB.NEXTVAL FROM DUAL ");
		}

		Query query = em.createNativeQuery(sql.toString());
		BigDecimal correlativo = (BigDecimal) query.getSingleResult();
		em.flush();
		correlative.setCorrelativeNumber(Integer.valueOf(correlativo.intValue()));
		return correlative;

		// Correlative correlative =correlativeServiceBean.getCorrelativeServiceBean(correlativeFilter);
		// correlative.setCorrelativeNumber(correlative.getCorrelativeNumber()+1);
		// return correlative;
	}
	
	public AccreditationOperation confirmState(AccreditationOperation accreditationOperation){
		StringBuilder queryBuilder=new StringBuilder();
		queryBuilder.append("update AccreditationOperation ao set op");
		return new AccreditationOperation();
		
	}

	/** Searchh accreditation detail mixed.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception */
	@SuppressWarnings("unchecked")
	public List<AccreditationDetail> searchhAccreditationDetailMixed(Long accreditationOperationId) throws ServiceException {
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		ParameterTableTO filterParameterTable1 = new ParameterTableTO();
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		List<ParameterTable> lstTypeAffectacion = generalParameterFacade
				.getListParameterTableServiceBean(filterParameterTable);

		filterParameterTable1.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		List<ParameterTable> lstLevelAffectation = generalParameterFacade
				.getListParameterTableServiceBean(filterParameterTable1);

		List<AccreditationDetail> accreditationDetail = new ArrayList<AccreditationDetail>();

		BlockOperation bo = new BlockOperation();

		String strQuery = "	SELECT DISTINCT AD FROM AccreditationDetail AD " + "	INNER JOIN FETCH  AD.holder	"
				+ "	INNER JOIN FETCH  AD.holderAccount	" + "	INNER JOIN FETCH  AD.security	"
				+ "	INNER JOIN FETCH  AD.idParticipantFk	" + "	LEFT OUTER JOIN FETCH  AD.blockOperationCertifications BOC	"
				+ "	WHERE AD.accreditationOperation.idAccreditationOperationPk=:accreditationOperationId	";
		Query query = em.createQuery(strQuery);
		query.setParameter("accreditationOperationId", accreditationOperationId);
		accreditationDetail = query.getResultList();
		for (AccreditationDetail ad : accreditationDetail) {
			ad.getAccreditationReported().size();
			for (com.pradera.model.custody.accreditationcertificates.BlockOperationCertification boc : ad
					.getBlockOperationCertifications()) {
				bo = boc.getBlockOperation();
				bo.getBlockLevel();
				bo.getBlockRequest().getBlockEntity().getIdBlockEntityPk();
				bo.getCustodyOperation().getIdCustodyOperationPk();
				bo.getSecurities().getIssuer().getIdIssuerPk();
				bo.getHolderAccount().getIdHolderAccountPk();
				bo.getParticipant().getIdParticipantPk();
				for (ParameterTable parameter : lstTypeAffectacion) {
					if (parameter.getParameterTablePk().equals(boc.getBlockType())) {
						boc.getBlockOperation().getBlockRequest().setBlockTypeDescription(parameter.getDescription());
					}
				}
				for (ParameterTable parameter : lstLevelAffectation) {
					if (parameter.getParameterTablePk().equals(bo.getBlockLevel())) {
						boc.getBlockOperation().setBlockLevelDescription(parameter.getDescription());
					}
				}
			}
			for (ReportAccreditation ra : ad.getAccreditationReported()) {
				ra.getHolderAccountOperationFk().getMechanismOperation().getIdMechanismOperationPk();
			}
		}
		return accreditationDetail;
	}

	/** Find accreditation operation.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation
	 * @throws ServiceException the service exception */
	public AccreditationOperation findAccreditationOperation(Long accreditationOperationId) throws ServiceException {
		// String builder
		StringBuilder sbQuery = new StringBuilder();
		// AccreditationOperation accreditationOperation = new AccreditationOperation();
		// Creating query
		sbQuery.append("Select ao ");
		sbQuery.append("FROM AccreditationOperation AO ");
		sbQuery.append(" left JOIN fetch AO.holder H ");
		sbQuery.append(" left JOIN fetch AO.participantPetitioner PP ");
		sbQuery.append(" inner JOIN fetch AO.custodyOperation co ");
		sbQuery.append(" left JOIN fetch AO.listAccreditationDeliveryInformation lad ");
		sbQuery.append("where AO.idAccreditationOperationPk = :accreditationOperationId ");
		Map<String, Object> parameters = new HashMap<String, Object>();
		// Setting parameter
		parameters.put("accreditationOperationId", accreditationOperationId);
		// Query Result
		return (AccreditationOperation) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/** Accreditation consult.
	 * 
	 * @param aOperation the a operation
	 * @param initialDate the initial date
	 * @param endDate the end date
	 * @param currency the currency
	 * @param securityClass the security class
	 * @param issuer the issuer
	 * @return the list
	 * @throws ServiceException the service exception */
	@SuppressWarnings("unchecked")
	public List<AccreditationOperation> accreditationAvailableConsult(AccreditationOperation aOperation, Date initialDate,
			Date endDate, Integer currency, Integer securityClass, String issuer) throws ServiceException {

		StringBuilder sbQuery = new StringBuilder();
		List<Integer> lstCertificationTypeAll = new ArrayList<Integer>();
		lstCertificationTypeAll.add(CertificationType.AVAILABLE_BALANCE.getCode());
		lstCertificationTypeAll.add(CertificationType.BLOCKED_BALANCE.getCode());
		lstCertificationTypeAll.add(CertificationType.REPORTED_BALANCE.getCode());

		List<Integer> lstCertificationTypeAR = new ArrayList<Integer>();
		lstCertificationTypeAR.add(CertificationType.AVAILABLE_BALANCE.getCode());
		lstCertificationTypeAR.add(CertificationType.REPORTED_BALANCE.getCode());

		List<Integer> lstCertificationTypeBR = new ArrayList<Integer>();
		lstCertificationTypeBR.add(CertificationType.BLOCKED_BALANCE.getCode());
		lstCertificationTypeBR.add(CertificationType.REPORTED_BALANCE.getCode());

		// Building the Query
		sbQuery.append("Select");
		sbQuery.append(" ao ");
		sbQuery.append(" from AccreditationDetail ad ");
		sbQuery.append(" left join ad.accreditationOperation ao ");
		sbQuery.append(" left join fetch ao.custodyOperation co ");
		sbQuery.append(" left join ao.participantPetitioner participantPetitioner ");

		sbQuery.append(" WHERE 1=1");
		if (aOperation.getCertificationType() != null) {
			sbQuery.append(" AND ao.certificationType in :certificationType");
		}

		if (aOperation.getAccreditationState() != null) {
			sbQuery.append(" AND ao.accreditationState =:statePrm");
		}

		if (Validations.validateIsNotNullAndNotEmpty(aOperation.getCatType())) {
			sbQuery.append(" AND ao.catType =:catType");
		}

		Query query = null;

		if (aOperation.getPetitionerType() != null && aOperation.getPetitionerType() != 0) {
			sbQuery.append(" AND ao.petitionerType =:petitionerPrm");
		}

		// if (aOperation.getHolder().getIdHolderPk() != null) {
		// sbQuery.append(" AND ao.holder.idHolderPk =:holderIdPrm");
		// }

		if (aOperation.getParticipantPetitioner() != null
				&& Validations.validateIsNotNull(aOperation.getParticipantPetitioner().getIdParticipantPk())
				&& aOperation.getParticipantPetitioner().getIdParticipantPk() != 0) {
			sbQuery.append(" AND ao.participantPetitioner.idParticipantPk =:participantPetitionerPrm");
		}

		if (aOperation.getMotive() != null) {
			sbQuery.append(" AND ao.motive =:motivePrm ");
		}
		if (aOperation.getCertificateNumber() != null) {
			sbQuery.append(" AND ao.certificateNumber =:certificateNumber ");
		}

		// if (aOperation.getSecurity().getIdSecurityCodePk() != null) {
		// sbQuery.append(" AND ad.security.idSecurityCodePk =:securityCodepk ");
		// }

		if (currency != null) {
			sbQuery.append(" AND ad.security.currency =:currency ");
		}

		if (securityClass != null) {
			sbQuery.append(" AND ad.security.securityClass =:securityClass ");
		}
		if (issuer != null) {
			sbQuery.append(" AND ad.security.issuer.idIssuerPk =:issuer ");
		}

		if (aOperation.getCustodyOperation() != null) {
			if (aOperation.getCustodyOperation().getOperationNumber() != null
					&& aOperation.getCustodyOperation().getOperationNumber() != 0) {
				sbQuery.append(" AND ao.custodyOperation.operationNumber =:requestNumber");
			} else if (Validations.validateIsNullOrNotPositive(aOperation.getCertificateNumber())) {
				sbQuery.append(" AND Trunc(ao.custodyOperation.registryDate) between :registerDatePrm and :endDatePrm ");
				sbQuery.append(" ORDER BY ao.custodyOperation.operationNumber DESC ");
			}
		}

		query = em.createQuery(sbQuery.toString());

		if (aOperation.getAccreditationState() != null) {
			query.setParameter("statePrm", aOperation.getAccreditationState());
		}

		if (aOperation.getCertificateNumber() != null) {
			query.setParameter("certificateNumber", aOperation.getCertificateNumber());
		}

		if (currency != null) {
			query.setParameter("currency", currency);
		}

		if (securityClass != null) {
			query.setParameter("securityClass", securityClass);
		}

		if (issuer != null) {
			query.setParameter("issuer", issuer);
		}

		if (Validations.validateIsNotNullAndNotEmpty(aOperation.getCatType())) {
			query.setParameter("catType", aOperation.getCatType());
		}

		if (aOperation.getCertificationType() != null) {
			if (aOperation.getCertificationType().equals(CertificationType.REPORTED_BALANCE.getCode())) {
				query.setParameter("certificationType", lstCertificationTypeAll);
			} else {
				if (aOperation.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())) {
					query.setParameter("certificationType", lstCertificationTypeAR);
				}
				if (aOperation.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())) {
					query.setParameter("certificationType", lstCertificationTypeBR);
				}

			}
		}

		if (aOperation.getCustodyOperation() != null) {
			if (aOperation.getCustodyOperation().getOperationNumber() != null
					&& aOperation.getCustodyOperation().getOperationNumber() != 0) {
				query.setParameter("requestNumber", aOperation.getCustodyOperation().getOperationNumber());
			} else if (Validations.validateIsNullOrNotPositive(aOperation.getCertificateNumber())) {
				query.setParameter("registerDatePrm", initialDate);
				query.setParameter("endDatePrm", endDate);
			}
		}

		if (aOperation.getPetitionerType() != null && aOperation.getPetitionerType() != 0) {
			query.setParameter("petitionerPrm", aOperation.getPetitionerType());
		}

		// if (aOperation.getHolder().getIdHolderPk() != null) {
		// query.setParameter("holderIdPrm", aOperation.getHolder().getIdHolderPk());
		// }

		if (aOperation.getParticipantPetitioner() != null
				&& Validations.validateIsNotNull(aOperation.getParticipantPetitioner().getIdParticipantPk())
				&& aOperation.getParticipantPetitioner().getIdParticipantPk() != 0) {
			query.setParameter("participantPetitionerPrm", aOperation.getParticipantPetitioner().getIdParticipantPk());
		}

		if (aOperation.getMotive() != null) {
			query.setParameter("motivePrm", aOperation.getMotive());
		}

		// if (aOperation.getSecurity().getIdSecurityCodePk() != null) {
		// query.setParameter("securityCodepk", aOperation.getSecurity().getIdSecurityCodePk());
		// }

		return (List<AccreditationOperation>) query.getResultList();
	}

	/** Search holder account balance.
	 * 
	 * @param idHolderAccountPk
	 * @param security
	 * @return
	 * @throws ServiceException */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> searchHolderAccountBalance(Long idHolderAccountPk, String security)
			throws ServiceException {

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select hab ");
		sbQuery.append("from HolderAccountBalance hab ");
		sbQuery.append("left join fetch hab.security sec ");
		sbQuery.append("left join fetch hab.holderAccount ha ");
		sbQuery.append("left join fetch ha.participant pa ");
		sbQuery.append("where hab.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append("and hab.security.idSecurityCodePk = :securityPk ");
		sbQuery.append("and hab.availableBalance > 0");

		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("securityPk", security);

		List<HolderAccountBalance> list =(List<HolderAccountBalance>) query.getResultList();
		return list;
	}

}
