package com.pradera.notification.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.SecuritiesAlmostObjectTO;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationMarketFact;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.BusinessProcess;
import com.pradera.model.process.ProcessLogger;
import com.pradera.notification.service.NotificationPaymentAgentServiceBean;
import com.pradera.notification.to.SecuritiesAlmostToExpireTO;
import com.pradera.notification.to.SecuritiesNotifiedCollectionTO;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.PaymentAgent;

public class NotificationPaymentAgentFacadeBean  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private PraderaLogger log;

	@Inject
	private NotificationPaymentAgentServiceBean notificationServiceBean;
	
	@Inject
	private CollectionRigthServiceBean collectionRigthServiceBean;


	/** The accreditation facade bean. */
	@Inject
	private AccreditationCertificatesToNotifyFacadeBean accreditationfacadeBean;

	@Inject
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/** valor del motivo para el registro del CAT */
	private static final Integer MOTIVE_VALUE_COLLECTION_ECONOMIC = 2021;

	/** valor del tipo de certificado a registrar */
	private static final Integer CERTIFICATE_TYPE_MIXED_VALUE = 1736;

	/** valor del tipo de solicitante del CAT en este caso es Participante */
	private static final Integer PETITIONER_TYPE_PARTICIPANT_VALUE = 224;

	/** valor de la manera de pago SIN COBRO */
	private static final Integer WAY_OF_PAYMENT_VALUE = 636;

	private static final Integer CAT_TYPE_VALUE = 2368;

	private List<HolderAccountBalance> tempAccountBalance;

	/** The accreditation operation. */
	private AccreditationOperation accreditationOperation;

	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;

	/** The obj accreditation detail. */
	private AccreditationDetail objAccreditationDetail;

	/** The accreditation details. */
	private List<AccreditationDetail> accreditationDetails = new ArrayList<AccreditationDetail>();

	/** The ao selection. */
	private List<AccreditationOperation> listAccreditationOperationSelection;

	/** The ao selection. */
	private AccreditationOperation accreditationOperationSelection;
	

	/** The confirmeded type code. */
	private Integer confirmededTypeCode = AccreditationOperationStateType.CONFIRMED.getCode();
	
	@Inject
	private NotificationServiceFacade notificationServiceFacade;

	
	/** inicializando AccreditationOperation por defecto
	 * 
	 * @param idParticipant
	 * @param datePayment
	 * @param cui */
	private synchronized AccreditationOperation  initAccreditationOperation(Long idParticipant, Date datePayment, String alternativeCode) {
		accreditationOperation = new AccreditationOperation();
		accreditationOperation.setRegisterDate(CommonsUtilities.currentDate());
		accreditationOperation.setIdAccreditationOperationPk(null);
		accreditationOperation.setCustodyOperation(new CustodyOperation());
		accreditationOperation.setParticipantPetitioner(new Participant(idParticipant));
		accreditationOperation.setIssuanceDate(CommonsUtilities.currentDate());
		accreditationOperation.setExpirationDate(datePayment);
		accreditationOperation.setRealExpirationDate(datePayment);
		accreditationOperation.setHolder(notificationServiceBean.find(Holder.class,collectionRigthServiceBean.extractHolder(alternativeCode)));
		accreditationOperation.setAccreditationCertificate(null);
		accreditationOperation.setSustentFileName(null);
		accreditationOperation.setPetitionerType(PETITIONER_TYPE_PARTICIPANT_VALUE);
		accreditationOperation.setMotive(MOTIVE_VALUE_COLLECTION_ECONOMIC);
		accreditationOperation.setCertificationType(CERTIFICATE_TYPE_MIXED_VALUE);
		accreditationOperation.setWayOfPayment(WAY_OF_PAYMENT_VALUE);
		accreditationOperation.setValidityDays(CommonsUtilities.getDaysBetween(accreditationOperation.getIssuanceDate(),datePayment));
		accreditationOperation.setVersion(Long.valueOf(0));
		accreditationOperation.setCatType(CAT_TYPE_VALUE);
		return accreditationOperation;
	}
	
	/** metodo para preparar el CAT con los parametros enviados
	 * 
	 * @param idHolderAccountPk
	 * @param idSecurityCodePk
	 * @throws ServiceException */
	private synchronized void buildCat(Long idHolderAccountPk, String idSecurityCodePk) throws ServiceException {
		tempAccountBalance = accreditationfacadeBean.searchHolderAccountBalance(idHolderAccountPk, idSecurityCodePk);
		for (HolderAccountBalance hab : tempAccountBalance) {
			hab.setTransitBalance(new BigDecimal(0));
			hab.setBanBalance(new BigDecimal(0));
			hab.setOtherBlockBalance(new BigDecimal(0));
			hab.setSaleBalance(new BigDecimal(0));
			hab.setPurchaseBalance(new BigDecimal(0));
			hab.setReportedBalance(new BigDecimal(0));
			hab.setReportingBalance(new BigDecimal(0));
			hab.setMarginBalance(new BigDecimal(0));
			hab.setAccreditationBalance(new BigDecimal(0));
			// transferir por defecto todo el saldo disponible
			hab.setTransferAmmount(hab.getAvailableBalance());

			setMarketFactUI(hab);
		}
	}

	/** Sets the market fact ui.
	 * 
	 * @param objHolAccBal the new market fact ui */
	private void setMarketFactUI(HolderAccountBalance objHolAccBal) {
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBal.getSecurity().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBal.getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBal.getHolderAccount().getParticipant().getIdParticipantPk());
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		if (objHolAccBal.getTransferAmmount() != null && !objHolAccBal.getTransferAmmount().equals(BigDecimal.ZERO)) {
			marketFactBalance.setBalanceResult(objHolAccBal.getTransferAmmount());
			marketFactBalance.setBalanceResult(BigDecimal.ZERO);
		}

		try {
			holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		selectMarketFactBalance();
	}

	
	/** Select market fact balance. */
	private void selectMarketFactBalance() {
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		for (HolderAccountBalance hab : tempAccountBalance) {
			if (hab.getHolderAccount().getParticipant().getIdParticipantPk().equals(marketFactBalance.getParticipantPk())
					&& hab.getHolderAccount().getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk())
					&& hab.getSecurity().getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())) {
				accreditBalance(hab, marketFactBalance);
				return;
			}
		}
	}

	/** Accredit Available Balance. Method that accredited available balance
	 * 
	 * @param holderAccountBalance the holder account balance
	 * @param marketList the market list */
	private void accreditBalance(HolderAccountBalance holderAccountBalance, MarketFactBalanceHelpTO marketList) {
		BigDecimal bdSumTotalB = null;
		if ((holderAccountBalance.getTransferAmmount().doubleValue() <= holderAccountBalance.getAvailableBalance()
				.doubleValue())) {
			if (holderAccountBalance.getTransferAmmount().doubleValue() >= 0) {

				objAccreditationDetail = new AccreditationDetail();

				Security sec = new Security();
				sec.setIdSecurityCodePk(holderAccountBalance.getSecurity().getIdSecurityCodePk());
				objAccreditationDetail.setSecurity(sec);
				objAccreditationDetail.setAvailableBalance(holderAccountBalance.getTransferAmmount());
				objAccreditationDetail.setIdParticipantFk(holderAccountBalance.getHolderAccount().getParticipant());
				objAccreditationDetail.setHolder(accreditationOperation.getHolder());
				objAccreditationDetail.setAccreditationOperation(accreditationOperation);
				objAccreditationDetail.setHolderAccount(holderAccountBalance.getHolderAccount());

				objAccreditationDetail.setBanBalance(holderAccountBalance.getBanBalance());
				objAccreditationDetail.setPawnBalance(holderAccountBalance.getPawnBalance());
				objAccreditationDetail.setOtherBalance(holderAccountBalance.getOtherBlockBalance());
				objAccreditationDetail.setReportingBalance(holderAccountBalance.getReportingBalance());
				bdSumTotalB = objAccreditationDetail.getAvailableBalance().add(
						objAccreditationDetail.getBanBalance().add(
								objAccreditationDetail.getPawnBalance().add(
										objAccreditationDetail.getOtherBalance().add(
												objAccreditationDetail.getReportingBalance()))));
				objAccreditationDetail.setTotalBalance(bdSumTotalB);
			}
			if (marketList != null) {
				List<AccreditationMarketFact> lstAMF = new ArrayList<AccreditationMarketFact>();
				objAccreditationDetail.setAccreditationMarketFactList(new ArrayList<AccreditationMarketFact>());
				for (MarketFactDetailHelpTO mkfDetTO : marketList.getMarketFacBalances()) {
					AccreditationMarketFact marketFact = new AccreditationMarketFact();
					marketFact.setIdAccreditationMF(mkfDetTO.getMarketFactBalancePk());
					marketFact.setQuantityToAccredit(mkfDetTO.getTotalBalance());
					marketFact.setMarketDate(mkfDetTO.getMarketDate());
					marketFact.setMarketPrice(mkfDetTO.getMarketPrice());
					marketFact.setMarketRate(mkfDetTO.getMarketRate());
					marketFact.setAccreditationDetailFk(objAccreditationDetail);

					lstAMF.add(marketFact);
				}
				objAccreditationDetail.setAccreditationMarketFactList(lstAMF);
				accreditationDetails.add(objAccreditationDetail);
			}
		}
	}

	/** Save reported.
	 * 
	 * @return true, if successful */
	private synchronized Long registreCertificate() {
		Long idAccreditationOperationPk = Long.valueOf(0);

		if (accreditationDetails.size() > 0) {
			for (AccreditationDetail objAccreditationDetail : accreditationDetails) {
				
				List<AccreditationDetail> lstAd = new ArrayList<AccreditationDetail>();
				lstAd.add(objAccreditationDetail);
				accreditationOperation.setAccreditationDetails(lstAd);
				accreditationOperation.setAccreditationState(AccreditationOperationStateType.REGISTRED.getCode());
				accreditationOperation.setIndDelivered(BooleanType.NO.getCode());
				accreditationOperation.setIndGenerated(BooleanType.NO.getCode());
				accreditationOperation.setDestinationCertificate(BooleanType.NO.getCode());
				// Creating Custody Operation Object
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_ACCREDITATION.getCode());
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				
				custodyOperation.setLastModifyDate(CommonsUtilities.currentDate());
				custodyOperation.setLastModifyApp(789);
				custodyOperation.setLastModifyUser("ADMIN");
				custodyOperation.setLastModifyIp("192.168.100.241");
				// TODO userInfo no funciona en esta prueba, corregir
				// custodyOperation.setRegistryUser(this.userInfo.getUserAccountSession().getUserName());
				custodyOperation.setRegistryUser("ADMIN");

				custodyOperation.setState(accreditationOperation.getAccreditationState());
				accreditationOperation.setCustodyOperation(custodyOperation);
				// Saving
				try {
					accreditationOperation = accreditationfacadeBean.insertReported(accreditationOperation);
					log.info("CAT REGISTRADO CON EXITO: El ID DEL CAT ES: "
							+ accreditationOperation.getIdAccreditationOperationPk());
					idAccreditationOperationPk = accreditationOperation.getIdAccreditationOperationPk();
				} catch (ServiceException e) {
					idAccreditationOperationPk = Long.valueOf(0);
					log.info("error al registrar CAT");
				}
			}
		}
		return idAccreditationOperationPk;
	}
	
	public Long searchIdRegistreAccreditationOperation(Long idParticipantPk,Long idHolderAccountPk, String idSecurityCodePk){
		return notificationServiceBean.searchAccreditationOperation(idParticipantPk, idHolderAccountPk, idSecurityCodePk);
	}

	// ================= metodos para la confirmacion ============================
	/** Accreditation consult. This method is executed in Search.xml, it searchs all accreditation */
	@LoggerAuditWeb
	private void accreditationConsult() throws Exception {
		listAccreditationOperationSelection = new ArrayList<AccreditationOperation>();
		AccreditationOperation aoAux = new AccreditationOperation();
		CustodyOperation co = new CustodyOperation();

		if (accreditationOperation.getCustodyOperation().getOperationNumber() != null
				&& accreditationOperation.getCustodyOperation().getOperationNumber() != 0) {
			co.setOperationNumber(accreditationOperation.getCustodyOperation().getOperationNumber());
			aoAux.setCustodyOperation(co);
		}
		List<AccreditationOperation> accOperationList = accreditationfacadeBean.accreditationAvailableConsult(aoAux, null,
				null, null, null, null);
		for (int j = 0; j < accOperationList.size(); j++) {
			// setting values by generate and deliver columns
			if (accOperationList.get(j).getIndGenerated() != null
					&& accOperationList.get(j).getIndGenerated().equals(new Integer(1))) {
				accOperationList.get(j).setStrGenerated(BooleanType.YES.getValue());
			} else {
				accOperationList.get(j).setStrGenerated(BooleanType.NO.getValue());
			}

			if (accOperationList.get(j).getIndDelivered() != null
					&& accOperationList.get(j).getIndDelivered().equals(new Integer(1))) {
				accOperationList.get(j).setStrDelivered(BooleanType.YES.getValue());
			} else {
				accOperationList.get(j).setStrDelivered(BooleanType.NO.getValue());
			}
		}
		listAccreditationOperationSelection = new ArrayList<AccreditationOperation>(accOperationList);
		if (listAccreditationOperationSelection.size() == 1) {
			accreditationOperationSelection = new AccreditationOperation();
			accreditationOperationSelection = listAccreditationOperationSelection.get(0);
		}

	}

	/** Find accreditation operation.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation */
	@LoggerAuditWeb
	private AccreditationOperation findAccreditationOperation(Long accreditationOperationId) throws Exception {
		AccreditationOperation accreditationOperationFound = null;
		accreditationOperationFound = accreditationfacadeBean.findAccreditationOperation(accreditationOperationId);
		return accreditationOperationFound;
	}

	/** Load mixto accreditation.
	 * 
	 * @param from the from */
	private void loadMixtoAccreditation() throws Exception {
		accreditationConsult();
		Long accreditationOperationId = accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk();
		listAccreditationOperationSelection = new ArrayList<AccreditationOperation>();
		accreditationOperationSelection = findAccreditationOperation(accreditationOperationId);
		// Searching accreditation details searchhAccreditationDetailMixed
		accreditationDetails = accreditationfacadeBean.findAccreditationDetailMixed(accreditationOperationId);
	}

	/** Change Accreditation Status. Change the accreditation Status
	 * 
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws InterruptedException the interrupted exception */
	@LoggerAuditWeb
	public void changeAccreditationStatus() throws Exception {
		loadMixtoAccreditation();
		// BusinessProcess businessProcess = new BusinessProcess();
		accreditationfacadeBean.confirmAccreditationRequest(accreditationOperationSelection, confirmededTypeCode);
		accreditationDetails = new ArrayList<AccreditationDetail>();
		// businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode());
		// // INICIO ENVIO DE NOTIFICACIONES
		// Object[] parameters = {
		// String.valueOf(accreditationOperationToChange.getCustodyOperation().getOperationNumber()) };
		// notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
		// null, parameters);
		// // FIN ENVIO DE NOTIFICACIONES
	}// end changeAccreditationStatus

	/**Buscamos todo los valores a ser notificados
	 * @param idParticipantPk
	 * @param idIssuerPk
	 * @throws Exception */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SecuritiesAlmostObjectTO getListSecuritiesToNotifyCurrency(ProcessLogger processLogger,Long idParticipantPk,String idIssuerPk,String mNemonicPaymentAgent, Integer currency,Long idHolderPk,Date paymentDate,Date nextBusinessDay,Integer securityClass,Long idBankPk,String depAccountNumber) throws Exception {
		
		SecuritiesNotifiedCollectionTO collectionTO = notificationServiceBean.getSecuritiesToNotify(idParticipantPk,idIssuerPk,mNemonicPaymentAgent,currency,idHolderPk,paymentDate,nextBusinessDay,securityClass,idBankPk,depAccountNumber);
		List<CollectionEconomicRigth> listSecuritiesToNotify = null;
		if (collectionTO != null && collectionTO.getListCollectionEconomics() != null) 
			listSecuritiesToNotify = collectionTO.getListCollectionEconomics();
		
		HolderAccountBalance holderAccountBalance = null;
		String descBalance = "N/D";
		BusinessProcess businessProcess;
		if(collectionTO != null && collectionTO.getSecuritiesLockedBalance()!=null)
			for (HolderAccountBalancePK accountBalancePK : collectionTO.getSecuritiesLockedBalance()) {
				try {
					holderAccountBalance = collectionRigthServiceBean.find(HolderAccountBalance.class, accountBalancePK);
					descBalance = collectionRigthServiceBean.descHolderAccountBalance(holderAccountBalance);
				} catch (Exception e) { 
					descBalance = null;
					e.printStackTrace();
				}
				if(descBalance!=null){
					businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_SECURITIES_NOT_SENT_FOR_CAT.getCode());
					String descPaymentDate = CommonsUtilities.convertDatetoString(paymentDate);
					Object[] parameters = { accountBalancePK.getIdSecurityCodePk(), descBalance,descPaymentDate};
					notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null, parameters);
				}else{
					 businessProcess = new BusinessProcess();
					 businessProcess.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_NOT_AVALAILABLE_BALANCE.getCode());
					 //String descPaymentDate = CommonsUtilities.convertDatetoString(paymentDate);
					 String idSecurityCodePk = accountBalancePK.getIdSecurityCodePk();
					 Participant participant = null;
					 try {
						 participant = collectionRigthServiceBean.find(Participant.class, idParticipantPk);
					} catch (Exception e) {
						e.printStackTrace();
					}
					 String descParticipant = participant.getDescription();
					 Object[] parameters = {idSecurityCodePk, descParticipant};
					 notificationServiceFacade.sendNotification(processLogger.getIdUserAccount(), businessProcess, null, parameters);
				}
			}

		if(listSecuritiesToNotify == null||listSecuritiesToNotify.size()<1)
			return null;
		
		SecuritiesAlmostToExpireTO toExpireTO = null;
		HashMap<Long,PaymentAgent> mapContactPayment =  null;
		List<String> listSecuritiesNotHavePayingAgent = new  ArrayList<>();
		List<SecuritiesAlmostToExpireTO>  almostToExpireTOs = new ArrayList<>();
		for (CollectionEconomicRigth cer : listSecuritiesToNotify) {
			toExpireTO = new SecuritiesAlmostToExpireTO();
			toExpireTO.setIdParticipantPk(idParticipantPk);
			toExpireTO.setIdIssuerPk(idIssuerPk);
			toExpireTO.setSecurityClass(securityClass);
			//toExpireTO.setInstrumentType(instrumentType);
			toExpireTO.setCollectionEconomicRigth(cer);
			mapContactPayment = collectionRigthServiceBean.getListContactAgent(toExpireTO);
			if(mapContactPayment == null){
				listSecuritiesNotHavePayingAgent.add(toExpireTO.getCollectionEconomicRigth().getIdSecurityCodeFk());
				continue;
			}
			toExpireTO.setMapContactPaymentAgent(mapContactPayment);   
			almostToExpireTOs.add(toExpireTO);
		}
		/**valores que no tienen contacto y/o agente pagador*/
		if(listSecuritiesNotHavePayingAgent.size()>0){
			SecuritiesAlmostObjectTO objectTO = new SecuritiesAlmostObjectTO();
			objectTO.setListSecurities(listSecuritiesNotHavePayingAgent);
			return objectTO;
		}
		/**Registramos solicitud de CAT*/
		Long idAccreditationOperation = null;
		for (SecuritiesAlmostToExpireTO securityNotify : almostToExpireTOs) {
			idAccreditationOperation = 0L;
			/*CUI = Numero de cuenta*/
			initAccreditationOperation(securityNotify.getIdParticipantPk(), securityNotify.getCollectionEconomicRigth().getPaymentDate(),securityNotify.getCollectionEconomicRigth().getAlternateCode());
			buildCat(securityNotify.getCollectionEconomicRigth().getIdHolderAccountPk(), securityNotify.getCollectionEconomicRigth().getIdSecurityCodeFk());
			idAccreditationOperation = registreCertificate();
			if (idAccreditationOperation != 0) {
				log.info("CDDE ::: CAT REGISTRADO, ID: " + idAccreditationOperation + " VALOR : "+ securityNotify.getCollectionEconomicRigth().getIdSecurityCodeFk());
				/**Confirmando el CAT registrado*/
				changeAccreditationStatus();
				/**Asignando id para generar el reporte*/
				securityNotify.setIdAccreditationOperationPk(idAccreditationOperation);
			}
		}
		SecuritiesAlmostObjectTO objectTO = new SecuritiesAlmostObjectTO();
		objectTO.setAlmostToExpireTOs(almostToExpireTOs);
		return objectTO;
	}
}
