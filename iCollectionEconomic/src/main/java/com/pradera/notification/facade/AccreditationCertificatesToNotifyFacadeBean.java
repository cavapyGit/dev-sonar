package com.pradera.notification.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.CorrelativeServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDeliveryInformation;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationMarketFact;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.CatType;
import com.pradera.model.custody.type.CertificationType;
import com.pradera.model.generalparameter.Correlative;
import com.pradera.model.generalparameter.type.CorrelativeType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.notification.service.AccreditationCertificatesToNotifyServiceBean;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AccreditationCertificatesToNotifyFacadeBean {

	/** The bean service. */
	@Inject
	AccreditationCertificatesToNotifyServiceBean beanService;

	/** The executor component service bean. */
	@Inject
	Instance<AccountsComponentService> accountsComponentService;

	@Inject
	CorrelativeServiceBean correlativeService;

	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/** Confirm accreditation request.
	 * 
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception */
	@ProcessAuditLogger(process = BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM)
	public void confirmAccreditationRequest(AccreditationOperation accreditationOperationToChange, Integer accreditationState)
			throws ServiceException {
		Long accreditationOperationId = accreditationOperationToChange.getCustodyOperation().getIdCustodyOperationPk();
//		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
//		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());

		accreditationOperationToChange.setAccreditationState(accreditationState);

		Correlative objCorrelative = new Correlative();
		objCorrelative.setCorrelativeCd(CorrelativeType.CAT_CODE_COR.getValue());

		// llamada a singleton
		Correlative correlative = beanService.correlativeServiceBean(objCorrelative, CatType.CAT);
		// Integer actualCorrelative=correlative.getCorrelativeNumber()+1;
		Integer actualCorrelative = correlative.getCorrelativeNumber();

		accreditationOperationToChange.setCertificateNumber(actualCorrelative);
		/*** SETTING AUDIT ***/
		String strSecurityCodeCat = getSecurityCodeCat(accreditationOperationToChange, actualCorrelative);
		strSecurityCodeCat = strSecurityCodeCat + "-" + actualCorrelative.toString();
		if (Validations.validateIsNotNullAndNotEmpty(strSecurityCodeCat)) {
			accreditationOperationToChange.setSecurityCodeCat(strSecurityCodeCat);
		}

		beanService.update(accreditationOperationToChange);
		correlative.setCorrelativeNumber(actualCorrelative);
		correlativeService.update(correlative);

		List<HolderAccountBalanceTO> accountBalanceMixedTOs = this.getAccountBalanceTOsFromMixed(accreditationOperationId);

		if (accreditationOperationToChange.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())) {
			List<HolderAccountBalanceTO> accountBalanceTOs = this.getHolderAccountBalanceTOs(accreditationOperationId);
			accountsComponentService.get().executeAccountsComponent(
					this.getAccountsComponentTO(accreditationOperationId,
							BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode(), accountBalanceTOs,
							ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));
			CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
			//TODO 
			//custodyOperation.setConfirmUser(loggerUser.getUserName());
			custodyOperation.setConfirmUser("ROLY");
			custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
			custodyOperation.setState(AccreditationOperationStateType.CONFIRMED.getCode());
			this.beanService.update(custodyOperation);

		} else if (accreditationOperationToChange.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())) {
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationToChange
					.getIdAccreditationOperationPk());
			for (BlockOperation objBlockOperation : blockOperations) {
				objBlockOperation.setIndAccreditation(BooleanType.YES.getCode());
				this.beanService.updateBlockOperation(objBlockOperation);
			}
		} else {

			if (Validations.validateListIsNotNullAndNotEmpty(accountBalanceMixedTOs)) {
				// Calling component
				accountsComponentService.get().executeAccountsComponent(
						this.getAccountsComponentTO(accreditationOperationId,
								BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode(), accountBalanceMixedTOs,
								ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));
			}
			CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
			//TODO 
			//custodyOperation.setConfirmUser(loggerUser.getUserName());
			custodyOperation.setConfirmUser("ADMIN");
			custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
			custodyOperation.setState(AccreditationOperationStateType.CONFIRMED.getCode());
			this.beanService.update(custodyOperation);

			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationToChange
					.getIdAccreditationOperationPk());
			if (blockOperations != null) {
				for (BlockOperation objBlockOperation : blockOperations) {
					objBlockOperation.setIndAccreditation(BooleanType.YES.getCode());
					this.beanService.updateBlockOperation(objBlockOperation);
				}
			}

			Integer state = accreditationOperationToChange.getAccreditationState();
			List<HolderAccountOperation> lstHAO = searchHolderAccountOperationToUpdate(accreditationOperationToChange);
			if (lstHAO != null) {
				beanService.updateReportedOperation(lstHAO, state);
			}
		}

		// /*
		// * CALLING THE CLIENT WEB SERVICE
		// */
		//
		// if(!accreditationOperationToChange.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())
		// && Validations.validateListIsNotNullAndNotEmpty(accountBalanceMixedTOs)){
		//
		// for(AccreditationDetail detail :
		// beanService.searchAccreditationDetailByAccreditationOperation(accreditationOperationToChange.getIdAccreditationOperationPk())){
		// if(detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
		// detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
		// try {
		// custodyServiceConsumer.sendBlockAccreditationOperationWebClient(accreditationOperationToChange, loggerUser);
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// }
		// }

	}

	/** Search holder account operation to update.
	 * 
	 * @param objAO the obj ao
	 * @return the list
	 * @throws ServiceException the service exception */
	public List<HolderAccountOperation> searchHolderAccountOperationToUpdate(AccreditationOperation objAO)
			throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return this.beanService.searchHolderAccountOperationToUpdate(objAO, loggerUser);
	}

	/** * BATCH METHOD **.
	 * 
	 * @param idAccreditationPk the id accreditation pk
	 * @return the list
	 * @throws ServiceException the service exception */
	public List<BlockOperation> searchBlockOperations(Long idAccreditationPk) throws ServiceException {
		return this.beanService.searchBlockOperations(idAccreditationPk);
	}

	/** Gets the holder account balance t os.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception */
	public List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(Long accreditationOperationId) throws ServiceException {
		// accreditationDetail list Object
		List<AccreditationDetail> accreditationDetailList = this.beanService
				.searchAccreditationDetailByOperation(accreditationOperationId);

		if (Validations.validateListIsNullOrEmpty(accreditationDetailList)) {
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
		// HolderAccountBalanceTO list
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		for (AccreditationDetail acreditationDetail : accreditationDetailList) {
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(acreditationDetail.getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(acreditationDetail.getHolderAccount().getParticipant()
					.getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(acreditationDetail.getSecurity().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(acreditationDetail.getTotalBalance());
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for (AccreditationMarketFact accreditationMarketFact : acreditationDetail.getAccreditationMarketFactList()) {
				if (accreditationMarketFact.getIndActive().equals(BooleanType.YES.getCode())) {
					MarketFactAccountTO mark = new MarketFactAccountTO();
					mark.setMarketDate(accreditationMarketFact.getMarketDate());
					mark.setMarketRate(accreditationMarketFact.getMarketRate());
					mark.setMarketPrice(accreditationMarketFact.getMarketPrice());
					mark.setQuantity(accreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mark);
				}
			}
			accountBalanceTOs.add(holderAccountBalanceTO);
		}
		return accountBalanceTOs;
	}

	/** Gets the holder account balance t os.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception */
	public List<HolderAccountBalanceTO> getAccountBalanceTOsFromMixed(Long accreditationOperationId) throws ServiceException {
		List<AccreditationDetail> accreditationDetailList = this.beanService
				.searchAccreditationDetailByOperation(accreditationOperationId);
		// log.info("size accreditationDetailList: " + accreditationDetailList.size());
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		for (AccreditationDetail acreditationDetail : accreditationDetailList) {
			// log.info("acreditationDetail.getAvailableBalance(): acreditationDetail.getAvailableBalance()");
			if (acreditationDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0) {
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdHolderAccount(acreditationDetail.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(acreditationDetail.getHolderAccount().getParticipant()
						.getIdParticipantPk());
				holderAccountBalanceTO.setIdSecurityCode(acreditationDetail.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(acreditationDetail.getAvailableBalance());
				holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for (AccreditationMarketFact accreditationMarketFact : acreditationDetail.getAccreditationMarketFactList()) {
					MarketFactAccountTO mark = new MarketFactAccountTO();
					mark.setMarketDate(accreditationMarketFact.getMarketDate());
					mark.setMarketRate(accreditationMarketFact.getMarketRate());
					mark.setMarketPrice(accreditationMarketFact.getMarketPrice());
					mark.setQuantity(accreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mark);
				}
				accountBalanceTOs.add(holderAccountBalanceTO);
			}
		}
		return accountBalanceTOs;
	}

	/** Gets the security code cat.
	 * 
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param intCertificateNumber the certificate number
	 * @return the security code cat */
	private String getSecurityCodeCat(AccreditationOperation accreditationOperationToChange, Integer intCertificateNumber) {
		String strSecurityCodeCat = GeneralConstants.EMPTY_STRING;
		AccreditationOperation objAccreditationOperation = beanService.find(AccreditationOperation.class,
				accreditationOperationToChange.getIdAccreditationOperationPk());
		// Generate security code cat (11)
		String documentNumber = objAccreditationOperation.getAccreditationDetails().get(0).getHolder().getDocumentNumber();
		Integer accountNumber = objAccreditationOperation.getAccreditationDetails().get(0).getHolderAccount()
				.getAccountNumber();
		String idSecurityPk = objAccreditationOperation.getAccreditationDetails().get(0).getSecurity().getIdSecurityCodePk();
		try {
			String certicateNumber11 = CommonsUtilities.obtenerModulo11(intCertificateNumber.toString());
			String documentNumber11 = CommonsUtilities.obtenerModulo11(documentNumber);
			String accountNumber11 = CommonsUtilities.obtenerModulo11(accountNumber.toString());
			String securityPk11 = CommonsUtilities.obtenerModulo11(idSecurityPk);
			String strIssuanceDate11 = CommonsUtilities.obtenerModulo11(objAccreditationOperation.getIssuanceDate());
			strSecurityCodeCat = certicateNumber11 + documentNumber11 + accountNumber11 + securityPk11 + strIssuanceDate11;
			String globalCode = CommonsUtilities.obtenerModulo11(strSecurityCodeCat);
			strSecurityCodeCat = strSecurityCodeCat + globalCode;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strSecurityCodeCat;
	}

	/** Find accreditation detail mixed.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception */
	public List<AccreditationDetail> findAccreditationDetailMixed(Long accreditationOperationId) throws ServiceException {
		return beanService.searchhAccreditationDetailMixed(accreditationOperationId);
	}

	/** Find accreditation operation.
	 * 
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation
	 * @throws ServiceException the service exception */
	public AccreditationOperation findAccreditationOperation(Long accreditationOperationId) throws ServiceException {
		return beanService.findAccreditationOperation(accreditationOperationId);
	}

	/** Accreditation available consult.
	 * 
	 * @param accreditationOperation the accreditation operation
	 * @param initialDate the initial date
	 * @param endDate the end date
	 * @param currency the currency
	 * @param securityClass the security class
	 * @param issuer the issuer
	 * @return the list
	 * @throws ServiceException the service exception */
	 @ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_QUERY)
	public List<AccreditationOperation> accreditationAvailableConsult(AccreditationOperation accreditationOperation,
			Date initialDate, Date endDate, Integer currency, Integer securityClass, String issuer) throws ServiceException {
		// LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		// loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<AccreditationOperation> listAccreditationOperation = new ArrayList<AccreditationOperation>();
		listAccreditationOperation = beanService.accreditationAvailableConsult(accreditationOperation, initialDate, endDate,
				currency, securityClass, issuer);
		for (int i = 0; i < listAccreditationOperation.size(); i++) {
			listAccreditationOperation.get(i).getCustodyOperation().getOperationNumber();
			listAccreditationOperation.get(i).getAccreditationDetails().size();
			if (listAccreditationOperation.get(i).getListAccreditationDeliveryInformation() != null) {
				listAccreditationOperation.get(i).getListAccreditationDeliveryInformation().size();
			}
			if (listAccreditationOperation.get(i).getParticipantPetitioner() != null) {
				listAccreditationOperation.get(i).getParticipantPetitioner().getIdParticipantPk();
			}
			if (listAccreditationOperation.get(i).getHolder() != null) {
				listAccreditationOperation.get(i).getHolder().getIdHolderPk();
			}
			listAccreditationOperation.get(i).setFormatSecurities(
					listAccreditationOperation.get(i).getAccreditationDetails().get(0).getSecurity().getIdSecurityCodePk()
							.toString());
		}
		return listAccreditationOperation;
	}

	/** Search holder account balance.
	 * 
	 * @param idHolderAccountPk
	 * @param security
	 * @return
	 * @throws ServiceException */
	public List<HolderAccountBalance> searchHolderAccountBalance(Long idHolderAccountPk, String security)
			throws ServiceException {
		List<HolderAccountBalance> list = beanService.searchHolderAccountBalance(idHolderAccountPk, security);
		return list;
	}

	/** Register new Reported Balance request.
	 * 
	 * @param accreditationOperation the accreditation operation
	 * @return String
	 * @throws ServiceException the service exception */
	@ProcessAuditLogger(process = BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER)
	public AccreditationOperation insertReported(AccreditationOperation accreditationOperation) throws ServiceException {
		// LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		// loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		// VALIDATING Holder And Participant
		// this.validationsBeforeSave(accreditationOperation);
		// Calling operation Number
		Long operationNumber = beanService
				.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_ACCREDITATION_CERTIFICATE);
		accreditationOperation.getCustodyOperation().setOperationNumber(operationNumber);

		if (Validations.validateListIsNotNullAndNotEmpty(accreditationOperation.getListAccreditationDeliveryInformation())) {
			for (AccreditationDeliveryInformation objDeliveryInfo : accreditationOperation
					.getListAccreditationDeliveryInformation()) {
				objDeliveryInfo.setIdAccreditationDeliveryPk(null);
			}
		}

		List<AccreditationDetail> accreditationDetailList = accreditationOperation.getAccreditationDetails();
		if (Validations.validateListIsNotNullAndNotEmpty(accreditationDetailList)) {
			if (accreditationDetailList.get(0).getAccreditationReported() != null) {
				for (int i = 0; i < accreditationDetailList.size(); i++) {
					accreditationDetailList.get(0).getAccreditationReported().get(0).setIdReportAccreditationPk(null);
				}
			}
			/* if (accreditationDetailList.get(0).getBlockOperationCertifications()!=null){ for (int i = 0; i <
			 * accreditationDetailList.size(); i++) {
			 * accreditationDetailList.get(0).getBlockOperationCertifications().get(0).setBlockOperationCertPk(null); } } */
		}

		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		for (AccreditationDetail objAccreditationDetail : accreditationDetailList) {
			if (!objAccreditationDetail.getAvailableBalance().equals(BigDecimal.ZERO)) {
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdHolderAccount(objAccreditationDetail.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(objAccreditationDetail.getHolderAccount().getParticipant()
						.getIdParticipantPk());
				holderAccountBalanceTO.setIdSecurityCode(objAccreditationDetail.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(objAccreditationDetail.getAvailableBalance());
				holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for (AccreditationMarketFact objAccreditationMarketFact : objAccreditationDetail
						.getAccreditationMarketFactList()) {
					MarketFactAccountTO mfaTO = new MarketFactAccountTO();
					mfaTO.setMarketPrice(objAccreditationMarketFact.getMarketPrice());
					mfaTO.setMarketDate(objAccreditationMarketFact.getMarketDate());
					mfaTO.setMarketRate(objAccreditationMarketFact.getMarketRate());
					mfaTO.setQuantity(objAccreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mfaTO);
				}
				accountBalanceTOs.add(holderAccountBalanceTO);
			}
		}

		if (accreditationOperation.getExpirationDate() != null) {
			Date realExpirationDateWithTime = CommonsUtilities.setTimeToDate(accreditationOperation.getExpirationDate(),
					CommonsUtilities.currentDateTime());
			accreditationOperation.setRealExpirationDate(realExpirationDateWithTime);
		}

		AccreditationOperation accOperationSaved = beanService.create(accreditationOperation);
		if (accountBalanceTOs.size() > 0) {
			//Long idCat=this.getAccountsComponentTO(accreditationOperation.getIdAccreditationOperationPk());
			AccountsComponentTO accountsComponentTO=getAccountsComponentTO(accreditationOperation.getIdAccreditationOperationPk(),
					BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER.getCode(), accountBalanceTOs,
					ParameterOperationType.SECURITIES_ACCREDITATION.getCode());
			accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
		}
		return accOperationSaved;
	}

	/** Gets the accounts component to.
	 * 
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to */
	public AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType,
			List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType) {
		// ComponentTO Object
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();

		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(1);
		return objAccountComponent;
	}
}
