package com.pradera.notification.facade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import com.pradera.collection.right.to.ResultUrlAccusePaymentTO;
import com.pradera.collection.right.to.ResultoWsNotification;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.integration.exception.ServiceException;
import com.pradera.notification.service.ElectronicCatWebServiceBean;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ElectronicCatWebFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private PraderaLogger praderaLogger;

	@Inject
	ElectronicCatWebServiceBean electronicCatWebServiceBean;

	/** metodo para obtener informacion de las glosas que fueron notificadas por concepto de cobro de derechos economicos desde
	 * el CAT Electronico
	 * 
	 * @throws ServiceException */
	public List<ResultoWsNotification> getInfoNotificationCollectionEconomic() throws ServiceException {
		List<ResultoWsNotification> lstResultoWsNotification = null;
		try {
			lstResultoWsNotification = electronicCatWebServiceBean.getInfoNotificationCollectionEconomic();
			praderaLogger.info(":::: CONFIRMACION EXITOSA DE RECEPCION DE GLOSA COBRO DE DERECHOS ECONOMICOS DESDE EL CAT ELECTRONICO ::::" + new Date().toString());
		} catch (EJBException e) {
			praderaLogger.error(e.getMessage());
			praderaLogger.error(":::: ERROR AL LEER DATOS DE GLOSA DE COBRO DE DERECHOS EN EL CAT ELECTRONICO ::::" + new Date().toString());
		} catch (Exception e) {
			praderaLogger.error(e.getMessage());
			praderaLogger.error(":::: ERROR DESCONOCIDO AL LEER DATOS DE GLOSA DE COBRO DE DERECHOS EN EL CAT ELECTRONICO ::::" + new Date().toString());
		}
		return lstResultoWsNotification;
	}
	
	/** metodo para obtener informacion de las glosas que fueron pagadas por concepto de cobro de derechos economicos desde el
	 * CAT electronico
	 * 
	 * @return
	 * @throws ServiceException */
	public List<ResultUrlAccusePaymentTO> getInfoPaymentCollectionEconomic() throws ServiceException {
		List<ResultUrlAccusePaymentTO> lstResultoWsNotification = null;
		try {
			lstResultoWsNotification = electronicCatWebServiceBean.getInfoPaymentCollectionEconomic();
			praderaLogger.info(":::: DATOS PARA EL DEPOSITO MANUAL EXTRAIDOS CON EXITO ::::" + new Date().toString());
		} catch (EJBException e) {
			praderaLogger.error(e.getMessage());
			praderaLogger.error(":::: ERROR AL LEER DATOS DE GLOSA DE COBRO DE DERECHOS EN EL CAT ELECTRONICO (method: getInfoPaymentCollectionEconomic) ::::"
					+ new Date().toString());
		} catch (Exception e) {
			praderaLogger.error(e.getMessage());
			praderaLogger.error(":::: ERROR DESCONOCIDO AL LEER DATOS DE GLOSA DE COBRO DE DERECHOS EN EL CAT ELECTRONICO (method: getInfoPaymentCollectionEconomic) ::::"
					+ new Date().toString());
		}
		return lstResultoWsNotification;
	}
}