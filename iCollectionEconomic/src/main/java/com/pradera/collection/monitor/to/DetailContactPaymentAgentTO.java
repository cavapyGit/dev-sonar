package com.pradera.collection.monitor.to;

import java.io.Serializable;

public class DetailContactPaymentAgentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 362636035682974315L;
	
	private Long idContanctPaymnetAgent;
	private String fullName;
	private String emailContact;
	private Integer notificationReSent;
	
	//constructor
	public DetailContactPaymentAgentTO(){}

	public Long getIdContanctPaymnetAgent() {
		return idContanctPaymnetAgent;
	}

	public void setIdContanctPaymnetAgent(Long idContanctPaymnetAgent) {
		this.idContanctPaymnetAgent = idContanctPaymnetAgent;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public Integer getNotificationReSent() {
		return notificationReSent;
	}

	public void setNotificationReSent(Integer notificationReSent) {
		this.notificationReSent = notificationReSent;
	}
	
	

}
