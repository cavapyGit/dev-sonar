package com.pradera.collection.monitor.view;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.ResultMonitorSearchTO;
import com.pradera.collection.monitor.to.SearchFilterMonitorTO;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;

@DepositaryWebBean
@LoggerCreateBean
public class SearchCollectionMonitorMgmtBean extends GenericBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Participant> listParticipants;
	/**Parametros de busqueda de la pantalla*/
	private SearchFilterMonitorTO searchFilterMonitorTO;
	
	private List<ParameterTable> lstCurrency;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	private GenericDataModel<ResultMonitorSearchTO> listResultMonitorTOs;
	
	private ResultMonitorSearchTO monitorTOviewDetaiil;
	
	private boolean visibleResult;
	
	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;
	
	@Inject
	private UserInfo userInfo;
	
	private BigDecimal totalAmountReceivableBOB;
	private BigDecimal totalAmountReceivableUSD;
    private BigDecimal totalAmountReceived;
    private BigDecimal difference;
    
	
	@PostConstruct
	public void init(){
		listParticipants = parameterServiceBean.getListParticipantForCollection();
		//listParticipants = participantServiceBean.getLisParticipantServiceBean();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterMonitorTO = new SearchFilterMonitorTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterMonitorTO = new SearchFilterMonitorTO();
		//lstCurrency = parameterServiceBean.getListParameterTableServiceBean(53);
		lstCurrency = collectionEconomicFacade.listCurrencyForCollection();
	}
	
	@LoggerAuditWeb	
	public void searchFromFilters(){
		/*Busqueda de valores vencen hoy y su cobro deberia ser hoy, cn*/
		listResultMonitorTOs = collectionEconomicFacade.searchExpirationClassifiedByGlossToMonitorCollection(searchFilterMonitorTO);
		
		totalAmountReceivableBOB =  BigDecimal.ZERO;
		totalAmountReceivableUSD =  BigDecimal.ZERO;
		totalAmountReceived =  BigDecimal.ZERO;
		difference =  BigDecimal.ZERO;
		/**Sumando montos totales en moneda BOB*/
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.PYG.getCode()))
		for (ResultMonitorSearchTO monitorSearchTO : listResultMonitorTOs) {
			totalAmountReceivableBOB = totalAmountReceivableBOB.add(monitorSearchTO.getMonitorTO().getAmountReceivable());
			totalAmountReceived = totalAmountReceived.add(monitorSearchTO.getMonitorTO().getAmountDeposited());
		}
		/**Sumando montos totales en moneda USD*/
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.USD.getCode()))
		for (ResultMonitorSearchTO monitorSearchTO : listResultMonitorTOs) {
			totalAmountReceivableUSD = totalAmountReceivableUSD.add(monitorSearchTO.getMonitorTO().getAmountReceivable());
			totalAmountReceived = totalAmountReceived.add(monitorSearchTO.getMonitorTO().getAmountDeposited());
		}
		
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.PYG.getCode()))
			difference = totalAmountReceivableBOB.subtract(totalAmountReceived);
		
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.USD.getCode()))
			difference = totalAmountReceivableUSD.subtract(totalAmountReceived);
		
	}
	
	public void loadDetailCollectionMovements(ActionEvent event){
		monitorTOviewDetaiil = new ResultMonitorSearchTO();
		monitorTOviewDetaiil = (ResultMonitorSearchTO) event.getComponent().getAttributes().get("resultMonitorTOView");
		visibleResult = true;
		
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVDetailCollectionForGlosss').show();");
	}
	/**
	 * Verificamos los cambios modificados en los filtros de busqueda
	 * Si no hubiese existido cambios en los filtros, se limpia la pantalla de busqueda
	 * **/
	public String clerearFilters(){
		if(isDataAdded()){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wVCleanSearchCollectionMonitor').show();");
	    	return null;
		}
		else
			runCleaning();
		return null;
	}	
	
	private boolean isDataAdded(){
		if(!userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||!userInfo.getUserAccountSession().isParticipantInstitucion())
		if(searchFilterMonitorTO.getIdParticipantPk()!=null)
			return true;
		if(searchFilterMonitorTO.getIssuer().getIdIssuerPk()!=null)
			return true;
		if(searchFilterMonitorTO.getCurrency()!=null)
			return true;
		else 
			return false;
	}
	
	public void runCleaning(){
		listResultMonitorTOs = new GenericDataModel<>();
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				||userInfo.getUserAccountSession().isParticipantInstitucion())
			searchFilterMonitorTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterMonitorTO = new SearchFilterMonitorTO();	
	    JSFUtilities.updateComponent("frmSearchMonitorCollection");
	}
	public List<Participant> getListParticipants() {
		return listParticipants;
	}
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}
	public SearchFilterMonitorTO getSearchFilterMonitorTO() {
		return searchFilterMonitorTO;
	}
	public void setSearchFilterMonitorTO(SearchFilterMonitorTO searchFilterMonitorTO) {
		this.searchFilterMonitorTO = searchFilterMonitorTO;
	}
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public ParameterServiceBean getParameterServiceBean() {
		return parameterServiceBean;
	}
	public void setParameterServiceBean(ParameterServiceBean parameterServiceBean) {
		this.parameterServiceBean = parameterServiceBean;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}


	public BigDecimal getTotalAmountReceivableBOB() {
		return totalAmountReceivableBOB;
	}

	public void setTotalAmountReceivableBOB(BigDecimal totalAmountReceivableBOB) {
		this.totalAmountReceivableBOB = totalAmountReceivableBOB;
	}

	public BigDecimal getTotalAmountReceivableUSD() {
		return totalAmountReceivableUSD;
	}

	public void setTotalAmountReceivableUSD(BigDecimal totalAmountReceivableUSD) {
		this.totalAmountReceivableUSD = totalAmountReceivableUSD;
	}

	public BigDecimal getTotalAmountReceived() {
		return totalAmountReceived;
	}

	public void setTotalAmountReceived(BigDecimal totalAmountReceived) {
		this.totalAmountReceived = totalAmountReceived;
	}

	public GenericDataModel<ResultMonitorSearchTO> getListResultMonitorTOs() {
		return listResultMonitorTOs;
	}

	public void setListResultMonitorTOs(
			GenericDataModel<ResultMonitorSearchTO> listResultMonitorTOs) {
		this.listResultMonitorTOs = listResultMonitorTOs;
	}

	public ResultMonitorSearchTO getMonitorTOviewDetaiil() {
		return monitorTOviewDetaiil;
	}

	public void setMonitorTOviewDetaiil(ResultMonitorSearchTO monitorTOviewDetaiil) {
		this.monitorTOviewDetaiil = monitorTOviewDetaiil;
	}

	public BigDecimal getDifference() {
		return difference;
	}

	public void setDifference(BigDecimal difference) {
		this.difference = difference;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
}
