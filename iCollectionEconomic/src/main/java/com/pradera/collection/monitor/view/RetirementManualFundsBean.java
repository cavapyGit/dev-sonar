package com.pradera.collection.monitor.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.bank.account.service.BankAccountServiceBean;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.ManualDepositTO;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.RetirementStateType;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;

@DepositaryWebBean
@LoggerCreateBean
public class RetirementManualFundsBean extends GenericBaseBean {

       /**
       * 
        */
       private static final long serialVersionUID = -8443666423562427891L;

       private ManualDepositTO searchRetirementTO, registreRetirementTO, viewhRetirementTO;
       
       private DetailCollectionGloss detailCollectionGlossSelected;

       @Inject
       private ParameterServiceBean parameterServiceBean;

       @Inject
       private BankAccountServiceBean accountServiceBean;
       
       @Inject
       private UserPrivilege userPrivilege;
       
       @Inject
   	   private PraderaLogger log;

       @Inject
       private MonitoriCollectionEconomicFacade collectionEconomicFacade;

       private GenericDataModel<DetailCollectionGloss> listDetailCollectionGloss;
       
       @PostConstruct
       public void init() {
             searchRetirementTO = new ManualDepositTO();
             List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
             searchRetirementTO.setListParticipant(listParticipant);
             List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
             searchRetirementTO.setListCurrency(listCurrency);
             searchRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
             List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),null,searchRetirementTO.getDepositStatus());
             searchRetirementTO.setListGloss(listGloss);
             List<Bank> listBanks = accountServiceBean.getListAllBank();
             searchRetirementTO.setListBanks(listBanks);
             searchRetirementTO.setDepositStatus(null);
             listDetailCollectionGloss = null;
       }
       /**
        * Metodo para regresar a la pagina de principal
        * */
       public String backViewMainPageRegiry(){
          registreRetirementTO = new ManualDepositTO();
           List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
           registreRetirementTO.setListParticipant(listParticipant);
           List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
           registreRetirementTO.setListCurrency(listCurrency);
           registreRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
           List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),null,null,null);
           registreRetirementTO.setListGloss(listGloss);
           List<Bank> listBanks = accountServiceBean.getListAllBank();
           registreRetirementTO.setListBanks(listBanks);
           return "backRegisterRetirementManual";
       }
       
       /**
       * Cargamos la pagina de registro
       * 
       */
       public String loadPageRegistreDeposit() {
             registreRetirementTO = new ManualDepositTO();
             List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
             registreRetirementTO.setListParticipant(listParticipant);
             List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
             registreRetirementTO.setListCurrency(listCurrency);
             registreRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
             List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),null,null,null);
             registreRetirementTO.setListGloss(listGloss);
             List<Bank> listBanks = accountServiceBean.getListAllBank();
             registreRetirementTO.setListBanks(listBanks);
              return "pageRegisterRetirementManual";
       }
       /**
       * Registrando el desblqueo de la cuenta bancaria
       * */
       @LoggerAuditWeb
       public StreamedContent getStreamedContent() throws IOException{
             byte[] fileContent = registreRetirementTO.getAuxDepositFile();
             return getStreamedContentFromFile(fileContent, null, registreRetirementTO.getAuxDepositFileName());
       }
       /**
       * *Guaradamos el registro de deposito
       * */
       public void beforeSaveRetirement() {
             if(existDataInView(registreRetirementTO)){
                    /**Ejeutar dialogo para conffirmar el deposito*/
                    JSFUtilities.executeJavascriptFunction("PF('wVsaveRegistryDeposit').show();");
                    JSFUtilities.updateComponent("opnlDialogs");
             }else{
                    JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
                    JSFUtilities.putViewMap("headerMessageView","Existen datos obligatorios para el registro.");
                    JSFUtilities.showSimpleValidationDialog();
             }
       }
       
       /**Metodo para guardar el registro de retiro*/
       public synchronized void execuetSave(){
             RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
             retirementObjetTO.setGloss(registreRetirementTO.getGloss());
             retirementObjetTO.setIdParticipantPk(registreRetirementTO.getIdParticipantPk());
             retirementObjetTO.setState(null);
             List<DetailCollectionGloss> listCollectionGlosses = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
             DetailCollectionGloss collectionGloss = listCollectionGlosses.get(0);
             
             collectionGloss.setAmountTransferred(registreRetirementTO.getAmountRetirement());
             collectionGloss.setIdBankPk(registreRetirementTO.getIdBankPk());
             collectionGloss.setRetirementDate(CommonsUtilities.currentDate());
             
             try {
                          collectionEconomicFacade.saveManualRetirement(collectionGloss);
                          registreRetirementTO = new  ManualDepositTO();
      
                           JSFUtilities.updateComponent("opnlDialogs");
                          JSFUtilities.executeJavascriptFunction("PF('wvDialogResult').show();");
      
                    } catch (Exception e) {
                          String msg = PropertiesUtilities.getMessage("msg.result.error");
                          showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msg);
                          JSFUtilities.showSimpleValidationDialog();
                   }
       }

       /**
       * Buscamos todos los retiros
       */
       public void beforeSearch() {
             log.info("CDE::: Buscando registros de notificaciones deposito/retiro");
             RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
             retirementObjetTO.setPaymentDate(searchRetirementTO.getRegistreDate());
             if(searchRetirementTO.getIdParticipantPk()==null)
            	 return;
             retirementObjetTO.setIdParticipantPk(searchRetirementTO.getIdParticipantPk());
             if(searchRetirementTO.getGloss()!=null)
             retirementObjetTO.setGloss(searchRetirementTO.getGloss());
             if(searchRetirementTO.getRetirementtStatus()!=null)
            	 retirementObjetTO.setStateRetirement(searchRetirementTO.getRetirementtStatus());
             else
            	 retirementObjetTO.setStateRetirement(Integer.valueOf(0));
             
             List<DetailCollectionGloss> listCollectionGlosses = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);

             listDetailCollectionGloss = new GenericDataModel<>(listCollectionGlosses);

       }
       
       public String loadPageSearch(){
    	   beforeSearch();
    	   return "searchRetiremnetFunds";
       }
       
       /** metodo para retornar la moneda segun corresponda
       * 
        * @param currencyCode
       * @return */
       public String getTextCurrency(Integer currencyCode) {
             if (currencyCode.equals(CurrencyType.PYG.getCode()))
                    return CurrencyType.PYG.getCodeIso();
             if (currencyCode.equals(CurrencyType.USD.getCode()))
                    return CurrencyType.USD.getCodeIso();
             if (currencyCode.equals(CurrencyType.UFV.getCode()))
                    return CurrencyType.UFV.getCodeIso();
             if (currencyCode.equals(CurrencyType.EU.getCode()))
                    return CurrencyType.EU.getCodeIso();
             if (currencyCode.equals(CurrencyType.DMV.getCode()))
                    return CurrencyType.DMV.getCodeIso();
             return "-";
       }
       
       /** metodo para retornar el estado de un deposito segun corresponda
       * 
        * @param status
       * @return */
       public String getTestStatusRetirement(Integer status) {
             if (status.equals(RetirementStateType.REGISTERED.getCode()))
                    return RetirementStateType.REGISTERED.getValue();
             if (status.equals(RetirementStateType.REJECTED.getCode()))
                    return RetirementStateType.REJECTED.getValue();
             if (status.equals(RetirementStateType.CONFIRMED.getCode()))
                    return RetirementStateType.CONFIRMED.getValue();
             return "-";
       }

       /**
        * Buscando glosas de acuerdo al participante*/
       public void onChangeParticipant(){
          if(searchRetirementTO.getIdParticipantPk()!=null){
                searchRetirementTO.setIdBankPk(null);
                searchRetirementTO.setBcbCode(null);
                searchRetirementTO.setCurrency(null);
                searchRetirementTO.setGloss(null);
                searchRetirementTO.setDepositStatus(null);
                searchRetirementTO.setAmountDeposited(null);
                searchRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
                try {
                       List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),searchRetirementTO.getIdParticipantPk(),null,searchRetirementTO.getRetirementtStatus());
                   searchRetirementTO.setListGloss(listGloss);
                    } catch (Exception e) {
                           e.printStackTrace();
                    }
         }
       }
       
       /** metodo para para construir un dialog con su cabecera y body como prametros
       * 
        * @param headerMessageConfirmation
       * @param bodyMessageConfirmation */
       private void showMessageOnDialogDepositFunds(String headerMessageConfirmation, String bodyMessageConfirmation) {
             JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
             JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
       }
       /** metodo para verificar si se selecciono un registro antes de confirma o rechazar un deposito
       * 
        * @return true si selecciono un registro, false si no selecciono nada */
       private boolean isSelectedRow() {
             if (Validations.validateIsNotNull(detailCollectionGlossSelected)) {
                    if (Validations.validateIsNull(detailCollectionGlossSelected.getIdDetailCollectionGlossPk())) {
                           String msg = PropertiesUtilities.getMessage("lbl.message.alert.no.select");
                           showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), msg);
                           JSFUtilities.showSimpleValidationDialog();
                    } else {
                           return true;
                    }
             } else {
                    String msg = PropertiesUtilities.getMessage("lbl.message.alert.no.select");
                    showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), msg);
                    JSFUtilities.showSimpleValidationDialog();
             }
             return false;
       }
       
       /**
       * metodo para llenar la data necesaria para el view de deposito de fondos
       * @param detailCollectionGloss
       */
       private void fillDataForViewDeposit(DetailCollectionGloss detailCollectionGloss) {
             // este es el objeto que vamos a mostrar en el view
             registreRetirementTO = new ManualDepositTO();
             List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
             registreRetirementTO.setListParticipant(listParticipant);
             List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
             registreRetirementTO.setListCurrency(listCurrency);
             List<String> listGloss = collectionEconomicFacade
                           .getListGlossCollection(detailCollectionGloss.getPaymentDate(), detailCollectionGloss.getIdParticipantPk(), detailCollectionGloss.getDepositStatus(), null);
             registreRetirementTO.setListGloss(listGloss);
             List<Bank> listBanks = accountServiceBean.getListAllBank();
             if(detailCollectionGloss.getIdBankPk()!=null){
             Bank bank = parameterServiceBean.find(Bank.class, detailCollectionGloss.getIdBankPk());
                 if (bank.getIdBcbCode() != null)
                        registreRetirementTO.setBcbCode(bank.getIdBcbCode().toString());
                 else
                        registreRetirementTO.setBcbCode("-");
                 registreRetirementTO.setListBanks(listBanks);
             }
             registreRetirementTO.setIdParticipantPk(detailCollectionGloss.getIdParticipantPk());
             registreRetirementTO.setIdBankPk(detailCollectionGloss.getIdBankPk());
             registreRetirementTO.setRegistreDate(detailCollectionGloss.getPaymentDate());
             registreRetirementTO.setCurrency(detailCollectionGloss.getCurrency());
             registreRetirementTO.setAmountRetirement(detailCollectionGloss.getAmountTransferred());
             registreRetirementTO.setAuxDepositFile(detailCollectionGloss.getFileAux());
             registreRetirementTO.setAuxDepositFileName(detailCollectionGloss.getFileAuxName());
             registreRetirementTO.setGloss(detailCollectionGloss.getGlosaNotClassified());
       }
       
       public void beforeSaveReject(){
             JSFUtilities.executeJavascriptFunction("PF('wvDialogRejectRetirement').show();");
        JSFUtilities.updateComponent("opnlDialogs");
       }
       public void beforeSaveConfirm(){
             JSFUtilities.executeJavascriptFunction("PF('wvDialogConfirmRetirement').show();");
        JSFUtilities.updateComponent("opnlDialogs");
       }
       
       // TODO botones de rechazo y confirmacion
       /** metodo para cambiar el estado del deposito a rechazado */
       public void saveRejectRetirement() {
               detailCollectionGlossSelected.setTransferStatus(null);
               detailCollectionGlossSelected.setAmountTransferred(BigDecimal.ZERO);
               detailCollectionGlossSelected.setRetirementDate(null);
             try {
                    collectionEconomicFacade.saveChangeStatusDeposit(detailCollectionGlossSelected);
                    beforeSearch();
                    JSFUtilities.updateComponent("opnlDialogs");
                    JSFUtilities.executeJavascriptFunction("PF('wvDialogResultReject').show();");
             } catch (ServiceException e) {
                    String msg = PropertiesUtilities.getMessage("msg.result.error");
                    showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msg);
                    JSFUtilities.showSimpleValidationDialog();
             }
       }
       
       /** metodo para cambiar el estado del deposito a confirm */
       public void saveConfirmRetirement(){
            detailCollectionGlossSelected.setTransferStatus(RetirementStateType.CONFIRMED.getCode());
             try {
                    collectionEconomicFacade.saveChangeStatusDeposit(detailCollectionGlossSelected);
                    beforeSearch();
                    
                    JSFUtilities.updateComponent("opnlDialogs");
                    JSFUtilities.executeJavascriptFunction("PF('wvDialogResult').show();");
                    
             } catch (ServiceException e) {
                    String msg = PropertiesUtilities.getMessage("msg.result.error");
                    showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msg);
                    JSFUtilities.showSimpleValidationDialog();
             }
       }
       
       /** metodo antes de ir a la pantalla de rechazo
       * 
        * @return */
       public String beforeReject() {

             // validando si se ha seleccionado un registro
             if (!isSelectedRow())
                    return null;

             // validando si el registro esta en estado diferente de registrado
             if (!detailCollectionGlossSelected.getTransferStatus().equals(RetirementStateType.REGISTERED.getCode())) {
                    String msg = PropertiesUtilities.getMessage("msg.validation.required.state.register");
                    showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), msg);
                    JSFUtilities.showSimpleValidationDialog();
                    return null;
             }

             fillDataForViewDeposit(detailCollectionGlossSelected);

             // mostrando solo el boton de rechazo
             registreRetirementTO.setBtnReject(true);

             return "viewRetirementFunds";
       }

       /** metodo antes de ir a la pantalla de confirmacion
       * 
        * @return */
       public String beforeConfirm() {
             // validando si se ha seleccionado un registro
             if (!isSelectedRow())
                    return null;

             // validando si el registro esta en estado diferente de registrado
             if (!detailCollectionGlossSelected.getTransferStatus().equals(RetirementStateType.REGISTERED.getCode())) {
                    String msg = PropertiesUtilities.getMessage("msg.validation.required.state.register");
                    showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), msg);
                    JSFUtilities.showSimpleValidationDialog();
                    return null;
             }

             fillDataForViewDeposit(detailCollectionGlossSelected);

             // mostrando solo el boton de confirmacion
             registreRetirementTO.setBtnConfirm(true);

             return "viewRetirementFunds";
       }

       /** metodo para cargar los datos para el view de deposistos
       * 
        * @param event */
       public void loadDetailDepositFunds(ActionEvent event) {
             // este objeto enviara los parametros a la query
             DetailCollectionGloss detailCollGloss = new DetailCollectionGloss();
             detailCollGloss = (DetailCollectionGloss) event.getComponent().getAttributes().get("resultDepositFundsView");
             fillDataForViewDeposit(detailCollGloss);
             // mostrando solo el boton de view
             registreRetirementTO.setBtnView(true);
       }
       
       /** metodo para descargar el archivo adjunto
       * 
        * @return
       * @throws IOException */
       public StreamedContent getFileStreamContent() throws IOException {
             if (registreRetirementTO.getAuxDepositFile() != null) {
                    InputStream is = new ByteArrayInputStream(registreRetirementTO.getAuxDepositFile());
                    StreamedContent strContent = new DefaultStreamedContent(is, null, registreRetirementTO.getAuxDepositFileName());
                    is.close();
                    return strContent;
             } else
                    return null;
       }

       /** Buscamos el detalle de la notificaion */
       public void onChangeGloss() {
             RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
             retirementObjetTO.setGloss(searchRetirementTO.getGloss());
             retirementObjetTO.setStateRetirement(searchRetirementTO.getRetirementtStatus());
             List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
             if (collectionGlossList == null|| collectionGlossList.isEmpty()) {
                    JSFUtilities.putViewMap("headerMessageView", "!Alerta!");
                    JSFUtilities.putViewMap("bodyMessageView","No se encontraron registros.");
                    JSFUtilities.showSimpleValidationDialog();
             } else {
                   DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
                    searchRetirementTO.setCurrency(collectionGloss.getCurrency());
                    searchRetirementTO.setAmountDeposited(collectionGloss
                                  .getAmountReceivable());
             }
       }
       /** Buscamos el detalle de la notificaion */
       public void onChangeGlossRegistry() {
             RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
             retirementObjetTO.setGloss(registreRetirementTO.getGloss());
             retirementObjetTO.setState(null);
             List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
             DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
             if (collectionGloss == null) {
                    JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
                    JSFUtilities.putViewMap("headerMessageView","No se encontro registro en estado registrado.");
                    JSFUtilities.showSimpleValidationDialog();
             } else {
                    registreRetirementTO.setCurrency(collectionGloss.getCurrency());
                    registreRetirementTO.setAmountRetirement(collectionGloss.getAmountReceivable());
                    CollectionEconomicRigth  collectionEconomicRigth = collectionEconomicFacade.searchCollectiionEconomicByGloss(null, collectionGloss.getGlosaNotClassified());
                    registreRetirementTO.setIdBankPk(collectionEconomicRigth.getIdBankFk());
             }
       }

       /**
       * Cambio de banco, se debe actualizar el BCB CODE
       **/
       public void onChangeBank() {
             if (searchRetirementTO.getIdBankPk() != null) {
                    Bank bank = parameterServiceBean.find(Bank.class,
                                  searchRetirementTO.getIdBankPk());
                    if (bank.getIdBcbCode() != null){
                    searchRetirementTO.setBcbCode(bank.getIdBcbCode().toString());
                    onChangeOperationDate();
                    }else {
                           JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
                           JSFUtilities.putViewMap("headerMessageView","El Banco seleccionado no cuenta con codigo BIC");
                           JSFUtilities.showSimpleValidationDialog();
                           searchRetirementTO.setIdBankPk(null);
                    }
             } else
                    searchRetirementTO.setBcbCode(null);
       }
       /**Cambio de participante en la pantalla de registro*/
       public void onChangeParticipantRegistry(){
             if(registreRetirementTO.getIdParticipantPk()!=null){
                    registreRetirementTO.setIdBankPk(null);
                    registreRetirementTO.setBcbCode(null);
                    registreRetirementTO.setCurrency(null);
                    registreRetirementTO.setGloss(null);
                    registreRetirementTO.setAmountDeposited(null);
                    registreRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
                    List<String> listGloss = collectionEconomicFacade.getListGlossCollectionForDeposit(registreRetirementTO.getRegistreDate(),registreRetirementTO.getIdParticipantPk(),registreRetirementTO.getIdBankPk(),false);
                    registreRetirementTO.setListGloss(listGloss);
             }
       }
       /**
       * Cambio de banco, se debe actualizar el BCB CODE
       **/
       public void onChangeBankRegistry() {
             if (registreRetirementTO.getIdBankPk() != null) {
                    Bank bank = parameterServiceBean.find(Bank.class,registreRetirementTO.getIdBankPk());
                    if (bank.getIdBcbCode() != null){
                           registreRetirementTO.setBcbCode(bank.getIdBcbCode().toString());
                           onChangeOperationDateRegistry();
                    }else {
                           JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
                           JSFUtilities.putViewMap("headerMessageView","El Banco seleccionado no cuenta con codigo BIC");
                           JSFUtilities.showSimpleValidationDialog();
                           registreRetirementTO.setIdBankPk(null);
                    }
             } else
                    registreRetirementTO.setBcbCode(null);
       }
       
       
       /**regreso a a pagina de busqueda*/
       public String backViewMainPageRegitry(){
          if (existDataInView(registreRetirementTO)) {
               JSFUtilities.updateComponent("opnlDialogs");
               JSFUtilities.executeJavascriptFunction("PF('wVbackSearchDeposit').show();");
               return "";
        } else{
                    registreRetirementTO = new ManualDepositTO();
            List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
            registreRetirementTO.setListParticipant(listParticipant);
            List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
            registreRetirementTO.setListCurrency(listCurrency);
            registreRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
            List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),null,null,null);
            registreRetirementTO.setListGloss(listGloss);
            List<Bank> listBanks = accountServiceBean.getListAllBank();
            registreRetirementTO.setListBanks(listBanks);
            return "backRegisterRetirementManual";
        }
       }
       /**
       * Evento de cambio de fecha en la pantalla de registro
       * */
       public void onChangeOperationDateRegistry() {
             if (registreRetirementTO.getRegistreDate() != null) {
                    List<String> listGloss = collectionEconomicFacade.getListGlossCollectionForDeposit(registreRetirementTO.getRegistreDate(),registreRetirementTO.getIdParticipantPk(),registreRetirementTO.getIdBankPk(),false);
                    registreRetirementTO.setListGloss(listGloss);
                    registreRetirementTO.setCurrency(null);
                    registreRetirementTO.setAmountRetirement(null);
                    registreRetirementTO.setGloss(null);
             } else {
                    registreRetirementTO.setGloss(null);
                    registreRetirementTO.setCurrency(null);
                    registreRetirementTO.setAmountRetirement(null);
             }
       }
       /**
       * Cambio de evento de fecha en la vista
       * */
       public void onChangeOperationDate() {
             if (searchRetirementTO.getRegistreDate() != null) {
                    List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),searchRetirementTO.getIdParticipantPk(),null,searchRetirementTO.getRetirementtStatus());
                    searchRetirementTO.setListGloss(listGloss);
                    searchRetirementTO.setCurrency(null);
                    searchRetirementTO.setAmountRetirement(null);
                    searchRetirementTO.setGloss(null);
             } else {
                    searchRetirementTO.setGloss(null);
                    searchRetirementTO.setCurrency(null);
                    searchRetirementTO.setAmountRetirement(null);
             }
       }
       
       /** metodo para regresar a la pantalla de search de deposito de fondos
       * 
        * @return */
       public String clickToBackSearch() {
             registreRetirementTO = new ManualDepositTO();
             // action para regresar a la pantalla search de deposito
             return "searchRetirementFunds";
       }
       
       /* Limpiar pantalla */
       public void cleanView() {
             if (existDataInView(searchRetirementTO)) {
                    JSFUtilities.updateComponent("opnlDialogs");
                    JSFUtilities.executeJavascriptFunction("PF('wVCleanSearchDeposit').show();");
             } else{
             init();
                 JSFUtilities.updateComponent("frmSearchDeposit");
             }
       }
       /* Limpiar pantalla */
       public String cleanViewRegistry() {
             if (existDataInView(registreRetirementTO)) {
                    JSFUtilities.updateComponent("opnlDialogs");
                    JSFUtilities.executeJavascriptFunction("PF('wVcleanRegistryDeposit').show();");
             } else{
             registreRetirementTO = new ManualDepositTO();
                 List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
                 registreRetirementTO.setListParticipant(listParticipant);
                 List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
                 registreRetirementTO.setListCurrency(listCurrency);
                 registreRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
                 List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),null,null,null);
                 registreRetirementTO.setListGloss(listGloss);
                 List<Bank> listBanks = accountServiceBean.getListAllBank();
                 registreRetirementTO.setListBanks(listBanks);
             }
             return "";
       }
       
       /**Metodo para limpiar la pantalla de registro*/
       public void exeCleanRegistryDeposit(){
          registreRetirementTO = new ManualDepositTO();
           List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
           registreRetirementTO.setListParticipant(listParticipant);
           List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
           registreRetirementTO.setListCurrency(listCurrency);
           registreRetirementTO.setRegistreDate(CommonsUtilities.currentDate());
           List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchRetirementTO.getRegistreDate(),null,null,null);
           registreRetirementTO.setListGloss(listGloss);
           List<Bank> listBanks = accountServiceBean.getListAllBank();
           registreRetirementTO.setListBanks(listBanks);
           
           JSFUtilities.updateComponent("frmRegDeposit");
       }
       

       /** Existe data en la visata para ejecutar la limpieza? */
       private boolean existDataInView(ManualDepositTO depositTO) {
             if (depositTO.getIdParticipantPk() != null)
                    return true;
             if (depositTO.getCurrency() != null)
                    return true;
             if(depositTO.getIdBankPk()!=null)
                    return true;
             if(depositTO.getGloss()!=null)
                    return true;
             if(depositTO.getAmountDeposited()!=null)
                    return true;
             return false;
       }

       /** Elimina el certificado cargado */
       public String deleteauxDepositFile() {
             searchRetirementTO.setAuxDepositFile(null);
             searchRetirementTO.setAuxDepositFileName(null);
             return null;
       }

       /**
       * Carga de certificado
       */
       /**
       * Usado en la pantalla de registro de cuantas bancarias
       * */
       /**
       * Usado en la pantalla de registro de cuantas bancarias
       * */
       public void documentAttachFile(FileUploadEvent event){
             String fDisplayName=fUploadValidateFile(event.getFile(),null, "5000000","pdf|doc|docx|jpg|jpeg|png");
             if(fDisplayName!=null){
                    registreRetirementTO.setAuxDepositFileName(fDisplayName);
                    registreRetirementTO.setAuxDepositFile(event.getFile().getContents());
                    JSFUtilities.updateComponent("idDeleteFile");
             }else{
                    JSFUtilities.putViewMap("headerMessageView",PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR));
                    JSFUtilities.putViewMap("bodyMessageView", PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
                    JSFUtilities.showSimpleValidationDialog();
             }
       }


       public GenericDataModel<DetailCollectionGloss> getListDetailCollectionGloss() {
             return listDetailCollectionGloss;
       }

       public void setListDetailCollectionGloss(
                    GenericDataModel<DetailCollectionGloss> listDetailCollectionGloss) {
             this.listDetailCollectionGloss = listDetailCollectionGloss;
       }

       public DetailCollectionGloss getDetailCollectionGlossSelected() {
             return detailCollectionGlossSelected;
       }
       public void setDetailCollectionGlossSelected(DetailCollectionGloss detailCollectionGlossSelected) {
             this.detailCollectionGlossSelected = detailCollectionGlossSelected;
       }
       
       
             public ManualDepositTO getSearchRetirementTO() {
             return searchRetirementTO;
       }
       public void setSearchRetirementTO(ManualDepositTO searchRetirementTO) {
             this.searchRetirementTO = searchRetirementTO;
       }
       public ManualDepositTO getRegistreRetirementTO() {
             return registreRetirementTO;
       }
       public void setRegistreRetirementTO(ManualDepositTO registreRetirementTO) {
             this.registreRetirementTO = registreRetirementTO;
       }
       public ManualDepositTO getViewhRetirementTO() {
             return viewhRetirementTO;
       }
       public void setViewhRetirementTO(ManualDepositTO viewhRetirementTO) {
             this.viewhRetirementTO = viewhRetirementTO;
       }
       public ParameterServiceBean getParameterServiceBean() {
             return parameterServiceBean;
       }
       public void setParameterServiceBean(ParameterServiceBean parameterServiceBean) {
             this.parameterServiceBean = parameterServiceBean;
       }
     public UserPrivilege getUserPrivilege() {
            return userPrivilege;
     }
     public void setUserPrivilege(UserPrivilege userPrivilege) {
            this.userPrivilege = userPrivilege;
     }
}