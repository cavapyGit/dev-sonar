package com.pradera.collection.monitor.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.bank.account.service.BankAccountServiceBean;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.ManualDepositTO;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.utils.view.GeneralPropertiesConstants;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;

@DepositaryWebBean
@LoggerCreateBean
public class DepositManualFundsBean extends GenericBaseBean {

	/**
	* 
	 */
	private static final long serialVersionUID = -8443666423562427891L;

	private ManualDepositTO searchDepositTO, registreDepositTO, viewhDepositTO;

	private DetailCollectionGloss detailCollectionGlossSelected;

	@Inject
	private ParameterServiceBean parameterServiceBean;

	@Inject
	private UserPrivilege userPrivilege;

	@Inject
	private BankAccountServiceBean accountServiceBean;

	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;

	private GenericDataModel<DetailCollectionGloss> listDetailCollectionGloss;

	@PostConstruct
	public void init() {
		searchDepositTO = new ManualDepositTO();
		List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
		searchDepositTO.setListParticipant(listParticipant);
		List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		searchDepositTO.setListCurrency(listCurrency);
		searchDepositTO.setRegistreDate(CommonsUtilities.currentDate());
		List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
				null, searchDepositTO.getDepositStatus());
		searchDepositTO.setListGloss(listGloss);
		List<Bank> listBanks = accountServiceBean.getListAllBank();
		searchDepositTO.setListBanks(listBanks);
		searchDepositTO.setDepositStatus(null);
		listDetailCollectionGloss = null;
	}

	/**
	 * Metodo para regresar a la pagina de principal
	 */
	public String backViewMainPageRegiry() {
		registreDepositTO = new ManualDepositTO();
		List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
		registreDepositTO.setListParticipant(listParticipant);
		List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		registreDepositTO.setListCurrency(listCurrency);
		registreDepositTO.setRegistreDate(CommonsUtilities.currentDate());
		List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
				null, null, null);
		registreDepositTO.setListGloss(listGloss);
		List<Bank> listBanks = accountServiceBean.getListAllBank();
		registreDepositTO.setListBanks(listBanks);
		return "backRegisterDepositManual";
	}

	/**
	 * Cargamos la pagina de registro
	 * 
	 */
	public String loadPageRegistreDeposit() {
		registreDepositTO = new ManualDepositTO();
		List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
		registreDepositTO.setListParticipant(listParticipant);
		List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		registreDepositTO.setListCurrency(listCurrency);
		registreDepositTO.setRegistreDate(CommonsUtilities.currentDate());
		List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
				null, null, null);
		registreDepositTO.setListGloss(listGloss);
		List<Bank> listBanks = accountServiceBean.getListAllBank();
		registreDepositTO.setListBanks(listBanks);
		return "pageRegisterDepositManual";
	}

	/**
	 * Registrando el desblqueo de la cuenta bancaria
	 */
	@LoggerAuditWeb
	public StreamedContent getStreamedContent() throws IOException {
		byte[] fileContent = registreDepositTO.getAuxDepositFile();
		return getStreamedContentFromFile(fileContent, null, registreDepositTO.getAuxDepositFileName());
	}

	/**
	 * *Guaradamos el registro de deposito
	 */
	public void beforeSaveDeposit() {
		if (existDataInView(registreDepositTO)) {
			if (registreDepositTO.getAmountDeposited().compareTo(BigDecimal.ZERO)==0||registreDepositTO.getAmountDeposited().compareTo(BigDecimal.ZERO)==-1) {
				JSFUtilities.putViewMap("bodyMessageView",PropertiesUtilities.getMessage("lbl.message.alert.amount.zero"));
				JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR));
				JSFUtilities.showSimpleValidationDialog();
				return ;
			}
			/** Ejeutar dialogo para conffirmar el deposito */
			JSFUtilities.executeJavascriptFunction("PF('wVsaveRegistryDeposit').show();");
			JSFUtilities.updateComponent("opnlDialogs");
		} else {
			JSFUtilities.putViewMap("bodyMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR));
			JSFUtilities.putViewMap("headerMessageView", "Existen datos obligatorios para el registro.");
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/** Metodo para guardar el registro de deposito */
	public void execuetSave() {
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setGloss(registreDepositTO.getGloss());
		retirementObjetTO.setIdParticipantPk(registreDepositTO.getIdParticipantPk());
		retirementObjetTO.setState(null);

		List<DetailCollectionGloss> listCollectionGlosses = collectionEconomicFacade
				.searchDetailCollectionGlosList(retirementObjetTO);
		DetailCollectionGloss collectionGloss = listCollectionGlosses.get(0);

		collectionGloss.setAmountDeposited(registreDepositTO.getAmountDeposited());
		collectionGloss.setIdBankPk(registreDepositTO.getIdBankPk());
		collectionGloss.setDepositDate(CommonsUtilities.currentDate());
		collectionGloss.setDepositStatus(DepositStateType.REGISTERED.getCode());
		if (registreDepositTO.getAuxDepositFileName() != null) {
			collectionGloss.setFileAux(registreDepositTO.getAuxDepositFile());
			collectionGloss.setFileAuxName(registreDepositTO.getAuxDepositFileName());
		}

		try {
			collectionEconomicFacade.saveManualDeposit(collectionGloss);
			registreDepositTO = new ManualDepositTO();

			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wvDialogResultRegister').show();");

		} catch (Exception e) {
			String msg = PropertiesUtilities.getMessage("msg.result.error");
			showMessageOnDialogDepositFunds(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msg);
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/**
	 * Buscamos todos los depositos
	 */
	public void beforeSearch() {
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setPaymentDate(searchDepositTO.getRegistreDate());
		if (searchDepositTO.getIdParticipantPk() == null)
			return;
		retirementObjetTO.setIdParticipantPk(searchDepositTO.getIdParticipantPk());

		if (searchDepositTO.getGloss() != null)
			retirementObjetTO.setGloss(searchDepositTO.getGloss());

		if (searchDepositTO.getDepositStatus() != null)
			retirementObjetTO.setState(searchDepositTO.getDepositStatus());
		else
			// esto se coloca para que pueda obtener todos los registros con estado
			// distintos de null
			retirementObjetTO.setState(Integer.valueOf(0));

		List<DetailCollectionGloss> listCollectionGlosses = collectionEconomicFacade
				.searchDetailCollectionGlosList(retirementObjetTO);

		listDetailCollectionGloss = new GenericDataModel<>(listCollectionGlosses);

	}

	/**
	 * metodo para retornar la moneda segun corresponda
	 * 
	 * @param currencyCode
	 * @return
	 */
	public String getTextCurrency(Integer currencyCode) {
		if (currencyCode.equals(CurrencyType.PYG.getCode()))
			return CurrencyType.PYG.getCodeIso();
		if (currencyCode.equals(CurrencyType.USD.getCode()))
			return CurrencyType.USD.getCodeIso();
		if (currencyCode.equals(CurrencyType.UFV.getCode()))
			return CurrencyType.UFV.getCodeIso();
		if (currencyCode.equals(CurrencyType.EU.getCode()))
			return CurrencyType.EU.getCodeIso();
		if (currencyCode.equals(CurrencyType.DMV.getCode()))
			return CurrencyType.DMV.getCodeIso();
		return "-";
	}

	/**
	 * metodo para retornar el estado de un deposito segun corresponda
	 * 
	 * @param status
	 * @return
	 */
	public String getTestStatusDeposit(Integer status) {
		if (status.equals(DepositStateType.REGISTERED.getCode()))
			return DepositStateType.REGISTERED.getValue();
		if (status.equals(DepositStateType.REJECTED.getCode()))
			return DepositStateType.REJECTED.getValue();
		if (status.equals(DepositStateType.CONFIRMED.getCode()))
			return DepositStateType.CONFIRMED.getValue();
		return "-";
	}

	/**
	 * Buscando glosas de acuerdo al participante
	 */
	public void onChangeParticipant() {
		if (searchDepositTO.getIdParticipantPk() != null) {
			searchDepositTO.setIdBankPk(null);
			searchDepositTO.setBcbCode(null);
			searchDepositTO.setCurrency(null);
			searchDepositTO.setGloss(null);
			searchDepositTO.setDepositStatus(null);
			searchDepositTO.setAmountDeposited(null);
			searchDepositTO.setRegistreDate(CommonsUtilities.currentDate());
			try {
				List<String> listGloss = collectionEconomicFacade.getListGlossCollection(
						searchDepositTO.getRegistreDate(), searchDepositTO.getIdParticipantPk(),
						searchDepositTO.getDepositStatus(), null);
				searchDepositTO.setListGloss(listGloss);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * metodo para para construir un dialog con su cabecera y body como prametros
	 * 
	 * @param headerMessageConfirmation
	 * @param bodyMessageConfirmation
	 */
	private void showMessageOnDialogDepositFunds(String headerMessageConfirmation, String bodyMessageConfirmation) {
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}

	/**
	 * metodo para verificar si se selecciono un registro antes de confirma o
	 * rechazar un deposito
	 * 
	 * @return true si selecciono un registro, false si no selecciono nada
	 */
	private boolean isSelectedRow() {
		if (Validations.validateIsNotNull(detailCollectionGlossSelected)) {
			if (Validations.validateIsNull(detailCollectionGlossSelected.getIdDetailCollectionGlossPk())) {
				String msg = PropertiesUtilities.getMessage("lbl.message.alert.no.select");
				showMessageOnDialogDepositFunds(
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), msg);
				JSFUtilities.showSimpleValidationDialog();
			} else {
				return true;
			}
		} else {
			String msg = PropertiesUtilities.getMessage("lbl.message.alert.no.select");
			showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					msg);
			JSFUtilities.showSimpleValidationDialog();
		}
		return false;
	}

	/**
	 * metodo para llenar la data necesaria para el view de deposito de fondos
	 * 
	 * @param detailCollectionGloss
	 */
	private void fillDataForViewDeposit(DetailCollectionGloss detailCollectionGloss) {
		// este es el objeto que vamos a mostrar en el view
		registreDepositTO = new ManualDepositTO();
		List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
		registreDepositTO.setListParticipant(listParticipant);
		List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		registreDepositTO.setListCurrency(listCurrency);
		List<String> listGloss = collectionEconomicFacade.getListGlossCollection(detailCollectionGloss.getPaymentDate(),
				detailCollectionGloss.getIdParticipantPk(), detailCollectionGloss.getDepositStatus(), null);
		registreDepositTO.setListGloss(listGloss);
		List<Bank> listBanks = accountServiceBean.getListAllBank();
		if (detailCollectionGloss.getIdBankPk() != null) {
			Bank bank = parameterServiceBean.find(Bank.class, detailCollectionGloss.getIdBankPk());
			if (bank.getIdBcbCode() != null)
				registreDepositTO.setBcbCode(bank.getIdBcbCode().toString());
			else
				registreDepositTO.setBcbCode("-");
			registreDepositTO.setListBanks(listBanks);
		}
		registreDepositTO.setIdParticipantPk(detailCollectionGloss.getIdParticipantPk());
		registreDepositTO.setIdBankPk(detailCollectionGloss.getIdBankPk());
		registreDepositTO.setRegistreDate(detailCollectionGloss.getPaymentDate());
		registreDepositTO.setCurrency(detailCollectionGloss.getCurrency());
		registreDepositTO.setAmountDeposited(detailCollectionGloss.getAmountDeposited());
		registreDepositTO.setAuxDepositFile(detailCollectionGloss.getFileAux());
		registreDepositTO.setAuxDepositFileName(detailCollectionGloss.getFileAuxName());
		registreDepositTO.setGloss(detailCollectionGloss.getGlosaNotClassified());
	}

	public void beforeSaveReject() {
		JSFUtilities.executeJavascriptFunction("PF('wvDialogRejectDeposit').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}

	public void beforeSaveConfirm() {
		JSFUtilities.executeJavascriptFunction("PF('wvDialogConfirmDeposit').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}

	/** metodo para cambiar el estado del deposito a rechazado */
	public void saveRejectDeposit() {
		detailCollectionGlossSelected.setDepositStatus(null);
		detailCollectionGlossSelected.setAmountDeposited(BigDecimal.ZERO);
		detailCollectionGlossSelected.setDepositDate(null);
		try {
			collectionEconomicFacade.saveChangeStatusDeposit(detailCollectionGlossSelected);
			beforeSearch();

			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wvDialogResultReject').show();");

		} catch (ServiceException e) {
			String msg = PropertiesUtilities.getMessage("msg.result.error");
			showMessageOnDialogDepositFunds(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msg);
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/** metodo para cambiar el estado del deposito a confirm */
	public void saveConfirmDeposit() {
		detailCollectionGlossSelected.setDepositStatus(DepositStateType.CONFIRMED.getCode());
		try {
			collectionEconomicFacade.saveChangeStatusDeposit(detailCollectionGlossSelected);
			beforeSearch();

			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wvDialogResult').show();");

		} catch (ServiceException e) {
			String msg = PropertiesUtilities.getMessage("msg.result.error");
			showMessageOnDialogDepositFunds(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msg);
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/**
	 * metodo antes de ir a la pantalla de rechazo
	 * 
	 * @return
	 */
	public String beforeReject() {

		// validando si se ha seleccionado un registro
		if (!isSelectedRow())
			return null;

		// validando si el registro esta en estado diferente de registrado
		if (!detailCollectionGlossSelected.getDepositStatus().equals(DepositStateType.REGISTERED.getCode())) {
			String msg = PropertiesUtilities.getMessage("msg.validation.required.state.register");
			showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					msg);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}

		fillDataForViewDeposit(detailCollectionGlossSelected);

		// mostrando solo el boton de rechazo
		registreDepositTO.setBtnReject(true);

		return "viewDepositFunds";
	}

	/**
	 * metodo antes de ir a la pantalla de confirmacion
	 * 
	 * @return
	 */
	public String beforeConfirm() {
		// validando si se ha seleccionado un registro
		if (!isSelectedRow())
			return null;

		// validando si el registro esta en estado diferente de registrado
		if (!detailCollectionGlossSelected.getDepositStatus().equals(DepositStateType.REGISTERED.getCode())) {
			String msg = PropertiesUtilities.getMessage("msg.validation.required.state.register");
			showMessageOnDialogDepositFunds(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					msg);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}

		fillDataForViewDeposit(detailCollectionGlossSelected);

		// mostrando solo el boton de confirmacion
		registreDepositTO.setBtnConfirm(true);

		return "viewDepositFunds";
	}

	/**
	 * metodo para cargar los datos para el view de deposistos
	 * 
	 * @param event
	 */
	public void loadDetailDepositFunds(ActionEvent event) {
		// este objeto enviara los parametros a la query
		DetailCollectionGloss detailCollGloss = new DetailCollectionGloss();
		detailCollGloss = (DetailCollectionGloss) event.getComponent().getAttributes().get("resultDepositFundsView");
		fillDataForViewDeposit(detailCollGloss);
		// mostrando solo el boton de view
		registreDepositTO.setBtnView(true);
	}

	/**
	 * metodo para descargar el archivo adjunto
	 * 
	 * @return
	 * @throws IOException
	 */
	public StreamedContent getFileStreamContent() throws IOException {
		if (registreDepositTO.getAuxDepositFile() != null) {
			InputStream is = new ByteArrayInputStream(registreDepositTO.getAuxDepositFile());
			StreamedContent strContent = new DefaultStreamedContent(is, null,
					registreDepositTO.getAuxDepositFileName());
			is.close();
			return strContent;
		} else
			return null;
	}

	/** Buscamos el detalle de la notificaion */
	public void onChangeGloss() {
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setGloss(searchDepositTO.getGloss());
		retirementObjetTO.setState(searchDepositTO.getDepositStatus());
		List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade
				.searchDetailCollectionGlosList(retirementObjetTO);
		if (collectionGlossList == null || collectionGlossList.isEmpty()) {
			JSFUtilities.putViewMap("headerMessageView", "!Alerta!");
			JSFUtilities.putViewMap("bodyMessageView", "No se encontraron registros.");
			JSFUtilities.showSimpleValidationDialog();
		} else {
			DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
			searchDepositTO.setCurrency(collectionGloss.getCurrency());
			searchDepositTO.setAmountDeposited(collectionGloss.getAmountReceivable());
		}
	}

	/** Buscamos el detalle de la notificaion */
	public void onChangeGlossRegistry() {
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setGloss(registreDepositTO.getGloss());
		retirementObjetTO.setState(null);
		List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade
				.searchDetailCollectionGlosList(retirementObjetTO);
		DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
		if (collectionGloss == null) {
			JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
			JSFUtilities.putViewMap("headerMessageView", "No se encontro registro en estado registrado.");
			JSFUtilities.showSimpleValidationDialog();
		} else {
			registreDepositTO.setCurrency(collectionGloss.getCurrency());
			registreDepositTO.setAmountDeposited(collectionGloss.getAmountReceivable());
			CollectionEconomicRigth collectionEconomicRigth = collectionEconomicFacade
					.searchCollectiionEconomicByGloss(null, collectionGloss.getGlosaNotClassified());
			registreDepositTO.setIdBankPk(collectionEconomicRigth.getIdBankFk());
		}
	}

	/**
	 * Cambio de banco, se debe actualizar el BCB CODE
	 **/
	public void onChangeBank() {
		if (searchDepositTO.getIdBankPk() != null) {
			Bank bank = parameterServiceBean.find(Bank.class, searchDepositTO.getIdBankPk());
			if (bank.getIdBcbCode() != null)
				searchDepositTO.setBcbCode(bank.getIdBcbCode().toString());
			else {
				JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
				JSFUtilities.putViewMap("headerMessageView", "El Banco seleccionado no cuenta con codigo BIC");
				JSFUtilities.showSimpleValidationDialog();
				searchDepositTO.setIdBankPk(null);
			}
		} else
			searchDepositTO.setBcbCode(null);
	}

	/** Cambio de participante en la pantalla de registro */
	public void onChangeParticipantRegistry() {
		if (registreDepositTO.getIdParticipantPk() != null) {
			registreDepositTO.setIdBankPk(null);
			registreDepositTO.setBcbCode(null);
			registreDepositTO.setCurrency(null);
			registreDepositTO.setGloss(null);
			registreDepositTO.setAmountDeposited(null);
			registreDepositTO.setRegistreDate(CommonsUtilities.currentDate());
			List<String> listGloss = collectionEconomicFacade.getListGlossCollectionForDeposit(
					registreDepositTO.getRegistreDate(), registreDepositTO.getIdParticipantPk(),
					registreDepositTO.getIdBankPk(), true);
			registreDepositTO.setListGloss(listGloss);
		}
	}

	/**
	 * Cambio de banco, se debe actualizar el BCB CODE
	 **/
	public void onChangeBankRegistry() {
		if (registreDepositTO.getIdBankPk() != null) {
			Bank bank = parameterServiceBean.find(Bank.class, registreDepositTO.getIdBankPk());
			if (bank.getIdBcbCode() != null) {
				registreDepositTO.setBcbCode(bank.getIdBcbCode().toString());
				// onChangeOperationDateRegistry();
			} else {
				JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
				JSFUtilities.putViewMap("headerMessageView", "El Banco seleccionado no cuenta con codigo BIC");
				JSFUtilities.showSimpleValidationDialog();
				registreDepositTO.setIdBankPk(null);
			}
		} else
			registreDepositTO.setBcbCode(null);
	}

	/** regreso a a pagina de busqueda */
	public String backViewMainPageRegitry() {
		if (existDataInView(registreDepositTO)) {
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVbackSearchDeposit').show();");
			return "";
		} else {
			registreDepositTO = new ManualDepositTO();
			List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
			registreDepositTO.setListParticipant(listParticipant);
			List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
			registreDepositTO.setListCurrency(listCurrency);
			registreDepositTO.setRegistreDate(CommonsUtilities.currentDate());
			List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
					null, null, null);
			registreDepositTO.setListGloss(listGloss);
			List<Bank> listBanks = accountServiceBean.getListAllBank();
			registreDepositTO.setListBanks(listBanks);
			return "backRegisterDepositManual";
		}
	}

	/**
	 * Evento de cambio de fecha en la pantalla de registro
	 */
	public void onChangeOperationDateRegistry() {
		if (registreDepositTO.getRegistreDate() != null) {
			List<String> listGloss = collectionEconomicFacade.getListGlossCollectionForDeposit(
					registreDepositTO.getRegistreDate(), registreDepositTO.getIdParticipantPk(),
					registreDepositTO.getIdBankPk(), true);
			registreDepositTO.setListGloss(listGloss);
			registreDepositTO.setCurrency(null);
			registreDepositTO.setAmountDeposited(null);
		} else {
			registreDepositTO.setGloss(null);
			registreDepositTO.setCurrency(null);
			registreDepositTO.setAmountDeposited(null);
		}
	}

	/**
	 * Cambio de evento de fecha en la vista
	 */
	public void onChangeOperationDate() {
		if (searchDepositTO.getRegistreDate() != null) {
			List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
					searchDepositTO.getIdParticipantPk(), searchDepositTO.getDepositStatus(), null);
			searchDepositTO.setListGloss(listGloss);
			searchDepositTO.setCurrency(null);
			searchDepositTO.setAmountDeposited(null);
		} else {
			searchDepositTO.setGloss(null);
			searchDepositTO.setCurrency(null);
			searchDepositTO.setAmountDeposited(null);
		}
	}

	/**
	 * metodo para regresar a la pantalla de search de deposito de fondos
	 * 
	 * @return
	 */
	public String clickToBackSearch() {
		registreDepositTO = new ManualDepositTO();
		// searchDepositTO = new ManualDepositTO();

		// action para regresar a la pantalla search de deposito
		return "searchDepositFunds";
	}

	/* Limpiar pantalla */
	public void cleanView() {
		if (existDataInView(searchDepositTO)) {
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities
					.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVCleanSearchDeposit').show();");
		} else {
			init();
			JSFUtilities.updateComponent("frmSearchDeposit");
		}
	}

	/* Limpiar pantalla */
	public String cleanViewRegistry() {
		if (existDataInView(registreDepositTO)) {
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVcleanRegistryDeposit').show();");
		} else {
			registreDepositTO = new ManualDepositTO();
			List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
			registreDepositTO.setListParticipant(listParticipant);
			List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
			registreDepositTO.setListCurrency(listCurrency);
			registreDepositTO.setRegistreDate(CommonsUtilities.currentDate());
			List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
					null, null, null);
			registreDepositTO.setListGloss(listGloss);
			List<Bank> listBanks = accountServiceBean.getListAllBank();
			registreDepositTO.setListBanks(listBanks);
			JSFUtilities.updateComponent("frmRegDeposit");
		}
		return "";
	}

	public String loadPageSearch() {
		beforeSearch();
		return "searchDepositFunds";
	}

	/** Metodo para limpiar la pantalla de registro */
	public void exeCleanRegistryDeposit() {
		registreDepositTO = new ManualDepositTO();
		List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
		registreDepositTO.setListParticipant(listParticipant);
		List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		registreDepositTO.setListCurrency(listCurrency);
		registreDepositTO.setRegistreDate(CommonsUtilities.currentDate());
		List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getRegistreDate(),
				null, null, null);
		registreDepositTO.setListGloss(listGloss);
		List<Bank> listBanks = accountServiceBean.getListAllBank();
		registreDepositTO.setListBanks(listBanks);

		JSFUtilities.updateComponent("frmRegDeposit");
	}

	/** Existe data en la visata para ejecutar la limpieza? */
	private boolean existDataInView(ManualDepositTO depositTO) {
		if (depositTO.getIdParticipantPk() != null)
			return true;
		if (depositTO.getCurrency() != null)
			return true;
		if (depositTO.getIdBankPk() != null)
			return true;
		if (depositTO.getGloss() != null)
			return true;
		if (depositTO.getAmountDeposited() != null)
			return true;
		return false;
	}

	/** Elimina el certificado cargado */
	public String deleteauxDepositFile() {
		searchDepositTO.setAuxDepositFile(null);
		searchDepositTO.setAuxDepositFileName(null);
		return null;
	}

	/**
	 * Carga de certificado
	 */
	/**
	 * Usado en la pantalla de registro de cuantas bancarias
	 */
	/**
	 * Usado en la pantalla de registro de cuantas bancarias
	 */
	public void documentAttachFile(FileUploadEvent event) {
		String fDisplayName = fUploadValidateFile(event.getFile(), null, "5000000", "pdf|doc|docx|jpg|jpeg|png");
		if (fDisplayName != null) {
			registreDepositTO.setAuxDepositFileName(fDisplayName);
			registreDepositTO.setAuxDepositFile(event.getFile().getContents());
			JSFUtilities.updateComponent("idDeleteFile");
		} else {
			JSFUtilities.putViewMap("headerMessageView",
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR));
			JSFUtilities.putViewMap("bodyMessageView",
					PropertiesUtilities.getGenericMessage(GeneralPropertiesConstants.FUPL_ERROR_INVALID_FILE_TYPE));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	public ManualDepositTO getSearchDepositTO() {
		return searchDepositTO;
	}

	public void setSearchDepositTO(ManualDepositTO searchDepositTO) {
		this.searchDepositTO = searchDepositTO;
	}

	public GenericDataModel<DetailCollectionGloss> getListDetailCollectionGloss() {
		return listDetailCollectionGloss;
	}

	public void setListDetailCollectionGloss(GenericDataModel<DetailCollectionGloss> listDetailCollectionGloss) {
		this.listDetailCollectionGloss = listDetailCollectionGloss;
	}

	public DetailCollectionGloss getDetailCollectionGlossSelected() {
		return detailCollectionGlossSelected;
	}

	public void setDetailCollectionGlossSelected(DetailCollectionGloss detailCollectionGlossSelected) {
		this.detailCollectionGlossSelected = detailCollectionGlossSelected;
	}

	public ManualDepositTO getRegistreDepositTO() {
		return registreDepositTO;
	}

	public void setRegistreDepositTO(ManualDepositTO registreDepositTO) {
		this.registreDepositTO = registreDepositTO;
	}

	public ManualDepositTO getViewhDepositTO() {
		return viewhDepositTO;
	}

	public void setViewhDepositTO(ManualDepositTO viewhDepositTO) {
		this.viewhDepositTO = viewhDepositTO;
	}

	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
}