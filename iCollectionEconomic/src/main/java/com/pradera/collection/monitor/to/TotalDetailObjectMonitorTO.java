package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class TotalDetailObjectMonitorTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1043440434411523731L;

	private BigDecimal totalAmountCharge;
	
	private List<DetailObjectMonitorTO> lisDetailObjectMonitorTOs;
	
	public TotalDetailObjectMonitorTO() {
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getTotalAmountCharge() {
		return totalAmountCharge;
	}

	public void setTotalAmountCharge(BigDecimal totalAmountCharge) {
		this.totalAmountCharge = totalAmountCharge;
	}

	public List<DetailObjectMonitorTO> getLisDetailObjectMonitorTOs() {
		return lisDetailObjectMonitorTOs;
	}

	public void setLisDetailObjectMonitorTOs(
			List<DetailObjectMonitorTO> lisDetailObjectMonitorTOs) {
		this.lisDetailObjectMonitorTOs = lisDetailObjectMonitorTOs;
	}
}
