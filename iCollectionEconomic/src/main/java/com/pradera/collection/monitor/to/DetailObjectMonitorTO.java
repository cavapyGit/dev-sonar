package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DetailObjectMonitorTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5939080561109845262L;
	/*Id*/
	private Long idCollectionEconomicPk;
	/*Fecha de vencimiento*/
	private Date dueDate; 
	/*Fecha de cobro*/
	private Date paymetDate; 
	/*Cantiad de valores*/
	private String idSecurityCodePk;
	/*Cantidad de valores*/
	private BigDecimal amountSecurities;
	/*Numero de cupon*/
	private Integer couponNumber;
	/*Moneda a cobrar*/
	private Integer collectionCurrency;
	/*Moneda de cobro*/
	private String descCollectionCurrency;
	/*Monto a cobrar*/
	private BigDecimal amountReceivable;
	/*Banco*/
	private Long idBankPk;
	/*Descripcion del Banco*/
	private String descBank;
	/*Cuenta*/
	private String accountNumberDeposit;
	
	public DetailObjectMonitorTO() {
		// TODO Auto-generated constructor stub
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getPaymetDate() {
		return paymetDate;
	}

	public void setPaymetDate(Date paymetDate) {
		this.paymetDate = paymetDate;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public BigDecimal getAmountSecurities() {
		return amountSecurities;
	}

	public void setAmountSecurities(BigDecimal amountSecurities) {
		this.amountSecurities = amountSecurities;
	}

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Integer getCollectionCurrency() {
		return collectionCurrency;
	}

	public void setCollectionCurrency(Integer collectionCurrency) {
		this.collectionCurrency = collectionCurrency;
	}


	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}

	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}

	public Long getIdCollectionEconomicPk() {
		return idCollectionEconomicPk;
	}

	public void setIdCollectionEconomicPk(Long idCollectionEconomicPk) {
		this.idCollectionEconomicPk = idCollectionEconomicPk;
	}

	public String getDescCollectionCurrency() {
		return descCollectionCurrency;
	}

	public void setDescCollectionCurrency(String descCollectionCurrency) {
		this.descCollectionCurrency = descCollectionCurrency;
	}

	public Long getIdBankPk() {
		return idBankPk;
	}

	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

	public String getDescBank() {
		return descBank;
	}

	public void setDescBank(String descBank) {
		this.descBank = descBank;
	}

	public String getAccountNumberDeposit() {
		return accountNumberDeposit;
	}

	public void setAccountNumberDeposit(String accountNumberDeposit) {
		this.accountNumberDeposit = accountNumberDeposit;
	}
}
