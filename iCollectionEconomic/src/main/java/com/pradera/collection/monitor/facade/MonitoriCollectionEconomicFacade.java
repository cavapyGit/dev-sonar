package com.pradera.collection.monitor.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.olap4j.impl.ArrayMap;

import com.pradera.alert.service.ManageRetirementServiceBean;
import com.pradera.alert.webclient.CollectionServiceConsumer;
import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.alertt.bath.to.XmlObjectResponse;
import com.pradera.collection.monitor.service.MonitoriCollectionEconomicService;
import com.pradera.collection.monitor.to.DetailMatchGlossTO;
import com.pradera.collection.monitor.to.EquivalentDepositTO;
import com.pradera.collection.monitor.to.ManualDepositTO;
import com.pradera.collection.monitor.to.ResultMonitorSearchTO;
import com.pradera.collection.monitor.to.ResultMonitorTO;
import com.pradera.collection.monitor.to.SearchFilterMonitorTO;
import com.pradera.collection.monitor.to.TotalDetailObjectMonitorTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.funds.to.AutomaticSendType;
import com.pradera.integration.component.funds.to.FundsTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.integration.usersession.UserAcctions;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.funds.constant.FundsAutomaticConstants;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.pradera.model.generalparameter.type.RetirementStateType;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.EquivalentDepositGlosses;
import com.edv.collection.economic.rigth.FundsCollection;
import com.edv.collection.economic.rigth.GlossNoticationDetail;
import com.edv.collection.economic.rigth.PaymentAgent;


@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class MonitoriCollectionEconomicFacade implements Serializable {
	
	/**
	   * 
	    */
	private static final long serialVersionUID = -7461446443019888633L;

	@Inject
	private MonitoriCollectionEconomicService monitorService;

	@Inject
	private ManageRetirementServiceBean manageRetirementServiceBean;

	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	@Inject
	private CollectionServiceConsumer collectionServiceConsumer;

	@Inject
	private ParticipantServiceBean participantServiceBean;

	@Inject
	@Configurable
	private String mnemonicParticipantEDB;

	@Inject
	private PraderaLogger log;

	@Inject
	@Configurable
	private String accountNumberCollection;

	/** metodo para retornar la glosa notificada */
	public DetailCollectionGloss getLstIdCollectionEconomicRigthForOneGloss(String gloss) {
		return monitorService.findDetailCollectionGloss(gloss);
	}

	/**
	 * metodo para cambiar el indicador del reeenvio de notificacion a REENVIADO
	 */
	public void modifiedIndReSenderNotification(GlossNoticationDetail glossNoticationDetail) {
		monitorService.modifiedReSenderNotification(glossNoticationDetail);
	}

	/**
	 * Busqueda de vencimientos registrados para el cobro de derechos economicos
	 * 
	 * @param searchFilterMonitorTO filtros de busqueda
	 * @return lista vencimientos agrupados por participante, moneda y fecha de
	 *         vencimiento y/o cobro
	 */
	public GenericDataModel<ResultMonitorSearchTO> searchExpirationClassifiedByGlossToReenvio(
			SearchFilterMonitorTO searchFilterMonitorTO) {
		/** Busqueda de registros de detail collection gloss */
		// searchFilterMonitorTO.getDatePayment();
		List<DetailCollectionGloss> listDetailCollectionGlosses = monitorService.searchDetailsCollectionGlossToReenvio(
				searchFilterMonitorTO.getIdParticipantPk(), searchFilterMonitorTO.getIssuer().getIdIssuerPk(),
				searchFilterMonitorTO.getDatePayment(), searchFilterMonitorTO.getCurrency(),
				searchFilterMonitorTO.getGlossCode() == null ? CommonsUtilities.STR_BLANK
						: searchFilterMonitorTO.getGlossCode());

		List<ResultMonitorSearchTO> listResult = new ArrayList<ResultMonitorSearchTO>();

		ResultMonitorSearchTO monitorDetailCollection;
		ResultMonitorTO monitorTO;
		TotalDetailObjectMonitorTO totalDetailObjectMonitorTO = new TotalDetailObjectMonitorTO();
		PaymentAgent paymentAgentJuridic;
		for (DetailCollectionGloss detailCollectionGloss : listDetailCollectionGlosses) {

			monitorTO = new ResultMonitorTO();

			paymentAgentJuridic = monitorService
					.getPaymentAgentJuridic(detailCollectionGloss.getIdDetailCollectionGlossPk());
			if (paymentAgentJuridic != null)
				monitorTO.setDescJuridicPaymentAgent(paymentAgentJuridic.getDescription());
			else
				monitorTO.setDescJuridicPaymentAgent(CommonsUtilities.STR_BLANK);

			monitorTO.setPaymentDate(detailCollectionGloss.getPaymentDate());
			monitorTO.setGloss(detailCollectionGloss.getGlosaNotClassified());

			monitorTO.setIdParticipantPk(detailCollectionGloss.getIdParticipantPk());
			monitorTO.setMnemonic(
					monitorService.find(Participant.class, searchFilterMonitorTO.getIdParticipantPk()).getMnemonic());

			/** Verificando se ya se ejecuto el deposito */
			// if(detailCollectionGloss.getTransferStatus()!=null&&detailCollectionGloss.getTransferStatus().equals(DepositStateType.DEPOSITED.getCode())){
			if (detailCollectionGloss.getAmountDeposited() != null && detailCollectionGloss.getAmountDeposited()
					.compareTo(detailCollectionGloss.getAmountReceivable()) == 0) {
				monitorTO.setPaidObligation(true);
				monitorTO.setPaidObligationDesc(BooleanType.YES.getValue());
			} else {
				monitorTO.setPaidObligation(false);
				monitorTO.setPaidObligationDesc(BooleanType.NO.getValue());
			}
			monitorTO.setCurrency(detailCollectionGloss.getCurrency());
			monitorTO.setCurrencyDesc(
					monitorService.find(ParameterTable.class, searchFilterMonitorTO.getCurrency()).getText1());

			totalDetailObjectMonitorTO = monitorService.getDetailObjectMonitorTO(
					detailCollectionGloss.getIdParticipantPk(), detailCollectionGloss.getRegistreDate(),
					detailCollectionGloss.getGlosaNotClassified());
			// totalDetailObjectMonitorTO =
			// monitorService.getDetailObjectMonitorTO(detailCollectionGloss.getIdParticipantPk(),
			// detailCollectionGloss.getPaymentDate(),
			// detailCollectionGloss.getGlosaNotClassified());
			/** Monto a cobrar segun glosa */
			monitorTO.setAmountReceivable(detailCollectionGloss.getAmountReceivable());

			monitorTO.setAmountDeposited(detailCollectionGloss.getAmountDeposited() == null ? BigDecimal.ZERO
					: detailCollectionGloss.getAmountDeposited());
			/** Monto recibido */
			monitorDetailCollection = new ResultMonitorSearchTO();
			monitorDetailCollection.setMonitorTO(monitorTO);
			monitorDetailCollection
					.setDetailObjectMonitorTOs(totalDetailObjectMonitorTO.getLisDetailObjectMonitorTOs());
			// obteniendo los contactos agentes pagadores
			monitorDetailCollection
					.setLstGlossNoticationDetail(monitorService.getLstGlossNoticationDetailOfOneDetailCollectionGloss(
							detailCollectionGloss.getIdDetailCollectionGlossPk()));
			listResult.add(monitorDetailCollection);
		}
		return new GenericDataModel<>(listResult);
	}

	/**
	 * Busqueda de vencimientos registrados para el cobro de derechos economicos
	 * 
	 * @param searchFilterMonitorTO filtros de busqueda
	 * @return lista vencimientos agrupados por participante, moneda y fecha de
	 *         vencimiento y/o cobro
	 */
	public GenericDataModel<ResultMonitorSearchTO> searchExpirationClassifiedByGlossToMonitorCollection(
			SearchFilterMonitorTO searchFilterMonitorTO) {
		/** Busqueda de registros de detail collection gloss */
		List<DetailCollectionGloss> listDetailCollectionGlosses = monitorService
				.searchDetailsCollectionGlossToMotinorCollection(searchFilterMonitorTO.getIdParticipantPk(),
						searchFilterMonitorTO.getIssuer().getIdIssuerPk(), searchFilterMonitorTO.getMinDate(),
						searchFilterMonitorTO.getMaxDate(), searchFilterMonitorTO.getCurrency(),
						searchFilterMonitorTO.getGlossCode() == null ? CommonsUtilities.STR_BLANK
								: searchFilterMonitorTO.getGlossCode());

		List<ResultMonitorSearchTO> listResult = new ArrayList<ResultMonitorSearchTO>();

		ResultMonitorSearchTO monitorDetailCollection;
		ResultMonitorTO monitorTO;
		TotalDetailObjectMonitorTO totalDetailObjectMonitorTO = new TotalDetailObjectMonitorTO();
		PaymentAgent paymentAgentJuridic;
		for (DetailCollectionGloss detailCollectionGloss : listDetailCollectionGlosses) {

			monitorTO = new ResultMonitorTO();
			paymentAgentJuridic = monitorService
					.getPaymentAgentJuridic(detailCollectionGloss.getIdDetailCollectionGlossPk());
			if (paymentAgentJuridic != null)
				monitorTO.setDescJuridicPaymentAgent(paymentAgentJuridic.getDescription());
			else
				monitorTO.setDescJuridicPaymentAgent(CommonsUtilities.STR_BLANK);
			monitorTO.setPaymentDate(detailCollectionGloss.getPaymentDate());
			monitorTO.setGloss(detailCollectionGloss.getGlosaNotClassified());

			monitorTO.setIdParticipantPk(detailCollectionGloss.getIdParticipantPk());
			monitorTO.setMnemonic(
					monitorService.find(Participant.class, searchFilterMonitorTO.getIdParticipantPk()).getMnemonic());

			/** Verificando se ya se ejecuto el deposito */
			// if(detailCollectionGloss.getTransferStatus()!=null&&detailCollectionGloss.getTransferStatus().equals(DepositStateType.DEPOSITED.getCode())){
			if (detailCollectionGloss.getAmountDeposited() != null && detailCollectionGloss.getAmountDeposited()
					.compareTo(detailCollectionGloss.getAmountReceivable()) == 0) {
				monitorTO.setPaidObligation(true);
				monitorTO.setPaidObligationDesc("Si");
			} else {
				monitorTO.setPaidObligation(false);
				monitorTO.setPaidObligationDesc("No");
			}
			monitorTO.setCurrency(detailCollectionGloss.getCurrency());
			monitorTO.setCurrencyDesc(
					monitorService.find(ParameterTable.class, searchFilterMonitorTO.getCurrency()).getText1());
			
			monitorTO.setNotificationDate(detailCollectionGloss.getRegistreDate());

			totalDetailObjectMonitorTO = monitorService.getDetailObjectMonitorTO(
					detailCollectionGloss.getIdParticipantPk(), detailCollectionGloss.getRegistreDate(),
					detailCollectionGloss.getGlosaNotClassified());
			/** Monto a cobrar segun glosa */
			monitorTO.setAmountReceivable(detailCollectionGloss.getAmountReceivable());

			monitorTO.setAmountDeposited(detailCollectionGloss.getAmountDeposited() == null ? BigDecimal.ZERO
					: detailCollectionGloss.getAmountDeposited());
			/** Monto recibido */
			monitorDetailCollection = new ResultMonitorSearchTO();
			monitorDetailCollection.setMonitorTO(monitorTO);
			monitorDetailCollection
					.setDetailObjectMonitorTOs(totalDetailObjectMonitorTO.getLisDetailObjectMonitorTOs());
			// obteniendo los contactos agentes pagadores
			monitorDetailCollection
					.setLstGlossNoticationDetail(monitorService.getLstGlossNoticationDetailOfOneDetailCollectionGloss(
							detailCollectionGloss.getIdDetailCollectionGlossPk()));
			listResult.add(monitorDetailCollection);
		}
		return new GenericDataModel<>(listResult);
	}

	/**
	 * Busqueda de vencimientos registrados para el cobro de derechos economicos
	 * 
	 * @param searchFilterMonitorTO filtros de busqueda
	 * @return lista vencimientos agrupados por participante, moneda y fecha de
	 *         vencimiento y/o cobro
	 */
	public GenericDataModel<ResultMonitorSearchTO> searchExpirationClassifiedByGloss(
			SearchFilterMonitorTO searchFilterMonitorTO) {
		/** Busqueda de registros de detail collection gloss */
		// searchFilterMonitorTO.getDatePayment();
		// searchDetailsCollectionGlossToMotinorCollection asdasdasdasd
		List<DetailCollectionGloss> listDetailCollectionGlosses = monitorService.searchDetailsCollectionGloss(
				searchFilterMonitorTO.getIdParticipantPk(), searchFilterMonitorTO.getIssuer().getIdIssuerPk(),
				searchFilterMonitorTO.getDatePayment(), searchFilterMonitorTO.getCurrency(),
				searchFilterMonitorTO.getGlossCode() == null ? CommonsUtilities.STR_BLANK
						: searchFilterMonitorTO.getGlossCode());

		List<ResultMonitorSearchTO> listResult = new ArrayList<ResultMonitorSearchTO>();

		ResultMonitorSearchTO monitorDetailCollection;
		ResultMonitorTO monitorTO;
		TotalDetailObjectMonitorTO totalDetailObjectMonitorTO = new TotalDetailObjectMonitorTO();
		PaymentAgent paymentAgentJuridic;
		for (DetailCollectionGloss detailCollectionGloss : listDetailCollectionGlosses) {

			monitorTO = new ResultMonitorTO();

			boolean disableButton = true;
			if (detailCollectionGloss.getDepositStatus() != null
					&& detailCollectionGloss.getDepositStatus().equals(DepositStateType.CONFIRMED.getCode()))
				disableButton = false;
			if (detailCollectionGloss.getTransferStatus() != null
					&& detailCollectionGloss.getTransferStatus().equals(RetirementStateType.CONFIRMED.getCode()))
				disableButton = true;
			if (detailCollectionGloss.getSourceDeposit() != null
					&& detailCollectionGloss.getSourceDeposit().equals(BooleanType.NO.getCode())) {
				disableButton = true;
			}

			/** Si el deposito es manual, no se ejecuta la tranferencia */
			monitorTO.setDisableButton(disableButton);

			paymentAgentJuridic = monitorService
					.getPaymentAgentJuridic(detailCollectionGloss.getIdDetailCollectionGlossPk());
			if (paymentAgentJuridic != null)
				monitorTO.setDescJuridicPaymentAgent(paymentAgentJuridic.getDescription());
			else
				monitorTO.setDescJuridicPaymentAgent(CommonsUtilities.STR_BLANK);

			monitorTO.setPaymentDate(detailCollectionGloss.getPaymentDate());
			monitorTO.setGloss(detailCollectionGloss.getGlosaNotClassified());

			monitorTO.setIdParticipantPk(detailCollectionGloss.getIdParticipantPk());
			monitorTO.setMnemonic(
					monitorService.find(Participant.class, searchFilterMonitorTO.getIdParticipantPk()).getMnemonic());

			/** Verificando se ya se ejecuto el deposito */
			// if(detailCollectionGloss.getTransferStatus()!=null&&detailCollectionGloss.getTransferStatus().equals(DepositStateType.DEPOSITED.getCode())){
			if (detailCollectionGloss.getAmountDeposited() != null && (detailCollectionGloss.getAmountDeposited()
					.compareTo(detailCollectionGloss.getAmountReceivable()) == 0
					|| detailCollectionGloss.getAmountDeposited()
							.compareTo(detailCollectionGloss.getAmountReceivable()) == 1)) {
				monitorTO.setPaidObligation(true);
				monitorTO.setPaidObligationDesc(BooleanType.YES.getValue());
			} else {
				monitorTO.setPaidObligation(false);
				monitorTO.setPaidObligationDesc(BooleanType.NO.getValue());
			}
			monitorTO.setCurrency(detailCollectionGloss.getCurrency());
			monitorTO.setCurrencyDesc(
					monitorService.find(ParameterTable.class, searchFilterMonitorTO.getCurrency()).getText1());

			totalDetailObjectMonitorTO = monitorService.getDetailObjectMonitorTO(
					detailCollectionGloss.getIdParticipantPk(), detailCollectionGloss.getRegistreDate(),
					detailCollectionGloss.getGlosaNotClassified());
			// totalDetailObjectMonitorTO =
			// monitorService.getDetailObjectMonitorTO(detailCollectionGloss.getIdParticipantPk(),
			// detailCollectionGloss.getPaymentDate(),
			// detailCollectionGloss.getGlosaNotClassified());
			/* Estado de transferencia */

			monitorTO.setDescTransferStatus(getDescStateTransfer(detailCollectionGloss.getTransferStatus()));
			/** Monto a cobrar segun glosa */
			monitorTO.setAmountReceivable(detailCollectionGloss.getAmountReceivable());
			monitorTO.setAmountDeposited(detailCollectionGloss.getAmountDeposited() == null ? BigDecimal.ZERO
					: detailCollectionGloss.getAmountDeposited());
			/** Monto recibido */
			monitorDetailCollection = new ResultMonitorSearchTO();
			monitorDetailCollection.setMonitorTO(monitorTO);
			monitorDetailCollection
					.setDetailObjectMonitorTOs(totalDetailObjectMonitorTO.getLisDetailObjectMonitorTOs());
			// obteniendo los contactos agentes pagadores
			monitorDetailCollection
					.setLstGlossNoticationDetail(monitorService.getLstGlossNoticationDetailOfOneDetailCollectionGloss(
							detailCollectionGloss.getIdDetailCollectionGlossPk()));
			listResult.add(monitorDetailCollection);
		}
		return new GenericDataModel<>(listResult);
	}

	private String getDescStateTransfer(Integer state) {
		if (state != null) {
			if (state.equals(RetirementStateType.REGISTERED.getCode()))
				return RetirementStateType.REGISTERED.getValue();
			if (state.equals(RetirementStateType.REJECTED.getCode()))
				return RetirementStateType.REJECTED.getValue();
			if (state.equals(RetirementStateType.CONFIRMED.getCode()))
				return RetirementStateType.CONFIRMED.getValue();
		}
		return "Sin estado";
	}

	/**
	 * Cargar monedas de cobro
	 */

	public List<ParameterTable> listCurrencyForCollection() {
		List<ParameterTable> list = new ArrayList<>();

		list.add(monitorService.find(ParameterTable.class, CurrencyType.USD.getCode()));
		list.add(monitorService.find(ParameterTable.class, CurrencyType.PYG.getCode()));

		return list;
	}

	/**
	 * Bucamos la cuenta de efectivo registrada para el pago de derechos economicos
	 * 
	 * @param institutionCashAccount Cuenta de efectivo base para hacer la busqueda
	 */
	public InstitutionCashAccount searchInstitutionCashAccount(InstitutionCashAccount institutionCashAccount) {
		return manageRetirementServiceBean.getActivatedCashAccount(institutionCashAccount);
	}

	public List<String> getListGlossCollection(Date registreDate, Long idParticipantPk, Integer transferStatus) {
		return manageRetirementServiceBean.getListGlossCollection(registreDate, idParticipantPk, transferStatus);
	}

	public List<String> getListGlossCollection(Date registreDate, Long idParticipantPk, Integer depositStatus,
			Integer transferStatus) {
		return manageRetirementServiceBean.getListGlossCollection(registreDate, idParticipantPk, depositStatus,
				transferStatus);
	}

	public List<String> getListGlossCollectionForDeposit(Date registreDate, Long idParticipantPk) {
		return manageRetirementServiceBean.getListGlossCollectionForDeposit(registreDate, idParticipantPk);
	}

	public List<String> getListGlossCollectionForDeposit(Date registreDate, Long idParticipantPk, Long idBank,
			boolean depositBoolean) {
		return manageRetirementServiceBean.getListGlossCollectionForDeposit(registreDate, idParticipantPk, idBank,
				depositBoolean);
	}

	public List<DetailCollectionGloss> searchDetailCollectionGlosList(RetirementObjetTO retirementObjetTO) {
		return manageRetirementServiceBean.searchDetailCollectionGlossList(retirementObjetTO);
	}

	public void updateTranferCollection(DetailCollectionGloss collectionGloss) throws ServiceException {
		manageRetirementServiceBean.updateWithOutFlush(collectionGloss);
	}

	/* Registrar operacion de retiro */
	public Integer registerTransferOperationForCollection(RetirementObjetTO retirementObjetTO) throws ServiceException {
		FundsTransferRegisterTO objFundsTransferRegisterTO = null;
		LoggerUser loggerUser = null;
			objFundsTransferRegisterTO = buildTransferObject(retirementObjetTO.getCurrency(),CommonsUtilities.currentDate());
			collectionServiceConsumer.getObjectRetirement(objFundsTransferRegisterTO.getAutomaticSendTypeTO(),
					retirementObjetTO);
			loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if (loggerUser == null || loggerUser.getIdPrivilegeOfSystem() == null) {
			if (loggerUser == null) {
				loggerUser = new LoggerUser();
				loggerUser.setUserAction(new UserAcctions());
			}
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		/** Respuesta */
		XmlObjectResponse xmlObjectResponse = null;
		try {
			xmlObjectResponse = collectionServiceConsumer.sendTransferForCollections(objFundsTransferRegisterTO,
					loggerUser);
			if (xmlObjectResponse != null && xmlObjectResponse.getReturnCode().equals("0000")) {
				return RetirementStateType.CONFIRMED.getCode();
			} else {// 0017 fuera de horario
				log.error("CDE:::: Error al momento de ejecutar la transferencia ");
				return null;
			}
			//verificar saldo
			
		} catch (ServiceException e) {
			log.info("CDE::::::No se pudo ejecutar la transferencia.");
		}
		return null;
	}
	
	
	/**
	 * Verificacion de saldo en el BCB
	 * 
	 * */
	public BigDecimal verifyBalance(Integer currency) {
		try {
			Map<String, Object> parameters = new ArrayMap<String, Object>();
			parameters.put("pk", currency);
			Integer currencyBcb = monitorService.findObjectByNamedQuery(ParameterTable.FIND_BCB_CODE_CURRENCY, parameters);
			return collectionServiceConsumer.verifyBalance(currencyBcb);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * Construimos el objeto de tranferencia
	 */
	private FundsTransferRegisterTO buildTransferObject(Integer currency, Date operationaDate) throws ServiceException {

		Long idSecuenceSendLip = getSecuenceSendFunds();

		String[] parametersEdb = null;
		FundsTransferRegisterTO objFundsTransferRegisterTO = new FundsTransferRegisterTO();
		AutomaticSendType objAutomaticSendType = new AutomaticSendType();
		parametersEdb = getEDBInformationCollectionEconomic(currency);
		if (parametersEdb != null) {
			objAutomaticSendType.setCodTypeOperation(ComponentConstant.TRANSFERS_WITH_OPEN_GLOSS);
			objAutomaticSendType.setDate(operationaDate);
			String correlative = CommonsUtilities.completeLeftZero(idSecuenceSendLip.toString(), 3);
			String operationDateFormat = CommonsUtilities.convertDatetoStringOptional(objAutomaticSendType.getDate());
			String finalCorrelative = parametersEdb[0] + FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB
					+ operationDateFormat + correlative;
			// nro_correlativo
			objAutomaticSendType.setCorrelativeNumber(finalCorrelative);
			objAutomaticSendType.setOriginParticipantCode(parametersEdb[0]);
			objAutomaticSendType.setOriginBankAccountCode(parametersEdb[1]);
			objAutomaticSendType.setOriginCurrency(parametersEdb[2]);
			objAutomaticSendType.setTargetCurrency(parametersEdb[2]);
			objAutomaticSendType.setOriginDepartment(FundsAutomaticConstants.DEPARTMENT_DEFECT_BCB);
		}
		objFundsTransferRegisterTO.setAutomaticSendTypeTO(objAutomaticSendType);
		if (parametersEdb == null || parametersEdb.length <= 0) {
			throw new ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_NOTEXITS_BANK_ACCOUNT_EDB);
		} else {
			return objFundsTransferRegisterTO;
		}
	}

	public Long getSecuenceSendFunds() throws ServiceException {
		return collectionServiceConsumer.getNextCorrelativeOperations(GeneralConstants.SQ_SEND_LIP_CORP_PK);
	}

	/**
	 * Informacion de datos de retiro
	 */
	public String[] getEDBInformationCollectionEconomic(Integer currency) throws ServiceException {
		String[] strParameters = null;
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(mnemonicParticipantEDB);
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		strParameters = new String[6];

		strParameters[0] = objParticipant.getIdBcbCode().toString();
		strParameters[1] = accountNumberCollection;
		ParameterTable objParameterTable = participantServiceBean.find(currency, ParameterTable.class);
		strParameters[2] = objParameterTable.getShortInteger().toString();
		strParameters[3] = objParticipant.getIdParticipantPk().toString();
		strParameters[4] = objParticipant.getMnemonic();
		strParameters[5] = objParameterTable.getParameterTablePk().toString();
		return strParameters;
	}

	/**
	 * metodo para cambiar el estado de un deposito
	 * 
	 * @param gloss
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public void saveChangeStatusDeposit(DetailCollectionGloss gloss) throws ServiceException {
		monitorService.updateWithOutFlush(gloss);
	}

	/**
	 * Registrando el deposito manual
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public synchronized void saveManualDeposit(DetailCollectionGloss gloss) {
		/* sorce 1 = LIP 0 = manual */
		gloss.setSourceDeposit(BooleanType.NO.getCode());
		gloss.setDepositStatus(DepositStateType.REGISTERED.getCode());
		try {
			monitorService.updateWithOutFlush(gloss);

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Registrando el retiro manual
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public synchronized void saveManualRetirement(DetailCollectionGloss gloss) {
		/* sorce 1 = LIP 0 = manual */
		gloss.setSourceDeposit(BooleanType.NO.getCode());
		gloss.setTransferStatus(RetirementStateType.REGISTERED.getCode());
		try {
			monitorService.updateWithOutFlush(gloss);

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Buscamos cualquier valor que tenga la misma glosa, ya que tendra la misma
	 * cuenta bancaria y nuemro de cuenta
	 */
	public CollectionEconomicRigth searchCollectiionEconomicByGloss(String idSecurityCodePk, String gloss) {
		return monitorService.searchCollectionEconomicByGloss(idSecurityCodePk, null, gloss);
	}

	/** Busqueda de cuenta de efectivo y banco */
	public Long searchIdBankPkByInstitutionCashAccount(Long idInstitutionCashAccountPk) {
		Long idInstitutionBankAccountPk = manageRetirementServiceBean
				.searhIdInstitutionBankAccountPk(idInstitutionCashAccountPk);
		Long idBankPk = manageRetirementServiceBean.getIdBankPkByInstitutionBankAccount(idInstitutionBankAccountPk);
		return idBankPk;
	}

	/**
	 * Registrando el deposito manual
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public synchronized void saveManualFundCollection(FundsCollection fundsCollection) {
		fundsCollection.setOperationState(DepositStateType.REGISTERED.getCode());
		try {
			monitorService.updateWithOutFlush(fundsCollection);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	public Participant getParticipantByCodeBcb(String idBcbCode) {
		return manageRetirementServiceBean.getParticipantByCodeBcb(idBcbCode);
	}

	public Bank getBankByCodeBcb(String idBcbCode) {
		return manageRetirementServiceBean.getBankByCodeBcb(idBcbCode);
	}

	/** Buscar la moneda equivalente */
	public Integer findEquivalentCurrency(Integer currencyFormatBcb) {
		return manageRetirementServiceBean.getEquivalentCurrency(currencyFormatBcb);
	}

	/**
	 * Registrando el deposito manual
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public synchronized void confirmFundCollection(FundsCollection fundsCollection) {
		fundsCollection.setOperationState(DepositStateType.CONFIRMED.getCode());

		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setGloss(fundsCollection.getGloss());
		List<DetailCollectionGloss> collectionGlossList = manageRetirementServiceBean
				.searchDetailCollectionGlossList(retirementObjetTO);
		try {
			DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
			collectionGloss.setAmountDeposited(fundsCollection.getAmount());
			collectionGloss.setSourceDeposit(BooleanType.YES.getCode());//AUtomatico por natchero
			collectionGloss.setDepositStatus(DepositStateType.CONFIRMED.getCode());

			monitorService.updateWithOutFlush(collectionGloss);

			monitorService.updateWithOutFlush(fundsCollection);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Buscamos todas las glosas equivalentes registradas
	 */
	public List<DetailMatchGlossTO> searchGlossMatchs(ManualDepositTO searchDepositTO) {
		DetailMatchGlossTO detailMatchGlossTO = null;
		ArrayList<DetailMatchGlossTO> list = new ArrayList<>();
		List<Object[]> listResult = monitorService.searchGlossMatchs(searchDepositTO);
		for (Object[] objects : listResult) {
			detailMatchGlossTO = new DetailMatchGlossTO();
			/* Id de la tabla de matcheo de glosas */
			detailMatchGlossTO.setIdEqDepositGlossesPk(new BigDecimal(objects[0].toString()).longValue());
			/* Descripsion de participante */
			detailMatchGlossTO.setDescParticipant(objects[1].toString());
			/* Glosa */
			detailMatchGlossTO.setGloss(objects[2].toString());
			/* descripsion de agente pagador */
			detailMatchGlossTO.setDescPaymentAgent(objects[3].toString());
			/* Fecha de pago */
			detailMatchGlossTO.setPaymentDate(CommonsUtilities.convertObjectToDate(objects[4]));
			/* Agregando moneda */
			detailMatchGlossTO.setCurrency(new BigDecimal(objects[5].toString()).intValue());
			/* Agregando monto a cobrar */
			detailMatchGlossTO.setAmountReceivable(new BigDecimal(objects[6].toString()));
			/* Numero de operacion LIP */
			detailMatchGlossTO.setRequestNumber(new BigDecimal(objects[7].toString()).longValue());
			/* Agregando monto */
			detailMatchGlossTO.setAmount(objects[8] == null ? BigDecimal.ZERO : new BigDecimal(objects[8].toString()));
			/* agregando pk de participante */
			detailMatchGlossTO
					.setFilterIdParticipantPk(objects[9] == null ? null : Long.parseLong(objects[9].toString()));
			/* agregando moneda */
			detailMatchGlossTO.setFilterCurrency(objects[10] == null ? null : Integer.parseInt(objects[10].toString()));
			/* agregando filtro de fecha de cobro */
			detailMatchGlossTO.setFilterPaymentDate(CommonsUtilities.convertObjectToDate(objects[11]));
			/* Agregado filtro de glosa */
			detailMatchGlossTO.setFilterGloss(objects[12] == null ? null : objects[12].toString());
			/* Agregando la glosa seleccionada */
			detailMatchGlossTO.setIdDetailCollectionGloss(Long.parseLong(objects[13].toString()));
			/* Glosa de deposito del LIP */
			// detailMatchGlossTO.setGlossLip(objects[15]==null?CommonsUtilities.STR_BLANK:objects[15].toString());
			/* Estado de la solicitud */
			detailMatchGlossTO.setStateDesc(objects[16] == null ? CommonsUtilities.STR_BLANK : objects[16].toString());
			/* Lista de fondos selecionados */
			List<String> listString = CommonsUtilities.getListOfComponentsForUpdate(objects[17].toString());
			List<FundsCollection> listFundsCollections = new ArrayList<>();
			FundsCollection collection = null;
			StringBuilder listGlossLip = new StringBuilder();
			for (String string : listString) {
				collection = new FundsCollection();
				collection = monitorService.find(FundsCollection.class, new Long(string));
				if (collection == null)
					continue;
				listFundsCollections.add(collection);
				if (collection.getGloss() != null)
					listGlossLip.append(" " + collection.getGloss());
			}
			/* Agregando a la lista */
			detailMatchGlossTO.setGlossLip(listGlossLip.toString());
			detailMatchGlossTO.setListFundsCollection(listFundsCollections);
			list.add(detailMatchGlossTO);
		}
		return list;
	}

	/**
	 * Busqueda de detalle de deposito y envio de fondos(tabla principal)
	 */
	public List<DetailCollectionGloss> searchGlossDetails(Long idEqDepositGlossesPk) {
		PaymentAgent paymentAgent = null;
		ParameterTable parameterTable = null;
		List<DetailCollectionGloss> list = monitorService.searchDetailsCollectionGloss(idEqDepositGlossesPk);
		for (DetailCollectionGloss detailCollectionGloss : list) {
			paymentAgent = monitorService.getPaymentAgentJuridic(detailCollectionGloss.getIdDetailCollectionGlossPk());
			detailCollectionGloss.setPaymentAgentDesc(paymentAgent.getDescription());
			parameterTable = monitorService.find(ParameterTable.class, detailCollectionGloss.getCurrency());
			detailCollectionGloss.setCurrencyDesc(parameterTable.getDescription());
		}
		return list;
	}

	/**
	 * Busqueda de detalle de deposito y envio de fondos(tabla principal)
	 */
	public List<DetailCollectionGloss> searchDetailsCollectionGlossNotConfirmed(ManualDepositTO registreDepositTO) {
		PaymentAgent paymentAgent = null;
		ParameterTable parameterTable = null;
		List<DetailCollectionGloss> list = monitorService.searchDetailsCollectionGlossNotConfirmed(
				registreDepositTO.getIdParticipantPk(), registreDepositTO.getPaymentDate(),
				registreDepositTO.getCurrency(), registreDepositTO.getGloss());
		for (DetailCollectionGloss detailCollectionGloss : list) {
			paymentAgent = monitorService.getPaymentAgentJuridic(detailCollectionGloss.getIdDetailCollectionGlossPk());
			detailCollectionGloss.setPaymentAgentDesc(paymentAgent.getDescription());
			parameterTable = monitorService.find(ParameterTable.class, detailCollectionGloss.getCurrency());
			detailCollectionGloss.setCurrencyDesc(parameterTable.getDescription());
			detailCollectionGloss
					.setAmountDeposited(detailCollectionGloss.getAmountDeposited() == null ? BigDecimal.ZERO
							: detailCollectionGloss.getAmountDeposited());
		}
		return list;
	}

	public DetailCollectionGloss findDetailCollectionGloss(Long idDetailCollectionGlossPk) {
		return monitorService.find(DetailCollectionGloss.class, idDetailCollectionGlossPk);
	}

	/**
	 * Registro de igualacion de glosa
	 * 
	 * @throws ServiceException
	 */
	public Long recordGlossMatch(EquivalentDepositTO eqDepositMatch, UserInfo userInfo, String gloss)
			throws ServiceException {
		EquivalentDepositGlosses eqDepositGlosses = null;
		eqDepositGlosses = new EquivalentDepositGlosses();
		eqDepositGlosses
				.setIdDetailCollectionGlossFk(new DetailCollectionGloss(eqDepositMatch.getIdDetailCollectionGlossPk()));

		/* Concatenamos los fondos seleccionados */
		String stringFundsSelecteds = null;
		StringBuilder builderFundsSelecteds = new StringBuilder();
		for (FundsCollection fundsCollection : eqDepositMatch.getListFundCollections()) {
			builderFundsSelecteds.append(fundsCollection.getIdFundsCollectionPk().toString());
			builderFundsSelecteds.append(",");
		}
		stringFundsSelecteds = builderFundsSelecteds.toString().substring(0,
				builderFundsSelecteds.toString().length() - 1);
		eqDepositGlosses.setListFundsCollection(stringFundsSelecteds);

		/* Concatenamos los fondos mostrados en la pantalla de registro */
		String stringFundsView = null;
		StringBuilder builderFundsView = new StringBuilder();
		for (FundsCollection fundsCollectionView : eqDepositMatch.getListFundsCollectionShow()) {
			builderFundsView.append(fundsCollectionView.getIdFundsCollectionPk().toString());
			builderFundsView.append(",");
		}
		stringFundsView = builderFundsView.toString().substring(0, builderFundsView.toString().length() - 1);
		eqDepositGlosses.setListFundsCollectionShow(stringFundsView);

		/* Concatenamos las glosas mostradas en la pantalla de registro */
		String stringGlossView = null;
		StringBuilder builderGlossView = new StringBuilder();
		for (DetailCollectionGloss detailCollectionGloss : eqDepositMatch.getListDetailCollectionShow()) {
			builderGlossView.append(detailCollectionGloss.getIdDetailCollectionGlossPk().toString());
			builderGlossView.append(",");
		}
		stringGlossView = builderGlossView.toString().substring(0, builderGlossView.toString().length() - 1);
		eqDepositGlosses.setListDetailCollectionShow(stringGlossView);

		eqDepositGlosses.setRegisterUser(userInfo.getUserAccountSession().getUserName());
		eqDepositGlosses.setRegistreDate(CommonsUtilities.currentDate());
		eqDepositGlosses.setOperationState(StatesConstants.REGISTERED_MATCH_GLOSSES);
		/* Campos de auditoria */
		eqDepositGlosses.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		eqDepositGlosses.setLastModifyDate(CommonsUtilities.currentDate());
		eqDepositGlosses.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		eqDepositGlosses.setLastModifyApp(userInfo.getLastModifyPriv());
		/* Registrando filtros de busqueda */
		eqDepositGlosses.setIdParticipantFk(eqDepositMatch.getIdParticipantPk());
		eqDepositGlosses.setCurrency(eqDepositMatch.getCurrency());
		eqDepositGlosses.setPaymentDate(eqDepositMatch.getPaymentDate());
		eqDepositGlosses.setGloss(eqDepositMatch.getGloss());
		/* Sumando monto de deposito */
		BigDecimal totalAmount = BigDecimal.ZERO;
		/* Actualizar estados de los fondos registrados */
		for (FundsCollection fundsCollection : eqDepositMatch.getListFundCollections()) {
			totalAmount = totalAmount.add(fundsCollection.getAmount());
			fundsCollection.setOperationState(StatesConstants.REGISTERED_MATCH_GLOSSES);
			monitorService.update(fundsCollection);
		}
		Integer currencyBcb = eqDepositGlosses.getCurrency() == CurrencyType.PYG.getCode() ? 69 : 34;
		BigDecimal sumConfirmedFundCollection = monitorService.getSumFundCollection(gloss, currencyBcb.shortValue());
		totalAmount = totalAmount
				.add(sumConfirmedFundCollection == null ? BigDecimal.ZERO : sumConfirmedFundCollection);
		eqDepositGlosses.setTotalAmount(totalAmount);
		monitorService.createWithFlush(eqDepositGlosses);

		return eqDepositGlosses.getIdEqDepositGlossesPk();

	}

	/**
	 * Buscamos registros de todos los depositos
	 */
	public List<FundsCollection> searchFundCollections(Date registreDate, Integer currency, boolean enableStete,
			String gloss) {
		return monitorService.searchFundCollections(registreDate, currency, enableStete, gloss);
	}

	/**
	 * Buscamos registros de todos los depositos
	 */
	public List<FundsCollection> searchFundCollections(Long idFundCollectionPk) {
		return monitorService.searchFundCollections(idFundCollectionPk);
	}

	/**
	 * Verificamos si ya existe un registro previo
	 */
	public boolean isExistRegisteredGloss(Long idDetailCollectionGlossPk) {
		return monitorService.isExistRegisteredGloss(idDetailCollectionGlossPk);
	}

	/**
	 * registro de rechazo
	 * 
	 * @throws ServiceException
	 */
	public void recordGlossRejection(DetailMatchGlossTO detailMatchGlossTO, List<FundsCollection> listCollections)
			throws ServiceException {
		monitorService.recordGlossRejection(detailMatchGlossTO, listCollections);
	}

	public void recordGlossConfirm(DetailMatchGlossTO detailMatchGlossTO, List<FundsCollection> fundsCollections)
			throws ServiceException {
		monitorService.recordGlossConfirm(detailMatchGlossTO, fundsCollections);
	}

	/**
	 * Obetenemos el estado de la operacion de igualacion de glosa
	 */
	public Integer getOperationStateMatchGloss(Long idEqDepositGlossesPk) {
		return monitorService.getOperationStateMatchGloss(idEqDepositGlossesPk);
	}

	/**
	 * Buscar si un registro de glosa ya existe en algun registro
	 */
	public boolean isExistRegisterGloss(Long idDetailCollectionGlossPk) {
		return monitorService.isExistRegisteredGloss(idDetailCollectionGlossPk);
	}
}