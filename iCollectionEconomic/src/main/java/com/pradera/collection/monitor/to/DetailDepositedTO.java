package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class DetailDepositedTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3238381531470464219L;

	private BigDecimal amount;
	
	private Integer currency;
	
	public DetailDepositedTO() {
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
}
