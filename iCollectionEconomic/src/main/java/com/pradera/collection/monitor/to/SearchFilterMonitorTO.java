package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * @author hcoarite
 *
 */
public class SearchFilterMonitorTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -509053716805859586L;
	
	private Long idParticipantPk;
	
	private Integer currency;
	
	private Issuer issuer;
	
	private Date datePayment;
	
	private Date maxDate;
	
	private Date minDate;
	
	private String glossCode;
	
	public SearchFilterMonitorTO() {
		datePayment = CommonsUtilities.currentDate();
		issuer = new Issuer();
		idParticipantPk =  null;
		minDate = CommonsUtilities.currentDate();
		maxDate = CommonsUtilities.currentDate();
	}
	public SearchFilterMonitorTO(Long idParticipantPk) {
		issuer = new Issuer();
		minDate = CommonsUtilities.currentDate();
		maxDate = CommonsUtilities.currentDate();
		this.idParticipantPk = idParticipantPk;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Date getDatePayment() {
		return datePayment;
	}
	public void setDatePayment(Date datePayment) {
		this.datePayment = datePayment;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public String getGlossCode() {
		return glossCode;
	}
	public void setGlossCode(String glossCode) {
		this.glossCode = glossCode;
	}
	
}
