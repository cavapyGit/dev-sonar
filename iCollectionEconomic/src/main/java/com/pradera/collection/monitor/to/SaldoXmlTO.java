package com.pradera.collection.monitor.to;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.pradera.integration.component.business.ComponentConstant;

@XmlRootElement(name = "saldo")
public class SaldoXmlTO {
	
	/*
	<saldo>
	<cod_tipo_operacion>S02</cod_tipo_operacion>
	<cod_participante>1424</cod_participante>
	<cod_cuenta>8046</cod_cuenta>
	<cod_moneda>69<cod_moneda>
	<fecha>27/01/2021</fecha>
	</saldo>*/
	
	@XmlElement(name = "cod_tipo_operacion", required = true)
	private String codTipoOperacion = ComponentConstant.INTERFACE_BCB_LIP_BALANCE;
	
	@XmlElement(name = "cod_participante", required = true)
	private Integer codParticipante = 1424;
	
	@XmlElement(name = "cod_cuenta", required = true)
	private Integer codCuenta = 8046;
	
	@XmlElement(name = "cod_cuenta", required = true)
	private Integer codMoneda;
	
	@XmlElement(name = "fecha", required = true)
	private Integer fecha;

	public String getCodTipoOperacion() {
		return codTipoOperacion;
	}

	public void setCodTipoOperacion(String codTipoOperacion) {
		this.codTipoOperacion = codTipoOperacion;
	}

	public Integer getCodParticipante() {
		return codParticipante;
	}

	public void setCodParticipante(Integer codParticipante) {
		this.codParticipante = codParticipante;
	}

	public Integer getCodCuenta() {
		return codCuenta;
	}

	public void setCodCuenta(Integer codCuenta) {
		this.codCuenta = codCuenta;
	}

	public Integer getCodMoneda() {
		return codMoneda;
	}

	public void setCodMoneda(Integer codMoneda) {
		this.codMoneda = codMoneda;
	}

	public Integer getFecha() {
		return fecha;
	}

	public void setFecha(Integer fecha) {
		this.fecha = fecha;
	}

}
