package com.pradera.collection.monitor.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.ResultMonitorSearchTO;
import com.pradera.collection.monitor.to.SearchFilterMonitorTO;
import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.util.GenerateAccreditationGertificateUtil;
import com.pradera.collection.right.util.GenerateUrlNotification;
import com.pradera.collection.right.util.NotificationConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.funds.InstitutionCashAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.pradera.model.process.BusinessProcess;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.GlossNoticationDetail;


@DepositaryWebBean
@LoggerCreateBean
public class SearchForwardNotificationCollectionMgmtBean extends GenericBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Participant> listParticipants;
	
	private GenericDataModel<ResultMonitorSearchTO> listResultMonitorTOs;
	
	@Inject
	private GenerateAccreditationGertificateUtil accreditationGertificateUtil;
	
	@Inject
	NotificationServiceFacade notificationServiceFacade;
	
	@Inject
	private CollectionRigthFacade facadeCollection;
	
	private List<ParameterTable> lstCurrency;
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	private ResultMonitorSearchTO monitorTOviewDetaiil;
	
	private ResultMonitorSearchTO monitorToPay;
	
	private boolean visibleResult;
	boolean renderGlossDetailToResentEmail;
	
	@Inject
	private UserInfo userInfo;
	
	private SearchFilterMonitorTO searchFilterMonitorTO;
	
	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;
	
	private BigDecimal totalAmountReceivableBOB;
	private BigDecimal totalAmountReceivableUSD;
    private BigDecimal totalAmountReceived;
    private BigDecimal difference;
	
    @Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;
    
	@PostConstruct
	public void init(){
		listParticipants = parameterServiceBean.getListParticipantForCollection();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterMonitorTO = new SearchFilterMonitorTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterMonitorTO = new SearchFilterMonitorTO();
		//lstCurrency = parameterServiceBean.getListParameterTableServiceBean(53);
		lstCurrency = collectionEconomicFacade.listCurrencyForCollection();
		/*Solo cargar BOB y USD*/
		searchFilterMonitorTO.setMaxDate(new Date());
		searchFilterMonitorTO.setMinDate(holidayQueryServiceBean.subtractBusinessDays(new Date(), 3));
	}
	public File getFileTemporal(byte[] fileArray){
		File temp = null;
		OutputStream opStream;
		try {
			temp = File.createTempFile("Nota",".pdf");
			opStream = new FileOutputStream(temp);
			opStream.write(fileArray);
	        opStream.flush();
		} catch (Exception e) {
			System.out.println("::: error en la converson de bytes a pdf");
		}
        return temp;
	}
	//TODO metodo en cosntruccion
	@LoggerAuditWeb
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void executeResentNotification() {
		
		//verificando si hay al menos un registro seleccionado para realizar el reenvio
		boolean existRowSelected = false;
		for (GlossNoticationDetail contactAgentPayment : monitorTOviewDetaiil.getLstGlossNoticationDetail()) {
			if (contactAgentPayment.isSelected()) {
				existRowSelected = true;
				break;
			}
		}
		if(existRowSelected){
			//este objeto sirve para recuperar (los id de los cat enviados para notificar al contanct agent payment)
			DetailCollectionGloss glosa = collectionEconomicFacade.getLstIdCollectionEconomicRigthForOneGloss(monitorTOviewDetaiil.getMonitorTO().getGloss());
			
			List<File> listFileReSenderNotification;
			List<File> listFileCat = new ArrayList<>();
			File fileReSenderNotification;
			String[] lstIdCatsSender=glosa.getIdCatListaText().split(",");
			//recuperando los cats para su reenvio
			for(String idCat:lstIdCatsSender){
				fileReSenderNotification=accreditationGertificateUtil.getCertificateAccreditationFromBBDD(Long.parseLong(idCat));
				if(fileReSenderNotification!=null){
					listFileCat.add(fileReSenderNotification);	
				}
			}
			
			//paramentrso para ejecutar el envio
			List<UserAccountSession> lstUserAccount = new ArrayList<>();
			UserAccountSession accountSession;
			String userName = userInfo.getUserAccountSession().getUserName();
			BusinessProcess bp = new BusinessProcess(BusinessProcessType.NOTIFICATION_CERTIFICATE_ACCREDITATION_PAYMENT_AGENT.getCode(), "");

			for (GlossNoticationDetail contactAgentPayment : monitorTOviewDetaiil.getLstGlossNoticationDetail()) {
				//ejecutando reenvio solo con los contacto que fueron seleccionados 
				if (contactAgentPayment.isSelected()) {
					accountSession = new UserAccountSession();
					accountSession.setUserName(contactAgentPayment.getIdFkPaymentAgent().getFirstName());
					accountSession.setEmail(contactAgentPayment.getIdFkPaymentAgent().getEmail());
					accountSession.setPhoneNumber(contactAgentPayment.getIdFkPaymentAgent().getCellPhone());
					accountSession.setFullName(contactAgentPayment.getIdFkPaymentAgent().getFirstName());
					lstUserAccount = new ArrayList<UserAccountSession>();
					lstUserAccount.add(accountSession);
					
					fileReSenderNotification=getFileTemporal(contactAgentPayment.getGlossPdf());
					listFileReSenderNotification=new ArrayList<>();
					listFileReSenderNotification.add(fileReSenderNotification);
					listFileReSenderNotification.addAll(listFileCat);

					//realizado el reenvio con corrreo electronico
					notificationServiceFacade.sendNotificationManualEmailAdjuntHtmlContent(userName, bp, lstUserAccount,
							NotificationConstants.SUBJECT_EMAIL_PAYMENT_AGENT, contactAgentPayment.getHtmlMessage(), null, listFileReSenderNotification);
					
					//realizar modificacion de reenvio a este contacto agente pagador 
					contactAgentPayment.setNotificationSent(NotificationConstants.RESENDER_NOTIFICATION);
					collectionEconomicFacade.modifiedIndReSenderNotification(contactAgentPayment);
				}
			}
			
			String headerMessage = PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities
					.getMessage("lbl.message.successful.registre.resender.notification");
			showMessageOnDialogRegCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities
					.executeJavascriptFunction("PF('wvAlertResenderNotification').show();");
		}else{
			//enviando mensaje al usuario de que tiene que seleccionar al menos un registro
			String headerMessage = PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities
					.getMessage("lbl.requiered.contactpaymet.message");
			showMessageOnDialogRegCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogsRequired");
			JSFUtilities
					.executeJavascriptFunction("PF('wvMessageContactPaymentRequired').show();");
		}
		
		
	}
	private void showMessageOnDialogRegCollection(
			String headerMessageConfirmation, String bodyMessageConfirmation) {
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	public void searchFromFilters(){
		listResultMonitorTOs = collectionEconomicFacade.searchExpirationClassifiedByGlossToReenvio(searchFilterMonitorTO);
		
		totalAmountReceivableBOB =  BigDecimal.ZERO;
		totalAmountReceivableUSD =  BigDecimal.ZERO;
		totalAmountReceived =  BigDecimal.ZERO;
		difference =  BigDecimal.ZERO;
		/**Sumando montos totales en moneda BOB*/
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.PYG.getCode()))
			for (ResultMonitorSearchTO monitorSearchTO : listResultMonitorTOs) {
				totalAmountReceivableBOB = totalAmountReceivableBOB.add(monitorSearchTO.getMonitorTO().getAmountReceivable());
				totalAmountReceived = totalAmountReceived.add(monitorSearchTO.getMonitorTO().getAmountDeposited());
			}
			/**Sumando montos totales en moneda USD*/
			if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.USD.getCode()))
			for (ResultMonitorSearchTO monitorSearchTO : listResultMonitorTOs) {
				totalAmountReceivableUSD = totalAmountReceivableUSD.add(monitorSearchTO.getMonitorTO().getAmountReceivable());
				totalAmountReceived = totalAmountReceived.add(monitorSearchTO.getMonitorTO().getAmountDeposited());
			}
			
			if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.PYG.getCode()))
				difference = totalAmountReceivableBOB.subtract(totalAmountReceived);
			
			if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.USD.getCode()))
				difference = totalAmountReceivableUSD.subtract(totalAmountReceived);
		
	}
	public void loadDetailCollectionMovements(ActionEvent event){
		monitorTOviewDetaiil = new ResultMonitorSearchTO();
		monitorTOviewDetaiil = (ResultMonitorSearchTO) event.getComponent().getAttributes().get("resultMonitorTOView");
		visibleResult = true;
		
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVDetailCollection').show();");
	
	}
	
	//contacto agente pagadres que se realizo el envio de notificacion de una determinada glosa
	public void loadGlossDetailToResentEmail(ActionEvent event){
		monitorTOviewDetaiil = new ResultMonitorSearchTO();
		monitorTOviewDetaiil = (ResultMonitorSearchTO) event.getComponent().getAttributes().get("resultMonitorResentEmailTOView");
		
		renderGlossDetailToResentEmail = true;
		
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvContactPaymentResentEmail').show();");
	}
	
	public void beforeExecutePay(ActionEvent event){
		
		monitorToPay = new ResultMonitorSearchTO();
		monitorToPay = (ResultMonitorSearchTO) event.getComponent().getAttributes().get("selectedObject");
		
		
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		
		retirementObjetTO.setIdParticipantPk(monitorToPay.getMonitorTO().getIdParticipantPk());
		retirementObjetTO.setCurrency(monitorToPay.getMonitorTO().getCurrency());
		retirementObjetTO.setGloss(monitorToPay.getMonitorTO().getGloss());
		
		List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
		DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
		if(collectionGloss==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.no.exists.registre.deposited");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return ;
		}
		
		if(collectionGloss.getTransferStatus()!=null&&collectionGloss.getTransferStatus().equals(DepositStateType.CONFIRMED.getCode())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.no.process.retired");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return ;
		}
		
		/**Verificando si tiene cuenta de efectivo para el desposito*/
		
		InstitutionCashAccount institutionCashAccount = new InstitutionCashAccount();
		institutionCashAccount.setParticipant(new Participant(monitorToPay.getMonitorTO().getIdParticipantPk()));
		institutionCashAccount.setCurrency(monitorToPay.getMonitorTO().getCurrency());
		
		InstitutionCashAccount searchInstitutionCashAccount = collectionEconomicFacade.searchInstitutionCashAccount(institutionCashAccount);
		if(searchInstitutionCashAccount == null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.no.exist.effective.account.retire");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return ;
		}
		
		
		if(monitorToPay.getMonitorTO().isPaidObligation()){
			Object[] params= new Object[2];
			/*Participante*/
			params[0]= monitorToPay.getMonitorTO().getMnemonic();
			/*Monto a pagar*/
			params[1]= monitorToPay.getMonitorTO().getAmountReceivable();
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.dialog.confirm.retirement",params);
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wvPreExecuteRetirement').show();");
		}else{
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.dialog.insufficient.amounts");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**Ejecutamos el pago*/
	public void executePay(){
		
		if(monitorToPay == null){
			return;
		}
		
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setIdParticipantPk(monitorToPay.getMonitorTO().getIdParticipantPk());
		retirementObjetTO.setPaymentDate(monitorToPay.getMonitorTO().getPaymentDate());
		retirementObjetTO.setCurrency(monitorToPay.getMonitorTO().getCurrency());
		retirementObjetTO.setAmount(monitorToPay.getMonitorTO().getAmountReceivable().setScale(2, RoundingMode.UP));
		retirementObjetTO.setGloss( monitorToPay.getMonitorTO().getGloss());
		retirementObjetTO.setUserName(userInfo.getUserAccountSession().getUserName());
		
		log.info("Iniciando transferencia o retiro de fondos para cobro de derecho economicos");
		try {
			collectionEconomicFacade.registerTransferOperationForCollection(retirementObjetTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	public void clearFilters(){
		//listParticipants = participantServiceBean.getLisParticipantServiceBean();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterMonitorTO = new SearchFilterMonitorTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterMonitorTO = new SearchFilterMonitorTO();
		//lstCurrency = parameterServiceBean.getListParameterTableServiceBean(53);
		/*Solo cargar BOB y USD*/
		searchFilterMonitorTO.setMaxDate(new Date());
		searchFilterMonitorTO.setMinDate(holidayQueryServiceBean.subtractBusinessDays(new Date(), 3));
	}
	public List<Participant> getListParticipants() {
		return listParticipants;
	}
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}
	public SearchFilterMonitorTO getSearchFilterMonitorTO() {
		return searchFilterMonitorTO;
	}
	public void setSearchFilterMonitorTO(SearchFilterMonitorTO searchFilterMonitorTO) {
		this.searchFilterMonitorTO = searchFilterMonitorTO;
	}
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public ParameterServiceBean getParameterServiceBean() {
		return parameterServiceBean;
	}
	public void setParameterServiceBean(ParameterServiceBean parameterServiceBean) {
		this.parameterServiceBean = parameterServiceBean;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public GenericDataModel<ResultMonitorSearchTO> getListResultMonitorTOs() {
		return listResultMonitorTOs;
	}
	public void setListResultMonitorTOs(
			GenericDataModel<ResultMonitorSearchTO> listResultMonitorTOs) {
		this.listResultMonitorTOs = listResultMonitorTOs;
	}
	public CollectionRigthFacade getFacadeCollection() {
		return facadeCollection;
	}
	public void setFacadeCollection(CollectionRigthFacade facadeCollection) {
		this.facadeCollection = facadeCollection;
	}
	public BigDecimal getTotalAmountReceivableBOB() {
		return totalAmountReceivableBOB;
	}
	public void setTotalAmountReceivableBOB(BigDecimal totalAmountReceivableBOB) {
		this.totalAmountReceivableBOB = totalAmountReceivableBOB;
	}
	public BigDecimal getTotalAmountReceivableUSD() {
		return totalAmountReceivableUSD;
	}
	public void setTotalAmountReceivableUSD(BigDecimal totalAmountReceivableUSD) {
		this.totalAmountReceivableUSD = totalAmountReceivableUSD;
	}
	public BigDecimal getTotalAmountReceived() {
		return totalAmountReceived;
	}
	public void setTotalAmountReceived(BigDecimal totalAmountReceived) {
		this.totalAmountReceived = totalAmountReceived;
	}
	public BigDecimal getDifference() {
		return difference;
	}
	public void setDifference(BigDecimal difference) {
		this.difference = difference;
	}
	public ResultMonitorSearchTO getMonitorTOviewDetaiil() {
		return monitorTOviewDetaiil;
	}
	public void setMonitorTOviewDetaiil(ResultMonitorSearchTO monitorTOviewDetaiil) {
		this.monitorTOviewDetaiil = monitorTOviewDetaiil;
	}
	public boolean isRenderGlossDetailToResentEmail() {
		return renderGlossDetailToResentEmail;
	}
	public void setRenderGlossDetailToResentEmail(boolean renderGlossDetailToResentEmail) {
		this.renderGlossDetailToResentEmail = renderGlossDetailToResentEmail;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
}