package com.pradera.collection.monitor.to;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "respuesta")
@XmlAccessorType(XmlAccessType.FIELD) 
public class BalanceReponseXmlTO {

	/**
	 * <respuesta>
   <cod_participante>1424</cod_participante>
   <cod_cuenta>8046</cod_cuenta>
   <cod_moneda>69</cod_moneda>
   <fecha>01/02/2021</fecha>
   <saldo>100000.00</saldo>
   <mensaje_recibido>*/
	
	@XmlElement(name = "cod_participante")
	private Integer codParticipante;
	
	@XmlElement(name = "cod_cuenta")
	private Integer codCuenta;
	
	@XmlElement(name = "cod_moneda")
	private Integer codMoneda;
	
	@XmlElement(name = "fecha")
	private Integer fecha;
	
	@XmlElement(name = "saldo")
	private String saldo;
	
	@XmlElement(name = "mensaje_recibido")
	private String mensajeRecibido;

	public Integer getCodParticipante() {
		return codParticipante;
	}

	public void setCodParticipante(Integer codParticipante) {
		this.codParticipante = codParticipante;
	}

	public Integer getCodCuenta() {
		return codCuenta;
	}

	public void setCodCuenta(Integer codCuenta) {
		this.codCuenta = codCuenta;
	}

	public Integer getCodMoneda() {
		return codMoneda;
	}

	public void setCodMoneda(Integer codMoneda) {
		this.codMoneda = codMoneda;
	}

	public Integer getFecha() {
		return fecha;
	}

	public void setFecha(Integer fecha) {
		this.fecha = fecha;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public String getMensajeRecibido() {
		return mensajeRecibido;
	}

	public void setMensajeRecibido(String mensajeRecibido) {
		this.mensajeRecibido = mensajeRecibido;
	}
	
}
