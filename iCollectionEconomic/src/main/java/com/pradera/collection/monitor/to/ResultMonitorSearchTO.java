package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.edv.collection.economic.rigth.GlossNoticationDetail;

public class ResultMonitorSearchTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1603681336800802300L;

	private ResultMonitorTO monitorTO;
	
	private List<DetailObjectMonitorTO> detailObjectMonitorTOs;
	
	private List<GlossNoticationDetail> lstGlossNoticationDetail;
	
	public ResultMonitorSearchTO() {
		lstGlossNoticationDetail=new ArrayList<>();
	}

	public ResultMonitorTO getMonitorTO() {
		return monitorTO;
	}

	public void setMonitorTO(ResultMonitorTO monitorTO) {
		this.monitorTO = monitorTO;
	}

	public List<GlossNoticationDetail> getLstGlossNoticationDetail() {
		return lstGlossNoticationDetail;
	}

	public void setLstGlossNoticationDetail(List<GlossNoticationDetail> lstGlossNoticationDetail) {
		this.lstGlossNoticationDetail = lstGlossNoticationDetail;
	}

	public List<DetailObjectMonitorTO> getDetailObjectMonitorTOs() {
		return detailObjectMonitorTOs;
	}

	public void setDetailObjectMonitorTOs(
			List<DetailObjectMonitorTO> detailObjectMonitorTOs) {
		this.detailObjectMonitorTOs = detailObjectMonitorTOs;
	}
	
}
