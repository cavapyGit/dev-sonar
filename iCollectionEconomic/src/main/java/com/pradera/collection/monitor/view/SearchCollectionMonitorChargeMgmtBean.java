package com.pradera.collection.monitor.view;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import com.pradera.alertt.bath.to.RetirementObjetTO;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.ResultMonitorSearchTO;
import com.pradera.collection.monitor.to.SearchFilterMonitorTO;
import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.pradera.model.generalparameter.type.RetirementStateType;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;


@DepositaryWebBean
@LoggerCreateBean
public class SearchCollectionMonitorChargeMgmtBean extends GenericBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Participant> listParticipants;
	
	private GenericDataModel<ResultMonitorSearchTO> listResultMonitorTOs;
	
	@Inject
	private CollectionRigthFacade facadeCollection;
	
	private List<ParameterTable> lstCurrency;
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	private ResultMonitorSearchTO monitorTOviewDetaiil;
	
	private ResultMonitorSearchTO monitorToPay;
	
	private boolean visibleResult;
	
	@Inject
	private UserInfo userInfo;
	
	private SearchFilterMonitorTO searchFilterMonitorTO;
	
	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;
	
	private BigDecimal totalAmountReceivableBOB;
	private BigDecimal totalAmountReceivableUSD;
    private BigDecimal totalAmountReceived;
    private BigDecimal difference;
	
    @Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;
    
	@PostConstruct
	public void init(){
		listParticipants = parameterServiceBean.getListParticipantForCollection();
		//listParticipants = participantServiceBean.getLisParticipantServiceBean();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterMonitorTO = new SearchFilterMonitorTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterMonitorTO = new SearchFilterMonitorTO();
		//lstCurrency = parameterServiceBean.getListParameterTableServiceBean(53);
		lstCurrency = collectionEconomicFacade.listCurrencyForCollection();
		/*Solo cargar BOB y USD*/
		searchFilterMonitorTO.setMaxDate(CommonsUtilities.currentDate());
		searchFilterMonitorTO.setMinDate(holidayQueryServiceBean.subtractBusinessDays(CommonsUtilities.currentDate(), 3));
	}
	public void searchFromFilters(){
		listResultMonitorTOs = collectionEconomicFacade.searchExpirationClassifiedByGloss(searchFilterMonitorTO);
		
		totalAmountReceivableBOB =  BigDecimal.ZERO;
		totalAmountReceivableUSD =  BigDecimal.ZERO;
		totalAmountReceived =  BigDecimal.ZERO;
		difference =  BigDecimal.ZERO;
		/**Sumando montos totales en moneda BOB*/
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.PYG.getCode()))
			for (ResultMonitorSearchTO monitorSearchTO : listResultMonitorTOs) {
				totalAmountReceivableBOB = totalAmountReceivableBOB.add(monitorSearchTO.getMonitorTO().getAmountReceivable());
				totalAmountReceived = totalAmountReceived.add(monitorSearchTO.getMonitorTO().getAmountDeposited());
			}
		/**Sumando montos totales en moneda USD*/
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.USD.getCode()))
		for (ResultMonitorSearchTO monitorSearchTO : listResultMonitorTOs) {
			totalAmountReceivableUSD = totalAmountReceivableUSD.add(monitorSearchTO.getMonitorTO().getAmountReceivable());
			totalAmountReceived = totalAmountReceived.add(monitorSearchTO.getMonitorTO().getAmountDeposited());
		}
			
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.PYG.getCode()))
			difference = totalAmountReceivableBOB.subtract(totalAmountReceived);
		
		if(searchFilterMonitorTO.getCurrency().equals(CurrencyType.USD.getCode()))
			difference = totalAmountReceivableUSD.subtract(totalAmountReceived);
		
	}
	public void loadDetailCollectionMovements(ActionEvent event){
		monitorTOviewDetaiil = new ResultMonitorSearchTO();
		monitorTOviewDetaiil = (ResultMonitorSearchTO) event.getComponent().getAttributes().get("resultMonitorTOView");
		visibleResult = true;
		
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wVDetailCollection').show();");
	
	}
	
	public synchronized void beforeExecutePay(ActionEvent event){
		
		monitorToPay = new ResultMonitorSearchTO();
		monitorToPay = (ResultMonitorSearchTO) event.getComponent().getAttributes().get("selectedObject");
		
		
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		
		retirementObjetTO.setIdParticipantPk(monitorToPay.getMonitorTO().getIdParticipantPk());
		retirementObjetTO.setCurrency(monitorToPay.getMonitorTO().getCurrency());
		retirementObjetTO.setGloss(monitorToPay.getMonitorTO().getGloss());
		
		List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
		DetailCollectionGloss collectionGloss = collectionGlossList.get(0);
		
		if(collectionGloss==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.no.exists.registre.deposited");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return ;
		}
		if(collectionGloss.getTransferStatus()!=null&&collectionGloss.getTransferStatus().equals(DepositStateType.CONFIRMED.getCode())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.no.process.retired");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return ;
		}
		if(monitorToPay.getMonitorTO().isPaidObligation()){
			Object[] params= new Object[2];
			/*Participante*/
			params[0]= monitorToPay.getMonitorTO().getMnemonic();
			/*Monto a pagar*/
			params[1]= monitorToPay.getMonitorTO().getAmountReceivable();
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.dialog.confirm.retirement",params);
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.updateComponent("opnlDialogs");
	    	JSFUtilities.executeJavascriptFunction("PF('wvPreExecuteRetirement').show();");
		}else{
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("label.dialog.insufficient.amounts");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**Ejecutamos el pago*/
	public synchronized void executePay(){
		
		if(monitorToPay == null){
			return;
		}
		RetirementObjetTO retirementObjetTO = new RetirementObjetTO();
		retirementObjetTO.setIdParticipantPk(monitorToPay.getMonitorTO().getIdParticipantPk());
		retirementObjetTO.setPaymentDate(monitorToPay.getMonitorTO().getPaymentDate());
		retirementObjetTO.setCurrency(monitorToPay.getMonitorTO().getCurrency());
		retirementObjetTO.setAmount(monitorToPay.getMonitorTO().getAmountReceivable().setScale(2, RoundingMode.UP));
		retirementObjetTO.setGloss( monitorToPay.getMonitorTO().getGloss());
		retirementObjetTO.setUserName(userInfo.getUserAccountSession().getUserName());
		
		log.info("CDE :::::::::::::Iniciando transferencia o retiro de fondos para cobro de derecho economicos");
		
		BigDecimal currentBalanceBefore = collectionEconomicFacade.verifyBalance(monitorToPay.getMonitorTO().getCurrency());
		
		DetailCollectionGloss collectionGloss =  new DetailCollectionGloss();
		List<DetailCollectionGloss> collectionGlossList = collectionEconomicFacade.searchDetailCollectionGlosList(retirementObjetTO);
		collectionGloss = collectionGlossList.get(0);
		try {
			/*Buscamos el numero de cuenta a la cual se debe depositar*/
			CollectionEconomicRigth collectionEconomicRigth = collectionEconomicFacade.searchCollectiionEconomicByGloss(null, collectionGloss.getGlosaNotClassified());
			retirementObjetTO.setIdBankPk(collectionEconomicRigth.getIdBankFk());
			retirementObjetTO.setAccountNumber(collectionEconomicRigth.getDepAccountNumber());
			retirementObjetTO.setIdHolderPk(collectionEconomicRigth.getIdHolderPk());
			/*Metodo que devuelve un estado de la transferencia*/
			Integer transferStatus =  collectionEconomicFacade.registerTransferOperationForCollection(retirementObjetTO);
			BigDecimal currentBalanceAfter = collectionEconomicFacade.verifyBalance(monitorToPay.getMonitorTO().getCurrency());
			if(currentBalanceBefore.compareTo(currentBalanceAfter)==0) {
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.balance.amount.no.change");
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.updateComponent("frmSearchMonitorCharges");
				return;
			}
			if(transferStatus!=null&&transferStatus.equals(RetirementStateType.CONFIRMED.getCode())){
				collectionGloss.setTransferStatus(transferStatus);
				collectionGloss.setRetirementDate(CommonsUtilities.currentDate());
				collectionGloss.setAmountTransferred(retirementObjetTO.getAmount());
				collectionEconomicFacade.updateTranferCollection(collectionGloss);
				searchFromFilters();
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.transfer.ok");
				
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				JSFUtilities.updateComponent("frmSearchMonitorCharges");
				return;
			}else{
				log.info("CDE::::: No se ejecuto la transferencia para la glosa :"+retirementObjetTO.getGloss());
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.transfer.no.ok");
				JSFUtilities.putViewMap("headerMessageView",headerMessage);
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			log.info("CDE ::: NO SE PUDO EJECUTAR LA TRANSFERENCIA, VERIFIQUE EN SU AMBIENTE DE LIP");
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.transfer.no.ok");
			JSFUtilities.putViewMap("headerMessageView",headerMessage);
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	public void clearFilters(){
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterMonitorTO = new SearchFilterMonitorTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterMonitorTO = new SearchFilterMonitorTO();
		/*Solo cargar BOB y USD*/
		searchFilterMonitorTO.setMaxDate(CommonsUtilities.currentDate());
		searchFilterMonitorTO.setMinDate(holidayQueryServiceBean.subtractBusinessDays(new Date(), 3));
	}
	public List<Participant> getListParticipants() {
		return listParticipants;
	}
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}
	public SearchFilterMonitorTO getSearchFilterMonitorTO() {
		return searchFilterMonitorTO;
	}
	public void setSearchFilterMonitorTO(SearchFilterMonitorTO searchFilterMonitorTO) {
		this.searchFilterMonitorTO = searchFilterMonitorTO;
	}
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public ParameterServiceBean getParameterServiceBean() {
		return parameterServiceBean;
	}
	public void setParameterServiceBean(ParameterServiceBean parameterServiceBean) {
		this.parameterServiceBean = parameterServiceBean;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public GenericDataModel<ResultMonitorSearchTO> getListResultMonitorTOs() {
		return listResultMonitorTOs;
	}
	public void setListResultMonitorTOs(
			GenericDataModel<ResultMonitorSearchTO> listResultMonitorTOs) {
		this.listResultMonitorTOs = listResultMonitorTOs;
	}
	public CollectionRigthFacade getFacadeCollection() {
		return facadeCollection;
	}
	public void setFacadeCollection(CollectionRigthFacade facadeCollection) {
		this.facadeCollection = facadeCollection;
	}
	public BigDecimal getTotalAmountReceivableBOB() {
		return totalAmountReceivableBOB;
	}
	public void setTotalAmountReceivableBOB(BigDecimal totalAmountReceivableBOB) {
		this.totalAmountReceivableBOB = totalAmountReceivableBOB;
	}
	public BigDecimal getTotalAmountReceivableUSD() {
		return totalAmountReceivableUSD;
	}
	public void setTotalAmountReceivableUSD(BigDecimal totalAmountReceivableUSD) {
		this.totalAmountReceivableUSD = totalAmountReceivableUSD;
	}
	public BigDecimal getTotalAmountReceived() {
		return totalAmountReceived;
	}
	public void setTotalAmountReceived(BigDecimal totalAmountReceived) {
		this.totalAmountReceived = totalAmountReceived;
	}
	public BigDecimal getDifference() {
		return difference;
	}
	public void setDifference(BigDecimal difference) {
		this.difference = difference;
	}
	public ResultMonitorSearchTO getMonitorTOviewDetaiil() {
		return monitorTOviewDetaiil;
	}
	public void setMonitorTOviewDetaiil(ResultMonitorSearchTO monitorTOviewDetaiil) {
		this.monitorTOviewDetaiil = monitorTOviewDetaiil;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
}
