package com.pradera.collection.monitor.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.collection.monitor.to.DetailDepositedTO;
import com.pradera.collection.monitor.to.DetailMatchGlossTO;
import com.pradera.collection.monitor.to.DetailObjectMonitorTO;
import com.pradera.collection.monitor.to.ManualDepositTO;
import com.pradera.collection.monitor.to.SearchFilterMonitorTO;
import com.pradera.collection.monitor.to.TotalDetailObjectMonitorTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.component.type.ParameterFundsOperationType;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.funds.type.OperationClassType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.pradera.model.generalparameter.type.RetirementStateType;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.EquivalentDepositGlosses;
import com.edv.collection.economic.rigth.FundsCollection;
import com.edv.collection.economic.rigth.GlossNoticationDetail;
import com.edv.collection.economic.rigth.PaymentAgent;
import com.edv.collection.economic.rigth.ReqCollectionEconomicRight;

@Stateless
@Performance
public class MonitoriCollectionEconomicService extends CrudDaoServiceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8429676851755055956L;
	
	@Inject
	private PraderaLogger log;
	
	@Inject
    private UserInfo userInfo;
	
	/** metodo para retornar el agente pagador juridico de una determinada glosa
	 * 
	 * @param idGlossPk
	 * @return null si no encuentra dicho agente pagador juridico */
	@SuppressWarnings("unchecked")
	public PaymentAgent getPaymentAgentJuridic(Long idGlossPk) {
		List<GlossNoticationDetail> lst = new ArrayList<>();
		PaymentAgent paymentAgentJuridic;
		PaymentAgent paymentAgentContact;
		StringBuilder sd = new StringBuilder();
		sd.append(" select dg");
		sd.append(" from GlossNoticationDetail dg");
		sd.append(" inner join fetch dg.idFkPaymentAgent cpa");
		sd.append(" where  1 =  1");
		sd.append(" and dg.idFkGlossCode.idDetailCollectionGlossPk = :idGlossPk");
		try {
			Query q = em.createQuery(sd.toString());
			q.setParameter("idGlossPk", idGlossPk);
			lst = q.getResultList();
			if (!lst.isEmpty()) {
				paymentAgentContact = lst.get(0).getIdFkPaymentAgent();
				paymentAgentJuridic = em.find(PaymentAgent.class, paymentAgentContact.getIdPaymentAgentFk() == null ? 0L : paymentAgentContact.getIdPaymentAgentFk());
				return paymentAgentJuridic;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @param idParticipantPk filtro de entrada,corregir
	 * **/
	public List<String> searchGlossCollections(Long idParticipantPk,String idIssuerPk,Date paymentDate,Integer collectionCurrency){
		StringBuilder  stringBuilder = new StringBuilder();
		stringBuilder.append(" SELECT DISTINCT(CER.GLOSA_NOT_CLASSIFIED) FROM COLLECTION_ECONOMIC_RIGHT CER ");
		stringBuilder.append(" INNER JOIN REQ_COLLECTION_ECONOMIC_RIGHT RCER ON RCER.ID_REQ_COLLECTION_ECONOMIC_PK = CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		stringBuilder.append(" INNER JOIN SECURITY SE ON SE.ID_SECURITY_CODE_PK =  CER.ID_SECURITY_CODE_FK ");
		stringBuilder.append(" WHERE 1 = 1 ");
		stringBuilder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NOT NULL ");
		stringBuilder.append(" AND TRUNC(CER.PAYMENT_DATE) = :paymentDate ");
		stringBuilder.append(" AND RCER.ID_PARTICIPANT_FK = :idParticipantPk ");
		stringBuilder.append(" AND CER.CURRENCY = :currency ");
		if(idIssuerPk!=null)
			stringBuilder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk");

		Query query = em.createNativeQuery(stringBuilder.toString());
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("currency", collectionCurrency);
		if(idIssuerPk!=null)
			query.setParameter("idIssuerPk", idIssuerPk);
		
		@SuppressWarnings("unchecked")
		List<String> listGloss = query.getResultList();
		return listGloss;
	}
	
	/**
	 * obteer los contactos agentes pagadores, cat enviados (detalle de una glosa )
	 * @param idGlossPk
	 * @return
	 */
	public List<GlossNoticationDetail> getLstGlossNoticationDetailOfOneDetailCollectionGloss(Long idGlossPk){
		List<GlossNoticationDetail> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append("select gd");
		sd.append(" from GlossNoticationDetail gd");
		sd.append(" inner join fetch gd.idFkPaymentAgent");
		sd.append(" where 0=0");
		sd.append(" and gd.idFkGlossCode.idDetailCollectionGlossPk = :idGlossPk");
		try {
			Query query=em.createQuery(sd.toString());
			query.setParameter("idGlossPk", idGlossPk);
			lst=query.getResultList();
		} catch (Exception e) {
			System.out.println("::: Error en en el metodo ");
		}
		
		return lst;
	}
	/**
	 * Busqueda de detalle de glosa, deposit, cobro y deposito segun glosa
	 * */
	public DetailCollectionGloss findDetailCollectionGloss(String glossCode){
		List<DetailCollectionGloss> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append("select g ");
		sd.append(" from DetailCollectionGloss g");
		sd.append(" where 1 = 1");
		sd.append(" and g.glosaNotClassified = :glossCode");
		try {
			Query query=em.createQuery(sd.toString());
			query.setParameter("glossCode", glossCode);
			lst=query.getResultList();
			if(!lst.isEmpty()){
				return lst.get(0);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Busqueda de detalle de glosa, deposit, cobro y deposito segun glosa
	 * */
	public DetailCollectionGloss findDetailCollectionGloss(Long idDetailCollectionGlossPk){
		List<DetailCollectionGloss> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append("select g");
		sd.append(" from DetailCollectionGloss g");
		sd.append(" where 1 = ");
		sd.append(" and g.idDetailCollectionGlossPk = :idDetailCollectionGlossPk");
		try {
			Query query=em.createQuery(sd.toString());
			query.setParameter("idDetailCollectionGlossPk", idDetailCollectionGlossPk);
			lst=query.getResultList();
			if(!lst.isEmpty()){
				return lst.get(0);
			}	
		} catch (Exception e) {
			System.out.println("::: error en el metodo findDetailCollectionGloss(String glossCode) "+e.getMessage());
		}
		return null;
	}
	/**
	 * metodo para cambiar el indicador del reeenvio de notificacion a REENVIADO
	 * @param glossNoticationDetail
	 */
	public void modifiedReSenderNotification(GlossNoticationDetail glossNoticationDetail){
		try {
			em.merge(glossNoticationDetail);
			em.flush();
		} catch (Exception e) {
			System.out.println("::: error en el metodo modifiedReSenderNotification(GlossNoticationDetail glossNoticationDetail) "+e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<DetailCollectionGloss> searchDetailsCollectionGlossToReenvio(Long idParticipantPk,String idIssuerPk,Date registreDate,Integer currency, String glossCode){
		StringBuilder builder = new StringBuilder();
		builder.append(" select dcg from DetailCollectionGloss dcg ");
		builder.append(" where 1 = 1 ");
		builder.append(" and dcg.idParticipantPk = :idParticipantPk ");
		builder.append(" and dcg.currency = :currency ");
		builder.append(" and trunc(dcg.registreDate) = :registreDate ");
		
		if(glossCode.length()>0)
			builder.append(" and dcg.glosaNotClassified like '%'||:glossCode||'%'");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("currency", currency);
		query.setParameter("registreDate", registreDate);
		if(glossCode.length()>0)
			query.setParameter("glossCode", glossCode);
		
		List<DetailCollectionGloss> listCollectionGlosses = null;
		
		listCollectionGlosses = query.getResultList();
		
		return listCollectionGlosses;
	}
	/**
	 * Buscamo depositos solo en estado no depositado ni confirmado
	 * */
	@SuppressWarnings("unchecked")
	public List<DetailCollectionGloss> searchDetailsCollectionGlossNotConfirmed(Long idParticipantPk,Date registreDate,Integer currency, String glossCode){
		StringBuilder builder = new StringBuilder();
		builder.append(" select dcg from DetailCollectionGloss dcg ");
		builder.append(" where 1 = 1 ");
		builder.append(" and dcg.idParticipantPk = :idParticipantPk ");
		
		if(currency!=null)
			builder.append(" and dcg.currency = :currency ");
		
		builder.append(" and trunc(dcg.paymentDate) = :registreDate ");
		//builder.append(" and trunc(dcg.registreDate) = :registreDate ");
		if (glossCode!=null&&glossCode.length() > 0)
			builder.append(" and dcg.glosaNotClassified like '%'||:glossCode||'%'");
		/*Solo consideramos depositos que no tengan ningun estado*/
		builder.append(" and dcg.depositStatus is null ");
		//builder.append(" and (dcg.amountDeposited   is null or dcg.amountDeposited = 0) ");
		builder.append(" and (dcg.amountDeposited   is null or dcg.amountDeposited < dcg.amountReceivable) ");
		builder.append(" and (dcg.amountTransferred is null or dcg.amountTransferred = 0)");

		Query query = em.createQuery(builder.toString());
		query.setParameter("idParticipantPk", idParticipantPk);
		
		if(currency!=null)
			query.setParameter("currency", currency);
		query.setParameter("registreDate", registreDate);
		
		if (glossCode!=null&&glossCode.length() > 0)
			query.setParameter("glossCode", glossCode);
		
		List<DetailCollectionGloss> listCollectionGlosses = null;
		
		listCollectionGlosses = query.getResultList();
		
		return listCollectionGlosses;
	}
	@SuppressWarnings("unchecked")
	public List<DetailCollectionGloss> searchDetailsCollectionGloss(Long idParticipantPk,String idIssuerPk,Date registreDate,Integer currency, String glossCode){
		StringBuilder builder = new StringBuilder();
		builder.append(" select dcg from DetailCollectionGloss dcg ");
		builder.append(" where 1 = 1 ");
		builder.append(" and dcg.idParticipantPk = :idParticipantPk ");
		
		if(currency!=null)
			builder.append(" and dcg.currency = :currency ");
		
		builder.append(" and trunc(dcg.paymentDate) = :registreDate ");
		//builder.append(" and trunc(dcg.registreDate) = :registreDate ");
		if (glossCode!=null&&glossCode.length() > 0)
			builder.append(" and dcg.glosaNotClassified like '%'||:glossCode||'%'");
		if (idIssuerPk != null)
			builder.append(" and dcg.idIssuerFk = :idIssuerPk ");

		Query query = em.createQuery(builder.toString());
		query.setParameter("idParticipantPk", idParticipantPk);
		
		if(currency!=null)
			query.setParameter("currency", currency);
		query.setParameter("registreDate", registreDate);
		
		if (glossCode!=null&&glossCode.length() > 0)
			query.setParameter("glossCode", glossCode);
		if (idIssuerPk != null)
			query.setParameter("idIssuerPk", idIssuerPk);
		
		List<DetailCollectionGloss> listCollectionGlosses = null;
		
		listCollectionGlosses = query.getResultList();
		
		return listCollectionGlosses;
	}
	/**
	 * Cargamos la lista de glosas que se mostraron al momento de registrar, este es solo para la vista
	 * */
	@SuppressWarnings("unchecked")
	public List<DetailCollectionGloss> searchDetailsCollectionGloss(Long idEqDepositGlossesPk){
		StringBuilder builder = new StringBuilder();
		builder.append(" select edg.listDetailCollectionShow from EquivalentDepositGlosses edg ");
		builder.append(" where 1 = 1 ");
		builder.append(" and edg.idEqDepositGlossesPk = :idEqDepositGlossesPk ");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("idEqDepositGlossesPk", idEqDepositGlossesPk);
		
		String stringList = (String) query.getSingleResult();
		List<String> listString = CommonsUtilities.getListOfComponentsForUpdate(stringList);
		
		List<DetailCollectionGloss> listCollectionGlosses = new ArrayList<>();
		DetailCollectionGloss detail = new DetailCollectionGloss();
		for (String string : listString){
			detail = this.find(DetailCollectionGloss.class, new Long(string));
			listCollectionGlosses.add(detail);
		}
		return listCollectionGlosses;
	}
	
	/**
	 * Cargamos la lista de glosas que se mostraron al momento de registrar, este es solo para la vista
	 * */
	@SuppressWarnings("unchecked")
	public List<FundsCollection> searchFundCollections(Long idEqDepositGlossesPk){
		StringBuilder builder = new StringBuilder();
		builder.append(" select edg.listFundsCollectionShow from EquivalentDepositGlosses edg ");
		builder.append(" where 1 = 1 ");
		builder.append(" and edg.idEqDepositGlossesPk = :idEqDepositGlossesPk ");
		
		Query query = em.createQuery(builder.toString());
		query.setParameter("idEqDepositGlossesPk", idEqDepositGlossesPk);
		
		String stringList = (String) query.getSingleResult();
		List<String> listString = CommonsUtilities.getListOfComponentsForUpdate(stringList);
		
		List<FundsCollection> lisCollections = new ArrayList<>();
		FundsCollection fundsCollection = new FundsCollection();
		for (String string : listString){
			fundsCollection = this.find(FundsCollection.class, new Long(string));
			if(fundsCollection.getOriginCurrency().equals(new Short("69")))
				fundsCollection.setDescOriginCurrency("BOLIVIANOS");
			if(fundsCollection.getOriginCurrency().equals(new Short("34")))
				fundsCollection.setDescOriginCurrency("DOLARES AMERICANOS");
			lisCollections.add(fundsCollection);
		}
		return lisCollections;
	}
	
	/**
	 * Buscando depositos registrados por parte LIP
	 * S**/
	@SuppressWarnings("unchecked")
	public List<FundsCollection> searchFundCollections(Date registreDate,Integer currency,boolean enableState,String gloss){
		Integer originCurrency = null;
		if(currency!=null&&currency.equals(CurrencyType.PYG.getCode())){
			ParameterTable cuurencyBOB = this.find(ParameterTable.class, currency);
			originCurrency = cuurencyBOB.getShortInteger();
		}
		
		if(currency!=null&&currency.equals(CurrencyType.USD.getCode())){
			ParameterTable cuurencyUSD = this.find(ParameterTable.class, currency);
			originCurrency = cuurencyUSD.getShortInteger();
		}
			
		
		StringBuilder builder = new StringBuilder();
		builder.append(" select fc from FundsCollection fc ");
		builder.append(" where 1 = 1 ");
		if(enableState)
			builder.append(" and fc.operationState is null ");//+StatesConstants.REGISTERED_MATCH_GLOSSES);
		
		if(currency!=null)
			builder.append(" and fc.originCurrency = :originCurrency ");
		
		if(gloss!=null)
			builder.append(" and fc.gloss like (:gloss) ");
			
		//calcular 3 dias habiles hacia atras
		//Ini = holidayQueryServiceBean.subtractBusinessDays(CommonsUtilities.currentDate(), i);
		builder.append(" and trunc(fc.dateOperation) = :dateOperation ");
		builder.append(" order by fc.numberRequest desc ");
		
		Query query = em.createQuery(builder.toString());
		
		if(currency!=null)
			query.setParameter("originCurrency", Short.parseShort(originCurrency.toString()));
		
		if(gloss!=null){
			String mNemonic = gloss.substring(0,3);
			query.setParameter("gloss","%"+mNemonic+"%");
		}
		
		//query.setParameter("dateOperation", registreDate);
		query.setParameter("dateOperation", CommonsUtilities.currentDate());
		
		List<FundsCollection> listFundsCollections = null;
		
		listFundsCollections = query.getResultList();
		for (FundsCollection fundsCollection : listFundsCollections) {
			if(fundsCollection.getOriginCurrency()==69)
				fundsCollection.setDescOriginCurrency("BOLIVIANOS");
			if(fundsCollection.getOriginCurrency()==34)
				fundsCollection.setDescOriginCurrency("DOLARES AMERICANOS");
		}
		
		
		return listFundsCollections;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<DetailCollectionGloss> searchDetailsCollectionGlossToMotinorCollection(Long idParticipantPk,String idIssuerPk,Date minDate, Date maxDate,Integer currency, String glossCode){
		StringBuilder builder = new StringBuilder();
		builder.append(" select dcg from DetailCollectionGloss dcg ");
		builder.append(" where 1 = 1 ");
		builder.append(" and dcg.idParticipantPk = :idParticipantPk ");
		builder.append(" and dcg.currency = :currency ");
		builder.append(" and trunc(dcg.paymentDate) between :minDate and :maxDate ");
		if (glossCode.length() > 0)
			builder.append(" and dcg.glosaNotClassified like '%'||:glossCode||'%'");
		if (idIssuerPk != null)
			builder.append(" and dcg.idIssuerFk = :idIssuerPk ");

		Query query = em.createQuery(builder.toString());
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("currency", currency);
		query.setParameter("minDate", minDate);
		query.setParameter("maxDate", maxDate);
		if (glossCode.length() > 0)
			query.setParameter("glossCode", glossCode);
		if (idIssuerPk != null)
			query.setParameter("idIssuerPk", idIssuerPk);
		
		List<DetailCollectionGloss> listCollectionGlosses = null;
		
		listCollectionGlosses = query.getResultList();
		
		return listCollectionGlosses;
	}
	
	/**Busqueda de todas las glosa existentes que vencen hoy y por moneda
	 * @param dueDate fecha de vencimiento y/o fecha de pago 
	 * @param collectionCurrency moneda de cobro registrad para el cobro
	 * @return Lista de glosa generadas
	 * */
	@SuppressWarnings("unchecked")
	public List<String> searchGlossCollections(Long idParticipantPk,Date dueDate,Integer collectionCurrency,Integer securityClass){
		List<String> allGlosses = new ArrayList<>();
		/*Query sin otra otra moneda de cobro*/
		StringBuilder sqlString = new StringBuilder();
		sqlString.append(" select distinct(cer.glosaNotClassified) from CollectionEconomicRigth cer ");
		sqlString.append(" where 1 = 1 ");
		sqlString.append(" and trunc(cer.dueDate) =  :dueDate ");
		sqlString.append(" and cer.checkOther = 0 ");
		sqlString.append(" and cer.currency = :collectionCurrency ");
		/*glosa != null --> significa que se envio notificacion*/
		sqlString.append(" and cer.glosaNotClassified is not null ");
		Query query = em.createQuery(sqlString.toString());
		query.setParameter("collectionCurrency", collectionCurrency);
		query.setParameter("dueDate", dueDate);
		
		allGlosses = query.getResultList();
		
		/*Consulta con otra moneda de cobro */
		sqlString = new StringBuilder();
		sqlString.append(" select distinct(cer.glosaNotClassified) from CollectionEconomicRigth cer ");
		sqlString.append(" where 1 = 1 ");
		sqlString.append(" and trunc(cer.dueDate) =  :dueDate ");
		sqlString.append(" and cer.checkOther = 1 ");
		sqlString.append(" and cer.anotherCurency = :anotherCurency ");
		/*glosa != null --> significa que se envio notificacion*/
		sqlString.append(" and cer.glosaNotClassified is not null ");
		query = em.createQuery(sqlString.toString());
		query.setParameter("anotherCurency", collectionCurrency);
		query.setParameter("dueDate", dueDate);
		
		allGlosses.addAll(query.getResultList());
		return allGlosses;
	}
	
	/**
	 * Busqueda de registros de cobro de derechos economicos
	 * @param idParticipantPk id de participante buscado segun filtros
	 * @param dueDate fecha de vencimiento o de pago
	 * @param gloss glosa para el deposito en el LIP
	 * @return lista de vencimientos registrados para cobro de derechos economicos
	 * */
	public List<CollectionEconomicRigth> getListCollectionEconomicRigths(Long idParticipantPk,Date paymentDate,String glosaNotClassified){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		//queryBuilder.append(" and trunc(cer.paymentDate) = :paymentDate ");
		queryBuilder.append(" and cer.glosaNotClassified = :glosaNotClassified ");
		queryBuilder.append(" order by cer.idCollectionEconomicPk asc ");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		//query.setParameter("paymentDate",paymentDate);
		query.setParameter("glosaNotClassified",glosaNotClassified);
			
		@SuppressWarnings("unchecked")
		List<CollectionEconomicRigth> collectionEconomicRights = query.getResultList();
		return collectionEconomicRights;
	}
	/**
	 * Buscamos los cobros registrados y almacenamos en POJO
	 * @param idParticipantPk id de participante de filtro de busqueda
	 * @param paymentDate fecha de pago
	 * @return detalle de cobro participnate y glosa
	 * */
	public TotalDetailObjectMonitorTO getDetailObjectMonitorTO(Long idParticipantPk,Date paymentDate,String glosaNotClassified){
		/*Buscamos todos los cobros registrados con los paramentros de busqueda*/
		List<CollectionEconomicRigth> listCollectionEconomicRigths = getListCollectionEconomicRigths(idParticipantPk, paymentDate, glosaNotClassified);
		DetailObjectMonitorTO detailObjectMonitorTO = null;
		List<DetailObjectMonitorTO> listresult = new ArrayList<>();
		BigDecimal totalAmountCharge = BigDecimal.ZERO;
		for (CollectionEconomicRigth collectionEconomicRigth : listCollectionEconomicRigths) {
			detailObjectMonitorTO =  new DetailObjectMonitorTO();
			detailObjectMonitorTO.setIdCollectionEconomicPk(collectionEconomicRigth.getIdCollectionEconomicPk());
			detailObjectMonitorTO.setDueDate(collectionEconomicRigth.getDueDate());
			detailObjectMonitorTO.setPaymetDate(collectionEconomicRigth.getPaymentDate());
			detailObjectMonitorTO.setIdSecurityCodePk(collectionEconomicRigth.getIdSecurityCodeFk());
			/*Cantidad de valores*/
			detailObjectMonitorTO.setAmountSecurities(collectionEconomicRigth.getAmountSecurities());
			detailObjectMonitorTO.setCouponNumber(collectionEconomicRigth.getCouponNumber());
			/*Sin otra moneda de cobro*/
			if(collectionEconomicRigth.getCheckOther().equals(0)){
				totalAmountCharge = totalAmountCharge.add(collectionEconomicRigth.getAmountReceivable());
				detailObjectMonitorTO.setAmountReceivable(collectionEconomicRigth.getAmountReceivable());
				detailObjectMonitorTO.setCollectionCurrency(collectionEconomicRigth.getCurrency());
				detailObjectMonitorTO.setDescCollectionCurrency(find(ParameterTable.class,collectionEconomicRigth.getCurrency()).getText1());
			}
			else{
				totalAmountCharge = totalAmountCharge.add(collectionEconomicRigth.getAnotherAmountReceivable());
				detailObjectMonitorTO.setAmountReceivable(collectionEconomicRigth.getAnotherAmountReceivable());
				detailObjectMonitorTO.setCollectionCurrency(collectionEconomicRigth.getAnotherCurency());
				detailObjectMonitorTO.setDescCollectionCurrency(find(ParameterTable.class,collectionEconomicRigth.getAnotherCurency()).getText1());
			}
			detailObjectMonitorTO.setIdBankPk(collectionEconomicRigth.getIdBankFk());
			detailObjectMonitorTO.setDescBank(find(Bank.class,collectionEconomicRigth.getIdBankFk()).getDescription());
			detailObjectMonitorTO.setAccountNumberDeposit(collectionEconomicRigth.getDepAccountNumber());
			listresult.add(detailObjectMonitorTO);
		}
		TotalDetailObjectMonitorTO totalDetailObjectMonitorTO = new TotalDetailObjectMonitorTO();
		totalDetailObjectMonitorTO.setTotalAmountCharge(totalAmountCharge);
		totalDetailObjectMonitorTO.setLisDetailObjectMonitorTOs(listresult);
		return totalDetailObjectMonitorTO;
	}
	
	/**
	 * Busqueda de todos los cobros registrados
	 * @param currency Moneda de cobro
	 * @param idReqCollectionEconomicPk id de la solicitud de cobro de derechos economicos
	 * @return {@link CollectionEconomicRigth} Retornamos la lista de cobros de derechos economicos
	 *  */
	public List<CollectionEconomicRigth> getListCollectionEconomicRigths(Integer currency,Long idReqCollectionEconomicPk){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select new com.edv.collection.economic.rigth.CollectionEconomicRigth (");
	    queryBuilder.append(" cer.idCollectionEconomicPk,");
	    queryBuilder.append(" trunc(cer.paymentDate), ");
		queryBuilder.append(" case cer.checkOther ");
		queryBuilder.append(" when 0 then cer.currency ");
		queryBuilder.append(" when 1 then cer.anotherCurency ");
		queryBuilder.append(" end as currency, ");
		
		queryBuilder.append(" case cer.checkOther ");
		queryBuilder.append(" when 0 then cer.amountReceivable ");
		queryBuilder.append(" when 1 then cer.anotherAmountReceivable ");
		queryBuilder.append(" end as amountReceivable, ");
		queryBuilder.append(" cer.idBankFk,");
		queryBuilder.append(" cer.depAccountNumber) ");
		
		queryBuilder.append(" from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and cer.currency = :currency ");
		queryBuilder.append(" and cer.idReqCollectionEconomicFk = :idReqCollectionEconomicFk ");
		queryBuilder.append(" order by cer.idCollectionEconomicPk asc ");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		query.setParameter("currency",currency);
		query.setParameter("idReqCollectionEconomicFk",idReqCollectionEconomicPk);
			
		@SuppressWarnings("unchecked")
		List<CollectionEconomicRigth> collectionEconomicRights = query.getResultList();
		return collectionEconomicRights;
	}
	
	/**Verificamos si se hizo el deposito de acuerdo a la glosa que se encuentra 
	 * registrada en los depositos del LIP
	 * @param glosa Glosa con la cual deberia haberse ejecuatdo el deposito
	 * @return Si existe un registro en FoundOperation con esa glosa, significa que el deposito se hizo*/
	public DetailDepositedTO getDetailDeposited(String glosaNotClassified,Integer currency,BigDecimal amount,Date paymentDate){
		List<FundsOperation> fundsOperationList = this.getDepositsOfCollections(glosaNotClassified,currency,amount,paymentDate);
		
		BigDecimal amountRecept = BigDecimal.ZERO;
		
		
		for (FundsOperation fundsOperation : fundsOperationList) {
			amountRecept =  amountRecept.add(fundsOperation.getOperationAmount());
		}
		
		DetailDepositedTO depositedTO = new DetailDepositedTO();
		
		depositedTO.setAmount(amountRecept);
		depositedTO.setCurrency(currency);
		return depositedTO;
	}
	
	/**
	 * Obtenemos el monto del deposito segun la glosa 
	 * @param glosaNotClassified Glosa con la cual deberia haberse ejecuatdo el deposito
	 * @param currency moneda de cobro
	 * @param amunt monto de cobro de derechos economicos
	 * @param paymentDate fecha de pago, deberia ser la fecha del desposito
	 * @return Si existe un registro en FoundOperation con esa glosa, significa que el deposito se hizo*/
	public BigDecimal getAmountOfTheDepositForGloss(String glosaNotClassified,Integer currency,Date paymentDate){
		List<FundsOperation> fundsOperationList = this.getDepositsOfCollections(glosaNotClassified,currency,paymentDate);
		FundsOperation fundsOperation = null;
		
		if(fundsOperationList!=null&&fundsOperationList.size()>0){
			fundsOperation = fundsOperationList.get(0);
			return fundsOperation.getOperationAmount();
		}
		else 
			return BigDecimal.ZERO;
	}
	
	/**
	 * Busqueda de los depositos
	 * @see FundsOperation
	 * @param searchFilterMonitorTO Filtros de busqueda
	 * @return {@link List} */
	public List<FundsOperation> getDepositsOfCollections(String glosaNotClassified,Integer currency,BigDecimal amount,Date paymentDate){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select fo from FundsOperation fo ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and fo.depositGloss = :depositGloss ");
		//queryBuilder.append(" and fo.operationAmount =  :amount ");
		queryBuilder.append(" and fo.fundsOperationType = :fundsOperationType ");
		queryBuilder.append(" and fo.fundsOperationClass = :fundsOperationClass ");
		
		queryBuilder.append(" and fo.currency = :currency ");
		//queryBuilder.append(" and trunc(fo.registryDate) = :registryDate ");
		queryBuilder.append(" order by fo.idFundsOperationPk desc ");
		
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("depositGloss", glosaNotClassified);
		//query.setParameter("amount", amount);
		//query.setParameter("fundsOperationType", ParameterFundsOperationType.PAYMENT_BENEFITS_FUND_DEPOSITS.getCode().longValue());
		query.setParameter("fundsOperationType", ParameterFundsOperationType.OPERATIONS_COLLECTION_DEPOSIT.getCode());
		query.setParameter("fundsOperationClass", OperationClassType.RECEPTION_FUND.getCode());
		
		query.setParameter("currency", currency);
		//query.setParameter("registryDate", paymentDate);
		
		@SuppressWarnings("unchecked")
		List<FundsOperation> fundsOperationList = query.getResultList();
		return fundsOperationList;
	}
	
	/**
	 * Busqueda de los depositos
	 * @see FundsOperation
	 * @param searchFilterMonitorTO Filtros de busqueda
	 * @return {@link List} */
	public List<FundsOperation> getDepositsOfCollections(String glosaNotClassified,Integer currency,Date paymentDate){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select fo from FundsOperation fo ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and fo.depositGloss = :depositGloss ");
		queryBuilder.append(" and fo.fundsOperationType = :fundsOperationType ");
		queryBuilder.append(" and fo.currency = :currency ");
		queryBuilder.append(" and trunc(fo.registryDate) = :registryDate ");
		queryBuilder.append(" order by fo.idFundsOperationPk desc ");
		
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("depositGloss", glosaNotClassified);
		query.setParameter("fundsOperationType", ParameterFundsOperationType.PAYMENT_BENEFITS_FUND_DEPOSITS.getCode());
		query.setParameter("currency", currency);
		query.setParameter("registryDate", paymentDate);
		
		@SuppressWarnings("unchecked")
		List<FundsOperation> fundsOperationList = query.getResultList();
		return fundsOperationList;
	}
	
	public  List<ReqCollectionEconomicRight>getListReqCollectionEconomic(SearchFilterMonitorTO searchFilterMonitorTO){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select new com.edv.collection.economic.rigth.ReqCollectionEconomicRight(rce.idReqCollectionEconomicPk,rce.participant,rce.idIssuerFk) from ReqCollectionEconomicRight rce ");
		queryBuilder.append(" where 1 = 1 ");
		
		if(searchFilterMonitorTO.getIdParticipantPk()!=null)
			queryBuilder.append(" and rce.participant.idParticipantPk = :idParticipantPk ");
		
		/*if(searchFilterMonitorTO.getCurrency()!=null)
			queryBuilder.append(" and rce.currency = :currency ");*/
		
		queryBuilder.append(" and rce.state = :state ");
		queryBuilder.append(" and trunc(rce.processDate) = :processDate ");
		Query query = em.createQuery(queryBuilder.toString());
		
		query.setParameter("processDate", CommonsUtilities.currentDate());
		query.setParameter("state",StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);
		
		if(searchFilterMonitorTO.getIdParticipantPk()!=null)
			query.setParameter("idParticipantPk", searchFilterMonitorTO.getIdParticipantPk());
		/*if(searchFilterMonitorTO.getCurrency()!=null)
			query.setParameter("currency", searchFilterMonitorTO.getCurrency());*/
		
		@SuppressWarnings("unchecked")
		List<ReqCollectionEconomicRight> collectionEconomicRights = query.getResultList();
		
		for (ReqCollectionEconomicRight reqCollectionEconomicRight : collectionEconomicRights) {
			reqCollectionEconomicRight.setParticipant(searchParticipantObject(reqCollectionEconomicRight.getIdReqCollectionEconomicPk()));
		}
		return collectionEconomicRights;
	}
	
	private Participant searchParticipantObject(Long idReqCollectionPk){
		StringBuilder builder = new StringBuilder();
		
		builder.append(" select rce.ID_PARTICIPANT_FK from Req_Collection_Economic_Right rce ");
		builder.append(" where 1 = 1 ");
		builder.append(" and rce.ID_REQ_COLLECTION_ECONOMIC_PK = ");
		builder.append(idReqCollectionPk);
		
		Query query = em.createNativeQuery(builder.toString());
		
		Object object = query.getSingleResult();
		
		Participant participant = find(Participant.class,Long.parseLong(object.toString()));
		return participant;
		
	}
	/**
	 * Busqueda de parameter table*/
	public ParameterTable searchParameterTable(Integer parameterTablePk){
		return find(ParameterTable.class, parameterTablePk);
	}
	/**
	 * Busqueda de registros de cobro de derechos economicos
	 * @param idParticipantPk id de participante buscado segun filtros
	 * @param dueDate fecha de vencimiento o de pago
	 * @param gloss glosa para el deposito en el LIP
	 * @return lista de vencimientos registrados para cobro de derechos economicos
	 * */
	public CollectionEconomicRigth searchCollectionEconomicByGloss(String idSecurityCodePk,Date paymentDate,String glosaNotClassified){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		if(Validations.validateIsNotNullAndNotEmpty(idSecurityCodePk))
			queryBuilder.append(" and cer.idSecurityCodeFk = :idSecurityCodePk ");
		//queryBuilder.append(" and trunc(cer.paymentDate) = :paymentDate ");
		queryBuilder.append(" and cer.glosaNotClassified = :glosaNotClassified ");
		queryBuilder.append(" order by cer.idCollectionEconomicPk asc ");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		//query.setParameter("paymentDate",paymentDate);
		query.setParameter("glosaNotClassified",glosaNotClassified);
			
		@SuppressWarnings("unchecked")
		List<CollectionEconomicRigth> collectionEconomicRights =  query.getResultList();
		return collectionEconomicRights.get(0);
	}
	
	/**
	 * Buscamos si ya existe un registro con la glosa
	 * */
	public boolean isExistRegisteredGloss(Long idDetailCollectionPk){
		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT EDG.ID_DETAIL_COLLECTION_GLOSS_FK FROM EQUIVALENT_DEPOSIT_GLOSSES EDG ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND EDG.OPERATION_STATE IN ("+StatesConstants.REGISTERED_MATCH_GLOSSES+","+StatesConstants.REGISTERED_MATCH_GLOSSES+")" );
		builder.append(" AND EDG.ID_DETAIL_COLLECTION_GLOSS_FK = :idDetailCollectionPk ");

		Query query = em.createNativeQuery(builder.toString());
		query.setParameter("idDetailCollectionPk", idDetailCollectionPk);
		List<BigDecimal> resultList = query.getResultList();
		if(resultList.size()>0)
			return true;
		else 
			return false;
		
	}
	
	/**
	 *Busqueda de registros de igualacion de glosas
	 *
	 **/
	public List<Object[]> searchGlossMatchs(ManualDepositTO searchDepositTO){
		StringBuilder  stringBuilder = new StringBuilder();
		stringBuilder.append(" SELECT EDG.ID_EQ_DEPOSIT_GLOSSES_PK,P.DESCRIPTION AS DESC_PARTICIPANT, ");
		stringBuilder.append(" DCG.GLOSA_NOT_CLASSIFIED,PA.DESCRIPTION AS DESC_PAYMENT_AGENT,DCG.PAYMENT_DATE, ");
		stringBuilder.append(" DCG.CURRENCY,DCG.AMOUNT_RECEIVABLE AS MONTO_COBRAR, ");
		stringBuilder.append(" 0 AS NUM_OPE_LIP,EDG.TOTAL_AMOUNT AS TOTAL_AMOUNT, ");
		/*filtro para la pantalla de registro*/
		stringBuilder.append(" EDG.ID_PARTICIPANT_FK AS FILTER_ID_PARTICIPANT_PK,EDG.CURRENCY AS FILTER_CURRENCY,EDG.PAYMENT_DATE AS FILTER_PAYMENT_DATE,EDG.GLOSS AS FILTER_GLOSS, ");
		/*glosa y fondos seleccionados*/
		stringBuilder.append(" EDG.ID_DETAIL_COLLECTION_GLOSS_FK, '0 AS 'ID_TEMP,'0' AS GLOSS_LIP,PT1.DESCRIPTION, ");
		stringBuilder.append(" EDG.LIST_FUNDS_COLLECTION ");
		stringBuilder.append(" FROM EQUIVALENT_DEPOSIT_GLOSSES EDG ");
		stringBuilder.append(" INNER JOIN DETAIL_COLLECTION_GLOSS DCG ON EDG.ID_DETAIL_COLLECTION_GLOSS_FK = DCG.ID_DETAIL_COLLECTION_GLOSS_PK ");
		stringBuilder.append(" INNER JOIN PAYMENT_AGENT PA ON PA.ID_PAYMENT_AGENT_PK = DCG.ID_PAYMENT_AGENT_FK ");
		stringBuilder.append(" INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = DCG.ID_PARTICIPANT_PK ");
		stringBuilder.append(" INNER JOIN PARAMETER_TABLE PT1 ON EDG.OPERATION_STATE = PT1.PARAMETER_TABLE_PK ");
		stringBuilder.append(" WHERE 1 = 1 ");
		
		if(searchDepositTO.getIdParticipantPk()!=null)
			stringBuilder.append(" AND DCG.ID_PARTICIPANT_PK = :idParticipantPk ");
		
		if(searchDepositTO.getIdBankPk()!=null)
			stringBuilder.append(" AND DCG.ID_BANK_FK = :idBankPk");
		
		if(searchDepositTO.getHolder().getIdHolderPk()!=null)
			stringBuilder.append(" AND DCG.ID_HOLDER_FK = :idHolderPk");
		
		if(searchDepositTO.getCurrency()!=null)
			stringBuilder.append(" AND DCG.CURRENCY = :currency ");
		
		if(searchDepositTO.getPaymentDate()!=null)
			stringBuilder.append(" AND TRUNC(DCG.PAYMENT_DATE) = :paymentDate ");
		
		if(searchDepositTO.getGloss()!=null)
			stringBuilder.append(" AND DCG.GLOSA_NOT_CLASSIFIED = :gloss ");
		
		stringBuilder.append(" ORDER BY EDG.ID_EQ_DEPOSIT_GLOSSES_PK DESC ");
			
		Query query = em.createNativeQuery(stringBuilder.toString());
		
		if(searchDepositTO.getIdParticipantPk()!=null)
			query.setParameter("idParticipantPk", searchDepositTO.getIdParticipantPk());
		
		if(searchDepositTO.getIdBankPk()!=null)
			query.setParameter("idBankPk", searchDepositTO.getIdBankPk());
		
		if(searchDepositTO.getHolder().getIdHolderPk()!=null)
			query.setParameter("idHolderPk", searchDepositTO.getHolder().getIdHolderPk());
		
		if(searchDepositTO.getCurrency()!=null)
			query.setParameter("currency", searchDepositTO.getCurrency());
		
		if(searchDepositTO.getPaymentDate()!=null)
			query.setParameter("paymentDate", searchDepositTO.getPaymentDate());
		
		if(searchDepositTO.getGloss()!=null)
			query.setParameter("gloss", searchDepositTO.getGloss());
		
		@SuppressWarnings("unchecked")
		List<Object[]> resultList = query.getResultList();
		return resultList;
	}
	
	/**
	 * Registro de rechao de igualacion de glosa
	 * @throws ServiceException 
	 * */
	@LoggerAuditWeb
	public void recordGlossRejection(DetailMatchGlossTO detailMatchGlossTO,List<FundsCollection> fundsCollections) throws ServiceException{
		EquivalentDepositGlosses equivalentDepositGlosses = this.find(EquivalentDepositGlosses.class, detailMatchGlossTO.getIdEqDepositGlossesPk());
		equivalentDepositGlosses.setOperationState(StatesConstants.REJECT_MATCH_GLOSSES);
		update(equivalentDepositGlosses);
		for (FundsCollection fundsCollection : fundsCollections) {
			/*Regresamos al estado anterior, se habilita para ser buscado nuevamente en la pantalla de registro*/
			fundsCollection.setOperationState(null);
			update(fundsCollection);
		}
	}
	
	/**
	 * Registro de rechao de igualacion de glosa
	 * @throws ServiceException 
	 * */
	@LoggerAuditWeb
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public void recordGlossConfirm(DetailMatchGlossTO detailMatchGlossTO,List<FundsCollection> fundsCollections) throws ServiceException{
		EquivalentDepositGlosses eqDepositGloss = null;
		/*Actualizando deposito*/
		eqDepositGloss = getEquivalentDepositGlosses(detailMatchGlossTO.getIdDetailCollectionGloss(),detailMatchGlossTO.getIdEqDepositGlossesPk(),StatesConstants.REGISTERED_MATCH_GLOSSES);
		eqDepositGloss.setOperationState(StatesConstants.CONFIRMED_MATCH_GLOSSES);
		eqDepositGloss.setConfirmDate(CommonsUtilities.currentDate());
		eqDepositGloss.setConfirmUser(userInfo.getUserAccountSession().getUserName());
		update(eqDepositGloss);
		for (FundsCollection fundsCollection : fundsCollections) {
			fundsCollection.setOperationState(StatesConstants.CONFIRMED_MATCH_GLOSSES);
			update(fundsCollection);
		}
		/*Actualizamos la tabla principal*/
		DetailCollectionGloss collectionGloss = this.find(DetailCollectionGloss.class,detailMatchGlossTO.getIdDetailCollectionGloss());
		if(collectionGloss.getTransferStatus()!=null&&
				collectionGloss.getTransferStatus().equals(RetirementStateType.CONFIRMED.getCode())){
			log.info("CDE:::: El registro se ya se encuentra transferido, no se puede actualizar :"+collectionGloss.getGlosaNotClassified());
			throw new ServiceException();
		}
		collectionGloss.setDepositStatus(DepositStateType.CONFIRMED.getCode());
		collectionGloss.setDepositDate(CommonsUtilities.currentDate());
		collectionGloss.setAmountDeposited(eqDepositGloss.getTotalAmount());
		this.update(collectionGloss);
		log.info("CDE:::: Se actualizo el monto de la glosa :"+collectionGloss.getGlosaNotClassified());
	}
	/**
	 * Busqueda de estado de un registro
	 * **/
	public Integer getOperationStateMatchGloss(Long idEqDepositGlossesPk ){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select edg.operationState from EquivalentDepositGlosses edg ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and  edg.idEqDepositGlossesPk = :idEqDepositGlossesPk ");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		query.setParameter("idEqDepositGlossesPk",idEqDepositGlossesPk);
		
		Integer varResult= (Integer) query.getSingleResult();
		
		return varResult;
	}
	/**
	 * Busqueda de equivalencias de glosa y depositosS
	 * **/
	private EquivalentDepositGlosses getEquivalentDepositGlosses(Long idDetailCollectionGlossFk,Long idEqDepositGlossesPk,Integer operationState){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select edg from EquivalentDepositGlosses edg ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and  edg.idDetailCollectionGlossFk.idDetailCollectionGlossPk = :idDetailCollectionGlossFk ");
		queryBuilder.append(" and  edg.idEqDepositGlossesPk = :idEqDepositGlossesPk ");
		queryBuilder.append(" and  edg.operationState = :operationState ");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		query.setParameter("idDetailCollectionGlossFk",idDetailCollectionGlossFk);
		query.setParameter("idEqDepositGlossesPk",idEqDepositGlossesPk);
		query.setParameter("operationState",operationState);
		
		EquivalentDepositGlosses varResult = (EquivalentDepositGlosses) query.getSingleResult();
		
		return varResult;
	}
	public boolean isExistRegisterGloss(Long idDetailCollectionGlossPk){
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select edg.idEqDepositGlossesPk from EquivalentDepositGlosses edg ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and  edg.idDetailCollectionGlossFk.idDetailCollectionGlossPk = :idDetailCollectionGlossFk ");
		queryBuilder.append(" and  edg.operationState in ("+StatesConstants.CONFIRMED_MATCH_GLOSSES+","+StatesConstants.REGISTERED_MATCH_GLOSSES+")");
		
		Query query = em.createQuery(queryBuilder.toString());
		
		query.setParameter("idDetailCollectionGlossFk",idDetailCollectionGlossPk);
		
		try {
			query.getSingleResult();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Buscamos lista de fondos confirmados
	 * **/
	public BigDecimal getSumFundCollection(String gloss, Short currencyBcb){
		StringBuilder builder = new StringBuilder();
		builder.append(" select sum(fc.amount) from FundsCollection fc ");
		builder.append(" where 1 = 1 ");
		builder.append(" and fc.originCurrency = :currencyBcb ");
		builder.append(" and fc.gloss = :gloss ");
		builder.append(" and fc.operationState = "+StatesConstants.CONFIRMED_MATCH_GLOSSES);

		Query query = em.createQuery(builder.toString());
		query.setParameter("currencyBcb",currencyBcb );
		query.setParameter("gloss",gloss );
		
		try {
			BigDecimal sum = (BigDecimal) query.getSingleResult();
			return sum;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return BigDecimal.ZERO;
	}
}
