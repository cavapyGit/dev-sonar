package com.pradera.collection.monitor.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.bank.account.service.BankAccountServiceBean;
import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.DetailMatchGlossTO;
import com.pradera.collection.monitor.to.ManualDepositTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.FundsCollection;

@DepositaryWebBean
@LoggerCreateBean
public class SearchDepositModificationForCollectionBean extends GenericBaseBean {

	/**
      * 
      */
	private static final long serialVersionUID = -8443666423562427891L;

	/**
	 * Redireccionamiento de paginas para modificacion de glosa o matcheo por
	 * depositos
	 */
	private static final String LOAD_REGISTRATION_PAGE = "registerModificationDepositGloss";
	
	private static final String LOAD_VIEW_PAGE = "viewModificationDepositGloss";
	
	private static final String RETURN_TO_SEARCH_PAGE_FROM_VIEW = "returnSearchPageFromView";

	private ManualDepositTO searchDepositTO, viewDepositTO;

	private FundsCollection fundsCollectionSelecction;

	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	@Inject
	private GeneralParametersFacade generalParametersFacade;

	@Inject
	private UserPrivilege userPrivilege;

	@Inject
	private BankAccountServiceBean accountServiceBean;

	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;
	
	@Inject
	private HelperComponentFacade helperComponentFacade;

	/* Lista de detalles de glosa */
	private GenericDataModel<DetailMatchGlossTO> modelEquivalentGloss;
	/* Lista*/
	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;
	
	private DetailMatchGlossTO equivalentSelected;
	
	/*Variables para vizualizar el registro*/
	private GenericDataModel<DetailCollectionGloss> dataModelDetailCollectionGloss;
	
	private GenericDataModel<FundsCollection> dataModelFundCollections;
	
	/* Objeto de detalle de glosa seleccionada */
	private DetailCollectionGloss idGlossSelected;

	private List<FundsCollection> idFundCollestions;

	private GenericDataModel<FundsCollection> listFundsCollectons;
	
	List<Participant> listParticipant = null;
	
	List<ParameterTable> listCurrency = null;
	
	List<Bank> listBanks = null;
	
	@PostConstruct
	public void init() {
		searchDepositTO = new ManualDepositTO();
		if(listParticipant==null)
			listParticipant = parameterServiceBean.getListParticipantForCollection();
		searchDepositTO.setListParticipant(listParticipant);
		if(listCurrency == null)
			listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		searchDepositTO.setListCurrency(listCurrency);
		searchDepositTO.setRegistreDate(CommonsUtilities.currentDate());
		if(listBanks==null)
			listBanks = accountServiceBean.getListAllBank();
		searchDepositTO.setListBanks(listBanks);
		searchDepositTO.setDepositStatus(null);
		searchDepositTO.setPaymentDate(CommonsUtilities.currentDate());
		searchDepositTO.setHolder(new Holder());
		modelEquivalentGloss = null;
		equivalentSelected = null;
		onChangeOperationDate();
		lstAccountTo = new GenericDataModel<>();
	}

	/**
	 * Cargamos la pagina de registro
	 * 
	 */
	public String loadPageRegistreDeposit() {
		return this.LOAD_REGISTRATION_PAGE;
	}


	/**
	 * Buscamos todos los depositos
	 */
	public void preSearchResults(){
		List<DetailMatchGlossTO> list = collectionEconomicFacade.searchGlossMatchs(searchDepositTO);
		modelEquivalentGloss = new GenericDataModel<>(list);
	}

	/**
	 * metodo para retornar la moneda segun corresponda
	 * 
	 * @param currencyCode
	 * @return
	 */
	public String getTextCurrency(Integer currencyCode) {
		/* Bolivianos */
		if (currencyCode.equals(CurrencyType.PYG.getCode()))
			return CurrencyType.PYG.getValue();
		/* Dolares */
		if (currencyCode.equals(CurrencyType.USD.getCode()))
			return CurrencyType.USD.getValue();
		return "-";
	}
	
	/**Rechazar solicitud
	 * */
	public void rejectGlossMatchRecord(){
		/*Lanzamos dialogo de confirmacion*/
		JSFUtilities.updateComponent("opnlDialogs");
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.title.message.dialog.correction",equivalentSelected.getIdDetailCollectionGloss());
		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		
		JSFUtilities.executeJavascriptFunction("PF('wVRegistreGlossingMatchReject').show();");
	}
	/*Ejecucion de rechazo de */
	public void runRejectGlossMatchRecord(){
		try {
			collectionEconomicFacade.recordGlossRejection(equivalentSelected,idFundCollestions);
		} catch (ServiceException e) {
			e.printStackTrace();
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.modification.was.made.verify");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		/*Lanzamos dialogo de aviso de confirmacion exitosa el rechazo*/
		JSFUtilities.updateComponent("opnlDialogs");
		String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.dialog.reject.gloss",equivalentSelected.getIdEqDepositGlossesPk());
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('wVRegistreGlossingMatchRejectConfirm').show();");
	}
	/**
	 * Se ejecuta la confirmacion de la solicitud
	 * **/
	public void runConfimSGlossMatchRecord(){
		try {
			collectionEconomicFacade.recordGlossConfirm(equivalentSelected,idFundCollestions);
			} catch (ServiceException e) {
			e.printStackTrace();
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.modification.was.made.verify");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		/*Lanzamos dialogo de aviso de confirmacion exitosa*/
		JSFUtilities.updateComponent("opnlDialogs");
		String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.dialog.confirm.gloss",equivalentSelected.getIdEqDepositGlossesPk());
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('wVRegistreGlossingMatchConfirm').show();");
	}
	/**
	 * carga para ver el detalle y confirmar la solictud*/
	public String preConfirmGlossMatchRecord(){
		Integer stateOp = collectionEconomicFacade.getOperationStateMatchGloss(equivalentSelected.getIdEqDepositGlossesPk());
		if(stateOp.equals(StatesConstants.CONFIRMED_MATCH_GLOSSES)||stateOp.equals(StatesConstants.REJECT_MATCH_GLOSSES)	){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("msg.validation.required.state.register");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		if(equivalentSelected==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.no.select");
			
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			
			JSFUtilities.showSimpleValidationDialog();
		}else{
			viewDepositTO = new ManualDepositTO();
			viewDepositTO.setBtnConfirm(true);
			viewDepositTO.setBtnReject(false);
			loadDataForDiewDetail();
			return LOAD_VIEW_PAGE;
		}
		return CommonsUtilities.STR_BLANK;
	}
	
	/**Confirmamo la solicitud*/
	public void confirmGlossMatchRecord(){
		JSFUtilities.updateComponent("opnlDialogs");
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.title.message.dialog.confirm.gloss",equivalentSelected.getIdEqDepositGlossesPk());
		
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		JSFUtilities.executeJavascriptFunction("PF('wVConfirmGlossingMatch').show();");
	}

	/***
	 * metodo para cargar los datos para el view de deposistos
	 * 
	 * @param event
	 * 
	 * */
	public void loadDetailDepositMathGloss(ActionEvent event) {
		DetailMatchGlossTO detailMatchGlossTO = new DetailMatchGlossTO();
		detailMatchGlossTO = (DetailMatchGlossTO) event.getComponent().getAttributes().get("resultDepositMathGloss");
		viewDepositTO = new ManualDepositTO();
		/*Configurando botones*/
		viewDepositTO.setBtnConfirm(false);
		viewDepositTO.setBtnReject(false);
		/*carga de datos para vista*/
		equivalentSelected =detailMatchGlossTO; 
		loadDataForDiewDetail();
	}
	/**
	 * Carga de detaos para la vizualizacion del registro
	 * */
	private void loadDataForDiewDetail(){
		/*cargando participantes*/
		viewDepositTO.setListParticipant(searchDepositTO.getListParticipant());
		viewDepositTO.setIdParticipantPk(equivalentSelected.getFilterIdParticipantPk());
		/*cargando moneda*/
		viewDepositTO.setListCurrency(searchDepositTO.getListCurrency());
		/*Cargando moneda*/
		viewDepositTO.setCurrency(equivalentSelected.getFilterCurrency());
		/*Cargando fecha de cobro como filtro de busqueda*/
		viewDepositTO.setPaymentDate(equivalentSelected.getFilterPaymentDate());
		/*Cargando glosa como filtro*/
		viewDepositTO.setGloss(equivalentSelected.getFilterGloss());
		/*Cargamos la glosa seleccionada*/
		idGlossSelected = new DetailCollectionGloss(equivalentSelected.getIdDetailCollectionGloss());
		/*Buscamos todas las glosas generadas en esa fecha*/
		List<DetailCollectionGloss> listDetailCollectionGloss = collectionEconomicFacade.searchGlossDetails(equivalentSelected.getIdEqDepositGlossesPk());
		/*Solo mostrar glosa que no esten registradas*/
		dataModelDetailCollectionGloss = new GenericDataModel<>(listDetailCollectionGloss);
		/*buscamos todas los depositos registrados al momento de hacer lectura del BCB*/
		/*enableState = true : Considera solo que esten en nulo*/
		List<FundsCollection> listFundCollections = collectionEconomicFacade.searchFundCollections(equivalentSelected.getIdEqDepositGlossesPk());
		dataModelFundCollections = new GenericDataModel<>(listFundCollections);
		/*Cargar lista de fondos seleccionados*/
		idFundCollestions = equivalentSelected.getListFundsCollection();
	}



	/**
	 * Cambio de banco, se debe actualizar el BCB CODE
	 **/
	public void onChangeBank() {
		if (searchDepositTO.getIdBankPk() != null) {
			Bank bank = parameterServiceBean.find(Bank.class,searchDepositTO.getIdBankPk());
			if (bank.getIdBcbCode() != null)
				searchDepositTO.setBcbCode(bank.getIdBcbCode().toString());
			else {
				JSFUtilities.putViewMap("bodyMessageView", "!Alerta!");
				JSFUtilities.putViewMap("headerMessageView","El Banco seleccionado no cuenta con codigo BIC");
				JSFUtilities.showSimpleValidationDialog();
				searchDepositTO.setIdBankPk(null);
			}
		} else
			searchDepositTO.setBcbCode(null);
	}

	/** Cambio de participante en la pantalla de registro */
	public void onChangeParticipant() {
		if (searchDepositTO.getIdParticipantPk() != null) {
			searchDepositTO.setIdBankPk(null);
			searchDepositTO.setBcbCode(null);
			searchDepositTO.setCurrency(null);
			searchDepositTO.setGloss(null);
			searchDepositTO.setAmountDeposited(null);
		}
	}

	/**
	 * Cambio de evento de fecha en la vista
	 * */
	public void onChangeOperationDate() {
		if (searchDepositTO.getRegistreDate() != null) {
			List<String> listGloss = collectionEconomicFacade
					.getListGlossCollection(searchDepositTO.getRegistreDate(),
							searchDepositTO.getIdParticipantPk(),
							searchDepositTO.getDepositStatus(), null);
			searchDepositTO.setListGloss(listGloss);
			searchDepositTO.setCurrency(null);
			searchDepositTO.setAmountDeposited(null);
		} else {
			searchDepositTO.setGloss(null);
			searchDepositTO.setCurrency(null);
			searchDepositTO.setAmountDeposited(null);
		}
		JSFUtilities.updateComponent("idGlossSearch");
	}

	/**
	 *  Limpiar pantalla */
	public void cleanView() {
		if (existDataInView(searchDepositTO)) {
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVCleanSearchDeposit').show();");
		} else {
			init();
			JSFUtilities.updateComponent("frmSearchDeposit");
			
			JSFUtilities.updateComponent("idHelpHolder");
			
		}
	}

	 /**
     * Cui change.
     */
    public void holderChangeEvent(){	
    	try {
			Holder holder = searchDepositTO.getHolder();		
			if (Validations.validateIsNull(holder) || Validations.validateIsNull(holder.getIdHolderPk())) {

			} else {
				HolderTO holderTO = new HolderTO();
				holderTO.setHolderId(holder.getIdHolderPk());
				holderTO.setFlagAllHolders(true);
				holderTO.setParticipantFk(searchDepositTO.getIdParticipantPk());
				List<HolderAccountHelperResultTO> lista = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if (lista==null || lista.isEmpty()){
					lstAccountTo = new GenericDataModel<>();
				}else{
					lstAccountTo = new GenericDataModel<>(lista);
					if(lista.size() == ComponentConstant.ONE){
						searchDepositTO.setSelectedAccountTO(lista.get(0)); 
						JSFUtilities.updateComponent("frmSearchMatchDepositGloss:idResultHelperMatchGloss");
					}
				}
				if(lstAccountTo.getSize()<0){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getGenericMessage("Participante incorrecto"));//PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
					searchDepositTO.setHolder(new Holder());
					JSFUtilities.showSimpleValidationDialog();
				} else {
					List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
					for (HolderAccountHelperResultTO objAccount : lstAccountTo) 
						for (ParameterTable parameterTable : lstHolderAccountState) 
							objAccount.setAccountStatusDescription(parameterTable.getParameterName());
				}
			}
			if (lstAccountTo!=null){
				//JSFUtilities.executeJavascriptFunction("resultHelperMatchGloss.clearFilters(); resultHelperMatchGloss.unselectAllRows();");
				//JSFUtilities.updateComponent("frmSearchMatchDepositGloss:idResultHelperMatchGloss");
				JSFUtilities.updateComponent("idResultHelperMatchGloss");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
    /**
	 * Cambio de evento de fecha de cobro y actulizar glosas de acuerdo a resultado
	 * */
	public void onChangePaymentDate() {
		if (searchDepositTO.getPaymentDate() != null) {
			List<String> listGloss = collectionEconomicFacade.getListGlossCollection(searchDepositTO.getPaymentDate(),searchDepositTO.getIdParticipantPk(), null,null);
			searchDepositTO.setListGloss(listGloss);
			searchDepositTO.setGloss(null);
		} else {
			searchDepositTO.setGloss(null);
		}
	}

	/** Existe data en la visata para ejecutar la limpieza? */
	private boolean existDataInView(ManualDepositTO depositTO) {
		String strDate = CommonsUtilities.convertDatetoStringForTemplate(CommonsUtilities.currentDate(),"dd/MM/yyyy");
		if (depositTO.getIdParticipantPk() != null)
			return true;
		if (depositTO.getCurrency() != null)
			return true;
		if (depositTO.getIdBankPk() != null)
			return true;
		if (depositTO.getGloss() != null)
			return true;
		if (depositTO.getAmountDeposited() != null)
			return true;
		if(!CommonsUtilities.convertDatetoStringForTemplate(depositTO.getPaymentDate(),"dd/MM/yyyy").equals(strDate))
			return true;
		if(depositTO.getHolder().getIdHolderPk()!=null)	
			return true;
		return false;
	}

	/**
	 * Rechazando el registros*/
	public String preRejectGlosaMatchRecord(){
		if(equivalentSelected==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.no.select");
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		Integer stateOp = collectionEconomicFacade.getOperationStateMatchGloss(equivalentSelected.getIdEqDepositGlossesPk());
		if(stateOp.equals(StatesConstants.CONFIRMED_MATCH_GLOSSES)||stateOp.equals(StatesConstants.REJECT_MATCH_GLOSSES)	){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("msg.validation.required.state.register");
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
		viewDepositTO = new ManualDepositTO();
		viewDepositTO.setBtnConfirm(false);
		viewDepositTO.setBtnReject(true);
		loadDataForDiewDetail();
		return LOAD_VIEW_PAGE;
	}  
	/**
	 * Regresar a la pagina principal
	 * */
	public String backViewMainPageSearch(){
			preSearchResults();
			return RETURN_TO_SEARCH_PAGE_FROM_VIEW;
	}
	
	public ManualDepositTO getSearchDepositTO() {
		return searchDepositTO;
	}

	public void setSearchDepositTO(ManualDepositTO searchDepositTO) {
		this.searchDepositTO = searchDepositTO;
	}

	public ManualDepositTO getViewDepositTO() {
		return viewDepositTO;
	}

	public void setViewDepositTO(ManualDepositTO viewDepositTO) {
		this.viewDepositTO = viewDepositTO;
	}

	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}

	public FundsCollection getFundsCollectionSelecction() {
		return fundsCollectionSelecction;
	}

	public void setFundsCollectionSelecction(
			FundsCollection fundsCollectionSelecction) {
		this.fundsCollectionSelecction = fundsCollectionSelecction;
	}

	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}

	public void setLstAccountTo(
			GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}

	public GenericDataModel<DetailMatchGlossTO> getModelEquivalentGloss() {
		return modelEquivalentGloss;
	}

	public void setModelEquivalentGloss(
			GenericDataModel<DetailMatchGlossTO> modelEquivalentGloss) {
		this.modelEquivalentGloss = modelEquivalentGloss;
	}

	public DetailMatchGlossTO getEquivalentSelected() {
		return equivalentSelected;
	}

	public void setEquivalentSelected(DetailMatchGlossTO equivalentSelected) {
		this.equivalentSelected = equivalentSelected;
	}

	public GenericDataModel<DetailCollectionGloss> getDataModelDetailCollectionGloss() {
		return dataModelDetailCollectionGloss;
	}

	public void setDataModelDetailCollectionGloss(
			GenericDataModel<DetailCollectionGloss> dataModelDetailCollectionGloss) {
		this.dataModelDetailCollectionGloss = dataModelDetailCollectionGloss;
	}

	public GenericDataModel<FundsCollection> getDataModelFundCollections() {
		return dataModelFundCollections;
	}

	public void setDataModelFundCollections(
			GenericDataModel<FundsCollection> dataModelFundCollections) {
		this.dataModelFundCollections = dataModelFundCollections;
	}

	public DetailCollectionGloss getIdGlossSelected() {
		return idGlossSelected;
	}

	public void setIdGlossSelected(DetailCollectionGloss idGlossSelected) {
		this.idGlossSelected = idGlossSelected;
	}

	public List<FundsCollection> getIdFundCollestions() {
		return idFundCollestions;
	}

	public void setIdFundCollestions(List<FundsCollection> idFundCollestions) {
		this.idFundCollestions = idFundCollestions;
	}

	public GenericDataModel<FundsCollection> getListFundsCollectons() {
		return listFundsCollectons;
	}

	public void setListFundsCollectons(
			GenericDataModel<FundsCollection> listFundsCollectons) {
		this.listFundsCollectons = listFundsCollectons;
	}
}