package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.FundsCollection;

public class EquivalentDepositTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2324771357784750685L;

	/*Glosa seleccionada*/
	private Long idDetailCollectionGlossPk;
	/*Glosas mostradas en la pantalla*/
	private List<DetailCollectionGloss> listDetailCollectionShow;
	/*Lista de fondos seleccionados*/
	private List<FundsCollection> listFundCollections;
	/*Lista de fondos mostrados en la pantalla*/
	private List<FundsCollection> listFundsCollectionShow;
	private String registerUser;
	
	private String confirmUser;
	
	private Date registerDate;
	
	private Date confirmDate;
	/*Filtros*/
	private Long idParticipantPk;
	
	private Date paymentDate;
	
	private String gloss;
	
	private Integer currency;
	
	public EquivalentDepositTO() {
	}

	public Long getIdDetailCollectionGlossPk() {
		return idDetailCollectionGlossPk;
	}

	public void setIdDetailCollectionGlossPk(Long idDetailCollectionGlossPk) {
		this.idDetailCollectionGlossPk = idDetailCollectionGlossPk;
	}

	public List<DetailCollectionGloss> getListDetailCollectionShow() {
		return listDetailCollectionShow;
	}

	public void setListDetailCollectionShow(List<DetailCollectionGloss> listDetailCollectionShow) {
		this.listDetailCollectionShow = listDetailCollectionShow;
	}

	public List<FundsCollection> getListFundCollections() {
		return listFundCollections;
	}

	public void setListFundCollections(List<FundsCollection> listFundCollections) {
		this.listFundCollections = listFundCollections;
	}

	public List<FundsCollection> getListFundsCollectionShow() {
		return listFundsCollectionShow;
	}

	public void setListFundsCollectionShow(List<FundsCollection> listFundsCollectionShow) {
		this.listFundsCollectionShow = listFundsCollectionShow;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public String getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
}
