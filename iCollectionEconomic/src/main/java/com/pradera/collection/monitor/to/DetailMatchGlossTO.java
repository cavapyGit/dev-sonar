package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.edv.collection.economic.rigth.FundsCollection;

public class DetailMatchGlossTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7913467708004687208L;
	

	private Long idEqDepositGlossesPk;
	
	private String descParticipant;
	
	private String gloss;
	
	private String descPaymentAgent;
	
	private Date paymentDate;
	
	private Integer currency;
	
	private Long requestNumber;
	
	private BigDecimal amountReceivable;
	
	private BigDecimal amount;
	
	/*filtros de busqueda*/
	private Long filterIdParticipantPk;
	
	private Integer filterCurrency;
	
	private Date filterPaymentDate;
	
	private String filterGloss;
	
	private Long idDetailCollectionGloss;
	
	private String glossLip;
	
	private String stateDesc;
	
	private List<FundsCollection> listFundsCollection;

	public Long getIdEqDepositGlossesPk() {
		return idEqDepositGlossesPk;
	}

	public void setIdEqDepositGlossesPk(Long idEqDepositGlossesPk) {
		this.idEqDepositGlossesPk = idEqDepositGlossesPk;
	}

	public String getDescParticipant() {
		return descParticipant;
	}

	public void setDescParticipant(String descParticipant) {
		this.descParticipant = descParticipant;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public String getDescPaymentAgent() {
		return descPaymentAgent;
	}

	public void setDescPaymentAgent(String descPaymentAgent) {
		this.descPaymentAgent = descPaymentAgent;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Long getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}

	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getFilterIdParticipantPk() {
		return filterIdParticipantPk;
	}

	public void setFilterIdParticipantPk(Long filterIdParticipantPk) {
		this.filterIdParticipantPk = filterIdParticipantPk;
	}

	public Integer getFilterCurrency() {
		return filterCurrency;
	}

	public void setFilterCurrency(Integer filterCurrency) {
		this.filterCurrency = filterCurrency;
	}

	public Date getFilterPaymentDate() {
		return filterPaymentDate;
	}

	public void setFilterPaymentDate(Date filterPaymentDate) {
		this.filterPaymentDate = filterPaymentDate;
	}

	public String getFilterGloss() {
		return filterGloss;
	}

	public void setFilterGloss(String filterGloss) {
		this.filterGloss = filterGloss;
	}

	public Long getIdDetailCollectionGloss() {
		return idDetailCollectionGloss;
	}

	public void setIdDetailCollectionGloss(Long idDetailCollectionGloss) {
		this.idDetailCollectionGloss = idDetailCollectionGloss;
	}

	public String getGlossLip() {
		return glossLip;
	}

	public void setGlossLip(String glossLip) {
		this.glossLip = glossLip;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public List<FundsCollection> getListFundsCollection() {
		return listFundsCollection;
	}

	public void setListFundsCollection(List<FundsCollection> listFundsCollection) {
		this.listFundsCollection = listFundsCollection;
	}
	
}
