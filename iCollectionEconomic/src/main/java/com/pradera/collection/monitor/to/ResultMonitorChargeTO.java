package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.util.Date;

public class ResultMonitorChargeTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6346060140455343383L;

	private String idParticipantOrIssuer;
	
	private Date paymentDate;
	
	private Integer currency;
	
	private boolean paidObligation;
	
	public ResultMonitorChargeTO() {
	}

	public String getIdParticipantOrIssuer() {
		return idParticipantOrIssuer;
	}

	public void setIdParticipantOrIssuer(String idParticipantOrIssuer) {
		this.idParticipantOrIssuer = idParticipantOrIssuer;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public boolean isPaidObligation() {
		return paidObligation;
	}

	public void setPaidObligation(boolean paidObligation) {
		this.paidObligation = paidObligation;
	}
}
