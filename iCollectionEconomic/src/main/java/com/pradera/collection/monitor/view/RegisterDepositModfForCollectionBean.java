package com.pradera.collection.monitor.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.pradera.collection.monitor.facade.MonitoriCollectionEconomicFacade;
import com.pradera.collection.monitor.to.EquivalentDepositTO;
import com.pradera.collection.monitor.to.ManualDepositTO;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.FundsCollection;

@DepositaryWebBean
@LoggerCreateBean
public class RegisterDepositModfForCollectionBean extends GenericBaseBean {

	/**
      * 
      */
	private static final long serialVersionUID = -8443666423562427891L;

	/**
	 * Redireccionamiento de paginas para modificacion de glosa o matcheo por
	 * depositos
	 */

	private static final String RETURN_TO_SEARCH_PAGE = "returnSearchPage";

	private ManualDepositTO registreDepositTO;

	private GenericDataModel<DetailCollectionGloss> dataModelDetailCollectionGloss;
	
	private GenericDataModel<FundsCollection> dataModelFundCollections;
	
	/* Objeto de detalle de glosa seleccionada */
	private DetailCollectionGloss idGlossSelected;

	private List<FundsCollection> idFundCollestions;

	@Inject
	private ParameterServiceBean parameterServiceBean;

	@Inject
    private UserInfo userInfo;
	
	@Inject
	private PraderaLogger log;

	@Inject
	private MonitoriCollectionEconomicFacade collectionEconomicFacade;

	private GenericDataModel<FundsCollection> listFundsCollectons;
	
	@Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;

	@PostConstruct
	public void init() {
		registreDepositTO = new ManualDepositTO();
		Date minDate = holidayQueryServiceBean.subtractBusinessDays(CommonsUtilities.currentDate(), 3);
		registreDepositTO.setMinDate(minDate);
		List<Participant> listParticipant = parameterServiceBean.getListParticipantForCollection();
		registreDepositTO.setListParticipant(listParticipant);
		List<ParameterTable> listCurrency = collectionEconomicFacade.listCurrencyForCollection();
		registreDepositTO.setListCurrency(listCurrency);
		registreDepositTO.setPaymentDate(CommonsUtilities.currentDate());
		registreDepositTO.setDepositStatus(null);
		listFundsCollectons = null;
		idGlossSelected = null;
		onChangePaymentDate();
		idFundCollestions = new ArrayList<>();
		dataModelDetailCollectionGloss = null;
	}

	/**
	 * Buscamos los detalles de glosa registrados para cobros y 
	 * todos los depositos registrados
	 * */
	public void searchGlossDetail() {
		/*Buscamos todas las glosas generadas en esa fecha*/
		List<DetailCollectionGloss> listDetailCollectionGloss = collectionEconomicFacade.searchDetailsCollectionGlossNotConfirmed(registreDepositTO);
		/*Solo mostrar glosa que no esten registradas*/
		List<DetailCollectionGloss> listFinal = new ArrayList<>();
		for (DetailCollectionGloss detailCollectionGloss : listDetailCollectionGloss) {
			/*verificamos si la glosa ya esta en otra solictud anterior registrada*/
			if(!collectionEconomicFacade.isExistRegisterGloss(detailCollectionGloss.getIdDetailCollectionGlossPk()))
				listFinal.add(detailCollectionGloss);
		}
		dataModelDetailCollectionGloss = new GenericDataModel<>(listFinal);
		/*buscamos todas los depositos registrados al momento de hacer lectura del BCB*/
		/*enableState = true : Considera solo que esten en nulo*/
		if(listDetailCollectionGloss!=null||listDetailCollectionGloss.size()>0){
			List<FundsCollection> listFundCollections = collectionEconomicFacade.searchFundCollections(registreDepositTO.getPaymentDate(),registreDepositTO.getCurrency(),true,registreDepositTO.getGloss());
			dataModelFundCollections = new GenericDataModel<>(listFundCollections);
		}
	}
	/** Cambio de participante en la pantalla de registro */
	public void onChangeParticipant() {
		if (registreDepositTO.getIdParticipantPk() != null) {
			registreDepositTO.setIdBankPk(null);
			registreDepositTO.setBcbCode(null);
			registreDepositTO.setCurrency(null);
			registreDepositTO.setGloss(null);
			registreDepositTO.setAmountDeposited(null);
		}
	}

	/* Limpiar pantalla */
	public void preCleanRegistrationScreen() {
		if (existDataInView(registreDepositTO)) {
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVCleanSearchDeposit').show();");
		} else {
			init();
			JSFUtilities.updateComponent("frmRegistreMatchGloss");
		}
	}

	/** Existe data en la visata para ejecutar la limpieza? */
	private boolean existDataInView(ManualDepositTO depositTO) {
		if(idGlossSelected!=null||
				idFundCollestions.size()>0||
		   registreDepositTO.getIdParticipantPk()!=null||
		   registreDepositTO.getCurrency()!=null||
		   registreDepositTO.getGloss()!=null)
			return true;
		else
			return false;
	}

	/**
	 * Cambio de evento de fecha de cobro
	 * */
	public void onChangePaymentDate() {
		if (registreDepositTO.getPaymentDate() != null) {
			List<String> listGloss = collectionEconomicFacade.getListGlossCollection(registreDepositTO.getPaymentDate(),registreDepositTO.getIdParticipantPk(), null,null);
			registreDepositTO.setListGloss(listGloss);
			registreDepositTO.setGloss(null);
		} else {
			registreDepositTO.setGloss(null);
		}
	}
	
	/**
	 * Guardar los datos 
	 * */
	public void preSaveEqualityGlosses() {
		if (idGlossSelected==null||idFundCollestions.size()==0) {
			/*Alerta de glosa notificada*/
			if(idGlossSelected==null){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.select.notified.deposit");
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(idFundCollestions.size()==0){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.select.needed.central.bank");
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}else{
			/*Verificar la suma de los montos que se mayor o igual*/
			BigDecimal sumAmount = BigDecimal.ZERO;
			for (FundsCollection fundsCollection : idFundCollestions) 
				sumAmount = sumAmount.add(fundsCollection.getAmount());
			
			
			sumAmount = sumAmount.add(idGlossSelected.getAmountDeposited()==null?BigDecimal.ZERO:idGlossSelected.getAmountDeposited());
			if(sumAmount.compareTo(idGlossSelected.getAmountReceivable())==-1){
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.dialgog.no.match.amount"); //PropertiesUtilities.getMessage("lbl.message.alert.dialgog.no.match.amount");
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			/*Dialogo de confirmacion de registro*/
			String listIdFundCollectionString="";
			for (FundsCollection fundCollection : idFundCollestions) {
				listIdFundCollectionString = listIdFundCollectionString+fundCollection.getNumberRequest()+",";
			}
			listIdFundCollectionString = listIdFundCollectionString.substring(0, listIdFundCollectionString.length()-1);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.registre.gloss.equals",idGlossSelected.getGlosaNotClassified(),listIdFundCollectionString);
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
            JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVRegistreGlossingMatch').show();");
			JSFUtilities.updateComponent("opnlDialogs");
		}
		/*verificamos si ya existe una solicitud anterior*/
		if(collectionEconomicFacade.isExistRegisteredGloss(idGlossSelected.getIdDetailCollectionGlossPk())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.message.alert.registre");
			JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
			JSFUtilities.putViewMap("headerMessageView", headerMessage);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	/**
	 * Guardamos la igualacion de glosa*/
	public void saveEqualityGlosses(){
		EquivalentDepositTO depositTO = new EquivalentDepositTO();
		depositTO.setRegisterUser(userInfo.getUserAccountSession().getUserName());
		depositTO.setRegisterDate(CommonsUtilities.currentDate());
		depositTO.setIdDetailCollectionGlossPk(idGlossSelected.getIdDetailCollectionGlossPk());
		depositTO.setListFundCollections(idFundCollestions);
		/*Filtros de la pantalla de busqueda*/
		depositTO.setIdParticipantPk(registreDepositTO.getIdParticipantPk());
		depositTO.setCurrency(registreDepositTO.getCurrency());
		depositTO.setPaymentDate(registreDepositTO.getPaymentDate());
		depositTO.setGloss(registreDepositTO.getGloss());
		depositTO.setListDetailCollectionShow(dataModelDetailCollectionGloss.getDataList());
		depositTO.setListFundsCollectionShow(dataModelFundCollections.getDataList());
		/*Persistir el registro*/
		Long pk = null;
		try {
			pk = collectionEconomicFacade.recordGlossMatch(depositTO,userInfo,idGlossSelected.getGlosaNotClassified());
		} catch (ServiceException e) {
			log.error("CDE::: No se puso registrar la solicitud de igualacion de glosas");;
			e.printStackTrace();
		}
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.message.confim.regitre",pk.toString());
		JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
		JSFUtilities.putViewMap("headerMessageView", headerMessage);
		
		JSFUtilities.executeJavascriptFunction("PF('wVRegistreGlossingMatchSatisfactory').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	public String preBackViewMainPageRegiry(){
		if(existDataInView(registreDepositTO)){
		   JSFUtilities.executeJavascriptFunction("PF('wVBackMainPage').show();");
		   JSFUtilities.updateComponent("opnlDialogs");
		}else{
			return backViewMainPageRegiry();
		}
		return null;
	}
	
	/**
	 * Regresar a la pagina principal
	 * */
	public String backViewMainPageRegiry(){
		this.init();
		JSFUtilities.resetViewRoot();
		JSFUtilities.updateComponent("frmRegistreMatchGloss");
		return RETURN_TO_SEARCH_PAGE;
	}

	public ManualDepositTO getRegistreDepositTO() {
		return registreDepositTO;
	}

	public void setRegistreDepositTO(ManualDepositTO registreDepositTO) {
		this.registreDepositTO = registreDepositTO;
	}

	public GenericDataModel<DetailCollectionGloss> getDataModelDetailCollectionGloss() {
		return dataModelDetailCollectionGloss;
	}

	public void setDataModelDetailCollectionGloss(
			GenericDataModel<DetailCollectionGloss> dataModelDetailCollectionGloss) {
		this.dataModelDetailCollectionGloss = dataModelDetailCollectionGloss;
	}

	public GenericDataModel<FundsCollection> getDataModelFundCollections() {
		return dataModelFundCollections;
	}

	public void setDataModelFundCollections(
			GenericDataModel<FundsCollection> dataModelFundCollections) {
		this.dataModelFundCollections = dataModelFundCollections;
	}

	public DetailCollectionGloss getIdGlossSelected() {
		return idGlossSelected;
	}

	public void setIdGlossSelected(DetailCollectionGloss idGlossSelected) {
		this.idGlossSelected = idGlossSelected;
	}
	
	public List<FundsCollection> getIdFundCollestions() {
		return idFundCollestions;
	}

	public void setIdFundCollestions(List<FundsCollection> idFundCollestions) {
		this.idFundCollestions = idFundCollestions;
	}

	public GenericDataModel<FundsCollection> getListFundsCollectons() {
		return listFundsCollectons;
	}

	public void setListFundsCollectons(
			GenericDataModel<FundsCollection> listFundsCollectons) {
		this.listFundsCollectons = listFundsCollectons;
	}
}