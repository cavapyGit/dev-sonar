package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class RegistrebjectColletionTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3989376052896416086L;
	
	
	private Long idParticipantPk;
	
	private BigDecimal amountReceivable;
	
	private Integer currency;
	
	private String gloss;

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}

	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}
	
}
