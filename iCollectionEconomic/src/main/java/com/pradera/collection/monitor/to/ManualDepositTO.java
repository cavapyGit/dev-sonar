
package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.DepositStateType;
import com.pradera.model.generalparameter.type.RetirementStateType;

public class ManualDepositTO implements Serializable {

       private static final long serialVersionUID = 1L;

       private List<DepositStateType> lstDepositStatus;

       private Integer depositStatus;
       
       private Holder holder;

       private List<RetirementStateType> lstRetirementStatus;
       
       private HolderAccountHelperResultTO selectedAccountTO;
       
       private Integer retirementtStatus;

       private List<Participant> listParticipant;

       private Long idParticipantPk;

       private Long idBankPk;

       private List<Bank> listBanks;

       private String bcbCode;

       private List<ParameterTable> listCurrency;

       private Integer currency;

       private List<String> listGloss;

       private String gloss;

       private Date registreDate, maxDate, minDate, paymentDate;

       private String auxDepositFileName;

       private BigDecimal amountDeposited;
       
       private BigDecimal amountRetirement;

       private byte[] auxDepositFile;

       private boolean btnReject;

       private boolean btnConfirm;

       private boolean btnView;
       
       private boolean btnModify;
       
       private Long resquestNumber;
       

       //constructor
       public ManualDepositTO() {
             maxDate = null;
             minDate = null;
             List<DepositStateType> list =DepositStateType.lst;
             lstDepositStatus = new ArrayList<>();
             for(DepositStateType statusDep:list)
            	 if(!statusDep.getCode().equals(DepositStateType.REJECTED.getCode()))
            		 lstDepositStatus.add(statusDep);
             
             List<RetirementStateType> listAux =RetirementStateType.lst;
             lstRetirementStatus = new ArrayList<>();
             for(RetirementStateType status:listAux)
            	 if(!status.getCode().equals(RetirementStateType.REJECTED.getCode()))
            		 lstRetirementStatus.add(status);
             holder = new Holder();
       }


	public List<DepositStateType> getLstDepositStatus() {
		return lstDepositStatus;
	}


	public void setLstDepositStatus(List<DepositStateType> lstDepositStatus) {
		this.lstDepositStatus = lstDepositStatus;
	}


	public Integer getDepositStatus() {
		return depositStatus;
	}


	public void setDepositStatus(Integer depositStatus) {
		this.depositStatus = depositStatus;
	}


	public Holder getHolder() {
		return holder;
	}


	public void setHolder(Holder holder) {
		this.holder = holder;
	}


	public List<RetirementStateType> getLstRetirementStatus() {
		return lstRetirementStatus;
	}


	public void setLstRetirementStatus(List<RetirementStateType> lstRetirementStatus) {
		this.lstRetirementStatus = lstRetirementStatus;
	}


	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}


	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}


	public Integer getRetirementtStatus() {
		return retirementtStatus;
	}


	public void setRetirementtStatus(Integer retirementtStatus) {
		this.retirementtStatus = retirementtStatus;
	}


	public List<Participant> getListParticipant() {
		return listParticipant;
	}


	public void setListParticipant(List<Participant> listParticipant) {
		this.listParticipant = listParticipant;
	}


	public Long getIdParticipantPk() {
		return idParticipantPk;
	}


	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}


	public Long getIdBankPk() {
		return idBankPk;
	}


	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}


	public List<Bank> getListBanks() {
		return listBanks;
	}


	public void setListBanks(List<Bank> listBanks) {
		this.listBanks = listBanks;
	}


	public String getBcbCode() {
		return bcbCode;
	}


	public void setBcbCode(String bcbCode) {
		this.bcbCode = bcbCode;
	}


	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}


	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}


	public Integer getCurrency() {
		return currency;
	}


	public void setCurrency(Integer currency) {
		this.currency = currency;
	}


	public List<String> getListGloss() {
		return listGloss;
	}


	public void setListGloss(List<String> listGloss) {
		this.listGloss = listGloss;
	}


	public String getGloss() {
		return gloss;
	}


	public void setGloss(String gloss) {
		this.gloss = gloss;
	}


	public Date getRegistreDate() {
		return registreDate;
	}


	public void setRegistreDate(Date registreDate) {
		this.registreDate = registreDate;
	}


	public Date getMaxDate() {
		return maxDate;
	}


	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}


	public Date getMinDate() {
		return minDate;
	}


	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}


	public Date getPaymentDate() {
		return paymentDate;
	}


	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}


	public String getAuxDepositFileName() {
		return auxDepositFileName;
	}


	public void setAuxDepositFileName(String auxDepositFileName) {
		this.auxDepositFileName = auxDepositFileName;
	}


	public BigDecimal getAmountDeposited() {
		return amountDeposited;
	}


	public void setAmountDeposited(BigDecimal amountDeposited) {
		this.amountDeposited = amountDeposited;
	}


	public BigDecimal getAmountRetirement() {
		return amountRetirement;
	}


	public void setAmountRetirement(BigDecimal amountRetirement) {
		this.amountRetirement = amountRetirement;
	}


	public byte[] getAuxDepositFile() {
		return auxDepositFile;
	}


	public void setAuxDepositFile(byte[] auxDepositFile) {
		this.auxDepositFile = auxDepositFile;
	}


	public boolean isBtnReject() {
		return btnReject;
	}


	public void setBtnReject(boolean btnReject) {
		this.btnReject = btnReject;
	}


	public boolean isBtnConfirm() {
		return btnConfirm;
	}


	public void setBtnConfirm(boolean btnConfirm) {
		this.btnConfirm = btnConfirm;
	}


	public boolean isBtnView() {
		return btnView;
	}


	public void setBtnView(boolean btnView) {
		this.btnView = btnView;
	}


	public boolean isBtnModify() {
		return btnModify;
	}


	public void setBtnModify(boolean btnModify) {
		this.btnModify = btnModify;
	}


	public Long getResquestNumber() {
		return resquestNumber;
	}


	public void setResquestNumber(Long resquestNumber) {
		this.resquestNumber = resquestNumber;
	}
       
}