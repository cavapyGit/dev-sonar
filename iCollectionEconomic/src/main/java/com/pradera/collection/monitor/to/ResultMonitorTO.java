package com.pradera.collection.monitor.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;

public class ResultMonitorTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -509053716805859586L;
	
	private String gloss;
	
	private String partIssuer;
	
	private Long idParticipantPk;
	
	private String mnemonic;
	
	private String idIssuerFk;
	
	private Integer currency;
	
	private String currencyDesc;
	
	private Date paymentDate;
	
	private Date notificationDate;
	/*Monto a cobrar*/
	private BigDecimal amountReceivable;
	/*Moneda de deposito*/
	private Integer currencyDeposited;
	/**/
	private String currencyDepositedDesc;
	
	private Integer transferStatus;
	
	private String descTransferStatus;
	
	private boolean disableButton;
	
	/**Monto depositado en funds operation segun glosa*/
	private BigDecimal amountDeposited;
	
	private boolean paidObligation;
	
	private String paidObligationDesc;
	
	private String descJuridicPaymentAgent;
	
	public ResultMonitorTO() {
		descTransferStatus= CommonsUtilities.STR_BLANK;
	}
	
	public ResultMonitorTO(Long idParticipantPk, String mnemonic,String idIssuerFk,Date paymetDate,Integer currency,BigDecimal amountReceivable){
		this.idParticipantPk = idParticipantPk;
		this.mnemonic = mnemonic;
		this.idIssuerFk = idIssuerFk;
		this.paymentDate = paymetDate;
		this.currency = currency;
		this.amountReceivable = amountReceivable;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public String getPartIssuer() {
		return partIssuer;
	}

	public void setPartIssuer(String partIssuer) {
		this.partIssuer = partIssuer;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getIdIssuerFk() {
		return idIssuerFk;
	}

	public void setIdIssuerFk(String idIssuerFk) {
		this.idIssuerFk = idIssuerFk;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}

	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}

	public Integer getCurrencyDeposited() {
		return currencyDeposited;
	}

	public void setCurrencyDeposited(Integer currencyDeposited) {
		this.currencyDeposited = currencyDeposited;
	}

	public String getCurrencyDepositedDesc() {
		return currencyDepositedDesc;
	}

	public void setCurrencyDepositedDesc(String currencyDepositedDesc) {
		this.currencyDepositedDesc = currencyDepositedDesc;
	}

	public Integer getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(Integer transferStatus) {
		this.transferStatus = transferStatus;
	}

	public String getDescTransferStatus() {
		return descTransferStatus;
	}

	public void setDescTransferStatus(String descTransferStatus) {
		this.descTransferStatus = descTransferStatus;
	}

	public boolean isDisableButton() {
		return disableButton;
	}

	public void setDisableButton(boolean disableButton) {
		this.disableButton = disableButton;
	}

	public BigDecimal getAmountDeposited() {
		return amountDeposited;
	}

	public void setAmountDeposited(BigDecimal amountDeposited) {
		this.amountDeposited = amountDeposited;
	}

	public boolean isPaidObligation() {
		return paidObligation;
	}

	public void setPaidObligation(boolean paidObligation) {
		this.paidObligation = paidObligation;
	}

	public String getPaidObligationDesc() {
		return paidObligationDesc;
	}

	public void setPaidObligationDesc(String paidObligationDesc) {
		this.paidObligationDesc = paidObligationDesc;
	}

	public String getDescJuridicPaymentAgent() {
		return descJuridicPaymentAgent;
	}

	public void setDescJuridicPaymentAgent(String descJuridicPaymentAgent) {
		this.descJuridicPaymentAgent = descJuridicPaymentAgent;
	}
	
}
