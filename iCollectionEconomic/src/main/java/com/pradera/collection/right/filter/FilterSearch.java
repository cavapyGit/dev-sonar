package com.pradera.collection.right.filter;

import java.io.Serializable;


public class FilterSearch  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean disabled;
	
	public FilterSearch() {
		disabled = true;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
