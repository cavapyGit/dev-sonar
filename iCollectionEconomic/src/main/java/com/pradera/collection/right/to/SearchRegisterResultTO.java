package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.collection.right.filter.AmountChargeFilter;
import com.pradera.collection.right.filter.CollectionAccountBankFilter;
import com.pradera.collection.right.filter.CollectionBankFilter;
import com.pradera.collection.right.filter.CollectionCurrencyFilter;
import com.pradera.collection.right.filter.NotificationFilter;

public class SearchRegisterResultTO implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idCollectionEconomicPk;
	
	private Long idReqModCollection;
	
	private Long idGeneralPurposePk;
	
	private Long idParticipantPk;
	
	private String idIssuerPK;
	
	private Long idHolderPk;
	
	private String dueDate;
	
	private String alternateCode;
	
	private Date paymentDate;
	
	private String idSecurityCodePk;
	
	private Long idHolderAccountPk;
	
	private Integer accountNumber;
	
	private Integer couponNumber;
	
	private BigDecimal interestRate;
	
	private BigDecimal initialNominalValue;
	
	private String descPayment;
	
	private boolean interest;
	
	/*Moneda de cobro*/
	private Integer currency;
	
	/*Otra moneda de cobro*/
	private CollectionCurrencyFilter currencyFilter;
	
	private AmountChargeFilter amountChargeFilter;
	
	/**Tipos de Saldo*/
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private BigDecimal transitBalance;
	
	private BigDecimal pawnBalance;
	
	private BigDecimal banBalance;
	
	private BigDecimal otherBlockBalance;
	
	private BigDecimal saleBalance;
	
	private BigDecimal purchaseBalance;
	
	private BigDecimal reportedBalance;
	
	private BigDecimal reportingBalance;
	
	private BigDecimal marginBalance;
	
	private BigDecimal acreditationBalance;
	
	private BigDecimal borrowerBalance;
	
	private BigDecimal lenderBalance;
	
	private BigDecimal reserveBalance;
	
	private BigDecimal loanableBalance;
	
	private BigDecimal opositionBalance;
	
	private BigDecimal grantedBalance;
	
	private BigDecimal grantBalance;
	
	private BigDecimal amortization;
	
	private BigDecimal totalAmount;
	
	private Integer state;
	
	private CollectionBankFilter bankFilter;
	
	private boolean checkOther;
	
	private boolean checkCharge;
	
	private CollectionAccountBankFilter accountFilter;
	
	private NotificationFilter notificationFilter;
	
	private String currencyDesc;
	
	private String typeBalance;
	
	public SearchRegisterResultTO() {
		currencyFilter= new CollectionCurrencyFilter();
		amountChargeFilter = new AmountChargeFilter();
		accountFilter = new CollectionAccountBankFilter();
		bankFilter = new CollectionBankFilter();
		checkOther = false;
		totalAmount = BigDecimal.ZERO;
	}

	/** metodo para clonar objeto */
	@Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

	@Override
	public String toString() {
		return idSecurityCodePk;
	}

	public Long getIdCollectionEconomicPk() {
		return idCollectionEconomicPk;
	}

	public void setIdCollectionEconomicPk(Long idCollectionEconomicPk) {
		this.idCollectionEconomicPk = idCollectionEconomicPk;
	}

	public Long getIdReqModCollection() {
		return idReqModCollection;
	}

	public void setIdReqModCollection(Long idReqModCollection) {
		this.idReqModCollection = idReqModCollection;
	}

	public Long getIdGeneralPurposePk() {
		return idGeneralPurposePk;
	}

	public void setIdGeneralPurposePk(Long idGeneralPurposePk) {
		this.idGeneralPurposePk = idGeneralPurposePk;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public String getIdIssuerPK() {
		return idIssuerPK;
	}

	public void setIdIssuerPK(String idIssuerPK) {
		this.idIssuerPK = idIssuerPK;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getAlternateCode() {
		return alternateCode;
	}

	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}

	public BigDecimal getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	public BigDecimal getInitialNominalValue() {
		return initialNominalValue;
	}

	public void setInitialNominalValue(BigDecimal initialNominalValue) {
		this.initialNominalValue = initialNominalValue;
	}

	public String getDescPayment() {
		return descPayment;
	}

	public void setDescPayment(String descPayment) {
		this.descPayment = descPayment;
	}

	public boolean isInterest() {
		return interest;
	}

	public void setInterest(boolean interest) {
		this.interest = interest;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public CollectionCurrencyFilter getCurrencyFilter() {
		return currencyFilter;
	}

	public void setCurrencyFilter(CollectionCurrencyFilter currencyFilter) {
		this.currencyFilter = currencyFilter;
	}

	public AmountChargeFilter getAmountChargeFilter() {
		return amountChargeFilter;
	}

	public void setAmountChargeFilter(AmountChargeFilter amountChargeFilter) {
		this.amountChargeFilter = amountChargeFilter;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public BigDecimal getTransitBalance() {
		return transitBalance;
	}

	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}

	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	public BigDecimal getBanBalance() {
		return banBalance;
	}

	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	public BigDecimal getSaleBalance() {
		return saleBalance;
	}

	public void setSaleBalance(BigDecimal saleBalance) {
		this.saleBalance = saleBalance;
	}

	public BigDecimal getPurchaseBalance() {
		return purchaseBalance;
	}

	public void setPurchaseBalance(BigDecimal purchaseBalance) {
		this.purchaseBalance = purchaseBalance;
	}

	public BigDecimal getReportedBalance() {
		return reportedBalance;
	}

	public void setReportedBalance(BigDecimal reportedBalance) {
		this.reportedBalance = reportedBalance;
	}

	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	public BigDecimal getMarginBalance() {
		return marginBalance;
	}

	public void setMarginBalance(BigDecimal marginBalance) {
		this.marginBalance = marginBalance;
	}

	public BigDecimal getAcreditationBalance() {
		return acreditationBalance;
	}

	public void setAcreditationBalance(BigDecimal acreditationBalance) {
		this.acreditationBalance = acreditationBalance;
	}

	public BigDecimal getBorrowerBalance() {
		return borrowerBalance;
	}

	public void setBorrowerBalance(BigDecimal borrowerBalance) {
		this.borrowerBalance = borrowerBalance;
	}

	public BigDecimal getLenderBalance() {
		return lenderBalance;
	}

	public void setLenderBalance(BigDecimal lenderBalance) {
		this.lenderBalance = lenderBalance;
	}

	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}

	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}

	public BigDecimal getLoanableBalance() {
		return loanableBalance;
	}

	public void setLoanableBalance(BigDecimal loanableBalance) {
		this.loanableBalance = loanableBalance;
	}

	public BigDecimal getOpositionBalance() {
		return opositionBalance;
	}

	public void setOpositionBalance(BigDecimal opositionBalance) {
		this.opositionBalance = opositionBalance;
	}

	public BigDecimal getGrantedBalance() {
		return grantedBalance;
	}

	public void setGrantedBalance(BigDecimal grantedBalance) {
		this.grantedBalance = grantedBalance;
	}

	public BigDecimal getGrantBalance() {
		return grantBalance;
	}

	public void setGrantBalance(BigDecimal grantBalance) {
		this.grantBalance = grantBalance;
	}

	public BigDecimal getAmortization() {
		return amortization;
	}

	public void setAmortization(BigDecimal amortization) {
		this.amortization = amortization;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public CollectionBankFilter getBankFilter() {
		return bankFilter;
	}

	public void setBankFilter(CollectionBankFilter bankFilter) {
		this.bankFilter = bankFilter;
	}

	public boolean isCheckOther() {
		return checkOther;
	}

	public void setCheckOther(boolean checkOther) {
		this.checkOther = checkOther;
	}

	public boolean isCheckCharge() {
		return checkCharge;
	}

	public void setCheckCharge(boolean checkCharge) {
		this.checkCharge = checkCharge;
	}

	public CollectionAccountBankFilter getAccountFilter() {
		return accountFilter;
	}

	public void setAccountFilter(CollectionAccountBankFilter accountFilter) {
		this.accountFilter = accountFilter;
	}

	public NotificationFilter getNotificationFilter() {
		return notificationFilter;
	}

	public void setNotificationFilter(NotificationFilter notificationFilter) {
		this.notificationFilter = notificationFilter;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public String getTypeBalance() {
		return typeBalance;
	}

	public void setTypeBalance(String typeBalance) {
		this.typeBalance = typeBalance;
	}
	
}
