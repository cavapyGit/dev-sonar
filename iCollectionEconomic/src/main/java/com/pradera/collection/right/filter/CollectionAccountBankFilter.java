package com.pradera.collection.right.filter;

import java.util.ArrayList;
import java.util.List;

public class CollectionAccountBankFilter extends FilterSearch  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<String> accountList;
	
	private String accountSelected;
	
	public CollectionAccountBankFilter() {
		accountSelected = "";
		accountList = new ArrayList<String>();
	}

	

	public List<String> getAccountList() {
		return accountList;
	}



	public void setAccountList(List<String> accountList) {
		this.accountList = accountList;
	}



	public String getAccountSelected() {
		return accountSelected;
	}

	public void setAccountSelected(String accountSelected) {
		this.accountSelected = accountSelected;
	}
}
