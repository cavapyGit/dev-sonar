package com.pradera.collection.right.filter;

import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;

public class CollectionCurrencyFilter extends FilterSearch{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer currency;
	
	private List<ParameterTable> listCurrency;

	public CollectionCurrencyFilter() {
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}

	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}
}
