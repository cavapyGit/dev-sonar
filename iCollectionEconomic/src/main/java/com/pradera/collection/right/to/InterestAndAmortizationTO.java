package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class InterestAndAmortizationTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2575525133711141140L;
	
	private BigDecimal interest;;
	
	private BigDecimal amortization;

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getAmortization() {
		return amortization;
	}

	public void setAmortization(BigDecimal amortization) {
		this.amortization = amortization;
	}
	
	
}
