package com.pradera.collection.right.filter;


public class ParticipantFilter extends FilterSearch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idParticipantPk;
	
	public ParticipantFilter() {
		
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

}
