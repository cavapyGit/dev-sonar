package com.pradera.collection.right.to;

import java.io.Serializable;

import com.pradera.model.accounts.holderaccounts.Bank;

public class BankAccountMasiveTO implements  Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6999074623581520071L;

	private Bank bank;
	
	private Integer currency;
	
	private String currencyDesc;
	
	private String[] idBenBankAccountPks;
	
	private String idBenBankAccountPksString;

	public BankAccountMasiveTO() {
		// TODO Auto-generated constructor stub
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public String[] getIdBenBankAccountPks() {
		return idBenBankAccountPks;
	}

	public void setIdBenBankAccountPks(String[] idBenBankAccountPks) {
		this.idBenBankAccountPks = idBenBankAccountPks;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}

	public String getIdBenBankAccountPksString() {
		return idBenBankAccountPksString;
	}

	public void setIdBenBankAccountPksString(String idBenBankAccountPksString) {
		this.idBenBankAccountPksString = idBenBankAccountPksString;
	}
}
