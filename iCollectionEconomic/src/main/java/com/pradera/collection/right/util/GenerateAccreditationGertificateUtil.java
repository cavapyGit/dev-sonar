package com.pradera.collection.right.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;

import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.custody.type.DigitalSignatureType;
import com.pradera.model.process.BusinessProcess;
import com.pradera.notification.service.BuildReportGlosaServiceBean;
import com.pradera.notification.to.SecurityDetailObjectTO;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPKCS7;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfString;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class GenerateAccreditationGertificateUtil implements Serializable{

	/**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	
	 @Resource	
	 private TransactionSynchronizationRegistry transactionRegistry;
	 
	 @Inject
	 private BatchProcessServiceFacade batchProcessServiceFacade;
	 
	 @Inject
	 private CollectionRigthServiceBean collectionRigthServiceBean;
	 
	 private OutputStream opStream = null;
	 
	 @Inject
	 private PraderaLogger log;
	 
	 @Inject
	 Instance<ReportGenerationService> reportService;
	 
	 private  DecimalFormat formatCollection = new DecimalFormat("#,##0.00");
	 
	 public GenerateAccreditationGertificateUtil() {
	}
	
	@Lock(LockType.READ)
	public void registerBatch(Long idAccreditationOperationPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeExecute()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeExecute());
		}else{
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idAccreditationOperationPk",idAccreditationOperationPk);	
		params.put("idReportPk",ReportIdType.ACCREDITATION_CERTIFICATE.getCode());	//id del reporte de certificado de acreditacion
		params.put("userNameLogged", loggerUser.getUserName());
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_GENERATION.getCode());
		try {
			batchProcessServiceFacade.registerBatch(loggerUser.getUserName(),businessProcess,params);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	/**
	 * metodo para recupera desde la bbdd el certificado de acreditacion
	 * @param idAccreditationOperationPk
	 * @return
	 */
	public File getCertificateAccreditationFromBBDD(Long idAccreditationOperationPk){
		File file = null;
		try {
			file = getFileCertificateAccreditation(idAccreditationOperationPk);
		} catch (Exception e) {
			System.out.println("::: se ha producido un erro en el metodo getCertificateAccreditationFromBBDD(Long idAccreditationOperationPk) "+e.getMessage());
			return null;
		}
		return file;
	}
	/**
	 * Se genera un archivo temporal del certificado de acreditacion CAT
	 * @param idAccreditationOperationPk
	 * @return File
	 * */
	public File generateCertificateAccreditation(Long idAccreditationOperationPk){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.getUserAction().setGenerate(true);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeGenerate());
        File file = null;
		try {
			/*ID REPORT 6 -> REPORTE DE CERTIFICADO DE ACREDITACION CAT*/
			log.info("CDDE ::: EJECUTANDO REPORTE DE CERTIFICADO DE ACREDITACION ID :"+idAccreditationOperationPk);
			this.reportService.get().setAccreditationCertificate(new Long(6), idAccreditationOperationPk, loggerUser.getUserName(), loggerUser);
			Thread.sleep(10000);
			//Aproximandamete 10 minutos
	    	for (int i = 0;i<=300;i++) {
				if(file==null){
					log.info("CDDE :::  ARCHIVO NULO, ESPERANDO QUE SE GENERE EL CAT");
					file = getFileCertificateAccreditation(idAccreditationOperationPk);
				}				
				else{
					log.info("CDDE ::: NOMBRE CAT :"+file.getName());
					log.info("CDDE ::: UBICACION :"+file.getAbsolutePath());
					log.info("CDDE ::: SE RECUPERO CORRECTAMENTE EL CAT PARA EL COBRO DE DERECHOS ECONOMICOS::::::::::::");
					return file;
				}
				Thread.sleep(2000);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
		if(file==null)
			log.info("CDDE ::: NO SE PUDO RECUPERAR EL CAT, SE ENVIA SIN CAT EL EMAIL ");
		return file;
	}
	private File getFileCertificateAccreditation(Long idAccreditationCertificatePk){
		File temp = null;
		try {
			byte[] fileArray =  collectionRigthServiceBean.getAccreditationCertification(idAccreditationCertificatePk);
			if(fileArray==null)
				return temp;
			temp = File.createTempFile("CertificadoAcreditacion"+idAccreditationCertificatePk,".pdf");
			opStream = new FileOutputStream(temp);
			opStream.write(fileArray);
            opStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return temp; 
	}
	
	/** Firma digital en CAT
	 * 
	 * @param personSignature
	 * @param fileContent
	 * @param xOne
	 * @param yOne
	 * @param xTwo
	 * @param yTwo
	 * @param fieldName
	 * @param reason
	 * @param location
	 * @return */
	private byte[] setDigitallySign(Integer personSignature, byte[] fileContent, int xOne, int yOne, int xTwo, int yTwo,
			String fieldName, String reason, String location) {
		String passwordPKCS12 = null;
		try {
			KeyStore ks = KeyStore.getInstance("pkcs12");
			List<DigitalSignature> lstDigitalSignature = collectionRigthServiceBean
					.getListDigitalSignatureBlob(personSignature);
			byte[] digitalCertificateFirst = null;
			for (DigitalSignature digitalSignature : lstDigitalSignature) {
				digitalCertificateFirst = digitalSignature.getSignature();
				try {
					passwordPKCS12 = AESEncryptUtils.decrypt(AESKeyType.PRADERAKEY.getValue(),
							digitalSignature.getPasswordSignature());
				} catch (Exception e) {
					e.printStackTrace();
					log.error("CDE:::: No se pudo recuperar la firma, verifique la contrasena.");
				}
			}
			if (!Validations.validateIsNotNullAndNotEmpty(digitalCertificateFirst)) {
				log.error("CDE:::: No se puede encontrar firma digital.");
				return null;
			}

			ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(digitalCertificateFirst);
			ks.load(arrayInputStream, passwordPKCS12.toCharArray());
			String alias = (String) ks.aliases().nextElement();
			PrivateKey key = (PrivateKey) ks.getKey(alias, passwordPKCS12.toCharArray());
			Certificate[] chain = ks.getCertificateChain(alias);

			PdfReader pdfReader = new PdfReader(fileContent);
			ByteArrayOutputStream fout = new ByteArrayOutputStream();

			PdfStamper pdfStamper;
			pdfStamper = PdfStamper.createSignature(pdfReader, fout, '\0', null, true);
			PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
			InputStream inputIO = this.getClass().getResourceAsStream("/logo/logoEdv.PNG");
			byte[] bytes = IOUtils.toByteArray(inputIO);
			Image image = Image.getInstance(bytes);
			image.scaleToFit(40, 40);
			image.setAbsolutePosition(xOne - 50, yOne + 10);
			PdfContentByte contentByte = pdfStamper.getOverContent(pdfReader.getNumberOfPages());
			contentByte.addImage(image);
			//X509Certificate cert = (X509Certificate) ks.getCertificate(alias);
			/*Map<String, String> mapDetailCertificate = new HashMap<String, String>();
			String detailCertificate = cert.getSubjectX500Principal().toString();
			String list[] = detailCertificate.split(",");
			for (String value : list) {
				String v[] = value.split("=");
				if (v != null && v.length == 2)
					mapDetailCertificate.put(v[0].trim(), v[1].trim());
			}*/
			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.WINANSI, BaseFont.EMBEDDED);
			contentByte.saveState();
			contentByte.beginText();
			contentByte.setColorFill(new BaseColor(128, 128, 128));
			contentByte.setFontAndSize(bf, 5);
			String signName = PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("CN");
			String office = PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("T");
			String dateString = CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),CommonsUtilities.DATE_PATTERN);
			contentByte.showTextAligned(Element.ALIGN_LEFT, signName, xOne, yOne + 25, 0f);
			contentByte.showTextAligned(Element.ALIGN_LEFT, office, xOne, yOne + 20, 0f);
			contentByte.showTextAligned(Element.ALIGN_LEFT, dateString, xOne, yOne + 15, 0f);
			contentByte.showTextAligned(Element.ALIGN_LEFT, location, xOne, yOne + 10, 0f);
			contentByte.saveState();
			contentByte.endText();
			contentByte.restoreState();
			////////////////////INI
			sap.setSignDate(new GregorianCalendar());
			sap.setCrypto(null, chain, null, null);
			sap.setAcro6Layers(true);
			// sap.setRender(PdfSignatureAppearance.SignatureRenderNameAndDescription);
			PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
			dic.setDate(new PdfDate(sap.getSignDate()));
			dic.setName(PdfPKCS7.getSubjectFields((X509Certificate) chain[0]).getField("CN"));
			if (sap.getReason() != null) {
				dic.setReason(sap.getReason());
			}
			if (sap.getLocation() != null) {
				dic.setLocation(sap.getLocation());
			}
			sap.setCryptoDictionary(dic);
			int csize = 4000;
			HashMap exc = new HashMap();
			exc.put(PdfName.CONTENTS, new Integer(csize * 2 + 2));
			sap.preClose(exc);

			CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
			generator.addSigner(key, (X509Certificate) chain[0], CMSSignedDataGenerator.DIGEST_SHA256);

			ArrayList listCert = new ArrayList();
			for (int i = 0; i < chain.length; i++) {
				listCert.add(chain[i]);
			}
			CertStore chainStore;
			try {
				chainStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(listCert),
						"BC");
				generator.addCertificatesAndCRLs(chainStore);
				CMSProcessable content = new CMSProcessableRange(sap);
				CMSSignedData signedData = generator.generate(content, false, "BC");
				byte[] pk = signedData.getEncoded();

				byte[] outc = new byte[csize];

				PdfDictionary dic2 = new PdfDictionary();

				System.arraycopy(pk, 0, outc, 0, pk.length);
				dic2.put(PdfName.CONTENTS, new PdfString(outc).setHexWriting(true));
				sap.close(dic2);
			} catch (InvalidAlgorithmParameterException | NoSuchProviderException | CertStoreException | CMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			////////////////////FIN
			
			//sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
			//pdfStamper.close();
			byte[] data = fout.toByteArray();
			return data;
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (KeyStoreException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (ServiceException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (CertificateException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} catch (DocumentException e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * metodo para generar la glosa en pdf
	 * @param content
	 * @return
	 */
	public File generateGlosaPdf(Map<String, Object> parameters, List<SecurityDetailObjectTO> lstSecurityDetailObjectTO){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.getUserAction().setGenerate(true);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeGenerate());
        File file = null;
		try {
			//tiempo de espera 1 minuto para generar la glosa
	    	for (int i = 0;i<=60000;i++) {
				if(file==null){
					log.info("CDDE :::  ARCHIVO NULO, ESPERANDO QUE SE GENERE LA GLOSA");
					
					BuildReportGlosaServiceBean builReportBean=new BuildReportGlosaServiceBean();
					byte[] fileArray =  builReportBean.buildReportFromHtml(parameters,lstSecurityDetailObjectTO);
					
					try{
						int x = 30;
						int y = 40;
						fileArray=setDigitallySign(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode(), fileArray,x+78 ,y+44 ,x+208, y+85,"first",DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getValue(),"La Paz - Bolivia");
						fileArray=setDigitallySign(DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode(), fileArray,x+318, y+44, x+448, y+85,"second",DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getValue(),"La Paz - Bolivia");
						//signPdfDocuemrnt(fileArray);
					}catch(Exception e){
						e.printStackTrace();
					}
					file = getFileGlosaPdf(fileArray);
				}				
				else{
					log.info("CDDE ::: NOMBRE GLOSA :"+file.getName());
					log.info("CDDE ::: UBICACION :"+file.getAbsolutePath());
					log.info("CDDE ::: SE RECUPERO CORRECTAMENTE LA GLOSA PARA EL COBRO DE DERECHOS ECONOMICOS::::::::::::");
					return file;
				}
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
		if(file==null)
			log.info("CDDE ::: NO SE PUDO GENERAR GLOSA, SE ENVIA SIN CAT EL EMAIL ");
		return file;
	}
	
	/**
	 * metodo para generar la glosa en formato pdf
	 * @param content
	 * @return
	 */
	private File getFileGlosaPdf(byte[] fileArray){
		File temp = null;
		try {
			if(fileArray==null)
				return temp;
			temp = File.createTempFile("nota",".pdf");
			opStream = new FileOutputStream(temp);
			opStream.write(fileArray);
            opStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp; 
	}

	public DecimalFormat getFormatCollection() {
		return formatCollection;
	}
}
