package com.pradera.collection.right.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;

public class SearchCollectionRightTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idParticipantPk;
	
	private Integer typeIncome;
	
	private Long requestNumber;
	
	private Integer validated;
	
	private Integer state; 
	
	private Date initialDate;
	
	private Date finalDate;
	
	public SearchCollectionRightTO() {
		initialDate = CommonsUtilities.currentDate();
		finalDate =  CommonsUtilities.currentDate();
	}
	public SearchCollectionRightTO(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
		initialDate = CommonsUtilities.currentDate();
		finalDate =  CommonsUtilities.currentDate();
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Integer getTypeIncome() {
		return typeIncome;
	}

	public void setTypeIncome(Integer typeIncome) {
		this.typeIncome = typeIncome;
	}

	public Long getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Integer getValidated() {
		return validated;
	}

	public void setValidated(Integer validated) {
		this.validated = validated;
	}
	
}
