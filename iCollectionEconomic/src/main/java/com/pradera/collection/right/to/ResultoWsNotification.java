package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author rlarico
 *
 */
public class ResultoWsNotification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8658869377960036013L;

	private Long idPaymentAgent;
	
	private List<Long> idCollectionEconomicPks;
	
	private Date generationDate;
	
	//ESTOS ATRIBUTOS SON PARA LA URL DE ACUSE DE PAGO
	private Long idParticiapntPk;
	private Date dateOperaion;
	private Integer currency;
	private BigDecimal amountPayment;
	private String glossCode;
	private Long IdBank;
	
	//ATRIBUTOS PARA LA REDACCION DE ACUSE DE PAGO
	private String descPaymentAgentJuridical;
	private String accountBankNumber;
	private String descCuiHolder;
	
	//SE UTILIZA CUANDO SE EJECUTA EL BATCHERO 40837
	private String paramUrlReceived;
	
	
	//class constructor
	public ResultoWsNotification() {
	}

	public Long getIdPaymentAgent() {
		return idPaymentAgent;
	}

	public void setIdPaymentAgent(Long idPaymentAgent) {
		this.idPaymentAgent = idPaymentAgent;
	}

	public Long getIdParticiapntPk() {
		return idParticiapntPk;
	}

	public String getDescPaymentAgentJuridical() {
		return descPaymentAgentJuridical;
	}

	public void setDescPaymentAgentJuridical(String descPaymentAgentJuridical) {
		this.descPaymentAgentJuridical = descPaymentAgentJuridical;
	}

	public String getParamUrlReceived() {
		return paramUrlReceived;
	}

	public void setParamUrlReceived(String paramUrlReceived) {
		this.paramUrlReceived = paramUrlReceived;
	}

	public String getAccountBankNumber() {
		return accountBankNumber;
	}

	public void setAccountBankNumber(String accountBankNumber) {
		this.accountBankNumber = accountBankNumber;
	}

	public String getDescCuiHolder() {
		return descCuiHolder;
	}

	public void setDescCuiHolder(String descCuiHolder) {
		this.descCuiHolder = descCuiHolder;
	}

	public void setIdParticiapntPk(Long idParticiapntPk) {
		this.idParticiapntPk = idParticiapntPk;
	}

	public Date getDateOperaion() {
		return dateOperaion;
	}

	public void setDateOperaion(Date dateOperaion) {
		this.dateOperaion = dateOperaion;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public BigDecimal getAmountPayment() {
		return amountPayment;
	}

	public void setAmountPayment(BigDecimal amountPayment) {
		this.amountPayment = amountPayment;
	}

	public String getGlossCode() {
		return glossCode;
	}

	public void setGlossCode(String glossCode) {
		this.glossCode = glossCode;
	}

	public Long getIdBank() {
		return IdBank;
	}

	public void setIdBank(Long idBank) {
		IdBank = idBank;
	}

	public List<Long> getIdCollectionEconomicPks() {
		return idCollectionEconomicPks;
	}

	public void setIdCollectionEconomicPks(List<Long> idCollectionEconomicPks) {
		this.idCollectionEconomicPks = idCollectionEconomicPks;
	}

	public Date getGenerationDate() {
		return generationDate;
	}

	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}

	@Override
	public String toString() {
		return "ResultoWsNotification [idPaymentAgent=" + idPaymentAgent + ", generationDate=" + generationDate + ", idParticiapntPk=" + idParticiapntPk
				+ ", dateOperaion=" + dateOperaion + ", currency=" + currency + ", amountPayment=" + amountPayment + ", glossCode=" + glossCode + ", IdBank=" + IdBank
				+ ", descPaymentAgentJuridical=" + descPaymentAgentJuridical + ", accountBankNumber=" + accountBankNumber + ", descCuiHolder=" + descCuiHolder + "]";
	}
}
