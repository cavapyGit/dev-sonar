package com.pradera.collection.right.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pradera.notification.to.SecuritiesAlmostToExpireTO;

public class SecuritiesAlmostObjectTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1416221191799053033L;

	private List<SecuritiesAlmostToExpireTO> almostToExpireTOs;
	
	private List<String> listSecurities;
	
	public SecuritiesAlmostObjectTO() {
		almostToExpireTOs = new ArrayList<>();
		listSecurities = new ArrayList<String>();
	}

	public List<SecuritiesAlmostToExpireTO> getAlmostToExpireTOs() {
		return almostToExpireTOs;
	}

	public void setAlmostToExpireTOs(
			List<SecuritiesAlmostToExpireTO> almostToExpireTOs) {
		this.almostToExpireTOs = almostToExpireTOs;
	}

	public List<String> getListSecurities() {
		return listSecurities;
	}

	public void setListSecurities(List<String> listSecurities) {
		this.listSecurities = listSecurities;
	}
}
