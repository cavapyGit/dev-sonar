package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.edv.collection.economic.rigth.GlossNoticationDetail;

public class DetailCollectionGlossTO implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1527989626214666038L;
	private Long idParticipantPk;
	private String htmlMessage;
	private String glosaNotClassified;
	private BigDecimal amountReceivable;
	private Date paymentDate;
	private Integer currency;
	private Integer securityClass;
	private Integer transferStatus;
	private List<Long> listIdCertificateAccreditations;
	private String arrayStringIdCatSender;
	private String idIssuerPk;
	private Long idHolderPk;
	/*Id del agente pagador*/
	private Long idPaymentAgentFk;
	
	private List<GlossNoticationDetail> lstGlossNoticationDetail;
	
	public DetailCollectionGlossTO() {
		// TODO Auto-generated constructor stub
		lstGlossNoticationDetail=new ArrayList<>();
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public String getArrayStringIdCatSender() {
		return arrayStringIdCatSender;
	}

	public void setArrayStringIdCatSender(String arrayStringIdCatSender) {
		this.arrayStringIdCatSender = arrayStringIdCatSender;
	}

	public String getIdIssuerPk() {
		return idIssuerPk;
	}

	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}

	public List<GlossNoticationDetail> getLstGlossNoticationDetail() {
		return lstGlossNoticationDetail;
	}

	public void setLstGlossNoticationDetail(List<GlossNoticationDetail> lstGlossNoticationDetail) {
		this.lstGlossNoticationDetail = lstGlossNoticationDetail;
	}

	public String getHtmlMessage() {
		return htmlMessage;
	}

	public void setHtmlMessage(String htmlMessage) {
		this.htmlMessage = htmlMessage;
	}

	public String getGlosaNotClassified() {
		return glosaNotClassified;
	}

	public void setGlosaNotClassified(String glosaNotClassified) {
		this.glosaNotClassified = glosaNotClassified;
	}

	public BigDecimal getAmountReceivable() {
		return amountReceivable;
	}

	public void setAmountReceivable(BigDecimal amountReceivable) {
		this.amountReceivable = amountReceivable;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Integer getTransferStatus() {
		return transferStatus;
	}

	public void setTransferStatus(Integer transferStatus) {
		this.transferStatus = transferStatus;
	}

	public List<Long> getListIdCertificateAccreditations() {
		return listIdCertificateAccreditations;
	}

	public void setListIdCertificateAccreditations(
			List<Long> listIdCertificateAccreditations) {
		this.listIdCertificateAccreditations = listIdCertificateAccreditations;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Long getIdPaymentAgentFk() {
		return idPaymentAgentFk;
	}

	public void setIdPaymentAgentFk(Long idPaymentAgentFk) {
		this.idPaymentAgentFk = idPaymentAgentFk;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
}
