package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

public class RegisterCollectionRightTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idReqModfCollectionPk;

	private Long idParticipantPk;
	
	private Holder holder;
	
	private Security security;
	
	private Integer typeIncome;
	
	private Issuer issuer;
	
	private Integer currency;
	
	private Integer accountNumber;
	
	private Integer securityClass;
	
	private Date initialDate;
	
	private Date minDate;
	
	private Date finalDate;
	
	private Date maxDate;
	
	private Date paymentDate;
	
	private boolean renderedRejected;
	
	private boolean renderedConfirmModify;
	
	private boolean renderedConfirmRegistre;
	
	private boolean renderdModify, renderedRV;
	
	private BigDecimal paymentAmount;
	
	private HolderAccountHelperResultTO selectedAccountTO;

	public RegisterCollectionRightTO() {
		initialDate = CommonsUtilities.currentDate();
		finalDate =  CommonsUtilities.currentDate();
		maxDate = CommonsUtilities.currentDate();
		security = new Security();
		issuer = new Issuer();
		initialDate = new Date();
		finalDate = new Date();
		this.renderedRejected = false;
		this.renderedConfirmModify = false;
		this.renderedConfirmModify =  false;
		this.renderdModify = false;
		holder = new Holder();
	}
	public RegisterCollectionRightTO(Long idParticipantPk,Integer typeIncome) {
		this.idParticipantPk = idParticipantPk;
		this.typeIncome = typeIncome;
		this.issuer = new Issuer();
		this.security = new Security();
		this.initialDate = new Date();
		this.finalDate = new Date();
		this.renderedRejected = false;
		this.renderedConfirmModify = false;
		this.renderedConfirmRegistre = false;
		this.renderdModify = false;
		this.holder = new Holder();
	}
	public RegisterCollectionRightTO(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
		this.typeIncome = typeIncome;
		this.issuer = new Issuer();
		this.security = new Security();
		this.initialDate = new Date();
		this.finalDate = new Date();
		this.renderedRejected = false;
		this.renderedConfirmModify = false;
		this.renderedConfirmRegistre = false;
		this.renderdModify = false;
		this.holder = new Holder();
	}
	public RegisterCollectionRightTO(Integer typeIncome) {
		this.typeIncome = typeIncome;
		this.issuer = new Issuer();
		this.security = new Security();
		this.initialDate = new Date();
		this.finalDate = new Date();
		this.holder = new Holder();
	}
	public Long getIdReqModfCollectionPk() {
		return idReqModfCollectionPk;
	}
	public void setIdReqModfCollectionPk(Long idReqModfCollectionPk) {
		this.idReqModfCollectionPk = idReqModfCollectionPk;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public Integer getTypeIncome() {
		return typeIncome;
	}
	public void setTypeIncome(Integer typeIncome) {
		this.typeIncome = typeIncome;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getMinDate() {
		return minDate;
	}
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public Date getMaxDate() {
		return maxDate;
	}
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public boolean isRenderedRejected() {
		return renderedRejected;
	}
	public void setRenderedRejected(boolean renderedRejected) {
		this.renderedRejected = renderedRejected;
	}
	public boolean isRenderedConfirmModify() {
		return renderedConfirmModify;
	}
	public void setRenderedConfirmModify(boolean renderedConfirmModify) {
		this.renderedConfirmModify = renderedConfirmModify;
	}
	public boolean isRenderedConfirmRegistre() {
		return renderedConfirmRegistre;
	}
	public void setRenderedConfirmRegistre(boolean renderedConfirmRegistre) {
		this.renderedConfirmRegistre = renderedConfirmRegistre;
	}
	public boolean isRenderdModify() {
		return renderdModify;
	}
	public void setRenderdModify(boolean renderdModify) {
		this.renderdModify = renderdModify;
	}
	public boolean isRenderedRV() {
		return renderedRV;
	}
	public void setRenderedRV(boolean renderedRV) {
		this.renderedRV = renderedRV;
	}
	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}
	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}
	
}
