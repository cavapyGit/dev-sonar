package com.pradera.collection.right.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.collection.right.to.DetailCollectionGlossTO;
import com.pradera.collection.right.to.InterestAndAmortizationTO;
import com.pradera.collection.right.to.RegisterCollectionRightTO;
import com.pradera.collection.right.to.SearchCollectionRightTO;
import com.pradera.collection.right.to.SearchRegisterResultTO;
import com.pradera.collection.right.util.SituationConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.notification.to.SecuritiesAlmostToExpireTO;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.FundsCollection;
import com.edv.collection.economic.rigth.GlossNoticationDetail;
import com.edv.collection.economic.rigth.ModfCollEcoRigth;
import com.edv.collection.economic.rigth.PaymentAgent;
import com.edv.collection.economic.rigth.ReqCollectionEconomicRight;
import com.edv.collection.economic.rigth.ReqModfCollEcoRight;

@Stateless
@Performance
public class CollectionRigthServiceBean extends CrudDaoServiceBean implements Serializable {

	/**
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private UserInfo userInfo;

	@Inject
	private PraderaLogger log;

	@Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;

	private static final String CUIS = "451,452,453,454,455,456,460";

	public CollectionRigthServiceBean() {

	}

	/** Obtiene la lista de clase de valor de acuerdo al tipo de instrumento */
	public List<ParameterTable> getSecurityClassList(Integer instrumentType) throws Exception {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" Select distinct pt from ParameterTable pt, SecurityClassSetup scs ");
		// queryBuilder.append(" inner join SecurityClassSetup scs ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and pt.parameterTablePk = scs.securityClass ");
		queryBuilder.append(" and scs.instrumentType = :instrumentType ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("instrumentType", instrumentType);
		@SuppressWarnings("unchecked")
		List<ParameterTable> lstResult = query.getResultList();
		return lstResult;
	}

	/**
	 * Buscamos todos lo registros de cobro de derechos economicos segun la
	 * solicitud
	 * 
	 * @param Long idReqCollectionEconomic
	 * @return List<CollectionEconomicRigth>
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<CollectionEconomicRigth> getListCollectionEconomicsRegistered(Long idReqCollectionEconomicFk,
			Integer state) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and cer.idReqCollectionEconomicFk = :idReqCollectionEconomicFk ");
		queryBuilder.append(" and cer.state = :state ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idReqCollectionEconomicFk", idReqCollectionEconomicFk);
		query.setParameter("state", state);
		List<CollectionEconomicRigth> list = query.getResultList();
		return list;
	}

	/**
	 * Buscamos todos lo registros de cobro de derechos economicos segun la
	 * solicitud
	 * 
	 * @param Long idReqCollectionEconomic
	 * @return List<CollectionEconomicRigth>
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<CollectionEconomicRigth> getListCollectionEconomics(Date paymentDate, Date nextBusinessDay) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and cer.state = " + StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
		queryBuilder.append(" and trunc(cer.paymentDate) between :paymentDate and :nextBusinessDay");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);
		return query.getResultList();
	}

	/**
	 * Buscamos todos lo registros de cobro de derechos economicos segun la
	 * solicitud
	 * 
	 * @param Long idReqCollectionEconomic
	 * @return List<CollectionEconomicRigth>
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<ModfCollEcoRigth> getListCollectionEconomicsModifieds(Long idReqModfCollEcoFk) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select mcer from ModfCollEcoRigth mcer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and mcer.idReqModfCollEcoFk = :idReqModfCollEcoFk ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idReqModfCollEcoFk", idReqModfCollEcoFk);
		return query.getResultList();
	}

	/** Buscamos la ultima solicitud de modificacion registrada **/
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ReqModfCollEcoRight getLastRequestModification(Long idReqColleEconomicRightFk) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select max(rmcer) from ReqModfCollEcoRight rmcer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and rmcer.idReqColleEconomicRightFk = :idReqColleEconomicRightFk ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idReqColleEconomicRightFk", idReqColleEconomicRightFk);
		return (ReqModfCollEcoRight) query.getSingleResult();
	}

	/** Actualizamos los datos de modificacion en la solicitud */
	@SuppressWarnings("unchecked")
	public void replaceCollectionEconomics(Long idReqColleEconomicRightFk, Long idReqModfCollectionEconomicPk) {
		/* Actualizar los estados de registrado a modificado */
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and cer.idReqCollectionEconomicFk = :idReqColleEconomicRightFk ");
		queryBuilder.append(" and cer.state = " + StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idReqColleEconomicRightFk", idReqColleEconomicRightFk);
		List<CollectionEconomicRigth> list = query.getResultList();
		try {
			// Actualizamos los registros(ESTADO = Modificado)
			for (CollectionEconomicRigth collectionEconomicRigth : list)
				collectionEconomicRigth.setState(StatesConstants.MODIFIED_COLLECTION_ECONOMIC);
			updateList(list);
			/* Buscando las modificaciones */
			queryBuilder = new StringBuilder();
			queryBuilder.append(" select mcer from ModfCollEcoRigth mcer ");
			queryBuilder.append(" where 1 = 1 ");
			queryBuilder.append(" and mcer.idReqModfCollEcoFk = :idReqModfCollectionEconomicPk ");
			query = em.createQuery(queryBuilder.toString());
			query.setParameter("idReqModfCollectionEconomicPk", idReqModfCollectionEconomicPk);
			List<ModfCollEcoRigth> modfCollEcoRigths = query.getResultList();
			CollectionEconomicRigth collectionEconomicRigth;
			for (ModfCollEcoRigth modfCollEcoRigth : modfCollEcoRigths) {
				collectionEconomicRigth = new CollectionEconomicRigth();
				collectionEconomicRigth.setState(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
				collectionEconomicRigth.setIdReqCollectionEconomicFk(idReqColleEconomicRightFk);
				// fecha de vencimiento
				collectionEconomicRigth.setDueDate(modfCollEcoRigth.getDueDate());
				// Fecha de cobro
				collectionEconomicRigth.setPaymentDate(modfCollEcoRigth.getPaymentDate());
				collectionEconomicRigth.setAlternateCode(modfCollEcoRigth.getAlternateCode());
				collectionEconomicRigth.setIdSecurityCodeFk(modfCollEcoRigth.getIdSecurityCodeFk());
				collectionEconomicRigth.setCurrency(modfCollEcoRigth.getCurrency());
				collectionEconomicRigth.setIdGeneralPurposePk(modfCollEcoRigth.getIdGeneralPurposePk());
				collectionEconomicRigth.setIdHolderAccountPk(modfCollEcoRigth.getIdHolderAccountPk());
				collectionEconomicRigth.setAccountNumber(modfCollEcoRigth.getAccountNumber());
				collectionEconomicRigth.setCouponNumber(modfCollEcoRigth.getCouponNumber());
				// cantidad de valores
				collectionEconomicRigth.setAmountReceivable(modfCollEcoRigth.getAmountSecurities());
				// interes
				collectionEconomicRigth.setInterest(modfCollEcoRigth.getInterest());
				// amortizacion
				collectionEconomicRigth.setAmortization(modfCollEcoRigth.getAmortization());
				// monto total
				collectionEconomicRigth.setAmountReceivable(modfCollEcoRigth.getAmountReceivable());
				// check
				collectionEconomicRigth.setCheckOther(modfCollEcoRigth.getCheckOther());
				// Otra moneda de cobro
				collectionEconomicRigth.setAnotherCurency(modfCollEcoRigth.getAnotherCurency());
				// monto a cobrar
				collectionEconomicRigth.setAnotherAmountReceivable(modfCollEcoRigth.getAnotherAmountReceivable());
				collectionEconomicRigth.setIdBankFk(modfCollEcoRigth.getIdBankFk());
				collectionEconomicRigth.setDepAccountNumber(modfCollEcoRigth.getDepAccountNumber());
				collectionEconomicRigth.setAmountSecurities(modfCollEcoRigth.getAmountSecurities());
				collectionEconomicRigth.setLastModifyApp(userInfo.getLastModifyPriv());
				collectionEconomicRigth.setRegistreDate(CommonsUtilities.currentDate());
				collectionEconomicRigth.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
				collectionEconomicRigth.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
				collectionEconomicRigth.setLastModifyDate(CommonsUtilities.currentDate());
				saveCollectionEconomicRigth(collectionEconomicRigth);
			}
			ReqCollectionEconomicRight reqCollectionEconomicRight = find(ReqCollectionEconomicRight.class,
					idReqColleEconomicRightFk);
			reqCollectionEconomicRight.setState(StatesConstants.REGISTERED_REQUEST_COLLECTION_ECONOMIC);
			update(reqCollectionEconomicRight);
			ReqModfCollEcoRight modfCollEcoRight = find(ReqModfCollEcoRight.class, idReqModfCollectionEconomicPk);
			modfCollEcoRight.setState(StatesConstants.CONFIRMED_MODIFY_REQUEST_COLLECTION_ECONOMIC);
			update(modfCollEcoRight);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Guardamos la solicitud de cobro de derechos economico
	 * 
	 * @return ReqCollectionEconomicRight
	 */
	public ReqCollectionEconomicRight saveRequestCollection(ReqCollectionEconomicRight collectionEconomicRight) {
		em.persist(collectionEconomicRight);
		em.flush();
		return collectionEconomicRight;
	}

	/** obteneiendo un CollectionEconomic en concreto */
	public CollectionEconomicRigth getOneCollectionEconomicRigth(Long idCollectionEconomicPk) {
		CollectionEconomicRigth cer = em.find(CollectionEconomicRigth.class, idCollectionEconomicPk);
		return cer;
	}

	/**
	 * Guardamos la solicitud de modificacion cobro de derechos economico
	 * 
	 * @return ReqModfCollEconomicRight
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public ReqModfCollEcoRight saveModfRequestCollection(ReqModfCollEcoRight modfCollEconomicRight) {
		em.persist(modfCollEconomicRight);
		em.flush();
		return modfCollEconomicRight;
	}

	/** Obtenemos la lista de solicitudes de cobro de derecho economico */
	public List<ReqCollectionEconomicRight> getListRegCollectionEconomic(SearchCollectionRightTO searchFilterTO) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select rce from ReqCollectionEconomicRight rce ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and rce.participant.idParticipantPk = :idParticipantPk ");

		if (searchFilterTO.getTypeIncome() != null)
			queryBuilder.append(" and rce.instrumentType = :instrumentType ");

		if (searchFilterTO.getRequestNumber() != null)
			queryBuilder.append(" and rce.idReqCollectionEconomicPk = :idReqCollectionEconomicPk ");

		if (searchFilterTO.getValidated() != null)
			queryBuilder.append(" and rce.submit = :submit");

		if (searchFilterTO.getState() != null)
			queryBuilder.append(" and rce.state = :state ");

		queryBuilder.append(" and trunc(rce.processDate) between :initialDate and :finalDate ");
		queryBuilder.append(" order by rce.idReqCollectionEconomicPk desc ");

		Query query = em.createQuery(queryBuilder.toString());

		query.setParameter("idParticipantPk", searchFilterTO.getIdParticipantPk());

		if (searchFilterTO.getTypeIncome() != null)
			query.setParameter("instrumentType", searchFilterTO.getTypeIncome());

		if (searchFilterTO.getRequestNumber() != null)
			query.setParameter("idReqCollectionEconomicPk", searchFilterTO.getRequestNumber());

		if (searchFilterTO.getValidated() != null)
			query.setParameter("submit", searchFilterTO.getValidated());

		if (searchFilterTO.getState() != null)
			query.setParameter("state", searchFilterTO.getState());

		query.setParameter("initialDate", searchFilterTO.getInitialDate());

		query.setParameter("finalDate", searchFilterTO.getFinalDate());

		@SuppressWarnings("unchecked")
		List<ReqCollectionEconomicRight> collectionEconomicRights = query.getResultList();
		return collectionEconomicRights;
	}

	public List<ParameterTable> getSecurityClassListAll() {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select parameter_table_pk,text1,description from Parameter_Table ,Security_Class_Setup ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and Parameter_Table.Parameter_Table_Pk = Security_Class_Setup.Security_Class ");
		queryBuilder.append(" and Parameter_Table.Master_Table_Fk = " + MasterTableType.SECURITIES_CLASS.getCode());
		queryBuilder.append(" and Security_Class_Setup.Instrument_Type in  (");
		queryBuilder.append(InstrumentType.MIXED_INCOME.getCode().toString() + ",");
		queryBuilder.append(InstrumentType.FIXED_INCOME.getCode().toString() + ",");
		queryBuilder.append(InstrumentType.VARIABLE_INCOME.getCode().toString() + ")");
		Query query = em.createNativeQuery(queryBuilder.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.getResultList();
		List<ParameterTable> listParameterTables = new ArrayList<>();
		ParameterTable parameterTable;
		for (Object[] objectParameter : list) {
			parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk(Integer.parseInt(objectParameter[0].toString()));
			parameterTable.setText1(objectParameter[1].toString());
			parameterTable.setDescription(objectParameter[2].toString());
			listParameterTables.add(parameterTable);
		}
		return listParameterTables;
	}

	/** Buscamos la lista de monedas excluyendo la moneda como parametro */
	public List<ParameterTable> getCurrencyList(Integer currency) throws Exception {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select parameter_table_pk,text1,description from parameter_table ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and master_table_fk = " + MasterTableType.CURRENCY.getCode());
		queryBuilder.append(" and parameter_table_pk  not in (" + currency + ",1304,1734,1853)");

		Query query = em.createNativeQuery(queryBuilder.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.getResultList();
		if (list == null)
			return null;
		List<ParameterTable> listParameterTables = new ArrayList<>();
		ParameterTable parameterTable;
		parameterTable = find(ParameterTable.class, currency);
		listParameterTables.add(0, parameterTable);
		int i = 1;
		for (Object[] objectParameter : list) {
			parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk(Integer.parseInt(objectParameter[0].toString()));
			parameterTable.setText1(objectParameter[1].toString());
			parameterTable.setDescription(objectParameter[2].toString());
			listParameterTables.add(i, parameterTable);
			i++;
		}
		return listParameterTables;
	}

	/** Buscamos la lista de monedas excluyendo la moneda como parametro */
	public List<ParameterTable> getCurrencyListBobUsd(Integer currency) throws Exception {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select parameter_table_pk,text1,description from parameter_table ");
		queryBuilder.append(" where 1 = 1 ");
		// queryBuilder.append(" and master_table_fk = 53 ");
		queryBuilder.append(
				" and parameter_table_pk  in (" + CurrencyType.PYG.getCode() + "," + CurrencyType.PYG.getCode() + ")");

		Query query = em.createNativeQuery(queryBuilder.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.getResultList();
		if (list == null)
			return null;
		List<ParameterTable> listParameterTables = new ArrayList<>();
		int i = 0;
		ParameterTable parameterTable;
		for (Object[] objectParameter : list) {
			parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk(Integer.parseInt(objectParameter[0].toString()));
			parameterTable.setText1(objectParameter[1].toString());
			parameterTable.setDescription(objectParameter[2].toString());
			listParameterTables.add(i, parameterTable);
			i++;
		}
		return listParameterTables;
	}

	/**
	 * Busqueda de Bancos segun participante y moneda
	 * 
	 * @param idParticipantPk,currency
	 * @return List<Bank>
	 */
	public List<Bank> getlistBankProviderByParticipantAndHolder(Long idParticipantPk, Integer accountNumber,
			Integer currency) {
		List<Long> beneficiaryBankAccounts = getLisBeneficiaryBankAccounts(idParticipantPk,	Long.parseLong(accountNumber.toString()), currency);
		List<Bank> banks = new ArrayList<>();
		Bank bank;
		for (Long long1 : beneficiaryBankAccounts) {
			bank = em.find(Bank.class, long1);
			banks.add(bank);
		}
		return banks;
	}

	/** Obtenemos la lista de titulares a partir del codigo alterno */
	public Long extractHolder(String alternativeCode) {
		if (alternativeCode == null)
			return null;
		int ini = alternativeCode.indexOf("(");
		int fin = alternativeCode.indexOf(")");
		String string = alternativeCode.substring(ini + 1, fin);
		String holders[] = string.split(",");
		List<Long> longs = new ArrayList<Long>();
		for (int i = 0; i < holders.length; i++)
			longs.add(Long.parseLong(holders[i]));

		List<String> list = Arrays.asList(CUIS.split("\\s*,\\s*"));

		for (String cuiString : list)
			for (Long cuiLong : longs)
				if (cuiString.equals(cuiLong.toString()))
					return Long.parseLong(cuiString);
		return null;
	}

	/**
	 * Se obtiene los contactos agente pagaador En caso de tratarse de un DPF
	 * verificar si tiene agente pagador, si no tubiera este, tomar en cuenta el
	 * mismo emisor del valor el agente pagador
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public HashMap<Long, PaymentAgent> getListContactAgent(SecuritiesAlmostToExpireTO toExpireTO) {
		/** Si la clase de valor fuera DPF, el mismo emisor es el agente pagador */
		String mNemonicPaymentAgent = null;
		if (toExpireTO.getSecurityClass().equals(SecurityClassType.DPF.getCode())) {
			Security security = em.find(Security.class, toExpireTO.getCollectionEconomicRigth().getIdSecurityCodeFk());
			mNemonicPaymentAgent = security.getPaymentAgent();
			/** Verificar si tiene agente pagador, validar*/
			if (!Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent)) {
				Issuer issuer = em.find(Issuer.class, toExpireTO.getIdIssuerPk());
				mNemonicPaymentAgent = issuer.getMnemonic();
			}
		} else {
			Security security = em.find(Security.class, toExpireTO.getCollectionEconomicRigth().getIdSecurityCodeFk());//aqui solo buscar el 
			mNemonicPaymentAgent = security.getPaymentAgent();
		}
		if (mNemonicPaymentAgent == null) {
			log.error("CDDE :::   No existe nemonico de agente pagador para el valor : "
					+ toExpireTO.getCollectionEconomicRigth().getIdSecurityCodeFk());
			return null;
		}
		// Buscamos el agente pagador

		/** Buscamo el agente pagador con ese nemonico */
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select new com.edv.collection.economic.rigth.PaymentAgent(pa.idPaymentAgentPk) ");
		queryBuilder.append(" from PaymentAgent pa ");
		queryBuilder.append(" where  1 = 1 ");
		queryBuilder.append(" and pa.idPaymentAgentFk is null ");
		queryBuilder.append(" and pa.mnemonic = :mNemonicPaymentAgent");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("mNemonicPaymentAgent", mNemonicPaymentAgent);

		PaymentAgent paymentAgent = null;
		try {
			paymentAgent = (PaymentAgent) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}

		queryBuilder = new StringBuilder();
		queryBuilder.append(" select pa from PaymentAgent pa ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and pa.securityClassList like '%" + toExpireTO.getSecurityClass() + "%' ");
		queryBuilder.append(" and pa.idPaymentAgentFk in (:idPaymentAgentFk) ");
		queryBuilder.append(" and pa.state = :state ");// + StatesConstants.CONFIRMED_PAYER_AGENT);
		queryBuilder.append(" and pa.situation = :situation ");
		queryBuilder.append(" order by pa.idPaymentAgentPk asc ");
		query = em.createQuery(queryBuilder.toString());
		query.setParameter("idPaymentAgentFk", paymentAgent.getIdPaymentAgentPk());
		query.setParameter("state", StatesConstants.CONFIRMED_PAYER_AGENT);
		query.setParameter("situation", SituationConstants.ACTIVE_AGENT_CONTACT_PAYMENT);
		List<PaymentAgent> list = query.getResultList();
		HashMap<Long, PaymentAgent> hasfFinal = new HashMap<>();
		if (list != null && list.size() > 0) {
			for (PaymentAgent paymentAgentContact : list)
				hasfFinal.put(paymentAgentContact.getIdPaymentAgentPk(), paymentAgentContact);
			return hasfFinal;
		} else
			return null;
	}//aqui no hay respuesta*

	/**
	 * Busqueda de Bancos segun participante y moneda
	 * 
	 * @param idParticipantPk id del participante con la cual se realiza las
	 *                        busquedas
	 * @param accountNumber   nuemero de cuenta del titular
	 * @param currency        Moneda de cobro para la cuenta bancaria
	 * @return Lista de todos los id de los banco
	 */
	@SuppressWarnings("unchecked")
	private List<Long> getLisBeneficiaryBankAccounts(Long idParticipantPk, Long accountNumber, Integer currency) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select distinct (bba.idBankFk) from BeneficiaryBankAccount bba ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and bba.idParticipantFk = :idParticipantFk");
		queryBuilder.append(" and bba.currency = :currency ");
		queryBuilder.append(" and bba.state = " + StatesConstants.CONFIRMED_BANK_ACCOUNT);// Confirmado
		queryBuilder.append(" and bba.accountNumber  = :accountNumber ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idParticipantFk", idParticipantPk);
		query.setParameter("accountNumber", accountNumber.intValue());
		query.setParameter("currency", currency);
		List<Long> list = null;
		try {
			list = query.getResultList();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Busqueda de nuemeros de cuenta Segun participante, banco y moneda
	 * 
	 * @param idParticipantPk id del participante con la que se realiza las
	 *                        busquedas
	 * @param accountNumber   numero de cuenta titular (Cui-AccountNumber)
	 * @return Lista de todas las cuentas asigandas al participante, numero de
	 *         cuenta,banco y moneda
	 */
	@SuppressWarnings("unchecked")
	public List<String> getAccountNumberList(Long idParticipantPk, Integer accountNumber, Long idBankPk,
			Integer currency) {
		List<String> accountList = new ArrayList<>();
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select distinct (bba.depAccountNumber) from BeneficiaryBankAccount bba ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and bba.idParticipantFk = :idParticipantFk");
		queryBuilder.append(" and bba.idBankFk = :idBankFk ");
		queryBuilder.append(" and bba.currency = :currency ");
		queryBuilder.append(" and bba.accountNumber  = :accountNumber  ");
		queryBuilder.append(" and bba.state = " + StatesConstants.CONFIRMED_BANK_ACCOUNT);

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idParticipantFk", idParticipantPk);
		query.setParameter("currency", currency);
		query.setParameter("accountNumber", Integer.parseInt(accountNumber.toString()));
		query.setParameter("idBankFk", idBankPk);
		accountList = query.getResultList();
		return accountList;
	}

	/**
	 * Buscamos todos los intereses Primero buscamos los intereses luego los cupones
	 * RF, CON SALDO DISPONIBLE
	 * 
	 * @param sumInterest
	 * @param registerFilterTO
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SearchRegisterResultTO> getListSecuritiesToPay(RegisterCollectionRightTO registerFilterTO) {
		if (registerFilterTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode())) {
			StringBuilder builder = new StringBuilder();
			builder.append(" SELECT HAB.ID_PARTICIPANT_PK,HAB.ID_HOLDER_ACCOUNT_PK,HAB.ID_SECURITY_CODE_PK,HAB.AVAILABLE_BALANCE ");
			builder.append(" ,SE.CURRENCY,HAD.ID_HOLDER_FK, SE.ID_ISSUER_FK,SE.INITIAL_NOMINAL_VALUE,HA.ALTERNATE_CODE ");
			builder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
			builder.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAB.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
			builder.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
			builder.append(" INNER JOIN SECURITY SE ON HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
			builder.append(" INNER JOIN ISSUER ISS ON SE.ID_ISSUER_FK = ISS.ID_ISSUER_PK ");
			
			builder.append(" WHERE 1 = 1 ");
			builder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
			builder.append(" AND SE.INSTRUMENT_TYPE = "+InstrumentType.VARIABLE_INCOME.getCode());
			if(registerFilterTO.getIdParticipantPk()!=null)
				builder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
			
			if(registerFilterTO.getHolder().getIdHolderPk()!=null)
				builder.append(" AND HAD.ID_HOLDER_FK = :idHolderPk ");
			
			if(registerFilterTO.getSecurity().getIdSecurityCodePk()!=null)
				builder.append("AND SE.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
			
			if(registerFilterTO.getCurrency()!=null)
				builder.append(" AND SE.CURRENCY = :currency ");	
			
			if(registerFilterTO.getSecurityClass()!=null)
				builder.append(" AND SE.SECURITY_CLASS = :securityClass ");	
			
			if(registerFilterTO.getIssuer().getIdIssuerPk()!=null)
				builder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk ");
			
			Query query = em.createNativeQuery(builder.toString());	
			
			if(registerFilterTO.getIdParticipantPk()!=null)
				query.setParameter("idParticipantPk", registerFilterTO.getIdParticipantPk());
			
			if(registerFilterTO.getHolder().getIdHolderPk()!=null)
				query.setParameter("idHolderPk",registerFilterTO.getHolder().getIdHolderPk());
			
			if(registerFilterTO.getSecurity().getIdSecurityCodePk()!=null)
				query.setParameter("idSecurityCodePk",registerFilterTO.getSecurity().getIdSecurityCodePk());
			
			if(registerFilterTO.getCurrency()!=null)
				query.setParameter("currency", registerFilterTO.getCurrency());
			
			if(registerFilterTO.getSecurityClass()!=null)
				query.setParameter("securityClass",registerFilterTO.getSecurityClass());	
			
			if(registerFilterTO.getIssuer().getIdIssuerPk()!=null)
				query.setParameter("idIssuerPk", registerFilterTO.getIssuer().getIdIssuerPk());
			
			List<Object[]> list  = null;
			try {
				list = query.getResultList();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			SearchRegisterResultTO resultTO = new SearchRegisterResultTO();
			List<SearchRegisterResultTO> listResult = new ArrayList<SearchRegisterResultTO>();
			
			long gp=1;
			for ( Object[] objects: list) {
				resultTO = new SearchRegisterResultTO();
				resultTO.setIdParticipantPk(Long.parseLong(objects[0].toString()));
				resultTO.setIdHolderAccountPk(Long.parseLong(objects[1].toString()));
				resultTO.setIdSecurityCodePk(objects[2].toString());	
				resultTO.setTotalBalance(new BigDecimal(objects[3].toString()));
				resultTO.setAvailableBalance(new BigDecimal(objects[3].toString()));
				resultTO.setCurrency(Integer.parseInt(objects[4].toString()));
				resultTO.setIdHolderPk(Long.parseLong(objects[5].toString()));	
				resultTO.setIdIssuerPK(objects[6].toString());
				resultTO.setInitialNominalValue(new BigDecimal(objects[7].toString()));
				resultTO.setAlternateCode(objects[8].toString());
				resultTO.setCurrencyDesc(find(ParameterTable.class, resultTO.getCurrency()).getText1());
				HolderAccount holderAccount = em.find(HolderAccount.class,resultTO.getIdHolderAccountPk());
				resultTO.setAccountNumber(holderAccount.getAccountNumber());
				resultTO.setCouponNumber(BigDecimal.ZERO.intValue());
				resultTO.setDueDate(CommonsUtilities.convertDatetoString(CommonsUtilities.currentDate()));
				resultTO.setPaymentDate(CommonsUtilities.currentDate());
				resultTO.setInterestRate(BigDecimal.ZERO);
				resultTO.setAmortization(BigDecimal.ZERO);
				resultTO.setInterest(true);
				resultTO.setIdGeneralPurposePk(gp);
				gp++;
				listResult.add(resultTO);
			}
			return listResult;
		}

		StringBuilder queryBuilder = new StringBuilder();
		/* 0 */
		queryBuilder.append(" SELECT HAB.ID_PARTICIPANT_PK,");
		/* 1 */
		queryBuilder.append(" S.ID_SECURITY_CODE_PK, ");
		/* 2 */
		queryBuilder.append(" S.CURRENCY, ");
		/* 3 */
		queryBuilder.append(" I.ID_ISSUER_PK, ");
		/* 4 */
		queryBuilder.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
		/* 5 */
		queryBuilder.append(" HA.ACCOUNT_NUMBER, ");
		/* 6 */
		queryBuilder.append(" HA.ALTERNATE_CODE, ");
		/* 7 */
		queryBuilder.append(" PIC.COUPON_NUMBER, ");
		/* 8 */
		queryBuilder.append(" NVL(PIC.INTEREST_RATE,0) INTEREST_RATE, ");
		/* 9 */
		queryBuilder.append(" TO_CHAR(PIC.EXPERITATION_DATE, 'DD/MM/YYYY') EXPIRATION_DATE, ");
		// queryBuilder.append(" TO_CHAR(S.EXPIRATION_DATE, 'DD/MM/YYYY')
		// EXPIRATION_DATE, ");
		/* 10 */
		queryBuilder.append(" TO_CHAR(PIC.PAYMENT_DATE, 'DD/MM/YYYY') PAYMENT_DATE, ");
		/* 11 */
		queryBuilder.append(" HAB.TOTAL_BALANCE, ");
		/* 12 */
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, ");
		/* 13 */
		queryBuilder.append(" CASE ");
		queryBuilder.append("     WHEN S.IND_PREPAID = 1 ");
		queryBuilder.append(" THEN ROUND(S.INITIAL_NOMINAL_VALUE,2) ");
		queryBuilder.append("     ELSE ROUND(PIC.COUPON_AMOUNT,2) ");
		queryBuilder.append(" END INITIAL_NOMINAL_VALUE, ");
		/* 14 */
		queryBuilder.append(" CASE ");
		queryBuilder.append("     WHEN S.IND_PREPAID = 1 ");
		queryBuilder.append(" THEN ROUND(ROUND(S.INITIAL_NOMINAL_VALUE,2)*HAB.TOTAL_BALANCE,2) ");
		queryBuilder.append("      ELSE ROUND(ROUND(PIC.COUPON_AMOUNT,2)*HAB.TOTAL_BALANCE,2) ");
		queryBuilder.append(" END COUPON_AMOUNT, ");
		/* 15 */
		queryBuilder.append(" PIC.ID_PROGRAM_INTEREST_PK,");
		/* 16 */
		queryBuilder.append("'INTERES' TYPE ");

		queryBuilder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
		queryBuilder.append(
				" INNER JOIN SECURITY S                         ON S.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK ");
		queryBuilder.append(
				" INNER JOIN HOLDER_ACCOUNT HA                  ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(
				" INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS      ON S.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK ");
		queryBuilder.append(
				" INNER JOIN PROGRAM_INTEREST_COUPON PIC        ON IPS.ID_INT_PAYMENT_SCHEDULE_PK=PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");

		if (registerFilterTO.getHolder().getIdHolderPk() != null)
			queryBuilder.append(" AND HAD.ID_HOLDER_FK = :idHolderPk ");
		else
			queryBuilder.append(" AND HAD.ID_HOLDER_FK IN (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
		queryBuilder.append(" AND S.STATE_SECURITY = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND S.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PIC.COUPON_AMOUNT >  0  ");

		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIdParticipantPk()))
			queryBuilder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getTypeIncome()))
			queryBuilder.append(" AND S.INSTRUMENT_TYPE = :instrumentType ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIssuer().getIdIssuerPk()))
			queryBuilder.append(" AND S.ID_ISSUER_FK = '" + registerFilterTO.getIssuer().getIdIssuerPk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurityClass()))
			queryBuilder.append(" AND S.SECURITY_CLASS = " + registerFilterTO.getSecurityClass());
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurity().getIdSecurityCodePk()))
			queryBuilder.append(
					" AND S.ID_SECURITY_CODE_PK = '" + registerFilterTO.getSecurity().getIdSecurityCodePk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getCurrency()))
			queryBuilder.append(" and S.CURRENCY = " + registerFilterTO.getCurrency());

		queryBuilder.append(" AND TRUNC(PIC.PAYMENT_DATE) BETWEEN :initialDate AND :finalDate ");

		queryBuilder.append(" UNION ");

		queryBuilder.append(" SELECT HAB.ID_PARTICIPANT_PK,");
		/* 1 */
		queryBuilder.append(" S.ID_SECURITY_CODE_PK, ");
		/* 2 */
		queryBuilder.append(" S.CURRENCY, ");
		/* 3 */
		queryBuilder.append(" I.ID_ISSUER_PK, ");
		/* 4 */
		queryBuilder.append(" HA.ID_HOLDER_ACCOUNT_PK, ");
		/* 5 */
		queryBuilder.append(" HA.ACCOUNT_NUMBER, ");
		/* 6 */
		queryBuilder.append(" HA.ALTERNATE_CODE, ");
		/* 7 */
		queryBuilder.append(" PAC.COUPON_NUMBER, ");
		/* 8 */
		queryBuilder.append(" NVL(S.INTEREST_RATE,0) INTEREST_RATE, ");
		/* 9 */
		queryBuilder.append(" TO_CHAR(PAC.EXPRITATION_DATE, 'DD/MM/YYYY') EXPIRATION_DATE, ");
		/* 10 */
		queryBuilder.append(" TO_CHAR(PAC.PAYMENT_DATE, 'DD/MM/YYYY') PAYMENT_DATE, ");
		/* 11 */
		queryBuilder.append(" HAB.TOTAL_BALANCE, ");
		/* 12 */
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, ");
		/* 13 */
		queryBuilder.append(" CASE ");
		queryBuilder.append("     WHEN S.IND_PREPAID = 1 ");
		queryBuilder.append(" THEN ROUND(S.INITIAL_NOMINAL_VALUE,2) ");
		queryBuilder.append("     ELSE ROUND(PAC.COUPON_AMOUNT,2) ");
		queryBuilder.append(" END INITIAL_NOMINAL_VALUE, ");
		/* 14 */
		queryBuilder.append(" CASE ");
		queryBuilder.append("     WHEN S.IND_PREPAID = 1 ");
		queryBuilder.append(" THEN ROUND(ROUND(S.INITIAL_NOMINAL_VALUE,2)*HAB.TOTAL_BALANCE,2) ");
		queryBuilder.append("      ELSE ROUND(ROUND(PAC.COUPON_AMOUNT,2)*HAB.TOTAL_BALANCE,2) ");
		queryBuilder.append(" END AMORTIZATION_AMOUNT, ");
		/* 15 */
		queryBuilder.append(" PAC.ID_PROGRAM_AMORTIZATION_PK, ");
		/* 16 */
		queryBuilder.append("'AMORTIZACION' TYPE ");

		queryBuilder.append(" FROM SECURITY S ");
		queryBuilder.append(" INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE hab         ON S.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HA                  ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE APS  ON APS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
		queryBuilder.append(" INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		if (registerFilterTO.getHolder().getIdHolderPk() != null)
			queryBuilder.append(" and had.ID_HOLDER_FK = :idHolderPk ");
		else
			queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
		queryBuilder.append(" AND S.STATE_SECURITY = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND S.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PAC.COUPON_AMOUNT >  0  ");

		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIdParticipantPk()))
			queryBuilder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getTypeIncome()))
			queryBuilder.append(" AND S.INSTRUMENT_TYPE = :instrumentType ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIssuer().getIdIssuerPk()))
			queryBuilder.append(" AND S.ID_ISSUER_FK = '" + registerFilterTO.getIssuer().getIdIssuerPk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurityClass()))
			queryBuilder.append(" AND S.SECURITY_CLASS = " + registerFilterTO.getSecurityClass());
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurity().getIdSecurityCodePk()))
			queryBuilder.append(" AND S.ID_SECURITY_CODE_PK = '" + registerFilterTO.getSecurity().getIdSecurityCodePk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getCurrency()))
			queryBuilder.append(" and S.CURRENCY = " + registerFilterTO.getCurrency());

		queryBuilder.append(" AND TRUNC(PAC.PAYMENT_DATE) BETWEEN :initialDate AND :finalDate ");

		queryBuilder.append(" ORDER BY TYPE DESC ");

		Query query = em.createNativeQuery(queryBuilder.toString());
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", registerFilterTO.getIdParticipantPk());

		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getTypeIncome()))
			query.setParameter("instrumentType", registerFilterTO.getTypeIncome());

		query.setParameter("initialDate", registerFilterTO.getInitialDate());
		query.setParameter("finalDate", registerFilterTO.getFinalDate());

		if (registerFilterTO.getHolder().getIdHolderPk() != null)
			query.setParameter("idHolderPk", registerFilterTO.getHolder().getIdHolderPk());

		
		List<Object[]> resultList;
		try {
			resultList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		List<SearchRegisterResultTO> listSecurities = new ArrayList<>();
		long gp=1;
		SearchRegisterResultTO resultTO;
		for (Object[] objects : resultList) {

			resultTO = new SearchRegisterResultTO();
			resultTO.setIdParticipantPk(Long.parseLong(objects[0].toString()));
			resultTO.setIdSecurityCodePk(objects[1].toString());
			resultTO.setCurrency(Integer.parseInt(objects[2].toString()));
			resultTO.setIdIssuerPK(objects[3].toString());
			resultTO.setIdHolderAccountPk(Long.parseLong(objects[4].toString()));
			resultTO.setAccountNumber(Integer.parseInt(objects[5].toString()));
			resultTO.setAlternateCode(objects[6].toString());
			resultTO.setCouponNumber(Integer.parseInt(objects[7].toString()));
			resultTO.setDueDate(objects[9] == null ? "" : objects[9].toString());
			
			resultTO.setPaymentDate(CommonsUtilities.convertStringtoDate(objects[10].toString()));
			/*
			 * No cargamos valores que ya esten registrados en solicitud de cobro Puede
			 * existir valores que se paguen en una sola fecha pero distintos numero de
			 * cupon
			 */
			if (isExistsEconomicRight(resultTO))
				continue;

			resultTO.setTotalBalance(new BigDecimal(objects[11].toString()));
			resultTO.setAvailableBalance(objects[12] == null ? BigDecimal.ZERO : new BigDecimal(objects[12].toString()));
			resultTO.setInitialNominalValue(objects[13] == null ? BigDecimal.ZERO : new BigDecimal(objects[13].toString()));
			/* Monto total de interes */
			if (objects[16].toString().equals("INTERES")) {
				resultTO.setInterestRate(objects[14] == null ? BigDecimal.ZERO : new BigDecimal(objects[14].toString()));
				resultTO.setAmortization(BigDecimal.ZERO);
				resultTO.setInterest(true);
			}

			//resultTO.setIdGeneralPurposePk(Long.parseLong(objects[15].toString()));
			resultTO.setIdGeneralPurposePk(gp);
			gp++;
			/** Buscamos el nemonico de la moneda */
			resultTO.setCurrencyDesc(find(ParameterTable.class, resultTO.getCurrency()).getText1());
			/** Buscar amortizacion o las amortizaciones que pueda existir */
			if (objects[16].toString().equals("AMORTIZACION")) {
				resultTO.setAmortization(objects[14] == null ? BigDecimal.ZERO : new BigDecimal(objects[14].toString()));
				resultTO.setInterestRate(BigDecimal.ZERO);
				resultTO.setInterest(false);
			}
			/** Set holder a partir de codigo alterno */
			if (registerFilterTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				resultTO.setIdHolderPk(registerFilterTO.getHolder().getIdHolderPk());
			else
				resultTO.setIdHolderPk(extractHolder(resultTO.getAlternateCode()));

			listSecurities.add(resultTO);
		}
		/** Agrupar valores que tengan el interes con fecha de pago el mismo dia */
		HashMap<ObjectCollection, SearchRegisterResultTO> mapSecuritiesToPay = new HashMap<ObjectCollection, SearchRegisterResultTO>();
		for (SearchRegisterResultTO var1 : listSecurities) {
			/**
			 * Modificando la fecha de cobro en caso de ser una feriado o un dia no habil
			 **/
			try {
				Date paymentDate = var1.getPaymentDate();
				if (holidayQueryServiceBean.isHolidayDateServiceBean(paymentDate)) {
					Date nextBusinessDay = holidayQueryServiceBean.getNextWorkingDayServiceBean(paymentDate);
					var1.setPaymentDate(nextBusinessDay);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			/** Asignando valores a objeto del map principal */
			ObjectCollection objectCollection = new ObjectCollection();
			objectCollection.setIdParticipantPk(var1.getIdParticipantPk());
			objectCollection.setIdSecurityCodePk(var1.getIdSecurityCodePk());
			objectCollection.setAlternativeCode(var1.getAlternateCode());
			objectCollection.setCurrency(var1.getCurrency());
			objectCollection.setPaymentDate(CommonsUtilities.convertDatetoString(var1.getPaymentDate()));
			objectCollection.setExpirationDate(var1.getDueDate());

			if (!containsKey(mapSecuritiesToPay, objectCollection)) 
				mapSecuritiesToPay.put(objectCollection, var1);
		}
		for (Object value : mapSecuritiesToPay.values()) {
			SearchRegisterResultTO var1 = new SearchRegisterResultTO();
			var1 = (SearchRegisterResultTO) value;
			BigDecimal sumInterest = BigDecimal.ZERO;
			BigDecimal sumAmortization = BigDecimal.ZERO;

			for (SearchRegisterResultTO var2 : listSecurities) {
				if (var1.getIdParticipantPk().equals(var2.getIdParticipantPk())
						&& var1.getIdSecurityCodePk().equals(var2.getIdSecurityCodePk())
						&& var1.getAlternateCode().equals(var2.getAlternateCode())
						&& var1.getPaymentDate().equals(var2.getPaymentDate())
						&& var1.getCurrency().equals(var2.getCurrency())
						&& var1.getDueDate().equals(var2.getDueDate())) {
					sumInterest = sumInterest.add(var2.getInterestRate());
					sumAmortization = sumAmortization.add(var2.getAmortization());
				}
			}
			var1.setInterestRate(sumInterest);
			var1.setAmortization(sumAmortization);
			var1.setTotalAmount(var1.getAmortization().add(var1.getInterestRate()));
		}

		List<SearchRegisterResultTO> listSecuritiesToPay = new ArrayList<SearchRegisterResultTO>(mapSecuritiesToPay.values());
		return listSecuritiesToPay;
	}
	
	/**
	 * Buscamos todos los intereses Primero buscamos los intereses luego los cupones
	 * RF, CON SALDO DISPONIBLE
	 * 
	 * @param registerFilterTO
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SearchRegisterResultTO> getListSecuritiesNotAvailableBalanceRV(RegisterCollectionRightTO registerFilterTO){

		StringBuilder builder = new StringBuilder();
		builder.append(" SELECT HAB.TOTAL_BALANCE, HAB.AVAILABLE_BALANCE,HAB.TRANSIT_BALANCE,HAB.PAWN_BALANCE ");
		builder.append(" ,HAB.BAN_BALANCE,HAB.OTHER_BLOCK_BALANCE,HAB.SALE_BALANCE,HAB.PURCHASE_BALANCE,HAB.REPORTED_BALANCE,HAB.REPORTING_BALANCE,HAB.MARGIN_BALANCE ");
		builder.append(" ,HAB.ACCREDITATION_BALANCE,HAB.BORROWER_BALANCE,HAB.LENDER_BALANCE,HAB.RESERVE_BALANCE,HAB.LOANABLE_BALANCE,HAB.OPPOSITION_BALANCE,HAB.GRANTED_BALANCE ");
		builder.append(" ,HAB.GRANT_BALANCE, HAB.ID_SECURITY_CODE_PK ");
		builder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
		builder.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL HAD ON HAB.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		builder.append(" INNER JOIN SECURITY SE ON HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
		builder.append(" INNER JOIN ISSUER ISS ON SE.ID_ISSUER_FK = ISS.ID_ISSUER_PK ");
		
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND HAB.AVAILABLE_BALANCE = 0 ");
		builder.append(" AND SE.STATE = 131 ");
		builder.append(" AND SE.INSTRUMENT_TYPE = "+InstrumentType.VARIABLE_INCOME.getCode());
		if(registerFilterTO.getIdParticipantPk()!=null)
			builder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		
		if(registerFilterTO.getHolder().getIdHolderPk()!=null)
			builder.append(" AND HAD.ID_HOLDER_FK = :idHolderPk ");
		
		if(registerFilterTO.getSecurity().getIdSecurityCodePk()!=null)
			builder.append("AND SE.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
		
		if(registerFilterTO.getCurrency()!=null)
			builder.append(" AND SE.CURRENCY = :currency ");		
		
		if(registerFilterTO.getIssuer().getIdIssuerPk()!=null)
			builder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk ");
		
		Query query = em.createNativeQuery(builder.toString());	
		
		if(registerFilterTO.getIdParticipantPk()!=null)
			query.setParameter("idParticipantPk", registerFilterTO.getIdParticipantPk());
		
		if(registerFilterTO.getHolder().getIdHolderPk()!=null)
			query.setParameter("idHolderPk",registerFilterTO.getHolder().getIdHolderPk());
		
		if(registerFilterTO.getSecurity().getIdSecurityCodePk()!=null)
			query.setParameter("idSecurityCodePk",registerFilterTO.getSecurity().getIdSecurityCodePk());
		
		if(registerFilterTO.getCurrency()!=null)
			query.setParameter("currency", registerFilterTO.getCurrency());
		
		if(registerFilterTO.getIssuer().getIdIssuerPk()!=null)
			query.setParameter("idIssuerPk", registerFilterTO.getIssuer().getIdIssuerPk());
		
		List<Object[]> list  = null;
		try {
			list = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		SearchRegisterResultTO resultTO = new SearchRegisterResultTO();
		List<SearchRegisterResultTO> listResult = new ArrayList<SearchRegisterResultTO>();
		for ( Object[] objects: list) {
			resultTO = new SearchRegisterResultTO();
			resultTO.setTotalBalance(objects[0] == null ? BigDecimal.ZERO : new BigDecimal(objects[0].toString()));
			resultTO.setAvailableBalance(objects[1] == null ? BigDecimal.ZERO : new BigDecimal(objects[1].toString()));
			resultTO.setTransitBalance(objects[2] == null ? BigDecimal.ZERO : new BigDecimal(objects[2].toString()));
			resultTO.setPawnBalance(objects[3] == null ? BigDecimal.ZERO : new BigDecimal(objects[3].toString()));
			resultTO.setBanBalance(objects[4] == null ? BigDecimal.ZERO : new BigDecimal(objects[4].toString()));
			resultTO.setOtherBlockBalance(objects[5] == null ? BigDecimal.ZERO : new BigDecimal(objects[5].toString()));
			resultTO.setSaleBalance(objects[6] == null ? BigDecimal.ZERO : new BigDecimal(objects[6].toString()));
			resultTO.setPurchaseBalance(objects[7] == null ? BigDecimal.ZERO : new BigDecimal(objects[7].toString()));
			resultTO.setReportedBalance(objects[8] == null ? BigDecimal.ZERO : new BigDecimal(objects[8].toString()));
			resultTO.setReportingBalance(objects[9] == null ? BigDecimal.ZERO : new BigDecimal(objects[9].toString()));
			resultTO.setMarginBalance(objects[10] == null ? BigDecimal.ZERO : new BigDecimal(objects[10].toString()));
			resultTO.setAcreditationBalance(objects[11] == null ? BigDecimal.ZERO : new BigDecimal(objects[11].toString()));
			resultTO.setBorrowerBalance(objects[12] == null ? BigDecimal.ZERO : new BigDecimal(objects[12].toString()));
			resultTO.setLenderBalance(objects[13] == null ? BigDecimal.ZERO : new BigDecimal(objects[13].toString()));
			resultTO.setReserveBalance(objects[14] == null ? BigDecimal.ZERO : new BigDecimal(objects[14].toString()));
			resultTO.setLoanableBalance(objects[15] == null ? BigDecimal.ZERO : new BigDecimal(objects[15].toString()));
			resultTO.setOpositionBalance(objects[16] == null ? BigDecimal.ZERO : new BigDecimal(objects[16].toString()));
			resultTO.setGrantedBalance(objects[17] == null ? BigDecimal.ZERO : new BigDecimal(objects[17].toString()));
			resultTO.setGrantBalance(objects[18] == null ? BigDecimal.ZERO : new BigDecimal(objects[18].toString()));	
			
	        resultTO.setIdSecurityCodePk(objects[19].toString());
	        resultTO.setDueDate(CommonsUtilities.STR_BLANK);
	        
			listResult.add(resultTO);
		}
		return listResult;
	}
	

	/**
	 * Buscamos todos los intereses Primero buscamos los intereses luego los cupones
	 * RF, CON SALDO DISPONIBLE
	 * 
	 * @param registerFilterTO
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SearchRegisterResultTO> getListSecuritiesNotAvailableBalanceRF(
			RegisterCollectionRightTO registerFilterTO) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" SELECT ");
		/* 0 */
		queryBuilder.append(" HAB.TOTAL_BALANCE, ");
		/* 1 */
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, ");
		/* 2 */
		queryBuilder.append(" HAB.TRANSIT_BALANCE, ");
		/* 3 */
		queryBuilder.append(" HAB.PAWN_BALANCE, ");
		/* 4 */
		queryBuilder.append(" HAB.BAN_BALANCE, ");
		/* 5 */
		queryBuilder.append(" HAB.OTHER_BLOCK_BALANCE, ");
		/* 6 */
		queryBuilder.append(" HAB.SALE_BALANCE, ");
		/* 7 */
		queryBuilder.append(" HAB.REPORTED_BALANCE, ");
		/* 8 */
		queryBuilder.append(" HAB.REPORTING_BALANCE, ");
		/* 9 */
		queryBuilder.append(" HAB.MARGIN_BALANCE, ");
		/* 10 */
		queryBuilder.append(" HAB.ACCREDITATION_BALANCE, ");
		/* 11 */
		queryBuilder.append(" HAB.BORROWER_BALANCE, ");
		/* 12 */
		queryBuilder.append(" HAB.LENDER_BALANCE, ");
		/* 13 */
		queryBuilder.append(" HAB.RESERVE_BALANCE, ");
		/* 14 */
		queryBuilder.append(" HAB.LOANABLE_BALANCE, ");
		/* 15 */
		queryBuilder.append(" HAB.OPPOSITION_BALANCE, ");
		/* 16 */
		queryBuilder.append(" HAB.GRANTED_BALANCE, ");
		/* 17 */
		queryBuilder.append(" HAB.GRANT_BALANCE, ");
		/* 18 */
		queryBuilder.append(" S.ID_SECURITY_CODE_PK, ");
		/* 19 */
		queryBuilder.append(" TO_CHAR(S.EXPIRATION_DATE,'dd/MM/yyyy') ");
		
		queryBuilder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
		queryBuilder.append(" INNER JOIN SECURITY S ON S.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HA ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN ISSUER I ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(" INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS      ON S.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK ");
		queryBuilder.append(" INNER JOIN PROGRAM_INTEREST_COUPON PIC        ON IPS.ID_INT_PAYMENT_SCHEDULE_PK=PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE = 0 ");
		queryBuilder.append(" AND HAB.TOTAL_BALANCE > 0 ");
		queryBuilder.append(" AND S.STATE_SECURITY  = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND S.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PIC.COUPON_AMOUNT >  0  ");

		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIdParticipantPk()))
			queryBuilder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getTypeIncome()))
			queryBuilder.append(" AND S.INSTRUMENT_TYPE = :instrumentType ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIssuer().getIdIssuerPk()))
			queryBuilder.append(" AND S.ID_ISSUER_FK = '" + registerFilterTO.getIssuer().getIdIssuerPk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurityClass()))
			queryBuilder.append(" AND S.SECURITY_CLASS = " + registerFilterTO.getSecurityClass());
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurity().getIdSecurityCodePk()))
			queryBuilder.append(" AND S.ID_SECURITY_CODE_PK = '" + registerFilterTO.getSecurity().getIdSecurityCodePk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getCurrency()))
			queryBuilder.append(" and S.CURRENCY = " + registerFilterTO.getCurrency());

		queryBuilder.append(" AND TRUNC(PIC.PAYMENT_DATE) BETWEEN :initialDate AND :finalDate ");

		queryBuilder.append(" UNION ");

		queryBuilder.append(" SELECT ");
		/* 0 */
		queryBuilder.append(" HAB.TOTAL_BALANCE, ");
		/* 1 */
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, ");
		/* 2 */
		queryBuilder.append(" HAB.TRANSIT_BALANCE, ");
		/* 3 */
		queryBuilder.append(" HAB.PAWN_BALANCE, ");
		/* 4 */
		queryBuilder.append(" HAB.BAN_BALANCE, ");
		/* 5 */
		queryBuilder.append(" HAB.OTHER_BLOCK_BALANCE, ");
		/* 6 */
		queryBuilder.append(" HAB.SALE_BALANCE, ");
		/* 7 */
		queryBuilder.append(" HAB.REPORTED_BALANCE, ");
		/* 8 */
		queryBuilder.append(" HAB.REPORTING_BALANCE, ");
		/* 9 */
		queryBuilder.append(" HAB.MARGIN_BALANCE, ");
		/* 10 */
		queryBuilder.append(" HAB.ACCREDITATION_BALANCE, ");
		/* 11 */
		queryBuilder.append(" HAB.BORROWER_BALANCE, ");
		/* 12 */
		queryBuilder.append(" HAB.LENDER_BALANCE, ");
		/* 13 */
		queryBuilder.append(" HAB.RESERVE_BALANCE, ");
		/* 14 */
		queryBuilder.append(" HAB.LOANABLE_BALANCE, ");
		/* 15 */
		queryBuilder.append(" HAB.OPPOSITION_BALANCE, ");
		/* 16 */
		queryBuilder.append(" HAB.GRANTED_BALANCE, ");
		/* 17 */
		queryBuilder.append(" HAB.GRANT_BALANCE, ");
		/* 18 */
		queryBuilder.append(" S.ID_SECURITY_CODE_PK, ");
		/* 19 */
		queryBuilder.append(" TO_CHAR(S.EXPIRATION_DATE,'dd/MM/yyyy') ");

		queryBuilder.append(" FROM SECURITY S ");
		queryBuilder.append(" INNER JOIN ISSUER I ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT_BALANCE hab         ON S.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HA                  ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE APS  ON APS.ID_SECURITY_CODE_FK = S.ID_SECURITY_CODE_PK ");
		queryBuilder.append(" INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE = 0 ");
		queryBuilder.append(" AND HAB.TOTAL_BALANCE > 0 ");
		queryBuilder.append(" AND S.STATE_SECURITY = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND S.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PAC.COUPON_AMOUNT >  0  ");

		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIdParticipantPk()))
			queryBuilder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getTypeIncome()))
			queryBuilder.append(" AND S.INSTRUMENT_TYPE = :instrumentType ");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIssuer().getIdIssuerPk()))
			queryBuilder.append(" AND S.ID_ISSUER_FK = '" + registerFilterTO.getIssuer().getIdIssuerPk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurityClass()))
			queryBuilder.append(" AND S.SECURITY_CLASS = " + registerFilterTO.getSecurityClass());
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getSecurity().getIdSecurityCodePk()))
			queryBuilder.append(" AND S.ID_SECURITY_CODE_PK = '" + registerFilterTO.getSecurity().getIdSecurityCodePk() + "'");
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getCurrency()))
			queryBuilder.append(" and S.CURRENCY = " + registerFilterTO.getCurrency());

		queryBuilder.append(" AND TRUNC(PAC.PAYMENT_DATE) BETWEEN :initialDate AND :finalDate ");

		Query query = em.createNativeQuery(queryBuilder.toString());
		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", registerFilterTO.getIdParticipantPk());

		if (Validations.validateIsNotNullAndNotEmpty(registerFilterTO.getTypeIncome()))
			query.setParameter("instrumentType", registerFilterTO.getTypeIncome());

		query.setParameter("initialDate", registerFilterTO.getInitialDate());
		query.setParameter("finalDate", registerFilterTO.getFinalDate());
		List<Object[]> resultList;
		try {
			resultList = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		List<SearchRegisterResultTO> listSecurities = new ArrayList<>();
		SearchRegisterResultTO resultTO;
		for (Object[] objects : resultList) {

			resultTO = new SearchRegisterResultTO();
			resultTO.setTotalBalance(new BigDecimal(objects[0].toString()));
			resultTO.setAvailableBalance(objects[1] == null ? BigDecimal.ZERO : new BigDecimal(objects[1].toString()));
			resultTO.setTransitBalance(objects[2] == null ? BigDecimal.ZERO : new BigDecimal(objects[2].toString()));
			resultTO.setPawnBalance(objects[3] == null ? BigDecimal.ZERO : new BigDecimal(objects[3].toString()));
			resultTO.setBanBalance(objects[4] == null ? BigDecimal.ZERO : new BigDecimal(objects[4].toString()));
			resultTO.setOtherBlockBalance(objects[5] == null ? BigDecimal.ZERO : new BigDecimal(objects[5].toString()));
			resultTO.setSaleBalance(objects[6] == null ? BigDecimal.ZERO : new BigDecimal(objects[6].toString()));
			resultTO.setReportedBalance(objects[7] == null ? BigDecimal.ZERO : new BigDecimal(objects[7].toString()));
			resultTO.setReportingBalance(objects[8] == null ? BigDecimal.ZERO : new BigDecimal(objects[8].toString()));
			resultTO.setMarginBalance(objects[9] == null ? BigDecimal.ZERO : new BigDecimal(objects[9].toString()));
			resultTO.setAcreditationBalance(objects[10] == null ? BigDecimal.ZERO : new BigDecimal(objects[10].toString()));
			resultTO.setBorrowerBalance(objects[11] == null ? BigDecimal.ZERO : new BigDecimal(objects[11].toString()));
			resultTO.setLenderBalance(objects[12] == null ? BigDecimal.ZERO : new BigDecimal(objects[12].toString()));
			resultTO.setReserveBalance(objects[13] == null ? BigDecimal.ZERO : new BigDecimal(objects[13].toString()));
			resultTO.setLoanableBalance(objects[14] == null ? BigDecimal.ZERO : new BigDecimal(objects[14].toString()));
			resultTO.setOpositionBalance(objects[15] == null ? BigDecimal.ZERO : new BigDecimal(objects[15].toString()));
			resultTO.setGrantedBalance(objects[16] == null ? BigDecimal.ZERO : new BigDecimal(objects[16].toString()));
			resultTO.setGrantBalance(objects[17] == null ? BigDecimal.ZERO : new BigDecimal(objects[17].toString()));
			resultTO.setIdSecurityCodePk(objects[18].toString());
			resultTO.setDueDate(objects[19].toString());

			listSecurities.add(resultTO);
		}
		return listSecurities;
	}

	/**
	 * Buscamos los datos de los valores que tenga fecha de pago y que se encuentren
	 * registrados en cobro de derechos economicos
	 * 
	 * @param idParticipantPk id del participante
	 * @param idIssuerPk      id del emisor
	 * @param securityClass   Clase de valor buscado para la agrupacion
	 * @param currencyType    Tipo de moneda para la clasificacion
	 * @param paymentDate     Fecha inicial para la busqueda
	 * @praram nextBusinessDay dia siguiente habil
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<Object[]> getSecuritiesToNotify(CollectionEconomicRigth collectionEconomicRigth,

			String idIssuerPk, Integer securityClass, Long idParticipantPk, Date paymentDate, Date nextBusinessDay) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" SELECT ");
		queryBuilder.append(" HAB.ID_PARTICIPANT_PK, "); // 0
		queryBuilder.append(" SE.ID_SECURITY_CODE_PK, "); // 1
		queryBuilder.append(" SE.CURRENCY, "); // 2
		queryBuilder.append(" SE.ID_ISSUER_FK, "); // 3
		queryBuilder.append(" SE.SECURITY_CLASS, "); // 4
		queryBuilder.append(" SE.INSTRUMENT_TYPE, "); // 5
		queryBuilder.append(" PIC.EXPERITATION_DATE AS EXPIRATION_DATE, "); // 6
		queryBuilder.append(" PIC.PAYMENT_DATE, "); // 7
		queryBuilder.append(" PIC.COUPON_AMOUNT, "); // 8
		queryBuilder.append(" PIC.COUPON_NUMBER, "); // 9
		queryBuilder.append(" HAB.ID_HOLDER_ACCOUNT_PK, "); // 10
		queryBuilder.append(" HA.ACCOUNT_NUMBER, "); // 11
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, "); // 12
		queryBuilder.append(" HAB.TRANSIT_BALANCE, "); // 13
		queryBuilder.append(" HAB.PAWN_BALANCE, "); // 14
		queryBuilder.append(" HAB.BAN_BALANCE, "); // 15
		queryBuilder.append(" HAB.OTHER_BLOCK_BALANCE, "); // 16
		queryBuilder.append(" HAB.SALE_BALANCE, "); // 17
		queryBuilder.append(" HAB.PURCHASE_BALANCE, "); // 18
		queryBuilder.append(" HAB.REPORTED_BALANCE, "); // 19
		queryBuilder.append(" HAB.REPORTING_BALANCE, "); // 20
		queryBuilder.append(" HAB.MARGIN_BALANCE, "); // 21
		queryBuilder.append(" HAB.ACCREDITATION_BALANCE, "); // 22
		queryBuilder.append(" HAB.BORROWER_BALANCE, "); // 23
		queryBuilder.append(" HAB.LENDER_BALANCE, "); // 24
		queryBuilder.append(" HAB.RESERVE_BALANCE, "); // 25
		queryBuilder.append(" HAB.LOANABLE_BALANCE,"); // 26
		queryBuilder.append(" HAB.OPPOSITION_BALANCE, "); // 27
		queryBuilder.append(" HAB.GRANTED_BALANCE, "); // 28
		queryBuilder.append(" HAB.GRANT_BALANCE, "); // 29
		queryBuilder.append(" PIC.ID_PROGRAM_INTEREST_PK AS ID_GENERAL_PK "); // 30
		queryBuilder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
		queryBuilder.append(
				" INNER JOIN SECURITY SE                        ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK ");
		queryBuilder.append(
				" INNER JOIN HOLDER_ACCOUNT HA                  ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN ISSUER I                           ON SE.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(
				" INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS      ON SE.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK ");
		queryBuilder.append(
				" INNER JOIN PROGRAM_INTEREST_COUPON PIC        ON IPS.ID_INT_PAYMENT_SCHEDULE_PK=PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
		queryBuilder.append(" AND SE.STATE_SECURITY = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND SE.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PIC.COUPON_AMOUNT >  0 ");
		/** FILTROS DE BUSQUEDA */
		if (idIssuerPk != null)
			queryBuilder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk ");
		if (securityClass != null)
			queryBuilder.append(" AND SE.SECURITY_CLASS = :securityClass ");// o clase de valor
		if (idParticipantPk != null)
			queryBuilder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		if (collectionEconomicRigth != null && collectionEconomicRigth.getIdSecurityCodeFk() != null)
			queryBuilder
					.append(" AND SE.ID_SECURITY_CODE_PK IN (" + collectionEconomicRigth.getIdSecurityCodeFk() + ")");
		queryBuilder.append(" AND TRUNC(PIC.PAYMENT_DATE) BETWEEN :paymentDate AND :nextBusinessDay ");

		// UNION CON AMORTIZACIONS
		queryBuilder.append(" UNION ");

		queryBuilder.append(" SELECT ");
		queryBuilder.append(" HAB.ID_PARTICIPANT_PK, "); // 0
		queryBuilder.append(" SE.ID_SECURITY_CODE_PK, "); // 1
		queryBuilder.append(" SE.CURRENCY, "); // 2
		queryBuilder.append(" SE.ID_ISSUER_FK, "); // 3
		queryBuilder.append(" SE.SECURITY_CLASS, "); // 4
		queryBuilder.append(" SE.INSTRUMENT_TYPE, "); // 5
		queryBuilder.append(" SE.EXPIRATION_DATE as EXPIRATION_DATE, "); // 6
		queryBuilder.append(" PAC.PAYMENT_DATE, "); // 7
		queryBuilder.append(" PAC.COUPON_AMOUNT, "); // 8
		queryBuilder.append(" PAC.COUPON_NUMBER, "); // 9
		queryBuilder.append(" HAB.ID_HOLDER_ACCOUNT_PK, "); // 10
		queryBuilder.append(" HA.ACCOUNT_NUMBER, "); // 11
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, "); // 12
		queryBuilder.append(" HAB.TRANSIT_BALANCE, "); // 13
		queryBuilder.append(" HAB.PAWN_BALANCE, "); // 14
		queryBuilder.append(" HAB.BAN_BALANCE, "); // 15
		queryBuilder.append(" HAB.OTHER_BLOCK_BALANCE, "); // 16
		queryBuilder.append(" HAB.SALE_BALANCE, "); // 17
		queryBuilder.append(" HAB.PURCHASE_BALANCE, "); // 18
		queryBuilder.append(" HAB.REPORTED_BALANCE, "); // 19
		queryBuilder.append(" HAB.REPORTING_BALANCE, "); // 20
		queryBuilder.append(" HAB.MARGIN_BALANCE, "); // 21
		queryBuilder.append(" HAB.ACCREDITATION_BALANCE, "); // 22
		queryBuilder.append(" HAB.BORROWER_BALANCE, "); // 23
		queryBuilder.append(" HAB.LENDER_BALANCE, "); // 24
		queryBuilder.append(" HAB.RESERVE_BALANCE, "); // 25
		queryBuilder.append(" HAB.LOANABLE_BALANCE,"); // 26
		queryBuilder.append(" HAB.OPPOSITION_BALANCE, "); // 27
		queryBuilder.append(" HAB.GRANTED_BALANCE, "); // 28
		queryBuilder.append(" HAB.GRANT_BALANCE, "); // 29
		queryBuilder.append(" PAC.ID_PROGRAM_AMORTIZATION_PK AS ID_GENERAL_PK "); // 30

		queryBuilder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
		queryBuilder.append(
				" INNER JOIN SECURITY SE                        ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK ");
		queryBuilder.append(
				" INNER JOIN HOLDER_ACCOUNT HA                  ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN ISSUER I                           ON SE.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(
				" INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE APS  ON APS.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		queryBuilder.append(
				" INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");

		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
		queryBuilder.append(" AND SE.STATE_SECURITY = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND SE.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PAC.COUPON_AMOUNT >  0 ");
		/** FILTROS DE BUSQUEDA */
		if (idIssuerPk != null)
			queryBuilder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk ");
		if (securityClass != null)
			queryBuilder.append(" AND SE.SECURITY_CLASS = :securityClass ");// o clase de valor
		if (idParticipantPk != null)
			queryBuilder.append(" AND HAB.ID_PARTICIPANT_PK = :idParticipantPk ");
		/* Cadena de valores para el IN sql */
		if (collectionEconomicRigth.getIdSecurityCodeFk() != null)
			queryBuilder
					.append(" AND SE.ID_SECURITY_CODE_PK IN (" + collectionEconomicRigth.getIdSecurityCodeFk() + ")");
		queryBuilder.append(" AND TRUNC(PAC.PAYMENT_DATE) BETWEEN :paymentDate AND :nextBusinessDay ");

		Query query = em.createNativeQuery(queryBuilder.toString());

		if (idIssuerPk != null)
			query.setParameter("idIssuerPk", idIssuerPk);// no encuntra por emiros
		if (securityClass != null)
			query.setParameter("securityClass", securityClass);
		if (idParticipantPk != null)
			query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);
		List<Object[]> list = query.getResultList();
		return list;
	}

	/**
	 * Buscamo los valores registrados para el cobro de derechos economicos, usado
	 * en notificaciones
	 * 
	 * @param paymentDate     Dia del pago/cobro de derecho economico
	 * @param nextBusinessDay Dia siguiente habil para el cobro
	 * @param idParticipantPk Id de particpante que busca sus cobros
	 * @param idIssuerPk      Id del emisor
	 * @param instrumentType  Tipo de instrumento o clase de valor
	 * @param currencyType    Tipo de moneda con el cual se efectuara el cobro de
	 *                        derecho economico
	 * @return Retornamos todas id de los valores que estan registrados para el
	 *         cobro de derechos de economicos
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<CollectionEconomicRigth> getListSecuritiesNotify(Date paymentDate, Date nextBusinessDay,
			Long idParticipantPk, String idIssuerPk, String mNemonicPaymentAgent,
			Integer currencyType, Long idHolderPk, Integer securityClass, Integer stateReqCollection,
			Integer stateCollection, Long idBankPk, String depAccountNumber) {
		/* PRIMERO BUSCAMOS SIN MODIFICACION DE LA MONEDA DE COBRO */
		StringBuilder builder = new StringBuilder();
		/* 0 */
		builder.append(" SELECT CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		/* 1,2 */
		builder.append(" ,CER.ID_COLLECTION_ECONOMIC_PK,CER.ID_SECURITY_CODE_FK ");
		/* 3,4 */
		builder.append(" ,CER.ID_HOLDER_ACCOUNT_PK,CER.ACCOUNT_NUMBER ");
		/* 5,6 */
		builder.append(" ,CER.STATE,CER.CHECK_OTHER ");
		/* 7,8 */
		builder.append(" ,CER.CURRENCY,CER.AMOUNT_RECEIVABLE ");
		/* 9,10 */
		builder.append(" ,CER.ANOTHER_CURENCY,CER.ANOTHER_AMOUNT_RECEIVABLE ");
		/* 11,12 */
		builder.append(" ,CER.ID_BANK_FK,CER.DEP_ACCOUNT_NUMBER ");
		/* 13,14 */
		builder.append(" ,CER.COUPON_NUMBER, CER.AMOUNT_SECURITIES ");
		/* 15,16 */
		builder.append(" ,TRUNC(CER.PAYMENT_DATE),TRUNC(CER.DUE_DATE) ");
		/* 17 */
		builder.append(" ,CER.ALTERNATE_CODE ");
		/*18*/
		builder.append(" ,CER.ID_HOLDER_PK ");

		builder.append(" FROM COLLECTION_ECONOMIC_RIGHT CER,SECURITY SE ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND CER.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");

		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND CER.CHECK_OTHER = 0 ");// SIN OTRA MONEDA DE COBRO

		if (Validations.validateIsNotNullAndNotEmpty(securityClass))
			builder.append(" AND SE.SECURITY_CLASS = :securityClass ");

		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");

		if (Validations.validateIsNotNullAndNotEmpty(securityClass))
			if (securityClass.equals(SecurityClassType.DPF.getCode())) {
				if (Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent)) {
					builder.append(" AND ( ");
					builder.append(
							" (SELECT SE.PAYMENT_AGENT  FROM SECURITY SE WHERE SE.ID_SECURITY_CODE_PK =  CER.ID_SECURITY_CODE_FK) = :mNemonicPaymentAgent ");
					builder.append(" OR ");
					builder.append(
							" (SELECT ISS.MNEMONIC  FROM ISSUER ISS  WHERE ISS.ID_ISSUER_PK = SE.ID_ISSUER_FK) = :mNemonicPaymentAgent ");
					builder.append(" ) ");
				}
			} else {
				if (Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent))
					builder.append(
							" AND (SELECT SE.PAYMENT_AGENT  FROM SECURITY SE WHERE SE.ID_SECURITY_CODE_PK =  CER.ID_SECURITY_CODE_FK) = :mNemonicPaymentAgent ");
			}

		if (Validations.validateIsNotNullAndNotEmpty(stateCollection))
			builder.append(" AND CER.STATE = :stateCollection ");

		if (Validations.validateIsNotNullAndNotEmpty(idIssuerPk))
			builder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk ");

		if (Validations.validateIsNotNullAndNotEmpty(currencyType))
			builder.append(" AND CER.CURRENCY = :currencyType");

		if (Validations.validateIsNotNullAndNotEmpty(idHolderPk))
			builder.append(" AND CER.ID_HOLDER_PK = :idHolderPk ");

		if (Validations.validateIsNotNullAndNotEmpty(idBankPk))
			builder.append(" AND CER.ID_BANK_FK = :idBankPk ");

		if (Validations.validateIsNotNullAndNotEmpty(depAccountNumber))
			builder.append(" AND CER.DEP_ACCOUNT_NUMBER = :depAccountNumber ");

		builder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN :paymentDate and :nextBusinessDay ");

		builder.append(" AND CER.ID_REQ_COLLECTION_ECONOMIC_FK IN ( ");

		builder.append(" SELECT RCER.ID_REQ_COLLECTION_ECONOMIC_PK ");
		builder.append(" FROM REQ_COLLECTION_ECONOMIC_RIGHT RCER ");
		builder.append(" WHERE 1 = 1  ");

		if (Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
			builder.append(" AND RCER.ID_PARTICIPANT_FK = :idParticipantPk ");

		if (Validations.validateIsNotNullAndNotEmpty(stateReqCollection))
			builder.append(" AND RCER.STATE = :stateReqCollection ");
		builder.append(" )");
		Query query = em.createNativeQuery(builder.toString());

		if (Validations.validateIsNotNullAndNotEmpty(stateReqCollection))
			query.setParameter("stateReqCollection", stateReqCollection);

		if (Validations.validateIsNotNullAndNotEmpty(stateCollection))
			query.setParameter("stateCollection", stateCollection);

		if (Validations.validateIsNotNullAndNotEmpty(idIssuerPk))
			query.setParameter("idIssuerPk", idIssuerPk);

		if (Validations.validateIsNotNullAndNotEmpty(securityClass))
			query.setParameter("securityClass", securityClass);

		if (Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent))
			query.setParameter("mNemonicPaymentAgent", mNemonicPaymentAgent);

		if (Validations.validateIsNotNullAndNotEmpty(currencyType))
			query.setParameter("currencyType", currencyType);

		if (Validations.validateIsNotNullAndNotEmpty(idHolderPk))
			query.setParameter("idHolderPk", idHolderPk);

		if (Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
			query.setParameter("idParticipantPk", idParticipantPk);

		if (Validations.validateIsNotNullAndNotEmpty(idBankPk))
			query.setParameter("idBankPk", idBankPk);

		if (Validations.validateIsNotNullAndNotEmpty(depAccountNumber))
			query.setParameter("depAccountNumber", depAccountNumber);

		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);

		@SuppressWarnings("unchecked")
		List<Object[]> listMain = query.getResultList();

		/* Segunda Lista con check other = 1 */
		/* PRIMERO BUSCAMOS CON MODIFICACION DE LA MONEDA DE COBRO */
		builder = new StringBuilder();
		/* 0 */
		builder.append(" SELECT CER.ID_REQ_COLLECTION_ECONOMIC_FK ");
		/* 1,2 */
		builder.append(" ,CER.ID_COLLECTION_ECONOMIC_PK,CER.ID_SECURITY_CODE_FK ");
		/* 3,4 */
		builder.append(" ,CER.ID_HOLDER_ACCOUNT_PK,CER.ACCOUNT_NUMBER ");
		/* 5,6 */
		builder.append(" ,CER.STATE,CER.CHECK_OTHER ");
		/* 7,8 */
		builder.append(" ,CER.CURRENCY,CER.AMOUNT_RECEIVABLE ");
		/* 9,10 */
		builder.append(" ,CER.ANOTHER_CURENCY,CER.ANOTHER_AMOUNT_RECEIVABLE ");
		/* 11,12 */
		builder.append(" ,CER.ID_BANK_FK,CER.DEP_ACCOUNT_NUMBER ");
		/* 13,14 */
		builder.append(" ,CER.COUPON_NUMBER, CER.AMOUNT_SECURITIES ");
		/* 15,16 */
		builder.append(" ,TRUNC(CER.PAYMENT_DATE),TRUNC(CER.DUE_DATE) ");
		/* 17 */
		builder.append(" ,CER.ALTERNATE_CODE ");
		/*18*/
		builder.append(" ,CER.ID_HOLDER_PK ");

		builder.append(" FROM COLLECTION_ECONOMIC_RIGHT CER,SECURITY SE ");
		builder.append(" WHERE 1 = 1 ");
		builder.append(" AND CER.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");

		builder.append(" AND CER.CHECK_OTHER = 1 ");// CON CHECK EN OTRA MONEDA DE COBRO

		if (Validations.validateIsNotNullAndNotEmpty(securityClass))
			builder.append(" AND SE.SECURITY_CLASS = :securityClass ");

		if (Validations.validateIsNotNullAndNotEmpty(securityClass))
			if (securityClass.equals(SecurityClassType.DPF.getCode())) {
				if (Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent)) {
					builder.append(" AND ( ");
					builder.append(
							" (SELECT SE.PAYMENT_AGENT  FROM SECURITY SE WHERE SE.ID_SECURITY_CODE_PK =  CER.ID_SECURITY_CODE_FK) = :mNemonicPaymentAgent ");
					builder.append(" OR ");
					builder.append(
							" (SELECT ISS.MNEMONIC  FROM ISSUER ISS  WHERE ISS.ID_ISSUER_PK = SE.ID_ISSUER_FK) = :mNemonicPaymentAgent ");
					builder.append(" ) ");
				}
			} else {
				if (Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent))
					builder.append(
							" AND (SELECT SE.PAYMENT_AGENT  FROM SECURITY SE WHERE SE.ID_SECURITY_CODE_PK =  CER.ID_SECURITY_CODE_FK) = :mNemonicPaymentAgent ");
			}

		if (Validations.validateIsNotNullAndNotEmpty(stateCollection))
			builder.append(" AND CER.STATE = :stateCollection ");

		if (Validations.validateIsNotNullAndNotEmpty(idIssuerPk))
			builder.append(" AND SE.ID_ISSUER_FK = :idIssuerPk ");

		if (Validations.validateIsNotNullAndNotEmpty(currencyType))
			builder.append(" AND CER.ANOTHER_CURENCY = :currencyType");

		if (Validations.validateIsNotNullAndNotEmpty(idHolderPk))
			builder.append(" AND CER.ID_HOLDER_PK = :idHolderPk ");

		if (Validations.validateIsNotNullAndNotEmpty(idBankPk))
			builder.append(" AND CER.ID_BANK_FK = :idBankPk ");

		if (Validations.validateIsNotNullAndNotEmpty(depAccountNumber))
			builder.append(" AND CER.DEP_ACCOUNT_NUMBER = :depAccountNumber ");

		builder.append(" AND TRUNC(CER.PAYMENT_DATE) BETWEEN :paymentDate and :nextBusinessDay ");
		builder.append(" AND CER.GLOSA_NOT_CLASSIFIED IS NULL ");
		builder.append(" AND CER.ID_REQ_COLLECTION_ECONOMIC_FK IN ( ");

		builder.append(" SELECT RCER.ID_REQ_COLLECTION_ECONOMIC_PK ");
		builder.append(" FROM REQ_COLLECTION_ECONOMIC_RIGHT RCER ");
		builder.append(" WHERE 1 = 1  ");

		if (Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
			builder.append(" AND RCER.ID_PARTICIPANT_FK = :idParticipantPk ");

		if (Validations.validateIsNotNullAndNotEmpty(stateReqCollection))
			builder.append(" AND RCER.STATE = :stateReqCollection ");

		builder.append(" )");

		query = em.createNativeQuery(builder.toString());

		if (Validations.validateIsNotNullAndNotEmpty(stateReqCollection))
			query.setParameter("stateReqCollection", stateReqCollection);

		if (Validations.validateIsNotNullAndNotEmpty(stateCollection))
			query.setParameter("stateCollection", stateCollection);

		if (Validations.validateIsNotNullAndNotEmpty(idIssuerPk))
			query.setParameter("idIssuerPk", idIssuerPk);

		if (Validations.validateIsNotNullAndNotEmpty(securityClass))
			query.setParameter("securityClass", securityClass);

		if (Validations.validateIsNotNullAndNotEmpty(mNemonicPaymentAgent))
			query.setParameter("mNemonicPaymentAgent", mNemonicPaymentAgent);

		if (Validations.validateIsNotNullAndNotEmpty(currencyType))
			query.setParameter("currencyType", currencyType);

		if (Validations.validateIsNotNullAndNotEmpty(idHolderPk))
			query.setParameter("idHolderPk", idHolderPk);

		if (Validations.validateIsNotNullAndNotEmpty(idParticipantPk))
			query.setParameter("idParticipantPk", idParticipantPk);

		if (Validations.validateIsNotNullAndNotEmpty(idBankPk))
			query.setParameter("idBankPk", idBankPk);

		if (Validations.validateIsNotNullAndNotEmpty(depAccountNumber))
			query.setParameter("depAccountNumber", depAccountNumber);

		query.setParameter("paymentDate", paymentDate);
		query.setParameter("nextBusinessDay", nextBusinessDay);

		@SuppressWarnings("unchecked")
		List<Object[]> listAux = query.getResultList();

		listMain.addAll(listAux);

		CollectionEconomicRigth cer = null;
		List<CollectionEconomicRigth> listCollectionEconomic = new ArrayList<>();
		for (Object[] objects : listMain) {
			cer = new CollectionEconomicRigth();
			/* Id de solicitud de cobro */
			cer.setIdReqCollectionEconomicFk(new BigDecimal(objects[0].toString()).longValue());
			/* Id de cobro de derecho economico */
			cer.setIdCollectionEconomicPk(new BigDecimal(objects[1].toString()).longValue());
			/* Codigo del valor */
			cer.setIdSecurityCodeFk(objects[2].toString());
			/* id cuenta titular */
			cer.setIdHolderAccountPk(new BigDecimal(objects[3].toString()).longValue());
			/* Numero de cuenta */
			cer.setAccountNumber(new BigDecimal(objects[4].toString()).intValue());
			/* Estado */
			cer.setState(new BigDecimal(objects[5].toString()).intValue());
			/* Check Other */
			cer.setCheckOther(objects[6] != null ? new BigDecimal(objects[6].toString()).intValue() : 0);
			/* Moneda */
			cer.setCurrency(new BigDecimal(objects[7].toString()).intValue());
			/* Monto a cobrar */
			cer.setAmountReceivable(new BigDecimal(objects[8].toString()));
			/* Otra moneda de cobro */
			cer.setAnotherCurency(objects[9] != null ? new BigDecimal(objects[9].toString()).intValue() : 0);
			/* Monto a cobrar otra */
			cer.setAnotherAmountReceivable(
					objects[10] != null ? new BigDecimal(objects[10].toString()) : BigDecimal.ZERO);
			/* Id bank */
			cer.setIdBankFk(new BigDecimal(objects[11].toString()).longValue());
			/* Dep account number */
			cer.setDepAccountNumber(objects[12].toString());
			/* Numero de cupon */
			cer.setCouponNumber(objects[13] != null ? new BigDecimal(objects[13].toString()).intValue() : 0);
			/* Cantidad de valores */
			cer.setAmountSecurities(objects[14] != null ? new BigDecimal(objects[14].toString()) : BigDecimal.ZERO);
			/* Fecha de pago */
			cer.setPaymentDate(CommonsUtilities.convertObjectToDate(objects[15]));
			/* Fecha de expiracion */
			cer.setDueDate(CommonsUtilities.convertObjectToDate(objects[16]));
			/** Codigo alterno */
			cer.setAlternateCode(objects[17]==null?CommonsUtilities.STR_BLANK:objects[17].toString());
			/*idHolderPk*/
			cer.setIdHolderPk(objects[18] != null ? new BigDecimal(objects[18].toString()).longValue() : null);
			listCollectionEconomic.add(cer);
		}
		return listCollectionEconomic;
	}

	/** Lista de valores registrados */

	/**
	 * Buscamo la amortizacion si es que existiera
	 * 
	 * @param resultTO datos del
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private SearchRegisterResultTO searchAmortizationObject(SearchRegisterResultTO resultTO, Date initialDate,
			Date finalDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" SELECT ");
		queryBuilder.append(" TO_CHAR(PAC.EXPRITATION_DATE, 'DD/MM/YYYY') EXPIRATION_DATE, ");
		queryBuilder.append(" TO_CHAR(PAC.PAYMENT_DATE, 'DD/MM/YYYY') PAYMENT_DATE, ");
		queryBuilder.append(" HAB.TOTAL_BALANCE, ");
		queryBuilder.append(" HAB.AVAILABLE_BALANCE, ");
		queryBuilder.append(" CASE ");
		queryBuilder.append(" WHEN S.IND_PREPAID = 1 ");
		queryBuilder.append(" THEN (ROUND(NVL(S.INITIAL_NOMINAL_VALUE,0),2))  ");
		queryBuilder.append(" ELSE (ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) ");
		queryBuilder.append(" END INITIAL_NOMINAL_VALUE, ");

		queryBuilder.append(" CASE ");
		queryBuilder.append(" WHEN S.IND_PREPAID = 1 ");
		queryBuilder.append(" THEN ((ROUND(NVL(S.INITIAL_NOMINAL_VALUE,0),2)) * HAB.TOTAL_BALANCE) ");
		queryBuilder.append(" ELSE ((ROUND(NVL(PAC.COUPON_AMOUNT,0),2)) * HAB.TOTAL_BALANCE) ");
		queryBuilder.append(" END MONTO ");

		queryBuilder.append(" FROM SECURITY S ");

		queryBuilder.append(" INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(
				" INNER JOIN HOLDER_ACCOUNT_BALANCE hab         on s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK ");
		queryBuilder.append(
				" INNER JOIN holder_account ha                  on hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(
				" INNER JOIN amortization_payment_schedule APS  ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk ");
		queryBuilder.append(
				" INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
		queryBuilder.append(" AND I.ID_ISSUER_PK = :idIssuerPk ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER = :accountNumber");
		queryBuilder.append(" AND S.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
		queryBuilder.append(" AND TRUNC(PAC.PAYMENT_DATE) BETWEEN :initialDate AND :finalDate ");

		Query query = em.createNativeQuery(queryBuilder.toString());

		query.setParameter("idIssuerPk", resultTO.getIdIssuerPK());
		query.setParameter("accountNumber", resultTO.getAccountNumber());
		query.setParameter("idSecurityCodePk", resultTO.getIdSecurityCodePk());
		query.setParameter("initialDate", initialDate);
		query.setParameter("finalDate", finalDate);

		Object[] resultObject;
		try {
			resultObject = (Object[]) query.getSingleResult();
		} catch (NoResultException e) {
			// e.printStackTrace();
			SearchRegisterResultTO registerResultTO = new SearchRegisterResultTO();
			registerResultTO.setAmortization(BigDecimal.ZERO);
			return registerResultTO;
		}
		SearchRegisterResultTO result;
		result = new SearchRegisterResultTO();
		result.setDueDate(resultObject[0] == null ? "" : resultObject[0].toString());
		result.setPaymentDate(CommonsUtilities.convertStringtoDate(resultObject[1].toString()));
		result.setTotalBalance(new BigDecimal(resultObject[2].toString()));
		result.setAvailableBalance(
				resultObject[3] == null ? BigDecimal.ZERO : new BigDecimal(resultObject[3].toString()));
		result.setInitialNominalValue(
				resultObject[4] == null ? BigDecimal.ZERO : new BigDecimal(resultObject[4].toString()));
		result.setTotalAmount(resultObject[5] == null ? BigDecimal.ZERO : new BigDecimal(resultObject[5].toString()));
		return result;
	}

	/**
	 * Buscamo la amortizacion si es que existiera
	 * 
	 * @param resultTO datos del
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private InterestAndAmortizationTO searchInterestObject(SearchRegisterResultTO resultTO, Date initialDate,
			Date finalDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" SELECT ");
		queryBuilder.append(
				" SUM(CASE   WHEN SE.IND_PREPAID = 1  THEN ROUND(ROUND(SE.INITIAL_NOMINAL_VALUE, 2)*HAB.TOTAL_BALANCE,  2) ");
		queryBuilder.append(" ELSE ROUND(ROUND(PIC.COUPON_AMOUNT, 2)*HAB.TOTAL_BALANCE,  2) ");
		queryBuilder.append(" END) COUPON_AMOUNT ");

		queryBuilder.append(" FROM HOLDER_ACCOUNT_BALANCE HAB ");
		queryBuilder.append(
				" INNER JOIN SECURITY SE                        ON SE.ID_SECURITY_CODE_PK=HAB.ID_SECURITY_CODE_PK ");
		queryBuilder.append(
				" INNER JOIN HOLDER_ACCOUNT HA                  ON HAB.ID_HOLDER_ACCOUNT_PK= HA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" INNER JOIN ISSUER I                           ON SE.ID_ISSUER_FK = I.ID_ISSUER_PK ");
		queryBuilder.append(
				" INNER JOIN INTEREST_PAYMENT_SCHEDULE IPS      ON SE.ID_SECURITY_CODE_PK=IPS.ID_SECURITY_CODE_FK ");
		queryBuilder.append(
				" INNER JOIN PROGRAM_INTEREST_COUPON PIC        ON IPS.ID_INT_PAYMENT_SCHEDULE_PK=PIC.ID_INT_PAYMENT_SCHEDULE_FK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER IN ( ");

		queryBuilder.append(" SELECT HAA.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT_DETAIL HAD ");
		queryBuilder.append(" INNER JOIN HOLDER_ACCOUNT HAA ON HAD.ID_HOLDER_ACCOUNT_FK = HAA.ID_HOLDER_ACCOUNT_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" and HAA.ID_PARTICIPANT_FK = :idParticipantPk ");
		queryBuilder.append(" and had.ID_HOLDER_FK in (" + CUIS + ") ");

		queryBuilder.append(" ) ");

		queryBuilder.append(" ) ");
		queryBuilder.append(" AND HAB.AVAILABLE_BALANCE > 0 ");
		queryBuilder.append(" AND SE.STATE_SECURITY = " + SecurityStateType.REGISTERED.getCode());
		queryBuilder.append(" AND SE.INSTRUMENT_TYPE = " + InstrumentType.FIXED_INCOME.getCode());
		queryBuilder.append(" AND PIC.COUPON_AMOUNT >  0 ");
		/** FILTROS DE BUSQUEDA */
		queryBuilder.append(" AND I.ID_ISSUER_PK = :idIssuerPk ");
		queryBuilder.append(" AND HA.ACCOUNT_NUMBER = :accountNumber");
		queryBuilder.append(" AND SE.ID_SECURITY_CODE_PK = :idSecurityCodePk ");
		queryBuilder.append(" AND TRUNC(PIC.PAYMENT_DATE) BETWEEN :initialDate AND :finalDate ");

		Query query = em.createNativeQuery(queryBuilder.toString());

		query.setParameter("idIssuerPk", resultTO.getIdIssuerPK());
		query.setParameter("accountNumber", resultTO.getAccountNumber());
		query.setParameter("idSecurityCodePk", resultTO.getIdSecurityCodePk());
		query.setParameter("initialDate", initialDate);
		query.setParameter("finalDate", finalDate);

		Object resultObject = null;
		InterestAndAmortizationTO amortizationTO;
		try {
			resultObject = (Object) query.getSingleResult();
		} catch (NoResultException e) {
			// e.printStackTrace();
			amortizationTO = new InterestAndAmortizationTO();
			amortizationTO.setInterest(BigDecimal.ZERO);
			amortizationTO.setAmortization(BigDecimal.ZERO);
		}
		amortizationTO = new InterestAndAmortizationTO();
		amortizationTO.setInterest(resultObject == null ? BigDecimal.ZERO : new BigDecimal(resultObject.toString()));
		return amortizationTO;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveCollectionEconomicRigth(CollectionEconomicRigth collectionEconomicRigth) {
		em.persist(collectionEconomicRigth);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveModfCollectionEconomicRigth(ModfCollEcoRigth modfCollEconomicRigth) {
		em.merge(modfCollEconomicRigth);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveModficationCollectionEconomicRigth(CollectionEconomicRigth cer) {
		em.merge(cer);
		em.flush();
	}

	/**
	 * Guardamos los datos de monto, glosa, participante
	 * 
	 * @throws UnknownHostException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public synchronized void registerNotificationDetail(DetailCollectionGlossTO detailCollectionGlossTO,
			ProcessLogger processLogger) throws UnknownHostException {

		log.info("CDE::::: Persistiendo DetailCollectionEconomic para la glosa:"
				+ detailCollectionGlossTO.getGlosaNotClassified());

		DetailCollectionGloss collectionGloss = new DetailCollectionGloss();
		/** Participante el cual tiene el cobro */
		if (detailCollectionGlossTO.getIdParticipantPk() != null)
			collectionGloss.setIdParticipantPk(detailCollectionGlossTO.getIdParticipantPk());
		/** Glosa */
		if (detailCollectionGlossTO.getGlosaNotClassified() != null)
			collectionGloss.setGlosaNotClassified(detailCollectionGlossTO.getGlosaNotClassified());
		/** Monto a cobrar */
		if (detailCollectionGlossTO.getAmountReceivable() != null)
			collectionGloss.setAmountReceivable(detailCollectionGlossTO.getAmountReceivable());

		collectionGloss.setAmountDeposited(BigDecimal.ZERO);
		collectionGloss.setAmountTransferred(BigDecimal.ZERO);

		/** Moneda */
		if (detailCollectionGlossTO.getCurrency() != null)
			collectionGloss.setCurrency(detailCollectionGlossTO.getCurrency());
		/** Fecha de registro */
		collectionGloss.setRegistreDate(CommonsUtilities.currentDateTime());

		/** listas de id de los cat que fueron enviados a notificacion */
		if (detailCollectionGlossTO.getArrayStringIdCatSender() != null)
			collectionGloss.setIdCatListaText(detailCollectionGlossTO.getArrayStringIdCatSender());

		if (detailCollectionGlossTO.getPaymentDate() != null)
			collectionGloss.setPaymentDate(detailCollectionGlossTO.getPaymentDate());

		if (detailCollectionGlossTO.getIdIssuerPk() != null)
			collectionGloss.setIdIssuerFk(detailCollectionGlossTO.getIdIssuerPk());

		/** Id del agente pagador */
		if (detailCollectionGlossTO.getIdPaymentAgentFk() != null)
			collectionGloss.setIdPaymentAgentFk(detailCollectionGlossTO.getIdPaymentAgentFk());
		/** Clase de Valor */
		if (detailCollectionGlossTO.getSecurityClass() != null)
			collectionGloss.setSecurityClass(detailCollectionGlossTO.getSecurityClass());

		/** campos de auditoria */
		collectionGloss.setLastModifyApp(processLogger.getLastModifyApp());
		collectionGloss.setTransferStatus(null);
		collectionGloss.setRegistreDate(CommonsUtilities.currentDate());
		collectionGloss.setLastModifyUser(processLogger.getLastModifyUser());
		collectionGloss.setLastModifyIp(processLogger.getLastModifyIp());
		collectionGloss.setLastModifyDate(CommonsUtilities.currentDate());

		if (detailCollectionGlossTO.getIdHolderPk() != null)
			collectionGloss.setIdHolderFk(detailCollectionGlossTO.getIdHolderPk());
		em.persist(collectionGloss);
		em.flush();

		// registrando la tabla hija: GlossNotificationDetail
		for (GlossNoticationDetail glossNoticationDetail : detailCollectionGlossTO.getLstGlossNoticationDetail()) {
			// seteando idFkGlossCode (clave foranea)
			glossNoticationDetail.setIdFkGlossCode(collectionGloss);
			// pistas de auditoria
			glossNoticationDetail.setLastModifyApp(processLogger.getLastModifyApp().longValue());
			glossNoticationDetail.setRegistreDate(CommonsUtilities.currentDate());
			glossNoticationDetail.setLastModifyUser(processLogger.getLastModifyUser());
			glossNoticationDetail.setLastModifyIp(processLogger.getLastModifyIp());
			glossNoticationDetail.setLastModifyDate(CommonsUtilities.currentDate());

			em.persist(glossNoticationDetail);
			em.flush();
		}
	}

	/**
	 * Verificamos si existe registros
	 * 
	 * @param resultTO resultado de la busqueda
	 * @return true o false si existe el registro del cobro
	 **/
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean isExistsEconomicRight(SearchRegisterResultTO resultTO) {

		// obtencion del CUI
		
		Long cuiTarget = this.extractHolder(resultTO.getAlternateCode());
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer.idCollectionEconomicPk from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		if (resultTO.getDueDate() != null)
			queryBuilder.append(" and cer.dueDate = :dueDate ");
		queryBuilder.append(" and cer.idSecurityCodeFk = :idSecurityCodeFk ");
		queryBuilder.append(" and cer.alternateCode = :alternateCode ");
		queryBuilder.append(" and cer.idHolderAccountPk = :idHolderAccountPk ");
		queryBuilder.append(" and cer.accountNumber = :accountNumber ");
		queryBuilder.append(" and cer.idHolderPk = :cuiTarget ");
		queryBuilder.append(" and cer.paymentDate = :paymentDate ");
//        queryBuilder.append(" and cer.couponNumber = :couponNumber ");
		/* Indicador de si el valor ya ha sido registrado */
		queryBuilder.append(" and cer.notificationReading is null ");
		queryBuilder.append(" and cer.state = " + StatesConstants.REGISTERED_COLLECTION_ECONOMIC);

		Query query = em.createQuery(queryBuilder.toString());

		if (resultTO.getDueDate() != null)
			query.setParameter("dueDate", CommonsUtilities.convertStringtoDate(resultTO.getDueDate()));
		query.setParameter("idSecurityCodeFk", resultTO.getIdSecurityCodePk());
		query.setParameter("alternateCode", resultTO.getAlternateCode());
		query.setParameter("idHolderAccountPk", resultTO.getIdHolderAccountPk());
		query.setParameter("accountNumber", resultTO.getAccountNumber());
		query.setParameter("cuiTarget", cuiTarget);
		query.setParameter("paymentDate", resultTO.getPaymentDate());
		try {
			ArrayList<BigDecimal> arr = (ArrayList<BigDecimal>) query.getResultList();
			if (arr != null && arr.size() > 0)
				return true;
			else
				return false;
		} catch (NoResultException e) {
			// e.printStackTrace();
			return false;
		}
	}


	@SuppressWarnings("unchecked")
	public List<DigitalSignature> getListDigitalSignatureBlob(Integer typeSignature) throws ServiceException {
		List<DigitalSignature> lstDigitalSignature = new ArrayList<DigitalSignature>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select distinct ds from DigitalSignature ds ");
			sbQuery.append(
					" where ds.typeSignature = :typeSignature and ds.activeSignature = " + BooleanType.YES.getCode());
			sbQuery.append(" order by ds. idDigitalSignaturePk desc ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("typeSignature", typeSignature);
			lstDigitalSignature = (List<DigitalSignature>) query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return lstDigitalSignature;
	}

	/**
	 * Buscamos el certificado de accredtitacion generado CAT
	 * 
	 * @param certificationId Id del certificado registrado
	 * @return archivo en formato PDF y en array de bytes
	 */
	public byte[] getAccreditationCertification(Long certificationId) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT AO.certificationFile ");
		sbQuery.append(" FROM AccreditationOperation AO ");
		sbQuery.append(" WHERE 1 = 1 ");
		sbQuery.append(" and AO.idAccreditationOperationPk =:certificationId ");
		sbQuery.append(" and AO.certificationFile is not null ");
		// sbQuery.append(" and AO.accreditationState = :state ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("certificationId", certificationId);
		try {
			return (byte[]) query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CollectionEconomicRigth getCollectionEconomicRigth(Long idCollectionEconomicPk, Date paymentDate,
			String glosa) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select cer from CollectionEconomicRigth cer ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and cer.idCollectionEconomicPk = :idCollectionEconomicPk ");
		queryBuilder.append(" and cer.paymentDate = :paymentDate ");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("idCollectionEconomicPk", idCollectionEconomicPk);
		query.setParameter("paymentDate", paymentDate);

		CollectionEconomicRigth collectionEconomicRigth = null;
		try {
			collectionEconomicRigth = (CollectionEconomicRigth) query.getSingleResult();
			return collectionEconomicRigth;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Correlativo de glosa
	 * 
	 * @param idParticipantPk
	 * @return retorna el id correlativo de la glosa para las notificaciones
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long getCorrelativeGloss(Long idParticipantPk) {
		final String SEQUENCE = "SQ_CORRELATIVE_OF_GLOSS";
		return super.getNextCorrelativeOperations(SEQUENCE);
	}

	public String descHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		if (holderAccountBalance.getTransitBalance().compareTo(BigDecimal.ZERO) == 1)
			return "TRANSITO";
		if (holderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO) == 1)
			return "PRENDA";
		if (holderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) == 1)
			return "PROHIBIDO";
		if (holderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO) == 1)
			return "OTRO BLOQUEO";
		if (holderAccountBalance.getSaleBalance().compareTo(BigDecimal.ZERO) == 1)
			return "VENTA";
		if (holderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO) == 1)
			return "REPORTADO";
		if (holderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO) == 1)
			return "REPORTANDO";
		if (holderAccountBalance.getMarginBalance().compareTo(BigDecimal.ZERO) == 1)
			return "MARGINADO";
		if (holderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO) == 1)
			return "ACREDITADO";
		if (holderAccountBalance.getBorrowerBalance().compareTo(BigDecimal.ZERO) == 1)
			return "PRESTARIO";
		if (holderAccountBalance.getLenderBalance().compareTo(BigDecimal.ZERO) == 1)
			return "PRESTADOR";
		if (holderAccountBalance.getReserveBalance().compareTo(BigDecimal.ZERO) == 1)
			return "RESERVADO";
		if (holderAccountBalance.getLoanableBalance().compareTo(BigDecimal.ZERO) == 1)
			return "PRESTABLE";
		if (holderAccountBalance.getOppositionBalance().compareTo(BigDecimal.ZERO) == 1)
			return "OPOSICION";
		/*
		 * if(holderAccountBalance.get.compareTo(BigDecimal.ZERO)==0) return
		 * "CONCEDIDO";
		 * if(holderAccountBalance.getGrantBalance().compareTo(BigDecimal.ZERO)==0)
		 * return "CONCEDER";
		 */
		else
			return null;
	}

	/**
	 * **/
	public String descHolderAccountBalance(SearchRegisterResultTO registerResultTO) {
		if (registerResultTO.getTransitBalance().compareTo(BigDecimal.ZERO) == 0)
			return "TRANSITO";
		if (registerResultTO.getPawnBalance().compareTo(BigDecimal.ZERO) == 0)
			return "PRENDA";
		if (registerResultTO.getBanBalance().compareTo(BigDecimal.ZERO) == 0)
			return "PROHIBIDO";
		if (registerResultTO.getOtherBlockBalance().compareTo(BigDecimal.ZERO) == 0)
			return "OTRO BLOQUEO";
		if (registerResultTO.getSaleBalance().compareTo(BigDecimal.ZERO) == 0)
			return "VENTA";
		if (registerResultTO.getReportedBalance().compareTo(BigDecimal.ZERO) == 0)
			return "REPORTADO";
		if (registerResultTO.getReportingBalance().compareTo(BigDecimal.ZERO) == 0)
			return "REPORTANDO";
		if (registerResultTO.getMarginBalance().compareTo(BigDecimal.ZERO) == 0)
			return "MARGINADO";
		if (registerResultTO.getAcreditationBalance().compareTo(BigDecimal.ZERO) == 0)
			return "ACREDITADO";
		if (registerResultTO.getBorrowerBalance().compareTo(BigDecimal.ZERO) == 0)
			return "PRESTARIO";
		if (registerResultTO.getLenderBalance().compareTo(BigDecimal.ZERO) == 0)
			return "PRESTADOR";
		if (registerResultTO.getReserveBalance().compareTo(BigDecimal.ZERO) == 0)
			return "RESERVADO";
		if (registerResultTO.getLoanableBalance().compareTo(BigDecimal.ZERO) == 0)
			return "PRESTABLE";
		if (registerResultTO.getOpositionBalance().compareTo(BigDecimal.ZERO) == 0)
			return "OPOSICION";
		if (registerResultTO.getGrantedBalance().compareTo(BigDecimal.ZERO) == 0)
			return "CONCEDIDO";
		if (registerResultTO.getGrantBalance().compareTo(BigDecimal.ZERO) == 0)
			return "CONCEDER";
		else
			return "DESCONOCIDO";
	}

	/**
	 * Busqueda de todos lo bancos que se encuentran en los resultados de la
	 * busqueda de cobros
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> seachAllBanksAccounts(List<Long> listIdHoldersPks) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" SELECT ");
		queryBuilder.append(" DISTINCT(BBA.ID_BANK_FK),B.DESCRIPTION  ");
		queryBuilder.append(
				" ,BBA.CURRENCY ,(select text1 from parameter_table where parameter_table_pk = BBA.CURRENCY ) as DESC_CU ");
		queryBuilder.append(
				" ,LISTAGG(BBA.ID_BEN_BANK_ACCOUNT_PK, ',') WITHIN GROUP  (ORDER BY BBA.ID_BEN_BANK_ACCOUNT_PK desc)  as ID_BEN_ACCO  ");
		queryBuilder.append(" FROM BENEFICIARY_BANK_ACCOUNT BBA ");
		queryBuilder.append(" LEFT JOIN BANK B ON BBA.ID_BANK_FK =  B.ID_BANK_PK ");
		queryBuilder.append(" WHERE 1 = 1 ");
		queryBuilder.append(" AND BBA.ID_HOLDER_FK IN ( :listIdHoldersPks ) ");
		queryBuilder.append(" AND BBA.STATE = " + StatesConstants.CONFIRMED_BANK_ACCOUNT); // 2382
		queryBuilder.append(" GROUP BY BBA.ID_BANK_FK,BBA.CURRENCY, B.DESCRIPTION ");

		Query query = em.createNativeQuery(queryBuilder.toString());

		query.setParameter("listIdHoldersPks", listIdHoldersPks);

		return query.getResultList();
	}

	/** Buscamos la lista de monedas de pago */
	public List<ParameterTable> getCurrencyListPay() throws Exception {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select parameter_table_pk,text1,description from parameter_table ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and master_table_fk = " + MasterTableType.CURRENCY.getCode().toString());
		queryBuilder.append(" and parameter_table_pk  in (");
		queryBuilder.append(CurrencyType.PYG.getCode().toString() + ",");
		queryBuilder.append(CurrencyType.USD.getCode().toString() + ")");

		Query query = em.createNativeQuery(queryBuilder.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.getResultList();
		if (list == null)
			return null;
		List<ParameterTable> listParameterTables = new ArrayList<ParameterTable>();
		ParameterTable parameterTable;
		int i = 0;
		for (Object[] objectParameter : list) {
			parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk(Integer.parseInt(objectParameter[0].toString()));
			parameterTable.setText1(objectParameter[1].toString());
			parameterTable.setDescription(objectParameter[2].toString());
			listParameterTables.add(i, parameterTable);
			i++;
		}
		return listParameterTables;
	}

	public List<Object[]> getListDetailCollection(Date operationDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select dcg.glosaNotClassified,dcg.amountReceivable from DetailCollectionGloss dcg ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and dcg.depositDate is null ");
		queryBuilder.append(" and dcg.retirementDate is null ");
		queryBuilder.append(" and dcg.transferStatus is null ");
		queryBuilder.append(" and dcg.paymentDate = :operationDate ");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("operationDate", operationDate);

		List<Object[]> list = query.getResultList();
		return list;
	}

	public List<Object[]> getListFundCollection(Date operationDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select fc.gloss,sum(fc.amount) from FundsCollection fc ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and fc.operationState is null ");
		queryBuilder.append(" and fc.dateOperation = :operationDate ");
		queryBuilder.append(" group by fc.gloss ");

		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("operationDate", operationDate);

		List<Object[]> list = query.getResultList();
		return list;
	}

	// TODO:
	private boolean containsKey(HashMap<ObjectCollection, SearchRegisterResultTO> mapSecuritiesToPay,
			ObjectCollection objCollection) {
		boolean sw = false;
		for (Map.Entry<ObjectCollection, SearchRegisterResultTO> entry : mapSecuritiesToPay.entrySet()) {
			ObjectCollection keyValue = entry.getKey();
			if (objCollection.getIdParticipantPk().equals(keyValue.getIdParticipantPk())
					&& objCollection.getIdSecurityCodePk().equals(keyValue.getIdSecurityCodePk())
					&& objCollection.getAlternativeCode().equals(keyValue.getAlternativeCode())
					&& objCollection.getPaymentDate().equals(keyValue.getPaymentDate())
					&& objCollection.getCurrency().equals(keyValue.getCurrency())
					&& objCollection.getExpirationDate().equals(keyValue.getExpirationDate()))
				sw = true;
		}
		return sw;
	}

	/**
	 * Buscamos el detalle de la glosa segun glosa de notificaicon
	 */
	public DetailCollectionGloss searchDepositNotification(String gloss, Date operationDate) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" select dcg from DetailCollectionGloss dcg ");
		queryBuilder.append(" where 1 = 1 ");
		queryBuilder.append(" and dcg.depositDate is null ");
		queryBuilder.append(" and dcg.retirementDate is null ");
		queryBuilder.append(" and dcg.transferStatus is null ");
		queryBuilder.append(" and dcg.glosaNotClassified = :gloss ");

		if (operationDate != null)
			queryBuilder.append(" and dcg.paymentDate = :operationDate ");

		queryBuilder.append(" order by dcg.idDetailCollectionGlossPk desc ");
		Query query = em.createQuery(queryBuilder.toString());
		query.setParameter("gloss", gloss);
		if (operationDate != null)
			query.setParameter("operationDate", operationDate);
		try {
			DetailCollectionGloss collectionGloss = (DetailCollectionGloss) query.getSingleResult();
			return collectionGloss;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	/***
	 * Actualizando los registros que se encuentran correctamente
	 **/
	public void updateCollections(String gloss,Integer state) {
		List<FundsCollection> list = this.searchFundsCollection(gloss);
		for (FundsCollection fundsCollection : list) {
			fundsCollection.setOperationState(state);
			em.merge(fundsCollection);
			em.flush();
		}
	}

	/**
	 * Buscamos fondos que no esten en ningun registro
	 * 
	 */
	public List<FundsCollection> searchFundsCollection(String gloss) {
		StringBuilder builder = new StringBuilder();
		builder.append(" select f  from FundsCollection f ");
		builder.append(" where 1 = 1 ");
		builder.append(" and f.gloss = :gloss ");
		builder.append(" and f.operationState is null");
		Query query = em.createQuery(builder.toString());
		query.setParameter("gloss", gloss);
		List<FundsCollection> list = query.getResultList();
		return list;
	}

	class ObjectCollection {

		private Long idParticipantPk;

		private String idSecurityCodePk;

		private String alternativeCode;

		private Integer currency;

		private String paymentDate;

		private String expirationDate;

		public String getIdSecurityCodePk() {
			return idSecurityCodePk;
		}

		public void setIdSecurityCodePk(String idSecurityCodePk) {
			this.idSecurityCodePk = idSecurityCodePk;
		}

		public String getPaymentDate() {
			return paymentDate;
		}

		public void setPaymentDate(String paymentDate) {
			this.paymentDate = paymentDate;
		}

		public Long getIdParticipantPk() {
			return idParticipantPk;
		}

		public void setIdParticipantPk(Long idParticipantPk) {
			this.idParticipantPk = idParticipantPk;
		}

		public String getAlternativeCode() {
			return alternativeCode;
		}

		public void setAlternativeCode(String alternativeCode) {
			this.alternativeCode = alternativeCode;
		}

		public Integer getCurrency() {
			return currency;
		}

		public void setCurrency(Integer currency) {
			this.currency = currency;
		}

		public String getExpirationDate() {
			return expirationDate;
		}

		public void setExpirationDate(String expirationDate) {
			this.expirationDate = expirationDate;
		}

	}
}
