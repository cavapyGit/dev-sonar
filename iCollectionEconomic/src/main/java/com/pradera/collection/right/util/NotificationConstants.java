package com.pradera.collection.right.util;

public class NotificationConstants {

	public static final String SUBJECT_EMAIL_ACCUSE_PAYMENT = "Acuse de Pago";
	public static final String SUBJECT_EMAIL_PAYMENT_AGENT = "Cobro de Derechos Economicos";

	public static final String DRA_PREVISION = "Dra. Roxana Gentile Rojas";
	public static final String DRA_FUTURO = "Dra. Rebeca Mendoza Gallardo";

	public static final String SECURITIES_SINGULAR = "del valor desmaterializado detallado";
	public static final String SECURITIES_PLURAL = "de los valores desmaterializados detallados";
	
	public static final Integer NO_RESENDER_NOTIFICATION = 0;
	public static final Integer RESENDER_NOTIFICATION = 1;
	
}
