package com.pradera.collection.right.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.to.ObjectRegModfCollectionResultTO;
import com.pradera.collection.right.to.RegisterCollectionRightTO;
import com.pradera.collection.right.to.SearchCollectionRightTO;
import com.pradera.collection.right.to.SearchRegisterResultTO;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.process.BusinessProcess;
import com.edv.collection.economic.rigth.ReqCollectionEconomicRight;

@DepositaryWebBean
@LoggerCreateBean
public class SearchCollectionRightMgmtBean extends GenericBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	@Inject
	private NotificationServiceFacade notificationServiceFacade;
	
	@Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	@Inject
	private UserInfo userInfo;
	
	@Inject
	private UserPrivilege userPrivilege;
	
	private SearchCollectionRightTO searchFilterTO;
	
	private List<Participant> listParticipants;
	
	private List<ParameterTable> listTypeIncome;
	
	private List<ParameterTable> listState;
	
	private List<ParameterTable>  listValidated;
	
	private ReqCollectionEconomicRight reqCollectionEconomicView;
	@Inject
	private CollectionRigthFacade facade;
	
	@Inject
	private PraderaLogger log;
	
	private GenericDataModel<ReqCollectionEconomicRight> reqCollectionEconomicRigths;
	
	/**
	 * Usado para la modificacion de la solicitud
	 * */
	private RegisterCollectionRightTO viewRegistreTO;
	private List<ParameterTable> lstCurrency;
	private List<ParameterTable>  lstSecurityClass;
	private GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs;
	private GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOsAux;
	private List<SearchRegisterResultTO> listResult;
	private boolean visibleResult;
	
	private List<ParameterTable> listCurrencyBobUsd = new ArrayList<>();
	
	@PostConstruct
	public void init(){
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterTO = new SearchCollectionRightTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterTO = new SearchCollectionRightTO();
		try {
			listParticipants = parameterServiceBean.getListParticipantForCollection();
			listTypeIncome =  new ArrayList<>();
			listTypeIncome.add(parameterServiceBean.getParameterTableById(InstrumentType.FIXED_INCOME.getCode()));
			listTypeIncome.add(parameterServiceBean.getParameterTableById(InstrumentType.VARIABLE_INCOME.getCode()));
			searchFilterTO.setTypeIncome(InstrumentType.FIXED_INCOME.getCode());
			ParameterTable parameterTable = new ParameterTable();
			listValidated = new ArrayList<>();
			parameterTable.setParameterTablePk(1);
			parameterTable.setDescription(BooleanType.YES.getValue());
			listValidated.add(parameterTable);
			parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk(2);
			parameterTable.setDescription(BooleanType.NO.getValue());
			listValidated.add(parameterTable);
			//listState = parameterServiceBean.getListParameterTableServiceBean(MasterTableType.STATE_COLLECTION_ECONOMIC.getCode());
			listState = parameterServiceBean.getListParameterTableServiceBean(1183);
			
			ParameterTable currencyBob= parameterServiceBean.find(ParameterTable.class, CurrencyType.PYG.getCode());//BOB
			ParameterTable currencyUsd= parameterServiceBean.find(ParameterTable.class, CurrencyType.USD.getCode());//USD
			
			listCurrencyBobUsd.add(currencyBob);
			listCurrencyBobUsd.add(currencyUsd);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		viewRegistreTO = new RegisterCollectionRightTO();
		viewRegistreTO.setMaxDate(holidayQueryServiceBean.getNextWorkingDayServiceBean(viewRegistreTO.getInitialDate()));
	}
	/**
	 * Cargamos el detalle de la solicitud de registro
	 * 
	 * **/
	public void loadDetailReqCollectionEconomic(ActionEvent event){
		reqCollectionEconomicView = new ReqCollectionEconomicRight();
		reqCollectionEconomicView = (ReqCollectionEconomicRight) event.getComponent().getAttributes().get("requestCollectionView");
		if(reqCollectionEconomicView.getState().equals(StatesConstants.MODIFIED_REQUEST_COLLECTION_ECONOMIC)
			||reqCollectionEconomicView.getState().equals(StatesConstants.REGISTERED_REQUEST_COLLECTION_ECONOMIC)
			||reqCollectionEconomicView.getState().equals(StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC)){
			loadDetailView(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
		}
		if(reqCollectionEconomicView.getState().equals(StatesConstants.REJECTED_REQUEST_COLLECTION_ECONOMIC)){
			loadDetailView(StatesConstants.REJECTED_COLLECTION_ECONOMIC);
		}
	}
	
	
	/**
	 * Racargamos las listas de Bancos segun la nueva moneda seleccionada en modificacion 
	 * 
	 * */
	public void currencyChangeEvent(ValueChangeEvent event){
		SearchRegisterResultTO registerResultTO = new SearchRegisterResultTO();
		registerResultTO = (SearchRegisterResultTO) event.getComponent().getAttributes().get("selectedObject");
		Integer currency = (Integer) event.getNewValue();
		List<Bank> listBankProvider;
		for (SearchRegisterResultTO mainObject : lisRegisterResultTOs) {
			if(mainObject.getIdCollectionEconomicPk().equals(registerResultTO.getIdCollectionEconomicPk())
					&&mainObject.getIdHolderAccountPk().equals(registerResultTO.getIdHolderAccountPk())
					&&mainObject.getIdSecurityCodePk().equals(registerResultTO.getIdSecurityCodePk())){
				mainObject.getCurrencyFilter().setCurrency(currency);
				listBankProvider = facade.reSearchBankProvider(mainObject.getIdParticipantPk(), mainObject.getAccountNumber(),currency);
				mainObject.getBankFilter().setListBankProvider(listBankProvider);
				mainObject.getBankFilter().setIdBankPkSelected(null);
				mainObject.getAccountFilter().setAccountList(null);
				mainObject.getAccountFilter().setAccountSelected(null);
			}
		}
	}
	/**
	 * Buscamos los nuevo bancos
	 * */
	public void bankChangeEvent(ValueChangeEvent event){
		SearchRegisterResultTO registerResultTO 	= new SearchRegisterResultTO();
		registerResultTO = (SearchRegisterResultTO) event.getComponent().getAttributes().get("selectedObject");
		Long idBankPk = (Long) event.getNewValue();
		List<String> accountList = null;
		for (SearchRegisterResultTO mainObject : lisRegisterResultTOs) {
			if(mainObject.getIdCollectionEconomicPk().equals(registerResultTO.getIdCollectionEconomicPk())
				&&mainObject.getIdHolderAccountPk().equals(registerResultTO.getIdHolderAccountPk())
				&&mainObject.getIdSecurityCodePk().equals(registerResultTO.getIdSecurityCodePk())){
				mainObject.getBankFilter().setIdBankPkSelected(idBankPk);
				if(mainObject.isCheckOther())
					accountList = facade.reSearchAccountNumber(mainObject.getIdParticipantPk(),mainObject.getAccountNumber(),idBankPk,mainObject.getCurrencyFilter().getCurrency());
				else
					accountList = facade.reSearchAccountNumber(mainObject.getIdParticipantPk(),mainObject.getAccountNumber(),idBankPk,mainObject.getCurrency());
				mainObject.getAccountFilter().setAccountList(accountList);
				mainObject.getAccountFilter().setAccountSelected(null);
			}
		}
	} 
	/**
	 * Cargando detalle de la solicitud de registro de un cobro de derecho economico
	 * La variable viewRegistreTO es donde se establecen los datos de los filtros de busqueda
	 * con los que se hicierono las busqueda de venvimientos
	 * */
	private void loadDetailView(Integer state){
		viewRegistreTO = new RegisterCollectionRightTO();
		List<SearchRegisterResultTO> lstAux=new ArrayList<>();
		try {
			viewRegistreTO.setIdParticipantPk(reqCollectionEconomicView.getParticipant().getIdParticipantPk());
			viewRegistreTO.setTypeIncome(reqCollectionEconomicView.getInstrumentType());
			if(viewRegistreTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				viewRegistreTO.setRenderedRV(BooleanType.YES.getBooleanValue());
			viewRegistreTO.setIssuer(new Issuer(reqCollectionEconomicView.getIdIssuerFk()));
			lstSecurityClass = facade.getSecurityClassList(reqCollectionEconomicView.getInstrumentType());
			viewRegistreTO.setSecurityClass(reqCollectionEconomicView.getSecurityClass());
			viewRegistreTO.setSecurity(new Security(reqCollectionEconomicView.getIdSecurityCodeFk()));
			lstCurrency =  parameterServiceBean.getListParameterTableServiceBean(MasterTableType.CURRENCY.getCode());
			viewRegistreTO.setCurrency(reqCollectionEconomicView.getCurrency());
			viewRegistreTO.setInitialDate(reqCollectionEconomicView.getInitialDate());
			viewRegistreTO.setFinalDate(reqCollectionEconomicView.getFinalDate());
			listResult =  facade.getListCollectionEconomics(reqCollectionEconomicView,state);
			lstAux =  facade.getListCollectionEconomics(reqCollectionEconomicView,state);
		} catch (Exception e) {
			e.printStackTrace();
		}
		lisRegisterResultTOs = new GenericDataModel<>(listResult);
		lisRegisterResultTOsAux  = new GenericDataModel<>(lstAux);
	}
	/**
	 * Cargamos los datos del registro de la solcitud
	 * */
	/*private void loadDetailViewToMOdify(){
		viewRegistreTO = new RegisterCollectionRightTO();
		try {
			viewRegistreTO.setIdParticipantPk(reqCollectionEconomicView.getParticipant().getIdParticipantPk());
			viewRegistreTO.setTypeIncome(reqCollectionEconomicView.getInstrumentType());//Renta fija por defecto
			viewRegistreTO.setIssuer(new Issuer(reqCollectionEconomicView.getIdIssuerFk()));
			lstSecurityClass = facade.getSecurityClassList(reqCollectionEconomicView.getInstrumentType());
			viewRegistreTO.setSecurityClass(reqCollectionEconomicView.getSecurityClass());
			viewRegistreTO.setSecurity(new Security(reqCollectionEconomicView.getIdSecurityCodeFk()));
			lstCurrency =  parameterServiceBean.getListParameterTableServiceBean(53);
			viewRegistreTO.setCurrency(reqCollectionEconomicView.getCurrency());
			viewRegistreTO.setInitialDate(reqCollectionEconomicView.getInitialDate());
			viewRegistreTO.setFinalDate(reqCollectionEconomicView.getFinalDate());
			listResult =  facade.getListCollectionEconomicsToModify(reqCollectionEconomicView);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		lisRegisterResultTOs = new GenericDataModel<>(listResult);
	}*/
	/**
	 * En caso de existir modificaciones, cargamos los datos modificados
	 * */
	public void loadDetailViewModify()
	{
		viewRegistreTO = new RegisterCollectionRightTO();
		try {
			/*Filtros de busqueda*/
			viewRegistreTO.setIdParticipantPk(reqCollectionEconomicView.getParticipant().getIdParticipantPk());
			viewRegistreTO.setTypeIncome(reqCollectionEconomicView.getInstrumentType());
			if(viewRegistreTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				viewRegistreTO.setRenderedRV(BooleanType.YES.getBooleanValue());
			viewRegistreTO.setIssuer(new Issuer(reqCollectionEconomicView.getIdIssuerFk()));
			lstSecurityClass = facade.getSecurityClassList(reqCollectionEconomicView.getInstrumentType());
			viewRegistreTO.setSecurityClass(reqCollectionEconomicView.getSecurityClass());
			viewRegistreTO.setSecurity(new Security(reqCollectionEconomicView.getIdSecurityCodeFk()));
			lstCurrency =  parameterServiceBean.getListParameterTableServiceBean(53);
			viewRegistreTO.setCurrency(reqCollectionEconomicView.getCurrency());
			viewRegistreTO.setInitialDate(reqCollectionEconomicView.getInitialDate());
			viewRegistreTO.setFinalDate(reqCollectionEconomicView.getFinalDate());
			/*Carga de datos*/
			ObjectRegModfCollectionResultTO collectionResultTO = facade.getListCollectionEconomicsModified(reqCollectionEconomicView);
			listResult =  collectionResultTO.getList();
			viewRegistreTO.setIdReqModfCollectionPk(collectionResultTO.getIdRegModfCollection());
		} catch (Exception e) {
			e.printStackTrace();
		}
		lisRegisterResultTOs = new GenericDataModel<>(listResult);
	}
	/**
	 * Modificando la solicitud
	 * @throws Exception 
	 * */	
	public String beforeModify() throws Exception{
		List<SearchRegisterResultTO>  listAux;
		List<SearchRegisterResultTO>  listBackup;
		if(isSelectedReqCollectionEconomic()){
			viewRegistreTO.setInitialDate(reqCollectionEconomicView.getInitialDate());
			viewRegistreTO.setFinalDate(reqCollectionEconomicView.getFinalDate());
			if(reqCollectionEconomicView.getState().equals(StatesConstants.REGISTERED_REQUEST_COLLECTION_ECONOMIC)){
				/*Cargamos los valores de busqueda*/
				loadDetailView(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
				/*Buscamos nuevamente los vencimientos */
				/*listAux = facade.getListSecuritiesToPay(viewRegistreTO);
				listBackup= facade.getListSecuritiesToPay(viewRegistreTO);
				List<SearchRegisterResultTO> listFinal = new ArrayList<>();
				if(listAux!=null)
				for (SearchRegisterResultTO resultTO : listAux) {
					resultTO.setCouponNumber(null);
					if(!collectionRigthServiceBean.isExistsEconomicRight(resultTO))
						listFinal.add(resultTO);
				}
				if(listFinal.size()>0){
					listAux.addAll(lisRegisterResultTOs.getDataList());
					lisRegisterResultTOs = new GenericDataModel<>(listAux);
					lisRegisterResultTOsAux = new GenericDataModel<>(listBackup);
				}*/
				viewRegistreTO.setRenderdModify(true);
				return "modifyRegistreCollectionRigh";
			}else{ 
				if(reqCollectionEconomicView.getState().equals(StatesConstants.MODIFIED_REQUEST_COLLECTION_ECONOMIC)){
					loadDetailView(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
					/*Buscamos nuevamente los vencimientos*/
					/*listAux = facade.getListSecuritiesToPay(viewRegistreTO);
					listBackup= facade.getListSecuritiesToPay(viewRegistreTO);
					List<SearchRegisterResultTO> listFinal = new ArrayList<>();
					if(listAux!=null&&listAux.size()>0)
					for (SearchRegisterResultTO resultTO : listAux) {
						resultTO.setCouponNumber(null);
						if(!collectionRigthServiceBean.isExistsEconomicRight(resultTO))
							listFinal.add(resultTO);
					}
					if(listFinal.size()>0){
						listAux.addAll(lisRegisterResultTOs.getDataList());
						lisRegisterResultTOs = new GenericDataModel<>(listAux);
						lisRegisterResultTOsAux = new GenericDataModel<>(listBackup);
					}
					*/
					viewRegistreTO.setRenderdModify(true);
					return "modifyRegistreCollectionRigh";
				}else{
					String msg = PropertiesUtilities.getMessage("lbl.alert.not.registered.modified.state");
					showMessageOnDialogCollection(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		return null;
	}
	private void showMessageOnDialogCollection(String headerMessageConfirmation,String bodyMessageConfirmation){
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	public String beforeReject(){
		if(isSelectedReqCollectionEconomic()){
			if(reqCollectionEconomicView.getState().equals(StatesConstants.REGISTERED_REQUEST_COLLECTION_ECONOMIC)||reqCollectionEconomicView.getState().equals(StatesConstants.MODIFIED_REQUEST_COLLECTION_ECONOMIC)){
				//Rechazando una solicitud en estado registrado
				loadDetailView(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
				viewRegistreTO.setRenderedRejected(true);
				viewRegistreTO.setRenderdModify(false);
				viewRegistreTO.setRenderedConfirmModify(false);
				viewRegistreTO.setRenderedConfirmRegistre(false);
				return "viewRegistreCollectionRigh";
			}else{
				String msg = PropertiesUtilities.getMessage("lbl.alert.not.registered.modified.state");
				showMessageOnDialogCollection(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		return null;
	}
	/**
	 * Comprobamos si la solicitud tiene estado registrado
	 * Iniciamos la confirmacion
	 * */
	public String beforeConfirm(){
		if(isSelectedReqCollectionEconomic()){
			if(reqCollectionEconomicView.getState().equals(StatesConstants.REGISTERED_REQUEST_COLLECTION_ECONOMIC)||reqCollectionEconomicView.getState().equals(StatesConstants.MODIFIED_REQUEST_COLLECTION_ECONOMIC)){
				//Confirmando una solicitud en estado registrado
				loadDetailView(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
				viewRegistreTO.setRenderedRejected(false);
				viewRegistreTO.setRenderdModify(false);
				viewRegistreTO.setRenderedConfirmModify(false);
				/*Confirmando Solicitud*/
				viewRegistreTO.setRenderedConfirmRegistre(true);
				return "viewRegistreCollectionRigh";
			}else{
				String msg = PropertiesUtilities.getMessage("lbl.alert.not.registered.modified.state");
				showMessageOnDialogCollection(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		return null;
	}
	/**
	 * Busqueda de solicitudes registradas
	 * */
	public void searchFromFilters(){
		lisRegisterResultTOs= null;
		listResult = null;
		reqCollectionEconomicRigths = facade.searchReqCollectionEconomicRigth(searchFilterTO);
		if(reqCollectionEconomicRigths!=null&&reqCollectionEconomicRigths.getRowCount()>0)
			visibleResult=false;
		else
			visibleResult =true;
		JSFUtilities.updateComponent("idResultBankAccount");
	}
	public void clearAllFilters(){
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				||userInfo.getUserAccountSession().isParticipantInvestorInstitucion())
			searchFilterTO = new SearchCollectionRightTO(userInfo.getUserAccountSession().getParticipantCode());
		else
			searchFilterTO = new SearchCollectionRightTO();
		searchFilterTO.setTypeIncome(InstrumentType.FIXED_INCOME.getCode());
		reqCollectionEconomicRigths = null;
		lisRegisterResultTOs = null;
		listResult = null;
		visibleResult = false;
	}
	/**
	 * 
	 * Verificamos si se ha seleccionado un registro*/
	private boolean isSelectedReqCollectionEconomic(){
		if(reqCollectionEconomicView==null){
			String msg = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MESSAGE_NO_SELECT);
			showMessageOnDialogCollection(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),msg);
			JSFUtilities.showSimpleValidationDialog();
			return false;
		}else
			return true;
	}
	
	/** metodo para verificar si se realizo una modificacion en algun valor de la listas de valores de la solicitud
	 * 
	 * @return true si se modifico, false si no hubo ningun cambio */
	private boolean checkIsModified() {
		
		List<SearchRegisterResultTO> list1 =lisRegisterResultTOs.getDataList();
		List<SearchRegisterResultTO> list2 =lisRegisterResultTOsAux.getDataList();
		
		SearchRegisterResultTO array_element1 = null;
		SearchRegisterResultTO array_element2 = null;
		for (int i = 0; i < list1.size(); i++) {
			array_element1 = list1.get(i);
			array_element2 = list2.get(i);
			if (!array_element1.getCurrencyFilter().getCurrency().equals(array_element2.getCurrencyFilter().getCurrency()))		
				return true;		
			// verificando si se modifico Monto a Cobrar	
			if (array_element1.getAmountChargeFilter().getAmount().compareTo(array_element2.getAmountChargeFilter().getAmount()) != 0)	
				return true;			
			// verificando si se modifico banco		
			if (!array_element1.getBankFilter().getIdBankPkSelected().equals(array_element2.getBankFilter().getIdBankPkSelected()))	
				return true;				
			// verificando si se modifico Cuenta	
			if (!array_element1.getAccountFilter().getAccountSelected().equals(array_element2.getAccountFilter().getAccountSelected()))
			    return true;
		}
		return false;
	}
	
	/** metodo para validar si existen registros sin banco asignado
	 * 
	 * @return true si todos tienen asignado banco */
	private boolean checkIsSelectedBank() {
		for (SearchRegisterResultTO search1 : lisRegisterResultTOs)
			if (!Validations.validateIsNotNull(search1.getBankFilter().getIdBankPkSelected()))
				return false;
		return true;
	}

	/** metodo para validar si existen registros sin nro de cuenta asignado
	 * 
	 * @return true si todos tienen asignado nro de cuenta */
	private boolean checkIsSelectedAccountNumber() {
		for (SearchRegisterResultTO search1 : lisRegisterResultTOs)
			if (!Validations.validateIsNotNull(search1.getAccountFilter().getAccountSelected()))
				return false;
		return true;
	}
	
	/** Dialogo de confirmacion para el registro la modificaicon de la solicitud */
	public void modifyReqCollectionDialog(){
		// validando que no exista monotos en menor o igual a cero
		for (SearchRegisterResultTO register : lisRegisterResultTOs) {
			if (register.getAmountChargeFilter().getAmount().compareTo(BigDecimal.ZERO) <= 0) {
				// validando que no puede registrar valores iguales o menores a 0
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.required.amount.great.zero");
				showMessageOnDialogCollection(headerMessage, bodyMessage);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqAmountGreatZero').show();");
				return;
			}
		}

		// validando que se todos tengan seleccionado un banco
		if (!checkIsSelectedBank()) {
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.required.selected.bank");
			showMessageOnDialogCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqAmountGreatZero').show();");
			return;
		}

		// validando que los registros tengan seleccionada nro de cuenta
		if (!checkIsSelectedAccountNumber()) {
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.required.selected.account");
			showMessageOnDialogCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqAmountGreatZero').show();");
			return;
		}

		// validando que se haya realizado una modificacion
		if (!checkIsModified()) {
			// enviando mensaje de que no se realizo ninguna modificacion. debe modificar al menos un valor
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.required.modified");
			showMessageOnDialogCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqAmountGreatZero').show();");
			return;
		}

		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.modify.request.collection");
		showMessageOnDialogCollection(headerMessage, bodyMessage);
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('vwDialogConfirmModfReqCollection').show();");
	}
	/**
	 * Confirmando la solictud de modificacion
	 * */
	public void confirmModfCollectionDialogConfirmYes(){
		for (SearchRegisterResultTO mainObject : lisRegisterResultTOs){
			if(mainObject.getBankFilter().getIdBankPkSelected()==null||
					mainObject.getAccountFilter().getAccountSelected()==null){
				showMessageAlert("lbl.requiered.message.alert.bank.account");
				return;
			}
		}
		/*Guardamos*/
		try {
			facade.saveAllModfCollectionEconomic(lisRegisterResultTOs,reqCollectionEconomicView);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		reqCollectionEconomicRigths = facade.searchReqCollectionEconomicRigth(searchFilterTO);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.successful.modify.request.collection",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqCollectionOk').show();");
	}
	/**
	 * Rechazamos la solicitud de cobro de derecho economico
	 **/
	public void rejectReqCollectionDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.dialog.message.confirm.reject",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogRejectReqCollection').show();");
	}
	/**
	 * Rechazando la solicitud de cobro de derechos economicos
	 * */
	public void rejectReqCollectionDialogYes(){
		try {
			facade.rejectReqCollectionEconomic(reqCollectionEconomicView);
			//Enviar notificacion
			BusinessProcess businessProc = new BusinessProcess();
			String parameter1 = reqCollectionEconomicView.getIdReqCollectionEconomicPk().toString();
			Object[] parameters = {parameter1};
			businessProc.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_REJECT_COLLECTION_ECONOMIC.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null, parameters);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		searchFromFilters();
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.dialog.message.successful.reject",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogRejectReqCollectionOk').show();");
	}
	/**
	 * Confirmando la solicitud de cobro de derecho economico
	 * */
	public void confirmReqCollectionDialog(){
		BusinessProcess n = new BusinessProcess();
		n.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CONFIRM_REGISTRE_COLLECTION.getCode());
		Object[] parameters = {reqCollectionEconomicView.getIdReqCollectionEconomicPk().toString()};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),n, null, parameters);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.dialog.message.confirm.dialog",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('vwDialogConfirmReqCollection').show();");
	}
	
	/**
	 * Confirmando la solicitud de cobro de derecho economico
	 * y actualizando los datos en la solitud de modificacion
	 * */
	public void confirmModfReqCollectionDialog(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.dialog.message.confirm.dialog",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('vWConfirmModfReqCollection').show();");
	}
	/**
	 * Confirmando la solicitud de modificacion
	 * */
	public void confirmModfReqCollectionDialogYes(){
		try {
			facade.confirmModfReqCollectionEconomic(reqCollectionEconomicView.getIdReqCollectionEconomicPk(),viewRegistreTO.getIdReqModfCollectionPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		searchFromFilters();
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.dialog.message.successful.confirm",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogConfirmReqCollectionOk').show();");
	}
	/**
	 * Confirma la solicitud de cobro de derechos economicos
	 * @param Object ReqCollectionEconomicRight
	 * @return
	 * */
	public void confirmReqCollectionDialogYes(){
		try {
			facade.confirmReqCollectionEconomic(reqCollectionEconomicView);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		searchFromFilters();
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.dialog.message.successful.confirm",reqCollectionEconomicView.getIdReqCollectionEconomicPk());
		showMessageOnDialogCollection(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent("opnlDialogs");
    	JSFUtilities.executeJavascriptFunction("PF('wvDialogConfirmReqCollectionOk').show();");
	}
	/**
	 * Evento de cuando seleccionamos otra moneda de cobro
	 * Se habilitan y deshabilitan opsiones
	 * */
	
	public void checkCollectionRecord(SearchRegisterResultTO mainObject){
		log.info("CDE::: Check:"+mainObject.isCheckOther()+", Moneda :"+mainObject.getCurrency()+" Otra moneda :"+mainObject.getCurrencyFilter().getCurrency() );
		if(mainObject.isCheckOther()){
			List<Bank> listBankProvider;
			for (SearchRegisterResultTO resultTO : lisRegisterResultTOs) {
				if(mainObject.getIdCollectionEconomicPk().equals(resultTO.getIdCollectionEconomicPk())){
					mainObject.getCurrencyFilter().setDisabled(false);
					mainObject.getAmountChargeFilter().setDisabled(false);
					if(mainObject.getTotalAmount().equals(mainObject.getAmountChargeFilter().getAmount())){
						mainObject.getAmountChargeFilter().setAmount(BigDecimal.ZERO);
						listBankProvider = facade.reSearchBankProvider(mainObject.getIdParticipantPk(), mainObject.getAccountNumber(), mainObject.getCurrencyFilter().getCurrency());
						mainObject.getBankFilter().setListBankProvider(listBankProvider);
						mainObject.getBankFilter().setIdBankPkSelected(null);
						mainObject.getAccountFilter().setAccountList(null);
						mainObject.getAccountFilter().setAccountSelected(null);
					}
					return;
				}
			}
		}else{
			List<Bank> listBankProvider;
			for (SearchRegisterResultTO resultTO : lisRegisterResultTOs) {
				if(mainObject.getIdCollectionEconomicPk().equals(resultTO.getIdCollectionEconomicPk())){
					mainObject.getCurrencyFilter().setDisabled(true);
					mainObject.getAmountChargeFilter().setDisabled(true);
					mainObject.getAmountChargeFilter().setAmount(mainObject.getTotalAmount());
					if(!mainObject.getCurrency().equals(CurrencyType.UFV.getCode())){
						Integer currencyBack=mainObject.getCurrency();
						mainObject.getCurrencyFilter().setCurrency(currencyBack);
					}
					//if(mainObject.getBankFilter().getIdBankPkSelected()==null){
						listBankProvider = facade.reSearchBankProvider(mainObject.getIdParticipantPk(), mainObject.getAccountNumber(), mainObject.getCurrency());
						mainObject.getBankFilter().setListBankProvider(listBankProvider);
						mainObject.getBankFilter().setIdBankPkSelected(null);
						mainObject.getAccountFilter().setAccountList(null);
						mainObject.getAccountFilter().setAccountSelected(null);
					//}
					return;
				}
			}
		}
	}
	
	
	/**
	 * Mensaje de alerta*/
	private void showMessageAlert(String propertieString){
			showMessageOnDialogCollection(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage(propertieString));
			JSFUtilities.showSimpleValidationDialog();
	}
	/*Getters and Setters*/
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public SearchCollectionRightTO getSearchFilterTO() {
		return searchFilterTO;
	}
	public void setSearchFilterTO(SearchCollectionRightTO searchFilterTO) {
		this.searchFilterTO = searchFilterTO;
	}
	public List<Participant> getListParticipants() {
		return listParticipants;
	}
	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}
	public List<ParameterTable> getListTypeIncome() {
		return listTypeIncome;
	}
	public void setListTypeIncome(List<ParameterTable> listTypeIncome) {
		this.listTypeIncome = listTypeIncome;
	}
	public List<ParameterTable> getListState() {
		return listState;
	}
	public void setListState(List<ParameterTable> listState) {
		this.listState = listState;
	}
	public List<ParameterTable> getListValidated() {
		return listValidated;
	}
	public void setListValidated(List<ParameterTable> listValidated) {
		this.listValidated = listValidated;
	}
	public GenericDataModel<ReqCollectionEconomicRight> getReqCollectionEconomicRigths() {
		return reqCollectionEconomicRigths;
	}
	public void setReqCollectionEconomicRigths(
			GenericDataModel<ReqCollectionEconomicRight> reqCollectionEconomicRigths) {
		this.reqCollectionEconomicRigths = reqCollectionEconomicRigths;
	}
	public RegisterCollectionRightTO getViewRegistreTO() {
		return viewRegistreTO;
	}
	public void setViewRegistreTO(RegisterCollectionRightTO viewRegistreTO) {
		this.viewRegistreTO = viewRegistreTO;
	}
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	public GenericDataModel<SearchRegisterResultTO> getLisRegisterResultTOs() {
		return lisRegisterResultTOs;
	}
	public void setLisRegisterResultTOs(
			GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs) {
		this.lisRegisterResultTOs = lisRegisterResultTOs;
	}
	public List<SearchRegisterResultTO> getListResult() {
		return listResult;
	}
	public void setListResult(List<SearchRegisterResultTO> listResult) {
		this.listResult = listResult;
	}
	public ReqCollectionEconomicRight getReqCollectionEconomicView() {
		return reqCollectionEconomicView;
	}
	public void setReqCollectionEconomicView(
			ReqCollectionEconomicRight reqCollectionEconomicView) {
		this.reqCollectionEconomicView = reqCollectionEconomicView;
	}
	public boolean isVisibleResult() {
		return visibleResult;
	}
	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}
}
