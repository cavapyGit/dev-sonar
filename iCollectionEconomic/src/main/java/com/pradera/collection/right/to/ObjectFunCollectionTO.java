package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class ObjectFunCollectionTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String gloss;
	
	private BigDecimal amount;
	
	public ObjectFunCollectionTO(String gloss,BigDecimal amount) {
		this.gloss = gloss;
		this.amount = amount;
	}

	public String getGloss() {
		return gloss;
	}

	public void setGloss(String gloss) {
		this.gloss = gloss;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
}
