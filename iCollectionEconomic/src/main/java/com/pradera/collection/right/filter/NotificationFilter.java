package com.pradera.collection.right.filter;

import java.util.List;

public class NotificationFilter extends FilterSearch {

	/**
	 * 
	 */
	
	private List<String> listStates;
	private Integer stateNotification;
	private String stateDescription;
	private static final long serialVersionUID = 1L;
	
	public NotificationFilter() {
		stateDescription = "No Enviada";
	}
	public NotificationFilter(Integer state) {
		stateNotification= state;
		if(state==1)
			stateDescription = "Recepcionada";
		if(state==0)
			stateDescription = "Enviada";
	}
	public List<String> getListStates() {
		return listStates;
	}
	public void setListStates(List<String> listStates) {
		this.listStates = listStates;
	}
	public Integer getStateNotification() {
		return stateNotification;
	}
	public void setStateNotification(Integer stateNotification) {
		this.stateNotification = stateNotification;
	}
	public String getStateDescription() {
		return stateDescription;
	}
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
}
