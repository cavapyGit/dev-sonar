package com.pradera.collection.right.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.swing.text.StyledEditorKit.BoldAction;

import com.pradera.collection.right.filter.NotificationFilter;
import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.BankAccountMasiveTO;
import com.pradera.collection.right.to.ObjectRegModfCollectionResultTO;
import com.pradera.collection.right.to.RegisterCollectionRightTO;
import com.pradera.collection.right.to.SearchCollectionRightTO;
import com.pradera.collection.right.to.SearchRegisterResultTO;
import com.pradera.collection.right.util.StatesConstants;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.process.BusinessProcess;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.ModfCollEcoRigth;
import com.edv.collection.economic.rigth.ReqCollectionEconomicRight;
import com.edv.collection.economic.rigth.ReqModfCollEcoRight;

@Stateless	
@Performance
public class CollectionRigthFacade {

	@Inject
	private PraderaLogger log;

	@Inject
	private CollectionRigthServiceBean bean;

	@Inject
	private ParameterServiceBean parameterServiceBean;

	@Inject
	private UserInfo userInfo;

	public CollectionRigthFacade() {

	}

	public List<ParameterTable> getSecurityClassList(Integer parameterTablePk) {
		try {
			return bean.getSecurityClassList(parameterTablePk);
		} catch (Exception e) {
			log.error("CDDE ::: Error, No se pudo obtener la lista de las clases de valor");
		}
		return null;
	}

	public List<ParameterTable> getSecurityClassListAll() {
		try {
			return bean.getSecurityClassListAll();
		} catch (Exception e) {
			log.error("CDDE ::: Error");
		}
		return null;
	}

	public GenericDataModel<ReqCollectionEconomicRight> searchReqCollectionEconomicRigth(
			SearchCollectionRightTO searchFilterTO) {
		ParameterTable parameterTable;

		List<ReqCollectionEconomicRight> list = bean.getListRegCollectionEconomic(searchFilterTO);
		for (ReqCollectionEconomicRight rce : list) {
			rce.setParticipantDesc(rce.getParticipant().getMnemonic() + " - " + rce.getParticipant().getDescription());
			parameterTable = bean.find(ParameterTable.class, rce.getState());
			rce.setStateDesc(parameterTable.getDescription());
			parameterTable = bean.find(ParameterTable.class, rce.getInstrumentType());
			rce.setInstrumentTypeDesc(parameterTable.getText1() + " - " + parameterTable.getDescription());
			rce.setSubmitDesc(rce.getSubmit().equals(0) ? BooleanType.NO.getValue() : BooleanType.YES.getValue());
		}
		GenericDataModel<ReqCollectionEconomicRight> dataModel = new GenericDataModel<>(list);
		return dataModel;
	}

	public <T> T find(Class<T> type, Object id) {
		return bean.find(type, id);
	}

	public List<ParameterTable> getSecurityClassList(String list) {
		String[] idParameterTable = list.split(",");
		List<ParameterTable> parameterTables = new ArrayList<ParameterTable>();
		ParameterTable p;
		for (int i = 0; i < idParameterTable.length; i++) {
			p = bean.find(ParameterTable.class, Integer.parseInt(idParameterTable[i]));
			parameterTables.add(p);
		}
		return parameterTables;
	}

	/**
	 * Busqueda de valores que no se encuentran registrados con saldo disponible
	 */

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SearchRegisterResultTO> getListSecuritiesToPayNotAvailableBalance(
			RegisterCollectionRightTO registerFilterTO) throws Exception {
		if (registerFilterTO.getTypeIncome().equals(InstrumentType.FIXED_INCOME.getCode()))
			return bean.getListSecuritiesNotAvailableBalanceRF(registerFilterTO);
		else
			return bean.getListSecuritiesNotAvailableBalanceRV(registerFilterTO);
	}

	/**
	 * Buscamos los vencimientos registrados y si existieran nuevos, los cargamos en
	 * la solicitud
	 * 
	 * @param RegisterCollectionRightTO registerFilterTO
	 * @return List<SearchRegisterResultTO> list
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SearchRegisterResultTO> getListSecuritiesToPay(RegisterCollectionRightTO registerFilterTO)
			throws Exception {
		/* Lista de vencimientos a cobrar */
		List<SearchRegisterResultTO> list = bean.getListSecuritiesToPay(registerFilterTO);
		if (list == null || list.size() == 0)
			return null;

		ParameterTable currencyBob = bean.find(ParameterTable.class, CurrencyType.PYG.getCode());
		ParameterTable currencyUsd = bean.find(ParameterTable.class, CurrencyType.USD.getCode());

		List<ParameterTable> listCurrencyBobUsd = new ArrayList<>();
		listCurrencyBobUsd.add(currencyBob);
		listCurrencyBobUsd.add(currencyUsd);

		List<Bank> listBankProvider;
		for (SearchRegisterResultTO searchRegisterResultTO : list) {
			/* Cargando monedas */
			if (searchRegisterResultTO.getCurrency().equals(CurrencyType.UFV.getCode())) {
				searchRegisterResultTO.getCurrencyFilter().setCurrency(currencyBob.getParameterTablePk());
				searchRegisterResultTO.getCurrencyFilter().setListCurrency(listCurrencyBobUsd);
				
				/***/
				searchRegisterResultTO.getCurrencyFilter().setDisabled(false);
				searchRegisterResultTO.getAmountChargeFilter().setDisabled(false);
				searchRegisterResultTO.setCheckOther(true);
				Date expiritationDate = CommonsUtilities.convertStringtoDate(searchRegisterResultTO.getDueDate());
				DailyExchangeRates rate = parameterServiceBean.getDayExchangeRate(expiritationDate,CurrencyType.UFV.getCode());
				searchRegisterResultTO.getAmountChargeFilter().setAmount(rate.getBuyPrice().multiply(searchRegisterResultTO.getTotalAmount()));
			}
			if (searchRegisterResultTO.getCurrency().equals(CurrencyType.PYG.getCode())) {
				searchRegisterResultTO.getCurrencyFilter().setCurrency(currencyBob.getParameterTablePk());
				searchRegisterResultTO.getCurrencyFilter().setListCurrency(listCurrencyBobUsd);
				searchRegisterResultTO.getAmountChargeFilter().setAmount(searchRegisterResultTO.getTotalAmount());
			}
			if (searchRegisterResultTO.getCurrency().equals(CurrencyType.USD.getCode())) {
				searchRegisterResultTO.getCurrencyFilter().setCurrency(currencyUsd.getParameterTablePk());
				searchRegisterResultTO.getCurrencyFilter().setListCurrency(listCurrencyBobUsd);
				searchRegisterResultTO.getAmountChargeFilter().setAmount(searchRegisterResultTO.getTotalAmount());
			}
			listBankProvider = this.reSearchBankProvider(searchRegisterResultTO.getIdParticipantPk(),searchRegisterResultTO.getAccountNumber(),searchRegisterResultTO.getCurrencyFilter().getCurrency());
			searchRegisterResultTO.getBankFilter().setListBankProvider(listBankProvider);
			searchRegisterResultTO.getBankFilter().setIdBankPkSelected(null);
			searchRegisterResultTO.getAccountFilter().setAccountList(null);
			searchRegisterResultTO.getAccountFilter().setAccountSelected(null);
			if(registerFilterTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode())) {
				searchRegisterResultTO.setCheckOther(BooleanType.YES.getBooleanValue());
				searchRegisterResultTO.getAmountChargeFilter().setDisabled(false);
				searchRegisterResultTO.getCurrencyFilter().setDisabled(false);
			}
		}

		return list;
	}

	/**
	 * Buscamos todos los cobros registrados
	 * 
	 * @param reqCollectionEconomicRight Solicitud a la cual se selecciono para ver
	 *                                   el detalle
	 * @return Lista de todos los cobros registrados en esa solicitud
	 * @throws Exception
	 */
	public List<SearchRegisterResultTO> getListCollectionEconomics(
			ReqCollectionEconomicRight reqCollectionEconomicRight, Integer state) throws Exception {
		List<CollectionEconomicRigth> list = bean.getListCollectionEconomicsRegistered(reqCollectionEconomicRight.getIdReqCollectionEconomicPk(), state);
		List<SearchRegisterResultTO> listRegisterResultTOs = new ArrayList<SearchRegisterResultTO>();
		SearchRegisterResultTO resultTO;
		List<String> accountList;
		List<Bank> listBanks;
		for (CollectionEconomicRigth cer : list) {
			resultTO = new SearchRegisterResultTO();
			resultTO.setIdCollectionEconomicPk(cer.getIdCollectionEconomicPk());
			resultTO.setIdParticipantPk(reqCollectionEconomicRight.getParticipant().getIdParticipantPk());
			resultTO.setDueDate(CommonsUtilities.convertDatetoString(cer.getDueDate()));
			resultTO.setPaymentDate(cer.getPaymentDate());
			resultTO.setAlternateCode(cer.getAlternateCode());
			resultTO.setIdSecurityCodePk(cer.getIdSecurityCodeFk());
			resultTO.setAccountNumber(cer.getAccountNumber());
			resultTO.setIdHolderAccountPk(cer.getIdHolderAccountPk());
			resultTO.setIdGeneralPurposePk(cer.getIdGeneralPurposePk());
			resultTO.setCurrency(cer.getCurrency());
			resultTO.setCurrencyDesc((bean.find(ParameterTable.class, cer.getCurrency()).getDescription()));
			resultTO.setAvailableBalance(cer.getAmountSecurities());
			resultTO.setInterestRate(cer.getInterest());
			resultTO.setAmortization(cer.getAmortization());
			resultTO.setTotalAmount(cer.getAmountReceivable());
			resultTO.setCheckOther(cer.getCheckOther() == 0 ? false : true);
			resultTO.getCurrencyFilter().setListCurrency(bean.getCurrencyListPay());
			resultTO.getCurrencyFilter().setCurrency(cer.getAnotherCurency());
			resultTO.getAmountChargeFilter().setAmount(cer.getAnotherAmountReceivable());
			if (resultTO.isCheckOther())
				listBanks = bean.getlistBankProviderByParticipantAndHolder(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), resultTO.getAccountNumber(),
						cer.getAnotherCurency());
			else
				listBanks = bean.getlistBankProviderByParticipantAndHolder(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), resultTO.getAccountNumber(),
						cer.getCurrency());
			resultTO.getBankFilter().setListBankProvider(listBanks);
			resultTO.getBankFilter().setIdBankPkSelected(cer.getIdBankFk());

			if (resultTO.isCheckOther())
				accountList = bean.getAccountNumberList(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), resultTO.getAccountNumber(),
						cer.getIdBankFk(), cer.getAnotherCurency());
			else
				accountList = bean.getAccountNumberList(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), resultTO.getAccountNumber(),
						cer.getIdBankFk(), cer.getCurrency());
			resultTO.getAccountFilter().setAccountList(accountList);
			resultTO.getAccountFilter().setAccountSelected(cer.getDepAccountNumber());
			// Buscamos las notoficaciones a los agentes pagadores
			if (cer.getNotificationReading() != null)
				resultTO.setNotificationFilter(new NotificationFilter(cer.getNotificationReading()));
			else
				resultTO.setNotificationFilter(new NotificationFilter());
			log.info("CDE ::: ESTADO:" + resultTO.getNotificationFilter().getStateDescription());
			listRegisterResultTOs.add(resultTO);

		}
		return listRegisterResultTOs;
	}

	public List<SearchRegisterResultTO> getListCollectionEconomicsToModify(
			ReqCollectionEconomicRight reqCollectionEconomicRight) throws Exception {
		List<CollectionEconomicRigth> list = bean.getListCollectionEconomicsRegistered(
				reqCollectionEconomicRight.getIdReqCollectionEconomicPk(),
				StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
		List<SearchRegisterResultTO> listRegisterResultTOs = new ArrayList<SearchRegisterResultTO>();
		SearchRegisterResultTO resultTO;
		List<String> accountList;
		List<Bank> listBanks;
		for (CollectionEconomicRigth cer : list) {
			resultTO = new SearchRegisterResultTO();
			resultTO.setIdParticipantPk(reqCollectionEconomicRight.getParticipant().getIdParticipantPk());
			resultTO.setDueDate(CommonsUtilities.convertDatetoString(cer.getDueDate()));
			resultTO.setPaymentDate(cer.getPaymentDate());
			resultTO.setAlternateCode(cer.getAlternateCode());
			resultTO.setIdGeneralPurposePk(cer.getIdGeneralPurposePk());
			resultTO.setIdSecurityCodePk(cer.getIdSecurityCodeFk());
			resultTO.setIdHolderAccountPk(cer.getIdHolderAccountPk());
			resultTO.setAccountNumber(cer.getAccountNumber());
			resultTO.setCurrency(cer.getCurrency());
			resultTO.setCurrencyDesc((bean.find(ParameterTable.class, cer.getCurrency()).getDescription()));
			resultTO.setAvailableBalance(cer.getAmountSecurities());
			resultTO.setInterestRate(cer.getInterest());
			resultTO.setAmortization(cer.getAmortization());
			resultTO.setTotalAmount(cer.getAmountReceivable());
			resultTO.setCheckOther(cer.getCheckOther() == 0 ? false : true);
			if (resultTO.isCheckOther()) {
				resultTO.getCurrencyFilter().setDisabled(false);
				resultTO.getAmountChargeFilter().setDisabled(false);
			}
			resultTO.getCurrencyFilter().setListCurrency(bean.getCurrencyListPay());
			resultTO.getCurrencyFilter().setCurrency(cer.getAnotherCurency());
			resultTO.getAmountChargeFilter().setAmount(cer.getAnotherAmountReceivable());
			resultTO.setAccountNumber(cer.getAccountNumber());
			if (resultTO.isCheckOther())
				listBanks = bean.getlistBankProviderByParticipantAndHolder(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), cer.getAccountNumber(),
						cer.getAnotherCurency());
			else
				listBanks = bean.getlistBankProviderByParticipantAndHolder(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), cer.getAccountNumber(),
						cer.getCurrency());
			resultTO.getBankFilter().setListBankProvider(listBanks);
			resultTO.getBankFilter().setIdBankPkSelected(cer.getIdBankFk());

			if (resultTO.isCheckOther())
				accountList = bean.getAccountNumberList(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), cer.getAccountNumber(),
						cer.getIdBankFk(), cer.getAnotherCurency());
			else
				accountList = bean.getAccountNumberList(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), cer.getAccountNumber(),
						cer.getIdBankFk(), cer.getCurrency());
			resultTO.getAccountFilter().setAccountList(accountList);
			resultTO.getAccountFilter().setAccountSelected(cer.getDepAccountNumber());
			// Buscamos las notoficaciones a los agentes pagadores
			resultTO.setNotificationFilter(new NotificationFilter());
			listRegisterResultTOs.add(resultTO);
		}
		return listRegisterResultTOs;
	}

	/**
	 * Buscar la ultima modifcacion registrada
	 * 
	 * @throws Exception
	 */
	public ObjectRegModfCollectionResultTO getListCollectionEconomicsModified(
			ReqCollectionEconomicRight reqCollectionEconomicRight) throws Exception {
		ReqModfCollEcoRight modfCollEcoRight = bean
				.getLastRequestModification(reqCollectionEconomicRight.getIdReqCollectionEconomicPk());
		List<ModfCollEcoRigth> list = bean.getListCollectionEconomicsModifieds(modfCollEcoRight.getIdReqMofCollEcoPk());
		List<SearchRegisterResultTO> listRegisterResultTOs = new ArrayList<SearchRegisterResultTO>();
		SearchRegisterResultTO resultTO;
		List<String> accountList;
		List<Bank> listBanks;
		for (ModfCollEcoRigth mcer : list) {
			resultTO = new SearchRegisterResultTO();
			resultTO.setIdReqModCollection(modfCollEcoRight.getIdReqMofCollEcoPk());
			resultTO.setIdParticipantPk(reqCollectionEconomicRight.getParticipant().getIdParticipantPk());
			resultTO.setDueDate(CommonsUtilities.convertDatetoString(mcer.getDueDate()));
			resultTO.setPaymentDate(mcer.getPaymentDate());
			resultTO.setAlternateCode(mcer.getAlternateCode());
			resultTO.setIdSecurityCodePk(mcer.getIdSecurityCodeFk());
			resultTO.setCurrency(mcer.getCurrency());
			resultTO.setIdGeneralPurposePk(mcer.getIdGeneralPurposePk());
			resultTO.setIdHolderAccountPk(mcer.getIdHolderAccountPk());
			resultTO.setAccountNumber(mcer.getAccountNumber());
			resultTO.setCouponNumber(mcer.getCouponNumber());
			resultTO.setCurrencyDesc((bean.find(ParameterTable.class, mcer.getCurrency()).getDescription()));
			resultTO.setAvailableBalance(mcer.getAmountSecurities());
			resultTO.setInterestRate(mcer.getInterest());
			resultTO.setAmortization(mcer.getAmortization());
			resultTO.setTotalAmount(mcer.getAmountReceivable());
			resultTO.setCheckOther(mcer.getCheckOther() == 0 ? false : true);
			if (resultTO.isCheckOther()) {
				resultTO.getCurrencyFilter().setDisabled(false);
				resultTO.getAmountChargeFilter().setDisabled(false);
			}
			resultTO.getCurrencyFilter().setListCurrency(bean.getCurrencyListPay());
			resultTO.getCurrencyFilter().setCurrency(mcer.getAnotherCurency());
			resultTO.getAmountChargeFilter().setAmount(mcer.getAnotherAmountReceivable());
			resultTO.setAccountNumber(mcer.getAccountNumber());
			if (resultTO.isCheckOther())
				listBanks = bean.getlistBankProviderByParticipantAndHolder(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), mcer.getAccountNumber(),
						mcer.getAnotherCurency());
			else
				listBanks = bean.getlistBankProviderByParticipantAndHolder(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), mcer.getAccountNumber(),
						mcer.getCurrency());
			resultTO.getBankFilter().setListBankProvider(listBanks);
			resultTO.getBankFilter().setIdBankPkSelected(mcer.getIdBankFk());

			if (resultTO.isCheckOther())
				accountList = bean.getAccountNumberList(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), mcer.getAccountNumber(),
						mcer.getIdBankFk(), mcer.getAnotherCurency());
			else
				accountList = bean.getAccountNumberList(
						reqCollectionEconomicRight.getParticipant().getIdParticipantPk(), mcer.getAccountNumber(),
						mcer.getIdBankFk(), mcer.getCurrency());
			resultTO.getAccountFilter().setAccountList(accountList);
			resultTO.getAccountFilter().setAccountSelected(mcer.getDepAccountNumber());
			// Buscamos las notoficaciones a los agentes pagadores
			resultTO.setNotificationFilter(new NotificationFilter());
			listRegisterResultTOs.add(resultTO);
		}
		ObjectRegModfCollectionResultTO collectionResultTO = new ObjectRegModfCollectionResultTO();
		collectionResultTO.setList(listRegisterResultTOs);
		collectionResultTO.setIdRegModfCollection(modfCollEcoRight.getIdReqMofCollEcoPk());
		return collectionResultTO;
	}

	/**
	 * Buscamos la lista de bancos asiganada al participante, cui y la moneda
	 * seleccionada o cargada
	 * 
	 * @param Long idParticipantPk,List<Long> listCui,Integer currency
	 * @return List<Bank>
	 */
	public List<Bank> reSearchBankProvider(Long idParticipantPk, Integer accountNumber, Integer currency) {
		return bean.getlistBankProviderByParticipantAndHolder(idParticipantPk, accountNumber, currency);
	}

	/**
	 * */
	public List<String> reSearchAccountNumber(Long idParticipantPk, Integer accountNumber, Long idBankPk,
			Integer currency) {
		return bean.getAccountNumberList(idParticipantPk, accountNumber, idBankPk, currency);
	}

	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public void confirmReqCollectionEconomic(ReqCollectionEconomicRight reqCollectionView) throws ServiceException {
		reqCollectionView.setState(StatesConstants.CONFIRMED_REQUEST_COLLECTION_ECONOMIC);
		bean.update(reqCollectionView);
	}

	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public void confirmModfReqCollectionEconomic(Long idReqCollEconPk, Long idReqModfCollEcoPk)
			throws ServiceException {
		// los datos de la modificacion pasan a ser los datos de collection
		// econommic(tab)
		bean.replaceCollectionEconomics(idReqCollEconPk, idReqModfCollEcoPk);
	}

	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public void rejectReqCollectionEconomic(ReqCollectionEconomicRight reqCollectionView) throws ServiceException {
		reqCollectionView.setState(StatesConstants.REJECTED_REQUEST_COLLECTION_ECONOMIC);
		bean.update(reqCollectionView);
		List<CollectionEconomicRigth> list = bean.getListCollectionEconomicsRegistered(
				reqCollectionView.getIdReqCollectionEconomicPk(), StatesConstants.REGISTERED_COLLECTION_ECONOMIC);
		for (CollectionEconomicRigth collectionEconomicRigth : list) {
			collectionEconomicRigth.setState(StatesConstants.REJECTED_COLLECTION_ECONOMIC);
			bean.update(collectionEconomicRigth);
		}
	}

	@Inject
	private NotificationServiceFacade notificationServiceFacade;

	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public void saveAllCollectionEconomic(GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs,
			RegisterCollectionRightTO registerFilterTO) {
		ReqCollectionEconomicRight reqCollection = new ReqCollectionEconomicRight();
		/* Guardamos los filtros de la pantalla de busqueda */
		reqCollection.setParticipant(new Participant(registerFilterTO.getIdParticipantPk()));
		reqCollection.setInstrumentType(registerFilterTO.getTypeIncome());
		reqCollection.setIdIssuerFk(registerFilterTO.getIssuer().getIdIssuerPk());
		reqCollection.setSecurityClass(registerFilterTO.getSecurityClass());
		reqCollection.setIdSecurityCodeFk(registerFilterTO.getSecurity().getIdSecurityCodePk());
		reqCollection.setCurrency(registerFilterTO.getCurrency());
		reqCollection.setInitialDate(registerFilterTO.getInitialDate());
		reqCollection.setFinalDate(registerFilterTO.getFinalDate());
		reqCollection.setState(StatesConstants.REGISTERED_REQUEST_COLLECTION_ECONOMIC);
		reqCollection.setProcessDate(CommonsUtilities.currentDate());

		reqCollection.setSubmit(0);
		reqCollection.setAmountSecurities(lisRegisterResultTOs.getRowCount());// actualizar con la suma despues;
		reqCollection.setLastModifyApp(28338);
		reqCollection.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		reqCollection.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		reqCollection.setLastModifyDate(CommonsUtilities.currentDate());
		reqCollection = bean.saveRequestCollection(reqCollection);
		BusinessProcess businessProc = new BusinessProcess();
		String parameter1 = reqCollection.getIdReqCollectionEconomicPk().toString();
		Object[] parameters = { parameter1 };
		businessProc.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_REGISTRE_COLLECTION_ECONOMIC.getCode());
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProc, null,
				parameters);
		CollectionEconomicRigth collectionEconomicRigth;
		for (SearchRegisterResultTO registre : lisRegisterResultTOs) {
			collectionEconomicRigth = new CollectionEconomicRigth();
			collectionEconomicRigth.setState(StatesConstants.REGISTERED_COLLECTION_ECONOMIC);

			collectionEconomicRigth.setIdGeneralPurposePk(registre.getIdGeneralPurposePk());
			collectionEconomicRigth.setIdReqCollectionEconomicFk(reqCollection.getIdReqCollectionEconomicPk());
			collectionEconomicRigth.setIdHolderAccountPk(registre.getIdHolderAccountPk());
			collectionEconomicRigth.setAccountNumber(registre.getAccountNumber());
			// fecha de vencimiento
			collectionEconomicRigth.setDueDate(CommonsUtilities.convertStringtoDate(registre.getDueDate()));
			// Fecha de cobro
			collectionEconomicRigth.setPaymentDate(registre.getPaymentDate());
			collectionEconomicRigth.setAlternateCode(registre.getAlternateCode());
			if (registerFilterTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode()))
				collectionEconomicRigth.setIdHolderPk(registre.getIdHolderPk());
			else
				collectionEconomicRigth.setIdHolderPk(bean.extractHolder(registre.getAlternateCode()));
			collectionEconomicRigth.setCheckCharge(registre.isCheckCharge()==true?BooleanType.YES.getCode():BooleanType.NO.getCode());
			
			collectionEconomicRigth.setIdSecurityCodeFk(registre.getIdSecurityCodePk());
			collectionEconomicRigth.setCurrency(registre.getCurrency());
			// cantidad de valores
			collectionEconomicRigth.setAmountReceivable(registre.getAmountChargeFilter().getAmount());
			// interes
			collectionEconomicRigth.setInterest(registre.getInterestRate());
			// amortizacion
			collectionEconomicRigth.setAmortization(registre.getAmortization());
			// monto total
			collectionEconomicRigth.setAmountReceivable(registre.getTotalAmount());
			// check
			collectionEconomicRigth.setCheckOther(
					registre.getAmountChargeFilter().getAmount().equals(registre.getTotalAmount()) ? 0 : 1);
			// Otra moneda de cobro
			collectionEconomicRigth.setAnotherCurency(registre.getCurrencyFilter().getCurrency());
			// monto a cobrar
			collectionEconomicRigth.setAnotherAmountReceivable(registre.getAmountChargeFilter().getAmount());
			collectionEconomicRigth.setIdBankFk(registre.getBankFilter().getIdBankPkSelected());
			collectionEconomicRigth.setDepAccountNumber(registre.getAccountFilter().getAccountSelected());
			collectionEconomicRigth.setAmountSecurities(registre.getAvailableBalance());
			collectionEconomicRigth.setLastModifyApp(28338);
			collectionEconomicRigth.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			collectionEconomicRigth.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			collectionEconomicRigth.setLastModifyDate(CommonsUtilities.currentDate());
			collectionEconomicRigth.setRegistreDate(CommonsUtilities.currentDate());
			collectionEconomicRigth.setCouponNumber(registre.getCouponNumber());

			bean.saveCollectionEconomicRigth(collectionEconomicRigth);
		}
	}

	/**
	 * Guardamos la solicitud de modificacion
	 * 
	 * @throws ServiceException
	 */
	@ProcessAuditLogger(process = BusinessProcessType.AUDIT_PROCESS_LOG)
	public void saveAllModfCollectionEconomic(GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs,
			ReqCollectionEconomicRight reqCollectionEconomicRight) throws ServiceException {
		ReqModfCollEcoRight reqModfCollEconomicRight = new ReqModfCollEcoRight();
		reqModfCollEconomicRight
				.setIdReqColleEconomicRightFk(reqCollectionEconomicRight.getIdReqCollectionEconomicPk());
		reqModfCollEconomicRight.setState(StatesConstants.REGISTERED_MODIFY_REQUEST_COLLECTION_ECONOMIC);
		reqModfCollEconomicRight.setRegistreDate(CommonsUtilities.currentDate());
		reqModfCollEconomicRight.setLastModifyApp(28338);
		reqModfCollEconomicRight.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
		reqModfCollEconomicRight.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
		reqModfCollEconomicRight.setLastModifyDate(CommonsUtilities.currentDate());
		reqModfCollEconomicRight = bean.saveModfRequestCollection(reqModfCollEconomicRight);
//		ModfCollEcoRigth modfCollEconomicRigth = new ModfCollEcoRigth();
		CollectionEconomicRigth cer;
		for (SearchRegisterResultTO registre : lisRegisterResultTOs) {

			/* Se obtiene el vencimiento registrado y se actualiza sobre el */
			cer = bean.getOneCollectionEconomicRigth(registre.getIdCollectionEconomicPk());
			cer.setCheckOther(registre.isCheckOther() ? BooleanType.YES.getCode() : BooleanType.NO.getCode());
			cer.setAnotherCurency(registre.getCurrencyFilter().getCurrency());
			cer.setAnotherAmountReceivable(registre.getAmountChargeFilter().getAmount());
			cer.setIdBankFk(registre.getBankFilter().getIdBankPkSelected());
			cer.setDepAccountNumber(registre.getAccountFilter().getAccountSelected());
			/* Solo actualizamos los campos modificables arriba */

			cer.setLastModifyApp(28338);
			cer.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			cer.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			cer.setLastModifyDate(CommonsUtilities.currentDate());
			bean.saveModficationCollectionEconomicRigth(cer);

//			modfCollEconomicRigth.setIdReqModfCollEcoFk(reqModfCollEconomicRight.getIdReqMofCollEcoPk());
//			//fecha de vencimiento
//			modfCollEconomicRigth.setDueDate(CommonsUtilities.convertStringtoDate(registre.getExperitationDate()));
//			//Fecha de cobro
//			modfCollEconomicRigth.setPaymentDate(CommonsUtilities.convertStringtoDate(registre.getPaymentDate()));
//			modfCollEconomicRigth.setAlternateCode(registre.getAlternateCode());
//			modfCollEconomicRigth.setIdSecurityCodeFk(registre.getIdSecurityCodePk());
//			modfCollEconomicRigth.setCurrency(registre.getCurrency());
//			//cantidad de valores
//			modfCollEconomicRigth.setAmountReceivable(registre.getAmountChargeFilter().getAmount());
//			//interes
//			modfCollEconomicRigth.setInterest(registre.getInterestRate());
//			//amortizacion
//			modfCollEconomicRigth.setAmortization(registre.getAmortization());
//			//monto total
//			modfCollEconomicRigth.setAmountReceivable(registre.getTotalAmount());
//			//check
//			modfCollEconomicRigth.setCheckOther(registre.getAmountChargeFilter().getAmount().equals(registre.getTotalAmount())?0:1);
//			//Otra moneda de cobro
//			modfCollEconomicRigth.setAnotherCurency(registre.getCurrencyFilter().getCurrency());
//			modfCollEconomicRigth.setIdGeneralPurposePk(registre.getIdGeneralPurposePk());
//			modfCollEconomicRigth.setAccountNumber(registre.getAccountNumber());
//			modfCollEconomicRigth.setIdHolderAccountPk(registre.getIdHolderAccountPk());
//			modfCollEconomicRigth.setRegistreDate(CommonsUtilities.currentDate());
//			//monto a cobrar
//			modfCollEconomicRigth.setAnotherAmountReceivable(registre.getAmountChargeFilter().getAmount());
//			modfCollEconomicRigth.setIdBankFk(registre.getBankFilter().getIdBankPkSelected());
//			modfCollEconomicRigth.setDepAccountNumber(registre.getAccountFilter().getAccountSelected());
//			modfCollEconomicRigth.setAmountSecurities(registre.getAvailableBalance());
//			modfCollEconomicRigth.setLastModifyApp(28338);
//			modfCollEconomicRigth.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
//			modfCollEconomicRigth.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
//			modfCollEconomicRigth.setLastModifyDate(CommonsUtilities.currentDate());
//			modfCollEconomicRigth.setCouponNumber(registre.getCouponNumber());
//			bean.saveModfCollectionEconomicRigth(modfCollEconomicRigth);
		}
		reqCollectionEconomicRight.setState(StatesConstants.MODIFIED_REQUEST_COLLECTION_ECONOMIC);
		bean.update(reqCollectionEconomicRight);
	}

	/**
	 * Carga de bancos con sus respectivas cuentas bancarias
	 */
	public List<BankAccountMasiveTO> loadBankAndDepAccountNumber(List<Long> listIdHoldersPks) {
		List<Object[]> list = bean.seachAllBanksAccounts(listIdHoldersPks);
		BankAccountMasiveTO accountMasiveTO = null;

		Bank bank = null;
		List<BankAccountMasiveTO> listResult = new ArrayList<>();
		for (Object[] objects : list) {
			bank = new Bank();
			accountMasiveTO = new BankAccountMasiveTO();
			// bank.setIdBankPk(Long.parseLong(objects[0].toString()));
			bank = bean.find(Bank.class, Long.parseLong(objects[0].toString()));
			if (objects[1] != null)
				bank.setDescription(objects[1].toString());

			accountMasiveTO.setBank(bank);

			accountMasiveTO.setCurrency(Integer.parseInt(objects[2].toString()));
			accountMasiveTO.setCurrencyDesc(objects[3].toString());

			String[] listIdBens = (objects[4].toString().split(","));
			accountMasiveTO.setIdBenBankAccountPks(listIdBens);
			accountMasiveTO.setIdBenBankAccountPksString(objects[4].toString());

			listResult.add(accountMasiveTO);
		}
		return listResult;
	}

	/**
	 * Busqueda de glosas que no sean pagado
	 * 
	 */
	public List<Object[]> getListDetailCollection(Date operationDate) {
		return bean.getListDetailCollection(operationDate);
	}

	public List<Object[]> getListFundCollection(Date operationDate) {
		return bean.getListFundCollection(operationDate);
	}
}
