package com.pradera.collection.right.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.codec.binary.Base64;

import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.ResultUrlAccusePaymentTO;
import com.pradera.collection.right.to.ResultoWsNotification;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.funds.FundsOperation;
import com.pradera.model.paymentagent.type.PaymentAgentType;
import com.pradera.notification.to.SecurityDetailObjectTO;
import com.edv.collection.economic.rigth.CollectionEconomicRigth;
import com.edv.collection.economic.rigth.DetailCollectionGloss;
import com.edv.collection.economic.rigth.NotificationReadRecord;
import com.edv.collection.economic.rigth.PaymentAgent;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class GenerateUrlNotification implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7267717822462184888L;

	private final String SECRET_KEY = "secret _key _for_notifications"; //llave para encriptar datos
	
	private String base64EncryptedString = null;
	
	private final String ALGORITHM = "DESede";
	
	@Inject
	private PraderaLogger log;
	
	@Inject
    private CollectionRigthServiceBean collectionRigthServiceBean;
	
	@Inject
	@DepositaryDataBase
	private EntityManager em;
	
	/** metodo para retornar el token desde bitly
	 * 
	 * @return */
	public String getTokenBitly() {
		StringBuilder sql = new StringBuilder();
		sql.append(" select pt.DESCRIPTION ");
		sql.append(" from PARAMETER_TABLE pt");
		sql.append(" where pt.PARAMETER_TABLE_PK = ");
		sql.append(StatesConstants.TOKEN_FROM_BITLY);
		try {
			Query q = em.createNativeQuery(sql.toString());
			Object obj = q.getSingleResult();
			if (obj != null)
				return obj.toString();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			log.info("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
	private String encrypt(String texto) {
        try {
        	base64EncryptedString ="";
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] digestOfPassword = md.digest(SECRET_KEY.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

            SecretKey key = new SecretKeySpec(keyBytes, ALGORITHM);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] plainTextBytes = texto.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            base64EncryptedString = new String(base64Bytes);

        } catch (Exception ex) {
        }
        return base64EncryptedString;
    }

	
	public PaymentAgent getContactPaymetAgent(Long idPaymentAgent){
		return em.find(PaymentAgent.class, idPaymentAgent);
	}
    private String decrypt(String textoEncriptado) throws Exception {
        String base64EncryptedString = "";
        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] digestOfPassword = md.digest(SECRET_KEY.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, ALGORITHM);

            Cipher decipher = Cipher.getInstance(ALGORITHM);
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);

            base64EncryptedString = new String(plainText, "UTF-8");

        } catch (Exception ex) {
        }
        return base64EncryptedString;
    }
    /**
     * metodo para retornar los resultaso la url de acuse de pago
     * @param parameter
     * @return
     */
    public ResultUrlAccusePaymentTO getResutWsNotificationToAccusePayment(String parameter){
    	String parameters = null;
		try {
			parameters = decrypt(parameter);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		String[] parts = parameters.split(",");
		Long idPaymentAgentFk;
		Long idParticipant;
		Integer currency;
		BigDecimal amountPayment=BigDecimal.ZERO;
		String glossCode;
		Long idBank;
		ResultUrlAccusePaymentTO result = null;
		try {
			result = new ResultUrlAccusePaymentTO();
			//id contacto agente pagador
			idPaymentAgentFk = Long.parseLong(parts[0]);
			result.setIdPaymentAgent(idPaymentAgentFk);
			
			//id participante
			idParticipant = Long.parseLong(parts[1]);
			result.setIdParticiapntPk(idParticipant);
			
			//moneda de cobro
			currency = Integer.parseInt(parts[2]);
			result.setCurrency(currency);
			
			//monto de pago
			amountPayment = new BigDecimal(parts[3]);
			result.setAmountPayment(amountPayment);
			
			//codigo de glosa
			glossCode=parts[4];
			result.setGlossCode(glossCode);
			
			//id banco
			idBank=Long.parseLong(parts[5]);
			result.setIdBank(idBank);
			
		} catch (Exception e) {
			return null;
		}
    	return result;
    }
    public ResultoWsNotification getResutWsNotification(String parameter){
    	String parameters = null;
		try {
			parameters = decrypt(parameter);
		} catch (Exception e) {
			return null;
		}
		
		String[] parts = parameters.split(",");
		Long idPaymentAgentFk;
		String idCollectionEconomicPks;
		String [] idCollections;
		Date generationDate;
		ResultoWsNotification result = null;
		
		Long idParticipant;
		Integer currency;
		BigDecimal amountPayment=BigDecimal.ZERO;
		String glossCode;
		Long idBank;
		String descPaymentAgentJuridical;
		String accountBankNumber;
		String descCuiHolder;
		try {
			result = new ResultoWsNotification();
			
			idPaymentAgentFk = Long.parseLong(parts[0]);
			result.setIdPaymentAgent(idPaymentAgentFk);
			
			idCollectionEconomicPks = parts[1];
			idCollections = idCollectionEconomicPks.split(";");
			List<Long> pks = new ArrayList<Long>();
			for (String string : idCollections) {
				pks.add(new Long(string));
			}
			result.setIdCollectionEconomicPks(pks);
			generationDate = CommonsUtilities.convertStringtoDate(parts[2],CommonsUtilities.DATE_TIME_PATTERN);
			result.setGenerationDate(generationDate);
			
			//extracion de los parametros para url de acuse de pago
			//id participante
			idParticipant = Long.parseLong(parts[3]);
			result.setIdParticiapntPk(idParticipant);
			
			//moneda de cobro
			currency = Integer.parseInt(parts[4]);
			result.setCurrency(currency);
			
			//monto de pago
			amountPayment = new BigDecimal(parts[5]);
			result.setAmountPayment(amountPayment);
			
			//codigo de glosa
			glossCode=parts[6];
			result.setGlossCode(glossCode);
			
			//id banco
			idBank=Long.parseLong(parts[7]);
			result.setIdBank(idBank);
			
			//descripcion del agente pagador juridico
			descPaymentAgentJuridical = parts[8];
			result.setDescPaymentAgentJuridical(descPaymentAgentJuridical);
			
			//nro de cuenta del banco
			accountBankNumber = parts[9];
			result.setAccountBankNumber(accountBankNumber);
			
			//descripcion cui titular
			descCuiHolder = parts[10];
			result.setDescCuiHolder(descCuiHolder);
			
		} catch (Exception e) {
			return null;
		}
    	return result;
    }
    
    /**
     * metodo para generar la url de acuse de pago para los agentes pagadores que no pagan con LIP
     * @return
     */
    public String generateUrlAcknowledgmentPayment(Long idPaymnetAgentFk, Long idParticipantPk, Integer currency, BigDecimal amountPayment, String glossCode,Long idBank){
    	StringBuilder builder = new StringBuilder();
    	
    	//en caso de que se trate de una agente pagador SOMA-BCB entonces colocar la url correspondiente a nuestra red
    	PaymentAgent pa = em.find(PaymentAgent.class, idPaymnetAgentFk);
    	if(pa.getIdPaymentAgentFk().toString().equals(PaymentAgentType.PAYMNET_AGENT_SOMA.getCode().toString())){
    		builder.append(getHostNameEdv());
    		builder.append("/iCollectionEconomic/resources/");	
    	}else{
    		builder.append(getHostNameServerCat());
        	builder.append("/edv-serviciosElectronicos/resources/");	
    	}
    	
    	builder.append("loginServiceToAccusePayment/admission?id=");
    	
    	// 0 idPaymentAgent
    	// 1 idParticipantPk
    	// 2 currency
    	// 3 amountPayment
    	// 4 glossCode
    	// 5 idBank
		String param = encrypt(idPaymnetAgentFk.toString() + "," + idParticipantPk.toString() + "," + currency.toString() + "," + amountPayment.toString() + ","+ glossCode + "," + idBank.toString());
		try {
			param = URLEncoder.encode(param, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    	builder.append(param);
    	return builder.toString();
    }
    
    public String generateUrlNotification(
    		PaymentAgent  contactPaymen,
    		List<SecurityDetailObjectTO> listSecurityDetailObjectTOs, 
    		Long idParticipantPk, 
    		Integer currency, 
    		BigDecimal amountPayment, 
    		String glossCode,
    		String descPaymentAgentJuridical,
    		String descCuiHolder,
    		boolean isNolip){
    	StringBuilder builder = new StringBuilder();
		// si es no lip asignar hostname de catelectronico
		// si es lip asignar el hostname de la edv
		if (isNolip) {
			//builder.append(getHostNameServerCat());
			//builder.append("/edv-serviciosElectronicos/resources/");
			/**Esta logica debera ser presentada en un nuevo issue
			 * se envia notificacion solo a correos internos
			 * */
			if(contactPaymen.getEmail().contains("@edv.com.bo")){
				builder.append(getHostNameEdv());
				builder.append("/iCollectionEconomic/resources/");
			}else
				return GeneralConstants.EMPTY_STRING;
			
		} else {
			builder.append(getHostNameEdv());
			builder.append("/iCollectionEconomic/resources/");
		}
    	builder.append("loginServiceToAccuseReceived/admission?id=");
    	
    	String idCollectionEconomicPks="";
    	boolean sw = true;
		for (SecurityDetailObjectTO securityDetailObjectTO : listSecurityDetailObjectTOs) {
			if (sw) {
				sw = false;
				idCollectionEconomicPks = securityDetailObjectTO.getIdCollectionEconomicPk().toString();
				continue;
			}
			idCollectionEconomicPks = idCollectionEconomicPks + ";" + securityDetailObjectTO.getIdCollectionEconomicPk();
		}
    	//0 idPaymentAgent
    	//1 idCollectionEconomicPks
    	//2 fecha
    	//============== PARAMETROS PARA URL ACUSE DE PAGO ========
    	// 3 idParticipantPk
    	// 4 currency
    	// 5 amountPayment
    	// 6 glossCode
    	// 7 idBank
    	//============ PARAMETROS PARA LA REDACCION DEL CORREO ACUSE DE PAGO, EN CASO DE SER NO LIP =======
    	// 8 descPaymentAgentJuridical
    	// 9 accountBankNumber
    	//10 descCuiHolder
		String param = encrypt(contactPaymen.getIdPaymentAgentPk().toString() + "," + idCollectionEconomicPks + ","
				+ CommonsUtilities.convertDateToString(new Date(), CommonsUtilities.DATE_TIME_PATTERN) 
				+ "," + idParticipantPk.toString() + "," + currency.toString() + "," + amountPayment.toString() 
				+ ","+ glossCode + "," + contactPaymen.getIdBank().toString()+","+descPaymentAgentJuridical+","+contactPaymen.getCurrentAccountNumber()+","+descCuiHolder);
    	try {
			param = URLEncoder.encode(param,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    	builder.append(param);
    	return builder.toString();
    }
    
    public String generateGlosaNotClassified(Long idParticipantPk,String nemonicoPart){
    	StringBuilder builder = new StringBuilder();
    	builder.append(nemonicoPart);
    	builder.append("-");
    	//correlativo
    	Long idSecuenceGlosaCollection = collectionRigthServiceBean.getCorrelativeGloss(idParticipantPk);
    	builder.append(idSecuenceGlosaCollection);
    	return builder.toString();
    }
    
    public void updateGlosa(Long idCollectionEconomicPk,Date paymentDate,String glosa){
    	CollectionEconomicRigth collectionEconomicRigth;
    	try {
    		collectionEconomicRigth = collectionRigthServiceBean.getCollectionEconomicRigth(idCollectionEconomicPk,paymentDate,glosa);
    		if(collectionEconomicRigth!=null){
    			collectionEconomicRigth.setGlosaNotClassified(glosa);
    			collectionRigthServiceBean.update(collectionEconomicRigth);
    		}else
    			log.info("CDDE ::: No se pudo actualizar la glosa para el envio PK COLLECTION :"+idCollectionEconomicPk);
    		
		} catch (ServiceException e) {
			e.printStackTrace();
		}
    }
    /**
     * obtenemos la direccion ip del servicio, posteriormente el dominio y modulo
     * */
    private String getHostNameServerCat(){
		StringBuilder builder = new StringBuilder();
		builder.append(" select p.DESCRIPTION from PARAMETER_TABLE p");
		builder.append(" where 1 = 1  ");
		//2442 constanteque indica el hostname del cat electronico
	    builder.append(" and p.PARAMETER_TABLE_PK = 2448");
	    String portName;
	    try {
	    	Query query= em.createNativeQuery(builder.toString());
			portName = (String) query.getSingleResult();	
		} catch (Exception e) {
			e.printStackTrace();
			log.info("::: Se ha producido un error en el metodo getHostNameServerCat() "+e.getMessage());
			//si no encuentra retornar un host invalido
			portName="http://error:8080";
		}
		return portName;
	}
    /**
     * metodo para obtener el hostname de la edv
     * @return
     */
    private String getHostNameEdv(){
		StringBuilder builder = new StringBuilder();
		builder.append("select s.SYSTEM_URL from SECURITY.SYSTEM s where ID_SYSTEM_PK = 2");
	    String portName;
	    try {
	    	Query query= em.createNativeQuery(builder.toString());
			portName = (String) query.getSingleResult();	
		} catch (Exception e) {
			e.printStackTrace();
			log.info("::: Se ha producido un error en el metodo getHostNameEdv() "+e.getMessage());
			//si no encuentra retornar un host invalido
			portName="http://error:8080";
		}
		return portName;
	}
    /**Actualizamos la notificacion agente/emisor
     * @param idCollectionEconomicPk Id del registro de cobro de derechos economicos
     * @param idGeneralPurposePk Id del pk CUPON INTERES para RF
     * @param paymentDate Fecha de pago del interes
     * @param state Estado que tiene la notificacion 0 enviado, 1 recepcionada o leida y null sin estado o desconocido*/    
    public void updatNotification(Long idCollectionEconomicPk, Date paymentDate,Integer state){
    	StringBuilder builder = new StringBuilder();
    	builder.append(" update CollectionEconomicRigth set notificationReading = :notificationReading ");
    	builder.append(" where 1 = 1 ");
    	builder.append(" and idCollectionEconomicPk = :idCollectionEconomicPk ");
    	
    	Query query = em.createQuery(builder.toString());
    	query.setParameter("idCollectionEconomicPk", idCollectionEconomicPk);
    	query.setParameter("notificationReading", state);
    	query.executeUpdate();
    }
    
	/** metodo para obtener una glosa a partir de su codigo de glosa
	 * 
	 * @param codeGloss
	 * @return */
	private DetailCollectionGloss getDetailCollectionGlossFromCodeGloss(String codeGloss) {
		List<DetailCollectionGloss> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select g");
		sd.append(" from DetailCollectionGloss g");
		sd.append(" where g.glosaNotClassified = :codeGloss");
		try {
			Query q = em.createQuery(sd.toString());
			q.setParameter("codeGloss", codeGloss);
			lst = q.getResultList();
			if (lst.isEmpty())
				return null;
			else
				return lst.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	/**
	 * metodo para registrar el pago de una glosa de una contacto agente pagador que NO paga con LIP
	 * @param fundsOperation
	 * @throws Exception
	 */
	public void registerPaymentGloss(FundsOperation fundsOperation) throws Exception{
		em.persist(fundsOperation);
		em.flush();
	}
    
	/** metodo para actualizar el contenido de email enviado para realizar el pago mediante url (esto es para los contacto
	 * agente pagador que NO pagan con LIP)
	 * 
	 * @param idPaymentAgentPk
	 * @param codeGloss
	 * @param contentHtmlPayment */
	public void updateNotificationPayment(Long idPaymentAgentPk, String codeGloss, String contentHtmlPayment) {
		DetailCollectionGloss gloss = getDetailCollectionGlossFromCodeGloss(codeGloss);
		if (gloss != null) {
			StringBuilder sd = new StringBuilder();
			sd.append(" update GlossNoticationDetail dg ");
			sd.append(" set dg.htmlMessagePayment = :contentHtmlPayment");
			sd.append(" where dg.idFkPaymentAgent.idPaymentAgentPk = :idPaymentAgentPk");
			sd.append(" and dg.idFkGlossCode.idDetailCollectionGlossPk = :idDetailCollectionGlossPk");
			try {
				Query q = em.createQuery(sd.toString());
				q.setParameter("contentHtmlPayment", contentHtmlPayment);
				q.setParameter("idPaymentAgentPk", idPaymentAgentPk);
				q.setParameter("idDetailCollectionGlossPk", gloss.getIdDetailCollectionGlossPk());
				q.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("::: se ha producido un error: " + e.getMessage());
			}
		} else {
			System.out.println("::: El codigo de glosa es inexistente");
		}
	}
    
    public void registreNotificationReadRecord(Long idPaymentAgentFk,List<SecurityDetailObjectTO> listSecurityDetailObjectTOs){
    	NotificationReadRecord notificationReadRecord = new NotificationReadRecord();
    	for (SecurityDetailObjectTO objectTO : listSecurityDetailObjectTOs) {
    		
    		notificationReadRecord.setIdPaymetAgentFk(idPaymentAgentFk);
        	//notificationReadRecord.setIdGeneralPurposePk(objectTO.getIdGeneralPurposePk());21/12/2018
        	notificationReadRecord.setIdSecurityCodeFk(objectTO.getIdSecurityCodePkOriginal()==null?"error nulo":objectTO.getIdSecurityCodePkOriginal().length()==0?"error vacio":objectTO.getIdSecurityCodePkOriginal());
        	notificationReadRecord.setNoticeDate(CommonsUtilities.currentDate());
        	notificationReadRecord.setLastModifyIp("192.168.100.142");
        	notificationReadRecord.setLastModifyDate(CommonsUtilities.currentDate());
        	notificationReadRecord.setLastModifyUser("ADMIN");
        	notificationReadRecord.setLastModifyApp(123L);
        	em.persist(notificationReadRecord);
        	em.flush();
		}
    }
    /**Actualizamos la notificacion agente/emisor
     * @param idCollectionEconomicPk Id del registro de cobro de derechos economicos
     * @param idGeneralPurposePk Id del pk CUPON INTERES para RF
     * @param paymentDate Fecha de pago del interes
     * @param state Estado que tiene la notificacion 0 enviado, 1 recepcionada o leida y null sin estado o desconocido*/    
    public Long searchPaymenAgentByNotificationId(String mNemonicPaymentAgent){
    	StringBuilder builder = new StringBuilder();
    	builder.append(" select pa.idPaymentAgentPk from PaymentAgent pa ");
    	builder.append(" where 1 = 1 ");
    	builder.append(" and pa.mnemonic  = :mNemonicPaymentAgent ");
    	
    	Query query = em.createQuery(builder.toString());
    	query.setParameter("mNemonicPaymentAgent", mNemonicPaymentAgent);
    	@SuppressWarnings("unchecked")
		List<Long> idPaymentAgentPks = query.getResultList();
    	return idPaymentAgentPks.get(0);
    }
}
