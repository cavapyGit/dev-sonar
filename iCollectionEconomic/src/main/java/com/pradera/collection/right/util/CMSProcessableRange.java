package com.pradera.collection.right.util;



import com.itextpdf.text.pdf.PdfSignatureAppearance;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;

/**
 *
 * @author hcoarite
 */
class CMSProcessableRange implements CMSProcessable {

    private PdfSignatureAppearance sap;
    private byte[] buf = new byte[8192];

    public CMSProcessableRange(PdfSignatureAppearance sap) {
        this.sap = sap;
    }

    public void write(OutputStream outputStream) throws IOException, CMSException {
        InputStream s = sap.getRangeStream();
        int read = 0;
        while ((read = s.read(buf, 0, buf.length)) > 0) {
            outputStream.write(buf, 0, read);
        }
    }

    public Object getContent() {
        return sap;
    }
}
