package com.pradera.collection.right.to;

import java.io.Serializable;
import java.util.List;

public class ObjectRegModfCollectionResultTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<SearchRegisterResultTO> list;
	
	private Long idRegModfCollection;
	
	public ObjectRegModfCollectionResultTO() {
	}

	public List<SearchRegisterResultTO> getList() {
		return list;
	}

	public void setList(List<SearchRegisterResultTO> list) {
		this.list = list;
	}

	public Long getIdRegModfCollection() {
		return idRegModfCollection;
	}

	public void setIdRegModfCollection(Long idRegModfCollection) {
		this.idRegModfCollection = idRegModfCollection;
	}
}
