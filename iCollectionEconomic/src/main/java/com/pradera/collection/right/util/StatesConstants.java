package com.pradera.collection.right.util;

public class StatesConstants {
	
	//#### MASTER_TABLE : 1191 ###
	//Id de parametro del token otorgado por Bitly
	public static final Integer TOKEN_FROM_BITLY	= 2445;//	ID TOKEN OTERGADO POR BILTY
	
	//#### MASTER_TABLE : 1180 ###
	//Registros para usuarios agente pagador(ESTADOS)
	public static final Integer REGISTERED_PAYER_AGENT	= 2372;//	REGISTRADO
	
	public static final Integer CONFIRMED_PAYER_AGENT = 2373;  //   CONFIRMED
	
	public static final Integer REJECTED_PAYER_AGENT = 2374;   //    REJECTED
	
	public static final Integer BLOCKED_PAYER_AGENT = 2375;	   //    BLOCKED
	
	//#### MASTER_TABLE : 1182 ###
	//Registros para estados cuentas/beneficiario bancos(ESTADOS)
	public static final Integer REGISTERED_BANK_ACCOUNT	= 2381;//	REGISTRADO
	
	public static final Integer CONFIRMED_BANK_ACCOUNT = 2382;  //   CONFIRMED
	
	public static final Integer REJECTED_BANK_ACCOUNT = 2383;   //    REJECTED
	
	public static final Integer BLOCKED_BANK_ACCOUNT = 2384;	 //    BLOCKED
	
	//#### MASTER_TABLE : 1183 ###
	//Registro de solicitud de cobro de derecho economico
	public static final Integer REGISTERED_REQUEST_COLLECTION_ECONOMIC = 2385;
	
	public static final Integer REJECTED_REQUEST_COLLECTION_ECONOMIC = 2386;
			
	public static final Integer CONFIRMED_REQUEST_COLLECTION_ECONOMIC = 2387;
	
	public static final Integer MODIFIED_REQUEST_COLLECTION_ECONOMIC = 2388;
	
	
	//##MASTER TABLE : 1184
	//MOdificacion de una solicitud de cobro
	
	public static final Integer REGISTERED_MODIFY_REQUEST_COLLECTION_ECONOMIC = 2389;
	
	public static final Integer CONFIRMED_MODIFY_REQUEST_COLLECTION_ECONOMIC = 2390;
	
	
			
	public static final Integer REGISTERED_COLLECTION_ECONOMIC = 2391;
	
	public static final Integer REJECTED_COLLECTION_ECONOMIC = 2405;
	
	public static final Integer MODIFIED_COLLECTION_ECONOMIC = 2392;
	
	/*Master de estado para igualacion de glosas*/
	//master table 1197
	public static final Integer REGISTERED_MATCH_GLOSSES = 2464;  //REGISTRADO
	
	public static final Integer CONFIRMED_MATCH_GLOSSES  = 2465;  //CONFIRMED
	
	public static final Integer REJECT_MATCH_GLOSSES = 2466;      //RECHAZADO
}
