package com.pradera.collection.right.filter;

import java.util.List;

import com.pradera.model.accounts.holderaccounts.Bank;

public class CollectionBankFilter extends FilterSearch {

	/**
	 * 
	 */
	//filtro banco 
	private static final long serialVersionUID = 1L;
	
	private List<Bank> listBankProvider;

	private Long idBankPkSelected;
	
	public CollectionBankFilter() {
	}

	public List<Bank> getListBankProvider() {
		return listBankProvider;
	}

	public void setListBankProvider(List<Bank> listBankProvider) {
		this.listBankProvider = listBankProvider;
	}

	public Long getIdBankPkSelected() {
		return idBankPkSelected;
	}

	public void setIdBankPkSelected(Long idBankPkSelected) {
		this.idBankPkSelected = idBankPkSelected;
	}
}
