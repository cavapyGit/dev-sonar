package com.pradera.collection.right.filter;

import java.math.BigDecimal;

public class AmountChargeFilter extends FilterSearch {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal amount;

	public AmountChargeFilter() {
		
	}

	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
