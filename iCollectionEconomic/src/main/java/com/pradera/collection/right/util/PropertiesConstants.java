package com.pradera.collection.right.util;

public class PropertiesConstants {
	
	public static final String PARAM_ID_PARTICIPANT_PK = "paramParticipantPk";
	
	public static final String PARAM_PAYMENT_DATE = "paramPaymentDate";
	
	public static final String PARAM_CURRENCY = "paramCurrency";
	
	public static final String PARAM_AMOUNT = "paramAmount";
	
	public static final String PARAM_GLOSS = "paramGloss";
	
	public static final String PARAM_USER_NAME ="paramUserName";
	
	public static final String PARAM_ID_BANK_PK = "paramIdBankPk";
	
	public static final String PARAM_ID_COLLECTION_ECONOMIC_PK = "paramIdCollectionEconomicPk";
	
	public static final String PARAM_ACCOUNT_NUMBER = "paramAccountNumber";
	
	
	public static final String ERROR_RECORD_NOT_SELECTED="error.record.not.selected";
	
	public static final String ERROR_NOT_STATE_REGISTERED = "error.not.state.registered";
	
	public static final String ALERT_REJECT_PAYER_AGENT = "alert.reject.payer.agent";
	
	public static final String ALERT_REJECT_PAYMENT_AGENT = "alert.reject.payment.agent";
	
	public static final String ALERT_REJECT_NOTIFICATION = "alert.reject.notification";
	
	public static final String ALERT_REJECT_NOTIFICATION_PAYMENT_AGENT = "alert.reject.notification.payment.agent";
	
	public static final String ALERT_CONFIRM_NOTIFICATION = "alert.confirm.notification";
	
	public static final String ALERT_CONFIRM_NOTIFICATION_PAYMENT_AGENT = "alert.confirm.notification.payment.agent";
	
	public static final String ALERT_CONFIRM_NOTIFICATION_CONFIRM_PAYMENT_AGENT = "alert.confirm.notification.payment.agent.succesfull";
	
	public static final String ALERT_CONFIRM_NOTIFICATION_CONFIRM = "alert.confirm.notification.succesfull";
	
	public static final String ALERT_SUCCESFULL_REGISTRE_AGENT = "alert.confirm.notification.registre.agent.succesfull";
	
	public static final String ALERT_SUCCESFULL_REGISTRE_PAYMENT_AGENT = "alert.confirm.notification.registre.payment.agent.succesfull";
	
	public static final String ALERT_NOTIFY_CONTACT_NO_ENABLE = "alert.notify.contact.no.enable";
	
	public static final String ALERT_CONFIRM_BLOCK_PAYER_AGENT = "alert.confirm.block.payer.agent";
	
	public static final String ALERT_CONFIRM_BLOCK_PAYMENT_AGENT = "alert.confirm.block.payment.agent";
	
	//Se registró satisfactoriamente el desbloqueo del Contacto Agente Pagador {0}
	public static final String ALERT_CONFIRM_UNBLOCK_REGISTRE_PAYER_AGENT = "alert.confirm.registre.unBlock.agent";
	
	public static final String ALERT_CONFIRM_REGISTRE_BLOCK_AGENT = "alert.confirm.registre.block.agent";
	
	public static final String ALERT_CONFIRM_REGISTRE_BLOCK_PAYMENT_AGENT = "alert.confirm.registre.block.payment.agent";
	
	public static final String ALERT_CONFIRM_REGISTRE_BLOCK_AGENT_PAYMENT = "alert.confirm.registre.block.agent.payment";
	
	public static final String ALERT_CONFIRM_LOOK_PAYER_AGENT =  "alert.confirm.look.payer.agent";
	
	public static final String ALERT_CONFIRM_LOOK_PAYMENT_AGENT =  "alert.confirm.look.payment.agent";
	
	public static final String ALERT_CONFIRM_UNLOOK_PAYER_AGENT = "alert.confirm.unlook.payer.agent";
	
	public static final String ALERT_CONFIRM_RGISTRY_UNLOOK_PAYER_AGENT = "alert.confirm.registre.unblock.agent.payment";
	
	public static final String ALERT_CONFIRM_UNLOOK_PAYMENT_AGENT = "alert.confirm.unlook.payment.agent";
	
	public static final String ALERT_CONFIRM_UNLOCK_PAYMENT_AGENT = "alert.correct.un.block.payment.agent";
		
	public static final String ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN = "alert.confirm.clean.registre.screen";
	
	public static final String ALERT_CONFIRM_BACK_REGISTRE_SCREEN = "alert.confirm.back.registre.screen";
	
	public static final String ALERT_CONFIRM_REGISTRE_MODIFY = "alert.confirm.registre.modify.contact";
	
	public static final String ALERT_REGISTERED_MODIFICATION_AGENT = "alert.registered.modification.successfully";
	
	public static final String ALERT_REGISTERED_MODIFICATION_PAYMENT_AGENT = "alert.registered.modification.successfully.payment.agent";
	
	public static final String ALERT_MESSAGE_SAVE_CONFIRM_NOTIFY = "lbl.message.alert.save.confirm.notify";
	
	public static final String ALERT_MESSAGE_NO_SELECT = "lbl.message.alert.no.select";
	
	public static final String ALERT_NO_REGISTRED_SELECT = "lbl.message.alert.no.registered";
	
	public static final String CONFIRM_BANK_ACCOUNT = "lbl.confirm.regstre.bank.account";
	
	public static final String CONFIRM_SUCCESFULL_BANK_ACCOUNT = "lbl.successful.confirmation.bank.account";
	
	public static final String CONFIRM_REJECT_DIALOG_BANK_ACCOUNT = "lbl.alert.confirm.dialog.reject.bank.account";
	
	public static final String CONFIRM_DIALOG_REJECT_SUCCESFULL =  "lbl.alert.successful.confirm.reject";
	
	public static final String CONFIRM_DIALOG_LOOK_BANK_ACCOUNT = "lbl.alert.confirm.block.bank.account";
	
	public static final String CONFIRM_SUCCESFULL_LOOK_BANK_ACCOUNT = "lbl.alert.successful.look.bank.account";
	
	public static final String CONFIRM_DIALOG_UN_BLOCK_BANK_ACCOUNT = "lbl.alert.confirm.un.block";
	
	public static final String CONFIRM_DIALOG_UN_BLOCK_SUCCESFULL =  "lbl.alert.confirm.successful.un.block";
}