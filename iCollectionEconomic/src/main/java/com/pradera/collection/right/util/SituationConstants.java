package com.pradera.collection.right.util;

public class SituationConstants {
	/*CONTACTOS AGENTE PAGADOR*/
	public static final Integer ACTIVE_AGENT_CONTACT_PAYMENT = 2376;//	ACTIVO
	
	public static final Integer PENDING_TO_MODIFY_CONTACT_PAYMET = 2377;//	PENDIENTE DE MODIFICAR
	
	public static final Integer PENDING_LOCK_CONTACT_PAYMENT = 2378 ;//PENDIENTE DE BLOQUEO
	
	public static final Integer UNLOCKING_PENDING_CONTACT_PAYMENT = 2380; //PENDIENTE DE DESBLOQUEO

	//siatuasio para registro de cuentas bancarias
	/**
	 * 2394,ACTIVO
	   2395,PENDIENTE DE BLOQUEO
	   2396,PENDIENTE DE DESBLOQUEO
	*/
	//public static final Integer ACTIVE_BANK_ACCOUNT = 2394;//	ACTIVO
	
	//public static final Integer PENDING_TO_MODIFY_BANK_ACCOUNT = 2386;//	PENDIENTE DE MODIFICAR
	
	public static final Integer PENDING_LOCK_BANK_ACCOUNT = 2395 ;//PENDIENTE DE BLOQUEO
	
	public static final Integer UNLOCKING_PENDING_BANK_ACCOUNT = 2396; //PENDIENTE DE DESBLOQUEO
}
