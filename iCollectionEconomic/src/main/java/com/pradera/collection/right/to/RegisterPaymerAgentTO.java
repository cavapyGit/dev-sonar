package com.pradera.collection.right.to;

public class RegisterPaymerAgentTO {

	private Integer idPaymetAgent;
	
	public RegisterPaymerAgentTO() {
	}

	public Integer getIdPaymetAgent() {
		return idPaymetAgent;
	}

	public void setIdPaymetAgent(Integer idPaymetAgent) {
		this.idPaymetAgent = idPaymetAgent;
	}
	
}
