package com.pradera.collection.right.to;

import java.io.Serializable;
import java.util.List;

public class DepAccountNumberTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3718140668942212713L;

	private Long idBenDepAccountNumber;
	
	private List<String> listDepAccountNumbers;

	public Long getIdBenDepAccountNumber() {
		return idBenDepAccountNumber;
	}

	public void setIdBenDepAccountNumber(Long idBenDepAccountNumber) {
		this.idBenDepAccountNumber = idBenDepAccountNumber;
	}

	public List<String> getListDepAccountNumbers() {
		return listDepAccountNumbers;
	}

	public void setListDepAccountNumbers(List<String> listDepAccountNumbers) {
		this.listDepAccountNumbers = listDepAccountNumbers;
	}
}
