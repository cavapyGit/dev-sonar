package com.pradera.collection.right.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

import com.pradera.collection.right.facade.CollectionRigthFacade;
import com.pradera.collection.right.service.CollectionRigthServiceBean;
import com.pradera.collection.right.to.BankAccountMasiveTO;
import com.pradera.collection.right.to.RegisterCollectionRightTO;
import com.pradera.collection.right.to.SearchRegisterResultTO;
import com.pradera.collection.right.util.PropertiesConstants;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.Bank;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.edv.collection.economic.rigth.BeneficiaryBankAccount;


@DepositaryWebBean
@LoggerCreateBean
public class RegisterCollectionRightMgmtBean extends GenericBaseBean {

	/**
	* 
	 * 	
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	ParticipantServiceBean bean;

	@Inject
	private ParameterServiceBean parameterServiceBean;

	@Inject
	private CollectionRigthServiceBean collectionRigthServiceBean;

	@Inject
	private CollectionRigthFacade facade;

	@Inject
	private UserInfo userInfo;
	
	@Inject
	private HolidayQueryServiceBean holidayQueryServiceBean;

	private RegisterCollectionRightTO registerFilterTO;

	private List<Participant> listParticipants;

	private List<ParameterTable> listTypeIncome;

	private List<ParameterTable> lstCurrency;

	private List<ParameterTable> lstSecurityClass;

	private List<SearchRegisterResultTO> listResult;

	private List<SearchRegisterResultTO> listResultNotAvailableBalance;

	private GenericDataModel<HolderAccountHelperResultTO> lstAccountTo;

	private boolean renderedListNotAvailable;

	private SearchRegisterResultTO selectedSearchRegisterResultTO;

	private GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs;

	private boolean visibleResult;

	private String messageNoAvailableBalance = CommonsUtilities.STR_BLANK;

	private Date dateRF;

	/** Variables para registro masivo BANCOO */
	private BankAccountMasiveTO bankAccountMasiveTO;

	private String idBenAccountPkSelecteds;

	private List<BankAccountMasiveTO> lisAccountMasiveTOs;

	/** Num de cuenta */
	private Long idBenAccontSelected;

	private BeneficiaryBankAccount accountSelected;

	private List<BeneficiaryBankAccount> listBenAccountMasive = new ArrayList<>();
	
	private Map<Long, SearchRegisterResultTO> listSelecteds;

	@PostConstruct
	public void init() {
		registerFilterTO = new RegisterCollectionRightTO();
		listSelecteds = null;
		try {
			listParticipants = parameterServiceBean.getListParticipantForCollection();
			if (userInfo.getUserAccountSession().isParticipantInstitucion()) {
				registerFilterTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
			listTypeIncome = new ArrayList<>(2);
			listTypeIncome.add(parameterServiceBean.getParameterTableById(InstrumentType.FIXED_INCOME.getCode()));
			listTypeIncome.add(parameterServiceBean.find(ParameterTable.class, InstrumentType.VARIABLE_INCOME.getCode()));
			registerFilterTO.setTypeIncome(InstrumentType.FIXED_INCOME.getCode());
			registerFilterTO.setRenderedRV(BooleanType.NO.getBooleanValue());
			lstCurrency = parameterServiceBean.getListParameterTableServiceBean(MasterTableType.CURRENCY.getCode());
			lstSecurityClass = facade.getSecurityClassList(registerFilterTO.getTypeIncome());
			registerFilterTO.setInitialDate(CommonsUtilities.currentDate());
			registerFilterTO.setFinalDate(CommonsUtilities.currentDate());
			registerFilterTO.setMaxDate(dateRF);
			registerFilterTO.setMinDate(CommonsUtilities.currentDate());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		visibleResult = false;
	}

	/**
	 *  Tipo de RF y RV 
	 *  
	 *  */
	public void changeTypeIncome() {
		lstSecurityClass = facade.getSecurityClassList(registerFilterTO.getTypeIncome());
		if (registerFilterTO.getTypeIncome().equals(InstrumentType.FIXED_INCOME.getCode())) {
			registerFilterTO.setMaxDate(dateRF);
			registerFilterTO.setRenderedRV(BooleanType.NO.getBooleanValue());
		} else {
			registerFilterTO.setPaymentAmount(BigDecimal.ZERO);
			registerFilterTO.setPaymentDate(CommonsUtilities.currentDate());
			registerFilterTO.setInitialDate(CommonsUtilities.currentDate());
			registerFilterTO.setRenderedRV(BooleanType.YES.getBooleanValue());
		}
		lisRegisterResultTOs = new GenericDataModel<SearchRegisterResultTO>();
		JSFUtilities.updateComponent("frmSearchRegistreCollection:idLabelDateInitial");
	}

	public void clearAllFilters() {
		registerFilterTO = new RegisterCollectionRightTO();
		if (userInfo.getUserAccountSession().isParticipantInstitucion()) {
			registerFilterTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		registerFilterTO.setTypeIncome(InstrumentType.FIXED_INCOME.getCode());
		listResultNotAvailableBalance = null;
		renderedListNotAvailable = false;
		idBenAccontSelected = null;
		//init();
	}

	/**
	 * Busqueda de vencimientos
	 */
	public void searchFromFilters() {
		listSelecteds = null;
		lisRegisterResultTOs = null;
		listResult = null;
		messageNoAvailableBalance = CommonsUtilities.STR_BLANK;
		if (registerFilterTO.getIdParticipantPk() == null) {
			showMessageAlert("lbl.title.collection.data.not.complete");
			JSFUtilities.updateComponent("idParRegCollectionEconomic");
			return;
		}
		listResult = new ArrayList<SearchRegisterResultTO>();
		try {
			listResult = facade.getListSecuritiesToPay(registerFilterTO);
			if (listResult != null && listResult.size() > 0)
				visibleResult = false;
			else
				visibleResult = true;
			JSFUtilities.updateComponent("idVisibleReqCollection");
		} catch (Exception e) {
			e.printStackTrace();
		}
		lisRegisterResultTOs = new GenericDataModel<>(listResult);
		messageNoAvailableBalance = CommonsUtilities.STR_BLANK;
		try {
			listResultNotAvailableBalance = facade.getListSecuritiesToPayNotAvailableBalance(registerFilterTO);
			StringBuilder builder = new StringBuilder();
			if (listResultNotAvailableBalance.size() > 0) {
				renderedListNotAvailable = true;
				for (SearchRegisterResultTO element : listResultNotAvailableBalance) {
					String typeBalance = collectionRigthServiceBean.descHolderAccountBalance(element);
					builder.append(element.getIdSecurityCodePk() + " " + typeBalance + " " + element.getDueDate()+ ", ");
				}
				messageNoAvailableBalance = builder.toString().substring(0,builder.toString().length() - 2);
			} else
				renderedListNotAvailable = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		/** Cargando titulares y nuemeros de cuenta para el registro masivo */
		Map<Long, Long> registeredHolders = new HashMap<>();
		for (SearchRegisterResultTO registerResultTO : lisRegisterResultTOs)
			registeredHolders.put(registerResultTO.getIdHolderPk(), registerResultTO.getIdHolderPk());

		if (registeredHolders.size() == 0)
			return;
		/** Obtenermos las lista e bancos a cargar */
		List<Long> listHoldersForRegistreMasive = new ArrayList<>(registeredHolders.values());
		lisAccountMasiveTOs = facade.loadBankAndDepAccountNumber(listHoldersForRegistreMasive);
		idBenAccontSelected = null;
		listSelecteds = new HashMap<Long, SearchRegisterResultTO>();
	}

	/**
	 * Racargamos las listas de Bancos segun la nueva moneda seleccionada
	 * 
	 */
	public void currencyChangeEvent(ValueChangeEvent event) {
		SearchRegisterResultTO registerResultTO = new SearchRegisterResultTO();
		registerResultTO = (SearchRegisterResultTO) event.getComponent().getAttributes().get("selectedObject");
		Integer currency = (Integer) event.getNewValue();

		List<Bank> listBankProvider;
		for (SearchRegisterResultTO mainObject : lisRegisterResultTOs) {
			if (mainObject.getIdGeneralPurposePk().equals(registerResultTO.getIdGeneralPurposePk())) {
				mainObject.getCurrencyFilter().setCurrency(currency);
				// participante cui moneda
				listBankProvider = facade.reSearchBankProvider(mainObject.getIdParticipantPk(),	mainObject.getAccountNumber(), currency);
				mainObject.getBankFilter().setListBankProvider(listBankProvider);
				mainObject.getBankFilter().setIdBankPkSelected(null);
				mainObject.getAccountFilter().setAccountList(null);
				mainObject.getAccountFilter().setAccountSelected(null);
			}
		}
	}

	/**
	 * Buscamos la cuentas bancarias registradas sengun el banco seleccionado
	 * 
	 * @param event Evento camputarado desde el *.xhtml parametro
	 *              #{searchRegisterResultTO}
	 */
	public void bankChangeEvent(ValueChangeEvent event) {
		SearchRegisterResultTO registerResultTO = new SearchRegisterResultTO();
		registerResultTO = (SearchRegisterResultTO) event.getComponent().getAttributes().get("selectedObject");
		Long idBankPk = (Long) event.getNewValue();
		List<String> accountList;
		for (SearchRegisterResultTO mainObject : lisRegisterResultTOs) {
			if (mainObject.getIdGeneralPurposePk().equals(registerResultTO.getIdGeneralPurposePk())) {
				mainObject.getBankFilter().setIdBankPkSelected(idBankPk);
				/**
				 * Si estuviera seleccionado otra moneda de cobro, buscar cuentas bancarias de
				 * esa moneda
				 */
				if (mainObject.isCheckOther())
					accountList = facade.reSearchAccountNumber(mainObject.getIdParticipantPk(),mainObject.getAccountNumber(), idBankPk, mainObject.getCurrencyFilter().getCurrency());
				else
					accountList = facade.reSearchAccountNumber(mainObject.getIdParticipantPk(),mainObject.getAccountNumber(), idBankPk, mainObject.getCurrency());
				mainObject.getAccountFilter().setAccountList(accountList);
				mainObject.getAccountFilter().setAccountSelected(null);
				return;
			}
		}
	}

	/**
	 * Actualizamos la lista de bancos cuando se selecciona un nuevo banco
	 */
	public String outComePageSearch() {
		return "pageSearchCollectionEconomic";
	}

	/**
	 * Confirmacion de registro de cobro de derechos economicos Se abre el dialogo
	 * de confirmacion para el registro de cobro de derechos economicos
	 */
	public void preSaveCharges() {
		if(registerFilterTO.getTypeIncome().equals(InstrumentType.VARIABLE_INCOME.getCode())) {
			if(listSelecteds.size()==0) {
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
				String bodyMessage = PropertiesUtilities.getMessage("error.record.not.selected");
				JSFUtilities.putViewMap("bodyMessageView", bodyMessage);
				JSFUtilities.putViewMap("headerMessageView", headerMessage);
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			listResult = new ArrayList<SearchRegisterResultTO>(listSelecteds.values());
			for (SearchRegisterResultTO register : listResult) {
				if (register.getAmountChargeFilter().getAmount() ==null||register.getAmountChargeFilter().getAmount().compareTo(BigDecimal.ZERO) <= 0) {
					String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.required.amount.great.zero");
					showMessageOnDialogRegCollection(headerMessage, bodyMessage);
					JSFUtilities.updateComponent("opnlDialogs");
					JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqAmountGreatZero').show();");
					return;
				}
			}
		}else {
			for (SearchRegisterResultTO register : lisRegisterResultTOs) {
				if (register.getAmountChargeFilter().getAmount().compareTo(BigDecimal.ZERO) <= 0) {
					String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.required.amount.great.zero");
					showMessageOnDialogRegCollection(headerMessage, bodyMessage);
					JSFUtilities.updateComponent("opnlDialogs");
					JSFUtilities.executeJavascriptFunction("PF('wvDialogModfReqAmountGreatZero').show();");
					return;
				}
			}
		}
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
		String bodyMessage = PropertiesUtilities.getMessage("lbl.alert.message.confirm.registration.collection.economic.right");
		showMessageOnDialogRegCollection(headerMessage, bodyMessage);
		JSFUtilities.updateComponent("opnlDialogs");
		JSFUtilities.executeJavascriptFunction("PF('wVConfirmRegistreCollection').show();");
	}

	/**
	 * Cargamos los datos de los seleccionados en otra moneda de cobro en la lista
	 * principal Ademas verificamos que los datos se hayan ingresado correctamente.
	 */
	public void confirmRegistreCollectionDialogConfirmYes() {
		for (SearchRegisterResultTO mainObject : listResult) {
			if (mainObject.getBankFilter().getIdBankPkSelected() == null
					|| mainObject.getAccountFilter().getAccountSelected() == null) {
				showMessageAlert("lbl.requiered.message.alert.bank.account");
				return;
			}
		}
		lisRegisterResultTOs = new GenericDataModel<SearchRegisterResultTO>(listResult);
		saveAllCollectionEcomic(lisRegisterResultTOs, registerFilterTO);
	}

	/**
	 * Registramos las solicitudes de cobro de derecho economico Se muestra mensaje
	 * de alerta en caso no de poder registrar las solicitudes
	 * 
	 * @param lisRegisterResultTOs Lista de los cobros
	 * @param registerFilterTO     filtros de la pantalla
	 */
	private void saveAllCollectionEcomic(GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs,RegisterCollectionRightTO registerFilterTO) {
		try {
			facade.saveAllCollectionEconomic(lisRegisterResultTOs, registerFilterTO);
			lisRegisterResultTOs = null;
			listResult = null;
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage("lbl.message.successful.registre.collection.economic");
			showMessageOnDialogRegCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVAlertRegistreCollection').show();");
		} catch (Exception e) {
			showMessageAlert("lbl.alert.no.registre.collection.economic");
		}
		exeCleanPageSearchCollectionEconomicDialog();
	}

	/**
	 * Verificamos los cambios modificados en los filtros de busqueda Si no hubiese
	 * existido cambios en los filtros, se regresa a la pantalla de busqueda
	 **/
	public String backPageSearchCollectionEconomicDialog() {
		if (isModifiedSearchFilstersCollectionEconomic()) {
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_BACK_REGISTRE_SCREEN);
			showMessageOnDialogRegCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVBackSearchCollectionEconomic').show();");
			return null;
		} else {
			lisRegisterResultTOs = null;
			visibleResult = false;
			clearAllFilters();
			JSFUtilities.updateComponent("frmSearchRegistreCollection");
			return "loadSearchPageCollectionEconomic";
		}
	}

	/**
	 * Verificamos los cambios modificados en los filtros de busqueda Si no hubiese
	 * existido cambios en los filtros, se limpia la pantalla de busqueda
	 **/
	public String cleanPageSearchCollectionEconomicDialog() {
		if (isModifiedSearchFilstersCollectionEconomic()) {
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CONFIRM_CLEAN_REGISTRE_SCREEN);
			showMessageOnDialogRegCollection(headerMessage, bodyMessage);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('wVCleanSearchCollectionEconomic').show();");
			return null;
		} else
			exeCleanPageSearchCollectionEconomicDialog();
		return null;
	}
	
	public String returnCleaningScreen() {
		listResult = null;
		lisRegisterResultTOs = new GenericDataModel<SearchRegisterResultTO>();
		listResultNotAvailableBalance = null;
		renderedListNotAvailable = false;
		return "loadSearchPageCollectionEconomic";
	}

	public void exeCleanPageSearchCollectionEconomicDialog() {
		listResult = null;
		lisRegisterResultTOs = new GenericDataModel<SearchRegisterResultTO>();
		listResultNotAvailableBalance = null;
		renderedListNotAvailable = false;
		init();
		JSFUtilities.updateComponent("frmSearchRegistreCollection");
	}

	private boolean isModifiedSearchFilstersCollectionEconomic() {
		if (!userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
				|| !userInfo.getUserAccountSession().isParticipantInstitucion())
			if (registerFilterTO.getIdParticipantPk() != null)
				return true;
		if (registerFilterTO.getIssuer().getIdIssuerPk() != null)
			return true;
		if (registerFilterTO.getSecurityClass() != null)
			return true;
		if (registerFilterTO.getSecurity().getIdSecurityCodePk() != null)
			return true;
		if (registerFilterTO.getCurrency() != null)
			return true;
		else
			return false;
	}

	/**
	 * Mensaje de alerta
	 */
	private void showMessageAlert(String propertieString) {
		showMessageOnDialogRegCollection(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(propertieString));
		JSFUtilities.showSimpleValidationDialog();
	}

	private void showMessageOnDialogRegCollection(String headerMessageConfirmation, String bodyMessageConfirmation) {
		JSFUtilities.putViewMap("bodyMessageView", bodyMessageConfirmation);
		JSFUtilities.putViewMap("headerMessageView", headerMessageConfirmation);
	}
	
	
	public void checkChargeSecurity(SearchRegisterResultTO mainObject) {
		if(mainObject!=null)
		if(mainObject.isCheckCharge())
			listSelecteds.put(mainObject.getIdGeneralPurposePk(),mainObject);
		else
			listSelecteds.remove(mainObject.getIdGeneralPurposePk());
	}

	/**
	 * Evento de cuando seleccionamos otra moneda de cobro Se habilitan y
	 * deshabilitan opsiones
	 */
	public void checkCollectionRecord(SearchRegisterResultTO mainObject) {
		if (mainObject.isCheckOther()) {
			List<Bank> listBankProvider;
			for (SearchRegisterResultTO resultTO : lisRegisterResultTOs) {
				if (mainObject.getIdGeneralPurposePk().equals(resultTO.getIdGeneralPurposePk())) {
					mainObject.getCurrencyFilter().setDisabled(false);
					mainObject.getAmountChargeFilter().setDisabled(false);
					if (mainObject.getTotalAmount().equals(mainObject.getAmountChargeFilter().getAmount())) {
						// mainObject.getAmountChargeFilter().setAmount(BigDecimal.ZERO);
						/** Calculando monto a cobrar */
						if (mainObject.getCurrency().equals(CurrencyType.UFV.getCode())) {
							Date expiritationDate = CommonsUtilities.convertStringtoDate(mainObject.getDueDate());
							DailyExchangeRates rate = parameterServiceBean.getDayExchangeRate(expiritationDate,	CurrencyType.UFV.getCode());
							if (rate == null) {
								showMessageAlert("label.alert.no.exist.rate.exchange");
								return;
							}
							mainObject.getAmountChargeFilter().setAmount(rate.getBuyPrice().multiply(mainObject.getTotalAmount()));
						}
						listBankProvider = facade.reSearchBankProvider(mainObject.getIdParticipantPk(),mainObject.getAccountNumber(), mainObject.getCurrencyFilter().getCurrency());
						mainObject.getBankFilter().setListBankProvider(listBankProvider);
						mainObject.getBankFilter().setIdBankPkSelected(null);
						mainObject.getAccountFilter().setAccountList(null);
						mainObject.getAccountFilter().setAccountSelected(null);
					}
					return;
				}
			}
		} else {
			List<Bank> listBankProvider;
			for (SearchRegisterResultTO resultTO : lisRegisterResultTOs) {
				if (mainObject.getIdGeneralPurposePk().equals(resultTO.getIdGeneralPurposePk())) {
					mainObject.getCurrencyFilter().setDisabled(true);
					mainObject.getAmountChargeFilter().setDisabled(true);
					mainObject.getAmountChargeFilter().setAmount(mainObject.getTotalAmount());
					Integer currencyBack = mainObject.getCurrency();
					mainObject.getCurrencyFilter().setCurrency(currencyBack);
					if (mainObject.getBankFilter().getIdBankPkSelected() == null) {
						listBankProvider = facade.reSearchBankProvider(mainObject.getIdParticipantPk(),mainObject.getAccountNumber(), mainObject.getCurrency());
						mainObject.getBankFilter().setListBankProvider(listBankProvider);
						mainObject.getBankFilter().setIdBankPkSelected(null);
						mainObject.getAccountFilter().setAccountList(null);
						mainObject.getAccountFilter().setAccountSelected(null);
					}
					return;
				}
			}
		}
	}

	/** Holder */
	public void holderChangeEvent() {
	}

	public List<Participant> getListParticipants() {
		return listParticipants;
	}

	public void setListParticipants(List<Participant> listParticipants) {
		this.listParticipants = listParticipants;
	}

	public List<ParameterTable> getListTypeIncome() {
		return listTypeIncome;
	}

	public void setListTypeIncome(List<ParameterTable> listTypeIncome) {
		this.listTypeIncome = listTypeIncome;
	}

	public RegisterCollectionRightTO getRegisterFilterTO() {
		return registerFilterTO;
	}

	public void setRegisterFilterTO(RegisterCollectionRightTO registerFilterTO) {
		this.registerFilterTO = registerFilterTO;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	public GenericDataModel<SearchRegisterResultTO> getLisRegisterResultTOs() {
		return lisRegisterResultTOs;
	}

	public void setLisRegisterResultTOs(GenericDataModel<SearchRegisterResultTO> lisRegisterResultTOs) {
		this.lisRegisterResultTOs = lisRegisterResultTOs;
	}

	public SearchRegisterResultTO getSelectedSearchRegisterResultTO() {
		return selectedSearchRegisterResultTO;
	}

	public void setSelectedSearchRegisterResultTO(SearchRegisterResultTO selectedSearchRegisterResultTO) {
		this.selectedSearchRegisterResultTO = selectedSearchRegisterResultTO;
	}

	public boolean isVisibleResult() {
		return visibleResult;
	}

	public void setVisibleResult(boolean visibleResult) {
		this.visibleResult = visibleResult;
	}

	public boolean isRenderedListNotAvailable() {
		return renderedListNotAvailable;
	}

	public void setRenderedListNotAvailable(boolean renderedListNotAvailable) {
		this.renderedListNotAvailable = renderedListNotAvailable;
	}

	public String getMessageNoAvailableBalance() {
		return messageNoAvailableBalance;
	}

	public void setMessageNoAvailableBalance(String messageNoAvailableBalance) {
		this.messageNoAvailableBalance = messageNoAvailableBalance;
	}

	/**
	 * Buscamos la lista de cuentas bancarias segun objeto seleccionado
	 */
	public void eventChangeBank() {
		String[] listIdBens = (idBenAccountPkSelecteds.split(","));
		// System.out.println("CUENTAS : "+idBenAccountPkSelecteds);
		BeneficiaryBankAccount account = null;
		listBenAccountMasive = new ArrayList<>();
		for (int i = 0; i < listIdBens.length; i++) {
			account = new BeneficiaryBankAccount();
			account = holidayQueryServiceBean.find(BeneficiaryBankAccount.class,
					Long.parseLong(listIdBens[i].toString()));
			listBenAccountMasive.add(account);
		}
		idBenAccontSelected = null;
	}

	/**
	 * Cargar bancos y cuentas
	 * 
	 */
	public void loadBankAccounts() {
		if (idBenAccontSelected == null)
			return;
		accountSelected = holidayQueryServiceBean.find(BeneficiaryBankAccount.class, idBenAccontSelected);
		// System.out.println("ID CUENTA SELECCIONADA
		// :"+idBenAccontSelected.toString());
		if (accountSelected != null) {
			for (SearchRegisterResultTO registerResultTO : lisRegisterResultTOs) {
				// CUI
				if (accountSelected.getAccountNumber().equals(registerResultTO.getAccountNumber())
						// MONEDA
						&& accountSelected.getCurrency().equals(registerResultTO.getCurrencyFilter().getCurrency())) {
					registerResultTO.getBankFilter().setIdBankPkSelected(accountSelected.getIdBankFk());
					registerResultTO.getAccountFilter().setAccountSelected(accountSelected.getDepAccountNumber());
					ArrayList<String> list = new ArrayList<>();
					list.add(accountSelected.getDepAccountNumber());
					registerResultTO.getAccountFilter().setAccountList(list);
				}
			}

		}
	}

	public List<BeneficiaryBankAccount> getListBenAccountMasive() {
		return listBenAccountMasive;
	}

	public void setAccountSelected(BeneficiaryBankAccount accountSelected) {
		this.accountSelected = accountSelected;
	}

	/** Banco y numero de cuenta para hacer el registro masivo */
	public List<BankAccountMasiveTO> getLisAccountMasiveTOs() {
		return lisAccountMasiveTOs;
	}

	public void setLisAccountMasiveTOs(List<BankAccountMasiveTO> lisAccountMasiveTOs) {
		this.lisAccountMasiveTOs = lisAccountMasiveTOs;
	}

	public BankAccountMasiveTO getBankAccountMasiveTO() {
		return bankAccountMasiveTO;
	}

	public void setBankAccountMasiveTO(BankAccountMasiveTO bankAccountMasiveTO) {
		this.bankAccountMasiveTO = bankAccountMasiveTO;
	}

	public GenericDataModel<HolderAccountHelperResultTO> getLstAccountTo() {
		return lstAccountTo;
	}

	public void setLstAccountTo(GenericDataModel<HolderAccountHelperResultTO> lstAccountTo) {
		this.lstAccountTo = lstAccountTo;
	}

	public String getIdBenAccountPkSelecteds() {
		return idBenAccountPkSelecteds;
	}

	public void setIdBenAccountPkSelecteds(String idBenAccountPkSelecteds) {
		this.idBenAccountPkSelecteds = idBenAccountPkSelecteds;
	}

	public Long getIdBenAccontSelected() {
		return idBenAccontSelected;
	}

	public void setIdBenAccontSelected(Long idBenAccontSelected) {
		this.idBenAccontSelected = idBenAccontSelected;
	}

	public ParticipantServiceBean getBean() {
		return bean;
	}

	public void setBean(ParticipantServiceBean bean) {
		this.bean = bean;
	}

	public ParameterServiceBean getParameterServiceBean() {
		return parameterServiceBean;
	}

	public void setParameterServiceBean(ParameterServiceBean parameterServiceBean) {
		this.parameterServiceBean = parameterServiceBean;
	}

	public CollectionRigthServiceBean getCollectionRigthServiceBean() {
		return collectionRigthServiceBean;
	}

	public void setCollectionRigthServiceBean(CollectionRigthServiceBean collectionRigthServiceBean) {
		this.collectionRigthServiceBean = collectionRigthServiceBean;
	}

	public CollectionRigthFacade getFacade() {
		return facade;
	}

	public void setFacade(CollectionRigthFacade facade) {
		this.facade = facade;
	}

	public HolidayQueryServiceBean getHolidayQueryServiceBean() {
		return holidayQueryServiceBean;
	}

	public void setHolidayQueryServiceBean(HolidayQueryServiceBean holidayQueryServiceBean) {
		this.holidayQueryServiceBean = holidayQueryServiceBean;
	}

	public List<SearchRegisterResultTO> getListResult() {
		return listResult;
	}

	public void setListResult(List<SearchRegisterResultTO> listResult) {
		this.listResult = listResult;
	}

	public List<SearchRegisterResultTO> getListResultNotAvailableBalance() {
		return listResultNotAvailableBalance;
	}

	public void setListResultNotAvailableBalance(List<SearchRegisterResultTO> listResultNotAvailableBalance) {
		this.listResultNotAvailableBalance = listResultNotAvailableBalance;
	}

	public Date getDateRF() {
		return dateRF;
	}

	public void setDateRF(Date dateRF) {
		this.dateRF = dateRF;
	}

	public Map<Long, SearchRegisterResultTO> getListSelecteds() {
		return listSelecteds;
	}

	public void setListSelecteds(Map<Long, SearchRegisterResultTO> listSelecteds) {
		this.listSelecteds = listSelecteds;
	}

	public BeneficiaryBankAccount getAccountSelected() {
		return accountSelected;
	}

	public void setListBenAccountMasive(List<BeneficiaryBankAccount> listBenAccountMasive) {
		this.listBenAccountMasive = listBenAccountMasive;
	}
	
}