package com.pradera.collection.right.to;

import java.io.Serializable;

public class BankAccountTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idParticipantPk;
	
	private Integer currency;
	
	private Long idBankPk;
	
	private String accountNumber;
	
	public BankAccountTO() {
		// TODO Auto-generated constructor stub
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public Long getIdBankPk() {
		return idBankPk;
	}

	public void setIdBankPk(Long idBankPk) {
		this.idBankPk = idBankPk;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
