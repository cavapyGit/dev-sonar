package com.pradera.collection.right.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** clase para obtener los datos necesarios para realiza el deposito manual
 * 
 * @author rlarico */
public class ResultUrlAccusePaymentTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3527987721167178152L;

	private Long idPaymentAgent;
	private Long idParticiapntPk;
	private Date dateOperaion;
	private Integer currency;
	private BigDecimal amountPayment;
	private String glossCode;
	private Long IdBank;

	/** primer constructor */
	public ResultUrlAccusePaymentTO() {
	}

	// TODO methods getter and setter
	public Long getIdPaymentAgent() {
		return idPaymentAgent;
	}

	public void setIdPaymentAgent(Long idPaymentAgent) {
		this.idPaymentAgent = idPaymentAgent;
	}

	public Long getIdParticiapntPk() {
		return idParticiapntPk;
	}

	public String getGlossCode() {
		return glossCode;
	}

	public void setGlossCode(String glossCode) {
		this.glossCode = glossCode;
	}

	public Long getIdBank() {
		return IdBank;
	}

	public void setIdBank(Long idBank) {
		IdBank = idBank;
	}

	public void setIdParticiapntPk(Long idParticiapntPk) {
		this.idParticiapntPk = idParticiapntPk;
	}

	public Date getDateOperaion() {
		return dateOperaion;
	}

	public void setDateOperaion(Date dateOperaion) {
		this.dateOperaion = dateOperaion;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public BigDecimal getAmountPayment() {
		return amountPayment;
	}

	public void setAmountPayment(BigDecimal amountPayment) {
		this.amountPayment = amountPayment;
	}

	@Override
	public String toString() {
		return "ResultUrlAccusePaymentTO [idPaymentAgent=" + idPaymentAgent + ", idParticiapntPk=" + idParticiapntPk + ", dateOperaion=" + dateOperaion + ", currency="
				+ currency + ", amountPayment=" + amountPayment + ", glossCode=" + glossCode + ", IdBank=" + IdBank + "]";
	}
}