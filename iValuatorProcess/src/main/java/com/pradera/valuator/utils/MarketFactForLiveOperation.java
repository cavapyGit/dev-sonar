package com.pradera.valuator.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MarketFactForLiveOperation implements Serializable{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String securityCodePk;
	private Long participantPk;
	private Long holderAccountPk;
	private BigDecimal marketRate;
	private Date marketDate;
	private BigDecimal marketPrice;
	private BigDecimal marketQuantity;
	private Long mechanismOperationPk;
	private Long custodyOperationPk;
	private Long settlementAccOperationPk;
	private Integer role;
	private Long settlemetMarketFactPk;
	private Integer active;
	private Long mechanismType;
	
	private Integer indBlockOperation;
	private Long custodyOperationMrkFactPk;
	private BigDecimal initialQuantity;
	
	private Long physicalMrkFactPk;
	public String getSecurityCodePk() {
		return securityCodePk;
	}
	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}
	public Long getParticipantPk() {
		return participantPk;
	}
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}
	public BigDecimal getMarketRate() {
		return marketRate;
	}
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	public Date getMarketDate() {
		return marketDate;
	}
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public Long getMechanismOperationPk() {
		return mechanismOperationPk;
	}
	public void setMechanismOperationPk(Long mechanismOperationPk) {
		this.mechanismOperationPk = mechanismOperationPk;
	}
	public Integer getRole() {
		return role;
	}
	public void setRole(Integer role) {
		this.role = role;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
	public Long getCustodyOperationPk() {
		return custodyOperationPk;
	}
	public void setCustodyOperationPk(Long custodyOperationPk) {
		this.custodyOperationPk = custodyOperationPk;
	}
	public Long getSettlemetMarketFactPk() {
		return settlemetMarketFactPk;
	}
	public void setSettlemetMarketFactPk(Long settlemetMarketFactPk) {
		this.settlemetMarketFactPk = settlemetMarketFactPk;
	}
	public BigDecimal getMarketQuantity() {
		return marketQuantity;
	}
	public void setMarketQuantity(BigDecimal marketQuantity) {
		this.marketQuantity = marketQuantity;
	}
	public Long getSettlementAccOperationPk() {
		return settlementAccOperationPk;
	}
	public void setSettlementAccOperationPk(Long settlementAccOperationPk) {
		this.settlementAccOperationPk = settlementAccOperationPk;
	}
	public Integer getIndBlockOperation() {
		return indBlockOperation;
	}
	public void setIndBlockOperation(Integer indBlockOperation) {
		this.indBlockOperation = indBlockOperation;
	}
	public Long getCustodyOperationMrkFactPk() {
		return custodyOperationMrkFactPk;
	}
	public void setCustodyOperationMrkFactPk(Long custodyOperationMrkFactPk) {
		this.custodyOperationMrkFactPk = custodyOperationMrkFactPk;
	}
	public BigDecimal getInitialQuantity() {
		return initialQuantity;
	}
	public void setInitialQuantity(BigDecimal initialQuantity) {
		this.initialQuantity = initialQuantity;
	}
	public Long getPhysicalMrkFactPk() {
		return physicalMrkFactPk;
	}
	public void setPhysicalMrkFactPk(Long physicalMrkFactPk) {
		this.physicalMrkFactPk = physicalMrkFactPk;
	}
	public Long getMechanismType() {
		return mechanismType;
	}
	public void setMechanismType(Long mechanismType) {
		this.mechanismType = mechanismType;
	}
}