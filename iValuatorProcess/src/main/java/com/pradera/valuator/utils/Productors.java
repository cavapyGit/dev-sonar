package com.pradera.valuator.utils;

import javax.enterprise.inject.Produces;

import com.pradera.valuator.annotation.FreeMemory;

public class Productors {

	@Produces @FreeMemory
	public Long getFreeMemory(){
		return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
	}
}
