package com.pradera.valuator.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 
 * 
 * */
public class MarketFactForEvent implements Serializable{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String securityCode;
	private BigDecimal marketRate;
	private Date marketDate;
	private BigDecimal marketPrice;
	private BigDecimal theoricPrice;
	private Long valuatorExecutionId;
	private Integer indHomolog;
	private Long mechanismOperationId;
	private BigDecimal valuatorRate;
	private BigDecimal economicTerm;
	private Date processDate;
	private Long valuatorCodPk;
	private Integer valuatorOperationType;
	public String getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
	public BigDecimal getMarketRate() {
		return marketRate;
	}
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}
	public Date getMarketDate() {
		return marketDate;
	}
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}
	public MarketFactForEvent() {
		super();
	}
	public Long getValuatorExecutionId() {
		return valuatorExecutionId;
	}
	public void setValuatorExecutionId(Long valuatorExecutionId) {
		this.valuatorExecutionId = valuatorExecutionId;
	}
	public Integer getIndHomolog() {
		return indHomolog;
	}
	public void setIndHomolog(Integer indHomolog) {
		this.indHomolog = indHomolog;
	}
	public Long getMechanismOperationId() {
		return mechanismOperationId;
	}
	public void setMechanismOperationId(Long mechanismOperationId) {
		this.mechanismOperationId = mechanismOperationId;
	}
	public BigDecimal getTheoricPrice() {
		return theoricPrice;
	}
	public void setTheoricPrice(BigDecimal theoricPrice) {
		this.theoricPrice = theoricPrice;
	}
	public BigDecimal getValuatorRate() {
		return valuatorRate;
	}
	public void setValuatorRate(BigDecimal valuatorRate) {
		this.valuatorRate = valuatorRate;
	}
	public BigDecimal getEconomicTerm() {
		return economicTerm;
	}
	public void setEconomicTerm(BigDecimal economicTerm) {
		this.economicTerm = economicTerm;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	@Override
	public String toString() {
		return "MarketFactForEvent [securityCode=" + securityCode
				+ ", marketRate=" + marketRate + ", marketDate=" + marketDate
				+ ", marketPrice=" + marketPrice + ", theoricPrice="
				+ theoricPrice + ", valuatorExecutionId=" + valuatorExecutionId
				+ ", indHomolog=" + indHomolog + ", mechanismOperationId="
				+ mechanismOperationId + ", valuatorRate=" + valuatorRate
				+ ", economicTerm=" + economicTerm + ", processDate="
				+ processDate + "]";
	}
	public Long getValuatorCodPk() {
		return valuatorCodPk;
	}
	public void setValuatorCodPk(Long valuatorCodPk) {
		this.valuatorCodPk = valuatorCodPk;
	}
	public Integer getValuatorOperationType() {
		return valuatorOperationType;
	}
	public void setValuatorOperationType(Integer valuatorOperationType) {
		this.valuatorOperationType = valuatorOperationType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((marketDate == null) ? 0 : marketDate.hashCode());
		result = prime * result
				+ ((marketRate == null) ? 0 : marketRate.hashCode());
		result = prime
				* result
				+ ((mechanismOperationId == null) ? 0 : mechanismOperationId
						.hashCode());
		result = prime * result
				+ ((processDate == null) ? 0 : processDate.hashCode());
		result = prime * result
				+ ((securityCode == null) ? 0 : securityCode.hashCode());
		result = prime
				* result
				+ ((valuatorOperationType == null) ? 0 : valuatorOperationType
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
//		if (getClass() != obj.getClass())
//			return false;
		MarketFactForEvent other = (MarketFactForEvent) obj;
		if (marketDate == null) {
			if (other.marketDate != null)
				return false;
		} else if (!marketDate.equals(other.marketDate))
			return false;
		if (marketRate == null) {
			if (other.marketRate != null)
				return false;
		} else if (!marketRate.equals(other.marketRate))
			return false;
		if (mechanismOperationId == null) {
			if (other.mechanismOperationId != null)
				return false;
		} else if (!mechanismOperationId.equals(other.mechanismOperationId))
			return false;
		if (processDate == null) {
			if (other.processDate != null)
				return false;
		} else if (!processDate.equals(other.processDate))
			return false;
		if (securityCode == null) {
			if (other.securityCode != null)
				return false;
		} else if (!securityCode.equals(other.securityCode))
			return false;
		if (valuatorOperationType == null) {
			if (other.valuatorOperationType != null)
				return false;
		} else if (!valuatorOperationType.equals(other.valuatorOperationType))
			return false;
		return true;
	}
}