package com.pradera.valuator.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class ValuatorFormuls {
	
	public static Integer DAYS_OF_YEAR = 360;
	
	public static Integer ROUND_LENGTH_SMALL = 20;
	
	public static Integer ROUND_LENGTH_RATE = 10;
	
	public static Integer ROUND_LENGTH_BIG = 100;

	/**		          (1+((POR*TR))^((PEQ/POR))
	 *                    -------                  - 1
	 *                      360
	 * TRE = ------------------------------------ * 360
	 * 		    			PEQ
	 * POR  (Plazo entre la fecha de vencimiento y la fecha de emision)
	 * PEQ  (Plazo entre la fecha de vencimiento y la fecha de calculo)
	 * TR   (Tasa de Rendimiento Relevante)
	 * */
	public static BigDecimal getTRE(BigDecimal POR, BigDecimal PEQ, BigDecimal TR){
		BigDecimal POR_TR = multiply(POR,TR);
		BigDecimal PEQ_POR = divide(PEQ,POR);
		BigDecimal ONE = BigDecimal.ONE;
		BigDecimal DAYS = new BigDecimal(DAYS_OF_YEAR);
		
		BigDecimal TER = pow(add(ONE,divide(POR_TR,DAYS)), PEQ_POR);
		TER            = multiply(substract(TER,ONE),DAYS);
		TER            = divide(TER,PEQ);
		return roundRate(TER);
	}
	
	/**
	 * VFi = Vli * (1 + TRe * (Pl)   )
	 * 						------
	 * 						 360
	 * VFi   (Valor Final o valor al vencimiento del Valor i.)
	 * VIi   (Valor Inicial del Valor i en la fecha de emision.)
	 * TRe   (Tasa de Rendimiento de emision.)
	 * Pl    (Numero de dias entre la fecha de emision y la fecha de vencimiento.)
	 * */
	public static BigDecimal getVFi(BigDecimal vli, BigDecimal tre, BigDecimal pl){
		BigDecimal one  = BigDecimal.ONE;
		BigDecimal DAYS = new BigDecimal(DAYS_OF_YEAR);
		BigDecimal VF   = add(one, multiply(tre,divide(pl, DAYS)));
				   VF   = multiply(VF,vli);
		return round(VF);
	}
	
	/**
	 * Pit  =        VFi
	 * 		  -------------------	
	 * 			1 + (TRE * Pl  )
	 *                    ----
	 *                    360
	 * Pit   =	Precio del Valor i en el dia t de calculo.
	 * VFi   = 	Valor Final del Valor i en la fecha de vencimiento, que normalmente es el Valor
	 *          Nominal o Facial del Valor y es conocido.
	 * TRE   =	Tasa de Rendimiento Equivalente.
	 * Pl    =	Numero de dias entre la fecha t de calculo y la fecha de vencimiento del Valor.                   
	 * */
	public static BigDecimal getPit(BigDecimal vfi, BigDecimal tre, BigDecimal pl){
		BigDecimal one  = BigDecimal.ONE;
		BigDecimal DAYS = new BigDecimal(DAYS_OF_YEAR);
		BigDecimal pit  = add(one, multiply(tre,divide(pl, DAYS)));
		           pit  = divide(vfi,pit);
		return round(pit);
	}
	
	/**
	 *  Vci   = VNi * (TRe * Pl / 360)
	 *  
	 *  VCi   =	Monto del cupon al vencimiento del mismo (pueden ser intereses solamente, o 	capital mas intereses en el caso de prepagos).
	 *  VNi   =	Valor Nominal del Valor i en la fecha de inicio del devengamiento de intereses del
	 *          cupon (o saldo de capital en el caso de prepagos).
	 *  TRe   =	Tasa de Rendimiento de emision (o tasa base en el caso de Valores emitidos a tasa 	de rendimiento variable).        
	 *  Pl    =	Numero de dias entre la fecha inicial de devengamiento de intereses y la fecha de 
	 *          vencimiento del cupon.  
	 * */
	public static BigDecimal getVCi(BigDecimal vni, BigDecimal tre, BigDecimal pl){
		BigDecimal DAYS = new BigDecimal(DAYS_OF_YEAR);
		BigDecimal vci  = tre.multiply(divide(pl, DAYS));
		           vci  = vni.multiply(vci);
		return vci;
	}
	
	public static BigDecimal getNominalValueForVci(BigDecimal nomValueInitial, BigDecimal... factorAmount){
		return BigDecimal.ZERO;
	}
	
	public static BigDecimal getPitFormuleOther(List<BigDecimal> fcList, BigDecimal tr, List<BigDecimal> plList){
		BigDecimal sumTotal = BigDecimal.ZERO;
		for(int i=0; i<fcList.size(); i++){
			BigDecimal subTotal = BigDecimal.ONE;
			BigDecimal fc=fcList.get(i);
			for(int j=0; j<=i; j++){
				subTotal = multiply(subTotal,getCombination(BigDecimal.ONE, tr, plList.get(j)));
			}
			sumTotal = add(sumTotal,divide(fc,subTotal));
		}
		return sumTotal;
	}
	
	public static BigDecimal getCombination(BigDecimal combination, BigDecimal tr, BigDecimal pl){
		BigDecimal one  = BigDecimal.ONE;
		BigDecimal DAYS = new BigDecimal(DAYS_OF_YEAR);
		BigDecimal comb = add(one, tr.multiply(divide(pl, DAYS)));
		comb 			= comb.multiply(combination);
		return comb;
	}
	
	public static BigDecimal getPitFormule(List<BigDecimal> fcList, BigDecimal tr, List<BigDecimal> plList){
//		System.out.println(CommonsUtilities.currentTime());
		BigDecimal sumTotal = BigDecimal.ZERO;
		BigDecimal sumCombination = BigDecimal.ONE;
		for(int i=0; i<fcList.size(); i++){
			BigDecimal fc=fcList.get(i),pl=plList.get(i);
			sumCombination = getCombination(sumCombination, tr, pl);
			BigDecimal subResult = divide(fc, sumCombination);
			sumTotal = add(sumTotal, subResult);
		}
//		System.out.println(CommonsUtilities.currentTime());
		return sumTotal;
	}
	
	/***
	 * Formule to calculate economic term Version 2.0
	 * @param fcList
	 * @param tr
	 * @param plList
	 * @param fcPlcList
	 * @return
	 */
	public static BigDecimal getPEit_V_2(List<BigDecimal> fcList, BigDecimal tr, List<BigDecimal> plList, List<BigDecimal> fcPlcList){
		BigDecimal pe = getPitFormuleOther(fcPlcList, tr, plList);
		return divide(pe, getPitFormuleOther(fcList, tr, plList));
	}
	
	/***
	 * Formule to calculate economic term Version 1.0
	 * @param fcList
	 * @param tr
	 * @param plList
	 * @param fcPlcList
	 * @return
	 */
	public static BigDecimal getPEit(List<BigDecimal> fcList, BigDecimal tr, List<BigDecimal> plList, List<BigDecimal> fcPlcList){
		BigDecimal pe = getPitFormule(fcPlcList, tr, plList);
		return divide(pe, getPitFormule(fcList, tr, plList));
	}
	
	/***
	 * Formule to get number of threads in some process
	 * Nro Threads = Nro cores / (1 - coefiencient IO)
	 * @param numberCores
	 * @param coeficientIO
	 * @return
	 */
	public static Integer getNumberOfThreads(Integer numberCores, BigDecimal coeficientIO){
		BigDecimal divisor = substract(BigDecimal.ONE, coeficientIO);
		return divide(new BigDecimal(numberCores), divisor).intValue();
	}
	
	/*******************GENERIC ARITMETICS FORMULS***********************/
	private static BigDecimal add(BigDecimal inNumber, BigDecimal...  numbers){
		for(BigDecimal number: numbers){
			inNumber = inNumber.add(number);
		}
		return inNumber;
	}
	
	private static BigDecimal substract(BigDecimal inNumber, BigDecimal...  numbers){
		for(BigDecimal number: numbers){
			inNumber = inNumber.subtract(number);
		}
		return inNumber;
	}
	
	private static BigDecimal pow(BigDecimal number, BigDecimal exp){
		return new BigDecimal(Math.pow(number.doubleValue(), exp.doubleValue()));
	}
	
	private static BigDecimal divide(BigDecimal number_1, BigDecimal divisor){
		return number_1.divide(divisor,ROUND_LENGTH_BIG,RoundingMode.HALF_UP);
	}
	
	private static BigDecimal multiply(BigDecimal number_1, BigDecimal divisor){
		return number_1.multiply(divisor).setScale(ROUND_LENGTH_BIG,RoundingMode.HALF_UP);
	}
	
	private static BigDecimal round(BigDecimal number_1){
		return number_1.setScale(ROUND_LENGTH_SMALL,RoundingMode.HALF_UP);
	}
	
	private static BigDecimal roundRate(BigDecimal number_1){
		return number_1.setScale(ROUND_LENGTH_RATE,RoundingMode.HALF_UP);
	}
	/**************************************************************************/
	
	public static void main(String[] args) {
//		BigDecimal POR = new BigDecimal(182);
//		BigDecimal PEQ = new BigDecimal(1);
//		BigDecimal TR = new BigDecimal(0.045);		
//		System.out.println(Formulas.getTRE(POR, PEQ, TR));
		
		BigDecimal VFi = new BigDecimal("0.1257");
		BigDecimal TRE = new BigDecimal("0.044499");
		BigDecimal PL = new BigDecimal(182);
		
		System.out.println(ValuatorFormuls.getPit(VFi, TRE, PL));
	}
}