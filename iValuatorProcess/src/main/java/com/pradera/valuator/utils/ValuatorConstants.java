package com.pradera.valuator.utils;

import java.math.BigDecimal;

public class ValuatorConstants {
	
	public static final String PARAM_SECURITY = "bySecurity";
	
	public static final String PARAM_SECURITY_CODE = "bySecurityCode";
	
	public static final String PARAM_PARTICIPANT = "byParticipant";
	
	public static final String PARAM_PARTICIPANT_CODE = "byParticipantCode";
	
	public static final String PARAM_INCONSISTENCE_CODE = "byInconsistence";
	
	public static final String PARAM_VALUATOR_PROCESS_PK = "valuatorProcessPk";
	
	public static final String PARAM_VALUATOR_DETAIL_TYPE = "valuatorDetailType";
	
	public static final String PARAM_HOLDER_ACCOUNT = "byHolderAccount";
	
	public static final String PARAM_USER_EMAIL = "userEmail";
	
	public static final String PARAM_USER_NAME = "userName";
	
	public static final String PARAM_VALUATOR_DATE = "valuatorDateParam";
	
	public static final String PARAM_VALUATOR_CURRENCY = "valuatorCurrencyParam";
	
	public static final String PARAM_VALUATOR_EXECUTION_TYPE = "valuatorExecTypeParam";
	
	public static final String PARAM_VALUATOR_INSTRUMENT = "valuatorInstrumentParam";
	
	public static final String PARAM_HOLDER_ACCOUNT_CODE = "byHolderAccountCode";
	
	public static final String PARAM_HOLDER_CODE = "byHolderCode";
	
	public static final String STR_COMMA = ",";
	
	public static final String PARAM_PREPAID = "P";
	
	public static final String PARAM_SUBORDINADE = "S";
	
	public static final String PARAM_ZERO = BigDecimal.ZERO.toString();
	
	public static final String PARAM_VALUATOR_PROCESS_SIMULATOR = "valuatorProcessSimulator";
	
	public static final Integer SIZE_OF_BATCH = 10000;
	
	public static final Integer SIZE_OF_SMALL_BATCH = 1000;
	
	public static final Integer SIZE_OF_MAX_STRING = 4000;
	
	public static final String EMPTY_MESSAGE = "N/A";
	
	public static final String BATCH_USER = "ADMIN";
	
	public static final Double ONE_HUNDRED = 100D;
	
	public static final BigDecimal UNKNOW_RATE = new BigDecimal(-9999);
}