package com.pradera.valuator.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;

@Stateless
public class ValuatorNotificatorService extends CrudDaoServiceBean{
	
	/***
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<ValuatorEventBehavior> getValuatorEventList() throws ServiceException{
		StringBuilder querySQL = new StringBuilder();
		querySQL.append("Select event From ValuatorEventBehavior event							");
		querySQL.append("Where event.indActive = :oneParam										");
		
		Query query = em.createQuery(querySQL.toString());
		query.setParameter("oneParam", ComponentConstant.ONE);
		
		return query.getResultList();
	}
}