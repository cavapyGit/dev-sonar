package com.pradera.valuator.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;
import com.pradera.model.valuatorprocess.type.ValuatorEventType;
import com.pradera.model.valuatorprocess.type.ValuatorExecutionStateType;
import com.pradera.valuator.notificator.ValuatorNotificationEvent;
import com.pradera.valuator.notificator.ValuatorNotificationTrack;
import com.pradera.valuator.notificator.ValuatorTrackDetail;

@Singleton
@Startup
public class ValuatorNotificatorFacade {

	@EJB private ValuatorNotificatorService notificatorService;
	@EJB private GeneralParametersFacade generalParameterFacade;
	
	private List<ParameterTable> securityClassList;
	private List<ValuatorEventBehavior> valuatorEventList;
	
	@PostConstruct
	public void init(){
		try {
			ParameterTableTO parameterTO = new ParameterTableTO();
			parameterTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			
			valuatorEventList = notificatorService.getValuatorEventList();
			securityClassList = generalParameterFacade.getListParameterTableServiceBean(parameterTO);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<ParameterTable> loadSecurityClassParameter() throws ServiceException{
		return securityClassList;
	}
	
	public List<ValuatorEventBehavior> getValuatorEventList() throws ServiceException{
		return valuatorEventList;
	}

	/***
	 * Method to get ValuatorNotification from new Event
	 * @param notificationTrack
	 * @param newEvent
	 * @return
	 * @throws ServiceException
	 */
	public ValuatorNotificationTrack getValuatorEventTrack(ValuatorNotificationTrack notificationTrack, ValuatorNotificationEvent newEvent) throws ServiceException{
		
//		if(notificationTrack==null || notificationTrack.getExecutionTrackDetail()==null || notificationTrack.getExecutionTrackDetail().isEmpty()){
//			return null;
//		}
		
		/**Verified if event has errors*/
		if(newEvent.getIndHasError()!=null && newEvent.getIndHasError().equals(BooleanType.YES.getCode())){
			/**If notification process has all trackin to show*/
			if(notificationTrack!=null){
				/**Iterate all events and the current in execution*/
				for(ValuatorTrackDetail lstTrackDetail : notificationTrack.getExecutionTrackDetail()){
					/**If event is running*/
					if(lstTrackDetail.getProcessState().equals(ValuatorExecutionStateType.VALUATING.getCode())){
						/**If event has securityClass*/
						if(lstTrackDetail.getSecurityClassList()!=null){
							/**Iterate all events of security class and the current in execution*/
							for(ValuatorTrackDetail lstClassDetail : lstTrackDetail.getSecurityClassList()){
								/**If event of security class is running*/
								if(lstClassDetail.getProcessState().equals(ValuatorExecutionStateType.VALUATING.getCode())){
									/**Update indicator with error, state and finishTime*/
									lstClassDetail.setIndHasError(BooleanType.YES.getCode());
									lstClassDetail.setFinishTime(CommonsUtilities.currentDateTime());
									lstClassDetail.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
								}
							}
						}
						/**Update indicator with error, state and finishTime*/
						lstTrackDetail.setIndHasError(BooleanType.YES.getCode());
						lstTrackDetail.setFinishTime(CommonsUtilities.currentDateTime());
						lstTrackDetail.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
					}
				}
				notificationTrack.setFinishTime(CommonsUtilities.currentDateWithTime());
			}
			return notificationTrack;
		}
		
		if(newEvent.getEventTypeCode().equals(ValuatorEventType.EVENT_1_1_TRANSIT_BALANCES.getCode()) && newEvent.getFinishTime()==null){
			notificationTrack = new ValuatorNotificationTrack();
			notificationTrack.setBeginTime(CommonsUtilities.currentDateTime());
			notificationTrack.setExecutionTrackDetail(new ArrayList<ValuatorTrackDetail>());
			notificationTrack.setIndHasError(BooleanType.NO.getCode());

			/**If is the first event, we must add new Notification track*/
			if(newEvent.getFinishTime()==null){
				notificationTrack.setExecutionTrackDetail(beginTrackDetail(null,ValuatorEventType.EVENT_VALIDATIONS.getCode(), null,null,null));
			}
		}
		
		if(notificationTrack==null){
			notificationTrack = new ValuatorNotificationTrack();
			notificationTrack.setBeginTime(CommonsUtilities.currentDateTime());
			notificationTrack.setExecutionTrackDetail(new ArrayList<ValuatorTrackDetail>());
			notificationTrack.setIndHasError(BooleanType.NO.getCode());
			notificationTrack.setExecutionTrackDetail(beginTrackDetail(null,newEvent.getEventTypeCode(), null,null,null));
		}
		
		/**Pre-config notificationTrack with event registered*/
		notificationTrack.setExecutionTrackDetail(getTrackDetailPreConfig(notificationTrack.getExecutionTrackDetail()));
		/**Get list of tracking*/
		List<ValuatorTrackDetail> lstTrackDetail = notificationTrack.getExecutionTrackDetail();
		
		if(newEvent.getEventTypeCode().equals(ValuatorEventType.EVENT_2_1_PRICE_FIXED_INCOME.getCode())){
			/**If is the first event, we must add new Notification track*/
			if(newEvent.getFinishTime()==null && newEvent.getSecurityClass()==null){
				finishTrackinDetail(notificationTrack.getExecutionTrackDetail(), null, ValuatorEventType.EVENT_VALIDATIONS.getCode(), null);
				notificationTrack.setExecutionTrackDetail(beginTrackDetail(lstTrackDetail,ValuatorEventType.EVENT_CALCULATION_PRICE.getCode(), null,null,null));
			}
		}
		
		if(newEvent.getEventTypeCode().equals(ValuatorEventType.EVENT_UPDATING_PORTFOLY.getCode())){
			/**If is the first event, we must add new Notification track*/
			if(newEvent.getFinishTime()==null){
				finishTrackinDetail(notificationTrack.getExecutionTrackDetail(), null, ValuatorEventType.EVENT_CALCULATION_PRICE.getCode(), null);
				notificationTrack.setFinishTime(null);
			}
		}
		
		if(newEvent.getEventTypeCode().equals(ValuatorEventType.EVENT_4_1_UPDATE_BLOCK_OPERATION.getCode())){
			/**If is the first event, we must add new Notification track*/
			if(newEvent.getFinishTime()==null){
				finishTrackinDetail(notificationTrack.getExecutionTrackDetail(), null, ValuatorEventType.EVENT_UPDATING_PORTFOLY.getCode(), null);
				notificationTrack.setExecutionTrackDetail(beginTrackDetail(lstTrackDetail,ValuatorEventType.EVENT_UPDATING_OPERATION.getCode(), null,null,null));
				notificationTrack.setFinishTime(null);
			}
		}
		
		if(newEvent.getFinishTime()==null){
			Long eventType = newEvent.getEventTypeCode();
			Integer securityClass = newEvent.getSecurityClass();
			Long rowQuantity = newEvent.getRowQuantity();
			Integer percentage = newEvent.getPercentage();
			notificationTrack.setExecutionTrackDetail(beginTrackDetail(lstTrackDetail, eventType,securityClass,rowQuantity,percentage));
		}else{
			finishTrackinDetail(notificationTrack.getExecutionTrackDetail(), newEvent.getSecurityClass(), newEvent.getEventTypeCode(), newEvent.getIndHasError());
			
			if(newEvent.getEventTypeCode().equals(ValuatorEventType.EVENT_4_3_UPDATE_PHYSICAL_CERTIFICATE.getCode())){
				finishTrackinDetail(notificationTrack.getExecutionTrackDetail(), null, ValuatorEventType.EVENT_UPDATING_OPERATION.getCode(), null);
				notificationTrack.setFinishTime(CommonsUtilities.currentDateTime());
			}
		}
		
		Collections.sort(notificationTrack.getExecutionTrackDetail(),COMPARE_BY_VALUATOR_CODE);
		
		return notificationTrack;
	}
	
	public static Comparator<ValuatorTrackDetail> COMPARE_BY_VALUATOR_CODE = new Comparator<ValuatorTrackDetail>() {
		 public int compare(ValuatorTrackDetail o1, ValuatorTrackDetail o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
				int resultado = o1.getEventTypeCode().intValue()-o2.getEventTypeCode().intValue();
		        if ( resultado != 0 ) { return resultado; }
				return resultado;
			}
	    };
	
	/***
	 * Method to pre config Track detail of process of valorization
	 * @param lstDetailTrack
	 * @return
	 * @throws ServiceException
	 */
	public List<ValuatorTrackDetail> getTrackDetailPreConfig(List<ValuatorTrackDetail> lstDetailTrack) throws ServiceException{
		
		iteration:
		for(ValuatorEventBehavior eventBehavior: valuatorEventList){
			Long valuatorEventCode = eventBehavior.getIdValuatorEventPk();
			
			for(ValuatorTrackDetail trackDetail: lstDetailTrack){
				if(trackDetail.getEventTypeCode().equals(valuatorEventCode)){
					continue iteration;
				}
			}
			
			/**Create new ValuatorTrackDetail*/
			ValuatorTrackDetail valuatorEventDetail = new ValuatorTrackDetail();
			valuatorEventDetail.setEventTypeCode(valuatorEventCode);
			valuatorEventDetail.setEventName(eventBehavior.getEventName());
			valuatorEventDetail.setSequence(eventBehavior.getExecutionSequence());
			valuatorEventDetail.setProcessState(ValuatorExecutionStateType.REGISTERED.getCode());
			
			lstDetailTrack.add(valuatorEventDetail);
		}
	
		return lstDetailTrack;
	}
	
	/***
	 * 
	 * @param valuatorTrack
	 * @param eventType
	 * @throws ServiceException
	 */
	public void finishTrackinDetail(List<ValuatorTrackDetail> lstEventDetail, Integer secClass, Long eventType, Integer hasErrors) throws ServiceException{
		for(ValuatorTrackDetail eventDetail: lstEventDetail){
			
			/**If event detail it's the same than parameter*/
			if(eventDetail.getEventTypeCode().equals(eventType)){
				
				if(secClass!=null){
					secClass:
					for(ValuatorTrackDetail trackClass: eventDetail.getSecurityClassList()){
						if(secClass.equals(trackClass.getSecurityClass())){
							trackClass.setFinishTime(CommonsUtilities.currentDateTime());
							trackClass.setIndHasError(BooleanType.NO.getCode());
							trackClass.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
							break secClass;
						}
					}
					continue;
				}
				
				eventDetail.setFinishTime(CommonsUtilities.currentDateTime());
				eventDetail.setIndHasError(BooleanType.NO.getCode()); 
				eventDetail.setProcessState(ValuatorExecutionStateType.FINISHED.getCode());
			}
		}
	}
	
	
	/****
	 * Method to get Tracking detail from event Code
	 * @param eventType
	 * @param securityClass
	 * @return
	 * @throws ServiceException
	 */
	public List<ValuatorTrackDetail> beginTrackDetail(List<ValuatorTrackDetail> lstTrackDetail, Long eventType, Integer securityClass, 
																										Long rowQuantity, Integer percentage)throws ServiceException{
		
		if(lstTrackDetail!=null && !lstTrackDetail.isEmpty()){
			/***Iterate current ValuatorTrackDetail*/
			for(ValuatorTrackDetail trackDetail :lstTrackDetail){
				/**If it's the same, we must update state*/
				if(trackDetail.getEventTypeCode().equals(eventType)){
					
					if(securityClass==null){
						trackDetail.setBeginTime(CommonsUtilities.currentDateTime());
					}else {
						/**Its same securityClass with percentage*/
						if(percentage==null){
							ValuatorTrackDetail trackDetailClass = new ValuatorTrackDetail();
							ParameterTable pTable = getParameterTable(securityClass);
							
							trackDetailClass.setBeginTime(CommonsUtilities.currentDateTime());
							trackDetailClass.setSecurityClass(securityClass);
							trackDetailClass.setClassDescription(pTable.getText1().concat(" - ").concat(pTable.getParameterName()));
							trackDetailClass.setRowQuantity(rowQuantity);
							trackDetailClass.setIndHasError(BooleanType.NO.getCode());
							trackDetailClass.setProcessState(ValuatorExecutionStateType.VALUATING.getCode());
							trackDetailClass.setPercentage(ComponentConstant.ONE);
							
							if(trackDetail.getSecurityClassList()==null || trackDetail.getSecurityClassList().isEmpty()){
								trackDetail.setSecurityClassList(new ArrayList<ValuatorTrackDetail>());
							}
							
							trackDetail.getSecurityClassList().add(trackDetailClass);
						}else{
							/**Process is updating the percentage*/
							for(ValuatorTrackDetail secClassTracking: trackDetail.getSecurityClassList()){
								/**If securityClass it's the same, update percentage*/
								if(secClassTracking.getSecurityClass().equals(securityClass)){
									secClassTracking.setPercentage(percentage);
								}
							}
						}
					}
					
					trackDetail.setProcessState(ValuatorExecutionStateType.VALUATING.getCode());
					break;
				}
			}			
		}else{ 
			/**Create new ValuatorTrackDetail*/
			ValuatorTrackDetail valuatorEventDetail = new ValuatorTrackDetail();
			valuatorEventDetail.setEventTypeCode(eventType);
			valuatorEventDetail.setEventName(getEventBehavior(eventType).getEventName());
			valuatorEventDetail.setSequence(getEventBehavior(eventType).getExecutionSequence());
			valuatorEventDetail.setProcessState(ValuatorExecutionStateType.VALUATING.getCode());
			valuatorEventDetail.setBeginTime(CommonsUtilities.currentDateTime());
			valuatorEventDetail.setIndHasError(BooleanType.NO.getCode());
			
			/**Create new ArrayList*/
			lstTrackDetail = new ArrayList<>();
			lstTrackDetail.add(valuatorEventDetail);
		}
		return lstTrackDetail;
	}
	
	/****
	 * Method to get Parameter from pk
	 * @param secClass
	 * @return
	 * @throws ServiceException
	 */
	public ParameterTable getParameterTable(Integer secClass) throws ServiceException{
		for(ParameterTable pTable: securityClassList){
			if(pTable.getParameterTablePk().equals(secClass)){
				return pTable;
			}
		}
		return null;
	}
	
	/****
	 * Method to get behavior from code
	 * @param eventType
	 * @return
	 * @throws ServiceException
	 */
	public ValuatorEventBehavior getEventBehavior(Long eventType) throws ServiceException{
		for(ValuatorEventBehavior eventBehavior: valuatorEventList){
			if(eventBehavior.getIdValuatorEventPk().equals(eventType)){
				return eventBehavior;
			}
		}
		return null;
	}
}
