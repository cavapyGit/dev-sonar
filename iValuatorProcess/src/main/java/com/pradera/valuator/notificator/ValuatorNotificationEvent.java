package com.pradera.valuator.notificator;

import java.io.Serializable;
import java.util.Date;

/***
 * 
 * @author mmax
 *
 */
public class ValuatorNotificationEvent implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long eventTypeCode;
	private Integer indBegin;
	private Integer indFinish;
	private Date beginTime;
	private Date finishTime;
	private Integer securityClass;
	private Integer indHasError;
	private Long rowQuantity;
	private Integer percentage;
	
	public Integer getIndBegin() {
		return indBegin;
	}
	public void setIndBegin(Integer indBegin) {
		this.indBegin = indBegin;
	}
	public Integer getIndFinish() {
		return indFinish;
	}
	public void setIndFinish(Integer indFinish) {
		this.indFinish = indFinish;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getIndHasError() {
		return indHasError;
	}
	public void setIndHasError(Integer indHasError) {
		this.indHasError = indHasError;
	}
	public Long getRowQuantity() {
		return rowQuantity;
	}
	public void setRowQuantity(Long rowQuantity) {
		this.rowQuantity = rowQuantity;
	}
	public ValuatorNotificationEvent() {
		super();
	}
	public Long getEventTypeCode() {
		return eventTypeCode;
	}
	public void setEventTypeCode(Long eventTypeCode) {
		this.eventTypeCode = eventTypeCode;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	@Override
	public String toString() {
		return "ValuatorNotificationEvent [eventTypeCode=" + eventTypeCode
				+ ", indBegin=" + indBegin + ", indFinish=" + indFinish
				+ ", beginTime=" + beginTime + ", finishTime=" + finishTime
				+ ", securityClass=" + securityClass + ", indHasError="
				+ indHasError + ", rowQuantity=" + rowQuantity + "]";
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
}