package com.pradera.valuator.notificator;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/***
 * 
 * @author mmax
 *
 */
public class ValuatorNotificationTrack implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date beginTime;
	private Date finishTime;
	private Integer indHasError;
	private List<ValuatorTrackDetail> executionTrackDetail;
	
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getIndHasError() {
		return indHasError;
	}
	public void setIndHasError(Integer indHasError) {
		this.indHasError = indHasError;
	}
	public List<ValuatorTrackDetail> getExecutionTrackDetail() {
		return executionTrackDetail;
	}
	public void setExecutionTrackDetail(
			List<ValuatorTrackDetail> executionTrackDetail) {
		this.executionTrackDetail = executionTrackDetail;
	}
}