package com.pradera.valuator.notificator;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Named;

import com.pradera.commons.view.GenericBaseBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.valuatorprocess.ValuatorEventBehavior;
import com.pradera.valuator.service.ValuatorNotificatorFacade;

@ApplicationScoped
@Named
public class NotificatorListenerBean extends GenericBaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8101226807096755090L;
	
	@EJB private ValuatorNotificatorFacade notifatorFacade;
	
	private List<ParameterTable> securityClassList;
	private List<ValuatorEventBehavior> valuatorEventList;
	private ValuatorNotificationTrack executionRuning;
	
//	private final PushContext pushContext = PushContextFactory.getDefault().getPushContext();
	
	@PostConstruct
	public void init(){
		try {
			loadSecurityClassParameter();
			loadValuatorEvents();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	@Asynchronous
	public void receiveValuatorEvent(@Observes ValuatorNotificationEvent notificationEvent) throws ServiceException{
//		if(executionRuning!=null){
			executionRuning = notifatorFacade.getValuatorEventTrack(executionRuning, notificationEvent);
//		}
//		pushContext.push("valuatorChanel", null);
	}
	

	private void loadSecurityClassParameter() throws ServiceException{
		securityClassList = notifatorFacade.loadSecurityClassParameter();
	}
	
	private void loadValuatorEvents()  throws ServiceException{
		valuatorEventList = notifatorFacade.getValuatorEventList(); 
	}
	
	public void finishProcess(){
		
	}

	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}

	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}

	public List<ValuatorEventBehavior> getValuatorEventList() {
		return valuatorEventList;
	}

	public void setValuatorEventList(List<ValuatorEventBehavior> valuatorEventList) {
		this.valuatorEventList = valuatorEventList;
	}

	public ValuatorNotificationTrack getExecutionRuning() {
		return executionRuning;
	}

	public void setExecutionRuning(ValuatorNotificationTrack executionRuning) {
		this.executionRuning = executionRuning;
	}
}