package com.pradera.valuator.notificator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/***
 * 
 * @author mmax
 *
 */
public class ValuatorTrackDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long 	eventTypeCode;
	private Date 	beginTime;
	private Date  	finishTime;
	private Integer securityClass;
	private String  classDescription;
	private Integer indHasError;
	private Long 	rowQuantity;
	private String 	eventName;
	private Integer processState;
	private BigDecimal 	sequence;
	private Integer percentage;
	
	private List<ValuatorTrackDetail> securityClassList;
	
	public Long getEventTypeCode() {
		return eventTypeCode;
	}
	public void setEventTypeCode(Long eventTypeCode) {
		this.eventTypeCode = eventTypeCode;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public String getClassDescription() {
		return classDescription;
	}
	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}
	public Integer getIndHasError() {
		return indHasError;
	}
	public void setIndHasError(Integer indHasError) {
		this.indHasError = indHasError;
	}
	public Long getRowQuantity() {
		return rowQuantity;
	}
	public void setRowQuantity(Long rowQuantity) {
		this.rowQuantity = rowQuantity;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Integer getProcessState() {
		return processState;
	}
	public void setProcessState(Integer processState) {
		this.processState = processState;
	}
	public List<ValuatorTrackDetail> getSecurityClassList() {
		return securityClassList;
	}
	public void setSecurityClassList(List<ValuatorTrackDetail> securityClassList) {
		this.securityClassList = securityClassList;
	}
	public BigDecimal getSequence() {
		return sequence;
	}
	public void setSequence(BigDecimal sequence) {
		this.sequence = sequence;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}
}