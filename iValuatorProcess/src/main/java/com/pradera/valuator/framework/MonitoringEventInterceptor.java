package com.pradera.valuator.framework;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.valuator.annotation.ValuatorDispatcherEvent;
import com.pradera.valuator.notificator.ValuatorNotificationEvent;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating MonitorableThread objects.
 */
@ValuatorDispatcherEvent
@Interceptor
public class MonitoringEventInterceptor{
	
	@Inject
	private Event<ValuatorNotificationEvent> valuatorNotificationEvent;
	
	public MonitoringEventInterceptor() {
		// TODO Auto-generated constructor stub
	}

	@AroundInvoke
	public Object aroundProcessInvoke(InvocationContext ic) throws Exception {
		Integer indYes = BooleanType.YES.getCode();
		Integer indNot = BooleanType.NO.getCode();
		Integer hasError  = BooleanType.NO.getCode();
		ValuatorDispatcherEvent valuatorEvent = ic.getMethod().getAnnotation(ValuatorDispatcherEvent.class);
		if(valuatorEvent!=null){
			valuatorNotificationEvent.fire(getNotificationEvent(valuatorEvent.eventType().getCode(), indYes, indNot,indNot));
			//TODO check if is necessary control security about participants,issues and balances
			try {
				return ic.proceed();
			}catch(Exception ex){
//				valuatorNotificationEvent.fire(getNotificationEvent(valuatorEvent.eventType().getCode(), indNot, indYes,indYes));
				hasError  = BooleanType.YES.getCode();
				throw ex;
			}finally{
				valuatorNotificationEvent.fire(getNotificationEvent(valuatorEvent.eventType().getCode(), indNot, indYes,hasError));
			}
		}
		return ic.proceed();
	}

	/***
	 * 
	 * @param eventCode
	 * @param indBegin
	 * @param indFinish
	 * @return
	 */
	public ValuatorNotificationEvent getNotificationEvent(Long eventCode, Integer indBegin, Integer indFinish, Integer hasError){
		ValuatorNotificationEvent notificationEvent = new ValuatorNotificationEvent();
		notificationEvent.setEventTypeCode(eventCode);
		notificationEvent.setIndHasError(hasError);
		if(indBegin.equals(BooleanType.YES.getCode())){
			notificationEvent.setBeginTime(CommonsUtilities.currentDateTime());
			notificationEvent.setIndBegin(BooleanType.YES.getCode());
		}else if(indBegin.equals(BooleanType.NO.getCode())){
			notificationEvent.setFinishTime(CommonsUtilities.currentDateTime());
			notificationEvent.setIndFinish(BooleanType.YES.getCode());
		}
		return notificationEvent;
	}
}