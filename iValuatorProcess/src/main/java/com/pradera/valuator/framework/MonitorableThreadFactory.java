package com.pradera.valuator.framework;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating MonitorableThread objects.
 */
public class MonitorableThreadFactory implements ThreadFactory {

	/** The thread number. */
	final AtomicInteger threadNumber = new AtomicInteger(1);
	
	/** The name prefix. */
	private String namePrefix;

	/**
	 * Instantiates a new monitorable thread factory.
	 */
	public MonitorableThreadFactory() {
		this("xray-rest-pool");
	}

	/**
	 * Instantiates a new monitorable thread factory.
	 *
	 * @param namePrefix the name prefix
	 */
	public MonitorableThreadFactory(String namePrefix) {
		this.namePrefix = namePrefix;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
	 */
	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r);
		t.setName(createName());
		if (t.isDaemon()) {
			t.setDaemon(false);
		}
		if (t.getPriority() != Thread.NORM_PRIORITY) {
			t.setPriority(Thread.NORM_PRIORITY);
		}
		return t;
	}

	/**
	 * Creates a new MonitorableThread object.
	 *
	 * @return the string
	 */
	String createName() {
		return namePrefix + "-" + threadNumber.incrementAndGet();
	}

	/**
	 * Gets the number of created threads.
	 *
	 * @return the number of created threads
	 */
	public int getNumberOfCreatedThreads() {
		return threadNumber.get();
	}
}