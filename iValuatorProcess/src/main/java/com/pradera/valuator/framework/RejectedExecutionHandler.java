package com.pradera.valuator.framework;

import java.util.concurrent.ThreadPoolExecutor;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Interface RejectedExecutionHandler.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/09/2014
 */
public interface RejectedExecutionHandler {

	/**
	 * Rejected execution.
	 *
	 * @param r the r
	 * @param executor the executor
	 */
	void rejectedExecution(Runnable r, ThreadPoolExecutor executor);
}