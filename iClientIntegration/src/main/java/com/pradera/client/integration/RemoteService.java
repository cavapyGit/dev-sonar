package com.pradera.client.integration;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Stereotype;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Stereotype
@Alternative
@Target({ METHOD })
@Retention(RUNTIME)
@Documented
public @interface RemoteService {

}
