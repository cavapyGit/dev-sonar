package com.pradera.client.integration.component;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.pradera.client.integration.RemoteService;
import com.pradera.commons.configuration.type.StageApplication;
import com.pradera.commons.services.remote.BeanLocator;
import com.pradera.commons.services.remote.RemoteServicesConstants;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.FundsComponentService;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.service.SalePurchaseUpdateComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.service.SplitCouponComponentService;
import com.pradera.integration.component.business.service.VaultControlService;

@ApplicationScoped
public class ComponentsProducer implements Serializable {
	
	@Inject 
	StageApplication stateApplication;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6801522554079969610L;
	
	public ComponentsProducer() {
		// TODO Auto-generated constructor stub
	}
	
	@Produces
	public InterfaceComponentService getInterfaceComponent(){
		InterfaceComponentService interfaceComponentService = (InterfaceComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.INTERFACE_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(InterfaceComponentService.class).locate();
		return interfaceComponentService;
	}
	
	@Produces
	public AccountsComponentService getAccountComponent(){
		AccountsComponentService accountsComponentService = (AccountsComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.ACCOUNT_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(AccountsComponentService.class).locate();
		return accountsComponentService;
	}
	
	@Produces @RemoteService
	public AccountsComponentService getAccountRemoteComponent(){
		AccountsComponentService accountsComponentService = (AccountsComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.ACCOUNT_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(AccountsComponentService.class).locateRemote(stateApplication.name());
		return accountsComponentService;
	}
	
	@Produces
	public SecuritiesComponentService getSecuritiesComponent(){
		SecuritiesComponentService securitiesComponentService = (SecuritiesComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.SECURITIES_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(SecuritiesComponentService.class).locate();
		return securitiesComponentService;
	}
	
	@Produces @RemoteService
	public SecuritiesComponentService getSecuritiesRemoteComponent(){
		SecuritiesComponentService securitiesComponentService = (SecuritiesComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.SECURITIES_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(SecuritiesComponentService.class).locateRemote(stateApplication.name());
		return securitiesComponentService;
	}
	

	@Produces
	public FundsComponentService getFundsComponent(){
		FundsComponentService fundsComponentService = (FundsComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.FUNDS_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(FundsComponentService.class).locate();
		return fundsComponentService;
	}
	
	@Produces @RemoteService
	public FundsComponentService getFundsRemoteComponent(){
		FundsComponentService fundsComponentService = (FundsComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.FUNDS_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(FundsComponentService.class).locateRemote(stateApplication.name());
		return fundsComponentService;
	}
	
	@Produces
	public SalePurchaseUpdateComponentService getSalePurchaseUpdateComponent(){
		SalePurchaseUpdateComponentService salePurchaseUpdateComponentService = (SalePurchaseUpdateComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.SALE_PURCHASE_UPDATE_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(SalePurchaseUpdateComponentService.class).locate();
		return salePurchaseUpdateComponentService;
	}
	
	@Produces @RemoteService
	public SalePurchaseUpdateComponentService getSalePurchaseUpdateRemoteComponent(){
		SalePurchaseUpdateComponentService salePurchaseUpdateComponentService = (SalePurchaseUpdateComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.SALE_PURCHASE_UPDATE_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(SalePurchaseUpdateComponentService.class).locateRemote(stateApplication.name());
		return salePurchaseUpdateComponentService;
	}
	
	@Produces
	public SplitCouponComponentService getSplitCouponComponent(){
		SplitCouponComponentService splitCouponComponentService = (SplitCouponComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.SPLIT_COUPON_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(SplitCouponComponentService.class).locate();
		return splitCouponComponentService;
	}
	
	@Produces @RemoteService
	public SplitCouponComponentService getSplitCouponRemoteComponent(){
		SplitCouponComponentService splitCouponComponentService = (SplitCouponComponentService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.SPLIT_COUPON_COMPONENT_SERVICE_IMPL).
				withBusinessInterface(SplitCouponComponentService.class).locateRemote(stateApplication.name());
		return splitCouponComponentService;
	}
	
	@Produces
	public VaultControlService getVaultControlComponent(){
		VaultControlService vaultControlService = (VaultControlService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.VAULT_CONTROL_SERVICE_IMPL).
				withBusinessInterface(VaultControlService.class).locate();
		return vaultControlService;
	}
	
	@Produces @RemoteService
	public VaultControlService getVaultControlRemoteComponent(){
		VaultControlService vaultControlRemoteComponent = (VaultControlService) new BeanLocator.
				GlobalJNDIName().withModuleName(ModuleWarType.BUSINESS.getValue()).withBeanName(RemoteServicesConstants.VAULT_CONTROL_SERVICE_IMPL).
				withBusinessInterface(VaultControlService.class).locateRemote(stateApplication.name());
		return vaultControlRemoteComponent;
	}
}
