package com.pradera.custody.thomsonreuters.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.custody.thomsonreuters.service.DATA.REGISTROS;
import com.pradera.custody.thomsonreuters.service.ThomsonReutersServiceBean;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class ThomsonReutersFacade {
	
	@EJB
	ThomsonReutersServiceBean thomsonReutersServiceBean;	
	
	public REGISTROS getDataTR() {
		return thomsonReutersServiceBean.getDataTR();
	}
	
	public String[] getLstEmailForCopy(){
		List<String> lstEmailForCopy= thomsonReutersServiceBean.getLstEmailForCopy();
		String [] lstForCopy=new String[lstEmailForCopy.size()];
		int i=0;
		for(String emailForCopy:lstEmailForCopy){
			lstForCopy[i++] = emailForCopy;
		}
		return lstForCopy;
	}
}
