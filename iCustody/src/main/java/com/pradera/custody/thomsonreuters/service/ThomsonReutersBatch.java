package com.pradera.custody.thomsonreuters.service;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.thomsonreuters.facade.ThomsonReutersFacade;
import com.pradera.custody.thomsonreuters.service.DATA.REGISTROS;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.EmailExternalType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.ProcessLogger;
/**
 * BATCH GENERACION Y ENVIO XML THOMSONREUTERS
 */
@BatchProcess(name="ThomsonReutersBatch")
@RequestScoped
public class ThomsonReutersBatch implements Serializable, JobExecution {
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	ThomsonReutersFacade thomsonReutersFacade;
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	@Inject
	private PraderaLogger log;
	
	private String emailDestination;
	
	private String listEmailDestinatioCopy[];
	
	@PostConstruct
	public void inicio() {
		
		// correo destino
		ParameterTable emailExternal=getEmailExternal();
		if(Validations.validateIsNotNull(emailExternal)
				&& Validations.validateIsNotNull(emailExternal.getDescription())){
			log.info("::: correo extraido desde ParameterTable: "+emailExternal.getDescription());
			emailDestination = emailExternal.getDescription();
		}else{
			log.info("::: el correo NO se pudo extarer desde ParameterTable, se colocara el que esta por defecto para el destinatario Thompon ");
			emailDestination = "MarketData-LATAM@thomson.com";
		}
//		emailDestination = "rlarico@edv.com.bo";
		
		// lisa de correo a copiar
//		listEmailDestinatioCopy = new String[2];
//		listEmailDestinatioCopy[0] = "cvillavicencio@edv.com.bo";
//		listEmailDestinatioCopy[1] = "nnunez@edv.com.bo";
		
		// obteniendo los correos a copiar
		listEmailDestinatioCopy = thomsonReutersFacade.getLstEmailForCopy();
	}
	
	public ParameterTable getEmailExternal() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setParameterTablePk(EmailExternalType.THOMPSON.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.EMAIL_FOR_EXTERNAL.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO).get(0);
		} catch (Exception ex) {
			return null;
		}
	}
	
	@Override
	public void startJob(ProcessLogger processLogger) {
		StringBuilder xmlResponse = new StringBuilder();
		REGISTROS thomsonReutersTO = new REGISTROS();
		thomsonReutersTO = thomsonReutersFacade.getDataTR();		
		if(!thomsonReutersTO.getREGISTRO().isEmpty()) {
			DATA thomsonReuters = new DATA();
			thomsonReuters.setREGISTROS(thomsonReutersTO);
			String DATE_FORMAT = "YYMMdd";
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			String fecha = sdf.format(CommonsUtilities.currentDate());			
			try {
				log.info("::::INSTANCIANDO ARCHIVO XML::::");
				String filename = "EDV"+fecha+".xml";
				String rute="/home/jboss/thomsonreuters/";//produccion
//				String rute=("D:\\THOMSONREUTERS\\");//pruebas
				File file = new File(rute+filename);
	            JAXBContext jaxbContext = JAXBContext.newInstance(DATA.class);
	            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	            jaxbMarshaller.marshal(thomsonReuters, file);
//	            jaxbMarshaller.marshal(thomsonReuters, System.out);
	            log.info("::::GENERO ARCHIVO EXITOSAMENTE::::");
	            try {
	            	log.info("::::INSTANCIANDO ENVIO DE ARCHIVO::::");
	            	Properties props = new Properties();	            	
	            	props.put("mail.smtp.host", "smtp.gmail.com");
	                props.setProperty("mail.smtp.starttls.enable", "true");
	                props.setProperty("mail.smtp.port", "25");
	                props.setProperty("mail.smtp.user", "drive.pradera@gmail.com");
	                props.setProperty("mail.smtp.auth", "true");
	                Session session = Session.getDefaultInstance(props, null);	                
	                BodyPart texto = new MimeBodyPart();
	                texto.setText("Archivo Thomson Reuters "+fecha);
	                BodyPart adjunto = new MimeBodyPart();
	                adjunto.setDataHandler(new DataHandler(new FileDataSource("/home/jboss/thomsonreuters/"+filename)));//produccion
	                adjunto.setFileName("/home/jboss/thomsonreuters/"+filename);//produccion
//	                adjunto.setDataHandler(new DataHandler(new FileDataSource("D:\\THOMSONREUTERS\\"+filename)));//pruebas
//	                adjunto.setFileName("D:\\THOMSONREUTERS\\"+filename);//pruebas
	                MimeMultipart multiParte = new MimeMultipart();
	                multiParte.addBodyPart(texto);
	                multiParte.addBodyPart(adjunto);
	                MimeMessage message = new MimeMessage(session);
	                message.setFrom(new InternetAddress("drive.pradera@gmail.com"));//pruebas

	                log.info("Enviando archivo a correo: " + emailDestination);
	                message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDestination));
	                
	                // cargando la lista de correos a ser enviado
	                if(listEmailDestinatioCopy != null && listEmailDestinatioCopy.length > 0){
	                	
	                	InternetAddress listInternetAddress[] = new InternetAddress[listEmailDestinatioCopy.length];	                	
	                	for(int i = 0 ; i < listEmailDestinatioCopy.length ; i++){
	                		log.info("Enviando archivo CC a correo: " + listEmailDestinatioCopy[i]);
	                		listInternetAddress[i] = new InternetAddress(listEmailDestinatioCopy[i]);	                		
	                	}
	                	message.addRecipients(Message.RecipientType.CC, listInternetAddress);
	                }
	                
	                message.setSubject("Archivo Thomson Reuters");
	                message.setContent(multiParte);
	                Transport t = session.getTransport("smtp");
	                t.connect("drive.pradera@gmail.com", "Mongolito161525*");
	                t.sendMessage(message, message.getAllRecipients());
	                t.close();
	                log.info("::::ENVIO ARCHIVO EXITOSAMENTE::::");
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
			} catch (JAXBException e) {
	            e.printStackTrace();
	        }			
		} else {
			xmlResponse.append("NO HAY DATOS A ESA FECHA");
			log.info("::::NO HAY DATOS A ESA FECHA::::");
		}
		log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA ::::");
	}
	
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	@Override
	public boolean sendNotification() {
		return false;
	}
}
