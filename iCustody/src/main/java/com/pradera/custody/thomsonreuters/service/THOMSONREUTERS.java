package com.pradera.custody.thomsonreuters.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "registros"
})
@XmlRootElement(name = "DATA")
public class THOMSONREUTERS {

    @XmlElement(name = "REGISTROS", required = true)
    protected THOMSONREUTERS.REGISTROS registros;
     
    public THOMSONREUTERS.REGISTROS getREGISTROS() {
        return registros;
    }

    public void setREGISTROS(THOMSONREUTERS.REGISTROS value) {
        this.registros = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "registro"
    })
    public static class REGISTROS {

        @XmlElement(name = "REGISTRO", required = true)
        protected List<THOMSONREUTERS.REGISTROS.REGISTRO> registro;

        public List<THOMSONREUTERS.REGISTROS.REGISTRO> getREGISTRO() {
            if (registro == null) {
                registro = new ArrayList<THOMSONREUTERS.REGISTROS.REGISTRO>();
            }
            return this.registro;
        }

        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fechainformacion",
            "serie",
            "isin",
            "cfi",
            "instrumento",
            "preciomercado",
            "tasamercado",
            "valornominal",
            "moneda",
            "fechaemision",
            "fechavencimiento",
            "emisor"
        })
        public static class REGISTRO {

        	@XmlElement(name = "FECHA_INFORMACION", required = true)
            protected String fechainformacion;
            @XmlElement(name = "SERIE", required = true)
            protected String serie;
            @XmlElement(name = "ISIN", required = true)
            protected String isin;
            @XmlElement(name = "CFI", required = true)
            protected String cfi;
            @XmlElement(name = "INSTRUMENTO", required = true)
            protected String instrumento;
            @XmlElement(name = "PRECIO_MERCADO", required = true, nillable = true)
            protected String preciomercado;
            @XmlElement(name = "TASA_MERCADO", required = true, nillable = true)
            protected String tasamercado;
            @XmlElement(name = "VALOR_NOMINAL", required = true, nillable = true)
            protected String valornominal;
            @XmlElement(name = "MONEDA", required = true, nillable = true)
            protected String moneda;
            @XmlElement(name = "FECHA_EMISION", required = true, nillable = true)
            protected String fechaemision;
            @XmlElement(name = "FECHA_VENCIMIENTO", required = true, nillable = true)
            protected String fechavencimiento;
            @XmlElement(name = "EMISOR", required = true, nillable = true)
            protected String emisor;

            public String getFechainformacion() {
                return fechainformacion;
            }

            public void setFechainformacion(String fechainformacion) {
                this.fechainformacion = fechainformacion;
            }

            public String getSerie() {
                return serie;
            }

            public void setSerie(String serie) {
                this.serie = serie;
            }

            public String getIsin() {
                return isin;
            }

            public void setIsin(String isin) {
                this.isin = isin;
            }

            public String getCfi() {
                return cfi;
            }

            public void setCfi(String cfi) {
                this.cfi = cfi;
            }

            public String getInstrumento() {
                return instrumento;
            }

            public void setInstrumento(String instrumento) {
                this.instrumento = instrumento;
            }

            public String getPreciomercado() {
                return preciomercado;
            }

            public void setPreciomercado(String preciomercado) {
                this.preciomercado = preciomercado;
            }

            public String getTasamercado() {
                return tasamercado;
            }

            public void setTasamercado(String tasamercado) {
                this.tasamercado = tasamercado;
            }

            public String getValornominal() {
                return valornominal;
            }

            public void setValornominal(String valornominal) {
                this.valornominal = valornominal;
            }

            public String getMoneda() {
                return moneda;
            }

            public void setMoneda(String moneda) {
                this.moneda = moneda;
            }

            public String getFechaemision() {
                return fechaemision;
            }

            public void setFechaemision(String fechaemision) {
                this.fechaemision = fechaemision;
            }

            public String getFechavencimiento() {
                return fechavencimiento;
            }

            public void setFechavencimiento(String fechavencimiento) {
                this.fechavencimiento = fechavencimiento;
            }

            public String getEmisor() {
                return emisor;
            }

            public void setEmisor(String emisor) {
                this.emisor = emisor;
            }

        }

    }

}
