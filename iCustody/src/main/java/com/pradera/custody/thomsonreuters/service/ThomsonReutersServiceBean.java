package com.pradera.custody.thomsonreuters.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.thomsonreuters.service.DATA.REGISTROS;
import com.pradera.custody.thomsonreuters.service.DATA.REGISTROS.REGISTRO;

@Stateless
public class ThomsonReutersServiceBean extends CrudDaoServiceBean {
	
	public static final String SUBJECT_EMAIL_THOMSON = "ARCHIVO THOMSON REUTERS";
	
	public List<String> getLstEmailForCopy(){
		StringBuilder q=new StringBuilder();
		q.append(" select DN.EMAIL from DESTINATION_NOTIFICATION dn                                                     ");
		q.append(" inner join PROCESS_NOTIFICATION pn on pn.ID_PROCESS_NOTIFICATION_PK = dn.ID_PROCESS_NOTIFICATION_FK  ");
		q.append(" inner join BUSINESS_PROCESS bp on bp.ID_BUSINESS_PROCESS_PK = pn.ID_BUSINESS_PROCESS_FK              ");
		q.append(" where bp.ID_BUSINESS_PROCESS_PK = :idBusinessProcessPk");
		q.append(" and PN.IND_STATUS = 1");
		q.append(" and upper(PN.NOTIFICATION_SUBJECT) = :subject ");
		
		Query query = em.createNativeQuery(q.toString());
		query.setParameter("idBusinessProcessPk", 40815L);
		query.setParameter("subject", SUBJECT_EMAIL_THOMSON);
		return query.getResultList();
		
	}
	
	/**
	 * Search data object list.
	 */
	public List<Object[]> getData () {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select "
				+"   s.ID_SECURITY_CODE_ONLY SERIE,"
				+"   s.ID_ISIN_CODE ISIN,"
				+"   s.CFI_CODE CFI," 
				+"   (select TEXT1 from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.SECURITY_CLASS) AS INSTRUMENTO," 
				+"   round(mfv.MARKET_PRICE,2) PRECIO_MERCADO,"
				+"   round(mfv.MARKET_RATE,2) TASA_MERCADO,"
				+"   s.CURRENT_NOMINAL_VALUE as VALOR_NOMINAL,"
				+"   (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.CURRENCY   ) AS MONEDA,"
				+"   TO_CHAR(s.ISSUANCE_DATE,'DD/MM/YYYY') FECHA_EMISION,"
				+"   TO_CHAR(s.EXPIRATION_DATE,'DD/MM/YYYY') FECHA_VENCIMIENTO,"
				+"   (SELECT  MNEMONIC FROM ISSUER where ID_ISSUER_PK = s.ID_ISSUER_FK ) EMISOR"
				+" from MARKET_FACT_VIEW mfv, SECURITY s " 
				+" where 1=1" 
				+"   and mfv.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK" 
				+"   and s.STATE_SECURITY = 131" 
				+"   and s.SECURITY_CLASS not in (1854,1855,414,415,420,1954,1976,2241,2246,126)" 
				+"   and s.DESMATERIALIZED_BALANCE > 0" 
				+"   and mfv.CUT_DATE = :date"
//				+"   and s.EXPIRATION_DATE >= :date"
				+"   and mfv.TOTAL_BALANCE > 0"  
				+" group by s.ID_SECURITY_CODE_ONLY,s.ID_ISIN_CODE,s.CFI_CODE,s.SECURITY_CLASS,mfv.MARKET_PRICE,mfv.MARKET_RATE,"
				+" s.CURRENT_NOMINAL_VALUE,s.CURRENCY,s.ISSUANCE_DATE,s.EXPIRATION_DATE,s.ID_ISSUER_FK");
								
		Query query = em.createNativeQuery(sbQuery.toString());		
		query.setParameter("date",CommonsUtilities.truncateDateTime(CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(), -1)));
		return (List<Object[]>)query.getResultList();
	}
	
	public REGISTROS getDataTR () {
		List<Object[]> objectData = getData();		
		REGISTROS registros = new REGISTROS();
		for (Object[] obj : objectData) {			
			REGISTRO registro = new REGISTRO();			
			registro.setFECHAINFORMACION(CommonsUtilities.convertDateToString(CommonsUtilities.currentDate(),"dd/MM/yyyy"));
			registro.setSERIE((obj[0] != null) ? String.valueOf(obj[0]) : "");
			registro.setISIN((obj[1] != null) ? String.valueOf(obj[1]) : "");
			registro.setCFI((obj[2] != null) ? String.valueOf(obj[2]) : "");
			registro.setINSTRUMENTO((obj[3] != null) ? String.valueOf(obj[3]) : "");
			registro.setPRECIOMERCADO((obj[4] != null) ? String.valueOf(obj[4]) : "");
			registro.setTASAMERCADO((obj[5] != null) ? String.valueOf(obj[5]) : "");
		    registro.setVALORNOMINAL((obj[6] != null) ? String.valueOf(obj[6]) : "");
			registro.setMONEDA((obj[7] != null) ? String.valueOf(obj[7]) : "");
			registro.setFECHAEMISION((obj[8] != null) ? String.valueOf(obj[8]) : "");
			registro.setFECHAVENCIMIENTO((obj[9] != null) ? String.valueOf(obj[9]) : "");
			registro.setEMISOR((obj[10] != null) ? String.valueOf(obj[10]) : "");
		
			registros.getREGISTRO().add(registro);
		}
		return registros;
	}
	
}
