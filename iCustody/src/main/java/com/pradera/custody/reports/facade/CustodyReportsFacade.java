package com.pradera.custody.reports.facade;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.custody.reports.PortafolioDetailCustodiaTO;
import com.pradera.custody.reports.service.CustodyReportsService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CustodyReportsFacade.
 *
 * @author Pradera Technologies
 */

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class CustodyReportsFacade {
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
		
	/** The custody reports service. */
	@EJB
	private CustodyReportsService custodyReportsService;
	
	@Inject
	private PraderaLogger log;

	/**
	 * Send reports.
	 *
	 * @param lstPortafolioDetailCustodiaTO the list portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @param idParameterTablePk the id parameter table pk
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void sendReports(List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO, Long idReportPk
			, Integer idParameterTablePk) throws ServiceException, IOException, InterruptedException {
		LoggerUser objLoggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		custodyReportsService.sendReports(lstPortafolioDetailCustodiaTO, idReportPk, objLoggerUser, idParameterTablePk);				
	}
	
	/**
	 * Send reports.
	 *
	 * @param lstPortafolioDetailCustodiaTO the list portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @param idParameterTablePk the id parameter table pk
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void sendReportsSync(List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO, Long idReportPk
			, Integer idParameterTablePk) throws ServiceException, IOException, InterruptedException {
		LoggerUser objLoggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		custodyReportsService.sendReportsSync(lstPortafolioDetailCustodiaTO, idReportPk, objLoggerUser, idParameterTablePk);				
	}		
	
	/**
	 * Gets the holders by filter.
	 *
	 * @param filterTO the filter to
	 * @return the holders by filter
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getHoldersByFilter(HolderHelperOutputTO filterTO) throws ServiceException {
		return custodyReportsService.getHoldersByFilter(filterTO);
	}	
	
	/**
	 * Send report characteristic security.
	 *
	 * @param portafolioDetailCustodiaTO the portafolio detail custody to
	 * @param listReport the list report
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	public void sendReportCharacteristicSecurity(PortafolioDetailCustodiaTO portafolioDetailCustodiaTO, List<Long> listReport) throws ServiceException, IOException, InterruptedException {
		LoggerUser objLoggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		custodyReportsService.sendReportCharacteristicSecurity(portafolioDetailCustodiaTO, listReport, objLoggerUser);				
	}
	
	/**
	 * Gets the details pending expiration securities.
	 *
	 * @param filterReport the filter report
	 * @return the details pending expiration securities
	 * @throws ServiceException the service exception
	 */
	public List<PortafolioDetailCustodiaTO> getDetailsPendingExpirationSecurities(PortafolioDetailCustodiaTO filterReport) throws ServiceException {
		return custodyReportsService.getDetailsPendingExpirationSecurities(filterReport);
	}
	
	/**
	 * Gets the expiration securities issuer.
	 *
	 * @param filterReport the filter report
	 * @return the expiration securities issuer
	 * @throws ServiceException the service exception
	 */
	public List<PortafolioDetailCustodiaTO> getExpirationSecuritiesIssuer(PortafolioDetailCustodiaTO filterReport) throws ServiceException {
		return custodyReportsService.getExpirationSecuritiesIssuer(filterReport);
	}
	
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void processNominalValue() throws ServiceException {
		LoggerUser objLoggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		log.info("::::::::::::: OBTENEMOS EL COMPORTAMIENTO DEL PROCESO :::::::::::::");
		ParameterTable parameterTable = custodyReportsService.find(ParameterTable.class, GeneralConstants.IND_REPROCESS_NOMINAL_VALUE);
		if(Integer.parseInt(parameterTable.getIndicator1()) == GeneralConstants.ONE_VALUE_INTEGER){
			log.info("::::::::::::: REPROCESAMOS DESDE : " + parameterTable.getText1() + " HASTA: " + parameterTable.getText2());
			//DECLARAMOS LAS VARIABLE DE INICIO Y FIN 
			Date initialDate = CommonsUtilities.convertStringtoDate(parameterTable.getText1());
			Date endDate = CommonsUtilities.convertStringtoDate(parameterTable.getText2());
			while(initialDate.compareTo(endDate) <= 0){
				custodyReportsService.processNominalValue(initialDate,objLoggerUser);
				log.info("::::::::::::: SE PROCESO LA FECHA: "+CommonsUtilities.convertDatetoString(initialDate));
				initialDate = CommonsUtilities.addDate(initialDate, 1);
			}
		}else{
			log.info("::::::::::::: PROCESAMOS EL DIA ACTUAL :::::::::::::");
			Date processDate = CommonsUtilities.currentDate();
			custodyReportsService.processNominalValue(processDate,objLoggerUser);
		}
	}
	
}
