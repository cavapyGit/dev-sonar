package com.pradera.custody.reports.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.reports.PortafolioDetailCustodiaTO;
import com.pradera.custody.reports.facade.CustodyReportsFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * The Class ExpirationSecuritiesIssuerBatch.
 */
@BatchProcess(name="AutomaticCustodyReportsBatch")
public class AutomaticCustodyReportsBatch implements JobExecution, Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The custody reports facade. */
	@EJB
	private CustodyReportsFacade custodyReportsFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		logger.info("AutomaticCustodyReportsBatch: Starting the process ...");
		try {
			List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();			
			PortafolioDetailCustodiaTO objPortafolioDetailCustodiaTO;
			
			logger.info("ENVIO :::: INICIO REPORTE DE VENCIMIENTO DE VALORES POR EMISOR ");	
			Date currentDateAdd = CommonsUtilities.addDate(CommonsUtilities.currentDate(), 1);
			PortafolioDetailCustodiaTO filterReportOne = new PortafolioDetailCustodiaTO();
			filterReportOne.setInitialDate(currentDateAdd);
			filterReportOne.setEndDate(currentDateAdd);
			lstPortafolioDetailCustodiaTO = custodyReportsFacade.getExpirationSecuritiesIssuer(filterReportOne);						
			custodyReportsFacade.sendReports(lstPortafolioDetailCustodiaTO, ReportIdType.EXPIRATION_SECURITIES_BY_ISSUER.getCode(), 
					GeneralConstants.PK_SEND_CLOSE_DAY_REPORTS);	
			logger.info("ENVIO :::: FIN REPORTE DE VENCIMIENTO DE VALORES POR EMISOR ");
			
			logger.info("ENVIO :::: INICIO REPORTE DE DETALLE DE TITULOS PENDIENTES DE VENCIMIENTO ");	
			lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();
			PortafolioDetailCustodiaTO filterReport = new PortafolioDetailCustodiaTO();
			filterReport.setInitialDate(CommonsUtilities.currentDate());
			filterReport.setEndDate(CommonsUtilities.currentDate());	
			lstPortafolioDetailCustodiaTO = custodyReportsFacade.getDetailsPendingExpirationSecurities(filterReport);			
			custodyReportsFacade.sendReports(lstPortafolioDetailCustodiaTO, ReportIdType.REPORT_DETAILS_PENDING_EXPIRATION_SECURITIES.getCode(), 
					GeneralConstants.PK_SEND_CLOSE_DAY_REPORTS);	
			logger.info("ENVIO :::: FIN REPORTE DE DETALLE DE TITULOS PENDIENTES DE VENCIMIENTO ");						
			
			logger.info("ENVIO :::: INICIO REPORTE DE RESUMEN DE CONCILIACION DIARIA ");	
			lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();
			objPortafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
			objPortafolioDetailCustodiaTO.setProcessDate(CommonsUtilities.currentDate());			
			objPortafolioDetailCustodiaTO.setReportFormat(ReportFormatType.PDF.getCode());	
			lstPortafolioDetailCustodiaTO.add(objPortafolioDetailCustodiaTO);
			custodyReportsFacade.sendReports(lstPortafolioDetailCustodiaTO, ReportIdType.SUMMARY_RECONCILIATION_REPORT_DAILY.getCode(), 
					GeneralConstants.PK_SEND_CLOSE_DAY_REPORTS);
			logger.info("ENVIO :::: FIN REPORTE DE RESUMEN DE CONCILIACION DIARIA ");
			
			logger.info("ENVIO :::: INICIO - REPORTE DE CARTERA DE CLIENTES - DESMATERIALIZADA TOTAL ");	
			lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();
			
			objPortafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
			objPortafolioDetailCustodiaTO.setIssuanceType(IssuanceType.PHYSICAL.getCode());
			objPortafolioDetailCustodiaTO.setOptionBoolean(BooleanType.NO.getCode());
			objPortafolioDetailCustodiaTO.setProcessDate(CommonsUtilities.currentDate());			
			objPortafolioDetailCustodiaTO.setReportFormat(ReportFormatType.PDF.getCode());	
			lstPortafolioDetailCustodiaTO.add(objPortafolioDetailCustodiaTO);
			
			objPortafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
			objPortafolioDetailCustodiaTO.setIssuanceType(IssuanceType.DEMATERIALIZED.getCode());
			objPortafolioDetailCustodiaTO.setOptionBoolean(BooleanType.NO.getCode());
			objPortafolioDetailCustodiaTO.setProcessDate(CommonsUtilities.currentDate());			
			objPortafolioDetailCustodiaTO.setReportFormat(ReportFormatType.PDF.getCode());	
			lstPortafolioDetailCustodiaTO.add(objPortafolioDetailCustodiaTO);			
											
			custodyReportsFacade.sendReports(lstPortafolioDetailCustodiaTO, ReportIdType.REPORT_DEMATERIALIZED_TOTAL_PORTFOLIO_CLIENTS.getCode(), 
					GeneralConstants.PK_SEND_CLOSE_DAY_REPORTS);			
			logger.info("ENVIO :::: FIN - REPORTE DE CARTERA DE CLIENTES - DESMATERIALIZADA TOTAL ");
			
		} catch(Exception ex) {
			logger.error(":::::::::::::::::::::: ERROR AutomaticCustodyReportsBatch ::::::::::::::::::::::::");
			ex.printStackTrace();
		}		
		logger.info("AutomaticCustodyReportsBatch: Finishing the process ...");
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
