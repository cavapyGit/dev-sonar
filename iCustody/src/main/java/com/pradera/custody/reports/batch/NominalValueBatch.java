package com.pradera.custody.reports.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.reports.facade.CustodyReportsFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SendBlockAccreditationOperationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@BatchProcess(name="NominalValueBatch")
@RequestScoped
public class NominalValueBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The accreditation certificates service facade. */
	@EJB
	private CustodyReportsFacade custodyReportsFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	@EJB
	private ParameterServiceBean parameterService;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) 
	{
		try {
			log.info("::::::::::::: INICIO DEL BATCH :::::::::::::");
			custodyReportsFacade.processNominalValue();
			
//			ParameterTable parameterTable = parameterService.getParameterTableById(GeneralConstants.IND_REPROCESS_NOMINAL_VALUE);
//			if(Integer.parseInt(parameterTable.getIndicator1()) == GeneralConstants.ONE_VALUE_INTEGER){
//				log.info("::::::::::::: REPROCESAMOS DESDE : " + parameterTable.getText1() + " HASTA: " + parameterTable.getText2());
//				//DECLARAMOS LAS VARIABLE DE INICIO Y FIN 
//				Date initialDate = CommonsUtilities.convertStringtoDate(parameterTable.getText1());
//				Date endDate = CommonsUtilities.convertStringtoDate(parameterTable.getText2());
//				while(initialDate.compareTo(endDate) <= 0){
//					custodyReportsFacade.processNominalValue(initialDate);
//					log.info("::::::::::::: SE PROCESO LA FECHA: "+CommonsUtilities.convertDatetoString(initialDate));
//					initialDate = CommonsUtilities.addDate(initialDate, 1);
//				}
//			}else{
//				log.info("::::::::::::: PROCESAMOS EL DIA ACTUAL :::::::::::::");
//				Date processDate = CommonsUtilities.currentDate();
//				custodyReportsFacade.processNominalValue(processDate);
//			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
