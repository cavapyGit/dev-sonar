package com.pradera.custody.reports.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.reports.PortafolioDetailCustodiaTO;
import com.pradera.custody.reports.facade.CustodyReportsFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PortafolioDetailCustodyBatch.
 *
 * @author Pradera Technologies
 */

@BatchProcess(name="RelationCurrentBlockingBatch")
@RequestScoped
public class RelationCurrentBlockingBatch implements JobExecution , Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The fcc. */
	private String FCC = "FCC";
	
	/** The fci. */
	private String FCI = "FCI";
	
	/** The frd. */
	private String FRD = "FRD";
	
	private static final String CUI_453 = "453";
	
	private static final String CUI_454 = "454";
	
	/** The list type funds. */
	private List<String> lstTypeFunds =  new ArrayList<String>();
	
	/** The list economic activity. */
	private List<Integer> lstEconomicActivity = new ArrayList<Integer>();		

	/** The custody reports facade. */
	@EJB
	private CustodyReportsFacade custodyReportsFacade;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		logger.info("PortafolioDetailCustodyBatch: Starting the process ...");
		
		List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO;
		List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTOSend;
		
		lstEconomicActivity = fillEconomicActivity();
		lstTypeFunds = fillTypeFunds();
		Date currentDate = CommonsUtilities.currentDate();
		lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();
		
		try {
			for(Integer economicActivity : lstEconomicActivity){
				if(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode().equals(economicActivity)){
					for(String strFundType : lstTypeFunds){
						HolderHelperOutputTO objHolderHelperOutputTO = new HolderHelperOutputTO();
						objHolderHelperOutputTO.setEconomicActivity(economicActivity);
						objHolderHelperOutputTO.setStrFundType(strFundType);						
						List<Object[]> listHolders = custodyReportsFacade.getHoldersByFilter(objHolderHelperOutputTO);
						if(Validations.validateListIsNotNullAndNotEmpty(listHolders)){
							PortafolioDetailCustodiaTO objPortafolioDetailCustodiaTO;
							for(Object[] objHolders: listHolders){
								objPortafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
								if(Validations.validateIsNotNullAndNotEmpty(objHolders[2])){
									objPortafolioDetailCustodiaTO.setCuiHolder(objHolders[2].toString());
								}
								if(Validations.validateIsNotNullAndNotEmpty(objHolders[1])){
									objPortafolioDetailCustodiaTO.setCuiDescription(objHolders[1].toString());
								}
								if(Validations.validateIsNotNullAndNotEmpty(objHolders[5])){
									objPortafolioDetailCustodiaTO.setDocumentNumber(objHolders[5].toString());
								}
								if(Validations.validateIsNotNullAndNotEmpty(objHolders[4])){
									objPortafolioDetailCustodiaTO.setDocumentType(new Integer(objHolders[4].toString()));
								}
								if(Validations.validateIsNotNullAndNotEmpty(objHolders[6])){
									objPortafolioDetailCustodiaTO.setTransferNumber(objHolders[6].toString());;
								}	
								objPortafolioDetailCustodiaTO.setEconomicActivity(String.valueOf(economicActivity));
								objPortafolioDetailCustodiaTO.setDescEconomicActivity(EconomicActivityType.get(economicActivity).getValue());
								objPortafolioDetailCustodiaTO.setFoundType(strFundType);
								objPortafolioDetailCustodiaTO.setDescFoundType(strFundType);
								objPortafolioDetailCustodiaTO.setProcessDate(currentDate);
								lstPortafolioDetailCustodiaTO.add(objPortafolioDetailCustodiaTO);
							}
						}												
					}
				}else{
					HolderHelperOutputTO objHolderHelperOutputTO = new HolderHelperOutputTO();
					objHolderHelperOutputTO.setEconomicActivity(economicActivity);											
					List<Object[]> listHolders = custodyReportsFacade.getHoldersByFilter(objHolderHelperOutputTO);
					if(Validations.validateListIsNotNullAndNotEmpty(listHolders)){
						PortafolioDetailCustodiaTO objPortafolioDetailCustodiaTO;
						for(Object[] objHolders: listHolders){
							objPortafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();								
							objPortafolioDetailCustodiaTO.setCuiHolder(objHolders[2].toString());
							objPortafolioDetailCustodiaTO.setCuiDescription(objHolders[1].toString());
							objPortafolioDetailCustodiaTO.setDocumentNumber(objHolders[5].toString());
							objPortafolioDetailCustodiaTO.setDocumentType(new Integer(objHolders[4].toString()));
							objPortafolioDetailCustodiaTO.setTransferNumber(objHolders[6].toString());
							objPortafolioDetailCustodiaTO.setEconomicActivity(String.valueOf(economicActivity));
							objPortafolioDetailCustodiaTO.setDescEconomicActivity(EconomicActivityType.get(economicActivity).getValue());
							objPortafolioDetailCustodiaTO.setProcessDate(currentDate);
							lstPortafolioDetailCustodiaTO.add(objPortafolioDetailCustodiaTO);
						}
					}
				}
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstPortafolioDetailCustodiaTO)){
				lstPortafolioDetailCustodiaTOSend = new ArrayList<PortafolioDetailCustodiaTO>();
				List<PortafolioDetailCustodiaTO> lstAuxPortafolio = new ArrayList<PortafolioDetailCustodiaTO>(lstPortafolioDetailCustodiaTO);	
				PortafolioDetailCustodiaTO objPortafolioTwo;
				for(int i=0; i < lstAuxPortafolio.size(); i++){
					objPortafolioTwo = new PortafolioDetailCustodiaTO();
					if(lstAuxPortafolio.get(i).getCuiHolder().equals(CUI_453) || 
							lstAuxPortafolio.get(i).getCuiHolder().equals(CUI_454)){
						continue;
					}else{
						objPortafolioTwo.setCuiHolder(lstAuxPortafolio.get(i).getCuiHolder());
						objPortafolioTwo.setCuiDescription(lstAuxPortafolio.get(i).getCuiDescription());
						objPortafolioTwo.setDocumentNumber(lstAuxPortafolio.get(i).getDocumentNumber());
						objPortafolioTwo.setDocumentType(lstAuxPortafolio.get(i).getDocumentType());
						objPortafolioTwo.setTransferNumber(lstAuxPortafolio.get(i).getTransferNumber());
						objPortafolioTwo.setEconomicActivity(lstAuxPortafolio.get(i).getEconomicActivity());	
						objPortafolioTwo.setDescEconomicActivity(lstAuxPortafolio.get(i).getDescEconomicActivity());
						objPortafolioTwo.setDescFoundType(lstAuxPortafolio.get(i).getDescFoundType());
						objPortafolioTwo.setProcessDate(lstAuxPortafolio.get(i).getProcessDate());
						objPortafolioTwo.setReportFormat(ReportFormatType.PDF.getCode());										
						lstPortafolioDetailCustodiaTOSend.add(objPortafolioTwo);				
					}
				}
				logger.info("PortafolioDetailCustodyBatch: size list portafolio  ..." + lstPortafolioDetailCustodiaTOSend.size());
				custodyReportsFacade.sendReports(lstPortafolioDetailCustodiaTOSend, 
						ReportIdType.RELATION_CURRENT_BLOCKING.getCode(), GeneralConstants.PK_BLOQUEOS_VIGENTES);				
			}
			
		} catch(Exception ex) {
			logger.error(":::::::::::::::::::::: ERROR PortafolioDetailCustodyBatch ::::::::::::::::::::::::");
			ex.printStackTrace();
		}		
		logger.info("PortafolioDetailCustodyBatch: Finishing the process ...");
	}
	
	/**
	 * Fill economic activity.
	 *
	 * @return the list
	 */
	public List<Integer> fillEconomicActivity(){
		List<Integer> lstEconomicActivity = new ArrayList<Integer>();
		lstEconomicActivity.add(EconomicActivityType.ADMINISTRADORAS_FONDOS_PENSIONES.getCode());
		lstEconomicActivity.add(EconomicActivityType.COMPANIAS_SEGURO.getCode());		
		return lstEconomicActivity;
	}
	
	/**
	 * Fill type funds.
	 *
	 * @return the list
	 */
	private List<String> fillTypeFunds(){
		List<String> lstFunds= new ArrayList<String>();		
		lstFunds.add(FCC);
		lstFunds.add(FCI);
		lstFunds.add(FRD);
		return lstFunds;
	}
	
	/**
	 * Fill economic activity.
	 *
	 * @return the list
	 */
	public List<Integer> fillReportFormat(){
		List<Integer> lstReportFormat = new ArrayList<Integer>();
		lstReportFormat.add(ReportFormatType.PDF.getCode());
		lstReportFormat.add(ReportFormatType.TXT.getCode());		
		return lstReportFormat;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
