package com.pradera.custody.reports.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.FileData;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.report.ReportCodeType;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.custody.reports.CustodyReportsSender;
import com.pradera.custody.reports.PortafolioDetailCustodiaTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.report.type.ReportFormatType;
import com.pradera.model.report.type.ReportLoggerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CustodyReportsService.
 *
 * @author Pradera Technologies
 */

@Stateless
public class CustodyReportsService extends CrudDaoServiceBean {
	
	
	/** The custody reports sender. */
	@EJB 
	private CustodyReportsSender custodyReportsSender;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Send reports.
	 *
	 * @param lstPortafolioDetailCustodiaTO the list portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @param objLoggerUser the object logger user
	 * @param idParameterTablePk the id parameter table pk
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@Asynchronous	
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void sendReports(List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO, Long idReportPk, 
			LoggerUser objLoggerUser, Integer idParameterTablePk) throws ServiceException, IOException, InterruptedException {
			
		//Envio a procesar todos los reportes
		 List<Object[]> lstObjReportLogger  = new ArrayList<>();
		lstObjReportLogger = custodyReportsSender.sendReports(lstPortafolioDetailCustodiaTO, idReportPk, objLoggerUser);		
		List<FileData> listDataReport = new ArrayList<FileData>();
		List<Long> listReportCodeType = ReportCodeType.listPk;	
		String nameReport = GeneralConstants.NAME_REPORT_DEAFULT;
		String strMnemonicReport = GeneralConstants.EMPTY_STRING;	
		String strDate = GeneralConstants.EMPTY_STRING;			
		if(Validations.validateIsNotNullAndNotEmpty(idParameterTablePk)) {
			
			//Verifico uno a uno los reportes para saber si terminaron
			for(Object[] objReportLogger : lstObjReportLogger){		
				FileData objFileData = new FileData();
				Long idReportLoggerPk = new Long(objReportLogger[0].toString());
				
				//Verificamos si finalizo
				Boolean isExecutingProcess = isExecutingBatchProcess(idReportLoggerPk);
				int cont=0;
				while(!isExecutingProcess && cont<10) {
					
					//Si el reporte no termino  mando sleep
					log.info("SLEEP :::::::::::"+isExecutingProcess+"-"+cont+"-"+idReportLoggerPk);					
					Thread.sleep(60000);
					
					//Verifico si ya finalizo
					isExecutingProcess= isExecutingBatchProcess(idReportLoggerPk);
					if (isExecutingProcess){ cont=10; } cont++;
				}
				 
				//Si el proceso finalizo genero el archivo en txt y lo mando al FTP con los nombres que corresponde
				if(isExecutingProcess) {
					Object [] objDataReport = custodyReportsSender.getDataReportObject(idReportLoggerPk);
					if(objDataReport != null){
						
						//0 NameFile 1Ruta
						//Path path = Paths.get(objDataReport[1].toString()+objDataReport[0].toString());
						//byte[] dataReport = Files.readAllBytes(path);
						byte [] dataReport = (byte []) objDataReport[1]; 
						String nameFile =  objDataReport[0].toString();
						if(listReportCodeType.contains(idReportPk)) {
							String strFormat = ReportFormatType.PDF.getValue();
							strMnemonicReport = ReportCodeType.get(idReportPk).getMnemonic();
							strDate = CommonsUtilities.convertDateToStringReport(CommonsUtilities.currentDate());
							nameFile = nameReport + strMnemonicReport;
							if(Validations.validateIsNotNullAndNotEmpty(objReportLogger[4])) {
								nameFile = nameFile + objReportLogger[4].toString() + GeneralConstants.UNDERLINE_SEPARATOR;
							}
							if(Validations.validateIsNotNullAndNotEmpty(objReportLogger[5])) {
								if(IssuanceType.DEMATERIALIZED.getCode().equals(new Integer(objReportLogger[5].toString()))) {
									nameFile = nameFile + "DESMAT" + GeneralConstants.UNDERLINE_SEPARATOR;
								} else {
									nameFile = nameFile + "FISICO" + GeneralConstants.UNDERLINE_SEPARATOR;
								}								
							}
							nameFile = nameFile + strDate + GeneralConstants.DOT + strFormat;
						} else {
							nameFile = createNameFile(objReportLogger, idReportPk);
						}						
						objFileData.setReportName(nameFile);
						objFileData.setReportData(dataReport);
						listDataReport.add(objFileData);						
					}
				}
				
			}
			if(Validations.validateListIsNotNullAndNotEmpty(listDataReport)){
				custodyReportsSender.sendFtpUpload(listDataReport, idParameterTablePk);
			}
		}						
	}
	
	/**
	 * *
	 * Method to split all list in chunks.
	 *
	 * @param securitiesCode the securities code
	 * @return the string by max row
	 * @throws ServiceException the service exception
	 */
	public static List<List<PortafolioDetailCustodiaTO>> getlstPortfolioChunk(List<PortafolioDetailCustodiaTO> lstPortaf) throws ServiceException{
		
		List<PortafolioDetailCustodiaTO> lstSubAddPortaf =  new ArrayList<>();
		List<List<PortafolioDetailCustodiaTO>> getListChunk = new ArrayList<>();
		
		Integer count = 0, site =0;
		
		for(PortafolioDetailCustodiaTO lstlstPortaf : lstPortaf){
			lstSubAddPortaf.add(lstlstPortaf);
			site++; count++;
			if(count == 15 || lstPortaf.size() == site){
				getListChunk.add(lstSubAddPortaf);
				lstSubAddPortaf =  new ArrayList<>();
				count = 0;
			}
		}
		return getListChunk;
	}
	
	/**
	 * Send reports Sync.
	 * Exactamente el mismo metodo que sendReports Excepto que este no es Asincrono
	 * @param lstPortafolioDetailCustodiaTO the list portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @param objLoggerUser the object logger user
	 * @param idParameterTablePk the id parameter table pk
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@TransactionTimeout(unit=TimeUnit.HOURS,value=1)
	public void sendReportsSync(List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO, Long idReportPk, 
			LoggerUser objLoggerUser, Integer idParameterTablePk) throws ServiceException, IOException, InterruptedException {
			
		//Envio a procesar todos los reportes
		
		List<List<PortafolioDetailCustodiaTO>> getlstPortfolioChunk = getlstPortfolioChunk(lstPortafolioDetailCustodiaTO);
		
		for(List<PortafolioDetailCustodiaTO> lstPortfolioChunk : getlstPortfolioChunk){
			List<Object[]> lstObjReportLogger  = new ArrayList<>();
			lstObjReportLogger = custodyReportsSender.sendReports(lstPortfolioChunk, idReportPk, objLoggerUser);		
			List<FileData> listDataReport = new ArrayList<FileData>();
			List<Long> listReportCodeType = ReportCodeType.listPk;	
			String nameReport = GeneralConstants.NAME_REPORT_DEAFULT;
			String strMnemonicReport = GeneralConstants.EMPTY_STRING;	
			String strDate = GeneralConstants.EMPTY_STRING;			
			if(Validations.validateIsNotNullAndNotEmpty(idParameterTablePk)) {
				
				//Verifico uno a uno los reportes para saber si terminaron
				for(Object[] objReportLogger : lstObjReportLogger){		
					FileData objFileData = new FileData();
					Long idReportLoggerPk = new Long(objReportLogger[0].toString());
					
					//Verificamos si finalizo
					Boolean isExecutingProcess = isExecutingBatchProcess(idReportLoggerPk);
					int cont=0;
					while(!isExecutingProcess && cont<15) {
						
						//Si el reporte no termino  mando sleep
						log.info("SLEEP ::::::::::: "+isExecutingProcess+" - "+cont+" - "+idReportLoggerPk);					
						Thread.sleep(60000);
						
						//Verifico si ya finalizo
						isExecutingProcess= isExecutingBatchProcess(idReportLoggerPk);
						if (isExecutingProcess){ cont=20; } cont++;
					}
					 
					//Si el proceso finalizo genero el archivo en txt y lo mando al FTP con los nombres que corresponde
					if(isExecutingProcess) {
						Object [] objDataReport = custodyReportsSender.getDataReportObject(idReportLoggerPk);
						if(objDataReport != null){
							
							//0 NameFile 1Ruta
							//Path path = Paths.get(objDataReport[1].toString()+objDataReport[0].toString());
							//byte[] dataReport = Files.readAllBytes(path);
							byte [] dataReport = (byte []) objDataReport[1]; 
							String nameFile =  objDataReport[0].toString();
							if(listReportCodeType.contains(idReportPk)) {
								String strFormat = ReportFormatType.PDF.getValue();
								strMnemonicReport = ReportCodeType.get(idReportPk).getMnemonic();
								strDate = CommonsUtilities.convertDateToStringReport(CommonsUtilities.currentDate());
								nameFile = nameReport + strMnemonicReport;
								if(Validations.validateIsNotNullAndNotEmpty(objReportLogger[4])) {
									nameFile = nameFile + objReportLogger[4].toString() + GeneralConstants.UNDERLINE_SEPARATOR;
								}
								if(Validations.validateIsNotNullAndNotEmpty(objReportLogger[5])) {
									if(IssuanceType.DEMATERIALIZED.getCode().equals(new Integer(objReportLogger[5].toString()))) {
										nameFile = nameFile + "DESMAT" + GeneralConstants.UNDERLINE_SEPARATOR;
									} else {
										nameFile = nameFile + "FISICO" + GeneralConstants.UNDERLINE_SEPARATOR;
									}								
								}
								nameFile = nameFile + strDate + GeneralConstants.DOT + strFormat;
							} else {
								nameFile = createNameFile(objReportLogger, idReportPk);
							}						
							objFileData.setReportName(nameFile);
							objFileData.setReportData(dataReport);
							listDataReport.add(objFileData);						
						}
					}
					
				}
				if(Validations.validateListIsNotNullAndNotEmpty(listDataReport)){
					for (FileData test : listDataReport){
						log.info("FTP_TEST :" + test.getReportName());
					}
					custodyReportsSender.sendFtpUpload(listDataReport, idParameterTablePk);
				}
			}	
		}
	}
	
	/**
	 * Create Name File For Send ASFI FCC.
	 *
	 * @param objectReport the object report
	 * @param idReportPk the id report pk
	 * @return the string
	 */
	public String createNameFile(Object[] objectReport, Long idReportPk){
		Date dateSend=CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1);
		
		//si es el report FCC - FCI - FRUV
		if(idReportPk.equals(ReportIdType.PORTAFOLIO_DETAIL_CUSTODY.getCode())){
			dateSend = (Date)objectReport[6];
		}
		
		StringBuilder nameFile=new StringBuilder();
		String holderCUI=objectReport[3].toString();
		String transferNumber=objectReport[1].toString();
		String strFormat = ReportFormatType.TXT.getValue();
		String dateString=CommonsUtilities.convertDatetoStringForTemplate(dateSend, CommonsUtilities.DATE_PATTERN_BILLING);
		String fci="83CC0";
		String fruv="83CC0";

		if(ReportFormatType.TXT.getCode().equals(new Integer(objectReport[2].toString()))) {
			if(listCuiFCC()!=null && listCuiFCC().contains(holderCUI)){
				nameFile.append(transferNumber);
				nameFile.append(ReportFormatType.CC.getValue());
//				nameFile.append(GeneralConstants.ZERO_VALUE_STRING);
				strFormat = ReportFormatType.FCC.getValue();
			}else if(listCuiFRUV()!=null && listCuiFRUV().contains(holderCUI)){
				nameFile.append(fruv);
				nameFile.append(transferNumber);
				strFormat = ReportFormatType.FRD.getValue();
			}else if(listCuiFCI()!=null && listCuiFCI().contains(holderCUI)){
				nameFile.append(fci);
				nameFile.append(transferNumber);
				strFormat = ReportFormatType.FCI.getValue();
			}else{
				nameFile.append(transferNumber);
				strFormat = ReportFormatType.CC.getValue();
			}
			
		}else if(ReportFormatType.PDF.getCode().equals(new Integer(objectReport[2].toString()))){
			if(idReportPk.equals(ReportIdType.RELATION_CURRENT_BLOCKING.getCode())){
				dateSend = CommonsUtilities.currentDate();
				dateString = CommonsUtilities.convertDatetoStringForTemplate(dateSend, CommonsUtilities.DATE_PATTERN_BILLING);
			}
			nameFile.append(holderCUI);
			strFormat = ReportFormatType.PDF.getValue();
		}
		
		nameFile.append(dateString);
		nameFile.append(GeneralConstants.DOT).append(strFormat);
		
		return nameFile.toString();
	}
	
	/**
	 * Gets the data report object.
	 *
	 * @param idReportLoggerPk the id report logger pk
	 * @return the data report object
	 * @throws ServiceException the service exception
	 */
	public Object [] getDataReportObject(Long idReportLoggerPk) throws ServiceException {
		return custodyReportsSender.getDataReportObject(idReportLoggerPk);
	}
	
	/**
	 * Gets the holders by filter.
	 *
	 * @param filterTO the filter to
	 * @return the holders by filter
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getHoldersByFilter(HolderHelperOutputTO filterTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
 		sbQuery.append("select distinct h.fund_type ");//0
 		sbQuery.append(",  h.full_name ");//1
 		sbQuery.append(",  h.id_holder_pk ");//2
 		sbQuery.append(",  h.economic_activity ");//3
 		sbQuery.append(",  h.document_type ");//4
 		sbQuery.append(",  h.document_number ");//5
 		sbQuery.append(",  h.transfer_number ");//6
 		sbQuery.append(" from holder h ");
 		sbQuery.append(" where 1=1 ");
 		if(Validations.validateIsNotNullAndPositive(filterTO.getHolderId())){
 			sbQuery.append(" and h.id_holder_pk= :holderPk");			
 		} else if(Validations.validateIsNotNullAndNotEmpty(filterTO.getFundCUI())){
 			sbQuery.append(" and h.id_holder_pk= :fundCUI");
 		}
 		if(Validations.validateIsNotNull(filterTO.getLstFundType()) && filterTO.getLstFundType().size()>0){
 			sbQuery.append(" and h.fund_type in :lisFoundType");			
 		} else if(Validations.validateIsNotNullAndNotEmpty(filterTO.getStrFundType())){
 			sbQuery.append(" and h.fund_type= :foundType");			
 		}
 		if(Validations.validateIsNotNullAndPositive(filterTO.getEconomicActivity())){
 			sbQuery.append(" and h.economic_activity= :economicAct");			
 		} else if(Validations.validateListIsNotNullAndNotEmpty(filterTO.getLstEconomicActivity())){
 			sbQuery.append(" and h.economic_activity in (:lstEconomicAct)");			
 		}
 		sbQuery.append(" order by h.id_holder_pk "); 		
 		Query query = em.createNativeQuery(sbQuery.toString()); 		
 		if(Validations.validateIsNotNull(filterTO.getHolderId())){
 			query.setParameter("holderPk", filterTO.getHolderId());
 		} else if(Validations.validateIsNotNull(filterTO.getFundCUI())){
 			query.setParameter("fundCUI", filterTO.getFundCUI());
 		}
 		if(Validations.validateIsNotNull(filterTO.getLstFundType()) && filterTO.getLstFundType().size()>0){
 			query.setParameter("lisFoundType", filterTO.getLstFundType());			
 		} else if(Validations.validateIsNotNullAndNotEmpty(filterTO.getStrFundType())){
 			query.setParameter("foundType", filterTO.getStrFundType());			
 		}
 		if(Validations.validateIsNotNullAndPositive(filterTO.getEconomicActivity())){
 			query.setParameter("economicAct", filterTO.getEconomicActivity());				
 		} else if(Validations.validateListIsNotNullAndNotEmpty(filterTO.getLstEconomicActivity())){
 			query.setParameter("lstEconomicAct", filterTO.getLstEconomicActivity());			
 		}
 		return query.getResultList();
 	}
	
	/**
	 * Send report characteristic security.
	 *
	 * @param portafolioDetailCustodiaTO the portafolio detail custodia to
	 * @param listReport the list report
	 * @param objLoggerUser the obj logger user
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@Asynchronous
	public void sendReportCharacteristicSecurity(PortafolioDetailCustodiaTO portafolioDetailCustodiaTO,List<Long> listReport, 
			LoggerUser objLoggerUser) throws ServiceException, IOException, InterruptedException {
	
		String tipoReporte = null;
		List<Object[]> lstObjReportLogger= custodyReportsSender.sendReportCharacteristicSecurity(portafolioDetailCustodiaTO, listReport, objLoggerUser);	
		
		for(Object[] objReportLogger : lstObjReportLogger){
			Long idReportLoggerPk = new Long(objReportLogger[0].toString());	

			boolean isExecutingProcess= isExecutingBatchProcess(idReportLoggerPk);
			int cont=0;
			while(!isExecutingProcess && cont<5) {
				log.info("SLEEP :::::::::::"+isExecutingProcess+cont+idReportLoggerPk);
				
				try {
					Thread.sleep(10*1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				isExecutingProcess= isExecutingBatchProcess(idReportLoggerPk);
				if (isExecutingProcess){
					cont=5;
				}
				
				cont++;
			}
			
		        
			if (isExecutingProcess) {
				Object [] objDataReport = custodyReportsSender.getDataReportObject(idReportLoggerPk);
				if(objDataReport != null){	
					String strFormat = ReportFormatType.TXT.getValue();
					if(ReportFormatType.TXT.getCode().equals(new Integer(objReportLogger[1].toString()))) {
						strFormat = "EDB";
					} else {
						strFormat = ReportFormatType.PDF.getValue();
					}
					tipoReporte = objReportLogger[2].toString();
					String nameFile = "M"
							+ CommonsUtilities.convertDatetoStringForTemplate(portafolioDetailCustodiaTO.getProcessDate(), CommonsUtilities.DATE_PATTERN_MINIMAL_REV)+tipoReporte
							+ GeneralConstants.DOT + strFormat;
//					Path path = Paths.get(objDataReport[1].toString()+objDataReport[0].toString());
//					byte [] dataReport = Files.readAllBytes(path);
					byte [] dataReport = (byte []) objDataReport[1];
					custodyReportsSender.sendFtpUpload(nameFile, dataReport,GeneralConstants.PK_INFORMES_ASFI);
				}	
			}
			
			
		}
	}
	
	/**
	 * Checks if is executing batch process.
	 *
	 * @param idReportLoggerPk the id report logger pk
	 * @return true, if is executing batch process
	 */
	private boolean isExecutingBatchProcess(Long idReportLoggerPk){
		
		boolean reportExist = false;
		BigDecimal reportLogger = null;
		
		StringBuilder sbQuery = new StringBuilder();
 		sbQuery.append(" SELECT r.ID_REPORT_LOGGER_PK ");
 		sbQuery.append(" from REPORT_LOGGER r ");
 		sbQuery.append(" where r.ID_REPORT_LOGGER_PK = :idReportLoggerPk ");
 		sbQuery.append(" and r.REPORT_STATE = :reportState ");
		
 		Query query = em.createNativeQuery(sbQuery.toString());
 		query.setParameter("idReportLoggerPk", idReportLoggerPk);
 		query.setParameter("reportState", ReportLoggerStateType.FINISHED.getCode());
 		
 		try {
 			reportLogger = (BigDecimal)query.getSingleResult();
 			if (reportLogger!=null){
 	 			reportExist=true;
 	 		}
		} catch (Exception e) {
			reportExist=false;
		}
 		return reportExist;
	}

	/**
	 * List CUI FRUV.
	 *
	 * @return the list
	 */
	public  List<String> listCuiFRUV(){
		List<String> listCUI=new ArrayList<String>();
		listCUI.add("130125");
		return listCUI;
	}
	
	/**
	 * List CUI FCC.
	 *
	 * @return the list
	 */
	public  List<String> listCuiFCC(){
		List<String> listCUI=new ArrayList<String>();
		listCUI.add("453");
		listCUI.add("454");
		return listCUI;
	}
	
	/**
	 * List CUI FCI.
	 *
	 * @return the list
	 */
	public  List<String> listCuiFCI(){
		List<String> listCUI=new ArrayList<String>();
		listCUI.add("451");
		listCUI.add("452");
		return listCUI;
	}
	
	/**
	 * List CUI Aseg.
	 *
	 * @return the list
	 */
	public  List<String> listCuiAseg(){
		List<String> listCUI=new ArrayList<String>();
		
		return listCUI;
	}
	
	/**
	 * Gets the details pending expiration securities.
	 *
	 * @param filterReport the filter report
	 * @return the details pending expiration securities
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<PortafolioDetailCustodiaTO> getDetailsPendingExpirationSecurities(PortafolioDetailCustodiaTO filterReport) throws ServiceException {
		StringBuilder stringBuilderQry = new StringBuilder();
		PortafolioDetailCustodiaTO portafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
		List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();		
		stringBuilderQry.append(" 	SELECT  ISSU.ID_ISSUER_PK , ISSU.BUSINESS_NAME  															");		
		stringBuilderQry.append("  	FROM HOLDER_ACCOUNT_BALANCE HAB  																					");
		stringBuilderQry.append(" 	INNER JOIN SECURITY SEC   						ON SEC.ID_SECURITY_CODE_PK = HAB.ID_SECURITY_CODE_PK                ");
		stringBuilderQry.append("   INNER JOIN HOLDER_ACCOUNT HA                  	ON HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK 				");
		stringBuilderQry.append("	INNER JOIN ISSUER ISSU                          ON SEC.ID_ISSUER_FK = ISSU.ID_ISSUER_PK 							");
		stringBuilderQry.append("	INNER JOIN PARTICIPANT PART                     ON PART.ID_PARTICIPANT_PK = HAB.ID_PARTICIPANT_PK  					");
		stringBuilderQry.append("	INNER JOIN interest_payment_schedule IPS      	ON SEC.id_security_code_pk = IPS.id_security_code_fk  				");
		stringBuilderQry.append("   INNER JOIN program_interest_coupon PIC        	ON IPS.id_int_payment_schedule_pk = PIC.id_int_payment_schedule_fk	");
		stringBuilderQry.append("   INNER JOIN amortization_payment_schedule APS   	ON APS.ID_SECURITY_CODE_FK = SEC.id_security_code_pk 				");
		stringBuilderQry.append("   INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC   	ON (PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK ");
		stringBuilderQry.append("    													AND TRUNC(PIC.EXPERITATION_DATE) = TRUNC(PAC.EXPRITATION_DATE)) ");
		stringBuilderQry.append("	WHERE  HAB.TOTAL_BALANCE > 0		                                            									");		
		stringBuilderQry.append("	AND trunc(PIC.EXPERITATION_DATE) >= to_date(:dateInitial,'dd/MM/yyyy') 				                         		");
		stringBuilderQry.append("	AND trunc(PIC.EXPERITATION_DATE) <= to_date(:dateEnd,'dd/MM/yyyy')  												");	
		stringBuilderQry.append("	AND SEC.SECURITY_CLASS NOT IN (415,414,420)  																			");
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filterReport.getInitialDate())){
			query.setParameter("dateInitial", CommonsUtilities.convertDatetoString(filterReport.getInitialDate()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filterReport.getEndDate())){
			query.setParameter("dateEnd", CommonsUtilities.convertDatetoString(filterReport.getEndDate()));
		}			
		List<Object[]> lstResultReport = query.getResultList();	
		List<String> listIssuerPk = new ArrayList<String>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResultReport)) {
			for(Object[] objectReport : lstResultReport){				
				if(!listIssuerPk.contains(objectReport[0].toString())) {
					listIssuerPk.add(objectReport[0].toString());
					portafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
					portafolioDetailCustodiaTO.setOptionBoolean(BooleanType.YES.getCode());
					portafolioDetailCustodiaTO.setIssuerCode(objectReport[0].toString());
					portafolioDetailCustodiaTO.setIssuerDescription(objectReport[1].toString());
					portafolioDetailCustodiaTO.setInitialDate(filterReport.getInitialDate());
					portafolioDetailCustodiaTO.setEndDate(filterReport.getEndDate());
					portafolioDetailCustodiaTO.setReportFormat(ReportFormatType.PDF.getCode());	
					lstPortafolioDetailCustodiaTO.add(portafolioDetailCustodiaTO);
				}				
			}
		} else {
			lstPortafolioDetailCustodiaTO = null;
		}		
		return lstPortafolioDetailCustodiaTO;
	}
	
	/**
	 * Gets the expiration securities issuer.
	 *
	 * @param filterReport the filter report
	 * @return the expiration securities issuer
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<PortafolioDetailCustodiaTO> getExpirationSecuritiesIssuer(PortafolioDetailCustodiaTO filterReport) throws ServiceException {
		StringBuilder stringBuilderQry = new StringBuilder();
		PortafolioDetailCustodiaTO portafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
		List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO = new ArrayList<PortafolioDetailCustodiaTO>();		
		stringBuilderQry.append(" 	SELECT DATA_FORM.ID_ISSUER_PK, DATA_FORM.BUSINESS_NAME FROM (  											");		
		stringBuilderQry.append("  	SELECT I.ID_ISSUER_PK , I.BUSINESS_NAME FROM HOLDER_ACCOUNT_BALANCE hab 											");
		stringBuilderQry.append("   INNER JOIN SECURITY S                         ON s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK               		");
		stringBuilderQry.append("   INNER JOIN HOLDER_ACCOUNT ha                  ON hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK 					");
		stringBuilderQry.append("	INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK									");
		stringBuilderQry.append("	INNER JOIN ISSUANCE ISS                       ON ISS.ID_ISSUANCE_CODE_PK = S.ID_ISSUANCE_CODE_FK					");
		stringBuilderQry.append("	INNER JOIN interest_payment_schedule ips      ON s.id_security_code_pk=ips.id_security_code_fk						");
		stringBuilderQry.append("   INNER JOIN program_interest_coupon pic        ON ips.id_int_payment_schedule_pk=pic.id_int_payment_schedule_fk		");
		stringBuilderQry.append("   WHERE trunc(PIC.EXPERITATION_DATE) >= to_date(:dateInitial,'dd/MM/yyyy')											");
		stringBuilderQry.append("   AND trunc(PIC.EXPERITATION_DATE) <= to_date(:dateEnd,'dd/MM/yyyy') 													");
		stringBuilderQry.append("   AND HAB.TOTAL_BALANCE > 0  																							");
		stringBuilderQry.append("   AND S.SECURITY_CLASS NOT IN (415,414,420)  																			");
		stringBuilderQry.append("	UNION SELECT I.ID_ISSUER_PK , I.BUSINESS_NAME FROM SECURITY S 														");		
		stringBuilderQry.append("	INNER JOIN ISSUER I                           ON S.ID_ISSUER_FK = I.ID_ISSUER_PK 									");
		stringBuilderQry.append("	INNER JOIN ISSUANCE ISS                       ON ISS.ID_ISSUANCE_CODE_PK = S.ID_ISSUANCE_CODE_FK 					");		
		stringBuilderQry.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE hab         on s.ID_SECURITY_CODE_PK=hab.ID_SECURITY_CODE_PK 						");	
		stringBuilderQry.append("	INNER JOIN holder_account ha                  on hab.ID_HOLDER_ACCOUNT_PK= ha.ID_HOLDER_ACCOUNT_PK 					");	
		stringBuilderQry.append("	INNER JOIN amortization_payment_schedule APS  ON APS.ID_SECURITY_CODE_FK = S.id_security_code_pk 					");	
		stringBuilderQry.append("	INNER JOIN PROGRAM_AMORTIZATION_COUPON PAC    ON PAC.ID_AMO_PAYMENT_SCHEDULE_FK = APS.ID_AMO_PAYMENT_SCHEDULE_PK	");		
		stringBuilderQry.append("	WHERE trunc(PAC.expritation_date) >= to_date(:dateInitial,'dd/MM/yyyy') 											");
		stringBuilderQry.append("	AND trunc(PAC.expritation_date) <= to_date(:dateEnd,'dd/MM/yyyy') 													");
		stringBuilderQry.append("	AND HAB.TOTAL_BALANCE > 0 AND S.SECURITY_CLASS NOT IN (415,414,420) ) DATA_FORM 									");				
		Query query = em.createNativeQuery(stringBuilderQry.toString());		
		if(Validations.validateIsNotNullAndNotEmpty(filterReport.getInitialDate())){
			query.setParameter("dateInitial", CommonsUtilities.convertDatetoString(filterReport.getInitialDate()));
		}
		if(Validations.validateIsNotNullAndNotEmpty(filterReport.getEndDate())){
			query.setParameter("dateEnd", CommonsUtilities.convertDatetoString(filterReport.getEndDate()));
		}			
		List<Object[]> lstResultReport = query.getResultList();	
		List<String> listIssuerPk = new ArrayList<String>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResultReport)) {
			for(Object[] objectReport : lstResultReport){
				if(!listIssuerPk.contains(objectReport[0].toString())) {
					portafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();				
					portafolioDetailCustodiaTO.setIssuerCode(objectReport[0].toString());
					portafolioDetailCustodiaTO.setIssuerDescription(objectReport[1].toString());
					portafolioDetailCustodiaTO.setInitialDate(filterReport.getInitialDate());
					portafolioDetailCustodiaTO.setEndDate(filterReport.getEndDate());
					portafolioDetailCustodiaTO.setReportFormat(ReportFormatType.PDF.getCode());	
					lstPortafolioDetailCustodiaTO.add(portafolioDetailCustodiaTO);
				}				
			}
		} else {
			lstPortafolioDetailCustodiaTO = null;
		}		
		return lstPortafolioDetailCustodiaTO;
	}
	
	public void processNominalValue(Date processDate,LoggerUser loggerUser) throws ServiceException{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_PROCESS_NOMINAL_VALUE(");
		stringBuffer.append(" 	:processDate, ");
		stringBuffer.append(" 	:lastUser, ");
		stringBuffer.append(" 	:lastApp, ");
		stringBuffer.append(" 	:lastDate, ");
		stringBuffer.append(" 	:lastIP); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("processDate", processDate);
		query.setParameter("lastUser", loggerUser.getUserName());
		query.setParameter("lastApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastDate", loggerUser.getAuditTime());
		query.setParameter("lastIP", loggerUser.getIpAddress());
		
		query.executeUpdate();
	}
	
}
