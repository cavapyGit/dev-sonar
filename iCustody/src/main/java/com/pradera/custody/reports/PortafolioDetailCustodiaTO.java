package com.pradera.custody.reports;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PortafolioDetailCustodiaTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 21-ago-2015
 */

public class PortafolioDetailCustodiaTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	
	/** The economic activity. */
	private String economicActivity;
	
	/** The desc economic activity. */
	private String descEconomicActivity;
	
	/** The found type. */
	private String foundType;
	
	/** The desc found type. */
	private String descFoundType;
	
	/** The cui holder. */
	private String cuiHolder;
	
	/** The cui description. */
	private String cuiDescription;
	
	/** The process date. */
	private Date processDate;
	
	/** The report format. */
	private Integer reportFormat;		
	
	/** The document type. */
	private Integer documentType;
	
	/** The document number. */
	private String documentNumber;

	/** The transfer Number. */
	private String transferNumber;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The issuance type. */
	private Integer issuanceType;
	
	/** The option boolean. */
	private Integer optionBoolean;
	
	/** The issuer code. */
	private String issuerCode;
	
	private String issuerDescription;
	
	
	/**
	 * Gets the economic activity.
	 *
	 * @return the economicActivity
	 */
	public String getEconomicActivity() {
		return economicActivity;
	}

	/**
	 * Sets the economic activity.
	 *
	 * @param economicActivity the economicActivity to set
	 */
	public void setEconomicActivity(String economicActivity) {
		this.economicActivity = economicActivity;
	}

	/**
	 * Gets the found type.
	 *
	 * @return the foundType
	 */
	public String getFoundType() {
		return foundType;
	}

	/**
	 * Sets the found type.
	 *
	 * @param foundType the foundType to set
	 */
	public void setFoundType(String foundType) {
		this.foundType = foundType;
	}

	/**
	 * Gets the cui holder.
	 *
	 * @return the cuiHolder
	 */
	public String getCuiHolder() {
		return cuiHolder;
	}

	/**
	 * Sets the cui holder.
	 *
	 * @param cuiHolder the cuiHolder to set
	 */
	public void setCuiHolder(String cuiHolder) {
		this.cuiHolder = cuiHolder;
	}

	/**
	 * Gets the process date.
	 *
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * Sets the process date.
	 *
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * Gets the report format.
	 *
	 * @return the reportFormat
	 */
	public Integer getReportFormat() {
		return reportFormat;
	}

	/**
	 * Sets the report format.
	 *
	 * @param reportFormat the reportFormat to set
	 */
	public void setReportFormat(Integer reportFormat) {
		this.reportFormat = reportFormat;
	}

	/**
	 * Gets the cui description.
	 *
	 * @return the cuiDescription
	 */
	public String getCuiDescription() {
		return cuiDescription;
	}

	/**
	 * Sets the cui description.
	 *
	 * @param cuiDescription the cuiDescription to set
	 */
	public void setCuiDescription(String cuiDescription) {
		this.cuiDescription = cuiDescription;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the documentType
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the documentNumber
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the documentNumber to set
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the desc economic activity.
	 *
	 * @return the descEconomicActivity
	 */
	public String getDescEconomicActivity() {
		return descEconomicActivity;
	}

	/**
	 * Sets the desc economic activity.
	 *
	 * @param descEconomicActivity the descEconomicActivity to set
	 */
	public void setDescEconomicActivity(String descEconomicActivity) {
		this.descEconomicActivity = descEconomicActivity;
	}

	/**
	 * Gets the desc found type.
	 *
	 * @return the descFoundType
	 */
	public String getDescFoundType() {
		return descFoundType;
	}

	/**
	 * Sets the desc found type.
	 *
	 * @param descFoundType the descFoundType to set
	 */
	public void setDescFoundType(String descFoundType) {
		this.descFoundType = descFoundType;
	}

	/**
	 * Gets the transfer number.
	 *
	 * @return the transferNumber
	 */
	public String getTransferNumber() {
		return transferNumber;
	}

	/**
	 * Sets the transfer number.
	 *
	 * @param transferNumber the transferNumber to set
	 */
	public void setTransferNumber(String transferNumber) {
		this.transferNumber = transferNumber;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the issuance type.
	 *
	 * @return the issuanceType
	 */
	public Integer getIssuanceType() {
		return issuanceType;
	}

	/**
	 * Sets the issuance type.
	 *
	 * @param issuanceType the issuanceType to set
	 */
	public void setIssuanceType(Integer issuanceType) {
		this.issuanceType = issuanceType;
	}

	/**
	 * Gets the option boolean.
	 *
	 * @return the optionBoolean
	 */
	public Integer getOptionBoolean() {
		return optionBoolean;
	}

	/**
	 * Sets the option boolean.
	 *
	 * @param optionBoolean the optionBoolean to set
	 */
	public void setOptionBoolean(Integer optionBoolean) {
		this.optionBoolean = optionBoolean;
	}

	/**
	 * Gets the issuer code.
	 *
	 * @return the issuerCode
	 */
	public String getIssuerCode() {
		return issuerCode;
	}

	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the issuerCode to set
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	/**
	 * @return the issuerDescription
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}

	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the issuerDescription to set
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
}
