package com.pradera.custody.reports;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;

import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.report.ReportUser;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.FileData;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.ftp.UploadFtp;
import com.pradera.core.component.swift.to.FTPParameters;
import com.pradera.custody.reports.service.CustodyReportsService;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CustodyReportsSender.
 *
 * @author PraderaTechnologies
 */
@Stateless
public class CustodyReportsSender implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/** The custody reports service. */
	@EJB
	private CustodyReportsService custodyReportsService;
	
	/**
	 * Send reports.
	 *
	 * @param lstPortafolioDetailCustodiaTO the list portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @param objLoggerUser the obj logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	public List<Object[]> sendReports(List<PortafolioDetailCustodiaTO> lstPortafolioDetailCustodiaTO, Long idReportPk, 
			LoggerUser objLoggerUser) throws ServiceException, IOException, InterruptedException{
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		List<Object[]> lstIdReportLoggerPks = null;
		ReportUser objReportUser;
		if(reportService != null){
			lstIdReportLoggerPks = new ArrayList<Object[]>();
			for(PortafolioDetailCustodiaTO objPortafolioDetailCustodiaTO : lstPortafolioDetailCustodiaTO){
					Thread.sleep(1500);
					objReportUser = new ReportUser();
					objReportUser.setUserName(objLoggerUser.getUserName());
					objReportUser.setShowNotification(BooleanType.YES.getCode());
					objReportUser.setReportFormat(objPortafolioDetailCustodiaTO.getReportFormat());
					parameters = new HashMap<String, String>();	
					parameters = addParameters(objPortafolioDetailCustodiaTO, idReportPk);
					parameterDetails = addParametersDetail(objPortafolioDetailCustodiaTO, idReportPk);
					Long IdReportLoggerPks = reportService.get().saveReportInterfaces(idReportPk, parameters, parameterDetails, objReportUser, objLoggerUser);
					Object[] objReturn = {IdReportLoggerPks, objPortafolioDetailCustodiaTO.getTransferNumber(), 
							objPortafolioDetailCustodiaTO.getReportFormat(),objPortafolioDetailCustodiaTO.getCuiHolder(), 
							objPortafolioDetailCustodiaTO.getIssuerCode(), objPortafolioDetailCustodiaTO.getIssuanceType()
							,objPortafolioDetailCustodiaTO.getProcessDate()
					};
					lstIdReportLoggerPks.add(objReturn);
				}
		}
		return lstIdReportLoggerPks;
	}
	
	/**
	 * Gets the data report object.
	 *
	 * @param idReportLoggerPk the id report logger pk
	 * @return the data report object
	 * @throws ServiceException the service exception
	 */
	public Object [] getDataReportObject(Long idReportLoggerPk) throws ServiceException {
		return reportService.get().getDataReportObject(idReportLoggerPk);
	}
	
	/**
	 * Send FTP upload.
	 *
	 * @param listFileData the list file data
	 * @param parameterTablePk the parameter table pk
	 * @return true, if successful
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean sendFtpUpload(List<FileData> listFileData, Integer parameterTablePk) throws IOException {
		return reportService.get().sendFtpUpload(listFileData, parameterTablePk);
	}
	
	/**
	 * Send ftp upload.
	 *
	 * @param nameFile the name file
	 * @param dataReport the data report
	 * @param idParameterPk the id parameter pk
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void sendFtpUpload(String nameFile , byte [] dataReport,Integer idParameterPk) throws IOException{
		FTPParameters ftpParameters = new FTPParameters();
		ParameterTable parameter = custodyReportsService.find(ParameterTable.class, idParameterPk);		
		File file =new File(nameFile);
		FileUtils.writeByteArrayToFile(file, dataReport);
		ftpParameters.setServer(parameter.getText3());
		ftpParameters.setUser(parameter.getText1());
		ftpParameters.setPass(parameter.getText2());
		ftpParameters.setRemotePath(parameter.getDescription());
		ftpParameters.setTempInterfaceFile(file);	
		ftpParameters.setReportData(dataReport);
		UploadFtp upload= new UploadFtp(ftpParameters);
		upload.registerAndSendFile(nameFile);		
	}
	
	/**
	 * Adds the parameters.
	 *
	 * @param objPortafolioDetailCustodiaTO the object portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @return the map
	 */
	public Map<String,String> addParameters(PortafolioDetailCustodiaTO objPortafolioDetailCustodiaTO, Long  idReportPk) { 
		Map<String,String> parameters = new HashMap<String, String>();		
		if(ReportIdType.PORTAFOLIO_DETAIL_CUSTODY.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getEconomicActivity())){
				parameters.put("economic_activity", objPortafolioDetailCustodiaTO.getEconomicActivity());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getCuiHolder())){
				parameters.put("cui_holder", objPortafolioDetailCustodiaTO.getCuiHolder());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getCuiDescription())){
				parameters.put("cui_description", objPortafolioDetailCustodiaTO.getCuiDescription());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getFoundType())){
				parameters.put("found_type", objPortafolioDetailCustodiaTO.getFoundType());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getDocumentType())){
				parameters.put("document_type", objPortafolioDetailCustodiaTO.getDocumentType().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getDocumentNumber())){
				parameters.put("document_number", objPortafolioDetailCustodiaTO.getDocumentNumber());
			}		
			parameters.put("report_format", String.valueOf(objPortafolioDetailCustodiaTO.getReportFormat()));
		}
		
		if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_SECURITY.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
				
			}
		}else if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
		}else if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS_SUB.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("registry_date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
		}else if (ReportIdType.PORTAFOLIO_CUSTODY_AGB_SAFI_FIC.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
				parameters.put("report_format", ReportFormatType.TXT.getCode().toString());
			}
		} else if(ReportIdType.EXPIRATION_SECURITIES_BY_ISSUER.getCode().equals(idReportPk)) {
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getIssuerCode())) {
				parameters.put("issuer_code", objPortafolioDetailCustodiaTO.getIssuerCode().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getInitialDate())) {
				parameters.put("date_initial", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getInitialDate()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getEndDate())) {
				parameters.put("date_end", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getEndDate()));
			}
			parameters.put("bcb_format", BooleanType.YES.getCode().toString());
			parameters.put("ind_automatic", BooleanType.YES.getCode().toString());
			parameters.put("report_format", String.valueOf(objPortafolioDetailCustodiaTO.getReportFormat()));
		} else if(ReportIdType.SUMMARY_RECONCILIATION_REPORT_DAILY.getCode().equals(idReportPk)) {
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())) {
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
			parameters.put("report_format", String.valueOf(objPortafolioDetailCustodiaTO.getReportFormat()));
		} else if(ReportIdType.REPORT_DEMATERIALIZED_TOTAL_PORTFOLIO_CLIENTS.getCode().equals(idReportPk)) {
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())) {
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getOptionBoolean())) {
				parameters.put("available_balance", objPortafolioDetailCustodiaTO.getOptionBoolean().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getIssuanceType())) {
				parameters.put("portafolio_type", objPortafolioDetailCustodiaTO.getIssuanceType().toString());
			}
			parameters.put("report_format", String.valueOf(objPortafolioDetailCustodiaTO.getReportFormat()));
		} else if(ReportIdType.REPORT_DETAILS_PENDING_EXPIRATION_SECURITIES.getCode().equals(idReportPk)) {
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getInitialDate())) {
				parameters.put("date_initial", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getInitialDate()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getEndDate())) {
				parameters.put("date_end", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getEndDate()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getIssuerCode())) {
				parameters.put("issuer_code", objPortafolioDetailCustodiaTO.getIssuerCode().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getOptionBoolean())) {
				parameters.put("securities_situation", objPortafolioDetailCustodiaTO.getOptionBoolean().toString());
			}
			parameters.put("ind_automatic", BooleanType.YES.getCode().toString());
			parameters.put("report_format", String.valueOf(objPortafolioDetailCustodiaTO.getReportFormat()));
		}
		
		if(ReportIdType.RELATION_CURRENT_BLOCKING.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("generation_date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
				parameters.put("report_format", ReportFormatType.PDF.getCode().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getCuiHolder())){
				parameters.put("cui_holder", objPortafolioDetailCustodiaTO.getCuiHolder());
			}
		}
		
		parameters.put("reportType", idReportPk.toString());		
		return parameters;
	}
	
	
	/**
	 * Adds the parameters detail.
	 *
	 * @param objPortafolioDetailCustodiaTO the object portafolio detail custody to
	 * @param idReportPk the id report pk
	 * @return the map
	 */
	public Map<String,String> addParametersDetail(PortafolioDetailCustodiaTO objPortafolioDetailCustodiaTO, Long  idReportPk) { 
		Map<String,String> parameters = new HashMap<String, String>();		
		if(ReportIdType.PORTAFOLIO_DETAIL_CUSTODY.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getDescEconomicActivity())){
				parameters.put("economic_activity", objPortafolioDetailCustodiaTO.getDescEconomicActivity());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getCuiDescription())){
				parameters.put("cui_holder", objPortafolioDetailCustodiaTO.getCuiDescription());
				parameters.put("cui_description", objPortafolioDetailCustodiaTO.getCuiDescription());
			}			
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getDescFoundType())){
				parameters.put("found_type", objPortafolioDetailCustodiaTO.getDescFoundType());
			}			
		}		
		
		if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_SECURITY.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
				
			}
		}else if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
		}else if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS_SUB.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("registry_date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
			}
		}else if (ReportIdType.PORTAFOLIO_CUSTODY_AGB_SAFI_FIC.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
				parameters.put("report_format", ReportFormatType.TXT.getCode().toString());
			}
		} else if(ReportIdType.EXPIRATION_SECURITIES_BY_ISSUER.getCode().equals(idReportPk)) {
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getIssuerDescription())) {
				parameters.put("issuer_code", objPortafolioDetailCustodiaTO.getIssuerDescription().toString());
			}
			parameters.put("bcb_format", BooleanType.YES.getValue());
		} else if(ReportIdType.REPORT_DEMATERIALIZED_TOTAL_PORTFOLIO_CLIENTS.getCode().equals(idReportPk)) {			
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getOptionBoolean())) {				
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(objPortafolioDetailCustodiaTO.getOptionBoolean())){
					parameters.put("available_balance", BooleanType.lookup.get(Boolean.TRUE).getValue());					
				} else if(GeneralConstants.ZERO_VALUE_INTEGER.equals(objPortafolioDetailCustodiaTO.getOptionBoolean())){
					parameters.put("available_balance", BooleanType.lookup.get(Boolean.FALSE).getValue());					
				}								
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getIssuanceType())) {
				parameters.put("portafolio_type", IssuanceType.lookup.get(objPortafolioDetailCustodiaTO.getIssuanceType()).getValue());
			}			
		} else if(ReportIdType.REPORT_DETAILS_PENDING_EXPIRATION_SECURITIES.getCode().equals(idReportPk)) {			
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getIssuerDescription())) {
				parameters.put("issuer_code", objPortafolioDetailCustodiaTO.getIssuerDescription().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getOptionBoolean())) {
				parameters.put("securities_situation", IssuanceType.DEMATERIALIZED.getValue());
			}
		}
		
		if(ReportIdType.RELATION_CURRENT_BLOCKING.getCode().equals(idReportPk)){
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getProcessDate())){
				parameters.put("generation_date", CommonsUtilities.convertDatetoString(objPortafolioDetailCustodiaTO.getProcessDate()));
				parameters.put("report_format", ReportFormatType.PDF.getCode().toString());
			}
			if(Validations.validateIsNotNullAndNotEmpty(objPortafolioDetailCustodiaTO.getCuiHolder())){
				parameters.put("cui_holder", objPortafolioDetailCustodiaTO.getCuiHolder());
			}
		}
		
		return parameters;
	}
	
	/**
	 * Send sendReportCharacteristicSecurity.
	 *
	 * @param portafolioDetailCustodiaTO the portafolio detail custodia to
	 * @param listReport the list report
	 * @param objLoggerUser the obj logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	public List<Object[]> sendReportCharacteristicSecurity(PortafolioDetailCustodiaTO portafolioDetailCustodiaTO, List<Long> listReport, 
			LoggerUser objLoggerUser) throws ServiceException, IOException, InterruptedException{
		
		Map<String,String> parameters = new HashMap<String, String>();
		Map<String,String> parameterDetails = new HashMap<String, String>();
		List<Object[]> lstIdReportLoggerPks = null;
		ReportUser objReportUser;
		
		if(reportService != null){
			lstIdReportLoggerPks = new ArrayList<Object[]>();
			
			for (Long idReportPk : listReport){
				
				objReportUser = new ReportUser();
				
				objReportUser.setUserName(objLoggerUser.getUserName());
				objReportUser.setShowNotification(BooleanType.YES.getCode());
				objReportUser.setReportFormat(portafolioDetailCustodiaTO.getReportFormat());
				
				parameters = new HashMap<String, String>();	
				parameters = addParameters(portafolioDetailCustodiaTO, idReportPk);
				
				parameterDetails = addParametersDetail(portafolioDetailCustodiaTO, idReportPk);
				Long IdReportLoggerPks = reportService.get().saveReportInterfaces(idReportPk, parameters, parameterDetails, objReportUser, objLoggerUser);
				
				String tituloReport=null;
				if (ReportIdType.PORTAFOLIO_CHARACTERISTICS_SECURITY.getCode()==idReportPk){
					tituloReport="B";
				}else if(ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS.getCode()==idReportPk) {
					tituloReport="E";
				}else if(ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS_SUB.getCode()==idReportPk) {
					tituloReport="AF";
				}else{
					tituloReport="U";
				}
				Object[] objReturn = {IdReportLoggerPks, portafolioDetailCustodiaTO.getReportFormat(),tituloReport};
				
				lstIdReportLoggerPks.add(objReturn);
			}
			
			
		}
		
		
		return lstIdReportLoggerPks;
	}
			
}
