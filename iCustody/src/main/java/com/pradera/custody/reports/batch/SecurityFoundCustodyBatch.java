package com.pradera.custody.reports.batch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.report.ReportIdType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.helperui.to.HolderHelperOutputTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.reports.PortafolioDetailCustodiaTO;
import com.pradera.custody.reports.facade.CustodyReportsFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.type.EconomicActivityType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.report.type.ReportFormatType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PortafolioDetailCustodyBatch.
 *
 * @author Pradera Technologies
 */

@BatchProcess(name="SecurityFoundCustodyBatch")
@RequestScoped
public class SecurityFoundCustodyBatch implements JobExecution , Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The custody reports facade. */
	@EJB
	private CustodyReportsFacade custodyReportsFacade;
	
	private String tipoReporte=null;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		// TODO Auto-generated method stub
		logger.info("PortafolioDetailCustodyBatch: Starting the process ...");
		
		PortafolioDetailCustodiaTO portafolioDetailCustodiaTO;
		
		Date currentDate = CommonsUtilities.addDate(CommonsUtilities.currentDate(), -1);
		try {
			logger.info("SecurityFoundCustodyBatch: CARACTERISTICAS DE VALOR...");
			
			portafolioDetailCustodiaTO = new PortafolioDetailCustodiaTO();
			portafolioDetailCustodiaTO.setProcessDate(currentDate);
			portafolioDetailCustodiaTO.setReportFormat(ReportFormatType.TXT.getCode());
			
			//Caracteristicas de valor
			List<Long> listReport = new ArrayList<Long>();
			listReport.add(ReportIdType.PORTAFOLIO_CHARACTERISTICS_SECURITY.getCode());
			listReport.add(ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS.getCode());
			listReport.add(ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS_SUB.getCode());
			listReport.add(ReportIdType.PORTAFOLIO_CUSTODY_AGB_SAFI_FIC.getCode());
			
			custodyReportsFacade.sendReportCharacteristicSecurity(portafolioDetailCustodiaTO, listReport);

			//Caracteristicas de cupon desprendido
//			custodyReportsFacade.sendReportCharacteristicCouponsSub(portafolioDetailCustodiaTO, ReportIdType.PORTAFOLIO_CHARACTERISTICS_CUOPONS_SUB.getCode());
			
		} catch(Exception ex) {
			logger.error(":::::::::::::::::::::: ERROR PortafolioDetailCustodyBatch ::::::::::::::::::::::::");
			ex.printStackTrace();
		}		
		logger.info("PortafolioDetailCustodyBatch: Finishing the process ...");
	}
	
	
	
	/**
	 * Fill economic activity.
	 *
	 * @return the list
	 */
	public List<Integer> fillReportFormat(){
		List<Integer> lstReportFormat = new ArrayList<Integer>();
		lstReportFormat.add(ReportFormatType.PDF.getCode());
		lstReportFormat.add(ReportFormatType.TXT.getCode());		
		return lstReportFormat;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
