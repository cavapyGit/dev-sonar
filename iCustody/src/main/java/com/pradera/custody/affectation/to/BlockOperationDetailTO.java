package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockOperationDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class BlockOperationDetailTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The block request. */
	private BlockRequest blockRequest;
	
	/** The block operation to. */
	private BlockOperationTO blockOperationTO;
	
	/** The lst block movements. */
	private List<HolderAccountMovement> lstBlockMovements;
	
	/**
	 * Gets the block request.
	 *
	 * @return the block request
	 */
	public BlockRequest getBlockRequest() {
		return blockRequest;
	}
	
	/**
	 * Sets the block request.
	 *
	 * @param blockRequest the new block request
	 */
	public void setBlockRequest(BlockRequest blockRequest) {
		this.blockRequest = blockRequest;
	}
	
	/**
	 * Gets the block operation to.
	 *
	 * @return the block operation to
	 */
	public BlockOperationTO getBlockOperationTO() {
		return blockOperationTO;
	}
	
	/**
	 * Sets the block operation to.
	 *
	 * @param blockOperationTO the new block operation to
	 */
	public void setBlockOperationTO(BlockOperationTO blockOperationTO) {
		this.blockOperationTO = blockOperationTO;
	}
	
	/**
	 * Gets the lst block movements.
	 *
	 * @return the lst block movements
	 */
	public List<HolderAccountMovement> getLstBlockMovements() {
		return lstBlockMovements;
	}
	
	/**
	 * Sets the lst block movements.
	 *
	 * @param lstBlockMovements the new lst block movements
	 */
	public void setLstBlockMovements(List<HolderAccountMovement> lstBlockMovements) {
		this.lstBlockMovements = lstBlockMovements;
	}
}