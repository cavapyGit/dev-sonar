package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountBalanceTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class HolderAccountBalanceTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The isin name. */
	private String isinName;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The block balance. */
	private BigDecimal blockBalance;
	
	/** The quantity block balance. */
	private BigDecimal quantityBlockBalance;
	
	/** The valoritation quantity block balance. */
	private BigDecimal valoritationQuantityBlockBalance;
	
	/** The valoritation quantity block balance temp. */
	private BigDecimal valoritationQuantityBlockBalanceTemp;
	
	/** The reporting balance. */
	private BigDecimal reportingBalance;
	
	/** The selected benefits. */
	private List<Integer> selectedBenefits;
	
	/** The lst benefits. */
	private List<ParameterTable> lstBenefits;
	
	/** The bl check. */
	private boolean blCheck;
	
	/** The block. */
	private BigDecimal block;
	
	/** The reblock. */
	private BigDecimal reblock;		
	
	/** The block temp. */
	private BigDecimal blockTemp;
	
	/** The reblock temp. */
	private BigDecimal reblockTemp;
	
	/** The nominal value. */
	private BigDecimal nominalValue;
	
	/** The currency. */
	private Integer currency;
	
	/** The currency description. */
	private String currencyDescription;
	
	/** The valoritation val. */
	private BigDecimal valoritationVal;
	
	/** The last quotation. */
	private BigDecimal lastQuotation;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The holder name. */
	private String holderName;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The quantity remove balance. */
	private BigDecimal quantityRemoveBalance;
	
	/** The max size. */
	private Integer maxSize;
	
	/** The holder account state. */
	private Integer holderAccountState;
	
	/** The ban balance. */
	private BigDecimal oppositionBalance, banBalance;
	
	/** The market lock. */
	private Boolean marketLock;
	
	/** The block market fact detail. */
	private List<BlockMarketFactTO> blockMarketFactDetail;
	
	/** The retirement market fact detail. */
	private List<MarketFactTO> marketFactDetails;
	
	/** The support file. */
	private byte[] supportFile;
	
	/** The support file name. */
	private String supportFileName;
	
	/** The issuance sec type. */
	private Integer issuanceSecType;
	
	/** The credin rates scale. */
	private Integer credinRatesScale;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The Expiration date. */
	private Date ExpirationDate;
	
	/**
	 * Gets the selected benefits.
	 *
	 * @return the selected benefits
	 */
	public List<Integer> getSelectedBenefits() {
		return selectedBenefits;
	}
	
	/**
	 * Sets the selected benefits.
	 *
	 * @param lstSelecBenefits the new selected benefits
	 */
	public void setSelectedBenefits(List<Integer> lstSelecBenefits) {		
		this.selectedBenefits = lstSelecBenefits;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the issuance sec type.
	 *
	 * @return the issuance sec type
	 */
	public Integer getIssuanceSecType() {
		return issuanceSecType;
	}

	/**
	 * Sets the issuance sec type.
	 *
	 * @param issuanceSecType the new issuance sec type
	 */
	public void setIssuanceSecType(Integer issuanceSecType) {
		this.issuanceSecType = issuanceSecType;
	}

	/**
	 * Gets the credin rates scale.
	 *
	 * @return the credin rates scale
	 */
	public Integer getCredinRatesScale() {
		return credinRatesScale;
	}

	/**
	 * Sets the credin rates scale.
	 *
	 * @param credinRatesScale the new credin rates scale
	 */
	public void setCredinRatesScale(Integer credinRatesScale) {
		this.credinRatesScale = credinRatesScale;
	}

	/**
	 * Gets the lst benefits.
	 *
	 * @return the lst benefits
	 */
	public List<ParameterTable> getLstBenefits() {
		return lstBenefits;
	}
	
	/**
	 * Sets the lst benefits.
	 *
	 * @param lstBenefits the new lst benefits
	 */
	public void setLstBenefits(List<ParameterTable> lstBenefits) {
		this.lstBenefits = lstBenefits;
	}
	
	/**
	 * Checks if is bl check.
	 *
	 * @return true, if is bl check
	 */
	public boolean isBlCheck() {
		return blCheck;
	}
	
	/**
	 * Sets the bl check.
	 *
	 * @param blCheck the new bl check
	 */
	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}
	
	/**
	 * Gets the block.
	 *
	 * @return the block
	 */
	public BigDecimal getBlock() {
		return block;
	}
	
	/**
	 * Sets the block.
	 *
	 * @param block the new block
	 */
	public void setBlock(BigDecimal block) {
		this.block = block;
	}
	
	/**
	 * Gets the reblock.
	 *
	 * @return the reblock
	 */
	public BigDecimal getReblock() {
		return reblock;
	}
	
	/**
	 * Sets the reblock.
	 *
	 * @param reblock the new reblock
	 */
	public void setReblock(BigDecimal reblock) {
		this.reblock = reblock;
	}
	
	/**
	 * Gets the isin name.
	 *
	 * @return the isin name
	 */
	public String getIsinName() {
		return isinName;
	}
	
	/**
	 * Sets the isin name.
	 *
	 * @param isinName the new isin name
	 */
	public void setIsinName(String isinName) {
		this.isinName = isinName;
	}
	
	/**
	 * Gets the quantity block balance.
	 *
	 * @return the quantity block balance
	 */
	public BigDecimal getQuantityBlockBalance() {
		return quantityBlockBalance;
	}
	
	/**
	 * Sets the quantity block balance.
	 *
	 * @param quantityBlockBalance the new quantity block balance
	 */
	public void setQuantityBlockBalance(BigDecimal quantityBlockBalance) {
		this.quantityBlockBalance = quantityBlockBalance;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominal value
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the new nominal value
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the valoritation val.
	 *
	 * @return the valoritation val
	 */
	public BigDecimal getValoritationVal() {
		return valoritationVal;
	}

	/**
	 * Sets the valoritation val.
	 *
	 * @param valoritationVal the new valoritation val
	 */
	public void setValoritationVal(BigDecimal valoritationVal) {
		this.valoritationVal = valoritationVal;
	}
	
	/**
	 * Gets the block temp.
	 *
	 * @return the block temp
	 */
	public BigDecimal getBlockTemp() {
		return blockTemp;
	}
	
	/**
	 * Sets the block temp.
	 *
	 * @param blockTemp the new block temp
	 */
	public void setBlockTemp(BigDecimal blockTemp) {
		this.blockTemp = blockTemp;
	}
	
	/**
	 * Gets the reblock temp.
	 *
	 * @return the reblock temp
	 */
	public BigDecimal getReblockTemp() {
		return reblockTemp;
	}
	
	/**
	 * Sets the reblock temp.
	 *
	 * @param reblockTemp the new reblock temp
	 */
	public void setReblockTemp(BigDecimal reblockTemp) {
		this.reblockTemp = reblockTemp;
	}

	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the holder name.
	 *
	 * @return the holder name
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * Sets the holder name.
	 *
	 * @param holderName the new holder name
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}

	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}

	/**
	 * Gets the quantity remove balance.
	 *
	 * @return the quantity remove balance
	 */
	public BigDecimal getQuantityRemoveBalance() {
		return quantityRemoveBalance;
	}

	/**
	 * Sets the quantity remove balance.
	 *
	 * @param quantityRemoveBalance the new quantity remove balance
	 */
	public void setQuantityRemoveBalance(BigDecimal quantityRemoveBalance) {
		this.quantityRemoveBalance = quantityRemoveBalance;
	}

	/**
	 * Gets the max size.
	 *
	 * @return the max size
	 */
	public Integer getMaxSize() {
		return maxSize;
	}

	/**
	 * Sets the max size.
	 *
	 * @param maxSize the new max size
	 */
	public void setMaxSize(Integer maxSize) {
		this.maxSize = maxSize;
	}

	/**
	 * Gets the valoritation quantity block balance.
	 *
	 * @return the valoritation quantity block balance
	 */
	public BigDecimal getValoritationQuantityBlockBalance() {
		return valoritationQuantityBlockBalance;
	}

	/**
	 * Sets the valoritation quantity block balance.
	 *
	 * @param valoritationQuantityBlockBalance the new valoritation quantity block balance
	 */
	public void setValoritationQuantityBlockBalance(
			BigDecimal valoritationQuantityBlockBalance) {
		this.valoritationQuantityBlockBalance = valoritationQuantityBlockBalance;
	}

	/**
	 * Gets the valoritation quantity block balance temp.
	 *
	 * @return the valoritation quantity block balance temp
	 */
	public BigDecimal getValoritationQuantityBlockBalanceTemp() {
		return valoritationQuantityBlockBalanceTemp;
	}

	/**
	 * Sets the valoritation quantity block balance temp.
	 *
	 * @param valoritationQuantityBlockBalanceTemp the new valoritation quantity block balance temp
	 */
	public void setValoritationQuantityBlockBalanceTemp(
			BigDecimal valoritationQuantityBlockBalanceTemp) {
		this.valoritationQuantityBlockBalanceTemp = valoritationQuantityBlockBalanceTemp;
	}

	/**
	 * Gets the holder account state.
	 *
	 * @return the holder account state
	 */
	public Integer getHolderAccountState() {
		return holderAccountState;
	}

	/**
	 * Sets the holder account state.
	 *
	 * @param holderAccountState the new holder account state
	 */
	public void setHolderAccountState(Integer holderAccountState) {
		this.holderAccountState = holderAccountState;
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the block balance.
	 *
	 * @return the block balance
	 */
	public BigDecimal getBlockBalance() {
		return blockBalance;
	}

	/**
	 * Sets the block balance.
	 *
	 * @param blockBalance the new block balance
	 */
	public void setBlockBalance(BigDecimal blockBalance) {
		this.blockBalance = blockBalance;
	}

	/**
	 * Gets the reporting balance.
	 *
	 * @return the reporting balance
	 */
	public BigDecimal getReportingBalance() {
		return reportingBalance;
	}

	/**
	 * Sets the reporting balance.
	 *
	 * @param reportingBalance the new reporting balance
	 */
	public void setReportingBalance(BigDecimal reportingBalance) {
		this.reportingBalance = reportingBalance;
	}

	/**
	 * Gets the opposition balance.
	 *
	 * @return the opposition balance
	 */
	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}

	/**
	 * Sets the opposition balance.
	 *
	 * @param oppositionBalance the new opposition balance
	 */
	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

	/**
	 * Gets the ban balance.
	 *
	 * @return the ban balance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}

	/**
	 * Sets the ban balance.
	 *
	 * @param banBalance the new ban balance
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * Gets the last quotation.
	 *
	 * @return the last quotation
	 */
	public BigDecimal getLastQuotation() {
		return lastQuotation;
	}

	/**
	 * Sets the last quotation.
	 *
	 * @param lastQuotation the new last quotation
	 */
	public void setLastQuotation(BigDecimal lastQuotation) {
		this.lastQuotation = lastQuotation;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the block market fact detail.
	 *
	 * @return the block market fact detail
	 */
	public List<BlockMarketFactTO> getBlockMarketFactDetail() {
		return blockMarketFactDetail;
	}

	/**
	 * Sets the block market fact detail.
	 *
	 * @param blockMarketFactDetail the new block market fact detail
	 */
	public void setBlockMarketFactDetail(List<BlockMarketFactTO> blockMarketFactDetail) {
		this.blockMarketFactDetail = blockMarketFactDetail;
	}

	/**
	 * Gets the market lock.
	 *
	 * @return the market lock
	 */
	public Boolean getMarketLock() {
		return marketLock;
	}

	/**
	 * Sets the market lock.
	 *
	 * @param marketLock the new market lock
	 */
	public void setMarketLock(Boolean marketLock) {
		this.marketLock = marketLock;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idHolderAccountPk == null) ? 0 : idHolderAccountPk
						.hashCode());
		result = prime * result
				+ ((idHolderPk == null) ? 0 : idHolderPk.hashCode());
		result = prime
				* result
				+ ((idSecurityCodePk == null) ? 0 : idSecurityCodePk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HolderAccountBalanceTO other = (HolderAccountBalanceTO) obj;
		if (idHolderAccountPk == null) {
			if (other.idHolderAccountPk != null)
				return false;
		} else if (!idHolderAccountPk.equals(other.idHolderAccountPk))
			return false;
		if (idHolderPk == null) {
			if (other.idHolderPk != null)
				return false;
		} else if (!idHolderPk.equals(other.idHolderPk))
			return false;
		if (idSecurityCodePk == null) {
			if (other.idSecurityCodePk != null)
				return false;
		} else if (!idSecurityCodePk.equals(other.idSecurityCodePk))
			return false;
		return true;
	}

	/**
	 * Gets the support file.
	 *
	 * @return the support file
	 */
	public byte[] getSupportFile() {
		return supportFile;
	}

	/**
	 * Sets the support file.
	 *
	 * @param supportFile the new support file
	 */
	public void setSupportFile(byte[] supportFile) {
		this.supportFile = supportFile;
	}

	/**
	 * Gets the support file name.
	 *
	 * @return the support file name
	 */
	public String getSupportFileName() {
		return supportFileName;
	}

	/**
	 * Sets the support file name.
	 *
	 * @param supportFileName the new support file name
	 */
	public void setSupportFileName(String supportFileName) {
		this.supportFileName = supportFileName;
	}
	
	

	/**
	 * Gets the retirement market fact detail.
	 *
	 * @return the retirement market fact detail
	 */
	public List<MarketFactTO> getMarketFactDetails() {
		return marketFactDetails;
	}

	/**
	 * Sets the retirement market fact detail.
	 *
	 * @param retirementMarketFactDetail the new retirement market fact detail
	 */
	public void setMarketFactDetails(
			List<MarketFactTO> retirementMarketFactDetail) {
		this.marketFactDetails = retirementMarketFactDetail;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HolderAccountBalanceTO [idSecurityCodePk=" + idSecurityCodePk
				+ ", nominalValue="+ nominalValue + ", currency=" + currency
				+ ", valoritationVal=" + valoritationVal + ", lastQuotation="+ lastQuotation 
				+ ", \nissuanceSecType=" + issuanceSecType+ ", credinRatesScale=" + credinRatesScale
				+ ", instrumentType=" + instrumentType + ", securityClass=" +securityClass +"]";
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}

	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return ExpirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		ExpirationDate = expirationDate;
	}
	
}