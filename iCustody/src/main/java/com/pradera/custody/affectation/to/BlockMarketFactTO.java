package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class BlockMarketFactTO.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/
public class BlockMarketFactTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder market pk. */
	private Long idHolderMarketPk;
	
	/** The block amount. */
	private BigDecimal blockAmount;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market date. */
	private Date marketDate;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The available balance. */
	private BigDecimal availableBalance;

	/**
	 * Gets the id holder market pk.
	 *
	 * @return the id holder market pk
	 */
	public Long getIdHolderMarketPk() {
		return idHolderMarketPk;
	}

	/**
	 * Sets the id holder market pk.
	 *
	 * @param idHolderMarketPk the new id holder market pk
	 */
	public void setIdHolderMarketPk(Long idHolderMarketPk) {
		this.idHolderMarketPk = idHolderMarketPk;
	}

	/**
	 * Gets the block amount.
	 *
	 * @return the block amount
	 */
	public BigDecimal getBlockAmount() {
		return blockAmount;
	}

	/**
	 * Sets the block amount.
	 *
	 * @param blockAmount the new block amount
	 */
	public void setBlockAmount(BigDecimal blockAmount) {
		this.blockAmount = blockAmount;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}

	/**
	 * Instantiates a new block market fact to.
	 *
	 * @param idHolderMarketPk the id holder market pk
	 * @param marketPrice the market price
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param totalBalance the total balance
	 * @param availabeBalance the availabe balance
	 */
	public BlockMarketFactTO(Long idHolderMarketPk, BigDecimal marketPrice,
			BigDecimal marketRate, Date marketDate, BigDecimal totalBalance, BigDecimal availabeBalance) {
		super();
		this.idHolderMarketPk = idHolderMarketPk;
		this.marketPrice = marketPrice;
		this.marketRate = marketRate;
		this.marketDate = marketDate;
		this.totalBalance = totalBalance;
		this.availableBalance = availabeBalance;
	}

	/**
	 * Instantiates a new block market fact to.
	 */
	public BlockMarketFactTO() {
		super();
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
}