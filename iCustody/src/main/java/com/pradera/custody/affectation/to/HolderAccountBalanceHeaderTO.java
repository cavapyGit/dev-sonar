package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountBalanceHeaderTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class HolderAccountBalanceHeaderTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The holder. */
	private String holder;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The alternative code. */
	private String alternativeCode;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant. */
	private String participant;
	
	/** The block amount. */
	private BigDecimal blockAmount;
	
	/** The reblock amount. */
	private BigDecimal reblockAmount;
	
	/** The lst holder account balance to. */
	private List<HolderAccountBalanceTO> lstHolderAccountBalanceTO;
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the alternative code.
	 *
	 * @return the alternative code
	 */
	public String getAlternativeCode() {
		return alternativeCode;
	}
	
	/**
	 * Sets the alternative code.
	 *
	 * @param alternativeCode the new alternative code
	 */
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public String getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(String holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the lst holder account balance to.
	 *
	 * @return the lst holder account balance to
	 */
	public List<HolderAccountBalanceTO> getLstHolderAccountBalanceTO() {
		return lstHolderAccountBalanceTO;
	}
	
	/**
	 * Sets the lst holder account balance to.
	 *
	 * @param lstHolderAccountBalanceTO the new lst holder account balance to
	 */
	public void setLstHolderAccountBalanceTO(
			List<HolderAccountBalanceTO> lstHolderAccountBalanceTO) {
		this.lstHolderAccountBalanceTO = lstHolderAccountBalanceTO;
	}

	/**
	 * Gets the block amount.
	 *
	 * @return the block amount
	 */
	public BigDecimal getBlockAmount() {
		return blockAmount;
	}

	/**
	 * Sets the block amount.
	 *
	 * @param blockAmount the new block amount
	 */
	public void setBlockAmount(BigDecimal blockAmount) {
		this.blockAmount = blockAmount;
	}

	/**
	 * Gets the reblock amount.
	 *
	 * @return the reblock amount
	 */
	public BigDecimal getReblockAmount() {
		return reblockAmount;
	}

	/**
	 * Sets the reblock amount.
	 *
	 * @param reblockAmount the new reblock amount
	 */
	public void setReblockAmount(BigDecimal reblockAmount) {
		this.reblockAmount = reblockAmount;
	}

}