package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RegisterPawnOrReserveTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class RegisterPawnOrReserveTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The lst type request. */
	private List<ParameterTable> lstTypeRequest;
	
	/** The lst holder account. */
	private List<HolderAccountTO> lstHolderAccount;	
	
	/** The lst holder account balance header. */
	private List<HolderAccountBalanceHeaderTO> lstHolderAccountBalanceHeader;
	
	/** The parameter table type request. */
	private ParameterTable parameterTableTypeRequest;
	
	/** The number record. */
	private String numberRecord;
	
	/** The date record. */
	private Date dateRecord;
	
	/** The block entity. */
	private BlockEntity blockEntity;
	
	/** The bl no reserve. */
	private boolean blNoReserve;
	
	/** The bl reserve. */
	private boolean blReserve;
	
	/** The holder. */
	private Holder holder;
	
	/** The document file. */
	private byte[] documentFile;
	
	/** The file name. */
	private String fileName;
	
	/** The registry user. */
	private String registryUser;
	
	/** The id block request pk. */
	private Long idBlockRequestPk;
	
	/** The lst affectation form. */
	private List<ParameterTable> lstAffectationForm;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The parameter table affectation form. */
	private ParameterTable parameterTableAffectationForm;
	
	/** The parameter table currency. */
	private ParameterTable parameterTableCurrency;
	
	/** The bl cash valuation. */
	private boolean blCashValuation;
	
	/** The amount. */
	private BigDecimal amount;	
	
	/** The bd exchange rate. */
	private BigDecimal bdExchangeRate;
	
	/** The block valoritation amount. */
	private BigDecimal blockValoritationAmount = BigDecimal.ZERO;
	
	/** The bl opposition. */
	private boolean blOpposition;
	
	/** The bl no opposition. */
	private boolean blNoOpposition;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The ind registered depositary. */
	private Integer indRegisteredDepositary;
	
	/** The check auto settled. */
	private boolean checkAutoSettled ;

	/** The observations. */
	private String observations;
	/**
	 * Gets the lst holder account balance header.
	 *
	 * @return the lst holder account balance header
	 */
	public List<HolderAccountBalanceHeaderTO> getLstHolderAccountBalanceHeader() {
		return lstHolderAccountBalanceHeader;
	}

	/**
	 * Sets the lst holder account balance header.
	 *
	 * @param lstHolderAccountBalanceHeader the new lst holder account balance header
	 */
	public void setLstHolderAccountBalanceHeader(
			List<HolderAccountBalanceHeaderTO> lstHolderAccountBalanceHeader) {
		this.lstHolderAccountBalanceHeader = lstHolderAccountBalanceHeader;
	}

	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccountTO> getLstHolderAccount() {
		return lstHolderAccount;
	}

	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccountTO> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}

	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}

	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}

	/**
	 * Gets the date record.
	 *
	 * @return the date record
	 */
	public Date getDateRecord() {
		return dateRecord;
	}

	/**
	 * Sets the date record.
	 *
	 * @param dateRecord the new date record
	 */
	public void setDateRecord(Date dateRecord) {
		this.dateRecord = dateRecord;
	}

	

	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public BlockEntity getBlockEntity() {
		return blockEntity;
	}

	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}

	


	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the lst type request.
	 *
	 * @return the lst type request
	 */
	public List<ParameterTable> getLstTypeRequest() {
		return lstTypeRequest;
	}

	/**
	 * Sets the lst type request.
	 *
	 * @param lstTypeRequest the new lst type request
	 */
	public void setLstTypeRequest(List<ParameterTable> lstTypeRequest) {
		this.lstTypeRequest = lstTypeRequest;
	}

	/**
	 * Gets the parameter table type request.
	 *
	 * @return the parameter table type request
	 */
	public ParameterTable getParameterTableTypeRequest() {
		return parameterTableTypeRequest;
	}

	/**
	 * Sets the parameter table type request.
	 *
	 * @param parameterTableTypeRequest the new parameter table type request
	 */
	public void setParameterTableTypeRequest(
			ParameterTable parameterTableTypeRequest) {
		this.parameterTableTypeRequest = parameterTableTypeRequest;
	}

	/**
	 * Gets the document file.
	 *
	 * @return the document file
	 */
	public byte[] getDocumentFile() {
		return documentFile;
	}

	/**
	 * Sets the document file.
	 *
	 * @param documentFile the new document file
	 */
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Checks if is bl no reserve.
	 *
	 * @return true, if is bl no reserve
	 */
	public boolean isBlNoReserve() {
		return blNoReserve;
	}

	/**
	 * Sets the bl no reserve.
	 *
	 * @param blNoReserve the new bl no reserve
	 */
	public void setBlNoReserve(boolean blNoReserve) {
		this.blNoReserve = blNoReserve;
	}

	/**
	 * Checks if is bl reserve.
	 *
	 * @return true, if is bl reserve
	 */
	public boolean isBlReserve() {
		return blReserve;
	}

	/**
	 * Sets the bl reserve.
	 *
	 * @param blReserve the new bl reserve
	 */
	public void setBlReserve(boolean blReserve) {
		this.blReserve = blReserve;
	}

	/**
	 * Gets the id block request pk.
	 *
	 * @return the id block request pk
	 */
	public Long getIdBlockRequestPk() {
		return idBlockRequestPk;
	}

	/**
	 * Sets the id block request pk.
	 *
	 * @param idBlockRequestPk the new id block request pk
	 */
	public void setIdBlockRequestPk(Long idBlockRequestPk) {
		this.idBlockRequestPk = idBlockRequestPk;
	}

	/**
	 * Gets the lst affectation form.
	 *
	 * @return the lst affectation form
	 */
	public List<ParameterTable> getLstAffectationForm() {
		return lstAffectationForm;
	}

	/**
	 * Sets the lst affectation form.
	 *
	 * @param lstAffectationForm the new lst affectation form
	 */
	public void setLstAffectationForm(List<ParameterTable> lstAffectationForm) {
		this.lstAffectationForm = lstAffectationForm;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the parameter table affectation form.
	 *
	 * @return the parameter table affectation form
	 */
	public ParameterTable getParameterTableAffectationForm() {
		return parameterTableAffectationForm;
	}

	/**
	 * Sets the parameter table affectation form.
	 *
	 * @param parameterTableAffectationForm the new parameter table affectation form
	 */
	public void setParameterTableAffectationForm(
			ParameterTable parameterTableAffectationForm) {
		this.parameterTableAffectationForm = parameterTableAffectationForm;
	}

	/**
	 * Checks if is bl cash valuation.
	 *
	 * @return true, if is bl cash valuation
	 */
	public boolean isBlCashValuation() {
		return blCashValuation;
	}

	/**
	 * Sets the bl cash valuation.
	 *
	 * @param blCashValuation the new bl cash valuation
	 */
	public void setBlCashValuation(boolean blCashValuation) {
		this.blCashValuation = blCashValuation;
	}

	/**
	 * Gets the parameter table currency.
	 *
	 * @return the parameter table currency
	 */
	public ParameterTable getParameterTableCurrency() {
		return parameterTableCurrency;
	}

	/**
	 * Sets the parameter table currency.
	 *
	 * @param parameterTableCurrency the new parameter table currency
	 */
	public void setParameterTableCurrency(ParameterTable parameterTableCurrency) {
		this.parameterTableCurrency = parameterTableCurrency;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Gets the bd exchange rate.
	 *
	 * @return the bd exchange rate
	 */
	public BigDecimal getBdExchangeRate() {
		return bdExchangeRate;
	}

	/**
	 * Sets the bd exchange rate.
	 *
	 * @param bdExchangeRate the new bd exchange rate
	 */
	public void setBdExchangeRate(BigDecimal bdExchangeRate) {
		this.bdExchangeRate = bdExchangeRate;
	}

	/**
	 * Gets the block valoritation amount.
	 *
	 * @return the block valoritation amount
	 */
	public BigDecimal getBlockValoritationAmount() {
		return blockValoritationAmount;
	}

	/**
	 * Sets the block valoritation amount.
	 *
	 * @param blockValoritationAmount the new block valoritation amount
	 */
	public void setBlockValoritationAmount(BigDecimal blockValoritationAmount) {
		this.blockValoritationAmount = blockValoritationAmount;
	}

	/**
	 * Checks if is bl opposition.
	 *
	 * @return true, if is bl opposition
	 */
	public boolean isBlOpposition() {
		return blOpposition;
	}

	/**
	 * Sets the bl opposition.
	 *
	 * @param blOpposition the new bl opposition
	 */
	public void setBlOpposition(boolean blOpposition) {
		this.blOpposition = blOpposition;
	}

	/**
	 * Checks if is bl no opposition.
	 *
	 * @return true, if is bl no opposition
	 */
	public boolean isBlNoOpposition() {
		return blNoOpposition;
	}

	/**
	 * Sets the bl no opposition.
	 *
	 * @param blNoOpposition the new bl no opposition
	 */
	public void setBlNoOpposition(boolean blNoOpposition) {
		this.blNoOpposition = blNoOpposition;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Gets the ind registered depositary.
	 *
	 * @return the ind registered depositary
	 */
	public Integer getIndRegisteredDepositary() {
		return indRegisteredDepositary;
	}

	/**
	 * Sets the ind registered depositary.
	 *
	 * @param indRegisteredDepositary the new ind registered depositary
	 */
	public void setIndRegisteredDepositary(Integer indRegisteredDepositary) {
		this.indRegisteredDepositary = indRegisteredDepositary;
	}

	/**
	 * Checks if is check auto settled.
	 *
	 * @return true, if is check auto settled
	 */
	public boolean isCheckAutoSettled() {
		return checkAutoSettled;
	}

	/**
	 * Sets the check auto settled.
	 *
	 * @param checkAutoSettled the new check auto settled
	 */
	public void setCheckAutoSettled(boolean checkAutoSettled) {
		this.checkAutoSettled = checkAutoSettled;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the observations to set
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}
	
	
	
}
