package com.pradera.custody.affectation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.operation.to.MechanismOperationTO;
import com.pradera.custody.accreditationcertificates.facade.AccreditationCertificatesServiceFacade;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.affectation.to.BlockMarketFactTO;
import com.pradera.custody.affectation.to.HolderAccountBalanceHeaderTO;
import com.pradera.custody.affectation.to.HolderAccountBalanceTO;
import com.pradera.custody.affectation.to.RegisterBanOrOthersTO;
import com.pradera.custody.affectation.to.RegisterPawnOrReserveTO;
import com.pradera.custody.affectation.to.RequestAffectationTO;
import com.pradera.custody.affectation.to.ResultSearchTO;
import com.pradera.custody.affectation.to.SearchBlockOperationTO;
import com.pradera.custody.affectation.to.SearchRequestAffectationTO;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.accreditationcertificates.BlockRequestFile;
import com.pradera.model.custody.affectation.type.AffectationDocumentType;
import com.pradera.model.custody.affectation.type.AffectationFormType;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.affectation.type.RequestAffectationStateType;
import com.pradera.model.custody.blockenforce.type.BlockEnforceStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.custody.type.CustodyOperationStateType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.operations.RequestCorrelativeType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RequestAffectationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
@Stateless
@Performance
public class RequestAffectationServiceBean extends CrudDaoServiceBean {


	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/** The custody service consumer. */
	@EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	/** The accreditation facade bean. */	
	@Inject
	private AccreditationCertificatesServiceFacade accreditationfacadeBean;

	
	/**
	 * Gets the list request affectation.
	 *
	 * @param searchRequestAffectationTO the search request affectation to
	 * @return the list request affectation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListRequestAffectation(SearchRequestAffectationTO searchRequestAffectationTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("	SELECT DISTINCT BR.idBlockRequestPk, " + 	//0
						"	BR.blockNumber, "	+				//1
						"	BR.holder.idHolderPk, " +			//2
						"	BR.holder.name," + 					//3
						"	BR.registryDate, " +				//4
						"	(SELECT parameterName FROM ParameterTable " +
						"		where parameterTablePk = BR.blockType) as blockType, " +	//5
						"	BE.idBlockEntityPk, " +				//6
						"	BE.name, " +						//7		
						"	(SELECT parameterName FROM ParameterTable where parameterTablePk = BR.blockRequestState) as state," +//8
						"	BR.blockRequestState," +			//9
						"	BR.blockNumberDate," +				//10
						"	BE.fullName," +						//11
						"	BE.firstLastName," +				//12
						"	BE.secondLastName," +				//13
						"	BR.blockType," +					//14
						"	BR.indRequestDeclaration," +		//15
						"	BR.notificationDate,"	+			//16
						"	BR.holder.fullName," + 				//17
						"	BR.registryUser" + 				    //18 
						"	FROM BlockRequest BR, " +						
						"		 BlockEntity BE ");			
						if (Validations.validateIsNotNull(searchRequestAffectationTO.getIdIssuer())) {
							sbQuery.append(" ,BlockOperation BO ");
						}
		sbQuery.append("	WHERE BE.idBlockEntityPk = BR.blockEntity.idBlockEntityPk " +												
						"  	AND BE.stateBlockEntity = " + BlockEntityStateType.ACTIVATED.getCode() +
						"	AND BR.holder.stateHolder IN (" + HolderStateType.REGISTERED.getCode() +  
						"," + HolderStateType.BLOCKED.getCode() + ")");
						
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getParameterTableState().getParameterTablePk()))
			sbQuery.append("	AND BR.blockRequestState = :state");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getParameterTableTypeAffectation().getParameterTablePk()))
			sbQuery.append("	AND BR.blockType = :blockType");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getHolder().getIdHolderPk()))
			sbQuery.append("	AND BR.holder.idHolderPk = :idHolderPk");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getBlockEntity().getIdBlockEntityPk()))
			sbQuery.append("	AND BR.blockEntity.idBlockEntityPk = :idBlockEntityPk");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getNumberRecord()))
			sbQuery.append("	AND BR.blockNumber = :numberRecord");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getIdBlockRequestPk()))
			sbQuery.append("	AND BR.idBlockRequestPk = :idBlockRequestPk");
		
		if(Validations.validateIsNullOrEmpty(searchRequestAffectationTO.getIdBlockRequestPk())){
			if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getInitialDate()))
				sbQuery.append("	AND TRUNC(BR.registryDate) >= :initialDate");			
			if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getEndDate()))
				sbQuery.append("	AND TRUNC(BR.registryDate) <= :endDate");
		}
		
		if (Validations.validateIsNotNull(searchRequestAffectationTO.getIdParticipant())) {
			sbQuery.append("	AND BR.participant.idParticipantPk = :idParticipant ");
		}
		
		if (Validations.validateIsNotNull(searchRequestAffectationTO.getIdIssuer())) {
			sbQuery.append("	and BO.blockRequest.idBlockRequestPk = BR.idBlockRequestPk ");
			sbQuery.append("	and BO.securities.issuer.idIssuerPk = :idIssuer ");
			sbQuery.append("	and BO.securities.securityClass in (:lstSecurityClass) ");
		}
		sbQuery.append("	ORDER BY BR.idBlockRequestPk DESC");
		/*****************************************************************************************************************/				
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getParameterTableState().getParameterTablePk()))
			mapParam.put("state", new Integer(searchRequestAffectationTO.getParameterTableState().getParameterTablePk()));
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getParameterTableTypeAffectation().getParameterTablePk()))
			mapParam.put("blockType", new Integer(searchRequestAffectationTO.getParameterTableTypeAffectation().getParameterTablePk()));
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getHolder().getIdHolderPk()))
			mapParam.put("idHolderPk", searchRequestAffectationTO.getHolder().getIdHolderPk());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getBlockEntity().getIdBlockEntityPk()))
			mapParam.put("idBlockEntityPk", searchRequestAffectationTO.getBlockEntity().getIdBlockEntityPk());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getNumberRecord()))
			mapParam.put("numberRecord", searchRequestAffectationTO.getNumberRecord());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getIdBlockRequestPk()))
			mapParam.put("idBlockRequestPk", searchRequestAffectationTO.getIdBlockRequestPk());
		
		if(Validations.validateIsNullOrEmpty(searchRequestAffectationTO.getIdBlockRequestPk())){
			if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getInitialDate()))
				mapParam.put("initialDate", searchRequestAffectationTO.getInitialDate());			
			if(Validations.validateIsNotNullAndNotEmpty(searchRequestAffectationTO.getEndDate()))
				mapParam.put("endDate", searchRequestAffectationTO.getEndDate());	
		}
		
		if (Validations.validateIsNotNull(searchRequestAffectationTO.getIdParticipant())) {
			mapParam.put("idParticipant", searchRequestAffectationTO.getIdParticipant());
		}
		if (Validations.validateIsNotNull(searchRequestAffectationTO.getIdIssuer())) {
			mapParam.put("idIssuer", searchRequestAffectationTO.getIdIssuer());
			List<Integer> lstSecurityClass= new ArrayList<Integer>();
			lstSecurityClass.add(SecurityClassType.DPA.getCode());
			lstSecurityClass.add(SecurityClassType.DPF.getCode());
			mapParam.put("lstSecurityClass", lstSecurityClass);
		}
	
		Query query = em.createQuery(sbQuery.toString());		
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
		while(it.hasNext()){        	
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}
	
	
	/**
	 * Find affectations holder account balance by hol acc pk and part pk.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idParticipantPk the id participant pk
	 * @param idIssuer the id issuer
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findBalancesByAccountAndParticipant(Long idHolderAccountPk,Long idParticipantPk, String idIssuer) throws ServiceException{
		String strQuery = "		SELECT SE.idSecurityCodePk," +				//0
						  "			   NVL(SE.description,'N/A')," +		//1
						  "			   NVL(HAB.availableBalance,0)," +		//2
						  "			   NVL(HAB.banBalance,0)," +			//3
						  "			   NVL(HAB.totalBalance,0)," +			//4
						  "			   NVL(HAB.pawnBalance,0)," +			//5
						  "			   NVL(HAB.otherBlockBalance,0)," +		//6
						  "			   SE.instrumentType," +				//7
						  "			   NVL(HAB.reportingBalance,0)," +		//8
						  "			   NVL(HAB.reserveBalance,0)," +		//9	
						  "			   NVL(HAB.oppositionBalance,0)," +		//10
						  "			   (SELECT description FROM ParameterTable where parameterTablePk = SE.currency) as currency," +		//11
						  "			   SE.currentNominalValue," +			//12
						  "			   SE.currency," +						//13
						  "			   NVL(HAB.transitBalance,0)," +		//14
						  "			   SE.issuance.creditRatingScales, " +	//15
						  "			   SE.issuance.securityType," +			//16
						  "            SE.securityClass," +					//17
						  "			   SE.expirationDate" +					//18
						  "		FROM HolderAccountBalance HAB, Security SE" +
						  "		WHERE HAB.id.idHolderAccountPk = :idHolderAccountPk" +
						  "		AND HAB.id.idParticipantPk = :idParticipantPk" +
						  "		AND HAB.id.idSecurityCodePk = SE.idSecurityCodePk" +
				//		  "		AND SE.expirationDate >= :dateSystem "+
						  "		AND HAB.totalBalance > 0 "	+
						  "		AND SE.stateSecurity = :securityState";
		if (Validations.validateIsNotNull(idIssuer)) {
			strQuery += "	and SE.issuer.idIssuerPk = :idIssuer ";
			strQuery += "	and SE.securityClass in (:lstSecurityClass) ";
		}
		strQuery += "		ORDER BY SE.idSecurityCodePk ASC, HAB.availableBalance DESC ";
		Query query = em.createQuery(strQuery);
		//query.setParameter("dateSystem",CommonsUtilities.currentDate());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("securityState", SecurityStateType.REGISTERED.getCode());
		if (Validations.validateIsNotNull(idIssuer)) {
			query.setParameter("idIssuer", idIssuer);
			List<Integer> lstSecurityClass= new ArrayList<Integer>();
			lstSecurityClass.add(SecurityClassType.DPF.getCode());
			lstSecurityClass.add(SecurityClassType.DPA.getCode());
			query.setParameter("lstSecurityClass", lstSecurityClass);
		}
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Find affectations holder account balance by hol acc pk and part pk.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idParticipantPk the id participant pk
	 * @param idIssuer the id issuer
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findBalancesByAccountAndParticipantWithGuardaExclusive(Long idHolderAccountPk,Long idParticipantPk, String idIssuer) throws ServiceException{
		String strQuery = "		SELECT SE.idSecurityCodePk," +				//0
						  "			   NVL(SE.description,'N/A')," +		//1
						  "			   NVL(HAB.availableBalance,0)," +		//2
						  "			   NVL(HAB.banBalance,0)," +			//3
						  "			   NVL(HAB.totalBalance,0)," +			//4
						  "			   NVL(HAB.pawnBalance,0)," +			//5
						  "			   NVL(HAB.otherBlockBalance,0)," +		//6
						  "			   SE.instrumentType," +				//7
						  "			   NVL(HAB.reportingBalance,0)," +		//8
						  "			   NVL(HAB.reserveBalance,0)," +		//9	
						  "			   NVL(HAB.oppositionBalance,0)," +		//10
						  "			   (SELECT description FROM ParameterTable where parameterTablePk = SE.currency) as currency," +		//11
						  "			   SE.currentNominalValue," +			//12
						  "			   SE.currency," +						//13
						  "			   NVL(HAB.transitBalance,0)," +		//14
						  "			   SE.issuance.creditRatingScales, " +	//15
						  "			   SE.issuance.securityType," +			//16
						  "            SE.securityClass," +					//17
						  "			   SE.expirationDate" +					//18
						  "		FROM HolderAccountBalance HAB, Security SE" +
						  "		WHERE HAB.id.idHolderAccountPk = :idHolderAccountPk" +
						  "		AND HAB.id.idParticipantPk = :idParticipantPk" +
						  "		AND HAB.id.idSecurityCodePk = SE.idSecurityCodePk" +
				//		  "		AND SE.expirationDate >= :dateSystem "+
						  "		AND HAB.totalBalance > 0 "	+
						  "		AND SE.stateSecurity IN ( " + 
						  SecurityStateType.REGISTERED.getCode() + "," +
						  SecurityStateType.GUARDA_EXCLUSIVE.getCode() + ") " ;

		if (Validations.validateIsNotNull(idIssuer)) {
			strQuery += "	and SE.issuer.idIssuerPk = :idIssuer ";
			strQuery += "	and SE.securityClass in (:lstSecurityClass) ";
		}
		strQuery += "		ORDER BY SE.idSecurityCodePk ASC, HAB.availableBalance DESC ";
		Query query = em.createQuery(strQuery);
		//query.setParameter("dateSystem",CommonsUtilities.currentDate());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		//query.setParameter("securityState", SecurityStateType.REGISTERED.getCode());
		if (Validations.validateIsNotNull(idIssuer)) {
			query.setParameter("idIssuer", idIssuer);
			List<Integer> lstSecurityClass= new ArrayList<Integer>();
			lstSecurityClass.add(SecurityClassType.DPF.getCode());
			lstSecurityClass.add(SecurityClassType.DPA.getCode());
			query.setParameter("lstSecurityClass", lstSecurityClass);
		}
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}	

	/**
	 * Gets the last quotation.
	 *
	 * @param idIsinCodePk the id isin code pk
	 * @param negotiationMechanis the negotiation mechanis
	 * @return the last quotation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLastQuotation(String idIsinCodePk, Long negotiationMechanis) throws ServiceException {
		String strQuery = "			SELECT averagePrice," +					//0
							"		negotiationMechanism.description" +		//1
							"		FROM Quotation" +
							"		WHERE security.idSecurityCodePk = :idSecurityCodePk" +
							"		AND negotiationMechanism.idNegotiationMechanismPk = :negotation" +
							"		AND ROWNUM < 2" +
							"		ORDER BY quotationDate DESC";		
		Query query = em.createQuery(strQuery);
		query.setParameter("idSecurityCodePk", idIsinCodePk);	
		query.setParameter("negotation", negotiationMechanis);
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Collect reblocks.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Long> collectReblocks(com.pradera.integration.component.business.to.HolderAccountBalanceTO objHolderAccountBalanceTO) throws ServiceException{
		List<Long> lstResult = haveReblocks(objHolderAccountBalanceTO);
		List<Long> lstReblockOperations = new ArrayList<Long>();
		for(Long idBlockOperation:lstResult){
			if(!haveUnblockOperationRegister(idBlockOperation)){
				lstReblockOperations.add(idBlockOperation);
			}
		}
		return lstReblockOperations;
	}
	
	/**
	 * Have unblock operation register.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return true, if successful
	 */
	public boolean haveUnblockOperationRegister(Long idBlockOperationPk){
		try{
			String strQuery = 	"	SELECT UO.blockOperation.idBlockOperationPk" +
								"	FROM UnblockOperation UO" +
								"	WHERE UO.blockOperation.idBlockOperationPk = :idBlockOperationPk" +
								"	AND UO.custodyOperation.state = :state";
			Query query = em.createQuery(strQuery);
			query.setParameter("idBlockOperationPk", idBlockOperationPk);
			query.setParameter("state", RequestReversalsStateType.REGISTERED.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return true;
		}catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Have reblocks.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> haveReblocks(com.pradera.integration.component.business.to.HolderAccountBalanceTO objHolderAccountBalanceTO) throws ServiceException{
		String strQuery = "			SELECT BO.idBlockOperationPk" +
				"		FROM BlockOperation BO" +
				"		WHERE BO.holderAccount.idHolderAccountPk = :idHolderAccountPk" +
				"		AND BO.participant.idParticipantPk = :idParticipantPk" +
				"		AND BO.securities.idSecurityCodePk = :idSecurityCodePk" +
				"		AND BO.blockLevel = :level" +
				"		AND BO.actualBlockBalance > 0" +
				"		AND BO.blockState = :state";
		Query query = em.createQuery(strQuery);
		query.setParameter("idHolderAccountPk", objHolderAccountBalanceTO.getIdHolderAccount());
		query.setParameter("idParticipantPk", objHolderAccountBalanceTO.getIdParticipant());
		query.setParameter("idSecurityCodePk", objHolderAccountBalanceTO.getIdSecurityCode());
		query.setParameter("state", AffectationStateType.BLOCKED.getCode());
		query.setParameter("level", LevelAffectationType.REBLOCK.getCode());
		List<Long> lstResult = query.getResultList();
		return lstResult;
	}


	/**
	 * Find block operations.
	 *
	 * @param searchBlockOperationTO the search block operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findBlockOperations(SearchBlockOperationTO searchBlockOperationTO) throws ServiceException {
		String strQuery = "			SELECT idBlockOperationPk," +					//0
							"				custodyOperation.registryDate," +		//1
							"				blockRequest.holder.idHolderPk," +		//2
							"				blockRequest.holder.name," +			//3
							"				securities.idSecurityCodePk," +				//4
							"				participant.idParticipantPk," +			//5
							"		(SELECT parameterName FROM ParameterTable where parameterTablePk = BO.blockRequest.blockType)," +		//6
							"		(SELECT parameterName FROM ParameterTable where parameterTablePk = BO.blockLevel)," +					//7
							"		(SELECT parameterName FROM ParameterTable where parameterTablePk = BO.blockState)," +					//8
							"				actualBlockBalance," +					//9
							"				holderAccount.accountNumber," +			//10
							"				securities.description," +				//11
							"				participant.description," +				//12
							"				participant.mnemonic," +				//13
							"				blockRequest.holder.firstLastName," +	//14
							"				blockRequest.holder.secondLastName," +	//15
							"				blockRequest.holder.fullName," +		//16
							" 				custodyOperation.operationNumber "+		//17
							"		FROM BlockOperation BO" +
							"		WHERE trunc(custodyOperation.registryDate) >= :initialDate" +							
							"		AND trunc(custodyOperation.registryDate) <= :endDate";			
		
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getHolder().getIdHolderPk()))
			strQuery += "	AND blockRequest.holder.idHolderPk = :idHolderPk";
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getHolderAccount().getIdHolderAccountPk()))
			strQuery += "	AND holderAccount.idHolderAccountPk = :idHolderAccountPk";
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getSecurity().getIdSecurityCodePk()))
			strQuery += "	AND securities.idSecurityCodePk = :idSecurityCodePk";
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableLevelAffectation().getParameterTablePk()))
			strQuery += "	AND BO.blockLevel = :blockLevel";
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableStateAffectation().getParameterTablePk()))
			strQuery += "	AND BO.blockState = :blockState";
		else
			strQuery += "	AND BO.blockState in (:lstBlockOperationState)";
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableTypeAffectation().getParameterTablePk()))
			strQuery += "	AND BO.blockRequest.blockType = :blockType";
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParticipant().getIdParticipantPk()))
			strQuery += "	AND BO.participant.idParticipantPk = :idParticipantPk";	
		if (Validations.validateIsNotNull(searchBlockOperationTO.getIssuer().getIdIssuerPk())) {
			strQuery += "	AND BO.securities.issuer.idIssuerPk = :idIssuer";
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableSecurityClass().getParameterTablePk())) {
			strQuery += "	AND BO.securities.securityClass = :securityClass";
		}else if (Validations.validateIsNotNull(searchBlockOperationTO.getIssuer().getIdIssuerPk()) && 
				Validations.validateIsNullOrEmpty(searchBlockOperationTO.getParamTableSecurityClass().getParameterTablePk())) {
			strQuery += "	AND BO.securities.securityClass in (:lstSecurityClass)";
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableCurrency().getParameterTablePk())) {
			strQuery += "	AND BO.securities.currency = :currency";
		}
			
		
		strQuery += "	ORDER BY idBlockOperationPk DESC";
		
		Query query = em.createQuery(strQuery);
		query.setParameter("initialDate", searchBlockOperationTO.getInitialDate());
		query.setParameter("endDate", searchBlockOperationTO.getEndDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getHolder().getIdHolderPk()))
			query.setParameter("idHolderPk", searchBlockOperationTO.getHolder().getIdHolderPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getHolderAccount().getIdHolderAccountPk()))
			query.setParameter("idHolderAccountPk", searchBlockOperationTO.getHolderAccount().getIdHolderAccountPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", searchBlockOperationTO.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableLevelAffectation().getParameterTablePk()))
			query.setParameter("blockLevel", searchBlockOperationTO.getParamTableLevelAffectation().getParameterTablePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableStateAffectation().getParameterTablePk()))
			query.setParameter("blockState", searchBlockOperationTO.getParamTableStateAffectation().getParameterTablePk());
		else{
			List<Integer> lstBlockOperationState = new ArrayList<Integer>();
			lstBlockOperationState.add(AffectationStateType.BLOCKED.getCode());
			lstBlockOperationState.add(AffectationStateType.UNBLOCKED.getCode());
			query.setParameter("lstBlockOperationState", lstBlockOperationState);
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableTypeAffectation().getParameterTablePk()))
			query.setParameter("blockType", searchBlockOperationTO.getParamTableTypeAffectation().getParameterTablePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParticipant().getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchBlockOperationTO.getParticipant().getIdParticipantPk());	
		if (Validations.validateIsNotNull(searchBlockOperationTO.getIssuer().getIdIssuerPk())) {
			query.setParameter("idIssuer", searchBlockOperationTO.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableSecurityClass().getParameterTablePk())) {
			query.setParameter("securityClass", searchBlockOperationTO.getParamTableSecurityClass().getParameterTablePk());
		}else if (Validations.validateIsNotNull(searchBlockOperationTO.getIssuer().getIdIssuerPk()) && 
				Validations.validateIsNullOrEmpty(searchBlockOperationTO.getParamTableSecurityClass().getParameterTablePk())) {
				List<Integer> lstSecurityClass= new ArrayList<Integer>();
				lstSecurityClass.add(SecurityClassType.DPF.getCode());
				lstSecurityClass.add(SecurityClassType.DPA.getCode());
				query.setParameter("lstSecurityClass", lstSecurityClass);
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchBlockOperationTO.getParamTableCurrency().getParameterTablePk())) {
			query.setParameter("currency", searchBlockOperationTO.getParamTableCurrency().getParameterTablePk());
		}
		
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Gets the block request.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return the block request
	 */
	@SuppressWarnings("unchecked")
	public BlockRequest getBlockRequest(RequestAffectationTO requestAffectationTO){
		
		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append(" select br ");
		stringBuilderSql.append(" from BlockRequest br ");
		stringBuilderSql.append(" inner join fetch br.blockOperation bos ");
		stringBuilderSql.append(" inner join fetch bos.securities se ");
		stringBuilderSql.append(" inner join fetch bos.participant pa ");
		stringBuilderSql.append(" inner join fetch bos.holderAccount ha ");
		stringBuilderSql.append(" Where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(requestAffectationTO.getNumberRequest())){
			stringBuilderSql.append(" and br.idBlockRequestPk = :idBlockRequestPk ");
		}
  
		TypedQuery<BlockRequest> query = (TypedQuery<BlockRequest>) em.createQuery(stringBuilderSql.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(requestAffectationTO.getNumberRequest())){
			query.setParameter("idBlockRequestPk", new Long(requestAffectationTO.getNumberRequest()));
		}
		
		return query.getSingleResult();
	}

	/**
	 * Fill benefits.
	 *
	 * @param lstBenefits the lst benefits
	 * @param objBlockOpeDeta the obj block ope deta
	 */
	private void fillBenefits(List<Integer> lstBenefits, BlockOperation objBlockOpeDeta){
		objBlockOpeDeta.setIndInterest(BooleanType.NO.getCode());
		objBlockOpeDeta.setIndAmortization(BooleanType.NO.getCode());
		objBlockOpeDeta.setIndRescue(BooleanType.NO.getCode());
		objBlockOpeDeta.setIndCashDividend(BooleanType.NO.getCode());
		objBlockOpeDeta.setIndStockDividend(BooleanType.NO.getCode());		
		objBlockOpeDeta.setIndSuscription(BooleanType.NO.getCode());
		objBlockOpeDeta.setIndReturnContributions(BooleanType.NO.getCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstBenefits)){
			
			for (int i = 0 ; i < lstBenefits.size(); i++) {
				//INDICADORES RF
				if(ImportanceEventType.INTEREST_PAYMENT.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndInterest(BooleanType.YES.getCode());
				else if(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndAmortization(BooleanType.YES.getCode());
				else if(ImportanceEventType.EARLY_PAYMENT.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndRescue(BooleanType.YES.getCode());
				
				//INDICAODRES RV
				else if(ImportanceEventType.CASH_DIVIDENDS.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndCashDividend(BooleanType.YES.getCode());
				else if(ImportanceEventType.ACTION_DIVIDENDS.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndStockDividend(BooleanType.YES.getCode());				
				else if(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndSuscription(BooleanType.YES.getCode());
				else if(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode().equals(lstBenefits.get(i)))
					objBlockOpeDeta.setIndReturnContributions(BooleanType.YES.getCode());		
			}
		}		
	}
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param objBlockOperation the obj block operation
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> getHolderAccountBalanceTOs(BlockOperation objBlockOperation) throws ServiceException{
		List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<com.pradera.integration.component.business.to.HolderAccountBalanceTO>();
		com.pradera.integration.component.business.to.HolderAccountBalanceTO holderAccountBalanceTO = new com.pradera.integration.component.business.to.HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(objBlockOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(objBlockOperation.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getSecurities().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(objBlockOperation.getOriginalBlockBalance());
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs, Long operationType){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		if (BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode().equals(businessProcessType) ||
				 BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode().equals(businessProcessType) ||
				 BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_AUTOMATIC_EXECUTION.getCode().equals(businessProcessType) ||
				 BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode().equals(businessProcessType)){
			objAccountComponent.setLstTargetAccounts(accountBalanceTOs);
		}else{
			objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		}
		objAccountComponent.setIdOperationType(operationType);
		return objAccountComponent;
	}
	
	/**
	 * Validate entities states.
	 *
	 * @param blockRequest the block request
	 * @return the error service type
	 */
	private ErrorServiceType validateEntitiesStates(BlockRequest blockRequest){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idHolderPkParam", blockRequest.getHolder().getIdHolderPk());
		Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, params);
		if(!HolderStateType.REGISTERED.getCode().equals(holder.getStateHolder()))
			return ErrorServiceType.HOLDER_BLOCK;
		BlockEntity blockEntity = find(BlockEntity.class, blockRequest.getBlockEntity().getIdBlockEntityPk());
		if(!BlockEntityStateType.ACTIVATED.getCode().equals(blockEntity.getStateBlockEntity()))
			return ErrorServiceType.BLOCK_ENTITY_OFF;
		return null;
	}

	/**
	 * Validate entities states.
	 *
	 * @param blockOperation the block operation
	 * @param blHABlocked the bl ha blocked
	 * @return the error service type
	 */
	private ErrorServiceType validateEntitiesStates(BlockOperation blockOperation, boolean blHABlocked) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idHolderAccountPkParam", blockOperation.getHolderAccount().getIdHolderAccountPk());
		HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, params);
		if(blHABlocked){
			List<Integer> lstHAStates = new ArrayList<Integer>();
			lstHAStates.add(HolderAccountStatusType.ACTIVE.getCode());
			lstHAStates.add(HolderAccountStatusType.BLOCK.getCode());
			if(!lstHAStates.contains(holderAccount.getStateAccount()))
				return ErrorServiceType.HOLDER_ACCOUNT_BLOCK;
		}else{
			if(!HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()))
				return ErrorServiceType.HOLDER_ACCOUNT_BLOCK;
		}
		params = new HashMap<String, Object>();
		params.put("idSecurityCodePkParam", blockOperation.getSecurities().getIdSecurityCodePk());
		Security security = findObjectByNamedQuery(Security.SECURITY_STATE, params);
		if(!(SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity()) || SecurityStateType.GUARDA_EXCLUSIVE.getCode().equals(security.getStateSecurity())))
			return ErrorServiceType.SECURITY_BLOCK;
		params = new HashMap<String, Object>();
		params.put("idParticipantPkParam", blockOperation.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, params);
		if(!ParticipantStateType.REGISTERED.getCode().equals(participant.getState()))
			return ErrorServiceType.PARTICIPANT_BLOCK;
		return null;
	}
	
	/**
	 * Save pawn or reserve request.
	 *
	 * @param registerPawnOrReserveTO the register pawn or reserve to
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long savePawnOrReserveRequest(RegisterPawnOrReserveTO registerPawnOrReserveTO) throws ServiceException {
		Boolean handleMarketFact = Boolean.FALSE;
		//BigDecimal totalAmount = BigDecimal.ZERO;	
		//BigDecimal currentAmount = BigDecimal.ZERO;
		
		/*** SE CREA LA SOLICITUD DE AFECTACION ***/		
		BlockRequest objBlockOperationRequest = new BlockRequest();
		objBlockOperationRequest.setBlockOperation(new ArrayList<BlockOperation>());
		objBlockOperationRequest.setBlockRequestFile(new ArrayList<BlockRequestFile>());
		objBlockOperationRequest.setBlockNumber(registerPawnOrReserveTO.getNumberRecord());
		objBlockOperationRequest.setBlockNumberDate(registerPawnOrReserveTO.getDateRecord());
		objBlockOperationRequest.setBlockType(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk());			
		objBlockOperationRequest.setHolder(registerPawnOrReserveTO.getHolder());
		objBlockOperationRequest.setBlockEntity(registerPawnOrReserveTO.getBlockEntity());			
		objBlockOperationRequest.setRegistryUser(registerPawnOrReserveTO.getRegistryUser());
		objBlockOperationRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		objBlockOperationRequest.setBlockForm(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk());
		objBlockOperationRequest.setBlockRequestState(RequestAffectationStateType.REGISTERED.getCode());	
		objBlockOperationRequest.setIndIssueRequest(BooleanType.NO.getCode());
		objBlockOperationRequest.setIndRequestDeclaration(BooleanType.NO.getCode());
		objBlockOperationRequest.setComments(registerPawnOrReserveTO.getObservations());
		if(registerPawnOrReserveTO.isCheckAutoSettled()){
			objBlockOperationRequest.setIndAutoSettled(BooleanType.YES.getCode());
		}else{
			objBlockOperationRequest.setIndAutoSettled(BooleanType.NO.getCode());	
		}
		if (Validations.validateIsNotNull(registerPawnOrReserveTO.getIdParticipant())) {
			Participant objParticipant= new Participant();
			objParticipant.setIdParticipantPk(registerPawnOrReserveTO.getIdParticipant());
			objBlockOperationRequest.setParticipant(objParticipant);
		}
		
		if(Validations.validateIsNotNull(registerPawnOrReserveTO.getDocumentFile())){
			BlockRequestFile objBlockRequestFile = new BlockRequestFile();
			objBlockRequestFile.setRegistryUser(registerPawnOrReserveTO.getRegistryUser());
		    objBlockRequestFile.setRegistryDate(CommonsUtilities.currentDateTime());
		    objBlockRequestFile.setFileName(registerPawnOrReserveTO.getFileName());
		    objBlockRequestFile.setDocumentFile(registerPawnOrReserveTO.getDocumentFile());	
		    objBlockRequestFile.setBlockRequest(objBlockOperationRequest);
		    objBlockRequestFile.setDocumentType(AffectationDocumentType.SUPPORT_DOCUMENT.getCode());
		    objBlockOperationRequest.getBlockRequestFile().add(objBlockRequestFile);
		}
		
		ErrorServiceType entitiesStates = validateEntitiesStates(objBlockOperationRequest);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);			
		
		
		create(objBlockOperationRequest);
		
		/*******************************************/
		for(HolderAccountBalanceHeaderTO holAccBalHeaderTO:registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()){
			for(HolderAccountBalanceTO objHolAccBalaTO :holAccBalHeaderTO.getLstHolderAccountBalanceTO()){
				if(objHolAccBalaTO.isBlCheck() && holAccBalHeaderTO.getIdParticipantPk().equals(holAccBalHeaderTO.getIdParticipantPk())){
					CustodyOperation objCustodyOperation = new CustodyOperation();
					Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCKING);
					BlockOperation objBlockOperation = new BlockOperation();
					/*** DATOS DE LA OPERACION DE CUSTODIO ***/
					objCustodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
					objCustodyOperation.setRegistryUser(registerPawnOrReserveTO.getRegistryUser());
					objCustodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
					objBlockOperation.setBlockState(AffectationStateType.REGISTRED.getCode());
					objCustodyOperation.setState(CustodyOperationStateType.REGISTERED.getCode());
					objBlockOperation.setIndRegisteredDepositary(registerPawnOrReserveTO.getIndRegisteredDepositary());
					/*** SE GRABA LAS REFERENCIAS **/
					objBlockOperation.setBlockRequest(objBlockOperationRequest);
					objBlockOperation.setParticipant(new Participant(holAccBalHeaderTO.getIdParticipantPk()));
					objBlockOperation.setSecurities(new Security(objHolAccBalaTO.getIdSecurityCodePk()));
					objBlockOperation.setHolderAccount(new HolderAccount(holAccBalHeaderTO.getIdHolderAccountPk()));
					objBlockOperation.setNominalValue(objHolAccBalaTO.getNominalValue());
					
					/*SE GRABA EL ARCHIVO DE SUSTENTO*/
					objBlockOperation.setSupportFile(objHolAccBalaTO.getSupportFile());
					objBlockOperation.setSupportFileName(objHolAccBalaTO.getSupportFileName());
					/**********************************/
					
					/*** SE LLENAN LOS BENEFICIOS ***/
					fillBenefits(objHolAccBalaTO.getSelectedBenefits(),objBlockOperation);
					if(BigDecimal.ZERO.compareTo(objHolAccBalaTO.getReblock()) == 0){
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());
						if(AffectationType.RESERVE.getCode().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk()))
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_BLOCK.getCode());
						else
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_BLOCK.getCode());
						objBlockOperation.setActualBlockBalance(objHolAccBalaTO.getBlock());
						objBlockOperation.setOriginalBlockBalance(objHolAccBalaTO.getBlock());
						objCustodyOperation.setOperationNumber(operationNumber);
						objBlockOperation.setCustodyOperation(objCustodyOperation);
						objBlockOperation.setDocumentNumber(operationNumber);
						/***************************************************/							
						//totalAmount = totalAmount.add(objHolAccBalaTO.getBlock());
						//currentAmount = currentAmount.add(objHolAccBalaTO.getBlock());
						create(objBlockOperation);

						objBlockOperationRequest.getBlockOperation().add(objBlockOperation);
						entitiesStates = validateEntitiesStates(objBlockOperation, false);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
						/* Solo se aplica sobre los bloqueos 
						 * ya que afectan el saldo disponible en cambio los rebloqueos 
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */
						List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOperation);
						//Validamos si tiene Hechos de mercado	
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperation,objHolAccBalaTO,Boolean.FALSE);
							setAccountBalanceMarketFact(accountBalanceTOs, objBlockOperation);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperation.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOperation.getIdBlockOperationPk(),
																									 BusinessProcessType.SECURITY_BLOCK_PAWN_REGISTER.getCode(),
																									 accountBalanceTOs,
																									 objCustodyOperation.getOperationType()));
						/********************************************************************************/
					}else if(BigDecimal.ZERO.compareTo(objHolAccBalaTO.getBlock()) == 0){
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperation.setBlockLevel(LevelAffectationType.REBLOCK.getCode());
						if(AffectationType.RESERVE.getCode().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk()))
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_REBLOCK.getCode());
						else
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_REBLOCK.getCode());
						objBlockOperation.setActualBlockBalance(objHolAccBalaTO.getReblock());
						objBlockOperation.setOriginalBlockBalance(objHolAccBalaTO.getReblock());
						objCustodyOperation.setOperationNumber(operationNumber);
						objBlockOperation.setCustodyOperation(objCustodyOperation);
						objBlockOperation.setDocumentNumber(operationNumber);
						/***************************************************/
						//totalAmount = totalAmount.add(objHolAccBalaTO.getReblock());		
						create(objBlockOperation);
						objBlockOperationRequest.getBlockOperation().add(objBlockOperation);
						entitiesStates = validateEntitiesStates(objBlockOperation, false);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/* Solo se aplica sobre los bloqueos 
						 * ya que afectan el saldo disponible en cambio los rebloqueos 
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */						
						/********************************************************************************/
						/*SI TIENE HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperation,objHolAccBalaTO,Boolean.FALSE);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperation.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						/***********************************************************************************/
					}else{
						/************************************REGISTRAMOS EL BLOQUEO DE LA AFECTACION*****************************************/
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());		
						if(AffectationType.RESERVE.getCode().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk()))				
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_BLOCK.getCode());
						else
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_BLOCK.getCode());
						objBlockOperation.setActualBlockBalance(objHolAccBalaTO.getBlock());
						objBlockOperation.setOriginalBlockBalance(objHolAccBalaTO.getBlock());
						objCustodyOperation.setOperationNumber(operationNumber);
						objBlockOperation.setCustodyOperation(objCustodyOperation);						
						objBlockOperation.setDocumentNumber(operationNumber);
						/***************************************************/
						//totalAmount = totalAmount.add(objHolAccBalaTO.getBlock());		
						//currentAmount = currentAmount.add(objHolAccBalaTO.getBlock());
						create(objBlockOperation);
						objBlockOperationRequest.getBlockOperation().add(objBlockOperation);
						entitiesStates = validateEntitiesStates(objBlockOperation, false);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
						/* Solo se aplica sobre los bloqueos 
						 * ya que afectan el saldo disponible en cambio los rebloqueos 
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */	
						List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOperation);
						/*SI MANEJA HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperation,objHolAccBalaTO,Boolean.TRUE);
							setAccountBalanceMarketFact(accountBalanceTOs, objBlockOperation);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperation.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						/******************************************/
						accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOperation.getIdBlockOperationPk(),
																									 BusinessProcessType.SECURITY_BLOCK_PAWN_REGISTER.getCode(), 
																									 accountBalanceTOs,
																									 objCustodyOperation.getOperationType()));
						/********************************************************************************/
						
						/******************************************REGISTRAMOS EL REBLOQUEO DE LA AFECTACION*************************************************/
						CustodyOperation objCustodyOperationReblock = new CustodyOperation();
						BlockOperation objBlockOperationReblock = new BlockOperation();
						/*** DATOS DE LA OPERACION DE CUSTODIO ***/						
						objCustodyOperationReblock.setOperationDate(CommonsUtilities.currentDateTime());
						if(AffectationType.RESERVE.getCode().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk()))				
							objCustodyOperationReblock.setOperationType(ParameterOperationType.SECURITIES_RESERVE_REBLOCK.getCode());
						else
							objCustodyOperationReblock.setOperationType(ParameterOperationType.SECURITIES_PAWN_REBLOCK.getCode());									
						objCustodyOperationReblock.setRegistryUser(registerPawnOrReserveTO.getRegistryUser());
						objCustodyOperationReblock.setRegistryDate(CommonsUtilities.currentDateTime());									
						objBlockOperationReblock.setBlockState(AffectationStateType.REGISTRED.getCode());
						objCustodyOperationReblock.setOperationNumber(operationNumber);
						objCustodyOperationReblock.setState(AffectationStateType.REGISTRED.getCode());
						/*****************************************/
						/*** SE GRABA LAS REFERENCIAS **/
						objBlockOperationReblock.setBlockRequest(objBlockOperationRequest);						
						objBlockOperationReblock.setParticipant(new Participant(holAccBalHeaderTO.getIdParticipantPk()));						
						objBlockOperationReblock.setSecurities(new Security(objHolAccBalaTO.getIdSecurityCodePk()));
						objBlockOperationReblock.setHolderAccount(new HolderAccount(holAccBalHeaderTO.getIdHolderAccountPk()));								
						objBlockOperationReblock.setNominalValue(objHolAccBalaTO.getNominalValue());
						
						/*******************************/
						/*** SE LLENAN LOS BENEFICIOS ***/
						fillBenefits(objHolAccBalaTO.getSelectedBenefits(),objBlockOperationReblock);
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperationReblock.setBlockLevel(LevelAffectationType.REBLOCK.getCode());
						objBlockOperationReblock.setActualBlockBalance(objHolAccBalaTO.getReblock());
						objBlockOperationReblock.setOriginalBlockBalance(objHolAccBalaTO.getReblock());
						objCustodyOperationReblock.setOperationNumber(operationNumber);
						objBlockOperationReblock.setCustodyOperation(objCustodyOperationReblock);
						objBlockOperationReblock.setDocumentNumber(operationNumber);
						/***************************************************/
						//totalAmount = totalAmount.add(objHolAccBalaTO.getReblock());		
						create(objBlockOperationReblock);
						objBlockOperationRequest.getBlockOperation().add(objBlockOperationReblock);
						entitiesStates = validateEntitiesStates(objBlockOperationReblock, false);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
						/* Solo se aplica sobre los bloqueos
						 * ya que afectan el saldo disponible en cambio los rebloqueos
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */
						/********************************************************************************/
						/********************************************************************************/
						/*SI TIENE HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperationReblock,objHolAccBalaTO,Boolean.TRUE);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperationReblock.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						/***********************************************************************************/
					}
				}
			}//fin del for detalle
		}//fin del for cabecera
		//objBlockOperationRequest.setOriginBlockAmount(totalAmount);
		//objBlockOperationRequest.setCurrentBlockAmount(currentAmount);		
		update(objBlockOperationRequest);
		return objBlockOperationRequest.getIdBlockRequestPk();
	}

	/**
	 * Sets the block market fact operation.
	 *
	 * @param objBlockOperation the obj block operation
	 * @param objHolAccBalaTO the obj hol acc bala to
	 * @param haveReblock the have reblock
	 * @throws ServiceException the service exception
	 */
	private void setBlockMarketFactOperation(BlockOperation objBlockOperation, HolderAccountBalanceTO objHolAccBalaTO, Boolean haveReblock) throws ServiceException{
		// TODO Auto-generated method stub
		objBlockOperation.setBlockMarketFactOperations(new ArrayList<BlockMarketFactOperation>());
		/*SI NO TIENE HECHOS DE MERCADO, BUSCAMOS EL UNICO HECHO DE MERCADO PARA EL SALDO*/
		if(objHolAccBalaTO.getBlockMarketFactDetail()==null || objHolAccBalaTO.getBlockMarketFactDetail().isEmpty()){
			objHolAccBalaTO.setBlockMarketFactDetail(new ArrayList<BlockMarketFactTO>());
			BlockMarketFactTO blockMart = findMarketFactTOFromBalance(objHolAccBalaTO);
			blockMart.setTotalBalance(blockMart.getTotalBalance());
			blockMart.setAvailableBalance(blockMart.getAvailableBalance());
			blockMart.setBlockAmount(objBlockOperation.getActualBlockBalance());

			/*SI EXISTE REBLOQUEO ADICIONAMOS EL MONTO DE BLOQUEO MAS EL REBLOQUEO*/
			if(haveReblock){
				blockMart.setBlockAmount(blockMart.getBlockAmount().add(objHolAccBalaTO.getReblock()));
			}
			
			objHolAccBalaTO.getBlockMarketFactDetail().add(blockMart);
		}
		
		if(haveReblock){
			for(BlockMarketFactTO marketFatTO: objHolAccBalaTO.getBlockMarketFactDetail()){
				HolderMarketFactBalance holderMarketBalance = new HolderMarketFactBalance();
				holderMarketBalance.setIdMarketfactBalancePk(marketFatTO.getIdHolderMarketPk());
				
				BlockMarketFactOperation blockMarketFact = new BlockMarketFactOperation();

				if(objBlockOperation.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
					blockMarketFact.setOriginalBlockBalance(marketFatTO.getAvailableBalance());
					blockMarketFact.setActualBlockBalance(marketFatTO.getAvailableBalance());
				}else{
					blockMarketFact.setOriginalBlockBalance(marketFatTO.getBlockAmount().subtract(marketFatTO.getAvailableBalance()));
					blockMarketFact.setActualBlockBalance(marketFatTO.getBlockAmount().subtract(marketFatTO.getAvailableBalance()));
				}
				
				blockMarketFact.setBlockOperation(objBlockOperation);
//				blockMarketFact.setHolderMarketFactBalance(holderMarketBalance);
				blockMarketFact.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
				blockMarketFact.setRegistryUser(objBlockOperation.getCustodyOperation().getRegistryUser());
				blockMarketFact.setRegistryDate(objBlockOperation.getCustodyOperation().getRegistryDate());
				blockMarketFact.setMarketDate(marketFatTO.getMarketDate());
				blockMarketFact.setMarketRate(marketFatTO.getMarketRate());
				blockMarketFact.setMarketPrice(marketFatTO.getMarketPrice());
				
				objBlockOperation.getBlockMarketFactOperations().add(blockMarketFact);
			}
		}else{
			for(BlockMarketFactTO marketFatTO: objHolAccBalaTO.getBlockMarketFactDetail()){
				HolderMarketFactBalance holderMarketBalance = new HolderMarketFactBalance();
				holderMarketBalance.setIdMarketfactBalancePk(marketFatTO.getIdHolderMarketPk());
				
				BlockMarketFactOperation blockMarketFact = new BlockMarketFactOperation();
				blockMarketFact.setBlockOperation(objBlockOperation);
				blockMarketFact.setActualBlockBalance(marketFatTO.getBlockAmount());
				blockMarketFact.setOriginalBlockBalance(marketFatTO.getBlockAmount());
//				blockMarketFact.setHolderMarketFactBalance(holderMarketBalance);
				blockMarketFact.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
				blockMarketFact.setRegistryUser(objBlockOperation.getCustodyOperation().getRegistryUser());
				blockMarketFact.setRegistryDate(objBlockOperation.getCustodyOperation().getRegistryDate());
				blockMarketFact.setMarketDate(marketFatTO.getMarketDate());
				blockMarketFact.setMarketRate(marketFatTO.getMarketRate());
				blockMarketFact.setMarketPrice(marketFatTO.getMarketPrice());
				
				objBlockOperation.getBlockMarketFactOperations().add(blockMarketFact);
			}
		}
	}
	
	/**
	 * Find market fact to from balance.
	 *
	 * @param objHolAccBalaTO the obj hol acc bala to
	 * @return the block market fact to
	 * @throws ServiceException the service exception
	 */
	private BlockMarketFactTO findMarketFactTOFromBalance(HolderAccountBalanceTO objHolAccBalaTO) throws ServiceException{
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("Select new com.pradera.custody.affectation.to.BlockMarketFactTO(mark.IdMarketfactBalancePk,mark.marketPrice,mark.marketRate,mark.marketDate,"
				+ "																		mark.totalBalance, mark.availableBalance) ");
		stringSql.append("From HolderMarketFactBalance mark 	");
		stringSql.append("Where mark.security.idSecurityCodePk = :securityCode 			  And		");
		stringSql.append("		mark.participant.idParticipantPk = :participantCode 	  And		");
		stringSql.append("		mark.holderAccount.idHolderAccountPk = :holderAccountCode And		");
		stringSql.append("		mark.indActive = :oneValue											");
		
		Query query = em.createQuery(stringSql.toString());
		query.setParameter("securityCode", objHolAccBalaTO.getIdSecurityCodePk());
		query.setParameter("participantCode", objHolAccBalaTO.getIdParticipantPk());
		query.setParameter("holderAccountCode", objHolAccBalaTO.getIdHolderAccountPk());
		query.setParameter("oneValue", BooleanType.YES.getCode());
		
		try{
			return (BlockMarketFactTO) query.getSingleResult();
		}catch(NoResultException ex){
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
	}


	/**
	 * Sets the account balance market fact.
	 *
	 * @param accountBalanceTOs the account balance t os
	 * @param objBlockOperation the obj block operation
	 */
	private void setAccountBalanceMarketFact(List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs, BlockOperation objBlockOperation){
		// TODO Auto-generated method stub
		for(com.pradera.integration.component.business.to.HolderAccountBalanceTO hab: accountBalanceTOs){
			hab.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			if(hab.getIdParticipant().equals(objBlockOperation.getParticipant().getIdParticipantPk()) && 
			   hab.getIdSecurityCode().equals(objBlockOperation.getSecurities().getIdSecurityCodePk() )&&
			   hab.getIdHolderAccount().equals(objBlockOperation.getHolderAccount().getIdHolderAccountPk())){
				for(BlockMarketFactOperation blockHab: objBlockOperation.getBlockMarketFactOperations()){
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setQuantity(blockHab.getActualBlockBalance());
					marketFactAccountTO.setMarketDate(blockHab.getMarketDate());
					marketFactAccountTO.setMarketPrice(blockHab.getMarketPrice());
					marketFactAccountTO.setMarketRate(blockHab.getMarketRate());
					hab.getLstMarketFactAccounts().add(marketFactAccountTO);
				}
			}
		}
	}

	/**
	 * Save ban or others or opposition request.
	 *
	 * @param registerBanOrOthersTO the register ban or others to
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long saveBanOrOthersOrOppositionRequest(RegisterBanOrOthersTO registerBanOrOthersTO) throws ServiceException {
		
		Boolean handleMarketFact = Boolean.FALSE;

		BlockRequest objBlockOperationRequest = new BlockRequest();
		BigDecimal totalAmount = BigDecimal.ZERO;	
		//BigDecimal currentAmount = BigDecimal.ZERO;
		/*** SE CREA LA SOLICITUD DE AFECTACION ***/
		objBlockOperationRequest.setBlockOperation(new ArrayList<BlockOperation>());
		objBlockOperationRequest.setBlockNumber(registerBanOrOthersTO.getNumberRecord());
		objBlockOperationRequest.setBlockNumberDate(registerBanOrOthersTO.getDateRecord());
		objBlockOperationRequest.setBlockType(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk());
		objBlockOperationRequest.setHolder(registerBanOrOthersTO.getHolder());
		objBlockOperationRequest.setBlockEntity(registerBanOrOthersTO.getBlockEntity());
		objBlockOperationRequest.setRegistryUser(registerBanOrOthersTO.getRegistryUser());
		objBlockOperationRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		objBlockOperationRequest.setBlockRequestState(RequestAffectationStateType.REGISTERED.getCode());
		objBlockOperationRequest.setBlockForm(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk());
		objBlockOperationRequest.setIndIssueRequest(BooleanType.NO.getCode());
		objBlockOperationRequest.setIndRequestDeclaration(BooleanType.NO.getCode());
		if(registerBanOrOthersTO.isCheckAuthorityRestricted()){
			objBlockOperationRequest.setIndAuthorityRestricted(BooleanType.YES.getCode());
		}else{
			objBlockOperationRequest.setIndAuthorityRestricted(BooleanType.NO.getCode());
		}
		objBlockOperationRequest.setIndAutoSettled(BooleanType.NO.getCode());
		objBlockOperationRequest.setCircularNumber(registerBanOrOthersTO.getCircularNumber());
		objBlockOperationRequest.setFiscalOrigin(registerBanOrOthersTO.getFiscalProcedence());
		objBlockOperationRequest.setFiscalCurrency(registerBanOrOthersTO.getFiscalCurrency());
		objBlockOperationRequest.setFiscalAmount(registerBanOrOthersTO.getFiscalAmount());
		objBlockOperationRequest.setFiscalCharge(registerBanOrOthersTO.getFiscalChargeNumber());
		objBlockOperationRequest.setFiscalProcess(registerBanOrOthersTO.getFiscalProcess());
		objBlockOperationRequest.setFiscalAuthority(registerBanOrOthersTO.getFiscalAuthority());
		if (Validations.validateIsNotNull(registerBanOrOthersTO.getIdParticipant())) {
			Participant objParticipant= new Participant();
			objParticipant.setIdParticipantPk(registerBanOrOthersTO.getIdParticipant());
			objBlockOperationRequest.setParticipant(objParticipant);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getObservations())){
			objBlockOperationRequest.setComments(registerBanOrOthersTO.getObservations());
		}
		if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk())){
			objBlockOperationRequest.setCurrency(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk());
		}
		/*** DATOS SOLO DE DE EMBARGO ***/
		if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
			if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getStatementRequest())){
				if(BooleanType.YES.getCode().equals(new Integer(registerBanOrOthersTO.getStatementRequest())))
					objBlockOperationRequest.setIndRequestDeclaration(BooleanType.YES.getCode());			
				else 
					objBlockOperationRequest.setIndRequestDeclaration(BooleanType.NO.getCode());
			}else
				objBlockOperationRequest.setIndRequestDeclaration(BooleanType.NO.getCode());
		}		
		/********************************/
		/*** SOLO SI ES VALORIZACION ***/
		if(registerBanOrOthersTO.isBlCashValuation()){
			objBlockOperationRequest.setCurrency(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk());
			objBlockOperationRequest.setExchangeRate(registerBanOrOthersTO.getBdExchangeRate());
		}
		/********************************************/
		if(Validations.validateIsNotNull(registerBanOrOthersTO.getFileName())){
			BlockRequestFile objBlockRequestFile = new BlockRequestFile();
			objBlockOperationRequest.setBlockRequestFile(new ArrayList<BlockRequestFile>());
		    objBlockRequestFile.setRegistryUser(registerBanOrOthersTO.getRegistryUser());
		    objBlockRequestFile.setRegistryDate(CommonsUtilities.currentDateTime());
		    objBlockRequestFile.setFileName(registerBanOrOthersTO.getFileName());
		    objBlockRequestFile.setDocumentFile(registerBanOrOthersTO.getDocumentFile());		    	
		    objBlockRequestFile.setBlockRequest(objBlockOperationRequest);
		    //objBlockRequestFile.setDocumentType(AffectationDocumentType.SUPPORT_DOCUMENT.getCode());
		    objBlockOperationRequest.getBlockRequestFile().add(objBlockRequestFile);
		}
		
		ErrorServiceType entitiesStates = validateEntitiesStates(objBlockOperationRequest);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);			
		
		create(objBlockOperationRequest);
		
		/*******************************************/
		for(HolderAccountBalanceHeaderTO holAccBalHeaderTO:registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
			for(HolderAccountBalanceTO objHolAccBalaTO :holAccBalHeaderTO.getLstHolderAccountBalanceTO()){
				if(objHolAccBalaTO.isBlCheck() && objHolAccBalaTO.getIdParticipantPk().equals(holAccBalHeaderTO.getIdParticipantPk())
						&& objHolAccBalaTO.getIdHolderAccountPk().equals(holAccBalHeaderTO.getIdHolderAccountPk())
						){
					Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCKING);
					CustodyOperation objCustodyOperation = new CustodyOperation();
					BlockOperation objBlockOperation = new BlockOperation();
					/*** DATOS DE LA OPERACION DE CUSTODIA ***/						
					objCustodyOperation.setOperationDate(CommonsUtilities.currentDateTime());						
					objCustodyOperation.setRegistryUser(registerBanOrOthersTO.getRegistryUser());
					objCustodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
					objCustodyOperation.setOperationNumber(operationNumber);
					objBlockOperation.setDocumentNumber(operationNumber);
					objBlockOperation.setBlockState(AffectationStateType.REGISTRED.getCode());
					objCustodyOperation.setState(AffectationStateType.REGISTRED.getCode());
					objBlockOperation.setIndRegisteredDepositary(registerBanOrOthersTO.getIndRegisteredDepositary());
					/*****************************************/
					/*** SE ARMA LA MATRIZ ***/
					Participant objParticipant = new Participant();
					objParticipant.setIdParticipantPk(holAccBalHeaderTO.getIdParticipantPk());
					Security objSecurity = new Security();
					objSecurity.setIdSecurityCodePk(objHolAccBalaTO.getIdSecurityCodePk());
					HolderAccount objHolderAccount = new HolderAccount();
					objHolderAccount.setIdHolderAccountPk(holAccBalHeaderTO.getIdHolderAccountPk());
					/*************************/
					/*** SE GRABA LAS REFERENCIAS **/
					objBlockOperation.setBlockRequest(objBlockOperationRequest);
					objBlockOperationRequest.getBlockOperation().add(objBlockOperation);
					objBlockOperation.setParticipant(objParticipant);						
					objBlockOperation.setSecurities(objSecurity);						
					objBlockOperation.setHolderAccount(objHolderAccount);						
					/*******************************/
					/*SE GRABA EL ARCHIVO DE SUSTENTO*/
					objBlockOperation.setSupportFile(objHolAccBalaTO.getSupportFile());
					objBlockOperation.setSupportFileName(objHolAccBalaTO.getSupportFileName());
					objBlockOperation.setNominalValue(objHolAccBalaTO.getNominalValue());
					/*********************************/
					/*** SE LLENAN LOS BENEFICIOS ***/
					fillBenefits(objHolAccBalaTO.getSelectedBenefits(),objBlockOperation);
					/*** SOLO SI ES VALORIZACION ***/
					if(registerBanOrOthersTO.isBlCashValuation()){
						objBlockOperation.setNominalValue(objHolAccBalaTO.getNominalValue());
						objBlockOperation.setQuotationPrice(objHolAccBalaTO.getLastQuotation());
						totalAmount = totalAmount.add(objHolAccBalaTO.getValoritationQuantityBlockBalance());
					}
					/*******************************/
					if(BigDecimal.ZERO.compareTo(objHolAccBalaTO.getReblock()) == 0){
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());		
						if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))				
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_BLOCK.getCode());
						else if(AffectationType.OTHERS.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_BLOCK.getCode());
						else
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_BLOCK.getCode());
						objBlockOperation.setActualBlockBalance(objHolAccBalaTO.getBlock());
						objBlockOperation.setOriginalBlockBalance(objHolAccBalaTO.getBlock());
						objBlockOperation.setCustodyOperation(objCustodyOperation);
						/***************************************************/
						create(objBlockOperation);	
						entitiesStates = validateEntitiesStates(objBlockOperation, true);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
						/* Solo se aplica sobre los bloqueos 
						 * ya que afectan el saldo disponible en cambio los rebloqueos 
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */	
						List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOperation);
						
						/*SI MANEJA HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperation,objHolAccBalaTO,Boolean.FALSE);
							setAccountBalanceMarketFact(accountBalanceTOs, objBlockOperation);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperation.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						/****************************************/
						
						accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objCustodyOperation.getOperationNumber(),
																									 BusinessProcessType.SECURITY_BLOCK_REGISTER.getCode(), 
																									 accountBalanceTOs,
																									 objCustodyOperation.getOperationType()));
						/********************************************************************************/
					}else if(BigDecimal.ZERO.compareTo(objHolAccBalaTO.getBlock()) == 0){
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperation.setBlockLevel(LevelAffectationType.REBLOCK.getCode());
						if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))				
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_REBLOCK.getCode());
						else if(AffectationType.OTHERS.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_REBLOCK.getCode());
						else
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_REBLOCK.getCode());
						objBlockOperation.setActualBlockBalance(objHolAccBalaTO.getReblock());
						objBlockOperation.setOriginalBlockBalance(objHolAccBalaTO.getReblock());
						objBlockOperation.setCustodyOperation(objCustodyOperation);
						/***************************************************/
						create(objBlockOperation);
						entitiesStates = validateEntitiesStates(objBlockOperation, true);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
						/* Solo se aplica sobre los bloqueos 
						 * ya que afectan el saldo disponible en cambio los rebloqueos 
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */						
						/********************************************************************************/
						/*SI MANEJA HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperation,objHolAccBalaTO,Boolean.FALSE);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperation.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						/***********************************/
					}else{
						/**********************************************REGISTRAMOS EL BLOQUEO DE LA AFFECTACION*************************************************/
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());
						if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))				
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_BLOCK.getCode());
						else if(AffectationType.OTHERS.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_BLOCK.getCode());
						else
							objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_BLOCK.getCode());
						objBlockOperation.setActualBlockBalance(objHolAccBalaTO.getBlock());
						objBlockOperation.setOriginalBlockBalance(objHolAccBalaTO.getBlock());
						objBlockOperation.setCustodyOperation(objCustodyOperation);						
						/***************************************************/
						create(objBlockOperation);
						entitiesStates = validateEntitiesStates(objBlockOperation, true);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
						/* Solo se aplica sobre los bloqueos 
						 * ya que afectan el saldo disponible en cambio los rebloqueos 
						 * aun no entran ya que no se tiene saldo disponible pero si saldo contable */	
						List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOperation);
						/*SI MANEJA HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperation,objHolAccBalaTO,Boolean.TRUE);
							setAccountBalanceMarketFact(accountBalanceTOs, objBlockOperation);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperation.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
						/****************************************/
						accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objCustodyOperation.getOperationNumber(),
																									 BusinessProcessType.SECURITY_BLOCK_REGISTER.getCode(), 
																									 accountBalanceTOs,
																									 objCustodyOperation.getOperationType()));
						/********************************************************************************/
						
						/********************************REGISTRAMOS EL REBLOQUEO DE LA AFFECTACION***********************************************/
						CustodyOperation objCustodyOperationReblock = new CustodyOperation();
						BlockOperation objBlockOperationReblock = new BlockOperation();
						Long reblockOperationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCKING);
						/*** DATOS DE LA OPERACION DE CUSTODIO ***/						
						objCustodyOperationReblock.setOperationDate(CommonsUtilities.currentDateTime());
						if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))				
							objCustodyOperationReblock.setOperationType(ParameterOperationType.SECURITIES_BAN_REBLOCK.getCode());
						else if(AffectationType.OTHERS.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()))
							objCustodyOperationReblock.setOperationType(ParameterOperationType.SECURITIES_OTHERS_REBLOCK.getCode());
						else
							objCustodyOperationReblock.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_REBLOCK.getCode());								
						objCustodyOperationReblock.setRegistryUser(registerBanOrOthersTO.getRegistryUser());
						objCustodyOperationReblock.setRegistryDate(CommonsUtilities.currentDateTime());									
						objBlockOperationReblock.setBlockState(AffectationStateType.REGISTRED.getCode());
						objCustodyOperationReblock.setState(CustodyOperationStateType.REGISTERED.getCode());
						/*****************************************/
						/*** SE GRABA LAS REFERENCIAS **/
						objBlockOperationReblock.setBlockRequest(objBlockOperationRequest);
						objBlockOperationRequest.getBlockOperation().add(objBlockOperation);
						objBlockOperationReblock.setParticipant(objParticipant);
						objBlockOperationReblock.setSecurities(objSecurity);
						objBlockOperationReblock.setHolderAccount(objHolderAccount);
						objBlockOperationReblock.setNominalValue(objHolAccBalaTO.getNominalValue());
						/*******************************/
						/*** SE LLENAN LOS BENEFICIOS ***/
						fillBenefits(objHolAccBalaTO.getSelectedBenefits(),objBlockOperationReblock);
						/*** SOLO SI ES VALORIZACION ***/
						if(registerBanOrOthersTO.isBlCashValuation()){
							objBlockOperationReblock.setNominalValue(objHolAccBalaTO.getNominalValue());
							objBlockOperationReblock.setQuotationPrice(objHolAccBalaTO.getLastQuotation());							
						}
						/*******************************/
						/*** DATOS PREFERENTES SEGUN EL NIVEL DE BLOQUEO ***/
						objBlockOperationReblock.setBlockLevel(LevelAffectationType.REBLOCK.getCode());
						objBlockOperationReblock.setActualBlockBalance(objHolAccBalaTO.getReblock());
						objBlockOperationReblock.setOriginalBlockBalance(objHolAccBalaTO.getReblock());
						objBlockOperationReblock.setCustodyOperation(objCustodyOperationReblock);
						objCustodyOperationReblock.setOperationNumber(reblockOperationNumber);
						objBlockOperationReblock.setDocumentNumber(reblockOperationNumber);
						/***************************************************/
						create(objBlockOperationReblock);

						entitiesStates = validateEntitiesStates(objBlockOperationReblock, true);
						if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
							throw new ServiceException(entitiesStates);
						/*SI MANEJA HECHOS DE MERCADO*/
						if(handleMarketFact){
							setBlockMarketFactOperation(objBlockOperationReblock,objHolAccBalaTO,Boolean.TRUE);
							for(BlockMarketFactOperation blockMarketFact: objBlockOperationReblock.getBlockMarketFactOperations()){
								create(blockMarketFact);
							}
						}
					}					
				}
			}//fin del for detalle
		}//fin del for cabecera
		/*** SE ACTUALIZA EL MONTO TOTAL POR TODAS LAS AFECTACIONES ***/		
		if(AffectationFormType.CASH_VALUATION.getCode().equals(objBlockOperationRequest.getBlockForm())){
			objBlockOperationRequest.setOriginBlockAmount(totalAmount);
			objBlockOperationRequest.setCurrentBlockAmount(totalAmount);
		}
		update(objBlockOperationRequest);		
		return objBlockOperationRequest.getIdBlockRequestPk();
	}

	
	/**
	 * Approve affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean approveAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException
	{
		BlockRequest objBlockRequest = find(BlockRequest.class, requestAffectationTO.getNumberRequest());
		if(!RequestAffectationStateType.REGISTERED.getCode().equals(objBlockRequest.getBlockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objBlockRequest.getIdBlockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.BLOCK_REQUEST_STATE_MODIFY, bodyData);
		}
		objBlockRequest.getBlockEntity();
		objBlockRequest.getBlockRequestFile().size();
		objBlockRequest.getBlockOperation().size();
		ErrorServiceType errorServiceType = validateEntitiesStates(objBlockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
			throw new ServiceException(errorServiceType);
		
		objBlockRequest.setBlockRequestState(RequestAffectationStateType.APPROVED.getCode());
		objBlockRequest.setApproveDate(CommonsUtilities.currentDateTime());
		objBlockRequest.setApproveUser(requestAffectationTO.getApproveUser());
		if(requestAffectationTO.isCheckAutoSettled())
			objBlockRequest.setIndAutoSettled(BooleanType.YES.getCode());
		else
			objBlockRequest.setIndAutoSettled(BooleanType.NO.getCode());
		
		if(requestAffectationTO.isCheckAuthorityRestricted())
			objBlockRequest.setIndAuthorityRestricted(BooleanType.YES.getCode());
		else
			objBlockRequest.setIndAuthorityRestricted(BooleanType.NO.getCode());
		
		objBlockRequest.setCircularNumber(requestAffectationTO.getCircularNumber());
		objBlockRequest.setFiscalOrigin(requestAffectationTO.getFiscalProcedence());
		objBlockRequest.setFiscalCurrency(requestAffectationTO.getFiscalCurrency());
		objBlockRequest.setFiscalAmount(requestAffectationTO.getFiscalAmount());
		objBlockRequest.setFiscalCharge(requestAffectationTO.getFiscalChargeNumber());
		objBlockRequest.setFiscalProcess(requestAffectationTO.getFiscalProcess());
		objBlockRequest.setFiscalAuthority(requestAffectationTO.getFiscalAuthority());
		
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
		
		objBlockRequest.getBlockOperation().size();
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			BlockOperation objBlockOpeTemp = new BlockOperation();
			objBlockOpeTemp = objBlockOpe;
			if(lstAffectationType.contains(objBlockRequest.getBlockType()))
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, true);
			else
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, false);
			if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
				throw new ServiceException(errorServiceType);
								
			objBlockOpeTemp.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
			objBlockOpeTemp.getCustodyOperation().setApprovalUser(requestAffectationTO.getConfirmationUser());
		}
		update(objBlockRequest);
		return true;
	}
	
	/**
	 * Confirm affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @param loggerUser the logger user
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	public boolean confirmAffectation(RequestAffectationTO requestAffectationTO, LoggerUser loggerUser) throws Exception {
		BlockRequest objBlockRequest = find(BlockRequest.class, requestAffectationTO.getNumberRequest());
		if(!RequestAffectationStateType.APPROVED.getCode().equals(objBlockRequest.getBlockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objBlockRequest.getIdBlockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.BLOCK_REQUEST_STATE_MODIFY, bodyData);
		}
		objBlockRequest.getBlockEntity();
		objBlockRequest.getBlockRequestFile().size();
		objBlockRequest.getBlockOperation().size();
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
		
		ErrorServiceType errorServiceType = validateEntitiesStates(objBlockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
			throw new ServiceException(errorServiceType);
			
		objBlockRequest.setBlockRequestState(RequestAffectationStateType.CONFIRMED.getCode());
		objBlockRequest.setConfirmationDate(CommonsUtilities.currentDateTime());
		objBlockRequest.setConfirmationUser(requestAffectationTO.getConfirmationUser());
		if(requestAffectationTO.isCheckAutoSettled())
			objBlockRequest.setIndAutoSettled(BooleanType.YES.getCode());
		else
			objBlockRequest.setIndAutoSettled(BooleanType.NO.getCode());
		if(requestAffectationTO.isCheckAuthorityRestricted())
			objBlockRequest.setIndAuthorityRestricted(BooleanType.YES.getCode());
		else
			objBlockRequest.setIndAuthorityRestricted(BooleanType.NO.getCode());
		
		objBlockRequest.setCircularNumber(requestAffectationTO.getCircularNumber());
		objBlockRequest.setFiscalOrigin(requestAffectationTO.getFiscalProcedence());
		objBlockRequest.setFiscalCurrency(requestAffectationTO.getFiscalCurrency());
		objBlockRequest.setFiscalAmount(requestAffectationTO.getFiscalAmount());
		objBlockRequest.setFiscalCharge(requestAffectationTO.getFiscalChargeNumber());
		objBlockRequest.setFiscalProcess(requestAffectationTO.getFiscalProcess());
		objBlockRequest.setFiscalAuthority(requestAffectationTO.getFiscalAuthority());
		
		if(Validations.validateListIsNullOrEmpty(objBlockRequest.getBlockOperation())){			
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_NOT_FINDED);
		}
		
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			BlockOperation objBlockOpeTemp = new BlockOperation();
			objBlockOpeTemp = objBlockOpe;
			if(lstAffectationType.contains(objBlockRequest.getBlockType()))
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, true);
			else
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, false);
			if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
				throw new ServiceException(errorServiceType);
			
			objBlockOpeTemp.getCustodyOperation().setState(AffectationStateType.BLOCKED.getCode());
			objBlockOpeTemp.setBlockState(AffectationStateType.BLOCKED.getCode());					
			objBlockOpeTemp.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
			objBlockOpeTemp.getCustodyOperation().setConfirmUser(requestAffectationTO.getConfirmationUser());
		}
		
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOpe);
			/*CALLING METHOD TO SET HOLDER MARKET FACT*/
			setAccountBalanceMarketFact(accountBalanceTOs, objBlockOpe);
			/*************************************************************/
			accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOpe.getCustodyOperation().getIdCustodyOperationPk(),
																					 requestAffectationTO.getBusinessProcessTypeId(),
																					 accountBalanceTOs,objBlockOpe.getCustodyOperation().getOperationType()));
			
			/*
			 * CALLING THE CLIENT WEB SERVICE
			 */
			if(objBlockOpe.getSecurities().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
					objBlockOpe.getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
				custodyServiceConsumer.sendBlockOperationWebClient(objBlockOpe, loggerUser);
			}
			
		}
		update(objBlockRequest);
		return true;
	}


	/**
	 * Annul affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean annulAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException 
	{
		BlockRequest objBlockRequest = find(BlockRequest.class, new Long(requestAffectationTO.getNumberRequest()));
		if(!RequestAffectationStateType.REGISTERED.getCode().equals(objBlockRequest.getBlockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objBlockRequest.getIdBlockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.BLOCK_REQUEST_STATE_MODIFY, bodyData);
		}
		ErrorServiceType errorServiceType = validateEntitiesStates(objBlockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
			throw new ServiceException(errorServiceType);
		
		objBlockRequest.setBlockRequestState(RequestAffectationStateType.ANNULED.getCode());	
		objBlockRequest.setAnnulUser(requestAffectationTO.getAnnulUser());
		objBlockRequest.setAnnulDate(CommonsUtilities.currentDateTime());
	
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
		objBlockRequest.getBlockOperation().size();
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			BlockOperation objBlockOpeTemp = new BlockOperation();
			objBlockOpeTemp = objBlockOpe;
			
			if(lstAffectationType.contains(objBlockRequest.getBlockType()))
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, true);
			else
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, false);
			if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
				throw new ServiceException(errorServiceType);
			
			objBlockOpeTemp.getCustodyOperation().setState(AffectationStateType.ANNULLED.getCode());
			objBlockOpeTemp.getCustodyOperation().setAnnulationDate(CommonsUtilities.currentDateTime());
			objBlockOpeTemp.getCustodyOperation().setAnnulationUser(requestAffectationTO.getRejectUser());
			objBlockOpeTemp.setBlockState(AffectationStateType.ANNULLED.getCode());
			objBlockOpeTemp.getCustodyOperation().setRejectMotive(requestAffectationTO.getRejectMotive());
			objBlockOpeTemp.getCustodyOperation().setRejectMotiveOther(requestAffectationTO.getRejectMotiveOther());
		}
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			if(objBlockOpe.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
				List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOpe);
				/*CALLING METHOD TO SET HOLDER MARKET FACT*/
				setAccountBalanceMarketFact(accountBalanceTOs, objBlockOpe);
				/********************************************/
				accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOpe.getCustodyOperation().getOperationNumber(),
																						 requestAffectationTO.getBusinessProcessTypeId(), 
																						 accountBalanceTOs,objBlockOpe.getCustodyOperation().getOperationType()));
			}
		}				    
		update(objBlockRequest);
		return true;
	}
	
	/**
	 * Reject affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean rejectAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException {
		BlockRequest objBlockRequest = find(BlockRequest.class, new Long(requestAffectationTO.getNumberRequest()));
		if(!RequestAffectationStateType.APPROVED.getCode().equals(objBlockRequest.getBlockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objBlockRequest.getIdBlockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.BLOCK_REQUEST_STATE_MODIFY, bodyData);
		}
		
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
		
		ErrorServiceType errorServiceType = validateEntitiesStates(objBlockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
			throw new ServiceException(errorServiceType);
		
		objBlockRequest.setBlockRequestState(RequestAffectationStateType.REJECTED.getCode());	
		objBlockRequest.setRejectUser(requestAffectationTO.getRejectUser());
		objBlockRequest.setRejectDate(CommonsUtilities.currentDateTime());
		objBlockRequest.getBlockOperation().size();			
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			BlockOperation objBlockOpeTemp = new BlockOperation();
			objBlockOpeTemp = objBlockOpe;
			
			if(lstAffectationType.contains(objBlockRequest.getBlockType()))
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, true);
			else
				errorServiceType = validateEntitiesStates(objBlockOpeTemp, false);
			if(Validations.validateIsNotNullAndNotEmpty(errorServiceType))
				throw new ServiceException(errorServiceType);
			
			objBlockOpeTemp.getCustodyOperation().setState(AffectationStateType.ANNULLED.getCode());
			objBlockOpeTemp.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			objBlockOpeTemp.getCustodyOperation().setRejectUser(requestAffectationTO.getRejectUser());
			objBlockOpeTemp.getCustodyOperation().setRejectMotive(requestAffectationTO.getRejectMotive());
			objBlockOpeTemp.getCustodyOperation().setRejectMotiveOther(requestAffectationTO.getRejectMotiveOther());
			objBlockOpeTemp.setBlockState(AffectationStateType.ANNULLED.getCode());	
		}			
		for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
			if(objBlockOpe.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
				List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOpe);
				/*CALLING METHOD TO SET HOLDER MARKET FACT*/
				setAccountBalanceMarketFact(accountBalanceTOs, objBlockOpe);
				/********************************************/
				accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOpe.getCustodyOperation().getOperationNumber(),
																						 requestAffectationTO.getBusinessProcessTypeId(), 
																						 accountBalanceTOs,objBlockOpe.getCustodyOperation().getOperationType()));
			}
		}				    
		update(objBlockRequest);
		return true;
	}

	/**
	 * Find available balance.
	 *
	 * @param idHolAccBal the id hol acc bal
	 * @return the big decimal
	 * @throws ServiceException the service exception
	 */
	public BigDecimal findAvailableBalance(HolderAccountBalancePK idHolAccBal) throws ServiceException {
		try{
			String strQuery = "		SELECT hab.availableBalance" +
							  "		FROM HolderAccountBalance hab " +
							  "		WHERE hab.availableBalance > 0" +
							  "		AND hab.id.idHolderAccountPk = :idHolderAccountPk" +
							  "		AND hab.id.idIsinCodePk = :idIsinCodePk" +
							  "		AND hab.id.idParticipantPk = :idParticipantPk";
			Query query = em.createQuery(strQuery);		
			query.setParameter("idHolderAccountPk", idHolAccBal.getIdHolderAccountPk());
			query.setParameter("idParticipantPk", idHolAccBal.getIdParticipantPk());
			query.setParameter("idIsinCodePk", idHolAccBal.getIdSecurityCodePk());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return new BigDecimal(query.getSingleResult().toString());
		}catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
		return BigDecimal.ZERO;
	}

	/**
	 * Exist number record.
	 *
	 * @param numberRecord the number record
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> existNumberRecord(String numberRecord) throws ServiceException{
		String strQuery = "			SELECT idBlockRequestPk, " +
							" (SELECT parameterName FROM ParameterTable where parameterTablePk = BR.blockRequestState)" +					
							"		FROM BlockRequest BR" +
							"		WHERE BR.blockNumber = :blockNumber" +
							"		AND BR.blockRequestState in (:lstState)";		
		Query query = em.createQuery(strQuery);
		query.setParameter("blockNumber", numberRecord);
		List<Integer> lstState = new ArrayList<Integer>();
		lstState.add(RequestAffectationStateType.CONFIRMED.getCode());
		lstState.add(RequestAffectationStateType.REGISTERED.getCode());
		query.setParameter("lstState", lstState);
		return query.getResultList();
	}

	/**
	 * Find block operation detail.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return the block operation
	 * @throws ServiceException the service exception
	 */
	public BlockOperation findBlockOperationDetail(Long idBlockOperationPk) throws ServiceException{
		String strQuery = "			SELECT BO " +					
							"		FROM BlockOperation BO" +
							"		INNER JOIN FETCH BO.blockRequest BR" +
							"		INNER JOIN FETCH BR.blockEntity" +
							"		WHERE BO.idBlockOperationPk = :idBlockOperationPk";		
		Query query = em.createQuery(strQuery);
		query.setParameter("idBlockOperationPk", idBlockOperationPk);
		return (BlockOperation) query.getSingleResult();
	}

	/**
	 * Find movements for block operation.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovement> findMovementsForBlockOperation(Long idBlockOperationPk) {
		List<HolderAccountMovement> lstTotalMovements = new ArrayList<HolderAccountMovement>();
		String strQuery = "			SELECT HAM " +					
						"		FROM HolderAccountMovement HAM" +
						"		LEFT JOIN FETCH HAM.refCustodyOperation" +
						"		INNER JOIN FETCH HAM.movementType MOVT" +
						"		INNER JOIN FETCH HAM.custodyOperation" +
						"		WHERE MOVT.indVisibility = :oneParam " +
						"			AND (HAM.custodyOperation.idCustodyOperationPk = :idBlockOperationPk " +
						"				OR HAM.refCustodyOperation.idCustodyOperationPk = :idBlockOperationPk) " +
						"		ORDER BY HAM.idHolderAccountMovementPk DESC";		
		Query query = em.createQuery(strQuery);
		query.setParameter("idBlockOperationPk", idBlockOperationPk);
		query.setParameter("oneParam", BooleanType.YES.getCode());
		lstTotalMovements.addAll((List<HolderAccountMovement>)query.getResultList());
		/*
		strQuery = "			SELECT HAM " +					
				"		FROM HolderAccountMovement HAM" +
				"		INNER JOIN FETCH HAM.movementType	MOVT" +
				"		INNER JOIN FETCH HAM.custodyOperation" +						
				"		WHERE HAM.custodyOperation.idCustodyOperationPk = :idBlockOperationPk	AND MOVT.indVisibility = :oneParam" +
				"		ORDER BY HAM.idHolderAccountMovementPk DESC";		
		query = em.createQuery(strQuery);
		query.setParameter("idBlockOperationPk", idBlockOperationPk);
		query.setParameter("oneParam", BooleanType.YES.getCode());
		lstTotalMovements.addAll((List<HolderAccountMovement>)query.getResultList());
		*/		
		return lstTotalMovements;
	}


	/**
	 * Find block request detail.
	 *
	 * @param idBlockRequestPk the id block request pk
	 * @param idIssuer the id issuer
	 * @return the block request
	 */
	public BlockRequest findBlockRequestDetail(Long idBlockRequestPk, String idIssuer) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT BR FROM BlockRequest BR ");
		stringBuffer.append(" INNER JOIN FETCH BR.blockOperation BO ");
		stringBuffer.append(" INNER JOIN FETCH BR.blockEntity ");
		stringBuffer.append(" INNER JOIN FETCH BO.custodyOperation ");
		stringBuffer.append(" INNER JOIN FETCH BO.securities SE ");
		stringBuffer.append(" INNER JOIN FETCH SE.issuer ");
		stringBuffer.append(" WHERE BR.idBlockRequestPk = :idBlockRequestPk ");
		if (Validations.validateIsNotNull(idIssuer)) {
			stringBuffer.append(" and BO.securities.issuer.idIssuerPk = :idIssuer ");
		}
		stringBuffer.append(" ORDER BY BO.holderAccount.idHolderAccountPk desc ");

		TypedQuery<BlockRequest> query = em.createQuery(stringBuffer.toString(), BlockRequest.class);
		query.setParameter("idBlockRequestPk", idBlockRequestPk);
		if (Validations.validateIsNotNull(idIssuer)) {
			query.setParameter("idIssuer", idIssuer);
		}
		return query.getSingleResult();
	}

	/**
	 * Find block request for revert.
	 *
	 * @param oldDays the old days
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<BlockRequest> findBlockRequestForRevert(Integer oldDays) {
		String strQuery = "			SELECT BR " +					
						"		FROM BlockRequest BR" +
						"		INNER JOIN FETCH BR.blockOperation " +
						"		WHERE BR.registryDate = :oldDate";		
		Query query = em.createQuery(strQuery);
		query.setParameter("oldDate", CommonsUtilities.addDate(CommonsUtilities.currentDate(), -oldDays));
		return (List<BlockRequest>)query.getResultList();
	}

	/**
	 * Gets the amount for ban or opposition.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idParticipantPk the id participant pk
	 * @param idIsinCodePk the id isin code pk
	 * @return the amount for ban or opposition
	 * @throws ServiceException the service exception
	 */
	public BigDecimal getAmountForBanOrOpposition(Long idHolderAccountPk, Long idParticipantPk, String idIsinCodePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("SELECT SUM(actualBlockBalance) FROM BlockOperation bo");
			sbQuery.append("	WHERE bo.participant.idParticipantPk = :idParticipantPk");
			sbQuery.append("	AND bo.holderAccount.idHolderAccountPk = :idHolderAccountPk");
			sbQuery.append("	AND bo.securities.idSecurityCodePk = :idSecurityCodePk");
			sbQuery.append("	AND bo.blockRequest.blockType in (:banOrOpposition)");
			sbQuery.append("	AND bo.blockState = :blockState");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idParticipantPk", idParticipantPk);
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			query.setParameter("idSecurityCodePk", idIsinCodePk);
			query.setParameter("blockState", AffectationStateType.REGISTRED.getCode());
			List<Integer> lstBanOrOppo = new ArrayList<Integer>();
			lstBanOrOppo.add(AffectationType.BAN.getCode());
			lstBanOrOppo.add(AffectationType.OPPOSITION.getCode());
			query.setParameter("banOrOpposition", lstBanOrOppo);
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return new BigDecimal(query.getSingleResult().toString());
		} catch (NoResultException e) {
			return BigDecimal.ZERO;
		}
		return BigDecimal.ZERO;
	}
	
	/**
	 * Market fact business process.
	 *
	 * @param businessProcessType the business process type
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean marketFactBusinessProcess(Long businessProcessType) throws ServiceException{
		String querySql = "Select setup.indMarketFact From IdepositarySetup setup";
		String queryProcessSql = "Select bproc.indMarketFact From BusinessProcess bproc Where idBusinessProcessPk = :businessProcessPk";
		Query query = em.createQuery(querySql);
		Query queryProc = em.createQuery(queryProcessSql);
		queryProc.setParameter("businessProcessPk", businessProcessType);
		
		if(query.getSingleResult().equals(BooleanType.YES.getCode()) && queryProc.getSingleResult().equals(BooleanType.YES.getCode())){
			return Boolean.TRUE;
		}
		return Boolean.FALSE; 
	}
	
	/**
	 * Balance with market fact quantity.
	 *
	 * @param balancesID the balances id
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Integer> balanceWithMarketFactQuantity(
			List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> balancesID) throws ServiceException{
		
		List<String> balancesIDFormated = new ArrayList<String>();
		for(com.pradera.integration.component.business.to.HolderAccountBalanceTO hab: balancesID){
			balancesIDFormated.add(new StringBuilder().append(hab.getIdParticipant()).append(hab.getIdHolderAccount()).append(hab.getIdSecurityCode()).toString());
		}
		
		List<String> auxListBalances = new ArrayList<String>();
		List<Object[]> dataListResult = new ArrayList<Object[]>(); 
		for(int i=0;i<balancesIDFormated.size();i++){
				auxListBalances.add(balancesIDFormated.get(i));
			
			if(auxListBalances.size()==1000 || i==balancesIDFormated.size()-1){
				StringBuilder stringSql = new StringBuilder();
				stringSql.append("SELECT	"); 
				stringSql.append("hab.id.idParticipantPk, hab.id.idHolderAccountPk, hab.id.idSecurityCodePk, COUNT(hmb) as tam	");//0,1,2,3
				stringSql.append("FROM HolderAccountBalance hab	");
				stringSql.append("LEFT OUTER JOIN hab.holderMarketfactBalance hmb		");
				stringSql.append("WHERE (hab.id.idParticipantPk || hab.id.idHolderAccountPk || hab.id.idSecurityCodePk) ");
				stringSql.append("IN (:balancesId)	");
				stringSql.append("And hmb.indActive = :oneParam	");
				stringSql.append("Group By hab.id.idParticipantPk, hab.id.idHolderAccountPk, hab.id.idSecurityCodePk");
				
				Query query = em.createQuery(stringSql.toString());
				query.setParameter("balancesId", auxListBalances);
				query.setParameter("oneParam", BooleanType.YES.getCode());
				
				List<Object[]> dataList = query.getResultList();
				dataListResult.addAll(dataList);
				
				auxListBalances = new ArrayList<String>();
				
				em.flush(); 
				em.clear();
			}
			
		}
		
		Map<String,Integer> mapMarketFactResult = new HashMap<String, Integer>();
		
		for(Object[] data: dataListResult){
			StringBuilder balanceKey = new StringBuilder(); 
			balanceKey.append((Long) data[0]);
			balanceKey.append((Long) data[1]);
			balanceKey.append((String) data[2]);
			mapMarketFactResult.put(balanceKey.toString(), new Integer(data[3].toString()));
		}
		return mapMarketFactResult;
	}
	
	/**
	 * Gets the list request affectation by revert.
	 *
	 * @param searchRequestAffectationTO the search request affectation to
	 * @return the list request affectation by revert
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListRequestAffectationByRevert(SearchRequestAffectationTO searchRequestAffectationTO) throws ServiceException {
		StringBuilder stringSql = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();
		stringSql.append("	SELECT BR.idBlockRequestPk, BR.blockType, BR.blockRequestState FROM BlockRequest BR	"); 
		stringSql.append("	WHERE BR.blockRequestState in (:lstBlockRequestState) ");
		stringSql.append("	AND trunc(BR.registryDate) = trunc(:endDate)");				
		
		mapParam.put("lstBlockRequestState", searchRequestAffectationTO.getLstParamState());
		mapParam.put("endDate", searchRequestAffectationTO.getEndDate());				
		
		Query query = em.createQuery(stringSql.toString());		
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		while(it.hasNext()){        	
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	} 
	
	/**
	 * Gets the list issuers.
	 *
	 * @return the list issuers
	 */
	public List<Issuer> getListIssuers() {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT ISS ");
		stringBuffer.append(" FROM Issuer ISS ");
		stringBuffer.append(" WHERE ISS.stateIssuer = :stateIssuer ");
		
		TypedQuery<Issuer> typedQuery= em.createQuery(stringBuffer.toString(), Issuer.class);
		typedQuery.setParameter("stateIssuer", IssuerStateType.REGISTERED.getCode());
		return typedQuery.getResultList();
	}
	
	/**
	 * Verify block operation at other requests.
	 *
	 * @param idBlockOperation the id block operation
	 * @param operationNumber the operation number
	 * @throws ServiceException the service exception
	 */
	public void verifyBlockOperationAtOtherRequests(Long idBlockOperation, String operationNumber) throws ServiceException{
		if (existsBlockOperationAtTransferRequest(idBlockOperation)) {
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_EXISTS_SECURITIES_TRANSFER_OPERATION, new Object[]{operationNumber});
		}
		if (existsBlockOperationAtChangeOwnershipRequest(idBlockOperation)) {
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_EXISTS_CHANGE_OWNERCHIP_OPERATION, new Object[]{operationNumber});
		}
		if (existsBlockOperationAtUnblockRequest(idBlockOperation)) {
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_EXISTS_UNBLOCK_OPERATION, new Object[]{operationNumber});
		}
		if (existsBlockOperationAtEnforceRequest(idBlockOperation)) {
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_EXISTS_ENFORCE_OPERATION, new Object[]{operationNumber});
		}
		if (existsBlockOperationAtWithdrawalRequest(idBlockOperation)) {
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_EXISTS_WITHDRAWAL_OPERATION, new Object[]{operationNumber});
		}
	}
	
	/**
	 * Exists block operation at unblock request.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean existsBlockOperationAtUnblockRequest(Long idBlockOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT UO.idUnblockOperationPk ");
		stringBuffer.append(" FROM UnblockOperation UO ");
		stringBuffer.append(" WHERE UO.blockOperation.idBlockOperationPk = :idBlockOperation ");
		stringBuffer.append(" and UO.unblockState in (:lstUnblockState) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idBlockOperation", idBlockOperation);
		List<Integer> lstUnblockState= new ArrayList<Integer>();
		lstUnblockState.add(RequestReversalsStateType.REGISTERED.getCode());
		lstUnblockState.add(RequestReversalsStateType.APPROVED.getCode());
		query.setParameter("lstUnblockState", lstUnblockState);

		List<Object> lstObject= query.getResultList();
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Exists block operation at transfer request.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean existsBlockOperationAtTransferRequest(Long idBlockOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT BOT.idBlockOperationTransferPk ");
		stringBuffer.append(" FROM BlockOperationTransfer BOT ");
		stringBuffer.append(" WHERE BOT.blockOperation.idBlockOperationPk = :idBlockOperation ");
		stringBuffer.append(" and BOT.securityTransferOperation.state in (:lstBlockOperationTransferState) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idBlockOperation", idBlockOperation);
		List<Integer> lstBlockOperationTransferState= new ArrayList<Integer>();
		lstBlockOperationTransferState.add(TransferSecuritiesStateType.REGISTRADO.getCode());
		lstBlockOperationTransferState.add(TransferSecuritiesStateType.APROBADO.getCode());
		lstBlockOperationTransferState.add(TransferSecuritiesStateType.REVISADO.getCode());
		query.setParameter("lstBlockOperationTransferState", lstBlockOperationTransferState);

		List<Object> lstObject= query.getResultList();
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Exists block operation at change ownership request.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean existsBlockOperationAtChangeOwnershipRequest(Long idBlockOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT BCO.idBlockChangeOwnershipPk ");
		stringBuffer.append(" FROM BlockChangeOwnership BCO ");
		stringBuffer.append(" WHERE BCO.blockOperation.idBlockOperationPk = :idBlockOperation ");
		stringBuffer.append(" and BCO.changeOwnershipDetail.changeOwnershipOperation.state = :blockChangeOwnershipState ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idBlockOperation", idBlockOperation);
		query.setParameter("blockChangeOwnershipState", ChangeOwnershipStatusType.REGISTRADO.getCode());

		List<Object> lstObject= query.getResultList();
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Exists block operation at enforce request.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean existsBlockOperationAtEnforceRequest(Long idBlockOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT BED.idEnforceDetailPk ");
		stringBuffer.append(" FROM BlockEnforceDetail BED ");
		stringBuffer.append(" WHERE BED.blockOperation.idBlockOperationPk = :idBlockOperation ");
		stringBuffer.append(" and BED.blockEnforceOperation.operationState = :blockEnforceState ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idBlockOperation", idBlockOperation);
		query.setParameter("blockEnforceState", BlockEnforceStateType.REGISTERED.getCode());

		List<Object> lstObject= query.getResultList();
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Exists block operation at withdrawal request.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean existsBlockOperationAtWithdrawalRequest(Long idBlockOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT RBD.idRetirementBlockDetPk ");
		stringBuffer.append(" FROM RetirementBlockDetail RBD ");
		stringBuffer.append(" WHERE RBD.blockOperation.idBlockOperationPk = :idBlockOperation ");
		stringBuffer.append(" and RBD.retirementDetail.retirementOperation.state IN (:lstRetirementBlockState) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idBlockOperation", idBlockOperation);
		List<Integer> lstRetirementBlockState= new ArrayList<Integer>();
		lstRetirementBlockState.add(RetirementOperationStateType.REGISTERED.getCode());
		lstRetirementBlockState.add(RetirementOperationStateType.APPROVED.getCode());
		query.setParameter("lstRetirementBlockState", lstRetirementBlockState);

		List<Object> lstObject= query.getResultList();
		
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Verify block operation registered by edv.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyBlockOperationRegisteredByEDV(Long idBlockOperation) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT BO.idBlockOperationPk ");
		stringBuffer.append(" FROM BlockOperation BO");
		stringBuffer.append(" WHERE BO.idBlockOperationPk = :idBlockOperation ");
		stringBuffer.append(" and BO.indRegisteredDepositary = :indRegisteredByEDV ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("idBlockOperation", idBlockOperation);
		query.setParameter("indRegisteredByEDV", BooleanType.YES.getCode());
		
		List<Object> lstObject= query.getResultList();
		if (Validations.validateListIsNotNullAndNotEmpty(lstObject)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Identify block operations to send.
	 *
	 * @param sendDate the send date
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void identifyBlockOperationsToSend(Date sendDate,LoggerUser loggerUser) throws ServiceException{
		/** OBTENEMOS LA LISTA DE OP. DE BLOQUEO A ENVIAR **/
		List<BlockOperation> lstBlockOperation = getListBlockOperationWebService(sendDate);
		if(Validations.validateListIsNotNullAndNotEmpty(lstBlockOperation)){
			/** CALLING THE CLIENT WEB SERVICE **/
			for (BlockOperation objBlockOperation : lstBlockOperation){				
				custodyServiceConsumer.sendBlockOperationWebClient(objBlockOperation, loggerUser);
			}
		}
	}
	
	/**
	 * Gets the list block operation web service.
	 *
	 * @param sendDate the send date
	 * @return the list block operation web service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<BlockOperation> getListBlockOperationWebService(Date sendDate) throws ServiceException{
		List<BlockOperation> lstBlockOperation = null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT BO FROM BlockOperation BO ");
		stringBuffer.append(" 	inner join fetch BO.securities sec ");
		stringBuffer.append(" 	inner join fetch BO.blockRequest br ");
		stringBuffer.append(" 	inner join fetch BO.custodyOperation co ");
		stringBuffer.append(" 	inner join fetch BO.participant p ");
		stringBuffer.append(" 	inner join fetch BO.holderAccount ha ");
		stringBuffer.append(" 	inner join fetch br.blockEntity be ");
		stringBuffer.append(" WHERE trunc(br.confirmationDate) = trunc(:date) ");
		stringBuffer.append(" 	and br.blockRequestState = :blockRequestSt ");
		stringBuffer.append(" 	and co.indWebservice = :indWebService ");
		stringBuffer.append(" 	and BO.idBlockOperationPk not in ");
		stringBuffer.append("		(select rbo.blockOperation.idBlockOperationPk from RenewalBlockOperation rbo ");
		stringBuffer.append("		where BO.idBlockOperationPk = rbo.blockOperation.idBlockOperationPk) ");
		stringBuffer.append(" 	and sec.securityClass in (:classDPF,:classDPA) ");
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("date", sendDate);
		query.setParameter("blockRequestSt", RequestAffectationStateType.CONFIRMED.getCode());
		query.setParameter("indWebService", ComponentConstant.ZERO);
		query.setParameter("classDPF", SecurityClassType.DPF.getCode());
		query.setParameter("classDPA", SecurityClassType.DPA.getCode());
		lstBlockOperation  = (List<BlockOperation>) query.getResultList();
		return lstBlockOperation;
	}		
	
	/**
	 * Update ind web service.
	 *
	 * @param objCustodyOperation the obj custody operation
	 * @throws ServiceException the service exception
	 */
	public void updateIndWebService(CustodyOperation objCustodyOperation) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Update CustodyOperation co set co.indWebservice = :indWebService Where co.idCustodyOperationPk = :idCustodyOperation");
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idCustodyOperation", objCustodyOperation.getIdCustodyOperationPk());
		query.setParameter("indWebService", ComponentConstant.ONE);
		query.executeUpdate();	
	}
	
	/**
	 * Search reported operation.
	 *
	 * @param idHolderAccountPks the id holder account pks
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> searchReportedOperation(List<Long> idHolderAccountPks) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();
		Query query = null;
		List<MechanismOperationTO> mechanismOperationTOList = null;
		try {			
			//Creating query string
			sbQuery.append(" SELECT hao.holderAccount.participant.mnemonic, 					"); //0
			sbQuery.append(" hao.holderAccount.participant.idParticipantPk, 					"); //1
			sbQuery.append(" hao.holderAccount.accountNumber, 									"); //2
			sbQuery.append(" had.holder.name, 													"); //3
			sbQuery.append(" had.holder.firstLastName, 											"); //4
			sbQuery.append(" had.holder.secondLastName, 										"); //5
			sbQuery.append(" had.holder.fullName, 												"); //6
			sbQuery.append(" mo.operationState, 												"); //7
			sbQuery.append(" mo.operationNumber, 												"); //8
			sbQuery.append(" mo.securities.idSecurityCodePk, 									"); //9
			sbQuery.append(" mo.stockQuantity, 													"); //10
			sbQuery.append(" mo.operationDate, 													"); //11
			sbQuery.append(" mo.termSettlementDate, 											"); //12
			sbQuery.append(" (mo.termSettlementDate - mo.cashSettlementDate) AS plazo, 			"); //13
			sbQuery.append(" hao.holderAccount.idHolderAccountPk, 								"); //14
			sbQuery.append(" mo.idMechanismOperationPk, 										"); //15
			sbQuery.append(" had.holder.idHolderPk, 											"); //16
			sbQuery.append(" hao.idHolderAccountOperationPk 									"); //17
			sbQuery.append(" from MechanismOperation mo 										");
			sbQuery.append(" inner join mo.holderAccountOperations hao 							");
			sbQuery.append(" Inner join hao.holderAccount.holderAccountDetails had 				");
			sbQuery.append(" where mo.operationState = :stateOperation 							");
			sbQuery.append(" and hao.role = :roleType 											");
			sbQuery.append(" and mo.mechanisnModality.negotiationModality.indReportingBalance = :indReported ");
			mapParam.put("stateOperation", MechanismOperationStateType.CASH_SETTLED.getCode());
			mapParam.put("indReported", GeneralConstants.ONE_VALUE_INTEGER);
			mapParam.put("roleType", ParticipantRoleType.BUY.getCode());

			if(Validations.validateListIsNotNullAndNotEmpty(idHolderAccountPks)){
				sbQuery.append(" and hao.holderAccount.idHolderAccountPk in (:idHolderAccountPks) ");
				mapParam.put("idHolderAccountPks", idHolderAccountPks);
			}			
			query = em.createQuery(sbQuery.toString());			
			@SuppressWarnings("rawtypes")
			Iterator it= mapParam.entrySet().iterator();
	        while(it.hasNext()){
				Entry<String,Object> entry = (Entry<String,Object>)it.next();
	        	query.setParameter(entry.getKey(), entry.getValue());
	        }
	        
	        List<Object> objectList = query.getResultList(); 
	        
	        if(objectList.size() > 0){
				mechanismOperationTOList = new ArrayList<MechanismOperationTO>();
				for(int i=0; i<objectList.size(); ++i){					
					BigDecimal zero = new BigDecimal(0);
					MechanismOperationTO objMechanismOperationTO = new MechanismOperationTO();
					Object[] objResults = (Object[])objectList.get(i);

					if(objResults[2]!=null){
						objMechanismOperationTO.setAccountHolderNumber((Integer)objResults[2]);
					}
					if(objResults[3]!=null){
						objMechanismOperationTO.setNameHolder((String)objResults[3]);
					}
					if(objResults[4]!=null){
						objMechanismOperationTO.setFirstLastNameHolder((String)objResults[4]);
					}
					if(objResults[5]!=null){
						objMechanismOperationTO.setSecondLastNameHolder((String)objResults[5]);
					}
					if(objResults[6]!=null){
						objMechanismOperationTO.setFullNameHolder((String)objResults[6]);
					}else{
						objMechanismOperationTO.setFullNameHolder(GeneralConstants.DASH);
					}

					if(objResults[0]!=null){
						objMechanismOperationTO.setMnemonicParticipant((String)objResults[0]);
					}else{
						objMechanismOperationTO.setMnemonicParticipant(GeneralConstants.DASH);
					}
					if(objResults[1]!=null){
						objMechanismOperationTO.setIdParticipant((Long)objResults[1]);
						if(objMechanismOperationTO.getMnemonicParticipant()!=null){
							objMechanismOperationTO.setParticipant(objMechanismOperationTO.getMnemonicParticipant() + GeneralConstants.DASH + objMechanismOperationTO.getIdParticipant().toString());
						}
					}else{
						objMechanismOperationTO.setIdParticipant(zero.longValue());
					}

					if(objResults[9]!=null){
						objMechanismOperationTO.setIdSecurityCodePk((String)objResults[9]);
					}else{
						objMechanismOperationTO.setIdSecurityCodePk(GeneralConstants.DASH);
					}

					if(objResults[7]!=null){
						objMechanismOperationTO.setOperationState((Integer)objResults[7]);
						//return description of modality
						ParameterTableTO filterParameterTable = new ParameterTableTO();
						filterParameterTable.setParameterTablePk(MechanismOperationStateType.CASH_SETTLED.getCode());
						filterParameterTable.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
//						List<ParameterTable> lstModalityPT= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
//						if(lstModalityPT.size()>0){
//							ParameterTable objPT= lstModalityPT.get(0);
//							objMO.setModalityOperation(objPT.getDescription());
//						}
					}else{
						objMechanismOperationTO.setOperationState(zero.intValue());
					}
					if(objResults[8]!=null){
						objMechanismOperationTO.setOperationNumber((Long)objResults[8]);
					}else{
						objMechanismOperationTO.setOperationNumber(zero.longValue());
					}
					if(objResults[10]!=null){
						objMechanismOperationTO.setStockQuantity((BigDecimal)objResults[10]);
					}else{
						objMechanismOperationTO.setStockQuantity(zero);
					}
					if(objResults[11]!=null){
						objMechanismOperationTO.setOperationDate((Date)objResults[11]);
					}
					if(objResults[12]!=null){
						objMechanismOperationTO.setTermSettlementDate((Date)objResults[12]);
					}
					if(objResults[13]!=null){
						objMechanismOperationTO.setTermReportedOperation((Double)objResults[13]);
						//convert to int setTermReportedOperation
						objMechanismOperationTO.setTermInDays(objMechanismOperationTO.getTermReportedOperation().intValue());
					}else{
						objMechanismOperationTO.setTermReportedOperation(zero.doubleValue());
					}

					if(objResults[14]!=null){
						objMechanismOperationTO.setIdHolderAccount((Long)objResults[14]);
					}
					if(objResults[15]!=null){
						objMechanismOperationTO.setIdMechanismOperationPk((Long)objResults[15]);
					}
					if(objResults[16]!=null){
						objMechanismOperationTO.setIdHolder((Long)objResults[16]);
					}
					if(objResults[17]!=null){
						objMechanismOperationTO.setIdHolderAccountOperation((Long)objResults[17]);
					}
					//add each record into the list
					mechanismOperationTOList.add(objMechanismOperationTO);
				}
			}
			
		} catch (NoResultException e) {
			return null;
		}
		return mechanismOperationTOList;
	}
	
	/*Bloqueo*/
	public List<UserAccountSession> getListUsersOfIssuerLook(Long idHolderAccountPk, Long idBlockRequestPk){
		List <String> idScurityCodePks=getListIdSecurityCodePk(idHolderAccountPk, idBlockRequestPk);
		String sbQuery="select UA.ID_USER_PK,UA.ID_EMAIL,UA.PHONE_NUMBER,UA.FULL_NAME  from SECURITY.USER_ACCOUNT UA "
				+ " where UA.STATE = 30 and UA.SITUATION = 145 and UA.ID_PARTICIPANT_FK=(select ID_PARTICIPANT_PK from holder_account_balance where ID_HOLDER_ACCOUNT_PK="+idHolderAccountPk+" and TOTAL_BALANCE>0 and ID_SECURITY_CODE_PK IN( :idScurityCodePks ))";
		Query queryJpql = em.createNativeQuery(sbQuery.toString());
		queryJpql.setParameter("idScurityCodePks", idScurityCodePks);
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)queryJpql.getResultList();
			if(objectData.size()==0){
				objectData = null;
				List<BigDecimal> idPkParticipants=getIdParticipantsForLook(idHolderAccountPk, idBlockRequestPk);
				sbQuery="select UA.ID_USER_PK,UA.ID_EMAIL,UA.PHONE_NUMBER,UA.FULL_NAME  from SECURITY.USER_ACCOUNT UA "
					+ " where UA.STATE = 30 and UA.SITUATION = 145 and UA.ID_ISSUER_FK=(select ID_ISSUER_FK from PARTICIPANT where ID_PARTICIPANT_PK IN( :idPkParticipants ))";
				queryJpql = em.createNativeQuery(sbQuery.toString());
				queryJpql.setParameter("idPkParticipants", idPkParticipants);
				objectData = (List<Object[]>)queryJpql.getResultList();
			}
			UserAccountSession accountSession;
			List<UserAccountSession> list = new ArrayList<UserAccountSession>();
			for(Object[] objet: objectData){
				accountSession = new UserAccountSession();
				accountSession.setIdUserAccountPk(Integer.parseInt(objet[0].toString()));
				accountSession.setEmail(String.valueOf(objet[1]));
				accountSession.setPhoneNumber(String.valueOf(objet[2]));
				accountSession.setFullName(String.valueOf(objet[3]));
				list.add(accountSession);
			}
			return list;
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public ProcessNotification getProcessNotification(Long idProcessNotificationPk ){
		Object[] objects=null;
		try {
			String sbQuery="select NOTIFICATION_SUBJECT,NOTIFICATION_MESSAGE from PROCESS_NOTIFICATION where ID_BUSINESS_PROCESS_FK="+idProcessNotificationPk+" and NOTIFICATION_TYPE=1445 and DESTINATION_TYPE=1441 and INSTITUTION_TYPE=21 and IND_STATUS=1 and PRIORITY=1";
			Query queryJpql = em.createNativeQuery(sbQuery.toString());
			//queryJpql.setParameter("idProcessNotificationPk", idProcessNotificationPk);
			objects = (Object[]) queryJpql.getSingleResult();
			ProcessNotification notification=new ProcessNotification();
			notification.setNotificationSubject(objects[0].toString());
			notification.setNotificationMessage(objects[1].toString());
			return notification;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*Bloqueo*/
	public List<UserAccountSession> getListUsersOfIssuerUnBlock(Long idHolderAccountPk, Long idBlockRequestPk){
		List <String> idScurityCodePks=getListIdSecurityCodePk(idHolderAccountPk, idBlockRequestPk);
		String sbQuery="select UA.ID_USER_PK,UA.ID_EMAIL,UA.PHONE_NUMBER,UA.FULL_NAME  from SECURITY.USER_ACCOUNT UA "
				+ " where UA.STATE = 30 and UA.SITUATION = 145 and UA.ID_PARTICIPANT_FK=(select ID_PARTICIPANT_PK from holder_account_balance where ID_HOLDER_ACCOUNT_PK="+idHolderAccountPk+" and TOTAL_BALANCE>0 and ID_SECURITY_CODE_PK IN( :idScurityCodePks ))";
		Query queryJpql = em.createNativeQuery(sbQuery.toString());
		queryJpql.setParameter("idScurityCodePks", idScurityCodePks);
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)queryJpql.getResultList();
			if(objectData.size()==0){
				objectData = null;
				List<BigDecimal> idPkParticipants=getIdParticipantsForLook(idHolderAccountPk, idBlockRequestPk);
				sbQuery="select UA.ID_USER_PK,UA.ID_EMAIL,UA.PHONE_NUMBER,UA.FULL_NAME  from SECURITY.USER_ACCOUNT UA "
						+ " where UA.STATE = 30 and UA.SITUATION = 145 and UA.ID_ISSUER_FK=(select ID_ISSUER_FK from PARTICIPANT where ID_PARTICIPANT_PK IN( :idPkParticipants ))";
				queryJpql = em.createNativeQuery(sbQuery.toString());
				queryJpql.setParameter("idPkParticipants", idPkParticipants);
				objectData = (List<Object[]>)queryJpql.getResultList();
			}
			UserAccountSession accountSession;
			List<UserAccountSession> list = new ArrayList<UserAccountSession>();
			for(Object[] objet: objectData){
				accountSession = new UserAccountSession();
				accountSession.setIdUserAccountPk(Integer.parseInt(objet[0].toString()));
				accountSession.setEmail(String.valueOf(objet[1]));
				accountSession.setPhoneNumber(String.valueOf(objet[2]));
				accountSession.setFullName(String.valueOf(objet[3]));
				list.add(accountSession);
			}
			return list;
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return null;
	}
	/*Desbloqueo*/
	public List<UserAccountSession> getListUsersOfIssuerUnLock(Long idUnBlockRequestPk){
		List<UserAccountSession> accountSessions = new ArrayList<>();
		List<Long>idBlockRequestPks=this.getIdBlockOperationtPks(idUnBlockRequestPk);
		Long idHolderAccountPk=null;
		for (Long idBlockOperationFk : idBlockRequestPks) {
			idHolderAccountPk=this.getIdHolderAccountPk(idBlockOperationFk);
			accountSessions.addAll(this.getListUsersDatesOfIssuerUnLock(idHolderAccountPk,idBlockOperationFk));
		}
		return accountSessions;
	}
	public List<UserAccountSession> getListUsersDatesOfIssuerUnLock(Long idHolderAccountPk,Long idBlockOperationtPk){
		String sbQuery="select UA.ID_USER_PK,UA.ID_EMAIL,UA.PHONE_NUMBER,UA.FULL_NAME  "
				+ " from SECURITY.USER_ACCOUNT UA "
				+ " where UA.STATE = 30 and UA.SITUATION = 145 and UA.ID_PARTICIPANT_FK="
				+ " (select ID_PARTICIPANT_PK from HOLDER_ACCOUNT_BALANCE where ID_HOLDER_ACCOUNT_PK="+idHolderAccountPk+" and TOTAL_BALANCE>0 and ID_SECURITY_CODE_PK="
				+ " (Select ID_SECURITY_CODE_FK from BLOCK_OPERATION where ID_BLOCK_REQUEST_FK="
				+ " (select ID_BLOCK_REQUEST_FK from BLOCK_OPERATION where ID_BLOCK_OPERATION_PK="+idBlockOperationtPk+")))";
		Query queryJpql = em.createNativeQuery(sbQuery.toString());
		try{
			List<Object[]> objectData = (List<Object[]>)queryJpql.getResultList();
			UserAccountSession accountSession;
			List<UserAccountSession> list = new ArrayList<UserAccountSession>();
			for(Object[] objet: objectData){
				accountSession = new UserAccountSession();
				accountSession.setIdUserAccountPk(Integer.parseInt(objet[0].toString()));
				accountSession.setEmail(String.valueOf(objet[1]));
				accountSession.setPhoneNumber(String.valueOf(objet[2]));
				//accountSession.setFullName(String.valueOf(objet[3]));
				list.add(accountSession);
			}
			return list;
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return null;
	}
	public List<BigDecimal> getIdParticipantsForLook(Long idHolderAccountPk, Long idBlockRequestPk){
		List<String> idSecurityCodepks=getListIdSecurityCodePk(idHolderAccountPk,idBlockRequestPk);
		String sbQuery="select ID_PARTICIPANT_PK from holder_account_balance where ID_HOLDER_ACCOUNT_PK="+idHolderAccountPk+" and TOTAL_BALANCE>0 and ID_SECURITY_CODE_PK  IN( :idSecurityCodepks )";
		Query queryJpql = em.createNativeQuery(sbQuery.toString());
		queryJpql.setParameter("idSecurityCodepks", idSecurityCodepks);
		List<BigDecimal> list = queryJpql.getResultList();
		return list;
	}
	public List<String> getListIdSecurityCodePk(Long idHolderAccountPk, Long idBlockRequestPk){
		List<String> idSecurityCodePks=new ArrayList<>();
		String sbQuery="select ID_SECURITY_CODE_FK from BLOCK_OPERATION where ID_HOLDER_ACCOUNT_FK="+ idHolderAccountPk+" and ID_BLOCK_REQUEST_FK="+idBlockRequestPk;
		Query query = em.createNativeQuery(sbQuery.toString());
		idSecurityCodePks= query.getResultList();
		return idSecurityCodePks;
	}
	public  List<Long> getIdBlockOperationtPks(Long idUnBlockRequestPk){
		List<Long> idBlockRequestPks=new ArrayList<>();
		String sbQuery="select ID_BLOCK_OPERATION_FK from UNBLOCK_OPERATION where ID_UNBLOCK_REQUEST_FK="+idUnBlockRequestPk;
		Query query = em.createNativeQuery(sbQuery.toString());
        List<BigDecimal> list= query.getResultList();
        for (BigDecimal bigDecimal : list) {
        	idBlockRequestPks.add(bigDecimal.longValue());
		}
		return idBlockRequestPks;
	}
	public  Long getIdHolderAccountPk(Long idBlockOperationFk){
		String sbQuery="select ID_HOLDER_ACCOUNT_FK from BLOCK_OPERATION where ID_BLOCK_OPERATION_PK="+idBlockOperationFk;
		Query query = em.createNativeQuery(sbQuery.toString());
        BigDecimal idHolderAccountFk= (BigDecimal) query.getSingleResult();
		return idHolderAccountFk.longValue();
	}
	//select ID_PARTICIPANT_PK from holder_account_balance where TOTAL_BALANCE>0 and ID_SECURITY_CODE_PK=(select ID_SECURITY_CODE_FK from BLOCK_OPERATION where ID_BLOCK_REQUEST_FK=117);
	public ResultSearchTO obtainDataForMessages(Long idHolderAccountPk,Long idBlockOperationPk){
		String sbQuery="select ID_PARTICIPANT_FK, (select MNEMONIC from PARTICIPANT "
				+ " where ID_PARTICIPANT_PK=ID_PARTICIPANT_FK) as MNEMONIC,"
				+ " (select ACCOUNT_NUMBER from HOLDER_ACCOUNT where ID_HOLDER_ACCOUNT_PK=ID_HOLDER_ACCOUNT_FK) as ACCOUNT_NUMBER,"
				+ " ID_SECURITY_CODE_FK from BLOCK_OPERATION where ID_BLOCK_OPERATION_PK="+idBlockOperationPk;
		Query query = em.createNativeQuery(sbQuery.toString());
		 List<Object[]> list= query.getResultList();
		 ResultSearchTO resultSearchTO = null;
	    for (Object[] object : list) {
			resultSearchTO=new ResultSearchTO();
			resultSearchTO.setIdParticipantPk(((BigDecimal)(object[0])).longValue());
			resultSearchTO.setmNemonicParticipant(object[1].toString());
			resultSearchTO.setAccountNumber(object[2].toString());
			resultSearchTO.setIdSecurityCodePk(object[3].toString());
		}
	    return resultSearchTO;
	}
}