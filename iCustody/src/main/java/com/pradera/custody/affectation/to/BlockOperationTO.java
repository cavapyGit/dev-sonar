package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class BlockOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id block operation pk. */
	private Long idBlockOperationPk;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The holder description. */
	private String holderDescription;	
	
	/** The alternative code. */
	private String alternativeCode;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The isin description. */
	private String isinDescription;
	
	/** The id issuer pk. */
	private String idIssuerPk;
	
	/** The issuer description. */
	private String issuerDescription;
	
	/** The level affectation. */
	private String levelAffectation;
	
	/** The id level affectation. */
	private Integer idLevelAffectation;	
	
	/** The type affectation. */
	private String typeAffectation;
	
	/** The id type affectation. */
	private Integer idTypeAffectation;
	
	/** The state affectation. */
	private String stateAffectation;
	
	/** The id state affectation. */
	private Integer idStateAffectation;
	
	/** The isin class. */
	private String isinClass;		
	
	/** The instrument type. */
	private String instrumentType;
	
	/** The instrument type code. */
	private Integer instrumentTypeCode;
	
	/** The nominal val. */
	private BigDecimal nominalVal;	
	
	/** The lst benefits. */
	private List<ParameterTable> lstBenefits;
	
	/** The amount. */
	private BigDecimal amount;
	
	/** The actual amount. */
	private BigDecimal actualAmount;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The security code pk. */
	private String securityCodePk;
	
	/** The ind early redemption. */
	private Integer indEarlyRedemption = ComponentConstant.ZERO;
	
	/** The market fact operation list. */
	private List<BlockMarketFactOperation> marketFactOperationList;
	
	/** The holders name. */
	private String holdersName;
	
	/**
	 * Gets the alternative code.
	 *
	 * @return the alternative code
	 */
	public String getAlternativeCode() {
		return alternativeCode;
	}
	
	/**
	 * Sets the alternative code.
	 *
	 * @param alternativeCode the new alternative code
	 */
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the level affectation.
	 *
	 * @return the level affectation
	 */
	public String getLevelAffectation() {
		return levelAffectation;
	}
	
	/**
	 * Sets the level affectation.
	 *
	 * @param levelAffectation the new level affectation
	 */
	public void setLevelAffectation(String levelAffectation) {
		this.levelAffectation = levelAffectation;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the isin description.
	 *
	 * @return the isin description
	 */
	public String getIsinDescription() {
		return isinDescription;
	}
	
	/**
	 * Sets the isin description.
	 *
	 * @param isinDescription the new isin description
	 */
	public void setIsinDescription(String isinDescription) {
		this.isinDescription = isinDescription;
	}
	
	/**
	 * Gets the lst benefits.
	 *
	 * @return the lst benefits
	 */
	public List<ParameterTable> getLstBenefits() {
		return lstBenefits;
	}
	
	/**
	 * Sets the lst benefits.
	 *
	 * @param lstBenefits the new lst benefits
	 */
	public void setLstBenefits(List<ParameterTable> lstBenefits) {
		this.lstBenefits = lstBenefits;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public String getInstrumentType() {
		return instrumentType;
	}
	
	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}
	
	/**
	 * Gets the nominal val.
	 *
	 * @return the nominal val
	 */
	public BigDecimal getNominalVal() {
		return nominalVal;
	}
	
	/**
	 * Sets the nominal val.
	 *
	 * @param nominalVal the new nominal val
	 */
	public void setNominalVal(BigDecimal nominalVal) {
		this.nominalVal = nominalVal;
	}	
	
	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}
	
	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}
	
	/**
	 * Gets the isin class.
	 *
	 * @return the isin class
	 */
	public String getIsinClass() {
		return isinClass;
	}
	
	/**
	 * Sets the isin class.
	 *
	 * @param isinClass the new isin class
	 */
	public void setIsinClass(String isinClass) {
		this.isinClass = isinClass;
	}
	
	/**
	 * Gets the id issuer pk.
	 *
	 * @return the id issuer pk
	 */
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	
	/**
	 * Sets the id issuer pk.
	 *
	 * @param idIssuerPk the new id issuer pk
	 */
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	
	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}
	
	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}
	
	/**
	 * Gets the id block operation pk.
	 *
	 * @return the id block operation pk
	 */
	public Long getIdBlockOperationPk() {
		return idBlockOperationPk;
	}
	
	/**
	 * Sets the id block operation pk.
	 *
	 * @param idBlockOperationPk the new id block operation pk
	 */
	public void setIdBlockOperationPk(Long idBlockOperationPk) {
		this.idBlockOperationPk = idBlockOperationPk;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}	
	
	/**
	 * Gets the id level affectation.
	 *
	 * @return the id level affectation
	 */
	public Integer getIdLevelAffectation() {
		return idLevelAffectation;
	}
	
	/**
	 * Sets the id level affectation.
	 *
	 * @param idLevelAffectation the new id level affectation
	 */
	public void setIdLevelAffectation(Integer idLevelAffectation) {
		this.idLevelAffectation = idLevelAffectation;
	}
	
	/**
	 * Gets the type affectation.
	 *
	 * @return the type affectation
	 */
	public String getTypeAffectation() {
		return typeAffectation;
	}
	
	/**
	 * Sets the type affectation.
	 *
	 * @param typeAffectation the new type affectation
	 */
	public void setTypeAffectation(String typeAffectation) {
		this.typeAffectation = typeAffectation;
	}
	
	/**
	 * Gets the id type affectation.
	 *
	 * @return the id type affectation
	 */
	public Integer getIdTypeAffectation() {
		return idTypeAffectation;
	}
	
	/**
	 * Sets the id type affectation.
	 *
	 * @param idTypeAffectation the new id type affectation
	 */
	public void setIdTypeAffectation(Integer idTypeAffectation) {
		this.idTypeAffectation = idTypeAffectation;
	}
	
	/**
	 * Gets the state affectation.
	 *
	 * @return the state affectation
	 */
	public String getStateAffectation() {
		return stateAffectation;
	}
	
	/**
	 * Sets the state affectation.
	 *
	 * @param stateAffectation the new state affectation
	 */
	public void setStateAffectation(String stateAffectation) {
		this.stateAffectation = stateAffectation;
	}
	
	/**
	 * Gets the id state affectation.
	 *
	 * @return the id state affectation
	 */
	public Integer getIdStateAffectation() {
		return idStateAffectation;
	}
	
	/**
	 * Sets the id state affectation.
	 *
	 * @param idStateAffectation the new id state affectation
	 */
	public void setIdStateAffectation(Integer idStateAffectation) {
		this.idStateAffectation = idStateAffectation;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the actual amount.
	 *
	 * @return the actual amount
	 */
	public BigDecimal getActualAmount() {
		return actualAmount;
	}
	
	/**
	 * Sets the actual amount.
	 *
	 * @param actualAmount the new actual amount
	 */
	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}
	
	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}
	
	/**
	 * Gets the security code pk.
	 *
	 * @return the security code pk
	 */
	public String getSecurityCodePk() {
		return securityCodePk;
	}
	
	/**
	 * Sets the security code pk.
	 *
	 * @param securityCodePk the new security code pk
	 */
	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}
	
	/**
	 * Gets the market fact operation list.
	 *
	 * @return the market fact operation list
	 */
	public List<BlockMarketFactOperation> getMarketFactOperationList() {
		return marketFactOperationList;
	}
	
	/**
	 * Sets the market fact operation list.
	 *
	 * @param marketFactOperationList the new market fact operation list
	 */
	public void setMarketFactOperationList(
			List<BlockMarketFactOperation> marketFactOperationList) {
		this.marketFactOperationList = marketFactOperationList;
	}
	
	/**
	 * Gets the instrument type code.
	 *
	 * @return the instrument type code
	 */
	public Integer getInstrumentTypeCode() {
		return instrumentTypeCode;
	}
	
	/**
	 * Sets the instrument type code.
	 *
	 * @param instrumentTypeCode the new instrument type code
	 */
	public void setInstrumentTypeCode(Integer instrumentTypeCode) {
		this.instrumentTypeCode = instrumentTypeCode;
	}
	
	/**
	 * Gets the ind early redemption.
	 *
	 * @return the ind early redemption
	 */
	public Integer getIndEarlyRedemption() {
		return indEarlyRedemption;
	}
	
	/**
	 * Sets the ind early redemption.
	 *
	 * @param indEarlyRedemption the new ind early redemption
	 */
	public void setIndEarlyRedemption(Integer indEarlyRedemption) {
		this.indEarlyRedemption = indEarlyRedemption;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * @return the holdersName
	 */
	public String getHoldersName() {
		return holdersName;
	}

	/**
	 * @param holdersName the holdersName to set
	 */
	public void setHoldersName(String holdersName) {
		this.holdersName = holdersName;
	}
}