package com.pradera.custody.affectation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ActiveReblockBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
@BatchProcess(name = "ActiveReblockBatch")
@RequestScoped
public class ActiveReblockBatch implements JobExecution{

	/** The holder account balance to. */
	private HolderAccountBalanceTO holderAccountBalanceTO;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The bean service. */
	@EJB
	private RequestAffectationServiceBean beanService;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The logger. */
	@Inject
	private transient PraderaLogger logger;

		
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();		
		holderAccountBalanceTO = new HolderAccountBalanceTO();
		for(ProcessLoggerDetail processLoggerDetail:processLoggerDetails){
			if(ComponentConstant.HOLDER_ACCOUNT.equals(processLoggerDetail.getParameterName())){
				holderAccountBalanceTO.setIdHolderAccount(new Long(processLoggerDetail.getParameterValue()));
			}else if(ComponentConstant.SECURITIES.equals(processLoggerDetail.getParameterName())){
				holderAccountBalanceTO.setIdSecurityCode(processLoggerDetail.getParameterValue());
			}else if(ComponentConstant.PARTICIPANT.equals(processLoggerDetail.getParameterName())){
				holderAccountBalanceTO.setIdParticipant(new Long(processLoggerDetail.getParameterValue()));
			}
		}		
		
		try {
			//Recolectamos todos los rebloqueos de la cuenta	
			List<Long> lstReblocks = beanService.collectReblocks(holderAccountBalanceTO);
			HolderAccountBalancePK idHolAccBal= new HolderAccountBalancePK();
			idHolAccBal.setIdHolderAccountPk(holderAccountBalanceTO.getIdHolderAccount());
			idHolAccBal.setIdSecurityCodePk(holderAccountBalanceTO.getIdSecurityCode());
			idHolAccBal.setIdParticipantPk(holderAccountBalanceTO.getIdParticipant());
			//traemos el saldo disponible
			BigDecimal availableBalance = beanService.findAvailableBalance(idHolAccBal);
			for(Long lngIdBlockOperationPk:lstReblocks){
				if(availableBalance.compareTo(BigDecimal.ZERO) == 1)
					activeReblocks(lngIdBlockOperationPk);
			}
		} catch (ServiceException e) {			
			e.printStackTrace();
			logger.error("activeReblocks");
		}		
	}

	/**
	 * Active reblocks.
	 *
	 * @param lngIdBlockOperationPk the lng id block operation pk
	 */
	private void activeReblocks(Long lngIdBlockOperationPk) {
		try{
			BlockOperation reblockOpe = crudDaoServiceBean.find(BlockOperation.class, lngIdBlockOperationPk);
			HolderAccountBalancePK idHolAccBal= new HolderAccountBalancePK();
			idHolAccBal.setIdHolderAccountPk(holderAccountBalanceTO.getIdHolderAccount());
			idHolAccBal.setIdSecurityCodePk(holderAccountBalanceTO.getIdSecurityCode());
			idHolAccBal.setIdParticipantPk(holderAccountBalanceTO.getIdParticipant());
			//traemos el saldo disponible
			BigDecimal availableBalance = beanService.findAvailableBalance(idHolAccBal);
			/** 
			 * Verificamos que la cantidad actual sea mayor a 0
			 * y el saldo disponible sea mayor a 0
			 **/
			if(reblockOpe.getActualBlockBalance().compareTo(BigDecimal.ZERO) == 1 
					&& availableBalance.compareTo(BigDecimal.ZERO) == 1){				
				//Se crea un nuevo bloqueo
				BlockOperation objBlockOperation = new BlockOperation();
				CustodyOperation objCustodyOperation = new CustodyOperation();
				//Se coloca la referencia del rebloqueo que genera este bloqueo
				objBlockOperation.setBlockOperationRef(reblockOpe);	
				objCustodyOperation.setOperationDate(CommonsUtilities.currentDate());
				objCustodyOperation.setRegistryDate(CommonsUtilities.currentDate());
				objCustodyOperation.setConfirmDate(CommonsUtilities.currentDate());
				objCustodyOperation.setState(AffectationStateType.BLOCKED.getCode());
				objCustodyOperation.setRegistryUser(reblockOpe.getCustodyOperation().getRegistryUser());
				objCustodyOperation.setConfirmUser(reblockOpe.getCustodyOperation().getConfirmUser());
				//Campos de auditoria
				objBlockOperation.setLastModifyUser(reblockOpe.getLastModifyUser());
				objBlockOperation.setLastModifyIp(reblockOpe.getLastModifyIp());
				objBlockOperation.setLastModifyDate(CommonsUtilities.currentDate());					
				objCustodyOperation.setLastModifyUser(reblockOpe.getCustodyOperation().getLastModifyUser());
				objCustodyOperation.setLastModifyIp(reblockOpe.getCustodyOperation().getLastModifyIp());
				objCustodyOperation.setLastModifyDate(CommonsUtilities.currentDate());					
				objBlockOperation.setLastModifyApp(reblockOpe.getLastModifyApp());					
				objCustodyOperation.setLastModifyApp(reblockOpe.getCustodyOperation().getLastModifyApp());
				objBlockOperation.setBlockState(AffectationStateType.BLOCKED.getCode());
				objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());					
				//El blockrequest para este nuevo blockoperation sera el mismo q el padre (rebloqueo)
				objBlockOperation.setBlockRequest(reblockOpe.getBlockRequest());
				//Segun el tipo de operacion que tenia el rebloqueo sera para este bloqueo
				if(ParameterOperationType.SECURITIES_RESERVE_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType()))
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_BLOCK.getCode());
				else if(ParameterOperationType.SECURITIES_PAWN_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType()))
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_BLOCK.getCode());
				else if(ParameterOperationType.SECURITIES_BAN_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType()))
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_BLOCK.getCode());
				else if(ParameterOperationType.SECURITIES_OTHERS_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType()))
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_BLOCK.getCode());
				else if(ParameterOperationType.SECURITIES_OPPOSITION_REBLOCK.getCode().equals(reblockOpe.getCustodyOperation().getOperationType()))
					objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_BLOCK.getCode());						
				objBlockOperation.setBlockLevel(LevelAffectationType.BLOCK.getCode());
				//Se coloca los beneficios que tenia el rebloqueo al nuevo bloqueo
				fillBenefits(reblockOpe, objBlockOperation);					
				//Se coloca la matriz
				objBlockOperation.setParticipant(reblockOpe.getParticipant());
				objBlockOperation.setHolderAccount(reblockOpe.getHolderAccount());
				objBlockOperation.setSecurities(reblockOpe.getSecurities());
				//Si la cantidad actual del rebloqueo es mayor al saldo disponible de la cuenta				
				if(reblockOpe.getActualBlockBalance().compareTo(availableBalance) == 1){
					objBlockOperation.setActualBlockBalance(availableBalance);
					objBlockOperation.setOriginalBlockBalance(availableBalance);
					reblockOpe.setActualBlockBalance(reblockOpe.getActualBlockBalance().subtract(availableBalance));
					reblockOpe.getBlockRequest().setCurrentBlockAmount(reblockOpe.getBlockRequest().getCurrentBlockAmount().add(availableBalance));
				}else{//Si la cantidad actual del rebloqueo es menor o igual al saldo disponible de la cuenta
					objBlockOperation.setActualBlockBalance(reblockOpe.getActualBlockBalance());
					objBlockOperation.setOriginalBlockBalance(reblockOpe.getActualBlockBalance());
					reblockOpe.setActualBlockBalance(BigDecimal.ZERO);
					reblockOpe.setBlockState(AffectationStateType.UNBLOCKED.getCode());
					reblockOpe.getBlockRequest().setCurrentBlockAmount(reblockOpe.getBlockRequest().getCurrentBlockAmount().add(objBlockOperation.getActualBlockBalance()));					
				}
				objBlockOperation.setCustodyOperation(objCustodyOperation);
				crudDaoServiceBean.create(objBlockOperation);
				crudDaoServiceBean.update(reblockOpe);
				objBlockOperation.setDocumentNumber(objBlockOperation.getIdBlockOperationPk());
				objBlockOperation.getCustodyOperation().setOperationNumber(objBlockOperation.getIdBlockOperationPk());
				crudDaoServiceBean.update(objBlockOperation);										
				List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(objBlockOperation);
				/**SE GENERA EL PRIMER MOVIMIENTO DEL BLOQUEO DE DISPONIBLE A TRANSITO**/
				accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOperation.getIdBlockOperationPk(),objBlockOperation.getBlockOperationRef().getIdBlockOperationPk(),
																						 BusinessProcessType.SECURITY_BLOCK_REGISTER.getCode(), 
																						 accountBalanceTOs,objBlockOperation.getCustodyOperation().getOperationType()));
				/**SE GENERA EL SEGUNDO MOVIMIENTO DEL BLOQUEO DE TRANSITO A BLOQUEO**/
				accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objBlockOperation.getIdBlockOperationPk(),objBlockOperation.getBlockOperationRef().getIdBlockOperationPk(),
																						 BusinessProcessType.SECURITY_BLOCK_CONFIRM.getCode(), 
																						 accountBalanceTOs,objBlockOperation.getCustodyOperation().getOperationType()));
			}		
		}catch (ServiceException e) {
			logger.error("activeReblocks");
		}
	}
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param objBlockOperation the obj block operation
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> getHolderAccountBalanceTOs(BlockOperation objBlockOperation) throws ServiceException{
		List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<com.pradera.integration.component.business.to.HolderAccountBalanceTO>();
		com.pradera.integration.component.business.to.HolderAccountBalanceTO holderAccountBalanceTO = new com.pradera.integration.component.business.to.HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(objBlockOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(objBlockOperation.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getSecurities().getIdIsinCode());
		holderAccountBalanceTO.setStockQuantity(objBlockOperation.getOriginalBlockBalance());
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param idCustodyOperationRef the id custody operation ref
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long idCustodyOperationRef, Long businessProcessType, List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs, Long operationType){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);	
		objAccountComponent.setIdRefCustodyOperation(idCustodyOperationRef);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		return objAccountComponent;
	}	

	/**
	 * Fill benefits.
	 *
	 * @param reblockOpe the reblock ope
	 * @param objBlockOperation the obj block operation
	 */
	private void fillBenefits(BlockOperation reblockOpe, BlockOperation objBlockOperation){
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndRescue()))
			objBlockOperation.setIndRescue(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndRescue(BooleanType.NO.getCode());
		
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndAmortization()))
			objBlockOperation.setIndAmortization(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndAmortization(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndCashDividend()))
			objBlockOperation.setIndCashDividend(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndCashDividend(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndInterest()))
			objBlockOperation.setIndInterest(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndInterest(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndReturnContributions()))
			objBlockOperation.setIndReturnContributions(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndReturnContributions(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndStockDividend()))
			objBlockOperation.setIndStockDividend(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndStockDividend(BooleanType.NO.getCode());
	
		if(BooleanType.YES.getCode().equals(reblockOpe.getIndSuscription()))
			objBlockOperation.setIndSuscription(BooleanType.YES.getCode());
		else
			objBlockOperation.setIndSuscription(BooleanType.NO.getCode());
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
