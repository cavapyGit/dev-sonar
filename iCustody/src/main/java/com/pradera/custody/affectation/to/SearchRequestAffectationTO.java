package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;


// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class SearchRequestAffectationTO.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/
public class SearchRequestAffectationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The lst pt type affectation. */
	private List<ParameterTable> lstPTState, lstPTLevelAffectation, lstPTTypeAffectation, lstMotiveReject;	
	
	/** The parameter table type affectation. */
	private ParameterTable parameterTableState, parameterTableLevelAffectation, parameterTableTypeAffectation;
	
	/** The holder. */
	private Holder holder;
	
	/** The entity block. */
	private BlockEntity blockEntity;
	
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The number record. */
	private String numberRecord;
	
	/** The id block request pk. */
	private Long idBlockRequestPk;
	
	/** The search type. */
	private Integer searchType;
	
	/** The bl default search. */
	private boolean blDefaultSearch;

	/** The id issuer. */
	private String idIssuer;
	
	/** The id participant. */
	private Long idParticipant;
	
	private List<Integer> lstParamState;
	
	/**
	 * Gets the lst pt state.
	 *
	 * @return the lst pt state
	 */
	public List<ParameterTable> getLstPTState() {
		return lstPTState;
	}
	
	/**
	 * Sets the lst pt state.
	 *
	 * @param lstPTState the new lst pt state
	 */
	public void setLstPTState(List<ParameterTable> lstPTState) {
		this.lstPTState = lstPTState;
	}	
	
	/**
	 * Gets the lst pt type affectation.
	 *
	 * @return the lst pt type affectation
	 */
	public List<ParameterTable> getLstPTTypeAffectation() {
		return lstPTTypeAffectation;
	}
	
	/**
	 * Sets the lst pt type affectation.
	 *
	 * @param lstPTTypeAffectation the new lst pt type affectation
	 */
	public void setLstPTTypeAffectation(List<ParameterTable> lstPTTypeAffectation) {
		this.lstPTTypeAffectation = lstPTTypeAffectation;
	}
	
		
	/**
	 * Gets the lst pt level affectation.
	 *
	 * @return the lst pt level affectation
	 */
	public List<ParameterTable> getLstPTLevelAffectation() {
		return lstPTLevelAffectation;
	}
	
	/**
	 * Sets the lst pt level affectation.
	 *
	 * @param lstPTLevelAffectation the new lst pt level affectation
	 */
	public void setLstPTLevelAffectation(List<ParameterTable> lstPTLevelAffectation) {
		this.lstPTLevelAffectation = lstPTLevelAffectation;
	}
	
	/**
	 * Gets the parameter table state.
	 *
	 * @return the parameter table state
	 */
	public ParameterTable getParameterTableState() {
		return parameterTableState;
	}
	
	/**
	 * Sets the parameter table state.
	 *
	 * @param parameterTableState the new parameter table state
	 */
	public void setParameterTableState(ParameterTable parameterTableState) {
		this.parameterTableState = parameterTableState;
	}
	
	/**
	 * Gets the parameter table level affectation.
	 *
	 * @return the parameter table level affectation
	 */
	public ParameterTable getParameterTableLevelAffectation() {
		return parameterTableLevelAffectation;
	}
	
	/**
	 * Sets the parameter table level affectation.
	 *
	 * @param parameterTableLevelAffectation the new parameter table level affectation
	 */
	public void setParameterTableLevelAffectation(
			ParameterTable parameterTableLevelAffectation) {
		this.parameterTableLevelAffectation = parameterTableLevelAffectation;
	}
	
	/**
	 * Gets the parameter table type affectation.
	 *
	 * @return the parameter table type affectation
	 */
	public ParameterTable getParameterTableTypeAffectation() {
		return parameterTableTypeAffectation;
	}
	
	/**
	 * Sets the parameter table type affectation.
	 *
	 * @param parameterTableTypeAffectation the new parameter table type affectation
	 */
	public void setParameterTableTypeAffectation(
			ParameterTable parameterTableTypeAffectation) {
		this.parameterTableTypeAffectation = parameterTableTypeAffectation;
	}	
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	
	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public BlockEntity getBlockEntity() {
		return blockEntity;
	}

	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}

	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}
	
	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the id block request pk.
	 *
	 * @return the id block request pk
	 */
	public Long getIdBlockRequestPk() {
		return idBlockRequestPk;
	}

	/**
	 * Sets the id block request pk.
	 *
	 * @param idBlockRequestPk the new id block request pk
	 */
	public void setIdBlockRequestPk(Long idBlockRequestPk) {
		this.idBlockRequestPk = idBlockRequestPk;
	}

	/**
	 * Gets the search type.
	 *
	 * @return the search type
	 */
	public Integer getSearchType() {
		return searchType;
	}

	/**
	 * Sets the search type.
	 *
	 * @param searchType the new search type
	 */
	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}

	/**
	 * Checks if is bl default search.
	 *
	 * @return true, if is bl default search
	 */
	public boolean isBlDefaultSearch() {
		return blDefaultSearch;
	}

	/**
	 * Sets the bl default search.
	 *
	 * @param blDefaultSearch the new bl default search
	 */
	public void setBlDefaultSearch(boolean blDefaultSearch) {
		this.blDefaultSearch = blDefaultSearch;
	}

	/**
	 * Gets the lst motive reject.
	 *
	 * @return the lst motive reject
	 */
	public List<ParameterTable> getLstMotiveReject() {
		return lstMotiveReject;
	}

	/**
	 * Sets the lst motive reject.
	 *
	 * @param lstMotiveReject the new lst motive reject
	 */
	public void setLstMotiveReject(List<ParameterTable> lstMotiveReject) {
		this.lstMotiveReject = lstMotiveReject;
	}

	/**
	 * Gets the id issuer.
	 *
	 * @return the id issuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}

	/**
	 * Sets the id issuer.
	 *
	 * @param idIssuer the new id issuer
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * @return the lstParamState
	 */
	public List<Integer> getLstParamState() {
		return lstParamState;
	}

	/**
	 * @param lstParamState the lstParamState to set
	 */
	public void setLstParamState(List<Integer> lstParamState) {
		this.lstParamState = lstParamState;
	}

}
