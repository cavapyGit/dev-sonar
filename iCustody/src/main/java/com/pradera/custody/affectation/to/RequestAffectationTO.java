package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class RequestAffectationTO.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/

public class RequestAffectationTO implements Serializable{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The business process type id. */
	private Long businessProcessTypeId;
	
	/** The number request. */
	private Long numberRequest;
	
	/** The holder description. */
	private String holderDescription;
	
	/** The entity block description. */
	private String entityBlockDescription;
	
	/** The number record. */
	private String numberRecord;
	
	/** The date record. */
	private Date dateRecord;
	
	/** The holder. */
	private Integer holder;	
	
	/** The register date. */
	private Date registerDate;
	
	/** The type affectation. */
	private String typeAffectation;
	
	/** The level affectation. */
	private String levelAffectation;
	
	/** The entity block. */
	private Integer entityBlock;
	
	/** The amount. */
	private String amount;
	
	/** The state. */
	private String state;
	
	/** The id state. */
	private Integer idState;
	
	/** The id type affectation. */
	private Integer idTypeAffectation;
	
	/** The ind request declaration. */
	private Integer indRequestDeclaration;
	
	/** The notification date. */
	private Date notificationDate;
	
	/** The confirmation user. */
	private String confirmationUser;
	
	/** The reject user. */
	private String rejectUser;
	
	/** The register user. */
	private String registerUser;
	
	/** The approve user. */
	private String approveUser;
	
	/** The annul user. */
	private String annulUser;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject motive other. */
	private String rejectMotiveOther;
	
	/** The id issuer. */
	private String idIssuer;
	
	/** The check auto settled. */
	private boolean checkAutoSettled;
	
	/** The check authority restricted. */
	private boolean checkAuthorityRestricted;
	
	/** The bl pawn block. */
	private boolean blPawnBlock;
	
	/** The bl ban block. */
	private boolean blBanBlock;
	
	/** The circular number. */
	//fiscal info
	private String circularNumber;
	
	/** The fiscal procedence. */
	private String fiscalProcedence;
	
	/** The fiscal currency. */
	private Integer fiscalCurrency;
	
	/** The fiscal amount. */
	private BigDecimal fiscalAmount;
	
	/** The fiscal charge number. */
	private String fiscalChargeNumber;
	
	/** The fiscal process. */
	private String fiscalProcess;
	
	/** The fiscal authority. */
	private String fiscalAuthority;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}

	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}

	/**
	 * Gets the number request.
	 *
	 * @return the number request
	 */
	public Long getNumberRequest() {
		return numberRequest;
	}

	/**
	 * Sets the number request.
	 *
	 * @param numberRequest the new number request
	 */
	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}

	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}

	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}

	/**
	 * Gets the entity block description.
	 *
	 * @return the entity block description
	 */
	public String getEntityBlockDescription() {
		return entityBlockDescription;
	}

	/**
	 * Sets the entity block description.
	 *
	 * @param entityBlockDescription the new entity block description
	 */
	public void setEntityBlockDescription(String entityBlockDescription) {
		this.entityBlockDescription = entityBlockDescription;
	}

	
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Integer getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Integer holder) {
		this.holder = holder;
	}
	
	
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the type affectation.
	 *
	 * @return the type affectation
	 */
	public String getTypeAffectation() {
		return typeAffectation;
	}
	
	/**
	 * Sets the type affectation.
	 *
	 * @param typeAffectation the new type affectation
	 */
	public void setTypeAffectation(String typeAffectation) {
		this.typeAffectation = typeAffectation;
	}
	
	/**
	 * Gets the level affectation.
	 *
	 * @return the level affectation
	 */
	public String getLevelAffectation() {
		return levelAffectation;
	}
	
	/**
	 * Sets the level affectation.
	 *
	 * @param levelAffectation the new level affectation
	 */
	public void setLevelAffectation(String levelAffectation) {
		this.levelAffectation = levelAffectation;
	}
	
	/**
	 * Gets the entity block.
	 *
	 * @return the entity block
	 */
	public Integer getEntityBlock() {
		return entityBlock;
	}
	
	/**
	 * Sets the entity block.
	 *
	 * @param entityBlock the new entity block
	 */
	public void setEntityBlock(Integer entityBlock) {
		this.entityBlock = entityBlock;
	}
	
		
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the id state.
	 *
	 * @return the id state
	 */
	public Integer getIdState() {
		return idState;
	}

	/**
	 * Sets the id state.
	 *
	 * @param idState the new id state
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}

	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}

	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the date record.
	 *
	 * @return the date record
	 */
	public Date getDateRecord() {
		return dateRecord;
	}

	/**
	 * Sets the date record.
	 *
	 * @param dateRecord the new date record
	 */
	public void setDateRecord(Date dateRecord) {
		this.dateRecord = dateRecord;
	}

	/**
	 * Gets the id type affectation.
	 *
	 * @return the id type affectation
	 */
	public Integer getIdTypeAffectation() {
		return idTypeAffectation;
	}

	/**
	 * Sets the id type affectation.
	 *
	 * @param idTypeAffectation the new id type affectation
	 */
	public void setIdTypeAffectation(Integer idTypeAffectation) {
		this.idTypeAffectation = idTypeAffectation;
	}

	/**
	 * Gets the ind request declaration.
	 *
	 * @return the ind request declaration
	 */
	public Integer getIndRequestDeclaration() {
		return indRequestDeclaration;
	}

	/**
	 * Sets the ind request declaration.
	 *
	 * @param indRequestDeclaration the new ind request declaration
	 */
	public void setIndRequestDeclaration(Integer indRequestDeclaration) {
		this.indRequestDeclaration = indRequestDeclaration;
	}

	/**
	 * Gets the notification date.
	 *
	 * @return the notification date
	 */
	public Date getNotificationDate() {
		return notificationDate;
	}

	/**
	 * Sets the notification date.
	 *
	 * @param notificationDate the new notification date
	 */
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	/**
	 * Gets the business process type id.
	 *
	 * @return the businessProcessTypeId
	 */
	public Long getBusinessProcessTypeId() {
		return businessProcessTypeId;
	}

	/**
	 * Sets the business process type id.
	 *
	 * @param businessProcessTypeId the businessProcessTypeId to set
	 */
	public void setBusinessProcessTypeId(Long businessProcessTypeId) {
		this.businessProcessTypeId = businessProcessTypeId;
	}

	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}

	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}

	/**
	 * Gets the annul user.
	 *
	 * @return the annul user
	 */
	public String getAnnulUser() {
		return annulUser;
	}

	/**
	 * Sets the annul user.
	 *
	 * @param annulUser the new annul user
	 */
	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}

	/**
	 * Gets the id issuer.
	 *
	 * @return the id issuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}

	/**
	 * Sets the id issuer.
	 *
	 * @param idIssuer the new id issuer
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Checks if is check auto settled.
	 *
	 * @return true, if is check auto settled
	 */
	public boolean isCheckAutoSettled() {
		return checkAutoSettled;
	}

	/**
	 * Sets the check auto settled.
	 *
	 * @param checkAutoSettled the new check auto settled
	 */
	public void setCheckAutoSettled(boolean checkAutoSettled) {
		this.checkAutoSettled = checkAutoSettled;
	}

	/**
	 * Checks if is check authority restricted.
	 *
	 * @return true, if is check authority restricted
	 */
	public boolean isCheckAuthorityRestricted() {
		return checkAuthorityRestricted;
	}

	/**
	 * Sets the check authority restricted.
	 *
	 * @param checkAuthorityRestricted the new check authority restricted
	 */
	public void setCheckAuthorityRestricted(boolean checkAuthorityRestricted) {
		this.checkAuthorityRestricted = checkAuthorityRestricted;
	}

	/**
	 * Checks if is bl pawn block.
	 *
	 * @return true, if is bl pawn block
	 */
	public boolean isBlPawnBlock() {
		return blPawnBlock;
	}

	/**
	 * Sets the bl pawn block.
	 *
	 * @param blPawnBlock the new bl pawn block
	 */
	public void setBlPawnBlock(boolean blPawnBlock) {
		this.blPawnBlock = blPawnBlock;
	}

	/**
	 * Checks if is bl ban block.
	 *
	 * @return true, if is bl ban block
	 */
	public boolean isBlBanBlock() {
		return blBanBlock;
	}

	/**
	 * Sets the bl ban block.
	 *
	 * @param blBanBlock the new bl ban block
	 */
	public void setBlBanBlock(boolean blBanBlock) {
		this.blBanBlock = blBanBlock;
	}

	/**
	 * Gets the circular number.
	 *
	 * @return the circular number
	 */
	public String getCircularNumber() {
		return circularNumber;
	}

	/**
	 * Sets the circular number.
	 *
	 * @param circularNumber the new circular number
	 */
	public void setCircularNumber(String circularNumber) {
		this.circularNumber = circularNumber;
	}

	/**
	 * Gets the fiscal procedence.
	 *
	 * @return the fiscal procedence
	 */
	public String getFiscalProcedence() {
		return fiscalProcedence;
	}

	/**
	 * Sets the fiscal procedence.
	 *
	 * @param fiscalProcedence the new fiscal procedence
	 */
	public void setFiscalProcedence(String fiscalProcedence) {
		this.fiscalProcedence = fiscalProcedence;
	}

	/**
	 * Gets the fiscal currency.
	 *
	 * @return the fiscal currency
	 */
	public Integer getFiscalCurrency() {
		return fiscalCurrency;
	}

	/**
	 * Sets the fiscal currency.
	 *
	 * @param fiscalCurrency the new fiscal currency
	 */
	public void setFiscalCurrency(Integer fiscalCurrency) {
		this.fiscalCurrency = fiscalCurrency;
	}

	/**
	 * Gets the fiscal amount.
	 *
	 * @return the fiscal amount
	 */
	public BigDecimal getFiscalAmount() {
		return fiscalAmount;
	}

	/**
	 * Sets the fiscal amount.
	 *
	 * @param fiscalAmount the new fiscal amount
	 */
	public void setFiscalAmount(BigDecimal fiscalAmount) {
		this.fiscalAmount = fiscalAmount;
	}

	/**
	 * Gets the fiscal charge number.
	 *
	 * @return the fiscal charge number
	 */
	public String getFiscalChargeNumber() {
		return fiscalChargeNumber;
	}

	/**
	 * Sets the fiscal charge number.
	 *
	 * @param fiscalChargeNumber the new fiscal charge number
	 */
	public void setFiscalChargeNumber(String fiscalChargeNumber) {
		this.fiscalChargeNumber = fiscalChargeNumber;
	}

	/**
	 * Gets the fiscal process.
	 *
	 * @return the fiscal process
	 */
	public String getFiscalProcess() {
		return fiscalProcess;
	}

	/**
	 * Sets the fiscal process.
	 *
	 * @param fiscalProcess the new fiscal process
	 */
	public void setFiscalProcess(String fiscalProcess) {
		this.fiscalProcess = fiscalProcess;
	}

	/**
	 * Gets the fiscal authority.
	 *
	 * @return the fiscal authority
	 */
	public String getFiscalAuthority() {
		return fiscalAuthority;
	}

	/**
	 * Sets the fiscal authority.
	 *
	 * @param fiscalAuthority the new fiscal authority
	 */
	public void setFiscalAuthority(String fiscalAuthority) {
		this.fiscalAuthority = fiscalAuthority;
	}

	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return registerUser;
	}

	/**
	 * Sets the register user.
	 *
	 * @param registerUser the new register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}	
	
}
