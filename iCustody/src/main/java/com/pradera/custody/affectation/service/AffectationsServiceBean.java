
package com.pradera.custody.affectation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.blockentity.service.BlockEntityServiceBean;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.tranfersecurities.to.SecurityBlockTransferTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.custody.to.AffectationObjectTO;
import com.pradera.integration.component.custody.to.AffectationRegisterTO;
import com.pradera.integration.component.custody.to.LegalInformationObjetTO;
import com.pradera.integration.component.custody.to.StockUnblockObjectTO;
import com.pradera.integration.component.custody.to.StockUnblockRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.accreditationcertificates.BlockRequestFile;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.affectation.type.RequestAffectationStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.participantunion.BlockOperationUnion;
import com.pradera.model.custody.rectification.RectificationDetail;
import com.pradera.model.custody.transfersecurities.BlockOperationTransfer;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.BlockMarketFactOperationStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AffectationsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class AffectationsServiceBean extends CrudDaoServiceBean{

	/** The logger. */
	@Inject
	private transient PraderaLogger logger;
	
	/** The request reversals service bean. */
	@EJB
	RequestReversalsServiceBean requestReversalsServiceBean;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The holder balance movements service bean. */
	@Inject 
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The block entity service bean. */
 	@EJB
	BlockEntityServiceBean blockEntityServiceBean;
	

	/**
	 * Valid block amount in transfer available.
	 *
	 * @param idTransferBlockPk the id transfer block pk
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer validBlockAmountInTransferAvailable(Long idTransferBlockPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select count(*) From BlockOperationTransfer bot join bot.blockOperation bo join bo.blockRequest br join bot.securityTransferOperation sto ");
		sbQuery.append(" Where sto.idTransferOperationPk = :idTransferBlockPk and "
					 + " bo.actualBlockBalance = sto.quantityOperation and"
					 + " sto.indBlockTransfer = 1 ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idTransferBlockPk", idTransferBlockPk);	 
				   	    
		return Integer.parseInt(query.getSingleResult().toString()); 
	}

	/**
	 * Gets the block request.
	 *
	 * @param filters the filters
	 * @return the block request
	 * @throws ServiceException the service exception
	 */
	public BlockRequest getBlockRequest(BlockRequest filters) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select br From BlockRequest br ");
		sbQuery.append(" Where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdBlockRequestPk()))
			sbQuery.append(" and br.idBlockRequestPk = :idBlockRequestPkPrm ");
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getBlockNumber()))
			sbQuery.append(" and br.blockNumber = :blockNumberPrm ");
		
		TypedQuery<BlockRequest> query =  em.createQuery(sbQuery.toString(),BlockRequest.class); 

		if(Validations.validateIsNotNullAndNotEmpty(filters.getBlockNumber()))
			query.setParameter("blockNumberPrm", filters.getBlockNumber());

		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdBlockRequestPk()))
			query.setParameter("idBlockRequestPkPrm", filters.getIdBlockRequestPk());
		BlockRequest br = null;
		
		try{
			br = query.getSingleResult();
		}catch (NoResultException nre){
			br = null;
		}
		return br;
	}
	
	/**
	 * Gets the block request files.
	 *
	 * @param idBlockRequestPk the id block request pk
	 * @return the block request files
	 * @throws ServiceException the service exception
	 */
	public List<BlockRequestFile> getBlockRequestFiles(Long idBlockRequestPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select brf From BlockRequestFile brf ");
		sbQuery.append(" Where 1 = 1 ");
		
		if(Validations.validateIsNotNull(idBlockRequestPk))
			sbQuery.append(" and brf.blockRequest.idBlockRequestPk = :idBlockRequestPkPrm ");
		
		TypedQuery<BlockRequestFile> query =  em.createQuery(sbQuery.toString(),BlockRequestFile.class); 

		if(Validations.validateIsNotNull(idBlockRequestPk))
			query.setParameter("idBlockRequestPkPrm", idBlockRequestPk);
		
		List<BlockRequestFile> brs = query.getResultList();
		
		return brs;
	}
	
	/**
	 * Gets the block operation.
	 *
	 * @param filters the filters
	 * @return the block operation
	 * @throws ServiceException the service exception
	 */
	public BlockOperation getBlockOperation(BlockOperation filters) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select bo From BlockOperation bo ");
		sbQuery.append(" join fetch bo.participant join fetch bo.holderAccount ");
		sbQuery.append(" join fetch bo.securities join fetch bo.blockRequest br ");
		sbQuery.append(" join fetch br.blockRequestFile ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNullAndPositive(filters.getIdBlockOperationPk()))
			sbQuery.append(" and bo.idBlockOperationPk = :idBlockOperationPkPrm ");
		
		if( Validations.validateIsNotNull(filters.getBlockRequest())){
			if(Validations.validateIsNotNullAndPositive(filters.getBlockRequest().getIdBlockRequestPk()))
				sbQuery.append(" and bo.blockRequest.idBlockRequestFk = :idBlockRequestPkPrm ");
		}
		TypedQuery<BlockOperation> query =  em.createQuery(sbQuery.toString(),BlockOperation.class); 

		if(Validations.validateIsNotNullAndPositive(filters.getIdBlockOperationPk()))
			query.setParameter("idBlockOperationPkPrm", filters.getIdBlockOperationPk());
		
		if( Validations.validateIsNotNull(filters.getBlockRequest())){
			if(Validations.validateIsNotNullAndPositive(filters.getBlockRequest().getIdBlockRequestPk()))
				query.setParameter("idBlockRequestPkPrm", filters.getBlockRequest().getIdBlockRequestPk());
		}
		
		BlockOperation bo = query.getSingleResult();
		
		return bo;
	}
	
	/**
	 * Search security block transfer to.
	 *
	 * @param filters the filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityBlockTransferTO> searchSecurityBlockTransferTo(SecurityBlockTransferTO filters) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select bo.documentNumber, " +//0
					   " br.idBlockRequestPk, " +//1
					   " br.blockType, " +//2
					   " (SELECT description FROM ParameterTable where parameterTablePk = br.blockType) as blockTypeDesc, " +//3
					   " se.idSecurityCodePk, " +//4
					   " be.firstLastName, " +//5
					   " bo.actualBlockBalance, " +//6
					   " (SELECT description FROM ParameterTable where parameterTablePk = bo.blockLevel) as blockLeveDesc, " +//7
					   " bo.idBlockOperationPk, "+//8
					   " co.operationDate, "+//9
					   " (SELECT description FROM Security WHERE idSecurityCodePk = se.idSecurityCodePk) as isinDescription, "+//10
					   " be.name, " +//11
					   " be.secondLastName, "+//12
					   " be.fullName, "+//13
					   " be.personType, "+//14
					   " be.idBlockEntityPk, "+//15
					   " se.securityClass," +//16
					   " co.operationNumber" //17
					);
		sbQuery.append(" From BlockOperation bo " +
						" join bo.blockRequest br " +
						" join br.blockEntity be " +
						" join bo.custodyOperation co " +
						" join bo.securities se ");
		sbQuery.append(" Where 1 = 1 ");
		sbQuery.append(" and bo.blockState = "+AffectationStateType.BLOCKED.getCode());	//solo documentos de bloqueo en estado bloqueado
		sbQuery.append(" and bo.blockLevel = "+LevelAffectationType.BLOCK.getCode());	//solo nivel bloqueado
		if(Validations.validateIsNotNullAndPositive(filters.getDocumentNumber()))
			sbQuery.append(" and bo.documentNumber = :documentNumberPrm ");

		if(Validations.validateIsNotNullAndPositive(filters.getIdBlockOperationPk()))
			sbQuery.append(" and bo.idBlockOperationPk = :idBlockOperationPkPrm ");
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk()))
			sbQuery.append(" and bo.holderAccount.idHolderAccountPk = :idHolderAccountPkPrm ");
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceAccountNumber()))
			sbQuery.append(" and bo.holderAccount.accountNumber = :accountNumberPrm ");
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceIdParticipantPk()))
			sbQuery.append(" and bo.participant.idParticipantPk = :idParticipantPkPrm ");
		
		if(Validations.validateIsNotNull(filters.getIdSecurityCodePk()))
			sbQuery.append(" and se.idSecurityCodePk = :idSecurityCodePkPrm ");
		
		if(Validations.validateIsNotNullAndPositive(filters.getBlockType()))
			sbQuery.append(" and br.blockType = :blockTypePrm ");

		sbQuery.append(" Order by co.operationDate,se.idSecurityCodePk ");
		Query query = em.createQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndPositive(filters.getDocumentNumber()))
			query.setParameter("documentNumberPrm",filters.getDocumentNumber());

		if(Validations.validateIsNotNullAndPositive(filters.getIdBlockOperationPk()))
			query.setParameter("idBlockOperationPkPrm",filters.getIdBlockOperationPk());
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk()))
			query.setParameter("idHolderAccountPkPrm",Long.valueOf(filters.getSourceIdHolderAccountPk()));
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceAccountNumber()))
			query.setParameter("accountNumberPrm", filters.getSourceAccountNumber());
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceIdParticipantPk()))
			query.setParameter("idParticipantPkPrm", filters.getSourceIdParticipantPk());
		
		if(Validations.validateIsNotNull(filters.getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePkPrm", filters.getIdSecurityCodePk());
		
		if(Validations.validateIsNotNullAndPositive(filters.getBlockType()))
			query.setParameter("blockTypePrm", filters.getBlockType());
		
		List<SecurityBlockTransferTO> lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		SecurityBlockTransferTO securityBlockTo = null;

		List<Object> objectList = query.getResultList();
		for (int i = 0; i < objectList.size(); i++) {
			
			Object obj[] = (Object[])objectList.get(i);
			securityBlockTo = new SecurityBlockTransferTO();
			
			securityBlockTo.setDocumentNumber((Long)obj[0]);
			securityBlockTo.setIdBlockRequest((Long)obj[1]);
			securityBlockTo.setBlockType((Integer)obj[2]);
			securityBlockTo.setBlockTypeDescription((String)obj[3]);
			securityBlockTo.setIdSecurityCodePk((String)obj[4]);
			securityBlockTo.setSecurityClass( (Integer)obj[16] );
			
			String descriptionBlockEntity = "";
			if(Validations.validateIsNotNull(obj[14])){
				descriptionBlockEntity += obj[15].toString()+" - ";
					if(Integer.valueOf(obj[14].toString()).equals(PersonType.JURIDIC.getCode())){
						if(Validations.validateIsNotNull(obj[13])){
							descriptionBlockEntity += " "+obj[13].toString();
						}
					}else{
						if(Validations.validateIsNotNull(obj[11])){
							descriptionBlockEntity += " "+obj[11].toString();
						}
						if(Validations.validateIsNotNull(obj[5])){
							descriptionBlockEntity += " "+obj[5].toString();
						}
						if(Validations.validateIsNotNull(obj[12])){
							descriptionBlockEntity += " "+obj[12].toString();
						}
					}
			}
			securityBlockTo.setBlockEntityDescription(descriptionBlockEntity);
			securityBlockTo.setActualBlockBalance((BigDecimal)obj[6]);
			securityBlockTo.setBlockLeveDesc((String)obj[7]);
			securityBlockTo.setIdBlockOperationPk((Long)obj[8]);
			if(Validations.validateIsNotNull(obj[9])){
				securityBlockTo.setOperationDate((Date)obj[9]);
			}
			if(Validations.validateIsNotNull(obj[10])){
				securityBlockTo.setIsinDescription((String)obj[10]);
			}
			if(Validations.validateIsNotNull(obj[17])){
				securityBlockTo.setOperationNumber((Long)obj[17]);
			}
			lstSecurityBlockTransferTO.add(securityBlockTo);
		}
		
		return lstSecurityBlockTransferTO;
	}
	
	/**
	 * Search security block transfer to width document number.
	 *
	 * @param lstFilters the lst filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityBlockTransferTO> searchSecurityBlockTransferToWidthDocumentNumber(List<SecurityBlockTransferTO> lstFilters) throws ServiceException{

		List<SecurityBlockTransferTO> lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select bo.documentNumber, " +//0
					   " br.idBlockRequestPk, " +//1
					   " br.blockType, " +//2
					   " (SELECT description FROM ParameterTable where parameterTablePk = br.blockType) as blockTypeDesc, " +//3
					   " se.idSecurityCodePk, " +//4
					   " be.idBlockEntityPk, " +//5
					   " bo.actualBlockBalance, " +//6
					   " (SELECT description FROM ParameterTable where parameterTablePk = bo.blockLevel) as blockLeveDesc, " +//7
					   " bo.idBlockOperationPk, "+//8
					   " co.operationDate, "+//9
					   " (SELECT description FROM Security WHERE idSecurityCodePk = se.idSecurityCodePk) as isinDescription, "+ //10
					   " (be.name||' '||be.firstLastName||' '||be.secondLastName), "+//11
					   " be.fullName, "+//12
					   " be.personType, "+//13
					   " bo.blockLevel,"+//14
					   " co.operationNumber ");//15
		sbQuery.append(" From BlockOperation bo " +
						" join bo.blockRequest br " +
						" join br.blockEntity be " +
						" join bo.custodyOperation co " +
						" join bo.securities se ");
		 sbQuery.append(" Where 1 = 1 AND  bo.blockState in ("+AffectationStateType.BLOCKED.getCode()+")"); 
		if(lstFilters.size()>0){
			
			sbQuery.append(" and bo.documentNumber in (  ");
			for (int i = 0; i < lstFilters.size(); i++) {
				sbQuery.append(" "+lstFilters.get(i).getDocumentNumber());
				if( (lstFilters.size()-(i+1))>0 )
					sbQuery.append(",");
			}
			sbQuery.append(" ) ");
		
			Query query = em.createQuery(sbQuery.toString()); 
			
			SecurityBlockTransferTO securityBlockTo = null;
			List<Object> objectList = query.getResultList();
			
			for (int i = 0; i < objectList.size(); i++) {
				
				Object obj[] = (Object[])objectList.get(i);
				securityBlockTo = new SecurityBlockTransferTO();
				
				securityBlockTo.setDocumentNumber((Long)obj[0]);
				securityBlockTo.setIdBlockRequest((Long)obj[1]);
				securityBlockTo.setBlockType((Integer)obj[2]);
				securityBlockTo.setBlockTypeDescription((String)obj[3]);
				securityBlockTo.setIdSecurityCodePk((String)obj[4]);
				
				Integer personType = (Integer)obj[13];
				if(personType.equals(PersonType.NATURAL.getCode())){
					securityBlockTo.setBlockEntityDescription(((Long)obj[5]).toString()+" - "+(String)obj[11]);
				}else{
					securityBlockTo.setBlockEntityDescription(((Long)obj[5]).toString()+" - "+(String)obj[12]);
				}
				
				securityBlockTo.setActualBlockBalance((BigDecimal)obj[6]);
				securityBlockTo.setBlockLeveDesc((String)obj[7]);
				securityBlockTo.setIdBlockOperationPk((Long)obj[8]);
				if(Validations.validateIsNotNull(obj[9])){
					securityBlockTo.setOperationDate((Date)obj[9]);
				}
				if(Validations.validateIsNotNull(obj[10])){
					securityBlockTo.setIsinDescription((String)obj[10]);
				}
				securityBlockTo.setBlockLevel((Integer)obj[14]);
				if(obj[15]!=null)
					securityBlockTo.setOperationNumber(new Long(obj[15].toString()));
				lstSecurityBlockTransferTO.add(securityBlockTo);
			}
		
		}
		
		return lstSecurityBlockTransferTO;
	}
	
	/**
	 * Find block operations by filters.
	 *
	 * @param blockOperationTO the block operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<BlockOperation> findBlockOperationsByFilters(BlockOperationTO blockOperationTO) throws ServiceException {
		
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select bo From BlockOperation bo ");
		sbQuery.append("  inner join fetch bo.blockRequest br ");
		sbQuery.append("  inner join fetch br.blockEntity be ");
		sbQuery.append("  inner join fetch bo.custodyOperation co ");
		sbQuery.append(" Where 1 = 1 ");	
		
		if(blockOperationTO.getIndEarlyRedemption().equals(ComponentConstant.ONE)){
			sbQuery.append(" and bo.indRescue = :idEarlyRedemption");
			parameters.put("idEarlyRedemption", blockOperationTO.getIndEarlyRedemption());
		}
		
		if(Validations.validateIsNotNullAndPositive(blockOperationTO.getIdBlockOperationPk())){
			sbQuery.append(" and bo.idBlockOperationPk = :idBlockOperationPk ");
			parameters.put("idBlockOperationPk", blockOperationTO.getIdBlockOperationPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(blockOperationTO.getIdHolderAccountPk())){
			sbQuery.append(" and bo.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
			parameters.put("idHolderAccountPk", blockOperationTO.getIdHolderAccountPk());
		}
		
		if(Validations.validateIsNotNullAndPositive(blockOperationTO.getIdParticipantPk())){
			sbQuery.append(" and bo.participant.idParticipantPk= :idParticipantPk ");
			parameters.put("idParticipantPk", blockOperationTO.getIdParticipantPk());
		}

		if(Validations.validateIsNotNullAndNotEmpty(blockOperationTO.getSecurityCodePk())){
			sbQuery.append(" and bo.securities.idSecurityCodePk = :idSecurityCodePk ");
			parameters.put("idSecurityCodePk", blockOperationTO.getSecurityCodePk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(blockOperationTO.getIdStateAffectation())){
			sbQuery.append(" and bo.blockState = :idBlockState ");
			parameters.put("idBlockState", Integer.valueOf(blockOperationTO.getIdStateAffectation()));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(blockOperationTO.getIdTypeAffectation())){
			sbQuery.append(" and br.blockType = :idBlockType ");
			parameters.put("idBlockType",Integer.valueOf(blockOperationTO.getIdTypeAffectation()));
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(blockOperationTO.getIdLevelAffectation())){
			sbQuery.append(" and bo.blockLevel = :idBlockLevel ");
			parameters.put("idBlockLevel",Integer.valueOf(blockOperationTO.getIdLevelAffectation()));
		}
		
		return findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the block operation.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return the block operation
	 * @throws ServiceException the service exception
	 */
	public BlockOperation getBlockOperation(Long idBlockOperationPk) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select bo From BlockOperation bo ");
		sbQuery.append("  LEFT OUTER JOIN bo.blockRequest br ");
		sbQuery.append("  LEFT OUTER JOIN br.blockRequestFile brf ");
		sbQuery.append("  LEFT OUTER JOIN br.blockEntity be ");
		sbQuery.append(" Where 1 = 1 ");
		sbQuery.append(" and bo.idBlockOperationPk = :idBlockOperationPk ");
		
		TypedQuery<BlockOperation> q = em.createQuery(sbQuery.toString(),BlockOperation.class);
		q.setParameter("idBlockOperationPk", idBlockOperationPk);
		
		try {
			return q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Clone block operation.
	 *
	 * @param blockOperation the block operation
	 * @param user the user
	 * @return the block operation
	 */
	public BlockOperation cloneBlockOperation(BlockOperation blockOperation,String user) {
		
			BlockOperation objBlockOperation= new BlockOperation();
			objBlockOperation.setActualBlockAmount(blockOperation.getActualBlockAmount());
			objBlockOperation.setActualBlockBalance(blockOperation.getActualBlockBalance());
			objBlockOperation.setBlockLevel(blockOperation.getBlockLevel());
			BlockRequest objBlockRequest = new BlockRequest();
			objBlockRequest.setIdBlockRequestPk(blockOperation.getBlockRequest().getIdBlockRequestPk());
			objBlockRequest.setBlockEntity(blockOperation.getBlockRequest().getBlockEntity());
			objBlockRequest.setBlockForm(blockOperation.getBlockRequest().getBlockForm());
			objBlockRequest.setBlockNumber(blockOperation.getBlockRequest().getBlockNumber());
			objBlockRequest.setBlockNumberDate(blockOperation.getBlockRequest().getBlockNumberDate());
			objBlockRequest.setBlockRequestState(RequestAffectationStateType.CONFIRMED.getCode());
			objBlockRequest.setBlockType(blockOperation.getBlockRequest().getBlockType());
			objBlockRequest.setComments(blockOperation.getBlockRequest().getComments());
			objBlockRequest.setConfirmationDate(CommonsUtilities.currentDate());
			objBlockRequest.setConfirmationUser(user);
			objBlockRequest.setCourtDepositDate(blockOperation.getBlockRequest().getCourtDepositDate());
			objBlockRequest.setCurrency(blockOperation.getBlockRequest().getCurrency());
			objBlockRequest.setExchangeRate(blockOperation.getBlockRequest().getExchangeRate());
			objBlockRequest.setHolder(blockOperation.getBlockRequest().getHolder());
			objBlockRequest.setIndIssueRequest(blockOperation.getBlockRequest().getIndIssueRequest());
			objBlockRequest.setIndRequestDeclaration(blockOperation.getBlockRequest().getIndRequestDeclaration());
			objBlockRequest.setNotificationDate(blockOperation.getBlockRequest().getNotificationDate());
			objBlockRequest.setOriginBlockAmount(blockOperation.getBlockRequest().getOriginBlockAmount());
			objBlockRequest.setComments(blockOperation.getBlockRequest().getComments());
			objBlockRequest.setPendingBlockAmount(blockOperation.getBlockRequest().getPendingBlockAmount());
			objBlockRequest.setRegistryDate(CommonsUtilities.currentDate());
			objBlockRequest.setRegistryUser(user);
			objBlockRequest.setRejectDate(blockOperation.getBlockRequest().getRejectDate());
			objBlockRequest.setRejectUser(blockOperation.getBlockRequest().getRejectUser());
			objBlockRequest.setValorizationType(blockOperation.getBlockRequest().getValorizationType());
			objBlockRequest.setBlockOperation(new ArrayList<BlockOperation>());
			objBlockOperation.setBlockRequest(objBlockRequest);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("idBlockRequestPk", blockOperation.getBlockRequest().getIdBlockRequestPk());
			@SuppressWarnings("unchecked")
			List<BlockRequestFile> files =  findWithNamedQuery(BlockRequestFile.FIND_DATA_AND_INFO_BY_FK,params);
			if(files.size()>0){
				List<BlockRequestFile> lstBlockRequestFile=new ArrayList<BlockRequestFile>();
				for(BlockRequestFile f : files){
					f.setIdBlockRequestFilePk(null);
					f.setBlockRequest(new BlockRequest());
					f.getBlockRequest().setIdBlockRequestPk(blockOperation.getBlockRequest().getIdBlockRequestPk());
					f.setRegistryUser(user);
					f.setRegistryDate(CommonsUtilities.currentDate());
					lstBlockRequestFile.add(f);
				}
				objBlockOperation.getBlockRequest().setBlockRequestFile(lstBlockRequestFile);
			}
			
			
			objBlockOperation.setBlockState(blockOperation.getBlockState());
			objBlockOperation.setCurrentBlockAmount(blockOperation.getCurrentBlockAmount());
			CustodyOperation objCustodyOperation= new CustodyOperation();
			objCustodyOperation.setApplicationUser(blockOperation.getCustodyOperation().getApplicationUser());
			objCustodyOperation.setApplicationDate(blockOperation.getCustodyOperation().getApplicationDate());
			objCustodyOperation.setApprovalDate(blockOperation.getCustodyOperation().getApprovalDate());
			objCustodyOperation.setApprovalUser(blockOperation.getCustodyOperation().getApprovalUser());
			objCustodyOperation.setIndRectification(blockOperation.getCustodyOperation().getIndRectification());
			objCustodyOperation.setOperationDate(blockOperation.getCustodyOperation().getOperationDate());
			objCustodyOperation.setOperationNumber(blockOperation.getCustodyOperation().getOperationNumber());
			objCustodyOperation.setOperationType(blockOperation.getCustodyOperation().getOperationType());
			objCustodyOperation.setState(blockOperation.getCustodyOperation().getState());
			objBlockOperation.setCustodyOperation(objCustodyOperation);//
			
			objBlockOperation.setDocumentNumber(blockOperation.getDocumentNumber());
			objBlockOperation.setHolderAccount(blockOperation.getHolderAccount());
			objBlockOperation.setIndAccreditation(blockOperation.getIndAccreditation());
			objBlockOperation.setIndAmortization(blockOperation.getIndAmortization());
			objBlockOperation.setIndCashDividend(blockOperation.getIndCashDividend());
			objBlockOperation.setIndInterest(blockOperation.getIndInterest());
			objBlockOperation.setIndRescue(blockOperation.getIndRescue());
			objBlockOperation.setIndReturnContributions(blockOperation.getIndReturnContributions());
			objBlockOperation.setIndStockDividend(blockOperation.getIndStockDividend());
			objBlockOperation.setIndSuscription(blockOperation.getIndSuscription());
			objBlockOperation.setNominalValue(blockOperation.getNominalValue());
			objBlockOperation.setOriginalBlockAmount(blockOperation.getOriginalBlockAmount());
			objBlockOperation.setOriginalBlockBalance(blockOperation.getOriginalBlockBalance());
			objBlockOperation.setParticipant(blockOperation.getParticipant());
			objBlockOperation.setQuotationPrice(blockOperation.getQuotationPrice());
			objBlockOperation.setSecurities(blockOperation.getSecurities());
			
			return objBlockOperation;
	}
	
	
	/**
	 * Transfer block operations.
	 *
	 * @param transfers the transfers
	 * @param idBusinessProcessPk the id business process pk
	 * @param userName the user name
	 * @param indMarketFact the ind market fact
	 * @throws ServiceException the service exception
	 */
	public void transferBlockOperations(List<AffectationTransferTO> transfers, Long idBusinessProcessPk, String userName, Integer indMarketFact) throws ServiceException{
		
		// group block operations by holder to generate a new block Request
		Map<BlockRequest,List<AffectationTransferTO>> mapByRequest = new HashMap<BlockRequest, List<AffectationTransferTO>>();
		for(AffectationTransferTO affectationTransferTO : transfers){
			
			BlockOperation blockOperation = getBlockOperation(affectationTransferTO.getIdBlockOperationPk());
			BlockOperation blockOperationMirror=cloneBlockOperation(blockOperation,userName);
			List<AffectationTransferTO> list = mapByRequest.get(blockOperationMirror.getBlockRequest());
			if(list == null){
				list = new ArrayList<AffectationTransferTO>();
			}
			affectationTransferTO.setDetachedBlockOperation(blockOperationMirror);
			list.add(affectationTransferTO);
			mapByRequest.put(blockOperationMirror.getBlockRequest(), list);
		}
		
		//iterate over blockOperations and save on cascade by blockRequest 6
		for (Entry<BlockRequest, List<AffectationTransferTO>> entry : mapByRequest.entrySet()) {
			
			BlockRequest blockRequest = entry.getKey();
			List<BlockRequestFile> lstBRFile=blockRequest.getBlockRequestFile();
			blockRequest.setIdBlockRequestPk(null);
			create(blockRequest);
			if(Validations.validateIsNotNull(lstBRFile)){
				for(BlockRequestFile objBlockRequestFile : lstBRFile){
					objBlockRequestFile.setBlockRequest(blockRequest);
					create(objBlockRequestFile);
				}
			}
			for(AffectationTransferTO transferTO : entry.getValue()){
				BlockOperation blockOperation = transferTO.getDetachedBlockOperation();
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setOperationDate(CommonsUtilities.currentDate());
				custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
				custodyOperation.setConfirmUser(userName);
				custodyOperation.setRegistryDate(CommonsUtilities.currentDate());
				custodyOperation.setRegistryUser(userName);
				custodyOperation.setState(AffectationStateType.BLOCKED.getCode());
				custodyOperation.setOperationType(blockOperation.getCustodyOperation().getOperationType());
				custodyOperation.setOperationNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_BLOCKING));
				
				blockOperation.setActualBlockBalance(transferTO.getQuantity());
				blockOperation.setOriginalBlockBalance(transferTO.getQuantity());
				
				//blockOperation.setBlockOperationRef(blockOperationRef)// what can we do here
				blockOperation.setBlockState(AffectationStateType.BLOCKED.getCode());
				blockOperation.setHolderAccount(new HolderAccount(transferTO.getIdHolderAccountPk()));
				blockOperation.setSecurities(new Security(transferTO.getIdSecurityCodePk()));
				blockOperation.setParticipant(new Participant(transferTO.getIdParticipantPk()));
				blockRequest.setHolder(new Holder(transferTO.getIdHolderPk()));
				blockOperation.setBlockRequest(new BlockRequest());
				blockOperation.getBlockRequest().setIdBlockRequestPk(blockRequest.getIdBlockRequestPk());
				blockOperation.getBlockRequest().setBlockType(blockRequest.getBlockType());
				blockOperation.setCustodyOperation(custodyOperation);
				
				blockOperation.setBlockMarketFactOperations(null);
				create(blockOperation);
				if(indMarketFact.equals(ComponentConstant.ONE)){
					blockOperation.setBlockMarketFactOperations(new ArrayList<BlockMarketFactOperation>());
					BlockOperation blockOperationMk=new BlockOperation();
					blockOperationMk.setIdBlockOperationPk(blockOperation.getIdBlockOperationPk());
					for(MarketFactTO mktFactAffectation: transferTO.getMarketFactAffectations()){
						BlockMarketFactOperation blockMarketFactOperation = new BlockMarketFactOperation();
						blockMarketFactOperation.setMarketDate(mktFactAffectation.getMarketDate());
						blockMarketFactOperation.setMarketPrice(mktFactAffectation.getMarketPrice());
						blockMarketFactOperation.setMarketRate(mktFactAffectation.getMarketRate());
						blockMarketFactOperation.setOriginalBlockBalance(mktFactAffectation.getQuantity());
						blockMarketFactOperation.setActualBlockBalance(mktFactAffectation.getQuantity());
						blockMarketFactOperation.setBlockMarketState(BlockMarketFactOperationStateType.REGISTER.getCode());
						blockMarketFactOperation.setBlockOperation(blockOperationMk);
						blockMarketFactOperation.setRegistryDate(CommonsUtilities.currentDateTime());
						blockMarketFactOperation.setRegistryUser(userName);
						create(blockMarketFactOperation);
						blockOperation.getBlockMarketFactOperations().add(blockMarketFactOperation);
					}
				}
				
				// update ref to new block_operation  
				if(idBusinessProcessPk.equals(BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode())){
					BlockChangeOwnership bco = transferTO.getBlockChangeOwnership();
					if(bco!=null){
						bco.setRefBlockOperation(blockOperation);
						update(bco);
					}
				}else if(idBusinessProcessPk.equals(BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM.getCode())){
					RectificationDetail rec = transferTO.getRectificationDetail();
					if(rec!=null){
						rec.setBlockOperation(blockOperation);
						update(rec);
					}
				} else if(idBusinessProcessPk.equals(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode())){
					BlockOperationTransfer bsec = transferTO.getBlockOperationTransfer();
					if(bsec!=null){
						bsec.setRefBlockOperation(blockOperation);
						updateWithOutFlush(bsec);
					}
				} else if(idBusinessProcessPk.equals(BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode())){
					BlockOperationUnion bunion = transferTO.getBlockOperationUnion();
					if(bunion!=null){
						bunion.setRefBlockOperation(blockOperation);
						update(bunion);
					}
				}
				
				//execute the Component 
				List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdHolderAccount(blockOperation.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(blockOperation.getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdSecurityCode(blockOperation.getSecurities().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(blockOperation.getActualBlockBalance());
				if(indMarketFact.equals(ComponentConstant.ONE)){
					holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
					for(BlockMarketFactOperation blockMarketFactOperation : blockOperation.getBlockMarketFactOperations()){
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(blockMarketFactOperation.getMarketDate());
						marketFactAccountTO.setMarketPrice(blockMarketFactOperation.getMarketPrice());
						marketFactAccountTO.setMarketRate(blockMarketFactOperation.getMarketRate());
						marketFactAccountTO.setQuantity(blockMarketFactOperation.getActualBlockBalance());
						holderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
					}
				}
				accountBalanceTOs.add(holderAccountBalanceTO);
				
				AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
				objAccountComponent.setIdBusinessProcess(idBusinessProcessPk);
				objAccountComponent.setIdCustodyOperation(blockOperation.getIdBlockOperationPk() );		
				objAccountComponent.setLstTargetAccounts(accountBalanceTOs);
				objAccountComponent.setIdOperationType(blockOperation.getCustodyOperation().getOperationType());
				
				objAccountComponent.setIndMarketFact(indMarketFact);
				accountsComponentService.get().executeAccountsComponent(objAccountComponent);
				
			}
		}
		
	}
	
	
	/**
	 * Lst block market fact operation.
	 *
	 * @param idBlockMarketfactPk the id block marketfact pk
	 * @param idBlockOperationPk the id block operation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockMarketFactOperation> lstBlockMarketFactOperation(Long idBlockMarketfactPk, Long idBlockOperationPk) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select bmo From BlockMarketFactOperation bmo  ");//join fetch bmo.holderMarketFactBalance 
		sbQuery.append(" Where 1=1 AND indActive=:indActive ");
		
		if( Validations.validateIsNotNullAndPositive(idBlockMarketfactPk) ){
			sbQuery.append(" and bmo.idBlockMarketFactOperatPk = :idBlockMarketFactOperatPkPrm ");
		}
		if( Validations.validateIsNotNullAndPositive(idBlockOperationPk) ){
			sbQuery.append(" and bmo.blockOperation.idBlockOperationPk = :idBlockOperationPkPrm ");
		}
		
		TypedQuery<BlockMarketFactOperation> query = em.createQuery(sbQuery.toString(),BlockMarketFactOperation.class); 
		
		if( Validations.validateIsNotNullAndPositive(idBlockMarketfactPk) ){
			query.setParameter("idBlockMarketFactOperatPkPrm", idBlockMarketfactPk);
		}
		if( Validations.validateIsNotNullAndPositive(idBlockOperationPk) ){
			query.setParameter("idBlockOperationPkPrm", idBlockOperationPk);
		}
		query.setParameter("indActive", GeneralConstants.ONE_VALUE_INTEGER);
		
		return query.getResultList();
		
	}
	
	/**
	 * Lst holder market fact balance.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idParticipantPk the id participant pk
	 * @param idSecurityCodePk the id security code pk
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List<MarketFactAccountTO> lstHolderMarketFactBalance(Long idHolderAccountPk, Long idParticipantPk, String idSecurityCodePk) throws ServiceException,Exception{
		/** The market rate. */
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select mkf.marketDate, mkf.marketRate, mkf.marketPrice, mkf.availableBalance From "
					 + " HolderMarketFactBalance mkf ");
		sbQuery.append(" Where 1 = 1 ");
		sbQuery.append(" and mkf.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append(" and mkf.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append(" and mkf.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" and mkf.reportedBalance = 0 "
					 + " and mkf.reportingBalance = 0 "
					 + " and mkf.marginBalance = 0 "
					 + " and purchaseBalance = 0 "
					 + " and saleBalance = 0 ");
		Query query = em.createQuery(sbQuery.toString()); 

		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
		
		List<MarketFactAccountTO> lstHolderMarketFactBalance = new ArrayList<MarketFactAccountTO>();
		List<Object> objectList = query.getResultList();
		MarketFactAccountTO holderMarketFactBalance;
		
		if( objectList != null && objectList.size()>0 ){
		
			for (int i = 0; i < objectList.size(); i++) {
				
				Object obj[] = (Object[])objectList.get(i);
				holderMarketFactBalance = new MarketFactAccountTO();
				
				holderMarketFactBalance.setMarketDate((Date)obj[0]);
				if(Validations.validateIsNotNull(obj[1])){
					holderMarketFactBalance.setMarketRate((BigDecimal)obj[1]);
				}
				if(Validations.validateIsNotNull(obj[2])){
					holderMarketFactBalance.setMarketPrice((BigDecimal)obj[2]);
				}
				holderMarketFactBalance.setQuantity((BigDecimal)obj[3]);
				
				lstHolderMarketFactBalance.add(holderMarketFactBalance);
			}
		}
	
	return lstHolderMarketFactBalance;
	
	}
	
	/**
	 * Validate current reblocks.
	 *
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @param totalTransferQuantity the total transfer quantity
	 * @throws ServiceException the service exception
	 */
	public void validateCurrentReblocks(Long idParticipant, Long idHolderAccount, String idSecurityCode , BigDecimal totalTransferQuantity) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select bo.documentNumber From BlockOperation bo , CustodyOperation co , HolderAccountBalance hab");
		sbQuery.append(" Where bo.idBlockOperationPk = co.idCustodyOperationPk  ");
		sbQuery.append(" and bo.participant.idParticipantPk = hab.id.idParticipantPk ");
		sbQuery.append(" and bo.holderAccount.idHolderAccountPk = hab.id.idHolderAccountPk");
		sbQuery.append(" and bo.securities.idSecurityCodePk = hab.id.idSecurityCodePk ");
		sbQuery.append(" and bo.participant.idParticipantPk = :idParticipantPkPrm  ");
		sbQuery.append(" and bo.holderAccount.idHolderAccountPk = :idHolderAccountPkPrm ");
		sbQuery.append(" and bo.securities.idSecurityCodePk = :idSecurityCodePkPrm ");
		sbQuery.append(" and ( hab.totalBalance - :totalTransferQuantity ) < bo.actualBlockBalance  ");
		sbQuery.append(" and bo.blockState = :blockStatePrm ");
		sbQuery.append(" and bo.blockLevel = :blockLevelPrm ");
		
		TypedQuery<Long> query = em.createQuery(sbQuery.toString(),Long.class); 
		
		query.setParameter("idParticipantPkPrm", idParticipant);
		query.setParameter("idHolderAccountPkPrm", idHolderAccount);
		query.setParameter("idSecurityCodePkPrm", idSecurityCode);
		query.setParameter("blockStatePrm", AffectationStateType.BLOCKED.getCode());
		query.setParameter("blockLevelPrm", LevelAffectationType.REBLOCK.getCode());
		query.setParameter("totalTransferQuantity", totalTransferQuantity);
		
		List<Long> lstReblockNumbers = query.getResultList();
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstReblockNumbers)){
			Object[] bodyData = new Object[]{StringUtils.join(lstReblockNumbers.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
			
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_REBLOCKS_EXCEED_TOTAL,bodyData);
		}
	}
	
	/**
	 * Generate affectation register to.
	 *
	 * @param blockOperation the block operation
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateAffectationRegisterTO(BlockOperation blockOperation, Long idInterfaceProcess, 
																	LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		AffectationRegisterTO affectationRegisterTO= getAffectationRegisterTO(blockOperation);
		affectationRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(affectationRegisterTO, 
																GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransaction(affectationRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}
	
	/**
	 * Gets the affectation register to.
	 *
	 * @param blockOperation the block operation
	 * @return the affectation register to
	 * @throws ServiceException 
	 */
	public AffectationRegisterTO getAffectationRegisterTO(BlockOperation blockOperation) throws ServiceException {
		AffectationRegisterTO affectationRegisterTO = new AffectationRegisterTO();		
		affectationRegisterTO.setOperationNumber(blockOperation.getDocumentNumber());
		affectationRegisterTO.setIdCustodyOperation(blockOperation.getIdBlockOperationPk());
		affectationRegisterTO.setOperationCode(ComponentConstant.INTERFACE_STOCK_BLOCKING_DPF);	
		
		Security objSecurity = find(Security.class, blockOperation.getSecurities().getIdSecurityCodePk());
		Participant objParticipant = participantServiceBean.getParticipantIsIssuer(objSecurity.getIssuer().getIdIssuerPk());
		if(objParticipant != null){
			affectationRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			affectationRegisterTO.setEntityNemonic(objParticipant.getMnemonic());
		}		
		affectationRegisterTO.setOperationDate(CommonsUtilities.currentDate());
		
		HolderAccount objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountForCustody(blockOperation.getHolderAccount());
		if(objHolderAccount != null){
			affectationRegisterTO.setAccountNumber(objHolderAccount.getAccountNumber());
			affectationRegisterTO.setDocumentNumber(objHolderAccount.getHolderAccountDetails().get(0).getHolder().getDocumentNumber().trim());
		}		
		
		SecurityObjectTO securityObjectTO = holderBalanceMovementsServiceBean.populateSecurityObjectTO(blockOperation.getSecurities());
		securityObjectTO.setUnblockRequirement(GeneralConstants.STR_UNBLOCK_TAG);
		affectationRegisterTO.setSecurityObjectTO(securityObjectTO);
		
		AffectationObjectTO affectationObjetTO = new AffectationObjectTO();
		LegalInformationObjetTO legalInformationObjetTO = new LegalInformationObjetTO();
		
		affectationObjetTO.setAffectationCode(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode()) ?
				ComponentConstant.AFFECTATION_CODE_DPF_PRE : ComponentConstant.AFFECTATION_CODE_DPF_REJ);
		//classifying code affectation for auto settled and authority restricted
		if(ComponentConstant.ONE.equals(blockOperation.getBlockRequest().getIndAutoSettled()))
			affectationObjetTO.setAffectationCode(GeneralConstants.AFFECTATION_CODE_DPF_PAL);
		if(ComponentConstant.ONE.equals(blockOperation.getBlockRequest().getIndAuthorityRestricted()))
			affectationObjetTO.setAffectationCode(GeneralConstants.AFFECTATION_CODE_DPF_RAC);
		
		affectationObjetTO.setReferenceDocument(blockOperation.getBlockRequest().getBlockNumber());
		affectationObjetTO.setDocumentType(ComponentConstant.ONE);
		affectationObjetTO.setReceiptDate(CommonsUtilities.currentDate());
		
		if(blockOperation.getBlockRequest().getBlockEntity() != null){			
			BlockEntity objBlockEntity = blockEntityServiceBean.findBlockEntityByIdService(blockOperation.getBlockRequest().getBlockEntity().getIdBlockEntityPk());	
			if(objBlockEntity.getHolder() != null){
				affectationObjetTO.setIdHolder(objBlockEntity.getHolder().getIdHolderPk());
				affectationObjetTO.setDescriptionHolder(objBlockEntity.getHolder().getDescriptionHolder());
			} else {
				affectationObjetTO.setIdHolder(null);			
				affectationObjetTO.setDescriptionHolder(ComponentConstant.NOT_ACTIVE);
			}			
		}		
		affectationObjetTO.setLockingDays(null);
		legalInformationObjetTO.setCourtCode(null);
		legalInformationObjetTO.setJobNumber(null);
		legalInformationObjetTO.setRecord(null);
		legalInformationObjetTO.setApplicant(blockOperation.getBlockRequest().getBlockEntity().getDescriptionPerson());
		legalInformationObjetTO.setMotive(null);
		if(Validations.validateIsNotNullAndNotEmpty(blockOperation.getBlockRequest().getComments())){
			legalInformationObjetTO.setObservations(blockOperation.getBlockRequest().getComments());
		} else {
			legalInformationObjetTO.setObservations(ComponentConstant.NOT_ACTIVE);
		}				
		affectationObjetTO.setLegalInformationObjetTO(legalInformationObjetTO);
		affectationRegisterTO.setAffectationObjectTO(affectationObjetTO);
		return affectationRegisterTO;
	}
	
	/**
	 * Generate stock unblock register to.
	 *
	 * @param unblockOperation the unblock operation
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateStockUnblockRegisterTO(UnblockOperation unblockOperation, Long idInterfaceProcess, 
																	LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		StockUnblockRegisterTO stockUnblockRegisterTO= getStockUnblockRegisterTO(unblockOperation);
		stockUnblockRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(stockUnblockRegisterTO, 
															GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransaction(stockUnblockRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}
		
	/**
	 * Gets the stock unblock register to.
	 *
	 * @param unblockOperation the unblock operation
	 * @return the stock unblock register to
	 */
	public StockUnblockRegisterTO getStockUnblockRegisterTO(UnblockOperation unblockOperation) {
		StockUnblockRegisterTO stockUnblockRegisterTO= new StockUnblockRegisterTO();
		
		stockUnblockRegisterTO.setOperationNumber(unblockOperation.getDocumentNumber());
		stockUnblockRegisterTO.setIdCustodyOperation(unblockOperation.getIdUnblockOperationPk());
		stockUnblockRegisterTO.setOperationCode(ComponentConstant.INTERFACE_STOCK_UNBLOCKING_DPF);
		
		Security objSecurity = find(Security.class, unblockOperation.getBlockOperation().getSecurities().getIdSecurityCodePk());
		Participant objParticipant = participantServiceBean.getParticipantIsIssuer(objSecurity.getIssuer().getIdIssuerPk());
		if(objParticipant != null){
			stockUnblockRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			stockUnblockRegisterTO.setEntityNemonic(objParticipant.getMnemonic());//Lazy
		}				
		
		stockUnblockRegisterTO.setOperationDate(CommonsUtilities.currentDate());
		
		HolderAccount objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountForCustody(unblockOperation.getBlockOperation().getHolderAccount());
		if(objHolderAccount != null){
			stockUnblockRegisterTO.setAccountNumber(objHolderAccount.getAccountNumber());//Lazy
			stockUnblockRegisterTO.setDocumentNumber(objHolderAccount.getHolderAccountDetails().get(0).getHolder().getDocumentNumber().trim());//Lazy
		}		
		
		SecurityObjectTO securityObjectTO = holderBalanceMovementsServiceBean.populateSecurityObjectTO(unblockOperation.getBlockOperation().getSecurities());
		stockUnblockRegisterTO.setSecurityObjectTO(securityObjectTO);
		
		StockUnblockObjectTO stockUnblockObjectTO = new StockUnblockObjectTO();
		LegalInformationObjetTO legalInformationObjetTO = new LegalInformationObjetTO();
		
		stockUnblockObjectTO.setAffectationCode(unblockOperation.getBlockOperation().getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode()) ?
				ComponentConstant.AFFECTATION_CODE_DPF_PRE : ComponentConstant.AFFECTATION_CODE_DPF_REJ);
		//classifying code affectation for auto settled and authority restricted
		if (ComponentConstant.ONE.equals(unblockOperation.getBlockOperation().getBlockRequest().getIndAutoSettled()))
			stockUnblockObjectTO.setAffectationCode(GeneralConstants.AFFECTATION_CODE_DPF_PAL);
		if (ComponentConstant.ONE.equals(unblockOperation.getBlockOperation().getBlockRequest().getIndAuthorityRestricted()))
			stockUnblockObjectTO.setAffectationCode(GeneralConstants.AFFECTATION_CODE_DPF_RAC);
				
		stockUnblockObjectTO.setReferenceDocument(unblockOperation.getBlockOperation().getBlockRequest().getBlockNumber());
		if(Validations.validateIsNotNullAndNotEmpty(unblockOperation.getBlockOperation().getBlockRequest().getComments())){
			stockUnblockObjectTO.setObservations(unblockOperation.getBlockOperation().getBlockRequest().getComments());
		} else {
			stockUnblockObjectTO.setObservations(ComponentConstant.NOT_ACTIVE);
		}		
		
		legalInformationObjetTO.setCourtCode(null);
		legalInformationObjetTO.setJobNumber(null);
		legalInformationObjetTO.setRecord(null);
		
		BlockRequest objBlockRequest = find(BlockRequest.class, unblockOperation.getBlockOperation().getBlockRequest().getIdBlockRequestPk());				
		if(objBlockRequest != null){
			legalInformationObjetTO.setApplicant(objBlockRequest.getBlockEntity().getDescriptionPerson());
		}		
		legalInformationObjetTO.setMotive(null);		
		stockUnblockObjectTO.setLegalInformationObjetTO(legalInformationObjetTO);
		stockUnblockRegisterTO.setStockUnblockObjectTO(stockUnblockObjectTO);
		return stockUnblockRegisterTO;
	}
	
	/**
	 * Find unblock operations by filters.
	 *
	 * @param blockOperationTO the block operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UnblockOperation> findUnblockOperationsByFilters(BlockOperationTO blockOperationTO) throws ServiceException {		
		Map<String,Object> parameters = new HashMap<String, Object>();		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select uo From UnblockOperation uo ");
		sbQuery.append("  inner join fetch uo.unblockRequest ur ");
		sbQuery.append("  inner join fetch uo.custodyOperation co ");
		sbQuery.append(" Where 1 = 1 ");	
		if(Validations.validateIsNotNullAndPositive(blockOperationTO.getIdBlockOperationPk())){
			sbQuery.append(" and uo.blockOperation.idBlockOperationPk = :idBlockOperationPk ");
			parameters.put("idBlockOperationPk", blockOperationTO.getIdBlockOperationPk());
		}		
		return findListByQueryString(sbQuery.toString(), parameters);
	}

}
