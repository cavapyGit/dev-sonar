package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.physical.PhysicalCertificate;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountBalanceTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class TransferAccountBalanceTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private List<PhysicalCertificate> lstPhysicalCertificate;
	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The id security bc bcode. */
	private String idSecurityBCBcode;
	
	/** The bl check. */
	private boolean blCheck;
	
	/** The bl blCheckBlock. */
	private boolean blCheckBlock;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The holder name. */
	private String holderName;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The quantity remove balance. */
	private BigDecimal quantityRemoveBalance;
	
	/** The ban balance. */
	private BigDecimal banBalance = BigDecimal.ZERO;
	
	/** The pawn balance. */
	private BigDecimal pawnBalance = BigDecimal.ZERO;;
	
	/** The other block balance. */
	private BigDecimal otherBlockBalance = BigDecimal.ZERO;
	
	/** The holder account state. */
	private Integer holderAccountState;
	
	/** The market lock. */
	private Boolean marketLock;
	
	/** The market fact details. */
	private List<MarketFactTO> marketFactDetails;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The expiration date. */
	private Date expirationDate;
	
	/** The block operations. */
	private List<BlockOperation> blockOperations;
	
	/** The current block operations. */
	private List<BlockOperation> currentBlockOperations;
	
	/** The current affectation type. */
	private Integer currentAffectationType;
	
	/**
	 * Update current block operations.
	 *
	 * @param affectationType the affectation type
	 */
	public void updateCurrentBlockOperations(Integer affectationType){
		currentAffectationType = null;
		if(Validations.validateListIsNotNullAndNotEmpty(blockOperations)){
			currentAffectationType = affectationType;
			currentBlockOperations = new ArrayList<BlockOperation>();
			for (BlockOperation blockOperation : blockOperations) {
				if(currentAffectationType.equals(blockOperation.getBlockRequest().getBlockType())){
					currentBlockOperations.add(blockOperation);
				}
			}
		}
	}
	
	/**
	 * Gets the current block balance.
	 *
	 * @return the current block balance
	 */
	public BigDecimal getCurrentBlockBalance() {
		if(currentAffectationType!=null){
			if(AffectationType.BAN.getCode().equals(currentAffectationType)){
				return banBalance;
			} else if(AffectationType.PAWN.getCode().equals(currentAffectationType)){
				return pawnBalance;
			} else if(AffectationType.OTHERS.getCode().equals(currentAffectationType)){
				return otherBlockBalance;
			}
		}
		return null;
	}
	
	/**
	 * Sets the current block balance.
	 *
	 * @param blockBalance the new current block balance
	 */
	public void setCurrentBlockBalance(BigDecimal blockBalance) {
		if(currentAffectationType!=null){
			if(AffectationType.BAN.getCode().equals(currentAffectationType)){
				banBalance = blockBalance;
			} else if(AffectationType.PAWN.getCode().equals(currentAffectationType)){
				pawnBalance = blockBalance;
			} else if(AffectationType.OTHERS.getCode().equals(currentAffectationType)){
				otherBlockBalance = blockBalance;
			}
		}
	}
	
	/**
	 * Checks if is all block level checked.
	 *
	 * @return true, if is all block level checked
	 */
	public boolean isAllBlockLevelChecked() {
		if(currentAffectationType!=null){
			for (BlockOperation blockOperation : currentBlockOperations) {
				if(LevelAffectationType.BLOCK.getCode().equals(blockOperation.getBlockLevel()) && !blockOperation.getSelected()){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		if(blCheck){
			if(quantityRemoveBalance!=null && quantityRemoveBalance.compareTo(BigDecimal.ZERO) > 0){
				return true;
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(blockOperations)){
				for (BlockOperation blockOperation : blockOperations) {
					if(blockOperation.getSelected()){
						return true;
					}
				}
			}
			
		}
		return false;
	}
	
	/**
	 * Sets the all reblock level unchecked.
	 */
	public void setAllReblockLevelUnchecked() {
		for (BlockOperation blockOperation : currentBlockOperations) {
			if(LevelAffectationType.REBLOCK.getCode().equals(blockOperation.getBlockLevel()) && blockOperation.getSelected()){
				blockOperation.setSelected(false);
			}
		}
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}

	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Checks if is bl check.
	 *
	 * @return true, if is bl check
	 */
	public boolean isBlCheck() {
		return blCheck;
	}

	/**
	 * Sets the bl check.
	 *
	 * @param blCheck the new bl check
	 */
	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}

	/**
	 * Checks if is bl check.
	 *
	 * @return true, if is bl checkBlock
	 */
	public boolean isBlCheckBlock() {
		return blCheckBlock;
	}

	/**
	 * Sets the bl check.
	 *
	 * @param blCheckBlock the new bl CheckBlock
	 */
	public void setBlCheckBlock(boolean blCheckBlock) {
		this.blCheckBlock = blCheckBlock;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the holder name.
	 *
	 * @return the holder name
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * Sets the holder name.
	 *
	 * @param holderName the new holder name
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}

	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}

	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}

	/**
	 * Gets the quantity remove balance.
	 *
	 * @return the quantity remove balance
	 */
	public BigDecimal getQuantityRemoveBalance() {
		return quantityRemoveBalance;
	}

	/**
	 * Sets the quantity remove balance.
	 *
	 * @param quantityRemoveBalance the new quantity remove balance
	 */
	public void setQuantityRemoveBalance(BigDecimal quantityRemoveBalance) {
		this.quantityRemoveBalance = quantityRemoveBalance;
	}

	/**
	 * Gets the holder account state.
	 *
	 * @return the holder account state
	 */
	public Integer getHolderAccountState() {
		return holderAccountState;
	}

	/**
	 * Sets the holder account state.
	 *
	 * @param holderAccountState the new holder account state
	 */
	public void setHolderAccountState(Integer holderAccountState) {
		this.holderAccountState = holderAccountState;
	}

	/**
	 * Gets the market lock.
	 *
	 * @return the market lock
	 */
	public Boolean getMarketLock() {
		return marketLock;
	}

	/**
	 * Sets the market lock.
	 *
	 * @param marketLock the new market lock
	 */
	public void setMarketLock(Boolean marketLock) {
		this.marketLock = marketLock;
	}

	/**
	 * Gets the market fact details.
	 *
	 * @return the market fact details
	 */
	public List<MarketFactTO> getMarketFactDetails() {
		return marketFactDetails;
	}

	/**
	 * Sets the market fact details.
	 *
	 * @param marketFactDetails the new market fact details
	 */
	public void setMarketFactDetails(List<MarketFactTO> marketFactDetails) {
		this.marketFactDetails = marketFactDetails;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the block operations.
	 *
	 * @return the block operations
	 */
	public List<BlockOperation> getBlockOperations() {
		return blockOperations;
	}

	/**
	 * Sets the block operations.
	 *
	 * @param blockOperations the new block operations
	 */
	public void setBlockOperations(List<BlockOperation> blockOperations) {
		this.blockOperations = blockOperations;
	}

	/**
	 * Gets the current block operations.
	 *
	 * @return the current block operations
	 */
	public List<BlockOperation> getCurrentBlockOperations() {
		return currentBlockOperations;
	}

	/**
	 * Sets the current block operations.
	 *
	 * @param currentBlockOperations the new current block operations
	 */
	public void setCurrentBlockOperations(
			List<BlockOperation> currentBlockOperations) {
		this.currentBlockOperations = currentBlockOperations;
	}

	/**
	 * Gets the ban balance.
	 *
	 * @return the ban balance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}

	/**
	 * Sets the ban balance.
	 *
	 * @param banBalance the new ban balance
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}

	/**
	 * Gets the pawn balance.
	 *
	 * @return the pawn balance
	 */
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}

	/**
	 * Sets the pawn balance.
	 *
	 * @param pawnBalance the new pawn balance
	 */
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}

	/**
	 * Gets the other block balance.
	 *
	 * @return the other block balance
	 */
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}

	/**
	 * Sets the other block balance.
	 *
	 * @param otherBlockBalance the new other block balance
	 */
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}

	/**
	 * Gets the current affectation type.
	 *
	 * @return the current affectation type
	 */
	public Integer getCurrentAffectationType() {
		return currentAffectationType;
	}

	/**
	 * Sets the current affectation type.
	 *
	 * @param currentAffectationType the new current affectation type
	 */
	public void setCurrentAffectationType(Integer currentAffectationType) {
		this.currentAffectationType = currentAffectationType;
	}

	/**
	 * Gets the id security bc bcode.
	 *
	 * @return the id security bc bcode
	 */
	public String getIdSecurityBCBcode() {
		return idSecurityBCBcode;
	}

	/**
	 * Sets the id security bc bcode.
	 *
	 * @param idSecurityBCBcode the new id security bc bcode
	 */
	public void setIdSecurityBCBcode(String idSecurityBCBcode) {
		this.idSecurityBCBcode = idSecurityBCBcode;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public List<PhysicalCertificate> getLstPhysicalCertificate() {
		return lstPhysicalCertificate;
	}

	public void setLstPhysicalCertificate(List<PhysicalCertificate> lstPhysicalCertificate) {
		this.lstPhysicalCertificate = lstPhysicalCertificate;
	}

}