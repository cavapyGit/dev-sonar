package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.negotiation.NegotiationMechanism;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RegisterBanOrOthersTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class RegisterBanOrOthersTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst block motive. */
	private List<ParameterTable> lstBlockMotive;
	
	/** The parameter table block motive. */
	private ParameterTable parameterTableBlockMotive;	
	
	/** The lst affectation form. */
	private List<ParameterTable> lstAffectationForm;
	
	/** The parameter table affectation form. */
	private ParameterTable parameterTableAffectationForm;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The parameter table currency. */
	private ParameterTable parameterTableCurrency;
	
	/** The lst holder account. */
	private List<HolderAccountTO> lstHolderAccount;	
	
	/** The lst holder account balance header. */
	private List<HolderAccountBalanceHeaderTO> lstHolderAccountBalanceHeader;
	
	/** The holder. */
	private Holder holder;	
	
	/** The block entity. */
	private BlockEntity blockEntity;
	
	/** The document file. */
	private byte[] documentFile;
	
	/** The file name. */
	private String fileName;
	
	/** The number record. */
	private String numberRecord;
	
	/** The date record. */
	private Date dateRecord;
	
	/** The statement issued. */
	private String statementIssued;
	
	/** The statement request. */
	private String statementRequest;
	
	/** The bl show indicators. */
	private boolean blShowIndicators;
	
	/** The bl opposition. */
	private boolean blOpposition;
	
	/** The bl others. */
	private boolean blOthers;
	
	/** The bl no opposition. */
	private boolean blNoOpposition;
	
	/** The bl cash valuation. */
	private boolean blCashValuation;
	
	/** The amount. */
	private BigDecimal amount;	
	
	/** The date dep tribunal. */
	private Date dateDepTribunal;
	
	/** The notification date. */
	private Date notificationDate;
	
	/** The limit last quotation. */
	private String limitLastQuotation;
	
	/** The id negotiation mechanism. */
	private Long idNegotiationMechanism;	
	
	/** The bd exchange rate. */
	private BigDecimal bdExchangeRate;
	
	/** The lst negotiation mechanism. */
	private List<NegotiationMechanism> lstNegotiationMechanism;
	
	/** The registry user. */
	private String registryUser;
	
	/** The block valoritation amount. */
	private BigDecimal blockValoritationAmount = BigDecimal.ZERO;
	
	/** The others. */
	private String others;
	
	/** The observations. */
	private String observations;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The ind registered depositary. */
	private Integer indRegisteredDepositary;
	
	/** The check authority restricted. */
	private boolean checkAuthorityRestricted;
	
	/** The show authority restricted. */
	private boolean showAuthorityRestricted;
	
	/** The circular number. */
	//fiscal info
	private String circularNumber;
	
	/** The fiscal procedence. */
	private String fiscalProcedence;
	
	/** The fiscal currency. */
	private Integer fiscalCurrency;
	
	/** The fiscal amount. */
	private BigDecimal fiscalAmount;
	
	/** The fiscal charge number. */
	private String fiscalChargeNumber;
	
	/** The fiscal process. */
	private String fiscalProcess;
	
	/** The fiscal authority. */
	private String fiscalAuthority;
	

	/**
	 * Gets the lst affectation form.
	 *
	 * @return the lst affectation form
	 */
	public List<ParameterTable> getLstAffectationForm() {
		return lstAffectationForm;
	}
	
	/**
	 * Sets the lst affectation form.
	 *
	 * @param lstAffectationForm the new lst affectation form
	 */
	public void setLstAffectationForm(List<ParameterTable> lstAffectationForm) {
		this.lstAffectationForm = lstAffectationForm;
	}
	
	/**
	 * Gets the parameter table affectation form.
	 *
	 * @return the parameter table affectation form
	 */
	public ParameterTable getParameterTableAffectationForm() {
		return parameterTableAffectationForm;
	}
	
	/**
	 * Sets the parameter table affectation form.
	 *
	 * @param parameterTableAffectationForm the new parameter table affectation form
	 */
	public void setParameterTableAffectationForm(
			ParameterTable parameterTableAffectationForm) {
		this.parameterTableAffectationForm = parameterTableAffectationForm;
	}
	
	/**
	 * Gets the lst block motive.
	 *
	 * @return the lst block motive
	 */
	public List<ParameterTable> getLstBlockMotive() {
		return lstBlockMotive;
	}
	
	/**
	 * Sets the lst block motive.
	 *
	 * @param lstBlockMotive the new lst block motive
	 */
	public void setLstBlockMotive(List<ParameterTable> lstBlockMotive) {
		this.lstBlockMotive = lstBlockMotive;
	}
	
	/**
	 * Gets the parameter table block motive.
	 *
	 * @return the parameter table block motive
	 */
	public ParameterTable getParameterTableBlockMotive() {
		return parameterTableBlockMotive;
	}
	
	/**
	 * Sets the parameter table block motive.
	 *
	 * @param parameterTableBlockMotive the new parameter table block motive
	 */
	public void setParameterTableBlockMotive(
			ParameterTable parameterTableBlockMotive) {
		this.parameterTableBlockMotive = parameterTableBlockMotive;
	}
	
	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccountTO> getLstHolderAccount() {
		return lstHolderAccount;
	}
	
	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccountTO> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
	/**
	 * Gets the lst holder account balance header.
	 *
	 * @return the lst holder account balance header
	 */
	public List<HolderAccountBalanceHeaderTO> getLstHolderAccountBalanceHeader() {
		return lstHolderAccountBalanceHeader;
	}
	
	/**
	 * Sets the lst holder account balance header.
	 *
	 * @param lstHolderAccountBalanceHeader the new lst holder account balance header
	 */
	public void setLstHolderAccountBalanceHeader(
			List<HolderAccountBalanceHeaderTO> lstHolderAccountBalanceHeader) {
		this.lstHolderAccountBalanceHeader = lstHolderAccountBalanceHeader;
	}
	
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public BlockEntity getBlockEntity() {
		return blockEntity;
	}
	
	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}
	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}
	
	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}
	
	/**
	 * Gets the date record.
	 *
	 * @return the date record
	 */
	public Date getDateRecord() {
		return dateRecord;
	}
	
	/**
	 * Sets the date record.
	 *
	 * @param dateRecord the new date record
	 */
	public void setDateRecord(Date dateRecord) {
		this.dateRecord = dateRecord;
	}
	
	/**
	 * Gets the statement issued.
	 *
	 * @return the statement issued
	 */
	public String getStatementIssued() {
		return statementIssued;
	}
	
	/**
	 * Sets the statement issued.
	 *
	 * @param statementIssued the new statement issued
	 */
	public void setStatementIssued(String statementIssued) {
		this.statementIssued = statementIssued;
	}
	
	/**
	 * Gets the statement request.
	 *
	 * @return the statement request
	 */
	public String getStatementRequest() {
		return statementRequest;
	}
	
	/**
	 * Sets the statement request.
	 *
	 * @param statementRequest the new statement request
	 */
	public void setStatementRequest(String statementRequest) {
		this.statementRequest = statementRequest;
	}
	
	/**
	 * Checks if is bl show indicators.
	 *
	 * @return true, if is bl show indicators
	 */
	public boolean isBlShowIndicators() {
		return blShowIndicators;
	}
	
	/**
	 * Sets the bl show indicators.
	 *
	 * @param blShowIndicators the new bl show indicators
	 */
	public void setBlShowIndicators(boolean blShowIndicators) {
		this.blShowIndicators = blShowIndicators;
	}
	
	/**
	 * Checks if is bl cash valuation.
	 *
	 * @return true, if is bl cash valuation
	 */
	public boolean isBlCashValuation() {
		return blCashValuation;
	}
	
	/**
	 * Sets the bl cash valuation.
	 *
	 * @param blCashValuation the new bl cash valuation
	 */
	public void setBlCashValuation(boolean blCashValuation) {
		this.blCashValuation = blCashValuation;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the parameter table currency.
	 *
	 * @return the parameter table currency
	 */
	public ParameterTable getParameterTableCurrency() {
		return parameterTableCurrency;
	}
	
	/**
	 * Sets the parameter table currency.
	 *
	 * @param parameterTableCurrency the new parameter table currency
	 */
	public void setParameterTableCurrency(ParameterTable parameterTableCurrency) {
		this.parameterTableCurrency = parameterTableCurrency;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the date dep tribunal.
	 *
	 * @return the date dep tribunal
	 */
	public Date getDateDepTribunal() {
		return dateDepTribunal;
	}
	
	/**
	 * Sets the date dep tribunal.
	 *
	 * @param dateDepTribunal the new date dep tribunal
	 */
	public void setDateDepTribunal(Date dateDepTribunal) {
		this.dateDepTribunal = dateDepTribunal;
	}
	
	/**
	 * Gets the notification date.
	 *
	 * @return the notification date
	 */
	public Date getNotificationDate() {
		return notificationDate;
	}
	
	/**
	 * Sets the notification date.
	 *
	 * @param notificationDate the new notification date
	 */
	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}
	
	/**
	 * Gets the limit last quotation.
	 *
	 * @return the limit last quotation
	 */
	public String getLimitLastQuotation() {
		return limitLastQuotation;
	}
	
	/**
	 * Sets the limit last quotation.
	 *
	 * @param limitLastQuotation the new limit last quotation
	 */
	public void setLimitLastQuotation(String limitLastQuotation) {
		this.limitLastQuotation = limitLastQuotation;
	}
	
	/**
	 * Gets the id negotiation mechanism.
	 *
	 * @return the id negotiation mechanism
	 */
	public Long getIdNegotiationMechanism() {
		return idNegotiationMechanism;
	}
	
	/**
	 * Sets the id negotiation mechanism.
	 *
	 * @param idNegotiationMechanism the new id negotiation mechanism
	 */
	public void setIdNegotiationMechanism(Long idNegotiationMechanism) {
		this.idNegotiationMechanism = idNegotiationMechanism;
	}
	
	/**
	 * Gets the lst negotiation mechanism.
	 *
	 * @return the lst negotiation mechanism
	 */
	public List<NegotiationMechanism> getLstNegotiationMechanism() {
		return lstNegotiationMechanism;
	}
	
	/**
	 * Sets the lst negotiation mechanism.
	 *
	 * @param lstNegotiationMechanism the new lst negotiation mechanism
	 */
	public void setLstNegotiationMechanism(
			List<NegotiationMechanism> lstNegotiationMechanism) {
		this.lstNegotiationMechanism = lstNegotiationMechanism;
	}
	
	/**
	 * Gets the document file.
	 *
	 * @return the document file
	 */
	public byte[] getDocumentFile() {
		return documentFile;
	}
	
	/**
	 * Sets the document file.
	 *
	 * @param documentFile the new document file
	 */
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
	
	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Checks if is bl opposition.
	 *
	 * @return true, if is bl opposition
	 */
	public boolean isBlOpposition() {
		return blOpposition;
	}
	
	/**
	 * Sets the bl opposition.
	 *
	 * @param blOpposition the new bl opposition
	 */
	public void setBlOpposition(boolean blOpposition) {
		this.blOpposition = blOpposition;
	}
	
	/**
	 * Gets the bd exchange rate.
	 *
	 * @return the bd exchange rate
	 */
	public BigDecimal getBdExchangeRate() {
		return bdExchangeRate;
	}
	
	/**
	 * Sets the bd exchange rate.
	 *
	 * @param bdExchangeRate the new bd exchange rate
	 */
	public void setBdExchangeRate(BigDecimal bdExchangeRate) {
		this.bdExchangeRate = bdExchangeRate;
	}
	
	/**
	 * Gets the block valoritation amount.
	 *
	 * @return the block valoritation amount
	 */
	public BigDecimal getBlockValoritationAmount() {
		return blockValoritationAmount;
	}
	
	/**
	 * Sets the block valoritation amount.
	 *
	 * @param blockValoritationAmount the new block valoritation amount
	 */
	public void setBlockValoritationAmount(BigDecimal blockValoritationAmount) {
		this.blockValoritationAmount = blockValoritationAmount;
	}
	
	/**
	 * Checks if is bl no opposition.
	 *
	 * @return true, if is bl no opposition
	 */
	public boolean isBlNoOpposition() {
		return blNoOpposition;
	}
	
	/**
	 * Sets the bl no opposition.
	 *
	 * @param blNoOpposition the new bl no opposition
	 */
	public void setBlNoOpposition(boolean blNoOpposition) {
		this.blNoOpposition = blNoOpposition;
	}
	
	/**
	 * Gets the others.
	 *
	 * @return the others
	 */
	public String getOthers() {
		return others;
	}
	
	/**
	 * Sets the others.
	 *
	 * @param others the new others
	 */
	public void setOthers(String others) {
		this.others = others;
	}
	
	/**
	 * Checks if is bl others.
	 *
	 * @return true, if is bl others
	 */
	public boolean isBlOthers() {
		return blOthers;
	}
	
	/**
	 * Sets the bl others.
	 *
	 * @param blOthers the new bl others
	 */
	public void setBlOthers(boolean blOthers) {
		this.blOthers = blOthers;
	}
	
	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}
	
	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the ind registered depositary.
	 *
	 * @return the ind registered depositary
	 */
	public Integer getIndRegisteredDepositary() {
		return indRegisteredDepositary;
	}
	
	/**
	 * Sets the ind registered depositary.
	 *
	 * @param indRegisteredDepositary the new ind registered depositary
	 */
	public void setIndRegisteredDepositary(Integer indRegisteredDepositary) {
		this.indRegisteredDepositary = indRegisteredDepositary;
	}
	
	/**
	 * Checks if is check authority restricted.
	 *
	 * @return true, if is check authority restricted
	 */
	public boolean isCheckAuthorityRestricted() {
		return checkAuthorityRestricted;
	}
	
	/**
	 * Sets the check authority restricted.
	 *
	 * @param checkAuthorityRestricted the new check authority restricted
	 */
	public void setCheckAuthorityRestricted(boolean checkAuthorityRestricted) {
		this.checkAuthorityRestricted = checkAuthorityRestricted;
	}
	
	/**
	 * Checks if is show authority restricted.
	 *
	 * @return true, if is show authority restricted
	 */
	public boolean isShowAuthorityRestricted() {
		return showAuthorityRestricted;
	}
	
	/**
	 * Sets the show authority restricted.
	 *
	 * @param showAuthorityRestricted the new show authority restricted
	 */
	public void setShowAuthorityRestricted(boolean showAuthorityRestricted) {
		this.showAuthorityRestricted = showAuthorityRestricted;
	}
	
	/**
	 * Gets the circular number.
	 *
	 * @return the circular number
	 */
	public String getCircularNumber() {
		return circularNumber;
	}
	
	/**
	 * Sets the circular number.
	 *
	 * @param circularNumber the new circular number
	 */
	public void setCircularNumber(String circularNumber) {
		this.circularNumber = circularNumber;
	}
	
	/**
	 * Gets the fiscal procedence.
	 *
	 * @return the fiscal procedence
	 */
	public String getFiscalProcedence() {
		return fiscalProcedence;
	}
	
	/**
	 * Sets the fiscal procedence.
	 *
	 * @param fiscalProcedence the new fiscal procedence
	 */
	public void setFiscalProcedence(String fiscalProcedence) {
		this.fiscalProcedence = fiscalProcedence;
	}
	
	/**
	 * Gets the fiscal currency.
	 *
	 * @return the fiscal currency
	 */
	public Integer getFiscalCurrency() {
		return fiscalCurrency;
	}
	
	/**
	 * Sets the fiscal currency.
	 *
	 * @param fiscalCurrency the new fiscal currency
	 */
	public void setFiscalCurrency(Integer fiscalCurrency) {
		this.fiscalCurrency = fiscalCurrency;
	}
	
	/**
	 * Gets the fiscal amount.
	 *
	 * @return the fiscal amount
	 */
	public BigDecimal getFiscalAmount() {
		return fiscalAmount;
	}
	
	/**
	 * Sets the fiscal amount.
	 *
	 * @param fiscalAmount the new fiscal amount
	 */
	public void setFiscalAmount(BigDecimal fiscalAmount) {
		this.fiscalAmount = fiscalAmount;
	}
	
	/**
	 * Gets the fiscal charge number.
	 *
	 * @return the fiscal charge number
	 */
	public String getFiscalChargeNumber() {
		return fiscalChargeNumber;
	}
	
	/**
	 * Sets the fiscal charge number.
	 *
	 * @param fiscalChargeNumber the new fiscal charge number
	 */
	public void setFiscalChargeNumber(String fiscalChargeNumber) {
		this.fiscalChargeNumber = fiscalChargeNumber;
	}
	
	/**
	 * Gets the fiscal process.
	 *
	 * @return the fiscal process
	 */
	public String getFiscalProcess() {
		return fiscalProcess;
	}
	
	/**
	 * Sets the fiscal process.
	 *
	 * @param fiscalProcess the new fiscal process
	 */
	public void setFiscalProcess(String fiscalProcess) {
		this.fiscalProcess = fiscalProcess;
	}
	
	/**
	 * Gets the fiscal authority.
	 *
	 * @return the fiscal authority
	 */
	public String getFiscalAuthority() {
		return fiscalAuthority;
	}
	
	/**
	 * Sets the fiscal authority.
	 *
	 * @param fiscalAuthority the new fiscal authority
	 */
	public void setFiscalAuthority(String fiscalAuthority) {
		this.fiscalAuthority = fiscalAuthority;
	}
}