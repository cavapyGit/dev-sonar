package com.pradera.custody.affectation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.Query;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.affectation.to.BlockMarketFactTO;
import com.pradera.custody.affectation.to.HolderAccountBalanceTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityType;

// TODO: Auto-generated Javadoc
/**
 * The Class RequestAffectationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AffectationBanAditional extends CrudDaoServiceBean{

	/** The request affectation service bean. */
	@EJB
	RequestAffectationServiceBean requestAffectationServiceBean;

	/**
	 * Order balances by criteria.
	 *
	 * @param balances the balances
	 * @param currency the currency
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> orderBalancesByBusinessCriteria(List<HolderAccountBalanceTO> balances, final Integer currency) throws ServiceException{
		Comparator<HolderAccountBalanceTO> comparator = new Comparator<HolderAccountBalanceTO>(){
			@Override
			public int compare(HolderAccountBalanceTO o1, HolderAccountBalanceTO o2) 
			{	
				//comparamos por moneda
				int resultado = compareByCurrency(o1, o2, currency);
				if ( resultado != 0 ) { return resultado; }
				
		        /*ORDENAMOS LOS SALDOS POR TIPO DE INSTRUMENTO*/
		        resultado = compareByInstrumentType(o1, o2);
		        if ( resultado != 0 ) { return resultado; }
		
		        //comparamos por fecha de expiracion
				resultado = compareByExpirationDate(o1, o2);
				if ( resultado != 0 ) { return resultado; }
		        
		        //comparamos por precio ultima cotizacion (VN)
				resultado = compareByLastQuotation(o1, o2);
				if ( resultado != 0 ) { return resultado; }
				
				return resultado;
			}
		};
		Collections.sort(balances,comparator);
		return balances;
	}
	
	/**
	 * Compare by currency.
	 *
	 * @param b the b
	 * @param a the a
	 * @param currency the currency
	 * @return the integer
	 */
	private Integer compareByCurrency(HolderAccountBalanceTO b, HolderAccountBalanceTO a, Integer currency)
	{
		if (!a.getCurrency().equals(b.getCurrency())) {
			if (a.getCurrency().equals(currency)) {
				return 1;
			}
			if (b.getCurrency().equals(currency)) {
				return -1;
			}
		}
		return BooleanType.NO.getCode();
	}
	
	/**
	 * Compare by expiration date.
	 *
	 * @param b the b
	 * @param a the a
	 * @return the integer
	 */
	private Integer compareByExpirationDate(HolderAccountBalanceTO b, HolderAccountBalanceTO a)
	{
		if (Validations.validateIsNotNull(b.getExpirationDate()) && Validations.validateIsNotNull(a.getExpirationDate())) {
			if (b.getExpirationDate().compareTo(a.getExpirationDate()) > 0) {
				return -1;
			}
			if (b.getExpirationDate().compareTo(a.getExpirationDate()) < 0) {
				return 1;
			}
		}
		return BooleanType.NO.getCode();
	}
	
	/**
	 * Compare by last quotation.
	 *
	 * @param b the b
	 * @param a the a
	 * @return the integer
	 */
	private Integer compareByLastQuotation(HolderAccountBalanceTO b, HolderAccountBalanceTO a)
	{
		return b.getLastQuotation().compareTo(a.getLastQuotation());
	}
	
	/**
	 * Compare by security type.
	 *
	 * @param b the b
	 * @param a the a
	 * @return the integer
	 */
	private Integer compareBySecurityType(HolderAccountBalanceTO b, HolderAccountBalanceTO a){		
		if(!a.getIssuanceSecType().equals(b.getIssuanceSecType())){
			if(a.getIssuanceSecType().equals(SecurityType.PUBLIC.getCode())){
				return 1;
			}
			if(b.getIssuanceSecType().equals(SecurityType.PUBLIC.getCode())){
				return -1;
			}
		}
		return BooleanType.NO.getCode();
	}
	
	/**
	 * Compare by instrument type.
	 *
	 * @param b the b
	 * @param a the a
	 * @return the integer
	 */
	private Integer compareByInstrumentType(HolderAccountBalanceTO b, HolderAccountBalanceTO a){
		if(!a.getInstrumentType().equals(b.getInstrumentType())){
			if(a.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				return 1;
			}
			if(b.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				return -1;
			}
		}
		return BooleanType.NO.getCode();
	}
	
	/**
	 * Compare by clasification type.
	 *
	 * @param b the b
	 * @param a the a
	 * @return the integer
	 */
	private Integer compareByClasificationType(HolderAccountBalanceTO b, HolderAccountBalanceTO a){
		if(a.getCredinRatesScale()!=null && b.getCredinRatesScale()!=null){
			return Integer.compare(a.getCredinRatesScale(), b.getCredinRatesScale());
		}
		return BooleanType.NO.getCode(); 
	}
	
	/**
	 * Populate balance by amount.
	 *
	 * @param balance the balance
	 * @param amount the amount
	 * @param isReblock the is reblock
	 * @return the holder account balance to
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalanceTO populateBalanceByAmount(HolderAccountBalanceTO balance, BigDecimal amount, Boolean isReblock) throws ServiceException{
		BigDecimal quantityBlock = BigDecimal.ZERO;
		if(balance.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
			quantityBlock = amount.divide(balance.getNominalValue(),0,BigDecimal.ROUND_HALF_DOWN);
		}else if(balance.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
			quantityBlock = amount.divide(balance.getLastQuotation());
		}
		if(isReblock){
			balance.setQuantityBlockBalance(balance.getQuantityBlockBalance().add(quantityBlock));
		}else{
			balance.setQuantityBlockBalance(quantityBlock);
		}
		return balance;
	}
	
	/**
	 * Convert by exchange rate.
	 *
	 * @param amountBalance the amount balance
	 * @param currencyOperation the currency operation
	 * @param currencyBalance the currency balance
	 * @param exchangeRate the exchange rate
	 * @return the big decimal
	 */
	public BigDecimal convertByExchangeRate(BigDecimal amountBalance, Integer currencyOperation, Integer currencyBalance, BigDecimal exchangeRate){
		if(currencyBalance.equals(currencyOperation)){
			return amountBalance;
		}
		
		switch(CurrencyType.get(currencyOperation)){
			case USD:	return amountBalance.divide(exchangeRate,0,BigDecimal.ROUND_DOWN);
			case PYG:	return amountBalance.multiply(exchangeRate);
		default:
			break; 	
		}
		
		return BigDecimal.ZERO;
	}
	
	/**
	 * Generate block market fact.
	 *
	 * @param balanceList the balance list
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> generateBlockMarketFact(List<HolderAccountBalanceTO> balanceList) throws ServiceException{
		for(HolderAccountBalanceTO balance: balanceList){
			balance.setBlockMarketFactDetail(new ArrayList<BlockMarketFactTO>());
			
			if(balance.getQuantityBlockBalance()!=null && !balance.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
				List<HolderMarketFactBalance> marketFactBalances = findHolderMarketFactFromBalance(balance);
				/*VERIFICAMOS QUE EL SALDO TENGA HECHOS DE MERCADO*/
				if(marketFactBalances!=null && !marketFactBalances.isEmpty()){
					
					/*VERIFICAMOS LOS HECHOS DE MERCADO CON SALDO DISPONIBLE*/
					BigDecimal quantityOfBalanceToBlock = balance.getQuantityBlockBalance();
					for(HolderMarketFactBalance availabMarketFact: getMarketFactWithAvailableBalance(marketFactBalances)){
						BlockMarketFactTO blockMarketFact = new BlockMarketFactTO();
						blockMarketFact.setIdHolderMarketPk(availabMarketFact.getIdMarketfactBalancePk());
						blockMarketFact.setMarketDate(availabMarketFact.getMarketDate());
						blockMarketFact.setMarketPrice(availabMarketFact.getMarketPrice());
						blockMarketFact.setMarketRate(availabMarketFact.getMarketRate());
						
						if(quantityOfBalanceToBlock.compareTo(availabMarketFact.getAvailableBalance())>=0){
							blockMarketFact.setBlockAmount(availabMarketFact.getAvailableBalance());
							quantityOfBalanceToBlock = quantityOfBalanceToBlock.subtract(availabMarketFact.getAvailableBalance());
						}else{
							blockMarketFact.setBlockAmount(quantityOfBalanceToBlock);
							quantityOfBalanceToBlock = BigDecimal.ZERO;
						}
						balance.getBlockMarketFactDetail().add(blockMarketFact);
					}
					
					/*VERIFICAMOS LOS HECHOS DE MERCADO CON SALDO BLOQUEADOS, PARA LOS REBLOQUEOS*/
					if(!quantityOfBalanceToBlock.equals(BigDecimal.ZERO)){
						for(HolderMarketFactBalance marketFact: getMarketFactWithBlockBalance(marketFactBalances)){
							BlockMarketFactTO blockMarketFact = new BlockMarketFactTO();
							blockMarketFact.setIdHolderMarketPk(marketFact.getIdMarketfactBalancePk());
							blockMarketFact.setMarketDate(marketFact.getMarketDate());
							blockMarketFact.setMarketPrice(marketFact.getMarketPrice());
							blockMarketFact.setMarketRate(marketFact.getMarketRate());
							
							if(quantityOfBalanceToBlock.compareTo(marketFact.getAvailableBalance())>=0){
								BigDecimal totalBlock = marketFact.getBanBalance().add(marketFact.getPawnBalance()).add(marketFact.getOtherBlockBalance());
								blockMarketFact.setBlockAmount(totalBlock);
								quantityOfBalanceToBlock = quantityOfBalanceToBlock.subtract(totalBlock);
							}else{
								blockMarketFact.setBlockAmount(quantityOfBalanceToBlock);
								quantityOfBalanceToBlock = BigDecimal.ZERO;
							}
							balance.getBlockMarketFactDetail().add(blockMarketFact);
						}
					}
				}else{/*LA DATA ESTA INCONSISTENTE*/
					throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
				}
			}
		}
		return balanceList;
	}
	
	/**
	 * Gets the market fact with available balance.
	 *
	 * @param marketFactList the market fact list
	 * @return the market fact with available balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderMarketFactBalance> getMarketFactWithAvailableBalance(List<HolderMarketFactBalance> marketFactList) throws ServiceException{
		List<HolderMarketFactBalance> marketFactBalanceList = new ArrayList<HolderMarketFactBalance>();
		for(HolderMarketFactBalance marketFact: marketFactList){
			if(marketFact.getAvailableBalance()!=null && !marketFact.getAvailableBalance().equals(BigDecimal.ZERO)){
				marketFactBalanceList.add(marketFact);
			}
		}
		return marketFactBalanceList;
	}
	
	/**
	 * Gets the market fact with block balance.
	 *
	 * @param marketFactList the market fact list
	 * @return the market fact with block balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderMarketFactBalance> getMarketFactWithBlockBalance(List<HolderMarketFactBalance> marketFactList) throws ServiceException{
		List<HolderMarketFactBalance> marketFactBalanceList = new ArrayList<HolderMarketFactBalance>();
		for(HolderMarketFactBalance marketFact: marketFactList){
			if(marketFact.getAvailableBalance()!=null &&( 
														!marketFact.getPawnBalance().equals(BigDecimal.ZERO) || 
														!marketFact.getOtherBlockBalance().equals(BigDecimal.ZERO) || 
														!marketFact.getBanBalance().equals(BigDecimal.ZERO))){
				marketFactBalanceList.add(marketFact);
			}
		}
		return marketFactBalanceList;
	}
	
	/**
	 * Find holder market fact from balance.
	 *
	 * @param balance the balance
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> findHolderMarketFactFromBalance(HolderAccountBalanceTO balance) throws ServiceException{
		StringBuilder querySql = new StringBuilder();
		querySql.append("Select mark From HolderMarketFactBalance mark	");
		querySql.append("Where	mark.participant.idParticipantPk 	 = :participantId 		And	");
		querySql.append("		mark.holderAccount.idHolderAccountPk = :accountId 			And	");
		querySql.append("		mark.security.idSecurityCodePk 		 = :securityCode 		And	");
		querySql.append("		mark.indActive 		 				 = :booleanOne 				");
		querySql.append("Order By mark.marketDate												");
		
		Query query = em.createQuery(querySql.toString());
		query.setParameter("participantId", balance.getIdParticipantPk());
		query.setParameter("accountId", balance.getIdHolderAccountPk());
		query.setParameter("securityCode", balance.getIdSecurityCodePk());
		query.setParameter("booleanOne", BooleanType.YES.getCode());
		
		return query.getResultList();
	}
	
	
	/**
	 * Apply exchange rate quotation.
	 *
	 * @param quotation the quotation
	 * @param blockCurrency the block currency
	 * @param securitiesCurrency the securities currency
	 * @param mpExchangeRate the mp exchange rate
	 * @return the big decimal
	 */
	public BigDecimal applyExchangeRateQuotation(BigDecimal quotation, Integer blockCurrency, Integer securitiesCurrency, Map<Integer,BigDecimal> mpExchangeRate)
	{
		BigDecimal quotationExchange= quotation;
		if (!blockCurrency.equals(securitiesCurrency)) {
			if (!CurrencyType.PYG.getCode().equals(securitiesCurrency)) {
				//we change the quotation to BOB currency
				quotationExchange = quotationExchange.multiply(mpExchangeRate.get(securitiesCurrency));
				//the securities currency was changed according the rate
				quotationExchange= applyExchangeRateQuotation(quotationExchange, blockCurrency, CurrencyType.PYG.getCode(), mpExchangeRate);
			} else if (!CurrencyType.PYG.getCode().equals(blockCurrency)) {
				quotationExchange = quotationExchange.divide(mpExchangeRate.get(blockCurrency),8,BigDecimal.ROUND_HALF_UP);
			}
		}
		return quotationExchange;
	}
}