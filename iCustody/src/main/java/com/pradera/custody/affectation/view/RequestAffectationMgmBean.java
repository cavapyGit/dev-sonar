package com.pradera.custody.affectation.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.billing.service.BillingComponentService;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.operation.to.MechanismOperationTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.accreditationcertificates.facade.AccreditationCertificatesServiceFacade;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;
import com.pradera.custody.affectation.to.BlockMarketFactTO;
import com.pradera.custody.affectation.to.FileAffectationTO;
import com.pradera.custody.affectation.to.HolderAccountBalanceHeaderTO;
import com.pradera.custody.affectation.to.HolderAccountBalanceTO;
import com.pradera.custody.affectation.to.HolderAccountTO;
import com.pradera.custody.affectation.to.RegisterBanOrOthersTO;
import com.pradera.custody.affectation.to.RegisterPawnOrReserveTO;
import com.pradera.custody.affectation.to.RequestAffectationDetailTO;
import com.pradera.custody.affectation.to.RequestAffectationTO;
import com.pradera.custody.affectation.to.SearchRequestAffectationTO;
import com.pradera.custody.reversals.to.FileReversalsTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationFormType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.RequestAffectationStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class RequestAffectationMgmBean.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/
@LoggerCreateBean
@DepositaryWebBean
public class RequestAffectationMgmBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;	
	
	/** The request affectation service facade. */
	@EJB
	RequestAffectationServiceFacade requestAffectationServiceFacade;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	@Inject
	private AccreditationCertificatesServiceFacade accreditationCertificatesServiceFacade;
		
	/** The request affec deta to. */
	private RequestAffectationDetailTO requestAffecDetaTO;
	
	/** The search request affectation to. */
	private SearchRequestAffectationTO searchRequestAffectationTO;
	
	/** The register pawn or reserve to. */
	private RegisterPawnOrReserveTO registerPawnOrReserveTO;
	
	/** The register ban or others to. */
	private RegisterBanOrOthersTO registerBanOrOthersTO;
	
	/** The lst request affectation. */
	private GenericDataModel<RequestAffectationTO> lstRequestAffectation;
	
	/** The request ban or others. */
	private static final Integer REQUEST_BAN_OR_OTHERS_OR_OPPOSITION = 1;
	
	/** The request panw or reserve. */
	private static final Integer REQUEST_PANW_OR_RESERVE = 2;

	/** The id participant. */
	private Long idParticipant;
	
	/** The flag participant. */
	private boolean flagParticipant;
	
	/** The flag issuer block. */
	private boolean flagIssuerBlock;
	
	/** The request affectation to. */
	private RequestAffectationTO requestAffectationTO;	
	
	/** The list reported balance. */
	private List<MechanismOperationTO> lstReportedBalance;
	
	/** The log. */
	Logger log = Logger.getLogger(RequestAffectationMgmBean.class.getName());
	
	/** The bl confirm. */
	private boolean blConfirm;
	
	/** The bl reject. */
	private boolean blReject;
	
	/** The bl approve. */
	private boolean blApprove;
	
	/** The bl annul. */
	private boolean blAnnul;
	
	/** The blview detail. */
	private boolean blviewDetail;
	
	//--/** The blview detail. */
	private boolean blreturn = true;
	
	public boolean isBlreturn() {
		return blreturn;
	}
	public void setBlreturn(boolean blreturn) {
		this.blreturn = blreturn;
	}
	//--

	/** The Constant STR_VIEW_AFECTATION_MAPPING. */
	private static final String STR_VIEW_AFECTATION_MAPPING = "viewAfectacion";
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The bl no result. */
	private boolean blNoResult; 
	
	/** The rnt bcr. */
	@Inject @Configurable
	Long rntBCR;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The max days of custody request. */
	@Inject @Configurable
	Integer maxDaysOfCustodyRequest;
	
	/** The batch service facade. */
	@EJB
	BatchProcessServiceFacade batchServiceFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The handle market fact. */
	private Boolean handleMarketFact;
	
	/** The list file affectation to. */
	private List<FileAffectationTO> listFileAffectationTO;
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	private HolderAccountBalanceTO objHolAccBalTOTemp;
	
	private HolderAccountBalanceHeaderTO objHolAccBalHeTOTemp;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		//Se inicializa los TO
		searchRequestAffectationTO = new SearchRequestAffectationTO();	
		searchRequestAffectationTO.setHolder(new Holder());
		searchRequestAffectationTO.setBlockEntity(new BlockEntity());
		searchRequestAffectationTO.setParameterTableLevelAffectation(new ParameterTable());
		searchRequestAffectationTO.setParameterTableState(new ParameterTable());
		searchRequestAffectationTO.setParameterTableTypeAffectation(new ParameterTable());
		searchRequestAffectationTO.setLstMotiveReject(new ArrayList<ParameterTable>()); 
		//Se setea la fecha del sistema
		searchRequestAffectationTO.setInitialDate(CommonsUtilities.currentDate());
		searchRequestAffectationTO.setEndDate(CommonsUtilities.currentDate());
		//Se limpia la lista
		lstRequestAffectation = null;
		requestAffectationTO = null;
		blNoResult = false;
		//LLena los combos de la busqueda de solicitudes de afectacion
		searchRequestAffectationTO.setSearchType(GeneralConstants.TWO_VALUE_INTEGER);
		searchRequestAffectationTO.setBlDefaultSearch(false);
		fillCombos();
		
		try {
			loadIndMarketFact();
			flagParticipant = false;
			flagIssuerBlock = false;
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				flagParticipant = true;
				idParticipant = userInfo.getUserAccountSession().getParticipantCode();				
			}else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				flagIssuerBlock = true;
			}
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load ind market fact.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadIndMarketFact() throws ServiceException{
		// TODO Auto-generated method stub
		handleMarketFact = requestAffectationServiceFacade.marketFactBusinessProcess(BusinessProcessType.SECURITY_BLOCK_PROCESS.getCode());
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		init();
	}
	
	/**
	 * Validate block entity.
	 */
	public void validateBlockEntity(){
//		BlockEntity blockEntity = registerPawnOrReserveTO.getBlockEntity();
	}
	
	/**
	 * Search by.
	 */
	public void searchBy(){
		JSFUtilities.resetViewRoot();
		if(GeneralConstants.ONE_VALUE_INTEGER.equals(searchRequestAffectationTO.getSearchType())){
			searchRequestAffectationTO.setBlDefaultSearch(true);			
			searchRequestAffectationTO.setHolder(new Holder());
			searchRequestAffectationTO.setBlockEntity(new BlockEntity());
			searchRequestAffectationTO.getParameterTableLevelAffectation().setParameterTablePk(null);
			searchRequestAffectationTO.getParameterTableState().setParameterTablePk(null);
			searchRequestAffectationTO.getParameterTableTypeAffectation().setParameterTablePk(null);
			searchRequestAffectationTO.setInitialDate(CommonsUtilities.currentDate());
			searchRequestAffectationTO.setEndDate(CommonsUtilities.currentDate());
			searchRequestAffectationTO.setNumberRecord(null);
		}else{
			searchRequestAffectationTO.setBlDefaultSearch(false);
			searchRequestAffectationTO.setIdBlockRequestPk(null);
		}
	}
	
	/**
	 * Find request affectations.
	 */
	@LoggerAuditWeb
	public void findRequestAffectations(){
		try{
						
			//Seteamos a null el objeto seleccionado de la lista
			requestAffectationTO = null;
			searchRequestAffectationTO.setIdIssuer(userInfo.getUserAccountSession().getIssuerCode());
			searchRequestAffectationTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
			//Recuperamos todos las solicitudes de afectaciones		
			List<RequestAffectationTO> lstRequestAffecList = requestAffectationServiceFacade.getListRequestAffectation(searchRequestAffectationTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstRequestAffecList)){
				lstRequestAffectation = new GenericDataModel<RequestAffectationTO>(lstRequestAffecList);
				showPrivilegeButtons();
				blNoResult = false;
			}else{
				lstRequestAffectation = null;
				blNoResult = true;
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnApproveView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * View request detail.
	 *
	 * @param event the event
	 */
	public void viewRequestDetail(javax.faces.event.ActionEvent event){		
		try{
			RequestAffectationTO objRequestAffectationTO = new RequestAffectationTO();
			//Recuperamos el objeto del datamodel
			objRequestAffectationTO = (RequestAffectationTO)event.getComponent().getAttributes().get("requestAffectation");
			setFlagsFalse();
			this.requestAffectationTO= objRequestAffectationTO;
			objRequestAffectationTO.setIdIssuer(userInfo.getUserAccountSession().getIssuerCode());
			requestAffecDetaTO = requestAffectationServiceFacade.findAffectationRequestDetail(objRequestAffectationTO);
			this.blviewDetail=true;
		    listFileAffectationTO = new ArrayList<FileAffectationTO>();
		    FileAffectationTO fileAffectationTO = new FileAffectationTO();
		    fileAffectationTO.setDocumentFile(requestAffecDetaTO.getDocumentFile());
		    fileAffectationTO.setFileName(requestAffecDetaTO.getFileName());
		    if(requestAffecDetaTO.getObjBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())){
		    	objRequestAffectationTO.setBlBanBlock(true);
		    	objRequestAffectationTO.setBlPawnBlock(false);
		    }
		    else if(requestAffecDetaTO.getObjBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())){
		    	objRequestAffectationTO.setBlBanBlock(false);
		    	objRequestAffectationTO.setBlPawnBlock(true);
		    }
		    else{
		    	objRequestAffectationTO.setBlBanBlock(false);
		    	objRequestAffectationTO.setBlPawnBlock(false);
		    }
		    String extensionFile = "";
		    if(requestAffecDetaTO.getFileName()!=null){
		    	extensionFile = requestAffecDetaTO.getFileName().toLowerCase().substring(requestAffecDetaTO.getFileName().lastIndexOf(".")+1,requestAffecDetaTO.getFileName().length());
				fileAffectationTO.setFileExtension(extensionFile.toUpperCase());
				fileAffectationTO.setFileSize(Integer.valueOf(requestAffecDetaTO.getDocumentFile().length).longValue()/1000L);
				listFileAffectationTO.add(fileAffectationTO);
		    }		
			
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	
	/**
	 * Register new ban or others.
	 */	
	public void registerNewBanOrOthers(){
						
		//Inicializamos los objetos necesarios para la creacion de una solicitud de afection de tipo embargo, otros u oposicion
		registerBanOrOthersTO = new RegisterBanOrOthersTO();
		registerBanOrOthersTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		registerBanOrOthersTO.setBlockEntity(new BlockEntity());
		registerBanOrOthersTO.setHolder(new Holder());
		registerBanOrOthersTO.setParameterTableBlockMotive(new ParameterTable());
		registerBanOrOthersTO.setParameterTableAffectationForm(new ParameterTable());
		registerBanOrOthersTO.getParameterTableBlockMotive().setParameterTablePk(AffectationType.BAN.getCode());
		registerBanOrOthersTO.setParameterTableCurrency(new ParameterTable());
		if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
			registerBanOrOthersTO.setIndRegisteredDepositary(BooleanType.YES.getCode());
		} else {
			registerBanOrOthersTO.setIndRegisteredDepositary(BooleanType.NO.getCode());
		}
		listFileAffectationTO = new ArrayList<FileAffectationTO>();
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();	
		parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			registerBanOrOthersTO.setLstAffectationForm(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			if (userInfo.getUserAccountSession().isIssuerInstitucion() 
					||userInfo.getUserAccountSession().isIssuerDpfInstitucion()) {
				//to the issuers the quantity block is allowed only 
				registerBanOrOthersTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());
			}
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
			for(ParameterTable paramTab:generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				if(CurrencyType.USD.getCode().equals(paramTab.getParameterTablePk())
						|| CurrencyType.PYG.getCode().equals(paramTab.getParameterTablePk()))
					lstTemp.add(paramTab);
			}
			registerBanOrOthersTO.setLstCurrency(lstTemp);
			registerBanOrOthersTO.setLstNegotiationMechanism(requestAffectationServiceFacade.getNegotiationMechanism());
			listHolidays();
		} catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		//Se muestra cantidad exacta por default
		registerBanOrOthersTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());
		makeListTypeRequest(REQUEST_BAN_OR_OTHERS_OR_OPPOSITION);
		registerBanOrOthersTO.setDateRecord(CommonsUtilities.currentDate());
		registerBanOrOthersTO.setShowAuthorityRestricted(true);
		showIndicatorsFor();
	}
	
	/**
	 * Show indicators for.
	 */
	public void showIndicatorsFor(){
		if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
			registerBanOrOthersTO.setBlShowIndicators(true);
			registerBanOrOthersTO.setBlOpposition(false);
			registerBanOrOthersTO.setBlNoOpposition(true);
			registerBanOrOthersTO.setBlOthers(false);
			clearIndicatorsForBan();
			registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
			registerBanOrOthersTO.setLstHolderAccount(null);
			registerBanOrOthersTO.setHolder(new Holder());
			registerBanOrOthersTO.setShowAuthorityRestricted(true);
			//Se muestra cantidad exacta por default
			registerBanOrOthersTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());
		}else if(AffectationType.OPPOSITION.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
			registerBanOrOthersTO.setBlShowIndicators(false);
			registerBanOrOthersTO.setBlOpposition(true);
			registerBanOrOthersTO.setBlNoOpposition(false);
			registerBanOrOthersTO.setBlOthers(false);
			registerBanOrOthersTO.setShowAuthorityRestricted(false);
			clearIndicatorsForBan();
			clearForOpposition();
		}else if(AffectationType.OTHERS.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
			registerBanOrOthersTO.setObservations(null);
			registerBanOrOthersTO.setBlShowIndicators(false);
			registerBanOrOthersTO.setBlOpposition(false);
			registerBanOrOthersTO.setBlNoOpposition(true);
			registerBanOrOthersTO.setBlOthers(true);
			registerBanOrOthersTO.setShowAuthorityRestricted(false);
			//Se muestra cantidad exacta por default
			registerBanOrOthersTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());			
			clearIndicatorsForBan();
			registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
			registerBanOrOthersTO.setLstHolderAccount(null);
			registerBanOrOthersTO.setHolder(new Holder());
			registerBanOrOthersTO.setShowAuthorityRestricted(false);
		}else{
			registerBanOrOthersTO.setBlShowIndicators(false);
			registerBanOrOthersTO.setBlOpposition(false);
			registerBanOrOthersTO.setBlNoOpposition(false);
			registerBanOrOthersTO.setBlOthers(false);
			//Se muestra cantidad exacta por default
			registerBanOrOthersTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());			
			clearIndicatorsForBan();
			registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
			registerBanOrOthersTO.setLstHolderAccount(null);
			registerBanOrOthersTO.setHolder(new Holder());
			registerBanOrOthersTO.setShowAuthorityRestricted(false);
		}
		showParametersByAffectationForm();
	}
	
	/**
	 * Clear for opposition.
	 */
	public void clearForOpposition(){
		registerBanOrOthersTO.setHolder(new Holder());
		registerBanOrOthersTO.setLstHolderAccount(null);
		registerBanOrOthersTO.getParameterTableAffectationForm().setParameterTablePk(null);
		showParametersByAffectationForm();
	}
	
	/**
	 * Show parameters by affectation form.
	 */
	public void showParametersByAffectationForm(){
		if(AffectationFormType.EXACT_AMOUNT.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
			registerBanOrOthersTO.setBlCashValuation(false);
			clearForChangeAffectationForm();
		}else if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
			registerBanOrOthersTO.setBlCashValuation(true);
			clearForChangeAffectationForm();
		}else{
			registerBanOrOthersTO.setBlCashValuation(false);
			clearForChangeAffectationForm();
		}
	}
	
	/**
	 * Show parameters by affectation pawn form.
	 */
	public void showParametersByAffectationPawnForm(){
		if(AffectationFormType.EXACT_AMOUNT.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
			registerPawnOrReserveTO.setBlCashValuation(false);
			clearForChangeAffectationPawnForm();
		}else if(AffectationFormType.CASH_VALUATION.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
			registerPawnOrReserveTO.setBlCashValuation(true);
			clearForChangeAffectationPawnForm();
		}else{
			registerPawnOrReserveTO.setBlCashValuation(false);
			clearForChangeAffectationPawnForm();
		}
	}
	
	/**
	 * Clear for change affectation form.
	 */
	private void clearForChangeAffectationForm(){
		registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
		registerBanOrOthersTO.setLimitLastQuotation(null);
		registerBanOrOthersTO.setIdNegotiationMechanism(null);
		registerBanOrOthersTO.setAmount(null);
		registerBanOrOthersTO.getParameterTableCurrency().setParameterTablePk(null);
		registerBanOrOthersTO.setLstHolderAccount(null);
		registerBanOrOthersTO.setHolder(new Holder());
		registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
		registerBanOrOthersTO.setBdExchangeRate(null);
	}
	
	/**
	 * Clear for change affectation pawn form.
	 */
	private void clearForChangeAffectationPawnForm(){
		registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);		
		//registerPawnOrReserveTO.setIdNegotiationMechanism(null);
		registerPawnOrReserveTO.setAmount(null);
		registerPawnOrReserveTO.getParameterTableCurrency().setParameterTablePk(null);
		registerPawnOrReserveTO.setLstHolderAccount(null);
		registerPawnOrReserveTO.setHolder(new Holder());
		registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
		registerPawnOrReserveTO.setBdExchangeRate(null);
	}
	
	/**
	 * Clear indicators for ban.
	 */
	private void clearIndicatorsForBan(){
		registerBanOrOthersTO.setStatementIssued(null);
		registerBanOrOthersTO.setStatementRequest(BooleanType.NO.getCode().toString());
		registerBanOrOthersTO.setDateDepTribunal(null);
		registerBanOrOthersTO.setNotificationDate(null);
	}
	
	/**
	 * Register new pawn or reserve.
	 */
	public void registerNewPawnOrReserve(){
				
		//Inicializamos los objetos necesarios para la creacion de una solicitud de afectacion de tipo prenda o encaje
		registerPawnOrReserveTO = new RegisterPawnOrReserveTO();
		registerPawnOrReserveTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
		registerPawnOrReserveTO.setParameterTableTypeRequest(new ParameterTable());
		registerPawnOrReserveTO.getParameterTableTypeRequest().setParameterTablePk(AffectationType.PAWN.getCode());
		registerPawnOrReserveTO.setBlockEntity(new BlockEntity());
		registerPawnOrReserveTO.setHolder(new Holder());
		registerPawnOrReserveTO.setBlNoReserve(true);
		registerPawnOrReserveTO.setBlReserve(false);
		registerPawnOrReserveTO.setParameterTableAffectationForm(new ParameterTable());
		registerPawnOrReserveTO.setParameterTableCurrency(new ParameterTable());
		if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
			registerPawnOrReserveTO.setIndRegisteredDepositary(BooleanType.YES.getCode());
		} else {
			registerPawnOrReserveTO.setIndRegisteredDepositary(BooleanType.NO.getCode());
		}
		
		listFileAffectationTO = new ArrayList<FileAffectationTO>();
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();	
		parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			registerPawnOrReserveTO.setLstAffectationForm(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			if (userInfo.getUserAccountSession().isIssuerInstitucion() 
					|| userInfo.getUserAccountSession().isIssuerDpfInstitucion()) {
				//to the issuers the quantity block is allowed only 
				registerPawnOrReserveTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());
			} 
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
			for(ParameterTable paramTab:generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
				if(CurrencyType.USD.getCode().equals(paramTab.getParameterTablePk()) ||
					CurrencyType.PYG.getCode().equals(paramTab.getParameterTablePk()) ||
					CurrencyType.UFV.getCode().equals(paramTab.getParameterTablePk()) ||
					CurrencyType.EU.getCode().equals(paramTab.getParameterTablePk())) 
				{
					lstTemp.add(paramTab);
				}
			}
			registerPawnOrReserveTO.setLstCurrency(lstTemp);
			listHolidays();
		} catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		makeListTypeRequest(REQUEST_PANW_OR_RESERVE);
		registerPawnOrReserveTO.getParameterTableAffectationForm().setParameterTablePk(AffectationFormType.EXACT_AMOUNT.getCode());
		registerPawnOrReserveTO.setBlOpposition(false);
		registerPawnOrReserveTO.setBlNoOpposition(true);
		registerPawnOrReserveTO.setDateRecord(CommonsUtilities.currentDate());
		
	}
	
	/**
	 * Show bcr.
	 */
	public void showBCR(){
		JSFUtilities.resetComponent("frmPrendAffect:nidBlockEntity:nidBlockEntity");
		if(Validations.validateListIsNotNullAndNotEmpty(registerPawnOrReserveTO.getLstHolderAccountBalanceHeader())){
			for(HolderAccountBalanceHeaderTO holAccBalHeaderTO:registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()){
				for(HolderAccountBalanceTO holAccBalTO:holAccBalHeaderTO.getLstHolderAccountBalanceTO()){
					holAccBalTO.setSelectedBenefits(new ArrayList<Integer>());
					holAccBalTO.setQuantityBlockBalance(null);
					holAccBalTO.setBlock(null);
					holAccBalTO.setReblock(null);
				}
			}
		}
		if(AffectationType.RESERVE.getCode().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk())){
			registerPawnOrReserveTO.setBlReserve(true);
			registerPawnOrReserveTO.setBlNoReserve(false);
			try{
				//Se recupera la entidad de bloqueo BCR
				BlockEntity blockEntityBCR = requestAffectationServiceFacade.searchBlockEntityBCR(rntBCR);
				if(Validations.validateIsNullOrEmpty(blockEntityBCR.getStateBlockEntity())){
					registerPawnOrReserveTO.setBlockEntity(new BlockEntity());
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage("alt.affectation.bcr.notExist"));
					JSFUtilities.showSimpleValidationDialog();
				}else if(!blockEntityBCR.isActive()){
					registerPawnOrReserveTO.setBlockEntity(new BlockEntity());
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.bcr.notActive"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					registerPawnOrReserveTO.setBlockEntity(blockEntityBCR);
				}
			}catch (ServiceException e) {
				log.info(e.getMessage());
			    excepcion.fire(new ExceptionToCatchEvent(e));
			}			
		}else{
			registerPawnOrReserveTO.setBlockEntity(new BlockEntity());
			registerPawnOrReserveTO.setBlNoReserve(true);
			registerPawnOrReserveTO.setBlReserve(false);
		}		
	}
	
	/**
	 * Ask for clean pawn.
	 */
	public void askForCleanPawn(){
		JSFUtilities.resetViewRoot();
		if(validateRequiredFieldsForPawn()){
			cleanNewPawn();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.clean"));
			JSFUtilities.executeJavascriptFunction("cnfwCleanDialog.show()");
		}
	}
	
	/**
	 * Ask for clean ban.
	 */
	public void askForCleanBan(){
		JSFUtilities.resetViewRoot();
		if(validateRequiredFieldsForBan()){
			cleanNewBan();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
								PropertiesUtilities.getGenericMessage("conf.msg.clean"));
			JSFUtilities.executeJavascriptFunction("cnfwCleanDialog.show()");
		}
	}
	
	/**
	 * Clean new ban.
	 */
	public void cleanNewBan(){
		JSFUtilities.resetViewRoot();
		registerNewBanOrOthers();				
	}
	
	/**
	 * Clean new pawn.
	 */
	public void cleanNewPawn(){
		JSFUtilities.resetViewRoot();
		registerNewPawnOrReserve();		
	}	
	
	/**
	 * Back from register.
	 *
	 * @return the string
	 */
	public String backFromRegister(){		
		return "searchRequest";
	}
	public void backPawn() {
		if(!Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getObservations())){
			blreturn = true;
		} else {
			blreturn = false;
		}
		JSFUtilities.updateComponent(":observations");
	}
	public void backBan() {
		if(!Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getObservations())){
			blreturn = true; 
		} else {
			blreturn = false;
		}
		JSFUtilities.updateComponent(":observations");
	}
	
	/**
	 * Validate required fields for ban.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFieldsForBan(){
		if(Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getNumberRecord())
				&& Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getDateRecord())
				&& Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())
				&& Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getBlockEntity().getIdBlockEntityPk())
				&& Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getHolder().getIdHolderPk())){
			return true;
		}else
			return false;
	}
	
	/**
	 * Validate required fields for pawn.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFieldsForPawn(){
		if(Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getNumberRecord())
				&& Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getDateRecord())
				&& Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk())
				&& Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getBlockEntity().getIdBlockEntityPk())
				&& Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getHolder().getIdHolderPk())){
			return true;
		}else
			return false;
	}
	
	/**
	 * Clear hol acc bal header from ban.
	 */
	public void clearHolAccBalHeaderFromBan(){
		//Al cambiar de cuenta se debe borrar los saldos
		JSFUtilities.resetViewRoot();
		registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
	}
	
	/**
	 * Clear hol acc bal header from pawn.
	 */
	public void clearHolAccBalHeaderFromPawn(){
		//Al cambiar de cuenta se debe borrar los saldos
//		JSFUtilities.resetViewRoot();
		registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
	}
	
	/**
	 * Check automatic dpf for ban.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void checkAutomaticDPFForBan(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		if(objHolAccBalTO.isBlCheck()){
			if(objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPF.getCode()) || objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				//checking if the value has cat   
				if(searchAccreditationOperationByUnblock(objHolAccBalTO, objHolAccBalHeTO))
					return;
				//
				objHolAccBalTO.setBlCheck(true);
				objHolAccBalTO.setQuantityBlockBalance(objHolAccBalTO.getTotalBalance());
				validateAmountBlockForBanWithFactMarket(objHolAccBalTO,objHolAccBalHeTO);			   
			}
		}
		updateBlockAndReblockAmount(objHolAccBalTO,objHolAccBalHeTO);
	}
	
	/**
	 * Update block and reblock amount.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void updateBlockAndReblockAmount(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		if(registerBanOrOthersTO.isBlCashValuation()){		
			if(objHolAccBalTO.isBlCheck()){	
				if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getBlockTemp()))
					objHolAccBalTO.setBlockTemp(BigDecimal.ZERO);
				
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getBlock())){
					if(objHolAccBalTO.getBlock().compareTo(objHolAccBalTO.getBlockTemp()) != 0){
						BigDecimal currenBlocktAmount = objHolAccBalHeTO.getBlockAmount();						
						BigDecimal newBlockAmount = currenBlocktAmount;						
						newBlockAmount = newBlockAmount.subtract(objHolAccBalTO.getBlockTemp());
						newBlockAmount = newBlockAmount.add(objHolAccBalTO.getBlock());				
						objHolAccBalHeTO.setBlockAmount(newBlockAmount);
						objHolAccBalTO.setBlockTemp(objHolAccBalTO.getBlock());
					}
				}
				
				if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getReblockTemp()))
					objHolAccBalTO.setReblockTemp(BigDecimal.ZERO);
				
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getReblock())){
					if(objHolAccBalTO.getReblock().compareTo(objHolAccBalTO.getReblockTemp()) != 0){
						BigDecimal currenReBlocktAmount = objHolAccBalHeTO.getReblockAmount();
						BigDecimal newReBlockAmount = currenReBlocktAmount;
						newReBlockAmount = newReBlockAmount.subtract(objHolAccBalTO.getReblockTemp());
						newReBlockAmount = newReBlockAmount.add(objHolAccBalTO.getReblock());
						objHolAccBalHeTO.setReblockAmount(newReBlockAmount);
						objHolAccBalTO.setReblockTemp(objHolAccBalTO.getReblock());
					}				
				}
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getValoritationQuantityBlockBalance())){
					if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getValoritationQuantityBlockBalanceTemp()))
						objHolAccBalTO.setValoritationQuantityBlockBalanceTemp(BigDecimal.ZERO);
					
					registerBanOrOthersTO.setBlockValoritationAmount(registerBanOrOthersTO.getBlockValoritationAmount().subtract(objHolAccBalTO.getValoritationQuantityBlockBalanceTemp()));
					
					if(!objHolAccBalTO.getValoritationQuantityBlockBalance().equals(objHolAccBalTO.getValoritationQuantityBlockBalanceTemp())){
						objHolAccBalTO.setValoritationQuantityBlockBalanceTemp(objHolAccBalTO.getValoritationQuantityBlockBalance());
						registerBanOrOthersTO.setBlockValoritationAmount(registerBanOrOthersTO.getBlockValoritationAmount().add(objHolAccBalTO.getValoritationQuantityBlockBalance()));	
					}					
				}
			}else{
				subTractAmount(objHolAccBalTO, objHolAccBalHeTO);	
				clearRowForAffectation(objHolAccBalTO);
			}
		}else{
			if(!objHolAccBalTO.isBlCheck()){
				clearRowForAffectation(objHolAccBalTO);
			}
		}			
	}
	
	/**
	 * Clear row for affectation.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void clearRowForAffectation(HolderAccountBalanceTO objHolAccBalTO){
		clearQuantitiesBlocks(objHolAccBalTO);
		objHolAccBalTO.setSelectedBenefits(new ArrayList<Integer>());			
	}
	
	/**
	 * Validate amount block.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void validateAmountBlock(HolderAccountBalanceTO objHolAccBalTO){
		//Validacion de las cantidades para el registro de prenda o encaje
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance())){//Si la cantidad a bloquear es diferente de nulo o vacio
			BigDecimal subtracQuantity = null;
			if(AffectationType.PAWN.getCode().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk()))
				subtracQuantity = objHolAccBalTO.getReportingBalance().add(objHolAccBalTO.getBanBalance()).add(objHolAccBalTO.getOppositionBalance());
			else
				subtracQuantity = objHolAccBalTO.getReportingBalance();
			BigDecimal total = objHolAccBalTO.getTotalBalance().subtract(subtracQuantity);
			BigDecimal quantityBlock = objHolAccBalTO.getQuantityBlockBalance();
			if(quantityBlock.compareTo(total) == 1){
				clearQuantitiesBlocks(objHolAccBalTO);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				BigDecimal available = objHolAccBalTO.getAvailableBalance();
				if(available.compareTo(quantityBlock) >= 0){
					objHolAccBalTO.setBlock(quantityBlock);
					objHolAccBalTO.setReblock(BigDecimal.ZERO);
				}else if(quantityBlock.compareTo(available) == 1){
					BigDecimal partialResult = quantityBlock.subtract(available);
					if(total.compareTo(partialResult) >= 0){
						objHolAccBalTO.setBlock(available);
						objHolAccBalTO.setReblock(partialResult);
					}else{
						clearQuantitiesBlocks(objHolAccBalTO);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
						JSFUtilities.showSimpleValidationDialog();
					}
				}else{
					clearQuantitiesBlocks(objHolAccBalTO);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}else{
			//Se limpia las cantidades
			clearQuantitiesBlocks(objHolAccBalTO);
		}
	}
	
	/**
	 * Validate amount block for ban with fact market.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void validateAmountBlockForBanWithFactMarket(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		validateAmountBlockForBan(objHolAccBalTO,objHolAccBalHeTO);
		if(objHolAccBalTO.getQuantityBlockBalance()!=null || objHolAccBalTO.getBlock()!=null
				|| objHolAccBalTO.getReblock()!=null || objHolAccBalTO.getValoritationQuantityBlockBalance()!=null){
			setMarketForBanFactUI(objHolAccBalTO);
		}
		
	}
	
	/**
	 * Validate amount block for ban.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void validateAmountBlockForBan(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		//Validacion de los montos para embargo, otros u oposicion
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance())){
			//checking if the value has cat
			if(!objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPF.getCode()) && !objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				if(objHolAccBalTO.getQuantityBlockBalance().compareTo(objHolAccBalTO.getAvailableBalance())>0 
						&& (objHolAccBalTO.getQuantityBlockBalance().compareTo(objHolAccBalTO.getTotalBalance())==0 || (objHolAccBalTO.getQuantityBlockBalance().compareTo(objHolAccBalTO.getTotalBalance())<0))) {
					if(searchAccreditationOperationByUnblock(objHolAccBalTO, objHolAccBalHeTO))
						return;
				}
			}
			//
			BigDecimal total = objHolAccBalTO.getTotalBalance().subtract(objHolAccBalTO.getReportingBalance());
			BigDecimal quantityBlock = objHolAccBalTO.getQuantityBlockBalance();
			if(registerBanOrOthersTO.isBlCashValuation()){				
				if(quantityBlock.compareTo(total) == 1){
					subTractAmount(objHolAccBalTO, objHolAccBalHeTO);
					clearQuantitiesBlocks(objHolAccBalTO);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					BigDecimal available = objHolAccBalTO.getAvailableBalance();
					if(available.compareTo(quantityBlock) >= 0){
						objHolAccBalTO.setBlock(quantityBlock);
						objHolAccBalTO.setReblock(BigDecimal.ZERO);
						if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getLastQuotation())
								&& objHolAccBalTO.getLastQuotation().compareTo(BigDecimal.ZERO) == 1){
							if(objHolAccBalTO.getCurrency().equals(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk()))
								objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()));
							else{
								if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).divide(registerBanOrOthersTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
								else
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).multiply(registerBanOrOthersTO.getBdExchangeRate()));
							}
						}else{
							if(objHolAccBalTO.getCurrency().equals(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk()))
								objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()));
							else{
								if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).divide(registerBanOrOthersTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
								else
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).multiply(registerBanOrOthersTO.getBdExchangeRate()));
							}
						}
						updateBlockAndReblockAmount(objHolAccBalTO,objHolAccBalHeTO);
					}else if(quantityBlock.compareTo(available) == 1){
						BigDecimal partialResult = quantityBlock.subtract(available);
						if(total.compareTo(partialResult) >= 0){
							objHolAccBalTO.setBlock(available);
							objHolAccBalTO.setReblock(partialResult);
							if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getLastQuotation())
									&& objHolAccBalTO.getLastQuotation().compareTo(BigDecimal.ZERO) == 1){
								if(objHolAccBalTO.getCurrency().equals(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()));
								else{
									if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).divide(registerBanOrOthersTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
									else
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).multiply(registerBanOrOthersTO.getBdExchangeRate()));
								}
							}else{
								if(objHolAccBalTO.getCurrency().equals(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()));
								else{
									if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).divide(registerBanOrOthersTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
									else
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).multiply(registerBanOrOthersTO.getBdExchangeRate()));
								}
							}
							updateBlockAndReblockAmount(objHolAccBalTO,objHolAccBalHeTO);
						}else{
							subTractAmount(objHolAccBalTO, objHolAccBalHeTO);
							clearQuantitiesBlocks(objHolAccBalTO);
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
							JSFUtilities.showSimpleValidationDialog();
						}
					}else{
						subTractAmount(objHolAccBalTO, objHolAccBalHeTO);
						clearQuantitiesBlocks(objHolAccBalTO);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}else{			
				if(quantityBlock.compareTo(total) == 1){					
					clearQuantitiesBlocks(objHolAccBalTO);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					BigDecimal available = objHolAccBalTO.getAvailableBalance();
					if(available.compareTo(quantityBlock) >= 0){
						objHolAccBalTO.setBlock(quantityBlock);
						objHolAccBalTO.setReblock(BigDecimal.ZERO);						
					}else if(quantityBlock.compareTo(available) == 1){
						BigDecimal partialResult = quantityBlock.subtract(available);
						if(total.compareTo(partialResult) >= 0){
							objHolAccBalTO.setBlock(available);
							objHolAccBalTO.setReblock(partialResult);							
						}else{							
							clearQuantitiesBlocks(objHolAccBalTO);
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
							JSFUtilities.showSimpleValidationDialog();
						}
					}else{						
						clearQuantitiesBlocks(objHolAccBalTO);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}
		}else{
			subTractAmount(objHolAccBalTO, objHolAccBalHeTO);
			clearQuantitiesBlocks(objHolAccBalTO);	
		}
	}
	
	/**
	 * Sub tract amount.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	private void subTractAmount(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		if(registerBanOrOthersTO.isBlCashValuation()){	
			objHolAccBalTO.setBlockTemp(BigDecimal.ZERO);
			BigDecimal currenBlocktAmount = objHolAccBalHeTO.getBlockAmount();
			BigDecimal newBlockAmount = currenBlocktAmount;
			if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getBlock()))
				newBlockAmount = newBlockAmount.subtract(objHolAccBalTO.getBlock());			
			objHolAccBalHeTO.setBlockAmount(newBlockAmount);
			objHolAccBalTO.setReblockTemp(BigDecimal.ZERO);
			
			BigDecimal currenReBlocktAmount = objHolAccBalHeTO.getReblockAmount();
			BigDecimal newReBlockAmount = currenReBlocktAmount;
			if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getReblock()))
				newReBlockAmount = newReBlockAmount.subtract(objHolAccBalTO.getReblock());
			objHolAccBalHeTO.setReblockAmount(newReBlockAmount);
			
			if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getBlockValoritationAmount())
					&& Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getValoritationQuantityBlockBalance())){
				objHolAccBalTO.setValoritationQuantityBlockBalanceTemp(BigDecimal.ZERO);
				registerBanOrOthersTO.setBlockValoritationAmount(registerBanOrOthersTO.getBlockValoritationAmount().subtract(objHolAccBalTO.getValoritationQuantityBlockBalance()));
			}
		}
	}
	
	/**
	 * Ask for save pawn.
	 */
	public void askForSavePawn(){
		try{
			
			// validando si se adjunto archivo de repaldo
			if(listFileAffectationTO == null || (listFileAffectationTO != null && listFileAffectationTO.size() == 0)){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage("fupl.error.empty"));					
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(registerPawnOrReserveTO.getLstHolderAccountBalanceHeader())){
				boolean blHaveOne = false;
				boolean blEmptyQuantity = false;
				boolean blCoverAmount = true;
				BigDecimal bdTotal = BigDecimal.ZERO;
				for(HolderAccountBalanceHeaderTO objHolderAccBalHeaderTO: registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()){
					if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO())){
						//Validamos que se haya marcado por lo menos un registro
						for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
							if(objHolAccBalTO.isBlCheck()){
								blHaveOne = true;
								break;
							}
						}
						//Validamos que el registro marcado tenga el campo de cantidad lleno
						for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
							if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getQuantityBlockBalance()) && objHolAccBalTO.isBlCheck()){
								blEmptyQuantity = true;
								break;
							}
						}
						
						if(AffectationFormType.CASH_VALUATION.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
							//Se suma los valorizados						
							for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
								if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance()) && objHolAccBalTO.isBlCheck()){							
									bdTotal = bdTotal.add(objHolAccBalTO.getValoritationQuantityBlockBalance());
								}
							}
						}
					}
				}
				
				if(AffectationFormType.CASH_VALUATION.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
					//Validamos que el total de los registros sea igual al monto general
					if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getAmount())){
						if(bdTotal.compareTo(registerPawnOrReserveTO.getAmount()) == 0 ||
								bdTotal.compareTo(registerPawnOrReserveTO.getAmount()) == 1)
							blCoverAmount = true;
						else 
							blCoverAmount = false;
					}
				}
				
				if(blHaveOne && !blEmptyQuantity && blCoverAmount && Validations.validateIsEmpty(validateFieldsSavePawnOrReserve())){					
					if(!Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getObservations())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("message.observations.required"));
						JSFUtilities.showSimpleValidationDialog();
						blreturn = true; 
						JSFUtilities.updateComponent(":observations");
					} else {
						Object[] bodyData = new Object[2];
						for(ParameterTable paramTable:registerPawnOrReserveTO.getLstTypeRequest()){
							if(paramTable.getParameterTablePk().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk())){
								bodyData[0] = paramTable.getParameterName();
								break;
							}
						}	
						bodyData[1] = registerPawnOrReserveTO.getHolder().getIdHolderPk().toString();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
								PropertiesUtilities.getMessage("alt.affectation.askRegister", bodyData));
						JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");	
					}
				}else{
					if(Validations.validateIsEmpty(validateFieldsSavePawnOrReserve())){
						if(!blHaveOne){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.noSelectedAffectation"));
							JSFUtilities.showSimpleValidationDialog();
						}else if(blEmptyQuantity){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.emptyQuantity"));
							JSFUtilities.showSimpleValidationDialog();
						}else if(!blCoverAmount){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.amount.notCover"));								
							JSFUtilities.showSimpleValidationDialog();
						}
					}
//					else{
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), validateFieldsSavePawnOrReserve());
//						JSFUtilities.showSimpleValidationDialog();
//						if(registerPawnOrReserveTO.getFileName()==null || registerPawnOrReserveTO.getFileName().isEmpty()){
//							JSFUtilities.addContextMessage("frmPrendAffect:fuplPawn",  FacesMessage.SEVERITY_ERROR, 
//																								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
//																								validateFieldsSavePawnOrReserve());
//						}
//					}
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage("alt.affectation.noSelectedAffectation"));
				JSFUtilities.showSimpleValidationDialog();
			}			
		}catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Save pawn or reserve affectation.
	 */
	@LoggerAuditWeb
	public void savePawnOrReserveAffectation(){
		try{
			hideDialogsForPawn();
			Long lngResult = requestAffectationServiceFacade.registerNewPawnOrReserveRequest(registerPawnOrReserveTO);
			Object[] bodyData = new Object[1];
			/*
			for(ParameterTable paramTable:registerPawnOrReserveTO.getLstTypeRequest()){
				if(paramTable.getParameterTablePk().equals(registerPawnOrReserveTO.getParameterTableTypeRequest().getParameterTablePk())){
					bodyData[0] = paramTable.getParameterName();
					break;
				}
			}*/
			if(Validations.validateIsNotNullAndNotEmpty(lngResult)){
				bodyData[0] = lngResult.toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage("alt.affectation.register", bodyData) );
				JSFUtilities.showSimpleEndTransactionDialog("searchRequest");				
				
				// INICIO ENVIO DE NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_PAWN_REGISTER.getCode());
				Object[] parameters = {	lngResult.toString() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
				// FIN ENVIO DE NOTIFICACIONES		
				
				init();				
			}		
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.showSimpleEndTransactionDialog("searchRequest");
		}
	}
	
	/**
	 * Ask for save ban.
	 */
	public void askForSaveBan(){
		try{
			
			// validando si se adjunto archivo de repaldo
			if(listFileAffectationTO == null || (listFileAffectationTO != null && listFileAffectationTO.size() == 0)){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage("fupl.error.empty"));					
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(registerBanOrOthersTO.getLstHolderAccountBalanceHeader())){
				boolean blHaveOne = false;
				boolean blEmptyQuantity = false;
				boolean blCoverAmount = true;
				BigDecimal bdTotal = BigDecimal.ZERO;
				for(HolderAccountBalanceHeaderTO objHolderAccBalHeaderTO: registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
					if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO())){
						//Validamos que se haya marcado por lo menos un registro
						for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
							if(objHolAccBalTO.isBlCheck()){
								blHaveOne = true;
								break;
							}
						}
						//Validamos que ese registro marcado no tenga el campo de cantidad vacio
						for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
							if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getQuantityBlockBalance()) && objHolAccBalTO.isBlCheck()){
								blEmptyQuantity = true;
								break;
							}
						}					
						if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
							//Se suma los valorizados						
							for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
								if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance()) && objHolAccBalTO.isBlCheck()){							
									bdTotal = bdTotal.add(objHolAccBalTO.getValoritationQuantityBlockBalance());
								}
							}
						}
					}
				}
				if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
					//Validamos que el total de los registros sea igual al monto general
					if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getAmount())){
						if(bdTotal.compareTo(registerBanOrOthersTO.getAmount()) == 0 ||
								bdTotal.compareTo(registerBanOrOthersTO.getAmount()) == 1)
							blCoverAmount = true;
						else 
							blCoverAmount = false;
					}
				}
				if(blHaveOne && !blEmptyQuantity && blCoverAmount && Validations.validateIsEmpty(validateFieldsSaveBanOrOthersOrOpposition())){				
					if(!Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getObservations())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("message.observations.required"));
						JSFUtilities.showSimpleValidationDialog();
						blreturn = true; 
						JSFUtilities.updateComponent(":observations");
					} else {
						Object[] bodyData = new Object[2];
						for(ParameterTable paramTable:registerBanOrOthersTO.getLstBlockMotive()){
							if(paramTable.getParameterTablePk().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
								bodyData[0] = paramTable.getParameterName();
								break;
							}
						}	
						bodyData[1] = registerBanOrOthersTO.getHolder().getIdHolderPk().toString();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
								PropertiesUtilities.getMessage("alt.affectation.askRegister", bodyData));
						JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");		
					}
				}else{
					if(Validations.validateIsEmpty(validateFieldsSaveBanOrOthersOrOpposition())){
						if(!blHaveOne){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.noSelectedAffectation"));
							JSFUtilities.showSimpleValidationDialog();
						}else if(blEmptyQuantity){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.emptyQuantity"));
							JSFUtilities.showSimpleValidationDialog();
						}else if(!blCoverAmount){	
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.amount.notCover"));								
							JSFUtilities.showSimpleValidationDialog();
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, validateFieldsSaveBanOrOthersOrOpposition());
						if(registerBanOrOthersTO.getFileName()==null || registerBanOrOthersTO.getFileName().isEmpty()){
							JSFUtilities.addContextMessage("frmBanAffect:fuplBan",  FacesMessage.SEVERITY_ERROR, 
																								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
																								validateFieldsSaveBanOrOthersOrOpposition());
						}
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.affectation.noSelectedAffectation"));
				JSFUtilities.showSimpleValidationDialog();
			}
		}catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Save ban or others or opposition affectation.
	 */
	@LoggerAuditWeb
	public void saveBanOrOthersOrOppositionAffectation(){
		try {
			Object[] bodyData = new Object[1];
			/*
			for(ParameterTable paramTable:registerBanOrOthersTO.getLstBlockMotive()){
				if(paramTable.getParameterTablePk().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
					bodyData[0] = paramTable.getParameterName();
					break;
				}
			}*/
			List<HolderAccountBalanceTO> listHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();
			for(HolderAccountBalanceHeaderTO holAccBalHeaderTO:registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
				for(HolderAccountBalanceTO objHolAccBalaTO :holAccBalHeaderTO.getLstHolderAccountBalanceTO()){
					if(objHolAccBalaTO.isBlCheck()){
						listHolderAccountBalanceTO.add(objHolAccBalaTO);						
					}
				}
				holAccBalHeaderTO.setLstHolderAccountBalanceTO(listHolderAccountBalanceTO);
			}
						
			Long lngResult = requestAffectationServiceFacade.registerNewBanOrOthersOrOppositionRequest(registerBanOrOthersTO);
			if(Validations.validateIsNotNullAndNotEmpty(lngResult)){	
				bodyData[0] = lngResult.toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage("alt.affectation.register", bodyData));
				JSFUtilities.showSimpleEndTransactionDialog("searchRequest");				
				
				// INICIO ENVIO DE NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_REGISTER.getCode());
				Object[] parameters = {	lngResult.toString() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters );
				// FIN ENVIO DE NOTIFICACIONES	
				
				init();
			}
		} catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
			JSFUtilities.showSimpleEndTransactionDialog("searchRequest");
		}		
	}
	
	/**
	 * Send declaration.
	 */
	@LoggerAuditWeb
	public void sendDeclaration(){
		try {
			if(Validations.validateIsNullOrEmpty(requestAffectationTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						,PropertiesUtilities.getMessage("alt.affectation.record.not.selected"));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				if(!AffectationType.BAN.getCode().equals(requestAffectationTO.getIdTypeAffectation())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage("alt.affectation.declarationJustBan"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					if(RequestAffectationStateType.CONFIRMED.getCode().equals(requestAffectationTO.getIdState())){
						if(BooleanType.NO.getCode().equals(requestAffectationTO.getIndRequestDeclaration())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									,PropertiesUtilities.getMessage("alt.affectation.dontNeedDeclaration"));
							JSFUtilities.showSimpleValidationDialog();
						}else{
							Object[] bodyData = new Object[2];
							if(Validations.validateIsNotNullAndNotEmpty(requestAffectationTO.getNotificationDate())){
								bodyData[0] = CommonsUtilities.convertDatetoString(requestAffectationTO.getNotificationDate());
								bodyData[1] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										,PropertiesUtilities.getMessage("alt.affectation.sentDeclaration",bodyData));
								JSFUtilities.showSimpleValidationDialog();
							}else{
								boolean blResult = requestAffectationServiceFacade.updateRequestDeclarationDate(requestAffectationTO);
								if(blResult){
									bodyData[0] = CommonsUtilities.convertDatetoString(getCurrentSystemDate());
									bodyData[1] = requestAffectationTO.getNumberRequest().toString();
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
											,PropertiesUtilities.getMessage("alt.affectation.successDeclaration",bodyData));
									JSFUtilities.showSimpleValidationDialog();
								}else{
									bodyData[0] = CommonsUtilities.convertDatetoString(requestAffectationTO.getNotificationDate());
									bodyData[1] = requestAffectationTO.getNumberRequest().toString();
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											,PropertiesUtilities.getMessage("alt.affectation.sentDeclaration",bodyData));
									JSFUtilities.showSimpleValidationDialog();
								}
							}
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.justConfirmedBan"));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}
		} catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Sets the flags false.
	 */
	private void setFlagsFalse(){
		blApprove = false;
		blAnnul = false;
		blConfirm = false;
		blReject = false;
		blviewDetail= false;
//		blDisabledFields= true;
//		blRenderedMotives= false;
	}
	
	public void documentFile(){
		//Document File Information
		listFileAffectationTO = new ArrayList<FileAffectationTO>();
	    FileAffectationTO fileAffectationTO = new FileAffectationTO();
	    fileAffectationTO.setDocumentFile(requestAffecDetaTO.getDocumentFile());
	    fileAffectationTO.setFileName(requestAffecDetaTO.getFileName());
	    String extensionFile = "";
	    if(requestAffecDetaTO.getFileName()!=null){
	    	extensionFile = requestAffecDetaTO.getFileName().toLowerCase().substring(requestAffecDetaTO.getFileName().lastIndexOf(".")+1,requestAffecDetaTO.getFileName().length());
			fileAffectationTO.setFileExtension(extensionFile.toUpperCase());
			fileAffectationTO.setFileSize(Integer.valueOf(requestAffecDetaTO.getDocumentFile().length).longValue()/1000L);
			listFileAffectationTO.add(fileAffectationTO);
	    }
	}
	
	/**
	 * Validate annul.
	 *
	 * @return the string
	 */
	public String validateAnnul(){
		try{
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestAffectationTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestAffectationStateType.REGISTERED.getCode().equals(requestAffectationTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestAffectationTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  
					requestAffecDetaTO = requestAffectationServiceFacade.findAffectationRequestDetail(requestAffectationTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (BlockOperation objBlockOperation: requestAffecDetaTO.getObjBlockRequest().getBlockOperation()) {
							//the issuer shall annul their own securities only
							if (!issuerCode.equalsIgnoreCase(objBlockOperation.getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blAnnul = true;
					return STR_VIEW_AFECTATION_MAPPING; 
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = requestAffectationTO.getNumberRequest().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_REGISTERED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}				
			}	
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	public String validateApprove(){
		try{
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestAffectationTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestAffectationStateType.REGISTERED.getCode().equals(requestAffectationTO.getIdState()))
				{
					List<String> lstOperationUser = validateIsValidUser(requestAffectationTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  
					requestAffecDetaTO = requestAffectationServiceFacade.findAffectationRequestDetail(requestAffectationTO);
					//Document File Information
					documentFile();
				    //now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (BlockOperation objBlockOperation: requestAffecDetaTO.getObjBlockRequest().getBlockOperation()) {
							//the issuer shall approve their own securities only
							if (!issuerCode.equalsIgnoreCase(objBlockOperation.getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blApprove = true;
					return STR_VIEW_AFECTATION_MAPPING; 
				}else{					  
					Object[] bodyData = new Object[1];
					bodyData[0] = requestAffectationTO.getNumberRequest().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_REGISTERED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}				
			}	
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
	}
	
	
	/**
	 * Validate is valid user.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return the list
	 */
	private List<String> validateIsValidUser(RequestAffectationTO requestAffectationTO) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;	
		OperationUserTO operationUserTO = new OperationUserTO();  
		operationUserTO.setOperNumber(requestAffectationTO.getNumberRequest().toString());
		operationUserTO.setUserName(requestAffectationTO.getRegisterUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		try{
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestAffectationTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestAffectationStateType.APPROVED.getCode().equals(requestAffectationTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestAffectationTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  
					requestAffecDetaTO = requestAffectationServiceFacade.findAffectationRequestDetail(requestAffectationTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (BlockOperation objBlockOperation: requestAffecDetaTO.getObjBlockRequest().getBlockOperation()) {
							//the issuer shall confirm their own DPA and DPF securities only
							if (!issuerCode.equalsIgnoreCase(objBlockOperation.getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
							if (!SecurityClassType.DPF.getCode().equals(objBlockOperation.getSecurities().getSecurityClass()) &&
								!SecurityClassType.DPA.getCode().equals(objBlockOperation.getSecurities().getSecurityClass())) 
							{
								Object[] bodyData = new Object[1];
								bodyData[0] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_SECURITY_CLASS_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blConfirm = true;
					return STR_VIEW_AFECTATION_MAPPING; 
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = requestAffectationTO.getNumberRequest().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_APPROVED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}				
			}	
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
	}	
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		try{
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestAffectationTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestAffectationStateType.APPROVED.getCode().equals(requestAffectationTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestAffectationTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  					  
					requestAffecDetaTO = requestAffectationServiceFacade.findAffectationRequestDetail(requestAffectationTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (BlockOperation objBlockOperation: requestAffecDetaTO.getObjBlockRequest().getBlockOperation()) {
							//the issuer shall confirm their own DPA and DPF securities only
							if (!issuerCode.equalsIgnoreCase(objBlockOperation.getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
							if (!SecurityClassType.DPF.getCode().equals(objBlockOperation.getSecurities().getSecurityClass()) &&
								!SecurityClassType.DPA.getCode().equals(objBlockOperation.getSecurities().getSecurityClass())) 
							{
								Object[] bodyData = new Object[1];
								bodyData[0] = requestAffectationTO.getNumberRequest().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_SECURITY_CLASS_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blReject = true;
					return STR_VIEW_AFECTATION_MAPPING;				
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = requestAffectationTO.getNumberRequest().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_APPROVED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}				
			}	
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
	}
	
	/**
	 * Before action.
	 */
	@LoggerAuditWeb
	public void beforeAction(){
		if (blApprove) {
			Object[] bodyData = new Object[1];				
			bodyData[0] = requestAffecDetaTO.getNumberRequest().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_APPROVE,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		} else if (blAnnul) {
			Object[] bodyData = new Object[1];				
			bodyData[0] = requestAffecDetaTO.getNumberRequest().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_ANNUL,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		} else if(blConfirm){
			Object[] bodyData = new Object[1];				
			bodyData[0] = requestAffecDetaTO.getNumberRequest().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_CONFIRM,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}
		else if(blReject){
			Object[] bodyData = new Object[1];				
			bodyData[0] = requestAffecDetaTO.getNumberRequest().toString();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_REJECT,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}
	}	
	
	/**
	 * Before show reject.
	 */
	public void beforeShowReject(){
		JSFUtilities.resetComponent(":frmMotiveReject:dialogMotiveReject");
//		requestAffecDetaTO.setRejectMotive(null);
//		requestAffecDetaTO.setRejectMotiveOther(null);
		requestAffectationTO.setRejectMotive(null);
		requestAffectationTO.setRejectMotiveOther(null);
		JSFUtilities.showComponent(":frmMotiveReject:dialogMotiveReject");		
	}
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
	}
	
	/**
	 * Hide dialogs for pawn.
	 */
	public void hideDialogsForPawn(){
	}
	
	/**
	 * Hide dialogs for ban.
	 */
	public void hideDialogsForBan(){
	}
	
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		if (blApprove) {
			approveAffectation();
		} else if (blAnnul) {
			annulAffectation();
		} else if(blConfirm){
			confirmAffectation();
		}else if(blReject){
			rejectAffectation();
		}
	}

	/**
	 * Approve affectation.
	 */
	private void approveAffectation() 
	{
		try{
			BusinessProcess businessProcess = new BusinessProcess();
			boolean blResult ;
			if(requestAffectationTO.getIdTypeAffectation().equals(AffectationType.PAWN.getCode())){
				blResult = requestAffectationServiceFacade.approvePawnAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_PAWN_APPROVE.getCode());
			}else{
				blResult = requestAffectationServiceFacade.approveBanOthersAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_APPROVE.getCode());
			}
			
			if(blResult){
				Object[] bodyData = new Object[2];
				bodyData[0] = requestAffectationTO.getNumberRequest().toString();
				bodyData[1] = requestAffectationTO.getHolder();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_APPROVE, bodyData));
				JSFUtilities.showSimpleEndTransactionDialog("searchRequest");				
				
				// INICIO ENVIO DE NOTIFICACIONES								
				Object[] parameters = {	
						requestAffectationTO.getNumberRequest().toString(),
						requestAffectationTO.getHolder()
				};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcess,null, parameters );
				// FIN ENVIO DE NOTIFICACIONES	
				
				findRequestAffectations();
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleEndTransactionDialog("searchRequest");
		}
	}
	
	/**
	 * Annul affectation.
	 */
	private void annulAffectation()
	{
		try{
			BusinessProcess businessProcess = new BusinessProcess();
			boolean blResult ;
			if(requestAffectationTO.getIdTypeAffectation().equals(AffectationType.PAWN.getCode())){
				blResult = requestAffectationServiceFacade.annulPawnAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_PAWN_ANNUL.getCode());
			}else{
				blResult = requestAffectationServiceFacade.annulBanOthersAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_ANNUL.getCode());
			}
			
			if(blResult){
				Object[] bodyData = new Object[2];
				bodyData[0] = requestAffectationTO.getNumberRequest().toString();
				bodyData[1] = requestAffectationTO.getHolder();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ANNUL, bodyData));
				JSFUtilities.showSimpleEndTransactionDialog("searchRequest");				
								
				// INICIO ENVIO DE NOTIFICACIONES								
				Object[] parameters = {	
						requestAffectationTO.getNumberRequest().toString(),
						requestAffectationTO.getHolder()
				};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
				// FIN ENVIO DE NOTIFICACIONES								
				
				findRequestAffectations();
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleEndTransactionDialog("searchRequest");
		}
	}
	
	/**
	 * Confirm affectation.
	 */
	@EJB
	private BillingComponentService billingComponentService;
	private void confirmAffectation(){		
		try{
			BusinessProcess businessProcess = new BusinessProcess();
			boolean blResult ;
			if(requestAffectationTO.getIdTypeAffectation().equals(AffectationType.PAWN.getCode())){
				blResult = requestAffectationServiceFacade.confirmPawnAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_PAWN_CONFIRM.getCode());
			}else{
				blResult = requestAffectationServiceFacade.confirmAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_CONFIRM.getCode());
			}
			
			if(blResult){
				Object[] bodyData = new Object[1];
				bodyData[0] = requestAffectationTO.getNumberRequest().toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_CONFIRM, bodyData));
				JSFUtilities.showSimpleEndTransactionDialog("searchRequest");								
				
				// INICIO ENVIO DE NOTIFICACIONES
				BlockRequest objBlockRequest = requestAffectationServiceFacade.getBlockRequest(requestAffectationTO);
				for(BlockOperation objBlockOpe:objBlockRequest.getBlockOperation()){
					sendNotificactionsEmail(objBlockRequest,objBlockOpe,businessProcess);
				// FIN ENVIO DE NOTIFICACIONES
				findRequestAffectations();
				}
			}
		}catch (Exception e) {
			log.info(e.getMessage());
			if (e instanceof ServiceException) {
				ServiceException se= (ServiceException) e;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(), se.getParams()));
			} else {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			JSFUtilities.showSimpleEndTransactionDialog("searchRequest");
		}
	}	
	/**
	 * Reject affectation.
	 */
	private void rejectAffectation(){	
		try{
			BusinessProcess businessProcess = new BusinessProcess();
			boolean blResult ;
			if(requestAffectationTO.getIdTypeAffectation().equals(AffectationType.PAWN.getCode())){
				blResult = requestAffectationServiceFacade.rejectPawnAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_PAWN_REJECT.getCode());
			}else{
				blResult = requestAffectationServiceFacade.rejectAffectation(requestAffectationTO);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_BLOCK_REJECT.getCode());
			}
			
			if(blResult){
				Object[] bodyData = new Object[2];
				bodyData[0] = requestAffectationTO.getNumberRequest().toString();
				bodyData[1] = requestAffectationTO.getHolder();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_REJECT, bodyData));
				JSFUtilities.showSimpleEndTransactionDialog("searchRequest");				
				
				// INICIO ENVIO DE NOTIFICACIONES						
				Object[] parameters = {	
						requestAffectationTO.getNumberRequest().toString(),
						requestAffectationTO.getHolder()
				};
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
				// FIN ENVIO DE NOTIFICACIONES
				
				findRequestAffectations();
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleEndTransactionDialog("searchRequest");
		}
	}
	
	/**
	 * Validate fields.
	 *
	 * @return String
	 */
	private String validateFieldsSaveBanOrOthersOrOpposition(){
		if(Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getHolder().getIdHolderPk()))
			return PropertiesUtilities.getMessage("alt.affectation.needHolder");
		else if(Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getBlockEntity().getIdBlockEntityPk()))
			return PropertiesUtilities.getMessage("alt.affectation.needEntityBlock");
//		else if(Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getFileName())
//					&& Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getDocumentFile()))
//			return PropertiesUtilities.getMessage("alt.affectation.needFile");	
		else if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getAmount())
					&& registerBanOrOthersTO.getAmount().compareTo(BigDecimal.ZERO) == 0)
			return PropertiesUtilities.getMessage("alt.affectation.amount.zero");
		else
			return GeneralConstants.EMPTY_STRING;		
	}
	
	/**
	 * Validate fields save pawn or reserve.
	 *
	 * @return the string
	 */
	private String validateFieldsSavePawnOrReserve(){
		if(Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getHolder().getIdHolderPk()))
			return PropertiesUtilities.getMessage("alt.affectation.needHolder");
		else if(Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getBlockEntity().getIdBlockEntityPk()))
			return PropertiesUtilities.getMessage("alt.affectation.needEntityBlock");
//		else if(Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getFileName())
//				&& Validations.validateIsNullOrEmpty(registerPawnOrReserveTO.getDocumentFile()))
//			return PropertiesUtilities.getMessage("alt.affectation.needFile");
		else
			return GeneralConstants.EMPTY_STRING;		
	}

	/**
	 * Search list holder account.
	 */
	
	//aquie el cambio
	public void searchListHolderAccount(){
		/*Limpiamos el monto total valorizado*/
		registerPawnOrReserveTO.setBlockValoritationAmount(BigDecimal.ZERO);
		try{
			if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getHolder().getIdHolderPk())){
				Holder holder = requestAffectationServiceFacade.findHolderByRnt(registerPawnOrReserveTO.getHolder().getIdHolderPk());
				
				if(HolderStateType.REGISTERED.getCode().equals(holder.getStateHolder()) || HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())){
					List<HolderAccountTO> lstHolderAcc = requestAffectationServiceFacade.findHolderAccountByRNT(holder.getIdHolderPk(), 
																							userInfo.getUserAccountSession().getParticipantCode());
					registerPawnOrReserveTO.setLstHolderAccount(lstHolderAcc);
					registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
					registerPawnOrReserveTO.getHolder().setFullName(holder.getFullName());					
					if(AffectationFormType.CASH_VALUATION.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
						//add
						if(registerPawnOrReserveTO.getAmount() == null){
							throw new ServiceException(ErrorServiceType.BLOCKOPERATION_CASH_WITHOUT_COVER);
						}//end add
						
						BigDecimal cashValuation = registerPawnOrReserveTO.getAmount();
						Integer currency = registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk();
						registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(new ArrayList<HolderAccountBalanceHeaderTO>());						
						//Add
						if(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk() != null){
							if(currency.equals(CurrencyType.PYG.getCode())){
								DailyExchangeRates exchangeRate = generalPametersFacade.getdailyExchangeRate( getCurrentSystemDate(),CurrencyType.USD.getCode());
								if(exchangeRate!=null){
									registerPawnOrReserveTO.setBdExchangeRate(exchangeRate.getSellPrice());
								}
							}
						}//End add							
						if(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk() == null){
							throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_NOT_SEL);
						}
						if(registerPawnOrReserveTO.getBdExchangeRate() == null){
							throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE);
						}											
						List<HolderAccountBalanceTO> balancesWithCriteria = requestAffectationServiceFacade.findAutomaticBalancesForBan(lstHolderAcc,cashValuation,currency);						
						for(HolderAccountTO objHolAccTO:registerPawnOrReserveTO.getLstHolderAccount()){
							HolderAccountBalanceHeaderTO objHolAccBalHeader = new HolderAccountBalanceHeaderTO();
							objHolAccBalHeader.setIdHolderAccountPk(objHolAccTO.getIdHolderAccountPk());
							objHolAccBalHeader.setIdParticipantPk(objHolAccTO.getIdParticipantPk());
							objHolAccBalHeader.setHolder(objHolAccTO.getNumberAccount().toString());
							objHolAccBalHeader.setAlternativeCode(objHolAccTO.getAlternativeCode());
							objHolAccBalHeader.setParticipant(objHolAccTO.getParticipant());
							objHolAccBalHeader.setBlockAmount(BigDecimal.ZERO);
							objHolAccBalHeader.setReblockAmount(BigDecimal.ZERO);
							objHolAccBalHeader.setLstHolderAccountBalanceTO(new ArrayList<HolderAccountBalanceTO>());							
							for(HolderAccountBalanceTO balance:balancesWithCriteria){
								if(objHolAccBalHeader.getIdParticipantPk().equals(balance.getIdParticipantPk()) &&
								   objHolAccBalHeader.getIdHolderAccountPk().equals(balance.getIdHolderAccountPk())){
									List<Integer> lstSelecBenefits = new ArrayList<Integer>();
									for(ParameterTable paramTab:balance.getLstBenefits()){
										Integer strBenefi = paramTab.getParameterTablePk();
										lstSelecBenefits.add(strBenefi);
									}
									if(balance.getQuantityBlockBalance() != null && !balance.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
										balance.setBlCheck(Boolean.TRUE);
										balance.setSelectedBenefits(lstSelecBenefits);
									}
									objHolAccBalHeader.getLstHolderAccountBalanceTO().add(balance);
									validateAmountBlockForPawnWithMarketFact(balance,objHolAccBalHeader);
								}
							}
							if(objHolAccBalHeader.getLstHolderAccountBalanceTO().isEmpty()){
								objHolAccTO.setBlCheck(Boolean.FALSE);
							}else{
								registerPawnOrReserveTO.getLstHolderAccountBalanceHeader().add(objHolAccBalHeader);
							}
						}
					}
					if(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk()!=null 
						 &&	registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk().
							equals(CurrencyType.USD.getCode())
						 && registerPawnOrReserveTO.getBdExchangeRate()!=null){						
						registerPawnOrReserveTO.getBlockValoritationAmount();
						registerPawnOrReserveTO.setBlockValoritationAmount(registerPawnOrReserveTO.getBlockValoritationAmount().
								multiply(registerPawnOrReserveTO.getBdExchangeRate()));
					}
					
					boolean blCoverAmount = true;	
					BigDecimal bdTotal = BigDecimal.ZERO;
					if(registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()!=null){
					  for(HolderAccountBalanceHeaderTO objHolderAccBalHeaderTO: registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()){
							if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO())){


								if(AffectationFormType.CASH_VALUATION.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
									for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
										if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance()) && objHolAccBalTO.isBlCheck()){							
											bdTotal = bdTotal.add(objHolAccBalTO.getValoritationQuantityBlockBalance());
										}
									}
								}
							}

						}

					  if(AffectationFormType.CASH_VALUATION.getCode().equals(registerPawnOrReserveTO.getParameterTableAffectationForm().getParameterTablePk())){
					  	if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getAmount())){
								if(bdTotal.compareTo(registerPawnOrReserveTO.getAmount()) == 0 ||
										bdTotal.compareTo(registerPawnOrReserveTO.getAmount()) == 1)
									blCoverAmount = true;
								else 
									blCoverAmount = false;
							}
						}

					  if(!blCoverAmount){	
							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage("alt.affectation.amount.notCover"));								
							JSFUtilities.showSimpleValidationDialog();
							return;
					  }

					}
				}else{
					Object[] bodyData = new Object[2];
					bodyData[0] = registerPawnOrReserveTO.getHolder().getStateDescription();
					bodyData[1] = registerPawnOrReserveTO.getHolder().getIdHolderPk().toString();
					registerPawnOrReserveTO.setLstHolderAccount(null);
					registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
					registerPawnOrReserveTO.setHolder(new Holder());
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.holder.invalidState", bodyData));
					JSFUtilities.showSimpleValidationDialog();
				}								
			}else{
				registerPawnOrReserveTO.setLstHolderAccount(null);
				registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
				registerPawnOrReserveTO.setHolder(new Holder());
			}
			
			//CUANDO SOLO HAY UNA CUENTA LA SELECCIONAMOS POR DEFECTO
			if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getLstHolderAccount()) &&
					registerPawnOrReserveTO.getLstHolderAccount().size() == 1){
				registerPawnOrReserveTO.getLstHolderAccount().get(0).setBlCheck(true);
			}
			
		}catch (ServiceException e) {
			JSFUtilities.updateComponent(":frmBanAffect:pnlGridHolder");
			log.info(e.getMessage());
			registerPawnOrReserveTO.setHolder(new Holder());
			registerPawnOrReserveTO.setLstHolderAccount(null);
			registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Search list block entity for Ban.
	 */
	public void searchListBlockEntityBan(){
		if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getBlockEntity().getIdBlockEntityPk())){
			if(BlockEntityStateType.OFF.getCode().equals(registerBanOrOthersTO.getBlockEntity().getStateBlockEntity())){
				Object[] bodyData = new Object[1];
				bodyData[0] = registerBanOrOthersTO.getBlockEntity().getIdBlockEntityPk().toString();				
				registerBanOrOthersTO.setBlockEntity(new BlockEntity());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.affectation.block.entity.invalidState", bodyData));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{
			registerBanOrOthersTO.setBlockEntity(new BlockEntity());
		}
	}
	
	/**
	 * Search list block entity for Pawn.
	 */
	public void searchListBlockEntityPawn(){
		if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getBlockEntity().getIdBlockEntityPk())){
			if(BlockEntityStateType.OFF.getCode().equals(registerPawnOrReserveTO.getBlockEntity().getStateBlockEntity())){
				Object[] bodyData = new Object[1];
				bodyData[0] = registerPawnOrReserveTO.getBlockEntity().getIdBlockEntityPk().toString();				
				registerPawnOrReserveTO.setBlockEntity(new BlockEntity());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.affectation.block.entity.invalidState", bodyData));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{
			registerPawnOrReserveTO.setBlockEntity(new BlockEntity());
		}
	}
	
	/**
	 * Change affectation amount.
	 */
	public void changeAffectationAmount(){
		registerBanOrOthersTO.setLstHolderAccount(null);
		registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
		registerBanOrOthersTO.setHolder(new Holder());
		
//		BlockEntity blockEntity = registerBanOrOthersTO.getBlockEntity();
//		Holder holder = registerBanOrOthersTO.getHolder();
//		if(blockEntity!=null && blockEntity.getIdBlockEntityPk()!=null && holder!=null && holder.getIdHolderPk()!=null){
//			if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
//				if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
//					searchListHolderAccountForBan();
//					return;
//				}
//			}
//		}
	}		
	
	/**
	 * Change affectation amount pawn.
	 */
	public void changeAffectationAmountPawn(){
		registerPawnOrReserveTO.setLstHolderAccount(null);
		registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
		registerPawnOrReserveTO.setHolder(new Holder());
	}
	
	/**
	 * Search list holder account for ban.
	 */
	public void searchListHolderAccountForBan(){
		/*Limpiamos el monto total valorizado*/
		registerBanOrOthersTO.setBlockValoritationAmount(BigDecimal.ZERO);
		try{
			if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getHolder().getIdHolderPk())){
				Holder holder = requestAffectationServiceFacade.findHolderByRnt(registerBanOrOthersTO.getHolder().getIdHolderPk());
				if(HolderStateType.REGISTERED.getCode().equals(holder.getStateHolder()) || HolderStateType.BLOCKED.getCode().equals(holder.getStateHolder())){
					
					/*FIND HOLDER ACCOUNTS*/
					List<HolderAccountTO> lstHolderAcc = requestAffectationServiceFacade.findHolderAccountByRNT(registerBanOrOthersTO.getHolder().getIdHolderPk(),
																									userInfo.getUserAccountSession().getParticipantCode());
					registerBanOrOthersTO.setLstHolderAccount(lstHolderAcc);
					registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
					registerBanOrOthersTO.getHolder().setFullName(holder.getFullName());
					
					/*SI ES OPOSICION REALIZA TODAS LAS ACCIONES INMEDIATAMENTE*/
					if(AffectationType.OPPOSITION.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
						if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAcc)){
							for(HolderAccountTO objHolAccTO:lstHolderAcc){
								if(!objHolAccTO.isBlCantCheckForBan())
									objHolAccTO.setBlCheck(true);
							}
							searchListHolderAccountBalanceForBan();
							if(Validations.validateListIsNotNullAndNotEmpty(registerBanOrOthersTO.getLstHolderAccountBalanceHeader())){
								for(HolderAccountBalanceHeaderTO objHolAccBalHeaTO:registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
									for(HolderAccountBalanceTO objHolAccBal:objHolAccBalHeaTO.getLstHolderAccountBalanceTO()){
										objHolAccBal.setBlCheck(true);
										objHolAccBal.setQuantityBlockBalance(objHolAccBal.getTotalBalance().subtract(objHolAccBal.getReportingBalance()));
										validateAmountBlockForBan(objHolAccBal,objHolAccBalHeaTO);
										List<Integer> lstSelecBenefits = new ArrayList<Integer>();
										for(ParameterTable paramTab:objHolAccBal.getLstBenefits()){
											Integer strBenefi = paramTab.getParameterTablePk();
											lstSelecBenefits.add(strBenefi);
										}
										objHolAccBal.setSelectedBenefits(lstSelecBenefits);
									}
								}
							}
						}
					}else if(AffectationType.BAN.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk()) ||
							AffectationType.OTHERS.getCode().equals(registerBanOrOthersTO.getParameterTableBlockMotive().getParameterTablePk())){
						if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){							
							//Add
							if(registerBanOrOthersTO.getAmount() == null){
								throw new ServiceException(ErrorServiceType.BLOCKOPERATION_CASH_WITHOUT_COVER);
							}//End add							
							BigDecimal cashValuation = registerBanOrOthersTO.getAmount();
							Integer currency = registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk();
							registerBanOrOthersTO.setLstHolderAccountBalanceHeader(new ArrayList<HolderAccountBalanceHeaderTO>());							
							//Add
							if(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk() != null){
								if(currency.equals(CurrencyType.PYG.getCode())){
									DailyExchangeRates exchangeRate = generalPametersFacade.getdailyExchangeRate( getCurrentSystemDate(),CurrencyType.USD.getCode());
									if(exchangeRate!=null){
										registerBanOrOthersTO.setBdExchangeRate(exchangeRate.getSellPrice());
									}
								}
							}//End add								
							if(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk() == null){
								throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_NOT_SEL);
							}
							if(registerBanOrOthersTO.getBdExchangeRate() == null){
								throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE);
							}
							
							List<HolderAccountBalanceTO> balancesWithCriteria = requestAffectationServiceFacade.findAutomaticBalancesForBan(lstHolderAcc,cashValuation,currency);
							/*
							for (HolderAccountBalanceTO objHolderAccountBalanceTO: balancesWithCriteria)
							{
								HolderAccountBalanceHeaderTO objHolAccBalHeader = new HolderAccountBalanceHeaderTO();
								objHolAccBalHeader.setIdHolderAccountPk(objHolderAccountBalanceTO.getIdHolderAccountPk());
								objHolAccBalHeader.setIdParticipantPk(objHolderAccountBalanceTO.getIdParticipantPk());
								objHolAccBalHeader.setHolder(objHolderAccountBalanceTO.getIdHolderPk().toString());
								objHolAccBalHeader.setAlternativeCode(objHolderAccountBalanceTO.getAlternateCode());
								objHolAccBalHeader.setParticipant(objHolAccBalHeader.getParticipant());
								objHolAccBalHeader.setBlockAmount(BigDecimal.ZERO);
								objHolAccBalHeader.setReblockAmount(BigDecimal.ZERO);
								objHolAccBalHeader.setLstHolderAccountBalanceTO(new ArrayList<HolderAccountBalanceTO>());
								for(HolderAccountTO objHolderAccountTO: registerBanOrOthersTO.getLstHolderAccount()){
									if (objHolderAccountTO.getIdHolderAccountPk().equals(objHolderAccountBalanceTO.getIdHolderAccountPk()))
									{
										List<String> lstSelecBenefits = new ArrayList<String>();
										for(ParameterTable paramTab:objHolderAccountBalanceTO.getLstBenefits()){
											String strBenefi = paramTab.getParameterTablePk().toString();
											lstSelecBenefits.add(strBenefi);
										}
										if(objHolderAccountBalanceTO.getQuantityBlockBalance() != null && 
										   !objHolderAccountBalanceTO.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
											objHolderAccountBalanceTO.setBlCheck(Boolean.TRUE);
											objHolderAccountBalanceTO.setSelectedBenefits(lstSelecBenefits);
										}
										objHolAccBalHeader.getLstHolderAccountBalanceTO().add(objHolderAccountBalanceTO);
										validateAmountBlockForBanWithFactMarket(objHolderAccountBalanceTO,objHolAccBalHeader);
									}
								}
							}
							*/
							
							for(HolderAccountTO objHolAccTO:registerBanOrOthersTO.getLstHolderAccount()){
								HolderAccountBalanceHeaderTO objHolAccBalHeader = new HolderAccountBalanceHeaderTO();
								objHolAccBalHeader.setIdHolderAccountPk(objHolAccTO.getIdHolderAccountPk());
								objHolAccBalHeader.setIdParticipantPk(objHolAccTO.getIdParticipantPk());
								objHolAccBalHeader.setHolder(objHolAccTO.getNumberAccount().toString());
								objHolAccBalHeader.setAlternativeCode(objHolAccTO.getAlternativeCode());
								objHolAccBalHeader.setParticipant(objHolAccTO.getParticipant());
								objHolAccBalHeader.setBlockAmount(BigDecimal.ZERO);
								objHolAccBalHeader.setReblockAmount(BigDecimal.ZERO);
								objHolAccBalHeader.setLstHolderAccountBalanceTO(new ArrayList<HolderAccountBalanceTO>());
								
								for(HolderAccountBalanceTO balance:balancesWithCriteria){
									if(objHolAccBalHeader.getIdParticipantPk().equals(balance.getIdParticipantPk()) &&
									   objHolAccBalHeader.getIdHolderAccountPk().equals(balance.getIdHolderAccountPk())){
										List<Integer> lstSelecBenefits = new ArrayList<Integer>();
										for(ParameterTable paramTab:balance.getLstBenefits()){
											Integer strBenefi = paramTab.getParameterTablePk();
											lstSelecBenefits.add(strBenefi);
										}
										if(balance.getQuantityBlockBalance() != null && !balance.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
											balance.setBlCheck(Boolean.TRUE);
											balance.setSelectedBenefits(lstSelecBenefits);
										}
										objHolAccBalHeader.getLstHolderAccountBalanceTO().add(balance);
										validateAmountBlockForBanWithFactMarket(balance,objHolAccBalHeader);
									}
								}
								if(objHolAccBalHeader.getLstHolderAccountBalanceTO().isEmpty()){
									objHolAccTO.setBlCheck(Boolean.FALSE);
								}else{
									registerBanOrOthersTO.getLstHolderAccountBalanceHeader().add(objHolAccBalHeader);
								}
							}
						}
					}
					/*
					if(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk()!=null 
							 &&	registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk().
								equals(CurrencyType.USD.getCode())
							 && registerBanOrOthersTO.getBdExchangeRate()!=null){						
						registerBanOrOthersTO.getBlockValoritationAmount();
						registerBanOrOthersTO.setBlockValoritationAmount(registerBanOrOthersTO.getBlockValoritationAmount().
									multiply(registerBanOrOthersTO.getBdExchangeRate()));
					}*/
					
				boolean blCoverAmount = true;	
				BigDecimal bdTotal = BigDecimal.ZERO;
				if(registerBanOrOthersTO.getLstHolderAccountBalanceHeader()!=null){
				  for(HolderAccountBalanceHeaderTO objHolderAccBalHeaderTO: registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
						if(Validations.validateListIsNotNullAndNotEmpty(objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO())){


							if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
								for(HolderAccountBalanceTO objHolAccBalTO:objHolderAccBalHeaderTO.getLstHolderAccountBalanceTO()){
									if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance()) && objHolAccBalTO.isBlCheck()){							
										bdTotal = bdTotal.add(objHolAccBalTO.getValoritationQuantityBlockBalance());
									}
								}
							}
						}

					}

				  if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
				  	if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getAmount())){
							if(bdTotal.compareTo(registerBanOrOthersTO.getAmount()) == 0 ||
									bdTotal.compareTo(registerBanOrOthersTO.getAmount()) == 1)
								blCoverAmount = true;
							else 
								blCoverAmount = false;
						}
					}

				  if(!blCoverAmount){	
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage("alt.affectation.amount.notCover"));								
						JSFUtilities.showSimpleValidationDialog();
						return;
				  }
				}
					
				}else{
					Object[] bodyData = new Object[2];
					bodyData[0] = registerBanOrOthersTO.getHolder().getStateDescription();
					bodyData[1] = registerBanOrOthersTO.getHolder().getIdHolderPk().toString();
					registerBanOrOthersTO.setLstHolderAccount(null);
					registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
					registerBanOrOthersTO.setHolder(new Holder());
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.holder.invalidState", bodyData));
					JSFUtilities.showSimpleValidationDialog();
				}								
			}else{
				registerBanOrOthersTO.setLstHolderAccount(null);
				registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
				registerBanOrOthersTO.setHolder(new Holder());
			}	
		}catch (ServiceException e) {
			log.info(e.getMessage());
			registerBanOrOthersTO.setHolder(new Holder());
			registerBanOrOthersTO.setLstHolderAccount(null);
			registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Show exchange rate.
	 */
	public void showExchangeRate(){
		registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
		registerBanOrOthersTO.setLstHolderAccount(null);
		registerBanOrOthersTO.setHolder(new Holder());
		if(Validations.validateIsNotNullAndNotEmpty(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk())){
			//if(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk().equals(CurrencyType.USD.getCode())){
			if(registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk().equals(CurrencyType.PYG.getCode())){					
				registerBanOrOthersTO.setBdExchangeRate(null);
			}else{
				DailyExchangeRates exchangeRate = generalPametersFacade.getdailyExchangeRate( getCurrentSystemDate(),registerBanOrOthersTO.getParameterTableCurrency().getParameterTablePk());
				if(Validations.validateIsNullOrEmpty(exchangeRate)){
					registerBanOrOthersTO.setBdExchangeRate(null);
					Object[] bodyData = new Object[1];
					bodyData[0] = CommonsUtilities.convertDatetoString(getCurrentSystemDate());
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage("alt.affectation.notExchange",bodyData));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					registerBanOrOthersTO.setBdExchangeRate(exchangeRate.getBuyPrice());
				}						
			}
			//}
		}else
			registerBanOrOthersTO.setBdExchangeRate(null);
	}
	
	/**
	 * Show exchange rate pawn.
	 */
	public void showExchangeRatePawn(){
		registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
		registerPawnOrReserveTO.setLstHolderAccount(null);
		registerPawnOrReserveTO.setHolder(new Holder());
		if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk())){
			//if(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk().equals(CurrencyType.USD.getCode())){
			if(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk().equals(CurrencyType.PYG.getCode())){					
				registerPawnOrReserveTO.setBdExchangeRate(null);
			}else{	
				DailyExchangeRates dailyExchangeRates = generalPametersFacade.getdailyExchangeRate(getCurrentSystemDate(), registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk());
				
				if(Validations.validateIsNullOrEmpty(dailyExchangeRates)){
					registerPawnOrReserveTO.setBdExchangeRate(null);
					Object[] bodyData = new Object[1];
					bodyData[0] = CommonsUtilities.convertDatetoString(getCurrentSystemDate());
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage("alt.affectation.notExchange",bodyData));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					registerPawnOrReserveTO.setBdExchangeRate(dailyExchangeRates.getBuyPrice());
				}						
			}
			//}
		}else { registerPawnOrReserveTO.setBdExchangeRate(null);}		
	}
	
	/**
	 * Search list holder account balance.
	 */
	//aqui
	public void searchListHolderAccountBalance(){
		try{
			boolean blHaveOne = false;
			
			Map<Integer,List<ParameterTable>> mapBenefits=new HashMap<>();
			//Se carga todos los procesos que afectan		
			List<ParameterTable> listBenefitsFixed=new ArrayList<>();
			List<ParameterTable> listBenefitsVariable=new ArrayList<>();
			List<Integer> lstBenefitsFixed = new ArrayList<Integer>();
			lstBenefitsFixed.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
			lstBenefitsFixed.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
			lstBenefitsFixed.add(ImportanceEventType.EARLY_PAYMENT.getCode());
			
			List<Integer> lstBenefitsVariable = new ArrayList<Integer>();
			lstBenefitsVariable.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
			lstBenefitsVariable.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
			lstBenefitsVariable.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
			lstBenefitsVariable.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
			
			listBenefitsFixed=requestAffectationServiceFacade.findParametersFromId(lstBenefitsFixed);
			mapBenefits.put(InstrumentType.FIXED_INCOME.getCode(), listBenefitsFixed);
			listBenefitsVariable=requestAffectationServiceFacade.findParametersFromId(lstBenefitsVariable);
			mapBenefits.put(InstrumentType.VARIABLE_INCOME.getCode(), listBenefitsVariable);
			
			if(Validations.validateListIsNotNullAndNotEmpty(registerPawnOrReserveTO.getLstHolderAccount())){
				List<HolderAccountBalanceHeaderTO> lstHolAccBalaHeader = new ArrayList<HolderAccountBalanceHeaderTO>();
				for(HolderAccountTO objHolAccTO:registerPawnOrReserveTO.getLstHolderAccount()){
					if(objHolAccTO.isBlCheck()  || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						List<HolderAccountBalanceTO> lstHolAccBalTemp = new ArrayList<HolderAccountBalanceTO>();	
						blHaveOne = true;
						HolderAccountBalanceHeaderTO objHolAccBalHeader = new HolderAccountBalanceHeaderTO();
						objHolAccBalHeader.setIdHolderAccountPk(objHolAccTO.getIdHolderAccountPk());
						objHolAccBalHeader.setIdParticipantPk(objHolAccTO.getIdParticipantPk());
						objHolAccBalHeader.setHolder(objHolAccTO.getNumberAccount().toString());
						objHolAccBalHeader.setAlternativeCode(objHolAccTO.getAlternativeCode());
						objHolAccBalHeader.setParticipant(objHolAccTO.getParticipant());
						lstHolAccBalTemp = requestAffectationServiceFacade.findBalancesByHolderAccount(objHolAccTO.getIdHolderAccountPk(),
															objHolAccTO.getIdParticipantPk(), null, 
															userInfo.getUserAccountSession().getIssuerCode(),mapBenefits);
						if(handleMarketFact && !lstHolAccBalTemp.isEmpty()){
							lstHolAccBalTemp = requestAffectationServiceFacade.findAffectationWithMarketFact(lstHolAccBalTemp);
						}			
						for(HolderAccountBalanceTO hab : lstHolAccBalTemp){
							List<Integer> lstSelecBenefits = new ArrayList<Integer>();
							for(ParameterTable paramTab : hab.getLstBenefits()){
								Integer strBenefi = paramTab.getParameterTablePk();
								lstSelecBenefits.add(strBenefi);
							}
							hab.setSelectedBenefits(lstSelecBenefits);
						}
						if(Validations.validateListIsNotNullAndNotEmpty(lstHolAccBalTemp)){
							objHolAccBalHeader.setLstHolderAccountBalanceTO(lstHolAccBalTemp);
							lstHolAccBalaHeader.add(objHolAccBalHeader);
						}else{
							objHolAccTO.setBlCheck(false);
						}
					}
				}
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolAccBalaHeader)){
					registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(lstHolAccBalaHeader);
					removeUploadedFilePawnHandler();
				}else if(!blHaveOne){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage("alt.affectation.account.notSelected"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					registerPawnOrReserveTO.setLstHolderAccountBalanceHeader(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage("alt.affectation.noBalance"));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Search list holder account balance for ban.
	 */
	public void searchListHolderAccountBalanceForBan(){	
		try{
			boolean blHaveOne = false;
			if(Validations.validateListIsNotNullAndNotEmpty(registerBanOrOthersTO.getLstHolderAccount())){
				if(AffectationFormType.CASH_VALUATION.getCode().equals(registerBanOrOthersTO.getParameterTableAffectationForm().getParameterTablePk())){
					if(Validations.validateIsNullOrEmpty(registerBanOrOthersTO.getBdExchangeRate())){
						Object[] bodyData = new Object[1];
						bodyData[0] = CommonsUtilities.convertDatetoString(getCurrentSystemDate());
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.notExchange",bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
				
				Map<Integer,List<ParameterTable>> mapBenefits=new HashMap<>();
				//Se carga todos los procesos que afectan		
				List<ParameterTable> listBenefitsFixed=new ArrayList<>();
				List<ParameterTable> listBenefitsVariable=new ArrayList<>();
				List<Integer> lstBenefitsFixed = new ArrayList<Integer>();
				lstBenefitsFixed.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
				lstBenefitsFixed.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
				lstBenefitsFixed.add(ImportanceEventType.EARLY_PAYMENT.getCode());
				List<Integer> lstBenefitsVariable = new ArrayList<Integer>();
				lstBenefitsVariable.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
				lstBenefitsVariable.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
				lstBenefitsVariable.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
				lstBenefitsVariable.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
				
				listBenefitsFixed=requestAffectationServiceFacade.findParametersFromId(lstBenefitsFixed);
				mapBenefits.put(InstrumentType.FIXED_INCOME.getCode(), listBenefitsFixed);
				listBenefitsVariable=requestAffectationServiceFacade.findParametersFromId(lstBenefitsVariable);
				mapBenefits.put(InstrumentType.VARIABLE_INCOME.getCode(), listBenefitsVariable);
				
				List<HolderAccountBalanceHeaderTO> lstHolAccBalaHeader = new ArrayList<HolderAccountBalanceHeaderTO>();
				for(HolderAccountTO objHolAccTO:registerBanOrOthersTO.getLstHolderAccount()){
					if(objHolAccTO.isBlCheck()  || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
						List<HolderAccountBalanceTO> lstHolAccBalTemp = new ArrayList<HolderAccountBalanceTO>();
						blHaveOne = true;
						HolderAccountBalanceHeaderTO objHolAccBalHeader = new HolderAccountBalanceHeaderTO();
						objHolAccBalHeader.setIdHolderAccountPk(objHolAccTO.getIdHolderAccountPk());
						objHolAccBalHeader.setIdParticipantPk(objHolAccTO.getIdParticipantPk());
						objHolAccBalHeader.setHolder(objHolAccTO.getNumberAccount().toString());
						objHolAccBalHeader.setAlternativeCode(objHolAccTO.getAlternativeCode());
						objHolAccBalHeader.setParticipant(objHolAccTO.getParticipant());
						objHolAccBalHeader.setBlockAmount(BigDecimal.ZERO);
						objHolAccBalHeader.setReblockAmount(BigDecimal.ZERO);
						lstHolAccBalTemp = requestAffectationServiceFacade.findBalancesByHolderAccount(objHolAccTO.getIdHolderAccountPk(),
																			objHolAccTO.getIdParticipantPk(), registerBanOrOthersTO.getIdNegotiationMechanism(),
																			userInfo.getUserAccountSession().getIssuerCode(),mapBenefits);
						
						if(handleMarketFact && !lstHolAccBalTemp.isEmpty()){
							lstHolAccBalTemp = requestAffectationServiceFacade.findAffectationWithMarketFact(lstHolAccBalTemp);
						}
						for(HolderAccountBalanceTO hab : lstHolAccBalTemp){
							List<Integer> lstSelecBenefits = new ArrayList<Integer>();
							for(ParameterTable paramTab : hab.getLstBenefits()){
								Integer strBenefi = paramTab.getParameterTablePk();
								lstSelecBenefits.add(strBenefi);
							}
							hab.setSelectedBenefits(lstSelecBenefits);
						}
						if(Validations.validateListIsNotNullAndNotEmpty(lstHolAccBalTemp)){							
							objHolAccBalHeader.setLstHolderAccountBalanceTO(lstHolAccBalTemp);
							lstHolAccBalaHeader.add(objHolAccBalHeader);							
						}else{
							objHolAccTO.setBlCheck(false);
						}
					}
				}
				
				// add get list mechanism operation
				lstReportedBalance = new ArrayList<MechanismOperationTO>();
				
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolAccBalaHeader)){
					registerBanOrOthersTO.setLstHolderAccountBalanceHeader(lstHolAccBalaHeader);
					removeUploadedFileHandler();
					registerBanOrOthersTO.setBlockValoritationAmount(BigDecimal.ZERO);
					registerBanOrOthersTO.setObservations(null);
					
				}else if(!blHaveOne){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage("alt.affectation.account.notSelected"));
					JSFUtilities.showSimpleValidationDialog();
				}else{								
					registerBanOrOthersTO.setLstHolderAccountBalanceHeader(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage("alt.affectation.noBalance"));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Document file ban handler.
	 *
	 * @param event the event
	 */
	public void documentFileBanHandler(FileUploadEvent event) {
		try {
			 String fDisplayName= fUploadValidateFile(event.getFile(),null,null, super.getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				registerBanOrOthersTO.setFileName(fDisplayName);
				registerBanOrOthersTO.setDocumentFile(event.getFile().getContents());
				
				FileAffectationTO fileAffectationTO  = new FileAffectationTO();
				fileAffectationTO.setDocumentFile(event.getFile().getContents());
				fileAffectationTO.setFileName(fDisplayName);
				String extensionFile = event.getFile().getFileName().toLowerCase().substring(event.getFile().getFileName().lastIndexOf(".")+1,event.getFile().getFileName().length());
				fileAffectationTO.setFileExtension(extensionFile.toUpperCase());
				fileAffectationTO.setFileSize(event.getFile().getSize());
				listFileAffectationTO = new ArrayList<FileAffectationTO>();
				listFileAffectationTO.add(fileAffectationTO);
				
			 }
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the uploaded file handler.
	 */
	public void removeUploadedFileHandler(){
		try {
			registerBanOrOthersTO.setDocumentFile(null); 
			registerBanOrOthersTO.setFileName(null); 
			listFileAffectationTO = null;
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Removes the uploaded file pawn handler.
	 */
	public void removeUploadedFilePawnHandler(){
		try {
			registerPawnOrReserveTO.setDocumentFile(null); 
			registerPawnOrReserveTO.setFileName(null); 
			listFileAffectationTO = null;
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Document file pawn handler.
	 *
	 * @param event the event
	 */
	public void documentFilePawnHandler(FileUploadEvent event) {
		try {
			 String fDisplayName = fUploadValidateFile(event.getFile(),null,null, super.getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				registerPawnOrReserveTO.setFileName(fDisplayName);
				registerPawnOrReserveTO.setDocumentFile(event.getFile().getContents());
				
				FileAffectationTO fileAffectationTO  = new FileAffectationTO();
				fileAffectationTO.setDocumentFile(event.getFile().getContents());
				fileAffectationTO.setFileName(fDisplayName);
				String extensionFile = event.getFile().getFileName().toLowerCase().substring(event.getFile().getFileName().lastIndexOf(".")+1,event.getFile().getFileName().length());
				fileAffectationTO.setFileExtension(extensionFile.toUpperCase());
				fileAffectationTO.setFileSize(event.getFile().getSize());
				listFileAffectationTO = new ArrayList<FileAffectationTO>();
				listFileAffectationTO.add(fileAffectationTO);
				
			 }
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the streamed content file ban.
	 *
	 * @return the streamed content file ban
	 */
	public StreamedContent getStreamedContentFileBan(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(registerBanOrOthersTO.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	registerBanOrOthersTO.getFileName());
			inputStream.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file pawn.
	 *
	 * @return the streamed content file pawn
	 */
	public StreamedContent getStreamedContentFilePawn(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(registerPawnOrReserveTO.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	registerPawnOrReserveTO.getFileName());
			inputStream.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file detail.
	 *
	 * @return the streamed content file detail
	 */
	public StreamedContent getStreamedContentFileDetail(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(requestAffecDetaTO.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	requestAffecDetaTO.getFileName());
			inputStream.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContentFile;
	}
	
	/**
	 * Fill combos.
	 */
	private void fillCombos(){ 
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();	
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			//Lista de estados				
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION_REQUEST.getCode());
			searchRequestAffectationTO.setLstPTState(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de tipos de afectacion				
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			searchRequestAffectationTO.setLstPTTypeAffectation(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de niveles de afectacion						
			parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			searchRequestAffectationTO.setLstPTLevelAffectation(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			/*MOTIVOS DE RECHAZO DE DESAFECTACION*/
			parameterTableTO.setMasterTableFk(MasterTableType.AFECTATION_REJECT_MOTIVE.getCode());
			searchRequestAffectationTO.setLstMotiveReject(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
		}
		catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		 }
	}
	
	/**
	 * Make list type request.
	 *
	 * @param typeRequest the type request
	 */
	private void makeListTypeRequest(Integer typeRequest){
		List<ParameterTable> lstAffectationType = new ArrayList<ParameterTable>(); 
		for(ParameterTable paramTab:searchRequestAffectationTO.getLstPTTypeAffectation()){
			if(REQUEST_BAN_OR_OTHERS_OR_OPPOSITION.equals(typeRequest)){
				//Tipos embargo, otros y oposicion
				if(AffectationType.BAN.getCode().equals(paramTab.getParameterTablePk()) ||
						AffectationType.OTHERS.getCode().equals(paramTab.getParameterTablePk()) ||
							AffectationType.OPPOSITION.getCode().equals(paramTab.getParameterTablePk())){
					lstAffectationType.add(paramTab);					
				}
			}else{
				//Tipos prenda y encaje
				if(AffectationType.PAWN.getCode().equals(paramTab.getParameterTablePk()) ||
						AffectationType.RESERVE.getCode().equals(paramTab.getParameterTablePk())){
					lstAffectationType.add(paramTab);					
				}
			}	
		}
		if(REQUEST_BAN_OR_OTHERS_OR_OPPOSITION.equals(typeRequest))
			registerBanOrOthersTO.setLstBlockMotive(lstAffectationType);
		else
			registerPawnOrReserveTO.setLstTypeRequest(lstAffectationType);
	}
	
	/**
	 * Clear quantities blocks.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	private void clearQuantitiesBlocks(HolderAccountBalanceTO objHolAccBalTO){
		objHolAccBalTO.setQuantityBlockBalance(null);
		objHolAccBalTO.setBlock(null);
		objHolAccBalTO.setReblock(null);
		objHolAccBalTO.setValoritationQuantityBlockBalance(null);
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalPametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
		
	/**
	 * Gets the search request affectation to.
	 *
	 * @return the search request affectation to
	 */
	public SearchRequestAffectationTO getSearchRequestAffectationTO() {
		return searchRequestAffectationTO;
	}
	
	/**
	 * Sets the search request affectation to.
	 *
	 * @param searchRequestAffectationTO the new search request affectation to
	 */
	public void setSearchRequestAffectationTO(
			SearchRequestAffectationTO searchRequestAffectationTO) {
		this.searchRequestAffectationTO = searchRequestAffectationTO;
	}

	/**
	 * Gets the lst request affectation.
	 *
	 * @return the lst request affectation
	 */
	public GenericDataModel<RequestAffectationTO> getLstRequestAffectation() {
		return lstRequestAffectation;
	}

	/**
	 * Sets the lst request affectation.
	 *
	 * @param lstRequestAffectation the new lst request affectation
	 */
	public void setLstRequestAffectation(GenericDataModel<RequestAffectationTO> lstRequestAffectation) {
		this.lstRequestAffectation = lstRequestAffectation;
	}

	/**
	 * Gets the register pawn or reserve to.
	 *
	 * @return the register pawn or reserve to
	 */
	public RegisterPawnOrReserveTO getRegisterPawnOrReserveTO() {
		return registerPawnOrReserveTO;
	}

	/**
	 * Sets the register pawn or reserve to.
	 *
	 * @param registerPawnOrReserveTO the new register pawn or reserve to
	 */
	public void setRegisterPawnOrReserveTO(
			RegisterPawnOrReserveTO registerPawnOrReserveTO) {
		this.registerPawnOrReserveTO = registerPawnOrReserveTO;
	}

	/**
	 * Gets the register ban or others to.
	 *
	 * @return the register ban or others to
	 */
	public RegisterBanOrOthersTO getRegisterBanOrOthersTO() {
		return registerBanOrOthersTO;
	}

	/**
	 * Sets the register ban or others to.
	 *
	 * @param registerBanOrOthersTO the new register ban or others to
	 */
	public void setRegisterBanOrOthersTO(RegisterBanOrOthersTO registerBanOrOthersTO) {
		this.registerBanOrOthersTO = registerBanOrOthersTO;
	}

	/**
	 * Gets the request affec deta to.
	 *
	 * @return the request affec deta to
	 */
	public RequestAffectationDetailTO getRequestAffecDetaTO() {
		return requestAffecDetaTO;
	}

	/**
	 * Sets the request affec deta to.
	 *
	 * @param requestAffecDetaTO the new request affec deta to
	 */
	public void setRequestAffecDetaTO(RequestAffectationDetailTO requestAffecDetaTO) {
		this.requestAffecDetaTO = requestAffecDetaTO;
	}

	/**
	 * Gets the request affectation to.
	 *
	 * @return the request affectation to
	 */
	public RequestAffectationTO getRequestAffectationTO() {
		return requestAffectationTO;
	}

	/**
	 * Sets the request affectation to.
	 *
	 * @param requestAffectationTO the new request affectation to
	 */
	public void setRequestAffectationTO(RequestAffectationTO requestAffectationTO) {
		this.requestAffectationTO = requestAffectationTO;
	}

	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}

	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}

	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}

	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}

	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	//Code implementing with business Component

	/**
	 * Sets the market for ban fact ui.
	 *
	 * @param objHolAccBalTO the new market for ban fact ui
	 */
	public void setMarketForBanFactUI(HolderAccountBalanceTO objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBalTO.getIdParticipantPk());
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		marketFactBalance.setIndTotalBalance(BooleanType.YES.getCode());
		
		if(objHolAccBalTO.getBlockMarketFactDetail()!=null && !objHolAccBalTO.getBlockMarketFactDetail().isEmpty() &&
		   objHolAccBalTO.getMarketLock()!=null && objHolAccBalTO.getMarketLock()){
			
			for(BlockMarketFactTO blockMarketFact: objHolAccBalTO.getBlockMarketFactDetail()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(blockMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(blockMarketFact.getBlockAmount());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}else{
			if(objHolAccBalTO.getQuantityBlockBalance()!=null && !objHolAccBalTO.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
				marketFactBalance.setBalanceResult(objHolAccBalTO.getQuantityBlockBalance());
			}
		}
		try {
			holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		selectMarketFactBalanceForBan(); 
	}
	
	/**
	 * Sets the market for pawn fact ui.
	 *
	 * @param objHolAccBalTO the new market for pawn fact ui
	 */
	public void setMarketForPawnFactUI(HolderAccountBalanceTO objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBalTO.getIdParticipantPk());
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		marketFactBalance.setIndTotalBalance(BooleanType.YES.getCode());
		
		if(objHolAccBalTO.getBlockMarketFactDetail()!=null && !objHolAccBalTO.getBlockMarketFactDetail().isEmpty() &&
		   objHolAccBalTO.getMarketLock()!=null && objHolAccBalTO.getMarketLock()){
			
			for(BlockMarketFactTO blockMarketFact: objHolAccBalTO.getBlockMarketFactDetail()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(blockMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(blockMarketFact.getBlockAmount());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}else{
			if(objHolAccBalTO.getQuantityBlockBalance()!=null && !objHolAccBalTO.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
				marketFactBalance.setBalanceResult(objHolAccBalTO.getQuantityBlockBalance());
			}
		}
		try {
			holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		selectMarketFactBalance();
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void showMarketFactUI(HolderAccountBalanceTO objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBalTO.getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		marketFactBalance.setIndTotalBalance(BooleanType.YES.getCode());
		
		if(objHolAccBalTO.getBlockMarketFactDetail()!=null && !objHolAccBalTO.getBlockMarketFactDetail().isEmpty() &&
		   objHolAccBalTO.getMarketLock()!=null && objHolAccBalTO.getMarketLock()){
			
			for(BlockMarketFactTO blockMarketFact: objHolAccBalTO.getBlockMarketFactDetail()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(blockMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(blockMarketFact.getBlockAmount());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}else{
			if(objHolAccBalTO.getQuantityBlockBalance()!=null && !objHolAccBalTO.getQuantityBlockBalance().equals(BigDecimal.ZERO)){
				marketFactBalance.setBalanceResult(objHolAccBalTO.getQuantityBlockBalance());
			}
		}
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
	}
	
	/**
	 * Show security financial data.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialData(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		for(HolderAccountBalanceHeaderTO habh: this.registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()){
			for(HolderAccountBalanceTO hab: habh.getLstHolderAccountBalanceTO()){
				if(hab.getIdParticipantPk().equals(marketFactBalance.getParticipantPk()) &&
				   hab.getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk()) &&
				   hab.getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
					
					if(!marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
						hab.setQuantityBlockBalance(marketFactBalance.getBalanceResult());
						hab.setBlockMarketFactDetail(new ArrayList<BlockMarketFactTO>());
						
						for(MarketFactDetailHelpTO mkfDetTO: marketFactBalance.getMarketFacBalances()){
							BlockMarketFactTO marketFactTO = new BlockMarketFactTO();
							marketFactTO.setIdHolderMarketPk(mkfDetTO.getMarketFactBalancePk());
							marketFactTO.setBlockAmount(mkfDetTO.getEnteredBalance());
							marketFactTO.setMarketDate(mkfDetTO.getMarketDate());
							marketFactTO.setMarketPrice(mkfDetTO.getMarketPrice());
							marketFactTO.setMarketRate(mkfDetTO.getMarketRate());
							marketFactTO.setTotalBalance(mkfDetTO.getTotalBalance());
							marketFactTO.setAvailableBalance(mkfDetTO.getAvailableBalance());
							hab.getBlockMarketFactDetail().add(marketFactTO);
						}
						validateAmountBlockForPawn(hab, habh);
						//validateAmountBlock(hab);
					}else{
						clearQuantitiesBlocks(hab);
					}
					JSFUtilities.updateComponent("frmPrendAffect:outputPanelResult");
					return;
				}
			}
		}
		marketFactBalance.getTotalBalance();
	}
	
	/**
	 * Select market fact balance for ban.
	 */
	public void selectMarketFactBalanceForBan(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		for(HolderAccountBalanceHeaderTO habh: this.registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
			for(HolderAccountBalanceTO hab: habh.getLstHolderAccountBalanceTO()){
				/*if(hab.getIdParticipantPk().equals(marketFactBalance.getParticipantPk()) &&
				   hab.getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk()) &&
				   hab.getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
					
					if(!marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
						hab.setQuantityBlockBalance(marketFactBalance.getBalanceResult());
						hab.setBlockMarketFactDetail(new ArrayList<BlockMarketFactTO>());
						
						for(MarketFactDetailHelpTO mkfDetTO: marketFactBalance.getMarketFacBalances()){
							BlockMarketFactTO marketFactTO = new BlockMarketFactTO();
							marketFactTO.setIdHolderMarketPk(mkfDetTO.getMarketFactBalancePk());
							marketFactTO.setBlockAmount(mkfDetTO.getEnteredBalance());
							marketFactTO.setMarketDate(mkfDetTO.getMarketDate());
							marketFactTO.setMarketPrice(mkfDetTO.getMarketPrice());
							marketFactTO.setMarketRate(mkfDetTO.getMarketRate());
							marketFactTO.setTotalBalance(mkfDetTO.getTotalBalance());
							marketFactTO.setAvailableBalance(mkfDetTO.getAvailableBalance());
							hab.getBlockMarketFactDetail().add(marketFactTO);
						}*/
						validateAmountBlockForBan(hab, habh);
					/*}else{
						clearQuantitiesBlocks(hab);
					}*/
					JSFUtilities.updateComponent("frmBanAffect:outputPanelResultHABH");
					return;
				//}
			}
		}
	}
	/*Notificacion para bloqueos*/
	public void sendNotificactionsEmail(BlockRequest objBlockRequest,BlockOperation objBlockOpe,BusinessProcess businessProcess){
		List<UserAccountSession> accountSessions=new ArrayList<>();
		try {
			accountSessions=requestAffectationServiceFacade.getListUsersOfIssuerLook(objBlockOpe.getHolderAccount().getIdHolderAccountPk(),objBlockRequest.getIdBlockRequestPk());
			Integer idUserAccountPk=userInfo.getUserAccountSession().getIdUserAccountPk();
			//Verificamos si el usuario se encuentra dentro de la lista de los detinatarios
			for (UserAccountSession userAccountSession : accountSessions) {
				if(idUserAccountPk==userAccountSession.getIdUserAccountPk())
					return;
			}
			//notificacion para el parametro de solo participantes y emai(solo para agencias de bolsa)
			if(accountSessions.size()==0)
				return;
			
			ProcessNotification notification=requestAffectationServiceFacade.getProcessNotification(businessProcess.getIdBusinessProcessPk());
			
			Object[] parameters =new Object[4];
			parameters[0]=objBlockRequest.getIdBlockRequestPk();
			parameters[1]=objBlockOpe.getParticipant().getMnemonic();
			parameters[2]=objBlockOpe.getHolderAccount().getAccountNumber();
			parameters[3]=objBlockOpe.getSecurities().getIdSecurityCodePk();
			
			if(Validations.validateIsNotNullAndNotEmpty(notification)){
				String message=notification.getNotificationMessage();
				String messageNotification=null;
				if(message.contains("%s")){
					messageNotification=String.format(message, parameters);
				}else{
					messageNotification=message;
				}
				//notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
				//		businessProcess, null, parameters);
				notificationServiceFacade.sendNotificationManual(userInfo.getUserAccountSession().getUserName(), businessProcess, accountSessions,
						notification.getNotificationSubject(), messageNotification, NotificationType.EMAIL);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the handle market fact.
	 *
	 * @return the handle market fact
	 */
	public Boolean getHandleMarketFact() {
		return handleMarketFact;
	}

	/**
	 * Sets the handle market fact.
	 *
	 * @param handleMarketFact the new handle market fact
	 */
	public void setHandleMarketFact(Boolean handleMarketFact) {
		this.handleMarketFact = handleMarketFact;
	}
	
	//Add 
	
	/**
	 * Validate amount block for pawn with market fact.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void validateAmountBlockForPawnWithMarketFact(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		validateAmountBlockForPawn(objHolAccBalTO, objHolAccBalHeTO);
		if(objHolAccBalTO.getQuantityBlockBalance()!=null || objHolAccBalTO.getBlock()!=null || 
				objHolAccBalTO.getReblock()!=null || objHolAccBalTO.getValoritationQuantityBlockBalance()!=null){
			// setMarketForPawnFactUI(objHolAccBalTO);
		}
	}
	
	/**
	 * Validate amount block for pawn.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void validateAmountBlockForPawn(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		//Validacion de los montos para embargo, otros u oposicion
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getQuantityBlockBalance())){
			//checking if the value has cat
			if(!objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPF.getCode()) && !objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				if(objHolAccBalTO.getQuantityBlockBalance().compareTo(objHolAccBalTO.getAvailableBalance())>0
						&& (objHolAccBalTO.getQuantityBlockBalance().compareTo(objHolAccBalTO.getTotalBalance())==0 || (objHolAccBalTO.getQuantityBlockBalance().compareTo(objHolAccBalTO.getTotalBalance())<0))) {
					if(searchAccreditationOperationByUnblock(objHolAccBalTO, objHolAccBalHeTO))
						return;
				}
			}
			//
			BigDecimal total = objHolAccBalTO.getTotalBalance().subtract(objHolAccBalTO.getReportingBalance());
			BigDecimal quantityBlock = objHolAccBalTO.getQuantityBlockBalance();
			if(registerPawnOrReserveTO.isBlCashValuation()){				
				if(quantityBlock.compareTo(total) == 1){
					subTractAmountPawn(objHolAccBalTO, objHolAccBalHeTO);
					clearQuantitiesBlocks(objHolAccBalTO);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					BigDecimal available = objHolAccBalTO.getAvailableBalance();
					if(available.compareTo(quantityBlock) >= 0){
						objHolAccBalTO.setBlock(quantityBlock);
						objHolAccBalTO.setReblock(BigDecimal.ZERO);
						if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getLastQuotation())
								&& objHolAccBalTO.getLastQuotation().compareTo(BigDecimal.ZERO) == 1){
							if(objHolAccBalTO.getCurrency().equals(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk()))
								objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()));
							else{
								if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).divide(registerPawnOrReserveTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
								else
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).multiply(registerPawnOrReserveTO.getBdExchangeRate()));
							}
						}else{
							if(objHolAccBalTO.getCurrency().equals(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk()))
								objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()));
							else{
								if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).divide(registerPawnOrReserveTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
								else
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).multiply(registerPawnOrReserveTO.getBdExchangeRate()));
							}
						}
						updateBlockAndReblockAmountPawn(objHolAccBalTO,objHolAccBalHeTO);
					}else if(quantityBlock.compareTo(available) == 1){
						BigDecimal partialResult = quantityBlock.subtract(available);
						if(total.compareTo(partialResult) >= 0){
							objHolAccBalTO.setBlock(available);
							objHolAccBalTO.setReblock(partialResult);
							if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getLastQuotation())
									&& objHolAccBalTO.getLastQuotation().compareTo(BigDecimal.ZERO) == 1){
								if(objHolAccBalTO.getCurrency().equals(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()));
								else{
									if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).divide(registerPawnOrReserveTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
									else
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getLastQuotation()).multiply(registerPawnOrReserveTO.getBdExchangeRate()));
								}
							}else{
								if(objHolAccBalTO.getCurrency().equals(registerPawnOrReserveTO.getParameterTableCurrency().getParameterTablePk()))
									objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()));
								else{
									if(CurrencyType.PYG.getCode().equals(objHolAccBalTO.getCurrency()))
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).divide(registerPawnOrReserveTO.getBdExchangeRate(), 2 ,BigDecimal.ROUND_HALF_UP));
									else
										objHolAccBalTO.setValoritationQuantityBlockBalance(quantityBlock.multiply(objHolAccBalTO.getNominalValue()).multiply(registerPawnOrReserveTO.getBdExchangeRate()));
								}
							}
							updateBlockAndReblockAmountPawn(objHolAccBalTO,objHolAccBalHeTO);
						}else{
							subTractAmountPawn(objHolAccBalTO, objHolAccBalHeTO);
							clearQuantitiesBlocks(objHolAccBalTO);
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
							JSFUtilities.showSimpleValidationDialog();
						}
					}else{
						subTractAmountPawn(objHolAccBalTO, objHolAccBalHeTO);
						clearQuantitiesBlocks(objHolAccBalTO);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}else{			
				if(quantityBlock.compareTo(total) == 1){					
					clearQuantitiesBlocks(objHolAccBalTO);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
					JSFUtilities.showSimpleValidationDialog();
				}else{
					BigDecimal available = objHolAccBalTO.getAvailableBalance();
					if(available.compareTo(quantityBlock) >= 0){
						objHolAccBalTO.setBlock(quantityBlock);
						objHolAccBalTO.setReblock(BigDecimal.ZERO);
					}else if(quantityBlock.compareTo(available) == 1){
						BigDecimal partialResult = quantityBlock.subtract(available);
						if(total.compareTo(partialResult) >= 0){
							objHolAccBalTO.setBlock(available);
							objHolAccBalTO.setReblock(partialResult);
						}else{							
							clearQuantitiesBlocks(objHolAccBalTO);
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
							JSFUtilities.showSimpleValidationDialog();
						}
					}else{						
						clearQuantitiesBlocks(objHolAccBalTO);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("alt.affectation.tooMuchQuantity"));
						JSFUtilities.showSimpleValidationDialog();
					}
				}
			}
		}else{
			subTractAmountPawn(objHolAccBalTO, objHolAccBalHeTO);
			clearQuantitiesBlocks(objHolAccBalTO);	
		}
	}
	
	/**
	 * Sub tract amount pawn.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	private void subTractAmountPawn(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		if(registerPawnOrReserveTO.isBlCashValuation()){	
			objHolAccBalTO.setBlockTemp(BigDecimal.ZERO);
			BigDecimal currenBlocktAmount = objHolAccBalHeTO.getBlockAmount();
			BigDecimal newBlockAmount = currenBlocktAmount;
			if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getBlock()))
				newBlockAmount = newBlockAmount.subtract(objHolAccBalTO.getBlock());			
			objHolAccBalHeTO.setBlockAmount(newBlockAmount);
			objHolAccBalTO.setReblockTemp(BigDecimal.ZERO);
			
			BigDecimal currenReBlocktAmount = objHolAccBalHeTO.getReblockAmount();
			BigDecimal newReBlockAmount = currenReBlocktAmount;
			if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getReblock()))
				newReBlockAmount = newReBlockAmount.subtract(objHolAccBalTO.getReblock());
			objHolAccBalHeTO.setReblockAmount(newReBlockAmount);
			
			if(Validations.validateIsNotNullAndNotEmpty(registerPawnOrReserveTO.getBlockValoritationAmount())
					&& Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getValoritationQuantityBlockBalance())){
				objHolAccBalTO.setValoritationQuantityBlockBalanceTemp(BigDecimal.ZERO);
				registerPawnOrReserveTO.setBlockValoritationAmount(registerPawnOrReserveTO.getBlockValoritationAmount().subtract(objHolAccBalTO.getValoritationQuantityBlockBalance()));
			}
		}
	}
	
	/**
	 * Check automatic dpf for pawn.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void checkAutomaticDPFForPawn(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		if(objHolAccBalTO.isBlCheck()){
			if(objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPF.getCode()) || objHolAccBalTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				//checking if the value has cat
				if(searchAccreditationOperationByUnblock(objHolAccBalTO, objHolAccBalHeTO))
					return;
				//
				objHolAccBalTO.setBlCheck(true);
				objHolAccBalTO.setQuantityBlockBalance(objHolAccBalTO.getTotalBalance());
				validateAmountBlockForPawnWithMarketFact(objHolAccBalTO,objHolAccBalHeTO);			   
			}
		}
		updateBlockAndReblockAmountPawn(objHolAccBalTO,objHolAccBalHeTO);
	}
	
	/**
	 * Update block and reblock amount pawn.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param objHolAccBalHeTO the obj hol acc bal he to
	 */
	public void updateBlockAndReblockAmountPawn(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO){
		if(registerPawnOrReserveTO.isBlCashValuation()){
			if(objHolAccBalTO.isBlCheck()){	
				if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getBlockTemp()))
					objHolAccBalTO.setBlockTemp(BigDecimal.ZERO);
				
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getBlock())){
					if(objHolAccBalTO.getBlock().compareTo(objHolAccBalTO.getBlockTemp()) != 0){
						BigDecimal currenBlocktAmount = objHolAccBalHeTO.getBlockAmount();						
						BigDecimal newBlockAmount = currenBlocktAmount;						
						newBlockAmount = newBlockAmount.subtract(objHolAccBalTO.getBlockTemp());
						newBlockAmount = newBlockAmount.add(objHolAccBalTO.getBlock());				
						objHolAccBalHeTO.setBlockAmount(newBlockAmount);
						objHolAccBalTO.setBlockTemp(objHolAccBalTO.getBlock());
					}
				}
				
				if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getReblockTemp()))
					objHolAccBalTO.setReblockTemp(BigDecimal.ZERO);
				
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getReblock())){
					if(objHolAccBalTO.getReblock().compareTo(objHolAccBalTO.getReblockTemp()) != 0){
						BigDecimal currenReBlocktAmount = objHolAccBalHeTO.getReblockAmount();
						BigDecimal newReBlockAmount = currenReBlocktAmount;
						newReBlockAmount = newReBlockAmount.subtract(objHolAccBalTO.getReblockTemp());
						newReBlockAmount = newReBlockAmount.add(objHolAccBalTO.getReblock());
						objHolAccBalHeTO.setReblockAmount(newReBlockAmount);
						objHolAccBalTO.setReblockTemp(objHolAccBalTO.getReblock());
					}				
				}
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTO.getValoritationQuantityBlockBalance())){
					if(Validations.validateIsNullOrEmpty(objHolAccBalTO.getValoritationQuantityBlockBalanceTemp()))
						objHolAccBalTO.setValoritationQuantityBlockBalanceTemp(BigDecimal.ZERO);					
					registerPawnOrReserveTO.setBlockValoritationAmount(registerPawnOrReserveTO.getBlockValoritationAmount().subtract(objHolAccBalTO.getValoritationQuantityBlockBalanceTemp()));					
					if(!objHolAccBalTO.getValoritationQuantityBlockBalance().equals(objHolAccBalTO.getValoritationQuantityBlockBalanceTemp())){
						objHolAccBalTO.setValoritationQuantityBlockBalanceTemp(objHolAccBalTO.getValoritationQuantityBlockBalance());
						registerPawnOrReserveTO.setBlockValoritationAmount(registerPawnOrReserveTO.getBlockValoritationAmount().add(objHolAccBalTO.getValoritationQuantityBlockBalance()));	
					}					
				}
			}else{
				subTractAmountPawn(objHolAccBalTO, objHolAccBalHeTO);	
				clearRowForAffectation(objHolAccBalTO);
			}
		}else{
			if(!objHolAccBalTO.isBlCheck()){
				clearRowForAffectation(objHolAccBalTO);
			}
		}			
	}
	
	/**
	 * Accreditation expiration process for pawn
	 */
	@LoggerAuditWeb
	public void expirationAccreditationForPawn(){
		//Accreditation expiration process
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTOTemp) && Validations.validateIsNotNullAndNotEmpty(objHolAccBalHeTOTemp)) {
			Security objSecurityAccreditation = new Security();
			AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
			accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
			objSecurityAccreditation.setIdSecurityCodePk(objHolAccBalTOTemp.getIdSecurityCodePk());
			accreditationOperationTO.setSecurity(objSecurityAccreditation);	
			try {
				List<AccreditationDetail> lstAccreditationDetail = new ArrayList<>();
				lstAccreditationDetail = accreditationCertificatesServiceFacade.expirationAcreditationFromAffectation(accreditationOperationTO);
				//Notifying the participant that I generated the cat
				if(Validations.validateListIsNotNullAndNotEmpty(lstAccreditationDetail)) {
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_BLOCK.getCode());
					for(AccreditationDetail objAccreditationDetail:lstAccreditationDetail){
					    String descAffectationType = AffectationType.PAWN.getValue();
						AccreditationOperation accreditationOperationTemp = accreditationCertificatesServiceFacade.findAccreditationOperation(objAccreditationDetail.getAccreditationOperation().getIdAccreditationOperationPk());
						Object[] parameters =new Object[5];
						parameters[0]=userInfo.getUserAccountSession().getUserInstitutionName();
						parameters[1]=accreditationOperationTemp.getCertificateNumber();
						parameters[2]=accreditationOperationTemp.getHolder().getFullName();
						parameters[3]=accreditationOperationTemp.getHolder().getIdHolderPk();
						parameters[4]=descAffectationType;
						Long idParticipantPk = null;
						if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTemp.getParticipantPetitioner()))
							idParticipantPk = accreditationOperationTemp.getParticipantPetitioner().getIdParticipantPk();
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,idParticipantPk, parameters);
					}
				}
				searchListHolderAccountBalance();
				for(HolderAccountBalanceHeaderTO holAccBalHeaderTO:registerPawnOrReserveTO.getLstHolderAccountBalanceHeader()){
					if(Validations.validateIsNotNullAndPositive(holAccBalHeaderTO.getIdHolderAccountPk()) && Validations.validateIsNotNullAndPositive(holAccBalHeaderTO.getIdParticipantPk()))
					if(holAccBalHeaderTO.getIdHolderAccountPk().equals(objHolAccBalHeTOTemp.getIdHolderAccountPk())
							&& holAccBalHeaderTO.getIdParticipantPk().equals(objHolAccBalHeTOTemp.getIdParticipantPk())) {
						objHolAccBalHeTOTemp = holAccBalHeaderTO;
						break;
					}
				}
				for(HolderAccountBalanceTO holAccBalTO:objHolAccBalHeTOTemp.getLstHolderAccountBalanceTO()){
					if(Validations.validateIsNotNullAndPositive(holAccBalTO.getIdHolderAccountPk()) && Validations.validateIsNotNullAndPositive(holAccBalTO.getIdParticipantPk()) && Validations.validateIsNotNullAndNotEmpty(holAccBalTO.getIdSecurityCodePk())) 
					if (holAccBalTO.getIdHolderAccountPk().equals(objHolAccBalTOTemp.getIdHolderAccountPk())
							//&& holAccBalTO.getIdHolderPk().equals(objHolAccBalTOTemp.getIdHolderPk())
							&& holAccBalTO.getIdParticipantPk().equals(objHolAccBalTOTemp.getIdParticipantPk())
							&& holAccBalTO.getIdSecurityCodePk().equals(objHolAccBalTOTemp.getIdSecurityCodePk())) {
						BigDecimal quantityBlockBalance = objHolAccBalTOTemp.getQuantityBlockBalance();
						objHolAccBalTOTemp = holAccBalTO; 
						objHolAccBalTOTemp.setQuantityBlockBalance(quantityBlockBalance);
						break;
					}
				}
				objHolAccBalTOTemp.setBlCheck(true);
				if(objHolAccBalTOTemp.getSecurityClass().equals(SecurityClassType.DPF.getCode()) || objHolAccBalTOTemp.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
					checkAutomaticDPFForPawn(objHolAccBalTOTemp,objHolAccBalHeTOTemp);	
				}else {
					validateAmountBlockForPawnWithMarketFact(objHolAccBalTOTemp,objHolAccBalHeTOTemp);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				objHolAccBalTOTemp.setBlCheck(false);
				if(e.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
	}
	
	/**
	 * Accreditation expiration process
	 */
	@LoggerAuditWeb
	public void expirationAccreditationForBan(){
		//Accreditation expiration process
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTOTemp) && Validations.validateIsNotNullAndNotEmpty(objHolAccBalHeTOTemp)) {
			Security objSecurityAccreditation = new Security();
			AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
			accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
			objSecurityAccreditation.setIdSecurityCodePk(objHolAccBalTOTemp.getIdSecurityCodePk());
			accreditationOperationTO.setSecurity(objSecurityAccreditation);	
			try {
				List<AccreditationDetail> lstAccreditationDetail = new ArrayList<>();
				lstAccreditationDetail = accreditationCertificatesServiceFacade.expirationAcreditationFromAffectation(accreditationOperationTO);
				//Notifying the participant that I generated the cat
				if(Validations.validateListIsNotNullAndNotEmpty(lstAccreditationDetail)) {
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_BLOCK.getCode());
					String descAffectationType = AffectationType.BAN.getValue()+"/"+AffectationType.OTHERS.getValue();
					for(AccreditationDetail objAccreditationDetail:lstAccreditationDetail){
						AccreditationOperation accreditationOperationTemp = accreditationCertificatesServiceFacade.findAccreditationOperation(objAccreditationDetail.getAccreditationOperation().getIdAccreditationOperationPk());
						Object[] parameters =new Object[5];
						parameters[0]=userInfo.getUserAccountSession().getUserInstitutionName();
						parameters[1]=accreditationOperationTemp.getCertificateNumber();
						parameters[2]=accreditationOperationTemp.getHolder().getFullName();
						parameters[3]=accreditationOperationTemp.getHolder().getIdHolderPk();
						parameters[4]=descAffectationType;
						Long idParticipantPk = null;
						if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTemp.getParticipantPetitioner()))
							idParticipantPk = accreditationOperationTemp.getParticipantPetitioner().getIdParticipantPk();
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,idParticipantPk, parameters);
					}
				}
				searchListHolderAccountBalanceForBan();
				for(HolderAccountBalanceHeaderTO holAccBalHeaderTO:registerBanOrOthersTO.getLstHolderAccountBalanceHeader()){
					if(Validations.validateIsNotNullAndPositive(holAccBalHeaderTO.getIdHolderAccountPk()) && Validations.validateIsNotNullAndPositive(holAccBalHeaderTO.getIdParticipantPk()))
					if(holAccBalHeaderTO.getIdHolderAccountPk().equals(objHolAccBalHeTOTemp.getIdHolderAccountPk())
							&& holAccBalHeaderTO.getIdParticipantPk().equals(objHolAccBalHeTOTemp.getIdParticipantPk())) {
						objHolAccBalHeTOTemp = holAccBalHeaderTO;
						break;
					}
				}
				for(HolderAccountBalanceTO holAccBalTO:objHolAccBalHeTOTemp.getLstHolderAccountBalanceTO()){
					if(Validations.validateIsNotNullAndPositive(holAccBalTO.getIdHolderAccountPk()) && Validations.validateIsNotNullAndPositive(holAccBalTO.getIdParticipantPk()) && Validations.validateIsNotNullAndNotEmpty(holAccBalTO.getIdSecurityCodePk())) 
					if (holAccBalTO.getIdHolderAccountPk().equals(objHolAccBalTOTemp.getIdHolderAccountPk())
							//&& holAccBalTO.getIdHolderPk().equals(objHolAccBalTOTemp.getIdHolderPk())
							&& holAccBalTO.getIdParticipantPk().equals(objHolAccBalTOTemp.getIdParticipantPk())
							&& holAccBalTO.getIdSecurityCodePk().equals(objHolAccBalTOTemp.getIdSecurityCodePk())) {
						BigDecimal quantityBlockBalance = objHolAccBalTOTemp.getQuantityBlockBalance();
						objHolAccBalTOTemp = holAccBalTO; 
						objHolAccBalTOTemp.setQuantityBlockBalance(quantityBlockBalance);
						break;
					}
				}
				objHolAccBalTOTemp.setBlCheck(true);
				if(objHolAccBalTOTemp.getSecurityClass().equals(SecurityClassType.DPF.getCode()) || objHolAccBalTOTemp.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
					checkAutomaticDPFForBan(objHolAccBalTOTemp,objHolAccBalHeTOTemp);
				}else {
					validateAmountBlockForBanWithFactMarket(objHolAccBalTOTemp,objHolAccBalHeTOTemp);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
				objHolAccBalTOTemp.setBlCheck(false);
				if(e.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
										PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
	}
	
	public boolean searchAccreditationOperationByUnblock(HolderAccountBalanceTO objHolAccBalTO, HolderAccountBalanceHeaderTO objHolAccBalHeTO) {
		objHolAccBalTOTemp = null;
		objHolAccBalHeTOTemp = null;
		Security objSecurityAccreditation = new Security();
		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
		objSecurityAccreditation.setIdSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		accreditationOperationTO.setSecurity(objSecurityAccreditation);	
		try {
			List<AccreditationOperation> lstAccreditationOperation = accreditationCertificatesServiceFacade.searchAccreditationOperationByUnblock(accreditationOperationTO);
			if (Validations.validateListIsNotNullAndNotEmpty(lstAccreditationOperation)) {
				objHolAccBalTOTemp = objHolAccBalTO;
				objHolAccBalHeTOTemp = objHolAccBalHeTO;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage("alt.affectation.has.cat"));
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogAccreditation').show()");
				return true;
			}
		} catch (ServiceException e) 
			{e.printStackTrace();}
		return false;
	}
	
	public void hideDialogsCleanCheck() {
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalTOTemp)) {
			objHolAccBalTOTemp.setBlCheck(false);
			objHolAccBalTOTemp.setQuantityBlockBalance(null);
			clearQuantitiesBlocks(objHolAccBalTOTemp);	
		}
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotiveReject:dialogMotiveReject");
		JSFUtilities.hideComponent(":frmMotiveReject:dialogMotiveReject");	
	}

	/**
	 * Gets the list file affectation to.
	 *
	 * @return the list file affectation to
	 */
	public List<FileAffectationTO> getListFileAffectationTO() {
		return listFileAffectationTO;
	}

	/**
	 * Sets the list file affectation to.
	 *
	 * @param listFileAffectationTO the new list file affectation to
	 */
	public void setListFileAffectationTO(
			List<FileAffectationTO> listFileAffectationTO) {
		this.listFileAffectationTO = listFileAffectationTO;
	}

	/**
	 * Checks if is bl approve.
	 *
	 * @return true, if is bl approve
	 */
	public boolean isBlApprove() {
		return blApprove;
	}

	/**
	 * Sets the bl approve.
	 *
	 * @param blApprove the new bl approve
	 */
	public void setBlApprove(boolean blApprove) {
		this.blApprove = blApprove;
	}

	/**
	 * Checks if is bl annul.
	 *
	 * @return true, if is bl annul
	 */
	public boolean isBlAnnul() {
		return blAnnul;
	}

	/**
	 * Sets the bl annul.
	 *
	 * @param blAnnul the new bl annul
	 */
	public void setBlAnnul(boolean blAnnul) {
		this.blAnnul = blAnnul;
	}

	/**
	 * Checks if is blview detail.
	 *
	 * @return true, if is blview detail
	 */
	public boolean isBlviewDetail() {
		return blviewDetail;
	}

	/**
	 * Sets the blview detail.
	 *
	 * @param blviewDetail the new blview detail
	 */
	public void setBlviewDetail(boolean blviewDetail) {
		this.blviewDetail = blviewDetail;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Checks if is flag participant.
	 *
	 * @return true, if is flag participant
	 */
	public boolean isFlagParticipant() {
		return flagParticipant;
	}

	/**
	 * Sets the flag participant.
	 *
	 * @param flagParticipant the new flag participant
	 */
	public void setFlagParticipant(boolean flagParticipant) {
		this.flagParticipant = flagParticipant;
	}

	/**
	 * Checks if is flag issuer block.
	 *
	 * @return true, if is flag issuer block
	 */
	public boolean isFlagIssuerBlock() {
		return flagIssuerBlock;
	}

	/**
	 * Sets the flag issuer block.
	 *
	 * @param flagIssuerBlock the new flag issuer block
	 */
	public void setFlagIssuerBlock(boolean flagIssuerBlock) {
		this.flagIssuerBlock = flagIssuerBlock;
	}

	/**
	 * Gets the lst reported balance.
	 *
	 * @return the lstReportedBalance
	 */
	public List<MechanismOperationTO> getLstReportedBalance() {
		return lstReportedBalance;
	}

	/**
	 * Sets the lst reported balance.
	 *
	 * @param lstReportedBalance the lstReportedBalance to set
	 */
	public void setLstReportedBalance(List<MechanismOperationTO> lstReportedBalance) {
		this.lstReportedBalance = lstReportedBalance;
	}
	
}