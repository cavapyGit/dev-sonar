package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class SearchBlockOperationTO.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/
public class SearchBlockOperationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The participant. */
	private Participant participant;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The lst issuers. */
	private List<Issuer> lstIssuers;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The param table type affectation. */
	private ParameterTable paramTableTypeAffectation;
	
	/** The param table level affectation. */
	private ParameterTable paramTableLevelAffectation;
	
	/** The param table state affectation. */
	private ParameterTable paramTableStateAffectation;
	
	/** The param table security class. */
	private ParameterTable paramTableSecurityClass;
	
	/** The param table currency. */
	private ParameterTable paramTableCurrency;
	
	/** The lst type affectation. */
	private List<ParameterTable> lstTypeAffectation;
	
	/** The lst level affectation. */
	private List<ParameterTable> lstLevelAffectation;
	
	/** The lst state affectation. */
	private List<ParameterTable> lstStateAffectation;
	
	/** The lst security class. */
	private List<ParameterTable> lstSecurityClass;
	
	/** The lst currency. */
	private List<ParameterTable> lstCurrency;
	
	/** The lst block operation. */
	private List<BlockOperationTO> lstBlockOperation;	
	
	/** The security. */
	private Security security;
	
	/** The issuer. */
	private Issuer issuer;
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the param table type affectation.
	 *
	 * @return the param table type affectation
	 */
	public ParameterTable getParamTableTypeAffectation() {
		return paramTableTypeAffectation;
	}
	
	/**
	 * Sets the param table type affectation.
	 *
	 * @param paramTableTypeAffectation the new param table type affectation
	 */
	public void setParamTableTypeAffectation(
			ParameterTable paramTableTypeAffectation) {
		this.paramTableTypeAffectation = paramTableTypeAffectation;
	}
	
	/**
	 * Gets the param table level affectation.
	 *
	 * @return the param table level affectation
	 */
	public ParameterTable getParamTableLevelAffectation() {
		return paramTableLevelAffectation;
	}
	
	/**
	 * Sets the param table level affectation.
	 *
	 * @param paramTableLevelAffectation the new param table level affectation
	 */
	public void setParamTableLevelAffectation(
			ParameterTable paramTableLevelAffectation) {
		this.paramTableLevelAffectation = paramTableLevelAffectation;
	}
	
	/**
	 * Gets the param table state affectation.
	 *
	 * @return the param table state affectation
	 */
	public ParameterTable getParamTableStateAffectation() {
		return paramTableStateAffectation;
	}
	
	/**
	 * Sets the param table state affectation.
	 *
	 * @param paramTableStateAffectation the new param table state affectation
	 */
	public void setParamTableStateAffectation(
			ParameterTable paramTableStateAffectation) {
		this.paramTableStateAffectation = paramTableStateAffectation;
	}
	
	/**
	 * Gets the lst type affectation.
	 *
	 * @return the lst type affectation
	 */
	public List<ParameterTable> getLstTypeAffectation() {
		return lstTypeAffectation;
	}
	
	/**
	 * Sets the lst type affectation.
	 *
	 * @param lstTypeAffectation the new lst type affectation
	 */
	public void setLstTypeAffectation(List<ParameterTable> lstTypeAffectation) {
		this.lstTypeAffectation = lstTypeAffectation;
	}
	
	/**
	 * Gets the lst level affectation.
	 *
	 * @return the lst level affectation
	 */
	public List<ParameterTable> getLstLevelAffectation() {
		return lstLevelAffectation;
	}
	
	/**
	 * Sets the lst level affectation.
	 *
	 * @param lstLevelAffectation the new lst level affectation
	 */
	public void setLstLevelAffectation(List<ParameterTable> lstLevelAffectation) {
		this.lstLevelAffectation = lstLevelAffectation;
	}
	
	/**
	 * Gets the lst state affectation.
	 *
	 * @return the lst state affectation
	 */
	public List<ParameterTable> getLstStateAffectation() {
		return lstStateAffectation;
	}
	
	/**
	 * Sets the lst state affectation.
	 *
	 * @param lstStateAffectation the new lst state affectation
	 */
	public void setLstStateAffectation(List<ParameterTable> lstStateAffectation) {
		this.lstStateAffectation = lstStateAffectation;
	}
	
	/**
	 * Gets the lst block operation.
	 *
	 * @return the lst block operation
	 */
	public List<BlockOperationTO> getLstBlockOperation() {
		return lstBlockOperation;
	}
	
	/**
	 * Sets the lst block operation.
	 *
	 * @param lstBlockOperation the new lst block operation
	 */
	public void setLstBlockOperation(List<BlockOperationTO> lstBlockOperation) {
		this.lstBlockOperation = lstBlockOperation;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the lst issuers.
	 *
	 * @return the lst issuers
	 */
	public List<Issuer> getLstIssuers() {
		return lstIssuers;
	}
	
	/**
	 * Sets the lst issuers.
	 *
	 * @param lstIssuers the new lst issuers
	 */
	public void setLstIssuers(List<Issuer> lstIssuers) {
		this.lstIssuers = lstIssuers;
	}
	
	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}
	
	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	
	/**
	 * Gets the lst currency.
	 *
	 * @return the lst currency
	 */
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}
	
	/**
	 * Sets the lst currency.
	 *
	 * @param lstCurrency the new lst currency
	 */
	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}
	
	/**
	 * Gets the param table security class.
	 *
	 * @return the param table security class
	 */
	public ParameterTable getParamTableSecurityClass() {
		return paramTableSecurityClass;
	}
	
	/**
	 * Sets the param table security class.
	 *
	 * @param paramTableSecurityClass the new param table security class
	 */
	public void setParamTableSecurityClass(ParameterTable paramTableSecurityClass) {
		this.paramTableSecurityClass = paramTableSecurityClass;
	}
	
	/**
	 * Gets the param table currency.
	 *
	 * @return the param table currency
	 */
	public ParameterTable getParamTableCurrency() {
		return paramTableCurrency;
	}
	
	/**
	 * Sets the param table currency.
	 *
	 * @param paramTableCurrency the new param table currency
	 */
	public void setParamTableCurrency(ParameterTable paramTableCurrency) {
		this.paramTableCurrency = paramTableCurrency;
	}
}