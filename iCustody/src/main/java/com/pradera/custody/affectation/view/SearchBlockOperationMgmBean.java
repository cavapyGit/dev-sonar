package com.pradera.custody.affectation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;
import com.pradera.custody.affectation.to.BlockOperationDetailTO;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.affectation.to.SearchBlockOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class SearchBlockOperationMgmBean.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/
@DepositaryWebBean
public class SearchBlockOperationMgmBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The account facade. */
	@Inject
	AccountsFacade accountFacade;
	
	/** The request affectation facade. */
	@EJB
	RequestAffectationServiceFacade requestAffectationFacade;
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The search block operation to. */
	private SearchBlockOperationTO searchBlockOperationTO;
	
	/** The block operation detail to. */
	private BlockOperationDetailTO blockOperationDetailTO;
	
	/** The bl no result. */
	private boolean blNoResult;	
	
	/** The max days of custody request. */
	@Inject @Configurable
	Integer maxDaysOfCustodyRequest;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The bl participant. */
	private boolean blParticipant;
	
	/** The flag issuer block. */
	private boolean flagIssuerBlock;
	/** The list mnemonic document type. */
	private List<ParameterTable> lstMnemonicDocType;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		searchBlockOperationTO = new SearchBlockOperationTO();
		searchBlockOperationTO.setParamTableLevelAffectation(new ParameterTable());
		searchBlockOperationTO.setParamTableStateAffectation(new ParameterTable());
		searchBlockOperationTO.setParamTableTypeAffectation(new ParameterTable());
		searchBlockOperationTO.setParamTableSecurityClass(new ParameterTable());
		searchBlockOperationTO.setParamTableCurrency(new ParameterTable());
		searchBlockOperationTO.setParticipant(new Participant());
		searchBlockOperationTO.setHolder(new Holder());
		searchBlockOperationTO.setHolderAccount(new HolderAccount());
		searchBlockOperationTO.setSecurity(new Security());
		searchBlockOperationTO.setIssuer(new Issuer());
		searchBlockOperationTO.setInitialDate(getCurrentSystemDate());
		searchBlockOperationTO.setEndDate(getCurrentSystemDate());
		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			Issuer issuer= requestAffectationFacade.findIssuer(userInfo.getUserAccountSession().getIssuerCode());
			searchBlockOperationTO.setIssuer(issuer);
		}
		
		blNoResult = false;		
		flagIssuerBlock = false;
		fillCombos();
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			blParticipant = true;
			searchBlockOperationTO.getParticipant().setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		}
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			flagIssuerBlock = true;
		}
	}
	
	/**
	 * Search block operations.
	 */
	@LoggerAuditWeb
	public void searchBlockOperations(){
		JSFUtilities.hideGeneralDialogues();
		try{
			List<BlockOperationTO> lstResult = requestAffectationFacade.findBlockOperations(searchBlockOperationTO);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
					searchBlockOperationTO.setLstBlockOperation(lstResult);
					blNoResult = false;
			}else{
					searchBlockOperationTO.setLstBlockOperation(null);
					blNoResult = true;
			}
			
		}catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * View block operation detail.
	 *
	 * @param blockOperationTO the block operation to
	 */
	public void viewBlockOperationDetail(BlockOperationTO blockOperationTO){
		try{
			blockOperationDetailTO = requestAffectationFacade.findBlockOperationsDetail(blockOperationTO);

			loadMnemonicDocumentType();
		}catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	 
	/**
	 * Fill combos.
	 */
	private void fillCombos(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();						
			//Lista de tipos de afectacion				
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchBlockOperationTO.setLstTypeAffectation(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de niveles de afectacion						
			parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchBlockOperationTO.setLstLevelAffectation(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de los estados de los bloqueos
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			List<ParameterTable> lstTempPT = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
			List<ParameterTable> lstParamTableState = new ArrayList<ParameterTable>();
			for(ParameterTable paramTable:lstTempPT){
				if(AffectationStateType.BLOCKED.getCode().equals(paramTable.getParameterTablePk()) 
						|| AffectationStateType.UNBLOCKED.getCode().equals(paramTable.getParameterTablePk()))
					lstParamTableState.add(paramTable);
			}
			searchBlockOperationTO.setLstStateAffectation(lstParamTableState);
			//Security class list
			parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			List<ParameterTable> lstSecurityClassTemp= generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
			for (ParameterTable objParameterTable: lstSecurityClassTemp) {
				objParameterTable.setDescription(objParameterTable.getText1() + GeneralConstants.HYPHEN + objParameterTable.getDescription());
			}
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() || userInfo.getUserAccountSession().isIssuerInstitucion()){
				List<ParameterTable> lstSecurityClass = new ArrayList<ParameterTable>();
				for(ParameterTable objParameterTable:lstSecurityClassTemp){
					if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
							objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
						lstSecurityClass.add(objParameterTable);
					}
				}
				searchBlockOperationTO.setLstSecurityClass(lstSecurityClass);
			}else{
				searchBlockOperationTO.setLstSecurityClass(lstSecurityClassTemp);
			}
			//currency list
			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchBlockOperationTO.setLstCurrency(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de participantes
			Participant objParticipant = new Participant();
			objParticipant.setState(ParticipantStateType.REGISTERED.getCode());
			searchBlockOperationTO.setLstParticipants(accountFacade.getLisParticipantServiceBean(objParticipant));
			//Lista de emisores
			searchBlockOperationTO.setLstIssuers(requestAffectationFacade.getListIssuers());
		}
		catch (ServiceException ex) {
			ex.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load mnemonic document type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadMnemonicDocumentType() throws ServiceException{
		ParameterTableTO parameterTable=new ParameterTableTO();
		parameterTable.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTable.setMasterTableFk( MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode() );
		lstMnemonicDocType=generalParameterFacade.getListParameterTableServiceBean( parameterTable );
		
		billParametersTableMap(lstMnemonicDocType,true);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}

	/**
	 * Show security financial data.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialData(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);		
		JSFUtilities.updateComponent("frmSearchBlockOperations:detailSecurityHelp:securityDetailsecurityHelp");
	}
	
	
	/**
	 * Gets the lst mnemonic doc type.
	 *
	 * @return the lst mnemonic doc type
	 */
	public List<ParameterTable> getLstMnemonicDocType() {
		return lstMnemonicDocType;
	}

	/**
	 * Sets the lst mnemonic doc type.
	 *
	 * @param lstMnemonicDocType the new lst mnemonic doc type
	 */
	public void setLstMnemonicDocType(List<ParameterTable> lstMnemonicDocType) {
		this.lstMnemonicDocType = lstMnemonicDocType;
	}

	/**
	 * Gets the search block operation to.
	 *
	 * @return the search block operation to
	 */
	public SearchBlockOperationTO getSearchBlockOperationTO() {
		return searchBlockOperationTO;
	}

	/**
	 * Sets the search block operation to.
	 *
	 * @param searchBlockOperationTO the new search block operation to
	 */
	public void setSearchBlockOperationTO(
			SearchBlockOperationTO searchBlockOperationTO) {
		this.searchBlockOperationTO = searchBlockOperationTO;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}

	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	/**
	 * Gets the block operation detail to.
	 *
	 * @return the block operation detail to
	 */
	public BlockOperationDetailTO getBlockOperationDetailTO() {
		return blockOperationDetailTO;
	}

	/**
	 * Sets the block operation detail to.
	 *
	 * @param blockOperationDetailTO the new block operation detail to
	 */
	public void setBlockOperationDetailTO(
			BlockOperationDetailTO blockOperationDetailTO) {
		this.blockOperationDetailTO = blockOperationDetailTO;
	}

	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}

	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}

	/**
	 * Checks if is flag issuer block.
	 *
	 * @return true, if is flag issuer block
	 */
	public boolean isFlagIssuerBlock() {
		return flagIssuerBlock;
	}

	/**
	 * Sets the flag issuer block.
	 *
	 * @param flagIssuerBlock the new flag issuer block
	 */
	public void setFlagIssuerBlock(boolean flagIssuerBlock) {
		this.flagIssuerBlock = flagIssuerBlock;
	}
	
	
}
