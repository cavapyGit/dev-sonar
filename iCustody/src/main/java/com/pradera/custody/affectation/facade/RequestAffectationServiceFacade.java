package com.pradera.custody.affectation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.affectation.to.BlockOperationDetailTO;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.affectation.to.HolderAccountBalanceTO;
import com.pradera.custody.affectation.to.RegisterBanOrOthersTO;
import com.pradera.custody.affectation.to.RegisterPawnOrReserveTO;
import com.pradera.custody.affectation.to.RequestAffectationDetailTO;
import com.pradera.custody.affectation.to.RequestAffectationTO;
import com.pradera.custody.affectation.to.ResultSearchTO;
import com.pradera.custody.affectation.to.SearchBlockOperationTO;
import com.pradera.custody.affectation.to.SearchRequestAffectationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.RequestAffectationStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.generalparameter.DailyExchangeRates;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.notification.ProcessNotification;

// TODO: Auto-generated Javadoc
/**
 * The Class RequestAffectationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class RequestAffectationServiceFacade{

	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The bean service. */
	@EJB
	private RequestAffectationServiceBean requestAffectationServiceBean;
	
	/** The account facade. */
	@Inject
	private AccountsFacade accountFacade;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The holder service bean. */
	@EJB
	HolderQueryServiceBean holderServiceBean;
	
	/** The affectation ban aditional. */
	@EJB
	AffectationBanAditional affectationBanAditional;
	
	/** The billing conexion. */
	@Inject 
	Instance<BillingConexionService> billingConexion;


	/**
	 * Gets the list request affectation.
	 *
	 * @param searchRequestAffectationTO the search request affectation to
	 * @return the list request affectation
	 * @throws ServiceException the service exception
	 */
	public List<RequestAffectationTO> getListRequestAffectation(SearchRequestAffectationTO searchRequestAffectationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<RequestAffectationTO> lstRequestAffectation = new ArrayList<RequestAffectationTO>();
		List<Object[]> lstResult = requestAffectationServiceBean.getListRequestAffectation(searchRequestAffectationTO);			
		for(Object[] objResult:lstResult){
			RequestAffectationTO requestAffectationTO = new RequestAffectationTO();
			requestAffectationTO.setNumberRequest((Long)objResult[0]);
			requestAffectationTO.setNumberRecord(Validations.validateIsNotNullAndNotEmpty(objResult[1])?(objResult[1].toString()):null);
			requestAffectationTO.setHolder(Integer.valueOf(objResult[2].toString()));
			requestAffectationTO.setHolderDescription(""+objResult[17].toString());
			requestAffectationTO.setRegisterDate((Date) objResult[4]);
			if(Validations.validateIsNotNullAndNotEmpty(objResult[18])){
				requestAffectationTO.setRegisterUser(objResult[18].toString());
			}
			requestAffectationTO.setTypeAffectation(objResult[5]!=null?objResult[5].toString():"");
			requestAffectationTO.setEntityBlock(Integer.valueOf(objResult[6].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(objResult[7]))
				requestAffectationTO.setEntityBlockDescription(objResult[12].toString() + GeneralConstants.BLANK_SPACE
							+ (Validations.validateIsNotNullAndNotEmpty(objResult[13])?objResult[13].toString():GeneralConstants.EMPTY_STRING) 
							+ GeneralConstants.BLANK_SPACE + objResult[7].toString());
			else
				requestAffectationTO.setEntityBlockDescription(objResult[11]!=null?objResult[11].toString():"");
			requestAffectationTO.setState(objResult[8].toString());
			requestAffectationTO.setIdState(new Integer(objResult[9].toString()));
			requestAffectationTO.setDateRecord((Date)objResult[10]);
			requestAffectationTO.setIdTypeAffectation(new Integer(objResult[14].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(objResult[15]))
				requestAffectationTO.setIndRequestDeclaration(new Integer(objResult[15].toString()));
			else
				requestAffectationTO.setIndRequestDeclaration(BooleanType.NO.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(objResult[16]))
				requestAffectationTO.setNotificationDate((Date) objResult[16]);
			lstRequestAffectation.add(requestAffectationTO);
		}		
		return lstRequestAffectation;
	}

	/**
	 * Find holder account by rnt.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipant the id participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<com.pradera.custody.affectation.to.HolderAccountTO> findHolderAccountByRNT(Long idHolderPk, Long idParticipant) throws ServiceException{
		//Validamos el estado del titular
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("idHolderPkParam", idHolderPk);
		Holder holder = requestAffectationServiceBean.findObjectByNamedQuery(Holder.HOLDER_STATE, parameter);
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED, new Object[]{holder.getIdHolderPk().toString()});
		}
		
		//Obtenemos las cuentas de dicho titular
		HolderAccountTO holderAccountTO = new HolderAccountTO();
		List<Integer> lstPartState = new ArrayList<Integer>();
		lstPartState.add(ParticipantStateType.REGISTERED.getCode());
		lstPartState.add(ParticipantStateType.BLOCKED.getCode());
		holderAccountTO.setParticipantStates(lstPartState);
		holderAccountTO.setIdHolderPk(idHolderPk);
		holderAccountTO.setNeedHolder(true);
		holderAccountTO.setParticipantTO(idParticipant);
		holderAccountTO.setHolderaccountGroupType(HolderAccountGroupType.INVERSTOR.getCode());
		
		//obtenemos cuentas desde el titular
		List<HolderAccount> lstResult = accountFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
		if(lstResult==null || lstResult.isEmpty()){
			throw new ServiceException(ErrorServiceType.HOLDER_WITHOUT_HOLDERACCOUNTS);
		}
		
		List<com.pradera.custody.affectation.to.HolderAccountTO> lstHolderAccount = new ArrayList<com.pradera.custody.affectation.to.HolderAccountTO>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		Map<Integer,String> mapAccountStateDescription = new HashMap<Integer,String>();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : parameterServiceBean.getListParameterTableServiceBean(parameterTableTO)){
			mapAccountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
		}
		for(HolderAccount holderAccount:lstResult){
			com.pradera.custody.affectation.to.HolderAccountTO holAccTO = new com.pradera.custody.affectation.to.HolderAccountTO();			
			holAccTO.setNumberAccount(holderAccount.getAccountNumber());			
			holAccTO.setHolderAccount(holderAccount.getAccountRntAndDescription());			
			holAccTO.setParticipant(holderAccount.getParticipant().getDescription());
			holAccTO.setParticipantMnemonic(holderAccount.getParticipant().getMnemonic());
			holAccTO.setState(mapAccountStateDescription.get(holderAccount.getStateAccount()));
			holAccTO.setAlternativeCode(holderAccount.getAlternateCode());
			holAccTO.setIdHolderAccountPk(holderAccount.getIdHolderAccountPk());
			holAccTO.setIdParticipantPk(holderAccount.getParticipant().getIdParticipantPk());
			if(HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()) || 
				HolderAccountStatusType.BLOCK.getCode().equals(holderAccount.getStateAccount())	)
				holAccTO.setBlCantCheckForPawn(false);
			else
				holAccTO.setBlCantCheckForPawn(true);
			
			if(HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()) || 
			   HolderAccountStatusType.BLOCK.getCode().equals(holderAccount.getStateAccount()))
				holAccTO.setBlCantCheckForBan(false);
			else
				holAccTO.setBlCantCheckForBan(true);
			lstHolderAccount.add(holAccTO);
		}
		return lstHolderAccount;
	}
	
	/**
	 * Find automatic balances for ban.
	 *
	 * @param accountTOList the account to list
	 * @param affectAmount the affect amount
	 * @param currency the currency
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> findAutomaticBalancesForBan(List<com.pradera.custody.affectation.to.HolderAccountTO> accountTOList, 
				BigDecimal affectAmount, Integer currency) throws ServiceException
	{
		/*DECLARE VARIABLES PARA LA LISTA, Y EL MONTO UTILIZADO POR CADA ITERACION*/
		List<HolderAccountBalanceTO> lstHolderAccountBalanceTO = new ArrayList<HolderAccountBalanceTO>();
		BigDecimal blockAmount = affectAmount;
		DailyExchangeRates dayExchangeRateUSD = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.USD.getCode());
		//DailyExchangeRates dayExchangeRateUFV = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.UFV.getCode());
		//DailyExchangeRates dayExchangeRateEUR = parameterServiceBean.getDayExchangeRate(CommonsUtilities.currentDate(),CurrencyType.EU.getCode());

		Map<Integer,List<ParameterTable>> mapBenefits=new HashMap<>();
		//Se carga todos los procesos que afectan		
		List<ParameterTable> listBenefitsFixed=new ArrayList<>();
		List<ParameterTable> listBenefitsVariable=new ArrayList<>();
		List<Integer> lstBenefitsFixed = new ArrayList<Integer>();
		lstBenefitsFixed.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
		lstBenefitsFixed.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());
		lstBenefitsFixed.add(ImportanceEventType.EARLY_PAYMENT.getCode());
		
		List<Integer> lstBenefitsVariable = new ArrayList<Integer>();
		lstBenefitsVariable.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
		lstBenefitsVariable.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
		lstBenefitsVariable.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
		lstBenefitsVariable.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
		
		listBenefitsFixed=findParametersFromId(lstBenefitsFixed);
		mapBenefits.put(InstrumentType.FIXED_INCOME.getCode(), listBenefitsFixed);
		listBenefitsVariable=findParametersFromId(lstBenefitsVariable);
		mapBenefits.put(InstrumentType.VARIABLE_INCOME.getCode(), listBenefitsVariable);
		
		Object[] param= new Object[1];
		if(Validations.validateIsNull(dayExchangeRateUSD)){
			param[0]= CurrencyType.USD.getCodeIso();
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_PARAM, param);
		}
		
		/*if(Validations.validateIsNull(dayExchangeRateUFV)){
			param[0]= CurrencyType.UFV.getCodeIso();
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_PARAM, param);
		}
		if(Validations.validateIsNull(dayExchangeRateEUR)){
			param[0]= CurrencyType.EU.getCodeIso();
			throw new ServiceException(ErrorServiceType.SECURITY_BLOCK_EXCHANGE_RATE_PARAM, param);
		}
		*/
		
		Map<Integer,BigDecimal> mpExchangeRate= new HashMap<Integer,BigDecimal>();
		mpExchangeRate.put(CurrencyType.USD.getCode(), dayExchangeRateUSD.getBuyPrice());
		//mpExchangeRate.put(CurrencyType.UFV.getCode(), dayExchangeRateUFV.getBuyPrice());
		//mpExchangeRate.put(CurrencyType.EU.getCode(), dayExchangeRateEUR.getBuyPrice());

		for(com.pradera.custody.affectation.to.HolderAccountTO holderAccountTO: accountTOList){
			/*GETTING HOLDER_ACCOUNT_BALANCE FROM BD*/
			lstHolderAccountBalanceTO.addAll(findBalancesByHolderAccount(holderAccountTO.getIdHolderAccountPk(),
										holderAccountTO.getIdParticipantPk(),
										NegotiationMechanismType.BOLSA.getCode(),null,mapBenefits));
		}
		
		/*SORT ALL BALANCES BY CRITERIA ESTABLISHED*/
		lstHolderAccountBalanceTO = affectationBanAditional.orderBalancesByBusinessCriteria(lstHolderAccountBalanceTO, currency);
		
		boolean isReblock=false;
		//we block the available balance for each holder account
		blockAmount= blockStockQuantityHolderAccount(lstHolderAccountBalanceTO, currency, blockAmount, mpExchangeRate, isReblock);
		
		if (blockAmount.compareTo(BigDecimal.ZERO) > 0) {
			isReblock= true;
			//we reblock the balance for each holder account
			blockAmount= blockStockQuantityHolderAccount(lstHolderAccountBalanceTO, currency, blockAmount, mpExchangeRate, isReblock);
		}
		
		if (blockAmount.compareTo(BigDecimal.ZERO) > 0) {
			throw new ServiceException(ErrorServiceType.BLOCKOPERATION_CASH_WITHOUT_COVER);
		} else {
			// Se desactiva todo lo referido a marketfact
			// lstHolderAccountBalanceTO = affectationBanAditional.generateBlockMarketFact(lstHolderAccountBalanceTO);
		}
		
		return lstHolderAccountBalanceTO;
	}
	
	
	/**
	 * Block stock quantity holder account.
	 *
	 * @param lstHolderAccountBalanceTO the lst holder account balance to
	 * @param currency the currency
	 * @param blockAmount the block amount
	 * @param mpExchangeRate the mp exchange rate
	 * @param isReblock the is reblock
	 * @return the big decimal
	 */
	private BigDecimal blockStockQuantityHolderAccount(List<HolderAccountBalanceTO> lstHolderAccountBalanceTO, Integer currency, BigDecimal blockAmount, 
								Map<Integer, BigDecimal> mpExchangeRate, boolean isReblock)
	{
		BigDecimal currentBlockAmount= blockAmount;
		for(HolderAccountBalanceTO objHolderAccountBalanceTO: lstHolderAccountBalanceTO){
			//we get the quotation according the exchange rate
			BigDecimal quotationExchange= affectationBanAditional.applyExchangeRateQuotation(objHolderAccountBalanceTO.getLastQuotation(), 
														currency, objHolderAccountBalanceTO.getCurrency(), mpExchangeRate);
			BigDecimal stockQuantity= BigDecimal.ZERO; //required quantity to be blocked
			BigDecimal blockQuantity= BigDecimal.ZERO; //quantity to block
			//required quantity to be blocked
			if (CommonsUtilities.isMultipleOf(currentBlockAmount, quotationExchange)) {
				stockQuantity = currentBlockAmount.divide(quotationExchange);
			} else {
				stockQuantity = currentBlockAmount.divide(quotationExchange,0,BigDecimal.ROUND_UP);
			}
			//quantity to block
			if (!isReblock) {
				objHolderAccountBalanceTO.setQuantityBlockBalance(BigDecimal.ZERO);
				if (objHolderAccountBalanceTO.getAvailableBalance().compareTo(stockQuantity) >= 0) {
					blockQuantity= stockQuantity;
				} else {
					blockQuantity = objHolderAccountBalanceTO.getAvailableBalance();
				}
			} else {
				if (objHolderAccountBalanceTO.getTotalBalance().compareTo(stockQuantity) >= 0) {
					blockQuantity= stockQuantity;
				} else {
					blockQuantity = objHolderAccountBalanceTO.getTotalBalance();
				}
			}
			objHolderAccountBalanceTO.setQuantityBlockBalance(objHolderAccountBalanceTO.getQuantityBlockBalance().add(blockQuantity));
			//we decrease the required block amount
			currentBlockAmount = currentBlockAmount.subtract(blockQuantity.multiply(quotationExchange));
			
			if (BigDecimal.ZERO.compareTo(currentBlockAmount) >= 0) {
				break; //if we complete the entire block amount then we break out the blocking
			}
		}
		return currentBlockAmount;
	}

	/**
	 * Find affectations holder account balance by hol acc pk and part pk.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @param idParticipantPk the id participant pk
	 * @param idNegotationMechanism the id negotation mechanism
	 * @param idIssuer the id issuer
	 * @param parameter the parameter
	 * @return List
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> findBalancesByHolderAccount(Long idHolderAccountPk, Long idParticipantPk, 
																Long idNegotationMechanism, String idIssuer, Map parameter) throws ServiceException{
		List<HolderAccountBalanceTO> lstHolderAccountBalance = new ArrayList<HolderAccountBalanceTO>();
		List<Object[]> lstResult = requestAffectationServiceBean.findBalancesByAccountAndParticipantWithGuardaExclusive(idHolderAccountPk,idParticipantPk, idIssuer);
		for(Object[] objResult:lstResult){
			HolderAccountBalanceTO holAccBalance = new HolderAccountBalanceTO();
			holAccBalance.setIdSecurityCodePk(objResult[0].toString());
			holAccBalance.setIsinName(objResult!=null?objResult[1].toString():"");
			holAccBalance.setAvailableBalance(new BigDecimal(objResult[2].toString()));
			holAccBalance.setTotalBalance(new BigDecimal(objResult[4].toString()));
			BigDecimal totalBlockBalance = new BigDecimal(objResult[3].toString()).add(new BigDecimal(objResult[5].toString()));
			totalBlockBalance = totalBlockBalance.add(new BigDecimal(objResult[6].toString()).add(new BigDecimal(objResult[9].toString())));
			totalBlockBalance = totalBlockBalance.add(new BigDecimal(objResult[10].toString()));
			holAccBalance.setBlockBalance(totalBlockBalance);
			holAccBalance.setBanBalance(new BigDecimal(objResult[3].toString()));
			holAccBalance.setOppositionBalance(new BigDecimal(objResult[10].toString()));
			holAccBalance.setMaxSize(holAccBalance.getTotalBalance().toString().length());
			holAccBalance.setInstrumentType((Integer)objResult[7]);
			holAccBalance.setReportingBalance(new BigDecimal(objResult[8].toString()));
			holAccBalance.setCurrencyDescription(objResult[11].toString());
			holAccBalance.setNominalValue(new BigDecimal(objResult[12].toString()));
			holAccBalance.setCurrency(new Integer(objResult[13].toString()));
			BigDecimal transitBalance = new BigDecimal(objResult[14].toString());
			holAccBalance.setCredinRatesScale((Integer) objResult[15]);
			holAccBalance.setIssuanceSecType((Integer) objResult[16]);
			holAccBalance.setSecurityClass((Integer)objResult[17]);
			if (Validations.validateIsNotNull(objResult[18])) {
				holAccBalance.setExpirationDate((Date)objResult[18]);
			}
			
			holAccBalance.setIdParticipantPk(idParticipantPk);
			holAccBalance.setIdHolderAccountPk(idHolderAccountPk);
			
			if(transitBalance.compareTo(BigDecimal.ZERO) == 1){
				//Se valida si el saldo en transito es producto de embargos u oposiciones
				BigDecimal totalForBanOppo = requestAffectationServiceBean.getAmountForBanOrOpposition(idHolderAccountPk, idParticipantPk, holAccBalance.getIdSecurityCodePk());
				holAccBalance.setOppositionBalance(holAccBalance.getOppositionBalance().add(totalForBanOppo));
			}
			
			//RENTA FIJA
			if(InstrumentType.FIXED_INCOME.getCode().equals(Integer.valueOf(objResult[7].toString()))){
				holAccBalance.setLstBenefits((List<ParameterTable>)parameter.get(InstrumentType.FIXED_INCOME.getCode()));
				//Valorizacion
				holAccBalance.setValoritationVal(holAccBalance.getNominalValue().multiply(holAccBalance.getTotalBalance()));
				holAccBalance.setLastQuotation(holAccBalance.getNominalValue());
			}else{//RENTA VARIABLE
				holAccBalance.setLstBenefits((List<ParameterTable>)parameter.get(InstrumentType.VARIABLE_INCOME.getCode()));
				holAccBalance.setLastQuotation(holAccBalance.getNominalValue());
				if(Validations.validateIsNotNullAndNotEmpty(idNegotationMechanism)){
					List<Object[]> lstQuotation = requestAffectationServiceBean.getLastQuotation(holAccBalance.getIdSecurityCodePk(),idNegotationMechanism);
					if(Validations.validateListIsNotNullAndNotEmpty(lstQuotation)){
						for (Object[] objQuotation:lstQuotation) {
							//Valorizacion con ultima cotizacion
							holAccBalance.setLastQuotation(new BigDecimal(objQuotation[0].toString()));
							holAccBalance.setValoritationVal(new BigDecimal(objQuotation[0].toString()).multiply(holAccBalance.getTotalBalance()));
						}
					}else{
						//Valorizacion con VN
						holAccBalance.setValoritationVal(holAccBalance.getLastQuotation().multiply(holAccBalance.getTotalBalance()));
					}
				}else{
					//Valorizacion con VN
					holAccBalance.setValoritationVal(holAccBalance.getLastQuotation().multiply(holAccBalance.getTotalBalance()));
				}
			}
			
			lstHolderAccountBalance.add(holAccBalance);
		}
		return lstHolderAccountBalance;
	}
	
	/**
	 * Find affectation with market fact.
	 *
	 * @param accountBalance the account balance
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> findAffectationWithMarketFact(List<HolderAccountBalanceTO> accountBalance) throws ServiceException{
		List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> haBalancesTO = new ArrayList<com.pradera.integration.component.business.to.HolderAccountBalanceTO>();
		for(HolderAccountBalanceTO hab: accountBalance){
			com.pradera.integration.component.business.to.HolderAccountBalanceTO habTO = new com.pradera.integration.component.business.to.HolderAccountBalanceTO();
			habTO.setIdParticipant(hab.getIdParticipantPk());
			habTO.setIdHolderAccount(hab.getIdHolderAccountPk());
			habTO.setIdSecurityCode(hab.getIdSecurityCodePk());
			haBalancesTO.add(habTO);
		}
		
		Map<String, Integer> balanceKeyMap = requestAffectationServiceBean.balanceWithMarketFactQuantity(haBalancesTO);
		
		for(HolderAccountBalanceTO hab: accountBalance){
			StringBuilder balanceKey = new StringBuilder();
			balanceKey.append(hab.getIdParticipantPk());
			balanceKey.append(hab.getIdHolderAccountPk());
			balanceKey.append(hab.getIdSecurityCodePk());
			
			Integer quantityMarketFact = balanceKeyMap.get(balanceKey.toString());
			if(quantityMarketFact!=null && quantityMarketFact.equals(BigDecimal.ONE.intValue())){
				hab.setMarketLock(Boolean.FALSE);
			}else{
				hab.setMarketLock(Boolean.TRUE);
			}
		}
		return accountBalance;
	}

	/**
	 * Register new pawn or reserve request.
	 *
	 * @param registerPawnOrReserveTO the register pawn or reserve to
	 * @return String
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_REGISTER)
	public Long registerNewPawnOrReserveRequest(RegisterPawnOrReserveTO registerPawnOrReserveTO) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    if(Validations.validateIsNotNullAndNotEmpty(loggerUser)){		           
	    	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    }
	    
	    registerPawnOrReserveTO.setRegistryUser(loggerUser.getUserName());
		
	    return requestAffectationServiceBean.savePawnOrReserveRequest(registerPawnOrReserveTO);
	}
	
	/**
	 * Register new ban or others or opposition request.
	 *
	 * @param registerBanOrOthersTO the register ban or others to
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_REGISTER)
	public Long registerNewBanOrOthersOrOppositionRequest(RegisterBanOrOthersTO registerBanOrOthersTO) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
	    registerBanOrOthersTO.setRegistryUser(loggerUser.getUserName());
	    
	    return requestAffectationServiceBean.saveBanOrOthersOrOppositionRequest(registerBanOrOthersTO);
	}
	
	/**
	 * Find affectation request detail.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return the request affectation detail to
	 * @throws ServiceException the service exception
	 */
	public RequestAffectationDetailTO findAffectationRequestDetail(RequestAffectationTO requestAffectationTO) throws ServiceException{
		BlockRequest objBlockRequest = null;
		RequestAffectationDetailTO requestAffectationDetailTO = new RequestAffectationDetailTO();
		List<ParameterTable> lstPTStateAffecRequest = null;
		List<ParameterTable> lstPTTypeAffec = null;
		List<ParameterTable> lstPTLevelAffec = null;
		List<ParameterTable> lstPTInstrumentType = null;
		List<ParameterTable> lstPTStateAffec = null;
		List<Integer> lstBenefits = null;
		List<ParameterTable> lstPTSecurityClass = null;
		List<ParameterTable> lstPTAffecForm = null;
		List<ParameterTable> lstRejectMotive= null;
		List<BlockOperationTO> lstBlockOperationTO = new ArrayList<BlockOperationTO>();		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		objBlockRequest = requestAffectationServiceBean.findBlockRequestDetail(new Long(requestAffectationTO.getNumberRequest()),
																				requestAffectationTO.getIdIssuer());
		requestAffectationDetailTO.setObjBlockRequest(objBlockRequest);
		/*** Se levantan los PT a usar ***/
		//Lista de estados de solicitudes de afectacion	
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION_REQUEST.getCode());
		lstPTStateAffecRequest = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de tipos de afectacion				
		parameterTableTO.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		lstPTTypeAffec = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de niveles de afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		lstPTLevelAffec = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de tipos de instrumento						
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		lstPTInstrumentType = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de estados de afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
		lstPTStateAffec = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de clases del valor						
		parameterTableTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		lstPTSecurityClass = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de formas de afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
		lstPTAffecForm = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		
		parameterTableTO.setMasterTableFk(MasterTableType.AFECTATION_REJECT_MOTIVE.getCode());
		lstRejectMotive = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		
		parameterTableTO = new ParameterTableTO();
	    parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<ParameterTable> lstTemp = new ArrayList<ParameterTable>();
		for(ParameterTable paramTab:parameterServiceBean.getListParameterTableServiceBean(parameterTableTO)){
			if(CurrencyType.USD.getCode().equals(paramTab.getParameterTablePk())
					|| CurrencyType.PYG.getCode().equals(paramTab.getParameterTablePk()))
				lstTemp.add(paramTab);
		}
		requestAffectationTO.setLstCurrency(lstTemp);
		/*********************************/
		/*** Se ingresa todo la info al TO ***/			
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getBlockNumberDate()))
			requestAffectationDetailTO.setDateRecord(objBlockRequest.getBlockNumberDate());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getBlockNumber()))
			requestAffectationDetailTO.setNumberRecord(objBlockRequest.getBlockNumber());
		if(Validations.validateIsNotNull(requestAffectationDetailTO.getObjBlockRequest().getIndAutoSettled())){
			if(requestAffectationDetailTO.getObjBlockRequest().getIndAutoSettled().equals(GeneralConstants.ONE_VALUE_INTEGER))
				requestAffectationTO.setCheckAutoSettled(true);
			else
				requestAffectationTO.setCheckAutoSettled(false);
		}
		if(Validations.validateIsNotNull(requestAffectationDetailTO.getObjBlockRequest().getIndAuthorityRestricted())){
			if(requestAffectationDetailTO.getObjBlockRequest().getIndAuthorityRestricted().equals(GeneralConstants.ONE_VALUE_INTEGER))
				requestAffectationTO.setCheckAuthorityRestricted(true);
			else
				requestAffectationTO.setCheckAuthorityRestricted(false);
		}
		requestAffectationTO.setCircularNumber(requestAffectationDetailTO.getObjBlockRequest().getCircularNumber());
		requestAffectationTO.setFiscalProcedence(requestAffectationDetailTO.getObjBlockRequest().getFiscalOrigin());
		requestAffectationTO.setFiscalCurrency(requestAffectationDetailTO.getObjBlockRequest().getFiscalCurrency());
		requestAffectationTO.setFiscalAmount(requestAffectationDetailTO.getObjBlockRequest().getFiscalAmount());
		requestAffectationTO.setFiscalChargeNumber(requestAffectationDetailTO.getObjBlockRequest().getFiscalCharge());
		requestAffectationTO.setFiscalProcess(requestAffectationDetailTO.getObjBlockRequest().getFiscalProcess());
		requestAffectationTO.setFiscalAuthority(requestAffectationDetailTO.getObjBlockRequest().getFiscalAuthority());
		
		requestAffectationDetailTO.setRegistryDate(objBlockRequest.getRegistryDate());
		requestAffectationDetailTO.setRegistryUser(objBlockRequest.getRegistryUser());
		requestAffectationDetailTO.setApproveDate(objBlockRequest.getApproveDate());
		requestAffectationDetailTO.setApproveUser(objBlockRequest.getApproveUser());
		requestAffectationDetailTO.setAnnulDate(objBlockRequest.getAnnulDate());
		requestAffectationDetailTO.setAnnulUser(objBlockRequest.getAnnulUser());
		requestAffectationDetailTO.setComments(objBlockRequest.getComments());
		
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getConfirmationDate()))
			requestAffectationDetailTO.setConfirmationDate(objBlockRequest.getConfirmationDate());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getConfirmationUser()))
			requestAffectationDetailTO.setConfirmationUser(objBlockRequest.getConfirmationUser());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getRejectDate()))
			requestAffectationDetailTO.setRejectDate(objBlockRequest.getRejectDate());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getRejectUser()))
			requestAffectationDetailTO.setRejectUser(objBlockRequest.getRejectUser());
		if(BooleanType.YES.getCode().equals(objBlockRequest.getIndRequestDeclaration()))
			requestAffectationDetailTO.setRequestDeclaration(BooleanType.YES.getValue());
		else
			requestAffectationDetailTO.setRequestDeclaration(BooleanType.NO.getValue());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getNotificationDate()))
			requestAffectationDetailTO.setDeclarationDate(objBlockRequest.getNotificationDate());
		requestAffectationDetailTO.setBlockAmount(objBlockRequest.getOriginBlockAmount());
		//requestAffectationDetailTO.setCurrentBlockAmount(objBlockRequest.getCurrentBlockAmount()!=null?objBlockRequest.getCurrentBlockAmount().toString():null);			
		requestAffectationDetailTO.setEntityBlockDescription(objBlockRequest.getBlockEntity().getDescriptionPerson());
		requestAffectationDetailTO.setIdBlockEntityPk(objBlockRequest.getBlockEntity().getIdBlockEntityPk());
		requestAffectationDetailTO.setEntityBlockDocNumber(objBlockRequest.getBlockEntity().getDocumentNumber());
		requestAffectationDetailTO.setEntityBlockDocType(objBlockRequest.getBlockEntity().getDocumentTypeDescription());
		requestAffectationDetailTO.setHolderDescription(objBlockRequest.getHolder().getFullName());
		requestAffectationDetailTO.setIdHolderPk(objBlockRequest.getHolder().getIdHolderPk().toString());
		requestAffectationDetailTO.setHolderDocNumber(objBlockRequest.getHolder().getDocumentNumber());
		requestAffectationDetailTO.setHolderDocType(objBlockRequest.getHolder().getDocumentTypeDescription());
		requestAffectationDetailTO.setNumberRequest(objBlockRequest.getIdBlockRequestPk().toString());	
		if(Validations.validateListIsNotNullAndNotEmpty(objBlockRequest.getBlockRequestFile())){
			requestAffectationDetailTO.setDocumentFile(objBlockRequest.getBlockRequestFile().get(0).getDocumentFile());
			requestAffectationDetailTO.setFileName(objBlockRequest.getBlockRequestFile().get(0).getFileName());
		}
		for(ParameterTable objParamTable:lstPTStateAffecRequest){
			if(objParamTable.getParameterTablePk().equals(objBlockRequest.getBlockRequestState())){
				requestAffectationDetailTO.setState(objParamTable.getParameterName());
				break;
			}
		}
		for(ParameterTable objParamTable:lstPTAffecForm){
			if(objParamTable.getParameterTablePk().equals(objBlockRequest.getBlockForm())){
				requestAffectationDetailTO.setAffectationForm(objParamTable.getParameterName());
				break;
			}
		}
		for(ParameterTable objParamTable:lstPTTypeAffec){
			if(objParamTable.getParameterTablePk().equals(objBlockRequest.getBlockType())){
				requestAffectationDetailTO.setTypeAffectation(objParamTable.getParameterName());
				break;
			}
		}	
		if(objBlockRequest.getBlockType().equals(AffectationType.BAN.getCode())){
	    	requestAffectationTO.setBlBanBlock(true);
	    	requestAffectationTO.setBlPawnBlock(false);
	    }
	    else if(objBlockRequest.getBlockType().equals(AffectationType.PAWN.getCode())){
	    	requestAffectationTO.setBlBanBlock(false);
	    	requestAffectationTO.setBlPawnBlock(true);
	    }
	    else{
	    	requestAffectationTO.setBlBanBlock(false);
	    	requestAffectationTO.setBlPawnBlock(false);
	    }
		/*SE RECUPERA LOS DOCUMENTOS DE BLOQUEO*/
		for(BlockOperation objBlockOpe : objBlockRequest.getBlockOperation()){
			BlockOperationTO objBlockOpeTO = new BlockOperationTO();
			objBlockOpeTO.setIdBlockOperationPk(objBlockOpe.getIdBlockOperationPk());
			objBlockOpeTO.setAccountNumber(objBlockOpe.getHolderAccount().getAccountNumber());
			
			objBlockOpeTO.setAlternativeCode(objBlockOpe.getHolderAccount().getAlternateCode().toString());
			objBlockOpeTO.setOperationNumber(objBlockOpe.getCustodyOperation().getOperationNumber());
			objBlockOpeTO.setIdHolderAccountPk(objBlockOpe.getHolderAccount().getIdHolderAccountPk());
			objBlockOpeTO.setAmount(objBlockOpe.getOriginalBlockBalance());
			objBlockOpeTO.setActualAmount(objBlockOpe.getActualBlockBalance());
			objBlockOpeTO.setSecurityCodePk(objBlockOpe.getSecurities().getIdSecurityCodePk());
			objBlockOpeTO.setIsinDescription(objBlockOpe.getSecurities().getDescription());
			objBlockOpeTO.setIdParticipantPk(objBlockOpe.getParticipant().getIdParticipantPk());
			objBlockOpeTO.setParticipantDescription(objBlockOpe.getParticipant().getDescription());
			objBlockOpeTO.setNominalVal(objBlockOpe.getSecurities().getCurrentNominalValue());
			objBlockOpeTO.setIdLevelAffectation(objBlockOpe.getBlockLevel());
			objBlockOpeTO.setHolderDescription(objBlockOpe.getHolderAccount().getAccountRntAndDescription());
			objBlockOpeTO.setInstrumentTypeCode(objBlockOpe.getSecurities().getInstrumentType());
			objBlockOpe.getBlockMarketFactOperations().size();
			List<BlockMarketFactOperation> lstBlockMarketFactOperation = new ArrayList<BlockMarketFactOperation>();
			if(Validations.validateListIsNotNullAndNotEmpty(objBlockOpe.getBlockMarketFactOperations())){
				for(BlockMarketFactOperation objBlockMarketFactOperation: objBlockOpe.getBlockMarketFactOperations() ){
					if(GeneralConstants.ONE_VALUE_INTEGER.equals(objBlockMarketFactOperation.getIndActive())){
						lstBlockMarketFactOperation.add(objBlockMarketFactOperation);
					}
				}
			}
			objBlockOpeTO.setMarketFactOperationList(lstBlockMarketFactOperation);
			for(ParameterTable objParamTable:lstPTLevelAffec){
				if(objParamTable.getParameterTablePk().equals(objBlockOpe.getBlockLevel())){
					objBlockOpeTO.setLevelAffectation(objParamTable.getParameterName());
					break;
				}
			}				
			for(ParameterTable objParamTable:lstPTStateAffec){
				if(objParamTable.getParameterTablePk().equals(objBlockOpe.getBlockState())){
					objBlockOpeTO.setStateAffectation(objParamTable.getParameterName());
					break;
				}
			}
			for(ParameterTable objParamTable:lstPTInstrumentType){
				if(objParamTable.getParameterTablePk().equals(objBlockOpe.getSecurities().getInstrumentType())){
					objBlockOpeTO.setInstrumentType(objParamTable.getParameterName());
					break;
				}
			}
			for(ParameterTable objParamTable:lstPTSecurityClass){
				if(objParamTable.getParameterTablePk().equals(objBlockOpe.getSecurities().getSecurityClass())){
					objBlockOpeTO.setIsinClass(objParamTable.getParameterName());
					break;
				}
			}
			objBlockOpeTO.setIdIssuerPk(objBlockOpe.getSecurities().getIssuer().getIdIssuerPk());
			objBlockOpeTO.setIssuerDescription(objBlockOpe.getSecurities().getIssuer().getBusinessName());
			//RENTA FIJA
			if(InstrumentType.FIXED_INCOME.getCode().equals(objBlockOpe.getSecurities().getInstrumentType())) {
				lstBenefits = new ArrayList<Integer>();
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndInterest()))
					lstBenefits.add(ImportanceEventType.INTEREST_PAYMENT.getCode());
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndAmortization()))
					lstBenefits.add(ImportanceEventType.CAPITAL_AMORTIZATION.getCode());		
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndRescue()))
					lstBenefits.add(ImportanceEventType.EARLY_PAYMENT.getCode());
			}else{//RENTA VARIABLE
				lstBenefits = new ArrayList<Integer>();
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndCashDividend()))
					lstBenefits.add(ImportanceEventType.CASH_DIVIDENDS.getCode());
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndStockDividend()))
					lstBenefits.add(ImportanceEventType.ACTION_DIVIDENDS.getCode());
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndSuscription()))
					lstBenefits.add(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode());
				if(BooleanType.YES.getCode().equals(objBlockOpe.getIndReturnContributions()))
					lstBenefits.add(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode());
			}
			if(!lstBenefits.isEmpty()) {
				objBlockOpeTO.setLstBenefits(parameterServiceBean.findParametersFromId(lstBenefits));
			}			
			if (Validations.validateIsNotNull(objBlockOpe.getCustodyOperation().getRejectMotiveOther())) {
				requestAffectationDetailTO.setRejectMotiveDesc(objBlockOpe.getCustodyOperation().getRejectMotiveOther());
			} else if (Validations.validateIsNotNull(objBlockOpe.getCustodyOperation().getRejectMotive())) {
				for (ParameterTable parameterTable: lstRejectMotive) {
					if (parameterTable.getParameterTablePk().equals(objBlockOpe.getCustodyOperation().getRejectMotive())) {
						requestAffectationDetailTO.setRejectMotiveDesc(parameterTable.getDescription());
						break;
					}
				}
			} 
			lstBlockOperationTO.add(objBlockOpeTO);				
		}
		requestAffectationDetailTO.setLstBlockOperationTO(lstBlockOperationTO);
		/*************************************/		
		return requestAffectationDetailTO;
	}
	
	/**
	 * Approve pawn affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_PAWN_APPROVE)
	public boolean approvePawnAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
	    requestAffectationTO.setApproveUser(loggerUser.getUserName());
	    requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_PAWN_APPROVE.getCode());
	    return requestAffectationServiceBean.approveAffectation(requestAffectationTO);
	}
	
	/**
	 * Approve ban others affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_APPROVE)
	public boolean approveBanOthersAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
	    requestAffectationTO.setApproveUser(loggerUser.getUserName());
	    requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_APPROVE.getCode());
	    return requestAffectationServiceBean.approveAffectation(requestAffectationTO);
	}
	
	/**
	 * Annul pawn affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_PAWN_ANNUL)
	public boolean annulPawnAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
	    requestAffectationTO.setAnnulUser(loggerUser.getUserName());
	    requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_PAWN_ANNUL.getCode());
	    return requestAffectationServiceBean.annulAffectation(requestAffectationTO);
	}
	
	/**
	 * Annul ban others affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_ANNUL)
	public boolean annulBanOthersAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
	    requestAffectationTO.setAnnulUser(loggerUser.getUserName());
	    requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_ANNUL.getCode());
	    return requestAffectationServiceBean.annulAffectation(requestAffectationTO);
	}
	
	
	/**
	 * Confirm affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_CONFIRM)
	public boolean confirmAffectation(RequestAffectationTO requestAffectationTO) throws Exception{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    requestAffectationTO.setConfirmationUser(loggerUser.getUserName());
	    requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_CONFIRM.getCode());
	    return requestAffectationServiceBean.confirmAffectation(requestAffectationTO, loggerUser);
	}
	
	/**
	 * Confirm pawn affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_PAWN_CONFIRM)
	public boolean confirmPawnAffectation(RequestAffectationTO requestAffectationTO) throws Exception{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    requestAffectationTO.setConfirmationUser(loggerUser.getUserName());
	    requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_PAWN_CONFIRM.getCode());
	    return requestAffectationServiceBean.confirmAffectation(requestAffectationTO, loggerUser);
	}
	
	/**
	 * Reject affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_REJECT)
	public boolean rejectAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		requestAffectationTO.setRejectUser(loggerUser.getUserName());
		requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_REJECT.getCode());
		return requestAffectationServiceBean.rejectAffectation(requestAffectationTO);
	}
	
	/**
	 * Reject pawn affectation.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_PAWN_REJECT)
	public boolean rejectPawnAffectation(RequestAffectationTO requestAffectationTO) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		requestAffectationTO.setRejectUser(loggerUser.getUserName());
		requestAffectationTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_BLOCK_PAWN_REJECT.getCode());
		return requestAffectationServiceBean.rejectAffectation(requestAffectationTO);
	}
	
	/**
	 * Gets the negotiation mechanism.
	 *
	 * @return the negotiation mechanism
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<NegotiationMechanism> getNegotiationMechanism() throws ServiceException{
		return (List<NegotiationMechanism>)requestAffectationServiceBean.findWithNamedQuery(NegotiationMechanism.SEARCH_NEGOTIATIONS);
	}	
	
	/**
	 * Find block operations.
	 *
	 * @param searchBlockOperationTO the search block operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_DOCUMENTS_QUERY)
	public List<BlockOperationTO> findBlockOperations(SearchBlockOperationTO searchBlockOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<BlockOperationTO> lstBlockOperations = new ArrayList<BlockOperationTO>();
		List<Object[]> lstResult = requestAffectationServiceBean.findBlockOperations(searchBlockOperationTO);
		for(Object[] objResult:lstResult){
			BlockOperationTO blockOpeTO = new BlockOperationTO();
			blockOpeTO.setIdBlockOperationPk(new Long(objResult[0].toString()));
			blockOpeTO.setRegistryDate((Date)objResult[1]);
			blockOpeTO.setIdHolderPk(new Long(objResult[2].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(objResult[14]))
				blockOpeTO.setHolderDescription(objResult[14].toString() + GeneralConstants.BLANK_SPACE
							+ (Validations.validateIsNotNullAndNotEmpty(objResult[15])?objResult[15].toString():GeneralConstants.EMPTY_STRING) 
							+ GeneralConstants.BLANK_SPACE + objResult[3].toString());
			else
				blockOpeTO.setHolderDescription(objResult[16].toString());
			blockOpeTO.setIdIsinCodePk(objResult[4].toString());
			blockOpeTO.setIdParticipantPk(new Long(objResult[5].toString()));
			blockOpeTO.setTypeAffectation(objResult[6].toString());
			blockOpeTO.setLevelAffectation(objResult[7].toString());
			blockOpeTO.setStateAffectation(objResult[8].toString());
			blockOpeTO.setActualAmount(new BigDecimal(objResult[9].toString()));
			blockOpeTO.setAccountNumber(Integer.parseInt(objResult[10].toString()));
			if(objResult[11]!=null){
				blockOpeTO.setIsinDescription(objResult[11].toString());
			}			
			blockOpeTO.setParticipantDescription(objResult[12].toString());
			blockOpeTO.setParticipantMnemonic(objResult[13].toString());
			blockOpeTO.setOperationNumber(new Long(objResult[17].toString()));
			lstBlockOperations.add(blockOpeTO);
		}
		return lstBlockOperations;
	}

	/**
	 * Search block entity bcr.
	 *
	 * @param rntBCR the rnt bcr
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	public BlockEntity searchBlockEntityBCR(Long rntBCR) throws ServiceException{
		Holder objHolder = requestAffectationServiceBean.find(rntBCR, Holder.class);
		if(Validations.validateListIsNotNullAndNotEmpty(objHolder.getBlockEntity())){
			BlockEntity blockEntity = objHolder.getBlockEntity().get(0);
			if(BlockEntityStateType.ACTIVATED.getCode().equals(blockEntity.getStateBlockEntity()))
				return blockEntity;
			else{
				blockEntity = new BlockEntity();
				blockEntity.setStateBlockEntity(BlockEntityStateType.OFF.getCode());
				return blockEntity;
			}
		}else
			return new BlockEntity();	
	}
	
	/**
	 * Exist number record.
	 *
	 * @param numberRecord the number record
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<String> existNumberRecord(String numberRecord) throws ServiceException{
		List<Object[]> lstObject = requestAffectationServiceBean.existNumberRecord(numberRecord);
		List<String> lstResult = new ArrayList<String>();
		for(Object[] objResult:lstObject){
			lstResult.add(objResult[0].toString());
			lstResult.add(Validations.validateIsNotNullAndNotEmpty(objResult[1])?objResult[1].toString():GeneralConstants.EMPTY_STRING);
		}
		return lstResult;
	}

	/**
	 * Find block operations detail.
	 *
	 * @param blockOpeTO the block ope to
	 * @return the block operation detail to
	 * @throws ServiceException the service exception
	 */
	public BlockOperationDetailTO findBlockOperationsDetail(BlockOperationTO blockOpeTO) throws ServiceException{
		BlockOperationDetailTO blockOperationDetailTO = new BlockOperationDetailTO();
		List<ParameterTable> lstBenefits = null;
		List<ParameterTable> lstPTLevelAffec = null;
		List<ParameterTable> lstPTStateAffec = null;
		List<ParameterTable> lstPTStateAffecRequest = null;
		List<ParameterTable> lstPTFormAffec = null;
		List<ParameterTable> lstPTAffecType = null;
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		//Se recupera la operacion
		BlockOperation blockOperation = requestAffectationServiceBean.findBlockOperationDetail(blockOpeTO.getIdBlockOperationPk());
		//Se recupera los movimientos relacionados a la operacion de bloqueo
		List<HolderAccountMovement> lstTotalMovements = requestAffectationServiceBean.findMovementsForBlockOperation(blockOpeTO.getIdBlockOperationPk());
		/*
		List<HolderAccountMovement> lstTotalMovementsLast = new ArrayList<HolderAccountMovement>();
		for(HolderAccountMovement objHolAccMov:lstTotalMovements){
			if(Validations.validateIsNullOrEmpty(objHolAccMov.getCustodyOperation()))
				objHolAccMov.setCustodyOperation(objHolAccMov.getRefCustodyOperation());
			lstTotalMovementsLast.add(objHolAccMov);
		}*/
		/*** Se levantan los PT a usar ***/
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		//Lista de niveles de afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		lstPTLevelAffec = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de estados de afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
		lstPTStateAffec = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de estados de solicitud afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION_REQUEST.getCode());
		lstPTStateAffecRequest = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de formas de afectacion						
		parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
		lstPTFormAffec = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		//Lista de tipo de afectacion
		parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_TYPE.getCode());
		lstPTAffecType = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		/*********************************/				
		for(ParameterTable objParamTable:lstPTStateAffecRequest){
			if(objParamTable.getParameterTablePk().equals(blockOperation.getBlockRequest().getBlockRequestState())){
				blockOperation.getBlockRequest().setBlockStateDescription(objParamTable.getParameterName());
				break;
			}
		}	
		for(ParameterTable objParamTable:lstPTFormAffec){
			if(objParamTable.getParameterTablePk().equals(blockOperation.getBlockRequest().getBlockForm())){
				blockOperation.getBlockRequest().setBlockFormDescription(objParamTable.getParameterName());
				break;
			}
		}

		for(ParameterTable objParamTable:lstPTAffecType){
			if(objParamTable.getParameterTablePk().equals(blockOperation.getBlockRequest().getBlockType())){
				blockOperation.getBlockRequest().setBlockTypeDescription(objParamTable.getParameterName());
				break;
			}
		}
		
		//Se setea la solicitud padre
		blockOperationDetailTO.setBlockRequest(blockOperation.getBlockRequest());
		//Se setea los datos necesarios de la operacion
		BlockOperationTO blockOperationTO = new BlockOperationTO();
		blockOperationTO.setIdBlockOperationPk(blockOperation.getIdBlockOperationPk());
		blockOperationTO.setOperationNumber(blockOperation.getCustodyOperation().getOperationNumber());
		
		for(ParameterTable objParamTable:lstPTStateAffec){
			if(objParamTable.getParameterTablePk().equals(blockOperation.getBlockState())){
				blockOperationTO.setStateAffectation(objParamTable.getParameterName());
				break;
			}
		}		
		
		blockOperationTO.setActualAmount(blockOperation.getActualBlockBalance());
		blockOperationTO.setAmount(blockOperation.getOriginalBlockBalance());
		
		for(ParameterTable objParamTable:lstPTLevelAffec){
			if(objParamTable.getParameterTablePk().equals(blockOperation.getBlockLevel())){
				blockOperationTO.setLevelAffectation(objParamTable.getParameterName());
				break;
			}
		}
		
		blockOperationTO.setAccountNumber(blockOperation.getHolderAccount().getAccountNumber());
		blockOperationTO.setIdParticipantPk(blockOperation.getParticipant().getIdParticipantPk());
		blockOperationTO.setParticipantDescription(blockOperation.getParticipant().getDescription());
		blockOperationTO.setSecurityCodePk(blockOperation.getSecurities().getIdSecurityCodePk());
		blockOperationTO.setIsinDescription(blockOperation.getSecurities().getDescription());
		//Beneficios
		//RENTA FIJA
		if(InstrumentType.FIXED_INCOME.getCode().equals(blockOperation.getSecurities().getInstrumentType())) {
			lstBenefits = new ArrayList<ParameterTable>();
			if(BooleanType.YES.getCode().equals(blockOperation.getIndInterest()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.INTEREST_PAYMENT.getCode(), ImportanceEventType.INTEREST_PAYMENT.getValue()));
			if(BooleanType.YES.getCode().equals(blockOperation.getIndAmortization()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.CAPITAL_AMORTIZATION.getCode(), ImportanceEventType.CAPITAL_AMORTIZATION.getValue()));		
			if(BooleanType.YES.getCode().equals(blockOperation.getIndRescue()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.EARLY_PAYMENT.getCode(), ImportanceEventType.EARLY_PAYMENT.getValue()));
		}else{//RENTA VARIABLE
			lstBenefits = new ArrayList<ParameterTable>();
			if(BooleanType.YES.getCode().equals(blockOperation.getIndCashDividend()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.CASH_DIVIDENDS.getCode(), ImportanceEventType.CASH_DIVIDENDS.getValue()));
			if(BooleanType.YES.getCode().equals(blockOperation.getIndStockDividend()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.ACTION_DIVIDENDS.getCode(), ImportanceEventType.ACTION_DIVIDENDS.getValue()));
			if(BooleanType.YES.getCode().equals(blockOperation.getIndSuscription()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode(), ImportanceEventType.PREFERRED_SUSCRIPTION.getValue()));
			if(BooleanType.YES.getCode().equals(blockOperation.getIndReturnContributions()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode(), ImportanceEventType.RETURN_OF_CONTRIBUTION.getValue()));
		}
		blockOperationTO.setLstBenefits(lstBenefits);
		blockOperationDetailTO.setBlockOperationTO(blockOperationTO);		
		blockOperationDetailTO.setLstBlockMovements(lstTotalMovements);
		return blockOperationDetailTO;
	}

	/**
	 * Update request declaration date.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean updateRequestDeclarationDate(RequestAffectationTO requestAffectationTO) throws ServiceException{
		BlockRequest objBlockRequest = requestAffectationServiceBean.findBlockRequestDetail(new Long(requestAffectationTO.getNumberRequest()),
																							requestAffectationTO.getIdIssuer());
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getNotificationDate())){
			requestAffectationTO.setNotificationDate(objBlockRequest.getNotificationDate());
			return false;
		}
		objBlockRequest.setNotificationDate(CommonsUtilities.currentDate());
		requestAffectationServiceBean.update(objBlockRequest);
		return true;
	}
	
	/**
	 * Creates the pt benefit.
	 *
	 * @param paramTabPk the param tab pk
	 * @param paramName the param name
	 * @return the parameter table
	 */
	private ParameterTable createPTBenefit(Integer paramTabPk, String paramName){
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(paramTabPk);
		paramTab.setParameterName(paramName);
		return paramTab;
	}

	/**
	 * Market fact business process.
	 *
	 * @param businessProcessType the business process type
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean marketFactBusinessProcess(Long businessProcessType) throws ServiceException{
		return requestAffectationServiceBean.marketFactBusinessProcess(businessProcessType);
	}
	
	/**
	 * Security id with market fact.
	 *
	 * @param securityCodes the security codes
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<String,Object> securityIdWithMarketFact(List<String> securityCodes) throws ServiceException{
		
		return null;
	}
	
	/**
	 * Find holder by rnt.
	 *
	 * @param rntCode the rnt code
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByRnt(Long rntCode) throws ServiceException{
		return holderServiceBean.findHolderByRnt(rntCode);
	}
	
	/**
	 * Automatic affectation cancelations.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticAffectationCancelations(Date finalDate) throws ServiceException{						
		SearchRequestAffectationTO searchRequestAffectationTO = new SearchRequestAffectationTO();
		List <Integer> lstParamState = new ArrayList<>();
		lstParamState.add(RequestAffectationStateType.REGISTERED.getCode());
		lstParamState.add(RequestAffectationStateType.APPROVED.getCode());
		searchRequestAffectationTO.setLstParamState(lstParamState);
		searchRequestAffectationTO.setEndDate(finalDate);
		List<Object[]> lstResult = requestAffectationServiceBean.getListRequestAffectationByRevert(searchRequestAffectationTO);
		for (Object[] objResult : lstResult) {			
			RequestAffectationTO requestAffectationTO = new RequestAffectationTO();
			requestAffectationTO.setNumberRequest((Long) objResult[0]);
			requestAffectationTO.setIdTypeAffectation(new Integer(objResult[1].toString()));
			requestAffectationTO.setIdState(new Integer(objResult[2].toString()));
			if (requestAffectationTO.getIdTypeAffectation().equals(AffectationType.PAWN.getCode())) {
				if(requestAffectationTO.getIdState().equals(RequestAffectationStateType.REGISTERED.getCode())){
					annulPawnAffectation(requestAffectationTO);
				}else if(requestAffectationTO.getIdState().equals(RequestAffectationStateType.APPROVED.getCode())){
					rejectPawnAffectation(requestAffectationTO);
				}
			} else {
				if(requestAffectationTO.getIdState().equals(RequestAffectationStateType.REGISTERED.getCode())){
					annulBanOthersAffectation(requestAffectationTO);
				}else if(requestAffectationTO.getIdState().equals(RequestAffectationStateType.APPROVED.getCode())){
					rejectAffectation(requestAffectationTO);
				}
			}
		}	
	}

		
	/**
	 * Gets the block request.
	 *
	 * @param requestAffectationTO the request affectation to
	 * @return the block request
	 */
	public BlockRequest getBlockRequest(RequestAffectationTO requestAffectationTO) {
		return requestAffectationServiceBean.getBlockRequest(requestAffectationTO);
	}
	
	/**
	 * Find issuer.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuer
	 */
	public Issuer findIssuer(String idIssuer) {
		return requestAffectationServiceBean.find(Issuer.class, idIssuer);
	}
	
	/**
	 * Gets the list issuers.
	 *
	 * @return the list issuers
	 */
	public List<Issuer> getListIssuers() {
		return requestAffectationServiceBean.getListIssuers();
	}
	
	/**
	 * Verify block operation at other requests.
	 *
	 * @param idBlockOperation the id block operation
	 * @param operationNumber the operation number
	 * @throws ServiceException the service exception
	 */
	public void verifyBlockOperationAtOtherRequests(Long idBlockOperation, String operationNumber) throws ServiceException{
		requestAffectationServiceBean.verifyBlockOperationAtOtherRequests(idBlockOperation, operationNumber);
	}
	
	/**
	 * Verify block operation registered by edv.
	 *
	 * @param idBlockOperation the id block operation
	 * @return true, if successful
	 */
	public boolean verifyBlockOperationRegisteredByEDV(Long idBlockOperation) {
		return requestAffectationServiceBean.verifyBlockOperationRegisteredByEDV(idBlockOperation);
	}
	
	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param parameters the parameters
	 * @return the list parameter table service bean
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> findParametersFromId(List<Integer> parameters) throws ServiceException {
		return parameterServiceBean.findParametersFromId(parameters);
	}
	
	/**
	 * Identify block operations to send.
	 *
	 * @param sendDate the send date
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=10)
	public void identifyBlockOperationsToSend(Date sendDate) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        requestAffectationServiceBean.identifyBlockOperationsToSend(sendDate, loggerUser);
	}
	public List<UserAccountSession> getListUsersOfIssuerLook(Long idHolderAccountPk,Long idBlockRequestPk){
		return requestAffectationServiceBean.getListUsersOfIssuerLook(idHolderAccountPk,idBlockRequestPk);
	}
	public ProcessNotification getProcessNotification(Long idProcessNotificationPk){
		return requestAffectationServiceBean.getProcessNotification(idProcessNotificationPk);

	}
	public List<UserAccountSession> getListUsersOfIssuerUnLock(Long idBlockRequestPk){
		return requestAffectationServiceBean.getListUsersOfIssuerUnLock(idBlockRequestPk);
	}
	public List<ResultSearchTO> obtainDataForMessagesList(Long idUnblockRequestPk){
		List<Long>idBlockRequestPks=requestAffectationServiceBean.getIdBlockOperationtPks(idUnblockRequestPk);
		List<ResultSearchTO> list=new ArrayList<>();
		ResultSearchTO resultSearchTO=null;
		Long idHolderAccountPk=null;
		for (Long idBlockOperationPk : idBlockRequestPks) {
			idHolderAccountPk=requestAffectationServiceBean.getIdHolderAccountPk(idBlockOperationPk);
			resultSearchTO=requestAffectationServiceBean.obtainDataForMessages(idHolderAccountPk,idBlockOperationPk);
			list.add(resultSearchTO);
		}
		return list;
	}
}