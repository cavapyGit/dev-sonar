package com.pradera.custody.affectation.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class HolderAccountTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The number account. */
	private Integer numberAccount;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The alternative code. */
	private String alternativeCode;
	
	/** The participant. */
	private String participant;
	
	/** The holder account. */
	private String holderAccount;
		
	/** The state. */
	private String state;
	
	/** The bl check. */
	private boolean blCheck;		
		
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The bl cant check for ban. */
	private boolean blCantCheckForBan;
	
	/** The bl cant check for pawn. */
	private boolean blCantCheckForPawn;
	
	/**
	 * Gets the alternative code.
	 *
	 * @return the alternative code
	 */
	public String getAlternativeCode() {
		return alternativeCode;
	}
	
	/**
	 * Sets the alternative code.
	 *
	 * @param alternativeCode the new alternative code
	 */
	public void setAlternativeCode(String alternativeCode) {
		this.alternativeCode = alternativeCode;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Checks if is bl check.
	 *
	 * @return true, if is bl check
	 */
	public boolean isBlCheck() {
		return blCheck;
	}
	
	/**
	 * Sets the bl check.
	 *
	 * @param blCheck the new bl check
	 */
	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}
	
	/**
	 * Gets the number account.
	 *
	 * @return the number account
	 */
	public Integer getNumberAccount() {
		return numberAccount;
	}
	
	/**
	 * Sets the number account.
	 *
	 * @param numberAccount the new number account
	 */
	public void setNumberAccount(Integer numberAccount) {
		this.numberAccount = numberAccount;
	}
	
	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public String getParticipant() {
		return participant;
	}
	
	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(String participant) {
		this.participant = participant;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public String getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(String holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}

	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}

	/**
	 * Checks if is bl cant check for ban.
	 *
	 * @return true, if is bl cant check for ban
	 */
	public boolean isBlCantCheckForBan() {
		return blCantCheckForBan;
	}

	/**
	 * Sets the bl cant check for ban.
	 *
	 * @param blCantCheckForBan the new bl cant check for ban
	 */
	public void setBlCantCheckForBan(boolean blCantCheckForBan) {
		this.blCantCheckForBan = blCantCheckForBan;
	}

	/**
	 * Checks if is bl cant check for pawn.
	 *
	 * @return true, if is bl cant check for pawn
	 */
	public boolean isBlCantCheckForPawn() {
		return blCantCheckForPawn;
	}

	/**
	 * Sets the bl cant check for pawn.
	 *
	 * @param blCantCheckForPawn the new bl cant check for pawn
	 */
	public void setBlCantCheckForPawn(boolean blCantCheckForPawn) {
		this.blCantCheckForPawn = blCantCheckForPawn;
	}


}
