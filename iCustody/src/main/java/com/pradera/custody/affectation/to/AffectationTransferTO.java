package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.participantunion.BlockOperationUnion;
import com.pradera.model.custody.rectification.RectificationDetail;
import com.pradera.model.custody.securitiesrenewal.RenewalBlockOperation;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.transfersecurities.BlockOperationTransfer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AffectationTransferTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 05/04/2014
 */
public class AffectationTransferTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id block operation pk. */
	private Long idBlockOperationPk;
	
	/** The detached block operation. */
	private BlockOperation detachedBlockOperation; //no
	
	/** The quantity. */
	private BigDecimal quantity;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id participant pk. */
	private Long idParticipantPk;

	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The block change ownership. */
	private BlockChangeOwnership blockChangeOwnership; // only for CT
	
	/** The rectification detail. */
	private RectificationDetail rectificationDetail; // only for Rectification
	
	/** The block operation union. */
	private BlockOperationUnion blockOperationUnion; // only for participation unification
	
	/** The block operation transfer. */
	private BlockOperationTransfer blockOperationTransfer; // only for blocked securities transfer
	
	/** The retirement block detail. */
	private RetirementBlockDetail retirementBlockDetail; // only for blocked retirement operations transfer
	
	/** The market fact affectations. */
	private List<MarketFactTO> marketFactAffectations; 
	
	/** The renewal block operation. */
	private RenewalBlockOperation renewalBlockOperation;
	
	/**
	 * Gets the market fact affectations.
	 *
	 * @return the market fact affectations
	 */
	public List<MarketFactTO> getMarketFactAffectations() {
		return marketFactAffectations;
	}

	/**
	 * Sets the market fact affectations.
	 *
	 * @param marketFactAffectations the new market fact affectations
	 */
	public void setMarketFactAffectations(
			List<MarketFactTO> marketFactAffectations) {
		this.marketFactAffectations = marketFactAffectations;
	}

	/**
	 * Gets the block change ownership.
	 *
	 * @return the block change ownership
	 */
	public BlockChangeOwnership getBlockChangeOwnership() {
		return blockChangeOwnership;
	}

	/**
	 * Sets the block change ownership.
	 *
	 * @param blockChangeOwnership the new block change ownership
	 */
	public void setBlockChangeOwnership(BlockChangeOwnership blockChangeOwnership) {
		this.blockChangeOwnership = blockChangeOwnership;
	}

	/**
	 * Gets the rectification detail.
	 *
	 * @return the rectification detail
	 */
	public RectificationDetail getRectificationDetail() {
		return rectificationDetail;
	}

	/**
	 * Sets the rectification detail.
	 *
	 * @param rectificationDetail the new rectification detail
	 */
	public void setRectificationDetail(RectificationDetail rectificationDetail) {
		this.rectificationDetail = rectificationDetail;
	}

	/**
	 * Gets the block operation union.
	 *
	 * @return the block operation union
	 */
	public BlockOperationUnion getBlockOperationUnion() {
		return blockOperationUnion;
	}

	/**
	 * Sets the block operation union.
	 *
	 * @param blockOperationUnion the new block operation union
	 */
	public void setBlockOperationUnion(BlockOperationUnion blockOperationUnion) {
		this.blockOperationUnion = blockOperationUnion;
	}

	/**
	 * Gets the block operation transfer.
	 *
	 * @return the block operation transfer
	 */
	public BlockOperationTransfer getBlockOperationTransfer() {
		return blockOperationTransfer;
	}

	/**
	 * Sets the block operation transfer.
	 *
	 * @param blockOperationTransfer the new block operation transfer
	 */
	public void setBlockOperationTransfer(
			BlockOperationTransfer blockOperationTransfer) {
		this.blockOperationTransfer = blockOperationTransfer;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the idHolderAccountPk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the idHolderAccountPk to set
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the idParticipantPk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the idParticipantPk to set
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the id block operation pk.
	 *
	 * @return the id block operation pk
	 */
	public Long getIdBlockOperationPk() {
		return idBlockOperationPk;
	}

	/**
	 * Sets the id block operation pk.
	 *
	 * @param idBlockOperationPk the new id block operation pk
	 */
	public void setIdBlockOperationPk(Long idBlockOperationPk) {
		this.idBlockOperationPk = idBlockOperationPk;
	}

	/**
	 * Gets the detached block operation.
	 *
	 * @return the detachedBlockOperation
	 */
	public BlockOperation getDetachedBlockOperation() {
		return detachedBlockOperation;
	}

	/**
	 * Sets the detached block operation.
	 *
	 * @param detachedBlockOperation the detachedBlockOperation to set
	 */
	public void setDetachedBlockOperation(BlockOperation detachedBlockOperation) {
		this.detachedBlockOperation = detachedBlockOperation;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the retirement block detail.
	 *
	 * @return the retirement block detail
	 */
	public RetirementBlockDetail getRetirementBlockDetail() {
		return retirementBlockDetail;
	}

	/**
	 * Sets the retirement block detail.
	 *
	 * @param retirementBlockDetail the new retirement block detail
	 */
	public void setRetirementBlockDetail(RetirementBlockDetail retirementBlockDetail) {
		this.retirementBlockDetail = retirementBlockDetail;
	}

	/**
	 * Gets the renewal block operation.
	 *
	 * @return the renewalBlockOperation
	 */
	public RenewalBlockOperation getRenewalBlockOperation() {
		return renewalBlockOperation;
	}

	/**
	 * Sets the renewal block operation.
	 *
	 * @param renewalBlockOperation the renewalBlockOperation to set
	 */
	public void setRenewalBlockOperation(RenewalBlockOperation renewalBlockOperation) {
		this.renewalBlockOperation = renewalBlockOperation;
	}
	
	

}
