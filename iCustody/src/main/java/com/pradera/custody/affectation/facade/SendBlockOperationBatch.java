package com.pradera.custody.affectation.facade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.jfree.util.Log;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SendBlockOperationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19/08/2015
 */
@BatchProcess(name="SendBlockOperationBatch")
@RequestScoped
public class SendBlockOperationBatch implements JobExecution,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The request affectation service facade. */
	@EJB
	private RequestAffectationServiceFacade requestAffectationServiceFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) 
	{
		log.info("::::::::::::: CARGAMOS LA FECHA DE HOY PARA ENVIAR COMO PARAMETRO :::::::::::::");
		Date sendDate = CommonsUtilities.currentDate();
		
		try {
			Log.info("::::::::::::: BUSCAMOS LAS OPERACIONES QUE VAMOS A ENVIAR A BLOQUEAR :::::::::::::");
			requestAffectationServiceFacade.identifyBlockOperationsToSend(sendDate);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
