package com.pradera.custody.affectation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.custody.accreditationcertificates.BlockRequest;

// TODO: Auto-generated Javadoc
//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2014.</li>
* </ul>
* 
* The Class RequestAffectationDetailTO.
*
* @author PraderaTechnologies.
* @version 1.0 , 05/04/2014
*/

public class RequestAffectationDetailTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The number request. */
	private String numberRequest;
	
	/** The number record. */
	private String numberRecord;
	
	/** The date record. */
	private Date dateRecord;
	
	/** The type affectation. */
	private String typeAffectation;
	
	/** The affectation form. */
	private String affectationForm;
	
	/** The id block entity pk. */
	private Long idBlockEntityPk;
	
	/** The entity block description. */
	private String entityBlockDescription;
	
	/** The entity block doc type. */
	private String entityBlockDocType;
	
	/** The entity block doc number. */
	private String entityBlockDocNumber;
	
	/** The id holder pk. */
	private String idHolderPk;
	
	/** The holder description. */
	private String holderDescription;
	
	/** The holder doc type. */
	private String holderDocType;
	
	/** The holder doc number. */
	private String holderDocNumber;
	
	/** The current block amount. */
	private String currentBlockAmount;
	
	/** The block amount. */
	private BigDecimal blockAmount;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The approve date. */
	private Date approveDate;
	
	/** The approve user. */
	private String approveUser;
	
	/** The annul date. */
	private Date annulDate;
	
	/** The annul user. */
	private String annulUser;
	
	/** The confirmation date. */
	private Date confirmationDate;
	
	/** The confirmation user. */
	private String confirmationUser;
	
	/** The reject date. */
	private Date rejectDate;
	
	/** The reject user. */
	private String rejectUser;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The state. */
	private String state;
	
	/** The lst block operation to. */
	private List<BlockOperationTO> lstBlockOperationTO;
	
	/** The document file. */
	private byte[] documentFile;
	
	/** The file name. */
	private String fileName;
	
	/** The request declaration. */
	private String requestDeclaration;
	
	/** The declaration date. */
	private Date declarationDate;
	
	/** The reject motive other. */
	private String rejectMotiveOther;
	
	/** The reject motive desc. */
	private String rejectMotiveDesc;
	
	/** The comments. */
	private String comments;
	
	/** The obj block request. */
	private BlockRequest objBlockRequest;
	
	/**
	 * Gets the number request.
	 *
	 * @return the number request
	 */
	public String getNumberRequest() {
		return numberRequest;
	}
	
	/**
	 * Sets the number request.
	 *
	 * @param numberRequest the new number request
	 */
	public void setNumberRequest(String numberRequest) {
		this.numberRequest = numberRequest;
	}
	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}
	
	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}
	
	/**
	 * Gets the date record.
	 *
	 * @return the date record
	 */
	public Date getDateRecord() {
		return dateRecord;
	}
	
	/**
	 * Sets the date record.
	 *
	 * @param dateRecord the new date record
	 */
	public void setDateRecord(Date dateRecord) {
		this.dateRecord = dateRecord;
	}
	
	/**
	 * Gets the type affectation.
	 *
	 * @return the type affectation
	 */
	public String getTypeAffectation() {
		return typeAffectation;
	}
	
	/**
	 * Sets the type affectation.
	 *
	 * @param typeAffectation the new type affectation
	 */
	public void setTypeAffectation(String typeAffectation) {
		this.typeAffectation = typeAffectation;
	}
	
	/**
	 * Gets the affectation form.
	 *
	 * @return the affectation form
	 */
	public String getAffectationForm() {
		return affectationForm;
	}
	
	/**
	 * Sets the affectation form.
	 *
	 * @param affectationForm the new affectation form
	 */
	public void setAffectationForm(String affectationForm) {
		this.affectationForm = affectationForm;
	}
	
	
	/**
	 * Gets the id block entity pk.
	 *
	 * @return the id block entity pk
	 */
	public Long getIdBlockEntityPk() {
		return idBlockEntityPk;
	}
	
	/**
	 * Sets the id block entity pk.
	 *
	 * @param idBlockEntityPk the new id block entity pk
	 */
	public void setIdBlockEntityPk(Long idBlockEntityPk) {
		this.idBlockEntityPk = idBlockEntityPk;
	}
	
	/**
	 * Gets the entity block description.
	 *
	 * @return the entity block description
	 */
	public String getEntityBlockDescription() {
		return entityBlockDescription;
	}
	
	/**
	 * Sets the entity block description.
	 *
	 * @param entityBlockDescription the new entity block description
	 */
	public void setEntityBlockDescription(String entityBlockDescription) {
		this.entityBlockDescription = entityBlockDescription;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public String getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(String idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	
	/**
	 * Gets the current block amount.
	 *
	 * @return the current block amount
	 */
	public String getCurrentBlockAmount() {
		return currentBlockAmount;
	}
	
	/**
	 * Sets the current block amount.
	 *
	 * @param currentBlockAmount the new current block amount
	 */
	public void setCurrentBlockAmount(String currentBlockAmount) {
		this.currentBlockAmount = currentBlockAmount;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Gets the confirmation date.
	 *
	 * @return the confirmation date
	 */
	public Date getConfirmationDate() {
		return confirmationDate;
	}
	
	/**
	 * Sets the confirmation date.
	 *
	 * @param confirmationDate the new confirmation date
	 */
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}
	
	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}
	
	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the lst block operation to.
	 *
	 * @return the lst block operation to
	 */
	public List<BlockOperationTO> getLstBlockOperationTO() {
		return lstBlockOperationTO;
	}
	
	/**
	 * Sets the lst block operation to.
	 *
	 * @param lstBlockOperationTO the new lst block operation to
	 */
	public void setLstBlockOperationTO(List<BlockOperationTO> lstBlockOperationTO) {
		this.lstBlockOperationTO = lstBlockOperationTO;
	}
	
	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return rejectDate;
	}
	
	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}
	
	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return rejectUser;
	}
	
	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}
	
	/**
	 * Gets the block amount.
	 *
	 * @return the block amount
	 */
	public BigDecimal getBlockAmount() {
		return blockAmount;
	}
	
	/**
	 * Sets the block amount.
	 *
	 * @param blockAmount the new block amount
	 */
	public void setBlockAmount(BigDecimal blockAmount) {
		this.blockAmount = blockAmount;
	}
	
	/**
	 * Gets the entity block doc type.
	 *
	 * @return the entity block doc type
	 */
	public String getEntityBlockDocType() {
		return entityBlockDocType;
	}
	
	/**
	 * Sets the entity block doc type.
	 *
	 * @param entityBlockDocType the new entity block doc type
	 */
	public void setEntityBlockDocType(String entityBlockDocType) {
		this.entityBlockDocType = entityBlockDocType;
	}
	
	/**
	 * Gets the entity block doc number.
	 *
	 * @return the entity block doc number
	 */
	public String getEntityBlockDocNumber() {
		return entityBlockDocNumber;
	}
	
	/**
	 * Sets the entity block doc number.
	 *
	 * @param entityBlockDocNumber the new entity block doc number
	 */
	public void setEntityBlockDocNumber(String entityBlockDocNumber) {
		this.entityBlockDocNumber = entityBlockDocNumber;
	}
	
	/**
	 * Gets the holder doc type.
	 *
	 * @return the holder doc type
	 */
	public String getHolderDocType() {
		return holderDocType;
	}
	
	/**
	 * Sets the holder doc type.
	 *
	 * @param holderDocType the new holder doc type
	 */
	public void setHolderDocType(String holderDocType) {
		this.holderDocType = holderDocType;
	}
	
	/**
	 * Gets the holder doc number.
	 *
	 * @return the holder doc number
	 */
	public String getHolderDocNumber() {
		return holderDocNumber;
	}
	
	/**
	 * Sets the holder doc number.
	 *
	 * @param holderDocNumber the new holder doc number
	 */
	public void setHolderDocNumber(String holderDocNumber) {
		this.holderDocNumber = holderDocNumber;
	}
	
	/**
	 * Gets the document file.
	 *
	 * @return the document file
	 */
	public byte[] getDocumentFile() {
		return documentFile;
	}
	
	/**
	 * Sets the document file.
	 *
	 * @param documentFile the new document file
	 */
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
	
	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * Gets the request declaration.
	 *
	 * @return the request declaration
	 */
	public String getRequestDeclaration() {
		return requestDeclaration;
	}
	
	/**
	 * Sets the request declaration.
	 *
	 * @param requestDeclaration the new request declaration
	 */
	public void setRequestDeclaration(String requestDeclaration) {
		this.requestDeclaration = requestDeclaration;
	}
	
	/**
	 * Gets the declaration date.
	 *
	 * @return the declaration date
	 */
	public Date getDeclarationDate() {
		return declarationDate;
	}
	
	/**
	 * Sets the declaration date.
	 *
	 * @param declarationDate the new declaration date
	 */
	public void setDeclarationDate(Date declarationDate) {
		this.declarationDate = declarationDate;
	}
	
	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}
	
	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}
	
	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}
	
	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}
	
	/**
	 * Gets the obj block request.
	 *
	 * @return the obj block request
	 */
	public BlockRequest getObjBlockRequest() {
		return objBlockRequest;
	}
	
	/**
	 * Sets the obj block request.
	 *
	 * @param objBlockRequest the new obj block request
	 */
	public void setObjBlockRequest(BlockRequest objBlockRequest) {
		this.objBlockRequest = objBlockRequest;
	}
	
	/**
	 * Gets the approve date.
	 *
	 * @return the approve date
	 */
	public Date getApproveDate() {
		return approveDate;
	}
	
	/**
	 * Sets the approve date.
	 *
	 * @param approveDate the new approve date
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	
	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}
	
	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}
	
	/**
	 * Gets the annul date.
	 *
	 * @return the annul date
	 */
	public Date getAnnulDate() {
		return annulDate;
	}
	
	/**
	 * Sets the annul date.
	 *
	 * @param annulDate the new annul date
	 */
	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}
	
	/**
	 * Gets the annul user.
	 *
	 * @return the annul user
	 */
	public String getAnnulUser() {
		return annulUser;
	}
	
	/**
	 * Sets the annul user.
	 *
	 * @param annulUser the new annul user
	 */
	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}
	
	/**
	 * Gets the reject motive desc.
	 *
	 * @return the reject motive desc
	 */
	public String getRejectMotiveDesc() {
		return rejectMotiveDesc;
	}
	
	/**
	 * Sets the reject motive desc.
	 *
	 * @param rejectMotiveDesc the new reject motive desc
	 */
	public void setRejectMotiveDesc(String rejectMotiveDesc) {
		this.rejectMotiveDesc = rejectMotiveDesc;
	}
	
	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * Sets the comments.
	 *
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}