package com.pradera.custody.affectation.to;

public class ResultSearchTO {
	private Long idParticipantPk;
	private String mNemonicParticipant;
	private String AccountNumber;
	private String idSecurityCodePk;
	public String getmNemonicParticipant() {
		return mNemonicParticipant;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public void setmNemonicParticipant(String mNemonicParticipant) {
		this.mNemonicParticipant = mNemonicParticipant;
	}
	public String getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
}
