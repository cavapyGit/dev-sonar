package com.pradera.custody.digitalsignature.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.AESKeyType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.AESEncryptUtils;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.custody.digitalsignature.facade.DigitalSignatureServiceFacade;
import com.pradera.custody.digitalsignature.to.DigitalSignatureTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.custody.type.DigitalSignatureType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DigitalSignatureBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/07/2015
 */
@DepositaryWebBean
@LoggerCreateBean

public class DigitalSignatureBean  extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The document validate. */
    @Inject
    DocumentValidator documentValidator;
	
	/** The digital signature to. */
	private DigitalSignatureTO digitalSignatureTO;
	
	/** The digital signature. */
	private DigitalSignature digitalSignature;
	
	/** The list transfer type. */
	private List<ParameterTable> lstTypeSignature = new ArrayList<ParameterTable>();
	
	/** The list document type. */
	private List<ParameterTable> listDocumentType = new ArrayList<ParameterTable>();
	
	/** The list . */
	private List<BooleanType> listActive = new ArrayList<BooleanType>();
	
	/** The subsidiary users result. */
	private GenericDataModel<DigitalSignature> gdmDigitalSignature;
	
	/** The file signature. */
    private transient StreamedContent fileSignature;
    
    /** The file signature name. */
	private String fileSignatureName;
	
	/** The file signature temporal. */
    private byte[] fileSignatureTemp;
    
    /** The aux signature. */
    private boolean auxFuplSignature;
    
    /** The form state. */
    private boolean formState;
	
	/** The is active. */
	private Integer isActive;
	
	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;
	
	/** The Digital Signature service facade. */
	@EJB
	protected DigitalSignatureServiceFacade digitalSignatureServiceFacade;
	
	/** The signature password. */
	private String signaturePassword;

	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			digitalSignatureTO = new DigitalSignatureTO();
			loadParameterTable();
			showPrivileges();			
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
		signaturePassword=null;
	}
	
	/**
	 * Load Parameters Table.
	 */
	public void loadParameterTable() {
		try {
			
			// Load type signature
			
			lstTypeSignature = new ArrayList<ParameterTable>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.DIGITAL_SIGNATURE_TYPE.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
				lstTypeSignature.add(param);
			}
			
			// Load document type
			
			PersonTO personTo = new PersonTO();
			personTo.setPersonType(PersonType.NATURAL.getCode());
			personTo.setFlagInabilityIndicator(false);
			personTo.setFlagMinorIndicator(false);
			personTo.setFlagHolderIndicator(true);
			personTo.setFlagNationalResident(true);
			listDocumentType = documentValidator.getDocumentTypeResult(personTo);
			
			listActive = BooleanType.list;
			
			cleanImageSignature();
			
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}	
	
	/**
	 * Show privileges.
	 *
	 * @throws ServiceException the service exception
	 */
	private void showPrivileges() throws ServiceException {
		PrivilegeComponent privilegeComp = new PrivilegeComponent();
		privilegeComp.setBtnModifyView(true);
		privilegeComp.setBtnRegisterView(true);		
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	/**
	 * Search Subsidiary listener.
	 * 
	 * @param event the event
	 */
	public void searchDigitalSignatureListener(ActionEvent event){
		List<DigitalSignature> lstDigitalSignature = null;
		gdmDigitalSignature = null;
		try {
			lstDigitalSignature = digitalSignatureServiceFacade.getListDigitalSignature(digitalSignatureTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstDigitalSignature)){
				for(DigitalSignature objDigitalSignature : lstDigitalSignature){
					if(Validations.validateIsNotNullAndNotEmpty(objDigitalSignature.getTypeSignature())) {
						objDigitalSignature.setDescTypeSignature(generalParametersFacade.getParamDetailServiceFacade(objDigitalSignature.getTypeSignature()).getDescription());
					}
					if(Validations.validateIsNotNullAndNotEmpty(objDigitalSignature.getActiveSignature())){
						if(GeneralConstants.ONE_VALUE_INTEGER.equals(objDigitalSignature.getActiveSignature())){
							objDigitalSignature.setDescActiveSignature(BooleanType.lookup.get(Boolean.TRUE).getValue());
						} else if(GeneralConstants.ZERO_VALUE_INTEGER.equals(objDigitalSignature.getActiveSignature())){
							objDigitalSignature.setDescActiveSignature(BooleanType.lookup.get(Boolean.FALSE).getValue());
						}
						
					}
				}
			}
			gdmDigitalSignature = new GenericDataModel<DigitalSignature>(lstDigitalSignature);
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Subsidiary Detail.
	 *
	 * This method is called to see the detail information for the Subsidiary
	 * @param event the event
	 */
	@LoggerAuditWeb
	public void digitalSignatureDetail(ActionEvent event){
		cleanImageSignature();
		try {
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			digitalSignature = (DigitalSignature) event.getComponent().getAttributes().get("digSigSelected");
			JSFUtilities.putSessionMap("sessStreamedSignature", digitalSignature.getSignature());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * DigitalSignature modification action.
	 * @return the string
	 */
	@LoggerAuditWeb
	public String digitalSignatureModificationAction(){
		try {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);			
			if(digitalSignature == null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showComponent("cnfMsgCustomValidation");
				return null;
			} 
			setViewOperationType(ViewOperationsType.MODIFY.getCode());	
			if(digitalSignature.getSignature() != null){
				JSFUtilities.putSessionMap("sessStreamedSignature", digitalSignature.getSignature());
				fileSignatureName = "signature";
			}			
			return "newDigitalSignatureView";			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}		
	}
	
	/**
	 * Clean search Digital Signature listener.
	 *
	 * @param event the event
	 */
	public void cleanSearchDigitalSignatureListener(ActionEvent event){
		digitalSignatureTO = new DigitalSignatureTO();
		gdmDigitalSignature = null;
	}
	
	
	/**
	 * Clean image signature.
	 */
	public void cleanImageSignature(){
		JSFUtilities.removeSessionMap("sessStreamedSignature");
	}
	
	/**
	 * Subsidiary register action.
	 * @return the string
	 */
	
	public String registerDigitalSignatureListener(){
		digitalSignature = new DigitalSignature();
		digitalSignatureTO = new DigitalSignatureTO();	
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		cleanImageSignature();
		return "newDigitalSignatureView";
	}
	
	/**
	 * Before user registration listener.
	 * This method is  called when the button save is pressed
	 * If validation is successful it show the confirmation dialog
	 *
	 * @param event the event
	 */
	public void beforeDigitalSignatureRegistrationListener(ActionEvent event){
		try {
			Object[] argObj = new Object[1];
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);	
			signaturePassword=digitalSignature.getPasswordSignature();
			if(digitalSignature.getSignature() == null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_SIGNATURE_NULL));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(isViewOperationModify()&&digitalSignature.getActiveSignature().equals(BooleanType.YES.getCode()))
			if(digitalSignature.getPasswordSignature()==null){
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_SIGNATURE_INCOMPLETE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}else{
				signaturePassword=digitalSignature.getPasswordSignature();
			}
			if(isViewOperationModify()&&digitalSignature.getActiveSignature().equals(BooleanType.YES.getCode()))
			try {
				KeyStore ks = KeyStore.getInstance("pkcs12");
				ByteArrayInputStream arrayInputStream=new ByteArrayInputStream(digitalSignature.getSignature());
				ks.load(arrayInputStream, digitalSignature.getPasswordSignature().toCharArray());
			} catch (Exception e) {
				e.printStackTrace();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_SIGNATURE_INVALID_PASSWORD));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			DigitalSignatureTO digitalSignatureTO = null;
			boolean bandExitsActive = false;
			Object[] argObjError = new Object[1];
			if(isViewOperationRegister()){				
				digitalSignatureTO = new DigitalSignatureTO();
				if(digitalSignature != null && Validations.validateIsNotNullAndNotEmpty(digitalSignature.getTypeSignature())
						&& Validations.validateIsNotNullAndNotEmpty(digitalSignature.getActiveSignature()) 
						&& BooleanType.YES.getCode().equals(digitalSignature.getActiveSignature())){
					if(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode().equals(digitalSignature.getTypeSignature())){
						argObjError[0] = DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getValue();
						digitalSignatureTO.setTypeSignature(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode());
						bandExitsActive = exitsActiveSignatureByType(digitalSignatureTO);
					} else if(DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode().equals(digitalSignature.getTypeSignature())){
						argObjError[0] = DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getValue();
						digitalSignatureTO.setTypeSignature(DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode());
						bandExitsActive = exitsActiveSignatureByType(digitalSignatureTO);
					}
				}				
				if(bandExitsActive){					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_SIGNATURE_EXITS_ACTIVE_SIGNATURE, argObjError));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}

				argObj[0] = digitalSignature.getName();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_CONFIRM_REGISTER, argObj));				
			}
			if(isViewOperationModify()){				
				digitalSignatureTO = new DigitalSignatureTO();
				if(digitalSignature != null && Validations.validateIsNotNullAndNotEmpty(digitalSignature.getTypeSignature())
						&& Validations.validateIsNotNullAndNotEmpty(digitalSignature.getActiveSignature()) 
						&& BooleanType.YES.getCode().equals(digitalSignature.getActiveSignature())){
					digitalSignatureTO.setIdDigitalSignature(digitalSignature.getIdDigitalSignaturePk());
					if(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode().equals(digitalSignature.getTypeSignature())){
						argObjError[0] = DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getValue();						
						digitalSignatureTO.setTypeSignature(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode());
						bandExitsActive = exitsActiveSignatureByType(digitalSignatureTO);
					} else if(DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode().equals(digitalSignature.getTypeSignature())){
						argObjError[0] = DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getValue();
						digitalSignatureTO.setTypeSignature(DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode());
						bandExitsActive = exitsActiveSignatureByType(digitalSignatureTO);
					}
				}				
				if(bandExitsActive){					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_SIGNATURE_EXITS_ACTIVE_SIGNATURE, argObjError));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				argObj[0] = digitalSignature.getName();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
						PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_CONFIRM_MODIFY, argObj));				
			}	
			JSFUtilities.showComponent("frmMgmDigitalSignature:cnfSignatureAdm");	
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Subsidiary registration listener.
	 * This method is called after confirming, if save or update the user
	 */
	@LoggerAuditWeb
	public void saveSignatureListener(){
		try {
			JSFUtilities.putRequestMap("executeAction",Boolean.TRUE);
			if(isViewOperationRegister()){
				registrySignature();
			}else if(isViewOperationModify()) {
				modifySignature();
			}
			JSFUtilities.showComponent("cnfEndTransaction");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Registry Subsidiary.
	 *
	 * This method is called if the operation type is "register"
	 */
	@LoggerAuditWeb
	public void registrySignature(){
		try {			
			Object[] argObj=new Object[1];
			String passwordEncrypted=AESEncryptUtils.encrypt(AESKeyType.PRADERAKEY.getValue(), signaturePassword);
			digitalSignature.setPasswordSignature(passwordEncrypted);
			digitalSignature.setSignature(fileSignatureTemp);
			DigitalSignature objDigitalSignature = digitalSignatureServiceFacade.registerDigitalSignatureServiceFacade(digitalSignature);
			argObj[0] = objDigitalSignature.getName();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_CONFIRM_REGISTER_SUCCESS, argObj));
			cleanSignature();
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Exits active signature by type.
	 *
	 * @param digitalSignatureTO the digital signature to
	 * @return true, if successful
	 */
	public boolean exitsActiveSignatureByType(DigitalSignatureTO digitalSignatureTO) {
		boolean exitsActive = false;
		try {
			exitsActive = digitalSignatureServiceFacade.getExitsMostActiveSignature(digitalSignatureTO);
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return exitsActive;
	}
	
	/**
	 * Registry Subsidiary.
	 *
	 * This method is called if the operation type is "register"
	 */
	@LoggerAuditWeb
	public void modifySignature(){
		try {
			Object[] argObj=new Object[1];
			if(signaturePassword!=null){
				String passwordEncrypted=AESEncryptUtils.encrypt(AESKeyType.PRADERAKEY.getValue(), signaturePassword.trim());
				digitalSignature.setPasswordSignature(passwordEncrypted);
			}
			DigitalSignature objDigitalSignature = digitalSignatureServiceFacade.updateDigitalSignatureServiceFacade(digitalSignature);
			argObj[0] = objDigitalSignature.getName();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.DIGITALSIGNATURE_CONFIRM_MODIFY_SUCCESS, argObj));
			cleanSignature();
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean Signature.
	 *  This method clean the datatable and the entered filters
	 */
	public void cleanSignature(){
		digitalSignature = new DigitalSignature();
		digitalSignatureTO = new DigitalSignatureTO();		
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);				
		String fDisplayName = fUploadValidate(event.getFile(), null, null,null,"pfx");
		if(fDisplayName != null){			 
			try {				
				InputStream inputImage = new ByteArrayInputStream(event.getFile().getContents());
				fileSignature = new DefaultStreamedContent(inputImage, event.getFile().getContentType(), event.getFile().getFileName());  
				fileSignatureName = event.getFile().getFileName();
				digitalSignature.setSignature(event.getFile().getContents());			
				fileSignatureTemp = event.getFile().getContents();				
				JSFUtilities.putSessionMap("sessStreamedSignature", fileSignatureTemp);
				auxFuplSignature = Boolean.TRUE;
				formState = Boolean.TRUE;
			} catch (Exception e) {
				e.printStackTrace();
			}
		 }
	}
	
	/**
	 * Delete system image temp.
	 */
	public void deleteFileSignatureTemp(){
		fileSignatureTemp = null;
		fileSignatureName = "";
		digitalSignature.setSignature(null);
		fileSignature = null;
		JSFUtilities.putSessionMap("sessStreamedSignature", null);
	}
	
	/**
	 * The Method is used close the Confirm dialog on Pressing No.
	 */
	public void hideConfirm(){
		JSFUtilities.hideComponent("frmMgmDigitalSignature:cnfSignatureAdm");
	}

	/**
	 * Gets the digital signature to.
	 *
	 * @return the digitalSignatureTO
	 */
	public DigitalSignatureTO getDigitalSignatureTO() {
		return digitalSignatureTO;
	}

	/**
	 * Sets the digital signature to.
	 *
	 * @param digitalSignatureTO the digitalSignatureTO to set
	 */
	public void setDigitalSignatureTO(DigitalSignatureTO digitalSignatureTO) {
		this.digitalSignatureTO = digitalSignatureTO;
	}

	/**
	 * Gets the lst type signature.
	 *
	 * @return the lstTypeSignature
	 */
	public List<ParameterTable> getLstTypeSignature() {
		return lstTypeSignature;
	}

	/**
	 * Sets the lst type signature.
	 *
	 * @param lstTypeSignature the lstTypeSignature to set
	 */
	public void setLstTypeSignature(List<ParameterTable> lstTypeSignature) {
		this.lstTypeSignature = lstTypeSignature;
	}

	/**
	 * Gets the digital signature.
	 *
	 * @return the digitalSignature
	 */
	public DigitalSignature getDigitalSignature() {
		return digitalSignature;
	}

	/**
	 * Sets the digital signature.
	 *
	 * @param digitalSignature the digitalSignature to set
	 */
	public void setDigitalSignature(DigitalSignature digitalSignature) {
		this.digitalSignature = digitalSignature;
	}

	/**
	 * Gets the list document type.
	 *
	 * @return the listDocumentType
	 */
	public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}

	/**
	 * Sets the list document type.
	 *
	 * @param listDocumentType the listDocumentType to set
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	/**
	 * Gets the file signature.
	 *
	 * @return the fileSignature
	 */
	public StreamedContent getFileSignature() {
		return fileSignature;
	}

	/**
	 * Sets the file signature.
	 *
	 * @param fileSignature the fileSignature to set
	 */
	public void setFileSignature(StreamedContent fileSignature) {
		this.fileSignature = fileSignature;
	}

	/**
	 * Gets the file signature name.
	 *
	 * @return the fileSignatureName
	 */
	public String getFileSignatureName() {
		return fileSignatureName;
	}

	/**
	 * Sets the file signature name.
	 *
	 * @param fileSignatureName the fileSignatureName to set
	 */
	public void setFileSignatureName(String fileSignatureName) {
		this.fileSignatureName = fileSignatureName;
	}

	/**
	 * Gets the file signature temp.
	 *
	 * @return the fileSignatureTemp
	 */
	public byte[] getFileSignatureTemp() {
		return fileSignatureTemp;
	}

	/**
	 * Sets the file signature temp.
	 *
	 * @param fileSignatureTemp the fileSignatureTemp to set
	 */
	public void setFileSignatureTemp(byte[] fileSignatureTemp) {
		this.fileSignatureTemp = fileSignatureTemp;
	}

	/**
	 * Checks if is aux fupl signature.
	 *
	 * @return the auxFuplSignature
	 */
	public boolean isAuxFuplSignature() {
		return auxFuplSignature;
	}

	/**
	 * Sets the aux fupl signature.
	 *
	 * @param auxFuplSignature the auxFuplSignature to set
	 */
	public void setAuxFuplSignature(boolean auxFuplSignature) {
		this.auxFuplSignature = auxFuplSignature;
	}

	/**
	 * Checks if is form state.
	 *
	 * @return the formState
	 */
	public boolean isFormState() {
		return formState;
	}

	/**
	 * Sets the form state.
	 *
	 * @param formState the formState to set
	 */
	public void setFormState(boolean formState) {
		this.formState = formState;
	}

	/**
	 * Gets the checks if is active.
	 *
	 * @return the isActive
	 */
	public Integer getIsActive() {
		return isActive;
	}

	/**
	 * Sets the checks if is active.
	 *
	 * @param isActive the isActive to set
	 */
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the list active.
	 *
	 * @return the listActive
	 */
	public List<BooleanType> getListActive() {
		return listActive;
	}

	/**
	 * Sets the list active.
	 *
	 * @param listActive the listActive to set
	 */
	public void setListActive(List<BooleanType> listActive) {
		this.listActive = listActive;
	}

	/**
	 * Gets the gdm digital signature.
	 *
	 * @return the gdmDigitalSignature
	 */
	public GenericDataModel<DigitalSignature> getGdmDigitalSignature() {
		return gdmDigitalSignature;
	}

	/**
	 * Sets the gdm digital signature.
	 *
	 * @param gdmDigitalSignature the gdmDigitalSignature to set
	 */
	public void setGdmDigitalSignature(
			GenericDataModel<DigitalSignature> gdmDigitalSignature) {
		this.gdmDigitalSignature = gdmDigitalSignature;
	}

}
