package com.pradera.custody.digitalsignature.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SubsidiaryBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 02/07/2015
 */

public class DigitalSignatureTO  implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**  the idDigitalSignature. */
	private Long idDigitalSignature;
	
	/**  the typeSignature. */
	private Integer typeSignature;
	
	/** The active signature. */
	private Integer activeSignature;
	
	/** The name. */
	private String name;

	/**
	 * Gets the id digital signature.
	 *
	 * @return the idDigitalSignature
	 */
	public Long getIdDigitalSignature() {
		return idDigitalSignature;
	}

	/**
	 * Sets the id digital signature.
	 *
	 * @param idDigitalSignature the idDigitalSignature to set
	 */
	public void setIdDigitalSignature(Long idDigitalSignature) {
		this.idDigitalSignature = idDigitalSignature;
	}

	/**
	 * Gets the type signature.
	 *
	 * @return the typeSignature
	 */
	public Integer getTypeSignature() {
		return typeSignature;
	}

	/**
	 * Sets the type signature.
	 *
	 * @param typeSignature the typeSignature to set
	 */
	public void setTypeSignature(Integer typeSignature) {
		this.typeSignature = typeSignature;
	}

	/**
	 * Gets the active signature.
	 *
	 * @return the activeSignature
	 */
	public Integer getActiveSignature() {
		return activeSignature;
	}

	/**
	 * Sets the active signature.
	 *
	 * @param activeSignature the activeSignature to set
	 */
	public void setActiveSignature(Integer activeSignature) {
		this.activeSignature = activeSignature;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
