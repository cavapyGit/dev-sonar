package com.pradera.custody.digitalsignature.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.digitalsignature.to.DigitalSignatureTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;

//TODO: Auto-generated Javadoc
/**
* <ul>
* <li>
* Project iDepositary.
* Copyright PraderaTechnologies 2015.</li>
* </ul>
* 
* The Class DigitalSignatureServiceBean.
*
* @author PraderaTechnologies.
* @version 1.0 , 14-07-2015
*/

@Stateless
public class DigitalSignatureServiceBean extends CrudDaoServiceBean {
	
	/**
	 * Search Digital Signature by management service bean.
	 *
	 * @param digitalSignatureTO the Digital Signature
	 * @return the list Digital Signature 
	 * @throws ServiceException the service exception
	 */
	
	@SuppressWarnings("unchecked")
	public List<DigitalSignature> getListDigitalSignature(DigitalSignatureTO digitalSignatureTO) throws ServiceException{
		List<DigitalSignature> lstDigitalSignature = new ArrayList<DigitalSignature>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select ds from DigitalSignature ds ");			
			sbQuery.append(" where 1=1 ");	
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getTypeSignature())){
				sbQuery.append(" and ds.typeSignature = :typeSignature  ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getActiveSignature())){
				sbQuery.append(" and ds.activeSignature = :activeSignature  ");
			}
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getName())){
				sbQuery.append(" and ds.name = :name  ");
			}
			sbQuery.append(" ORDER BY ds.idDigitalSignaturePk desc ");
			Query query = em.createQuery(sbQuery.toString());
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getTypeSignature())){
				query.setParameter("typeSignature", digitalSignatureTO.getTypeSignature());				
			}
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getActiveSignature())){
				query.setParameter("activeSignature", digitalSignatureTO.getActiveSignature());				
			}
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getName())){
				query.setParameter("name", "%" + digitalSignatureTO.getName() + "%");
			}
			lstDigitalSignature = (List<DigitalSignature>) query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return lstDigitalSignature;
	}
	
	/**
	 * Get Digital Signature by management service bean.
	 *
	 * @param typeSignature the Type Digital Signature
	 * @return the list Digital Signature
	 * @throws ServiceException the service exception
	 */
	
	@SuppressWarnings("unchecked")
	public List<DigitalSignature> getListDigitalSignatureBlob(Integer typeSignature) throws ServiceException{
		List<DigitalSignature> lstDigitalSignature = new ArrayList<DigitalSignature>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select distinct ds from DigitalSignature ds ");			
			sbQuery.append(" where ds.typeSignature = :typeSignature and ds.activeSignature = 1");	
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("typeSignature", typeSignature);	
			lstDigitalSignature = (List<DigitalSignature>) query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
		return lstDigitalSignature;
	}
	
	/**
	 * Exits active signature service bean.
	 *
	 * @param typeSignature the Type Signature
	 * @return the Exits active signature
	 * @throws ServiceException the service exception
	 */
	
	public boolean getExitsMostActiveSignature(DigitalSignatureTO digitalSignatureTO)  throws ServiceException {
		try {			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	select count(*) ");
			sbQuery.append("	FROM DigitalSignature ds ");
			sbQuery.append("	where ds.typeSignature = :typeSignature  ");
			sbQuery.append("	and ds.activeSignature = 1 ");
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getIdDigitalSignature())){
				sbQuery.append("	and ds.idDigitalSignaturePk not in (:idDigitalSignature) ");
			}
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("typeSignature", digitalSignatureTO.getTypeSignature());
			if(Validations.validateIsNotNullAndNotEmpty(digitalSignatureTO.getIdDigitalSignature())){
				query.setParameter("idDigitalSignature", digitalSignatureTO.getIdDigitalSignature());
			}
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult())){
				if(new Integer(query.getSingleResult().toString()) >= 1){
					return true;
				} else {
					return false;
				}	
			}			
		} catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
}
