package com.pradera.custody.digitalsignature.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.custody.accreditationcertificates.facade.AccreditationCertificatesServiceFacade;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class VerificationCatBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 14/07/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class VerificationCatBean  extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The accreditation operation to. */	
	AccreditationOperationTO acccreditationOperationTO;
	
	/** The accreditation consult list. */
	private GenericDataModel<AccreditationDetail> accreditationOperationDataModel;
	
	/** The accreditation facade bean. */	
	@Inject
	private AccreditationCertificatesServiceFacade accreditationfacadeBean;
	
	/** The dematerialization certificate facade. */
	@Inject
	DematerializationCertificateFacade dematerializationCertificateFacade;
	
	/** The file download. */
	private transient StreamedContent fileDownload; 
	
	@PostConstruct
	public void init(){
		try {
			acccreditationOperationTO = new AccreditationOperationTO();
			showPrivileges();
		} catch(Exception ex){
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Show privileges.
	 *
	 * @throws ServiceException the service exception
	 */
	private void showPrivileges() throws ServiceException {
		PrivilegeComponent privilegeComp = new PrivilegeComponent();
		privilegeComp.setBtnSearchView(true);		
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	/**
	 * Search Accreditation Operation listener.
	 * 
	 * @param event the event
	 */
	public void searchAccreditationOperation(ActionEvent event){
		accreditationOperationDataModel = null;
		try {
			List<AccreditationDetail> accOperationList = accreditationfacadeBean.getAccreditationOperationForVerificationCat(acccreditationOperationTO);
			if(Validations.validateListIsNotNullAndNotEmpty(accOperationList)){
				for(AccreditationDetail objAccreditationDetail : accOperationList){
					AccreditationOperation objAccreditationOperation =  objAccreditationDetail.getAccreditationOperation();
					if(AccreditationOperationStateType.CANCELED.getCode().equals(objAccreditationOperation.getAccreditationState())
							|| AccreditationOperationStateType.UNBLOCKED.getCode().equals(objAccreditationOperation.getAccreditationState())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getMessage(PropertiesConstants.VERIFICATIONS_CATS_ERROR_UNBLOCKED));
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
					
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(objAccreditationDetail.getHolderAccount().getIdHolderAccountPk());
					holderAccount = dematerializationCertificateFacade.getHolderAccount(holderAccount);
					objAccreditationDetail.setHolderAccount(holderAccount);				
				}				
				accreditationOperationDataModel = new GenericDataModel<AccreditationDetail>(accOperationList);
			} else {
				accreditationOperationDataModel = new GenericDataModel<AccreditationDetail>();
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public StreamedContent getAccreditationCertification(Long certificationId){
		 try {
			 byte[] certificationPdf = this.accreditationfacadeBean.getAccreditationCertification(certificationId);			
			 if(Validations.validateIsNotNull(certificationPdf)){						
				 InputStream stream = new ByteArrayInputStream(certificationPdf);
				 fileDownload = new DefaultStreamedContent(stream, "pdf", "CertificadoAcreditacion"+certificationId+".pdf"); 
			 }
		 } catch (ServiceException e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		 }
		 return fileDownload;
	 }
	
	/**
	 * Clean search Accreditation Operation listener.
	 *
	 * @param event the event
	 */
	public void cleanSearchAccreditationOperation(ActionEvent event){
		acccreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationDataModel = null;
	}

	/**
	 * @return the acccreditationOperationTO
	 */
	public AccreditationOperationTO getAcccreditationOperationTO() {
		return acccreditationOperationTO;
	}

	/**
	 * @param acccreditationOperationTO the acccreditationOperationTO to set
	 */
	public void setAcccreditationOperationTO(
			AccreditationOperationTO acccreditationOperationTO) {
		this.acccreditationOperationTO = acccreditationOperationTO;
	}

	/**
	 * @return the accreditationOperationDataModel
	 */
	public GenericDataModel<AccreditationDetail> getAccreditationOperationDataModel() {
		return accreditationOperationDataModel;
	}

	/**
	 * @param accreditationOperationDataModel the accreditationOperationDataModel to set
	 */
	public void setAccreditationOperationDataModel(
			GenericDataModel<AccreditationDetail> accreditationOperationDataModel) {
		this.accreditationOperationDataModel = accreditationOperationDataModel;
	}

	/**
	 * @return the fileDownload
	 */
	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	/**
	 * @param fileDownload the fileDownload to set
	 */
	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}	

}
