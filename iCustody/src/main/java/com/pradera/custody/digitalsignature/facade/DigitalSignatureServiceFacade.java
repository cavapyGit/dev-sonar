package com.pradera.custody.digitalsignature.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.custody.digitalsignature.service.DigitalSignatureServiceBean;
import com.pradera.custody.digitalsignature.to.DigitalSignatureTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class DigitalSignatureServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class DigitalSignatureServiceFacade {

	/** The digital signature service bean. */
	@EJB
	private DigitalSignatureServiceBean digitalSignatureServiceBean;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/**
 	 * Register Digital Signature service facade.
 	 *
 	 * @param objDigitalSignature the DigitalSignature
 	 * @return DigitalSignature, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public DigitalSignature registerDigitalSignatureServiceFacade(DigitalSignature objDigitalSignature)  throws ServiceException {
 		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		digitalSignatureServiceBean.create(objDigitalSignature);
 		return objDigitalSignature;
 	}
 	
 	/**
 	 * Register Digital Signature service facade.
 	 *
 	 * @param objDigitalSignature the DigitalSignature
 	 * @return DigitalSignature, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public DigitalSignature updateDigitalSignatureServiceFacade(DigitalSignature objDigitalSignature)  throws ServiceException {
 		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify());
		digitalSignatureServiceBean.update(objDigitalSignature);
 		return objDigitalSignature;
 	}
 	
 	/**
	 * Search Digital Signature by management service bean.
	 *
	 * @param digitalSignatureTO the Digital Signature
	 * @return the list Digital Signature Filter
	 * @throws ServiceException the service exception
	 */		
	public List<DigitalSignature> getListDigitalSignature(DigitalSignatureTO digitalSignatureTO) throws ServiceException{
		return digitalSignatureServiceBean.getListDigitalSignature(digitalSignatureTO);
	}
	
	/**
	 * Exits active signature service bean.
	 *
	 * @param digitalSignatureTO the digital signature to
	 * @return the Exits active signature
	 * @throws ServiceException the service exception
	 */
	
	public boolean getExitsMostActiveSignature(DigitalSignatureTO digitalSignatureTO)  throws ServiceException {
		return digitalSignatureServiceBean.getExitsMostActiveSignature(digitalSignatureTO);
	}
	
}
