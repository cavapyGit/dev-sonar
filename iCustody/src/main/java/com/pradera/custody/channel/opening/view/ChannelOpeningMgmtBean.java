package com.pradera.custody.channel.opening.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.channel.opening.facade.ChannelOpeningServiceFacade;
import com.pradera.custody.channel.opening.to.ChannelOpeningModelTO;
import com.pradera.custody.channel.opening.to.Constatnt;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.channelopening.ChannelOpening;
import com.pradera.model.custody.channelopening.type.ChannelOpeningStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;

@LoggerCreateBean
@DepositaryWebBean
public class ChannelOpeningMgmtBean extends GenericBaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	

	@Inject
	private UserInfo userInfo;
	

	@Inject
	private UserPrivilege userPrivilege;
	

	@Inject
	private transient PraderaLogger log;
	

	@EJB
	NotificationServiceFacade notificationServiceFacade;
	

	@EJB
	private AccountsFacade accountsFacade;
	

	@EJB
	private GeneralParametersFacade generalParametersFacade;
	

	@Inject
	protected Event<ExceptionToCatchEvent> excepcion;
	

	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	

	@EJB
	private ChannelOpeningServiceFacade ejbChannelOpening;

	private ChannelOpeningModelTO modelTOReg;

	private ChannelOpeningModelTO modelTOSearch;

	private ChannelOpeningModelTO modelTOView;

	private boolean privilegeRegister;
	private boolean privilegeSearch;

	private boolean operationApprove;
	private boolean operationAnulate;
	private boolean operationConfirm;
	private boolean operationReject;
	private boolean operationExpiration;

	private ChannelOpeningDataModel searchChannelOpDataModel = new ChannelOpeningDataModel();
	private List<ChannelOpeningModelTO> lstChannelOpeningTO;

	private String fUploadFileTypesToChannelOpening = "jpg|jpeg|pdf|doc|docx|png";
	private String fUploadsDisplayToChannelOp = "*.jpg, *.jpeg, *.png, *.doc, *.docx, *.pdf";
	private String docFileNameDisplay;

	@PostConstruct
	public void init() {
		loadSearch();
	}

	/** redirige a la pagina de registro
	 * 
	 * @return */
	public String goPageRegister() {
		loadRegiter();
		return Constatnt.PAGE_REGISTER;
	}

	/** redireige a la pnatalla de administracion
	 * 
	 * @return */
	public String goPageSearch() {
		loadSearch();
		return Constatnt.PAGE_SEARCH;
	}

	/** cargando la pantalla de registro */
	public void loadRegiter() {
		modelTOReg = new ChannelOpeningModelTO();
		modelTOReg.setLstParticipant(getLstParticipant());
		modelTOReg.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
		
		modelTOReg.setParticipantLogin(userInfo.getUserAccountSession().getPartIssuerCode());
		modelTOReg.setUserIsParticipant(false);
		if (userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode())) {
			Participant participant = accountsFacade.getParticipantByPk(modelTOReg.getParticipantLogin());
			modelTOReg.setUserIsParticipant(true);
			modelTOReg.setIdParticipantSelected(modelTOReg.getParticipantLogin());

			if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode())) {
				privilegeRegister = false;
			}
			RequestContext.getCurrentInstance().update("cboparticipant");
		}

		// adiconando por defecto el siguiente dia habil
		Calendar c = Calendar.getInstance();
		int addDays = 0;
		modelTOReg.setChannelOpnDateWithoutOneDay(c.getTime());
		do {
			c.add(Calendar.DATE, ++addDays);
		} while (holidayQueryServiceBean.isNonWorkingDayServiceBean(c.getTime(), true));
		modelTOReg.setDateChannelOpening(c.getTime());
	}
	
	public void btnClear(){
		loadSearch();
		JSFUtilities.resetViewRoot();
	}

	public void btnClearRegister() {
		loadRegiter();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * metodo para validar el minimo y el maximo de horas permitidas en una solicitud de apertura de canal
	 */
	public boolean validateHourInitialAndFinal(){
		Calendar minHourAllowed=Calendar.getInstance();
		minHourAllowed.set(1970, Calendar.JANUARY, 1, 8, 29);
		Calendar maxHourAllowed=Calendar.getInstance();
		maxHourAllowed.set(1970, Calendar.JANUARY, 1, 16, 30);
		if(Validations.validateIsNotNull(modelTOReg.getInitialHour())){
			// validar la hora minima permitida es 8:30
			if(modelTOReg.getInitialHour().compareTo(minHourAllowed.getTime())<0){
				modelTOReg.setInitialHour(null);
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_MININITIALHOUR_ALLOWED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return true;
			}
		}
		if(Validations.validateIsNotNull(modelTOReg.getFinalHour())){
			// validar la hora maxima permitida es 16:30
			if(modelTOReg.getFinalHour().compareTo(maxHourAllowed.getTime())>0){
				modelTOReg.setFinalHour(null);
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_MAXFINALHOUR_ALLOWED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return true;
			}
		}
		return false;
	}

	/** antes de guardar la solicitud de apertura de canal */
	public void beforeSave() {
		// obteniendo la fecha de apertura, hora inicio y fin en formato string
		String dateChannelOp = CommonsUtilities.convertDatetoString(modelTOReg.getDateChannelOpening());
		String initialHour = CommonsUtilities.convertDatetoStringForTemplate(modelTOReg.getInitialHour(), CommonsUtilities.HOUR_PATTERN);
		String finalHour = CommonsUtilities.convertDatetoStringForTemplate(modelTOReg.getFinalHour(), CommonsUtilities.HOUR_PATTERN);
		
		// validando nuevamente el minimo yel maximo de horarios permitidos
		if(validateHourInitialAndFinal()){
			return;
		}

		// comprando las horas
		if(modelTOReg.getInitialHour().compareTo(modelTOReg.getFinalHour()) > 0){
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.VALIDATION_REQUIRED_INITIALHOUR_REQUIRED_LESS_FINALHOUR, null);
			JSFUtilities.showComponent("validationChannelOpening");
			return;
		}

		Object[] arrBodyData = { dateChannelOp, initialHour, finalHour };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REGITER_CHANNEL_OPENING, arrBodyData);
		JSFUtilities.showComponent("cnfChannelOpeningRegiter");
		
		JSFUtilities.hideComponent("validationChannelOpening");
		JSFUtilities.updateComponent("opnlDialogs");
	}

	/** guardando en BBDD la solicutd de apertura de canal */
	public void saveRegisterChannelOpening() {

		try {
			hideDialogsListener(null);
			
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOReg.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOReg.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOReg.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeRegister().longValue());
			
			ChannelOpening channelOpening = ejbChannelOpening.regiterChannelOpening(modelTOReg);
			
			String descriptionParticipat="";
			for(Participant p:modelTOReg.getLstParticipant()){
				if(p.getIdParticipantPk().equals(modelTOReg.getIdParticipantSelected())){
					descriptionParticipat = p.getDescription();
					break;
				}
			}
			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
			// envio de notificacion que se realizo un registro de solicitud de apertura de canal
			BusinessProcess businessProcessNotification = new BusinessProcess();
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_REGISTER.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
			businessProcessNotification,
			modelTOReg.getIdParticipantSelected(), parameters);
			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(channelOpening.getIdChannelOpeningPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REGISTER_CHANNEL_OPENING, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionChannelOpening");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/** metodo para cargar la pantalla de busqueda(administracion) */
	public void loadSearch() {

		modelTOSearch = new ChannelOpeningModelTO();
		if (userPrivilege.getUserAcctions().isRegister()) {
			log.info("::: tiene privilegios de registro");
			privilegeRegister = true;
		}
		if (userPrivilege.getUserAcctions().isSearch()) {
			log.info("::: tiene privilegios de busqueda");
			privilegeSearch = true;
		}

		modelTOSearch.setLstParticipant(getLstParticipant());
		modelTOSearch.setLstState(getLstStateChannelOpening());
		modelTOSearch.setLstMoviveChannelOpen(getLstMotiveChannelOpening());

		modelTOSearch.setInitialDate(new Date());
		modelTOSearch.setFinalDate(new Date());

		modelTOSearch.setParticipantLogin(userInfo.getUserAccountSession().getPartIssuerCode());
		modelTOSearch.setUserIsParticipant(false);
		if (userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.ISSUER_DPF.getCode())) {
			Participant participant = accountsFacade.getParticipantByPk(modelTOSearch.getParticipantLogin());
			modelTOSearch.setUserIsParticipant(true);
			modelTOSearch.setIdParticipantSelected(modelTOSearch.getParticipantLogin());

			if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode())) {
				privilegeRegister = false;
			}
			RequestContext.getCurrentInstance().update("cboparticipant");
		}
		lstChannelOpeningTO = new ArrayList<>();
		searchChannelOpDataModel = new ChannelOpeningDataModel();
	}

	/** realizando la busqueda segun los criterios seleccionados */
	public void btnSearch() {
		try {
			lstChannelOpeningTO = ejbChannelOpening.lstChannelOpeningModelTO(modelTOSearch);
			searchChannelOpDataModel = new ChannelOpeningDataModel(lstChannelOpeningTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/** lleva al detalle (pantalla de vista) un registro mediante su link
	 * 
	 * @param channelOp
	 * @return */
	public String linkView(ChannelOpeningModelTO channelOp) {
		modelTOView = (ChannelOpeningModelTO) channelOp.clone();
		modelTOView.setLstParticipant(getLstParticipant());
		modelTOView.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
		modelTOView.setLstState(getLstStateChannelOpening());

		// verificando si esta en estado vencido para obtener los demas datos
		if (modelTOView.getStateChannelOpening().equals(ChannelOpeningStateType.VENCIDO.getCode())) {
			ChannelOpening co = ejbChannelOpening.findChannelOpening(modelTOView.getIdChannelOpeningPk());
			if (Validations.validateIsNotNull(co)) {
				modelTOView.setLstMoviveChannelClose(getLstMotiveChannelCloussure());
				modelTOView.setMotiveChannelClosureSelected(co.getMotiveChannelClosure());
				modelTOView.setObservationChannelClosure(co.getObservationChannelClosure());
				if (Validations.validateIsNotNull(co.getFileAttached()) && Validations.validateIsNotNull(co.getFileName())) {
					modelTOView.setFileAttached(co.getFileAttached());
					modelTOView.setFileName(co.getFileName());
				}
			}
		}

		renderFalseAllOperation();
		return Constatnt.PAGE_VIEW;
	}

	/** regresando a la pantalla de search desde la pantalla view
	 * 
	 * @return */
	public String backToSearchFromView() {
		btnSearch();
		return Constatnt.PAGE_SEARCH;
	}

	/** cargando datos para la pantalla de aprobacion
	 * 
	 * @return */
	public String loadApproved() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdChannelOpeningPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateChannelOpening().equals(ChannelOpeningStateType.REGISTRADO.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
				modelTOView.setLstState(getLstStateChannelOpening());
				renderBtnApprove();
				return Constatnt.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado registrado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}

	/** antes de aprobar */
	public void beforeApprove() {
		// obteniendo la fecha de apertura, hora inicio y fin en formato string
		String numberRequest = modelTOView.getIdChannelOpeningPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_APPROVED_CHANNEL_OPENING, arrBodyData);
		JSFUtilities.showComponent("cnfChannelOpeningView");
	}

	/** guardando en la BBDD la aprobacion de una apertura de canal
	 * 
	 * @throws Exception */
	public void saveApproveChannelOpening() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApprove().longValue());
			
			modelTOView.setStateChannelOpening(ChannelOpeningStateType.APROBADO.getCode());
			ChannelOpening channelOpening = ejbChannelOpening.changeStateChannelOpening(modelTOView);

			// envio de notificacion que se realizo unA APROBACIONS de solicitud de apertura de canal
			String descriptionParticipat="";
			for(Participant p:modelTOView.getLstParticipant()){
				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
					descriptionParticipat = p.getDescription();
					break;
				}
			}
			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
			BusinessProcess businessProcessNotification = new BusinessProcess();
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_APPROVAL.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
			businessProcessNotification,
			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(channelOpening.getIdChannelOpeningPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_APPROVED_CHANNEL_OPENING, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionChannelOpening");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//throw new Exception();
		}
	}

	/** cargando datos para la pantalla de anulacion
	 * 
	 * @return */
	public String loadAnulate() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdChannelOpeningPk())) {
			// validando que el estado sea registrado
			if (modelTOView.getStateChannelOpening().equals(ChannelOpeningStateType.REGISTRADO.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
				modelTOView.setLstState(getLstStateChannelOpening());
				renderBtnAnulate();
				return Constatnt.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado registrado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_STATEREGISTERED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}

	/** antes de anular se lanza el mensaje de confirmacion */
	public void beforeAnulate() {
		String numberRequest = modelTOView.getIdChannelOpeningPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_ANULATE_CHANNEL_OPENING, arrBodyData);
		JSFUtilities.showComponent("cnfChannelOpeningView");
	}

	/** registrando en la BBDD la anulacion de una apertura de canal
	 * 
	 * @throws Exception */
	public void saveAnulateChannelOpening() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeAnnular().longValue());
			
			modelTOView.setStateChannelOpening(ChannelOpeningStateType.ANULADO.getCode());
			ChannelOpening channelOpening = ejbChannelOpening.changeStateChannelOpening(modelTOView);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(channelOpening.getIdChannelOpeningPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_ANULATE_CHANNEL_OPENING, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionChannelOpening");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//throw new Exception();
		}
	}

	/** cagando datos para la pantalla de confirmacion
	 * 
	 * @return */
	public String loadConfirm() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdChannelOpeningPk())) {
			// validando que el estado sea aprobado
			if (modelTOView.getStateChannelOpening().equals(ChannelOpeningStateType.APROBADO.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
				modelTOView.setLstState(getLstStateChannelOpening());
				renderBtnConfirm();
				return Constatnt.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_STATEAPPROVED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}

	/** antes cambiar a estado CONFIRMADO se lanza un mesnaje de confirmacion */
	public void beforeConfirm() {
		String numberRequest = modelTOView.getIdChannelOpeningPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_CONFIRM_CHANNEL_OPENING, arrBodyData);
		JSFUtilities.showComponent("cnfChannelOpeningView");
	}

	/** guardando en la BBDD una confirmacion de una apertura de canal
	 * 
	 * @throws Exception */
	public void saveConfirmChannelOpening() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeConfirm().longValue());
			
			modelTOView.setStateChannelOpening(ChannelOpeningStateType.CONFIRMADO.getCode());
			ChannelOpening channelOpening = ejbChannelOpening.changeStateChannelOpening(modelTOView);

			// envio de notificacion que se realizo una CONFIRMACION de solicitud de apertura de canal
			String descriptionParticipat="";
			for(Participant p:modelTOView.getLstParticipant()){
				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
					descriptionParticipat = p.getDescription();
				}
			}
			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
			BusinessProcess businessProcessNotification = new BusinessProcess();
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_CONFIRM.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
			businessProcessNotification,
			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(channelOpening.getIdChannelOpeningPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_CONFIRM_CHANNEL_OPENING, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionChannelOpening");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//throw new Exception();
		}
	}

	public String loadReject() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdChannelOpeningPk())) {
			// validando que el estado sea aprobado
			if (modelTOView.getStateChannelOpening().equals(ChannelOpeningStateType.APROBADO.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
				modelTOView.setLstState(getLstStateChannelOpening());
				renderBtnReject();
				return Constatnt.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_STATEAPPROVED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}

	public void beforeReject() {
		String numberRequest = modelTOView.getIdChannelOpeningPk().toString();

		Object[] arrBodyData = { numberRequest };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_REJECT_CHANNEL_OPENING, arrBodyData);
		JSFUtilities.showComponent("cnfChannelOpeningView");
	}

	public void saveRejectChannelOpening() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeReject().longValue());
			
			modelTOView.setStateChannelOpening(ChannelOpeningStateType.RECHAZADO.getCode());
			ChannelOpening channelOpening = ejbChannelOpening.changeStateChannelOpening(modelTOView);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(channelOpening.getIdChannelOpeningPk()) };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REJECT_CHANNEL_OPENING, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionChannelOpening");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//throw new Exception();
		}
	}

	public String loadExpiration() {

		if (Validations.validateIsNotNull(modelTOView) && Validations.validateIsNotNull(modelTOView.getIdChannelOpeningPk())) {
			// validando que el estado sea confirmado
			if (modelTOView.getStateChannelOpening().equals(ChannelOpeningStateType.CONFIRMADO.getCode())) {
				modelTOView.setLstParticipant(getLstParticipant());
				modelTOView.setLstMoviveChannelOpen(getLstMotiveChannelOpening());
				modelTOView.setLstMoviveChannelClose(getLstMotiveChannelCloussure());
				renderBtnExpiration();
				return Constatnt.PAGE_VIEW;
			} else {
				// lanzando mensaje de que tiene q estar en estado aprobado
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_STATECONFIRMED, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return null;
			}
		} else {
			// lanzar mensaje de requerimiento de seleccion
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_CHANNELOPENING_REQUIRED_SELECTED, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return null;
		}
	}

	public void beforeExpiration() {
		String numberRequest = modelTOView.getIdChannelOpeningPk().toString();
		String participatDescription = "";
		for (Participant p : modelTOView.getLstParticipant()) {
			if (modelTOView.getIdParticipantSelected().equals(p.getIdParticipantPk())) {
				participatDescription = p.getDescription();
				break;
			}
		}

		Object[] arrBodyData = { numberRequest, participatDescription };
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_EXPIRATION_CHANNEL_OPENING, arrBodyData);
		JSFUtilities.showComponent("cnfChannelOpeningView");
	}

	public void saveExpirationChannelOpening() throws Exception {

		try {
			hideDialogsListener(null);
			// opteniendo las pistas de auditoria manualmente por si el processLogger esta en null en el facade
			modelTOView.setAuditUserName(userInfo.getUserAccountSession().getUserName());
			modelTOView.setAuditIpAddress(userInfo.getUserAccountSession().getIpAddress());
			modelTOView.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeApply().longValue());
			
			modelTOView.setStateChannelOpening(ChannelOpeningStateType.VENCIDO.getCode());
			String participatDescription = "";
			for (Participant p : modelTOView.getLstParticipant()) {
				if (modelTOView.getIdParticipantSelected().equals(p.getIdParticipantPk())) {
					participatDescription = p.getDescription();
					break;
				}
			}
			ChannelOpening channelOpening = ejbChannelOpening.changeStateChannelOpening(modelTOView);

			// envio de notificacion que se realizo un VENCIMIENTO(CIERRE) de solicitud de apertura de canal
			String descriptionParticipat="";
			for(Participant p:modelTOView.getLstParticipant()){
				if(p.getIdParticipantPk().equals(modelTOView.getIdParticipantSelected())){
					descriptionParticipat = p.getDescription();
				}
			}
			Object[] parameters = { String.valueOf(channelOpening.getIdChannelOpeningPk()), descriptionParticipat};
			BusinessProcess businessProcessNotification = new BusinessProcess();
			businessProcessNotification.setIdBusinessProcessPk(BusinessProcessType.NOTIFICATION_CHANNEL_OPENING_EXPIRED.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
			businessProcessNotification,
			modelTOView.getIdParticipantSelected(), parameters);

			btnSearch();

			// mensaje de exito
			Object[] arrBodyData = { String.valueOf(channelOpening.getIdChannelOpeningPk()), participatDescription };
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_EXPIRATION_CHANNEL_OPENING, arrBodyData);
			JSFUtilities.showComponent("cnfEndTransactionChannelOpening");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			//throw new Exception();
		}
	}

	private String getDisplayName(UploadedFile fileUploaded) {
		String fDisplayName = null;
		if (fileUploaded.getFileName().length() > 30) {
			StringBuilder sbFileName = new StringBuilder();
			sbFileName.append("...");
			sbFileName.append(fileUploaded.getFileName().substring(fileUploaded.getFileName().length() - 27));
			fDisplayName = sbFileName.toString();
		} else {
			fDisplayName = fileUploaded.getFileName();
		}
		return fDisplayName;
	}

	public void documentAttachChannelOpFile(FileUploadEvent event) {
		String fDisplayName = getDisplayName(event.getFile());
		if (fDisplayName != null) {
			docFileNameDisplay = fDisplayName;
			modelTOView.setFileName(fUploadValidateFileNameLength(event.getFile().getFileName()));
			modelTOView.setFileAttached(event.getFile().getContents());
		}
	}

	public void saveOperationChangeState() throws Exception {
		if (operationApprove) {
			saveApproveChannelOpening();
			return;
		}
		if (operationAnulate) {
			saveAnulateChannelOpening();
			return;
		}
		if (operationConfirm) {
			saveConfirmChannelOpening();
			return;
		}
		if (operationReject) {
			saveRejectChannelOpening();
			return;
		}
		if (operationExpiration) {
			saveExpirationChannelOpening();
			return;
		}
	}

	// TODO metodos genericos para cargar las listas(combobox) necesarios para cualquier pantalla (registro, vista, search)
	public List<Participant> getLstParticipant() {
		try {
			Participant filter = new Participant();
			filter.setState(ParticipantStateType.REGISTERED.getCode());
			return accountsFacade.getLisParticipantServiceBean(filter);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public List<ParameterTable> getLstMotiveChannelOpening() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.MOTIVE_CHANNEL_OPENING.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public List<ParameterTable> getLstMotiveChannelCloussure() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.MOTIVE_CHANNEL_CLOUSSURE.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public List<ParameterTable> getLstStateChannelOpening() {
		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.STATUS_CHANNEL_OPENING.getCode());
			return generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
			return new ArrayList<>();
		}
	}

	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();

		JSFUtilities.hideComponent("cnfChannelOpeningRegiter");
		// JSFUtilities.hideComponent("cnfEndTransactionParticipant");
		// JSFUtilities.hideComponent("detailRepresentativeDialog");

	}

	// TODO metodos para el renderizados de los botones de las operaciones (aprobado, anulado, confirmado, rechzado, vencido)
	public void renderBtnApprove() {
		renderFalseAllOperation();
		operationApprove = true;
	}

	public void renderBtnAnulate() {
		renderFalseAllOperation();
		operationAnulate = true;
	}

	public void renderBtnConfirm() {
		renderFalseAllOperation();
		operationConfirm = true;
	}

	public void renderBtnReject() {
		renderFalseAllOperation();
		operationReject = true;
	}

	public void renderBtnExpiration() {
		renderFalseAllOperation();
		operationExpiration = true;
	}

	private void renderFalseAllOperation() {
		operationApprove = false;
		operationAnulate = false;
		operationConfirm = false;
		operationReject = false;
		operationExpiration = false;
	}
}
