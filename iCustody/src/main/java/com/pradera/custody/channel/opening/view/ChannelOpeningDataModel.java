package com.pradera.custody.channel.opening.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.custody.channel.opening.to.ChannelOpeningModelTO;

/**
 * DataModel de la clase ChannelOpeningModelTO
 * @author rlarico
 *
 */
public class ChannelOpeningDataModel extends ListDataModel<ChannelOpeningModelTO> implements SelectableDataModel<ChannelOpeningModelTO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// constructor
	public ChannelOpeningDataModel (){}
	
	//2do constructor
	public ChannelOpeningDataModel (List<ChannelOpeningModelTO> lstChannelOpening){
		super(lstChannelOpening);
	}

	@Override
	public ChannelOpeningModelTO getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<ChannelOpeningModelTO> lst= (List<ChannelOpeningModelTO>) getWrappedData();
		for(ChannelOpeningModelTO channelOp:lst){
			if(channelOp.getIdChannelOpeningPk().toString().equals(rowKey))
				return channelOp;
		}
		return null;
	}

	@Override
	public Object getRowKey(ChannelOpeningModelTO channelOp) {
		// TODO Auto-generated method stub
		return channelOp.getIdChannelOpeningPk();
	}

}
