package com.pradera.custody.channel.opening.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.channel.opening.to.ChannelOpeningModelTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.channelopening.ChannelOpening;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ChannelOpeningServiceBean extends CrudDaoServiceBean {
	
	public ChannelOpening updateChannelOpening(ChannelOpening channelOpening) throws ServiceException{
		return update(channelOpening);
	}
	
	public ChannelOpening regiterChannelOpening(ChannelOpening channelOpening){
		return createWithFlush(channelOpening);
	}
	
	public List<ChannelOpeningModelTO> getLstChannelOpeningTO(ChannelOpeningModelTO filter) throws ServiceException{
		List<ChannelOpeningModelTO> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append("select");
		sd.append(" new com.pradera.custody.channel.opening.to.ChannelOpeningModelTO(");
		sd.append(" co.idChannelOpeningPk, co.dateChannelOpening, co.initialHour, co.finalHour, co.motiveChannelOpening, ");
		sd.append(" co.observationChannelOpening, co.registryDate, co.state, p.idParticipantPk, p.mnemonic");
		sd.append(" ) ");
		sd.append(" from ChannelOpening co ");
		sd.append(" inner join co.idParticipantFk p");
		sd.append(" where 0=0");
		if(Validations.validateIsNotNull(filter.getInitialDate())
				&& Validations.validateIsNotNull(filter.getFinalDate())){
			sd.append(" and trunc(co.registryDate) between :initialDate and :finalDate");	
		}
		if(Validations.validateIsNotNull(filter.getIdParticipantSelected())){
			sd.append(" and p.idParticipantPk = :idParticipant");	
		}
		if(Validations.validateIsNotNull(filter.getMnemonicPart())){
			sd.append(" and p.mnemonic = :mnemonicParticipant");	
		}
		if(Validations.validateIsNotNull(filter.getDateChannelOpening())){
			sd.append(" and co.dateChannelOpening = :dateChannelOp");	
		}
		if(Validations.validateIsNotNull(filter.getStateChannelOpening())){
			sd.append(" and co.state = :stateChannelOp");	
		}
		if(Validations.validateIsNotNull(filter.getIdChannelOpeningPk())){
			sd.append(" and co.idChannelOpeningPk = :numberRequest");	
		}
		sd.append(" order by co.idChannelOpeningPk asc");
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		if(Validations.validateIsNotNull(filter.getInitialDate())
				&& Validations.validateIsNotNull(filter.getFinalDate())){
			parameters.put("initialDate", filter.getInitialDate());
			parameters.put("finalDate", filter.getFinalDate());
		}
		if(Validations.validateIsNotNull(filter.getIdParticipantSelected())){
			parameters.put("idParticipant", filter.getIdParticipantSelected());
		}
		if(Validations.validateIsNotNull(filter.getMnemonicPart())){
			parameters.put("mnemonicParticipant", filter.getMnemonicPart());
		}
		if(Validations.validateIsNotNull(filter.getDateChannelOpening())){
			parameters.put("dateChannelOp", filter.getDateChannelOpening());
		}
		if(Validations.validateIsNotNull(filter.getStateChannelOpening())){
			parameters.put("stateChannelOp", filter.getStateChannelOpening());
		}
		if(Validations.validateIsNotNull(filter.getIdChannelOpeningPk())){
			parameters.put("numberRequest", filter.getIdChannelOpeningPk());
		}
		lst = findListByQueryString(sd.toString(), parameters);
		return lst;
	}
	

}
