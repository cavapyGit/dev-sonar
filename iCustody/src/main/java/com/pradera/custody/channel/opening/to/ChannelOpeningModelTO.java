package com.pradera.custody.channel.opening.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;


/**
 * @author rlarico
 *
 */
public class ChannelOpeningModelTO implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idChannelOpeningPk;

    private Date dateChannelOpening;
    
    private Date initialHour;
    
    private Date finalHour;
    
    private String observationChannelOpening;
    
    private Date registryDate;
    
    private String observationChannelClosure;
    
    private Serializable fileAttached;
    
    private Integer stateChannelOpening;
    private String stateChannelOpText;
    
    private String fileName;
    
    private Long idParticipantSelected;
    
    private Integer motiveChannelOpeningSelected;
    private String motiveChannelOpText;
    
    private Integer motiveChannelClosureSelected;
    
    private List<Participant> lstParticipant=new ArrayList<>();
    
    private List<ParameterTable> lstMoviveChannelOpen=new ArrayList<>();
    
    private List<ParameterTable> lstMoviveChannelClose=new ArrayList<>();
    
    private List<ParameterTable> lstState=new ArrayList<>();
    
    private Date initialDate;
    private Date finalDate;
    
    private Date channelOpnDateWithoutOneDay;
    
    // atributos de session durante la apertura de canal
    private Long participantLogin;
    private boolean userIsParticipant;
    
    private String mnemonicPart;
    
    private String auditUserName;
    private String auditIpAddress;
    private Long auditIdPrivilege;
    
    // first constructor
    public ChannelOpeningModelTO(){}
    
    // constructor para el search de Apertura de canal
	public ChannelOpeningModelTO(Long idChannelOpeningPk, Date dateChannelOpening, Date initialHour, Date finalHour, Integer motiveChannelOpeningSelected,
			String observationChannelOpening, Date registryDate,
			Integer stateChannelOpening, Long idParticipant, String mnemonicPart) {
		this.idChannelOpeningPk = idChannelOpeningPk;
		this.dateChannelOpening = dateChannelOpening;
		this.initialHour = initialHour;
		this.finalHour = finalHour;
		this.observationChannelOpening = observationChannelOpening;
		this.registryDate = registryDate;
		this.stateChannelOpening = stateChannelOpening;
		this.motiveChannelOpeningSelected = motiveChannelOpeningSelected;
		this.idParticipantSelected = idParticipant;
		this.mnemonicPart = mnemonicPart;
	}
	
	/**
	 * metodo para verificar si algun campo fue llenado antes de limpiar los datos  de la pantalla de registro
	 * @return true si todos los campos estan vacios, false si se lleno algun campo
	 */
	public boolean checkIfAllLabelsIsNullToRegister(){
		if(Validations.validateIsNotNull(idParticipantSelected)){
			return false;
		}
		if(Validations.validateIsNotNull(dateChannelOpening)){
			return false;
		}
		if(Validations.validateIsNotNull(initialHour)){
			return false;
		}
		if(Validations.validateIsNotNull(finalHour)){
			return false;
		}
		if(Validations.validateIsNotNull(motiveChannelOpeningSelected)){
			return false;
		}
		if(Validations.validateIsNotNull(observationChannelOpening)){
			return false;
		}
		return true;
	}
	
	@Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}

	public Long getIdChannelOpeningPk() {
		return idChannelOpeningPk;
	}

	public void setIdChannelOpeningPk(Long idChannelOpeningPk) {
		this.idChannelOpeningPk = idChannelOpeningPk;
	}

	public Date getDateChannelOpening() {
		return dateChannelOpening;
	}

	public void setDateChannelOpening(Date dateChannelOpening) {
		this.dateChannelOpening = dateChannelOpening;
	}

	public Date getInitialHour() {
		return initialHour;
	}

	public void setInitialHour(Date initialHour) {
		this.initialHour = initialHour;
	}

	public Date getFinalHour() {
		return finalHour;
	}

	public void setFinalHour(Date finalHour) {
		this.finalHour = finalHour;
	}

	public String getObservationChannelOpening() {
		return observationChannelOpening;
	}

	public void setObservationChannelOpening(String observationChannelOpening) {
		this.observationChannelOpening = observationChannelOpening;
	}

	public Date getRegistryDate() {
		return registryDate;
	}

	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	public String getObservationChannelClosure() {
		return observationChannelClosure;
	}

	public void setObservationChannelClosure(String observationChannelClosure) {
		this.observationChannelClosure = observationChannelClosure;
	}

	public Serializable getFileAttached() {
		return fileAttached;
	}

	public void setFileAttached(Serializable fileAttached) {
		this.fileAttached = fileAttached;
	}

	public Integer getStateChannelOpening() {
		return stateChannelOpening;
	}

	public void setStateChannelOpening(Integer stateChannelOpening) {
		this.stateChannelOpening = stateChannelOpening;
	}

	public String getStateChannelOpText() {
		return stateChannelOpText;
	}

	public void setStateChannelOpText(String stateChannelOpText) {
		this.stateChannelOpText = stateChannelOpText;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getIdParticipantSelected() {
		return idParticipantSelected;
	}

	public void setIdParticipantSelected(Long idParticipantSelected) {
		this.idParticipantSelected = idParticipantSelected;
	}

	public Integer getMotiveChannelOpeningSelected() {
		return motiveChannelOpeningSelected;
	}

	public void setMotiveChannelOpeningSelected(Integer motiveChannelOpeningSelected) {
		this.motiveChannelOpeningSelected = motiveChannelOpeningSelected;
	}

	public String getMotiveChannelOpText() {
		return motiveChannelOpText;
	}

	public void setMotiveChannelOpText(String motiveChannelOpText) {
		this.motiveChannelOpText = motiveChannelOpText;
	}

	public Integer getMotiveChannelClosureSelected() {
		return motiveChannelClosureSelected;
	}

	public void setMotiveChannelClosureSelected(Integer motiveChannelClosureSelected) {
		this.motiveChannelClosureSelected = motiveChannelClosureSelected;
	}

	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	public List<ParameterTable> getLstMoviveChannelOpen() {
		return lstMoviveChannelOpen;
	}

	public void setLstMoviveChannelOpen(List<ParameterTable> lstMoviveChannelOpen) {
		this.lstMoviveChannelOpen = lstMoviveChannelOpen;
	}

	public List<ParameterTable> getLstMoviveChannelClose() {
		return lstMoviveChannelClose;
	}

	public void setLstMoviveChannelClose(List<ParameterTable> lstMoviveChannelClose) {
		this.lstMoviveChannelClose = lstMoviveChannelClose;
	}

	public List<ParameterTable> getLstState() {
		return lstState;
	}

	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Date getChannelOpnDateWithoutOneDay() {
		return channelOpnDateWithoutOneDay;
	}

	public void setChannelOpnDateWithoutOneDay(Date channelOpnDateWithoutOneDay) {
		this.channelOpnDateWithoutOneDay = channelOpnDateWithoutOneDay;
	}

	public Long getParticipantLogin() {
		return participantLogin;
	}

	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	public boolean isUserIsParticipant() {
		return userIsParticipant;
	}

	public void setUserIsParticipant(boolean userIsParticipant) {
		this.userIsParticipant = userIsParticipant;
	}

	public String getMnemonicPart() {
		return mnemonicPart;
	}

	public void setMnemonicPart(String mnemonicPart) {
		this.mnemonicPart = mnemonicPart;
	}

	public String getAuditUserName() {
		return auditUserName;
	}

	public void setAuditUserName(String auditUserName) {
		this.auditUserName = auditUserName;
	}

	public String getAuditIpAddress() {
		return auditIpAddress;
	}

	public void setAuditIpAddress(String auditIpAddress) {
		this.auditIpAddress = auditIpAddress;
	}

	public Long getAuditIdPrivilege() {
		return auditIdPrivilege;
	}

	public void setAuditIdPrivilege(Long auditIdPrivilege) {
		this.auditIdPrivilege = auditIdPrivilege;
	}
}
