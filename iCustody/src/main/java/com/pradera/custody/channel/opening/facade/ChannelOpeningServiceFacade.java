package com.pradera.custody.channel.opening.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.channel.opening.service.ChannelOpeningServiceBean;
import com.pradera.custody.channel.opening.to.ChannelOpeningModelTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.channelopening.ChannelOpening;
import com.pradera.model.custody.channelopening.ChannelOpeningHistory;
import com.pradera.model.custody.channelopening.type.ChannelOpeningStateType;
import com.pradera.model.generalparameter.ParameterTable;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ChannelOpeningServiceFacade {

	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	@EJB
	ChannelOpeningServiceBean channelOpeningServiceBean;
	 
	 /** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	public ChannelOpening findChannelOpening(Long idChannelOpeningPk){
		return channelOpeningServiceBean.find(ChannelOpening.class, idChannelOpeningPk);
	}

	/**
	 * metodo para el cambio de estado de una paertura de canal, tabien se guarda su historico
	 * @param modelTO
	 * @return
	 * @throws ServiceException
	 */
	public ChannelOpening changeStateChannelOpening(ChannelOpeningModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			if(modelTO.getStateChannelOpening().equals(ChannelOpeningStateType.APROBADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
			} else if(modelTO.getStateChannelOpening().equals(ChannelOpeningStateType.ANULADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
			} else if(modelTO.getStateChannelOpening().equals(ChannelOpeningStateType.CONFIRMADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			} else if(modelTO.getStateChannelOpening().equals(ChannelOpeningStateType.RECHAZADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			} else if(modelTO.getStateChannelOpening().equals(ChannelOpeningStateType.VENCIDO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
			}			
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo ChannelOpeningServiceFacade.changeStateChannelOpening. Se procedera a setear las pistas de auditoria manualemnte");
		}
        
        ChannelOpening channelOpening=channelOpeningServiceBean.find(ChannelOpening.class, modelTO.getIdChannelOpeningPk());
        channelOpening.setState(modelTO.getStateChannelOpening());
        
        // si el estado a cambiar es vencido entonces seteamos los que corresponde
        if(modelTO.getStateChannelOpening().equals(ChannelOpeningStateType.VENCIDO.getCode())){
        	channelOpening.setMotiveChannelClosure(modelTO.getMotiveChannelClosureSelected());
        	channelOpening.setObservationChannelClosure(modelTO.getObservationChannelClosure());
        	channelOpening.setFileAttached(modelTO.getFileAttached());
        	channelOpening.setFileName(modelTO.getFileName());
        }
        
        //pistas de auditoria
        channelOpening.setLastModifyUser(modelTO.getAuditUserName());
        channelOpening.setLastModifyDate(new Date());
        channelOpening.setLastModifyIp(modelTO.getAuditIpAddress());
        channelOpening.setLastModifyApp(modelTO.getAuditIdPrivilege());
        
        //preprarando su hisotrico
        ChannelOpeningHistory channelOpeningHis=new ChannelOpeningHistory();
        channelOpeningHis.setIdParticipantFk(modelTO.getIdParticipantSelected());
        channelOpeningHis.setDateChannelOpening(modelTO.getDateChannelOpening());
        channelOpeningHis.setInitialHour(modelTO.getInitialHour());
        channelOpeningHis.setFinalHour(modelTO.getFinalHour());
        channelOpeningHis.setMotiveChannelOpening(modelTO.getMotiveChannelOpeningSelected());
        channelOpeningHis.setObservationChannelOpening(modelTO.getObservationChannelOpening());
        channelOpeningHis.setMotiveChannelClosure(modelTO.getMotiveChannelClosureSelected());
        channelOpeningHis.setObservationChannelClosure(modelTO.getObservationChannelClosure());
        channelOpeningHis.setFileAttached(modelTO.getFileAttached());
        channelOpeningHis.setFileName(modelTO.getFileName());
        channelOpeningHis.setOperation(modelTO.getStateChannelOpening());
        
        channelOpeningHis.setRegistryDate(channelOpening.getRegistryDate());
        channelOpeningHis.setLastModifyUser(channelOpening.getLastModifyUser());
        channelOpeningHis.setLastModifyDate(channelOpening.getLastModifyDate());
        channelOpeningHis.setLastModifyIp(channelOpening.getLastModifyIp());
        channelOpeningHis.setLastModifyApp(channelOpening.getLastModifyApp());
        channelOpeningHis.setIdChannelOpeningFk(channelOpening);
        // para registro en cascada
        List<ChannelOpeningHistory> lstChannelOpHistory=new ArrayList<>();
        lstChannelOpHistory.add(channelOpeningHis);
        channelOpening.setChannelOpeningHistoryList(lstChannelOpHistory);
        
        return channelOpeningServiceBean.updateChannelOpening(channelOpening);
	}
	
	/**
	 * metodo para registarr una apertiura de canal, tabien se guarda su historico
	 * @param modelTO
	 * @return
	 * @throws ServiceException 
	 */
	public ChannelOpening regiterChannelOpening(ChannelOpeningModelTO modelTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());	
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo ChannelOpeningServiceFacade.regiterChannelOpening. Se procedera a setear las pistas de auditoria manualemnte");
		}
        
        ChannelOpening channelOpening=new ChannelOpening();
        channelOpening.setDateChannelOpening(modelTO.getDateChannelOpening());
        channelOpening.setInitialHour(modelTO.getInitialHour());
        channelOpening.setFinalHour(modelTO.getFinalHour());
        channelOpening.setMotiveChannelOpening(modelTO.getMotiveChannelOpeningSelected());
        channelOpening.setObservationChannelOpening(modelTO.getObservationChannelOpening());
        channelOpening.setState(ChannelOpeningStateType.REGISTRADO.getCode());
        channelOpening.setIdParticipantFk(new Participant(modelTO.getIdParticipantSelected()));
        channelOpening.setRegistryDate(new Date());
        
        //pistas de auditoria
        channelOpening.setLastModifyUser(modelTO.getAuditUserName());
        channelOpening.setLastModifyDate(new Date());
        channelOpening.setLastModifyIp(modelTO.getAuditIpAddress());
        channelOpening.setLastModifyApp(modelTO.getAuditIdPrivilege());
        
        //preprarando su hisotrico
        ChannelOpeningHistory channelOpeningHis=new ChannelOpeningHistory();
        channelOpeningHis.setDateChannelOpening(modelTO.getDateChannelOpening());
        channelOpeningHis.setInitialHour(modelTO.getInitialHour());
        channelOpeningHis.setFinalHour(modelTO.getFinalHour());
        channelOpeningHis.setMotiveChannelOpening(modelTO.getMotiveChannelOpeningSelected());
        channelOpeningHis.setObservationChannelOpening(modelTO.getObservationChannelOpening());
        channelOpeningHis.setOperation(ChannelOpeningStateType.REGISTRADO.getCode());
        channelOpeningHis.setIdParticipantFk(modelTO.getIdParticipantSelected());
        channelOpeningHis.setRegistryDate(channelOpening.getRegistryDate());
        channelOpeningHis.setLastModifyUser(channelOpening.getLastModifyUser());
        channelOpeningHis.setLastModifyDate(channelOpening.getLastModifyDate());
        channelOpeningHis.setLastModifyIp(channelOpening.getLastModifyIp());
        channelOpeningHis.setLastModifyApp(channelOpening.getLastModifyApp());
        channelOpeningHis.setIdChannelOpeningFk(channelOpening);
        // para registro en cascada
        List<ChannelOpeningHistory> lstChannelOpHistory=new ArrayList<>();
        lstChannelOpHistory.add(channelOpeningHis);
        channelOpening.setChannelOpeningHistoryList(lstChannelOpHistory);
        
        return channelOpeningServiceBean.regiterChannelOpening(channelOpening);
	}
	
	/**
	 * lista las apertura de canal segun los filtros enviados
	 * @param filter
	 * @return
	 * @throws ServiceException
	 */
	public List<ChannelOpeningModelTO> lstChannelOpeningModelTO(ChannelOpeningModelTO filter) throws ServiceException{
		List<ChannelOpeningModelTO> lst=channelOpeningServiceBean.getLstChannelOpeningTO(filter);

		for (ChannelOpeningModelTO channelOp : lst) {
			// capturando texto motivo
			for (ParameterTable motive : filter.getLstMoviveChannelOpen())
				if (motive.getParameterTablePk().equals(channelOp.getMotiveChannelOpeningSelected())) {
					channelOp.setMotiveChannelOpText(motive.getParameterName());
					break;
				}

			// capturando texto estado
			for (ParameterTable state : filter.getLstState())
				if (state.getParameterTablePk().equals(channelOp.getStateChannelOpening())) {
					channelOp.setStateChannelOpText(state.getParameterName());
					break;
				}
		}
		
		return lst;
	}
}
