package com.pradera.custody.rectificationoperation.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.component.OperationTypeCascade;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.rectification.RectificationDetail;
import com.pradera.model.custody.rectification.RectificationOperation;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class RectificationRequestResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Aug 10, 2013
 */
public class RectificationRequestTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The rectification operation. */
	private RectificationOperation rectificationOperation;
	
	/** The retirement operation. */
	private RetirementOperation retirementOperation;
	
	/** The change owner ship operation. */
	private ChangeOwnershipOperation changeOwnerShipOperation;
	
	/** The security transfer operation. */
	private SecurityTransferOperation securityTransferOperation;
	
	/** The account annotation operation. */
	private AccountAnnotationOperation accountAnnotationOperation;
	
	/** The balance type. */
	private Integer balanceType;
	
	/** The request type. */
	private Integer requestType;
	
	/** The list operation type. */
	private List<OperationTypeCascade> listOperationType;
	
	/** The operation number ref. */
	private Long operationNumberRef;
	
	/** The operation date. */
	private Date operationDate;
	
	/**
	 * Instantiates a new rectification request result to.
	 */
	public RectificationRequestTO() {
		rectificationOperation = new RectificationOperation();
		rectificationOperation.setRectifiedOperation(new CustodyOperation());
		rectificationOperation.setRectificationDetail(new ArrayList<RectificationDetail>());
		rectificationOperation.setCustodyOperation(new CustodyOperation());
	}

	/**
	 * Gets the rectification operation.
	 *
	 * @return the rectification operation
	 */
	public RectificationOperation getRectificationOperation() {
		return rectificationOperation;
	}

	/**
	 * Sets the rectification operation.
	 *
	 * @param rectificationOperation the new rectification operation
	 */
	public void setRectificationOperation(
			RectificationOperation rectificationOperation) {
		this.rectificationOperation = rectificationOperation;
	}

	/**
	 * Gets the change owner ship operation.
	 *
	 * @return the change owner ship operation
	 */
	public ChangeOwnershipOperation getChangeOwnerShipOperation() {
		return changeOwnerShipOperation;
	}

	/**
	 * Sets the change owner ship operation.
	 *
	 * @param changeOwnerShipOperation the new change owner ship operation
	 */
	public void setChangeOwnerShipOperation(
			ChangeOwnershipOperation changeOwnerShipOperation) {
		this.changeOwnerShipOperation = changeOwnerShipOperation;
	}

	/**
	 * Gets the balance type.
	 *
	 * @return the balance type
	 */
	public Integer getBalanceType() {
		return balanceType;
	}

	/**
	 * Sets the balance type.
	 *
	 * @param balanceType the new balance type
	 */
	public void setBalanceType(Integer balanceType) {
		this.balanceType = balanceType;
	}

	/**
	 * Gets the list operation type.
	 *
	 * @return the list operation type
	 */
	public List<OperationTypeCascade> getListOperationType() {
		return listOperationType;
	}

	/**
	 * Sets the list operation type.
	 *
	 * @param listOperationType the new list operation type
	 */
	public void setListOperationType(List<OperationTypeCascade> listOperationType) {
		this.listOperationType = listOperationType;
	}

	/**
	 * Gets the operation number ref.
	 *
	 * @return the operation number ref
	 */
	public Long getOperationNumberRef() {
		return operationNumberRef;
	}

	/**
	 * Sets the operation number ref.
	 *
	 * @param operationNumberRef the new operation number ref
	 */
	public void setOperationNumberRef(Long operationNumberRef) {
		this.operationNumberRef = operationNumberRef;
	}

	/**
	 * Gets the security transfer operation.
	 *
	 * @return the security transfer operation
	 */
	public SecurityTransferOperation getSecurityTransferOperation() {
		return securityTransferOperation;
	}

	/**
	 * Sets the security transfer operation.
	 *
	 * @param securityTransferOperation the new security transfer operation
	 */
	public void setSecurityTransferOperation(
			SecurityTransferOperation securityTransferOperation) {
		this.securityTransferOperation = securityTransferOperation;
	}

	/**
	 * Gets the account annotation operation.
	 *
	 * @return the account annotation operation
	 */
	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}

	/**
	 * Sets the account annotation operation.
	 *
	 * @param accountAnnotationOperation the new account annotation operation
	 */
	public void setAccountAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}

	/**
	 * Gets the retirement operation.
	 *
	 * @return the retirement operation
	 */
	public RetirementOperation getRetirementOperation() {
		return retirementOperation;
	}

	/**
	 * Sets the retirement operation.
	 *
	 * @param retirementOperation the new retirement operation
	 */
	public void setRetirementOperation(RetirementOperation retirementOperation) {
		this.retirementOperation = retirementOperation;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
}
