package com.pradera.custody.rectificationoperation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.rectificationoperation.facade.RectificationOperationFacade;
import com.pradera.custody.rectificationoperation.to.RectificationOperationTO;
import com.pradera.custody.rectificationoperation.to.RectificationRequestTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.rectification.RectificationDetail;
import com.pradera.model.custody.rectification.RectificationOperation;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.custody.type.RectificationMotiveType;
import com.pradera.model.custody.type.RectificationOperationStateType;
import com.pradera.model.custody.type.RectificationOperationType;
import com.pradera.model.custody.type.RectificationRejectMotiveType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RectificationOperationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class RectificationOperationBean extends GenericBaseBean implements Serializable{


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The rectification operation facade. */
	@EJB
	private RectificationOperationFacade rectificationOperationFacade;

	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;

	/** The country residence. */
	@Inject @Configurable 
	private Integer countryResidence;

	/** The general parameters facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/** The rectification operation motive type list. */
	private List<RectificationOperationType> rectificationOperationMotiveTypeList;
	
	/** The disable request number. */
	private boolean disableRequestNumber;
	
	/** The render demamterialization panel. */
	private boolean renderDematerializationPanel;
	
	/** The render retirment panel. */
	private boolean renderRetirementPanel;
	
	/** The transfer available panel. */
	private boolean transferAvailablePanel;
	
	/** The transfer block panel. */
	private boolean transferBlockPanel;
	
	/** The change owner ship panel. */
	private boolean changeOwnerShipPanel;
	
	/** The show motive text. */
	private boolean showMotiveText;
	
	/** The ind disable accept motive reject. */
	private boolean indDisableAcceptMotiveReject;
	
	/** The render other motive. */
	private boolean renderOtherMotive;
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The source r ectification detail. */
	private RectificationDetail rectificationDetail;
	
	/** The list source rectification detail. */
	private List<RectificationDetail> listSourceRectificationDetail;
	
	/** The list target rectification detail. */
	private List<RectificationDetail> listTargetRectificationDetail;
	
	/** The operation type parameters list. */
	private List<ParameterTable> operationTypeParametersList;
	
	/** The rectification operation motive list. */
	private List<ParameterTable> rectificationOperationMotiveList;
	
	/** The rectification state list. */
	private List<ParameterTable> rectificationStateList;
	
	/** The reject motive list. */
	private List<ParameterTable> rejectMotiveList;
	
	/** The rectification operation data model. */
	private GenericDataModel<RectificationOperationTO> rectificationOperationDataModel;
	
	/** The selected rectification operation. */
	private RectificationOperation selectedRectificationOperation;
	
	/** The selected rectification operation to. */
	private RectificationOperationTO selectedRectificationOperationTO;
	
	/** The rectification operation to. */
	private RectificationOperationTO rectificationOperationTO;
	
	/** The render available balance. */
	private boolean renderAvailableBalance;
	
	/** The render block balance. */
	private boolean renderBlockBalance;
	
	/** The requester info. */
	private String requesterInfo;
	
	/** The rectification request to. */
	private RectificationRequestTO rectificationRequestTO;

	/**
	 * Render other motive listener.
	 */
	public void renderOtherMotiveListener(){
		if(isViewOperationRegister()){
			if(rectificationRequestTO.getRectificationOperation().getRequestMotive()!=null){
				if(rectificationRequestTO.getRectificationOperation().getRequestMotive().equals(RectificationMotiveType.OTHERS.getCode())){
					renderOtherMotive=Boolean.TRUE;
				} else {
					renderOtherMotive=Boolean.FALSE;
				}
			} else {
				renderOtherMotive=Boolean.FALSE;
			}
		}
		if(isViewOperationDetail() || isViewOperationConfirm() || isViewOperationReject()){
			if(selectedRectificationOperation.getRequestMotive()!=null){
				if(selectedRectificationOperation.getRequestMotive().equals(RectificationMotiveType.OTHERS.getCode())){
					renderOtherMotive=Boolean.TRUE;
				} else {
					renderOtherMotive=Boolean.FALSE;
				}
			}
		}
	}

	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		if(isViewOperationConfirm()){
			confirmRectificationOperation();
		}
		if(isViewOperationReject()){
			rejectOperationListener();
		}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * On change reject motive.
	 */
	public void onChangeRejectMotive(){
		if(selectedRectificationOperation.getCustodyOperation().getRejectMotive().equals(RectificationRejectMotiveType.OTHERS.getCode())){
			this.setShowMotiveText(Boolean.TRUE);
			this.setIndDisableAcceptMotiveReject(Boolean.TRUE);
		}else{
			this.setIndDisableAcceptMotiveReject(Boolean.FALSE);
			this.setShowMotiveText(Boolean.FALSE);
		}
		selectedRectificationOperation.getCustodyOperation().setRejectMotiveOther(null);	
	}

	/**
	 * On blur other motive reject.
	 */
	public void onBlurOtherMotiveReject(){
		if(Validations.validateIsNotNullAndNotEmpty(
				selectedRectificationOperation.getCustodyOperation().getRejectMotiveOther())){
			setIndDisableAcceptMotiveReject(Boolean.FALSE);
		} else {
			setIndDisableAcceptMotiveReject(Boolean.TRUE); 
		}
	}
	
	
	/**
	 * Before do action.
	 *
	 * @param event the event
	 */
	public void beforeDoAction(ActionEvent event){
		if(isViewOperationConfirm()){
			Object[] bodyData = new Object[1];
			bodyData[0] = selectedRectificationOperation.getCustodyOperation().getOperationNumber().toString();
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_CONFIRM_CONFIRMATION,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");	
		}
		if(isViewOperationReject()){
			selectedRectificationOperation.getCustodyOperation().setRejectMotive(null);
			selectedRectificationOperation.getCustodyOperation().setRejectMotiveOther(GeneralConstants.EMPTY_STRING);
			
			Object[] bodyData = new Object[1];
			bodyData[0] = selectedRectificationOperation.getCustodyOperation().getOperationNumber().toString();
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
					,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_CONFIRM_CONFIRMATION,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show()");
		}
	}
	
	/**
	 * Accept reject motive.
	 */
	public void	acceptRejectMotive(){
		this.hideDialogs();
		Object[] bodydata = new Object[1];
		bodydata[0] = String.valueOf(selectedRectificationOperation.getCustodyOperation().getOperationNumber().toString());
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_COFIRNM_REJECT,bodydata));
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		JSFUtilities.executeJavascriptFunction("PF('widgConfirmActions').show()");
	}

	/**
	 * Reject operation listener.
	 */
	@LoggerAuditWeb
	public void rejectOperationListener(){
		JSFUtilities.hideGeneralDialogues();
		if(isViewOperationReject()){
			if(Validations.validateIsNotNull(selectedRectificationOperation)){
				try {
					rectificationOperationFacade.rejectRectificationOperation(selectedRectificationOperation);
					Object[] bodyData = new Object[1];
					bodyData[0] = String.valueOf(selectedRectificationOperation.getRequestNumber());
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
							,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_REJECTED_CONFIRMATION,bodyData));
					JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");	
					
					// INICIO ENVIO DE NOTIFICACIONES
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.RECTIFICATION_OPERATION_REJECT.getCode());
					Object[] parameters = {	String.valueOf(selectedRectificationOperation.getRequestNumber()) };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					search();
				} catch (ServiceException e) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
					JSFUtilities.executeJavascriptFunction("PF('widgTransactionDialog').show()");	
					search();
					return;
				}
			}
		}
	}

	/**
	 * Confirm rectification operation.
	 */
	@LoggerAuditWeb
	public void confirmRectificationOperation(){
		this.hideDialogs();
		if(isViewOperationConfirm()){
			if(Validations.validateIsNotNull(selectedRectificationOperation)){
				try {
					rectificationOperationFacade.confirmRectificationOperation(selectedRectificationOperation);
					Object[] bodyData = new Object[1];
					bodyData[0] = String.valueOf(selectedRectificationOperation.getRequestNumber());
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
							,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_CONFIRMED_CONFIRMATION,bodyData));
					JSFUtilities.executeJavascriptFunction("widgTransactionDialog.show()");	
					
					// INICIO ENVIO DE NOTIFICACIONES
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM.getCode());
					Object[] parameters = {	String.valueOf(selectedRectificationOperation.getRequestNumber()) };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					search();
				} catch (Exception e) {
					if (e instanceof ServiceException) {
						ServiceException se = (ServiceException) e;
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
								PropertiesUtilities.getExceptionMessage(se.getMessage(),se.getParams()));
						JSFUtilities.executeJavascriptFunction("widgTransactionDialog.show()");	
						return;						
					}
				}
			}
		}		
	}
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
		JSFUtilities.hideGeneralDialogues();		
	}

	/**
	 * Go to confirmation page.
	 *
	 * @return the string
	 */
	
	
	
	public String validateRequestConfirmState(){
		if(Validations.validateIsNotNull(selectedRectificationOperationTO)){
			if(selectedRectificationOperationTO.getState().equals(RectificationOperationStateType.REGISTERED.getCode())){
				if(selectedRectificationOperationTO.getRequestType().equals(RectificationOperationType.CHANGE_OWNERSHIP_AVIALBLE.getCode())){
					changeOwnerShipPanel = Boolean.TRUE;
				}
				else if(selectedRectificationOperationTO.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_AVAILABLE.getCode())){
					transferAvailablePanel = Boolean.TRUE;
				}
				
				setViewOperationType(ViewOperationsType.CONFIRM.getCode());
				loadDetailInformationListener();
				return "detail";	
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_WARNING_OPERATION_CONFIRM_INVALID));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{		
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			
		}
		return "";
	}
	
	/**
	 * Go back listener.
	 *
	 * @return the string
	 */
	public String goBackListener(){
		clearPanels();
		clearTables();
		return "search";
	}
	
	/**
	 * Go to reject page.
	 *
	 * @return the string
	 */
	public String validateRequestRejectState(){
		if(Validations.validateIsNotNull(selectedRectificationOperationTO)){
			if(selectedRectificationOperationTO.getState().equals(RectificationOperationStateType.REGISTERED.getCode())){
				if(selectedRectificationOperationTO.getRequestType().equals(RectificationOperationType.CHANGE_OWNERSHIP_AVIALBLE.getCode())||
						selectedRectificationOperationTO.getRequestType().equals(RectificationOperationType.CHANGE_OWNERCSHIP_BLOCK.getCode())){
					changeOwnerShipPanel = Boolean.TRUE;
				}
				else if(selectedRectificationOperationTO.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_AVAILABLE.getCode()) ||
						selectedRectificationOperationTO.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_BLOCK.getCode())){
					transferAvailablePanel = Boolean.TRUE;
				}
				rejectMotiveList = this.getParmeters(MasterTableType.RECTIFICATION_REJECT_MOTIVE.getCode(), 1);
				setViewOperationType(ViewOperationsType.REJECT.getCode());
				loadDetailInformationListener();
				
				return "detail";	
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_WARNING_OPERATION_REJECT_INVALID));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{		
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}

		return "";
	}
	
	/**
	 * Load detail information listener.
	 */
	public void loadDetailInformationListener(){
		requesterInfo = InstitutionType.get(userInfo.getUserAccountSession().getInstitutionType()).getValue();
		
		try {
			selectedRectificationOperation = rectificationOperationFacade.searchRectificationOperation(selectedRectificationOperationTO.getCustodyOperationPk());
			renderOtherMotiveListener();

			listSourceRectificationDetail = new ArrayList<RectificationDetail>();
			listTargetRectificationDetail = new ArrayList<RectificationDetail>();
			
			renderBlockBalance = false;
			if(selectedRectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_CHANGE_OWNERSHIP_BLOCK.getCode()) || 
					selectedRectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_TRANSFER_BLOCK.getCode()) ||
					selectedRectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode())){
				renderBlockBalance = true;
			}
			
			for(RectificationDetail rect : selectedRectificationOperation.getRectificationDetail()){
				if(rect.getIndSourceDestination().equals(ComponentConstant.SOURCE)){
					listSourceRectificationDetail.add(rect);
				}
				if(rect.getIndSourceDestination().equals(ComponentConstant.TARGET)){
					listTargetRectificationDetail.add(rect);
				}
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Confirm before save.
	 */
	public void beforeSave(){
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
				,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_CONFIRM_SAVE));
		JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmBeforeSave').show()");		
		
	}


	/**
	 * Save.
	 */
	@LoggerAuditWeb
	public void save(){
		if(isViewOperationRegister()){
			RectificationOperation rectificationOperation = rectificationRequestTO.getRectificationOperation();
			if(Validations.validateIsNotNull(rectificationOperation)){
				Object[] bodyData = new Object[1];
				
				try {
					rectificationOperationFacade.registerRectificationOperation(rectificationOperation);
					bodyData[0] = String.valueOf(rectificationOperation.getRequestNumber());
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
							,PropertiesUtilities.getMessage(PropertiesConstants.RECTIFICATION_CONFIRMED_SAVE,bodyData));
					//JSFUtilities.executeJavascriptFunction("cnfwConfirmedBeforeSave.show()");
					JSFUtilities.executeJavascriptFunction("PF('cnfwConfirmedBeforeSave').show()");
					// INICIO ENVIO DE NOTIFICACIONES
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.RECTIFICATION_OPERATION_REGISTER.getCode());
					Object[] parameters = {	String.valueOf(rectificationOperation.getRequestNumber()) };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					search();
					
				} catch (ServiceException e) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
					JSFUtilities.showSimpleValidationDialog();		
				}
			}
		}

	}
	
	/**
	 * Initialize data.
	 */
	public void initializeData(){
		rectificationDetail =new RectificationDetail();
		
		try {
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			participantList = accountsFacade.getLisParticipantServiceBean(filterPar);
		} catch (ServiceException se) {
 			excepcion.fire(new ExceptionToCatchEvent(se));
		}
		rectificationOperationMotiveList = this.getParmeters(MasterTableType.RECTIFICATION_MOTIVE.getCode(), BooleanType.YES.getCode());
	}

 	/**
	  * Load register information listener.
	  */
	 public void loadRegisterInformationListener(){
		requesterInfo  = GeneralConstants.EMPTY_STRING;
		
		rectificationRequestTO = new RectificationRequestTO();
		rectificationRequestTO.setOperationDate(CommonsUtilities.currentDate());
		
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		
		disableRequestNumber=Boolean.TRUE;
		renderAvailableBalance = false;
		renderOtherMotive = false;
		renderBlockBalance = false;
		renderDematerializationPanel = false;
		renderRetirementPanel = false;
		showMotiveText = false;
		
		clearPanels();
		
		rectificationOperationMotiveList = this.getParmeters(MasterTableType.RECTIFICATION_MOTIVE.getCode(), BooleanType.YES.getCode());
		List<Integer> lstParamTabPkNotIn = new ArrayList<>();
		lstParamTabPkNotIn.add(RectificationOperationType.TRANSFER_SECURITIE_BLOCK.getCode());
		lstParamTabPkNotIn.add(RectificationOperationType.CHANGE_OWNERCSHIP_BLOCK.getCode());
		operationTypeParametersList = this.getParmetersDepreciated(MasterTableType.RECTIFICATION_OPERATION_TYPE.getCode(),BooleanType.YES.getCode(),lstParamTabPkNotIn);
	}


	/**
	 * Gets the parmeters.
	 *
	 * @param master the master
	 * @param state the state
	 * @return the parmeters
	 */
	public List<ParameterTable> getParmeters(Integer master, Integer state){
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(master);
		filter.setState(state);
		try {
			return generalParametersFacade.getListParameterTableServiceBean(filter);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return new ArrayList<ParameterTable>();
	}

	/**
	 * Clear panels.
	 */
	public void clearPanels(){
		renderDematerializationPanel = Boolean.FALSE;
		renderRetirementPanel=Boolean.FALSE;
		transferAvailablePanel=Boolean.FALSE;
		changeOwnerShipPanel=Boolean.FALSE;
		transferBlockPanel=Boolean.FALSE;
	}


	/**
	 * Operation type change listener.
	 */
	public void onChangeOperationTypeListener(){
		if(isViewOperationRegister()){
			clearPanels();
			rectificationRequestTO.getRectificationOperation().setRectifiedOperation(null);
			rectificationRequestTO.setOperationNumberRef(null);
			
			if(Validations.validateIsNullOrEmpty(rectificationRequestTO.getRequestType())){
				disableRequestNumber=Boolean.TRUE;
			} else {
				disableRequestNumber = Boolean.FALSE;
			}
			
		}		
	}
	
	/**
	 * Load detail info listener.
	 *
	 * @param rectificationOperationTO the rectification operation to
	 * @return the string
	 */
	public String loadDetailView(RectificationOperationTO rectificationOperationTO){
		selectedRectificationOperationTO = rectificationOperationTO;
		setViewOperationType(ViewOperationsType.DETAIL.getCode());
		clearPanels();
		loadDetailInformationListener();
		return "detail";
	}
	
	/**
	 * Show security detail.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityDetail(String securityCode){
		securityHelpBean.setSecurityCode(securityCode);
		securityHelpBean.setName("securityHelp");
		securityHelpBean.searchSecurityHandler();
	}
	
	/**
	 * Find holder account balance entity.
	 *
	 * @param security the security
	 * @param participant the participant
	 * @param holderAccount the holder account
	 * @return the holder account balance
	 */
	private HolderAccountBalance findHolderAccountBalanceEntity(Security security, Participant participant, HolderAccount holderAccount){
		HolderAccountBalance returnHolderAccountBalance = new HolderAccountBalance();
		HolderAccountBalanceTO filter = new HolderAccountBalanceTO();
		filter.setIdSecurityCode(security.getIdSecurityCodePk());
		filter.setIdParticipant(participant.getIdParticipantPk());
		filter.setIdHolderAccount(holderAccount.getIdHolderAccountPk());
		returnHolderAccountBalance.setSecurity(security);
		returnHolderAccountBalance.setParticipant(participant);
		returnHolderAccountBalance.setHolderAccount(holderAccount);
		try {
			HolderAccountBalance temp = accountsFacade.holderAccountBalanceServiceBean(filter);
			if(temp!=null){
				if(temp.getAvailableBalance()!=null){
					returnHolderAccountBalance.setAvailableBalance(temp.getAvailableBalance());			
				}
				if(temp.getTotalBalance()!=null){
					returnHolderAccountBalance.setTotalBalance(temp.getTotalBalance());
				}
				if(temp.getBanBalance()!=null){
					returnHolderAccountBalance.setBanBalance(temp.getBanBalance());
				}
				if(temp.getPawnBalance()!=null){
					returnHolderAccountBalance.setPawnBalance(temp.getPawnBalance());
				}
				if(temp.getOtherBlockBalance()!=null){
					returnHolderAccountBalance.setOtherBlockBalance(temp.getOtherBlockBalance());
				}
			}
			return returnHolderAccountBalance;
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

		return null;
	}

	/**
	 * Find custody operation listener.
	 */
	@LoggerAuditWeb
	public void onChangeCustodyOperationNumber(){
		rectificationRequestTO.setAccountAnnotationOperation(null);
		rectificationRequestTO.setChangeOwnerShipOperation(null);
		rectificationRequestTO.setRetirementOperation(null);
		rectificationRequestTO.setSecurityTransferOperation(null);
		try{
			
			if(Validations.validateIsNotNullAndNotEmpty(rectificationRequestTO.getOperationNumberRef())){	
				
				rectificationOperationFacade.validateExistPreviousRequest(rectificationRequestTO.getRequestType(), rectificationRequestTO.getOperationNumberRef());
				
				requesterInfo = InstitutionType.get(userInfo.getUserAccountSession().getInstitutionType()).getValue();
				
				rectificationRequestTO.setListOperationType(rectificationOperationFacade.getOperationTypeCustodyByRectificationType(rectificationRequestTO.getRequestType()));
				
				clearTables();
				this.clearPanels();
				disableRequestNumber=Boolean.FALSE;
				
				if(rectificationRequestTO.getRequestType().equals(RectificationOperationType.DESMATERIALIZATION_CERTIFICATE.getCode())){	
					rectificationOperationFacade.getAccountAnnotationAndBuildRectification(rectificationRequestTO);
					renderAvailableBalance = Boolean.TRUE;						
					renderDematerializationPanel=Boolean.TRUE;
				}
				else if(rectificationRequestTO.getRequestType().equals(RectificationOperationType.CHANGE_OWNERSHIP_AVIALBLE.getCode())){
					rectificationRequestTO.setBalanceType(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode());
					rectificationOperationFacade.getChangeOwnershipAndBuildRectification(rectificationRequestTO);
					renderAvailableBalance=Boolean.TRUE;	
					changeOwnerShipPanel=Boolean.TRUE;
				}
				else if(rectificationRequestTO.getRequestType().equals(RectificationOperationType.CHANGE_OWNERCSHIP_BLOCK.getCode())){
					rectificationRequestTO.setBalanceType(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode());
					rectificationOperationFacade.getChangeOwnershipAndBuildRectification(rectificationRequestTO);
					renderBlockBalance=Boolean.TRUE;
					changeOwnerShipPanel=Boolean.TRUE;
				}
				else if(rectificationRequestTO.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_AVAILABLE.getCode())){
					rectificationRequestTO.setBalanceType(ComponentConstant.ZERO);
					rectificationOperationFacade.getSecuritiesTransferAndBuildRectification(rectificationRequestTO);
					renderAvailableBalance=Boolean.TRUE;
					transferAvailablePanel=Boolean.TRUE;
				}
				else if(rectificationRequestTO.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_BLOCK.getCode())){
					rectificationRequestTO.setBalanceType(ComponentConstant.ONE);
					rectificationOperationFacade.getSecuritiesTransferAndBuildRectification(rectificationRequestTO);
					renderBlockBalance=Boolean.TRUE;
					transferBlockPanel=Boolean.TRUE;
				}
				else if(rectificationRequestTO.getRequestType().equals(RectificationOperationType.SECURITIES_RETIREMENT.getCode())){
					rectificationOperationFacade.getRetirementOperationAndBuildRectification(rectificationRequestTO);
					renderAvailableBalance=Boolean.TRUE;
					renderRetirementPanel=Boolean.TRUE;
				}
				
			}
		
		} catch (ServiceException e) {
			rectificationRequestTO.setListOperationType(null);
			rectificationRequestTO.getRectificationOperation().setRectifiedOperation(null);
			rectificationRequestTO.setOperationNumberRef(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
			
	}

	/**
	 * Clear tables.
	 */
	public void clearTables(){
		renderBlockBalance = Boolean.FALSE;
		renderAvailableBalance = Boolean.FALSE;
	}

	/**
	 * Sets the true valid buttons.
	 */
	public void setTrueValidButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Search.
	 */
	public void search(){
		selectedRectificationOperationTO = null;
		setViewOperationType(ViewOperationsType.CONSULT.getCode());

		try {
			List<RectificationOperationTO> list = rectificationOperationFacade.searchRectificationOperations(rectificationOperationTO);
			rectificationOperationDataModel = new GenericDataModel<RectificationOperationTO>(list);
			
			setTrueValidButtons();

		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			e.printStackTrace();
		}
	}

	/**
	 * Alert.
	 *
	 * @param message the message
	 * @param requestNumber the request number
	 */
	public void alert(String message, Long requestNumber){
		Object[] data = new Object[1];
		data[0] =String.valueOf(requestNumber);	 
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				PropertiesUtilities.getMessage(message,data));
		JSFUtilities.showSimpleValidationDialog();
		
	}
	
	/**
	 * Alert.
	 *
	 * @param message the message
	 */
	public void alert(String message){		
		showMessageOnDialog("Mensaje de Advertencia", message);
		JSFUtilities.showSimpleValidationDialog();	
	}

	/**
	 * Clear register.
	 */
	public void clearRegister(){
		JSFUtilities.hideGeneralDialogues();	
		this.hideDialogs();
		this.loadRegisterInformationListener();
		resetComponents();
	}

	/**
	 * Clear search.
	 */
	public void clearSearch(){
		init();				
		resetComponents();		
		rectificationOperationDataModel = null;
	}
	
	/**
	 * Reset components.
	 */
	public void resetComponents(){
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Reset components.
	 */
	public void validateBack(){
		
		if(selectedRectificationOperation.getCustodyOperation().getRejectMotive()!=null){
		
				if (selectedRectificationOperation.getCustodyOperation().getRejectMotive().equals(RectificationRejectMotiveType.OTHERS.getCode())) {
				
					if(selectedRectificationOperation.getCustodyOperation().getRejectMotiveOther()!=null && !selectedRectificationOperation.getCustodyOperation().getRejectMotiveOther().trim().equals(GeneralConstants.EMPTY_STRING)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
						JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
					}
					else{
						resetComponents();
						JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
					}
					      
				}
				else{
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
					JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
				}
			
		}else{
			resetComponents();
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		}
		
		
	}


	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){	
		rectificationOperationTO = new RectificationOperationTO();
		rectificationOperationTO.setInitialDate(CommonsUtilities.currentDate());
		rectificationOperationTO.setEndDate(CommonsUtilities.currentDate());
		
		try {
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			participantList = accountsFacade.getLisParticipantServiceBean(filterPar);
			
			rectificationOperationMotiveList = getParmeters(MasterTableType.RECTIFICATION_MOTIVE.getCode(), BooleanType.YES.getCode());
			List<Integer> lstParamTabPkNotIn = new ArrayList<>();
			lstParamTabPkNotIn.add(RectificationOperationType.TRANSFER_SECURITIE_BLOCK.getCode());
			lstParamTabPkNotIn.add(RectificationOperationType.CHANGE_OWNERCSHIP_BLOCK.getCode());
			operationTypeParametersList= getParmetersDepreciated(MasterTableType.RECTIFICATION_OPERATION_TYPE.getCode(), BooleanType.YES.getCode(),lstParamTabPkNotIn);
			rectificationStateList = getParmeters(MasterTableType.RECTIFICATION_OPERATION_STATE.getCode(), BooleanType.YES.getCode());
			
			ParameterTableTO filter = new ParameterTableTO();
			filter.setLstMasterTableFk(new ArrayList<Integer>());
			filter.getLstMasterTableFk().add(MasterTableType.LEVEL_AFFECTATION.getCode());
			filter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
			filter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_REDEMPTION_TYPE.getCode());
			List<ParameterTable> lstAffectations = generalParametersFacade.getListParameterTableServiceBean(filter);
			for (ParameterTable parameterTable : lstAffectations) {
				getParametersTableMap().put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
			}
			
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
		
		try {
			this.listHolidays();
		} catch (ServiceException e) {
 			e.printStackTrace();
		}

	}

	/**
	 * Instantiates a new rectification operation bean.
	 */
	public RectificationOperationBean(){
		rectificationOperationTO = new RectificationOperationTO();
		
	}
	
	/**
	 * Gets the rectification operation motive type list.
	 *
	 * @return the rectification operation motive type list
	 */
	public List<RectificationOperationType> getRectificationOperationMotiveTypeList() {
		return rectificationOperationMotiveTypeList;
	}

	/**
	 * Sets the rectification operation motive type list.
	 *
	 * @param rectificationOperationMotiveTypeList the new rectification operation motive type list
	 */
	public void setRectificationOperationMotiveTypeList(
			List<RectificationOperationType> rectificationOperationMotiveTypeList) {
		this.rectificationOperationMotiveTypeList = rectificationOperationMotiveTypeList;
	}


	/**
	 * Checks if is disable request number.
	 *
	 * @return true, if is disable request number
	 */
	public boolean isDisableRequestNumber() {
		return disableRequestNumber;
	}

	/**
	 * Sets the disable request number.
	 *
	 * @param disableRequestNumber the new disable request number
	 */
	public void setDisableRequestNumber(boolean disableRequestNumber) {
		this.disableRequestNumber = disableRequestNumber;
	}

	/**
	 * Checks if is render demamterialization panel.
	 *
	 * @return true, if is render demamterialization panel
	 */
	public boolean isRenderDematerializationPanel() {
		return renderDematerializationPanel;
	}

	/**
	 * Sets the render demamterialization panel.
	 *
	 * @param renderDematerializationPanel the new render dematerialization panel
	 */
	public void setRenderDematerializationPanel(
			boolean renderDematerializationPanel) {
		this.renderDematerializationPanel = renderDematerializationPanel;
	}
	
	/**
	 * Checks if is render retirment panel.
	 *
	 * @return true, if is render retirment panel
	 */
	public boolean isRenderRetirementPanel() {
		return renderRetirementPanel;
	}
	
	/**
	 * Sets the render retirment panel.
	 *
	 * @param renderRetirementPanel the new render retirement panel
	 */
	public void setRenderRetirementPanel(boolean renderRetirementPanel) {
		this.renderRetirementPanel = renderRetirementPanel;
	}
	
	/**
	 * Gets the transfer available panel.
	 *
	 * @return the transfer available panel
	 */
	public boolean getTransferAvailablePanel() {
		return transferAvailablePanel;
	}
	
	/**
	 * Sets the transfer available panel.
	 *
	 * @param transferAvailablePanel the new transfer available panel
	 */
	public void setTransferAvailablePanel(boolean transferAvailablePanel) {
		this.transferAvailablePanel = transferAvailablePanel;
	}

	/**
	 * Gets the operation type parameters list.
	 *
	 * @return the operation type parameters list
	 */
	public List<ParameterTable> getOperationTypeParametersList() {
		return operationTypeParametersList;
	}
	
	/**
	 * Sets the operation type parameters list.
	 *
	 * @param operationTypeParametersList the new operation type parameters list
	 */
	public void setOperationTypeParametersList(
			List<ParameterTable> operationTypeParametersList) {
		this.operationTypeParametersList = operationTypeParametersList;
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}
	
	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the selected rectification operation.
	 *
	 * @return the selected rectification operation
	 */
	public RectificationOperation getSelectedRectificationOperation() {
		return selectedRectificationOperation;
	}
	
	/**
	 * Sets the selected rectification operation.
	 *
	 * @param selectedRectificationOperation the new selected rectification operation
	 */
	public void setSelectedRectificationOperation(
			RectificationOperation selectedRectificationOperation) {
		this.selectedRectificationOperation = selectedRectificationOperation;
	}
	

	
	/**
	 * Gets the rectification operation data model.
	 *
	 * @return the rectification operation data model
	 */
	public GenericDataModel<RectificationOperationTO> getRectificationOperationDataModel() {
		return rectificationOperationDataModel;
	}

	/**
	 * Sets the rectification operation data model.
	 *
	 * @param rectificationOperationDataModel the new rectification operation data model
	 */
	public void setRectificationOperationDataModel(
			GenericDataModel<RectificationOperationTO> rectificationOperationDataModel) {
		this.rectificationOperationDataModel = rectificationOperationDataModel;
	}

	/**
	 * Checks if is change owner ship panel.
	 *
	 * @return true, if is change owner ship panel
	 */
	public boolean isChangeOwnerShipPanel() {
		return changeOwnerShipPanel;
	}
	
	/**
	 * Sets the change owner ship panel.
	 *
	 * @param changeOwnerShipPanel the new change owner ship panel
	 */
	public void setChangeOwnerShipPanel(boolean changeOwnerShipPanel) {
		this.changeOwnerShipPanel = changeOwnerShipPanel;
	}

	
	/**
	 * Gets the rectification operation to.
	 *
	 * @return the rectification operation to
	 */
	public RectificationOperationTO getRectificationOperationTO() {
		return rectificationOperationTO;
	}
	
	/**
	 * Sets the rectification operation to.
	 *
	 * @param rectificationOperationTO the new rectification operation to
	 */
	public void setRectificationOperationTO(
			RectificationOperationTO rectificationOperationTO) {
		this.rectificationOperationTO = rectificationOperationTO;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	/**
	 * Gets the render available balance.
	 *
	 * @return the render available balance
	 */
	public boolean getRenderAvailableBalance() {
		return renderAvailableBalance;
	}
	
	/**
	 * Sets the render available balance.
	 *
	 * @param renderAvailableBalance the new render available balance
	 */
	public void setRenderAvailableBalance(boolean renderAvailableBalance) {
		this.renderAvailableBalance = renderAvailableBalance;
	}

	/**
	 * Gets the render block balance.
	 *
	 * @return the render block balance
	 */
	public boolean getRenderBlockBalance() {
		return renderBlockBalance;
	}

	/**
	 * Sets the render block balance.
	 *
	 * @param renderBlockBalance the new render block balance
	 */
	public void setRenderBlockBalance(boolean renderBlockBalance) {
		this.renderBlockBalance = renderBlockBalance;
	}	

	/**
	 * Checks if is render other motive.
	 *
	 * @return true, if is render other motive
	 */
	public boolean isRenderOtherMotive() {
		return renderOtherMotive;
	}

	/**
	 * Sets the render other motive.
	 *
	 * @param renderOtherMotive the new render other motive
	 */
	public void setRenderOtherMotive(boolean renderOtherMotive) {
		this.renderOtherMotive = renderOtherMotive;
	}

	/**
	 * Checks if is ind disable accept motive reject.
	 *
	 * @return true, if is ind disable accept motive reject
	 */
	public boolean isIndDisableAcceptMotiveReject() {
		return indDisableAcceptMotiveReject;
	}
	
	/**
	 * Sets the ind disable accept motive reject.
	 *
	 * @param indDisableAcceptMotiveReject the new ind disable accept motive reject
	 */
	public void setIndDisableAcceptMotiveReject(boolean indDisableAcceptMotiveReject) {
		this.indDisableAcceptMotiveReject = indDisableAcceptMotiveReject;
	}
	
	/**
	 * Gets the rectification detail.
	 *
	 * @return the rectification detail
	 */
	public RectificationDetail getRectificationDetail() {
		return rectificationDetail;
	}

	/**
	 * Sets the rectification detail.
	 *
	 * @param rectificationDetail the new rectification detail
	 */
	public void setRectificationDetail(RectificationDetail rectificationDetail) {
		this.rectificationDetail = rectificationDetail;
	}

	/**
	 * Gets the reject motive list.
	 *
	 * @return the reject motive list
	 */
	public List<ParameterTable> getRejectMotiveList() {
		return rejectMotiveList;
	}
	
	/**
	 * Sets the reject motive list.
	 *
	 * @param rejectMotiveList the new reject motive list
	 */
	public void setRejectMotiveList(List<ParameterTable> rejectMotiveList) {
		this.rejectMotiveList = rejectMotiveList;
	}
	
	/**
	 * Gets the rectification operation motive list.
	 *
	 * @return the rectification operation motive list
	 */
	public List<ParameterTable> getRectificationOperationMotiveList() {
		return rectificationOperationMotiveList;
	}

	/**
	 * Sets the rectification operation motive list.
	 *
	 * @param rectificationOperationMotiveList the new rectification operation motive list
	 */
	public void setRectificationOperationMotiveList(
			List<ParameterTable> rectificationOperationMotiveList) {
		this.rectificationOperationMotiveList = rectificationOperationMotiveList;
	}

	/**
	 * Checks if is show motive text.
	 *
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	/**
	 * Gets the rectification state list.
	 *
	 * @return the rectification state list
	 */
	public List<ParameterTable> getRectificationStateList() {
		return rectificationStateList;
	}

	/**
	 * Sets the rectification state list.
	 *
	 * @param rectificationStateList the new rectification state list
	 */
	public void setRectificationStateList(
			List<ParameterTable> rectificationStateList) {
		this.rectificationStateList = rectificationStateList;
	}
	
	/**
	 * Sets the show motive text.
	 *
	 * @param showMotiveText the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	/**
	 * Gets the requester info.
	 *
	 * @return the requester info
	 */
	public String getRequesterInfo() {
		return requesterInfo;
	}

	/**
	 * Sets the requester info.
	 *
	 * @param requesterInfo the new requester info
	 */
	public void setRequesterInfo(String requesterInfo) {
		this.requesterInfo = requesterInfo;
	}

	/**
	 * Gets the list source rectification detail.
	 *
	 * @return the list source rectification detail
	 */
	public List<RectificationDetail> getListSourceRectificationDetail() {
		return listSourceRectificationDetail;
	}

	/**
	 * Sets the list source rectification detail.
	 *
	 * @param listSourceRectificationDetail the new list source rectification detail
	 */
	public void setListSourceRectificationDetail(
			List<RectificationDetail> listSourceRectificationDetail) {
		this.listSourceRectificationDetail = listSourceRectificationDetail;
	}

	/**
	 * Gets the list target rectification detail.
	 *
	 * @return the list target rectification detail
	 */
	public List<RectificationDetail> getListTargetRectificationDetail() {
		return listTargetRectificationDetail;
	}

	/**
	 * Sets the list target rectification detail.
	 *
	 * @param listTargetRectificationDetail the new list target rectification detail
	 */
	public void setListTargetRectificationDetail(
			List<RectificationDetail> listTargetRectificationDetail) {
		this.listTargetRectificationDetail = listTargetRectificationDetail;
	}

	/**
	 * Checks if is transfer block panel.
	 *
	 * @return true, if is transfer block panel
	 */
	public boolean isTransferBlockPanel() {
		return transferBlockPanel;
	}

	/**
	 * Sets the transfer block panel.
	 *
	 * @param transferBlockPanel the new transfer block panel
	 */
	public void setTransferBlockPanel(boolean transferBlockPanel) {
		this.transferBlockPanel = transferBlockPanel;
	}

	/**
	 * Gets the rectification request to.
	 *
	 * @return the rectification request to
	 */
	public RectificationRequestTO getRectificationRequestTO() {
		return rectificationRequestTO;
	}

	/**
	 * Sets the rectification request to.
	 *
	 * @param rectificationRequestTO the new rectification request to
	 */
	public void setRectificationRequestTO(
			RectificationRequestTO rectificationRequestTO) {
		this.rectificationRequestTO = rectificationRequestTO;
	}

	/**
	 * Gets the selected rectification operation to.
	 *
	 * @return the selected rectification operation to
	 */
	public RectificationOperationTO getSelectedRectificationOperationTO() {
		return selectedRectificationOperationTO;
	}

	/**
	 * Sets the selected rectification operation to.
	 *
	 * @param selectedRectificationOperationTO the new selected rectification operation to
	 */
	public void setSelectedRectificationOperationTO(
			RectificationOperationTO selectedRectificationOperationTO) {
		this.selectedRectificationOperationTO = selectedRectificationOperationTO;
	}

	/**
	 * Gets the parmeters depreciated.
	 *
	 * @param master the master
	 * @param state the state
	 * @return the parmeters
	 */
	public List<ParameterTable> getParmetersDepreciated(Integer master, Integer state,List<Integer> lstParamTabPkNotIn){
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(master);
		filter.setState(state);
		filter.setLstParameterTablePkNotIn(lstParamTabPkNotIn);
		try {
			return generalParametersFacade.getListParameterTableServiceBean(filter);
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return new ArrayList<ParameterTable>();
	}
}
