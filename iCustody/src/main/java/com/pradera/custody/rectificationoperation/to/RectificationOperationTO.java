package com.pradera.custody.rectificationoperation.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RectificationOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class RectificationOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id security code. */
	private String idSecurityCode;
	
	/** The holder. */
	private Holder holder;
	
	/** The need detail. */
	private Boolean needDetail = Boolean.FALSE;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The state. */
	private Integer state;
	
	/** The state list. */
	private List<Integer> stateList;
	
	/** The custody operation pk. */
	private Long custodyOperationPk;
	
	/** The rectified operation pk. */
	private Long rectifiedOperationPk;
	
	/** The rectified operation number. */
	private Long rectifiedOperationNumber;
	
	/** The motive. */
	private Integer motive;
	
	/** The request type. */
	private Integer requestType;
	
	/** The motive description. */
	private String motiveDescription;
	
	/** The request type description. */
	private String requestTypeDescription;
	
	/** The state description. */
	private String stateDescription;
	
	/** The registry date. */
	private Date registryDate;
	

	/**
	 * Gets the state list.
	 *
	 * @return the state list
	 */
	public List<Integer> getStateList() {
		return stateList;
	}

	/**
	 * Sets the state list.
	 *
	 * @param stateList the new state list
	 */
	public void setStateList(List<Integer> stateList) {
		this.stateList = stateList;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}


	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}

	
	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the need detail.
	 *
	 * @return the need detail
	 */
	public Boolean getNeedDetail() {
		return needDetail;
	}

	/**
	 * Sets the need detail.
	 *
	 * @param needDetail the new need detail
	 */
	public void setNeedDetail(Boolean needDetail) {
		this.needDetail = needDetail;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	
	/**
	 * Gets the custody operation pk.
	 *
	 * @return the custody operation pk
	 */
	public Long getCustodyOperationPk() {
		return custodyOperationPk;
	}

	/**
	 * Sets the custody operation pk.
	 *
	 * @param custodyOperationPk the new custody operation pk
	 */
	public void setCustodyOperationPk(Long custodyOperationPk) {
		this.custodyOperationPk = custodyOperationPk;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}

	/**
	 * Gets the rectified operation pk.
	 *
	 * @return the rectified operation pk
	 */
	public Long getRectifiedOperationPk() {
		return rectifiedOperationPk;
	}

	/**
	 * Sets the rectified operation pk.
	 *
	 * @param rectifiedOperationPk the new rectified operation pk
	 */
	public void setRectifiedOperationPk(Long rectifiedOperationPk) {
		this.rectifiedOperationPk = rectifiedOperationPk;
	}

	/**
	 * Gets the rectified operation number.
	 *
	 * @return the rectified operation number
	 */
	public Long getRectifiedOperationNumber() {
		return rectifiedOperationNumber;
	}

	/**
	 * Sets the rectified operation number.
	 *
	 * @param rectifiedOperationNumber the new rectified operation number
	 */
	public void setRectifiedOperationNumber(Long rectifiedOperationNumber) {
		this.rectifiedOperationNumber = rectifiedOperationNumber;
	}

	/**
	 * Gets the motive description.
	 *
	 * @return the motive description
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}

	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the new motive description
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	/**
	 * Gets the request type description.
	 *
	 * @return the request type description
	 */
	public String getRequestTypeDescription() {
		return requestTypeDescription;
	}

	/**
	 * Sets the request type description.
	 *
	 * @param requestTypeDescription the new request type description
	 */
	public void setRequestTypeDescription(String requestTypeDescription) {
		this.requestTypeDescription = requestTypeDescription;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	
	
	

}
