package com.pradera.custody.rectificationoperation.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.ownershipexchange.facade.ChangeOwnershipServiceFacade;
import com.pradera.custody.rectificationoperation.to.RectificationOperationTO;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.tranfersecurities.to.SecurityTransferOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.OperationType;
import com.pradera.model.component.OperationTypeCascade;
import com.pradera.model.custody.rectification.RectificationDetail;
import com.pradera.model.custody.rectification.RectificationMarketFact;
import com.pradera.model.custody.rectification.RectificationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.custody.type.CustodyOperationStateType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RectificationOperationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
/*
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RectificationOperationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
public class RectificationOperationServiceBean extends CrudDaoServiceBean {


	/** The request reversals service bean. */
	@EJB
	RequestReversalsServiceBean requestReversalsServiceBean;

	/** The request affectation service bean. */
	@EJB
	private RequestAffectationServiceBean requestAffectationServiceBean;

	/** The affectations service bean. */
	@EJB
	private AffectationsServiceBean affectationsServiceBean;

	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;

	
	/** The change ownership service facade. */
	@Inject
	private ChangeOwnershipServiceFacade changeOwnershipServiceFacade;	 


	/**
	 * Search security trasnfer operation.
	 *
	 * @param filter the filter
	 * @return the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public SecurityTransferOperation searchSecurityTrasnferOperation(SecurityTransferOperationTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT sto FROM SecurityTransferOperation sto ");
		sbQuery.append("  inner join fetch sto.custodyOperation co");
		sbQuery.append("  inner join fetch sto.sourceParticipant ");
		sbQuery.append("  inner join fetch sto.targetParticipant ");
		sbQuery.append("  inner join fetch sto.securities ");
		sbQuery.append("  left join sto.blockOperationTransfer ");
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getOperationNumber())){
			sbQuery.append(" where co.operationNumber=:operationNumber");
			parameters.put("operationNumber", filter.getOperationNumber());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getListOperationTypePk())){
			sbQuery.append(" and co.operationType in :operationType");
			parameters.put("operationType", filter.getListOperationTypePk());
		}

		if(Validations.validateIsNotNullAndNotEmpty(filter.getBalanceType())){
			sbQuery.append(" and sto.indBlockTransfer = :indBalance");
			parameters.put("indBalance",filter.getBalanceType());
		}
		
		if(Validations.validateIsNotNull(filter.getOperationDate())){
			sbQuery.append(" and trunc(co.registryDate) = :operationdate");
			parameters.put("operationdate",filter.getOperationDate());
		}
		
		return (SecurityTransferOperation) findObjectByQueryString(sbQuery.toString(), parameters);

	}

	
	/**
	 * Search retirement operation by operation number and request type.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the retirement operation
	 */
	public RetirementOperation searchRetirementOperationByOperationNumberAndRequestType(RetirementOperationTO retirementOperationTO){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("SELECT ro FROM RetirementOperation ro join fetch ro.custodyOperation co");
		sbQuery.append(" where 1 = 1");
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getOperationNumber())){
			sbQuery.append(" and co.operationNumber=:operationNumber");
			parameters.put("operationNumber", retirementOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getLstOperationType())){
			sbQuery.append(" and co.operationType in :operationType");
			parameters.put("operationType", retirementOperationTO.getLstOperationType());
		}
		
		if(Validations.validateIsNotNull(retirementOperationTO.getOperationDate())){
			sbQuery.append(" and trunc(co.registryDate) = :operationDate");
			parameters.put("operationDate", retirementOperationTO.getOperationDate());
		}
		 		
		return (RetirementOperation) findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Search rectification operations.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RectificationOperationTO> searchRectificationOperations(RectificationOperationTO filter)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT ro.idRectificationOperationPk, ");
		sbQuery.append("  	    co.operationNumber, ");
		sbQuery.append("	    ro.requestMotive, ");
		sbQuery.append("	    ( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.requestMotive ), ");
		sbQuery.append("	    co.registryDate, ");
		sbQuery.append("	    ro.requestType, ");
		sbQuery.append("	    ( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.requestType ), ");
		sbQuery.append("	    ro.requestState, ");
		sbQuery.append("	    ( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.requestState ), ");
		sbQuery.append("	    rop.operationNumber, ");
		sbQuery.append("	    rop.idCustodyOperationPk "); // 10
		sbQuery.append(" From  RectificationOperation ro ");	
		sbQuery.append(" inner join ro.custodyOperation co");
		sbQuery.append(" inner join ro.rectifiedOperation rop ");
		sbQuery.append(" where trunc(co.registryDate) between :initalDate and :endDate ");
		
		parameters.put("initalDate", filter.getInitialDate());		
		parameters.put("endDate", filter.getEndDate());		
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIdParticipantPk())){			
			sbQuery.append(" and exists ( ");
			sbQuery.append("  select rd.participant.idParticipantPk from RectificationDetail rd where  ");
			sbQuery.append("  rd.rectificationOperation.idRectificationOperationPk  = ro.idRectificationOperationPk and ");
			sbQuery.append("  rd.participant.idParticipantPk = :idParticipant ");
			sbQuery.append(" ) ");
			parameters.put("idParticipant", filter.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getRequestNumber())){
			sbQuery.append(" and co.operationNumber =:requestNumber");
			parameters.put("requestNumber", filter.getRequestNumber());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getRequestType())){
			sbQuery.append(" and ro.requestType=:requestType");
			parameters.put("requestType", filter.getRequestType());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getMotive())){
			sbQuery.append(" and ro.requestMotive=:motive");
			parameters.put("motive", filter.getMotive());
		}	
		
		if(Validations.validateIsNotNullAndNotEmpty(filter.getState())){
			sbQuery.append(" and ro.requestState=:rState");
			parameters.put("rState", filter.getState());
		}
		
		sbQuery.append(" ORDER BY co.operationNumber desc");

		List<Object[]> listResults = (List<Object[]>) findListByQueryString(sbQuery.toString(), parameters);
		List<RectificationOperationTO> rectificationOperations = new ArrayList<RectificationOperationTO>();
		
		for (Object[] arrObject : listResults) {
			RectificationOperationTO rectificationOperationTO = new RectificationOperationTO();
			rectificationOperationTO.setCustodyOperationPk((Long) arrObject[0]);
			rectificationOperationTO.setRequestNumber((Long) arrObject[1]);
			rectificationOperationTO.setMotive((Integer) arrObject[2]);
			rectificationOperationTO.setMotiveDescription((String) arrObject[3]);
			rectificationOperationTO.setRegistryDate((Date) arrObject[4]);
			rectificationOperationTO.setRequestType((Integer) arrObject[5]);
			rectificationOperationTO.setRequestTypeDescription((String) arrObject[6]);
			rectificationOperationTO.setState((Integer) arrObject[7]);
			rectificationOperationTO.setStateDescription((String) arrObject[8]);
			rectificationOperationTO.setRectifiedOperationNumber((Long)arrObject[9]);
			rectificationOperationTO.setRectifiedOperationPk((Long)arrObject[10]);
			rectificationOperations.add(rectificationOperationTO);
		}
		
		return rectificationOperations;
 		
	}

	/**
	 * Search rectification operation for validate.
	 *
	 * @param idRectificationOperation the id rectification operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public RectificationOperation  getRectificationOperation(Long idRectificationOperation)throws ServiceException{
	
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ro From RectificationOperation ro ");
		sbQuery.append(" join fetch ro.rectifiedOperation");
		sbQuery.append(" where ro.idRectificationOperationPk =:idRectificationOperation ");
		
		parameters.put("idRectificationOperation", idRectificationOperation);
		
		return (RectificationOperation) findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the operation type custody by rectification type.
	 *
	 * @param rectificationType the rectification type
	 * @return the operation type custody by rectification type
	 */
	public List<OperationType> getOperationTypeCustodyByRectificationType(Integer rectificationType){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ot.idOperationTypePk From OperationType ot");	
		sbQuery.append(" where ot.rectificationType=:rectificationType");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("rectificationType",rectificationType);
		return (List<OperationType> )query.getResultList();
	}
	
	/**
	 * Gets the operation type custody by cascade.
	 *
	 * @param listOperationTypeCascade the list operation type cascade
	 * @return the operation type custody by cascade
	 */
	public List<OperationTypeCascade> getOperationTypeCustodyByCascade(List<OperationType> listOperationTypeCascade){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select otc.operationType.idOperationTypePk From OperationTypeCascade otc");	
		sbQuery.append(" where otc.operationTypeCascade.idOperationTypePk in :listOperationTypeCascade");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("listOperationTypeCascade",listOperationTypeCascade);
		return (List<OperationTypeCascade> )query.getResultList();	
	}

	/**
	 * Gets the rectification details.
	 *
	 * @param idRectificationOperation the id rectification operation
	 * @param indSourceTarget the ind source target
	 * @return the rectification details
	 */
	public List<RectificationDetail> getRectificationDetails(Long idRectificationOperation, Long indSourceTarget) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" SELECT rd from RectificationDetail rd ");
		sbQuery.append(" where rd.rectificationOperation.idRectificationOperationPk = :idRectificationOperation ");
		
		if(indSourceTarget!=null){
			sbQuery.append(" and rd.indSourceDestination = :indSourceTarget ");
			parameters.put("indSourceTarget", indSourceTarget);
		}
		
		parameters.put("idRectificationOperation", idRectificationOperation);
		
		return (List<RectificationDetail>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the rectification market facts.
	 *
	 * @param idRectificationDetailPk the id rectification detail pk
	 * @return the rectification market facts
	 */
	public List<RectificationMarketFact> getRectificationMarketFacts(Long idRectificationDetailPk) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" SELECT rmf from RectificationMarketFact rmf ");
		sbQuery.append(" where rmf.rectificationDetail.idRectificationDetailPk = :rectificationDetailPk ");
		
		parameters.put("rectificationDetailPk", idRectificationDetailPk);
		
		return (List<RectificationMarketFact>)findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Validate exist previous request.
	 *
	 * @param filterTO the filter to
	 * @throws ServiceException the service exception
	 */
	public void validateExistPreviousRequest(RectificationOperationTO filterTO) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select count(ro.idRectificationOperationPk) From RectificationOperation ro ");
		sbQuery.append(" where ro.rectifiedOperation.operationNumber=:operationNumber");
		sbQuery.append(" and ro.requestType=:requestType");
		sbQuery.append(" and ro.requestState in :listState");
		
		parameters.put("operationNumber", filterTO.getRequestNumber());
		parameters.put("requestType", filterTO.getRequestType());
		parameters.put("listState", filterTO.getStateList());
		
		Long sta =  (Long) findObjectByQueryString(sbQuery.toString(), parameters);
		
		if(sta > 0l){
			throw new ServiceException(ErrorServiceType.RECTIFICATION_REQUEST_EXISTS,new Object[]{filterTO.getRequestNumber().toString()});
		}	
		
	}

	/**
	 * The method is used to update state CustodyOperation.
	 *
	 * @param objRectificationOperation RectificationOperation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateCustodyOperation(RectificationOperation objRectificationOperation, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update CustodyOperation custodyOperation set custodyOperation.state = :parState , "+
				" custodyOperation.lastModifyUser = :parLastModifyUser, custodyOperation.lastModifyDate = :parLastModifyDate, " +
				" custodyOperation.lastModifyIp = :parLastModifyIp, custodyOperation.lastModifyApp = :parLastModifyApp "+
				" where custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objRectificationOperation.getRectifiedOperation().getIdCustodyOperationPk());
		parameters.put("parState", CustodyOperationStateType.RECTIFIED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}

	/**
	 * The method is used to update state ChangeOwnershipOperation.
	 *
	 * @param objRectificationOperation RectificationOperation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateChangeOwnershipOperation(RectificationOperation objRectificationOperation, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update ChangeOwnershipOperation cooper set cooper.state = :parState , "+
				" cooper.lastModifyUser = :parLastModifyUser, cooper.lastModifyDate = :parLastModifyDate, " +
				" cooper.lastModifyIp = :parLastModifyIp, cooper.lastModifyApp = :parLastModifyApp "+
				" where cooper.custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objRectificationOperation.getRectifiedOperation().getIdCustodyOperationPk());
		parameters.put("parState", ChangeOwnershipStatusType.RECTIFIED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update state AnnotationOperation.
	 *
	 * @param objRectificationOperation RectificationOperation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateAccountAnnotationOperation(RectificationOperation objRectificationOperation, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update AccountAnnotationOperation aaop set aaop.state = :parState , "+
				" aaop.lastModifyUser = :parLastModifyUser, aaop.lastModifyDate = :parLastModifyDate, " +
				" aaop.lastModifyIp = :parLastModifyIp, aaop.lastModifyApp = :parLastModifyApp "+
				" where aaop.custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objRectificationOperation.getRectifiedOperation().getIdCustodyOperationPk());
		parameters.put("parState", DematerializationStateType.RECTIFIED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update state RetirementOperation.
	 *
	 * @param objRectificationOperation RectificationOperation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateRetirementOperation(RectificationOperation objRectificationOperation, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update RetirementOperation rop set rop.state = :parState , " +
				" rop.lastModifyUser = :parLastModifyUser, rop.lastModifyDate = :parLastModifyDate, " +
				" rop.lastModifyIp = :parLastModifyIp, rop.lastModifyApp = :parLastModifyApp " +
				" where rop.custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objRectificationOperation.getRectifiedOperation().getIdCustodyOperationPk());
		parameters.put("parState", RetirementOperationStateType.RECTIFIED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update state SecurityTransferOperation.
	 *
	 * @param objRectificationOperation RectificationOperation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateSecurityTransferOperation(RectificationOperation objRectificationOperation, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update SecurityTransferOperation stop set stop.state = :parState , " +
				" stop.lastModifyUser = :parLastModifyUser, stop.lastModifyDate = :parLastModifyDate, " +
				" stop.lastModifyIp = :parLastModifyIp, stop.lastModifyApp = :parLastModifyApp " +
				" where stop.custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objRectificationOperation.getRectifiedOperation().getIdCustodyOperationPk());
		parameters.put("parState", TransferSecuritiesStateType.RECTIFIED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}

}
