package com.pradera.custody.rectificationoperation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.dematerializationcertificate.service.DematerializationCertificateServiceBean;
import com.pradera.custody.dematerializationcertificate.to.SearchAccountAnnotationTO;
import com.pradera.custody.ownershipexchange.facade.ChangeOwnershipServiceFacade;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipDetailTO;
import com.pradera.custody.rectificationoperation.service.RectificationOperationServiceBean;
import com.pradera.custody.rectificationoperation.to.RectificationOperationTO;
import com.pradera.custody.rectificationoperation.to.RectificationRequestTO;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.tranfersecurities.to.SecurityTransferOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.OperationType;
import com.pradera.model.component.OperationTypeCascade;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.changeownership.ChangeOwnershipMarketFact;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.rectification.RectificationDetail;
import com.pradera.model.custody.rectification.RectificationMarketFact;
import com.pradera.model.custody.rectification.RectificationOperation;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementMarketfact;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.BlockOperationTransfer;
import com.pradera.model.custody.transfersecurities.SecurityTransferMarketfact;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.custody.type.ChangeOwnershipMotiveType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.custody.type.CustodyOperationStateType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.RectificationOperationStateType;
import com.pradera.model.custody.type.RectificationOperationType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.operations.RequestCorrelativeType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RectificationOperationFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class RectificationOperationFacade {


	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;

	/** The rectification operation service bean. */
	@Inject
	private RectificationOperationServiceBean rectificationOperationServiceBean;	

	/** The executor component service bean. */
	@Inject
    private Instance<AccountsComponentService> accountsComponentService;
	
	/** The securities component service. */
	@Inject
    Instance<SecuritiesComponentService> securitiesComponentService;

	/** The request reversals service bean. */
	@Inject
	private RequestReversalsServiceBean requestReversalsServiceBean;

	/** The affectations service bean. */
	@Inject
	private AffectationsServiceBean affectationsServiceBean;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The change ownership service facade. */
	@Inject
	private ChangeOwnershipServiceFacade changeOwnershipServiceFacade;	 
	
	/** The dematerialization certificate facade. */
	@EJB
	private DematerializationCertificateServiceBean dematerializationCertificateServiceBean;


	/**
	 * Register rectification operation.
	 *
	 * @param rectificationOperation the rectification operation
	 * @return the rectification operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.RECTIFICATION_OPERATION_REGISTER)
	public RectificationOperation registerRectificationOperation(RectificationOperation rectificationOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		rectificationOperation.setRegistryDate(CommonsUtilities.currentDate());
		rectificationOperation.setRegistryUser(loggerUser.getUserName());
		rectificationOperation.setRequestState(RectificationOperationStateType.REGISTERED.getCode());
		
	    validateExistPreviousRequest(rectificationOperation.getRequestType(), rectificationOperation.getRectifiedOperation().getOperationNumber());
	    Long operationNumber = rectificationOperationServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_RECTIFICATION);
		rectificationOperation.getCustodyOperation().setOperationNumber(operationNumber);
		rectificationOperation.getCustodyOperation().setRegistryUser(loggerUser.getUserName());
		rectificationOperation.getCustodyOperation().setOperationDate(CommonsUtilities.currentDate());
		rectificationOperation.getCustodyOperation().setRegistryDate(CommonsUtilities.currentDateTime());
		rectificationOperation.getCustodyOperation().setState(CustodyOperationStateType.REGISTERED.getCode());
		rectificationOperation.getCustodyOperation().setIndRectification(ComponentConstant.ONE);
		rectificationOperation.setRequestNumber(operationNumber);
		
		rectificationOperationServiceBean.create(rectificationOperation);
		
		for (RectificationDetail rectificationDetail : rectificationOperation.getRectificationDetail()) {
			rectificationOperationServiceBean.create(rectificationDetail);
			rectificationOperationServiceBean.saveAll(rectificationDetail.getRectificationMarketFacts());
			
			if(Validations.validateListIsNotNullAndNotEmpty(rectificationDetail.getRectificationDetails())){
				for (RectificationDetail sourceDetail : rectificationDetail.getRectificationDetails()) {
					rectificationOperationServiceBean.create(sourceDetail);
					rectificationOperationServiceBean.saveAll(sourceDetail.getRectificationMarketFacts());
				}
			}
		}
		
		executeComponentForAvailables(rectificationOperation,BusinessProcessType.RECTIFICATION_OPERATION_REGISTER.getCode(),loggerUser,false);

		return rectificationOperation;
	}

	/**
	 * Reject rectification operation.
	 *
	 * @param rectificationOperation the rectification operation
	 * @return the rectification operation
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.RECTIFICATION_OPERATION_REJECT)
	public RectificationOperation rejectRectificationOperation(RectificationOperation rectificationOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());

		if(rectificationOperation.getRequestType().equals(RectificationOperationType.DESMATERIALIZATION_CERTIFICATE.getCode())||
				    rectificationOperation.getRequestType().equals(RectificationOperationType.SECURITIES_RETIREMENT.getCode())||
					rectificationOperation.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_AVAILABLE.getCode())||
					rectificationOperation.getRequestType().equals(RectificationOperationType.CHANGE_OWNERSHIP_AVIALBLE.getCode())){
			executeComponentForAvailables(rectificationOperation,BusinessProcessType.RECTIFICATION_OPERATION_REJECT.getCode(),loggerUser,false);
		}
		
		rectificationOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDate());
		rectificationOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		rectificationOperation.getCustodyOperation().setState(CustodyOperationStateType.REJECTED.getCode());
		rectificationOperation.getCustodyOperation().setIndRectification(ComponentConstant.ONE);
		
		rectificationOperation.setRequestState(RectificationOperationStateType.REJECTED.getCode());

		rectificationOperationServiceBean.update(rectificationOperation);
		return rectificationOperation;

	}



	/**
	 * Confirm rectification operation.
	 *
	 * @param rectificationOperation the rectification operation
	 * @return the rectification operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM)
	public RectificationOperation confirmRectificationOperation(RectificationOperation rectificationOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		executeComponentForAvailables(rectificationOperation,BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM.getCode(),  loggerUser,true);
		
		executeComponentForBlocked(rectificationOperation,loggerUser);
		
		if(rectificationOperation.getRequestType().equals(RectificationOperationType.DESMATERIALIZATION_CERTIFICATE.getCode())){
			
			rectificationOperationServiceBean.updateStateAccountAnnotationOperation(rectificationOperation, loggerUser);
			
		} else if(rectificationOperation.getRequestType().equals(RectificationOperationType.SECURITIES_RETIREMENT.getCode())){
			
			rectificationOperationServiceBean.updateStateRetirementOperation(rectificationOperation, loggerUser);
			
		} else if(rectificationOperation.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_AVAILABLE.getCode())
				|| rectificationOperation.getRequestType().equals(RectificationOperationType.TRANSFER_SECURITIE_BLOCK.getCode())){
			
			rectificationOperationServiceBean.updateStateSecurityTransferOperation(rectificationOperation, loggerUser);
			
		} else if(rectificationOperation.getRequestType().equals(RectificationOperationType.CHANGE_OWNERSHIP_AVIALBLE.getCode())
				|| rectificationOperation.getRequestType().equals(RectificationOperationType.CHANGE_OWNERCSHIP_BLOCK.getCode())){
			
			rectificationOperationServiceBean.updateStateChangeOwnershipOperation(rectificationOperation, loggerUser);
			
		}		
		rectificationOperationServiceBean.updateStateCustodyOperation(rectificationOperation, loggerUser);

		rectificationOperation.setRequestState(RectificationOperationStateType.CONFIRMED.getCode());
		rectificationOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDate());
		rectificationOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		rectificationOperation.getCustodyOperation().setIndRectification(ComponentConstant.ONE);
		rectificationOperation.getCustodyOperation().setState(CustodyOperationStateType.CONFIRMED.getCode());
		return rectificationOperationServiceBean.update(rectificationOperation);
	}


	/**
	 * Execute component for availables.
	 *
	 * @param rectificationOperation the rectification operation
	 * @param idBusinessProcess the id business process
	 * @param loggerUser the logger user
	 * @param fillTarget the fill target
	 * @throws ServiceException the service exception
	 */
	private void executeComponentForAvailables(RectificationOperation rectificationOperation, 
			Long idBusinessProcess, LoggerUser loggerUser, boolean fillTarget) throws ServiceException {
		
		List<RectificationDetail> lstRectificationDetails = rectificationOperationServiceBean.getRectificationDetails(rectificationOperation.getIdRectificationOperationPk(),null);
		
		List<HolderAccountBalanceTO> lstSourceAccounts = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> lstTargetAccounts = new ArrayList<HolderAccountBalanceTO>();
		
		// component should not be called when this combination happens, because balance is not present in source account
		boolean callComponent = true;
		if((rectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME_SRC.getCode()) ||
				rectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_VARIABLE_INCOME.getCode()) || 
				rectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_REVERSION.getCode()) )
				&& (BusinessProcessType.RECTIFICATION_OPERATION_REGISTER.getCode().equals(idBusinessProcess) || 
						BusinessProcessType.RECTIFICATION_OPERATION_REJECT.getCode().equals(idBusinessProcess))){
			callComponent = false;
		}
		
		if(callComponent){
			for(RectificationDetail rectificationDetail: lstRectificationDetails){
				if(rectificationDetail.getIndBlockDetail().equals(ComponentConstant.ZERO)){
					if(fillTarget && rectificationDetail.getIndSourceDestination().equals(ComponentConstant.TARGET)){
						HolderAccountBalanceTO holderAccountBalanceTO = getHolderAccountBalanceTO(rectificationDetail);
						lstTargetAccounts.add(holderAccountBalanceTO);
					} else if (rectificationDetail.getIndSourceDestination().equals(ComponentConstant.SOURCE)){
						HolderAccountBalanceTO holderAccountBalanceTO = getHolderAccountBalanceTO(rectificationDetail);
						lstSourceAccounts.add(holderAccountBalanceTO);
					}
				}
			}
				
			if(Validations.validateListIsNotNullAndNotEmpty(lstSourceAccounts) || Validations.validateListIsNotNullAndNotEmpty(lstTargetAccounts)){
				AccountsComponentTO objAccountComponent = new AccountsComponentTO();
				objAccountComponent.setLstSourceAccounts(lstSourceAccounts);
				objAccountComponent.setLstTargetAccounts(lstTargetAccounts);
				objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
				objAccountComponent.setIdBusinessProcess(idBusinessProcess);
				objAccountComponent.setIdOperationType(rectificationOperation.getCustodyOperation().getOperationType());
				objAccountComponent.setIdCustodyOperation(rectificationOperation.getCustodyOperation().getIdCustodyOperationPk());

				accountsComponentService.get().executeAccountsComponent(objAccountComponent);
			}
		}
		
		if((rectificationOperation.getRequestType().equals(RectificationOperationType.DESMATERIALIZATION_CERTIFICATE.getCode())||
				rectificationOperation.getRequestType().equals(RectificationOperationType.SECURITIES_RETIREMENT.getCode()))
				&& BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM.getCode().equals(idBusinessProcess)){
			
			List<RectificationDetail> lstSourceDetail = new ArrayList<RectificationDetail>();
			List<RectificationDetail> lstTargetDetail = new ArrayList<RectificationDetail>();
			
			for (RectificationDetail rectificationDetail : lstRectificationDetails) {
				if(rectificationDetail.getIndSourceDestination().equals(ComponentConstant.SOURCE)){
					lstSourceDetail.add(rectificationDetail);
				}else if(rectificationDetail.getIndSourceDestination().equals(ComponentConstant.TARGET)){
					lstTargetDetail.add(rectificationDetail);
				}
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSourceDetail)){
				List<SecuritiesOperation> lstSourceSecuritiesOperation = createSecuritiesOperation(rectificationOperation,lstSourceDetail);
				for (SecuritiesOperation securitiesOperation : lstSourceSecuritiesOperation) {
					rectificationOperationServiceBean.create(securitiesOperation);
					executeSecuritiesComponent(idBusinessProcess,securitiesOperation,ComponentConstant.SOURCE);
				}
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstTargetDetail)){
				List<SecuritiesOperation> lstTargetSecuritiesOperation = createSecuritiesOperation(rectificationOperation,lstSourceDetail);
				for (SecuritiesOperation securitiesOperation : lstTargetSecuritiesOperation) {
					rectificationOperationServiceBean.create(securitiesOperation);
					executeSecuritiesComponent(idBusinessProcess,securitiesOperation,ComponentConstant.TARGET);
				}
			}
		}
		
	}
	
	/**
	 * Execute securities component.
	 *
	 * @param idBusinessProcess the id business process
	 * @param securitiesOperation the securities operation
	 * @param sourceTarget the source target
	 * @throws ServiceException the service exception
	 */
	private void executeSecuritiesComponent(Long idBusinessProcess, SecuritiesOperation securitiesOperation, Long sourceTarget) throws ServiceException {
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
		securitiesComponentTO.setIdBusinessProcess(idBusinessProcess);
		securitiesComponentTO.setIdOperationType(securitiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(securitiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setIdSecurityCode(securitiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setDematerializedBalance(securitiesOperation.getStockQuantity());
		securitiesComponentTO.setPhysicalBalance(securitiesOperation.getStockQuantity());
		
		securitiesComponentTO.setCirculationBalance(securitiesOperation.getStockQuantity());
		securitiesComponentTO.setPlacedBalance(securitiesOperation.getStockQuantity());
		securitiesComponentTO.setShareBalance(securitiesOperation.getStockQuantity());
		
		securitiesComponentTO.setIndSourceTarget(sourceTarget);
		securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);		
	}

	/**
	 * Gets the holder account balance to.
	 *
	 * @param rectificationDetail the rectification detail
	 * @return the holder account balance to
	 */
	public HolderAccountBalanceTO getHolderAccountBalanceTO(RectificationDetail rectificationDetail){
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(rectificationDetail.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdSecurityCode(rectificationDetail.getSecurities().getIdSecurityCodePk());
		holderAccountBalanceTO.setIdParticipant(rectificationDetail.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setStockQuantity (rectificationDetail.getQuantity());
		
		if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(RectificationMarketFact rectificationMarketFact : rectificationDetail.getRectificationMarketFacts()){
				MarketFactAccountTO	marketFactAccountTO = new MarketFactAccountTO();
				marketFactAccountTO.setMarketDate(rectificationMarketFact.getMarketDate());
				marketFactAccountTO.setMarketPrice(rectificationMarketFact.getMarketPrice());
				marketFactAccountTO.setMarketRate(rectificationMarketFact.getMarketRate());
				marketFactAccountTO.setQuantity(rectificationMarketFact.getOperationQuantity());
				holderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
			}
		}
		return holderAccountBalanceTO;
	}

	/**
	 * Register securities operation.
	 *
	 * @param rectificationOperation the rectification operation
	 * @param rectificationDetails the rectification details
	 * @return the securities operation
	 * @throws ServiceException the service exception
	 */
	public List<SecuritiesOperation> createSecuritiesOperation(RectificationOperation rectificationOperation, List<RectificationDetail> rectificationDetails)throws ServiceException{

		Map<String,SecuritiesOperation> operationsBySecurities = new HashMap<String, SecuritiesOperation>();
		
		for (RectificationDetail rectificationDetail : rectificationDetails) {
			SecuritiesOperation securitiesOperation = operationsBySecurities.get(rectificationDetail.getSecurities().getIdSecurityCodePk());
			
			if(securitiesOperation == null){
				securitiesOperation = new SecuritiesOperation();
				securitiesOperation.setCustodyOperation(rectificationOperation.getCustodyOperation());
				securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
				securitiesOperation.setSecurities(rectificationDetail.getSecurities());
				Long idOperationType = getRectificationOperationType(rectificationOperation.getCustodyOperation().getOperationType(), rectificationDetail.getSecurities().getIssuanceForm());
				securitiesOperation.setOperationType(idOperationType);
				securitiesOperation.setStockQuantity(BigDecimal.ZERO);
				securitiesOperation.setCashAmount(BigDecimal.ZERO);
				operationsBySecurities.put(rectificationDetail.getSecurities().getIdSecurityCodePk(), securitiesOperation);
			}
			
			BigDecimal cashAmount = rectificationDetail.getQuantity().multiply(rectificationDetail.getSecurities().getCurrentNominalValue());
			securitiesOperation.setCashAmount(securitiesOperation.getCashAmount().add(cashAmount));
			
			securitiesOperation.setStockQuantity(securitiesOperation.getStockQuantity().add(rectificationDetail.getQuantity()));
		}
		List<SecuritiesOperation> lstSecuritiesOperation = new ArrayList<SecuritiesOperation>(operationsBySecurities.values());
		return lstSecuritiesOperation;
	}

	/**
	 * Gets the rectification operation type.
	 *
	 * @param operationType the operation type
	 * @param issuanceForm the issuance form
	 * @return the rectification operation type
	 */
	public Long getRectificationOperationType(Long operationType, Integer issuanceForm) {
		if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_AUCTION.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_AUCTION.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_DIRECT_SALE_BCB.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_BCB.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_DIRECT_SALE_TGN.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_TGN.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_DONATION.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DONATION.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_MONEY_PURCHASE.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_MONEY_PURCHASE.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_DEMATERIALIZATION.getCode())){
			if(issuanceForm.equals(IssuanceType.DEMATERIALIZED.getCode())){
				return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DEMATERIALIZATION.getCode();	
			} else {
				return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DEMATERIALIZATION_MIXED.getCode();
			}
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_PRIVATE_SALE.getCode())){
			if(issuanceForm.equals(IssuanceType.DEMATERIALIZED.getCode())){
				return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_PRIVATE_DEMATERIALIZED.getCode();	
			} else {
				return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_DIRECT_PURCHASE_PRIVATE_MIXED.getCode();
			}
		} else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_BANK_SHARES_PURCHASE.getCode())){
			if(issuanceForm.equals(IssuanceType.DEMATERIALIZED.getCode())){
				return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_PRIMARY_PLACEMENT_DEMATERIALIZED.getCode();	
			} else {
				return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_PRIMARY_PLACEMENT_MIXED.getCode();
			}	
		}
		
		
		if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode();
		}if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME_SRC.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME_SRC.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_VARIABLE_INCOME.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_REDEMPTION_VARIABLE_INCOME.getCode();
		}else if(operationType.equals(ParameterOperationType.RECTIFICATION_SECURITIES_REVERSION.getCode())){
			return ParameterOperationType.SEC_RECTIFICATION_SECURITIES_REVERSION.getCode();
		}
		return null;
	}

	/**
	 * Find security transfer operation.
	 *
	 * @param to the to
	 * @return the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public SecurityTransferOperation findSecurityTransferOperation(SecurityTransferOperationTO to)throws ServiceException{
		SecurityTransferOperation operation = rectificationOperationServiceBean.searchSecurityTrasnferOperation(to);
		
		if(operation!=null){
			
			if(!operation.getState().equals(TransferSecuritiesStateType.CONFIRMADO.getCode())){
				throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_CONFIRMED ,new Object[]{to.getOperationNumber().toString()});
			}
			
			for(HolderAccountDetail holderAccountDetail :  operation.getSourceHolderAccount().getHolderAccountDetails()){
				holderAccountDetail.getHolder().getIdHolderPk();
			}
			for(HolderAccountDetail holderAccountDetail :  operation.getTargetHolderAccount().getHolderAccountDetails()){
				holderAccountDetail.getHolder().getIdHolderPk();
			}
			if(Validations.validateIsNotNull(operation)){
				if(ComponentConstant.ONE.equals(operation.getIndBlockTransfer())){
					for(BlockOperationTransfer blockOperationTransfer :  operation.getBlockOperationTransfer()){
						blockOperationTransfer.getBlockOperation().getIdBlockOperationPk();
					}
				}
			}
		}
		return operation;		
	}

	/**
	 * Search rectification operations.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.RECTIFICATION_OPERATION_QUERY)
	public List<RectificationOperationTO> searchRectificationOperations(RectificationOperationTO filter)throws ServiceException{
		return rectificationOperationServiceBean.searchRectificationOperations(filter);
	}

	/**
	 * Search rectification operation.
	 *
	 * @param idRectificationOperation the id rectification operation
	 * @return the rectification operation
	 * @throws ServiceException the service exception
	 */
	public RectificationOperation searchRectificationOperation(Long idRectificationOperation)throws ServiceException{
		RectificationOperation operation = this.rectificationOperationServiceBean.getRectificationOperation(idRectificationOperation);
		
		for(RectificationDetail detail: operation.getRectificationDetail()){
			detail.getHolderAccount().getIdHolderAccountPk();
			detail.getRectificationOperation().getCustodyOperation().getIdCustodyOperationPk();
			detail.getParticipant().getIdParticipantPk();
			detail.getSecurities().getIdSecurityCodePk();
			if(detail.getBlockOperation()!=null){
				detail.getBlockOperation().getIdBlockOperationPk();
				detail.getBlockOperation().getCustodyOperation().getIdCustodyOperationPk();
				detail.getBlockOperation().getBlockRequest().getIdBlockRequestPk();
			}

		}
		return operation;
	}

	/**
	 * Search retirement operation by operation number and request type.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the retirement operation
	 * @throws ServiceException the service exception
	 */
	public RetirementOperation getRetirementOperation(RetirementOperationTO retirementOperationTO) throws ServiceException{
		
		RetirementOperation operation = rectificationOperationServiceBean.searchRetirementOperationByOperationNumberAndRequestType(retirementOperationTO);
		if(operation!=null){
			
			if(!operation.getState().equals(RetirementOperationStateType.CONFIRMED.getCode())){
				throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_CONFIRMED,new Object[]{retirementOperationTO.getOperationNumber()});
			}
			
			if(operation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_REVERSION.getCode())){
				throw new ServiceException(ErrorServiceType.RECTIFICATION_RETIREMENT_BY_REVERSION_NOT_VALID);
			}
			
			operation.getSecurity().getIdSecurityCodePk();
			if(operation.getTargetSecurity()!=null){
				operation.getTargetSecurity().getIdSecurityCodePk();
			}
			for(RetirementDetail retirementDetail : operation.getRetirementDetails()){
				for(HolderAccountDetail holderAccountDetail : retirementDetail.getHolderAccount().getHolderAccountDetails()){
					holderAccountDetail.getHolder().getIdHolderPk();
				}
			}
		}
		return operation;		
	}


	/**
	 * Group rectification detail by affectation.
	 *
	 * @param operation the operation
	 * @return the map
	 */
	public Map<Integer, List<RectificationDetail>> groupRectificationDetailByAffectation(RectificationOperation operation){
		Map<Integer, List<RectificationDetail>> blockDetailMap = new HashMap<Integer, List<RectificationDetail>>();
		for(RectificationDetail detail:operation.getRectificationDetail()){
			if(detail.getIndSourceDestination().equals(ComponentConstant.TARGET)){
				List<RectificationDetail> currentBlockList=  blockDetailMap.get(detail.getBlockOperation().getBlockRequest().getBlockType());
				if(currentBlockList==null){
					currentBlockList = new ArrayList<RectificationDetail>();
				}
				currentBlockList.add(detail);
				blockDetailMap.put(detail.getBlockOperation().getBlockRequest().getBlockType(), currentBlockList);
			}
		}

		return blockDetailMap;
	}
	
	/**
	 * Execute componente for blocked.
	 *
	 * @param rectificationOperation the operation
	 * @param loggerUser the user name
	 * @return the rectification operation
	 * @throws ServiceException the service exception
	 */
	public void executeComponentForBlocked(RectificationOperation rectificationOperation, LoggerUser loggerUser)throws ServiceException{
		
		Long idBusinessProcess = null;
		if(rectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_TRANSFER_BLOCK.getCode())){
			idBusinessProcess = BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM_TRANSFER_BLOCKED.getCode();
		}else if (rectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_CHANGE_OWNERSHIP_BLOCK.getCode()) ){
			idBusinessProcess = BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM_OWNERSHIP_EXCHANGE_BLOCKED.getCode();
		}else if (rectificationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode()) ){
			idBusinessProcess = BusinessProcessType.RECTIFICATION_OPERATION_REDEMPTION_BLOCKED.getCode();
		}
		
		if(idBusinessProcess!=null){
			
			List<RectificationDetail> lstRectificationDetails = rectificationOperationServiceBean.getRectificationDetails(
					rectificationOperation.getIdRectificationOperationPk(), ComponentConstant.TARGET);
			
			for(RectificationDetail rectificationDetail : lstRectificationDetails){
				if(rectificationDetail.getIndBlockDetail().equals(ComponentConstant.ONE)){
					
					for(RectificationDetail sourceRectificationDet : rectificationDetail.getRectificationDetails()){
					
						if(sourceRectificationDet.getQuantity().compareTo(sourceRectificationDet.getBlockOperation().getOriginalBlockBalance()) != 0 ){
							throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_BLOCK_INVALID);
						}
						
						// single block_operation for reversal
						BlockOpeForReversalsTO blockOpeForReversal = new BlockOpeForReversalsTO();
		        		blockOpeForReversal.setIdBlockOperationPk(sourceRectificationDet.getBlockOperation().getIdBlockOperationPk());
		        		blockOpeForReversal.setAmountUnblock(sourceRectificationDet.getQuantity());
						
		        		// header for creating the unblock_request: one per block_operation
		        		RegisterReversalTO registerReversalTO = new RegisterReversalTO();
		        		registerReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
		            	registerReversalTO.setRegistryUser(loggerUser.getUserName());
		            	registerReversalTO.setDocumentNumber(sourceRectificationDet.getBlockOperation().getBlockRequest().getBlockNumber()); // temporal
		        		registerReversalTO.setIdHolderPk(sourceRectificationDet.getHolderAccount().getRepresentativeHolder().getIdHolderPk());
		            	registerReversalTO.setTotalUnblock(sourceRectificationDet.getQuantity());
		            	registerReversalTO.setIdState(RequestReversalsStateType.APPROVED.getCode());
		            	registerReversalTO.getLstBlockOpeForReversals().add(blockOpeForReversal);
		            	
		            	if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
		            		blockOpeForReversal.setReversalsMarkFactList(new ArrayList<ReversalsMarkFactTO>());
		            		List<BlockMarketFactOperation> blockMarketFactOperations = affectationsServiceBean.lstBlockMarketFactOperation(null, sourceRectificationDet.getBlockOperation().getIdBlockOperationPk());
		            		sourceRectificationDet.getBlockOperation().setBlockMarketFactOperations(blockMarketFactOperations);
		    				for(BlockMarketFactOperation blockMarketFact: sourceRectificationDet.getBlockOperation().getBlockMarketFactOperations()){
		    					ReversalsMarkFactTO reversalMarketFact = new ReversalsMarkFactTO();
		    					reversalMarketFact.setMarketDate(blockMarketFact.getMarketDate());
		    					reversalMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
		    					reversalMarketFact.setMarketRate(blockMarketFact.getMarketRate());
		    					reversalMarketFact.setBlockAmount(blockMarketFact.getOriginalBlockBalance());
		    					blockOpeForReversal.getReversalsMarkFactList().add(reversalMarketFact);
		    				}
		    			}
		            	
		            	//TO for confirmation
		            	RequestReversalsTO requestReversalsTO = new RequestReversalsTO();
		                requestReversalsTO.setIdUnblockRequestPk(requestReversalsServiceBean.saveReversalRequest(registerReversalTO));
		                registerReversalTO.setConfirmationUser(loggerUser.getUserName());
		                requestReversalsTO.setBusinessProcessTypeId(idBusinessProcess);
		                requestReversalsServiceBean.confirmReversalRequest(requestReversalsTO, loggerUser, Boolean.FALSE);
					}
					
				}
	        }
			
			//2. execute account componente

			List<HolderAccountBalanceTO> lstSourceAccounts = new ArrayList<HolderAccountBalanceTO>();
			List<HolderAccountBalanceTO> lstTargetAccount = new ArrayList<HolderAccountBalanceTO>();

			for(RectificationDetail rectificationDetail : lstRectificationDetails){
				if(rectificationDetail.getIndBlockDetail().equals(ComponentConstant.ONE)){
					
					HolderAccountBalanceTO tgtHolderAccountBalanceTO = new HolderAccountBalanceTO();
					tgtHolderAccountBalanceTO.setIdHolderAccount(rectificationDetail.getHolderAccount().getIdHolderAccountPk());
					tgtHolderAccountBalanceTO.setIdSecurityCode(rectificationDetail.getSecurities().getIdSecurityCodePk());
					tgtHolderAccountBalanceTO.setIdParticipant(rectificationDetail.getParticipant().getIdParticipantPk());
					tgtHolderAccountBalanceTO.setStockQuantity(BigDecimal.ZERO);
					
					lstTargetAccount.add(tgtHolderAccountBalanceTO);
					
					for(RectificationDetail sourceRectificationDet : rectificationDetail.getRectificationDetails()){
					
						if(LevelAffectationType.BLOCK.getCode().equals(sourceRectificationDet.getBlockOperation().getBlockLevel())){
							HolderAccountBalanceTO srcHolderAccountBalanceTO = new HolderAccountBalanceTO();
							srcHolderAccountBalanceTO.setIdHolderAccount(sourceRectificationDet.getHolderAccount().getIdHolderAccountPk());
							srcHolderAccountBalanceTO.setIdSecurityCode(sourceRectificationDet.getSecurities().getIdSecurityCodePk());
							srcHolderAccountBalanceTO.setIdParticipant(sourceRectificationDet.getParticipant().getIdParticipantPk());
							srcHolderAccountBalanceTO.setStockQuantity(sourceRectificationDet.getQuantity());
							
							if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
								if(tgtHolderAccountBalanceTO.getLstMarketFactAccounts()== null){
									tgtHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
								}
								srcHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
								for(BlockMarketFactOperation blockMktFact : sourceRectificationDet.getBlockOperation().getBlockMarketFactOperations()){
									MarketFactAccountTO marketFactTO = new MarketFactAccountTO();
									marketFactTO.setMarketDate(blockMktFact.getMarketDate());
									marketFactTO.setMarketPrice(blockMktFact.getMarketPrice());
									marketFactTO.setMarketRate(blockMktFact.getMarketRate());
									marketFactTO.setQuantity(blockMktFact.getOriginalBlockBalance());
									srcHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactTO);
									tgtHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactTO);
								}
							}
							
							tgtHolderAccountBalanceTO.setStockQuantity(tgtHolderAccountBalanceTO.getStockQuantity().add(srcHolderAccountBalanceTO.getStockQuantity()));
							
							lstSourceAccounts.add(srcHolderAccountBalanceTO);
						}
					}
				}
				
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstSourceAccounts) && Validations.validateListIsNotNullAndNotEmpty(lstTargetAccount)){
				AccountsComponentTO objAccountComponent = new AccountsComponentTO();
				objAccountComponent.setIdBusinessProcess(BusinessProcessType.RECTIFICATION_OPERATION_CONFIRM.getCode());
				objAccountComponent.setIdCustodyOperation(rectificationOperation.getIdRectificationOperationPk());
				objAccountComponent.setIdOperationType(rectificationOperation.getCustodyOperation().getOperationType());
				objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());

				objAccountComponent.setLstSourceAccounts(lstSourceAccounts);
				objAccountComponent.setLstTargetAccounts(lstTargetAccount);
				
				accountsComponentService.get().executeAccountsComponent(objAccountComponent);
			}
			
			List<AffectationTransferTO> transfers = new ArrayList<AffectationTransferTO>();
			
			for(RectificationDetail rectificationDetail : lstRectificationDetails){

				if(rectificationDetail.getIndBlockDetail().equals(ComponentConstant.ONE)){
					
					for(RectificationDetail sourceRectificationDet : rectificationDetail.getRectificationDetails()){
					
						if(sourceRectificationDet.getQuantity().compareTo(sourceRectificationDet.getBlockOperation().getOriginalBlockBalance()) != 0 ){
							throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_BLOCK_INVALID);
						}
						
						AffectationTransferTO affectationTransfer = new AffectationTransferTO();
						affectationTransfer.setIdHolderPk(rectificationDetail.getHolderAccount().getRepresentativeHolder().getIdHolderPk());
						affectationTransfer.setIdHolderAccountPk(rectificationDetail.getHolderAccount().getIdHolderAccountPk());
						affectationTransfer.setIdParticipantPk(rectificationDetail.getParticipant().getIdParticipantPk());
						affectationTransfer.setIdSecurityCodePk(rectificationDetail.getSecurities().getIdSecurityCodePk());
						affectationTransfer.setRectificationDetail(rectificationDetail);
						affectationTransfer.setIdBlockOperationPk(sourceRectificationDet.getBlockOperation().getIdBlockOperationPk());
						affectationTransfer.setQuantity(BigDecimal.ZERO);
						affectationTransfer.setMarketFactAffectations(new ArrayList<MarketFactTO>());
						affectationTransfer.setQuantity(sourceRectificationDet.getBlockOperation().getOriginalBlockBalance());
						
						if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
							for(BlockMarketFactOperation blockMktFact : sourceRectificationDet.getBlockOperation().getBlockMarketFactOperations()){
								MarketFactTO marketFactAffectationTransferTO = new MarketFactTO();
								marketFactAffectationTransferTO.setMarketDate(blockMktFact.getMarketDate());
								marketFactAffectationTransferTO.setMarketPrice(blockMktFact.getMarketPrice());
								marketFactAffectationTransferTO.setMarketRate(blockMktFact.getMarketRate());
								marketFactAffectationTransferTO.setQuantity(blockMktFact.getOriginalBlockBalance());
								affectationTransfer.getMarketFactAffectations().add(marketFactAffectationTransferTO);
							}
		    			}
						transfers.add(affectationTransfer);
					}
					
				}
			}
			
			if(Validations.validateListIsNotNullAndNotEmpty(transfers)){
				affectationsServiceBean.transferBlockOperations(transfers,idBusinessProcess,loggerUser.getUserName(),idepositarySetup.getIndMarketFact());
			}
		}
		
	}

	/**
	 * Validate concurrency.
	 *
	 * @param requestType the request type
	 * @param operationRef the operation ref
	 * @throws ServiceException the service exception
	 */
	public void validateExistPreviousRequest(Integer requestType, Long operationRef)throws ServiceException{
		
		RectificationOperationTO filterTO = new RectificationOperationTO();
		filterTO.setRequestNumber(operationRef);
		filterTO.setRequestType(requestType);
		List<Integer> stateList = new ArrayList<Integer>();
		stateList.add(RectificationOperationStateType.CONFIRMED.getCode());
		stateList.add(RectificationOperationStateType.REGISTERED.getCode());
		filterTO.setStateList(stateList);
		
		rectificationOperationServiceBean.validateExistPreviousRequest(filterTO);
	}

	/**
	 * Gets the operation type custody by rectification type.
	 *
	 * @param rectificationType the rectification type
	 * @return the operation type custody by rectification type
	 */
	public List<OperationTypeCascade> getOperationTypeCustodyByRectificationType(Integer rectificationType){
		return getOperationTypeCustodyByCascade(rectificationOperationServiceBean.getOperationTypeCustodyByRectificationType(rectificationType));
	}
	
	/**
	 * Gets the operation type custody by cascade.
	 *
	 * @param listOperationTypeCascade the list operation type cascade
	 * @return the operation type custody by cascade
	 */
	public List<OperationTypeCascade> getOperationTypeCustodyByCascade(List<OperationType> listOperationTypeCascade){
		return rectificationOperationServiceBean.getOperationTypeCustodyByCascade(listOperationTypeCascade);
	}
	
	/**
	 * Gets the securities transfer and build rectification.
	 *
	 * @param rectificationRequestTO the rectification request to
	 * @return the securities transfer and build rectification
	 * @throws ServiceException the service exception
	 */
	public void getSecuritiesTransferAndBuildRectification(RectificationRequestTO rectificationRequestTO) throws ServiceException{
		
		RectificationOperation rectificationOperation = rectificationRequestTO.getRectificationOperation();
		
		SecurityTransferOperationTO filter = new SecurityTransferOperationTO();
		filter.setOperationNumber(rectificationRequestTO.getOperationNumberRef());
		filter.setBalanceType(rectificationRequestTO.getBalanceType());
		filter.setListOperationTypePk(rectificationRequestTO.getListOperationType());
		filter.setOperationDate(rectificationRequestTO.getOperationDate());
		
		SecurityTransferOperation securitiesTransferOperation = findSecurityTransferOperation(filter);
		if(Validations.validateIsNotNull(securitiesTransferOperation)){
			
			rectificationOperation.setRectifiedOperation(securitiesTransferOperation.getCustodyOperation());
			if(securitiesTransferOperation.getIndBlockTransfer().equals(ComponentConstant.ONE)){	
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_TRANSFER_BLOCK.getCode());
			}else if(securitiesTransferOperation.getIndBlockTransfer().equals(ComponentConstant.ZERO)){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_TRANSFER_AVAILABLE.getCode());
			}
			
			RectificationDetail rectificationDetailTarget = new RectificationDetail();					
			rectificationDetailTarget.setHolderAccount(securitiesTransferOperation.getSourceHolderAccount());
			rectificationDetailTarget.setParticipant(securitiesTransferOperation.getSourceParticipant());
			rectificationDetailTarget.setSecurities(securitiesTransferOperation.getSecurities());
			rectificationDetailTarget.setQuantity(securitiesTransferOperation.getQuantityOperation());
			rectificationDetailTarget.setIndSourceDestination(ComponentConstant.TARGET);
			rectificationDetailTarget.setRectificationOperation(rectificationOperation);	
			rectificationDetailTarget.setRectificationDetails(new ArrayList<RectificationDetail>());
			
			if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
				rectificationDetailTarget.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
				for (SecurityTransferMarketfact securityTransferMarketfact : securitiesTransferOperation.getSecurityTransferOperationnMarketFact()) {
					RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
					rectificationMarketFact.setMarketDate(securityTransferMarketfact.getMarketDate());
					rectificationMarketFact.setMarketRate(securityTransferMarketfact.getMarketRate());
					rectificationMarketFact.setMarketPrice(securityTransferMarketfact.getMarketPrice());
					rectificationMarketFact.setOperationQuantity(securityTransferMarketfact.getQuantityOperation());
					rectificationMarketFact.setRectificationDetail(rectificationDetailTarget);
					rectificationDetailTarget.getRectificationMarketFacts().add(rectificationMarketFact);
				}
			}
			
			rectificationOperation.getRectificationDetail().add(rectificationDetailTarget);
			
			if(rectificationRequestTO.getBalanceType().equals(ComponentConstant.ONE)){
				
				rectificationDetailTarget.setIndBlockDetail(ComponentConstant.ONE);
				
				for(BlockOperationTransfer tempBlock: securitiesTransferOperation.getBlockOperationTransfer()){
					if(tempBlock.getOriginalBlockBalance().compareTo(tempBlock.getRefBlockOperation().getOriginalBlockBalance()) != 0){
						throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_BLOCK_INVALID);
					}
					RectificationDetail	srcRectificationDetail = new RectificationDetail();
					srcRectificationDetail.setHolderAccount(securitiesTransferOperation.getTargetHolderAccount());
					srcRectificationDetail.setParticipant(securitiesTransferOperation.getTargetParticipant());
					srcRectificationDetail.setSecurities(securitiesTransferOperation.getSecurities());
					srcRectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
					srcRectificationDetail.setRectificationOperation(rectificationOperation);
					srcRectificationDetail.setQuantity(tempBlock.getOriginalBlockBalance());
					srcRectificationDetail.setBlockOperation(tempBlock.getRefBlockOperation());
					srcRectificationDetail.setIndBlockDetail(ComponentConstant.ONE);
					srcRectificationDetail.setRefRectificationDetail(rectificationDetailTarget);
					
					if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
						srcRectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
						for (BlockMarketFactOperation blockMarketfact : tempBlock.getBlockOperation().getBlockMarketFactOperations()) {
							RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
							rectificationMarketFact.setMarketDate(blockMarketfact.getMarketDate());
							rectificationMarketFact.setMarketRate(blockMarketfact.getMarketRate());
							rectificationMarketFact.setMarketPrice(blockMarketfact.getMarketPrice());
							rectificationMarketFact.setOperationQuantity(blockMarketfact.getOriginalBlockBalance());
							rectificationMarketFact.setRectificationDetail(srcRectificationDetail);
							srcRectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
						}
					}
					
					rectificationDetailTarget.getRectificationDetails().add(srcRectificationDetail);
				}
				
			}else{
				
				rectificationDetailTarget.setIndBlockDetail(ComponentConstant.ZERO);
				
				RectificationDetail srcRectificationDetail = new RectificationDetail();
				srcRectificationDetail.setHolderAccount(securitiesTransferOperation.getTargetHolderAccount());
				srcRectificationDetail.setParticipant(securitiesTransferOperation.getTargetParticipant());
				srcRectificationDetail.setSecurities(securitiesTransferOperation.getSecurities());
				srcRectificationDetail.setQuantity(securitiesTransferOperation.getQuantityOperation());
				srcRectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
				srcRectificationDetail.setRectificationOperation(rectificationOperation);
				srcRectificationDetail.setRefRectificationDetail(rectificationDetailTarget);
				
				if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
					srcRectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
					for (SecurityTransferMarketfact securityTransferMarketfact : securitiesTransferOperation.getSecurityTransferOperationnMarketFact()) {
						RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
						rectificationMarketFact.setMarketDate(securityTransferMarketfact.getMarketDate());
						rectificationMarketFact.setMarketRate(securityTransferMarketfact.getMarketRate());
						rectificationMarketFact.setMarketPrice(securityTransferMarketfact.getMarketPrice());
						rectificationMarketFact.setOperationQuantity(securityTransferMarketfact.getQuantityOperation());
						rectificationMarketFact.setRectificationDetail(srcRectificationDetail);
						srcRectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
					}
				}
				
				rectificationDetailTarget.getRectificationDetails().add(srcRectificationDetail);
			}
			
			rectificationOperation.setRequestType(rectificationRequestTO.getRequestType());
			rectificationRequestTO.setRectificationOperation(rectificationOperation);
			rectificationRequestTO.setSecurityTransferOperation(securitiesTransferOperation);
			
		} else {
			rectificationRequestTO.setSecurityTransferOperation(null);
			throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_FOUND, 
					new Object[]{rectificationRequestTO.getOperationNumberRef().toString()});
		}

	}

	/**
	 * Gets the change ownership and build rectification.
	 *
	 * @param rectificationRequestTO the rectification request to
	 * @return the change ownership and build rectification
	 * @throws ServiceException the service exception
	 */
	public void getChangeOwnershipAndBuildRectification(RectificationRequestTO rectificationRequestTO) throws ServiceException {
		
		ChangeOwnershipDetailTO filter = new ChangeOwnershipDetailTO();
		filter.setOperationNumber(rectificationRequestTO.getOperationNumberRef());
		filter.setListOperationType(rectificationRequestTO.getListOperationType());
		filter.setIdBalanceType(rectificationRequestTO.getBalanceType());
		filter.setRegisterDate(rectificationRequestTO.getOperationDate());
		
		ChangeOwnershipOperation changeOwnerShipOperation = changeOwnershipServiceFacade.getChangeOwnershipOperationByFilters(filter);
		RectificationOperation rectificationOperation = rectificationRequestTO.getRectificationOperation();
		
		if(Validations.validateIsNotNull(changeOwnerShipOperation)){
			
			if(changeOwnerShipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){
				rectificationRequestTO.setChangeOwnerShipOperation(null);
				throw new ServiceException(ErrorServiceType.RECTIFICATION_OWNERSHIP_EXCHANGE_SUCESION_NOT_VALID);
			}
			
			if(!changeOwnerShipOperation.getState().equals(ChangeOwnershipStatusType.CONFIRMADO.getCode())){
				rectificationRequestTO.setChangeOwnerShipOperation(null);
				throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_CONFIRMED ,new Object[]{filter.getOperationNumber().toString()});
			}
			
			rectificationOperation.setRectifiedOperation(changeOwnerShipOperation.getCustodyOperation());
			if(changeOwnerShipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){	
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_CHANGE_OWNERSHIP_AVAILABLE.getCode());
			}else if(changeOwnerShipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_CHANGE_OWNERSHIP_BLOCK.getCode());
			}
			
			for(ChangeOwnershipDetail detail: changeOwnerShipOperation.getChangeOwnershipDetails()){	

				RectificationDetail rectificationDetail = new RectificationDetail();
				rectificationDetail.setIndSourceDestination(ComponentConstant.TARGET);
				rectificationDetail.setHolderAccount(detail.getHolderAccount());
				rectificationDetail.setParticipant(detail.getParticipant());
				rectificationDetail.setSecurities(detail.getSecurity());
				rectificationDetail.setRectificationOperation(rectificationOperation);
				rectificationDetail.setQuantity(detail.getAvailableBalance());
				rectificationDetail.setRectificationDetails(new ArrayList<RectificationDetail>());
				rectificationDetail.setIndBlockDetail(ComponentConstant.ZERO);
				rectificationOperation.getRectificationDetail().add(rectificationDetail);
				
				if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
					rectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
					for (ChangeOwnershipMarketFact changeOwnershipMarketFact : detail.getChangeOwnershipMarketFacts()) {
						RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
						rectificationMarketFact.setOperationQuantity(changeOwnershipMarketFact.getMarketBalance());
						rectificationMarketFact.setMarketPrice(changeOwnershipMarketFact.getMarketPrice());
						rectificationMarketFact.setMarketRate(changeOwnershipMarketFact.getMarketRate());
						rectificationMarketFact.setMarketDate(changeOwnershipMarketFact.getMarketDate());
						rectificationMarketFact.setRectificationDetail(rectificationDetail);
						rectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
					}
				}
				
				if(filter.getIdBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
					
					rectificationDetail.setIndBlockDetail(ComponentConstant.ZERO);
					
					if(detail.getTargetChangeOwnershipDetails()!=null){
						
						for(ChangeOwnershipDetail detail2: detail.getTargetChangeOwnershipDetails()){
							RectificationDetail srcRectificationDetail = new RectificationDetail();
							srcRectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
							srcRectificationDetail.setHolderAccount(detail2.getHolderAccount());
							srcRectificationDetail.setParticipant(detail2.getParticipant());
							srcRectificationDetail.setSecurities(detail2.getSecurity());
							srcRectificationDetail.setRectificationOperation(rectificationOperation);
							srcRectificationDetail.setQuantity(detail2.getAvailableBalance());
							srcRectificationDetail.setRefRectificationDetail(rectificationDetail);
							rectificationDetail.getRectificationDetails().add(srcRectificationDetail);
							
							if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
								srcRectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
								for (ChangeOwnershipMarketFact changeOwnershipMarketFact : detail2.getChangeOwnershipMarketFacts()) {
									RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
									rectificationMarketFact.setOperationQuantity(changeOwnershipMarketFact.getMarketBalance());
									rectificationMarketFact.setMarketPrice(changeOwnershipMarketFact.getMarketPrice());
									rectificationMarketFact.setMarketRate(changeOwnershipMarketFact.getMarketRate());
									rectificationMarketFact.setMarketDate(changeOwnershipMarketFact.getMarketDate());
									rectificationMarketFact.setRectificationDetail(srcRectificationDetail);
									srcRectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
								}
							}
							
						}					
					}
					
				}else if(filter.getIdBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
					
					rectificationDetail.setQuantity(BigDecimal.ZERO);
					rectificationDetail.setIndBlockDetail(ComponentConstant.ONE);
					
					if(detail.getBlockChangeOwnerships()!=null){
						
						for(BlockChangeOwnership blockDetail: detail.getBlockChangeOwnerships()){							
							
							if(blockDetail.getBlockQuantity().compareTo(blockDetail.getRefBlockOperation().getOriginalBlockBalance()) != 0){
								throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_BLOCK_INVALID);
							}
							
							RectificationDetail srcBlockRectificationDetail = new RectificationDetail();
							srcBlockRectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
							srcBlockRectificationDetail.setHolderAccount(blockDetail.getHolderAccount());
							srcBlockRectificationDetail.setParticipant(blockDetail.getParticipant());
							srcBlockRectificationDetail.setSecurities(blockDetail.getChangeOwnershipDetail().getSecurity());
							srcBlockRectificationDetail.setBlockOperation(blockDetail.getRefBlockOperation());
							srcBlockRectificationDetail.setRectificationOperation(rectificationOperation);
							srcBlockRectificationDetail.setQuantity(blockDetail.getBlockQuantity());
							srcBlockRectificationDetail.setRefRectificationDetail(rectificationDetail);
							srcBlockRectificationDetail.setIndBlockDetail(ComponentConstant.ONE);
							rectificationDetail.getRectificationDetails().add(srcBlockRectificationDetail);
							
							if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
								srcBlockRectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
								for (ChangeOwnershipMarketFact changeOwnershipMarketFact : blockDetail.getChangeOwnershipMarketFacts()) {
									RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
									rectificationMarketFact.setOperationQuantity(changeOwnershipMarketFact.getMarketBalance());
									rectificationMarketFact.setMarketPrice(changeOwnershipMarketFact.getMarketPrice());
									rectificationMarketFact.setMarketRate(changeOwnershipMarketFact.getMarketRate());
									rectificationMarketFact.setMarketDate(changeOwnershipMarketFact.getMarketDate());
									rectificationMarketFact.setRectificationDetail(srcBlockRectificationDetail);
									srcBlockRectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
								}
							}
							
							rectificationDetail.setQuantity(rectificationDetail.getQuantity().add(srcBlockRectificationDetail.getQuantity()));
							
						}
					}	
					

				}
			}
			
			rectificationOperation.setRequestType(rectificationRequestTO.getRequestType());
			rectificationRequestTO.setRectificationOperation(rectificationOperation);
			rectificationRequestTO.setChangeOwnerShipOperation(changeOwnerShipOperation);
		}else {
			rectificationRequestTO.setChangeOwnerShipOperation(null);
			throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_FOUND, 
					new Object[]{rectificationRequestTO.getOperationNumberRef().toString()});
		}
		
	}

	/**
	 * Gets the account annotation and build rectification.
	 *
	 * @param rectificationRequestTO the rectification request to
	 * @return the account annotation and build rectification
	 * @throws ServiceException the service exception
	 */
	public void getAccountAnnotationAndBuildRectification(RectificationRequestTO rectificationRequestTO) throws ServiceException {
		
		RectificationOperation rectificationOperation = rectificationRequestTO.getRectificationOperation();

		SearchAccountAnnotationTO annotationSearchTO = new SearchAccountAnnotationTO();
		annotationSearchTO.setRequestNumber(rectificationRequestTO.getOperationNumberRef());
		annotationSearchTO.setListOperationTypePk(rectificationRequestTO.getListOperationType());
		annotationSearchTO.setOperationDate(rectificationRequestTO.getOperationDate());
		
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotationOperationByFilter(annotationSearchTO );
		
		if(Validations.validateIsNotNull(accountAnnotationOperation)){
			
			rectificationOperation.setRectifiedOperation(accountAnnotationOperation.getCustodyOperation());
			rectificationOperation.setRectificationDetail(new ArrayList<RectificationDetail>());
			if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_DEMATERIALIZATION.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_DEMATERIALIZATION.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_DONATION.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_DONATION.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_SUSCRIPTION.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_SUSPCRITION.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_AUCTION_PURCHASE.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_AUCTION.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_DIRECT_PURCHASE_BCB.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_DIRECT_SALE_BCB.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_DIRECT_PURCHASE_TGN.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_DIRECT_SALE_TGN.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_MONEY_PURCHASE.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_MONEY_PURCHASE.getCode());
			}else if(accountAnnotationOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_DIRECT_PURCHASE_PRIVATE.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_PRIVATE_SALE.getCode());
			}
			
			RectificationDetail rectificationDetail = new RectificationDetail();
			rectificationDetail.setHolderAccount(accountAnnotationOperation.getHolderAccount());
			rectificationDetail.setSecurities(accountAnnotationOperation.getSecurity());
			rectificationDetail.setParticipant(accountAnnotationOperation.getHolderAccount().getParticipant());
			rectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
			rectificationDetail.setQuantity(accountAnnotationOperation.getTotalBalance());
			rectificationDetail.setRectificationOperation(rectificationOperation);
			rectificationDetail.setIndBlockDetail(ComponentConstant.ZERO);
			rectificationOperation.getRectificationDetail().add(rectificationDetail);
			
			if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
				rectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
				for(AccountAnnotationMarketFact accountAnnotationMarketFact : accountAnnotationOperation.getAccountAnnotationMarketFact()){
					RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();					
					rectificationMarketFact.setMarketDate(accountAnnotationMarketFact.getMarketDate());
					rectificationMarketFact.setMarketPrice(accountAnnotationMarketFact.getMarketPrice());
					rectificationMarketFact.setMarketRate(accountAnnotationMarketFact.getMarketRate());
					rectificationMarketFact.setOperationQuantity(accountAnnotationMarketFact.getOperationQuantity());
					rectificationMarketFact.setRectificationDetail(rectificationDetail);
					rectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
				}
			}
			
			rectificationOperation.setRequestType(rectificationRequestTO.getRequestType());
			rectificationRequestTO.setRectificationOperation(rectificationOperation);
			rectificationRequestTO.setAccountAnnotationOperation(accountAnnotationOperation);
		}else{
			rectificationRequestTO.setAccountAnnotationOperation(null);
			throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_FOUND, 
					new Object[]{rectificationRequestTO.getOperationNumberRef().toString()});
		}
		
	}

	/**
	 * Gets the retirement operation and build rectification.
	 *
	 * @param rectificationRequestTO the rectification request to
	 * @return the retirement operation and build rectification
	 * @throws ServiceException the service exception
	 */
	public void getRetirementOperationAndBuildRectification(RectificationRequestTO rectificationRequestTO) throws ServiceException {
		
		RectificationOperation rectificationOperation = rectificationRequestTO.getRectificationOperation();
		
		RetirementOperationTO retirementOperationTO = new RetirementOperationTO();
		retirementOperationTO.setOperationNumber(rectificationRequestTO.getOperationNumberRef());
		retirementOperationTO.setLstOperationType(rectificationRequestTO.getListOperationType());
		retirementOperationTO.setOperationDate(rectificationRequestTO.getOperationDate());
		
		RetirementOperation retirementOperation = getRetirementOperation(retirementOperationTO);
		
		if(Validations.validateIsNotNull(retirementOperation)){
			
			rectificationOperation.setRectifiedOperation(retirementOperation.getCustodyOperation());
			rectificationOperation.setRectificationDetail(new ArrayList<RectificationDetail>());
			if(retirementOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_REDEMPTION_FIXED_INCOME.getCode())){
				if(retirementOperation.getTargetSecurity()!=null){
					rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode());
				}else{
					rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_FIXED_INCOME_SRC.getCode());
				}
				
			}else if(retirementOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_REDEMPTION_EQUITIES.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_REDEMPTION_VARIABLE_INCOME.getCode());
			}else if(retirementOperation.getCustodyOperation().getOperationType().equals(ParameterOperationType.SECURITIES_REVERSION.getCode())){
				rectificationOperation.getCustodyOperation().setOperationType(ParameterOperationType.RECTIFICATION_SECURITIES_REVERSION.getCode());
			}
			
			for (RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()) {
				
				if(retirementDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
					
					RectificationDetail rectificationDetail = new RectificationDetail();
					rectificationDetail.setHolderAccount(retirementDetail.getHolderAccount());
					rectificationDetail.setSecurities(retirementOperation.getSecurity());
					rectificationDetail.setParticipant(retirementDetail.getParticipant());
					rectificationDetail.setRectificationOperation(rectificationOperation);
					rectificationDetail.setQuantity(retirementDetail.getAvailableBalance());
					
					if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
						rectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
						for (RetirementMarketfact retirementMarketfact : retirementDetail.getRetirementMarketfacts()) {
							RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
							rectificationMarketFact.setOperationQuantity(retirementMarketfact.getOperationQuantity());
							rectificationMarketFact.setMarketPrice(retirementMarketfact.getMarketPrice());
							rectificationMarketFact.setMarketRate(retirementMarketfact.getMarketRate());
							rectificationMarketFact.setMarketDate(retirementMarketfact.getMarketDate());
							rectificationMarketFact.setRectificationDetail(rectificationDetail);
							rectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
						}
					}
					
					if(retirementOperation.getTargetSecurity()!=null){
						
						rectificationDetail.setIndSourceDestination(ComponentConstant.TARGET);
						rectificationDetail.setRectificationDetails(new ArrayList<RectificationDetail>());
						
						RectificationDetail srcRectificationDetail = new RectificationDetail();
						srcRectificationDetail.setHolderAccount(retirementDetail.getHolderAccount());
						srcRectificationDetail.setSecurities(retirementOperation.getTargetSecurity());
						srcRectificationDetail.setParticipant(retirementDetail.getParticipant());
						srcRectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
						srcRectificationDetail.setQuantity(retirementDetail.getAvailableBalance());
						srcRectificationDetail.setRectificationOperation(rectificationOperation);
						srcRectificationDetail.setRefRectificationDetail(rectificationDetail);
						
						if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
							srcRectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
							for (RetirementMarketfact retirementMarketfact : retirementDetail.getRetirementMarketfacts()) {
								RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
								rectificationMarketFact.setOperationQuantity(retirementMarketfact.getOperationQuantity());
								rectificationMarketFact.setMarketPrice(retirementMarketfact.getMarketPrice());
								rectificationMarketFact.setMarketRate(retirementMarketfact.getMarketRate());
								rectificationMarketFact.setMarketDate(retirementMarketfact.getMarketDate());
								rectificationMarketFact.setRectificationDetail(srcRectificationDetail);
								srcRectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
							}
						}
						
						rectificationDetail.getRectificationDetails().add(srcRectificationDetail);
					}else{
						rectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
					}
					
					rectificationOperation.getRectificationDetail().add(rectificationDetail);
					
				}else if(retirementDetail.getBanBalance().compareTo(BigDecimal.ZERO) > 0 || 
						retirementDetail.getPawnBalance().compareTo(BigDecimal.ZERO) > 0 || 
							retirementDetail.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){
					
					RectificationDetail rectificationDetail = new RectificationDetail();
					rectificationDetail.setHolderAccount(retirementDetail.getHolderAccount());
					rectificationDetail.setSecurities(retirementOperation.getSecurity());
					rectificationDetail.setParticipant(retirementDetail.getParticipant());
					rectificationDetail.setRectificationOperation(rectificationOperation);
					rectificationDetail.setIndBlockDetail(ComponentConstant.ONE);
					rectificationDetail.setRectificationDetails(new ArrayList<RectificationDetail>());
					rectificationDetail.setQuantity(BigDecimal.ZERO);
					rectificationDetail.setIndSourceDestination(ComponentConstant.TARGET);
					
					for(RetirementBlockDetail retirementBlockDetail : retirementDetail.getRetirementBlockDetails()){
						if(retirementBlockDetail.getBlockQuantity().compareTo(retirementBlockDetail.getBlockOperation().getOriginalBlockBalance()) != 0){
							throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_BLOCK_INVALID);
						}
						
						RectificationDetail srcRectificationDetail = new RectificationDetail();
						srcRectificationDetail.setHolderAccount(retirementDetail.getHolderAccount());
						srcRectificationDetail.setSecurities(retirementOperation.getTargetSecurity());
						srcRectificationDetail.setParticipant(retirementDetail.getParticipant());
						srcRectificationDetail.setIndSourceDestination(ComponentConstant.SOURCE);
						srcRectificationDetail.setIndBlockDetail(ComponentConstant.ONE);
						srcRectificationDetail.setBlockOperation(retirementBlockDetail.getBlockOperation());
						srcRectificationDetail.setQuantity(retirementBlockDetail.getBlockOperation().getOriginalBlockBalance());
						srcRectificationDetail.setRectificationOperation(rectificationOperation);
						srcRectificationDetail.setRefRectificationDetail(rectificationDetail);
						
						if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
							srcRectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
							for (BlockMarketFactOperation blockMarketfactOp : srcRectificationDetail.getBlockOperation().getBlockMarketFactOperations()) {
								RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
								rectificationMarketFact.setOperationQuantity(blockMarketfactOp.getOriginalBlockBalance());
								rectificationMarketFact.setMarketPrice(blockMarketfactOp.getMarketPrice());
								rectificationMarketFact.setMarketRate(blockMarketfactOp.getMarketRate());
								rectificationMarketFact.setMarketDate(blockMarketfactOp.getMarketDate());
								rectificationMarketFact.setRectificationDetail(srcRectificationDetail);
								srcRectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
							}
							
							if(rectificationDetail.getRectificationMarketFacts()==null){
								rectificationDetail.setRectificationMarketFacts(new ArrayList<RectificationMarketFact>());
							}
							
							for (BlockMarketFactOperation blockMarketfactOp : srcRectificationDetail.getBlockOperation().getBlockMarketFactOperations()) {
								RectificationMarketFact rectificationMarketFact = new RectificationMarketFact();
								rectificationMarketFact.setOperationQuantity(blockMarketfactOp.getOriginalBlockBalance());
								rectificationMarketFact.setMarketPrice(blockMarketfactOp.getMarketPrice());
								rectificationMarketFact.setMarketRate(blockMarketfactOp.getMarketRate());
								rectificationMarketFact.setMarketDate(blockMarketfactOp.getMarketDate());
								rectificationMarketFact.setRectificationDetail(rectificationDetail);
								rectificationDetail.getRectificationMarketFacts().add(rectificationMarketFact);
							}
						}
						
						rectificationDetail.setQuantity(rectificationDetail.getQuantity().add(srcRectificationDetail.getQuantity()));
						rectificationDetail.getRectificationDetails().add(srcRectificationDetail);
					}
					
					rectificationOperation.getRectificationDetail().add(rectificationDetail);
					
				}
				
			}
			
			rectificationOperation.setRequestType(rectificationRequestTO.getRequestType());
			rectificationRequestTO.setRectificationOperation(rectificationOperation);
			rectificationRequestTO.setRetirementOperation(retirementOperation);
			
		}else{
			rectificationRequestTO.setRetirementOperation(null);
			throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_FOUND, 
					new Object[]{rectificationRequestTO.getOperationNumberRef().toString()});
		}
		
	}
	
	
	/**
	 * Find account annotation operation by filter.
	 * Buscar el ACV
	 * @param filer the filer
	 * @return the account annotation operation
	 * @throws ServiceException the service exception
	 */
	public AccountAnnotationOperation findAccountAnnotationOperationByFilter(SearchAccountAnnotationTO filer)throws ServiceException{
		AccountAnnotationOperation aAnnotationOperation =  dematerializationCertificateServiceBean.findAccountAnnotationOperationByFilter(filer);
		if(aAnnotationOperation!=null){
			
			if(!aAnnotationOperation.getState().equals(DematerializationStateType.CONFIRMED.getCode())){
				throw new ServiceException(ErrorServiceType.RECTIFICATION_CUSTODY_OPERATION_NOT_CONFIRMED ,new Object[] {filer.getRequestNumber().toString()});
			}
			
			for(HolderAccountDetail holderAccountDetail : aAnnotationOperation.getHolderAccount().getHolderAccountDetails()){
				if(holderAccountDetail.getIndRepresentative().equals(ComponentConstant.ONE)){
					aAnnotationOperation.setHolder(holderAccountDetail.getHolder());
				}
				holderAccountDetail.getHolderAccount().getParticipant().getIdParticipantPk();
				holderAccountDetail.getHolder().getIdHolderPk();
			}
		}
		return aAnnotationOperation;
	}

}
