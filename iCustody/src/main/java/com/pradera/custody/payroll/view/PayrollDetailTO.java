package com.pradera.custody.payroll.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;

public class PayrollDetailTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long idCustodyOperationCertificatePk;
	private Long idCustodyOperationPk;
	private Long idCorporativeOperationPk;
	private Integer operationType;
	private String operationTypeDesc;	
	private Date registryDate;
	private Date confirmDate;
	private HolderAccount holderAccount;
	private String idSecurityCodePk;	
	private BigDecimal quantity;
	private Integer indSerializable;
	private Long idPayrollDetailPk;
	private Integer state;
	private String stateDesc;
	private byte[] documentFile;
	private String fileName;
	private Employees employee;
	private Employees employee2;
	private boolean blAnalyst1;
	private boolean blAnalyst2;
	private boolean blSelected;
	private Integer idVaultPk;
	private String vaultName;
	private String comments;
	private String certificatesRelated;
	private Long idPhysicalCertificatePk;
	private String serialCode;
	
	private String mnemonicParticipant;
	private String mnemonicIssuer;
	private Integer branchOffice;
	private String branchOfficeDesc;
	private Integer vaultLocation;
	private String vaultLocationDesc;
	private Long idPayrollHeaderPk;
	private String certificateNumber;
	private Date entryDate;
	private Date exitDate;
	private Integer situation;
	private String situationDesc;
	private Integer physicCuponCount=0;
	private Integer cuponNumber;
	private boolean blIsCupon;
	private BigDecimal securityNominalValue;
	private String participantNemonic;

	private String currencyDesc;
	private String issuerDesc;
	private Long idAnnotationOperationPk;
	private Integer motive;
	private String bovedaPath;
	private Date expirationDate;

	private Integer payRollType;
	private String payRollTypeDesc;
	
	private Integer indReenter;
	private Date lastReenterDate;

	private String annotationMotiveDescription;
	private Integer annotationMotive;
	
	private List<AccountAnnotationCupon> lstAccountAnnotationCupon;
	private Integer cuponQuantity;
	
	private String fromToCupons;
	private String positionInVault;
	
	private Integer certificateFrom;
	private Integer certificateTo;
	private BigDecimal nominalValue;
	private String securityClassDesc;

	private BigDecimal totalBalance = BigDecimal.ZERO;
	private BigDecimal availableBalance = BigDecimal.ZERO;
	private BigDecimal transitBalance = BigDecimal.ZERO;
	
	private Integer totalStock;
	private Integer detailCuponQuantity;

	private Integer payrollHeadTotalStock;
	private AccountAnnotationCupon accountAnnotationCupon;
	private RetirementDetail retirementDetail;
	private List<RetirementDetail> lstRetirementDetailVault;
	private BigDecimal nominalAmount;

	private Integer motiveDepRetPayroll;
	private String descMotiveDepRetPayroll;

	private Integer indCupon;
	private Long idProgramInterestPk;
	private Integer indCorporativeOperationPaid;
	private Integer motiveRetirement;
	private Boolean indIsRV;
	private Integer cuponQuantityReport;
	private Integer totalStockReport;
	
	public Long getIdCustodyOperationPk() {
		return idCustodyOperationPk;
	}
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}
	public Long getIdCorporativeOperationPk() {
		return idCorporativeOperationPk;
	}
	public void setIdCorporativeOperationPk(Long idCorporativeOperationPk) {
		this.idCorporativeOperationPk = idCorporativeOperationPk;
	}
	public Integer getOperationType() {
		return operationType;
	}
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}
	public String getOperationTypeDesc() {
		return operationTypeDesc;
	}
	public void setOperationTypeDesc(String operationTypeDesc) {
		this.operationTypeDesc = operationTypeDesc;
	}
	public Date getRegistryDate() {
		return registryDate;
	}
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	public Date getConfirmDate() {
		return confirmDate;
	}
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public boolean isBlSelected() {
		return blSelected;
	}
	public void setBlSelected(boolean blSelected) {
		this.blSelected = blSelected;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public Integer getIndSerializable() {
		return indSerializable;
	}
	public void setIndSerializable(Integer indSerializable) {
		this.indSerializable = indSerializable;
	}
	public Long getIdPayrollDetailPk() {
		return idPayrollDetailPk;
	}
	public void setIdPayrollDetailPk(Long idPayrollDetailPk) {
		this.idPayrollDetailPk = idPayrollDetailPk;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getStateDesc() {
		return stateDesc;
	}
	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}
	public byte[] getDocumentFile() {
		return documentFile;
	}
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Employees getEmployee() {
		return employee;
	}
	public void setEmployee(Employees employee) {
		this.employee = employee;
	}
	public boolean isBlAnalyst1() {
		return blAnalyst1;
	}
	public void setBlAnalyst1(boolean blAnalyst1) {
		this.blAnalyst1 = blAnalyst1;
	}
	public boolean isBlAnalyst2() {
		return blAnalyst2;
	}
	public void setBlAnalyst2(boolean blAnalyst2) {
		this.blAnalyst2 = blAnalyst2;
	}
	public Employees getEmployee2() {
		return employee2;
	}
	public void setEmployee2(Employees employee2) {
		this.employee2 = employee2;
	}
	public Integer getIdVaultPk() {
		return idVaultPk;
	}
	public void setIdVaultPk(Integer idVaultPk) {
		this.idVaultPk = idVaultPk;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Long getIdPayrollHeaderPk() {
		return idPayrollHeaderPk;
	}
	public void setIdPayrollHeaderPk(Long idPayrollHeaderPk) {
		this.idPayrollHeaderPk = idPayrollHeaderPk;
	}
	public Integer getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
	public String getVaultName() {
		return vaultName;
	}
	public void setVaultName(String vaultName) {
		this.vaultName = vaultName;
	}
	public String getMnemonicParticipant() {
		return mnemonicParticipant;
	}
	public void setMnemonicParticipant(String mnemonicParticipant) {
		this.mnemonicParticipant = mnemonicParticipant;
	}
	public String getMnemonicIssuer() {
		return mnemonicIssuer;
	}
	public void setMnemonicIssuer(String mnemonicIssuer) {
		this.mnemonicIssuer = mnemonicIssuer;
	}
	public String getBranchOfficeDesc() {
		return branchOfficeDesc;
	}
	public void setBranchOfficeDesc(String branchOfficeDesc) {
		this.branchOfficeDesc = branchOfficeDesc;
	}
	public Integer getVaultLocation() {
		return vaultLocation;
	}
	public void setVaultLocation(Integer vaultLocation) {
		this.vaultLocation = vaultLocation;
	}
	public String getVaultLocationDesc() {
		return vaultLocationDesc;
	}
	public void setVaultLocationDesc(String vaultLocationDesc) {
		this.vaultLocationDesc = vaultLocationDesc;
	}
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	public Date getExitDate() {
		return exitDate;
	}
	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public Long getIdPhysicalCertificatePk() {
		return idPhysicalCertificatePk;
	}
	public void setIdPhysicalCertificatePk(Long idPhysicalCertificatePk) {
		this.idPhysicalCertificatePk = idPhysicalCertificatePk;
	}
	public Integer getSituation() {
		return situation;
	}
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	public String getSituationDesc() {
		return situationDesc;
	}
	public void setSituationDesc(String situationDesc) {
		this.situationDesc = situationDesc;
	}
	public String getCertificatesRelated() {
		return certificatesRelated;
	}
	public void setCertificatesRelated(String certificatesRelated) {
		this.certificatesRelated = certificatesRelated;
	}
	public Integer getPhysicCuponCount() {
		return physicCuponCount;
	}
	public void setPhysicCuponCount(Integer physicCuponCount) {
		this.physicCuponCount = physicCuponCount;
	}
	public List<AccountAnnotationCupon> getLstAccountAnnotationCupon() {
		return lstAccountAnnotationCupon;
	}
	public void setLstAccountAnnotationCupon(List<AccountAnnotationCupon> lstAccountAnnotationCupon) {
		this.lstAccountAnnotationCupon = lstAccountAnnotationCupon;
	}
	public Integer getCuponQuantity() {
		return cuponQuantity;
	}
	public void setCuponQuantity(Integer cuponQuantity) {
		this.cuponQuantity = cuponQuantity;
	}
	public boolean isBlIsCupon() {
		return blIsCupon;
	}
	public void setBlIsCupon(boolean blIsCupon) {
		this.blIsCupon = blIsCupon;
	}
	public Integer getCuponNumber() {
		return cuponNumber;
	}
	public void setCuponNumber(Integer cuponNumber) {
		this.cuponNumber = cuponNumber;
	}
	public String getSerialCode() {
		return serialCode;
	}
	public void setSerialCode(String serialCode) {
		this.serialCode = serialCode;
	}
	public BigDecimal getSecurityNominalValue() {
		return securityNominalValue;
	}
	public void setSecurityNominalValue(BigDecimal securityNominalValue) {
		this.securityNominalValue = securityNominalValue;
	}
	public String getCurrencyDesc() {
		return currencyDesc;
	}
	public void setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
	}
	public String getIssuerDesc() {
		return issuerDesc;
	}
	public void setIssuerDesc(String issuerDesc) {
		this.issuerDesc = issuerDesc;
	}
	public Long getIdAnnotationOperationPk() {
		return idAnnotationOperationPk;
	}
	public void setIdAnnotationOperationPk(Long idAnnotationOperationPk) {
		this.idAnnotationOperationPk = idAnnotationOperationPk;
	}
	public String getParticipantNemonic() {
		return participantNemonic;
	}
	public void setParticipantNemonic(String participantNemonic) {
		this.participantNemonic = participantNemonic;
	}
	public Integer getMotive() {
		return motive;
	}
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	public String getBovedaPath() {
		return bovedaPath;
	}
	public void setBovedaPath(String bovedaPath) {
		this.bovedaPath = bovedaPath;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Integer getPayRollType() {
		return payRollType;
	}
	public void setPayRollType(Integer payRollType) {
		this.payRollType = payRollType;
	}
	public String getPayRollTypeDesc() {
		return payRollTypeDesc;
	}
	public void setPayRollTypeDesc(String payRollTypeDesc) {
		this.payRollTypeDesc = payRollTypeDesc;
	}
	public Long getIdCustodyOperationCertificatePk() {
		return idCustodyOperationCertificatePk;
	}
	public void setIdCustodyOperationCertificatePk(Long idCustodyOperationCertificatePk) {
		this.idCustodyOperationCertificatePk = idCustodyOperationCertificatePk;
	}
	public Integer getIndReenter() {
		return indReenter;
	}
	public void setIndReenter(Integer indReenter) {
		this.indReenter = indReenter;
	}
	public Date getLastReenterDate() {
		return lastReenterDate;
	}
	public void setLastReenterDate(Date lastReenterDate) {
		this.lastReenterDate = lastReenterDate;
	}
	public String getAnnotationMotiveDescription() {
		return annotationMotiveDescription;
	}
	public void setAnnotationMotiveDescription(String annotationMotiveDescription) {
		this.annotationMotiveDescription = annotationMotiveDescription;
	}
	public Integer getAnnotationMotive() {
		return annotationMotive;
	}
	public void setAnnotationMotive(Integer annotationMotive) {
		this.annotationMotive = annotationMotive;
	}
	public String getFromToCupons() {
		return fromToCupons;
	}
	public void setFromToCupons(String fromToCupons) {
		this.fromToCupons = fromToCupons;
	}
	public String getPositionInVault() {
		return positionInVault;
	}
	public void setPositionInVault(String positionInVault) {
		this.positionInVault = positionInVault;
	}
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	public BigDecimal getTransitBalance() {
		return transitBalance;
	}
	public void setTransitBalance(BigDecimal transitBalance) {
		this.transitBalance = transitBalance;
	}
	public Integer getCertificateFrom() {
		return certificateFrom;
	}
	public void setCertificateFrom(Integer certificateFrom) {
		this.certificateFrom = certificateFrom;
	}
	public Integer getCertificateTo() {
		return certificateTo;
	}
	public void setCertificateTo(Integer certificateTo) {
		this.certificateTo = certificateTo;
	}
	public BigDecimal getNominalValue() {
		return nominalValue;
	}
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}
	public String getSecurityClassDesc() {
		return securityClassDesc;
	}
	public void setSecurityClassDesc(String securityClassDesc) {
		this.securityClassDesc = securityClassDesc;
	}
	public Integer getPayrollHeadTotalStock() {
		return payrollHeadTotalStock;
	}
	public void setPayrollHeadTotalStock(Integer payrollHeadTotalStock) {
		this.payrollHeadTotalStock = payrollHeadTotalStock;
	}
	public AccountAnnotationCupon getAccountAnnotationCupon() {
		return accountAnnotationCupon;
	}
	public void setAccountAnnotationCupon(AccountAnnotationCupon accountAnnotationCupon) {
		this.accountAnnotationCupon = accountAnnotationCupon;
	}
	public RetirementDetail getRetirementDetail() {
		return retirementDetail;
	}
	public void setRetirementDetail(RetirementDetail retirementDetail) {
		this.retirementDetail = retirementDetail;
	}
	public List<RetirementDetail> getLstRetirementDetailVault() {
		return lstRetirementDetailVault;
	}
	public void setLstRetirementDetailVault(List<RetirementDetail> lstRetirementDetailVault) {
		this.lstRetirementDetailVault = lstRetirementDetailVault;
	}
	public Integer getTotalStock() {
		return totalStock;
	}
	public void setTotalStock(Integer totalStock) {
		this.totalStock = totalStock;
	}
	public BigDecimal getNominalAmount() {
		return nominalAmount;
	}
	public void setNominalAmount(BigDecimal nominalAmount) {
		this.nominalAmount = nominalAmount;
	}
	public Integer getMotiveDepRetPayroll() {
		return motiveDepRetPayroll;
	}
	public void setMotiveDepRetPayroll(Integer motiveDepRetPayroll) {
		this.motiveDepRetPayroll = motiveDepRetPayroll;
	}
	public String getDescMotiveDepRetPayroll() {
		return descMotiveDepRetPayroll;
	}
	public void setDescMotiveDepRetPayroll(String descMotiveDepRetPayroll) {
		this.descMotiveDepRetPayroll = descMotiveDepRetPayroll;
	}
	public Integer getDetailCuponQuantity() {
		return detailCuponQuantity;
	}
	public void setDetailCuponQuantity(Integer detailCuponQuantity) {
		this.detailCuponQuantity = detailCuponQuantity;
	}
	public Long getIdProgramInterestPk() {
		return idProgramInterestPk;
	}
	public void setIdProgramInterestPk(Long idProgramInterestPk) {
		this.idProgramInterestPk = idProgramInterestPk;
	}
	public Integer getIndCupon() {
		return indCupon;
	}
	public void setIndCupon(Integer indCupon) {
		this.indCupon = indCupon;
	}
	public Integer getIndCorporativeOperationPaid() {
		return indCorporativeOperationPaid;
	}
	public void setIndCorporativeOperationPaid(Integer indCorporativeOperationPaid) {
		this.indCorporativeOperationPaid = indCorporativeOperationPaid;
	}
	public Integer getMotiveRetirement() {
		return motiveRetirement;
	}
	public void setMotiveRetirement(Integer motiveRetirement) {
		this.motiveRetirement = motiveRetirement;
	}
	public Boolean getIndIsRV() {
		return indIsRV;
	}
	public void setIndIsRV(Boolean indIsRV) {
		this.indIsRV = indIsRV;
	}
	public Integer getCuponQuantityReport() {
		return cuponQuantityReport;
	}
	public void setCuponQuantityReport(Integer cuponQuantityReport) {
		this.cuponQuantityReport = cuponQuantityReport;
	}
	public Integer getTotalStockReport() {
		return totalStockReport;
	}
	public void setTotalStockReport(Integer totalStockReport) {
		this.totalStockReport = totalStockReport;
	}
	
}
