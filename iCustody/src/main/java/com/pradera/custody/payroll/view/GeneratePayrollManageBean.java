package com.pradera.custody.payroll.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

//import org.jboss.solder.exception.control.ExceptionToCatch;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.payroll.facade.GeneratePayrollFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.process.BusinessProcess;

@DepositaryWebBean
@LoggerCreateBean
public class GeneratePayrollManageBean extends GenericBaseBean {
	
	private static final long serialVersionUID = 1L;
	private Map<Integer, String> mpStates, mpPayrollTypes, mpDetailStates, mpOperationTypes;
	private List<ParameterTable> lstStates, lstPayrollTypes, lstBranchOffices, lstMotives;
	private List<Participant> lstParticipants;
	private SearchPayrollTO searchPayrollTO;
	private PayrollHeader payrollHeader, selectedPayrollHeader;
	private SummaryPayrollTO summaryPayrollTO;
	private List<PayrollDetailTO> lstPayrollDetailTO;
	private final static String NEW_MAPPING = "registerPayroll";
	private final static String SEARCH_MAPPING = "searchPayroll";
	private final static String VIEW_MAPPING = "viewPayroll";
	private boolean blAnnul, blApprove, blNoResult;
	private String toogleText = "Marcar/Desmarcar Todos";
	private Boolean toogleFlag = false;
	@Inject
	UserPrivilege userPrivilege;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@Inject
	AccountsFacade accountsFacade;
	@Inject
	GeneratePayrollFacade generatePayrollFacade;
	@Inject
	UserInfo userInfo;

	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	private boolean blParticipant = false;
	private Long defaultParticipant = null;
	private Date currentSystemDate = CommonsUtilities.currentDate();
	
	@PostConstruct
	public void init(){
		try {
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				blParticipant = true;
				defaultParticipant = userInfo.getUserAccountSession().getParticipantCode();
			}
			userPrivilege.getUserAcctions();
			showPrivilegeButtons();
			fillMaps();
			searchPayrollTO = new SearchPayrollTO();
			searchPayrollTO.setIssuer(new Issuer());
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			lstParticipants = accountsFacade.getLisParticipantServiceBean(filterPar);
			
			if(blParticipant && defaultParticipant!=null){
				searchPayrollTO.setIdParticipantPk(defaultParticipant);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	@LoggerAuditWeb
	public void search(){
		try {
			userPrivilege.getPrivilegeComponent();
			userPrivilege.getUserAcctions();
			
			searchPayrollTO.setLstPayrollHeader(null);
			searchPayrollTO.setBlResults(false);
			selectedPayrollHeader = null;
			List<PayrollHeader> lstResult = generatePayrollFacade.searchPayrollHeaders(searchPayrollTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(PayrollHeader payrollHeader:lstResult){
					payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
					payrollHeader.setPayrollTypeDesc(mpPayrollTypes.get(payrollHeader.getPayrollType()));
				}
				searchPayrollTO.setLstPayrollHeader(new GenericDataModel<>(lstResult));
				searchPayrollTO.setBlResults(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void toogleDelivery() {
		if(payrollHeader.getIndDelivery() != null && payrollHeader.getIndDelivery().equals(1)) {//si es SI
			cleanDelivery();
		}else {
			payrollHeader.setIndDelivery(1);
			Participant participantSelected = accountsFacade.getParticipantByPk(payrollHeader.getParticipant().getIdParticipantPk());
			payrollHeader.setDeliveryAddress(participantSelected.getAddress());
			payrollHeader.setContactName("");
			payrollHeader.setMobileContact("");
		}
	}
	
	public void cleanDelivery() {
		payrollHeader.setIndDelivery(0);
		payrollHeader.setDeliveryAddress("");
		payrollHeader.setContactName("");
		payrollHeader.setMobileContact("");
	}
	
	
	public String back(){
		return SEARCH_MAPPING;
	}
	
	public void beforeSave(){
		boolean blSelected = false;
		for(PayrollDetailTO payrollDetailTO:lstPayrollDetailTO){
			if(payrollDetailTO.isBlSelected()){
				blSelected = true;
				break;
			}
		}
		if(!blSelected){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					"Debe seleccionar por lo menos un registro.");
			JSFUtilities.showSimpleValidationDialog();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					"¿Está seguro que desea generar la planilla?");			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
	}
	
	@LoggerAuditWeb
	public void generatePayroll(){
		try {
			Long idPayrollHeaderPk = generatePayrollFacade.generatePayroll(payrollHeader, lstPayrollDetailTO);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, "Se generó satisfactoriamente la planilla con N° " + idPayrollHeaderPk.toString() + ".");
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {			
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public String generate(){
		payrollHeader = new PayrollHeader();
		payrollHeader.setIssuer(new Issuer());
		payrollHeader.setParticipant(new Participant());
		payrollHeader.setRegisterDate(getCurrentSystemDate());
		blNoResult = false;
		lstPayrollDetailTO = null;
		
		if(blParticipant && defaultParticipant!=null){
			payrollHeader.getParticipant().setIdParticipantPk(defaultParticipant);
		}
		
		return NEW_MAPPING;
	}
	
	public void searchOperations(){
		try {
			blNoResult = true;
			lstPayrollDetailTO = generatePayrollFacade.searchOperationsForPayroll(payrollHeader);		
			if(Validations.validateListIsNotNullAndNotEmpty(lstPayrollDetailTO)){
				blNoResult = false;
				updateRecordSelected();
			}
			cleanDelivery();
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void updateRecordSelected() {

		BigDecimal yesRecords = BigDecimal.ZERO;
		BigDecimal noRecords = BigDecimal.ZERO;
		
		summaryPayrollTO = new SummaryPayrollTO();
		summaryPayrollTO.setTotalRecords(new BigDecimal(lstPayrollDetailTO.size()));
		summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalRecords());
		
		for(PayrollDetailTO detail : lstPayrollDetailTO) {
			if(detail.isBlSelected()) {
				yesRecords = yesRecords.add(BigDecimal.ONE);
			}else {
				noRecords = noRecords.add(BigDecimal.ONE);
			}
		}
		summaryPayrollTO.setYesRecords(yesRecords);
		summaryPayrollTO.setNoRecords(noRecords);
		
	}

	public void updateSummaryRetirement(PayrollDetailTO temp){
		
		if( payrollHeader.getRetirementMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) || payrollHeader.getRetirementMotive().equals(RetirementOperationMotiveType.RETIREMENT_OTC.getCode()) ) {
			for(PayrollDetailTO detail : lstPayrollDetailTO) {
				if( detail.getIdSecurityCodePk().equals(temp.getIdSecurityCodePk()) && detail.getIndSerializable().equals(0)  ) {
					detail.setBlSelected(temp.isBlSelected());
					/*
					if(detail.isBlSelected()){
						summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(BigDecimal.ONE));
						summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().subtract(BigDecimal.ONE));
						summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(detail.getQuantity()));
						summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().subtract(detail.getQuantity()));
					}else{
						summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().subtract(BigDecimal.ONE));
						summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.ONE));
						summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().subtract(detail.getQuantity()));
						summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(detail.getQuantity()));
					}
				*/	
				}
			}
		}
		updateRecordSelected();
		/*
 		if(temp.isBlSelected()){
			summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(BigDecimal.ONE));
			summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().subtract(BigDecimal.ONE));
			summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(temp.getQuantity()));
			summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().subtract(temp.getQuantity()));
		}else{
			summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().subtract(BigDecimal.ONE));
			summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.ONE));
			summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().subtract(temp.getQuantity()));
			summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(temp.getQuantity()));
		}*/
	}
	
	public void updateSummary(PayrollDetailTO temp){
		
		for(PayrollDetailTO detail : lstPayrollDetailTO) {
			if( detail.getIdCustodyOperationPk().equals(temp.getIdCustodyOperationPk()) && detail.isBlIsCupon() ) {
				detail.setBlSelected(temp.isBlSelected());
			}
		}
		updateRecordSelected();
		/*
		BigDecimal yesRecords = BigDecimal.ZERO;
		BigDecimal noRecords = BigDecimal.ZERO;
		for(PayrollDetailTO detail : lstPayrollDetailTO) {
			if(detail.isBlSelected()) {
				yesRecords = yesRecords.add(BigDecimal.ONE);
			}else {
				noRecords = noRecords.add(BigDecimal.ONE);
			}
		}

		summaryPayrollTO.setYesRecords(yesRecords);
		summaryPayrollTO.setNoRecords(noRecords);*/
	}

	public void updateSummaryOri(PayrollDetailTO temp){
		
		for(PayrollDetailTO detail : lstPayrollDetailTO) {
			if( detail.getIdCustodyOperationPk().equals(temp.getIdCustodyOperationPk()) && detail.isBlIsCupon() ) {
				detail.setBlSelected(temp.isBlSelected());
				
				if(detail.isBlSelected()){			
					summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(BigDecimal.ONE));
					summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().subtract(BigDecimal.ONE));
					summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(detail.getQuantity()));
					summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().subtract(detail.getQuantity()));
				}else{
					summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().subtract(BigDecimal.ONE));
					summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.ONE));
					summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().subtract(detail.getQuantity()));
					summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(detail.getQuantity()));
				}
				
			}
		}
		
 		if(temp.isBlSelected()){			
			summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(BigDecimal.ONE));
			summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().subtract(BigDecimal.ONE));
			summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(temp.getQuantity()));
			summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().subtract(temp.getQuantity()));
		}else{
			summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().subtract(BigDecimal.ONE));
			summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.ONE));
			summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().subtract(temp.getQuantity()));
			summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(temp.getQuantity()));
		}
	}
	
	public String viewDetail(PayrollHeader payrollHeader){
		blApprove = false;
		blAnnul = false;
		return fillView(payrollHeader);
	}	
	
	public String validateAnnul(){
		if(selectedPayrollHeader != null){
			if(selectedPayrollHeader.getPayrollState().equals(PayrollHeaderStateType.GENERATE.getCode())){
				blAnnul = true;
				blApprove = false;
				return fillView(selectedPayrollHeader);
			}else{
				showMessageOnDialog("La planilla debe estar en estado GENERADO para realizar la anulación."
						, "Debe seleccionar un registro");		
				JSFUtilities.showSimpleValidationDialog();	
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "Debe seleccionar un registro");		
			JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}	
	
	public String validateApprove(){
		if(selectedPayrollHeader != null){
			if(selectedPayrollHeader.getPayrollState().equals(PayrollHeaderStateType.GENERATE.getCode())){
				blApprove = true;
				blAnnul = false;
				return fillView(selectedPayrollHeader);
			}else{
				showMessageOnDialog("La planilla debe estar en estado GENERADO para realizar la aprobacion."
						, "Debe seleccionar un registro");		
				JSFUtilities.showSimpleValidationDialog();	
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "Debe seleccionar un registro");		
			JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}
	
	
	public void beforeAnnul(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL)
				, "¿Está seguro que desea anular la planilla número " + payrollHeader.getIdPayrollHeaderPk() +"?");		
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}

	@LoggerAuditWeb
	public void beforeApprove(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE)
				, "¿Está seguro que desea aprobar la planilla número " + payrollHeader.getIdPayrollHeaderPk() +"?");		
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	

	@LoggerAuditWeb
	public void doAction(){
		try {
			if(blAnnul){
				generatePayrollFacade.annulPayroll(payrollHeader.getIdPayrollHeaderPk(), lstPayrollDetailTO);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, "Se anulo la planilla número " + payrollHeader.getIdPayrollHeaderPk() +".");
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}if(blApprove){
				generatePayrollFacade.approvePayroll(payrollHeader.getIdPayrollHeaderPk(), lstPayrollDetailTO);
				
				if(payrollHeader.getPayrollType().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode())) {
					/*
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.UTILITIES.getCode());//PHYSICAL_CERTIFICATE_REJECT_PAYROLL_DETAIL

					Object[] parameters = new Object[1];
					parameters[0] = payrollHeader.getIdPayrollHeaderPk();
					
					notificationServiceFacade.sendNotificationEmailParticipantNoConfig(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   "SOLICITUD DE RETIRO",
							   "Se ha generado la solicitud de retiro nro %s ",
							   payrollHeader.getParticipant().getIdParticipantPk(), 
							   parameters);
					*/
					
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_PAYROLL_APPROVE.getCode());

					Object[] parameters = new Object[1];
					parameters[0] = payrollHeader.getIdPayrollHeaderPk();
					
						
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,payrollHeader.getParticipant().getIdParticipantPk(), parameters);
					
					
				}
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, "Se aprobo la planilla número " + payrollHeader.getIdPayrollHeaderPk() +".");
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}
			search();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void resetRegister(){
		JSFUtilities.resetComponent(":frmPayroll:inputEntity");
		//payrollHeader.setParticipant(new Participant());
		payrollHeader.setIssuer(new Issuer());
		cleanOperations();
	}
	
	public void cleanOperations(){		
		lstPayrollDetailTO = null;
		blNoResult = false;
	}
	
	public void resetSearch(){
		selectedPayrollHeader = null;
		searchPayrollTO.setLstPayrollHeader(null);
		searchPayrollTO.setIssuer(new Issuer());

		if(blParticipant && defaultParticipant!=null){
			searchPayrollTO.setIdParticipantPk(defaultParticipant);
		}
	}
	
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		payrollHeader = new PayrollHeader();
		payrollHeader.setIssuer(new Issuer());
		payrollHeader.setParticipant(new Participant());
		payrollHeader.setRegisterDate(getCurrentSystemDate());
		lstPayrollDetailTO = null;
		
		if(blParticipant && defaultParticipant!=null){
			payrollHeader.getParticipant().setIdParticipantPk(defaultParticipant);
		}
	}
	
	public void clean(){
		JSFUtilities.resetViewRoot();
		searchPayrollTO = new SearchPayrollTO();
		searchPayrollTO.setIssuer(new Issuer());

		if(blParticipant && defaultParticipant!=null){
			searchPayrollTO.setIdParticipantPk(defaultParticipant);
		}
	}
	
	private String fillView(PayrollHeader payrollHeader){
		try {
			this.payrollHeader = generatePayrollFacade.getPayrollHeader(payrollHeader.getIdPayrollHeaderPk());
			this.payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
			lstPayrollDetailTO = generatePayrollFacade.getPayrollDetail(payrollHeader.getIdPayrollHeaderPk(), payrollHeader.getPayrollType());			
			summaryPayrollTO = new SummaryPayrollTO();			
			for(PayrollDetailTO temp:lstPayrollDetailTO){
				temp.setStateDesc(mpDetailStates.get(temp.getState()));
				temp.setOperationTypeDesc(mpOperationTypes.get(temp.getOperationType()));
				
				if(payrollHeader.getPayrollType().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode())) {
					summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalStock().add(BigDecimal.ONE));
				}else {
					summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalStock().add(BigDecimal.ONE));
					summaryPayrollTO.setTotalRecords(summaryPayrollTO.getTotalRecords().add(BigDecimal.ONE));
				}
				
			}
			
			if(payrollHeader.getPayrollType().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode())) {
				summaryPayrollTO.setTotalRecords(payrollHeader.getTotalStock());
			}
			
			summaryPayrollTO.setYesStock(summaryPayrollTO.getTotalStock());
			summaryPayrollTO.setYesRecords(summaryPayrollTO.getTotalRecords());
			return VIEW_MAPPING;	
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}
	
	private void fillMaps() throws ServiceException{
		mpStates = new HashMap<>();
		mpDetailStates = new HashMap<>();
		mpPayrollTypes = new HashMap<>();
		mpOperationTypes = new HashMap<>();
		lstStates = new ArrayList<>();
		lstPayrollTypes = new ArrayList<>();
		lstBranchOffices = new ArrayList<>();
		lstMotives = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_HEADER_STATE.getCode());
		List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstStates.add(paramTab);
			}	
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if( ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()) ){
				lstPayrollTypes.add(paramTab);
			}
			mpPayrollTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_DETAIL_STATE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			mpDetailStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstBranchOffices.add(paramTab);
			}
		}
		paramTabTO.setMasterTableFk(MasterTableType.CERTIFICATE_OPERATIONS.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp) {
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp) {
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				if(blParticipant) {
					if( !paramTab.getParameterTablePk().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) ) {
						lstMotives.add(paramTab);
					}
				}else {
					lstMotives.add(paramTab);
				}
			}
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
	}
	

	public void toogleFunction() { 
		this.toogleFlag = !(this.toogleFlag);
		for(PayrollDetailTO det: lstPayrollDetailTO) {
			det.setBlSelected(toogleFlag);
		}
		updateRecordSelected();
		//toogleText = (toogleFlag)?"Desmarcar Todos":"Marcar Todos";
	}
	
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnApproveView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	public Map<Integer, String> getMpStates() {
		return mpStates;
	}

	public void setMpStates(Map<Integer, String> mpStates) {
		this.mpStates = mpStates;
	}

	public Map<Integer, String> getMpPayrollTypes() {
		return mpPayrollTypes;
	}

	public void setMpPayrollTypes(Map<Integer, String> mpPayrollTypes) {
		this.mpPayrollTypes = mpPayrollTypes;
	}

	public SearchPayrollTO getSearchPayrollTO() {
		return searchPayrollTO;
	}

	public void setSearchPayrollTO(SearchPayrollTO searchPayrollTO) {
		this.searchPayrollTO = searchPayrollTO;
	}

	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}

	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}

	public List<PayrollDetailTO> getLstPayrollDetailTO() {
		return lstPayrollDetailTO;
	}

	public void setLstPayrollDetailTO(List<PayrollDetailTO> lstPayrollDetailTO) {
		this.lstPayrollDetailTO = lstPayrollDetailTO;
	}

	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	public List<ParameterTable> getLstPayrollTypes() {
		return lstPayrollTypes;
	}

	public void setLstPayrollTypes(List<ParameterTable> lstPayrollTypes) {
		this.lstPayrollTypes = lstPayrollTypes;
	}

	public PayrollHeader getSelectedPayrollHeader() {
		return selectedPayrollHeader;
	}

	public void setSelectedPayrollHeader(PayrollHeader selectedPayrollHeader) {
		this.selectedPayrollHeader = selectedPayrollHeader;
	}

	public SummaryPayrollTO getSummaryPayrollTO() {
		return summaryPayrollTO;
	}

	public void setSummaryPayrollTO(SummaryPayrollTO summaryPayrollTO) {
		this.summaryPayrollTO = summaryPayrollTO;
	}

	public Map<Integer, String> getMpDetailStates() {
		return mpDetailStates;
	}

	public void setMpDetailStates(Map<Integer, String> mpDetailStates) {
		this.mpDetailStates = mpDetailStates;
	}
	public boolean isBlAnnul() {
		return blAnnul;
	}
	public void setBlAnnul(boolean blAnnul) {
		this.blAnnul = blAnnul;
	}
	public List<ParameterTable> getLstBranchOffices() {
		return lstBranchOffices;
	}
	public void setLstBranchOffices(List<ParameterTable> lstBranchOffices) {
		this.lstBranchOffices = lstBranchOffices;
	}

	public boolean isBlNoResult() {
		return blNoResult;
	}

	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	public Map<Integer, String> getMpOperationTypes() {
		return mpOperationTypes;
	}

	public void setMpOperationTypes(Map<Integer, String> mpOperationTypes) {
		this.mpOperationTypes = mpOperationTypes;
	}

	public boolean isBlParticipant() {
		return blParticipant;
	}

	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}

	public boolean isBlApprove() {
		return blApprove;
	}

	public void setBlApprove(boolean blApprove) {
		this.blApprove = blApprove;
	}

	public String getToogleText() {
		return toogleText;
	}

	public void setToogleText(String toogleText) {
		this.toogleText = toogleText;
	}

	public Date getCurrentSystemDate() {
		return currentSystemDate;
	}

	public void setCurrentSystemDate(Date currentSystemDate) {
		this.currentSystemDate = currentSystemDate;
	}

	public List<ParameterTable> getLstMotives() {
		return lstMotives;
	}

	public void setLstMotives(List<ParameterTable> lstMotives) {
		this.lstMotives = lstMotives;
	}
	
}
