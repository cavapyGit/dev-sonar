package com.pradera.custody.payroll.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;

public class VaultSummary implements Serializable{

	private static final long serialVersionUID = 1L;
	private String armario;
	private String nivel;
	private String caja;
	private String folio;
	private String indiceCaja;
	private String serie;
	private String emisor;
	private String fechaIngreso;
	private String fechaVencimiento;
	private String depositante;
	private String idSecurityCodePk;
	private Long idPhysicalCertificatePk;
	private String securityNominalValue;
	private String currency;
	private Long quantity;
	private String certificateFromTo;
	
	
	public String getArmario() {
		return armario;
	}
	public void setArmario(String armario) {
		this.armario = armario;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getCaja() {
		return caja;
	}
	public void setCaja(String caja) {
		this.caja = caja;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getIndiceCaja() {
		return indiceCaja;
	}
	public void setIndiceCaja(String indiceCaja) {
		this.indiceCaja = indiceCaja;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getDepositante() {
		return depositante;
	}
	public void setDepositante(String depositante) {
		this.depositante = depositante;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public Long getIdPhysicalCertificatePk() {
		return idPhysicalCertificatePk;
	}
	public void setIdPhysicalCertificatePk(Long idPhysicalCertificatePk) {
		this.idPhysicalCertificatePk = idPhysicalCertificatePk;
	}
	public String getSecurityNominalValue() {
		return securityNominalValue;
	}
	public void setSecurityNominalValue(String securityNominalValue) {
		this.securityNominalValue = securityNominalValue;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public String getCertificateFromTo() {
		return certificateFromTo;
	}
	public void setCertificateFromTo(String certificateFromTo) {
		this.certificateFromTo = certificateFromTo;
	}
	
}
