package com.pradera.custody.payroll.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;

public class PhysicalCertificateTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Date entryDate;
	private String motiveDescription;
	private String rut;
	private Integer accountNumber;
	private String name;
	private String securityClassDescription;
	private String idSecurityCodePk;
	private String serialCode;
	private String electronic;
	private String issuer;
	private String currencyDescription;
	private BigDecimal securityNominalValue;
	private Date expirationDate;
	private Integer quantity;
	private Integer certificateFrom;
	private Integer certificateTo;
	private String comments;
	private Integer indElectronicCupon;
	private Integer cuponNumber;
	private Long idAnnotationOperation;
	
	
	public Date getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}
	public String getMotiveDescription() {
		return motiveDescription;
	}
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public Integer getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSecurityClassDescription() {
		return securityClassDescription;
	}
	public void setSecurityClassDescription(String securityClassDescription) {
		this.securityClassDescription = securityClassDescription;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public String getSerialCode() {
		return serialCode;
	}
	public void setSerialCode(String serialCode) {
		this.serialCode = serialCode;
	}
	public String getElectronic() {
		return electronic;
	}
	public void setElectronic(String electronic) {
		this.electronic = electronic;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getCurrencyDescription() {
		return currencyDescription;
	}
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}
	public BigDecimal getSecurityNominalValue() {
		return securityNominalValue;
	}
	public void setSecurityNominalValue(BigDecimal securityNominalValue) {
		this.securityNominalValue = securityNominalValue;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getCertificateFrom() {
		return certificateFrom;
	}
	public void setCertificateFrom(Integer certificateFrom) {
		this.certificateFrom = certificateFrom;
	}
	public Integer getCertificateTo() {
		return certificateTo;
	}
	public void setCertificateTo(Integer certificateTo) {
		this.certificateTo = certificateTo;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Integer getIndElectronicCupon() {
		return indElectronicCupon;
	}
	public void setIndElectronicCupon(Integer indElectronicCupon) {
		this.indElectronicCupon = indElectronicCupon;
	}
	public Integer getCuponNumber() {
		return cuponNumber;
	}
	public void setCuponNumber(Integer cuponNumber) {
		this.cuponNumber = cuponNumber;
	}
	public Long getIdAnnotationOperation() {
		return idAnnotationOperation;
	}
	public void setIdAnnotationOperation(Long idAnnotationOperation) {
		this.idAnnotationOperation = idAnnotationOperation;
	}
	
	
	
	
	
}
