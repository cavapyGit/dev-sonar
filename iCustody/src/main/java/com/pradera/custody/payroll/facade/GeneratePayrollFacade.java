package com.pradera.custody.payroll.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.custody.payroll.servicebean.GeneratePayrollServiceBean;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.payroll.view.SearchPayrollTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class GeneratePayrollFacade {
	
	@EJB
	GeneratePayrollServiceBean generatePayrollServiceBean;
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;	
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	public PayrollHeader searchPayrollHeaderWithAccountAnnotation(Long idAccountAnnotationOperationPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		List<PayrollHeader> lstPayrollHeader = generatePayrollServiceBean.lstSearchPayrollHeaderWithAccountAnnotation(idAccountAnnotationOperationPk);
		return (lstPayrollHeader!= null && lstPayrollHeader.size()>0)?lstPayrollHeader.get(0):null;
	}
	
	
	public List<PayrollHeader> searchPayrollHeaders(SearchPayrollTO searchPayrollTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return generatePayrollServiceBean.searchPayrollHeaders(searchPayrollTO);
	}

	public List<PayrollDetailTO> searchOperationsForPayroll(PayrollHeader payrollHeader) throws ServiceException{
		List<Object[]> lstResult = null; 
		List<AccountAnnotationCupon> lstAccountAnnotationCupon = null;
		List<PayrollDetailTO> lstPayrollDetail = new ArrayList<>();
		
		if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
			lstResult = generatePayrollServiceBean.searchAccountAnnotationOperations(payrollHeader);
		}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
			//lstResult = generatePayrollServiceBean.searchRetirementOperations(payrollHeader);
			lstResult = generatePayrollServiceBean.searchRetirementOperationAndDetail(payrollHeader);
		}
		
		String idSecurityCodeCut="";
		Long IdReferenceHelper =0L; 
		for(Object[] objResult:lstResult){
			
			if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
				
				Long idCustodyOperationPk = new Long(objResult[0].toString());
				Date registerDate = (Date) objResult[1];
				Integer motive = Integer.valueOf(objResult[2].toString());
				Security currentSecurity = (Security) objResult[3];
				Participant currentParticipant = (Participant) objResult[4];
				HolderAccount currentHolderAccount = (HolderAccount) objResult[5];
				PhysicalCertificate currentCertificate = (PhysicalCertificate) objResult[6];
				Issuer currentIssuer = (Issuer) objResult[7];
				Integer indRetirementExpiration = Integer.valueOf(objResult[8].toString());//retirement with corporative
				Integer motiveRetirement = Integer.valueOf(objResult[9].toString());
				String motiveRetirementDesc = ((String)objResult[10]);
				RetirementDetail currentRetirementDetail = (RetirementDetail) objResult[11];
				
				PayrollDetailTO payrollDetailCupon = new PayrollDetailTO();
				payrollDetailCupon.setIdCustodyOperationPk(idCustodyOperationPk);
				payrollDetailCupon.setRegistryDate(registerDate);
				payrollDetailCupon.setIdSecurityCodePk(currentSecurity.getIdSecurityCodePk());
				

				HolderAccountTO temp = new HolderAccountTO();
				temp.setIdHolderAccountPk(currentHolderAccount.getIdHolderAccountPk());
				payrollDetailCupon.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
				payrollDetailCupon.setBlSelected(false);
				payrollDetailCupon.setCuponNumber(currentCertificate.getCuponNumber());
				payrollDetailCupon.setIndSerializable( (currentCertificate.getExpirationDate()==null)?1:0 );
				payrollDetailCupon.setQuantity(currentCertificate.getCertificateQuantity());
				payrollDetailCupon.setSecurityNominalValue(currentCertificate.getNominalAmount());
				if(currentCertificate.getCuponNumber() != null) {
					payrollDetailCupon.setSerialCode(currentCertificate.getSerialNumber()+currentCertificate.getCertificateNumber()+"-"+currentCertificate.getCuponNumber());
				}else {
					payrollDetailCupon.setSerialCode(currentCertificate.getSerialNumber()+currentCertificate.getCertificateNumber());
				}
				if(indRetirementExpiration.equals(1)) {
					payrollDetailCupon.setBlIsCupon(false);
				}else {
					payrollDetailCupon.setBlIsCupon(currentCertificate.getIndCuponFlag());
				}
				payrollDetailCupon.setCurrencyDesc(CurrencyType.get(currentSecurity.getCurrency()).getCodeIso());
				payrollDetailCupon.setIssuerDesc(currentIssuer.getBusinessName());
				
				if(idSecurityCodeCut!=currentSecurity.getIdSecurityCodePk()) {
					IdReferenceHelper =0L; 
					Long idPhysicalCertificate = (currentCertificate.getPhysicalCertificateReference() != null)? currentCertificate.getPhysicalCertificateReference(): currentCertificate.getIdPhysicalCertificatePk();
					List<AccountAnnotationOperation> lstAccountAnnotationOperation = generatePayrollServiceBean.lstAccountAnnotationOperation(idPhysicalCertificate);
					if(lstAccountAnnotationOperation!=null && lstAccountAnnotationOperation.size()>0) {
						IdReferenceHelper = lstAccountAnnotationOperation.get(0).getIdAnnotationOperationPk();
					}
					idSecurityCodeCut = currentSecurity.getIdSecurityCodePk();
				}
				payrollDetailCupon.setIdPhysicalCertificatePk(currentCertificate.getIdPhysicalCertificatePk());
				payrollDetailCupon.setIdCustodyOperationCertificatePk(IdReferenceHelper);
				if(currentRetirementDetail !=null && Validations.validateIsNotNull(currentRetirementDetail.getIdRetirementDetailPk()) ) {
					RetirementDetail retirementDetail = generatePayrollServiceBean.find(RetirementDetail.class,currentRetirementDetail.getIdRetirementDetailPk());
					payrollDetailCupon.setRetirementDetail(retirementDetail);
				}
				payrollDetailCupon.setMotiveDepRetPayroll(motiveRetirement);
				payrollDetailCupon.setDescMotiveDepRetPayroll(motiveRetirementDesc);
				
				lstPayrollDetail.add(payrollDetailCupon);
				
			}else if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
				lstAccountAnnotationCupon = null;
				
				HolderAccountTO temp = new HolderAccountTO();
				temp.setIdHolderAccountPk(new Long(objResult[3].toString()));

				PayrollDetailTO payrollDetail = new PayrollDetailTO();
			
				payrollDetail.setIdCustodyOperationPk(new Long(objResult[0].toString()));
				payrollDetail.setIdCustodyOperationCertificatePk(new Long(objResult[0].toString()));
				payrollDetail.setRegistryDate((Date) objResult[1]);
				payrollDetail.setIdSecurityCodePk(Validations.validateIsNotNullAndNotEmpty(objResult[2])?objResult[2].toString():null);
				payrollDetail.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
				payrollDetail.setQuantity((BigDecimal) objResult[4]);	//Total balance del account annotation operation/cupon
				payrollDetail.setIndSerializable(new Integer(objResult[5].toString()));
				if(objResult[7] != null) {
					payrollDetail.setSerialCode((String)objResult[7]);
				}
				if(objResult[8] != null) {
					payrollDetail.setSecurityNominalValue((BigDecimal)objResult[8]);
				}
				Integer indElectroniCupon = (Integer)objResult[6];
				Integer cantidadCuponFisico = 0;
				if( indElectroniCupon!=null && indElectroniCupon.equals(0) ) {
					lstAccountAnnotationCupon = generatePayrollServiceBean.getCuponsWithAccountAnnotation(payrollDetail.getIdCustodyOperationPk());
				}
				payrollDetail.setPhysicCuponCount(cantidadCuponFisico);
				payrollDetail.setBlIsCupon(false);

				payrollDetail.setCurrencyDesc((String)objResult[9]);
				payrollDetail.setIssuerDesc((String)objResult[10]);
				String currencyDesc = "";
				if(objResult[11]!=null) {
					Integer currency = new Integer(objResult[11].toString());
					currencyDesc = CurrencyType.get(currency).getCodeIso();
				}
				Integer motiveAnnotation = Integer.valueOf(objResult[12].toString());
				String motiveAnnotationDesc = objResult[13].toString();
				payrollDetail.setCurrencyDesc(currencyDesc);
				payrollDetail.setBlSelected(false);
				payrollDetail.setLstAccountAnnotationCupon(lstAccountAnnotationCupon);
				
				payrollDetail.setMotiveDepRetPayroll(motiveAnnotation);
				payrollDetail.setDescMotiveDepRetPayroll(motiveAnnotationDesc);
				lstPayrollDetail.add(payrollDetail);
				
				if(lstAccountAnnotationCupon != null && lstAccountAnnotationCupon.size()>0) {
					for(AccountAnnotationCupon accountAnnotationCupon: lstAccountAnnotationCupon) {
						PayrollDetailTO payrollDetailCupon = new PayrollDetailTO();

						payrollDetailCupon.setAccountAnnotationCupon(accountAnnotationCupon);
						payrollDetailCupon.setIdCustodyOperationPk(payrollDetail.getIdCustodyOperationPk());
						payrollDetailCupon.setIdCustodyOperationCertificatePk(new Long(objResult[0].toString()));
						payrollDetailCupon.setRegistryDate(payrollDetail.getRegistryDate());
						payrollDetailCupon.setIdSecurityCodePk(payrollDetail.getIdSecurityCodePk());
						payrollDetailCupon.setHolderAccount(payrollDetail.getHolderAccount());
						payrollDetailCupon.setQuantity(payrollDetail.getQuantity());
						payrollDetailCupon.setIndSerializable(payrollDetail.getIndSerializable());	
						payrollDetailCupon.setBlSelected(false);
						payrollDetailCupon.setCuponNumber(accountAnnotationCupon.getCuponNumber());
						if(payrollDetail.getSerialCode() != null) {
							payrollDetailCupon.setSerialCode(payrollDetail.getSerialCode()+"-"+accountAnnotationCupon.getCuponNumber());
						}
						payrollDetailCupon.setBlIsCupon(true);
						
						payrollDetailCupon.setSecurityNominalValue(accountAnnotationCupon.getCuponAmount());//accountAnnotationCupon.getCuponAmount()

						payrollDetailCupon.setCurrencyDesc(payrollDetail.getCurrencyDesc());
						payrollDetailCupon.setIssuerDesc(payrollDetail.getIssuerDesc());
						payrollDetailCupon.setMotiveDepRetPayroll(motiveAnnotation);
						payrollDetailCupon.setDescMotiveDepRetPayroll(motiveAnnotationDesc);
						
						lstPayrollDetail.add(payrollDetailCupon);
					}
				}
			}			
			
			
		}		
		
		return lstPayrollDetail;
	}

	public Long generatePayroll(PayrollHeader payrollHeader, List<PayrollDetailTO> lstPayrollDetailTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		payrollHeader.setAudit(loggerUser);
		payrollHeader.setRegisterUser(loggerUser.getUserName());
		payrollHeader.setPayrollState(PayrollHeaderStateType.GENERATE.getCode());
		payrollHeader.setLstPayrollDetail(new ArrayList<PayrollDetail>());
		payrollHeader.setTotalStock(BigDecimal.ZERO);
		payrollHeader.setIndApprove(0);
		/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollHeader.getPayrollType()))
			payrollHeader.setParticipant(null);
		else*/
		payrollHeader.setIssuer(null);	
		
		BigDecimal totalPayroll = BigDecimal.ZERO;
		for(PayrollDetailTO payrollDetailTO:lstPayrollDetailTO){
			if(payrollDetailTO.isBlSelected()){
				PayrollDetail payrollDetail = new PayrollDetail();
				payrollDetail.setAudit(loggerUser);
				payrollDetail.setPayrollHeader(payrollHeader);
				payrollDetail.setBranchOffice(payrollHeader.getBranchOffice());
				/*
				if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
					payrollDetail.setCorporativeOperation(new CorporativeOperation(payrollDetailTO.getIdCorporativeOperationPk()));
				}else
				*/

				if(payrollDetailTO.isBlIsCupon()) {
					payrollDetail.setCuponNumber(payrollDetailTO.getCuponNumber());
					payrollDetail.setIndCupon(1);
				}
				
				{					
					if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
						
						AccountAnnotationOperation accountAnnotationOperation = generatePayrollServiceBean.find(AccountAnnotationOperation.class, payrollDetailTO.getIdCustodyOperationPk());
						if(!DematerializationStateType.REIGSTERED.getCode().equals(accountAnnotationOperation.getState()) && !payrollDetailTO.isBlIsCupon()) {
							throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);				
						}
						accountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
						accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.APPROVED.getCode());
						accountAnnotationOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDate());
						accountAnnotationOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
						payrollDetail.setAccountAnnotationOperation(accountAnnotationOperation);
						
						
					}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
						
						RetirementOperation retirementOperation = generatePayrollServiceBean.find(RetirementOperation.class, payrollDetailTO.getIdCustodyOperationPk());	
						if( !payrollDetailTO.isBlIsCupon() || 
							retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) ) {
							
							if( !RetirementOperationStateType.REGISTERED.getCode().equals(retirementOperation.getState())) {
								throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
							}
							retirementOperation.setState(RetirementOperationStateType.APPROVED.getCode());
							retirementOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
							retirementOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
							retirementOperation.getCustodyOperation().setState(RetirementOperationStateType.APPROVED.getCode());
							payrollDetail.setRetirementOperation(retirementOperation);
							
						}
						
						//solo para el caso de retiro
						if(payrollDetailTO.getCuponNumber() != null) {
							payrollDetail.setCuponNumber(payrollDetailTO.getCuponNumber());
							payrollDetail.setIndCupon(1);
						}else {
							payrollDetail.setCuponNumber(null);
							payrollDetail.setIndCupon(0);
						}
						
					}
					
				}				
				payrollDetail.setPayrollState(PayrollDetailStateType.GENERATED.getCode());
				payrollDetail.setPayrollQuantity(payrollDetailTO.getQuantity());
				payrollDetail.setSerialCode(payrollDetailTO.getSerialCode());
				payrollDetail.setAccountAnnotationCupon(payrollDetailTO.getAccountAnnotationCupon());
				payrollDetail.setRetirementDetail(payrollDetailTO.getRetirementDetail());
				
				///---------
				payrollHeader.getLstPayrollDetail().add(payrollDetail);
				payrollHeader.setTotalStock(payrollHeader.getTotalStock().add(BigDecimal.ONE));//payrollDetail.getPayrollQuantity()
				//----------
				//totalPayroll = totalPayroll.add(payrollDetail.getPayrollQuantity());
				//generatePayrollServiceBean.create(payrollDetail);
			}
		}
		//payrollHeader.setTotalStock(totalPayroll);
		generatePayrollServiceBean.create(payrollHeader,PayrollHeader.class);
		return payrollHeader.getIdPayrollHeaderPk();
	}

	public PayrollHeader getPayrollHeader(Long idPayrollHeaderPk) throws ServiceException{
		return generatePayrollServiceBean.getPayrollHeader(idPayrollHeaderPk);
	}

	public List<PayrollDetailTO> getPayrollDetail(Long idPayrollHeaderPk, Integer payrollType) throws ServiceException{
		List<Object[]> lstResult = null;
		List<PayrollDetailTO> lstPayrollDetailTO = new ArrayList<>();
		/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailCorporative(idPayrollHeaderPk);
		}else*/ 
		if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailAccountAnnotation(idPayrollHeaderPk);
		}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailRemoveSecurities(idPayrollHeaderPk);
		}/*else if(PayrollType.CERTIFICATE_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailCertificateOperation(idPayrollHeaderPk);
		}*/
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(Object[] objResult:lstResult){
				PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
				/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollType))
					payrollDetailTO.setIdCorporativeOperationPk(new Long(objResult[0].toString()));
				else*/{
					payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[0].toString()));
					payrollDetailTO.setRegistryDate((Date) objResult[1]);
					payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNullAndNotEmpty(objResult[2])?objResult[2].toString():null);				
					if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType) 
							|| PayrollType.CERTIFICATE_OPERATION.getCode().equals(payrollType)){
						payrollDetailTO.setQuantity((BigDecimal) objResult[3]);
						payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[4].toString()));
						payrollDetailTO.setState(new Integer(objResult[5].toString()));
						payrollDetailTO.setOperationType(new Integer(objResult[6].toString()));
						payrollDetailTO.setSecurityNominalValue((BigDecimal) objResult[10]);
						payrollDetailTO.setCurrencyDesc( CurrencyType.get(((Integer)objResult[11])).getCodeIso() );
						payrollDetailTO.setPayrollHeadTotalStock(new Integer(objResult[20].toString()));
						if(objResult[9]!=null) {
							payrollDetailTO.setSerialCode(objResult[9].toString());
						}
						payrollDetailTO.setDescMotiveDepRetPayroll((String)objResult[22]);
					}else if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollType)){
						HolderAccountTO temp = new HolderAccountTO();
						temp.setIdHolderAccountPk(new Long(objResult[3].toString()));
						payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
						payrollDetailTO.setQuantity((BigDecimal) objResult[4]);
						payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[5].toString()));
						payrollDetailTO.setState(new Integer(objResult[6].toString()));
						payrollDetailTO.setIndSerializable(new Integer(objResult[7].toString()));
						Integer indCupon = new Integer(objResult[8].toString());
						payrollDetailTO.setBlIsCupon((indCupon==1));
						payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[9].toString()):null);
						if(objResult[10]!=null) {
							payrollDetailTO.setSerialCode(objResult[10].toString());
						}
						payrollDetailTO.setSecurityNominalValue((BigDecimal) objResult[11]);
						payrollDetailTO.setIdAnnotationOperationPk(new Long(objResult[12].toString()));
						payrollDetailTO.setCurrencyDesc( CurrencyType.get(((Integer)objResult[15])).getCodeIso() );
						payrollDetailTO.setDescMotiveDepRetPayroll((String)objResult[19]);
					}					
				}
				lstPayrollDetailTO.add(payrollDetailTO);
			}
		}
		return lstPayrollDetailTO;
	}

	public void annulPayroll(Long idPayrollHeaderPk, List<PayrollDetailTO> lstPayrollDetailTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		PayrollHeader payrollHeader = generatePayrollServiceBean.find(PayrollHeader.class, idPayrollHeaderPk);
		if(!PayrollHeaderStateType.GENERATE.getCode().equals(payrollHeader.getPayrollState()))
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		BigDecimal detailsProcessed = generatePayrollServiceBean.getDetailsProcessed(idPayrollHeaderPk);
		if(detailsProcessed.compareTo(BigDecimal.ZERO) > 0)
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		generatePayrollServiceBean.updateDetailsAnnulate(idPayrollHeaderPk, loggerUser);
		for(PayrollDetailTO payrollDetailTO:lstPayrollDetailTO){
			if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
				AccountAnnotationOperation accountAnnotationOperation = generatePayrollServiceBean.find(AccountAnnotationOperation.class, payrollDetailTO.getIdCustodyOperationPk());
				accountAnnotationOperation.setState(DematerializationStateType.REIGSTERED.getCode());
				accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REIGSTERED.getCode());
				accountAnnotationOperation.getCustodyOperation().setApprovalDate(null);
				accountAnnotationOperation.getCustodyOperation().setApprovalUser(null);
			}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
				RetirementOperation retirementOperation = generatePayrollServiceBean.find(RetirementOperation.class, payrollDetailTO.getIdCustodyOperationPk());
				retirementOperation.getCustodyOperation().setApprovalUser(null);
				retirementOperation.getCustodyOperation().setApprovalDate(null);
				retirementOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
				retirementOperation.getCustodyOperation().setState(RetirementOperationStateType.REGISTERED.getCode());
			}
		}
		payrollHeader.setPayrollState(PayrollHeaderStateType.ANNULATE.getCode());
	}
	
	public void approvePayroll(Long idPayrollHeaderPk, List<PayrollDetailTO> lstPayrollDetailTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		PayrollHeader payrollHeader = generatePayrollServiceBean.find(PayrollHeader.class, idPayrollHeaderPk);
		if(!PayrollHeaderStateType.GENERATE.getCode().equals(payrollHeader.getPayrollState()))
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		/*BigDecimal detailsProcessed = generatePayrollServiceBean.getDetailsProcessed(idPayrollHeaderPk);
		if(detailsProcessed.compareTo(BigDecimal.ZERO) > 0)
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		generatePayrollServiceBean.updateDetailsAnnulate(idPayrollHeaderPk, loggerUser);*/
		payrollHeader.setIndApprove(1);
	}
	
}
