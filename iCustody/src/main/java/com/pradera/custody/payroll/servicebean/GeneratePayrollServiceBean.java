package com.pradera.custody.payroll.servicebean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.payroll.view.SearchPayrollTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationStateType;

@Stateless
public class GeneratePayrollServiceBean extends CrudDaoServiceBean {

	public PayrollHeader searchPayrollHeaderWithAccountAnnotation(Long idAccountAnnotationOperationPk) throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ph FROM PayrollHeader ph");
			sbQuery.append("	LEFT JOIN ph.lstPayrollDetail prd ");
			sbQuery.append("	LEFT JOIN prd.accountAnnotationOperation aao ");
			sbQuery.append("	WHERE aao.idAnnotationOperationPk = :idAccountAnnotationOperationPk ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idAccountAnnotationOperationPk", idAccountAnnotationOperationPk);
			
			return (PayrollHeader)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<PayrollHeader> lstSearchPayrollHeaderWithAccountAnnotation(Long idAccountAnnotationOperationPk) throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ph FROM PayrollHeader ph");
			sbQuery.append("	LEFT JOIN ph.lstPayrollDetail prd ");
			sbQuery.append("	LEFT JOIN prd.accountAnnotationOperation aao ");
			sbQuery.append("	WHERE aao.idAnnotationOperationPk = :idAccountAnnotationOperationPk ");
			sbQuery.append("	AND ph.payrollState not in (:payrollState) ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idAccountAnnotationOperationPk", idAccountAnnotationOperationPk);
			List<Integer> lstStateNotIn = new ArrayList<Integer>();
			lstStateNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstStateNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			
			query.setParameter("payrollState", lstStateNotIn);
			
			
			
			return (List<PayrollHeader>)query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<PayrollHeader> searchPayrollHeaders(SearchPayrollTO searchPayrollTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph FROM PayrollHeader ph");
		sbQuery.append("	LEFT JOIN ph.participant");
		sbQuery.append("	LEFT JOIN ph.issuer");
		sbQuery.append("	WHERE ph.payrollType = :payrollType");
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getIdParticipantPk()))
			sbQuery.append("	AND ph.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getIssuer().getIdIssuerPk()))
			sbQuery.append("	AND ph.issuer.idIssuerPk = :idIssuerPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getGenerateDate()))
			sbQuery.append("	AND ph.registerDate = :registerDate");
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getState()))
			sbQuery.append("	AND ph.payrollState = :payrollState");
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getBranchOffice()))
			sbQuery.append("	AND ph.branchOffice = :branchOffice");
		sbQuery.append("	ORDER BY ph.idPayrollHeaderPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", searchPayrollTO.getPayRollType());
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchPayrollTO.getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getIssuer().getIdIssuerPk()))
			query.setParameter("idIssuerPk", searchPayrollTO.getIssuer().getIdIssuerPk());		
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getGenerateDate()))
			query.setParameter("registerDate", searchPayrollTO.getGenerateDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getState()))
			query.setParameter("payrollState", searchPayrollTO.getState());
		if(Validations.validateIsNotNullAndNotEmpty(searchPayrollTO.getBranchOffice()))
			query.setParameter("branchOffice", searchPayrollTO.getBranchOffice());
		return (List<PayrollHeader>)query.getResultList();
	}

	public List<Object[]> searchCorporativeOperations(PayrollHeader payrollHeader, List<Integer> lstCorporativeTypes) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co.idCorporativeOperationPk,");
		sbQuery.append("	co.registryDate,");
		sbQuery.append("	co.deliveryDate,");
		sbQuery.append("	co.securities.idSecurityCodePk,");
		sbQuery.append("	'''',");
		sbQuery.append("	co.newShareCapital,");
		sbQuery.append("	FROM CorporativeOperation co");
		sbQuery.append("	WHERE co.issuer.idIssuerPk = :idIssuerPk");
		sbQuery.append("	AND co.state = :definitiveState");
		sbQuery.append("	AND co.corporativeEventType.corporativeEventType IN (:lstCorporativeTypes)");
		sbQuery.append("	ORDER co.idCorporativeOperationPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idIssuerPk", payrollHeader.getIssuer().getIdIssuerPk());
		query.setParameter("definitiveState", CorporateProcessStateType.DEFINITIVE.getCode());
		query.setParameter("lstCorporativeTypes", lstCorporativeTypes);		
		return query.getResultList();
	}
	
	public List<AccountAnnotationCupon> getCuponsWithAccountAnnotation(Long idAnnotationOperationPk){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aac FROM AccountAnnotationCupon aac ");
		sbQuery.append("	WHERE aac.accountAnnotationOperation.idAnnotationOperationPk = :idAnnotationOperationPk");			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);	
		
		return query.getResultList();
	}
	
	public List<Object[]> searchAccountAnnotationOperations(PayrollHeader payrollHeader) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ");
		sbQuery.append("	aao.custodyOperation.idCustodyOperationPk, ");//0
		sbQuery.append("	aao.custodyOperation.registryDate, ");//1
		sbQuery.append("	aao.security.idSecurityCodePk, ");//2
		sbQuery.append("	aao.holderAccount.idHolderAccountPk, ");//3
		sbQuery.append("	aao.totalBalance, ");//4
		sbQuery.append("	aao.indSerializable, ");//5
		sbQuery.append("	aao.indElectronicCupon, ");//6
		sbQuery.append("	aao.serialNumber||aao.certificateNumber, ");//7
		sbQuery.append("	aao.securityNominalValue, ");//8
		sbQuery.append("	(select pt.indicator1 from ParameterTable pt where pt.parameterTablePk = aao.securityCurrency), ");//9
		sbQuery.append("	i.businessName, ");//10
		sbQuery.append("	aao.securityCurrency, ");//11
		sbQuery.append("	aao.motive, ");//12
		sbQuery.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = aao.motive) ");//13
		sbQuery.append("	FROM AccountAnnotationOperation aao");
		sbQuery.append("	LEFT JOIN aao.issuer i ");
		sbQuery.append("	WHERE aao.holderAccount.participant.idParticipantPk = :idParticipantPk ");//and aao.indElectronicCupon = :notInd 
		sbQuery.append("	AND aao.state = :registerState");
		sbQuery.append("	AND aao.indImmobilization = :indImmobilization");
		//sbQuery.append("	AND aao.indApprove = :indApprove");
		
		//sbQuery.append("	AND aao.branchOffice = :branchOffice");
		if(payrollHeader.getSearchDate()!=null) {
			sbQuery.append("	AND TO_CHAR(aao.custodyOperation.registryDate,'dd/mm/yyyy') = TO_CHAR(:searchDate,'dd/mm/yyyy') ");
		}
		sbQuery.append("	ORDER BY aao.custodyOperation.idCustodyOperationPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", payrollHeader.getParticipant().getIdParticipantPk());
		query.setParameter("registerState", DematerializationStateType.REIGSTERED.getCode());
		query.setParameter("indImmobilization", 1);
		if(payrollHeader.getSearchDate()!=null) {
			query.setParameter("searchDate", payrollHeader.getSearchDate());
		}
		//query.setParameter("indApprove", 1);
		//query.setParameter("branchOffice", payrollHeader.getBranchOffice());
		//query.setParameter("notInd", 0);
		/*
		Long pkCustody = 0L;
		Integer i=0;
		Integer countPhysicalCupons = 0;
		List<Object[]> lstNoRepeat = null;
		if(query.getResultList() != null && query.getResultList().size()>0) {
		lstNoRepeat = new ArrayList<Object[]>();
		
			for (Object[] arrObj: (List<Object[]>)query.getResultList()) {
				Long tmpPkCustody = (Long)arrObj[0];
				if(!pkCustody.equals(tmpPkCustody) && i>0 ) {
					arrObj[7] = countPhysicalCupons;
					countPhysicalCupons = 0;
					lstNoRepeat.add(arrObj);
				}
				pkCustody = tmpPkCustody;
				i++;
			}
		}
		
		return lstNoRepeat;
		*/
		
		return query.getResultList();
	}	
	
	public List<Object[]> searchRetirementOperationAndDetail(PayrollHeader payrollHeader) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ");
		sbQuery.append("	ro.custodyOperation.idCustodyOperationPk, ");//0
		sbQuery.append("	ro.custodyOperation.registryDate, ");//1
		sbQuery.append("	ro.motive, ");//2
		sbQuery.append("	ro.security, ");//3
		sbQuery.append("	ro.participant, ");//4
		sbQuery.append("	ro.holderAccount, ");//5
		sbQuery.append("	rd.physicalCertificate, ");//6
		sbQuery.append("	s.issuer, ");//7
		sbQuery.append("	ro.indRetirementExpiration, ");//8
		sbQuery.append("	ro.motive, ");//9
		sbQuery.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.motive), ");//10
		sbQuery.append("	rd ");//11
		sbQuery.append("	FROM RetirementOperation ro ");
		sbQuery.append("	INNER JOIN ro.retirementDetails rd ");
		sbQuery.append("	INNER JOIN ro.security s ");
		sbQuery.append("	INNER JOIN rd.physicalCertificate pc ");
		sbQuery.append("	WHERE ro.state = :registerState");

		if(payrollHeader.getRetirementMotive() != null) {
			sbQuery.append("	AND ro.motive = :motive");
		}
		if(payrollHeader.getParticipant() != null && payrollHeader.getParticipant().getIdParticipantPk()!=null) {
			sbQuery.append("	AND ro.participant.idParticipantPk = :idParticipantPk");
		}
		if(payrollHeader.getSearchDate()!=null) {
			sbQuery.append("	AND TO_CHAR(ro.custodyOperation.registryDate,'dd/mm/yyyy') = TO_CHAR(:searchDate,'dd/mm/yyyy') ");
		}
		sbQuery.append("	ORDER BY s.idSecurityCodePk,pc.cuponNumber DESC ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(payrollHeader.getRetirementMotive() != null) {
			query.setParameter("motive", payrollHeader.getRetirementMotive());
		}
		if(payrollHeader.getParticipant() != null && payrollHeader.getParticipant().getIdParticipantPk()!=null) {
			query.setParameter("idParticipantPk", payrollHeader.getParticipant().getIdParticipantPk());
		}
		if(payrollHeader.getSearchDate()!=null) {
			query.setParameter("searchDate", payrollHeader.getSearchDate());
		}
		query.setParameter("registerState", RetirementOperationStateType.REGISTERED.getCode());
		return query.getResultList();
		
	}
	
	
	public List<Object[]> searchRetirementOperations(PayrollHeader payrollHeader) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ro.custodyOperation.idCustodyOperationPk,");
		sbQuery.append("	ro.custodyOperation.registryDate,");
		sbQuery.append("	ro.security.idSecurityCodePk,");
		sbQuery.append("	ro.totalBalance,");
		sbQuery.append("	ro.motive, ");
		sbQuery.append("	ro.physicalCertificate ");
		sbQuery.append("	FROM RetirementOperation ro");
		sbQuery.append("	WHERE ro.state = :registerState");
		if(payrollHeader.getParticipant() != null && payrollHeader.getParticipant().getIdParticipantPk()!=null) {
			sbQuery.append("	AND ro.participant.idParticipantPk = :idParticipantPk");
		}
		if(payrollHeader.getSearchDate()!=null) {
			sbQuery.append("	AND TO_CHAR(aao.custodyOperation.registryDate,'dd/mm/yyyy') = TO_CHAR(:searchDate,'dd/mm/yyyy') ");
		}
		sbQuery.append("	ORDER BY ro.custodyOperation.idCustodyOperationPk ASC");
		Query query = em.createQuery(sbQuery.toString());
		if(payrollHeader.getParticipant() != null && payrollHeader.getParticipant().getIdParticipantPk()!=null) {
			query.setParameter("idParticipantPk", payrollHeader.getParticipant().getIdParticipantPk());
		}
		if(payrollHeader.getSearchDate()!=null) {
			query.setParameter("searchDate", payrollHeader.getSearchDate());
		}
		query.setParameter("registerState", RetirementOperationStateType.REGISTERED.getCode());
		return query.getResultList();
	}
	
	public PayrollHeader getPayrollHeader(Long idPayrollHeaderPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ph FROM PayrollHeader ph");
			sbQuery.append("	LEFT JOIN ph.participant");
			sbQuery.append("	LEFT JOIN ph.issuer");
			sbQuery.append("	WHERE ph.idPayrollHeaderPk = :idPayrollHeaderPk");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
			return (PayrollHeader) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}

	public Boolean isRetirementPayrollDetailCorporative(Long idPayrollHeaderPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" select pd.ID_PAYROLL_DETAIL_PK, pd.ID_PAYROLL_HEADER_FK from PAYROLL_DETAIL pd ");
			sbQuery.append(" inner join retirement_operation ro ");
			sbQuery.append(" 	ON pd.ID_RETIREMENT_OPERATION_FK = ro.ID_RETIREMENT_OPERATION_PK ");
			sbQuery.append(" WHERE ro.motive = :retirementOperationMotiveType AND pd.PAYROLL_STATE != :payrollDetailStateType AND pd.ID_PAYROLL_HEADER_FK = :idPayrollHeaderPk");
			Query query = em.createNativeQuery(sbQuery.toString());
			query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
			query.setParameter("retirementOperationMotiveType", RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode());
			query.setParameter("payrollDetailStateType", PayrollDetailStateType.ANNULATE.getCode());
			List<Object[]> lstObject = query.getResultList();
			
			return (lstObject!=null && lstObject.size()>0);
		} catch (NoResultException e) {
			return false;
		}
		
	}

	
	public List<Object[]> getPayrollDetailAccountAnnotation(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk,");//0
		sbQuery.append("	pd.accountAnnotationOperation.custodyOperation.registryDate,");//1
		sbQuery.append("	pd.accountAnnotationOperation.security.idSecurityCodePk,");//2
		sbQuery.append("	pd.accountAnnotationOperation.holderAccount.idHolderAccountPk,");//3
		sbQuery.append("	pd.payrollQuantity,");//4
		sbQuery.append("	pd.idPayrollDetailPk,");//5
		sbQuery.append("	pd.payrollState,");//6
		sbQuery.append("	pd.accountAnnotationOperation.indSerializable, ");//7
		sbQuery.append("	pd.indCupon, ");//8
		sbQuery.append("	pd.cuponNumber, ");//9
		sbQuery.append("	pd.serialCode, ");//10
		sbQuery.append(" (	case when pd.indCupon = 0 then ");
		sbQuery.append("	nvl(pd.accountAnnotationOperation.securityNominalValue,0) ");//11
		sbQuery.append("	else ");
		sbQuery.append("	(select nvl(aac.cuponAmount,0) from AccountAnnotationCupon aac where aac.accountAnnotationOperation = pd.accountAnnotationOperation.idAnnotationOperationPk and aac.cuponNumber = pd.cuponNumber ) ");
		sbQuery.append("	end ) as securityNominalValue, ");
		sbQuery.append("	pd.accountAnnotationOperation.idAnnotationOperationPk, ");//12
		sbQuery.append("	pd.accountAnnotationOperation.issuer.mnemonic, ");//13
		//sbQuery.append("	pd.accountAnnotationOperation.securityExpirationDate, ");//14
		sbQuery.append(" (	case when pd.indCupon = 0 then ");
		sbQuery.append("	pd.accountAnnotationOperation.securityExpirationDate ");
		sbQuery.append("	else ");
		sbQuery.append("	(select aac.expirationDate from AccountAnnotationCupon aac where aac.accountAnnotationOperation = pd.accountAnnotationOperation.idAnnotationOperationPk and aac.cuponNumber = pd.cuponNumber ) ");
		sbQuery.append("	end ) as expirationDate, ");//14
		sbQuery.append("	pd.accountAnnotationOperation.securityCurrency, ");//15
		sbQuery.append("	pd.accountAnnotationOperation.motive, ");//16
		sbQuery.append("	pd.accountAnnotationOperation.securityClass, ");//17
		sbQuery.append("	pd.accountAnnotationOperation.motive, ");//18
		sbQuery.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = pd.accountAnnotationOperation.motive) ");//19
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	ORDER BY pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk DESC, pd.idPayrollDetailPk ASC ");//, pd.cuponNumber
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		return query.getResultList();
	}

	public List<Object[]> getPayrollDetailCorporative(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.corporativeOperation.idCorporativeOperationPk,");
		sbQuery.append("	pd.corporativeOperation.registryDate,");
		sbQuery.append("	pd.corporativeOperation.deliveryDate,");
		sbQuery.append("	pd.corporativeOperation.securities.idSecurityCodePk,");
		sbQuery.append("	'''',");
		sbQuery.append("	co.newShareCapital,");
		sbQuery.append("	pd.idPayrollDetailPk,");
		sbQuery.append("	pd.payrollState,");
		sbQuery.append("	1");
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE co.issuer.idIssuerPk = :idIssuerPk");
		sbQuery.append("	AND co.state = :definitiveState");
		sbQuery.append("	AND co.corporativeEventType.corporativeEventType IN (:lstCorporativeTypes)");
		sbQuery.append("	ORDER BY pd.corporativeOperation.idCorporativeOperationPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		return query.getResultList();
	}

	public List<Object[]> getPayrollDetailRemoveSecurities(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.retirementOperation.custodyOperation.idCustodyOperationPk,");//0
		sbQuery.append("	pd.retirementOperation.custodyOperation.registryDate,");//1
		sbQuery.append("	pd.retirementOperation.security.idSecurityCodePk,");//2
		sbQuery.append("	pd.payrollQuantity,");//3
		sbQuery.append("	pd.idPayrollDetailPk,");//4
		sbQuery.append("	pd.payrollState,");//5
		sbQuery.append("	pd.retirementOperation.motive, ");//6
		sbQuery.append("	pd.indCupon, ");//7
		sbQuery.append("	pd.cuponNumber, ");//8
		sbQuery.append("	pd.serialCode, ");//9
		sbQuery.append("	pd.retirementOperation.physicalCertificate.nominalAmount as securityNominalValue, ");//10
		sbQuery.append("	pd.retirementOperation.security.currency, ");//11
		sbQuery.append("	pd.indReenter, ");//12
		sbQuery.append("	pd.lastReenterDate, ");//13
		sbQuery.append("	pd.retirementOperation.idRetirementOperationPk, ");//14
		sbQuery.append("	pd.idPayrollDetailPk, ");//15
		sbQuery.append("	pd.retirementOperation.security.issuer.mnemonic, ");//16
		sbQuery.append("	pd.retirementOperation.physicalCertificate.expirationDate, ");//17
		sbQuery.append("	pd.retirementOperation.physicalCertificate.nominalValue, ");//18
		sbQuery.append("	pd.retirementOperation.physicalCertificate.idPhysicalCertificatePk, ");//19
		sbQuery.append("	( select count(rd.idRetirementDetailPk) from RetirementDetail rd where rd.retirementOperation.idRetirementOperationPk = pd.retirementOperation.custodyOperation.idCustodyOperationPk ) as totalStock, ");//20 pd.payrollHeader.totalStock,
		sbQuery.append("	pd.retirementOperation.motive, ");//21
		sbQuery.append("	(select pt.parameterName from ParameterTable pt where pt.parameterTablePk = pd.retirementOperation.motive) ");//22
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	ORDER BY pd.retirementOperation.custodyOperation.idCustodyOperationPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		return query.getResultList();
	}
	
	public List<Object[]> getCuponsInRetirementDetail(Long idRetirementOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pc.SERIAL_NUMBER ||''|| pc.CERTIFICATE_NUMBER ||'-'|| pc.CUPON_NUMBER, ");//0
		sbQuery.append("	pc.CUPON_NUMBER ");//1
		sbQuery.append("	FROM RETIREMENT_DETAIL rd ");
		sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append("	WHERE rd.ID_RETIREMENT_OPERATION_FK = :idRetirementOperationPk AND pc.CUPON_NUMBER IS NOT NULL ");
		sbQuery.append("	ORDER BY pc.CUPON_NUMBER ASC");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idRetirementOperationPk", idRetirementOperationPk);	
		return query.getResultList();
	}
	
	public RetirementOperation getRetirementOperation(Long idRetirementOperationPk){
		try {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ro FROM RetirementOperation ro ");
		sbQuery.append("	JOIN FETCH ro.accountAnnotationOperation aao ");
		sbQuery.append("	WHERE ro.idRetirementOperationPk = :idRetirementOperationPk");			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idRetirementOperationPk", idRetirementOperationPk);	
		
		return (RetirementOperation) query.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}catch(Exception e) {
			return null;
		}
	}

	public AccountAnnotationOperation getAccountAnnotation(Long idAnnotationOperationPk){
		try {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao ");
		sbQuery.append("	WHERE aao.idAnnotationOperationPk = :idAnnotationOperationPk");			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);	
		
		return (AccountAnnotationOperation) query.getSingleResult();
		}catch(NoResultException e) {
			return null;
		}catch(Exception e) {
			return null;
		}
	}

	public BigDecimal getDetailsProcessed(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT NVL(COUNT(pd),0)");
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	AND pd.payrollState <> :stateGenerate");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.setParameter("stateGenerate", PayrollDetailStateType.GENERATED.getCode());
		return new BigDecimal(query.getSingleResult().toString());
	}

	public void updateDetailsAnnulate(Long idPayrollHeaderPk, LoggerUser loggerUser) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	UPDATE PayrollDetail");
		sbQuery.append("	SET payrollState = :stateAnnul");
		sbQuery.append("	, lastModifyApp = :lastModifyApp");
		sbQuery.append("	, lastModifyDate = :lastModifyDate");
		sbQuery.append("	, lastModifyIp = :lastModifyIp");
		sbQuery.append("	, lastModifyUser = :lastModifyUser");
		sbQuery.append("	WHERE payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.setParameter("stateAnnul", PayrollDetailStateType.ANNULATE.getCode());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();		
	}

	public List<Object[]> getPayrollDetailCertificateOperation(Long idPayrollHeaderPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.certificateOperation.custodyOperation.idCustodyOperationPk,");
		sbQuery.append("	pd.certificateOperation.custodyOperation.registryDate,");
		sbQuery.append("	pd.certificateOperation.security.idSecurityCodePk,");
		sbQuery.append("	pd.payrollQuantity,");
		sbQuery.append("	pd.idPayrollDetailPk,");
		sbQuery.append("	pd.payrollState,");
		sbQuery.append("	pd.certificateOperation.certificateOperationType");
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	ORDER BY pd.certificateOperation.custodyOperation.idCustodyOperationPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		return query.getResultList();
	}

	public List<PhysicalCertificate> lstPhysicalCertificatesWithSecurityCode(String idSecurityCodePk) throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT pc FROM PhysicalCertificate pc ");
			sbQuery.append("	WHERE pc.securities.idSecurityCodePk = :idSecurityCodePk ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			
			return (List<PhysicalCertificate>)query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public List<AccountAnnotationOperation> lstAccountAnnotationOperation(Long idPhysicalCertificatePk) throws ServiceException {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao ");
			sbQuery.append("	WHERE aao.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
			sbQuery.append("	ORDER BY aao.registryDate desc ");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
			
			return (List<AccountAnnotationOperation>)query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}
}
