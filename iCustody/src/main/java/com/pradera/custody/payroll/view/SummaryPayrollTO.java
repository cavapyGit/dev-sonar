package com.pradera.custody.payroll.view;

import java.io.Serializable;
import java.math.BigDecimal;

public class SummaryPayrollTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private BigDecimal totalRecords = BigDecimal.ZERO;
	private BigDecimal totalStock = BigDecimal.ZERO;
	private BigDecimal yesRecords = BigDecimal.ZERO;
	private BigDecimal yesStock = BigDecimal.ZERO;
	private BigDecimal noRecords = BigDecimal.ZERO;
	private BigDecimal noStock = BigDecimal.ZERO;
	
	public BigDecimal getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(BigDecimal totalRecords) {
		this.totalRecords = totalRecords;
	}
	public BigDecimal getTotalStock() {
		return totalStock;
	}
	public void setTotalStock(BigDecimal totalStock) {
		this.totalStock = totalStock;
	}
	public BigDecimal getYesRecords() {
		return yesRecords;
	}
	public void setYesRecords(BigDecimal yesRecords) {
		this.yesRecords = yesRecords;
	}
	public BigDecimal getYesStock() {
		return yesStock;
	}
	public void setYesStock(BigDecimal yesStock) {
		this.yesStock = yesStock;
	}
	public BigDecimal getNoRecords() {
		return noRecords;
	}
	public void setNoRecords(BigDecimal noRecords) {
		this.noRecords = noRecords;
	}
	public BigDecimal getNoStock() {
		return noStock;
	}
	public void setNoStock(BigDecimal noStock) {
		this.noStock = noStock;
	}	
}
