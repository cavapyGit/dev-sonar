package com.pradera.custody.payroll.view;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.issuancesecuritie.Issuer;

public class SearchPayrollTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long idParticipantPk;
	private Issuer issuer;
	private Integer payRollType;
	private Date generateDate;
	private Integer state;
	private Integer branchOffice;
	private GenericDataModel<PayrollHeader> lstPayrollHeader;
	private boolean blResults = true;
	
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Integer getPayRollType() {
		return payRollType;
	}
	public void setPayRollType(Integer payRollType) {
		this.payRollType = payRollType;
	}
	public Date getGenerateDate() {
		return generateDate;
	}
	public void setGenerateDate(Date generateDate) {
		this.generateDate = generateDate;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}	
	public boolean isBlResults() {
		return blResults;
	}
	public void setBlResults(boolean blResults) {
		this.blResults = blResults;
	}
	public GenericDataModel<PayrollHeader> getLstPayrollHeader() {
		return lstPayrollHeader;
	}
	public void setLstPayrollHeader(GenericDataModel<PayrollHeader> lstPayrollHeader) {
		this.lstPayrollHeader = lstPayrollHeader;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Integer getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
}
