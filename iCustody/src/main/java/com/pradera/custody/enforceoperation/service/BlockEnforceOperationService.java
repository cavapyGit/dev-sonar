package com.pradera.custody.enforceoperation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.enforceoperation.view.EnforceBlockOperationTO;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockenforce.BlockEnforceDetail;
import com.pradera.model.custody.blockenforce.BlockEnforceFile;
import com.pradera.model.custody.blockenforce.BlockEnforceMarketfact;
import com.pradera.model.custody.blockenforce.BlockEnforceOperation;
import com.pradera.model.custody.blockenforce.type.BlockEnforceRejectMotiveType;
import com.pradera.model.custody.blockenforce.type.BlockEnforceStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockEnforceOperationService.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BlockEnforceOperationService extends CrudDaoServiceBean{
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/** The user info. */
	@Inject UserInfo userInfo;
	
	/** The reversal component service. */
	@EJB
	RequestReversalsServiceBean reversalComponentService;
	
	/** The request affectation service bean. */
	@EJB
	RequestAffectationServiceBean requestAffectationServiceBean;
	
	/**
	 * Find holder account block balance list.
	 *
	 * @param cuiCode the cui code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccountBlockBalanceList(Long cuiCode) throws ServiceException{
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("Select ha.idHolderAccountPk		");
		stringQuery.append("From HolderAccount ha			");
		stringQuery.append("Inner Join ha.holderAccountDetails det		");
		stringQuery.append("Inner Join det.holder ho		");
		stringQuery.append("Inner Join ha.participant		");
		stringQuery.append("Where ho.idHolderPk = :cuiCode");
		Query query = em.createQuery(stringQuery.toString());
		query.setParameter("cuiCode", cuiCode);
		List<Long> accountsId = query.getResultList();
		if(accountsId.size()<1){
			accountsId.add(new Long(0));
		}
		StringBuilder stringQueryAcc = new StringBuilder();
		stringQueryAcc.append("Select ha								");
		stringQueryAcc.append("From HolderAccount ha										");
		stringQueryAcc.append("Inner Join Fetch ha.holderAccountDetails det					");
		stringQueryAcc.append("Inner Join det.holder ho										");
		stringQueryAcc.append("Inner Join Fetch ha.participant								");
		stringQueryAcc.append("Inner Join ha.holderAccountBalances hab						");
		stringQueryAcc.append("Where ha.idHolderAccountPk IN :accountsId					");
		stringQueryAcc.append("And 	(hab.banBalance > :zer Or hab.otherBlockBalance > :zer	");
		stringQueryAcc.append("Or 	 hab.pawnBalance > :zer )								");
		
		Query queryAcc = em.createQuery(stringQueryAcc.toString());
		queryAcc.setParameter("accountsId", accountsId);
		queryAcc.setParameter("zer", new BigDecimal(ComponentConstant.ZERO));
		
		try{
			List<HolderAccount> holderAccountList = (List<HolderAccount>)queryAcc.getResultList().stream()
					.distinct()
					.collect(Collectors.toList());
			for(HolderAccount account: holderAccountList){
				for(HolderAccountDetail accountDetail: account.getHolderAccountDetails()){
					accountDetail.getHolder().toString();
				}
			}
			return holderAccountList;
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Find holder account list.
	 *
	 * @param cuiCode the cui code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccountList(Long cuiCode) throws ServiceException{
		StringBuilder stringQuery = new StringBuilder();
		stringQuery.append("Select ha.idHolderAccountPk		");
		stringQuery.append("From HolderAccount ha			");
		stringQuery.append("Inner Join ha.holderAccountDetails det		");
		stringQuery.append("Inner Join det.holder ho			");
		stringQuery.append("Inner Join ha.participant			");
		stringQuery.append("Where ho.idHolderPk = :cuiCode		");
		stringQuery.append("And   ha.accountGroup = :investment	");
		Query query = em.createQuery(stringQuery.toString());
		query.setParameter("cuiCode", cuiCode);
		query.setParameter("investment", HolderAccountGroupType.INVERSTOR.getCode());
		List<Long> accountsId = query.getResultList();
		if(accountsId.size()<1){
			accountsId.add(new Long(0));
		}
		StringBuilder stringQueryAcc = new StringBuilder();
		stringQueryAcc.append("Select Distinct ha											");
		stringQueryAcc.append("From HolderAccount ha										");
		stringQueryAcc.append("Inner Join Fetch ha.holderAccountDetails det					");
		stringQueryAcc.append("Inner Join Fetch det.holder ho								");
		stringQueryAcc.append("Inner Join Fetch ha.participant								");
		stringQueryAcc.append("Where ha.idHolderAccountPk IN :accountsId					");
		
		Query queryAcc = em.createQuery(stringQueryAcc.toString());
		queryAcc.setParameter("accountsId", accountsId);
//		queryAcc.setParameter("zer", new BigDecimal(ComponentConstant.ZERO));
		
		try{
			return (List<HolderAccount>)queryAcc.getResultList();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Find block operation.
	 *
	 * @param accountList the account list
	 * @param blockEnforce the block enforce
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<BlockOperation> findBlockOperation(List<Long> accountList, BlockEnforceOperation blockEnforce) throws ServiceException {
		
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("SELECT bo FROM BlockOperation bo									");
		stringSql.append("INNER JOIN FETCH bo.custodyOperation cust							");
		//stringSql.append("LEFT OUTER JOIN FETCH bo.blockMarketFactOperations mark			");
		stringSql.append("INNER JOIN FETCH bo.blockRequest br								");
		stringSql.append("INNER JOIN FETCH bo.holderAccount									");
		stringSql.append("INNER JOIN FETCH bo.securities									");
		stringSql.append("INNER JOIN FETCH bo.participant									");
		stringSql.append("WHERE	bo.holderAccount.idHolderAccountPk IN :accountList	And		");
		stringSql.append("		br.blockType = :blockType 							And		");
		stringSql.append("		bo.actualBlockBalance > 0 							And		");
		stringSql.append("		br.blockEntity.idBlockEntityPk = :idBlockEntity		And		");
		stringSql.append("		bo.blockLevel = :idblockLevel						And		");
		//stringSql.append("		mark.indActive = :oneParam							And		");
		stringSql.append("		bo.blockState IN :blockOperationState	");
		stringSql.append("ORDER BY bo.idBlockOperationPk DESC");
		
		Query query = em.createQuery(stringSql.toString());
		query.setParameter("accountList", accountList);
		query.setParameter("blockType", blockEnforce.getEnforceType());
		query.setParameter("idblockLevel", LevelAffectationType.BLOCK.getCode());
		query.setParameter("blockOperationState", AffectationStateType.BLOCKED.getCode());
		query.setParameter("idBlockEntity", blockEnforce.getBlockEntity().getIdBlockEntityPk());
		//query.setParameter("oneParam", BooleanType.YES.getCode());
		
		try{
			return (List<BlockOperation>)query.getResultList();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * Registry enforce operation.
	 *
	 * @param be the be
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	public BlockEnforceOperation registryEnforceOperation(BlockEnforceOperation be) throws ServiceException{
		/*CREATE FILE TO BLoCK ENFORCE OPERATION*/
		BlockEnforceFile enforceFile = new BlockEnforceFile();
		enforceFile.setBlockEnforceOperation(be);
		enforceFile.setEnforceFile(be.getFile());
		enforceFile.setFileName(be.getFileName());
		
		/*OBTENEMOS EL NRO DE OPERACION*/
		Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_UNBLOCKING);

		/*SET FILE TO BLOCK ENFORCE OPERATION*/
		be.setBlockEnforceFiles(new ArrayList<BlockEnforceFile>());
		be.getBlockEnforceFiles().add(enforceFile);
		be.getCustodyOperation().setOperationNumber(operationNumber);
		
		create(be);
		
		for(BlockEnforceDetail enforceDetail: be.getBlockEnforceDetails()){
			if(enforceDetail.getIsSelected()){
				requestAffectationServiceBean.verifyBlockOperationAtOtherRequests(enforceDetail.getBlockOperation().getIdBlockOperationPk(), 
																				  enforceDetail.getBlockOperation().getDocumentNumber().toString());
				
				enforceDetail.setBlockEnforceOperation(be);
				create(enforceDetail);
				
				/*VERIFICAMOS SI TIENE HECHO DE MERCADO*/
				if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
					if(enforceDetail.getBlockEnforceMarketfacts()!=null){
						for(BlockEnforceMarketfact enforceMarketFact: enforceDetail.getBlockEnforceMarketfacts()){
							enforceMarketFact.setBlockEnforceDetail(enforceDetail);
							create(enforceMarketFact);
						}
					}else{
						BlockMarketFactOperation blockMarketFact = enforceDetail.getBlockOperation().getBlockMarketFactOperations().get(0);
						BlockEnforceMarketfact enforceMarketFact = new BlockEnforceMarketfact();
						enforceMarketFact.setMarketDate(blockMarketFact.getMarketDate());
						enforceMarketFact.setMarketRate(blockMarketFact.getMarketRate());
						enforceMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
						enforceMarketFact.setBlockEnforceDetail(enforceDetail);
						enforceMarketFact.setMarketQuantity(enforceDetail.getEnforceQuantity());
						create(enforceMarketFact);
					}
				}
			}
		}
		
		for(BlockEnforceFile enfoFile: be.getBlockEnforceFiles()){
			create(enfoFile);
		}
		return be;
	}
	
	/**
	 * Reject enforce operation.
	 *
	 * @param be the be
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	public BlockEnforceOperation rejectEnforceOperation(BlockEnforceOperation be) throws ServiceException{
		BlockEnforceOperation blockEnforce = this.find(BlockEnforceOperation.class, be.getIdEnforceOperationPk());
		blockEnforce.setOperationState(BlockEnforceStateType.REJECTED.getCode());
		update(blockEnforce);
		

		blockEnforce.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
		blockEnforce.getCustodyOperation().setRejectUser(userInfo.getUserAccountSession().getUserName());
		blockEnforce.getCustodyOperation().setRejectMotive(be.getRequestMotive());
		if(be.getRequestMotive().equals(BlockEnforceRejectMotiveType.OTHERS_MOTIVES.getCode())){
			blockEnforce.getCustodyOperation().setRejectMotiveOther(be.getRequestOtherMotive());
		}
		
		update(blockEnforce.getCustodyOperation());
		
		return blockEnforce;
	}
	
	/**
	 * Confirm enforce operation.
	 *
	 * @param be the be
	 * @param loggerUser the logger user
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	public BlockEnforceOperation confirmEnforceOperation(BlockEnforceOperation be, LoggerUser loggerUser) throws ServiceException{
		BlockEnforceOperation enforceOperation = this.find(BlockEnforceOperation.class, be.getIdEnforceOperationPk());
		
		/*STEEP NRO 1*/
		/*CONFIG THE COMPONENT TU UNBLOCK THE BLOCK DOCUMENTS && PUT BAlANCE LIKE AVAILABLE*/
		
		/*REGISTRAMOS EL DESBLOQUEO*/
		RegisterReversalTO registerReversalTO = getRegisterReversal(enforceOperation);
		Long idReversalBlock = reversalComponentService.saveReversalRequest(registerReversalTO);
		
		/*CONFIRMAMOS EL DESBLOQUEO*/
		RequestReversalsTO reversalTO = new RequestReversalsTO();
		reversalTO.setIdUnblockRequestPk(idReversalBlock);
		reversalTO.setBusinessProcessTypeId(BusinessProcessType.BLOCK_EXECUTION_CONFIRM.getCode());
		reversalComponentService.confirmReversalRequest(reversalTO, loggerUser, Boolean.FALSE);
		
		/*STEEP NRO 2*/
		/*CONFIG THE COMPONENT TO TRANSFER BALANCES TO HOLDERACCOUNT CREDITOR*/
		List<HolderAccountBalanceTO> accountsSource = getAccountBalanceSourceTO(enforceOperation);
		List<HolderAccountBalanceTO> accountsTarget = getAccountBalanceTargetTO(enforceOperation);
		
		accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(enforceOperation.getIdEnforceOperationPk(), 
																					   BusinessProcessType.BLOCK_EXECUTION_CONFIRM.getCode(), 
																					   accountsSource, accountsTarget, 
																					   ParameterOperationType.BLOCK_ENFORCE_EXECUTION.getCode()));
		
		/*STEEP NRO 3*/
		/*CHANGE STATE OF BLOCKENFORCE OPERATION TO CONFIRMED*/
		enforceOperation.setOperationState(BlockEnforceStateType.CONFIRMED.getCode());
		update(enforceOperation);
		return enforceOperation;
	}
	
	/**
	 * Gets the register reversal.
	 *
	 * @param be the be
	 * @return the register reversal
	 * @throws ServiceException the service exception
	 */
	private RegisterReversalTO getRegisterReversal(BlockEnforceOperation be) throws ServiceException{
		// TODO Auto-generated method stub
		RegisterReversalTO registerReversalTO = new RegisterReversalTO();
		registerReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
		
		registerReversalTO.setDocumentNumber(be.getResolutionNumber());
		registerReversalTO.setRegistryUser(userInfo.getUserAccountSession().getUserName());
		registerReversalTO.setIdHolderPk(be.getHolderSource().getIdHolderPk());
		registerReversalTO.setTotalUnblock(BigDecimal.ZERO);
		registerReversalTO.setIndValidateBlockDocument(BooleanType.NO.getCode());
		
		for(BlockEnforceDetail enforceDetail: be.getBlockEnforceDetails()){
			BlockOperation blockOperation = enforceDetail.getBlockOperation();
			
			BlockOpeForReversalsTO objBlockOpeForReversal = new BlockOpeForReversalsTO();
			objBlockOpeForReversal.setReversalsMarkFactList(new ArrayList<ReversalsMarkFactTO>());
			objBlockOpeForReversal.setIdBlockOperationPk(blockOperation.getIdBlockOperationPk());
			objBlockOpeForReversal.setIdOperationEnforceDetailFk(enforceDetail.getIdEnforceDetailPk());
			objBlockOpeForReversal.setAmountUnblock(enforceDetail.getEnforceQuantity());
			registerReversalTO.setTotalUnblock(registerReversalTO.getTotalUnblock().add(enforceDetail.getEnforceQuantity()));
			
			/*VERIFICAMOS SI ESTA ACTIVADO EL INDICADOR DE HECHOS DE MERCADO*/
			if(idepositarySetup.getIndMarketFact().equals(BooleanType.YES.getCode())){
				for(BlockEnforceMarketfact blockMarketFact: enforceDetail.getBlockEnforceMarketfacts()){
					ReversalsMarkFactTO reversalMarketFact = new ReversalsMarkFactTO();
					reversalMarketFact.setMarketDate(blockMarketFact.getMarketDate());
					reversalMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
					reversalMarketFact.setMarketRate(blockMarketFact.getMarketRate());
					reversalMarketFact.setBlockAmount(blockMarketFact.getMarketQuantity());
					
					objBlockOpeForReversal.getReversalsMarkFactList().add(reversalMarketFact);
				}
			}
			registerReversalTO.getLstBlockOpeForReversals().add(objBlockOpeForReversal);
		}
		return registerReversalTO;
	}

	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param sourceBalances the source balances
	 * @param targetBalances the target balances
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, List<HolderAccountBalanceTO> sourceBalances, List<HolderAccountBalanceTO> targetBalances, Long operationType){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		objAccountComponent.setLstSourceAccounts(sourceBalances);
		objAccountComponent.setLstTargetAccounts(targetBalances);
		objAccountComponent.setIdOperationType(operationType);
		return objAccountComponent;
	}
	
	/**
	 * Gets the account balance source to.
	 *
	 * @param enforceOperation the enforce operation
	 * @return the account balance source to
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getAccountBalanceSourceTO(BlockEnforceOperation enforceOperation) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		for(BlockEnforceDetail objBlockOperation: enforceOperation.getBlockEnforceDetails()){
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(objBlockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(objBlockOperation.getBlockOperation().getParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getBlockOperation().getSecurities().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(objBlockOperation.getEnforceQuantity());
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(BlockEnforceMarketfact marketFact: objBlockOperation.getBlockEnforceMarketfacts()){
				MarketFactAccountTO marketIntegration = new MarketFactAccountTO();
				marketIntegration.setQuantity(marketFact.getMarketQuantity());
				marketIntegration.setMarketDate(marketFact.getMarketDate());
				marketIntegration.setMarketRate(marketFact.getMarketRate());
				marketIntegration.setMarketPrice(marketFact.getMarketPrice());
				holderAccountBalanceTO.getLstMarketFactAccounts().add(marketIntegration);
			}
			accountBalanceTOs.add(holderAccountBalanceTO);
		}
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the account balance target to.
	 *
	 * @param enforceOperation the enforce operation
	 * @return the account balance target to
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getAccountBalanceTargetTO(BlockEnforceOperation enforceOperation) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		for(BlockEnforceDetail objBlockOperation: enforceOperation.getBlockEnforceDetails()){
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(enforceOperation.getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(enforceOperation.getParticipantTarget().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getBlockOperation().getSecurities().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(objBlockOperation.getEnforceQuantity());
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(BlockEnforceMarketfact marketFact: objBlockOperation.getBlockEnforceMarketfacts()){
				MarketFactAccountTO marketIntegration = new MarketFactAccountTO();
				marketIntegration.setQuantity(marketFact.getMarketQuantity());
				marketIntegration.setMarketDate(marketFact.getMarketDate());
				marketIntegration.setMarketRate(marketFact.getMarketRate());
				marketIntegration.setMarketPrice(marketFact.getMarketPrice());
				holderAccountBalanceTO.getLstMarketFactAccounts().add(marketIntegration);
			}
			accountBalanceTOs.add(holderAccountBalanceTO);
		}
		return accountBalanceTOs;
	}
	
	/**
	 * Find block enforce to list.
	 *
	 * @param enforceTO the enforce to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<EnforceBlockOperationTO> findBlockEnforceTOList(EnforceBlockOperationTO enforceTO) throws ServiceException{
		Map<String,Object> parameters = new HashMap<String,Object>();
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("Select 																												");
		stringSql.append("		 enfor.idEnforceOperationPk, enfor.custodyOperation.registryDate,												");
		stringSql.append("		 enfor.resolutionNumber,	SUM(det.enforceQuantity),															");
		stringSql.append("		 enfor.enforceType, (SELECT parameterName FROM ParameterTable where parameterTablePk = enfor.enforceType),		");
		stringSql.append("		 enfor.operationState, (SELECT parameterName FROM ParameterTable where parameterTablePk = enfor.operationState),");
		stringSql.append("		 enfor.blockEntity.idBlockEntityPk, enfor.blockEntity.fullName													");
		stringSql.append("From BlockEnforceOperation enfor																						");
		stringSql.append("Inner Join enfor.blockEnforceDetails det																				");
		stringSql.append("Where 1 = 1																											");
		
		if(enforceTO.getSearchType().equals(BooleanType.YES.getCode())){
			if(enforceTO.getIdBlockEnforcePk()!=null){
				stringSql.append("And 	enfor.idEnforceOperationPk = :enforceOperationID				");
				parameters.put("enforceOperationID", enforceTO.getIdBlockEnforcePk());
			}
		}else{
			if(enforceTO.getBlockEntity().getIdBlockEntityPk()!=null){
				stringSql.append("And	enfor.blockEntity.idBlockEntityPk = :blockEntityID				");
				parameters.put("blockEntityID", enforceTO.getBlockEntity().getIdBlockEntityPk());
			}
			if(enforceTO.getHolder().getIdHolderPk()!=null){
				stringSql.append("And	enfor.holderSource.idHolderPk = :holderID						");
				parameters.put("holderID", enforceTO.getHolder().getIdHolderPk());
			}
			if(enforceTO.getEnforceType()!=null){
				stringSql.append("And	enfor.enforceType = :enforceTypeID								");
				parameters.put("enforceTypeID", enforceTO.getEnforceType());
			}
		    if(enforceTO.getActNumber()!=null){
		    	stringSql.append("And	enfor.resolutionNumber = :actNumber								");
				parameters.put("actNumber", enforceTO.getActNumber());
		    }
			
			stringSql.append("And	Trunc(enfor.custodyOperation.registryDate) >= Trunc(:initialDate)	");
			stringSql.append("And	Trunc(enfor.custodyOperation.registryDate) <= Trunc(:finalDate)		");
			parameters.put("initialDate", enforceTO.getInitialDate());
			parameters.put("finalDate", enforceTO.getFinalDate());
			
		}

		
		stringSql.append("Group By enfor.idEnforceOperationPk, enfor.custodyOperation.registryDate, enfor.resolutionNumber, 					");
		stringSql.append("		   enfor.blockEntity.idBlockEntityPk, enfor.blockEntity.fullName, enfor.enforceType,		");
		stringSql.append("		   enfor.operationState				 																			");
		
		List<Object[]> enforceListObject = this.findListByQueryString(stringSql.toString(), parameters);
		List<EnforceBlockOperationTO> enforceList = new ArrayList<EnforceBlockOperationTO>();
		for(Object[] objectData: enforceListObject){
			EnforceBlockOperationTO enforceDataTO = new EnforceBlockOperationTO();
			enforceDataTO.setIdBlockEnforcePk((Long) objectData[0]);
			enforceDataTO.setRegistryDate((Date) objectData[1]);
			enforceDataTO.setActNumber((String) objectData[2]);
			enforceDataTO.setEnforceQuantity((BigDecimal) objectData[3]);
			enforceDataTO.setEnforceType((Integer) objectData[4]);
			enforceDataTO.setEnforceTypeDescription((String) objectData[5]);
			enforceDataTO.setState((Integer) objectData[6]);
			enforceDataTO.setStateDescription((String) objectData[7]);
			
			BlockEntity blockEntity = new BlockEntity();
			blockEntity.setIdBlockEntityPk((Long) objectData[8]);
			blockEntity.setFullName((String) objectData[9]);
			
			enforceDataTO.setBlockEntity(blockEntity);
			enforceList.add(enforceDataTO);
		}
		return enforceList;
	}
	
	/**
	 * Find block enforce by id.
	 *
	 * @param idBlockEnforceId the id block enforce id
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	public BlockEnforceOperation findBlockEnforceById(Long idBlockEnforceId) throws ServiceException{
		StringBuilder stringSql = new StringBuilder();
		stringSql.append("Select enforce From BlockEnforceOperation enforce	");
		stringSql.append("Join Fetch enforce.participantTarget part			");
		stringSql.append("Join Fetch enforce.holderAccount ha				");
		stringSql.append("Join Fetch enforce.custodyOperation cus			");
		stringSql.append("Join Fetch enforce.holderSource sourc				");
		stringSql.append("Join Fetch enforce.holderTarget targ				");
		stringSql.append("Join Fetch enforce.blockEntity blockEnti			");
		
		stringSql.append("Join Fetch enforce.blockEnforceDetails det		");
		stringSql.append("Join Fetch det.blockOperation block				");
		stringSql.append("Join Fetch block.custodyOperation custBlock		");
		stringSql.append("Join Fetch block.securities sec					");
		stringSql.append("Join Fetch block.participant part					");
		stringSql.append("Join Fetch block.blockRequest bloreq				");
		stringSql.append("Join Fetch block.holderAccount hablo				");
		stringSql.append("Where enforce.idEnforceOperationPk = :id			");
		Query query = em.createQuery(stringSql.toString());
		query.setParameter("id", idBlockEnforceId);
		BlockEnforceOperation enforceOperation = (BlockEnforceOperation) query.getSingleResult();
		
		/*DO MANUAL FETCH TO HOLDER*/
		for(BlockEnforceDetail enforceDet: enforceOperation.getBlockEnforceDetails()){
			for(HolderAccountDetail haDet: enforceDet.getBlockOperation().getHolderAccount().getHolderAccountDetails()){
				haDet.getHolder().toString();
			}
		}
		
		/*DO MANUAL FETCH TO FILES*/
		for(BlockEnforceFile file: enforceOperation.getBlockEnforceFiles()){
			enforceOperation.setFileName(file.getFileName());
			enforceOperation.setFile(file.getEnforceFile());
		}
		
		return enforceOperation;
	}
}