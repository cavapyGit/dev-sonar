package com.pradera.custody.enforceoperation.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.blockentity.BlockEntity;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DetachmentCouponsMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class EnforceBlockOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id block enforce pk. */
	private Long idBlockEnforcePk;
	
	/** The block entity. */
	private BlockEntity blockEntity;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The holder. */
	private Holder holder;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The enforce type. */
	private Integer enforceType;
	
	/** The enforce type description. */
	private String enforceTypeDescription;
	
	/** The enforce quantity. */
	private BigDecimal enforceQuantity;
	
	/** The state. */
	private Integer state;
	
	/** The state description. */
	private String stateDescription;
	
	/** The security code pk. */
	private String securityCodePk;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The act number. */
	private String actNumber;
	
	/** The search type. */
	private Integer searchType;
	

	/**
	 * Gets the id block enforce pk.
	 *
	 * @return the id block enforce pk
	 */
	public Long getIdBlockEnforcePk() {
		return idBlockEnforcePk;
	}

	/**
	 * Sets the id block enforce pk.
	 *
	 * @param idBlockEnforcePk the new id block enforce pk
	 */
	public void setIdBlockEnforcePk(Long idBlockEnforcePk) {
		this.idBlockEnforcePk = idBlockEnforcePk;
	}

	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public BlockEntity getBlockEntity() {
		return blockEntity;
	}

	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the enforce type.
	 *
	 * @return the enforce type
	 */
	public Integer getEnforceType() {
		return enforceType;
	}

	/**
	 * Sets the enforce type.
	 *
	 * @param enforceType the new enforce type
	 */
	public void setEnforceType(Integer enforceType) {
		this.enforceType = enforceType;
	}

	/**
	 * Gets the enforce quantity.
	 *
	 * @return the enforce quantity
	 */
	public BigDecimal getEnforceQuantity() {
		return enforceQuantity;
	}

	/**
	 * Sets the enforce quantity.
	 *
	 * @param enforceQuantity the new enforce quantity
	 */
	public void setEnforceQuantity(BigDecimal enforceQuantity) {
		this.enforceQuantity = enforceQuantity;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the security code pk.
	 *
	 * @return the security code pk
	 */
	public String getSecurityCodePk() {
		return securityCodePk;
	}

	/**
	 * Sets the security code pk.
	 *
	 * @param securityCodePk the new security code pk
	 */
	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the act number.
	 *
	 * @return the act number
	 */
	public String getActNumber() {
		return actNumber;
	}

	/**
	 * Sets the act number.
	 *
	 * @param actNumber the new act number
	 */
	public void setActNumber(String actNumber) {
		this.actNumber = actNumber;
	}

	/**
	 * Instantiates a new enforce block operation to.
	 */
	public EnforceBlockOperationTO() {
		super();
	}

	/**
	 * Gets the search type.
	 *
	 * @return the search type
	 */
	public Integer getSearchType() {
		return searchType;
	}

	/**
	 * Sets the search type.
	 *
	 * @param searchType the new search type
	 */
	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}

	/**
	 * Gets the enforce type description.
	 *
	 * @return the enforce type description
	 */
	public String getEnforceTypeDescription() {
		return enforceTypeDescription;
	}

	/**
	 * Sets the enforce type description.
	 *
	 * @param enforceTypeDescription the new enforce type description
	 */
	public void setEnforceTypeDescription(String enforceTypeDescription) {
		this.enforceTypeDescription = enforceTypeDescription;
	}

	/**
	 * Instantiates a new enforce block operation to.
	 *
	 * @param idBlockEnforcePk the id block enforce pk
	 * @param registryDate the registry date
	 * @param actNumber the act number
	 * @param enforceQuantity the enforce quantity
	 * @param enforceType the enforce type
	 * @param enforceTypeDescription the enforce type description
	 * @param state the state
	 * @param stateDescription the state description
	 * @param blockEntityId the block entity id
	 * @param blockEntityFullName the block entity full name
	 */
	public EnforceBlockOperationTO(Long idBlockEnforcePk, Date registryDate,
			String actNumber,BigDecimal enforceQuantity, Integer enforceType,
			String enforceTypeDescription, Integer state, String stateDescription,
			Long blockEntityId, String blockEntityFullName) {
		super();
		this.idBlockEnforcePk = idBlockEnforcePk;
		this.enforceType = enforceType;
		this.enforceTypeDescription = enforceTypeDescription;
		this.enforceQuantity = enforceQuantity;
		this.state = state;
		this.stateDescription = stateDescription;
		this.registryDate = registryDate;
		this.actNumber = actNumber;
		this.blockEntity = new BlockEntity();
		this.blockEntity.setIdBlockEntityPk(blockEntityId);
		this.blockEntity.setFullName(blockEntityFullName);
	}
}