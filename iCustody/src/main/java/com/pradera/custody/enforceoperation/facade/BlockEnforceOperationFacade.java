package com.pradera.custody.enforceoperation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.helperui.service.HolderQueryServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.enforceoperation.service.BlockEnforceOperationService;
import com.pradera.custody.enforceoperation.view.EnforceBlockOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationFormType;
import com.pradera.model.custody.blockenforce.BlockEnforceDetail;
import com.pradera.model.custody.blockenforce.BlockEnforceOperation;
import com.pradera.model.custody.blockenforce.type.BlockEnforceStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.CustodyOperationStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockEnforceOperationFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BlockEnforceOperationFacade {
	
	/** The holder service bean. */
	@EJB
	HolderQueryServiceBean holderServiceBean;
	
	/** The block enforce service. */
	@EJB
	BlockEnforceOperationService blockEnforceService;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * Gets the block enforced operation.
	 *
	 * @return the block enforced operation
	 * @throws ServiceException the service exception
	 */
	public BlockEnforceOperation getBlockEnforcedOperation() throws ServiceException{
		BlockEnforceOperation enforceOperation = new BlockEnforceOperation();
		enforceOperation.setHolderSource(new Holder());
		enforceOperation.setHolderTarget(new Holder());
		enforceOperation.setHolderAccount(new HolderAccount());
		enforceOperation.setIndBlockEntityCreditor(BooleanType.YES.getCode());
		enforceOperation.setIsSelected(Boolean.TRUE);
		enforceOperation.setEnforceForm(AffectationFormType.EXACT_AMOUNT.getCode());
		enforceOperation.setCustodyOperation(new CustodyOperation());
		enforceOperation.setBlockEnforceDetails(new ArrayList<BlockEnforceDetail>());
		enforceOperation.setBlockEntity(new BlockEntity());
		enforceOperation.setOperationState(BlockEnforceStateType.REGISTERED.getCode());
		return enforceOperation;
	}
	
	/**
	 * Find holder by id.
	 *
	 * @param cuiCode the cui code
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderById(Long cuiCode) throws ServiceException{
		Holder holder = holderServiceBean.findHolderByRnt(cuiCode);
		
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED,new Object[]{holder.getIdHolderPk()});
		}
		
		return holder;
	}
	
	/**
	 * Find block operation.
	 *
	 * @param holderAccountList the holder account list
	 * @param enforOperation the enfor operation
	 * @param parameters the parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockEnforceDetail> findBlockOperation(List<HolderAccount> holderAccountList, BlockEnforceOperation enforOperation, Map<Integer,String> parameters) throws ServiceException{
		List<Long> accountList = new ArrayList<Long>();
		List<BlockEnforceDetail> enforceDetailList = new ArrayList<BlockEnforceDetail>();
		
		for(HolderAccount account: holderAccountList){
			if(account.getSelected()){
				accountList.add(account.getIdHolderAccountPk());
			}
		}
		

		if(accountList==null || accountList.isEmpty()){
			throw new ServiceException(ErrorServiceType.HOLDER_DONT_SELECT_HOLDERACCOUNTS);
		}
		
		if(enforOperation.getBlockEntity()==null || enforOperation.getBlockEntity().getIdBlockEntityPk()==null){
			throw new ServiceException(ErrorServiceType.ENFORCE_DONT_SELECT_BLOCKENTITY);
		}
		
		List<BlockOperation> blockOperationList = blockEnforceService.findBlockOperation(accountList, enforOperation);
		if(blockOperationList==null || blockOperationList.isEmpty()){
			throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_NOT_FINDED);
		}
		
		for(BlockOperation blockOperation: blockOperationList){
			blockOperation.setBlockStateDescription(parameters.get(blockOperation.getBlockState()));
			blockOperation.setBlockLevelDescription(parameters.get(blockOperation.getBlockLevel()));
			blockOperation.getBlockRequest().setBlockTypeDescription(parameters.get(blockOperation.getBlockRequest().getBlockType()));
			blockOperation.getBlockRequest().setBlockFormDescription(parameters.get(blockOperation.getBlockRequest().getBlockForm()));

			BlockEnforceDetail enforceDetail = new BlockEnforceDetail();
			
			if(blockOperation.getBlockMarketFactOperations()!=null && blockOperation.getBlockMarketFactOperations().size() == 1){
				enforceDetail.setMarketLook(Boolean.FALSE);
			}else{
				enforceDetail.setMarketLook(Boolean.TRUE);
			}
			
			enforceDetail.setBlockOperation(blockOperation);
			enforceDetail.setIsSelected(Boolean.FALSE);
			enforceDetail.setBlockEnforceOperation(enforOperation);
			enforceDetailList.add(enforceDetail);
		}
		
		return enforceDetailList;
	}
	
	/**
	 * Validate amount.
	 *
	 * @param enforceDetail the enforce detail
	 * @param blockQuantity the block quantity
	 * @throws ServiceException the service exception
	 */
	public void validateAmount(BlockEnforceDetail enforceDetail, BigDecimal blockQuantity) throws ServiceException{
		if(enforceDetail.getBlockOperation().getActualBlockBalance().compareTo(blockQuantity) < 0){
			throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_QUANTITY);
		}
		enforceDetail.setEnforceQuantity(blockQuantity);
		enforceDetail.setBlockQuantity(enforceDetail.getBlockOperation().getActualBlockBalance());
	}
	
	/**
	 * Validate object before save.
	 *
	 * @param enforceOperation the enforce operation
	 * @throws ServiceException the service exception
	 */
	public void validateObjectBeforeSave(BlockEnforceOperation enforceOperation) throws ServiceException{
		if(enforceOperation.getFile()==null){
			throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_FILE);
		}
		
		if(enforceOperation.getHolderAccount()==null || enforceOperation.getHolderAccount().getIdHolderAccountPk()==null){
			throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_ACCOUNT_CREDITOR);
		}
		
		if(enforceOperation.getBlockEnforceDetails()==null || enforceOperation.getBlockEnforceDetails().isEmpty()){
			throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_DETAILS);
		}
		
		Boolean existDetail = Boolean.FALSE;
		for(BlockEnforceDetail enforceDetail: enforceOperation.getBlockEnforceDetails()){
			if(enforceDetail.getIsSelected()){
				existDetail = Boolean.TRUE;
				if(enforceDetail.getBlockQuantity()==null || enforceDetail.getBlockQuantity().equals(BigDecimal.ZERO)){
					throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_DETAIL_SELECTED);
				}
			}
		}
		
		if(!existDetail){
			throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_DETAILS);
		}
	}
	
	/**
	 * Find holder by block entity.
	 *
	 * @param blockEntity the block entity
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder findHolderByBlockEntity(BlockEntity blockEntity) throws ServiceException{
		Holder holder = holderServiceBean.findHolderByDocumentInfo(blockEntity.getDocumentNumber(), blockEntity.getDocumentType());
		
		if(holder==null){
			throw new ServiceException(ErrorServiceType.BLOCKENTITY_WITHOUT_HOLDER,new Object[]{blockEntity.getIdBlockEntityPk()});
		}
		blockEntity.setHolder(holder);
		if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
			throw new ServiceException(ErrorServiceType.HOLDER_NOT_REGISTERED,new Object[]{holder.getIdHolderPk()});
		}
		return holder;
	}
	
	/**
	 * Find holder account list.
	 *
	 * @param cuiCode the cui code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccountList(Long cuiCode) throws ServiceException{
		List<HolderAccount> accountList = blockEnforceService.findHolderAccountList(cuiCode);
		if(accountList==null || accountList.isEmpty())
			throw new ServiceException(ErrorServiceType.HOLDER_WITHOUT_HOLDERACCOUNTS);
		return blockEnforceService.findHolderAccountList(cuiCode);
	}
	
	/**
	 * Find holder account block balance list.
	 *
	 * @param cuiCode the cui code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccountBlockBalanceList(Long cuiCode) throws ServiceException{
		List<HolderAccount> accountList = blockEnforceService.findHolderAccountBlockBalanceList(cuiCode);
		if(accountList==null || accountList.isEmpty()) {
			throw new ServiceException(ErrorServiceType.HOLDER_WITHOUT_HOLDERACCOUNTS);
		}
		return accountList;
	}
	
	/**
	 * Register block enforce operation.
	 *
	 * @param enforceOperation the enforce operation
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_EXECUTION_REGISTER)
	public BlockEnforceOperation registerBlockEnforceOperation(BlockEnforceOperation enforceOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
	    
		/*CREAMOS EL OBJETO CUSTODY OPERATION*/
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setAudit(loggerUser);
		custodyOperation.setState(CustodyOperationStateType.REGISTERED.getCode());
		custodyOperation.setOperationType(ParameterOperationType.BLOCK_ENFORCE_EXECUTION.getCode());
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
		enforceOperation.setCustodyOperation(custodyOperation);
		
		/*SETEAMOS EL PARTICIPANTE DESTINATARIO*/
		Participant participantTarget = enforceOperation.getHolderAccount().getParticipant();
		enforceOperation.setParticipantTarget(participantTarget);
		
		/*SETEAMOS EL INDICADOR DE BLOCK ENTITY IS CREDITOR*/
		enforceOperation.setIndBlockEntityCreditor(BooleanType.get(enforceOperation.getIsSelected()).getCode());
	    
	    return blockEnforceService.registryEnforceOperation(enforceOperation);
	}
	
	/**
	 * Confirm block enforce operation.
	 *
	 * @param enforceOperation the enforce operation
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_EXECUTION_CONFIRM)
	public BlockEnforceOperation confirmBlockEnforceOperation(BlockEnforceOperation enforceOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    return blockEnforceService.confirmEnforceOperation(enforceOperation, loggerUser);
	}
	
	/**
	 * Reject block enforce operation.
	 *
	 * @param enforceOperation the enforce operation
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_EXECUTION_REJECT)
	public BlockEnforceOperation rejectBlockEnforceOperation(BlockEnforceOperation enforceOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
	    return blockEnforceService.rejectEnforceOperation(enforceOperation);
	}
	
	/**
	 * Request block enforce operation.
	 *
	 * @param enforceTO the enforce to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_EXECUTION_QUERY)
	public List<EnforceBlockOperationTO> requestBlockEnforceOperation(EnforceBlockOperationTO enforceTO) throws ServiceException{
	    return blockEnforceService.findBlockEnforceTOList(enforceTO);
	}
	
	/**
	 * Find block enforce by id.
	 *
	 * @param idBlockEnforceId the id block enforce id
	 * @return the block enforce operation
	 * @throws ServiceException the service exception
	 */
	public BlockEnforceOperation findBlockEnforceById(Long idBlockEnforceId) throws ServiceException{
		BlockEnforceOperation blockEnforce = blockEnforceService.findBlockEnforceById(idBlockEnforceId);
		if(blockEnforce.getIndBlockEntityCreditor().equals(BooleanType.YES.getCode())){
			blockEnforce.setIsSelected(Boolean.TRUE);
		}
		blockEnforce.getHolderAccount().getHolderAccountDetails().size();
		for(BlockEnforceDetail detail : blockEnforce.getBlockEnforceDetails()){
			detail.getBlockEnforceMarketfacts().size();
			if(detail.getUnblockOperation()!=null){
				detail.getUnblockOperation().getCustodyOperation().toString();
			}
		}
		return blockEnforce;
	}
	
	/**
	 * Gets the accounts from enforce details.
	 *
	 * @param enforceDetails the enforce details
	 * @return the accounts from enforce details
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getAccountsFromEnforceDetails(List<BlockEnforceDetail> enforceDetails) throws ServiceException{
		Map<Long,HolderAccount> accountsMap = new HashMap<Long, HolderAccount>();
		for(BlockEnforceDetail enforceDetail: enforceDetails){
			HolderAccount holderAccount = enforceDetail.getBlockOperation().getHolderAccount();
			if(accountsMap.get(holderAccount.getIdHolderAccountPk())==null){
				accountsMap.put(holderAccount.getIdHolderAccountPk(),holderAccount);
			}
		}
		return new ArrayList<HolderAccount>(accountsMap.values());
	}
	
	/**
	 * Validate enforce operation state.
	 *
	 * @param enforce the enforce
	 * @param viewOperation the view operation
	 * @throws ServiceException the service exception
	 */
	public void validateEnforceOperationState(EnforceBlockOperationTO enforce, ViewOperationsType viewOperation) throws ServiceException{
		Boolean canExecuteAction = Boolean.FALSE;
		if(enforce!=null && enforce.getState()!=null){
			switch(viewOperation){
				case CONFIRM:
					if(enforce.getState().equals(BlockEnforceStateType.REGISTERED.getCode())){
						canExecuteAction = Boolean.TRUE;
					};break;
				case REJECT:
					if(enforce.getState().equals(BlockEnforceStateType.REGISTERED.getCode())){
						canExecuteAction = Boolean.TRUE;
					};break;
			default:
				break;
			}
		}
		if(!canExecuteAction){
			if(enforce!=null){
				throw new ServiceException(ErrorServiceType.TEMPLATE_INCORRECT_STATE);
			}else{
				throw new ServiceException();
			}
		}
	}
}