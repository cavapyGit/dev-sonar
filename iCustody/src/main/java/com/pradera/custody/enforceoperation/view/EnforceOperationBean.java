package com.pradera.custody.enforceoperation.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.enforceoperation.facade.BlockEnforceOperationFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.blockenforce.BlockEnforceDetail;
import com.pradera.model.custody.blockenforce.BlockEnforceFile;
import com.pradera.model.custody.blockenforce.BlockEnforceMarketfact;
import com.pradera.model.custody.blockenforce.BlockEnforceOperation;
import com.pradera.model.custody.blockenforce.type.BlockEnforceRejectMotiveType;
import com.pradera.model.custody.blockenforce.type.BlockEnforceStateType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DetachmentCouponsMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@LoggerCreateBean
@DepositaryWebBean
public class EnforceOperationBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The market fact ui. */
	private String marketFactUI = "balancemarket1";
	
	/** The general parameters facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The enforce operation. */
	private BlockEnforceOperation enforceOperation;
	
	/** The search enforce operation. */
	private EnforceBlockOperationTO searchEnforceOperation;
	
	/** The enforce operation selected. */
	private EnforceBlockOperationTO enforceOperationSelected;
	
	/** The enforce operation to list. */
	private GenericDataModel<EnforceBlockOperationTO> enforceOperationTOList;
	
	/** The holder account list. */
	private List<HolderAccount> holderAccountList;
	
	/** The holder account list ac. */
	private List<HolderAccount> holderAccountListAc;
	
	/** The holder account target list. */
	private GenericDataModel<HolderAccount> holderAccountTargetList;
	
	/** The affectation type list. */
	private List<ParameterTable> affectationTypeList;
	
	/** The affectation form list. */
	private List<ParameterTable> affectationFormList;
	
	/** The map parameters desc. */
	private Map<Integer,String> mapParametersDesc;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The enforce operation facade. */
	@EJB
	private BlockEnforceOperationFacade enforceOperationFacade;
	
	/** The market fact balance help. */
	private MarketFactBalanceHelpTO marketFactBalanceHelp;
	
	/** The map security instrument type. */
	Map<Integer, String> mapSecurityInstrumentType = new HashMap<Integer,String>();
	
	/** The map account state description. */
	Map<Integer,String> mapAccountStateDescription = new HashMap<Integer,String>();
	
	/** The map enforce operation state description. */
	Map<Integer,String> mapEnforceOperationStateDescription = new HashMap<Integer,String>();
	
	
	/** The list motives. */
	private List<ParameterTable> listMotives;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		try {
			mapParametersDesc = new HashMap<Integer, String>();
			enforceOperation = new BlockEnforceOperation();
			loadAffectationTypeList();
			loadAffectationFormList();
			loadBlockOperationStates();
			loadAffectationLevelList();
			loadEnforceOperationTO();
			loadPrivilegeComponent();
			getLstRejectMotivesType();
			
			List<ParameterTable> lstInstrumentType = new ArrayList<ParameterTable>();
			mapSecurityInstrumentType = new HashMap<Integer, String>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
				lstInstrumentType.add(param);
				mapSecurityInstrumentType.put(param.getParameterTablePk(), param.getParameterName());
			}
			paramTable= new ParameterTableTO();			
			paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
			for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)){
				mapAccountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
			}
			paramTable= new ParameterTableTO();			
			paramTable.setMasterTableFk(MasterTableType.BLOCK_ENFORCE_STATE.getCode());
			for(ParameterTable param : generalParametersFacade.getListParameterTableServiceBean(paramTable)){
				mapEnforceOperationStateDescription.put(param.getParameterTablePk(),param.getParameterName());
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Load privilege component.
	 */
	private void loadPrivilegeComponent() {
		// TODO Auto-generated method stub
		PrivilegeComponent privileges = new PrivilegeComponent();
		privileges.setBtnConfirmView(Boolean.TRUE);
		privileges.setBtnRejectView(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privileges);
	}

	/**
	 * Load enforce operation to.
	 */
	public void loadEnforceOperationTO() {
		// TODO Auto-generated method stub
		searchEnforceOperation = new EnforceBlockOperationTO();
		searchEnforceOperation.setInitialDate(CommonsUtilities.currentDate());
		searchEnforceOperation.setFinalDate(CommonsUtilities.currentDate());
		searchEnforceOperation.setBlockEntity(new BlockEntity());
		searchEnforceOperation.setHolderAccount(new HolderAccount());
		searchEnforceOperation.setHolder(new Holder());
		searchEnforceOperation.setSearchType(BooleanType.NO.getCode());
		enforceOperationTOList = null;
	}

	/**
	 * Load affectation level list.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAffectationLevelList() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		for(ParameterTable pm: generalParametersFacade.getListParameterTableServiceBean(parameterTO))
			mapParametersDesc.put(pm.getParameterTablePk(),pm.getDescription());
	}

	/**
	 * Load block operation states.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadBlockOperationStates() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
		for(ParameterTable pm: generalParametersFacade.getListParameterTableServiceBean(parameterTO))
			mapParametersDesc.put(pm.getParameterTablePk(),pm.getDescription());
	}

	/**
	 * Load affectation form list.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAffectationFormList() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
		affectationFormList = generalParametersFacade.getListParameterTableServiceBean(parameterTO);
		for(ParameterTable pm: affectationFormList)
			mapParametersDesc.put(pm.getParameterTablePk(),pm.getDescription());
	}

	/**
	 * Load affectation type list.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadAffectationTypeList() throws ServiceException{
		// TODO Auto-generated method stub
		ParameterTableTO parameterTO = new ParameterTableTO();
		parameterTO.setMasterTableFk(MasterTableType.AFFECTATION_TYPE.getCode());
		affectationTypeList = generalParametersFacade.getListParameterTableServiceBean(parameterTO);
		for(ParameterTable pm: affectationTypeList)
			mapParametersDesc.put(pm.getParameterTablePk(),pm.getDescription());
	}
	
	/**
	 * Search holder account list.
	 */
	public void searchHolderAccountList() {
		cleanHolderAccountSource();
		
		try {
			Long cuiCode = enforceOperation.getHolderSource().getIdHolderPk();
			if (cuiCode != null) {
				Holder holder = enforceOperationFacade.findHolderById(cuiCode);
				holderAccountList = enforceOperationFacade.findHolderAccountBlockBalanceList(holder.getIdHolderPk());
				for (HolderAccount ha : holderAccountList) {
					ha.setStateAccountDescription(mapAccountStateDescription.get(ha.getStateAccount()));
				}
				
				Long cuiTrg = enforceOperation.getHolderTarget().getIdHolderPk(); 
				if (cuiTrg!=null && cuiTrg.equals(cuiCode)) {
					throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_CUI);
				}
				
			} else {
				enforceOperation.setHolderSource(new Holder());
				holderAccountList = null;
			}
		} catch (ServiceException e) {
			enforceOperation.setHolderSource(new Holder());
			holderAccountList = null;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Change option selected.
	 */
	public void changeOptionSelected(){
		if(searchEnforceOperation.getSearchType().equals(BooleanType.YES.getCode())){
			searchEnforceOperation.setInitialDate(CommonsUtilities.currentDate());
			searchEnforceOperation.setFinalDate(CommonsUtilities.currentDate());
			searchEnforceOperation.setBlockEntity(new BlockEntity());
			searchEnforceOperation.setHolderAccount(new HolderAccount());
			searchEnforceOperation.setHolder(new Holder());
			enforceOperationTOList = null;			
		}
		else if(searchEnforceOperation.getSearchType().equals(BooleanType.NO.getCode())){
			searchEnforceOperation.setIdBlockEnforcePk(null);
			
			enforceOperationTOList = null;
		}
	}
	
	/**
	 * Search holder account target list.
	 */
	public void searchHolderAccountTargetList(){
		holderAccountTargetList = null;
		try{
			Long cuiCode=null;
			if(enforceOperation.getHolderTarget()!=null){
				cuiCode = enforceOperation.getHolderTarget().getIdHolderPk();
				if(cuiCode != null){
					Holder holder = enforceOperationFacade.findHolderById(cuiCode);
					List<HolderAccount> holderAccountTargets = enforceOperationFacade.findHolderAccountList(holder.getIdHolderPk());
					for(HolderAccount ha : holderAccountTargets){
						ha.setStateAccountDescription(mapAccountStateDescription.get(ha.getStateAccount()));
					}
					if(!holderAccountTargets.isEmpty()){
						holderAccountTargetList = new GenericDataModel<HolderAccount>(holderAccountTargets);
					}
					Long cuiSrc = enforceOperation.getHolderSource().getIdHolderPk();
					if(cuiSrc!=null && cuiCode.equals(cuiSrc)){
						throw new ServiceException(ErrorServiceType.ENFORCE_OPERATION_CUI);
					}
				}
			}
		}catch (ServiceException e) {
			enforceOperation.setHolderTarget(new Holder());
			holderAccountTargetList = null;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Search block entity.
	 */
	public void searchBlockEntity(){
		cleanHolderAccountSource();
		BlockEntity blockEntity = enforceOperation.getBlockEntity();
		enforceOperation.setIsSelected(Boolean.FALSE);
		try{
			if(blockEntity.getIdBlockEntityPk()!=null){
				
				if(!blockEntity.getStateBlockEntity().equals(BlockEntityStateType.ACTIVATED.getCode())){
					throw new ServiceException(ErrorServiceType.BLOCK_ENTITY_OFF);
				}
				
				enforceOperation.setIsSelected(Boolean.TRUE);
				Holder holder = enforceOperationFacade.findHolderByBlockEntity(blockEntity);
				enforceOperation.setHolderTarget(holder);
				
				List<HolderAccount> holderAccountTargets = enforceOperationFacade.findHolderAccountList(holder.getIdHolderPk());
				if(!holderAccountTargets.isEmpty()){
					holderAccountTargetList = new GenericDataModel<>(holderAccountTargets);
					for(HolderAccount ha : holderAccountTargetList){
						ha.setStateAccountDescription(mapAccountStateDescription.get(ha.getStateAccount()));
					}
				}
			}
		}catch(ServiceException e){
			enforceOperation.setHolderTarget(new Holder());
			enforceOperation.setHolderAccount(new HolderAccount());
			holderAccountTargetList = null;
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Clean holder account source.
	 */
	public void cleanHolderAccountSource(){
		enforceOperation.setBlockEnforceDetails(null);
	}
	
	/**
	 * Chng ind block entity creditor.
	 */
	public void chngIndBlockEntityCreditor(){
		enforceOperation.setHolderAccount(new HolderAccount());
		
		if(enforceOperation.getIsSelected()){
			searchBlockEntity();
		}else{
			enforceOperation.setHolderTarget(new Holder());
			holderAccountTargetList = null;
		}
	}
	
	/**
	 * Search block operations.
	 */
	public void searchBlockOperations(){
		try{
			List<BlockEnforceDetail> enforceDetail = enforceOperationFacade.findBlockOperation(holderAccountList, enforceOperation, mapParametersDesc);
			if(!enforceDetail.isEmpty()){
				enforceOperation.setBlockEnforceDetails(enforceDetail);
			}
		}catch(ServiceException e){
			enforceOperation.setHolderTarget(new Holder());
			enforceOperation.setHolderAccount(new HolderAccount());
			holderAccountTargetList = null;
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Save block enforce.
	 */
	@LoggerAuditWeb
	public void saveBlockEnforce(){
		try{
			switch(ViewOperationsType.get(this.getViewOperationType())){
				case REGISTER	: 	registerBlockEnforceOperation();break;
				case CONFIRM	: 	confirmBlockEnforceOperation();	break;
				case REJECT		: 	rejectBlockEnforceOperation();	break;
				default:											break;
			}
			searcEnforceOperationTO();
			JSFUtilities.showSimpleEndTransactionDialog("searchEnforce");
		}catch(ServiceException e){
			enforceOperation.setHolderTarget(new Holder());
			enforceOperation.setHolderAccount(new HolderAccount());
			holderAccountTargetList = null;
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/**
	 * Confirm block enforce operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void confirmBlockEnforceOperation() throws ServiceException{
		// TODO Auto-generated method stub
		enforceOperationFacade.confirmBlockEnforceOperation(enforceOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage("alt.blockenforce.operation.confirm.ok",new Object[]{enforceOperation.getIdEnforceOperationPk().toString()}));
		
		// INICIO ENVIO DE NOTIFICACIONES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_EXECUTION_CONFIRM.getCode());
		Object[] parameters = {	enforceOperation.getIdEnforceOperationPk().toString() };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
				businessProcess, null, parameters);
		// FIN ENVIO DE NOTIFICACIONES
	}

	/**
	 * Validate un block quantity.
	 *
	 * @param enforceDetail the enforce detail
	 */
	public void validateUnBlockQuantity(BlockEnforceDetail enforceDetail){
		try{
			enforceOperationFacade.validateAmount(enforceDetail, enforceDetail.getEnforceQuantity());
		}catch(ServiceException e){
			enforceDetail.setEnforceQuantity(null);
			enforceDetail.setBlockQuantity(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
		enforceDetail.setBlockQuantity(enforceDetail.getEnforceQuantity());
	}
	
	/**
	 * Reject block enforce operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void rejectBlockEnforceOperation() throws ServiceException{
		// TODO Auto-generated method stub
		enforceOperationFacade.rejectBlockEnforceOperation(enforceOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				PropertiesUtilities.getMessage("alt.blockenforce.operation.reject.ok",new Object[]{enforceOperation.getIdEnforceOperationPk().toString()}));
		
		// INICIO ENVIO DE NOTIFICACIONES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_EXECUTION_REJECT.getCode());
		Object[] parameters = {	enforceOperation.getIdEnforceOperationPk().toString() };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
				businessProcess, null, parameters);
		// FIN ENVIO DE NOTIFICACIONES
	}

	/**
	 * Register block enforce operation.
	 *
	 * @throws ServiceException the service exception
	 */
	private void registerBlockEnforceOperation() throws ServiceException{
		// TODO Auto-generated method stub
		enforceOperationFacade.registerBlockEnforceOperation(enforceOperation);
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				PropertiesUtilities.getMessage("alt.blockenforce.operation.register.ok",new Object[]{enforceOperation.getIdEnforceOperationPk().toString()}));
		
		// INICIO ENVIO DE NOTIFICACIONES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_EXECUTION_REGISTER.getCode());
		Object[] parameters = {	enforceOperation.getIdEnforceOperationPk().toString() };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
				businessProcess, null, parameters);
		// FIN ENVIO DE NOTIFICACIONES
		
	}
	
	/**
	 * Before reject privilege.
	 *
	 * @return the string
	 */
	public String beforeRejectPrivilege(){
		return getEnforceOperationSelectedAction(ViewOperationsType.REJECT);
	}
	
	/**
	 * Before confirm privilege.
	 *
	 * @return the string
	 */
	public String beforeConfirmPrivilege(){
		return getEnforceOperationSelectedAction(ViewOperationsType.CONFIRM);
	}
	
	/**
	 * Load block enforce operation.
	 *
	 * @param blockEnforceId the block enforce id
	 */
	public void loadBlockEnforceOperation(Long blockEnforceId){
		try {
			enforceOperation = enforceOperationFacade.findBlockEnforceById(blockEnforceId);
			holderAccountList = enforceOperationFacade.getAccountsFromEnforceDetails(enforceOperation.getBlockEnforceDetails());
	
			for(HolderAccount ha : holderAccountList){
				ha.setStateAccountDescription(mapAccountStateDescription.get(ha.getStateAccount()));
			}
			holderAccountListAc = new ArrayList<HolderAccount>();
			holderAccountListAc.add(enforceOperation.getHolderAccount());
			
			for(HolderAccount ha : holderAccountListAc){
				ha.setStateAccountDescription(mapAccountStateDescription.get(ha.getStateAccount()));
			}

			enforceOperation.setStateDescription(mapEnforceOperationStateDescription.get(enforceOperation.getOperationState()));
			if(enforceOperation.getOperationState().equals(BlockEnforceStateType.REJECTED.getCode())){
				enforceOperation.setRequestMotive(enforceOperation.getCustodyOperation().getRejectMotive());
				if(Validations.validateIsNotNullAndNotEmpty(enforceOperation.getCustodyOperation().getRejectMotiveOther())){
					enforceOperation.setRequestOtherMotive(enforceOperation.getCustodyOperation().getRejectMotiveOther());
				}
			}
			
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Show market fact detail.
	 *
	 * @param enforceDetail the enforce detail
	 */
	public void showMarketFactDetail(BlockEnforceDetail enforceDetail){
		marketFactBalanceHelp = new MarketFactBalanceHelpTO();
		marketFactBalanceHelp.setSecurityCodePk(enforceDetail.getBlockOperation().getSecurities().getIdSecurityCodePk());
		marketFactBalanceHelp.setSecurityDescription(enforceDetail.getBlockOperation().getSecurities().getDescription());
		marketFactBalanceHelp.setInstrumentType(enforceDetail.getBlockOperation().getSecurities().getInstrumentType());
		marketFactBalanceHelp.setInstrumentDescription(mapSecurityInstrumentType.get(enforceDetail.getBlockOperation().getSecurities().getInstrumentType()));
		
		marketFactBalanceHelp.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		if(enforceDetail.getBlockQuantity()!=null && !enforceDetail.getBlockQuantity().equals(BigDecimal.ZERO)){
			marketFactBalanceHelp.setBalanceResult(enforceDetail.getBlockQuantity());
		}
		
		boolean todate = false;
		if(enforceDetail.getBlockOperation().getCustodyOperation().getRegistryDate().equals(CommonsUtilities.currentDate())){
			todate = true;
		}
		
		if(enforceDetail.getBlockEnforceMarketfacts()!=null){
			for(BlockEnforceMarketfact blockMarkFact: enforceDetail.getBlockEnforceMarketfacts()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setEnteredBalance(blockMarkFact.getMarketQuantity());
				marketDetail.setMarketDate(blockMarkFact.getMarketDate());
				marketDetail.setMarketPrice(blockMarkFact.getMarketPrice());
				marketDetail.setMarketRate(blockMarkFact.getMarketRate());
				marketFactBalanceHelp.getMarketFacBalances().add(marketDetail);
				marketFactBalanceHelp.setTodate(todate);
			}
		}
	}
	
	
	/**
	 * Gets the enforce operation selected action.
	 *
	 * @param viewOperationsType the view operations type
	 * @return the enforce operation selected action
	 */
	public String getEnforceOperationSelectedAction(ViewOperationsType viewOperationsType){
		EnforceBlockOperationTO enforceSearch = enforceOperationSelected;
		try {
			enforceOperationFacade.validateEnforceOperationState(enforceSearch, viewOperationsType);
			enforceOperation = enforceOperationFacade.findBlockEnforceById(enforceSearch.getIdBlockEnforcePk());
			setViewOperationType(viewOperationsType.getCode());
			return "viewEnforceAction";
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								e.getErrorService()!=null
														?PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams())
														:PropertiesUtilities.getMessage("error.record.not.selected"));
			JSFUtilities.showSimpleValidationDialog();
			return "";
		}
	}

		

	/**
	 * Gets the enforce operation.
	 *
	 * @return the enforce operation
	 */
	public BlockEnforceOperation getEnforceOperation() {
		return enforceOperation;
	}

	/**
	 * Sets the enforce operation.
	 *
	 * @param enforceOperation the new enforce operation
	 */
	public void setEnforceOperation(BlockEnforceOperation enforceOperation) {
		this.enforceOperation = enforceOperation;
	}
	
	/**
	 * Load new enforce operation.
	 */
	public void loadNewEnforceOperation(){
		try {
			enforceOperation = enforceOperationFacade.getBlockEnforcedOperation();
			holderAccountList = null;
			holderAccountTargetList = null;
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			enforceOperation.setEnforceType(AffectationType.PAWN.getCode());
			enforceOperation.setResolutionDate(CommonsUtilities.currentDate());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Show market fact ui component.
	 *
	 * @param enforceDetail the enforce detail
	 */
	public void showMarketFactUIComponent(BlockEnforceDetail enforceDetail){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(enforceDetail.getBlockOperation().getSecurities().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(enforceDetail.getBlockOperation().getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(enforceDetail.getBlockOperation().getParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName(getMarketFactUI());
		marketFactBalance.setIndBlockBalance(BooleanType.YES.getCode());
		marketFactBalance.setIdBlockOperationPk(enforceDetail.getBlockOperation().getIdBlockOperationPk());
		marketFactBalance.setBlockOperationType(enforceDetail.getBlockOperation().getBlockRequest().getBlockType());
		
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		if(enforceDetail.getBlockQuantity()!=null && !enforceDetail.getBlockQuantity().equals(BigDecimal.ZERO)){
			marketFactBalance.setBalanceResult(enforceDetail.getEnforceQuantity());
		}
		
		if(enforceDetail.getBlockEnforceMarketfacts()!=null){
			for(BlockEnforceMarketfact blockMarkFact: enforceDetail.getBlockEnforceMarketfacts()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setEnteredBalance(blockMarkFact.getMarketQuantity());
				marketDetail.setMarketDate(blockMarkFact.getMarketDate());
				marketDetail.setMarketPrice(blockMarkFact.getMarketPrice());
				marketDetail.setMarketRate(blockMarkFact.getMarketRate());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance);		
	}
	
	/**
	 * Receive market balance.
	 */
	public void receiveMarketBalance(){
		MarketFactBalanceHelpTO marketBal = this.marketFactBalanceHelp;
		for(BlockEnforceDetail enforceDet: enforceOperation.getBlockEnforceDetails()){
			if(enforceDet.getBlockOperation().getIdBlockOperationPk().equals(marketBal.getIdBlockOperationPk())){
				if(!marketBal.getBalanceResult().equals(BigDecimal.ZERO)){
					enforceDet.setEnforceQuantity(marketBal.getBalanceResult());
					enforceDet.setBlockQuantity(marketBal.getBalanceResult());
					enforceDet.setBlockEnforceMarketfacts(new ArrayList<BlockEnforceMarketfact>());
					
					for(MarketFactDetailHelpTO mkfDetTO: marketBal.getMarketFacBalances()){
						BlockEnforceMarketfact marketFactTO = new BlockEnforceMarketfact();
						marketFactTO.setMarketQuantity(mkfDetTO.getEnteredBalance());
						marketFactTO.setMarketDate(mkfDetTO.getMarketDate());
						marketFactTO.setMarketPrice(mkfDetTO.getMarketPrice());
						marketFactTO.setMarketRate(mkfDetTO.getMarketRate());
						enforceDet.getBlockEnforceMarketfacts().add(marketFactTO);
					}
					validateAmountBlock(enforceDet);
				}
				JSFUtilities.updateComponent("frmBlockEnforceOperation:outputPanelResult");
				return;
			}
		}
	}

	/**
	 * Validate amount block.
	 *
	 * @param enforceDet the enforce det
	 */
	private void validateAmountBlock(BlockEnforceDetail enforceDet) {
		// TODO Auto-generated method stub
		try{
			enforceOperationFacade.validateAmount(enforceDet, enforceDet.getBlockQuantity());
		}catch(ServiceException e){
			enforceDet.setEnforceQuantity(null);
			enforceDet.setBlockQuantity(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Before save enforce operation.
	 */
	public void beforeSaveEnforceOperation(){
		try{
			enforceOperationFacade.validateObjectBeforeSave(enforceOperation);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
								PropertiesUtilities.getMessage("alt.blockenforce.operation.askregister"));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}catch(ServiceException e){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/**
	 * Before confirm enforce operation.
	 */
	public void beforeConfirmEnforceOperation(){
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
							PropertiesUtilities.getMessage("alt.blockenforce.operation.askconfirm",new Object[]{enforceOperation.getIdEnforceOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Gets the lst reject motives type.
	 *
	 * @return the lst reject motives type
	 */
	public void getLstRejectMotivesType(){
		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();

			parameterTableTO.setState(ParameterTableStateType.REGISTERED
					.getCode());
			parameterTableTO
					.setMasterTableFk(MasterTableType.ENFORCE_OPERATION_MOTIVE_REJECT
							.getCode());

			listMotives = generalParametersFacade
					.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Validate back.
	 */
	public void validateBack(){
		
		if(enforceOperation.getRequestMotive()!=null){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
			JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
		}else{
			JSFUtilities.resetViewRoot();
			JSFUtilities.executeJavascriptFunction("dlgMotive.hide()");
		}
		
	}
	
    /**
     * Evaluate motive.
     */
    public void evaluateMotive() {
		if (enforceOperation.getRequestMotive()!=null && enforceOperation.getRequestMotive()
					.equals(BlockEnforceRejectMotiveType.OTHERS_MOTIVES
							.getCode())) {
			enforceOperation.setRequestOtherMotive(GeneralConstants.EMPTY_STRING);
			enforceOperation.setShowMotiveText(true);

			} else {
				enforceOperation.setShowMotiveText(false);
			}
		 }
    
    /**
     * Reset components.
     */
    public void resetComponents(){
		JSFUtilities.resetViewRoot();
		
	}
	
	/**
	 * Before reject enforce operation.
	 */
	public void beforeRejectEnforceOperation(){		
		JSFUtilities.resetViewRoot();
		getLstRejectMotivesType();
		enforceOperation.setRequestMotive(null);	
		enforceOperation.setRequestOtherMotive(GeneralConstants.EMPTY_STRING);
		enforceOperation.setShowMotiveText(false);
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT), null);
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
	
	}
	
	/**
	 * Action reject enforce operation.
	 */
	public void actionRejectEnforceOperation(){
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage("alt.blockenforce.operation.askreject",new Object[]{enforceOperation.getIdEnforceOperationPk().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Document file handler.
	 *
	 * @param event the event
	 */
	public void documentFileHandler(FileUploadEvent event){
		 String fDisplayName= fUploadValidateFile(event.getFile(),null, getfUploadFileDocumentsSize(),getfUploadFileDocumentsTypes());			 
		 if(fDisplayName!=null){
			 enforceOperation.setFile(event.getFile().getContents());
			 enforceOperation.setFileName(fDisplayName);
			 
			 /**Create list with blockEnforceDetail*/
			 BlockEnforceFile enforceFile = new BlockEnforceFile();
			 enforceFile.setBlockEnforceOperation(enforceOperation);
			 enforceFile.setEnforceFile(event.getFile().getContents());
			 enforceFile.setFileName(fDisplayName);
			 enforceFile.setFileSize(event.getFile().getSize());
			 
			 enforceOperation.setBlockEnforceFiles(new ArrayList<BlockEnforceFile>());
			 enforceOperation.getBlockEnforceFiles().add(enforceFile);
		 }
	}
	
	/**
	 * Removes the uploaded file handler.
	 */
	public void removeUploadedFileHandler(){
		enforceOperation.setFile(null);
		enforceOperation.setFileName(null);
		enforceOperation.setBlockEnforceFiles(new ArrayList<BlockEnforceFile>());
	}
	
	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile(){
		InputStream inputStream = new ByteArrayInputStream(enforceOperation.getFile());
		StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, null,	enforceOperation.getFileName());
		try {
			inputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return streamedContentFile;
	}
	
	/**
	 * Searc enforce operation to.
	 */
	@LoggerAuditWeb
	public void searcEnforceOperationTO(){
		List<EnforceBlockOperationTO> enforceTOList = null;
		enforceOperationSelected = new EnforceBlockOperationTO();
		try {
			if(Validations.validateIsNull(searchEnforceOperation.getSearchType())) {
				searchEnforceOperation.setSearchType(BooleanType.YES.getCode());
			}
			enforceTOList = enforceOperationFacade.requestBlockEnforceOperation(searchEnforceOperation);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		if(enforceTOList==null || enforceTOList.isEmpty()){
			enforceOperationTOList = new GenericDataModel<EnforceBlockOperationTO>();
		}else{
			enforceOperationTOList = new GenericDataModel<EnforceBlockOperationTO>(enforceTOList);
		}
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the affectation type list.
	 *
	 * @return the affectation type list
	 */
	public List<ParameterTable> getAffectationTypeList() {
		return affectationTypeList;
	}

	/**
	 * Sets the affectation type list.
	 *
	 * @param affectationTypeList the new affectation type list
	 */
	public void setAffectationTypeList(List<ParameterTable> affectationTypeList) {
		this.affectationTypeList = affectationTypeList;
	}

	/**
	 * Gets the affectation form list.
	 *
	 * @return the affectation form list
	 */
	public List<ParameterTable> getAffectationFormList() {
		return affectationFormList;
	}

	/**
	 * Sets the affectation form list.
	 *
	 * @param affectationFormList the new affectation form list
	 */
	public void setAffectationFormList(List<ParameterTable> affectationFormList) {
		this.affectationFormList = affectationFormList;
	}

	/**
	 * Gets the holder account list.
	 *
	 * @return the holder account list
	 */
	public List<HolderAccount> getHolderAccountList() {
		return holderAccountList;
	}

	/**
	 * Sets the holder account list.
	 *
	 * @param holderAccountList the new holder account list
	 */
	public void setHolderAccountList(List<HolderAccount> holderAccountList) {
		this.holderAccountList = holderAccountList;
	}

	/**
	 * Gets the holder account target list.
	 *
	 * @return the holder account target list
	 */
	public GenericDataModel<HolderAccount> getHolderAccountTargetList() {
		return holderAccountTargetList;
	}

	/**
	 * Sets the holder account target list.
	 *
	 * @param holderAccountTargetList the new holder account target list
	 */
	public void setHolderAccountTargetList(
			GenericDataModel<HolderAccount> holderAccountTargetList) {
		this.holderAccountTargetList = holderAccountTargetList;
	}

	/**
	 * Gets the market fact balance help.
	 *
	 * @return the market fact balance help
	 */
	public MarketFactBalanceHelpTO getMarketFactBalanceHelp() {
		return marketFactBalanceHelp;
	}

	/**
	 * Sets the market fact balance help.
	 *
	 * @param marketFactBalanceHelp the new market fact balance help
	 */
	public void setMarketFactBalanceHelp(
			MarketFactBalanceHelpTO marketFactBalanceHelp) {
		this.marketFactBalanceHelp = marketFactBalanceHelp;
	}

	/**
	 * Gets the market fact ui.
	 *
	 * @return the market fact ui
	 */
	public String getMarketFactUI() {
		return marketFactUI;
	}

	/**
	 * Sets the market fact ui.
	 *
	 * @param marketFactUI the new market fact ui
	 */
	public void setMarketFactUI(String marketFactUI) {
		this.marketFactUI = marketFactUI;
	}

	/**
	 * Gets the search enforce operation.
	 *
	 * @return the search enforce operation
	 */
	public EnforceBlockOperationTO getSearchEnforceOperation() {
		return searchEnforceOperation;
	}

	/**
	 * Sets the search enforce operation.
	 *
	 * @param searchEnforceOperation the new search enforce operation
	 */
	public void setSearchEnforceOperation(
			EnforceBlockOperationTO searchEnforceOperation) {
		this.searchEnforceOperation = searchEnforceOperation;
	}

	/**
	 * Gets the enforce operation to list.
	 *
	 * @return the enforce operation to list
	 */
	public GenericDataModel<EnforceBlockOperationTO> getEnforceOperationTOList() {
		return enforceOperationTOList;
	}

	/**
	 * Sets the enforce operation to list.
	 *
	 * @param enforceOperationTOList the new enforce operation to list
	 */
	public void setEnforceOperationTOList(
			GenericDataModel<EnforceBlockOperationTO> enforceOperationTOList) {
		this.enforceOperationTOList = enforceOperationTOList;
	}

	/**
	 * Gets the enforce operation selected.
	 *
	 * @return the enforce operation selected
	 */
	public EnforceBlockOperationTO getEnforceOperationSelected() {
		return enforceOperationSelected;
	}

	/**
	 * Sets the enforce operation selected.
	 *
	 * @param enforceOperationSelected the new enforce operation selected
	 */
	public void setEnforceOperationSelected(
			EnforceBlockOperationTO enforceOperationSelected) {
		this.enforceOperationSelected = enforceOperationSelected;
	}

	/**
	 * Gets the holder account list ac.
	 *
	 * @return the holder account list ac
	 */
	public List<HolderAccount> getHolderAccountListAc() {
		return holderAccountListAc;
	}

	/**
	 * Sets the holder account list ac.
	 *
	 * @param holderAccountListAc the new holder account list ac
	 */
	public void setHolderAccountListAc(List<HolderAccount> holderAccountListAc) {
		this.holderAccountListAc = holderAccountListAc;
	}

	/**
	 * Gets the list motives.
	 *
	 * @return the list motives
	 */
	public List<ParameterTable> getListMotives() {
		return listMotives;
	}

	/**
	 * Sets the list motives.
	 *
	 * @param listMotives the new list motives
	 */
	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}
}