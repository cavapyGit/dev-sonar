package com.pradera.custody.controlcertificates.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.issuancesecuritie.Issuer;

public class SearchControlCertificateTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long idParticipantPk;
	private Issuer issuer;
	private Integer payRollType;
	private Integer controlType;
	private Date initialDate, endDate;
	private Integer state;
	private GenericDataModel<PayrollHeader> lstPayrollHeader;
	private PayrollHeader payrollHeader;
	private List<PayrollDetailTO> lstPayrollDetailTO;
	private List<Employees> lstAnalyst;
	private Long idEmployeePk;
	private boolean blResults = true;
	
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public Integer getPayRollType() {
		return payRollType;
	}
	public void setPayRollType(Integer payRollType) {
		this.payRollType = payRollType;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}	
	public boolean isBlResults() {
		return blResults;
	}
	public void setBlResults(boolean blResults) {
		this.blResults = blResults;
	}
	public GenericDataModel<PayrollHeader> getLstPayrollHeader() {
		return lstPayrollHeader;
	}
	public void setLstPayrollHeader(GenericDataModel<PayrollHeader> lstPayrollHeader) {
		this.lstPayrollHeader = lstPayrollHeader;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getControlType() {
		return controlType;
	}
	public void setControlType(Integer controlType) {
		this.controlType = controlType;
	}
	public List<PayrollDetailTO> getLstPayrollDetailTO() {
		return lstPayrollDetailTO;
	}
	public void setLstPayrollDetailTO(List<PayrollDetailTO> lstPayrollDetailTO) {
		this.lstPayrollDetailTO = lstPayrollDetailTO;
	}
	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}
	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}
	public List<Employees> getLstAnalyst() {
		return lstAnalyst;
	}
	public void setLstAnalyst(List<Employees> lstAnalyst) {
		this.lstAnalyst = lstAnalyst;
	}
	public Long getIdEmployeePk() {
		return idEmployeePk;
	}
	public void setIdEmployeePk(Long idEmployeePk) {
		this.idEmployeePk = idEmployeePk;
	}
}
