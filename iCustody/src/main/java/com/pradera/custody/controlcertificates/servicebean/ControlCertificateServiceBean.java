package com.pradera.custody.controlcertificates.servicebean;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.controlcertificates.view.SearchControlCertificateTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;

@Stateless
public class ControlCertificateServiceBean extends CrudDaoServiceBean{

	public List<PayrollHeader> searchPayrollHeaders(SearchControlCertificateTO searchControlCertificateTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph FROM PayrollHeader ph");
		sbQuery.append("	LEFT JOIN ph.participant");
		sbQuery.append("	LEFT JOIN ph.issuer");
		sbQuery.append("	WHERE ph.payrollType = :payrollType");			
		sbQuery.append("	AND TRUNC(ph.registerDate) >= :initialDate");
		sbQuery.append("	AND TRUNC(ph.registerDate) <= :endDate");
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getIdParticipantPk()))
			sbQuery.append("	AND ph.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getIssuer().getIdIssuerPk()))
			sbQuery.append("	AND ph.issuer.idIssuerPk = :idIssuerPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getState()))
			sbQuery.append("	AND ph.payrollState = :payrollState");
		sbQuery.append("	ORDER BY ph.idPayrollHeaderPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", searchControlCertificateTO.getPayRollType());		
		query.setParameter("initialDate", searchControlCertificateTO.getInitialDate());		
		query.setParameter("endDate", searchControlCertificateTO.getEndDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchControlCertificateTO.getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getIssuer().getIdIssuerPk()))
			query.setParameter("idIssuerPk", searchControlCertificateTO.getIssuer().getIdIssuerPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getState()))
			query.setParameter("payrollState", searchControlCertificateTO.getState());
		return (List<PayrollHeader>)query.getResultList();
	}

	public List<Object[]> getPayrollDetailAccountAnnotation(Long idPayrollHeaderPk, Long idEmployeesPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk,");//0
		sbQuery.append("	pd.accountAnnotationOperation.custodyOperation.registryDate,");//1
		sbQuery.append("	pd.accountAnnotationOperation.security.idSecurityCodePk,");//2
		sbQuery.append("	pd.accountAnnotationOperation.holderAccount.idHolderAccountPk,");//3
		sbQuery.append("	pd.payrollQuantity,");//4
		sbQuery.append("	pd.idPayrollDetailPk,");//5
		sbQuery.append("	pd.payrollState,");//6
		sbQuery.append("	pd.accountAnnotationOperation.indSerializable,");//7
		sbQuery.append("	pd.fileName,");//8
		sbQuery.append("	pd.documentFile,");//9
		sbQuery.append("	pd.employees.idEmployeePk,");//10
		sbQuery.append("	pd.secondEmployees.idEmployeePk,");//11
		sbQuery.append("	pd.indCupon,");//12
		sbQuery.append("	pd.cuponNumber,");//13
		sbQuery.append("	pd.serialCode,");//14
		sbQuery.append(" (	case when pd.indCupon = 0 then ");
		sbQuery.append("	nvl(pd.accountAnnotationOperation.securityNominalValue,0) ");//15
		sbQuery.append("	else ");
		sbQuery.append("	(select nvl(aac.cuponAmount,0) from AccountAnnotationCupon aac where aac.accountAnnotationOperation = pd.accountAnnotationOperation.idAnnotationOperationPk and aac.cuponNumber = pd.cuponNumber ) ");
		sbQuery.append("	end ) as securityNominalValue, ");
		sbQuery.append("	pd.accountAnnotationOperation.securityCurrency, ");//16
		sbQuery.append("	pd.accountAnnotationOperation.issuer.mnemonic ");//17
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	AND pd.payrollState NOT IN (:generated, :annulate)");
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeesPk)){
			sbQuery.append("	AND (pd.employees.idEmployeePk = :idEmployeesPk OR");
			sbQuery.append("		pd.secondEmployees.idEmployeePk = :idEmployeesPk)");	
		}
		sbQuery.append("	ORDER BY pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk, pd.serialCode ,pd.cuponNumber ASC ");//pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk, 
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.setParameter("generated", PayrollDetailStateType.GENERATED.getCode());
		query.setParameter("annulate", PayrollDetailStateType.ANNULATE.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeesPk))			
			query.setParameter("idEmployeesPk", idEmployeesPk);
		return query.getResultList();
	}

	public List<Object[]> getPayrollDetailCorporative(Long idPayrollHeaderPk, Long idEmployeesPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.corporativeOperation.idCorporativeOperationPk,");
		sbQuery.append("	pd.corporativeOperation.registryDate,");
		sbQuery.append("	pd.corporativeOperation.deliveryDate,");
		sbQuery.append("	pd.corporativeOperation.securities.idSecurityCodePk,");
		sbQuery.append("	'''',");
		sbQuery.append("	co.newShareCapital,");
		sbQuery.append("	pd.idPayrollDetailPk,");
		sbQuery.append("	pd.payrollState,");
		sbQuery.append("	1,");
		sbQuery.append("	pd.fileName,");
		sbQuery.append("	pd.documentFile,");
		sbQuery.append("	pd.employees.idEmployeePk,");
		sbQuery.append("	pd.secondEmployees.idEmployeePk");
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE co.issuer.idIssuerPk = :idIssuerPk");
		sbQuery.append("	AND co.state = :definitiveState");
		sbQuery.append("	AND co.corporativeEventType.corporativeEventType IN (:lstCorporativeTypes)");
		sbQuery.append("	AND pd.payrollState NOT IN (:generated, :annulate)");
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeesPk)){
			sbQuery.append("	AND (pd.employees.idEmployeePk = :idEmployeesPk OR");
			sbQuery.append("		pd.secondEmployees.idEmployeePk = :idEmployeesPk)");	
		}
		sbQuery.append("	ORDER BY pd.corporativeOperation.idCorporativeOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		query.setParameter("generated", PayrollDetailStateType.GENERATED.getCode());
		query.setParameter("annulate", PayrollDetailStateType.ANNULATE.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeesPk))			
			query.setParameter("idEmployeesPk", idEmployeesPk);
		return query.getResultList();
	}

	public List<Object[]> getPayrollDetailRemoveSecurities(Long idPayrollHeaderPk, Long idEmployeesPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.retirementOperation.custodyOperation.idCustodyOperationPk,");//0
		sbQuery.append("	pd.retirementOperation.custodyOperation.registryDate,");
		sbQuery.append("	pd.retirementOperation.security.idSecurityCodePk,");
		sbQuery.append("	pd.payrollQuantity,");
		sbQuery.append("	pd.idPayrollDetailPk,");
		sbQuery.append("	pd.payrollState,");
		sbQuery.append("	pd.fileName,");
		sbQuery.append("	pd.documentFile,");
		sbQuery.append("	pd.employees.idEmployeePk,");
		sbQuery.append("	pd.secondEmployees.idEmployeePk,");
		sbQuery.append("	pd.retirementOperation.motive,");//10
		sbQuery.append("	pd.serialCode, ");//11
		sbQuery.append("	pd.indCupon, ");//12
		sbQuery.append("	pd.cuponNumber, ");//13
		sbQuery.append("	pd.retirementOperation.security.initialNominalValue, ");//14
		sbQuery.append("	pd.retirementOperation.security.currency, ");//15
		sbQuery.append("	pd.retirementOperation.security.issuer.mnemonic, ");//16
		sbQuery.append("	pd.retirementOperation.physicalCertificate, ");//17
		sbQuery.append("	pd.retirementDetail.physicalCertificate.nominalAmount ");//18
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	AND pd.payrollState NOT IN (:generated, :annulate)");
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeesPk)){
			sbQuery.append("	AND (pd.employees.idEmployeePk = :idEmployeesPk OR");
			sbQuery.append("		pd.secondEmployees.idEmployeePk = :idEmployeesPk)");	
		}
		sbQuery.append("	ORDER BY pd.retirementOperation.custodyOperation.idCustodyOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		query.setParameter("generated", PayrollDetailStateType.GENERATED.getCode());
		query.setParameter("annulate", PayrollDetailStateType.ANNULATE.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeesPk))			
			query.setParameter("idEmployeesPk", idEmployeesPk);
		return query.getResultList();
	}

	public List<Employees> searchAnalysts(Integer rolType) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT e");
		sbQuery.append("	FROM Employees e");
		sbQuery.append("	WHERE e.rolType = :rolType");
		sbQuery.append("	AND e.employeeState = :state");
		sbQuery.append("	ORDER BY e.employeeLastNames ASC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("rolType", rolType);
		query.setParameter("state", 1);
		return query.getResultList();
	}

	public Employees getAnalyst(Integer idUserAccountPk, Integer rolType) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT e");
			sbQuery.append("	FROM Employees e");
			sbQuery.append("	WHERE e.rolType = :rolType");
			sbQuery.append("	AND e.employeeState = :state");
			sbQuery.append("	AND e.idUserFk = :idUserAccountPk");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idUserAccountPk", new Long(idUserAccountPk));
			query.setParameter("rolType", rolType);
			query.setParameter("state", 1);
			return (Employees) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Object[]> getPayrollDetailCertificateOperation(Long idPayrollHeaderPk, Long idEmployeePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.certificateOperation.custodyOperation.idCustodyOperationPk,");
		sbQuery.append("	pd.certificateOperation.custodyOperation.registryDate,");
		sbQuery.append("	pd.certificateOperation.security.idSecurityCodePk,");
		sbQuery.append("	pd.payrollQuantity,");
		sbQuery.append("	pd.idPayrollDetailPk,");
		sbQuery.append("	pd.payrollState,");
		sbQuery.append("	pd.fileName,");
		sbQuery.append("	pd.documentFile,");
		sbQuery.append("	pd.employees.idEmployeePk,");
		sbQuery.append("	pd.secondEmployees.idEmployeePk,");
		sbQuery.append("	pd.certificateOperation.certificateOperationType");
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		sbQuery.append("	AND pd.payrollState NOT IN (:generated, :annulate)");
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeePk)){
			sbQuery.append("	AND (pd.employees.idEmployeePk = :idEmployeesPk OR");
			sbQuery.append("		pd.secondEmployees.idEmployeePk = :idEmployeesPk)");	
		}
		sbQuery.append("	ORDER BY pd.certificateOperation.custodyOperation.idCustodyOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
		query.setParameter("generated", PayrollDetailStateType.GENERATED.getCode());
		query.setParameter("annulate", PayrollDetailStateType.ANNULATE.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(idEmployeePk))			
			query.setParameter("idEmployeesPk", idEmployeePk);
		return query.getResultList();	
	}

	public List<PayrollDetail> searchPayrollDetails(Long idPayrollHeadPk, Integer payrollState) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph ");
		sbQuery.append("	FROM PayrollHeader ph ");
		sbQuery.append("	JOIN FETCH ph.lstPayrollDetail pd ");
		sbQuery.append("	WHERE ");
		sbQuery.append("	ph.idPayrollHeaderPk = :idPayrollHeadPk ");
		sbQuery.append("	AND pd.payrollState = :payrollState ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeadPk", idPayrollHeadPk);
		query.setParameter("payrollState", payrollState);
		
		List<PayrollDetail> lstPayrollDetail = null;
		try {
			List<PayrollHeader> lstPh = query.getResultList();
			if(lstPh!=null && lstPh.size()>0) {
				lstPayrollDetail = new ArrayList<PayrollDetail>();
				lstPayrollDetail.addAll(lstPh.get(0).getLstPayrollDetail());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return lstPayrollDetail;
	}

}
