package com.pradera.custody.controlcertificates.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.custody.controlcertificates.servicebean.ControlCertificateServiceBean;
import com.pradera.custody.controlcertificates.view.SearchControlCertificateTO;
import com.pradera.custody.dematerializationcertificate.service.DematerializationCertificateServiceBean;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.retirementoperation.service.RetirementOperationServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.custody.payroll.ControlType;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.generalparameter.type.CurrencyType;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class ControlCertificateFacade {
	
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	@EJB
	ControlCertificateServiceBean controlCertificateServiceBean;
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	@Inject
	DematerializationCertificateServiceBean dematerializationCertificateServiceBean;
	@Inject
	RetirementOperationServiceBean retirementOperationServiceBean;
	
	public List<PayrollHeader> searchPayrollHeaders(SearchControlCertificateTO searchControlCertificateTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return controlCertificateServiceBean.searchPayrollHeaders(searchControlCertificateTO);
	}

	public List<PayrollDetailTO> getPayrollDetail(Long idPayrollHeaderPk, Integer payrollType, Long idEmployeePk) throws ServiceException{
		List<Object[]> lstResult = null;
		List<PayrollDetailTO> lstPayrollDetailTO = new ArrayList<>();
		/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollType))
			lstResult = controlCertificateServiceBean.getPayrollDetailCorporative(idPayrollHeaderPk, idEmployeePk);
		else */
		if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollType)) {
			lstResult = controlCertificateServiceBean.getPayrollDetailAccountAnnotation(idPayrollHeaderPk, idEmployeePk);
		}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType)) {
			lstResult = controlCertificateServiceBean.getPayrollDetailRemoveSecurities(idPayrollHeaderPk, idEmployeePk);
		}
		/*else if(PayrollType.CERTIFICATE_OPERATION.getCode().equals(payrollType))
			lstResult = controlCertificateServiceBean.getPayrollDetailCertificateOperation(idPayrollHeaderPk, idEmployeePk);*/
		for(Object[] objResult:lstResult){
			PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
			/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollType))
				payrollDetailTO.setIdCorporativeOperationPk(new Long(objResult[0].toString()));				
			else */
			{
				payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[0].toString()));
				payrollDetailTO.setRegistryDate((Date) objResult[1]);
				payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNullAndNotEmpty(objResult[2])?objResult[2].toString():null);
				if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType)
						|| PayrollType.CERTIFICATE_OPERATION.getCode().equals(payrollType)){
					payrollDetailTO.setQuantity((BigDecimal) objResult[3]);
					payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[4].toString()));
					payrollDetailTO.setState(new Integer(objResult[5].toString()));
					payrollDetailTO.setFileName(objResult[6]!=null?objResult[6].toString():null);
					payrollDetailTO.setDocumentFile(objResult[7]!=null?(byte[])objResult[7]:null);
					if(Validations.validateIsNotNullAndNotEmpty(objResult[8]))				
						payrollDetailTO.setEmployee(controlCertificateServiceBean.find(Employees.class, objResult[8]));
					if(Validations.validateIsNotNullAndNotEmpty(objResult[9]))				
						payrollDetailTO.setEmployee2(controlCertificateServiceBean.find(Employees.class, objResult[9]));
					payrollDetailTO.setOperationType(new Integer(objResult[10].toString()));
					

					
					if(objResult[11]!=null) {
						payrollDetailTO.setSerialCode(objResult[11].toString());
					}
					Integer indCupon = new Integer(objResult[12].toString());
					payrollDetailTO.setBlIsCupon((indCupon==1));
					payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[13].toString()):null);
					payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[14]);
					

					Integer currency = new Integer(objResult[15].toString());
					payrollDetailTO.setCurrencyDesc(CurrencyType.get(currency).getCodeIso());

					payrollDetailTO.setMnemonicIssuer(objResult[16].toString());
					PhysicalCertificate physicalCertificate = (PhysicalCertificate)objResult[17];
					payrollDetailTO.setIndSerializable( (physicalCertificate.getExpirationDate() == null)?1:0 );
					
					if(objResult[18]!=null) {
						payrollDetailTO.setNominalAmount((BigDecimal)objResult[18]);
					}else {
						payrollDetailTO.setNominalAmount((BigDecimal)objResult[14]);
					}
					
					AccountAnnotationOperation accountAnnotationOP = null;
					
					if(payrollDetailTO.getIndSerializable().equals(1)) {
						accountAnnotationOP = retirementOperationServiceBean.getAccountAnnotationOperation(physicalCertificate.getIdPhysicalCertificatePk());
					}else {
						if(payrollDetailTO.isBlIsCupon()) {
							accountAnnotationOP = retirementOperationServiceBean.getAccountAnnotationOperation(physicalCertificate.getPhysicalCertificateReference());
						}else {
							accountAnnotationOP = retirementOperationServiceBean.getAccountAnnotationOperation(physicalCertificate.getIdPhysicalCertificatePk());
						}
					}
					
					if(accountAnnotationOP!=null) {
						payrollDetailTO.setIdCustodyOperationCertificatePk(accountAnnotationOP.getIdAnnotationOperationPk());
					}
					
				}else if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollType)){
					HolderAccountTO temp = new HolderAccountTO();
					temp.setIdHolderAccountPk(new Long(objResult[3].toString()));
					payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
					payrollDetailTO.setQuantity((BigDecimal) objResult[4]);
					payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[5].toString()));
					payrollDetailTO.setState(new Integer(objResult[6].toString()));
					payrollDetailTO.setIndSerializable(new Integer(objResult[7].toString()));
					payrollDetailTO.setFileName(objResult[8]!=null?objResult[8].toString():null);
					payrollDetailTO.setDocumentFile(objResult[9]!=null?(byte[])objResult[9]:null);
					payrollDetailTO.setIdCustodyOperationCertificatePk(new Long(objResult[0].toString()));

					Integer indCupon = new Integer(objResult[12].toString());
					payrollDetailTO.setBlIsCupon((indCupon==1));
					payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[13].toString()):null);

					if(objResult[14]!=null) {
						payrollDetailTO.setSerialCode(objResult[14].toString());
					}
					if(objResult[15]!=null) {
						payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[15]);
					}
					
					Integer currency = new Integer(objResult[16].toString());
					payrollDetailTO.setCurrencyDesc(CurrencyType.get(currency).getCodeIso());
					payrollDetailTO.setMnemonicIssuer(objResult[17].toString());
					
					if(Validations.validateIsNotNullAndNotEmpty(objResult[10]))				
						payrollDetailTO.setEmployee(controlCertificateServiceBean.find(Employees.class, objResult[10]));
					if(Validations.validateIsNotNullAndNotEmpty(objResult[11]))				
						payrollDetailTO.setEmployee2(controlCertificateServiceBean.find(Employees.class, objResult[11]));
				}				
			}		
			lstPayrollDetailTO.add(payrollDetailTO);
		}
		return lstPayrollDetailTO;
	}

	public void processControlRevert(PayrollDetailTO payrollDetailTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
			PayrollDetail payrollDetail = controlCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
			if(PayrollDetailStateType.ANALISIS2.getCode().equals(payrollDetailTO.getState())){
				payrollDetail.setPayrollState(PayrollDetailStateType.ANALISIS1.getCode());
			}else if(PayrollDetailStateType.ANALYZED.getCode().equals(payrollDetailTO.getState())){
				payrollDetail.setPayrollState(PayrollDetailStateType.ANALISIS2.getCode());
			}	
	}
	
	public void processControl(SearchControlCertificateTO searchControlCertificateTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		if(ControlType.MICROFILMACION.getCode().equals(searchControlCertificateTO.getControlType())){
			for(PayrollDetailTO payrollDetailTO:searchControlCertificateTO.getLstPayrollDetailTO()){
				if(payrollDetailTO.isBlSelected()){
					PayrollDetail payrollDetail = controlCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
					if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(searchControlCertificateTO.getPayRollType()) 
							|| PayrollDetailStateType.VAULT.getCode().equals(payrollDetail.getPayrollState())
							|| PayrollDetailStateType.ANNULATE.getCode().equals(payrollDetail.getPayrollState())
						){
						if(PayrollDetailStateType.GENERATED.getCode().equals(payrollDetail.getPayrollState()))
							throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
					}
					
					if( payrollDetail.getPayrollState().equals(PayrollDetailStateType.RECEIVED.getCode()) || PayrollDetailStateType.TRANSIT.getCode().equals(payrollDetail.getPayrollState()) ){
						payrollDetail.setPayrollState(PayrollDetailStateType.MICROFILMACION.getCode());
					}
					payrollDetail.setFileName(payrollDetailTO.getFileName());
					payrollDetail.setDocumentFile(payrollDetailTO.getDocumentFile());
					
				}
			}
		}else if(ControlType.COORDINACION.getCode().equals(searchControlCertificateTO.getControlType())){
			for(PayrollDetailTO payrollDetailTO:searchControlCertificateTO.getLstPayrollDetailTO()){
				if( (payrollDetailTO.isBlAnalyst1() && payrollDetailTO.isBlAnalyst2()) ||
					(payrollDetailTO.getEmployee()!=null && payrollDetailTO.getEmployee2()!=null)
				){
					PayrollDetail payrollDetail = controlCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
					/*if(!PayrollDetailStateType.MICROFILMACION.getCode().equals(payrollDetail.getPayrollState()))
						throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);*/
					if(payrollDetail.getEmployees()==null && payrollDetail.getSecondEmployees()==null) {
						payrollDetail.setPayrollState(PayrollDetailStateType.ANALISIS1.getCode());
						payrollDetail.setEmployees(payrollDetailTO.getEmployee());
						payrollDetail.setSecondEmployees(payrollDetailTO.getEmployee2());
					}else {
						payrollDetail.setEmployees(payrollDetailTO.getEmployee());
						payrollDetail.setSecondEmployees(payrollDetailTO.getEmployee2());
					}
				}
			}
		}else if(ControlType.ANALISIS.getCode().equals(searchControlCertificateTO.getControlType())){
			for(PayrollDetailTO payrollDetailTO:searchControlCertificateTO.getLstPayrollDetailTO()){
				if(payrollDetailTO.isBlSelected()){
					PayrollDetail payrollDetail = controlCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
					if(PayrollDetailStateType.ANALISIS1.getCode().equals(payrollDetailTO.getState())){
						if(!PayrollDetailStateType.ANALISIS1.getCode().equals(payrollDetail.getPayrollState()))
							throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
						payrollDetail.setPayrollState(PayrollDetailStateType.ANALISIS2.getCode());
					}else if(PayrollDetailStateType.ANALISIS2.getCode().equals(payrollDetailTO.getState())){
						if(!PayrollDetailStateType.ANALISIS2.getCode().equals(payrollDetail.getPayrollState()))
							throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
						payrollDetail.setPayrollState(PayrollDetailStateType.ANALYZED.getCode());
						if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(searchControlCertificateTO.getPayRollType())){
//							dematerializationCertificateServiceBean.confirmAnnotationOperation(payrollDetail.getAccountAnnotationOperation(),loggerUser, false);							
						}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(searchControlCertificateTO.getPayRollType())){							
//							retirementOperationServiceBean.changeStateRetirementOperation(payrollDetail.getRetirementOperation(), loggerUser, RetirementOperationStateType.CONFIRMED.getCode());	
							
							//notificar
						}
					}					
				}
			}
		}		
	}

	public List<Employees> searchAnalysts(Integer rolType) throws ServiceException{
		return controlCertificateServiceBean.searchAnalysts(rolType);
	}

	public Employees getAnalyst(Integer idUserAccountPk, Integer rolType) throws ServiceException{
		return controlCertificateServiceBean.getAnalyst(idUserAccountPk, rolType);
	}

	public List<PayrollDetail> searchPayrollDetails(Long idPayrollHeadPk, Integer payrollState) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return controlCertificateServiceBean.searchPayrollDetails(idPayrollHeadPk, payrollState);
	}
}
