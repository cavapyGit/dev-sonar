package com.pradera.custody.controlcertificates.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
//import org.jboss.solder.exception.control.ExceptionToCatch;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.controlcertificates.facade.ControlCertificateFacade;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.payroll.ControlType;
import com.pradera.model.custody.payroll.Employees;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.process.BusinessProcess;

@DepositaryWebBean
@LoggerCreateBean
public class ControlCertificateManageBean extends GenericBaseBean {
	
	private static final long serialVersionUID = 1L;
	private Map<Integer, String> mpStates, mpPayrollTypes, mpDetailStates, mpOperationTypes;
	private List<ParameterTable> lstStates, lstPayrollTypes;
	private SearchControlCertificateTO searchControlCertificateTO;
	private List<Participant> lstParticipants;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@Inject
	AccountsFacade accountsFacade;
	@Inject
	ControlCertificateFacade controlCertificateFacade;
	@Inject
	UserInfo userInfo;

	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	@PostConstruct
	public void init(){
		try {
			fillParameters();
			searchControlCertificateTO = new SearchControlCertificateTO();
			searchControlCertificateTO.setInitialDate(getCurrentSystemDate());
			searchControlCertificateTO.setEndDate(getCurrentSystemDate());
			searchControlCertificateTO.setIssuer(new Issuer());
			searchControlCertificateTO.setState(PayrollHeaderStateType.PROCESSED.getCode());
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			lstParticipants = accountsFacade.getLisParticipantServiceBean(filterPar);
		} catch (Exception e){
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void clean(){
		JSFUtilities.resetViewRoot();
		searchControlCertificateTO = new SearchControlCertificateTO();
		searchControlCertificateTO.setInitialDate(getCurrentSystemDate());
		searchControlCertificateTO.setEndDate(getCurrentSystemDate());
		searchControlCertificateTO.setIssuer(new Issuer());
		searchControlCertificateTO.setState(PayrollHeaderStateType.PROCESSED.getCode());
	}
	
	@LoggerAuditWeb
	public void search(Integer controlType){
		try {
			cleanResult();
			searchControlCertificateTO.setControlType(controlType);
			searchControlCertificateTO.setBlResults(false);
			List<PayrollHeader> lstResult = controlCertificateFacade.searchPayrollHeaders(searchControlCertificateTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(PayrollHeader payrollHeader:lstResult){
					payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
					payrollHeader.setPayrollTypeDesc(mpPayrollTypes.get(payrollHeader.getPayrollType()));
				}
				searchControlCertificateTO.setLstPayrollHeader(new GenericDataModel<>(lstResult));
				searchControlCertificateTO.setBlResults(true);				
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void assignAnalyst(){
		if(Validations.validateIsNotNullAndNotEmpty(searchControlCertificateTO.getIdEmployeePk())){
			for(PayrollDetailTO payrollDetailTO:searchControlCertificateTO.getLstPayrollDetailTO()){
				if(payrollDetailTO.isBlAnalyst1() && payrollDetailTO.isBlAnalyst2() && payrollDetailTO.getEmployee() == null
						&& payrollDetailTO.getEmployee2() == null){
					for(Employees emp:searchControlCertificateTO.getLstAnalyst()){
						if(emp.getIdEmployeePk().equals(searchControlCertificateTO.getIdEmployeePk())){
							payrollDetailTO.setEmployee(emp);
							payrollDetailTO.setBlAnalyst1(true);//POR EL RENDER SE VUELVE FALSE POR ELLO SE FUERZA EL TRUE
							break;
						}
					}
					payrollDetailTO.setBlAnalyst2(false);
				}else if(payrollDetailTO.isBlAnalyst1() && payrollDetailTO.getEmployee() == null){
					if(payrollDetailTO.getEmployee2() != null && 
							payrollDetailTO.getEmployee2().getIdEmployeePk().equals(searchControlCertificateTO.getIdEmployeePk()))
						payrollDetailTO.setBlAnalyst1(false);
					else{
						for(Employees emp:searchControlCertificateTO.getLstAnalyst()){
							if(emp.getIdEmployeePk().equals(searchControlCertificateTO.getIdEmployeePk())){
								payrollDetailTO.setEmployee(emp);
								payrollDetailTO.setBlAnalyst1(true);
								break;
							}
						}
					}
				}else if(payrollDetailTO.isBlAnalyst2() && payrollDetailTO.getEmployee2() == null){
					if(payrollDetailTO.getEmployee() != null && 
							payrollDetailTO.getEmployee().getIdEmployeePk().equals(searchControlCertificateTO.getIdEmployeePk()))
						payrollDetailTO.setBlAnalyst2(false);
					else{
						for(Employees emp:searchControlCertificateTO.getLstAnalyst()){
							if(emp.getIdEmployeePk().equals(searchControlCertificateTO.getIdEmployeePk())){
								payrollDetailTO.setEmployee2(emp);
								payrollDetailTO.setBlAnalyst2(true);
								break;
							}	
						}
					}
				}
			}
		}
		searchControlCertificateTO.setIdEmployeePk(null);
	}

	public void toogleA1() {
		for(PayrollDetailTO payrollDetailTO: searchControlCertificateTO.getLstPayrollDetailTO()) {
			editAnalyst(payrollDetailTO);
		}
	}
	public void toogleA2() {
		for(PayrollDetailTO payrollDetailTO: searchControlCertificateTO.getLstPayrollDetailTO()) {
			editAnalyst2(payrollDetailTO);
		}
	}
	
	public void editAnalyst(PayrollDetailTO payrollDetailTO){
		payrollDetailTO.setEmployee(null);
		payrollDetailTO.setBlAnalyst1(false);
		if(payrollDetailTO.getEmployee2() == null)
			payrollDetailTO.setBlAnalyst2(false);
	}
	
	public void editAnalyst2(PayrollDetailTO payrollDetailTO){
		payrollDetailTO.setEmployee2(null);
		payrollDetailTO.setBlAnalyst2(false);
	}
	
	public void checkAllblAnalyst1() {
		if(searchControlCertificateTO.getLstPayrollDetailTO()!=null) {
			for(PayrollDetailTO detail: searchControlCertificateTO.getLstPayrollDetailTO()) {
				if(detail.getEmployee() == null && ( detail.getState().equals(PayrollDetailStateType.MICROFILMACION.getCode()) || detail.getState().equals(PayrollDetailStateType.ANALISIS1.getCode())) ) {
					detail.setBlAnalyst1(true);
				}
			}
		}
	}

	public void checkAllblAnalyst2() {
		if(searchControlCertificateTO.getLstPayrollDetailTO()!=null) {
			for(PayrollDetailTO detail: searchControlCertificateTO.getLstPayrollDetailTO()) {
				if(detail.getEmployee() != null && detail.getEmployee2() == null && ( detail.getState().equals(PayrollDetailStateType.MICROFILMACION.getCode()) || detail.getState().equals(PayrollDetailStateType.ANALISIS1.getCode()) || detail.getState().equals(PayrollDetailStateType.ANALISIS2.getCode())) ) {
					detail.setBlAnalyst2(true);
				}
			}
		}
	}
	
	public void viewDetail(SelectEvent event){
		try {
			PayrollHeader payrollHeader = (PayrollHeader) event.getObject();
			if(ControlType.COORDINACION.getCode().equals(searchControlCertificateTO.getControlType())
					|| ControlType.MICROFILMACION.getCode().equals(searchControlCertificateTO.getControlType())){
				searchControlCertificateTO.setLstPayrollDetailTO(controlCertificateFacade.getPayrollDetail(payrollHeader.getIdPayrollHeaderPk(), payrollHeader.getPayrollType(), null));
				if(Validations.validateListIsNullOrEmpty(searchControlCertificateTO.getLstPayrollDetailTO())){
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"La planilla esta generada pero los detalles no han sido aceptados.");
					return;
				}else{
					for(PayrollDetailTO temp:searchControlCertificateTO.getLstPayrollDetailTO()){
						temp.setStateDesc(mpDetailStates.get(temp.getState()));
						temp.setOperationTypeDesc(mpOperationTypes.get(temp.getOperationType()));
					}
					if(ControlType.COORDINACION.getCode().equals(searchControlCertificateTO.getControlType())){
						searchControlCertificateTO.setIdEmployeePk(null);
						searchControlCertificateTO.setLstAnalyst(controlCertificateFacade.searchAnalysts(1));
					}						
				}
			}else if(ControlType.ANALISIS.getCode().equals(searchControlCertificateTO.getControlType())){
				searchControlCertificateTO.setLstPayrollDetailTO(null);
				Employees employee = controlCertificateFacade.getAnalyst(userInfo.getUserAccountSession().getIdUserAccountPk(), 1);
				if(employee != null){
					searchControlCertificateTO.setLstPayrollDetailTO(controlCertificateFacade.getPayrollDetail(payrollHeader.getIdPayrollHeaderPk(), payrollHeader.getPayrollType(), employee.getIdEmployeePk()));
					if(Validations.validateListIsNotNullAndNotEmpty(searchControlCertificateTO.getLstPayrollDetailTO())){
						List<Employees> lstTemp = new ArrayList<>();
						lstTemp.add(employee);
						searchControlCertificateTO.setIdEmployeePk(employee.getIdEmployeePk());						
						searchControlCertificateTO.setLstAnalyst(lstTemp);
						for(PayrollDetailTO temp:searchControlCertificateTO.getLstPayrollDetailTO()){
							temp.setStateDesc(mpDetailStates.get(temp.getState()));
							temp.setOperationTypeDesc(mpOperationTypes.get(temp.getOperationType()));
						}
					}else{						
						JSFUtilities.showSimpleValidationDialog();
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								"El usuario no tiene títulos asignados para análisis.");	
						return;
					}
				}else{
					JSFUtilities.showSimpleValidationDialog();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"El usuario no es un analista.");
					return;
				}			
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	public void multipleDocumentFileHandler(FileUploadEvent event) {
		try {
	        UploadedFile uploadedFile = event.getFile();
			 String fDisplayName = fUploadValidateFile(uploadedFile,null,null, super.getfUploadFileTypes());
			 String fileNameWithOutExt = FilenameUtils.removeExtension(fDisplayName);

			 if(searchControlCertificateTO.getLstPayrollDetailTO()!=null) {
				 for(PayrollDetailTO payrollDetailTO:searchControlCertificateTO.getLstPayrollDetailTO()) {
					 
					 if(searchControlCertificateTO.getPayRollType().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode())) {
						 
						 if(fileNameWithOutExt.equals(payrollDetailTO.getSerialCode()) && payrollDetailTO.getIndSerializable().equals(0)) {					 //payrollDetailTO.getIdSecurityCodePk()
							 String fileExtension = fDisplayName.substring(fDisplayName.lastIndexOf(GeneralConstants.DOT), fDisplayName.length()).toLowerCase();
							 String cuponNumber = "0"+((payrollDetailTO.getCuponNumber()==null)?"0":payrollDetailTO.getCuponNumber());
							 //payrollDetailTO.setFileName(searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString().concat(payrollDetailTO.getIdCustodyOperationPk().toString().concat(cuponNumber).concat(fileExtension)));
							 payrollDetailTO.setFileName(fileNameWithOutExt.concat(fileExtension));
							 payrollDetailTO.setDocumentFile(event.getFile().getContents());
							 payrollDetailTO.setBlSelected(true);
							 break;
						 }else if(fileNameWithOutExt.equals(payrollDetailTO.getIdCustodyOperationPk().toString()) && payrollDetailTO.getIndSerializable().equals(1)) {
							 String fileExtension = fDisplayName.substring(fDisplayName.lastIndexOf(GeneralConstants.DOT), fDisplayName.length()).toLowerCase();
							 String cuponNumber = "0"+((payrollDetailTO.getCuponNumber()==null)?"0":payrollDetailTO.getCuponNumber());
							 //payrollDetailTO.setFileName(searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString().concat(payrollDetailTO.getIdCustodyOperationPk().toString().concat(cuponNumber).concat(fileExtension)));
							 payrollDetailTO.setFileName(fileNameWithOutExt.concat(fileExtension));
							 payrollDetailTO.setDocumentFile(event.getFile().getContents());
							 payrollDetailTO.setBlSelected(true);
							 break;
						 }
					 }else {
						 if(fileNameWithOutExt.equals(payrollDetailTO.getSerialCode()) && payrollDetailTO.getIndSerializable().equals(0)) {				 
							 String fileExtension = fDisplayName.substring(fDisplayName.lastIndexOf(GeneralConstants.DOT), fDisplayName.length()).toLowerCase();
							 String cuponNumber = "0"+((payrollDetailTO.getCuponNumber()==null)?"0":payrollDetailTO.getCuponNumber());
							 //payrollDetailTO.setFileName(searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString().concat(payrollDetailTO.getIdCustodyOperationPk().toString().concat(cuponNumber).concat(fileExtension)));
							 payrollDetailTO.setFileName(fileExtension.concat(fileExtension));
							 payrollDetailTO.setDocumentFile(event.getFile().getContents());
							 payrollDetailTO.setBlSelected(true);
							 break;
						 }else if(fileNameWithOutExt.equals(payrollDetailTO.getIdCustodyOperationPk().toString()) && payrollDetailTO.getIndSerializable().equals(1)) {		 
							 String fileExtension = fDisplayName.substring(fDisplayName.lastIndexOf(GeneralConstants.DOT), fDisplayName.length()).toLowerCase();
							 String cuponNumber = "0"+((payrollDetailTO.getCuponNumber()==null)?"0":payrollDetailTO.getCuponNumber());
							 //payrollDetailTO.setFileName(searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString().concat(payrollDetailTO.getIdCustodyOperationPk().toString().concat(cuponNumber).concat(fileExtension)));
							 payrollDetailTO.setFileName(fileExtension.concat(fileExtension));
							 payrollDetailTO.setDocumentFile(event.getFile().getContents());
							 payrollDetailTO.setBlSelected(true);
							 break;
							 
						 }
					 }
					 
					 
				 } 
			 }
			 
			 
	        /*
			 String fDisplayName = fUploadValidateFile(event.getFile(),null,null, super.getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				 PayrollDetailTO payrollDetailTO = (PayrollDetailTO) event.getComponent().getAttributes().get("detail");
				 String fileExtension = fDisplayName.substring(fDisplayName.lastIndexOf(GeneralConstants.DOT), fDisplayName.length()).toLowerCase();
				 String cuponNumber = "0"+((payrollDetailTO.getCuponNumber()==null)?"0":payrollDetailTO.getCuponNumber());
				 payrollDetailTO.setFileName(searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString().concat(payrollDetailTO.getIdCustodyOperationPk().toString().concat(cuponNumber).concat(fileExtension)));
				 payrollDetailTO.setDocumentFile(event.getFile().getContents());
				 payrollDetailTO.setBlSelected(true);
			 }*/
		} catch (Exception e) {
		    //excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	
	
	public void documentFileHandler(FileUploadEvent event) {
		try {
			 String fDisplayName = fUploadValidateFile(event.getFile(),null,null, super.getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				 PayrollDetailTO payrollDetailTO = (PayrollDetailTO) event.getComponent().getAttributes().get("detail");
				 String fileExtension = fDisplayName.substring(fDisplayName.lastIndexOf(GeneralConstants.DOT), fDisplayName.length()).toLowerCase();
				 String cuponNumber = "0"+((payrollDetailTO.getCuponNumber()==null)?"0":payrollDetailTO.getCuponNumber());
				 payrollDetailTO.setFileName(searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString().concat(payrollDetailTO.getIdCustodyOperationPk().toString().concat(cuponNumber).concat(fileExtension)));
				 payrollDetailTO.setDocumentFile(event.getFile().getContents());
				 payrollDetailTO.setBlSelected(true);
			 }
		} catch (Exception e) {
		    //excepcion.fire(new ExceptionToCatch(e));
		}
	}
		
	public StreamedContent getStreamedContentFile(PayrollDetailTO payrollDetailTO){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(payrollDetailTO.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	payrollDetailTO.getFileName());
			inputStream.close();
		} catch (Exception e) {
		    //excepcion.fire(new ExceptionToCatch(e));
		}
		return streamedContentFile;
	}
	
	public StreamedContent getStreamedContentFileWithType(PayrollDetailTO payrollDetailTO){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(payrollDetailTO.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, "application/pdf",	payrollDetailTO.getFileName());
			inputStream.close();
		} catch (Exception e) {
		    //excepcion.fire(new ExceptionToCatch(e));
		}
		return streamedContentFile;
	}
	
	public void removeFile(PayrollDetailTO payrollDetailTO){
		payrollDetailTO.setFileName(null);
		payrollDetailTO.setDocumentFile(null);
		payrollDetailTO.setBlSelected(false);
	}

	private PayrollDetailTO revertPayrollDetailTO;
	public void beforeRevert(PayrollDetailTO payrollDetailTO){

		if(payrollDetailTO!=null){
			revertPayrollDetailTO = payrollDetailTO;
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					"¿Está seguro que desea revertir los siguientes registros de la planilla?");			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogRevert').show();");
		}else{
			JSFUtilities.showSimpleValidationDialog();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Debe seleccion por lo menos un registro.");
		}
	}
	
	@LoggerAuditWeb
	public void processControlRevert(){
		try {
			controlCertificateFacade.processControlRevert(revertPayrollDetailTO);
			if(ControlType.ANALISIS.getCode().equals(searchControlCertificateTO.getControlType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						"Se realizo el análisis de los registros de la planilla N° " + searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString() + ".");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	public void beforeSave(){
		boolean blSelected = false;
		if(ControlType.MICROFILMACION.getCode().equals(searchControlCertificateTO.getControlType())){
			for(PayrollDetailTO temp:searchControlCertificateTO.getLstPayrollDetailTO()){
				if(temp.isBlSelected()){
					blSelected = true;
					break;
				}
			}
			if(blSelected){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
						"¿Está seguro que desea actualizar los siguientes registros de la planilla?");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			}else{
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe microfilmar por lo menos un registro.");
			}
		}else if(ControlType.COORDINACION.getCode().equals(searchControlCertificateTO.getControlType())){
			for(PayrollDetailTO temp:searchControlCertificateTO.getLstPayrollDetailTO()){
				if(/*temp.isBlAnalyst1() && temp.isBlAnalyst2()
						&&*/ temp.getEmployee() != null && temp.getEmployee2() != null){
					blSelected = true;
					break;
				}
			}
			if(blSelected){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
						"¿Está seguro que desea actualizar los siguientes registros de la planilla?");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			}else{
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe asignar un analista por lo menos a un registro.");
			}
		}else if(ControlType.ANALISIS.getCode().equals(searchControlCertificateTO.getControlType())){
			for(PayrollDetailTO temp:searchControlCertificateTO.getLstPayrollDetailTO()){
				if(temp.isBlSelected()){
					blSelected = true;
					break;
				}
			}
			if(blSelected){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
						"¿Está seguro que desea actualizar los siguientes registros de la planilla?");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			}else{
				JSFUtilities.showSimpleValidationDialog();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe analizar por lo menos un registro.");
			}
		}
	}
	
	public Boolean disabledCheckControlCertificate() {
		return true;
	}
	
	public Boolean renderedCheckControlCertificate() {
		return true;
	}

	@LoggerAuditWeb
	public void processControl(){
		try {
			searchControlCertificateTO.getPayrollHeader();
			controlCertificateFacade.processControl(searchControlCertificateTO);
			if(ControlType.MICROFILMACION.getCode().equals(searchControlCertificateTO.getControlType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						"Se realizo la microfilmacion de los registros de la planilla N° " + searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString() + ".");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}else if(ControlType.COORDINACION.getCode().equals(searchControlCertificateTO.getControlType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						"Se realizo la asignación de los registros de la planilla N° " + searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString() + ".");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}else if(ControlType.ANALISIS.getCode().equals(searchControlCertificateTO.getControlType())){
				
				try {
					Long idPayrollHeaderPk = searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk();
					List<PayrollDetail> lstPayrollDetail = controlCertificateFacade.searchPayrollDetails(idPayrollHeaderPk, PayrollDetailStateType.ANALYZED.getCode());
					
					if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(searchControlCertificateTO.getPayRollType()) && lstPayrollDetail!=null && lstPayrollDetail.size()>0) {
						if(!(searchControlCertificateTO.getPayrollHeader().getIndDelivery()!=null && searchControlCertificateTO.getPayrollHeader().getIndDelivery().equals(1)) ) {

							Object[] parameters = new Object[1];
							parameters[0] = idPayrollHeaderPk;
							/*
							BusinessProcess businessProcess = new BusinessProcess();
							businessProcess.setIdBusinessProcessPk(BusinessProcessType.UTILITIES.getCode());//PHYSICAL_CERTIFICATE_REJECT_PAYROLL_DETAIL
		
							notificationServiceFacade.sendNotificationEmailParticipantNoConfig(userInfo.getUserAccountSession().getUserName(), 
									   businessProcess, 
									   "SOLICITUD DE RETIRO",
									   "Se encuentra procesada su solicitud nro. %s para retiro",
									   searchControlCertificateTO.getPayrollHeader().getParticipant().getIdParticipantPk(), 
									   parameters);
							*/
							
							BusinessProcess businessProcess = new BusinessProcess();
							businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_PAYROLL_APPROVE_ANALIZED.getCode());

							notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,searchControlCertificateTO.getPayrollHeader().getParticipant().getIdParticipantPk(), parameters);
							
						}
					}
					
				}catch (Exception e) {
					System.out.println("*** no se pudo enviar la notificacion:: "+e.getMessage());
				}
				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						"Se realizo el análisis de los registros de la planilla N° " + searchControlCertificateTO.getPayrollHeader().getIdPayrollHeaderPk().toString() + ".");			
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	public void resetSearch(){
		cleanResult();
		searchControlCertificateTO.setIdParticipantPk(null);
		searchControlCertificateTO.setIssuer(new Issuer());
	}
	
	public void cleanResult(){
		searchControlCertificateTO.setPayrollHeader(null);
		searchControlCertificateTO.setLstPayrollHeader(null);
		searchControlCertificateTO.setLstPayrollDetailTO(null);
	}
	
	private void fillParameters() throws ServiceException{
		mpStates = new HashMap<>();
		mpDetailStates = new HashMap<>();
		mpPayrollTypes = new HashMap<>();
		mpOperationTypes = new HashMap<>();
		lstStates = new ArrayList<>();
		lstPayrollTypes = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_HEADER_STATE.getCode());
		List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstStates.add(paramTab);
			}	
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstPayrollTypes.add(paramTab);
			}
			mpPayrollTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_DETAIL_STATE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			mpDetailStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.CERTIFICATE_OPERATIONS.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp)
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		paramTabTO.setMasterTableFk(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp)
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	}

	public Map<Integer, String> getMpStates() {
		return mpStates;
	}

	public void setMpStates(Map<Integer, String> mpStates) {
		this.mpStates = mpStates;
	}

	public Map<Integer, String> getMpPayrollTypes() {
		return mpPayrollTypes;
	}

	public void setMpPayrollTypes(Map<Integer, String> mpPayrollTypes) {
		this.mpPayrollTypes = mpPayrollTypes;
	}

	public Map<Integer, String> getMpDetailStates() {
		return mpDetailStates;
	}

	public void setMpDetailStates(Map<Integer, String> mpDetailStates) {
		this.mpDetailStates = mpDetailStates;
	}

	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	public List<ParameterTable> getLstPayrollTypes() {
		return lstPayrollTypes;
	}

	public void setLstPayrollTypes(List<ParameterTable> lstPayrollTypes) {
		this.lstPayrollTypes = lstPayrollTypes;
	}

	public SearchControlCertificateTO getSearchControlCertificateTO() {
		return searchControlCertificateTO;
	}

	public void setSearchControlCertificateTO(
			SearchControlCertificateTO searchControlCertificateTO) {
		this.searchControlCertificateTO = searchControlCertificateTO;
	}

	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	public Map<Integer, String> getMpOperationTypes() {
		return mpOperationTypes;
	}

	public void setMpOperationTypes(Map<Integer, String> mpOperationTypes) {
		this.mpOperationTypes = mpOperationTypes;
	}
	
}
