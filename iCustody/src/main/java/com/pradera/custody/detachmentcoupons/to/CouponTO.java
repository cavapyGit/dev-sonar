package com.pradera.custody.detachmentcoupons.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CouponTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class CouponTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id program interest pk. */
	private Long idProgramInterestPk;
	
	/** The coupon number. */
	private Integer couponNumber;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The expiration date. */
	private Date expirationDate;
	
	/** The payment date. */
	private Date paymentDate;
	
	/** The detachment date. */
	private Date detachmentDate;
	
	/** The detachment quantity. */
	private BigDecimal detachmentQuantity;
	
	/** The bl detachment. */
	private boolean blDetachment;
	
	private boolean blNotAllwDay;
	
	/** The state. */
	private String state;
	
	/** The security generated. */
	private String securityGenerated;
	
	/** The state value. */
	private Integer stateValue;
	
	/** The periodicity days. */
	private Integer periodicityDays;
	
	/**
	 * Gets the id program interest pk.
	 *
	 * @return the id program interest pk
	 */
	public Long getIdProgramInterestPk() {
		return idProgramInterestPk;
	}
	
	/**
	 * Sets the id program interest pk.
	 *
	 * @param idProgramInterestPk the new id program interest pk
	 */
	public void setIdProgramInterestPk(Long idProgramInterestPk) {
		this.idProgramInterestPk = idProgramInterestPk;
	}
	
	/**
	 * Gets the coupon number.
	 *
	 * @return the coupon number
	 */
	public Integer getCouponNumber() {
		return couponNumber;
	}
	
	/**
	 * Sets the coupon number.
	 *
	 * @param couponNumber the new coupon number
	 */
	public void setCouponNumber(Integer couponNumber) {
		this.couponNumber = couponNumber;
	}
	
	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}
	
	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	/**
	 * Gets the payment date.
	 *
	 * @return the payment date
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}
	
	/**
	 * Sets the payment date.
	 *
	 * @param paymentDate the new payment date
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	/**
	 * Gets the detachment quantity.
	 *
	 * @return the detachment quantity
	 */
	public BigDecimal getDetachmentQuantity() {
		return detachmentQuantity;
	}
	
	/**
	 * Sets the detachment quantity.
	 *
	 * @param detachmentQuantity the new detachment quantity
	 */
	public void setDetachmentQuantity(BigDecimal detachmentQuantity) {
		this.detachmentQuantity = detachmentQuantity;
	}
	
	/**
	 * Checks if is bl detachment.
	 *
	 * @return true, if is bl detachment
	 */
	public boolean isBlDetachment() {
		return blDetachment;
	}
	
	/**
	 * Sets the bl detachment.
	 *
	 * @param blDetachment the new bl detachment
	 */
	public void setBlDetachment(boolean blDetachment) {
		this.blDetachment = blDetachment;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the detachment date.
	 *
	 * @return the detachment date
	 */
	public Date getDetachmentDate() {
		return detachmentDate;
	}
	
	/**
	 * Sets the detachment date.
	 *
	 * @param detachmentDate the new detachment date
	 */
	public void setDetachmentDate(Date detachmentDate) {
		this.detachmentDate = detachmentDate;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the security generated.
	 *
	 * @return the security generated
	 */
	public String getSecurityGenerated() {
		return securityGenerated;
	}

	/**
	 * Sets the security generated.
	 *
	 * @param securityGenerated the new security generated
	 */
	public void setSecurityGenerated(String securityGenerated) {
		this.securityGenerated = securityGenerated;
	}

	/**
	 * Gets the state value.
	 *
	 * @return the state value
	 */
	public Integer getStateValue() {
		return stateValue;
	}

	/**
	 * Sets the state value.
	 *
	 * @param stateValue the new state value
	 */
	public void setStateValue(Integer stateValue) {
		this.stateValue = stateValue;
	}

	public Integer getPeriodicityDays() {
		return periodicityDays;
	}

	public void setPeriodicityDays(Integer periodicityDays) {
		this.periodicityDays = periodicityDays;
	}

	public boolean isBlNotAllwDay() {
		return blNotAllwDay;
	}

	public void setBlNotAllwDay(boolean blNotAllwDay) {
		this.blNotAllwDay = blNotAllwDay;
	}
}
