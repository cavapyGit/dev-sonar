package com.pradera.custody.detachmentcoupons.to;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.custody.affectation.to.RequestAffectationTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CouponsTODataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class CouponsTODataModel extends ListDataModel<CouponTO> implements SelectableDataModel<CouponTO>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new coupons to data model.
	 *
	 * @param lstCoupons the lst coupons
	 */
	public CouponsTODataModel(List<CouponTO> lstCoupons) {
		super(lstCoupons);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public CouponTO getRowData(String rowKey) {
		List<CouponTO> lstCoupons = (List<CouponTO>)getWrappedData();
		for(CouponTO couponTO : lstCoupons){
			if(couponTO.getIdProgramInterestPk().toString().equals(rowKey))
				return couponTO;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(CouponTO couponTO) {
		return couponTO.getIdProgramInterestPk();
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public Integer getSize(){
		List<RequestAffectationTO> lstRequestAffectations = (List<RequestAffectationTO>)getWrappedData();
		return lstRequestAffectations.size();
	}
	
}
