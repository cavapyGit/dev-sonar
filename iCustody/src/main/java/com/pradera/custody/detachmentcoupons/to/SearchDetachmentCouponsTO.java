package com.pradera.custody.detachmentcoupons.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SearchDetachmentCouponsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class SearchDetachmentCouponsTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The lst states. */
	private List<ParameterTable> lstStates;	
	
	/** The state selected. */
	private Integer stateSelected;
	
	/** The holder selected. */
	private Holder holderSelected;
	
	/** The holder account selected. */
	private HolderAccount holderAccountSelected = new HolderAccount();
	
	/** The security selected. */
	private Security securitySelected;
	
	/** The end date. */
	private Date initialDate, endDate;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The lst detachment coupon request. */
	private GenericDataModel<SplitCouponRequest> lstDetachmentCouponRequest;
	
	/** The number request. */
	private Long numberRequest;
	
	/** The search type. */
	private Integer searchType;
	
	/** The bl default search. */
	private boolean blDefaultSearch;
	
	/** The lst holder account. */
	private List<HolderAccount> lstHolderAccount;
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}
	
	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}
	
	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}
	
	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}
	
	/**
	 * Gets the holder selected.
	 *
	 * @return the holder selected
	 */
	public Holder getHolderSelected() {
		return holderSelected;
	}
	
	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the new holder selected
	 */
	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}
	
	/**
	 * Gets the holder account selected.
	 *
	 * @return the holder account selected
	 */
	public HolderAccount getHolderAccountSelected() {
		return holderAccountSelected;
	}
	
	/**
	 * Sets the holder account selected.
	 *
	 * @param holderAccountSelected the new holder account selected
	 */
	public void setHolderAccountSelected(HolderAccount holderAccountSelected) {
		this.holderAccountSelected = holderAccountSelected;
	}
	
	/**
	 * Gets the security selected.
	 *
	 * @return the security selected
	 */
	public Security getSecuritySelected() {
		return securitySelected;
	}
	
	/**
	 * Sets the security selected.
	 *
	 * @param securitySelected the new security selected
	 */
	public void setSecuritySelected(Security securitySelected) {
		this.securitySelected = securitySelected;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the lst detachment coupon request.
	 *
	 * @return the lst detachment coupon request
	 */
	public GenericDataModel<SplitCouponRequest> getLstDetachmentCouponRequest() {
		return lstDetachmentCouponRequest;
	}
	
	/**
	 * Sets the lst detachment coupon request.
	 *
	 * @param lstDetachmentCouponRequest the new lst detachment coupon request
	 */
	public void setLstDetachmentCouponRequest(
			GenericDataModel<SplitCouponRequest> lstDetachmentCouponRequest) {
		this.lstDetachmentCouponRequest = lstDetachmentCouponRequest;
	}
	
	/**
	 * Gets the number request.
	 *
	 * @return the number request
	 */
	public Long getNumberRequest() {
		return numberRequest;
	}
	
	/**
	 * Sets the number request.
	 *
	 * @param numberRequest the new number request
	 */
	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}
	
	/**
	 * Gets the search type.
	 *
	 * @return the search type
	 */
	public Integer getSearchType() {
		return searchType;
	}
	
	/**
	 * Sets the search type.
	 *
	 * @param searchType the new search type
	 */
	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}
	
	/**
	 * Checks if is bl default search.
	 *
	 * @return true, if is bl default search
	 */
	public boolean isBlDefaultSearch() {
		return blDefaultSearch;
	}
	
	/**
	 * Sets the bl default search.
	 *
	 * @param blDefaultSearch the new bl default search
	 */
	public void setBlDefaultSearch(boolean blDefaultSearch) {
		this.blDefaultSearch = blDefaultSearch;
	}

	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}

	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}
	
	
}
