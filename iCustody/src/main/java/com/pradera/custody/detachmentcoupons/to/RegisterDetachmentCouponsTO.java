package com.pradera.custody.detachmentcoupons.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RegisterDetachmentCouponsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class RegisterDetachmentCouponsTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id detachment coupon request. */
	private Long idDetachmentCouponRequest;
	
	/** The opearation number. */
	private Long opearationNumber;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The holder selected. */
	private Holder holderSelected;
	
	/** The holder account selected. */
	private HolderAccount holderAccountSelected;
	
	/** The security selected. */
	private Security securitySelected;
	
	/** The security sub product. */
	private Security securitySubProduct;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The general quantity. */
	private BigDecimal generalQuantity;
	
	/** The lst coupons. */
	private CouponsTODataModel lstCoupons;
	
	/** The lst holder account balance. */
	private GenericDataModel<HolderAccountBalance> lstHolderAccountBalance;
	
	/** The coupons selected. */
	private List<CouponTO> couponsSelected;
	
	/** The available balance. */
	private BigDecimal availableBalance;
	
	/** The file name. */
	private String fileName;
	
	/** The document file. */
	private byte[] documentFile;
	
	/** The state. */
	private Integer state;
	
	/** The lst states. */
	private List<ParameterTable> lstMotives, lstStates;
	
	/** The motive selected. */
	private Integer motiveSelected;
	
	/** The comments. */
	private String comments;
	
	/** The bl motives. */
	private boolean blOthers, blMotives;
	
	/** The lst holder account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** The bl quantity off. */
	private boolean blQuantityOff;
	
	/** The lst mechanism operation. */
	private GenericDataModel<MechanismOperation> lstMechanismOperation;
	
	/** The lst split coupon market temp. */
	private List<SplitCouponMarketFact> lstSplitCouponMarketTemp;
	
	/** The ind detachment not blocked. */
	private boolean indDetachmentNotBlocked;
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}
	
	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}
	
	/**
	 * Gets the holder selected.
	 *
	 * @return the holder selected
	 */
	public Holder getHolderSelected() {
		return holderSelected;
	}
	
	/**
	 * Sets the holder selected.
	 *
	 * @param holderSelected the new holder selected
	 */
	public void setHolderSelected(Holder holderSelected) {
		this.holderSelected = holderSelected;
	}
	
	/**
	 * Gets the holder account selected.
	 *
	 * @return the holder account selected
	 */
	public HolderAccount getHolderAccountSelected() {
		return holderAccountSelected;
	}
	
	/**
	 * Sets the holder account selected.
	 *
	 * @param holderAccountSelected the new holder account selected
	 */
	public void setHolderAccountSelected(HolderAccount holderAccountSelected) {
		this.holderAccountSelected = holderAccountSelected;
	}
	
	/**
	 * Gets the security selected.
	 *
	 * @return the security selected
	 */
	public Security getSecuritySelected() {
		return securitySelected;
	}
	
	/**
	 * Sets the security selected.
	 *
	 * @param securitySelected the new security selected
	 */
	public void setSecuritySelected(Security securitySelected) {
		this.securitySelected = securitySelected;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}	
	
	/**
	 * Gets the lst holder account balance.
	 *
	 * @return the lst holder account balance
	 */
	public GenericDataModel<HolderAccountBalance> getLstHolderAccountBalance() {
		return lstHolderAccountBalance;
	}
	
	/**
	 * Sets the lst holder account balance.
	 *
	 * @param lstHolderAccountBalance the new lst holder account balance
	 */
	public void setLstHolderAccountBalance(
			GenericDataModel<HolderAccountBalance> lstHolderAccountBalance) {
		this.lstHolderAccountBalance = lstHolderAccountBalance;
	}
	
	/**
	 * Gets the general quantity.
	 *
	 * @return the general quantity
	 */
	public BigDecimal getGeneralQuantity() {
		return generalQuantity;
	}
	
	/**
	 * Sets the general quantity.
	 *
	 * @param generalQuantity the new general quantity
	 */
	public void setGeneralQuantity(BigDecimal generalQuantity) {
		this.generalQuantity = generalQuantity;
	}
	
	/**
	 * Gets the lst coupons.
	 *
	 * @return the lst coupons
	 */
	public CouponsTODataModel getLstCoupons() {
		return lstCoupons;
	}
	
	/**
	 * Sets the lst coupons.
	 *
	 * @param lstCoupons the new lst coupons
	 */
	public void setLstCoupons(CouponsTODataModel lstCoupons) {
		this.lstCoupons = lstCoupons;
	}
	
	/**
	 * Gets the id detachment coupon request.
	 *
	 * @return the id detachment coupon request
	 */
	public Long getIdDetachmentCouponRequest() {
		return idDetachmentCouponRequest;
	}
	
	/**
	 * Sets the id detachment coupon request.
	 *
	 * @param idDetachmentCouponRequest the new id detachment coupon request
	 */
	public void setIdDetachmentCouponRequest(Long idDetachmentCouponRequest) {
		this.idDetachmentCouponRequest = idDetachmentCouponRequest;
	}
	
	/**
	 * Gets the coupons selected.
	 *
	 * @return the coupons selected
	 */
	public List<CouponTO> getCouponsSelected() {
		return couponsSelected;
	}
	
	/**
	 * Sets the coupons selected.
	 *
	 * @param couponsSelected the new coupons selected
	 */
	public void setCouponsSelected(List<CouponTO> couponsSelected) {
		this.couponsSelected = couponsSelected;
	}
	
	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	
	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the document file.
	 *
	 * @return the document file
	 */
	public byte[] getDocumentFile() {
		return documentFile;
	}

	/**
	 * Sets the document file.
	 *
	 * @param documentFile the new document file
	 */
	public void setDocumentFile(byte[] documentFile) {
		this.documentFile = documentFile;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Checks if is bl motives.
	 *
	 * @return true, if is bl motives
	 */
	public boolean isBlMotives() {
		return blMotives;
	}

	/**
	 * Sets the bl motives.
	 *
	 * @param blMotives the new bl motives
	 */
	public void setBlMotives(boolean blMotives) {
		this.blMotives = blMotives;
	}

	/**
	 * Gets the lst motives.
	 *
	 * @return the lst motives
	 */
	public List<ParameterTable> getLstMotives() {
		return lstMotives;
	}

	/**
	 * Sets the lst motives.
	 *
	 * @param lstMotives the new lst motives
	 */
	public void setLstMotives(List<ParameterTable> lstMotives) {
		this.lstMotives = lstMotives;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Checks if is bl others.
	 *
	 * @return true, if is bl others
	 */
	public boolean isBlOthers() {
		return blOthers;
	}

	/**
	 * Sets the bl others.
	 *
	 * @param blOthers the new bl others
	 */
	public void setBlOthers(boolean blOthers) {
		this.blOthers = blOthers;
	}

	/**
	 * Gets the motive selected.
	 *
	 * @return the motive selected
	 */
	public Integer getMotiveSelected() {
		return motiveSelected;
	}

	/**
	 * Sets the motive selected.
	 *
	 * @param motiveSelected the new motive selected
	 */
	public void setMotiveSelected(Integer motiveSelected) {
		this.motiveSelected = motiveSelected;
	}

	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}

	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}

	/**
	 * Checks if is bl quantity off.
	 *
	 * @return true, if is bl quantity off
	 */
	public boolean isBlQuantityOff() {
		return blQuantityOff;
	}

	/**
	 * Sets the bl quantity off.
	 *
	 * @param blQuantityOff the new bl quantity off
	 */
	public void setBlQuantityOff(boolean blQuantityOff) {
		this.blQuantityOff = blQuantityOff;
	}

	/**
	 * Gets the lst mechanism operation.
	 *
	 * @return the lst mechanism operation
	 */
	public GenericDataModel<MechanismOperation> getLstMechanismOperation() {
		return lstMechanismOperation;
	}

	/**
	 * Sets the lst mechanism operation.
	 *
	 * @param lstMechanismOperation the new lst mechanism operation
	 */
	public void setLstMechanismOperation(
			GenericDataModel<MechanismOperation> lstMechanismOperation) {
		this.lstMechanismOperation = lstMechanismOperation;
	}

	/**
	 * Gets the lst split coupon market temp.
	 *
	 * @return the lst split coupon market temp
	 */
	public List<SplitCouponMarketFact> getLstSplitCouponMarketTemp() {
		return lstSplitCouponMarketTemp;
	}

	/**
	 * Sets the lst split coupon market temp.
	 *
	 * @param lstSplitCouponMarketTemp the new lst split coupon market temp
	 */
	public void setLstSplitCouponMarketTemp(
			List<SplitCouponMarketFact> lstSplitCouponMarketTemp) {
		this.lstSplitCouponMarketTemp = lstSplitCouponMarketTemp;
	}

	/**
	 * Checks if is ind detachment not blocked.
	 *
	 * @return true, if is ind detachment not blocked
	 */
	public boolean isIndDetachmentNotBlocked() {
		return indDetachmentNotBlocked;
	}

	/**
	 * Sets the ind detachment not blocked.
	 *
	 * @param indDetachmentNotBlocked the new ind detachment not blocked
	 */
	public void setIndDetachmentNotBlocked(boolean indDetachmentNotBlocked) {
		this.indDetachmentNotBlocked = indDetachmentNotBlocked;
	}

	/**
	 * Gets the security sub product.
	 *
	 * @return the security sub product
	 */
	public Security getSecuritySubProduct() {
		return securitySubProduct;
	}

	/**
	 * Sets the security sub product.
	 *
	 * @param securitySubProduct the new security sub product
	 */
	public void setSecuritySubProduct(Security securitySubProduct) {
		this.securitySubProduct = securitySubProduct;
	}

	/**
	 * Gets the opearation number.
	 *
	 * @return the opearation number
	 */
	public Long getOpearationNumber() {
		return opearationNumber;
	}

	/**
	 * Sets the opearation number.
	 *
	 * @param opearationNumber the new opearation number
	 */
	public void setOpearationNumber(Long opearationNumber) {
		this.opearationNumber = opearationNumber;
	}
	
	
	
}
