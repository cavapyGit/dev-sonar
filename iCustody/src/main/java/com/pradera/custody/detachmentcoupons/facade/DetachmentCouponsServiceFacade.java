package com.pradera.custody.detachmentcoupons.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.custody.detachmentcoupons.service.DetachmentCouponsServiceBean;
import com.pradera.custody.detachmentcoupons.to.CouponTO;
import com.pradera.custody.detachmentcoupons.to.CouponsTODataModel;
import com.pradera.custody.detachmentcoupons.to.RegisterDetachmentCouponsTO;
import com.pradera.custody.detachmentcoupons.to.SearchDetachmentCouponsTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.service.SplitCouponComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SplitCouponOperationTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcouponstype.SplitConfigurationType;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DetachmentCouponsServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class DetachmentCouponsServiceFacade {

	/** The detachment coupons service bean. */
	@EJB
	DetachmentCouponsServiceBean detachmentCouponsServiceBean;
	
	/** The general parameters facade. */
	@Inject
	ParameterServiceBean parameterServiceBean;
	
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The securities service bean. */
	@Inject
	SecuritiesServiceBean securitiesServiceBean;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The securities component service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentService;

	/** The split coupon component service. */
	@Inject
	Instance<SplitCouponComponentService> splitCouponComponentService;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getListParticipants() throws ServiceException{
		return detachmentCouponsServiceBean.getListParticipants();
	}

	/**
	 * Gets the request.
	 *
	 * @param searchDetachmentCouponsTO the search detachment coupons to
	 * @param blParticipant the bl participant
	 * @return the request
	 * @throws ServiceException the service exception
	 */
	public List<SplitCouponRequest> getRequest(SearchDetachmentCouponsTO searchDetachmentCouponsTO, boolean blParticipant) throws ServiceException{
		List<SplitCouponRequest> list = detachmentCouponsServiceBean.getRequest(searchDetachmentCouponsTO, blParticipant);
		return list;
	}

	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipant(Long participantCode) throws ServiceException{
		return detachmentCouponsServiceBean.find(participantCode, Participant.class);
	}

	/**
	 * Gets the balances.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @return the balances
	 * @throws ServiceException the service exception
	 */
	public void getBalances(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		registerDetachmentCouponsTO.setLstHolderAccountBalance(new GenericDataModel<HolderAccountBalance>(detachmentCouponsServiceBean.getHolderAccountBalance(registerDetachmentCouponsTO)));		
	}
	
	/**
	 * Gets the coupons.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @param mapDescriptions the map descriptions
	 * @return the coupons
	 * @throws ServiceException the service exception
	 */
	public void getCoupons(RegisterDetachmentCouponsTO registerDetachmentCouponsTO, Map<Integer,String> mapDescriptions) throws ServiceException{
		List<ProgramInterestCoupon> lstCoupons = detachmentCouponsServiceBean.getProgramInterestCoupon(registerDetachmentCouponsTO);
		List<CouponTO> lstResult = new ArrayList<CouponTO>();
		
		Integer maxSplitDays =  null;
		if(mapDescriptions.get(SplitConfigurationType.MAX_DAY_SPLIT.getCode())!=null){
			maxSplitDays = Integer.parseInt(mapDescriptions.get(SplitConfigurationType.MAX_DAY_SPLIT.getCode()));
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstCoupons)){
			for(ProgramInterestCoupon coupons:lstCoupons){
				CouponTO couponTO = new CouponTO();
				couponTO.setIdProgramInterestPk(coupons.getIdProgramInterestPk());
				couponTO.setCouponNumber(coupons.getCouponNumber());
				couponTO.setExpirationDate(coupons.getExperitationDate());
				couponTO.setPaymentDate(coupons.getPaymentDate());
				couponTO.setRegistryDate(CommonsUtilities.currentDate());
				couponTO.setStateValue(coupons.getStateProgramInterest());
				couponTO.setPeriodicityDays(CommonsUtilities.getDaysBetween(CommonsUtilities.currentDate(), coupons.getExperitationDate()));
				couponTO.setState(mapDescriptions.get(couponTO.getStateValue()));//TODO
				if(maxSplitDays!=null && maxSplitDays<couponTO.getPeriodicityDays()){
					couponTO.setBlNotAllwDay(Boolean.TRUE);
				}else{
					couponTO.setBlNotAllwDay(Boolean.FALSE);
				}
				lstResult.add(couponTO);
			}

			
			Security securitySelected = registerDetachmentCouponsTO.getSecuritySelected();
			if(securitySelected.getSecurity()!=null){
				String securityCodePk = securitySelected.getIdSecurityCodePk();
				List<ProgramInterestCoupon> interestList = detachmentCouponsServiceBean.getProgramInterestCouponFromParent(securityCodePk);
				for(ProgramInterestCoupon interesCoupon: interestList){
					Integer couponNumber = interesCoupon.getCouponNumber(); 
					if(!existCouponInSubProduct(lstResult, couponNumber)){
						CouponTO couponTO = new CouponTO();
						couponTO.setIdProgramInterestPk(interesCoupon.getIdProgramInterestPk());
						couponTO.setCouponNumber(interesCoupon.getCouponNumber());
						couponTO.setExpirationDate(interesCoupon.getExperitationDate());
						couponTO.setPaymentDate(interesCoupon.getPaymentDate());
						couponTO.setRegistryDate(CommonsUtilities.currentDate());
						couponTO.setStateValue(interesCoupon.getStateProgramInterest());
						couponTO.setState(mapDescriptions.get(couponTO.getStateValue()));//TODO
						couponTO.setDetachmentDate(interesCoupon.getRegistryDate());
						couponTO.setBlDetachment(Boolean.TRUE);
						
						lstResult.add(couponTO);
					}
				}
				Collections.sort(lstResult,COMPARE_BY_COUPON_NUMBER);
			}
		}
		
		registerDetachmentCouponsTO.setLstCoupons(new CouponsTODataModel(lstResult));
	}
	
	/** The compare by coupon number. */
	public static Comparator<CouponTO> COMPARE_BY_COUPON_NUMBER = new Comparator<CouponTO>() {
		 public int compare(CouponTO o1, CouponTO o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
				int resultado = o1.getCouponNumber()-o2.getCouponNumber();
		        if ( resultado != 0 ) { return resultado; }
				return resultado;
			}
	    }; 
	
	/**
	 * Exist coupon in sub product.
	 *
	 * @param subProductInterests the sub product interests
	 * @param couponNumber the coupon number
	 * @return the boolean
	 */
	private Boolean existCouponInSubProduct(List<CouponTO> subProductInterests, Integer couponNumber){
		for(CouponTO couponTO: subProductInterests){
			if(couponTO.getCouponNumber().equals(couponNumber)){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		return detachmentCouponsServiceBean.findHolderAccount(idHolderAccountPk);
	}

	/**
	 * Find holder accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccounts(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		List<HolderAccount> lstResult = detachmentCouponsServiceBean.findHolderAccounts(idHolderPk, idParticipantPk);
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());			
			Map<Integer, String> mpAccountType = new HashMap<Integer, String>();
			for(ParameterTable paramTab:parameterServiceBean.getListParameterTableServiceBean(paramTabTO)){
				mpAccountType.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			}
			for(HolderAccount holderAccount:lstResult){
				holderAccount.setAccountDescription(mpAccountType.get(holderAccount.getAccountType()));
			}
			return lstResult;
		}else
			return null;
	}
	
	/**
	 * Save request.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @param marketFactBalanceHelpTO the market fact balance help to
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long saveRequest(RegisterDetachmentCouponsTO registerDetachmentCouponsTO, MarketFactBalanceHelpTO marketFactBalanceHelpTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER); 
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		ErrorServiceType entitiesStates = validateEntities(registerDetachmentCouponsTO);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		SplitCouponRequest splitCouponRequest = new SplitCouponRequest();
		splitCouponRequest.setRegistryDate(CommonsUtilities.currentDateTime());
		splitCouponRequest.setRegistryUser(loggerUser.getUserName());
		splitCouponRequest.setRequestState(SplitCouponStateType.REGISTERED.getCode());
		splitCouponRequest.setHolder(registerDetachmentCouponsTO.getHolderSelected());
		splitCouponRequest.setHolderAccount(registerDetachmentCouponsTO.getHolderAccountSelected());
		splitCouponRequest.setParticipant(new Participant(registerDetachmentCouponsTO.getParticipantSelected()));		
		splitCouponRequest.setSecurity(registerDetachmentCouponsTO.getSecuritySelected());
		splitCouponRequest.setFileName(registerDetachmentCouponsTO.getFileName());
		splitCouponRequest.setDocumentFile(registerDetachmentCouponsTO.getDocumentFile());
		
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setOperationNumber(detachmentCouponsServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_COUPON_TRANSFER_OPERATION));
		if(registerDetachmentCouponsTO.isIndDetachmentNotBlocked()
				|| Validations.validateListIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()))
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_DETACHMENT_NOT_BLOCKED.getCode());
		else
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_DETACHMENT.getCode());
		custodyOperation.setState(SplitCouponStateType.REGISTERED.getCode());
		splitCouponRequest.setCustodyOperation(custodyOperation);
		detachmentCouponsServiceBean.create(splitCouponRequest);
		BigDecimal quantity = null;
		for(CouponTO couponTO:registerDetachmentCouponsTO.getCouponsSelected()){						
			SplitCouponOperation splitCouponOperation = new SplitCouponOperation();
			splitCouponOperation.setProgramInterestCoupon(new ProgramInterestCoupon(couponTO.getIdProgramInterestPk()));
			splitCouponOperation.setOperationQuantity(couponTO.getDetachmentQuantity());
			splitCouponOperation.setSplitCouponRequest(splitCouponRequest);
			detachmentCouponsServiceBean.create(splitCouponOperation);
			quantity = couponTO.getDetachmentQuantity();
		}
		List<MarketFactAccountTO> lstMarketFactTO = null;
		if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
			lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
			if(Validations.validateIsNotNullAndNotEmpty(marketFactBalanceHelpTO)){
				for(MarketFactDetailHelpTO marketDetail:marketFactBalanceHelpTO.getMarketFacBalances()){
					SplitCouponMarketFact splitCouponMarketFact = new SplitCouponMarketFact();
					splitCouponMarketFact.setSplitCouponRequest(splitCouponRequest);
					splitCouponMarketFact.setMarketDate(marketDetail.getMarketDate());
					splitCouponMarketFact.setMarketRate(marketDetail.getMarketRate());
					splitCouponMarketFact.setMarketPrice(marketDetail.getMarketPrice());
					splitCouponMarketFact.setOperationQuantity(marketDetail.getEnteredBalance());
					detachmentCouponsServiceBean.create(splitCouponMarketFact);
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(marketDetail.getMarketDate());
					marketFactAccountTO.setMarketRate(marketDetail.getMarketRate());
					marketFactAccountTO.setMarketPrice(marketDetail.getMarketPrice());
					marketFactAccountTO.setQuantity(marketDetail.getEnteredBalance());
					lstMarketFactTO.add(marketFactAccountTO);
				}
			}else{
				List<HolderMarketFactBalance> lstMarketFact = null;
				if(Validations.validateIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getGeneralQuantity())){
					lstMarketFact = detachmentCouponsServiceBean.findMarketFactBalance(splitCouponRequest.getSecurity().getIdSecurityCodePk(), splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFact)){
						for(HolderMarketFactBalance holderMarketFactBalance:lstMarketFact){
							SplitCouponMarketFact splitCouponMarketFact = new SplitCouponMarketFact();
							splitCouponMarketFact.setSplitCouponRequest(splitCouponRequest);
							splitCouponMarketFact.setMarketDate(holderMarketFactBalance.getMarketDate());
							splitCouponMarketFact.setMarketRate(holderMarketFactBalance.getMarketRate());
							splitCouponMarketFact.setMarketPrice(holderMarketFactBalance.getMarketPrice());
							splitCouponMarketFact.setOperationQuantity(registerDetachmentCouponsTO.getGeneralQuantity());
							detachmentCouponsServiceBean.create(splitCouponMarketFact);
							MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
							marketFactAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate());
							marketFactAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate());
							marketFactAccountTO.setMarketPrice(holderMarketFactBalance.getMarketPrice());
							marketFactAccountTO.setQuantity(registerDetachmentCouponsTO.getGeneralQuantity());
							lstMarketFactTO.add(marketFactAccountTO);						
						}
					}
				}
			}
			if(Validations.validateListIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getLstSplitCouponMarketTemp())){
				for(SplitCouponMarketFact splitCouMarkFact:registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()){
					splitCouMarkFact.setIdSplitMarketFactPk(null);
					splitCouMarkFact.setSplitCouponRequest(splitCouponRequest);
					detachmentCouponsServiceBean.create(splitCouMarkFact);
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(splitCouMarkFact.getMarketDate());
					marketFactAccountTO.setMarketRate(splitCouMarkFact.getMarketRate());
					marketFactAccountTO.setMarketPrice(splitCouMarkFact.getMarketPrice());
					marketFactAccountTO.setQuantity(splitCouMarkFact.getOperationQuantity());
					lstMarketFactTO.add(marketFactAccountTO);
				}
			}
		}
		if(ParameterOperationType.SECURITIES_DETACHMENT.getCode().equals(custodyOperation.getOperationType())){
			/*List<HolderMarketFactBalance> lstMarketFact = null;
			List<MarketFactAccountTO> lstMarketFactTO = null;
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
				lstMarketFact = detachmentCouponsServiceBean.findMarketFactBalance(splitCouponRequest.getSecurity().getIdSecurityCodePk(), splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
			if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFact)){
				lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
				BigDecimal totalRequired = quantity;
				for(HolderMarketFactBalance holderMarketFactBalance:lstMarketFact){
					totalRequired = totalRequired.subtract(holderMarketFactBalance.getAvailableBalance());
					if(totalRequired.compareTo(BigDecimal.ZERO) <= 0){
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate());
						marketFactAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate());
						marketFactAccountTO.setQuantity(holderMarketFactBalance.getAvailableBalance().add(totalRequired));
						lstMarketFactTO.add(marketFactAccountTO);
						SplitCouponMarketFact splitCouponMarketFact = new SplitCouponMarketFact();
						splitCouponMarketFact.setSplitCouponRequest(splitCouponRequest);
						splitCouponMarketFact.setMarketDate(holderMarketFactBalance.getMarketDate());
						splitCouponMarketFact.setMarketRate(holderMarketFactBalance.getMarketRate());
						splitCouponMarketFact.setOperationQuantity(holderMarketFactBalance.getAvailableBalance().add(totalRequired));
						detachmentCouponsServiceBean.create(splitCouponMarketFact);
						break;				
					}else{
						MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
						marketFactAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate());
						marketFactAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate());
						marketFactAccountTO.setQuantity(holderMarketFactBalance.getAvailableBalance());
						lstMarketFactTO.add(marketFactAccountTO);
						SplitCouponMarketFact splitCouponMarketFact = new SplitCouponMarketFact();
						splitCouponMarketFact.setSplitCouponRequest(splitCouponRequest);
						splitCouponMarketFact.setMarketDate(holderMarketFactBalance.getMarketDate());
						splitCouponMarketFact.setMarketRate(holderMarketFactBalance.getMarketRate());
						splitCouponMarketFact.setOperationQuantity(holderMarketFactBalance.getAvailableBalance());
						detachmentCouponsServiceBean.create(splitCouponMarketFact);
					}
				}
			}*/
			/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS DISPONIBLES AL TRANSITO ***/
			List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(splitCouponRequest, quantity, lstMarketFactTO);
			accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(splitCouponRequest.getIdSplitOperationPk(),
																	BusinessProcessType.COUPON_DETACHMENT_OPERATION_REGISTER.getCode(),
																	accountBalanceTOs, null, custodyOperation.getOperationType()));
		}
//		return splitCouponRequest.getIdSplitOperationPk();
		return custodyOperation.getOperationNumber();
	}
	
	/**
	 * Find market fact balance.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idHolderAccountPk the id holder account pk
	 * @return the list
	 */
	public List<HolderMarketFactBalance> findMarketFactBalance(String idSecurityCodePk, Long idHolderAccountPk){
		return detachmentCouponsServiceBean.findMarketFactBalance(idSecurityCodePk, idHolderAccountPk);
	}
	
	/**
	 * Find security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the list
	 */
	public Security findSecurity(String idSecurityCodePk){
		return detachmentCouponsServiceBean.findSecurity(idSecurityCodePk);
	}
	
	/**
	 * Approve request.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @throws ServiceException the service exception
	 */
	public void approveRequest(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);  
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
	    ErrorServiceType entitiesStates = validateEntities(registerDetachmentCouponsTO);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		SplitCouponRequest splitCouponRequest = detachmentCouponsServiceBean.find(SplitCouponRequest.class, registerDetachmentCouponsTO.getIdDetachmentCouponRequest());
		if(!SplitCouponStateType.REGISTERED.getCode().equals(splitCouponRequest.getRequestState()))
			throw new ServiceException(ErrorServiceType.SPLIT_COUPON_REQUEST_MODIFIED_STATE, new Object[]{splitCouponRequest.getIdSplitOperationPk().toString()});
		splitCouponRequest.setRequestState(SplitCouponStateType.APPROVED.getCode());
		splitCouponRequest.getCustodyOperation().setState(SplitCouponStateType.APPROVED.getCode());
		splitCouponRequest.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
		splitCouponRequest.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
	}

	/**
	 * Annul request.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @throws ServiceException the service exception
	 */
	public void annulRequest(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);  
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		SplitCouponRequest splitCouponRequest = detachmentCouponsServiceBean.find(SplitCouponRequest.class, registerDetachmentCouponsTO.getIdDetachmentCouponRequest());
		if(!SplitCouponStateType.REGISTERED.getCode().equals(splitCouponRequest.getRequestState()))
			throw new ServiceException(ErrorServiceType.SPLIT_COUPON_REQUEST_MODIFIED_STATE, new Object[]{splitCouponRequest.getIdSplitOperationPk().toString()});
		splitCouponRequest.setRequestState(SplitCouponStateType.ANNULLED.getCode());
		splitCouponRequest.getCustodyOperation().setState(SplitCouponStateType.ANNULLED.getCode());
		splitCouponRequest.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		splitCouponRequest.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
		splitCouponRequest.getCustodyOperation().setRejectMotive(registerDetachmentCouponsTO.getMotiveSelected());
		splitCouponRequest.getCustodyOperation().setRejectMotiveOther(registerDetachmentCouponsTO.getComments());
		splitCouponRequest.setMotiveAnnulReject(registerDetachmentCouponsTO.getMotiveSelected());
		splitCouponRequest.setComments(registerDetachmentCouponsTO.getComments());
		if(ParameterOperationType.SECURITIES_DETACHMENT.getCode().equals(splitCouponRequest.getCustodyOperation().getOperationType())){
			List<SplitCouponMarketFact> lstSplitCouponMarketFact = null;
			List<MarketFactAccountTO> lstMarketFactTO = null;
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
				lstSplitCouponMarketFact = detachmentCouponsServiceBean.findSplitCouponMarketFact(splitCouponRequest.getIdSplitOperationPk());
			}
			if(Validations.validateListIsNotNullAndNotEmpty(lstSplitCouponMarketFact)){
				lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
				for(SplitCouponMarketFact splitCouponMarketFact:lstSplitCouponMarketFact){
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(splitCouponMarketFact.getMarketDate());
					marketFactAccountTO.setMarketRate(splitCouponMarketFact.getMarketRate());
					marketFactAccountTO.setQuantity(splitCouponMarketFact.getOperationQuantity());
					lstMarketFactTO.add(marketFactAccountTO);
				}
			}
			/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS TRANSITO AL DISPONIBLE ***/
			List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(splitCouponRequest, 
														splitCouponRequest.getSplitCouponOperations().get(0).getOperationQuantity(), lstMarketFactTO);
			accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(splitCouponRequest.getIdSplitOperationPk(),
																	BusinessProcessType.COUPON_DETACHMENT_OPERATION_REJECT.getCode(),
																	accountBalanceTOs, null, splitCouponRequest.getCustodyOperation().getOperationType()));
		}
	}

	/**
	 * Confirm request.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @throws ServiceException the service exception
	 */
	public void confirmRequest(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);  
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
	    ErrorServiceType entitiesStates = validateEntities(registerDetachmentCouponsTO);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		SplitCouponOperationTO splitCouponOperationTO = new SplitCouponOperationTO();
		splitCouponOperationTO.setIdSplitCouponOperationPk(registerDetachmentCouponsTO.getIdDetachmentCouponRequest());
		splitCouponOperationTO.setIndMarketFact(idepositarySetup.getIndMarketFact());
		splitCouponComponentService.get().confirmSplitCouponProcess(splitCouponOperationTO);
	}

	/**
	 * Reject request.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @throws ServiceException the service exception
	 */
	public void rejectRequest(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);  
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		SplitCouponRequest splitCouponRequest = detachmentCouponsServiceBean.find(SplitCouponRequest.class, registerDetachmentCouponsTO.getIdDetachmentCouponRequest());
		if(!SplitCouponStateType.APPROVED.getCode().equals(splitCouponRequest.getRequestState()))
			throw new ServiceException(ErrorServiceType.SPLIT_COUPON_REQUEST_MODIFIED_STATE, new Object[]{splitCouponRequest.getIdSplitOperationPk().toString()});
		splitCouponRequest.setRequestState(SplitCouponStateType.REJECTED.getCode());
		splitCouponRequest.getCustodyOperation().setState(SplitCouponStateType.REJECTED.getCode());
		splitCouponRequest.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		splitCouponRequest.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
		splitCouponRequest.getCustodyOperation().setRejectMotive(registerDetachmentCouponsTO.getMotiveSelected());
		splitCouponRequest.getCustodyOperation().setRejectMotiveOther(registerDetachmentCouponsTO.getComments());
		splitCouponRequest.setMotiveAnnulReject(registerDetachmentCouponsTO.getMotiveSelected());
		splitCouponRequest.setComments(registerDetachmentCouponsTO.getComments());
		if(ParameterOperationType.SECURITIES_DETACHMENT.getCode().equals(splitCouponRequest.getCustodyOperation().getOperationType())){
			List<SplitCouponMarketFact> lstSplitCouponMarketFact = null;
			List<MarketFactAccountTO> lstMarketFactTO = null;
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
				lstSplitCouponMarketFact = detachmentCouponsServiceBean.findSplitCouponMarketFact(splitCouponRequest.getIdSplitOperationPk());
			}
			if(Validations.validateListIsNotNullAndNotEmpty(lstSplitCouponMarketFact)){
				lstMarketFactTO = new ArrayList<MarketFactAccountTO>();
				for(SplitCouponMarketFact splitCouponMarketFact:lstSplitCouponMarketFact){
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(splitCouponMarketFact.getMarketDate());
					marketFactAccountTO.setMarketRate(splitCouponMarketFact.getMarketRate());
					marketFactAccountTO.setQuantity(splitCouponMarketFact.getOperationQuantity());
					lstMarketFactTO.add(marketFactAccountTO);
				}
			}
			/*** SE LLAMA AL COMPONENTE PARA MOVILIZAR LOS SALDOS TRANSITO AL DISPONIBLE ***/
			List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(splitCouponRequest, 
														splitCouponRequest.getSplitCouponOperations().get(0).getOperationQuantity(), lstMarketFactTO);
			accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(splitCouponRequest.getIdSplitOperationPk(),
																	BusinessProcessType.COUPON_DETACHMENT_OPERATION_REJECT.getCode(),
																	accountBalanceTOs, null, splitCouponRequest.getCustodyOperation().getOperationType()));
		}
	}
	
	/**
	 * Validate entities.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @return the error service type
	 */
	private ErrorServiceType validateEntities(RegisterDetachmentCouponsTO registerDetachmentCouponsTO){
		Map<String, Object> mpEntities = new HashMap<String, Object>();
		mpEntities.put(parameterServiceBean.HOLDER_KEY, registerDetachmentCouponsTO.getHolderSelected().getIdHolderPk());
		mpEntities.put(parameterServiceBean.HOLDER_ACCOUNT_KEY, registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
		mpEntities.put(parameterServiceBean.PARTICIPANT_KEY, registerDetachmentCouponsTO.getParticipantSelected());
		mpEntities.put(parameterServiceBean.SECURITY_KEY, registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
		return parameterServiceBean.validateEntities(mpEntities);
	}
		
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param splitCouponRequest the split coupon request
	 * @param quantity the quantity
	 * @param lstMarketFactTO the lst market fact to
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(SplitCouponRequest splitCouponRequest, 
																	BigDecimal quantity, List<MarketFactAccountTO> lstMarketFactTO) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(splitCouponRequest.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(splitCouponRequest.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(splitCouponRequest.getSecurity().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(quantity);
		if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFactTO))
			holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactTO);
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param targetAccountBalanceTOs the target account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, 
								List<HolderAccountBalanceTO> accountBalanceTOs, List<HolderAccountBalanceTO> targetAccountBalanceTOs, Long operationType){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		if(Validations.validateListIsNotNullAndNotEmpty(targetAccountBalanceTOs))
			objAccountComponent.setLstTargetAccounts(targetAccountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		return objAccountComponent;
	}	
	
	/**
	 * Automatic detachment coupons cancelations.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticDetachmentCouponsCancelations(Date finalDate) throws ServiceException{						
		Integer ERROR_ANULATE_REJECT = 424;
		String COMMENTS_ANULATE_REJECT = "REVERSION NOCTURNA";
		SearchDetachmentCouponsTO searchDetachmentCouponsTO = new SearchDetachmentCouponsTO();
		searchDetachmentCouponsTO.setEndDate(finalDate);
		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(SplitCouponStateType.REGISTERED.getCode());
		lstStates.add(SplitCouponStateType.APPROVED.getCode());											
		List<SplitCouponRequest> lstSplitCouponRequest = detachmentCouponsServiceBean.getRequestByRevert(searchDetachmentCouponsTO, lstStates);				
		for (SplitCouponRequest objSplitCouponRequest : lstSplitCouponRequest){	
			RegisterDetachmentCouponsTO registerDetachmentCouponsTO = new RegisterDetachmentCouponsTO();
			registerDetachmentCouponsTO.setIdDetachmentCouponRequest(objSplitCouponRequest.getIdSplitOperationPk());
			registerDetachmentCouponsTO.setMotiveSelected(ERROR_ANULATE_REJECT);
			registerDetachmentCouponsTO.setComments(COMMENTS_ANULATE_REJECT);
			if (objSplitCouponRequest.getRequestState().equals(SplitCouponStateType.REGISTERED.getCode())) {				
				annulRequest(registerDetachmentCouponsTO);
			}else if (objSplitCouponRequest.getRequestState().equals(SplitCouponStateType.APPROVED.getCode())) {				
				rejectRequest(registerDetachmentCouponsTO);
			}					
		}
		
	}

	/**
	 * Find operations.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @param blCashPart the bl cash part
	 * @return the list
	 */
	public List<Long> findOperations(RegisterDetachmentCouponsTO registerDetachmentCouponsTO, boolean blCashPart) {
		return detachmentCouponsServiceBean.findOperations(registerDetachmentCouponsTO, blCashPart);
	}

	/**
	 * Gets the mechanism operations.
	 *
	 * @param lstOperations the lst operations
	 * @return the mechanism operations
	 */
	public List<MechanismOperation> getMechanismOperations(List<Long> lstOperations) {
		return detachmentCouponsServiceBean.getMechanismOperations(lstOperations);
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param idParticipant the id participant
	 * @param holderAccountPk the holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance getHolderAccountBalance(Long idParticipant,Long holderAccountPk,String idSecurityCodePk)throws ServiceException{
		return detachmentCouponsServiceBean.getHolderAccountBalance(idParticipant, holderAccountPk, idSecurityCodePk);
	}
	
	/**
	 * Gets the market fact detachment.
	 *
	 * @param custodyOperationPk the custody operation pk
	 * @param todate the todate
	 * @return the market fact detachment
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactDetailHelpTO> getMarketFactDetachment(Long custodyOperationPk, boolean todate)throws ServiceException{
		return detachmentCouponsServiceBean.getMarketFactDetachment(custodyOperationPk, todate);
	}
	
}
