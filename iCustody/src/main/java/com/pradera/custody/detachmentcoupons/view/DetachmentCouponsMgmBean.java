package com.pradera.custody.detachmentcoupons.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.helperui.view.SecuritiesPartHolAccount;
import com.pradera.custody.detachmentcoupons.facade.DetachmentCouponsServiceFacade;
import com.pradera.custody.detachmentcoupons.to.CouponTO;
import com.pradera.custody.detachmentcoupons.to.CouponsTODataModel;
import com.pradera.custody.detachmentcoupons.to.FileDetachmentTO;
import com.pradera.custody.detachmentcoupons.to.RegisterDetachmentCouponsTO;
import com.pradera.custody.detachmentcoupons.to.SearchDetachmentCouponsTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.custody.splitcoupons.SplitCouponOperation;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcouponstype.SplitCouponMotiveRejectType;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.MechanismOperation;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DetachmentCouponsMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@LoggerCreateBean
@DepositaryWebBean
public class DetachmentCouponsMgmBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder account. */
	private HolderAccount holderAccountRequest;
	
	/** The search detachment coupons to. */
	private SearchDetachmentCouponsTO searchDetachmentCouponsTO;
	
	/** The register detachment coupons to. */
	private RegisterDetachmentCouponsTO registerDetachmentCouponsTO;
	
	/** The list file detachment to. */
	private List<FileDetachmentTO> listFileDetachmentTO;
	
	/** The mp states. */
	private Map<Integer, String> mpStates;
	
	/** The bl reject. */
	private boolean blApprove, blAnnul, blConfirm, blReject;
	
	/** The bl participant. */
	private boolean blParticipant;
	
	/** The Constant VIEW_MAPPING. */
	private final static String VIEW_MAPPING = "viewSplitCoupons";
	
	/** The Constant REGISTER_MAPPING. */
	private final static String REGISTER_MAPPING = "registerSplitCoupons";
	
	/** The detachment coupon request. */
	private SplitCouponRequest detachmentCouponRequest;

	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/** The detachment coupons service facade. */
	@EJB
	DetachmentCouponsServiceFacade detachmentCouponsServiceFacade;
	
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	 /** The securities part hol account. */
 	@Inject
	SecuritiesPartHolAccount securitiesPartHolAccount;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The map security instrument type. */
	Map<Integer, String> mapSecurityInstrumentType = new HashMap<Integer,String>();
	
	/** The parameter description. */
	Map<Integer,String> parameterDescription = new HashMap<Integer,String>();
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		searchDetachmentCouponsTO = new SearchDetachmentCouponsTO();
		searchDetachmentCouponsTO.setInitialDate(getCurrentSystemDate());
		searchDetachmentCouponsTO.setEndDate(getCurrentSystemDate());
		searchDetachmentCouponsTO.setHolderSelected(new Holder());
		searchDetachmentCouponsTO.setHolderAccountSelected(new HolderAccount());
		searchDetachmentCouponsTO.setSecuritySelected(new Security());
		searchDetachmentCouponsTO.setSearchType(BooleanType.YES.getCode());
		searchDetachmentCouponsTO.setBlDefaultSearch(true);
		holderAccountRequest   = new HolderAccount();
		securitiesPartHolAccount.setDescription(null);
		try {
			fillCombos();
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				blParticipant = true;
				Participant participant = detachmentCouponsServiceFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				searchDetachmentCouponsTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
				List<Participant> lstParticipant = new ArrayList<Participant>();
				lstParticipant.add(participant);
				searchDetachmentCouponsTO.setLstParticipants(lstParticipant);
			}else{
				searchDetachmentCouponsTO.setLstParticipants(detachmentCouponsServiceFacade.getListParticipants());
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
		showPrivilegeButtons();		
		fillMaps();
	}
	
	/**
	 * Search request.
	 */
	@LoggerAuditWeb
	public void searchRequest(){
		try {
			cleanSearchResult();
			if(Validations.validateIsNull(searchDetachmentCouponsTO.getHolderAccountSelected())){
				searchDetachmentCouponsTO.setHolderAccountSelected(new HolderAccount());
			}
			List<SplitCouponRequest> lstResult = detachmentCouponsServiceFacade.getRequest(searchDetachmentCouponsTO, blParticipant);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(SplitCouponRequest detachmentCouponRequest:lstResult)
					detachmentCouponRequest.setRequestStateDescription(mpStates.get(detachmentCouponRequest.getRequestState()));
				searchDetachmentCouponsTO.setBlNoResult(false);
				searchDetachmentCouponsTO.setLstDetachmentCouponRequest(new GenericDataModel<SplitCouponRequest>(lstResult));
			}else{
				searchDetachmentCouponsTO.setBlNoResult(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Change participant search.
	 */
	public void changeParticipantSearch(){
		searchDetachmentCouponsTO.setHolderSelected(new Holder());
		searchDetachmentCouponsTO.setHolderAccountSelected(new HolderAccount());
		cleanSearchResult();
	}
	
	/**
	 * Change holder.
	 */
	public void changeHolder(){
		try{
		searchDetachmentCouponsTO.setLstHolderAccount(null);
		searchDetachmentCouponsTO.setHolderAccountSelected(new HolderAccount());
		if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getHolderSelected().getIdHolderPk())){
			
			if(Validations.validateIsNullOrEmpty(searchDetachmentCouponsTO.getParticipantSelected())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_PARTICIPANT_REQUIRED));
				JSFUtilities.showSimpleValidationDialog();
				searchDetachmentCouponsTO.setHolderSelected(new Holder());				
			}else{
				List<HolderAccount> lstResult = detachmentCouponsServiceFacade.findHolderAccounts(searchDetachmentCouponsTO.getHolderSelected().getIdHolderPk(), searchDetachmentCouponsTO.getParticipantSelected());
				if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
					if(lstResult.size()==1){
					   lstResult.get(0).setSelected(true);
					   searchDetachmentCouponsTO.setHolderAccountSelected(lstResult.get(0));
					   changeSelectAccountSearch();
					}
					searchDetachmentCouponsTO.setLstHolderAccount(lstResult);
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_NOT_ACCOUNTS));
					JSFUtilities.showSimpleValidationDialog();
					searchDetachmentCouponsTO.setHolderSelected(new Holder());	
				}
			}
		 }
		}
		catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		if(!blParticipant)
			searchDetachmentCouponsTO.setParticipantSelected(null);
		searchDetachmentCouponsTO.setHolderSelected(new Holder());
		searchDetachmentCouponsTO.setHolderAccountSelected(new HolderAccount());
		searchDetachmentCouponsTO.setSecuritySelected(new Security());
		searchDetachmentCouponsTO.setInitialDate(getCurrentSystemDate());
		searchDetachmentCouponsTO.setEndDate(getCurrentSystemDate());
		searchDetachmentCouponsTO.setStateSelected(null);
		searchDetachmentCouponsTO.setSearchType(BooleanType.YES.getCode());
		searchDetachmentCouponsTO.setNumberRequest(null);
		searchDetachmentCouponsTO.setBlDefaultSearch(true);
		searchDetachmentCouponsTO.setLstHolderAccount(null);
		cleanSearchResult();
	}
	
	/**
	 * Clean search result.
	 */
	public void cleanSearchResult(){
		detachmentCouponRequest = null;
		searchDetachmentCouponsTO.setLstDetachmentCouponRequest(null);
		searchDetachmentCouponsTO.setBlNoResult(false);
	}
	
	/**
	 * Search by.
	 */
	public void searchBy(){
		JSFUtilities.resetViewRoot();
		cleanSearchResult();
		if(BooleanType.YES.getCode().equals(searchDetachmentCouponsTO.getSearchType())){
			searchDetachmentCouponsTO.setBlDefaultSearch(true);
			searchDetachmentCouponsTO.setNumberRequest(null);
		}else{
			searchDetachmentCouponsTO.setBlDefaultSearch(false);
			if(!blParticipant)
				searchDetachmentCouponsTO.setParticipantSelected(null);
			searchDetachmentCouponsTO.setInitialDate(getCurrentSystemDate());
			searchDetachmentCouponsTO.setEndDate(getCurrentSystemDate());
			searchDetachmentCouponsTO.setStateSelected(null);
			searchDetachmentCouponsTO.setHolderSelected(new Holder());
			searchDetachmentCouponsTO.setHolderAccountSelected(new HolderAccount());
			searchDetachmentCouponsTO.setSecuritySelected(new Security());
		}
	}
	
	/**
	 * New request.
	 *
	 * @return the string
	 */
	public String newRequest(){
		registerDetachmentCouponsTO = new RegisterDetachmentCouponsTO();
		listFileDetachmentTO = new ArrayList<FileDetachmentTO>();
		registerDetachmentCouponsTO.setRegistryDate(getCurrentSystemDate());
		registerDetachmentCouponsTO.setHolderSelected(new Holder());
		registerDetachmentCouponsTO.setSecuritySelected(new Security());
		registerDetachmentCouponsTO.setLstParticipants(searchDetachmentCouponsTO.getLstParticipants());
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:pnlSecurityAccHelp");
		securitiesPartHolAccount.setDescription(null);
		if(blParticipant)
			registerDetachmentCouponsTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
		return REGISTER_MAPPING;
	}
	
	/**
	 * Search balances and coupons.
	 */
	public void searchBalancesAndCoupons(){
		cleanBalancesAndCoupons();
		JSFUtilities.resetComponent("frmDetachmentCoupons:txtQuantity");
		try {
			if(Validations.validateIsNullOrEmpty(registerDetachmentCouponsTO.getHolderAccountSelected())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_ACCOUNT_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			BigDecimal availableBalance = null;
			detachmentCouponsServiceFacade.getBalances(registerDetachmentCouponsTO);
			if(Validations.validateIsNotNull(registerDetachmentCouponsTO.getLstHolderAccountBalance())){
				if(Validations.validateListIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList())){
					availableBalance = registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList().get(0).getAvailableBalance();
					registerDetachmentCouponsTO.setAvailableBalance(availableBalance);
				}else{
					registerDetachmentCouponsTO.setLstHolderAccountBalance(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_NO_BALANCE));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			detachmentCouponsServiceFacade.getCoupons(registerDetachmentCouponsTO,parameterDescription);
			if(Validations.validateIsNull(registerDetachmentCouponsTO.getLstCoupons())){
				registerDetachmentCouponsTO.setLstCoupons(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_NO_PENDING_COUPONS));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(registerDetachmentCouponsTO.getLstCoupons().getSize() == 0){
				registerDetachmentCouponsTO.setLstCoupons(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_NO_PENDING_COUPONS));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			//Si hay saldo en compra mayor a 0 se buscan las operaciones que liquiden hoy
			if(registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList().get(0).getPurchaseBalance().compareTo(BigDecimal.ZERO) > 0){
				List<Long> lstOperations = detachmentCouponsServiceFacade.findOperations(registerDetachmentCouponsTO, true);
				if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
					List<MechanismOperation> lstResult = detachmentCouponsServiceFacade.getMechanismOperations(lstOperations);
					for(MechanismOperation mechaOpe:lstResult)
						mechaOpe.setSelected(false);
					registerDetachmentCouponsTO.setLstMechanismOperation(new GenericDataModel<MechanismOperation>(lstResult));
					registerDetachmentCouponsTO.setLstSplitCouponMarketTemp(new ArrayList<SplitCouponMarketFact>());
				}
			}
			//Si hay saldo reportado mayor a 0 se buscan las operaciones que liquiden hoy
			if(registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList().get(0).getReportedBalance().compareTo(BigDecimal.ZERO) > 0){
				List<Long> lstOperations = detachmentCouponsServiceFacade.findOperations(registerDetachmentCouponsTO, false);
				if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
					List<MechanismOperation> lstResult = detachmentCouponsServiceFacade.getMechanismOperations(lstOperations);
					for(MechanismOperation mechaOpe:lstResult)
						mechaOpe.setSelected(false);
					if(Validations.validateIsNull(registerDetachmentCouponsTO.getLstMechanismOperation()))
						registerDetachmentCouponsTO.getLstMechanismOperation().getDataList().addAll(lstResult);
					if(Validations.validateListIsNullOrEmpty(registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()))
						registerDetachmentCouponsTO.setLstSplitCouponMarketTemp(new ArrayList<SplitCouponMarketFact>());
				}
			}
			Security security = detachmentCouponsServiceFacade.findSecurity(registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
			if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode())
					&& Validations.validateIsNotNullAndPositive(availableBalance)) {
				registerDetachmentCouponsTO.setGeneralQuantity(GeneralConstants.ONE_VALUE_BIGDECIMAL);
				this.applyQuantity();
			}
			
			List<HolderMarketFactBalance> lstResult = detachmentCouponsServiceFacade.findMarketFactBalance(registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk()
									, registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				if(lstResult.size() > 1)
					registerDetachmentCouponsTO.setBlQuantityOff(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate security.
	 */
	public void validateSecurity(){
		cleanBalancesAndCoupons();
		if(Validations.validateIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk())){
			Security security = detachmentCouponsServiceFacade.findSecurity(registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
			if(SecurityStateType.BLOCKED.getCode().equals(security.getStateSecurity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_SECURITY_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerDetachmentCouponsTO.setSecuritySelected(new Security());
				securitiesPartHolAccount.setDescription(null);
			}else if(!InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_SECURITY_NOT_FIXED_INCOME));
				JSFUtilities.showSimpleValidationDialog();
				registerDetachmentCouponsTO.setSecuritySelected(new Security());
				securitiesPartHolAccount.setDescription(null);
			}else if(BooleanType.YES.getCode().equals(security.getIndIsCoupon())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_SECURITY_IS_COUPON));
				JSFUtilities.showSimpleValidationDialog();
				registerDetachmentCouponsTO.setSecuritySelected(new Security());
				securitiesPartHolAccount.setDescription(null);
			}else if(BooleanType.YES.getCode().equals(security.getIndEarlyRedemption())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_SECURITY_EARLY_REDEMPTION));
				JSFUtilities.showSimpleValidationDialog();
				registerDetachmentCouponsTO.setSecuritySelected(new Security());
				securitiesPartHolAccount.setDescription(null);
			}else if(BooleanType.NO.getCode().equals(security.getIndSplitCoupon())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_SECURITY_HASNT_SPLIT));						
				JSFUtilities.showSimpleValidationDialog();
				registerDetachmentCouponsTO.setSecuritySelected(new Security());
				securitiesPartHolAccount.setDescription(null);
			}
		}
	}
	
	/**
	 * Validate holder.
	 */
	public void validateHolder(){
		try {
			cleanBalancesAndCoupons();
			registerDetachmentCouponsTO.setLstHolderAccount(null);
			registerDetachmentCouponsTO.setHolderAccountSelected(null);
			if(Validations.validateIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getHolderSelected().getIdHolderPk())){
				if(HolderStateType.BLOCKED.getCode().equals(registerDetachmentCouponsTO.getHolderSelected().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_BLOCKED));
					JSFUtilities.showSimpleValidationDialog();
					registerDetachmentCouponsTO.setHolderSelected(new Holder());
					return;
				}
				if(Validations.validateIsNullOrEmpty(registerDetachmentCouponsTO.getParticipantSelected())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerDetachmentCouponsTO.setHolderSelected(new Holder());				
				}else{
					List<HolderAccount> lstResult = detachmentCouponsServiceFacade.findHolderAccounts(registerDetachmentCouponsTO.getHolderSelected().getIdHolderPk(), registerDetachmentCouponsTO.getParticipantSelected());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						if(lstResult.size()==1){
						   registerDetachmentCouponsTO.setHolderAccountSelected(lstResult.get(0));
						   changeSelectAccount();
						}
						registerDetachmentCouponsTO.setLstHolderAccount(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerDetachmentCouponsTO.setHolderSelected(new Holder());	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	public void changeSelectAccount(){
		if(registerDetachmentCouponsTO!=null){
			holderAccountRequest = new HolderAccount();
			holderAccountRequest.setIdHolderAccountPk(registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
			holderAccountRequest.setAccountNumber(registerDetachmentCouponsTO.getHolderAccountSelected().getAccountNumber());
		}
	}
	
	public void changeSelectAccountSearch(){
		if(searchDetachmentCouponsTO!=null){
			holderAccountRequest = new HolderAccount();
			holderAccountRequest.setIdHolderAccountPk(searchDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
			holderAccountRequest.setAccountNumber(searchDetachmentCouponsTO.getHolderAccountSelected().getAccountNumber());
		}
	}
	
	public void cleanDataTableHandler(){
		//Limpiamos informacion de la grilla
		JSFUtilities.resetComponent("frmHolderAccountBalancMov:pnlSecurityAccHelp");
	}
	/**
	 * Show market fact ui.
	 */
	public void showMarketFactUI(){		
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(registerDetachmentCouponsTO.getParticipantSelected());
		marketFactBalance.setUiComponentName("balancemarket");
//		marketFactBalance.setIndTotalBalance(BooleanType.YES.getCode());
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		if(registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()!=null && !registerDetachmentCouponsTO.getLstSplitCouponMarketTemp().isEmpty()){
			for(SplitCouponMarketFact splitCouponMarketFact: registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(splitCouponMarketFact.getIdSplitMarketFactPk());
				marketDetail.setEnteredBalance(splitCouponMarketFact.getOperationQuantity());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}
		else{
			if(Validations.validateIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getGeneralQuantity())
				&& registerDetachmentCouponsTO.getGeneralQuantity().compareTo(BigDecimal.ZERO) > 0){
				marketFactBalance.setBalanceResult(registerDetachmentCouponsTO.getGeneralQuantity());
			}
		}
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(), marketFactBalance); 
	}
	
	/**
	 * Show security.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurity(String securityCode){
//		<ark:securities-help isinCode=""/>
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(), securityData);
	}
	
	/**
	 * Select market fact.
	 */
	public void selectMarketFact(){
		registerDetachmentCouponsTO.setGeneralQuantity(holderMarketFactBalance.getBalanceResult());
		applyQuantity();
		JSFUtilities.updateComponent("frmDetachmentCoupons:opnlCoupons");
		JSFUtilities.updateComponent("frmDetachmentCoupons:opnlBalance");
		JSFUtilities.resetComponent("frmDetachmentCoupons:txtQuantity");
		JSFUtilities.updateComponent("frmDetachmentCoupons:txtQuantity");		
	}
	
	/**
	 * Validate operation quantity.
	 *
	 * @param mechanismOperation the mechanism operation
	 */
	public void validateOperationQuantity(MechanismOperation mechanismOperation){		
		if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getQuantity())
				&& mechanismOperation.getQuantity().compareTo(BigDecimal.ZERO) > 0
				&& mechanismOperation.getQuantity().compareTo(mechanismOperation.getStockQuantity()) <= 0){
			if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getTempQuantity()))
				subtractOrAddQuantity(mechanismOperation.getTempQuantity(), false);			
			subtractOrAddQuantity(mechanismOperation.getQuantity(), true);
			addOrDeleteMarketFact(mechanismOperation, true);
			if(!mechanismOperation.getQuantity().equals(mechanismOperation.getTempQuantity()))
				mechanismOperation.setTempQuantity(mechanismOperation.getQuantity());
		}else{
			if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getTempQuantity()))
				subtractOrAddQuantity(mechanismOperation.getTempQuantity(), false);
			addOrDeleteMarketFact(mechanismOperation, false);
			mechanismOperation.setQuantity(null);
			mechanismOperation.setTempQuantity(null);
		}
	}
	
	/**
	 * Select mechanism operation.
	 *
	 * @param mechanismOperation the mechanism operation
	 */
	public void selectMechanismOperation(MechanismOperation mechanismOperation){
		mechanismOperation.setTempQuantity(null);
		if(!mechanismOperation.isSelected()){			
			if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getQuantity()))
				subtractOrAddQuantity(mechanismOperation.getQuantity(), false);
			mechanismOperation.setQuantity(null);
			addOrDeleteMarketFact(mechanismOperation, false);
		}
	}
	
	/**
	 * Adds the or delete market fact.
	 *
	 * @param mechanismOperation the mechanism operation
	 * @param blAction the bl action
	 */
	private void addOrDeleteMarketFact(MechanismOperation mechanismOperation, boolean blAction){
		if(blAction){
			boolean blExist = false;
			for(SplitCouponMarketFact splitCouponMarketFact:registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()){
				if(splitCouponMarketFact.getIdSplitMarketFactPk().equals(mechanismOperation.getIdMechanismOperationPk())
						&& splitCouponMarketFact.getOperationQuantity().equals(mechanismOperation.getQuantity())){
					blExist = true;
				}					
			}			
			if(!blExist){
				addOrDeleteMarketFact(mechanismOperation, false);
				SplitCouponMarketFact splitCouponMarketFact = new SplitCouponMarketFact();
				splitCouponMarketFact.setIdSplitMarketFactPk(mechanismOperation.getIdMechanismOperationPk());
				splitCouponMarketFact.setMarketDate(mechanismOperation.getMechanismOperationMarketFacts().get(0).getMarketDate());
				splitCouponMarketFact.setMarketRate(mechanismOperation.getMechanismOperationMarketFacts().get(0).getMarketRate());
				splitCouponMarketFact.setOperationQuantity(mechanismOperation.getQuantity());
				registerDetachmentCouponsTO.getLstSplitCouponMarketTemp().add(splitCouponMarketFact);
			}
		}else{
			for(SplitCouponMarketFact splitCouponMarketFact:registerDetachmentCouponsTO.getLstSplitCouponMarketTemp()){
				if(splitCouponMarketFact.getIdSplitMarketFactPk().equals(mechanismOperation.getIdMechanismOperationPk())){
					registerDetachmentCouponsTO.getLstSplitCouponMarketTemp().remove(splitCouponMarketFact);
					break;
				}					
			}
		}
	}
	
	/**
	 * Subtract or add quantity.
	 *
	 * @param quantity the quantity
	 * @param blAction the bl action
	 */
	private void subtractOrAddQuantity(BigDecimal quantity, boolean blAction){
		if(blAction){
			for(CouponTO couponTO:registerDetachmentCouponsTO.getLstCoupons()){
				if(Validations.validateIsNullOrEmpty(couponTO.getDetachmentQuantity()))
					couponTO.setDetachmentQuantity(quantity);
				else
					couponTO.setDetachmentQuantity(couponTO.getDetachmentQuantity().add(quantity));				
			}		
		}else{
			for(CouponTO couponTO:registerDetachmentCouponsTO.getLstCoupons()){
				if(Validations.validateIsNullOrEmpty(couponTO.getDetachmentQuantity())){
					couponTO.setDetachmentQuantity(null);
				}else{
					couponTO.setDetachmentQuantity(couponTO.getDetachmentQuantity().subtract(quantity));
					if(couponTO.getDetachmentQuantity().compareTo(BigDecimal.ZERO) == 0)
						couponTO.setDetachmentQuantity(null);
				}				
			}
		}
	}
	
	/**
	 * Apply quantity.
	 */
	public void applyQuantity(){
		registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList().get(0).setAvailableBalance(registerDetachmentCouponsTO.getAvailableBalance());
		BigDecimal totalBalance = registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList().get(0).getTotalBalance().subtract(registerDetachmentCouponsTO.getLstHolderAccountBalance().getDataList().get(0).getReportingBalance());
		boolean blHasOne = false;
		if(Validations.validateIsNotNullAndNotEmpty(registerDetachmentCouponsTO.getGeneralQuantity())
				&& registerDetachmentCouponsTO.getGeneralQuantity().compareTo(BigDecimal.ZERO) > 0
				&& totalBalance.compareTo(registerDetachmentCouponsTO.getGeneralQuantity()) >= 0){
			for(CouponTO couponTO:registerDetachmentCouponsTO.getLstCoupons()){
					couponTO.setDetachmentQuantity(registerDetachmentCouponsTO.getGeneralQuantity());
					blHasOne = true;				
			}
			if(blHasOne){
				for(HolderAccountBalance holderAccountBalance:registerDetachmentCouponsTO.getLstHolderAccountBalance())
					holderAccountBalance.setAvailableBalance(holderAccountBalance.getAvailableBalance().subtract(registerDetachmentCouponsTO.getGeneralQuantity()));
				if(Validations.validateIsNotNull(registerDetachmentCouponsTO.getLstMechanismOperation())){
					for(MechanismOperation mechanismOperation:registerDetachmentCouponsTO.getLstMechanismOperation()){
						if(mechanismOperation.isSelected()){
							if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getQuantity())){
								subtractOrAddQuantity(mechanismOperation.getQuantity(), true);								
							}
						}
					}
				}
			}else{
				registerDetachmentCouponsTO.setGeneralQuantity(null);
				for(CouponTO couponTO:registerDetachmentCouponsTO.getLstCoupons())
					couponTO.setDetachmentQuantity(null);
			}
		}else{
			registerDetachmentCouponsTO.setGeneralQuantity(null);
			for(CouponTO couponTO:registerDetachmentCouponsTO.getLstCoupons())
				couponTO.setDetachmentQuantity(null);
			if(Validations.validateIsNotNull(registerDetachmentCouponsTO.getLstMechanismOperation())){
				for(MechanismOperation mechanismOperation:registerDetachmentCouponsTO.getLstMechanismOperation()){
					if(mechanismOperation.isSelected()){
						if(Validations.validateIsNotNullAndNotEmpty(mechanismOperation.getQuantity())){
							subtractOrAddQuantity(mechanismOperation.getQuantity(), true);								
						}
					}
				}
			}
		}
	}
	
	/**
	 * Document file handler.
	 *
	 * @param event the event
	 */
	public void documentFileHandler(FileUploadEvent event) {
		try {
			 String fDisplayName = fUploadValidateFile(event.getFile(), null, getfUploadFileDocumentsSize(), getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				registerDetachmentCouponsTO.setFileName(fDisplayName);
				registerDetachmentCouponsTO.setDocumentFile(event.getFile().getContents());	
				FileDetachmentTO fileDetachmentTO  = new FileDetachmentTO();
				fileDetachmentTO.setDocumentFile(event.getFile().getContents());
				fileDetachmentTO.setFileName(fDisplayName);
				String extensionFile = event.getFile().getFileName().toLowerCase().substring(event.getFile().getFileName().lastIndexOf(".")+1,event.getFile().getFileName().length());
				fileDetachmentTO.setFileExtension(extensionFile.toUpperCase());
				fileDetachmentTO.setFileSize(event.getFile().getSize());
				listFileDetachmentTO = new ArrayList<FileDetachmentTO>();
				listFileDetachmentTO.add(fileDetachmentTO);
			 }
		} catch (Exception e) {
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the uploaded file handler.
	 */
	public void removeUploadedFileHandler(){
		registerDetachmentCouponsTO.setDocumentFile(null); 
		registerDetachmentCouponsTO.setFileName(null);
		listFileDetachmentTO = null;
	}
	
	/**
	 * Before save.
	 */
	public void beforeSave(){
		if(Validations.validateListIsNullOrEmpty(registerDetachmentCouponsTO.getCouponsSelected())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_ERROR_COUPON_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}else if(Validations.validateIsNullOrEmpty(registerDetachmentCouponsTO.getFileName())
				&& !blParticipant){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_FILE_REQUIRED));
			JSFUtilities.showSimpleValidationDialog();
		}else if(Validations.validateIsNull(registerDetachmentCouponsTO.getLstMechanismOperation())){
			if(Validations.validateIsNullOrEmpty(registerDetachmentCouponsTO.getGeneralQuantity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_QUANTITY_REQUIRED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_ASK_REGISTER));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}else if(Validations.validateIsNotNull(registerDetachmentCouponsTO.getLstMechanismOperation())){
			if(Validations.validateIsNullOrEmpty(registerDetachmentCouponsTO.getGeneralQuantity())
					&& Validations.validateListIsNullOrEmpty(registerDetachmentCouponsTO.getLstSplitCouponMarketTemp())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_QUANTITY_REQUIRED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			for(MechanismOperation mechanismOperation:registerDetachmentCouponsTO.getLstMechanismOperation()){
				if(mechanismOperation.isSelected()
						&& Validations.validateIsNullOrEmpty(mechanismOperation.getQuantity())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_MECHANISM_OPERATION_QUANTITY_EMPTY));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_ASK_REGISTER));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
		else if(!validateBalanceForDetachmentCoupons()){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_INSUFFICIENT_BALANACE));
			JSFUtilities.showSimpleValidationDialog();
		}
		else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_ASK_REGISTER));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}		
	}
	
	/**
	 * Validate balance for detachment coupons.
	 *
	 * @return true, if successful
	 */
	public boolean validateBalanceForDetachmentCoupons(){
	
		boolean indicator=false;
		
	try{
		
		HolderAccountBalance balance = detachmentCouponsServiceFacade.getHolderAccountBalance(registerDetachmentCouponsTO.getParticipantSelected(),
				registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk(),registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());		
		//Cantidad a desprender es menor o igual que el balance disponible
		if(registerDetachmentCouponsTO.getCouponsSelected().get(0).getDetachmentQuantity().intValue()<=balance.getAvailableBalance().intValue()){
			indicator = true;			
		}
		else{
			registerDetachmentCouponsTO.setIndDetachmentNotBlocked(true);
		}
		//Cantidad a desprender es menor o igual que el balance contable menos el balance reportante
		if(registerDetachmentCouponsTO.getCouponsSelected().get(0).getDetachmentQuantity().intValue()<=(balance.getTotalBalance().intValue()-balance.getReportingBalance().intValue())){
			indicator = true;
		}
		//Cantidad a desprender es menor o igual que el balance reportado
		if(registerDetachmentCouponsTO.getCouponsSelected().get(0).getDetachmentQuantity().intValue()<=balance.getReportedBalance().intValue()){
			indicator = true;
		}
		
		//Si hay saldo en compra mayor a 0 se buscan las operaciones que liquiden hoy
		if(balance.getPurchaseBalance().compareTo(BigDecimal.ZERO) > 0){
			List<Long> lstOperations = detachmentCouponsServiceFacade.findOperations(registerDetachmentCouponsTO, true);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
				List<MechanismOperation> lstResult = detachmentCouponsServiceFacade.getMechanismOperations(lstOperations);
			    int sum=0;
				for(MechanismOperation mech : lstResult){
					sum+=mech.getStockQuantity().intValue();
			    }
				//Balance Disponible + cantidad a liquidar es mayor e igual a la  cantidad a desprender
				if(balance.getAvailableBalance().intValue()+sum>=registerDetachmentCouponsTO.getCouponsSelected().get(0).getDetachmentQuantity().intValue()){
				   indicator = true;	
				}
			}
		}
		//Si hay saldo reportado mayor a 0 se buscan las operaciones que liquiden hoy
		if(balance.getReportedBalance().compareTo(BigDecimal.ZERO) > 0){
			List<Long> lstOperations = detachmentCouponsServiceFacade.findOperations(registerDetachmentCouponsTO, false);
			if(Validations.validateListIsNotNullAndNotEmpty(lstOperations)){
				List<MechanismOperation> lstResult = detachmentCouponsServiceFacade.getMechanismOperations(lstOperations);
				int sum=0;
				for(MechanismOperation mech : lstResult){
					sum+=mech.getStockQuantity().intValue();
			    }
				
				//Balance Disponible + cantidad a liquidar es mayor e igual a la  cantidad a desprender
				if(balance.getAvailableBalance().intValue()+sum>=registerDetachmentCouponsTO.getCouponsSelected().get(0).getDetachmentQuantity().intValue()){
					   indicator = true;	
				}
				
			}
		}
	}
	catch(Exception e){
	    e.printStackTrace();
	}
		
		return indicator;
	}
	
	
	/**
	 * Save request.
	 */
	@LoggerAuditWeb
	public void saveRequest(){
		try {
			Long idDetachmentCouponRequest = detachmentCouponsServiceFacade.saveRequest(registerDetachmentCouponsTO, holderMarketFactBalance);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_SUCCESS_REGISTER,new Object[]{idDetachmentCouponRequest.toString()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Clean register.
	 */
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		securitiesPartHolAccount.setDescription(null);
		registerDetachmentCouponsTO = new RegisterDetachmentCouponsTO();
		registerDetachmentCouponsTO.setRegistryDate(getCurrentSystemDate());
		registerDetachmentCouponsTO.setHolderSelected(new Holder());
		registerDetachmentCouponsTO.setSecuritySelected(new Security());
		registerDetachmentCouponsTO.setLstParticipants(searchDetachmentCouponsTO.getLstParticipants());
		if(blParticipant)
			registerDetachmentCouponsTO.setParticipantSelected(userInfo.getUserAccountSession().getParticipantCode());
	}
	
	/**
	 * Clean balances and coupons.
	 */
	public void cleanBalancesAndCoupons(){
		registerDetachmentCouponsTO.setBlQuantityOff(false);
		registerDetachmentCouponsTO.setCouponsSelected(null);
		registerDetachmentCouponsTO.setLstHolderAccountBalance(null);
		registerDetachmentCouponsTO.setLstCoupons(null);
		registerDetachmentCouponsTO.setGeneralQuantity(null);
		registerDetachmentCouponsTO.setLstMechanismOperation(null);
		registerDetachmentCouponsTO.setLstSplitCouponMarketTemp(null);
		JSFUtilities.resetComponent("frmDetachmentCoupons:pnlSecurityAccHelp");
		removeUploadedFileHandler();
	}
	
	/**
	 * Change participant.
	 */
	public void changeParticipant(){
		registerDetachmentCouponsTO.setHolderSelected(new Holder());
		registerDetachmentCouponsTO.setHolderAccountSelected(null);
		registerDetachmentCouponsTO.setLstHolderAccount(null);
		cleanBalancesAndCoupons();
	}
	
	/**
	 * View request.
	 *
	 * @param detachmentCouponRequest the detachment coupon request
	 * @return the string
	 */
	public String viewRequest(SplitCouponRequest detachmentCouponRequest){
		try {
			setFlagsFalse();
			fillView(detachmentCouponRequest);
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Gets the streamed content file.
	 *
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(registerDetachmentCouponsTO.getDocumentFile());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	registerDetachmentCouponsTO.getFileName());
			inputStream.close();
		} catch (Exception e) {
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContentFile;
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(detachmentCouponRequest)){
				if(SplitCouponStateType.APPROVED.getCode().equals(detachmentCouponRequest.getRequestState())){
					blConfirm = true;
					fillView(detachmentCouponRequest);
					return VIEW_MAPPING;	
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_INVALID_STATE_APPROVE));
					JSFUtilities.showSimpleValidationDialog();
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(detachmentCouponRequest)){
				if(SplitCouponStateType.APPROVED.getCode().equals(detachmentCouponRequest.getRequestState())){
					blReject = true;
					fillView(detachmentCouponRequest);
					return VIEW_MAPPING;	
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_INVALID_STATE_APPROVE));
					JSFUtilities.showSimpleValidationDialog();
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	public String validateApprove(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(detachmentCouponRequest)){
				if(SplitCouponStateType.REGISTERED.getCode().equals(detachmentCouponRequest.getRequestState())){
					blApprove = true;
					fillView(detachmentCouponRequest);
					return VIEW_MAPPING;	
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_INVALID_STATE_REGISTER));
					JSFUtilities.showSimpleValidationDialog();
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate annul.
	 *
	 * @return the string
	 */
	public String validateAnnul(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(detachmentCouponRequest)){
				if(SplitCouponStateType.REGISTERED.getCode().equals(detachmentCouponRequest.getRequestState())){
					blAnnul = true;
					fillView(detachmentCouponRequest);
					return VIEW_MAPPING;	
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_INVALID_STATE_REGISTER));
					JSFUtilities.showSimpleValidationDialog();
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();			
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Reject others.
	 */
	public void rejectOthers(){
		JSFUtilities.resetViewRoot();
		registerDetachmentCouponsTO.setComments(null);
		registerDetachmentCouponsTO.setBlOthers(false);
		if(SplitCouponMotiveRejectType.OTHERS_MOTIVES.getCode().equals(registerDetachmentCouponsTO.getMotiveSelected()))
			registerDetachmentCouponsTO.setBlOthers(true);		
	}	
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		String headerText = null;
		String bodyText = null;
		if(blApprove){
			headerText = GeneralConstants.LBL_HEADER_ALERT_APPROVE;
			bodyText = PropertiesConstants.SPLIT_COUPON_ASK_APPROVE;
		}else if(blAnnul){
			headerText = GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL;
		}else if(blConfirm){
			headerText = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			bodyText = PropertiesConstants.SPLIT_COUPON_ASK_CONFIRM;
		}else if(blReject){
			headerText = GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT;
		}
		if(blConfirm || blApprove){
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerText), 
						PropertiesUtilities.getMessage(bodyText, new Object[]{registerDetachmentCouponsTO.getOpearationNumber().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
		if(blAnnul || blReject){
		resetComponents();
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerText),null);
		JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
	    registerDetachmentCouponsTO.setMotiveSelected(null);
	    registerDetachmentCouponsTO.setComments(null);
	    registerDetachmentCouponsTO.setBlOthers(false);
	    }
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try { 
			String bodyText = null;
			if(blApprove){
				detachmentCouponsServiceFacade.approveRequest(registerDetachmentCouponsTO);
				bodyText = PropertiesConstants.SPLIT_COUPON_SUCCESS_APPROVE;
			}else if(blAnnul){
				detachmentCouponsServiceFacade.annulRequest(registerDetachmentCouponsTO);
				bodyText = PropertiesConstants.SPLIT_COUPON_SUCCESS_ANNUL;
			}else if(blConfirm){
				detachmentCouponsServiceFacade.confirmRequest(registerDetachmentCouponsTO);
				bodyText = PropertiesConstants.SPLIT_COUPON_SUCCESS_CONFIRM;
			}else if(blReject){
				detachmentCouponsServiceFacade.rejectRequest(registerDetachmentCouponsTO);
				bodyText = PropertiesConstants.SPLIT_COUPON_SUCCESS_REJECT;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(bodyText,new Object[]{registerDetachmentCouponsTO.getOpearationNumber().toString()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			searchRequest();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.SPLIT_COUPON_STATE.getCode());
		searchDetachmentCouponsTO.setLstStates(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		for(ParameterTable parameter: searchDetachmentCouponsTO.getLstStates()){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
		}
		
		/**Method to get ProgramInterestCoupon states*/
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.PROGRAM_INTEREST_STATE.getCode());
		for(ParameterTable parameter: generalParametersFacade.getListParameterTableServiceBean(paramTabTO)){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getParameterName());
		}
		
		/**Method to get configuration of split*/
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.SPLIT_CONFIGURATION_TYPE.getCode());
		for(ParameterTable parameter: generalParametersFacade.getListParameterTableServiceBean(paramTabTO)){
			parameterDescription.put(parameter.getParameterTablePk(), parameter.getLongInteger().toString());
		}
	
		List<ParameterTable> lstInstrumentType = new ArrayList<ParameterTable>();
		mapSecurityInstrumentType = new HashMap<Integer, String>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstInstrumentType.add(param);
			mapSecurityInstrumentType.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Fill maps.
	 */
	private void fillMaps(){
		mpStates = new HashMap<Integer, String>();
		for(ParameterTable paramTab:searchDetachmentCouponsTO.getLstStates())
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Sets the flags false.
	 */
	private void setFlagsFalse(){
		blApprove = false;
		blAnnul = false;
		blConfirm = false;
		blReject = false;
	}
	
	/**
	 * Fill view.
	 *
	 * @param spliCouponRequest the spli coupon request
	 * @throws ServiceException the service exception
	 */
	private void fillView(SplitCouponRequest spliCouponRequest) throws ServiceException{
		registerDetachmentCouponsTO = new RegisterDetachmentCouponsTO();
		listFileDetachmentTO = new ArrayList<FileDetachmentTO>();
		registerDetachmentCouponsTO.setIdDetachmentCouponRequest(spliCouponRequest.getIdSplitOperationPk());
		registerDetachmentCouponsTO.setRegistryDate(spliCouponRequest.getRegistryDate());
		registerDetachmentCouponsTO.setLstStates(searchDetachmentCouponsTO.getLstStates());
		List<Participant> lstParticipant = new ArrayList<Participant>();
		lstParticipant.add(spliCouponRequest.getParticipant());
		registerDetachmentCouponsTO.setLstParticipants(lstParticipant);
		registerDetachmentCouponsTO.setParticipantSelected(spliCouponRequest.getParticipant().getIdParticipantPk());
		registerDetachmentCouponsTO.setHolderSelected(spliCouponRequest.getHolder());
		registerDetachmentCouponsTO.setSecuritySelected(spliCouponRequest.getSecurity());
		registerDetachmentCouponsTO.setSecuritySubProduct(spliCouponRequest.getSecurityProduct());
		registerDetachmentCouponsTO.setHolderAccountSelected(detachmentCouponsServiceFacade.findHolderAccount(spliCouponRequest.getHolderAccount().getIdHolderAccountPk()));
		registerDetachmentCouponsTO.setFileName(spliCouponRequest.getFileName());
		registerDetachmentCouponsTO.setDocumentFile(spliCouponRequest.getDocumentFile());
		registerDetachmentCouponsTO.setOpearationNumber(spliCouponRequest.getCustodyOperation().getOperationNumber());
		FileDetachmentTO fileDetachmentTO = new FileDetachmentTO();
		fileDetachmentTO.setDocumentFile(spliCouponRequest.getDocumentFile());
		fileDetachmentTO.setFileName(spliCouponRequest.getFileName());
		String extensionFile = "";
		if(spliCouponRequest.getFileName()!=null){
			extensionFile = spliCouponRequest.getFileName().toLowerCase().substring(spliCouponRequest.getFileName().lastIndexOf(".")+1,spliCouponRequest.getFileName().length());
			fileDetachmentTO.setFileExtension(extensionFile.toUpperCase());
			fileDetachmentTO.setFileSize(Integer.valueOf(spliCouponRequest.getDocumentFile().length).longValue()/1000L);
		}
		listFileDetachmentTO.add(fileDetachmentTO);		
		registerDetachmentCouponsTO.setState(spliCouponRequest.getRequestState());
		detachmentCouponsServiceFacade.getBalances(registerDetachmentCouponsTO);
		
		/*Para el helper de hechos del mercado*/
		holderMarketFactBalance = new MarketFactBalanceHelpTO();
		holderMarketFactBalance.setSecurityCodePk(spliCouponRequest.getSecurity().getIdSecurityCodePk());
		holderMarketFactBalance.setSecurityDescription(spliCouponRequest.getSecurity().getDescription());
		holderMarketFactBalance.setInstrumentType(spliCouponRequest.getSecurity().getInstrumentType());
		holderMarketFactBalance.setInstrumentDescription(mapSecurityInstrumentType.get(spliCouponRequest.getSecurity().getInstrumentType()));
		holderMarketFactBalance.setBalanceResult(spliCouponRequest.getSplitCouponOperations().get(0).getOperationQuantity());
		
		boolean todate = false;
		if(spliCouponRequest.getCustodyOperation().getRegistryDate().equals(CommonsUtilities.currentDate())){
			todate = true;
		}
		List<MarketFactDetailHelpTO> lstMarketFact = detachmentCouponsServiceFacade.getMarketFactDetachment(spliCouponRequest.getIdSplitOperationPk(), todate);
		if(lstMarketFact !=null && lstMarketFact.size()>0){
			holderMarketFactBalance.setMarketFacBalances(lstMarketFact);
			holderMarketFactBalance.setTodate(todate);
		}
		/**/
		
		
		List<CouponTO> lstCouponsTO = new ArrayList<CouponTO>();
		for(SplitCouponOperation splitCouponOperation:spliCouponRequest.getSplitCouponOperations()){
			CouponTO couponTO = new CouponTO();
			couponTO.setIdProgramInterestPk(splitCouponOperation.getProgramInterestCoupon().getIdProgramInterestPk());
			couponTO.setCouponNumber(splitCouponOperation.getProgramInterestCoupon().getCouponNumber());
			couponTO.setExpirationDate(splitCouponOperation.getProgramInterestCoupon().getExperitationDate());
			couponTO.setPaymentDate(splitCouponOperation.getProgramInterestCoupon().getPaymentDate());
			couponTO.setRegistryDate(splitCouponOperation.getProgramInterestCoupon().getRegistryDate());
			couponTO.setDetachmentQuantity(splitCouponOperation.getOperationQuantity());
			if(Validations.validateIsNotNull(splitCouponOperation.getProgramInterestCoupon().getSplitSecurities())&& Validations.validateIsNotNull(splitCouponOperation.getProgramInterestCoupon().getSplitSecurities().getIdSecurityCodePk())){
				couponTO.setSecurityGenerated(splitCouponOperation.getProgramInterestCoupon().getSplitSecurities().getIdSecurityCodePk());
			}
			lstCouponsTO.add(couponTO);
		}
		Collections.sort(lstCouponsTO,COMPARE_BY_COUPON_NUMBER);
		
		registerDetachmentCouponsTO.setLstCoupons(new CouponsTODataModel(lstCouponsTO));
		if(blAnnul || blReject || SplitCouponStateType.ANNULLED.getCode().equals(registerDetachmentCouponsTO.getState())
				|| SplitCouponStateType.REJECTED.getCode().equals(registerDetachmentCouponsTO.getState())){
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.SPLIT_COUPON_REQUEST_MOTIVE_REJECT.getCode());
			registerDetachmentCouponsTO.setLstMotives(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));	
		}
		if(SplitCouponStateType.ANNULLED.getCode().equals(registerDetachmentCouponsTO.getState())
				|| SplitCouponStateType.REJECTED.getCode().equals(registerDetachmentCouponsTO.getState())){
			registerDetachmentCouponsTO.setBlMotives(true);
			registerDetachmentCouponsTO.setMotiveSelected(spliCouponRequest.getMotiveAnnulReject());
			registerDetachmentCouponsTO.setComments(spliCouponRequest.getComments());
		}
	}
	
	/** The compare by coupon number. */
	public static Comparator<CouponTO> COMPARE_BY_COUPON_NUMBER = new Comparator<CouponTO>() {
		 public int compare(CouponTO o1, CouponTO o2) {
				/**ORDENAMOS LOS CUPONES POR NUMERO DE CUPON*/
				int resultado = o1.getCouponNumber()-o2.getCouponNumber();
		        if ( resultado != 0 ) { return resultado; }
				return resultado;
			}
	    };
	
   /**
    * Reset components.
    */
   public void resetComponents(){
		JSFUtilities.resetViewRoot();
		
   }
   
   /**
    * Reset motive.
    */
   public void resetMotive(){
	   registerDetachmentCouponsTO.setMotiveSelected(null);
	   registerDetachmentCouponsTO.setBlOthers(false);
	   registerDetachmentCouponsTO.setComments(null);
	   resetComponents();
   }
	
   /**
    * Validate back.
    */
   public void validateBack(){
		
		if(registerDetachmentCouponsTO.getMotiveSelected()!=null){			
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGES_ALERT),PropertiesUtilities.getGenericMessage(PropertiesConstants.MESSAGE_BACK));
					JSFUtilities.executeJavascriptFunction("PF('cnfwBackMotive').show()");
		}else{
			resetComponents();
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').hide()");
		}
	}
   
   /**
    * Confirm actions.
    */
   public void confirmActions(){
	   
	    String headerText = null;
		String bodyText = null;
	    
		if(blAnnul){
			headerText = GeneralConstants.LBL_HEADER_ALERT_ANNUL;
			bodyText = PropertiesConstants.SPLIT_COUPON_ASK_ANNUL;
		}else if(blReject){
			headerText = GeneralConstants.LBL_HEADER_ALERT_ANNUL;
			bodyText = PropertiesConstants.SPLIT_COUPON_ASK_REJECT;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerText), 
						PropertiesUtilities.getMessage(bodyText, new Object[]{registerDetachmentCouponsTO.getOpearationNumber().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
   }

	/**
	 * Gets the mp states.
	 *
	 * @return the mp states
	 */
	public Map<Integer, String> getMpStates() {
		return mpStates;
	}
	
	/**
	 * Sets the mp states.
	 *
	 * @param mpStates the mp states
	 */
	public void setMpStates(Map<Integer, String> mpStates) {
		this.mpStates = mpStates;
	}
	
	/**
	 * Checks if is bl approve.
	 *
	 * @return true, if is bl approve
	 */
	public boolean isBlApprove() {
		return blApprove;
	}
	
	/**
	 * Sets the bl approve.
	 *
	 * @param blApprove the new bl approve
	 */
	public void setBlApprove(boolean blApprove) {
		this.blApprove = blApprove;
	}
	
	/**
	 * Checks if is bl annul.
	 *
	 * @return true, if is bl annul
	 */
	public boolean isBlAnnul() {
		return blAnnul;
	}
	
	/**
	 * Sets the bl annul.
	 *
	 * @param blAnnul the new bl annul
	 */
	public void setBlAnnul(boolean blAnnul) {
		this.blAnnul = blAnnul;
	}
	
	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}
	
	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}
	
	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}
	
	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}
	
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Gets the detachment coupon request.
	 *
	 * @return the detachment coupon request
	 */
	public SplitCouponRequest getDetachmentCouponRequest() {
		return detachmentCouponRequest;
	}
	
	/**
	 * Sets the detachment coupon request.
	 *
	 * @param detachmentCouponRequest the new detachment coupon request
	 */
	public void setDetachmentCouponRequest(
			SplitCouponRequest detachmentCouponRequest) {
		this.detachmentCouponRequest = detachmentCouponRequest;
	}

	/**
	 * Gets the register detachment coupons to.
	 *
	 * @return the register detachment coupons to
	 */
	public RegisterDetachmentCouponsTO getRegisterDetachmentCouponsTO() {
		return registerDetachmentCouponsTO;
	}

	/**
	 * Sets the register detachment coupons to.
	 *
	 * @param registerDetachmentCouponsTO the new register detachment coupons to
	 */
	public void setRegisterDetachmentCouponsTO(
			RegisterDetachmentCouponsTO registerDetachmentCouponsTO) {
		this.registerDetachmentCouponsTO = registerDetachmentCouponsTO;
	}

	/**
	 * Gets the search detachment coupons to.
	 *
	 * @return the search detachment coupons to
	 */
	public SearchDetachmentCouponsTO getSearchDetachmentCouponsTO() {
		return searchDetachmentCouponsTO;
	}

	/**
	 * Sets the search detachment coupons to.
	 *
	 * @param searchDetachmentCouponsTO the new search detachment coupons to
	 */
	public void setSearchDetachmentCouponsTO(
			SearchDetachmentCouponsTO searchDetachmentCouponsTO) {
		this.searchDetachmentCouponsTO = searchDetachmentCouponsTO;
	}
	
	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Gets the list file detachment to.
	 *
	 * @return the list file detachment to
	 */
	public List<FileDetachmentTO> getListFileDetachmentTO() {
		return listFileDetachmentTO;
	}

	/**
	 * Sets the list file detachment to.
	 *
	 * @param listFileDetachmentTO the new list file detachment to
	 */
	public void setListFileDetachmentTO(List<FileDetachmentTO> listFileDetachmentTO) {
		this.listFileDetachmentTO = listFileDetachmentTO;
	}

	/**
	 * Gets the parameter description.
	 *
	 * @return the parameter description
	 */
	public Map<Integer, String> getParameterDescription() {
		return parameterDescription;
	}

	/**
	 * Sets the parameter description.
	 *
	 * @param parameterDescription the parameter description
	 */
	public void setParameterDescription(Map<Integer, String> parameterDescription) {
		this.parameterDescription = parameterDescription;
	}
	
	/**
	 * Gets the holder account request.
	 *
	 * @return the holder account request
	 */
	public HolderAccount getHolderAccountRequest() {
		return holderAccountRequest;
	}

	/**
	 * Sets the holder account request.
	 *
	 * @param holderAccount the new holder account request
	 */
	public void setHolderAccountRequest(HolderAccount holderAccount) {
		this.holderAccountRequest = holderAccount;
	}
}
