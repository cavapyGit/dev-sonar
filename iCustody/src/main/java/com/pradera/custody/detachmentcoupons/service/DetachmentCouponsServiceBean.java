package com.pradera.custody.detachmentcoupons.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.custody.detachmentcoupons.to.RegisterDetachmentCouponsTO;
import com.pradera.custody.detachmentcoupons.to.SearchDetachmentCouponsTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.splitcoupons.SplitCouponMarketFact;
import com.pradera.model.custody.splitcoupons.SplitCouponRequest;
import com.pradera.model.custody.splitcoupons.SubproductCode;
import com.pradera.model.custody.splitcouponstype.SplitCouponStateType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.negotiation.MechanismOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;
import com.pradera.model.settlement.type.OperationPartType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DetachmentCouponsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
public class DetachmentCouponsServiceBean extends CrudDaoServiceBean {

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> getListParticipants() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pa FROM Participant pa");
		sbQuery.append("	WHERE pa.state = :state");
		sbQuery.append("	 AND pa.indDepositary = :indDepositary");
		sbQuery.append("	ORDER BY pa.mnemonic ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ParticipantStateType.REGISTERED.getCode());
		query.setParameter("indDepositary", BooleanType.YES.getCode());
		return (List<Participant>)query.getResultList();		
	}

	/**
	 * Gets the request.
	 *
	 * @param searchDetachmentCouponsTO the search detachment coupons to
	 * @param blParticipant the bl participant
	 * @return the request
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SplitCouponRequest> getRequest(SearchDetachmentCouponsTO searchDetachmentCouponsTO, boolean blParticipant) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct scr FROM SplitCouponRequest scr");
		sbQuery.append("	INNER JOIN FETCH scr.holder");
		sbQuery.append("	INNER JOIN FETCH scr.holderAccount");
		sbQuery.append("	LEFT OUTER JOIN FETCH scr.participant");
		sbQuery.append("	INNER JOIN FETCH scr.security");
		sbQuery.append("	LEFT OUTER JOIN FETCH scr.securityProduct");
		sbQuery.append("	INNER JOIN FETCH scr.splitCouponOperations sco");
		sbQuery.append("	INNER JOIN FETCH sco.programInterestCoupon pic");
		sbQuery.append("    LEFT OUTER JOIN FETCH pic.splitSecurities");
		
		if(BooleanType.NO.getCode().equals(searchDetachmentCouponsTO.getSearchType())){
			sbQuery.append("	WHERE scr.idSplitOperationPk = :idSplitOperationPk");
//			if(blParticipant)
//				sbQuery.append("	AND scr.participant.idParticipantPk = :idParticipantPk");
		}else{
			sbQuery.append("	WHERE TRUNC(scr.registryDate) BETWEEN :initialDate AND :endDate");
//			sbQuery.append("	AND scr.participant.idParticipantPk = :idParticipantPk");
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getHolderSelected().getIdHolderPk()))
				sbQuery.append("	AND scr.holder.idHolderPk = :idHolderPk");			
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk()))
				sbQuery.append("	AND scr.holderAccount.idHolderAccountPk = :idHolderAccountPk");
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk()))
				sbQuery.append("	AND scr.security.idSecurityCodePk = :idSecurityCodePk");
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getStateSelected()))
				sbQuery.append("	AND scr.requestState = :state");
		}
		if(searchDetachmentCouponsTO.getParticipantSelected()!=null){
			sbQuery.append("	AND scr.participant.idParticipantPk = :idParticipantPk");
		}
		sbQuery.append("	ORDER BY scr.idSplitOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		if(BooleanType.NO.getCode().equals(searchDetachmentCouponsTO.getSearchType())){
			query.setParameter("idSplitOperationPk", searchDetachmentCouponsTO.getNumberRequest());
//			if(blParticipant)
//				query.setParameter("idParticipantPk", searchDetachmentCouponsTO.getParticipantSelected());
		}else{
			query.setParameter("initialDate", searchDetachmentCouponsTO.getInitialDate());
			query.setParameter("endDate", searchDetachmentCouponsTO.getEndDate());
//			query.setParameter("idParticipantPk", searchDetachmentCouponsTO.getParticipantSelected());
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getHolderSelected().getIdHolderPk()))
				query.setParameter("idHolderPk", searchDetachmentCouponsTO.getHolderSelected().getIdHolderPk());			
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk()))
				query.setParameter("idHolderAccountPk", searchDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk()))
				query.setParameter("idSecurityCodePk", searchDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
			if(Validations.validateIsNotNullAndNotEmpty(searchDetachmentCouponsTO.getStateSelected()))
				query.setParameter("state", searchDetachmentCouponsTO.getStateSelected());
		}
		if(searchDetachmentCouponsTO.getParticipantSelected()!=null){
			query.setParameter("idParticipantPk", searchDetachmentCouponsTO.getParticipantSelected());
		}
		return (List<SplitCouponRequest>)query.getResultList();	
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @return the holder account balance
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> getHolderAccountBalance(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hab FROM HolderAccountBalance hab");
		sbQuery.append("	WHERE hab.id.idParticipantPk = :idParticipantPk");
		sbQuery.append("	AND hab.id.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND hab.id.idSecurityCodePk = :idSecurityCodePk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantPk", registerDetachmentCouponsTO.getParticipantSelected());
		query.setParameter("idHolderAccountPk", registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
		query.setParameter("idSecurityCodePk", registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
		return (List<HolderAccountBalance>)query.getResultList();
	}

	/**
	 * Gets the program interest coupon.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @return the program interest coupon
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramInterestCoupon> getProgramInterestCoupon(RegisterDetachmentCouponsTO registerDetachmentCouponsTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pic FROM ProgramInterestCoupon pic");
		sbQuery.append("	LEFT JOIN FETCH pic.splitSecurities");
		sbQuery.append("	WHERE pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pic.stateProgramInterest IN (:states)");
		sbQuery.append("	ORDER BY pic.couponNumber");
		Query query = em.createQuery(sbQuery.toString());
		List<Integer> lstStates = new ArrayList<Integer>();
		lstStates.add(ProgramScheduleStateType.PENDING.getCode());
		lstStates.add(ProgramScheduleStateType.EXPIRED.getCode());
		//lstStates.add(ProgramScheduleSituationType.ISSUER_PAID.getCode());
		query.setParameter("states", lstStates);
		query.setParameter("idSecurityCodePk", registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
		return (List<ProgramInterestCoupon>)query.getResultList();
	}
	
	/**
	 * Gets the program interest coupon from parent.
	 *
	 * @param securityCodePk the security code pk
	 * @return the program interest coupon from parent
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramInterestCoupon> getProgramInterestCouponFromParent(String securityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT DISTINCT interest.idProgramInterestPk,				");
		sbQuery.append("		interest.couponNumber, interest.experitationDate, 		");
		sbQuery.append("		interest.paymentDate, interest.stateProgramInterest, 	");
		sbQuery.append("		interest.registryDate 									");
		sbQuery.append("	FROM Security	sec											");
		sbQuery.append("	INNER JOIN sec.security secParent							");
		sbQuery.append("	INNER JOIN secParent.interestPaymentSchedule paySched		");
		sbQuery.append("	INNER JOIN paySched.programInterestCoupons interest			");
		sbQuery.append("	WHERE sec.idSecurityCodePk = :idSecurityCodePk				");
		sbQuery.append("	AND interest.stateProgramInterest IN (:states)				");
		sbQuery.append("	ORDER BY interest.couponNumber								");
		Query query = em.createQuery(sbQuery.toString());
		List<Integer> lstStates = new ArrayList<Integer>();
		lstStates.add(ProgramScheduleStateType.PENDING.getCode());
		lstStates.add(ProgramScheduleStateType.EXPIRED.getCode());
		//lstStates.add(ProgramScheduleSituationType.ISSUER_PAID.getCode());
		query.setParameter("states", lstStates);
		query.setParameter("idSecurityCodePk", securityCodePk);
		
		List<ProgramInterestCoupon> couponInterestList  = new ArrayList<>();
		List<Object[]> objetData = query.getResultList();
		for(Object[] data: objetData){
			ProgramInterestCoupon interestCoupon = new ProgramInterestCoupon();
			interestCoupon.setIdProgramInterestPk((Long) data[0]);
			interestCoupon.setCouponNumber((Integer) data[1]);
			interestCoupon.setExperitationDate((Date) data[2]);
			interestCoupon.setPaymentDate((Date) data[3]);
			interestCoupon.setStateProgramInterest((Integer) data[4]);
			interestCoupon.setRegistryDate((Date) data[5]);
			couponInterestList.add(interestCoupon);
		}
		return couponInterestList;
	}

	/**
	 * Gets the detachment coupon request.
	 *
	 * @param idSplitOperationPk the id split operation pk
	 * @return the detachment coupon request
	 * @throws ServiceException the service exception
	 */
	public SplitCouponRequest getDetachmentCouponRequest(Long idSplitOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT scr FROM SplitCouponRequest scr");
		sbQuery.append("	INNER JOIN FETCH scr.holder");
		sbQuery.append("	INNER JOIN FETCH scr.holderAccount");
		sbQuery.append("	INNER JOIN FETCH scr.participant");
		sbQuery.append("	INNER JOIN FETCH scr.security se");
		sbQuery.append("	INNER JOIN FETCH se.issuance");
		sbQuery.append("	INNER JOIN FETCH se.issuer");
		sbQuery.append("	INNER JOIN FETCH scr.splitCouponOperations sco");
		sbQuery.append("	INNER JOIN FETCH sco.programInterestCoupon pic");
		sbQuery.append("	INNER JOIN FETCH pic.interestPaymentSchedule");
		sbQuery.append("	WHERE scr.idSplitOperationPk = :idSplitOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSplitOperationPk", idSplitOperationPk);
		return (SplitCouponRequest) query.getSingleResult();
	}

	/**
	 * Gets the available balance.
	 *
	 * @param idParticipant the id participant
	 * @param holderAccountPk the holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @return the available balance
	 * @throws ServiceException the service exception
	 */
	public HolderAccountBalance getHolderAccountBalance(Long idParticipant,Long holderAccountPk,String idSecurityCodePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT hab.availableBalance,hab.totalBalance,hab.reportingBalance,hab.reportedBalance FROM HolderAccountBalance hab");
			sbQuery.append("	WHERE hab.id.idParticipantPk = :idParticipantPk");
			sbQuery.append("	AND hab.id.idHolderAccountPk = :idHolderAccountPk");
			sbQuery.append("	AND hab.id.idSecurityCodePk = :idSecurityCodePk");
			sbQuery.append("	ORDER BY hab.totalBalance");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idParticipantPk", idParticipant);
			query.setParameter("idHolderAccountPk", holderAccountPk);
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			return (HolderAccountBalance)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}

	/**
	 * Gets the pending coupons.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the pending coupons
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> getPendingCoupons(String idSecurityCodePk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pic.couponNumber FROM ProgramInterestCoupon pic");
		sbQuery.append("	WHERE pic.interestPaymentSchedule.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pic.stateProgramInterest = :state");
		sbQuery.append("	ORDER BY pic.couponNumber");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		return (List<Integer>)query.getResultList();
	}

	/**
	 * Find subproduct code.
	 *
	 * @param couponsRelated the coupons related
	 * @return the subproduct code
	 */
	public SubproductCode findSubproductCode(String couponsRelated) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT sc FROM SubproductCode sc");
			sbQuery.append("	WHERE sc.couponsRelated = :couponsRelated");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("couponsRelated", couponsRelated);
			return (SubproductCode)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Find last subproduct code.
	 *
	 * @return the subproduct code
	 */
	public SubproductCode findLastSubproductCode() {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT sc FROM SubproductCode sc");
			sbQuery.append("	WHERE rownum < 2");
			sbQuery.append("	ORDER BY sc.idSubprodcutCodePk DESC");
			Query query = em.createQuery(sbQuery.toString());
			return (SubproductCode)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Find amortization payment schedule.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the amortization payment schedule
	 */
	public AmortizationPaymentSchedule findAmortizationPaymentSchedule(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aps FROM AmortizationPaymentSchedule aps");
		sbQuery.append("	INNER JOIN FETCH aps.programAmortizationCoupons pac");
		sbQuery.append("	WHERE aps.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pac.stateProgramAmortization = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		return (AmortizationPaymentSchedule) query.getSingleResult();
	}

	/**
	 * Find interest payment schedule.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the interest payment schedule
	 */
	public InterestPaymentSchedule findInterestPaymentSchedule(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ips FROM InterestPaymentSchedule ips");
		sbQuery.append("	INNER JOIN FETCH ips.programInterestCoupons pic");
		sbQuery.append("	WHERE ips.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pic.stateProgramInterest = :state");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		return (InterestPaymentSchedule) query.getSingleResult();
	}

	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk = :idHolderAccountPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			return (HolderAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Find holder accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccounts(Long idHolderPk, Long idParticipantPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct had.holderAccount.idHolderAccountPk ");
		sbQuery.append("	FROM HolderAccountDetail had ");
		sbQuery.append("	WHERE had.holder.idHolderPk = :idHolderPk ");
		sbQuery.append("	AND had.holderAccount.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append("	AND had.holderAccount.stateAccount = :state ");
		sbQuery.append("	AND had.holderAccount.accountGroup = :accountGroup ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("state", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
		List<Long> lstIdHolderAccountPk = (List<Long>)query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdHolderAccountPk)){
			sbQuery = new StringBuilder();
			sbQuery.append("	SELECT distinct ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	WHERE ha.idHolderAccountPk in (:lstIdHolderAccountPk)");
			query = em.createQuery(sbQuery.toString());
			query.setParameter("lstIdHolderAccountPk", lstIdHolderAccountPk);
			return (List<HolderAccount>)query.getResultList();
		}else
			return null;
	}
	
	/**
	 * Find market fact balance.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idHolderAccountPk the id holder account pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> findMarketFactBalance(String idSecurityCodePk, Long idHolderAccountPk){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hmfb");
		sbQuery.append("	FROM HolderMarketFactBalance hmfb");
		sbQuery.append("	WHERE hmfb.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hmfb.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	ORDER BY hmfb.marketDate, hmfb.marketRate ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		return (List<HolderMarketFactBalance>)query.getResultList();
	}
	
	/**
	 * Find market fact balance.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public Security findSecurity(String idSecurityCodePk){
		return find(Security.class, idSecurityCodePk);
	}

	/**
	 * Find split coupon market fact.
	 *
	 * @param idSplitOperationPk the id split operation pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<SplitCouponMarketFact> findSplitCouponMarketFact(Long idSplitOperationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT smf");
		sbQuery.append("	FROM SplitCouponMarketFact smf");
		sbQuery.append("	WHERE smf.splitCouponRequest.idSplitOperationPk = :idSplitOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSplitOperationPk", idSplitOperationPk);
		return (List<SplitCouponMarketFact>)query.getResultList();
	}
	
	/**
	 * Gets the request by revert.
	 *
	 * @param searchDetachmentCouponsTO the search detachment coupons to
	 * @param lstStates the lst states
	 * @return the request by revert
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<SplitCouponRequest> getRequestByRevert(SearchDetachmentCouponsTO searchDetachmentCouponsTO, List<Integer> lstStates) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT scr FROM SplitCouponRequest scr		");
		sbQuery.append("	WHERE TRUNC(scr.registryDate) = :endDate"	);
		sbQuery.append("	AND scr.requestState IN :lststate");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("endDate", searchDetachmentCouponsTO.getEndDate());
		query.setParameter("lststate", lstStates);
		return (List<SplitCouponRequest>)query.getResultList();	
	}

	/**
	 * Find operations.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @param blCashPart the bl cash part
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Long> findOperations(RegisterDetachmentCouponsTO registerDetachmentCouponsTO, boolean blCashPart) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct hao.mechanismOperation.idMechanismOperationPk FROM HolderAccountOperation hao, SettlementAccountOperation sao");
		sbQuery.append(" 	WHERE sao.holderAccountOperation.idHolderAccountOperationPk = hao.idHolderAccountOperationPk ");
		sbQuery.append("	AND hao.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND hao.mechanismOperation.securities.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND hao.holderAccountState = :holderAccountState");
		sbQuery.append("	AND sao.settlementOperation.settlementDate = :settlementDate ");
		sbQuery.append("	AND hao.operationPart = sao.settlementOperation.operationPart ");
		if(blCashPart){			
			sbQuery.append("	AND hao.role = :buyer");
			sbQuery.append("	AND hao.operationPart = :cashPart");			
		}else{		
			sbQuery.append("	AND hao.role = :seller");
			sbQuery.append("	AND hao.operationPart = :termPart");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());
		query.setParameter("idSecurityCodePk", registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
		query.setParameter("holderAccountState", HolderAccountOperationStateType.CONFIRMED.getCode());
		if(blCashPart){
			query.setParameter("buyer", ParticipantRoleType.BUY.getCode());
			query.setParameter("cashPart", OperationPartType.CASH_PART.getCode());			
		}else{
			query.setParameter("seller", ParticipantRoleType.SELL.getCode());
			query.setParameter("termPart", OperationPartType.TERM_PART.getCode());
		}
		query.setParameter("settlementDate", CommonsUtilities.currentDate());
		return (List<Long>)query.getResultList();
	}

	/**
	 * Gets the mechanism operations.
	 *
	 * @param lstOperations the lst operations
	 * @return the mechanism operations
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperation> getMechanismOperations(List<Long> lstOperations) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT mo FROM MechanismOperation mo");
		sbQuery.append("	INNER JOIN FETCH mo.mechanismOperationMarketFacts");
		sbQuery.append("	WHERE mo.idMechanismOperationPk in (:operations)");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("operations", lstOperations);
		return (List<MechanismOperation>)query.getResultList();
	}

	/**
	 * Exist detachment.
	 *
	 * @param registerDetachmentCouponsTO the register detachment coupons to
	 * @param idProgramInterestPk the id program interest pk
	 * @return true, if successful
	 */
	public boolean existDetachment(RegisterDetachmentCouponsTO registerDetachmentCouponsTO, Long idProgramInterestPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT COUNT(spd) FROM SplitCouponOperation spd");
		sbQuery.append("	WHERE spd.programInterestCoupon.idProgramInterestPk = :idProgramInterestPk");
		sbQuery.append("	AND spd.splitCouponRequest.holderAccount.idHolderAccountPk = :idHolderAccountPk");
		sbQuery.append("	AND spd.splitCouponRequest.security.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND spd.splitCouponRequest.requestState IN (:states)");
		Query query = em.createQuery(sbQuery.toString());
		List<Integer> lstStates = new ArrayList<Integer>();
		lstStates.add(SplitCouponStateType.REGISTERED.getCode());
		lstStates.add(SplitCouponStateType.APPROVED.getCode());
		lstStates.add(SplitCouponStateType.CONFIRMED.getCode());
		query.setParameter("states", lstStates);
		query.setParameter("idProgramInterestPk", idProgramInterestPk);
		query.setParameter("idSecurityCodePk", registerDetachmentCouponsTO.getSecuritySelected().getIdSecurityCodePk());
		query.setParameter("idHolderAccountPk", registerDetachmentCouponsTO.getHolderAccountSelected().getIdHolderAccountPk());			
		Integer result = new Integer(query.getSingleResult().toString());
		if(result > 0)
			return true;
		return false;
	}
	
	/**
	 * Gets the market fact detachment.
	 *
	 * @param custodyOperationPk the custody operation pk
	 * @param todate the todate
	 * @return the market fact detachment
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactDetailHelpTO> getMarketFactDetachment(Long custodyOperationPk, boolean todate) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select SCM.marketDate, " + 	//0
				   	   " SCM.marketRate, "+				//1
					   " SCM.operationQuantity ");		//2
					  if(todate){
		sbQuery.append(", HMB.totalBalance, "+			//3
					   " HMB.availableBalance ");		//4
					  }
		sbQuery.append(" From  SplitCouponMarketFact SCM JOIN SCM.splitCouponRequest SCO "); 
					 if(todate){
		sbQuery.append(",  	   HolderMarketFactBalance HMB ");
					 }
		sbQuery.append(" Where SCO.idSplitOperationPk = :idSplitOperationPk ");
					 if(todate){
		sbQuery.append(" and HMB.holderAccount.idHolderAccountPk = SCO.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and HMB.participant.idParticipantPk = SCO.participant.idParticipantPk ");
		sbQuery.append(" and HMB.security.idSecurityCodePk = SCO.security.idSecurityCodePk ");
		sbQuery.append(" and HMB.marketDate = SCM.marketDate ");
		sbQuery.append(" and nvl(HMB.marketRate,1) = decode( nvl(HMB.marketRate,1), nvl(SCM.marketRate,1), nvl(SCM.marketRate,1), nvl(HMB.marketRate,1) ) ");
		}
					 
		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("idSplitOperationPk", custodyOperationPk);
		

		List<MarketFactDetailHelpTO> lstMarketFactDetailHelpTO = new ArrayList<MarketFactDetailHelpTO>();
		MarketFactDetailHelpTO marketFactDetailHelpTO = null;
		for (int i = 0; i < query.getResultList().size(); i++) {
			
			Object obj[] = (Object[])query.getResultList().get(i);
			
			marketFactDetailHelpTO = new MarketFactDetailHelpTO();
			
			if(Validations.validateIsNotNull(obj[0])){
				marketFactDetailHelpTO.setMarketDate((Date)obj[0]);
			}
			
			if(Validations.validateIsNotNull(obj[1])){
				marketFactDetailHelpTO.setMarketRate(new BigDecimal((obj[1]).toString()));
			}
			
			if(Validations.validateIsNotNull(obj[2])){
				marketFactDetailHelpTO.setEnteredBalance(new BigDecimal((obj[2]).toString()));
			}

			if(todate){
				if(Validations.validateIsNotNull(obj[3])){
					marketFactDetailHelpTO.setTotalBalance(new BigDecimal((obj[3]).toString()));
				}
				
				if(Validations.validateIsNotNull(obj[4])){
					marketFactDetailHelpTO.setAvailableBalance(new BigDecimal((obj[4]).toString()));
				}
			}
			marketFactDetailHelpTO.setTodate(todate);
			
			lstMarketFactDetailHelpTO.add(marketFactDetailHelpTO);
		}
		return lstMarketFactDetailHelpTO;
	}
}
