package com.pradera.custody.utils.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import com.pradera.commons.utils.JSFUtilities;

/**
 * 
 * The Class ImageStreamerBean
 * @Project : PraderaSecurity
 * @author : PraderaTechnologies.
 * @Creation_Date : 
 * @version 1.1
 */

@Named
@RequestScoped
public class ImageStreamerBean implements Serializable{  

	private static final long serialVersionUID = 1L;

	private StreamedContent fileContent;
	
	private StreamedContent fileSignatureImage;
	
	/**
	 * @return fileContent
	 */
	public StreamedContent getFileContent() {
		try {
		    FacesContext context = FacesContext.getCurrentInstance();
		    if (context.getRenderResponse()) {
		        return new DefaultStreamedContent();
		    }
			Integer rowIndex=Integer.parseInt(JSFUtilities.getRequestParameterMap("rowIndex"));
			if(rowIndex.intValue()>=0){
				return new DefaultStreamedContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileContent;
	}
	
	/**
	 * @param fileContent
	 */
	public void setFileContent(StreamedContent fileContent) {
		this.fileContent = fileContent;
	}
	
	/**
	 * @return fileSignatureImage
	 */
	public StreamedContent getFileSignatureImage() {
		if(JSFUtilities.getSessionMap("sessStreamedImage")!=null){
			byte[] systemImage=(byte[])JSFUtilities.getSessionMap("sessStreamedImage");
			InputStream input = new ByteArrayInputStream(systemImage);
			fileSignatureImage=new DefaultStreamedContent(input, "image/jpeg"); 
		}
		return fileSignatureImage;
	}

	/**
	 * @param fileSignatureImage
	 */
	public void setFileSignatureImage(StreamedContent fileSignatureImage) {
		this.fileSignatureImage = fileSignatureImage;
	}
	
}
