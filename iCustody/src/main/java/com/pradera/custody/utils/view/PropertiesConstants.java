package com.pradera.custody.utils.view;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

// TODO: Auto-generated Javadoc
/**
 * The Class PropertiesConstants.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class PropertiesConstants {

	/**
	 * Instantiates a new properties constants.
	 */
	public PropertiesConstants() {
		super();
	}
	
	public static final String VALIDATION_MAXFINALHOUR_ALLOWED="validation.maxFinalHour.allowed";
	
	public static final String VALIDATION_MININITIALHOUR_ALLOWED="validation.minInitialHour.allowed";
	
	public static final String VALIDATION_REQUIRED_INITIALHOUR_REQUIRED_LESS_FINALHOUR="validation.required.initialHour.required.less.finalHour";
	
	public static final String MSG_SUCCESFUL_EXPIRATION_CHANNEL_OPENING="msg.succesful.expiration.channel.opening";
	
	public static final String LBL_MSG_CONFIRM_EXPIRATION_CHANNEL_OPENING="lbl.msg.confirm.expiration.channel.opening";
	
	public static final String MSG_ERROR_CHANNELOPENING_REQUIRED_STATECONFIRMED="msg.error.channelOpening.required.stateConfirmed";
	
	public static final String MSG_SUCCESFUL_REJECT_CHANNEL_OPENING="msg.succesful.reject.channel.opening";
	
	public static final String LBL_MSG_CONFIRM_REJECT_CHANNEL_OPENING="lbl.msg.confirm.reject.channel.opening";
	
	public static final String MSG_SUCCESFUL_CONFIRM_CHANNEL_OPENING="msg.succesful.confirm.channel.opening";
	
	public static final String LBL_MSG_CONFIRM_CONFIRM_CHANNEL_OPENING="lbl.msg.confirm.confirm.channel.opening";
	
	public static final String MSG_ERROR_CHANNELOPENING_REQUIRED_STATEAPPROVED="msg.error.channelOpening.required.stateApproved";
	
	public static final String MSG_SUCCESFUL_ANULATE_CHANNEL_OPENING="msg.succesful.anulate.channel.opening";
	
	public static final String LBL_MSG_CONFIRM_ANULATE_CHANNEL_OPENING="lbl.msg.confirm.anulate.channel.opening";
	
	public static final String MSG_SUCCESFUL_APPROVED_CHANNEL_OPENING="msg.succesful.approved.channel.opening";
	
	public static final String LBL_MSG_CONFIRM_APPROVED_CHANNEL_OPENING="lbl.msg.confirm.approved.channel.opening";
	
	public static final String MSG_ERROR_CHANNELOPENING_REQUIRED_STATEREGISTERED="msg.error.channelOpening.required.stateRegistered";
	
	public static final String MSG_ERROR_CHANNELOPENING_REQUIRED_SELECTED="msg.error.channelOpening.required.selected";
	
	public static final String MESSAGES_SUCCESFUL="messages.succesful";
	
	public static final String MESSAGES_CONFIRM="messages.confirm";
	
	public static final String LBL_MSG_CONFIRM_REGITER_CHANNEL_OPENING="lbl.msg.confirm.regiter.channel.opening";
	
	public static final String MSG_SUCCESFUL_REGISTER_CHANNEL_OPENING="msg.succesful.register.channel.opening";
	
	/** The Constant MSG_VALIDATION_REQUIRED_SECURITIES_BEFORE_CONFIRM. */
	public static final String MSG_VALIDATION_REQUIRED_SECURITIES_BEFORE_CONFIRM="msg.validation.required.securities.before.confirm";
	
	/** The Constant MSG_VALIDATION_REQUIRED_SECURITY_BEFORE_CONFIRM. */
	public static final String MSG_VALIDATION_REQUIRED_SECURITY_BEFORE_CONFIRM="msg.validation.required.security.before.confirm";
	
	/** The Constant VALIDATE_SECURITYCLASS_AND_SECURITYCODE. */
	public static final String VALIDATE_SECURITYCLASS_AND_SECURITYCODE="msg.validate.securityclass.and.codesecurity";
	
	/** The Constant EXECUTED_PROCESS_SUCCESFUL. */
	public static final String EXECUTED_PROCESS_SUCCESFUL= "executed.process.succesful";
	
	/** The Constant REVERSAL_ACTION_SEARCHING. */
	public static final String REVERSAL_ACTION_SEARCHING = "reversals.action.searchRequest";
	
	/** The Constant REVERSAL_ACTION_REGISTER. */
	public static final String REVERSAL_ACTION_REGISTER = "reversals.action.reversals";
	
	/** The Constant MESSAGE_TO. */
	public static final String MESSAGE_TO="lbl.to.message";
	
	/** The Constant NO_ACTION_VALID. */
	public static final String NO_ACTION_VALID = "commons.alert.noActionValid";
	
	/** The Constant NO_PARTICIPANT_TARGET. */
	public static final String NO_PARTICIPANT_TARGET = "transferVD.alert.noParticipantTarget";
	
	/** The Constant NO_PARTICIPANT_AND_ACCOUNT. */
	public static final String NO_PARTICIPANT_AND_ACCOUNT = "transferVD.alert.noParticipantAndAccount";
	
	/** The Constant EQUALS_PARTICIPANT. */
	public static final String EQUALS_PARTICIPANT = "transferVD.alert.equalsParticipant";
	
	/** The Constant BACK_TO_SCREEN. */
	public static final String BACK_TO_SCREEN = "commons.label.backToScreen";
	
	/** The Constant MESSAGE_DATA_REQUIRED. */
	public static final String MESSAGE_DATA_REQUIRED = "message.data.required";
	
    /** The Constant WARNING_OPERATION_CONFIRM_INVALID. */
    public static final String  WARNING_OPERATION_CONFIRM_INVALID = "warning.operation.confirm.invalid";
	
	/** The Constant WARNING_OPERATION_REJECT_INVALID. */
	public static final String  WARNING_OPERATION_REJECT_INVALID = "warning.operation.reject.invalid";
	
	/** The Constant WARNING_OPERATION_ANNULAR_INVALID. */
	public static final String  WARNING_OPERATION_ANNULAR_INVALID = "warning.operation.annular.invalid";
	
	/** The Constant WARNING_OPERATION_ANNULAR_INVALID. */
	public static final String  WARNING_OPERATION_NOT_REGISTERED = "warning.operation.not.registered";
	
	/** The Constant WARNING_OPERATION_DESACTIVE_INVALID. */
	public static final String  WARNING_OPERATION_DESACTIVE_INVALID = "warning.operation.desactive.invalid";
	
	/** The Constant WARNING_OPERATION_ACTIVE_INVALID. */
	public static final String  WARNING_OPERATION_ACTIVE_INVALID = "warning.operation.active.invalid";
	
	/** The Constant LBL_CUSTODY_ALLOWED_FILE_SIZE. */
	public static final String LBL_CUSTODY_ALLOWED_FILE_SIZE = "tamanio.file";
	
	/** The Constant LBL_CUSTODY_ALLOWED_PER_ONE_FILE_SIZE. */
	public static final String LBL_CUSTODY_ALLOWED_PER_ONE_FILE_SIZE = "tamanio.per.one.file";
	
	/** The Constant ERROR_CUSTODY_MAX_FILE_SIZE. */
	public static final String ERROR_CUSTODY_MAX_FILE_SIZE = "tamanio.max.file";
	
	/** The Constant ERROR_CUSTODY_VALIDATION_NOTPDF. */
	public static final String ERROR_CUSTODY_VALIDATION_NOTPDF = "file.validation.notpdf";
	
	/** The Constant MSG_CUSTODY_FILEUPLOAD_OK. */
	public static final String MSG_CUSTODY_FILEUPLOAD_OK = "message.custody.fileupload.ok";
	
	/** The Constant ERROR_CUSTODY_OPERATION_REJECT_MOTIVE. */
	public static final String ERROR_CUSTODY_OPERATION_REJECT_MOTIVE = "error.custody.operation.reject.motive";
	
	/** The Constant ERROR_CUSTODY_OPERATION_REJECT_OTHER_MOTIVE. */
	public static final String ERROR_CUSTODY_OPERATION_REJECT_OTHER_MOTIVE = "error.custody.operation.reject.other.motive";	
	
	/** The Constant PROPERTYFILE. */
	public static final String PROPERTYFILE = "Messages";
	
	/** The Constant INVALID_ACCOUNT. */
	public static final String INVALID_ACCOUNT = "tdv.msgs.Invalidaccount";
	
	/** The Constant ERROR_ON_ACCOUNT_AND_PARTICIPANT. */
	public static final String ERROR_ON_ACCOUNT_AND_PARTICIPANT = "error.accreditation.certificate.holder_participant";
	
	/** The Constant ERROR_ON_HOLDER_AND_ENTITY_BLOCK. */
	public static final String ERROR_ON_HOLDER_AND_ENTITY_BLOCK="error.accreditation.certificate.holder.entity.block";
	/** The Constant CONFIRM_ACCREDITATION_SAVE. */
	public static final String CONFIRM_ACCREDITATION_SAVE ="confirm.accreditation.certificate.save_confirm";
	
	/** The Constant ERROR_FINDING_HOLDER. */
	public static final String ERROR_FINDING_HOLDER = "error.accreditation.certificate.finding_holder";
	
	/** The Constant NOTHING_TO_ACCREDITATE. */
	public static final String NOTHING_TO_ACCREDITATE = "error.accreditation.certificate.nothing_to_accreditate";
	
	/** The Constant ACCREDITATION_SEARCH_BEFORE_SAVE. */
	public static final String ACCREDITATION_SEARCH_BEFORE_SAVE = "error.accreditation.certificate.search.first";
	/** The Constant ACCREDITATION_MESSAGE_ERROR. */
	public static final String ACCREDITATION_MESSAGE_ERROR = "error.accreditation.certificate.save_message_error";
	
	/**  TRANSFERENCIA DE VALORES. */
	public static final String TDV_MSG_HOLDER_NOT_REGISTERED = "tdv.msg.account.number.not.registered";
	
	/** The Constant TDV_MSG_HOLDER_NOT_EXISTED. */
	public static final String TDV_MSG_HOLDER_NOT_EXISTED= "tdv.msg.account.not.existed";
	
	
	
	/** The Constant TDV_MSG_HOLDER_EQUALS_RNT. */
	public static final String TDV_MSG_HOLDER_EQUALS_RNT = "tdv.msg.holder.equals.rnt";
	
	/** The Constant TDV_MSG_ACCOUNT_NOT_REGISTERED. */
	public static final String TDV_MSG_ACCOUNT_NOT_REGISTERED = "tdv.msg.account.not.registered";
	
	/** The Constant TDV_MSG_SRC_ACCOUNT_EQUALS_TARGET. */
	public static final String TDV_MSG_SRC_ACCOUNT_EQUALS_TARGET= "tdv.msg.account.equals.source.target";
	
	/** The Constant TDV_MSG_SECURITY_INVALID_INSTRUMENT_TYPE. */
	public static final String TDV_MSG_SECURITY_INVALID_INSTRUMENT_TYPE = "tdv.msg.security.instrument.type.invalid";
	
	/** The Constant TDV_MSG_SECURITY_NO_BALANCES. */
	public static final String TDV_MSG_SECURITY_NO_BALANCES = "tdv.msg.security.no.balances";
	
	/** The Constant AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT. */
	public static final String AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT = "tdv.msg.availablebalanceMinortransferamount";
	
	/** The Constant SECURITY_DONT_EXIST. */
	public static final String SECURITY_DONT_EXIST = "tdv.msg.securitydontexist";
	
	/** The Constant INSERT_THE_NEW_ORIGIN_ACCOUNT. */
	public static final String INSERT_THE_NEW_ORIGIN_ACCOUNT = "tdv.msg.insertoriginaccount";
	
	/** The Constant SELECT_ORIGIN_PARTICIPANT. */
	public static final String SELECT_ORIGIN_PARTICIPANT = "tdv.msg.selectOriginParticipant";
	
	/** The Constant SELECT_TARGET_PARTICIPANT. */
	public static final String SELECT_TARGET_PARTICIPANT = "tdv.msg.selectTargetParticipant";
	
	/** The Constant NOT_FIND_DATA. */
	public static final String NOT_FIND_DATA = "tdv.msg.notfinddata";
	
	/** The Constant NEED_MORE_DATA. */
	public static final String NEED_MORE_DATA = "tdv.msg.neddmoredata";
	
	/** The Constant NOT_DISTINCT_ACCOUNT. */
	public static final String NOT_DISTINCT_ACCOUNT = "tdv.msg.notdistinctaccount";
	
	/** The Constant NOT_SELECT_TRANSFER_TYPE. */
	public static final String NOT_SELECT_TRANSFER_TYPE = "tdv.msg.notselecttransfertype";
	
	/** The Constant REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE. */
	public static final String REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE = "tdv.msg.registercompletetransferesecurityavailable";
	
	/** The Constant REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE_PLURAL. */
	public static final String REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE_PLURAL = "tdv.msg.registercompletetransferesecurityavailable.plural";
	
	/** The Constant REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE_COMPLEMENT. */
	public static final String REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE_COMPLEMENT = "tdv.msg.registercompletetransferesecurityavailable.complement";
	
	/** The Constant REGISTER_COMPLETE_TRANSFER_SECURITY_BLOCK. */
	public static final String REGISTER_COMPLETE_TRANSFER_SECURITY_BLOCK = "tdv.msg.registercompletetransferesecurityblock";
	
	/** The Constant REGISTER_COMPLETE_TRANSFER_SECURITY_BLOCK_COMPLEMENT. */
	public static final String REGISTER_COMPLETE_TRANSFER_SECURITY_BLOCK_COMPLEMENT = "tdv.msg.registercompletetransferesecurityblock.complement";
	
	/** The Constant NOT_REGISTER_TRANSFER_SECURITY_AVAILABLE. */
	public static final String NOT_REGISTER_TRANSFER_SECURITY_AVAILABLE = "tdv.msg.notregistertransferesecurityavailable";
	
	/** The Constant INVALID_AMOUNT. */
	public static final String INVALID_AMOUNT = "tdv.msg.invalidamount";
	
	/** The Constant VALIDATION_ACCOUNT_STATE. */
	public static final String VALIDATION_ACCOUNT_STATE = "tdv.msg.validationaccountstate";
	
	/** The Constant VALIDATIONS_ACCOUNTS_STATES. */
	public static final String VALIDATIONS_ACCOUNTS_STATES = "tdv.msg.validationsaccountsstates";
	
	/**  fin TRANSFERENCIA DE VALORES??. */
	
	public final static String ERROR_NOT_ACCOUNT_SELECTED="error.accreditation.certificate.account.not.selected";

	public final static String MSG_VALIDATION_RECORDS_FOUND_CERTIFICATE_ACCREDITATION="msg.validation.records.found.certificate.accreditation";
	
	/** The Constant SAVING_ACCREDITATION_CERTIFICATE_SUCCESS_MESSAGE. */
	public final static String SAVING_ACCREDITATION_CERTIFICATE_SUCCESS_MESSAGE="saved.accreditation.certificate.message";
	
	/** The Constant REPEAT_SECURITY_ACCREDITATION_CERTIFICATE_MESSAGE. */
	public final static String REPEAT_SECURITY_ACCREDITATION_CERTIFICATE_MESSAGE="repeat.security.accreditation.certificate.message";

	/** The Constant BALANCE_AVAILABLE_ACCREDITATION_CERTIFICATE_MESSAGE. */
	public final static String BALANCE_AVAILABLE_ACCREDITATION_CERTIFICATE_MESSAGE="lbl.accreditation.certificate.available_balance";
	
	/** The Constant BLOCK_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE. */
	public final static String BLOCK_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE="lbl.accreditation.certificate.block_balance";
	
	/** The Constant REPORTER_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE. */
	public final static String REPORTER_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE="lbl.accreditation.certificate.reporter.balance";
	
	/** The Constant INVALID_REQUEST_REPORTED_ACCREDITATION_CERTIFICATE_MESSAGE. */
	public final static String INVALID_REQUEST_REPORTED_ACCREDITATION_CERTIFICATE_MESSAGE="invalid.request.reported.accreditation.certificate.message";
	
	/** The Constant INVALID_REQUEST_BLOCKED_ACCREDITATION_CERTIFICATE_MESSAGE. */
	public final static String INVALID_REQUEST_BLOCKED_ACCREDITATION_CERTIFICATE_MESSAGE="invalid.request.blocked.accreditation.certificate.message";
	
	/** The Constant ERROR_NOT_SUSTENTION_SELECTED. */
	public final static String ERROR_NOT_SUSTENTION_SELECTED="error.accreditation.certificate.sustention.not.selected";
	/** The Constant FUPL_ERROR_INVALID_FILE_TYPE. */
	public final static String FUPL_ERROR_INVALID_FILE_TYPE="fupl.error.invalid.file.type";
	
	/** The Constant EXP_DATE_LESS_THAN_REG_DATE. */
	public final static String EXP_DATE_LESS_THAN_REG_DATE="error.accreditation.certificate.expirationDate.less.registerDate";
	
	/** The Constant EMISSION_DATE_LESS_THAN_REG_DATE. */
	public final static String EMISSION_DATE_LESS_THAN_REG_DATE="error.accreditation.certificate.emissionDate.less.registerDate";
	
	/** The Constant UNION_DATE_LESS_THAN_CURRENT_DATE. */
	public final static String UNION_DATE_LESS_THAN_CURRENT_DATE="message.participant.union.date.union";
	/** The Constant FUPL_SUCCESS_UPLOAD. */
	public final static String FUPL_SUCCESS_UPLOAD="fupl.success.upload";
	
	/** The Constant ERROR_HOLDER_NOT_REGISTERED_STATE. */
	public final static String ERROR_HOLDER_NOT_REGISTERED_STATE ="error.accreditation.certificate.holder.not.registered";
	
	/** The Constant ERROR_HOLDER_ACCOUNT_NOT_HOLDER_ACCOUNT_ISSUER. */
	public final static String ERROR_HOLDER_ACCOUNT_NOT_HOLDER_ACCOUNT_ISSUER ="error.accreditation.certificate.holder.account.not.issuer";
	
	/** The Constant ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE. */
	public final static String ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE ="error.accreditation.certificate.holder.account.not.registered";
	
	/** The Constant ERROR_RECORD_NOT_SELECTED. */
	public static final String ERROR_RECORD_NOT_SELECTED = "error.record.not.selected";
	
	/** The Constant ERROR_RECORD_NOT_SELECTED_UNIFICATION_OPERATION. */
	public static final String ERROR_RECORD_NOT_SELECTED_UNIFICATION_OPERATION = "error.record.not.selected.unification.operation";

	
	/** The Constant ACCREDITATION_CONFIRM_SUCCESS. */
	public static final String ACCREDITATION_CONFIRM_SUCCESS="message.accreditation.certificate.confirm.success";
	
	/** The Constant ERROR_NOT_ACTION. */
	public static final String ERROR_NOT_ACTION="error.action.not.valid";
	
	/** The Constant ERROR_STATE_NOT_EQUALS. */
	public static final String ERROR_STATE_NOT_EQUALS="error.state.not.equals";

	/** The Constant ERROR_MESSAGE_INVALID_BALANCE_GENERATE. */
	public static final String ERROR_MESSAGE_INVALID_BALANCE_GENERATE="error.message.invalid.balance.generate";

	/** The Constant ERROR_MESSAGE_UNABLE_TO_GENERATE_CAT_FOR_DPF. */
	public static final String ERROR_MESSAGE_UNABLE_TO_GENERATE_CAT_FOR_DPF="error.message.unable.to.generate.cat.for.dpf";
	
	/** The Constant ERROR_HOLDER_ACCOUNT_NOT_EXIST. */
	public static final String ERROR_HOLDER_ACCOUNT_NOT_EXIST="deposito.err.holderaccount.notexists";
	
	/** The Constant ERROR_CERTIFICATE_EXIST. */
	public static final String ERROR_CERTIFICATE_EXIST="deposito.err.certificate.exists";
	
	/** The Constant ERROR_CORRELATIVO_INVALID. */
	public static final String ERROR_CORRELATIVO_INVALID="deposito.err.to.correlativo";
	
	/** The Constant ERROR_MODIFY_CERTIFICATE. */
	public static final String ERROR_MODIFY_CERTIFICATE="deposito.err.modify.certificate";
	
	/** The Constant ERROR_MODIFY_ELIMINAR_CERTIFICATE_SELECT. */
	public static final String ERROR_MODIFY_ELIMINAR_CERTIFICATE_SELECT="deposito.err.elimodify.condition";
	
	/** The Constant COMMONS_LABEL_NOSELECTDATA. */
	public static final String COMMONS_LABEL_NOSELECTDATA="commons.label.noSelectData";
	
	/** The Constant ERROR_REQUEST_NUMBER_NOT_EXISTS. */
	public static final String ERROR_REQUEST_NUMBER_NOT_EXISTS="deposito.err.request.number";
	
	/** The Constant ERROR_CERTIFICATE_LIST_EXIST. */
	public static final String ERROR_CERTIFICATE_LIST_EXIST="deposito.err.certificate.list";
	
	/** The Constant ERROR_NO_MODIFICATION_DEPOSIT_CERTIFICATE. */
	public static final String ERROR_NO_MODIFICATION_DEPOSIT_CERTIFICATE="deposito.warning.no.modification";
	
	/** The Constant ERROR_CERTIFICATE_REGISTRATION. */
	public static final String ERROR_CERTIFICATE_REGISTRATION="deposito.err.certificate";
	
	/** The Constant ERROR_CERTIFICATE_REGISTRATION_CONCURRENCY. */
	public static final String ERROR_CERTIFICATE_REGISTRATION_CONCURRENCY="deposiro.err.certificate.concurrency";
	
	/** The Constant ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED. */
	public static final String ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED="deposito.err.holderaccount.notAllowed";
	
	/** The Constant ERROR_CERTIFICATE_HOLDER_NOT_ALLOWED. */
	public static final String ERROR_CERTIFICATE_HOLDER_NOT_ALLOWED="deposito.err.holder.notAllowed";
	
	/** The Constant LBL_HEADER_ALERT_ANNULMENT. */
	public static final String LBL_HEADER_ALERT_ANNULMENT = "header.message.annulment";
	
	/** The Constant LBL_HEADER_ALERT_REJECTED. */
	public static final String LBL_HEADER_ALERT_REJECTED = "header.message.rejection";
	
	/** The Constant LBL_MSG_CLEAR_SCREEN. */
	public static final String LBL_MSG_CLEAR_SCREEN= "lbl.clear.screen";
	
	/** The Constant LBL_MSG_CANCEL_SCREEN. */
	public static final String LBL_MSG_CANCEL_SCREEN= "lbl.clear.cancel";
	
	
	/** The Constant SUCCESS_LIST_SECURITY_NUMBER. */
	public static final String SUCCESS_LIST_SECURITY_NUMBER="retiro.succ.security.list";
	
	/** The Constant CERTIFICATE_DETAIL_VIEW_ERRORe. */
	public static final String CERTIFICATE_DETAIL_VIEW_ERRORe="deposito.cert.detail.view";

	/** The Constant SUCCESS_MESSAGE. */
	public static final String SUCCESS_MESSAGE="deposito.success";
	
	/** The Constant MSG_SAVE. */
	public static final String MSG_SAVE="deposito.msg.save";
	
	/** The Constant MSG_CONFIRM_ADD. */
	public static final String MSG_CONFIRM_ADD="deposito.msg.add";
	
	/** The Constant MSG_CONFIRM_DELETE. */
	public static final String MSG_CONFIRM_DELETE="deposito.msg.delete.success";
	
	/** The Constant MSG_CONFIRM_ADD_SUCCESS. */
	public static final String MSG_CONFIRM_ADD_SUCCESS="deposito.msg.add.success";
	
	/** The Constant MSG_BACK_VALIDATION. */
	public static final String MSG_BACK_VALIDATION = "message.back";
	
	/** The Constant CHANGE_PARTICIPANT_QUESTION. */
	public static final String CHANGE_PARTICIPANT_QUESTION = "deposito.change.participant.differ";

	/** The Constant APPROVE_SUCCESS_MESSAGE. */
	public static final String APPROVE_SUCCESS_MESSAGE="lbl.msg.aprobar.success";

	/** The Constant ERRO_APPROVAL. */
	public static final String ERRO_APPROVAL="deposito.err.msg.approve";
	
	/** The Constant ERRO_AUTHORIZE. */
	public static final String ERRO_AUTHORIZE="deposito.err.msg.authorize";
	
	/** The Constant ERROR_CONCURRENT. */
	public static final String ERROR_CONCURRENT="deposito.err.msg.concur";
	
	/** The Constant ERROR_PARETICIPANT_REQUIRE. */
	public static final String ERROR_PARETICIPANT_REQUIRE="deposito.err.msg.participant.require";
	
	/** The Constant ERROR_ACCOUNT_INVALID. */
	public static final String ERROR_ACCOUNT_INVALID="deposito.err.msg.account.invalid";
	
	/** The Constant MSG_MODIFY_TITLE. */
	public static final String MSG_MODIFY_TITLE="deposito.msg.modify";
	
	/** The Constant MSG_ELIMINAR_TITLE. */
	public static final String MSG_ELIMINAR_TITLE="deposito.msg.eliminar";
	
	/** The Constant MSG_ELIMINAR_TITLE_ERROR. */
	public static final String MSG_ELIMINAR_TITLE_ERROR="deposito.msg.eliminar.error";
	
	
	/** The Constant MSG_NO_MODIFY. */
	public static final String MSG_NO_MODIFY="deposito.err.msg.no.modify";
	
	/** The Constant MSG_MODIFY_SUCCESS. */
	public static final String MSG_MODIFY_SUCCESS="deposito.msg.modify.succes";
	
	/** The Constant MSG_ELIMINAR_SUCCESS. */
	public static final String MSG_ELIMINAR_SUCCESS="deposito.msg.eliminar.success";
			
	/** The Constant MSG_APPROVED_PREVIOUSLY. */
	public static final String MSG_APPROVED_PREVIOUSLY="deposito.err.msg.aprove.prev";
	
	/** The Constant MSG_CANCELLED_PREVIOUSLY. */
	public static final String MSG_CANCELLED_PREVIOUSLY="deposito.err.msg.cancel.prev";
	
	/** The Constant MSG_CONFIRMED_PREVIOUSLY. */
	public static final String MSG_CONFIRMED_PREVIOUSLY="deposito.err.msg.confirm.prev";
	
	/** The Constant MSG_REJECTED_PREVIOUSLY. */
	public static final String MSG_REJECTED_PREVIOUSLY="deposito.err.msg.reject.prev";
	
	/** The Constant MSG_APPLIED_PREVIOUSLY. */
	public static final String MSG_APPLIED_PREVIOUSLY="deposito.err.msg.apply.prev";
	
	/** The Constant MSG_APPROVED_PREVIOUSLY_MULTI. */
	public static final String MSG_APPROVED_PREVIOUSLY_MULTI="deposito.err.msg.aprove.prev.multi";
	
	/** The Constant MSG_CANCELLED_PREVIOUSLY_MULTI. */
	public static final String MSG_CANCELLED_PREVIOUSLY_MULTI="deposito.err.msg.cancel.prev.multi";
	
	/** The Constant MSG_CONFIRMED_PREVIOUSLY_MULTI. */
	public static final String MSG_CONFIRMED_PREVIOUSLY_MULTI="deposito.err.msg.confirm.prev.multi";
	
	/** The Constant MSG_REJECTED_PREVIOUSLY_MULTI. */
	public static final String MSG_REJECTED_PREVIOUSLY_MULTI="deposito.err.msg.reject.prev.multi";
	
	/** The Constant MSG_APPLIED_PREVIOUSLY_MULTI. */
	public static final String MSG_APPLIED_PREVIOUSLY_MULTI="deposito.err.msg.apply.prev.multi";
	
	/** The Constant MSG_APPLIED_PREVIOUSLY_AUTHORIZED. */
	public static final String MSG_APPLIED_PREVIOUSLY_AUTHORIZED="deposito.err.msg.apply.prev.authorized";
	
	

	/** The Constant MSG_CONFIRM_APPROVAL. */
	public static final String MSG_CONFIRM_APPROVAL="deposito.msg.approve";
	
	/** The Constant MSG_CONFIRM_APPROVAL_MULTI. */
	public static final String MSG_CONFIRM_APPROVAL_MULTI="deposito.msg.approve.multi";
	
	/** The Constant MSG_CONFIRM_APPROVAL_SUCCESS. */
	public static final String MSG_CONFIRM_APPROVAL_SUCCESS="deposito.msg.approve.success";
	
	/** The Constant MSG_CONFIRM_APPROVAL_SUCCESS_MULTI. */
	public static final String MSG_CONFIRM_APPROVAL_SUCCESS_MULTI="deposito.msg.approve.success.multi";

	/** The Constant MSG_CONFIRM_ANNULAR. */
	public static final String MSG_CONFIRM_ANNULAR="deposito.msg.annular";
	
	/** The Constant MSG_CONFIRM_ANNULAR_MULTI. */
	public static final String MSG_CONFIRM_ANNULAR_MULTI="deposito.msg.annular.multi";
	
	/** The Constant MSG_CONFIRM_ANNULAR_SUCCESS. */
	public static final String MSG_CONFIRM_ANNULAR_SUCCESS="deposito.msg.annular.success";
	
	/** The Constant MSG_CONFIRM_ANNULAR_SUCCESS_MULTI. */
	public static final String MSG_CONFIRM_ANNULAR_SUCCESS_MULTI="deposito.msg.annular.success.multi";
	
	/** The Constant MSG_CONFIRM_REJECT. */
	public static final String MSG_CONFIRM_REJECT="deposito.msg.reject";
	
	/** The Constant MSG_CONFIRM_REJECT_MULTI. */
	public static final String MSG_CONFIRM_REJECT_MULTI="deposito.msg.reject.multi";
	
	/** The Constant MSG_CONFIRM_REJECT_SUCCESS. */
	public static final String MSG_CONFIRM_REJECT_SUCCESS="deposito.msg.reject.success";
	
	/** The Constant MSG_CONFIRM_REJECT_SUCCESS_MULTI. */
	public static final String MSG_CONFIRM_REJECT_SUCCESS_MULTI="deposito.msg.reject.success.multi";
	
	/** The Constant ERROR_CONFIRM. */
	public static final String ERROR_CONFIRM="deposito.err.msg.confirm";
	
	/** The Constant ERROR_APPLY. */
	public static final String ERROR_APPLY="deposito.err.msg.apply";
	
	/** The Constant ERROR_CANCEL. */
	public static final String ERROR_CANCEL="deposito.err.msg.cancel";
	
	/** The Constant ERROR_REJECT. */
	public static final String ERROR_REJECT="deposito.err.msg.reject";
	
	/** The Constant ERROR_HOLDER_ACCOUNT_ACTIVE. */
	public static final String ERROR_HOLDER_ACCOUNT_ACTIVE="deposito.err.holder.account.active";

	/** The Constant MSG_CONFIRM_CERTIFIACTE. */
	public static final String MSG_CONFIRM_CERTIFIACTE="deposito.msg.confirm";
	
	/** The Constant MSG_CONFIRM_CERTIFIACTE_MULTI. */
	public static final String MSG_CONFIRM_CERTIFIACTE_MULTI="deposito.msg.confirm.multi";
	
	/** The Constant MSG_CONFIRM_SUCCESS. */
	public static final String MSG_CONFIRM_SUCCESS="deposito.msg.confirm.success";
	
	/** The Constant MSG_CONFIRM_SUCCESS_MULTI. */
	public static final String MSG_CONFIRM_SUCCESS_MULTI="deposito.msg.confirm.success.multi";

	/** The Constant MSG_REJECT_CERTIFICATE. */
	public static final String MSG_REJECT_CERTIFICATE="deposito.msg.reject";
	
	/** The Constant MSG_REJECT_CERTIFICATE_MULTI. */
	public static final String MSG_REJECT_CERTIFICATE_MULTI="deposito.msg.reject.multi";
	
	/** The Constant MSG_ADD_CERTIFICATE_MULTI. */
	public static final String MSG_ADD_CERTIFICATE_MULTI="deposito.msg.add.success";
	
	/** The Constant MSG_REJECT_SUCCESS. */
	public static final String MSG_REJECT_SUCCESS="deposito.msg.reject.success";
	
	/** The Constant MSG_REJECT_SUCCESS_MULTI. */
	public static final String MSG_REJECT_SUCCESS_MULTI="deposito.msg.reject.success.multi";

	/** The Constant MSG_APLICAR_CERTIFICATE. */
	public static final String MSG_APLICAR_CERTIFICATE="deposito.msg.apply";
	
	/** The Constant MSG_APLICAR_CERTIFICATE_MULTI. */
	public static final String MSG_APLICAR_CERTIFICATE_MULTI="deposito.msg.apply.multi";
	
	/** The Constant MSG_APLICAR_SUCCESS. */
	public static final String MSG_APLICAR_SUCCESS="deposito.msg.apply.success";
	
	/** The Constant MSG_APLICAR_SUCCESS_MULTI. */
	public static final String MSG_APLICAR_SUCCESS_MULTI="deposito.msg.apply.success.multi";
	
	/** The Constant MSG_APLICAR_SUCCESS_AYNI_1000 . */
	public static final String MSG_APLICAR_SUCCESS_AYNI_0000="deposito.msg.apply.ayni.0000";
	
	/** The Constant MSG_APLICAR_SUCCESS_AYNI_2000 . */
	public static final String MSG_APLICAR_SUCCESS_AYNI_1000="deposito.msg.apply.ayni.1000";
	
	/** The Constant MSG_APLICAR_SUCCESS_AYNI_1000 . */
	public static final String MSG_AUTORIZAR_SUCCESS_AYNI_0000="lbl.msg.authorizar.ayni.0000";
	
	/** The Constant MSG_APLICAR_SUCCESS_AYNI_2000 . */
	public static final String MSG_AUTORIZAR_SUCCESS_AYNI_1000="lbl.msg.authorizar.ayni.1000";
	
	/** The Constant ANULER_SUCCESS_MESSAGE. */
	public static final String ANULER_SUCCESS_MESSAGE="lbl.msg.anuler.success";

	/** The Constant ANULER_SUCCESS_MESSAGE. */
	public static final String REJECTED_SUCCESS_MESSAGE="lbl.msg.reject.success";
	
	/** The Constant CONFIRM_SUCCESS_MESSAGE. */
	public static final String CONFIRM_SUCCESS_MESSAGE="lbl.msg.confirm.success";
	
	/** The Constant APLICAR_SUCCESS_MESSAGE. */
	public static final String APLICAR_SUCCESS_MESSAGE="lbl.msg.aplicar.success";
	
	/** The Constant AUTHORIZAR_SUCCESS_MESSAGE. */
	public static final String AUTHORIZAR_SUCCESS_MESSAGE="lbl.msg.authorizar.success";
	
	/** The Constant HOLDER_ACCOUNT_ERROR_MSG. */
	public static final String HOLDER_ACCOUNT_ERROR_MSG="deposito.req.holder.account";
	
	/** The Constant MSG_LOCATION_MODIFICATION_CNFM. */
	public static final String MSG_LOCATION_MODIFICATION_CNFM = "modify.location.cnfrm.msg";
	
	/** The Constant MSG_HEADER_MODIFICATION. */
	public static final String MSG_HEADER_MODIFICATION = "header.message.modification";
	
	/** The Constant MSG_ISSUER_NOTEXIST. */
	public static final String MSG_ISSUER_NOTEXIST="lbl.retiro.issuer.notexist";
	
	/** The Constant MSG_INSTRUMENT_SELECT. */
	public static final String MSG_INSTRUMENT_SELECT="deposito.req.intsrmnt.type";
	
	/** The Constant MSG_SECURITY_SELECT. */
	public static final String MSG_SECURITY_SELECT="deposito.req.secu.class";
	
	/** The Constant MSG_ISSUER_SELECT. */
	public static final String MSG_ISSUER_SELECT="deposito.req.emisor";
	
	/** The Constant MSG_PAYEMENT_SELECT. */
	public static final String MSG_PAYEMENT_SELECT="deposito.req.paymnt.period";
	
	/** The Constant MSG_INTEREST_RATE. */
	public static final String MSG_INTEREST_RATE="deposito.req.valor.tasa";
	
	/** The Constant MSG_CURRENCY_SELECT. */
	public static final String MSG_CURRENCY_SELECT="deposito.req.currency.tipo";
	
	/** The Constant MSG_NOMINAL_VALUE. */
	public static final String MSG_NOMINAL_VALUE="deposito.req.nominal.value";
	
	/** The Constant MSG_CERTIFICATE_NUMBER. */
	public static final String MSG_CERTIFICATE_NUMBER="deposito.req.numero.titulo";
	
	/** The Constant MSG_START_CORRELATIVE_NUMBER. */
	public static final String MSG_START_CORRELATIVE_NUMBER="deposito.req.start.cor";
	
	/** The Constant MSG_END_CORRELATIVE_NUMBER. */
	public static final String MSG_END_CORRELATIVE_NUMBER="deposito.req.end.cor";
	
	/** The Constant MSG_COMMENT_WITHDRAWAL_BY_DEMATERIALIZATION. */
	public static final String MSG_COMMENT_WITHDRAWAL_BY_DEMATERIALIZATION = "message.comment.withdrawal.dematerialization"; 
	
	/**  retirement. */
	
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_INVALID_CODE="msg.retirement.security.interface.invalid.code";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INTERFACE_INVALID_INSTITUTION. */
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_INVALID_INSTITUTION="msg.retirement.security.interface.invalid.institution";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INTERFACE_INVALID_CLASS. */
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_INVALID_CLASS="msg.retirement.security.interface.invalid.sec.class";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INTERFACE_CLASS_NOT_EXISTS. */
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_CLASS_NOT_EXISTS="msg.retirement.security.interface.sec.class.not.exists";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INTERFACE_MULTIPLE_MARKETFACTS. */
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_MULTIPLE_MARKETFACTS="msg.retirement.security.interface.sec.multiple.marketfacts";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INTERFACE_INSUFFICIENT_BALANCES. */
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_INSUFFICIENT_BALANCES="msg.retirement.security.interface.insufficient.balances";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INTERFACE_NO_MKTFACT_BALANCES. */
	public static final String MSG_RETIREMENT_SECURITIES_INTERFACE_NO_MKTFACT_BALANCES="msg.retirement.security.interface.no.marketfact.balances";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INCORRECT_BALANCES. */
	public static final String MSG_RETIREMENT_SECURITIES_INCORRECT_BALANCES="msg.retirement.security.account.incorrect.balances";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_INVALID_STATE_REQ. */
	public static final String MSG_RETIREMENT_SECURITIES_INVALID_STATE_REQ="msg.retirement.security.invalid.state";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_NO_PREPAID_SECURITY. */
	public static final String MSG_RETIREMENT_SECURITIES_NO_PREPAID_SECURITY="msg.retirement.security.no.prepaid.security";
	
	/** The Constant RETIREMENT_OPERATION_NOTHING_TO_RETIRE. */
	public static final String MSG_RETIREMENT_SECURITIES_NOTHING_TO_RETIRE="msg.retirement.security.nothing.to.retire";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_ZERO_TO_RETIRE. */
	public static final String MSG_RETIREMENT_SECURITIES_ZERO_TO_RETIRE="msg.retirement.security.zero.to.retire";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_ISSUANCE_TYPE_NOT_MIX. */
	public static final String MSG_RETIREMENT_SECURITIES_ISSUANCE_TYPE_NOT_MIX="msg.retirement.security.reversion.issuance.type";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_INSTRUMENT_TYPE_NOT_VARIABLE. */
	public static final String MSG_RETIREMENT_SECURITIES_INSTRUMENT_TYPE_NOT_VARIABLE="msg.retirement.security.reversion.instrument.type";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_INSTRUMENT_TYPE_NOT_FIXED. */
	public static final String MSG_RETIREMENT_SECURITIES_INSTRUMENT_TYPE_NOT_FIXED="msg.retirement.security.reversion.instrument.type.fixed";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_FIND_SECURITY. */
	public static final String MSG_RETIREMENT_SECURITIES_FIND_SECURITY="msg.retirement.security.find.security";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_FIND_SECURITY_NOT_EARLY_REDEMPTION. */
	public static final String MSG_RETIREMENT_SECURITIES_FIND_SECURITY_NOT_EARLY_REDEMPTION="msg.retirement.security.find.security.early.redemption";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_IN_BLOCKED_STATE. */
	public static final String MSG_RETIREMENT_SECURITIES_IN_BLOCKED_STATE="msg.retirement.security.in.blocked.state";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_FOREING_SOURCE. */
	public static final String MSG_RETIREMENT_SECURITIES_FOREING_SOURCE="msg.retirement.security.foreing.source";
	
	/** The Constant ERROR_RETIREMENT_SECURITY_BLOCKED_HOLDER. */
	public static final String MSG_RETIREMENT_SECURITIES_BLOCKED_HOLDER="msg.retirement.security.holder.blocked";
	
	/** The Constant REMOVE_SECURITY_ERROR_FINDING_ISINCODE. */
	public static final String MSG_RETIREMENT_SECURITIES_ERROR_FINDING_ISINCODE="msg.retirement.security.finding.isincode";
	
	/** The Constant REMOVE_SECURITY_ERROR_NOT_ISINCODE_ENTERED. */
	public static final String MSG_RETIREMENT_SECURITIES_ERROR_NOT_ISINCODE_ENTERED="msg.retirement.security.holder.not.isincode";
	
	/** The Constant REMOVE_SECURITY_HOLDER_ACCOUNT_NO_AVAI. */
	public static final String MSG_RETIREMENT_SECURITIES_HOLDER_ACCOUNT_NO_AVAI="msg.retirement.security.holder.account.no.available";
	
	/** The Constant ERROR_HOLDER_WITHOUT_HOLDER_ACCOUNT_BALANCE. */
	public static final String MSG_RETIREMENT_SECURITIES_HOLDER_WITHOUT_BALANCE="msg.retirement.security.holder.account.without.balances";
	
	/** The Constant RETIREMENT_OPERATION_ERROR_RETIREMENT_BALANCE. */
	public static final String MSG_RETIREMENT_SECURITIES_ERROR_RETIREMENT_BALANCE = "msg.retirement.security.retirement_balance";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_CONFIRM_SUCCESS. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRM_SUCCESS="msg.retirement.security.save";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_CONFIRM_ANNULATE. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRM_ANNULATE = "msg.retirement.security.anullate";
	
	/** The Constant MSG_REJECT_CERTIFICATERETIREMENT. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRM_REJECT = "msg.retirement.security.reject";
	
	/** The Constant MSG_CONFIRM_APPROVAL_MULTIRETIREMENT. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRM_APPROVE = "msg.retirement.security.approve";
	
	/** The Constant MSG_CONFIRM_CERTIFIACTE_MULTIRETIREMENT. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRM_CONFIRM = "msg.retirement.security.confirm";
	
	/** The Constant MSG_APPLY_CERTIFIACTE_MULTIRETIREMENT. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRM_APPLY = "msg.retirement.security.apply";
	
	/** The Constant RETIREMENT_OPERATION_SUCCESSFULLY_SAVED. */
	public static final String MSG_RETIREMENT_SECURITIES_REGISTER_SUCCESS="msg.retirement.security.saved";
	
	/** The Constant MSG_RETIREMENT_SECURITY_REJECTED_SUCCESS. */
	public static final String MSG_RETIREMENT_SECURITIES_REJECTED_SUCCESS="msg.retirement.security.reject.success";
	
	/** The Constant MSG_RETIREMENT_SECURITY_ANULLED_SUCCESS. */
	public static final String MSG_RETIREMENT_SECURITIES_ANULLED_SUCCESS="msg.retirement.security.anulled.success";
	
	/** The Constant MSG_RETIREMENT_SECURITY_APPROVED_SUCCESS. */
	public static final String MSG_RETIREMENT_SECURITIES_APPROVED_SUCCESS="msg.retirement.security.approve.success";
	
	/** The Constant MSG_RETIREMENT_SECURITY_CONFIRMED_SUCCESS. */
	public static final String MSG_RETIREMENT_SECURITIES_CONFIRMED_SUCCESS="msg.retirement.security.confirm.success";
	
	/** The Constant MSG_RETIREMENT_SECURITIES_MASSIVE_LABEL. */
	public static final String MSG_RETIREMENT_SECURITIES_MASSIVE_LABEL="iCustody.retirement.securities.massive";
	
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_CONFIRM_CONF. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_CONFIRM_CONF="message.ownershipexchange.confirm.conf";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_REJECT_CONF. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_REJECT_CONF="message.ownershipexchange.reject.conf";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_ACTION_OK. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_OK="message.ownershipexchange.confirm.ok";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_REJECT_OK. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_REJECT_OK="message.ownershipexchange.reject.ok";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_REGISTER. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_REGISTER="message.ownershipexchange.register";
	
	public static final String MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_REGISTER_ALLSECURITIES="message.ownershipexchange.register.all";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_REGISTER_OK. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_CONFIRMATION_REGISTER_OK="message.ownershipexchange.register.ok";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_ALL_TRANSFERED_OK. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_ALL_TRANSFERED_OK="message.ownershipexchange.all.transfered.ok";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_ALL_BLOCKED_TRANSFERED_OK. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_ALL_BLOCKED_TRANSFERED_OK="message.ownershipexchange.all.blocked.transfered.ok";
	
	/** The Constant MESSAGE_CHANGE_OWNERSHIP_NO_ACCOUNTS. */
	public static final String MESSAGE_CHANGE_OWNERSHIP_NO_ACCOUNTS="message.ownershipexchange.no.accounts.for.participant";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_SUCESION_CONTAINS_OWNERSHIP. */
	public static final String ERROR_CHANGE_OWNERSHIP_SUCESION_CONTAINS_OWNERSHIP="error.ownershipexchange.sucesion.contains.ownership";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_SUCESION_OWNERSHIP. */
	public static final String ERROR_CHANGE_OWNERSHIP_SUCESION_OWNERSHIP="error.ownershipexchange.sucesion.ownership.not.allowed";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_ACCOUNT_CLOSED. */
	public static final String ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_ACCOUNT_CLOSED="error.ownershipexchange.sourceholderaccount.closed";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TRANSIT_BALANCE_EXISTS. */
	public static final String ERROR_CHANGE_OWNERSHIP_TRANSIT_BALANCE_EXISTS="error.ownershipexchange.transitbalance.exists";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TRANSIT_BALANCE_EXISTS_SUCESION. */
	public static final String ERROR_CHANGE_OWNERSHIP_TRANSIT_BALANCE_EXISTS_SUCESION="error.ownershipexchange.transitbalance.exists.sucesion";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_ACCOUNT_ALREADY_ADDED. */
	public static final String ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_ACCOUNT_ALREADY_ADDED="error.ownershipexchange.targetholderaccount.alreadyadded";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_EMPTY_TARGET. */
	public static final String ERROR_CHANGE_OWNERSHIP_EMPTY_TARGET="error.ownershipexchange.target.empty";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TOTAL_AMOUNT_EXCEEDED. */
	public static final String ERROR_CHANGE_OWNERSHIP_TOTAL_AMOUNT_EXCEEDED="error.ownershipexchange.target.quantity.exceeded";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TOTAL_BLOCK_AMOUNT_EXCEEDED. */
	public static final String ERROR_CHANGE_OWNERSHIP_TOTAL_BLOCK_AMOUNT_EXCEEDED="error.ownershipexchange.target.block.quantity.exceeded";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_EQUAL_SOURCE. */
	public static final String ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_EQUAL_SOURCE = "error.ownershipexchange.targetholder.equal.source";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_NOT_REGISTERED. */
	public static final String ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_NOT_REGISTERED = "error.ownershipexchange.sourceholder.not.registered";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_SOURCE_BLOCKED_ENTIREDSTATE. */
	public static final String ERROR_CHANGE_OWNERSHIP_SOURCE_BLOCKED_ENTIREDSTATE = "error.ownershipexchange.sourceholder.blocked.entirestate";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TARGET_BALANCE_ALREADY_ADDED. */
	public static final String ERROR_CHANGE_OWNERSHIP_TARGET_BALANCE_ALREADY_ADDED="error.ownershipexchange.targetbalance.alreadyadded";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_BALANCE_ALREADY_ADDED. */
	public static final String ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_BALANCE_ALREADY_ADDED="error.ownershipexchange.block.targetbalance.alreadyadded";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_UNABLE_ADD_REBLOCK. */
	public static final String ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_UNABLE_ADD_REBLOCK ="error.ownershipexchange.block.targetbalance.unable.add.reblock";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_IN_ACCREDITATION. */
	public static final String ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_IN_ACCREDITATION ="error.ownershipexchange.block.targetbalance.in.accreditation";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TARGET_AMOUNT_ZERO. */
	public static final String ERROR_CHANGE_OWNERSHIP_TARGET_AMOUNT_ZERO="error.ownershipexchange.targetbalance.amount.zero";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_REQUIRES_TOTAL. */
	public static final String ERROR_CHANGE_OWNERSHIP_REQUIRES_TOTAL="error.ownershipexchange.requires.total";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_REQUIRES_TOTAL_BLOCK. */
	public static final String ERROR_CHANGE_OWNERSHIP_REQUIRES_TOTAL_BLOCK="error.ownershipexchange.requires.total.block";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_NO_DETAILS_FOUND. */
	public static final String ERROR_CHANGE_OWNERSHIP_NO_DETAILS_FOUND="error.ownershipexchange.no.details.found";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_NO_ATTACHMENTS_FOUND. */
	public static final String ERROR_CHANGE_OWNERSHIP_NO_ATTACHMENTS_FOUND="error.ownershipexchange.no.attachments.found";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_HAS_TRANSIT_ACCR. */
	public static final String ERROR_CHANGE_OWNERSHIP_HAS_TRANSIT_ACCR="error.ownershipexchange.source.has.transit.or.accreditation";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_REQUEST_ALREADY_EXISTS. */
	public static final String ERROR_CHANGE_OWNERSHIP_REQUEST_ALREADY_EXISTS="error.ownershipexchange.request.already.exists";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TARGET_ACCOUNT_CLOSED. */
	public static final String ERROR_CHANGE_OWNERSHIP_TARGET_ACCOUNT_CLOSED = "error.ownershipexchange.targetaccount.closed";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_ALREADY_EXISTS. */
	public static final String ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_ALREADY_EXISTS = "error.ownershipexchange.targetholder.alreadyexists";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_EMPTY_TARGET_DATATABLE. */
	public static final String ERROR_CHANGE_OWNERSHIP_EMPTY_TARGET_DATATABLE="error.ownershipexchange.target.datatable.empty";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_EMPTY_SOURCE_ACCOUNT. */
	public static final String ERROR_CHANGE_OWNERSHIP_EMPTY_SOURCE_ACCOUNT="error.ownershipexchange.sourceholderaccount.empty";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_NO_BALANCES_AVAILABLE. */
	public static final String ERROR_CHANGE_OWNERSHIP_NO_BALANCES_AVAILABLE="error.ownershipexchange.no.balances.available";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_NO_BLOCKED_BALANCES_AVAILABLE. */
	public static final String ERROR_CHANGE_OWNERSHIP_NO_BLOCKED_BALANCES_AVAILABLE="error.ownershipexchange.no.blocked.balances.avail";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_OWNERSHIP_BALANCE_TYPE_REQUIRED. */
	public static final String ERROR_CHANGE_OWNERSHIP_OWNERSHIP_BALANCE_TYPE_REQUIRED="error.ownershipexchange.balance.type.required";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_OWNERSHIP_MOTIVE_REQUIRED. */
	public static final String ERROR_CHANGE_OWNERSHIP_OWNERSHIP_MOTIVE_REQUIRED="error.ownershipexchange.motive.required";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_INVALID_MOTIVE_FOR_BLOCKED. */
	public static final String ERROR_CHANGE_OWNERSHIP_INVALID_MOTIVE_FOR_BLOCKED="error.ownershipexchange.invalid.motive.balance";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_REBLOCKS_NOT_ADDED. */
	public static final String ERROR_CHANGE_OWNERSHIP_REBLOCKS_NOT_ADDED="error.ownershipexchange.reblocks.not.added";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_NO_BLOCKED_BALANCES_AVAILABLE. */
	public static final String ERROR_CHANGE_OWNERSHIP_NOT_FOUND_BLOCK_OPERATIONS="error.ownershipexchange.block.operations.notfound";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_DIVISION_SOURCE. */
	public static final String ERROR_CHANGE_OWNERSHIP_DIVISION_SOURCE="error.ownershipexchange.ownership.division.not.allowed";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_DIVISION_DESTINATION. */
	public static final String ERROR_CHANGE_OWNERSHIP_DIVISION_DESTINATION="error.ownershipexchange.ownership.division.not.destination";
	
	/** The Constant ERROR_CHANGE_OWNERSHIP_INCLUDE_HOLDER. */
	public static final String ERROR_CHANGE_OWNERSHIP_INCLUDE_HOLDER="error.ownershipexchange.ownership.include.holder";
	
	/** The Constant DUPLICATE_SITUATION. */
	public static final String DUPLICATE_SITUATION="message.dupli.situation";
	
	/** The Constant MSG_MULTIPLE_USERS_UPDATION. */
	public static final String MSG_MULTIPLE_USERS_UPDATION="modify.location.multiple.users.updation";
	
	/** The Constant PARTICIPANT_UNIFICATION_PARTICIPANT_ORIGIN_NOT_REGITERED_BLOCKED. */
	public static final String PARTICIPANT_UNIFICATION_PARTICIPANT_ORIGIN_NOT_REGITERED_BLOCKED="message.participant.unification.error.participant.origin.not.registered";
	
	
	/** The Constant MSG_STATE_CFM. */
	public static final String MSG_STATE_CFM="modify.location.confirmed.state";
	
	/** The Constant MSG_MODIFICATION_SUCCESS. */
	public static final String MSG_MODIFICATION_SUCCESS="modify.location.success.msg";

	/** The Constant MSG_DELETE. */
	public static final String MSG_DELETE="lbl.msg.eliminar";

	
	/** The Constant MSG_RECORD_SELECT_CNF. */
	public static final String MSG_RECORD_SELECT_CNF="deposito.err.elimodify.condition";
	
	/** The Constant MSG_HOLDER_ACCOUNT_BLOCKED. */
	public static final String MSG_HOLDER_ACCOUNT_BLOCKED = "deposito.err.holder.account.block";

	/** The Constant MSG_HOLDER_ACCOUNT_CLOSED. */
	public static final String MSG_HOLDER_ACCOUNT_CLOSED = "deposito.err.holder.account.close";

	/** The Constant MSG_ISSUER_BLOCKED. */
	public static final String MSG_ISSUER_BLOCKED = "deposito.err.issuer.block";

	/** The Constant ERROR_ADM_BLOCK_ENTITY_FILTER_DATE_INITIAL. */
	public static final String ERROR_ADM_BLOCK_ENTITY_FILTER_DATE_INITIAL="error.adm.block.entity.filter.date.intial";

	/** The Constant ERROR_ADM_BLOCK_ENTITY_FILTER_DATE_FINAL_DATE_TODAY. */
	public static final String ERROR_ADM_BLOCK_ENTITY_FILTER_DATE_FINAL_DATE_TODAY="error.adm.block.entity.filter.date.final.date.today";
	
	/** The Constant ERROR_ADM_BLOCK_ENTITY_FILTER_DATE_INITIAL_DATE_TODAY. */
	public static final String ERROR_ADM_BLOCK_ENTITY_FILTER_DATE_INITIAL_DATE_TODAY="error.adm.block.entity.filter.date.initial.date.today";

	/** The Constant PARTICIPANT_UNIFICATION_PARTICIPANT_REGISTERED_IN_OTHER. */
	public static final String PARTICIPANT_UNIFICATION_PARTICIPANT_REGISTERED_IN_OTHER="message.participant.unification.error.participant.origin.registered";
	
	/** The Constant PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_NOT_REGISTERED. */
	public static final String PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_NOT_REGISTERED="message.participant.unification.error.participant.target.not.registered";
	
	/** The Constant PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_EQUALS_ORIGIN. */
	public static final String PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_EQUALS_ORIGIN="message.participant.unification.error.participant.target.equals.origin";
	
	/** The Constant PARTICIPANT_UNIFICATION_PARTICIPANT_SUSTENTS. */
	public static final String PARTICIPANT_UNIFICATION_PARTICIPANT_SUSTENTS="error.participant.unification.required.participant.sustents";

	/** The Constant MSG_WARNING_TITLE. */
	public static final String MSG_WARNING_TITLE="header.message.alert";
	
	/** The Constant MSG_ERROR_TITLE. */
	public static final String MSG_ERROR_TITLE="header.message.error";
	
	/** The Constant LBL_CONFIRM_HEADER_MESSAGE. */
	public static final String LBL_CONFIRM_HEADER_MESSAGE="lbl.confirm.header.message";
	
	/** The Constant ADM_BLOCK_ENTITY_REGISTRY_CONFIRM_MESSAGE. */
	public static final String ADM_BLOCK_ENTITY_REGISTRY_CONFIRM_MESSAGE="adm.block.entity.register.confirm.message";

	/** The Constant PARTICIPANT_UNIFICATION_REQUIRED_ERROR. */
	public static final String PARTICIPANT_UNIFICATION_REQUIRED_ERROR="error.accreditation.certificate.required";

	/** The Constant LBL_REGISTRY_BLOCKENTITY_OK. */
	public static final String LBL_REGISTRY_BLOCKENTITY_OK="lbl.registry.blockentity.ok";
	
	/** The Constant LBL_WARNING_ACTION. */
	public static final String LBL_WARNING_ACTION="lbl.warning.action";
	
	/** The Constant LBL_WARNING_ACTION_MESSAGE. */
	public static final String LBL_WARNING_ACTION_MESSAGE="lbl.warning.action.message";
	
	/** The Constant ADM_BLOCK_ENTITY_MODIFY_CONFIRM_MESSAGE. */
	public static final String ADM_BLOCK_ENTITY_MODIFY_CONFIRM_MESSAGE="adm.block.entity.modify.confirm.message";
	
	/** The Constant LBL_MODIFY_BLOCKENTITY_OK. */
	public static final String LBL_MODIFY_BLOCKENTITY_OK="lbl.modify.blockentity.ok";
	
	/** The Constant ADM_BLOCK_ENTITY_DESACTIVE_CONFIRM_MESSAGE. */
	public static final String ADM_BLOCK_ENTITY_DESACTIVE_CONFIRM_MESSAGE="adm.block.entity.desactive.confirm.message";
	
	/** The Constant ADM_BLOCK_ENTITY_ACTIVE_CONFIRM_MESSAGE. */
	public static final String ADM_BLOCK_ENTITY_ACTIVE_CONFIRM_MESSAGE="adm.block.entity.active.confirm.message";
	
	/** The Constant ADM_BLOCK_ENTITY_REQUIRED_NAMES_OR_FIRST_LASTNAME. */
	public static final String ADM_BLOCK_ENTITY_REQUIRED_NAMES_OR_FIRST_LASTNAME="adm.block.entity.required.names.or.first.lastname";
	
	/** The Constant ERROR_DOC_COUNT_TWO_ZERO_VALID. */
	public static final String ERROR_DOC_COUNT_TWO_ZERO_VALID = "error.doc.count.two.zero.valid";
	
	/** The Constant ERROR_DOC_COUNT_THREE_ZERO_VALID. */
	public static final String ERROR_DOC_COUNT_THREE_ZERO_VALID = "error.doc.count.three.zero.valid";
	
	/** The Constant ERROR_DOC_FORMAT_LAST_FOUR_DIGITS_1900. */
	public static final String ERROR_DOC_FORMAT_LAST_FOUR_DIGITS_1900 = "error.doc.format.last.four.digits.1900";
	
	/** The Constant ERROR_BLOCKENTITY_DOC_NUM_LEGTH_11. */
	public static final String ERROR_BLOCKENTITY_DOC_NUM_LEGTH_11 = "error.block.entity.docnumber.length.11";
	
	/** The Constant ERROR_BLOCKENTITY_DOC_NUM_LEGTH_9_11. */
	public static final String ERROR_BLOCKENTITY_DOC_NUM_LEGTH_9_11 = "error.block.entity.docnumber.length.11.or.9";
	
	/** The Constant ERROR_BLOCKENTITY_DOC_NUM_LEGTH_6_15. */
	public static final String ERROR_BLOCKENTITY_DOC_NUM_LEGTH_6_15= "error.block.entity.docnumber.length.6.to.15";
	
	/** The Constant ERROR_BLOCKENTITY_DOC_NUM_LEGTH_16_18_21. */
	public static final String ERROR_BLOCKENTITY_DOC_NUM_LEGTH_16_18_21 = "error.block.entity.docnumber.length.16.or.18.or.21";	
	
	/** The Constant LBL_DESACTIVE_BLOCKENTITY_OK. */
	public static final String LBL_DESACTIVE_BLOCKENTITY_OK="lbl.desactive.blockentity.ok";

	/** The Constant LBL_ACTIVATE_BLOCKENTITY_OK. */
	public static final String LBL_ACTIVATE_BLOCKENTITY_OK="lbl.activate.blockentity.ok";
	
	/** The Constant MSG_RNC_ERROR. */
	public static final String MSG_RNC_ERROR="lbl.msg.rnc.error";
	
	/** The Constant ERROR_RECORD_BLOCK_ENTITY_NOT_SELECTED. */
	public static final String ERROR_RECORD_BLOCK_ENTITY_NOT_SELECTED="error.record.block.entity.not.selected";

	/** The Constant MSG_APROVE_OPER. */
	public static final String MSG_APROVE_OPER="lbl.msg.aprobar";
	
	/** The Constant MSG_APROVE_OPER_MULTI. */
	public static final String MSG_APROVE_OPER_MULTI="lbl.msg.aprobar.multi";
	
	/** The Constant MSG_AUTHORIZED_OPER. */
	public static final String MSG_AUTHORIZED_OPER="lbl.msg.authorize";  
	
	/** The Constant MSG_AUTHORIZED_OPER_MULTI. */
	public static final String MSG_AUTHORIZED_OPER_MULTI="lbl.msg.authorize.multi";  
	
	/** The Constant MSG_APPLY_OPER. */
	public static final String MSG_APPLY_OPER="lbl.msg.aplicar";
	
	/** The Constant MSG_APPLY_OPER_MULTI. */
	public static final String MSG_APPLY_OPER_MULTI="lbl.msg.aplicar.multi";
	
	/** The Constant MSG_CONFIRM_OPER. */
	public static final String MSG_CONFIRM_OPER="lbl.retiro.msg.confirm";
	
	/** The Constant MSG_CONFIRM_OPER_MULTI. */
	public static final String MSG_CONFIRM_OPER_MULTI="lbl.retiro.msg.confirm.multi";
	
	/** The Constant MSG_ANULER_OPER. */
	public static final String MSG_ANULER_OPER="lbl.msg.anular";
	
	/** The Constant MSG_ANULER_OPER_MULTI. */
	public static final String MSG_ANULER_OPER_MULTI="lbl.msg.anular.multi";

	/** The Constant MSG_ANULER_OPER_MULTI. */
	public static final String MSG_REJECTED_OPER_MULTI="lbl.msg.reject.multi";
	
	/** The Constant MSG_ANULER_REASON_OPER. */
	public static final String MSG_ANULER_REASON_OPER="lbl.msg.anular";

	/** The Constant MSG_ANULER_REASON_OPER. */
	public static final String MSG_REJECT_REASON_OPER="lbl.msg.reject";
	
	/** The Constant MSG_ANULER_REASON_OPER_MULTI. */
	public static final String MSG_ANULER_REASON_OPER_MULTI="lbl.msg.anular.multi";

	/** The Constant MSG_ANULER_REASON_OPER_MULTI. */
	public static final String MSG_REJECT_REASON_OPER_MULTI="lbl.msg.reject.multi";
	
	/** The Constant ERR_MSG_PARTICIPANT. */
	public static final String ERR_MSG_PARTICIPANT = "adm.holderaccount.validation.participant";
	
	/** The Constant ERR_MSG_ACCOUNT. */
	public static final String ERR_MSG_ACCOUNT = "adm.holderaccount.validation.account";
	
	/** The Constant ERR_MSG_NEW_LOCATION. */
	public static final String ERR_MSG_NEW_LOCATION = "modify.location.err.msg";
	
	/** The Constant INI_DATE_GREATER_THAN_END_DATE. */
	public final static String INI_DATE_GREATER_THAN_END_DATE="error.participant.unification.initialDate.greater.than.endDate";
	
	/** The Constant END_DATE_GREATER_THAN_SYSTEM_DATE. */
	public final static String END_DATE_GREATER_THAN_SYSTEM_DATE="error.participant.unification.end.greater.than.systemDate";

	/** The Constant LBL_WARNING_BLOCK_ENTITY_EXIST_MESSAGE. */
	public static final String LBL_WARNING_BLOCK_ENTITY_EXIST_MESSAGE = "lbl.warning.block.entity.exist.message";
	
	/** The Constant LBL_WARNING_BLOCK_ENTITY_EXIST_MESSAGE2. */
	public static final String LBL_WARNING_BLOCK_ENTITY_EXIST_MESSAGE2 = "lbl.warning.block.entity.exist.message2";
	
	/** The Constant LBL_CONFIRM_MODIFY_OR_ACTIVATE_DESACTIVATE. */
	public static final String LBL_CONFIRM_MODIFY_OR_ACTIVATE_DESACTIVATE ="lbl.confirm.modify.or.activate.desactive";
	
	/** The Constant MSG_ISSUER_NOT_REGISTER. */
	public static final String MSG_ISSUER_NOT_REGISTER = "lbl.issuer.not.register";
	
	/** The Constant MSG_LOAD. */
	public static final String MSG_LOAD = "lbl.msg.load";
	
	/** The Constant MSG_ACCOUNTS_WITH_TRANSIT_BALANCE. */
	public static final String MSG_ACCOUNTS_WITH_TRANSIT_BALANCE="message.participant.unification.security.with.transit.balance";
	
	/** The Constant MSG_ACCOUNTS_WITH_NEGOTIATION_OPERATIONS. */
	public static final String MSG_ACCOUNTS_WITH_NEGOTIATION_OPERATIONS="message.participant.unification.security.with.liquidation.operation";
	
	/** The Constant MSG_PARTICIPANT_UNIFICATION_ANULLED_SUCCESS. */
	public static final String MSG_PARTICIPANT_UNIFICATION_ANULLED_SUCCESS="message.participant.unification.anulled.success";
	
	/** The Constant MSG_PARTICIPANT_UNIFICATION_PRELIMINARY_SUCCESS. */
	public static final String MSG_PARTICIPANT_UNIFICATION_PRELIMINARY_SUCCESS="message.participant.unification.preliminary.success";
	
	/** The Constant MSG_PARTICIPANT_UNIFICATION_DEFINITIVE_SUCCESS. */
	public static final String MSG_PARTICIPANT_UNIFICATION_DEFINITIVE_SUCCESS="message.participant.unification.definitive.success";
	
	/** The Constant MSG_ACCOUNTS_WIETH_ACCREDITATION_BALANCE. */
	public static final String MSG_ACCOUNTS_WIETH_ACCREDITATION_BALANCE="message.participant.unification.security.with.accreditation.balance";
	
	/** The Constant MSG_ACCOUNTS_WIETH_BUY_BALANCE. */
	public static final String MSG_ACCOUNTS_WIETH_BUY_BALANCE="message.participant.unification.security.with.buy.balance";
	
	/** The Constant MSG_ACCOUNTS_WIETH_REPORTED_BALANCE. */
	public static final String MSG_ACCOUNTS_WIETH_REPORTED_BALANCE="message.participant.unification.security.with.reported.balance";
	
	/** The Constant MSG_ACCOUNTS_WIETH_REPORTING_BALANCE. */
	public static final String MSG_ACCOUNTS_WIETH_REPORTING_BALANCE="message.participant.unification.security.with.reporting.balance";
	
	/** The Constant MSG_ACCOUNTS_WIETH_REVERSE_BALANCE. */
	public static final String MSG_ACCOUNTS_WIETH_REVERSE_BALANCE="message.participant.unification.security.with.reverse.balance";
	
	/** The Constant MSG_ACCOUNTS_WIETH_SELL_BALANCE. */
	public static final String MSG_ACCOUNTS_WIETH_SELL_BALANCE="message.participant.unification.security.with.sell.balance";
	
	/** The Constant MSG_SAVE_RETIRO. */
	public static final String MSG_SAVE_RETIRO="lbl.msg.grabar";
	
	
	
	/** The Constant ERROR_CONFIRM_RETIRE. */
	public static final String ERROR_CONFIRM_RETIRE="error.retire.confirm";
	
	/** The Constant ERROR_CANCEL_RETIRE. */
	public static final String ERROR_CANCEL_RETIRE="error.retire.anuler";
	
	/** The Constant ERROR_APPROVE_RETIRE. */
	public static final String ERROR_APPROVE_RETIRE="error.retire.approve";
	
	/** The Constant ERROR_APPLY_RETIRE. */
	public static final String ERROR_APPLY_RETIRE="error.retire.apply";
	
	/** The Constant ACCREDITATION_DELIVER_SUCCESS. */
	public static final String ACCREDITATION_DELIVER_SUCCESS = "message.accreditation.certificate.deliver.success";
	
	/** The Constant ACCREDITATION_GENERATE_CONSTANCE_SUCCESS. */
	public static final String ACCREDITATION_GENERATE_CONSTANCE_SUCCESS = "message.accreditation.certificate.generate.constance";
	
	/** The Constant ACCREDITATION_GENERATE_CONSTANCE_SUCCESS. */
	public static final String ACCREDITATION_GENERATE_CONSTANCES_SUCCESS = "message.accreditation.certificate.generate.constances";
	
	/** The Constant RETIREMENT_AUTHORIZE_PREV. */
	public static final String RETIREMENT_AUTHORIZE_PREV = "retiro.err.msg.authorize.prev";

	/** The Constant DELIVARY_DATE_ERR_MSG. */
	public static final String DELIVARY_DATE_ERR_MSG="error.accreditation.certificate.required";


	/** The Constant CERTIFICATE_DEPOSIT_ERR_MSG. */
	public static final String CERTIFICATE_DEPOSIT_ERR_MSG="deposito.err.register.msg.regaprove";
	
	/** The Constant CERTIFICATE_DEPOSIT_DPF_REGAPPROVE. */
	public static final String CERTIFICATE_DEPOSIT_DPF_REGAPPROVE="deposito.err.register.msg.dpf.regaprove";
	
	/** The Constant CERTIFICATE_DEPOSIT_ERR_DPF_INTERVAL. */
	public static final String CERTIFICATE_DEPOSIT_ERR_DPF_INTERVAL="deposito.err.register.msg.dpf.interval";
	
	
	/** The Constant CERTIFICATE_DEPOSIT_ERR_MSG_MAX. */
	public static final String CERTIFICATE_DEPOSIT_ERR_MSG_MAX= "deposito.err.register.msg.maxCount100";
	
	/** The Constant CERTIFICATE_DEPOSIT_REG_EXISTS. */
	public static final String CERTIFICATE_DEPOSIT_REG_EXISTS="deposito.err.register.msg.exists";
	
	/** The Constant CERTIFICATE_DEPOSIT_DEMATERIALIZED. */
	public static final String CERTIFICATE_DEPOSIT_DEMATERIALIZED = "deposito.err.register.msg.dematerialized";
	
	/** The Constant CERTIFICATE_DEPOSIT_DIFFERENT_DATA. */
	public static final String CERTIFICATE_DEPOSIT_DIFFERENT_DATA="deposito.err.register.msg.differ";
	
	/** The Constant ACCREDITATION_CERTIFICATE_HOLDER_WITH_NOT_ACCOUNT. */
	public static final String ACCREDITATION_CERTIFICATE_HOLDER_WITH_NOT_ACCOUNT="error.accreditation.certificate.no_accounts";
	
	/** The Constant REQUIRED_MESSAGE. */
	public static final String REQUIRED_MESSAGE="error.accreditation.certificate.required.paymentWay";
	
	/** The Constant ACCREDITATION_CERTIFICATE_HOLDER_REQUIRED. */
	public static final String ACCREDITATION_CERTIFICATE_HOLDER_REQUIRED="error.accreditation.certificate.required.holder";
	
	/** The Constant PARTICIPANT_UNION_CONSULT_BUTTONS. */
	public static final String PARTICIPANT_UNION_CONSULT_BUTTONS="error.participant.unification.consult.state";
	
	/** The Constant PARTICIPANT_REQUIRED. */
	public static final String PARTICIPANT_REQUIRED="lbl.participant.required";
	
	/** The Constant HOLDER_REQUIRED. */
	public static final String HOLDER_REQUIRED="lbl.holder.required";
	
	/** The Constant ACCOUNT_REQUIRED. */
	public static final String ACCOUNT_REQUIRED="lbl.account.required";
	
	/** The Constant PARTICIPANT_UNION_CONSULT_BUTTONS_TWO_STATE. */
	public static final String PARTICIPANT_UNION_CONSULT_BUTTONS_TWO_STATE="error.participant.unification.consult.state.with.other.state";
	
	/** The Constant PARTICIPANT_UNION_ANNULLATE_LBL. */
	public static final String PARTICIPANT_UNION_ANNULLATE_LBL="lbl.anulled";
	
	/** The Constant PARTICIPANT_UNION_REGISTERED_LBL. */
	public static final String PARTICIPANT_UNION_REGISTERED_LBL="lbl.registered";
	
	/** The Constant PARTICIPANT_UNION_EXECUTE_PRELIMINARY_LBL. */
	public static final String PARTICIPANT_UNION_EXECUTE_PRELIMINARY_LBL="lbl.preliminary";
	
	/** The Constant PARTICIPANT_UNION_EXECUTE_DEFINITIVE_LBL. */
	public static final String PARTICIPANT_UNION_EXECUTE_DEFINITIVE_LBL="lbl.definitive";
	
	/** The Constant LBL_HEADER_ALERT_WARNING. */
	public static final String LBL_HEADER_ALERT_WARNING = "lbl.header.alert.warning";
	
	/** The Constant LBL_HEADER_ALERT_DELETE. */
	public static final String LBL_HEADER_ALERT_DELETE = "lbl.header.alert.delete";
	
	/** The Constant LBL_HEADER_ALERT_CONFIRM. */
	public static final String LBL_HEADER_ALERT_CONFIRM = "lbl.header.alert.confirm";
	
	/** The Constant PARTICIPANT_UNION_MUST_SELECT_DOCUMENTS_ERROR. */
	public static final String PARTICIPANT_UNION_MUST_SELECT_DOCUMENTS_ERROR="error.participant.unification.required.documents";
	
	/** The Constant APPROVED_LBL. */
	public static final String APPROVED_LBL="lbl.approved";
	
	/** The Constant ADM_RECTIFICATION_REQUEST_CONCURRENCY. */
	public static final String ADM_RECTIFICATION_REQUEST_EXIST = "adm.rectification.request.exist";
	
	/** The Constant RECTIFICATION_CONFIRM_SAVE. */
	public static final String RECTIFICATION_CONFIRM_SAVE="adm.rectification.confirmSave";
	
	/** The Constant RECTIFICATION_CONFIRMED_SAVE. */
	public static final String RECTIFICATION_CONFIRMED_SAVE="adm.rectification.confirmedSave";
	
	/** The Constant RECTIFICATION_CONFIRM_CONFIRMATION. */
	public static final String RECTIFICATION_CONFIRM_CONFIRMATION="adm.rectification.confirm.operation";
	
	/** The Constant RECTIFICATION_COFIRNM_REJECT. */
	public static final String RECTIFICATION_COFIRNM_REJECT="adm.rectification.reject.operation";
	
	/** The Constant RECTIFICATION_CONFIRMED_CONFIRMATION. */
	public static final String RECTIFICATION_CONFIRMED_CONFIRMATION="adm.rectification.confirmed.operation";
	
	/** The Constant RECTIFICATION_REQUEST_NOT_FOUND. */
	public static final String RECTIFICATION_REQUEST_NOT_FOUND="adm.rectification.request.not.found";
	
	/** The Constant RECTIFICATION_REQUEST_NOT_CONFIRMED. */
	public static final String RECTIFICATION_REQUEST_NOT_CONFIRMED="adm.rectification.request.not.confirmed";

	/** The Constant RECTIFICATION_REJECTED_CONFIRMATION. */
	public static final String RECTIFICATION_REJECTED_CONFIRMATION="adm.rectification.rejected.operation";

	/** The Constant MESSAGE_VALIDATION_BLOCK_HOLDER_ACCOUNT. */
	public static final String MESSAGE_VALIDATION_BLOCK_HOLDER_ACCOUNT="message.validation.block.holder.account";

	/** The Constant BUSINESS_NEGATIVE_BALANCE. */
	public static final String BUSINESS_NEGATIVE_BALANCE="business.negative.balance";

	/** The Constant SECURITY_EXPIRATION_DATE_BEFORE_ACCREDITATION_EXPIRATION_DATE. */
	public static final String SECURITY_EXPIRATION_DATE_BEFORE_ACCREDITATION_EXPIRATION_DATE = "security.expiration.date.before.accreditation.expiration.date";
	
	/** The Constant IDLE_TIME_EXPIRED. */
	public static final String IDLE_TIME_EXPIRED = "idle.session.expired";
	
	
	/** The Constant ERROR_COUPON_GRANT_TRANSFER_GREATER_THAN_AVAILABLE. */
	public static final String ERROR_COUPON_GRANT_TRANSFER_GREATER_THAN_AVAILABLE="error.coupongrant.transfer.greater.than.available";
	
	/** The Constant ERROR_COUPON_GRANT_TRANSFER_GREATER_THAN_CURRENT. */
	public static final String ERROR_COUPON_GRANT_TRANSFER_GREATER_THAN_CURRENT="error.coupongrant.transfer.greater.than.current";
	
	/** The Constant MSG_COUPON_GRANT_CONFIRM_SAVE. */
	public static final String MSG_COUPON_GRANT_CONFIRM_SAVE="msg.coupongrant.confirm.save";
	
	/** The Constant MSG_COUPON_GRANT_SAVE_SUCESS. */
	public static final String MSG_COUPON_GRANT_SAVE_SUCESS="msg.coupongrant.save.sucess";
	
	/** The Constant MSG_COUPON_GRANT_CONFIRM_REJECT. */
	public static final String MSG_COUPON_GRANT_CONFIRM_REJECT="msg.coupongrant.confirm.reject";
	
	/** The Constant MSG_COUPON_GRANT_REJECT_SUCESS. */
	public static final String MSG_COUPON_GRANT_REJECT_SUCESS="msg.coupongrant.reject.sucess";
	
	/** The Constant MSG_COUPON_GRANT_CONFIRM_CONFIRM. */
	public static final String MSG_COUPON_GRANT_CONFIRM_CONFIRM="msg.coupongrant.confirm.confirm";
	
	/** The Constant MSG_COUPON_GRANT_CONFIRM_SUCESS. */
	public static final String MSG_COUPON_GRANT_CONFIRM_SUCESS="msg.coupongrant.confirm.sucess";
	
	/** The Constant ERROR_COUPON_TRANSFER_EMPTY_LIST. */
	public static final String ERROR_COUPON_TRANSFER_EMPTY_LIST="error.coupontransfer.empty.list";
	
	/** The Constant ERROR_COUPON_TRANSFER_NO_TRANSFER_LIST. */
	public static final String ERROR_COUPON_TRANSFER_NO_TRANSFER_LIST="error.coupontransfer.no.transfer.list";
	
	/** The Constant MSG_COUPON_TRANSFER_CONFIRM_SAVE. */
	public static final String MSG_COUPON_TRANSFER_CONFIRM_SAVE="msg.coupontransfer.confirm.save";
	
	/** The Constant MSG_COUPON_TRANSFER_SAVE_SUCESS. */
	public static final String MSG_COUPON_TRANSFER_SAVE_SUCESS="msg.coupontransfer.save.sucess";
	
	/** The Constant MSG_COUPON_TRANSFER_CONFIRM_REJECT. */
	public static final String MSG_COUPON_TRANSFER_CONFIRM_REJECT="msg.coupontransfer.confirm.reject";
	
	/** The Constant MSG_COUPON_TRANSFER_REJECT_SUCESS. */
	public static final String MSG_COUPON_TRANSFER_REJECT_SUCESS="msg.coupontransfer.reject.sucess";
	
	/** The Constant MSG_COUPON_TRANSFER_CONFIRM_CONFIRM. */
	public static final String MSG_COUPON_TRANSFER_CONFIRM_CONFIRM="msg.coupontransfer.confirm.confirm";
	
	/** The Constant MSG_COUPON_TRANSFER_CONFIRM_SUCESS. */
	public static final String MSG_COUPON_TRANSFER_CONFIRM_SUCESS="msg.coupontransfer.confirm.sucess";
	
	/** The Constant TRANSFER_SUCCES_APPROVE. */
	public static final String TRANSFER_SUCCES_APPROVE_VD = "tvd.label.message.success.approve.vd";//=Se Aprobo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_ANNULER. */
	public static final String TRANSFER_SUCCES_ANNULER_VD = "tvd.label.message.success.anuller.vd";//=Se Anulo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_REJECT. */
	public static final String TRANSFER_SUCCES_REJECT_VD = "tvd.label.message.success.reject.vd";//=Se Rechazo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_CONFIRM_B. */
	public static final String TRANSFER_SUCCES_CONFIRM_B_VD = "tvd.label.message.success.confirm.b.vd";//=Se Confirmo exitosamente la solicitud de traspaso
	
	/** The Constant TRANSFER_SUCCES_CONFIRM_D. */
	public static final String TRANSFER_SUCCES_CONFIRM_D_VD = "tvd.label.message.success.confirm.d.vd";//=Se Confirmo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_REVIEW. */
	public static final String TRANSFER_SUCCES_REVIEW_VD = "tvd.label.message.success.review.vd";//=Se Reviso exitosamente la solicitud N° {0}

	/** The Constant TRANSFER_SUCCES_APPROVE. */
	public static final String TRANSFER_SUCCES_APPROVE_VB = "tvd.label.message.success.approve.vb";//=Se Aprobo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_ANNULER. */
	public static final String TRANSFER_SUCCES_ANNULER_VB = "tvd.label.message.success.anuller.vb";//=Se Anulo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_REJECT. */
	public static final String TRANSFER_SUCCES_REJECT_VB = "tvd.label.message.success.reject.vb";//=Se Rechazo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_CONFIRM_B. */
	public static final String TRANSFER_SUCCES_CONFIRM_B_VB = "tvd.label.message.success.confirm.b.vb";//=Se Confirmo exitosamente la solicitud de traspaso
	
	/** The Constant TRANSFER_SUCCES_CONFIRM_D. */
	public static final String TRANSFER_SUCCES_CONFIRM_D_VB = "tvd.label.message.success.confirm.d.vb";//=Se Confirmo exitosamente la solicitud N° {0}
	
	/** The Constant TRANSFER_SUCCES_REVIEW. */
	public static final String TRANSFER_SUCCES_REVIEW_VB = "tvd.label.message.success.review.vb";//=Se Reviso exitosamente la solicitud N° {0}

	/** *******. */
	/** The Constant TRANSFER_QUESTION_MULTIPLE_SAVE. */
	public static final String TRANSFER_QUESTION_MULTIPLE_SAVE_VD = "tdv.msg.question.multiple.save.vd";
	
	/** The Constant TRANSFER_QUESTION_UNIQUE_SAVE. */
	public static final String TRANSFER_QUESTION_UNIQUE_SAVE_VD = "tdv.msg.question.unique.save.vd";

	/** The Constant TRANSFER_QUESTION_MULTIPLE_SAVE. */
	public static final String TRANSFER_QUESTION_MULTIPLE_SAVE_VB = "tdv.msg.question.multiple.save.vb";
	
	/** The Constant TRANSFER_QUESTION_UNIQUE_SAVE. */
	public static final String TRANSFER_QUESTION_UNIQUE_SAVE_VB = "tdv.msg.question.unique.save.vb";
	
	/** The Constant TRANSFER_NOT_EXECUTE_ACTION. */
	public static final String TRANSFER_NOT_EXECUTE_ACTION = "tdv.msg.alert.not.execute.action";
	
	/** The Constant TRANSFER_MOTIVE_ANULLED_REJECT. */
	public static final String TRANSFER_MOTIVE_ANULLED_REJECT = "tdv.msg.motive.anulled.reject";
	
	/** The Constant TRANSFER_NOT_TITULAR_CONTRAPART. */
	public static final String TRANSFER_NOT_TITULAR_CONTRAPART = "tdv.msg.alert.not.titular.contrapart";
	
	/** The Constant TRANSFER_CONFIRM_REGISTRY. */
	public static final String TRANSFER_CONFIRM_REGISTRY = "tdv.msg.alert.confirm.registry.trasnferAvailable";

	/** The Constant TRANSFER_CONFIRM_REGISTRY. */
	public static final String TRANSFER_NOT_NEGOTIABLE_SECURITY = "tdv.msg.alert.confirm.registry.not.negotiable.security";
	
	/** The Constant TRANSFER_NOT_SELECT_SECURITY. */
	public static final String TRANSFER_NOT_SELECT_SECURITY = "tdv.msg.alert.not.select.security";
	
	public static final String EXPIRATION_DATE_SECURITY = "tdv.msg.alert.expiration.date.security";
	
	/** The Constant ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT. */
	public static final String ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT = "error.message.issuer.dpf.not.have.participant";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_CONFIRM. */
	public static final String ERROR_MESSAGE_INVALID_USER_CONFIRM = "error.message.invalid.user.confirm";
	
	public static final String ERROR_MESSAGE_INVALID = "error.message.inalid.transfer.confirm";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_ANNUL. */
	public static final String ERROR_MESSAGE_INVALID_USER_ANNUL = "error.message.invalid.user.annul";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_REVISE. */
	public static final String ERROR_MESSAGE_INVALID_USER_REVISE = "error.message.invalid.user.revise";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_APPROVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_APPROVE = "error.message.invalid.user.approve";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_MODIFY. */
	public static final String ERROR_MESSAGE_INVALID_USER_MODIFY = "error.message.invalid.user.modify";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_ACTIVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_ACTIVE = "error.message.invalid.user.active";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_DESACTIVE. */
	public static final String ERROR_MESSAGE_INVALID_USER_DESACTIVE = "error.message.invalid.user.desactive";
	
	/** The Constant ERROR_MESSAGE_INVALID_USER_REJECT. */
	public static final String ERROR_MESSAGE_INVALID_USER_REJECT = "error.message.invalid.user.reject";
	
	/** The Constant TRANSFER_NOT_SELECT_AMOUNT_REGISTER. */
	public static final String TRANSFER_NOT_SELECT_AMOUNT_REGISTER = "tdv.msg.alert.not.select.amountregister";
	
	/** The Constant TRANSFER_NOT_SELECT_BLOCK_DOCUMENT. */
	public static final String TRANSFER_NOT_SELECT_BLOCK_DOCUMENT = "tdv.msg.alert.not.select.block.document";	
	
	/** The Constant TRANSFER_NOT_INSERT_SECURITY. */
	public static final String TRANSFER_NOT_INSERT_SECURITY="tdv.msg.alert.not.insert.security";
	
	/**  START DETACHMENT COUPONS *. */
	public static final String SPLIT_COUPON_ASK_REGISTER = "alt.splitcoupons.askRegister";
	
	/** The Constant SPLIT_COUPON_SUCCES_REGISTER. */
	public static final String SPLIT_COUPON_SUCCESS_REGISTER = "alt.splitcoupons.register";
	
	/** The Constant SPLIT_COUPON_ASK_APPROVE. */
	public static final String SPLIT_COUPON_ASK_APPROVE = "alt.splitcoupons.askApprove";
	
	/** The Constant SPLIT_COUPON_SUCCES_APPROVE. */
	public static final String SPLIT_COUPON_SUCCESS_APPROVE = "alt.splitcoupons.approve";
	
	/** The Constant SPLIT_COUPON_ASK_ANNUL. */
	public static final String SPLIT_COUPON_ASK_ANNUL = "alt.splitcoupons.askAnnul";
	
	/** The Constant SPLIT_COUPON_SUCCES_ANNUL. */
	public static final String SPLIT_COUPON_SUCCESS_ANNUL = "alt.splitcoupons.annul";
	
	/** The Constant SPLIT_COUPON_ASK_CONFIRM. */
	public static final String SPLIT_COUPON_ASK_CONFIRM = "alt.splitcoupons.askConfirm";
	
	/** The Constant SPLIT_COUPON_SUCCES_CONFIRM. */
	public static final String SPLIT_COUPON_SUCCESS_CONFIRM = "alt.splitcoupons.confirm";
	
	/** The Constant SPLIT_COUPON_ASK_REJECT. */
	public static final String SPLIT_COUPON_ASK_REJECT = "alt.splitcoupons.askReject";
	
	/** The Constant SPLIT_COUPON_SUCCES_REJECT. */
	public static final String SPLIT_COUPON_SUCCESS_REJECT = "alt.splitcoupons.reject";
	
	/** The Constant SPLIT_COUPON_RECORD_NOT_SELECTED. */
	public static final String SPLIT_COUPON_RECORD_NOT_SELECTED = "alt.splitcoupons.record.not.selected";
	
	/** The Constant SPLIT_COUPON_INVALID_STATE_REGISTER. */
	public static final String SPLIT_COUPON_INVALID_STATE_REGISTER = "alt.splitcoupons.invalid.state.register";
	
	/** The Constant SPLIT_COUPON_INVALID_STATE_APPROVE. */
	public static final String SPLIT_COUPON_INVALID_STATE_APPROVE = "alt.splitcoupons.invalid.state.approve";
	
	/** The Constant SPLIT_COUPON_ERROR_COUPON_NOT_SELECTED. */
	public static final String SPLIT_COUPON_ERROR_COUPON_NOT_SELECTED = "alt.splitcoupons.error.coupon.not.selected";
	
	/** The Constant SPLIT_COUPON_NO_BALANCE. */
	public static final String SPLIT_COUPON_NO_BALANCE = "alt.splitcoupons.no.balance";
	
	/** The Constant SPLIT_COUPON_NO_PENDING_COUPONS. */
	public static final String SPLIT_COUPON_NO_PENDING_COUPONS = "alt.splitcoupons.no.coupons";
	
	/** The Constant SPLIT_COUPON_ACCOUNT_NOT_SELECTED. */
	public static final String SPLIT_COUPON_ACCOUNT_NOT_SELECTED = "alt.splitcoupons.account.not.selected";
	
	/** The Constant SPLIT_COUPON_SECURITY_NOT_FIXED_INCOME. */
	public static final String SPLIT_COUPON_SECURITY_NOT_FIXED_INCOME = "alt.splitcoupons.security.not.fixedincome";
	
	/** The Constant SPLIT_COUPON_SECURITY_EARLY_REDEMPTION. */
	public static final String SPLIT_COUPON_SECURITY_EARLY_REDEMPTION = "alt.splitcoupons.security.error.early.redemption";
	
	/** The Constant SPLIT_COUPON_SECURITY_HASNT_SPLIT. */
	public static final String SPLIT_COUPON_SECURITY_HASNT_SPLIT = "alt.splitcoupons.security.error.hasnt.split";
	
	/** The Constant SPLIT_COUPON_SECURITY_IS_COUPON. */
	public static final String SPLIT_COUPON_SECURITY_IS_COUPON = "alt.splitcoupons.security.error.is.coupon";
	
	/** The Constant SPLIT_COUPON_SECURITY_BLOCKED. */
	public static final String SPLIT_COUPON_SECURITY_BLOCKED = "alt.splitcoupons.security.blocked";
	
	/** The Constant SPLIT_COUPON_HOLDER_BLOCKED. */
	public static final String SPLIT_COUPON_HOLDER_BLOCKED = "alt.splitcoupons.holder.blocked";
	
	/** The Constant SPLIT_COUPON_PARTICIPANT_REQUIRED. */
	public static final String SPLIT_COUPON_PARTICIPANT_REQUIRED = "alt.splitcoupons.participant.required";
	
	/** The Constant SPLIT_COUPON_HOLDER_NOT_ACCOUNTS. */
	public static final String SPLIT_COUPON_HOLDER_NOT_ACCOUNTS = "alt.splitcoupons.holder.error.accounts";
	
	/** The Constant SPLIT_COUPON_FILE_REQUIRED. */
	public static final String SPLIT_COUPON_FILE_REQUIRED = "alt.splitcoupons.file.required";	
	
	/** The Constant SPLIT_COUPON_QUANTITY_REQUIRED. */
	public static final String SPLIT_COUPON_QUANTITY_REQUIRED = "alt.splitcoupons.quantity.required";
	
	/** The Constant SPLIT_COUPON_MECHANISM_OPERATION_QUANTITY_EMPTY. */
	public static final String SPLIT_COUPON_MECHANISM_OPERATION_QUANTITY_EMPTY = "alt.splitcoupons.mechanismoperation.quantity.empty";
	/** END DETACHMENT COUPONS *. */	
	
	/** START ACCOUNT ANNOTATION **/
	public static final String ACCOUNT_ANNOTATION_MASSIVE_LABEL="icustody.dematerializationsecurities.massive";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_REGISTER. */
	public static final String ACCOUNT_ANNOTATION_ASK_REGISTER = "alt.accountannotation.askRegister";

	public static final String ACCOUNT_IMMOBILIZATION_ASK_REGISTER = "alt.inmobilization.askRegister";
	
	public static final String ALT_ACCOUNTANNOTATION_ASKREGISTER_NO_ATTACHMENT="alt.accountannotation.askRegister.no.attachment";
	
	public static final String ALT_ACCOUNTANNOTATION_ASKREGISTER_EMPTY_NUMCERTIFICATE="alt.accountannotation.askRegister.empty.numCertificate";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_REGISTER_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_REGISTER_BANK_SHARES = "alt.accountannotation.askRegister.bank.shares";
	
	public static final String ALT_ACCOUNTANNOTATION_ASKREGISTER_BANK_SHARES_NO_ATTACHMENT="alt.accountannotation.askRegister.bank.shares.no.attachment";
	
	public static final String ALT_ACCOUNTANNOTATION_ASKREGISTER_BANK_SHARES_EMPTY_NUMCERTIFICATE="alt.accountannotation.askRegister.bank.shares.empty.numCertificate";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_REGISTER. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_REGISTER = "alt.accountannotation.register";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_APPROVE. */
	public static final String ACCOUNT_ANNOTATION_ASK_APPROVE = "alt.accountannotation.askApprove";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_APPROVE_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_APPROVE_BANK_SHARES = "alt.accountannotation.askApprove.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_APPROVE. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_APPROVE = "alt.accountannotation.approve";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_ANNUL. */
	public static final String ACCOUNT_ANNOTATION_ASK_ANNUL = "alt.accountannotation.askAnnul";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_ANNUL_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_ANNUL_BANK_SHARES = "alt.accountannotation.askAnnul.bank.shares";
		
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_ANNUL. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_ANNUL = "alt.accountannotation.annul";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_CONFIRM. */
	public static final String ACCOUNT_ANNOTATION_ASK_CONFIRM = "alt.accountannotation.askConfirm";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CONFIRM. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CONFIRM = "alt.accountannotation.confirm";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_REJECT. */
	public static final String ACCOUNT_ANNOTATION_ASK_REJECT = "alt.accountannotation.askReject";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_REJECT_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_REJECT_BANK_SHARES = "alt.accountannotation.askReject.bank.shares";
		
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_REJECT. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_REJECT = "alt.accountannotation.reject";
	
	/** The Constant ACCOUNT_ANNOTATION_SECURITY_EMPTY. */
	public static final String ACCOUNT_ANNOTATION_SECURITY_EMPTY = "alt.accountannotation.security.empty";
	
	/** The Constant ACCOUNT_ANNOTATION_PHYSICAL_NOT_ENOUGH. */
	public static final String ACCOUNT_ANNOTATION_PHYSICAL_NOT_ENOUGH = "alt.accountannotation.physical.balance.not.enough";
	
	/** The Constant ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH. */
	public static final String ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH = "alt.accountannotation.issuance.balance.not.enough";
	
	/** The Constant ACCOUNT_ANNOTATION_SECURITY_BLOCKED. */
	public static final String ACCOUNT_ANNOTATION_SECURITY_BLOCKED = "alt.accountannotation.security.blocked";
	
	/** The Constant ACCOUNT_ANNOTATION_SECURITY_FOREIGN. */
	public static final String ACCOUNT_ANNOTATION_SECURITY_FOREIGN = "alt.accountannotation.security.foreign";
	
	/** The Constant ACCOUNT_ANNOTATION_SECURITY_DEMATERIALIZED. */
	public static final String ACCOUNT_ANNOTATION_SECURITY_DEMATERIALIZED = "alt.accountannotation.security.dematerialized";
	
	/** The Constant ACCOUNT_ANNOTATION_ISSUER_BLOCKED. */
	public static final String ACCOUNT_ANNOTATION_ISSUER_BLOCKED = "alt.accountannotation.issuer.blocked";
	
	/** The Constant ACCOUNT_ANNOTATION_HOLDER_BLOCKED. */
	public static final String ACCOUNT_ANNOTATION_HOLDER_BLOCKED = "alt.accountannotation.holder.blocked";
	
	/** The Constant ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS. */
	public static final String ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS = "alt.accountannotation.holder.error.accounts";
	
	/** The Constant ACCOUNT_ANNOTATION_HOLDER_HOLDER_ISSUER. */
	public static final String ACCOUNT_ANNOTATION_HOLDER_HOLDER_ISSUER = "alt.accountannotation.holder.error.validateHolder";
	
	/** The Constant ACCOUNT_ANNOTATION_ACCOUNT_NOT_SELECTED. */
	public static final String ACCOUNT_ANNOTATION_ACCOUNT_NOT_SELECTED = "alt.accountannotation.account.not.selected";
	
	/** The Constant ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED. */
	public static final String ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED = "alt.accountannotation.record.not.selected";
	
	/** The Constant ACCOUNT_ANNOTATION_INVALID_STATE_APPROVE. */
	public static final String ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED = "alt.accountannotation.invalid.state.approve";
	
	/** The Constant ACCOUNT_ANNOTATION_INVALID_STATE_APPROVE. */
	public static final String ACCOUNT_ANNOTATION_INVALID_MOTIVE = "alt.accountannotation.confirm.validateMotive";
	
	/** The Constant ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER. */
	public static final String ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER = "alt.accountannotation.invalid.state.register";
	
	public static final String ACCOUNT_ANNOTATION_INVALID_STATE_REJECT = "alt.accountannotation.invalid.state.reject";
	
	/** The Constant ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED. */
	public static final String ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED = "alt.accountannotation.participant.required";
	
	/** The Constant ACCOUNT_ANNOTATION_DPF_EXIST. */
	public static final String ACCOUNT_ANNOTATION_DPF_EXIST = "alt.accountannotation.dpf.exist";
	
	public static final String ALT_ACCOUNTANNOTATION_SECURITY_NO_EXIST="alt.accountannotation.security.no.exist";
	
	/** The Constant ACCOUNT_ANNOTATION_DPF_EXIST_DEMATERIALIZED. */
	public static final String ACCOUNT_ANNOTATION_DPF_EXIST_DEMATERIALIZED = "alt.accountannotation.dpf.exist.dematerialized";
	
	/** The Constant ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH ACCUMULATE. */
	public static final String ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH_ACCUMULATE = "alt.accountannotation.issuance.balance.not.enough.acumulate";
	
	/** The Constant ACCOUNT_ANNOTATION_INVALID_MOTIVE_FOR_SECURITY. */
	public static final String ACCOUNT_ANNOTATION_INVALID_MOTIVE_FOR_SECURITY = "alt.accountannotation.security.invalid.motive";
	
	/** The Constant COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT. */
	public static final String COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT = "commons.label.participant.cui.incorrect";
	
	/** The Constant MESSAGES_ALERT. */
	public static final String MESSAGES_ALERT = "messages.alert";
	
	/** The Constant MESSAGE_BACK. */
	public static final String MESSAGE_BACK = "message.back";
	
	/** The Constant INVALID_REQUEST_REPORTED_ACCREDITATION_DATE_EXPIRATION_MESSAGE. */
	public final static String INVALID_REQUEST_REPORTED_ACCREDITATION_DATE_EXPIRATION_MESSAGE="invalid.request.reported.accreditation.dateexpiration.message";
	
	/** The Constant ALERT_NO_ACCOUNT_SELECTED. */
	public final static String ALERT_NO_ACCOUNT_SELECTED = "transferVD.alert.noAccountSelected";
	
	/** The Constant ALERT_NO_ACCOUNT_SELECTED_SOURCE. */
	public final static String ALERT_NO_ACCOUNT_SELECTED_SOURCE = "transferVD.alert.noAccountSelected.source";
	
	/** The Constant ALERT_NO_ACCOUNT_SELECTED_TARGET. */
	public final static String ALERT_NO_ACCOUNT_SELECTED_TARGET = "transferVD.alert.noAccountSelected.target";
	
	/** The Constant ALERT_PARTICIPANT_ACCOUNT_NO_RELATION. */
	public final static String ALERT_PARTICIPANT_ACCOUNT_NO_RELATION = "lbl.error.msg.participant.account.notRelation";	
	
	/** The Constant ALERT_ORGIN_RNT_ACCOUNT_NO_RELATION. */
	public final static String ALERT_ORGIN_RNT_ACCOUNT_NO_RELATION = "lbl.error.msg.origin.rnt.account.notRelation";

	/** The Constant ALERT_SECURITY_STATE_INVALID. */
	public final static String ALERT_SECURITY_STATE_INVALID= "lbl.error.msg.security.state.invalid";
	
	public final static String ALERT_SECURITY_STATE_INVALID_GUARDA_EXCLUSIVE = "lbl.error.msg.security.state.invalid.guarda.exclusive";
	
	/** The Constant ALERT_ACCOUNT_IS_EQUAL. */
	public final static String ALERT_ACCOUNT_IS_EQUAL= "lbl.error.msg.accounts.is.equals";

	/** The Constant ALERT_ANULL. */
	public final static String ALERT_ANULL= "lbl.error.msg.anull";

	/** The Constant ALERT_ANULLED. */
	public final static String ALERT_ANULLED= "lbl.error.msg.anulled";

	/** The Constant ALERT_REJECT. */
	public final static String ALERT_REJECT= "lbl.error.msg.reject";

	/** The Constant ALERT_REJECTED. */
	public final static String ALERT_REJECTED= "lbl.error.msg.rejected";
	
	/** The Constant MSG_CONTRAPART_NOT_EXIST_ACCOUNT. */
	public final static String MSG_CONTRAPART_NOT_EXIST_ACCOUNT= "lbl.error.msg.contrapart.notexit.account";

	/** The Constant ALERT_ACCOUNT_SECURITY_NO_RELATION. */
	public final static String ALERT_ACCOUNT_SECURITY_NO_RELATION= "lbl.error.msg.account.security.notRelation";
	
	/** The Constant ALERT_SECURITY_NO_RELATION_PARTICIPANT_AND_ACCOUNT. */
	public final static String ALERT_SECURITY_NO_RELATION_PARTICIPANT_AND_ACCOUNT= "lbl.error.msg.security.notRelation.participant.and.account";
	
	/** The Constant ALERT_SECURITY_EXIST_IN_REGISTRY. */
	public final static String ALERT_SECURITY_EXIST_IN_REGISTRY= "lbl.error.msg.security.exist.in.registry";

	/** The Constant ALERT_AMOUNT_NOT_INSERT_IN_REGISTRY. */
	public final static String ALERT_AMOUNT_NOT_INSERT_IN_REGISTRY= "lbl.error.msg.ammount.not.insert.registry";

	/** The Constant ALERT_AMOUNT_NOT_EQUALS_IN_REGISTRY. */
	public final static String ALERT_AMOUNT_NOT_EQUALS_IN_REGISTRY= "lbl.error.msg.ammount.not.equals.registry";
	
	/** The Constant ALERT_ACCOUNT_NOT_SELECTED. */
	public final static String ALERT_ACCOUNT_NOT_SELECTED= "lbl.error.msg.accounts.not.selected";

	/** The Constant ALERT_CUI_NOT_REGISTER. */
	public final static String ALERT_CUI_NOT_REGISTER= "lbl.error.msg.cui.not.register";

	/** The Constant ALERT_MARKETFACT_NOT_SELECTED_IN_TARGET_SOURCE. */
	public final static String ALERT_MARKETFACT_NOT_SELECTED_IN_TARGET_SOURCE= "lbl.error.msg.marketfact.not.selected.in.target.source";

	/** The Constant ALERT_ISSUER_TYPE_IN_SECURITY_INCORRECT. */
	public final static String ALERT_ISSUER_TYPE_IN_SECURITY_INCORRECT= "lbl.error.msg.issuer.type.in.security.incorrect";
	
	/** The Constant ALERT_MARKETFACT_NOT_SELECTED. */
	public final static String ALERT_MARKETFACT_NOT_SELECTED= "tdv.msg.alert.not.select.marketfact";
	
	/** The Constant MSG_BLOCKS_DOCUMENTS. */
	public final static String MSG_BLOCKS_DOCUMENTS= "lbl.error.msg.blocks.documents";

	/** The Constant MSG_BLOCK_DOCUMENT. */
	public final static String MSG_BLOCK_DOCUMENT= "lbl.error.msg.block.document";

	/** The Constant MSG_STATE_INVALID. */
	public final static String MSG_STATE_INVALID= "lbl.error.msg.state.invalid";
	
	/** The Constant MSG_APPROVE. */
	public final static String MSG_APPROVE= "lbl.error.msg.approve";

	/** The Constant MSG_REVIEW. */
	public final static String MSG_REVIEW= "lbl.error.msg.review";

	/** The Constant MSG_CONFIRM. */
	public final static String MSG_CONFIRM= "lbl.error.msg.confirm";

	/** The Constant MSG_MOTIVE_OF. */
	public final static String MSG_MOTIVE_OF= "lbl.error.msg.motive.of";

	/** The Constant MSG_QUESTION_ANULLED_OPERATION. */
	public final static String MSG_QUESTION_ANULLED_OPERATION= "lbl.error.msg.question.anulled.operation";

	/** The Constant MSG_QUESTION_REJECTED_OPERATION. */
	public final static String MSG_QUESTION_REJECTED_OPERATION= "lbl.error.msg.question.rejected.operation";
	
	/** The Constant MSG_IN_REQUEST. */
	public final static String MSG_IN_REQUEST= "lbl.error.msg.in.request";

	/** The Constant MSG_AMOUNTS_INVALID_CHANGES. */
	public final static String MSG_AMOUNTS_INVALID_CHANGES= "lbl.error.msg.amounts.invalid.changes";

	/** The Constant MSG_QUESTION_EXIST_SOLICITUD_TRANSFER_BLOCK. */
	public final static String MSG_QUESTION_EXIST_SOLICITUD_TRANSFER_BLOCK= "lbl.question.msg.exist.solicitud.transfer.block";
	
	/** The Constant MSG_QUESTION_EXIST_SOLICITUD_TRANSFER_BLOCK_ALL. */
	public final static String MSG_QUESTION_EXIST_SOLICITUD_TRANSFER_BLOCK_ALL= "lbl.question.msg.exist.solicitud.transfer.block.all";
	
	/** The Constant MSG_QUESTION_REGISTRY_TRANSFER_BLOCK. */
	public final static String MSG_QUESTION_REGISTRY_TRANSFER_BLOCK= "lbl.question.msg.registry.solicitud.transfer.block";

	/** The Constant MSG_REQUEST_NUMBER_NOT_EXIST. */
	public final static String MSG_REQUEST_NUMBER_NOT_EXIST= "lbl.error.msg.request.number.not.exist";
	
	/** The Constant ERROR_TRANSFER_STATE_FOR_APROVE. */
	public static final String ERROR_TRANSFER_STATE_FOR_APROVE="lbl.error.msg.transfer.state.for.approve";
	
	/** The Constant ERROR_TRANSFER_STATE_FOR_ANULL. */
	public static final String ERROR_TRANSFER_STATE_FOR_ANULL="lbl.error.msg.transfer.state.for.anull";
	
	/** The Constant ERROR_TRANSFER_STATE_FOR_REVIEW. */
	public static final String ERROR_TRANSFER_STATE_FOR_REVIEW="lbl.error.msg.transfer.state.for.review";
	
	/** The Constant ERROR_TRANSFER_STATE_FOR_REJECT. */
	public static final String ERROR_TRANSFER_STATE_FOR_REJECT="lbl.error.msg.transfer.state.for.reject";
	
	/** The Constant ERROR_TRANSFER_STATE_FOR_CONFIRM. */
	public static final String ERROR_TRANSFER_STATE_FOR_CONFIRM="lbl.error.msg.transfer.state.for.confirm";
	
	/** The Constant LBL_MSG_EXCEL_FILE. */
	public static final String LBL_MSG_EXCEL_FILE="lbl.msg.file.excel";
	
	/** The Constant LBL_MSG_WORD_FILE. */
	public static final String LBL_MSG_WORD_FILE="lbl.msg.file.word";
	
	/** The Constant LBL_MSG_PDF_FILE. */
	public static final String LBL_MSG_PDF_FILE="lbl.msg.file.pdf";

	/** The Constant LBL_MSG_UNKNOW_FILE. */
	public static final String LBL_MSG_UNKNOW_FILE="lbl.msg.file.unknow";
	
	/** The Constant LBL_MSG_SUSTENT_SELECTED. */
	public static final String LBL_MSG_SUSTENT_SELECTED="lbl.error.msg.sustent.selected";

	/** The Constant LBL_MSG_INVALID_OPERATION_PHYSICAL_CERTIFICATE_IN_BANK. */
	public static final String LBL_MSG_INVALID_OPERATION_PHYSICAL_CERTIFICATE_IN_BANK="lbl.error.msg.invalid.operation.physical.in.bank";

	/** The Constant LBL_MSG_OPERATION_NOT_MODIFY_SITUATION. */
	public static final String LBL_MSG_OPERATION_NOT_MODIFY_SITUATION="lbl.error.msg.this.operation.not.modify.situation";

	/** The Constant LBL_MSG_ERROR_UNIFICATION_STATE_INVALID. */
	public static final String LBL_MSG_ERROR_UNIFICATION_STATE_INVALID="error.participant.unification.state.invalid";
	
	/** The Constant LBL_MSG_ERROR_UNIFICATION_STATE_INPROCESS_INVALID. */
	public static final String LBL_MSG_ERROR_UNIFICATION_STATE_INPROCESS_INVALID="error.participant.unification.state.inprocess.invalid";

	/** The Constant LBL_ERROR_TRANSFER_SOLICITUD_REQUIRED. */
	public static final String LBL_ERROR_TRANSFER_SOLICITUD_REQUIRED="lbl.error.transfer.solicitud.required";
	
	/** The Constant LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW. */
	public static final String LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW="lbl.error.marketfact.unknow";

	/** The Constant LBL_ERROR_MARKETFACT_AMOUNT_INVALID. */
	public static final String LBL_ERROR_MARKETFACT_AMOUNT_INVALID="lbl.error.marketfact.amount.invalid";
	
	/** The Constant LBL_ERROR_TRANSFER_AMOUNT_INVALID. */
	public static final String LBL_ERROR_TRANSFER_AMOUNT_INVALID="lbl.error.transfer.amount.invalid";

	/** The Constant LBL_ERROR_TRANSFER_ACCOUNT_IN_STATES. */
	public static final String LBL_ERROR_TRANSFER_ACCOUNT_IN_STATES="lbl.error.transfer.account.in.states";

	/** The Constant LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT. */
	public static final String LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT="deposito.cert.manage.marketfact.not.insert";
	
	/** The Constant LBL_ERROR_DEPOSIT_AMOUNT_DEPOSIT_EXCEED. */
	public static final String LBL_ERROR_DEPOSIT_AMOUNT_DEPOSIT_EXCEED="deposito.cert.manage.excedded.depositAmount";

	/** The Constant LBL_ERROR_DEPOSIT_COUNT_SECURITY_INVALID. */
	public static final String LBL_ERROR_DEPOSIT_COUNT_SECURITY_INVALID="lbl.error.deposit.count.security.invalid";

	/** The Constant LBL_ERROR_DEPOSIT_MARKETFACT_EXIST. */
	public static final String LBL_ERROR_DEPOSIT_MARKETFACT_EXIST="lbl.error.deposit.marketfact.exist";
	
	//msg.validation.required.attached
	public static final String MSG_VALIDATION_REQUIRED_ATTACHED="msg.validation.required.attached"; 
	
	/** The Constant RECTIFICATION_WARNING_OPERATION_CONFIRM_INVALID. */
	public static final String  RECTIFICATION_WARNING_OPERATION_CONFIRM_INVALID = "rectification.warning.operation.confirm.invalid";
	
	/** The Constant RECTIFICATION_WARNING_OPERATION_REJECT_INVALID. */
	public static final String  RECTIFICATION_WARNING_OPERATION_REJECT_INVALID = "rectification.warning.operation.confirm.invalid";
	
	/** The Constant MSG_ACCOUNTS_WITH_BALANCES_OPERATIONS_INVALID. */
	public static final String MSG_ACCOUNTS_WITH_BALANCES_OPERATIONS_INVALID = "message.participant.unification.security.with.balances.operations.incorrect";

	/** The Constant LBL_ERROR_MESSAGE_REBLOCK_IN_TRANSFER. */
	public static final String LBL_ERROR_MESSAGE_REBLOCK_IN_TRANSFER = "lbl.error.message.reblock.in.transferAvailable";
	
	public static final String LBL_ERROR_MESSAGE_REBLOCK_TRANSFER_AUTORIZE = "lbl.error.message.transferAvailable.not.autorize";
	
	/** The Constant SPLIT_COUPON_HOLDER_INVALID_STATE. */
	public static final String SPLIT_COUPON_HOLDER_INVALID_STATE = "alt.splitcoupons.holder.invalid.state";

	/** The Constant MESSAGES_WARNING. */
	public static final String MESSAGES_WARNING="messages.warning";
	
	/** The Constant ADM_HOLDERACCOUNT_ANULATE_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_ANULATE_BLOCK="adm.holderaccount.anulate.block";
	
	/** The Constant ADM_HOLDERACCOUNT_REJECT_BLOCK. */
	public static final String ADM_HOLDERACCOUNT_REJECT_BLOCK="adm.holderaccount.reject.block";
	
	/** The Constant ADM_HOLDERACCOUNT_VALIDATION_DOACTION. */
	public static final String ADM_HOLDERACCOUNT_VALIDATION_DOACTION= "adm.holderaccount.validation.doaction";
	
	/** The Constant MSGE_ACCREDITATION_CERTIFICATE_WRONG_BALANCE. */
	public static final String MSGE_ACCREDITATION_CERTIFICATE_WRONG_BALANCE = "message.accreditation.certificate.wrong.balance";
	
	/** The Constant MESSAGE_ACCREDITATION_CERTIFICATES_UNBLOCK. */
	public static final String MESSAGE_ACCREDITATION_CERTIFICATES_UNBLOCK = "message.accreditation.certificates.unblock";
	
	/** The Constant MESSAGE_ACCREDITATION_CERTIFICATES_CONFIRM. */
	public static final String MESSAGE_ACCREDITATION_CERTIFICATES_CONFIRM = "message.accreditation.certificates.confirm";
	
	/** The Constant ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED. */
	public static final String ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED = "alt.accountannotation.invalid.state.reviewed";
	
	/** The Constant ACCOUNT_ANNOTATION_INVALID_STATE_CERTIFIED. */
	public static final String ACCOUNT_ANNOTATION_INVALID_STATE_CERTIFIED = "alt.accountannotation.invalid.state.certified";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_REVIEW. */
	public static final String ACCOUNT_ANNOTATION_ASK_REVIEW = "alt.accountannotation.askReview";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_REVIEW_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_REVIEW_SHARES = "alt.accountannotation.askReview.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_ANNUL_REQUEST. */
	public static final String ACCOUNT_ANNOTATION_ANNUL_REQUEST="alt.accountannotation.annul.request";
	
	/** The Constant ACCOUNT_ANNOTATION_REJECT_REQUEST. */
	public static final String ACCOUNT_ANNOTATION_REJECT_REQUEST="alt.accountannotation.reject.request";
	
	/** The Constant ACCOUNT_ANNOTATION_OTHER_MOTIVE. */
	public static final String ACCOUNT_ANNOTATION_OTHER_MOTIVE= "alt.accountannotation.validation.other.motive";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_REVIEW. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_REVIEW = "alt.accountannotation.review.sucess";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_CERTIFY. */
	public static final String ACCOUNT_ANNOTATION_ASK_CERTIFY = "alt.accountannotation.askCertify";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_CERTIFY_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_CERTIFY_BANK_SHARES = "alt.accountannotation.askCertify.bank.shares";
	
	public static final String ACCOUNT_ANNOTATION_LBL_MOTIVE_ANULL = "lbl.motive.annular";
	
	public static final String ACCOUNT_ANNOTATION_LBL_MOTIVE_REJECT = "lbl.motive.reject";

	public static final String ACCOUNT_ANNOTATION_LBL_MOTIVE_OTHER = "lbl.motive.other";
	
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CERTIFY. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CERTIFY = "alt.accountannotation.certify.sucess";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_CANCEL. */
	public static final String ACCOUNT_ANNOTATION_ASK_CANCEL = "alt.accountannotation.askCancel";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_CANCEL_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_CANCEL_BANK_SHARES = "alt.accountannotation.askCancel.bank.shares";
	
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CANCEL. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CANCEL = "alt.accountannotation.cancel.sucess";
	
	/** The Constant ACCOUNT_ANNOTATION_ASK_CONFIRM_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_ASK_CONFIRM_BANK_SHARES = "alt.accountannotation.askConfirm.bank.shares";
	
	/** The Constant ALERT_DUPLICATE_CERTIFICATE_NUMBER_SAME_SECURITY. */
	public static final String ALERT_DUPLICATE_CERTIFICATE_NUMBER_SAME_SECURITY = "alt.accountannotation.duplicate.certificate.number";
	
	/** The Constant ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL. */
	public static final String ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL = "alt.accountannotation.duplicate.certificate.number.physical";
	
	/** The Constant ALERT_INSUFFICIENT_BALANACE. */
	public static final String ALERT_INSUFFICIENT_BALANACE = "alt.splitcoupons.balance.insufficient.balance";
	
	/** The Constant ALERT_ACCOUNTANNOTATION_INVALID_CONFIRM_DPF. */
	public static final String ALERT_ACCOUNTANNOTATION_INVALID_CONFIRM_DPF = "alt.accountannotation.invalid.confirm.dpf";
	
	/** The Constant ALERT_ONE_RECORDS_TO_APPROVE. */
	public static final String ALERT_ONE_RECORDS_TO_APPROVE = "alt.accountannotation.onlyOneRecords.toApprove";
	
	/** The Constant ALERT_ONE_RECORDS_TO_CONFIRM. */
	public static final String ALERT_ONE_RECORDS_TO_CONFIRM = "alt.accountannotation.onlyOneRecords.toConfirm";
	
	/** The Constant ALERT_ONE_RECORDS_TO_CERTIFY. */
	public static final String ALERT_ONE_RECORDS_TO_CERTIFY = "alt.accountannotation.onlyOneRecords.toCertify";
	
	/** The Constant ALERT_ONE_RECORDS_TO_REVIEW. */
	public static final String ALERT_ONE_RECORDS_TO_REVIEW = "alt.accountannotation.onlyOneRecords.toReview";
	
	/** The Constant ALERT_ONE_RECORDS_TO_CANCEL. */
	public static final String ALERT_ONE_RECORDS_TO_CANCEL = "alt.accountannotation.onlyOneRecords.toCancel";
	
	/** The Constant ALERT_EXCEDDED_DEPOSIT_AMOUNT. */
	public static final String ALERT_EXCEDDED_DEPOSIT_AMOUNT = "deposito.cert.manage.excedded.depositAmount";
	
	/** The Constant ALERT_EXCEDDED_DEPOSIT_AMOUNT_MANY. */
	public static final String ALERT_EXCEDDED_DEPOSIT_AMOUNT_MANY = "deposito.cert.manage.excedded.depositAmount.many";
	
	/** The Constant ALERT_MANAGE_NOT_SECURITY. */
	public static final String ALERT_MANAGE_NOT_SECURITY = "deposito.cert.manage.not.security";
	
	/** The Constant LBL_ERROR_MSG_THIS_OPERATION_NOT_SITUATION_MASSIVE. */
	public static final String LBL_ERROR_MSG_THIS_OPERATION_NOT_SITUATION_MASSIVE="lbl.error.msg.this.operation.not.modify.situation.massive";
	
	/** The Constant LBL_ERROR_MSG_SITUATION_DIFFERENT_STATE. */
	public static final String LBL_ERROR_MSG_SITUATION_DIFFERENT_STATE="lbl.error.msg.situation.different.state";
	
	/** The Constant LBL_ERROR_MSG_LOCATION_MULTIPLE_USERS_MASSIVE. */
	public static final String LBL_ERROR_MSG_LOCATION_MULTIPLE_USERS_MASSIVE="modify.location.multiple.users.updation.massive";
	
	/** The Constant LBL_ERROR_MSG_LOCATION_MSG_MASSIVE. */
	public static final String LBL_ERROR_MSG_LOCATION_MSG_MASSIVE="modify.location.cnfrm.msg.massive";
	
	public static final String ACCOUNT_ANNOTATION_REGISTER_MASIVE_SUCCESS="msg.accountannotation.register.masive.success";
	
	public static final String ACCOUNT_ANNOTATION_REGISTER_MASIVE_REQUIRE_EXCEL="msg.accountannotation.register.masive.require.excel";
	
	/** The Constant ACCOUNT_ANNOTATION_REGISTER_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_REGISTER_BANK_SHARES = "alt.accountannotation.register.bank.shares";
		
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_APPROVE_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_APPROVE_BANK_SHARES = "alt.accountannotation.approve.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_ANNUL_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_ANNUL_BANK_SHARES = "alt.accountannotation.annul.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_BANK_SHARES = "alt.accountannotation.confirm.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_AYNI_1000 = "alt.accountannotation.confirm.ayni.1000";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_AYNI_0000 = "alt.accountannotation.confirm.ayni.0000";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_REJECT_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_REJECT_BANK_SHARES = "alt.accountannotation.reject.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_REVIEW_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_REVIEW_BANK_SHARES = "alt.accountannotation.review.sucess.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CERTIFY_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CERTIFY_BANK_SHARES = "alt.accountannotation.certify.sucess.bank.shares";
	
	/** The Constant ACCOUNT_ANNOTATION_SUCCESS_CANCEL_BANK_SHARES. */
	public static final String ACCOUNT_ANNOTATION_SUCCESS_CANCEL_BANK_SHARES = "alt.accountannotation.cancel.sucess.bank.shares";
	
	/** The Constant PHYSICAL_CERTIFICATE_ERROR_EXIST_SECURITY. */
	public static final String PHYSICAL_CERTIFICATE_ERROR_EXIST_SECURITY = "lbl.error.msg.physicalcertificate.exist.security";
	
	/** The Constant ALERT_SECURITY_ALL_DEPOSITED. */
	public final static String ALERT_SECURITY_ALL_DEPOSITED= "lbl.error.msg.securty.all.deposited";
	
	/** The Constant ALERT_ACCOUNT_ANNOTATION_DPF_STATE_REVIEW. */
	public final static String ALERT_ACCOUNT_ANNOTATION_DPF_STATE_REVIEW= "alt.accountannotation.dpf.state.reviewed";
	
	/** The Constant ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CERTIFIED. */
	public final static String ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CERTIFIED= "alt.accountannotation.dpf.state.certified";
	
	/** The Constant ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CANCEL. */
	public final static String ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CANCEL= "alt.accountannotation.dpf.state.cancel";
	
	/** The Constant DAY_NOT_WORKING_ACCREDITATION. */
	public static final String DAY_NOT_WORKING_ACCREDITATION = "message.day.not.working.accreditation";
	
	/**  BLOCK OPERATION. */
	public final static String ALERT_AFFECTATION_INVALID_SECURITY_CLASS_CONFIFM =  "alt.affectation.invalid.security.class.confirm";
	
	/** The Constant ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM. */
	public final static String ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM =  "alt.affectation.invalid.issuer.confirm";
	
	/** The Constant ALERT_AFFECTATION_INVALID_NO_APPROVED. */
	public final static String ALERT_AFFECTATION_INVALID_NO_APPROVED =  "alt.affectation.invalid.no.approved";
	
	/** The Constant ALERT_AFFECTATION_INVALID_NO_REGISTERED. */
	public final static String ALERT_AFFECTATION_INVALID_NO_REGISTERED =  "alt.affectation.invalid.no.registered";
	
	/** The Constant ALERT_AFFECTATION_ASK_APPROVE. */
	public final static String ALERT_AFFECTATION_ASK_APPROVE =  "alt.affectation.ask.approve";
	
	/** The Constant ALERT_AFFECTATION_ASK_ANNUL. */
	public final static String ALERT_AFFECTATION_ASK_ANNUL =  "alt.affectation.ask.annul";
	
	/** The Constant ALERT_AFFECTATION_ASK_CONFIRM. */
	public final static String ALERT_AFFECTATION_ASK_CONFIRM =  "alt.affectation.askConfirm";
	
	/** The Constant ALERT_AFFECTATION_ASK_REJECT. */
	public final static String ALERT_AFFECTATION_ASK_REJECT =  "alt.affectation.askReject";
	
	/** The Constant ALERT_AFFECTATION_APPROVE. */
	public final static String ALERT_AFFECTATION_APPROVE = "alt.affectation.approve";
	
	/** The Constant ALERT_AFFECTATION_ANNUL. */
	public final static String ALERT_AFFECTATION_ANNUL = "alt.affectation.annul";
	
	/** The Constant ALERT_AFFECTATION_REJECT. */
	public final static String ALERT_AFFECTATION_REJECT = "alt.affectation.reject";
	
	/** The Constant ALERT_AFFECTATION_CONFIRM. */
	public final static String ALERT_AFFECTATION_CONFIRM = "alt.affectation.confirm";
	
	/** The Constant ALERT_CUSTODY_HOLDERACCOUNT_ACCOUNTGROUP_INVALID. */
	public final static String ALERT_CUSTODY_HOLDERACCOUNT_ACCOUNTGROUP_INVALID = "alt.custody.holderaccount.accountgroup.invalid";
	
	/**  WITHDRAWAL SIRTEX*. */ 
	public final static String ALERT_BCB_HOLDER_ACCOUNT_NOT_EXISTS = "alt.bcb.holderAccount.not.exists";
	
	/** The Constant ALERT_WITHDRAWAL_SIRTEX_REMOVE_QUANTITY_ZERO. */
	public final static String ALERT_WITHDRAWAL_SIRTEX_REMOVE_QUANTITY_ZERO = "alt.withdrawal.sirtex.removeQuantity.zero";
	
	/** The Constant ALERT_WITHDRAWAL_SIRTEX_SECURITIES_NO_SELECTED. */
	public final static String ALERT_WITHDRAWAL_SIRTEX_SECURITIES_NO_SELECTED = "alt.withdrawal.sirtex.securities.no.selected";
	
	/** The Constant ALERT_WITHDRAWAL_SIRTEX_REQUEST_NO_REGISTERED. */
	public final static String ALERT_WITHDRAWAL_SIRTEX_REQUEST_NO_REGISTERED = "alt.withdrawal.sirtex.request.no.registered";
	
	/** The Constant MSG_WITHDRAWAL_SIRTEX_REGISTER_ASK. */
	public final static String MSG_WITHDRAWAL_SIRTEX_REGISTER_ASK = "msg.withdrawal.sirtex.register.ask";
	
	/** The Constant MSG_WITHDRAWAL_SIRTEX_REGISTER_SUCCESS. */
	public final static String MSG_WITHDRAWAL_SIRTEX_REGISTER_SUCCESS = "msg.withdrawal.sirtex.register.success";
	
	/** The Constant MSG_WITHDRAWAL_SIRTEX_CONFIRM_ASK. */
	public final static String MSG_WITHDRAWAL_SIRTEX_CONFIRM_ASK = "msg.withdrawal.sirtex.confirm.ask";
	
	/** The Constant MSG_WITHDRAWAL_SIRTEX_CONFIRM_SUCCESS. */
	public final static String MSG_WITHDRAWAL_SIRTEX_CONFIRM_SUCCESS = "msg.withdrawal.sirtex.confirm.success";
	
	/** The Constant MSG_WITHDRAWAL_SIRTEX_REJECT_ASK. */
	public final static String MSG_WITHDRAWAL_SIRTEX_REJECT_ASK = "msg.withdrawal.sirtex.reject.ask";
	
	/** The Constant MSG_WITHDRAWAL_SIRTEX_REJECT_SUCCESS. */
	public final static String MSG_WITHDRAWAL_SIRTEX_REJECT_SUCCESS = "msg.withdrawal.sirtex.reject.success";
	
	/** The Constant MSG_OPERATION_DATE_INVALID. */
	public final static String MSG_OPERATION_DATE_INVALID = "msg.operation.date.invalid";
	
	/** The Constant MSG_OPERATION_LOCATION_GEOGRAPHIC_INVALID. */
	public final static String MSG_OPERATION_LOCATION_GEOGRAPHIC_INVALID = "msg.operation.location.geographic.invalid";

	/**  UNBLOCK ACCREDITATION OPERATION*. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_SELECTED = "msg.unblock.accreditation.not.selected";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_REGISTERED. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_REGISTERED = "msg.unblock.accreditation.request.no.registered";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_APPROVED. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_APPROVED = "msg.unblock.accreditation.request.no.approved";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_REGISTER_ASK. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REGISTER_ASK = "msg.unblock.accreditation.register.ask";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_REGISTER_SUCCESS. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REGISTER_SUCCESS = "msg.unblock.accreditation.register.success";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_ANNUL_ASK. */
	public final static String MSG_UNBLOCK_ACCREDITATION_ANNUL_ASK = "msg.unblock.accreditation.annul.ask";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_ANNUL_SUCCESS. */
	public final static String MSG_UNBLOCK_ACCREDITATION_ANNUL_SUCCESS = "msg.unblock.accreditation.annul.success";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_APPROVE_ASK. */
	public final static String MSG_UNBLOCK_ACCREDITATION_APPROVE_ASK = "msg.unblock.accreditation.approve.ask";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_APPROVE_SUCCESS. */
	public final static String MSG_UNBLOCK_ACCREDITATION_APPROVE_SUCCESS = "msg.unblock.accreditation.approve.success";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_CONFIRM_ASK. */
	public final static String MSG_UNBLOCK_ACCREDITATION_CONFIRM_ASK = "msg.unblock.accreditation.confirm.ask";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_CONFIRM_SUCCESS. */
	public final static String MSG_UNBLOCK_ACCREDITATION_CONFIRM_SUCCESS = "msg.unblock.accreditation.confirm.success";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_REJECT_ASK. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REJECT_ASK = "msg.unblock.accreditation.reject.ask";
	
	/** The Constant MSG_UNBLOCK_ACCREDITATION_REJECT_SUCCESS. */
	public final static String MSG_UNBLOCK_ACCREDITATION_REJECT_SUCCESS = "msg.unblock.accreditation.reject.success";
	
	/**  Retirement DPF's *. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_CODE = "msg.interface.invalid.operation.code";
	
	/** The Constant MSG_INTERFACE_SUCCESS. */
	public static final String MSG_INTERFACE_SUCCESS = "msg.interface.success";
	
	/** The Constant MSG_INTERFACE_INVALID_OPERATION_NUMBER_EXITS. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_NUMBER_EXITS = "msg.interface.invalid.operation.number.exits";
	
	/** The Constant MSG_INTERFACE_INVALID_ENTITY_MNEMONIC. */
	public static final String MSG_INTERFACE_INVALID_ENTITY_MNEMONIC = "msg.interface.invalid.entity.mnemonic";
	
	/** The Constant MSG_INTERFACE_INVALID_OPERATION_DATE. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_DATE = "msg.interface.invalid.operation.date";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_SERIAL. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_SERIAL = "msg.interface.invalid.security.serial";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_CLASS. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_CLASS = "msg.interface.invalid.security.class";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_EXPIRATION_DATE. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_EXPIRATION_DATE = "msg.interface.invalid.security.expiration.date";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_NUMBER_COUPONS. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_NUMBER_COUPONS = "msg.interface.invalid.security.number.coupons";
	
	/** The Constant MSG_INTERFACE_INVALID_HOLDERACCOUNT. */
	public static final String MSG_INTERFACE_INVALID_HOLDERACCOUNT = "msg.interface.invalid.holderaccount";
	
	/** The Constant MSG_INTERFACE_INVALID_BALANCE_AVAILABLE. */
	public static final String MSG_INTERFACE_INVALID_BALANCE_AVAILABLE = "msg.interface.invalid.balance.available";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_NOT_REGISTERED. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_NOT_REGISTERED = "msg.interface.invalid.security.not.regitered";
	
	/**  Reversion Retirement DPF's. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_DATE = "msg.interface.invalid.security.issuance.date";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_CURRENY. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_CURRENY = "msg.interface.invalid.security.currency";
	
	/** The Constant MSG_INTERFACE_INVALID_NOMINAL_VAUE. */
	public static final String MSG_INTERFACE_INVALID_NOMINAL_VAUE = "msg.interface.invalid.security.nominal.value";
	
	/** The Constant MSG_INTERFACE_INVALID_YIELD_RATE. */
	public static final String MSG_INTERFACE_INVALID_YIELD_RATE = "msg.interface.invalid.security.yield.rate";
	
	/** The Constant MSG_INTERFACE_INVALID_ALTERNATIVE_CODE. */
	public static final String MSG_INTERFACE_INVALID_ALTERNATIVE_CODE = "msg.interface.invalid.security.alternative.code";
	
	/** The Constant MSG_INTERFACE_INVALID_FORMAT_NUMBER_REVERT. */
	public static final String MSG_INTERFACE_INVALID_FORMAT_NUMBER_REVERT = "msg.interface.invalid.operation.format.number.revert";
	
	/** The Constant MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_NOTEXITS. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_NOTEXITS = "msg.interface.invalid.operation.retirement.notexits";
	
	/** The Constant MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_DATE. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_DATE = "msg.interface.invalid.operation.retirement.date";
	
	/** The Constant MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_SEC_SERIAL. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_SEC_SERIAL = "msg.interface.invalid.operation.retirement.security.serial";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_EXCEED_BALANCES. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_EXCEED_BALANCES = "msg.interface.invalid.security.exceed.balances";
	
	/** The Constant MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT. */
	public static final String MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT = "msg.interface.invalid.operation.retirement.recurrent";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_CALENDAR_DAYS. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_CALENDAR_DAYS = "msg.interface.invalid.security.calendar.days";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_SOURCE. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_SOURCE = "msg.interface.invalid.security.source";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_INTEREST_TYPE. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_INTEREST_TYPE = "msg.interface.invalid.security.interest.type";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_INDEXED. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_INDEXED = "msg.interface.invalid.security.indexed";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY = "msg.interface.invalid.security.interest.payment.modality";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_ISSUER. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_ISSUER = "msg.interface.invalid.security.issuer";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_ISSUANCE. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_ISSUANCE = "msg.interface.invalid.security.issuance";
	
	/** The Constant MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_COUNTRY. */
	public static final String MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_COUNTRY = "msg.interface.invalid.security.issuance.country";
	
	/** The Constant MSG_INTERFACE_INVALID_INTEREST_RATE. */
	public static final String MSG_INTERFACE_INVALID_INTEREST_RATE = "msg.interface.invalid.security.interest.rate";
	
	/** The Constant MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE. */
	public static final String MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE = "msg.interface.invalid.holderaccountbalance";
	
	/** The Constant DIGITALSIGNATURE_CONFIRM_REGISTER. */
	public static final String DIGITALSIGNATURE_CONFIRM_REGISTER = "digitalsignature.confirm.register";
	
	/** The Constant DIGITALSIGNATURE_CONFIRM_MODIFY. */
	public static final String DIGITALSIGNATURE_CONFIRM_MODIFY = "digitalsignature.confirm.modify";
	
	/** The Constant DIGITALSIGNATURE_CONFIRM_REGISTER_SUCCESS. */
	public static final String DIGITALSIGNATURE_CONFIRM_REGISTER_SUCCESS = "digitalsignature.msg.register.success";
	
	/** The Constant DIGITALSIGNATURE_CONFIRM_MODIFY_SUCCESS. */
	public static final String DIGITALSIGNATURE_CONFIRM_MODIFY_SUCCESS = "digitalsignature.msg.modify.success";
	
	/** The Constant DIGITALSIGNATURE_SIGNATURE_NULL. */
	public static final String DIGITALSIGNATURE_SIGNATURE_NULL= "digitalsignature.signature.null";
	
	/** The Constant VERIFICATIONS_CATS_ERROR_UNBLOCKED. */
	public static final String VERIFICATIONS_CATS_ERROR_UNBLOCKED= "verifications.cats.error.unblocked";
	
	/** The Constant DIGITALSIGNATURE_SIGNATURE_EXITS_ACTIVE_SIGNATURE. */
	public static final String DIGITALSIGNATURE_SIGNATURE_EXITS_ACTIVE_SIGNATURE= "digitalsignature.signature.exits.active.siganature";
	
	/** The Constant DIGITALSIGNATURE_SIGNATURE_INCOMPLETE. */
	public static final String DIGITALSIGNATURE_SIGNATURE_INCOMPLETE= "digitalsignature.signature.incomplete";
	
	/** The Constant DIGITALSIGNATURE_SIGNATURE_INVALID_PASSWORD. */
	public static final String DIGITALSIGNATURE_SIGNATURE_INVALID_PASSWORD="digitalsignature.signature.invalid.password";
	
	/** The Constant SECURITY_AMORTIZATION_COUPON_MESSAGE. */
	public final static String SECURITY_AMORTIZATION_COUPON_MESSAGE="invalid.request.certificate.accreditation.security.amortization.coupon";
	/** Constante para el mensaje de envio por email de bloqueos*/
	public final static String  LOCK_SECURITY_MESSAGE="blocks.messages.notification";
	/**Constante para desbloqueo y notificacion por via email*/
	public final static String UNLOCKING_SECURITY_MESSAGE="unlocking.security.notification";
	
	public final static String MSG_ALERT_VALIDATION_REQUIRED_AUTHORIZEDSECURITY="msg.alert.validation.required.authorizedSecurity";
	
	public final static String MSG_ALERT_VALIDATION_REQUIRED_AUTHORIZEDSECURITY_ESPECIFIED="msg.alert.validation.required.authorizedSecurity.especified";

	/* PHYSICAL CERTIFICATE MASSIVE REGISTER */
	public final static String MSG_PHYSICAL_CERTIFICATE_VALIDATION_ATTACHMENT_ERROR = "msg.physicalcertificate.massive.validation.attachment.error";

	
	/** Issue 1260*/
	public static final String VALIDATION_SIGNATURE_IMG="validation.signature.img";
	public static final String VALIDATION_SIZE_SIGNATURE_IMG="validation.size.signature.img";
	public static final String VALIDATION_EXTENSION_SIGNATURE_IMG="validation.extension.signature.img";
	public static final String VALIDATION_EXIST_AUTHORIZED_SIGNATURE_REGISTER="validation.exist.authorized.signature.state.register";
	public static final String VALIDATION_EXIST_AUTHORIZED_SIGNATURE_CONFIRM="validation.exist.authorized.signature.state.confirm";
	public static final String VALIDATION_DIMENSION_SIGNATURE_IMG="validation.dimension.signature.img";
	public static final String LBL_MSG_CONFIRM_AUTHORIZED_SIGNATURE="lbl.msg.confirm.authorized.signature";
	public static final String LBL_MSG_REJECT_AUTHORIZED_SIGNATURE="lbl.msg.reject.authorized.signature";
	public static final String MSG_SUCCESFUL_CONFIRM_AUTHORIZED_SIGNATURE="msg.succesful.confirm.authorized.signature";
	public static final String MSG_SUCCESFUL_REJECT_AUTHORIZED_SIGNATURE="msg.succesful.reject.authorized.signature";
	public static final String MSG_SUCCESFULL_MODIFY_AUTHORIZED_SIGNATURE="msg.succesful.modify.authorized.signature";
	public static final String MSG_SUCCESFULL_REGISTER_AUTHORIZED_SIGNATURE="msg.succesful.register.authorized.signature";
	public static final String LBL_MSG_CONFIRM_MODIFY_AUTHORIZED_SIGNATURE="lbl.msg.confirm.modify.authorized.signature";
	public static final String VALIDATION_AUTHORIZED_SIGNATURE_JOB_POSITION="validation.authorized.signature.job.position";
	public static final String LBL_MSG_CONFIRM_REGISTER_AUTHORIZED_SIGNATURE="lbl.msg.confirm.regiter.authorized.signature";
	public static final String VALIDATION_AUTHORIZED_SIGNATURE_DOCUMENT_NUMBER="validation.authorized.signature.document.number";
	public static final String MSG_ERROR_AUTHORIZED_SIGNATURE_REQUIRED_STATE_REGISTER="msg.error.authorized.signature.required.state.register";
	public static final String VALIDATION_AUTHORIZED_SIGNATURE_INSTITUTION_TYPE="validation.authorized.signature.institution.type";
	
	// issue 1379
	public static final String MESSAGE_EXCHANGESECURITIES_REGISTER_ALL="message.exchangeSecurities.register.all";
	public static final String MESSAGE_EXCHANGESECURITIES_REGISTER_OK="message.exchangeSecurities.register.ok";
	public static final String MESSAGE_EXCHANGESECURITIES_CONFIRM_CONF="message.exchangeSecurities.confirm.conf";
	public static final String MESSAGE_EXCHANGESECURITIES_REJECT_CONF="message.exchangeSecurities.reject.conf";
	public static final String MESSAGE_EXCHANGESECURITIES_CONFIRM_OK="message.exchangeSecurities.confirm.ok";
	public static final String MESSAGE_EXCHANGESECURITIES_REJECT_OK="message.exchangeSecurities.reject.ok";
	
	public static final String MESSAGE_VALIDATION_EXCHANGESECURITIES_ISSUANCEINVALID_NOTALLOWEDEXCHAGESECURITIES="message.validation.exchangeSecurities.issuanceInvalid.notAllowedExchageSecurities";
	public static final String MESSAGE_VALIDATION_EXCHANGESECURITIES_ISSUANCEINVALID_NOFOUNDPLACEMENTTRANCHES="message.validation.exchangeSecurities.issuanceInvalid.noFoundPlacementTranches";
	public static final String ERROR_EXCHANGESECURITIES_NO_ATTACHMENTS_FOUND="error.exchangeSecurities.no.attachments.found";
	public static final String ACCOUNT_ANNOTATION_BANK_NOT_FOUND="alt.accountannotation.bankNotFound";
}