package com.pradera.custody.securitiesrenewal.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.component.helperui.to.IssuerSearchTO;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.custody.operations.remove.service.RemoveOperationsServiceBean;
import com.pradera.custody.retirementoperation.facade.RetirementOperationServiceFacade;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.corporatives.service.CorporateProcessRemoteService;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.custody.to.SecuritiesRenewalQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.securitiesrenewal.RenewalBlockOperation;
import com.pradera.model.custody.securitiesrenewal.RenewalMarketfactOperation;
import com.pradera.model.custody.securitiesrenewal.RenewalOperationDetail;
import com.pradera.model.custody.securitiesrenewal.SecuritiesRenewalOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.custody.type.SecuritiesRenewalMotiveType;
import com.pradera.model.custody.type.SecuritiesRenewalStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityHistoryBalance;
import com.pradera.model.issuancesecuritie.type.IssuanceStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SecuritiesRenewalServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@Lock(LockType.READ)
public class SecuritiesRenewalServiceBean extends CrudDaoServiceBean{
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The issuer query service. */
	@EJB
	private IssuerQueryServiceBean issuerQueryService;
	
	/** The securities remote service. */
	@Inject
	private Instance<SecuritiesRemoteService> securitiesRemoteService;
	
	/** The securities component service. */
	@Inject
    private Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The corporate process remote service. */
	@Inject
	private Instance<CorporateProcessRemoteService> corporateProcessRemoteService;
	
	/** The executor component service bean. */
	@Inject
    private Instance<AccountsComponentService> accountsComponentService;
	
	/** The affectations service bean. */
	@EJB
	AffectationsServiceBean affectationsServiceBean;
	
	/** The remove security service facade. */
	@EJB
	RetirementOperationServiceFacade removeSecurityServiceFacade;
	
	/** The request reversals service bean. */
	@EJB
	private RequestReversalsServiceBean requestReversalsServiceBean;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The remove operations service bean. */
	@EJB
	private RemoveOperationsServiceBean removeOperationsServiceBean;
	
	/** The custody service consumer. */
	@EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	/** The accounts remote service. */
	@Inject
	private Instance<AccountsRemoteService> accountsRemoteService;
	
	/** The securities query service. */
	@EJB
	private SecuritiesQueryServiceBean securitiesQueryService;
	
	
	/** The idepositary setup. */
	@Inject
	@DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/**
	 * Validate and save renew securities tx.
	 *
	 * @param stockEarlyPaymentRegisterTO the stock early payment register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveRenewSecuritiesTx(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO , LoggerUser loggerUser) throws ServiceException {		
		Object objResult = validateAndSaveRenewSecurities(stockEarlyPaymentRegisterTO, loggerUser);		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	} 
	
	/**
	 * Validate and save renewal securities operation.
	 *
	 * @param stockEarlyPaymentRegisterTO the Stock early payment register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */	
	public Object validateAndSaveRenewSecurities(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO, LoggerUser loggerUser) throws ServiceException {
		
		Map<String,Object> params;
		
		// VALIDATE PARTICIPANT
		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(stockEarlyPaymentRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.getParticipantByPk(objParticipant);		
		if(objParticipant == null){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			stockEarlyPaymentRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		// CHECK OPERATION NUMBER EXITS
		
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), stockEarlyPaymentRegisterTO.getOperationNumber(), 
						stockEarlyPaymentRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// VALIDATE OPERATION DATE IS TODAY
		
		Date operationDate = CommonsUtilities.truncateDateTime(stockEarlyPaymentRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
		}
		
		// issue 1177: la fecha de emision no debe ser mayor a la fecha de operacion
		Date issuanceDateDate = CommonsUtilities.truncateDateTime(stockEarlyPaymentRegisterTO.getSecurityObjectTO().getIssuanceDate());
		if(issuanceDateDate.compareTo(operationDate)>0){
			System.out.println("::: ok ren");
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE_MUST_BE_LESS_OPERATION_DATE,null);
		}

		// VALIDATE OPERATION TYPE
		
		if(!(ComponentConstant.INTERFACE_RENEW_SECURITIES_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode()))){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// VALIDATE SECURITY CLASS
		
		SecurityObjectTO securityObjectTO = stockEarlyPaymentRegisterTO.getSecurityObjectTO();		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filter.setText1(securityObjectTO.getSecurityClassDesc());		
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			} else {
				securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
			}			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}
		
		// VALIDATE SECURITY TARGET 
		
		String idSecurityCodeTarget = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
		Security objSecurityTarget = null;		 
		if(idSecurityCodeTarget != null){
			//objSecurityTarget = find(Security.class, idSecurityCodeTarget);
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk(idSecurityCodeTarget);
			objSecurityTarget = securitiesQueryService.findSecuritiesByDpf(securityTO);
		}		
		if(objSecurityTarget == null){
			
			//	VALIDATE ISSUANCE DATE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getIssuanceDate())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
			}
			
			// VALIDATE CALENDAR DAYS
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getCalendarDays())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DAYS, null);
			}
			
			// VALIDATE EXPIRATION DATE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getExpirationDate())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
			// VALIDATE SECURITY SOURCE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getSecuritySource()) ){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_SOURCE, null);
			}			
			
			// VALIDATE CURRENCY SECURITY
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getCurrencyMnemonic())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
						
			// VALIDATE NOMINAL VALUE
						
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getNominalValue())) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, PropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VAUE);
			} 
			
			// VALIDATE INTEREST TYPE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getInterestType())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_TYPE, null);
			}						
			
			// VALIDATE INTEREST PAYMENT MODALATY
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getInterestPaymentModality())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_INTEREST_PAYMENT_MODALITY, null);
			}
			
			// VALIDATE ISSUER
			
			Issuer objIssuer = null;
			IssuerSearchTO objIssuerSearchTO = new IssuerSearchTO();
			objIssuerSearchTO.setMnemonic(securityObjectTO.getIssuerMnemonic());
			List<Issuer> lstIssuer = issuerQueryService.findIssuers(objIssuerSearchTO);			
			if(Validations.validateListIsNotNullAndNotEmpty(lstIssuer) ){
				objIssuer = new Issuer();
				objIssuer = lstIssuer.get(0);			
			}		
			if(objIssuer == null){
				securityObjectTO.setIssuerMnemonic(null);
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
																GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUER_CODE, null);
			} else {
				securityObjectTO.setIssuerMnemonic(objIssuer.getMnemonic());
				objIssuerSearchTO.setIdIssuerCode(objIssuer.getIdIssuerPk());				
			}
			
			// VALIDATE ALTERNATIVE CODE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getAlternativeCode())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
			}
			
			// VALIDATE ISSUANCE GEOGRAPHIC LOCATION ISSUANCE DEPARTMENT
			
			params = new HashMap<String, Object>();
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getCodeGeographicLocation())){
				params.put("codeGeographicLocPrm", securityObjectTO.getCodeGeographicLocation());
				params.put("typeGeographicLocPrm", GeographicLocationType.DEPARTMENT.getCode());
				GeographicLocation geographicLocation = findObjectByNamedQuery(GeographicLocation.FIND_BY_CODE_GEO_LOC, params);
				if(geographicLocation != null){
					securityObjectTO.setIdGeographicLocation(geographicLocation.getIdGeographicLocationPk());
				} else {
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_PLACE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ISSUANCE_PLACE, null);
			}			
					
			// VALIDATE PASSIVE RATE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getPassiveInterestRate())) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE, null);
			}
			
			// VALIDATE INTEREST RATE
			
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getInterestRate()) ) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
			}
			
			Integer calendarDays = new Integer(CommonsUtilities.getDaysBetween(securityObjectTO.getIssuanceDate(), securityObjectTO.getExpirationDate()));
			if(calendarDays.compareTo(GeneralConstants.DPA_DPF_MIN_DAYS_TERM) < GeneralConstants.ZERO_VALUE_INT  || 
					calendarDays.compareTo(GeneralConstants.DPA_DPF_MAX_DAYS_TERM_RENEWAL) > GeneralConstants.ZERO_VALUE_INT){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DAYS, null);
			}
			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_RENEW_ISINCODE_EXITS, null);
		}
		
		// VALIDATE SECURITY SOURCE EXITS
		
		boolean isBlocked = Boolean.FALSE;
		boolean isExpiration = Boolean.FALSE;
		String idSecurityCodeSource = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerialPrevious());
		Security objSecuritySource = new Security();		 
		if(idSecurityCodeSource != null){
			//objSecuritySource = find(Security.class, idSecurityCodeSource);
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk(idSecurityCodeSource);
			objSecuritySource = securitiesQueryService.findSecuritiesByDpf(securityTO);
		}		
		if(objSecuritySource == null){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL_PREVIUOS, null);
		} else {
			securityObjectTO.setSecuritySerialPrevious(objSecuritySource.getIdSecurityCodePk());
			securityObjectTO.setCurrencySource(objSecuritySource.getCurrency());
			if(objSecuritySource.getObservations() != null){
				securityObjectTO.setRestrictions(objSecuritySource.getObservations());
			}			
			
			// VALIDAR QUE EXISTA UNA SOLA RENOVACION DEL VALOR
			
			List<Security> lstSecuritySourceFind = getLisSourceSecurityCode(objSecuritySource.getIdSecurityCodePk());
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecuritySourceFind)){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_RENEW_EXITS, null);
			}
			
			// VALIDATE EXPIRATION DATE
			
			if(Validations.validateIsNullOrEmpty(objSecuritySource.getExpirationDate())){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_SOURCE, null);
			} else {
				if(objSecuritySource.getExpirationDate().compareTo(CommonsUtilities.currentDate()) > 0){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_SOURCE, null);
				}
			}
			
			// VALIDATE EXPIRATION DATE TARGET AND EXPIRATION DATE SOURCE
			
			if(securityObjectTO.getIssuanceDate().compareTo(objSecuritySource.getExpirationDate()) < 0){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_SOURCE, null);
			}
			
			Date dtExpiredSecurity = CommonsUtilities.addDate(objSecuritySource.getExpirationDate(), 5);		
			if(stockEarlyPaymentRegisterTO.getOperationDate().compareTo(dtExpiredSecurity) > 0){
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_SOURCE, null);
			}
			
		}
		
		// VALIDATE HOLDER ACCOUNT SOURCE
		
		HolderAccountBalance objHolderAccountBalance = new HolderAccountBalance();
		HolderAccount objHolderAccountTenedor = new HolderAccount();
		// Obtener datos del la cuenta matriz tenedora
		if(objParticipant != null && objSecuritySource != null){
			HolderAccountMovement objHolderAccountMovement = getHolderAccountMovementRenewal(objSecuritySource.getIdSecurityCodePk());
			if (Validations.validateIsNullOrEmpty(objHolderAccountMovement)) {				
				List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
						objParticipant.getIdParticipantPk(), objSecuritySource.getIdSecurityCodePk(), true);
				if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalances)) {
					objHolderAccountBalance = lstHolderAccountBalances.get(0);
				} else {
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE_PARTICIPANT, null);
				}								
			} else {
				objHolderAccountBalance = objHolderAccountMovement.getHolderAccountBalance();
			}			
		}				
		
		// Obtener datos del la cuenta matriz acreedora		
		HolderAccount objHolderAccountAcreedor = new HolderAccount();		
		if(objHolderAccountBalance != null && objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk() != null){			
			objHolderAccountTenedor = find(HolderAccount.class, objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
			// Buscar si existe cuenta en el emisor con el titular del valor a renovar, sino se crea una nueva cuenta
			HolderAccountObjectTO objHolderAccountObjectTOOrigen = new HolderAccountObjectTO();
			objHolderAccountObjectTOOrigen.setAccountType(objHolderAccountTenedor.getAccountType());
			objHolderAccountObjectTOOrigen.setIdParticipant(objParticipant.getIdParticipantPk());
			List<Long> lstIdHolderPk = new ArrayList<Long>();
			for(HolderAccountDetail objHolderAccountDetail : objHolderAccountTenedor.getHolderAccountDetails()){
				lstIdHolderPk.add(objHolderAccountDetail.getHolder().getIdHolderPk());
			}
			objHolderAccountObjectTOOrigen.setLstIdHolderPk(lstIdHolderPk);			
			Object objResponse = accountsRemoteService.get().createHolderAccountRemoteService(loggerUser, objHolderAccountObjectTOOrigen);
			
			if(objResponse instanceof RecordValidationType){
				RecordValidationType validation = (RecordValidationType) objResponse;		
				return validation;
			} else if (objResponse instanceof HolderAccountObjectTO){
				objHolderAccountObjectTOOrigen = (HolderAccountObjectTO) objResponse;
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTOOrigen.getIdHolderAccount())) {
				objHolderAccountAcreedor = find(HolderAccount.class, objHolderAccountObjectTOOrigen.getIdHolderAccount());
				stockEarlyPaymentRegisterTO.setHolderAccountObjectTO(new HolderAccountObjectTO());
				stockEarlyPaymentRegisterTO.getHolderAccountObjectTO().setAccountNumber(objHolderAccountAcreedor.getAccountNumber());
			} else {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT, null);
			}						
		}
		
		// VALIDATE ACCREDITATION OPERATIONS - SECURITY SOURCE
		
		if(objHolderAccountBalance != null && objHolderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO) > 0 ){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_ACCREDITATION_OPERATION_EXISTS, null);				
		}
		
		// VALIDATE REPORT OPERATIONS - SECURITY SOURCE
		
		if(objHolderAccountBalance != null && (objHolderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO) > 0
				|| objHolderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO) > 0)){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_REPORT_OPERATION_EXISTS, null);				
		}
		
		// VALIDATE AVAILABLE BALANCE, EXPIRED SECURITIES
		
		if(objHolderAccountBalance != null && objHolderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
			Object objResultExpired = corporateProcessRemoteService.get().executeExpiredSecuritiesDpf(loggerUser, objSecuritySource.getIdSecurityCodePk());
			if(objResultExpired instanceof RecordValidationType){
				RecordValidationType validation = (RecordValidationType) objResultExpired;
				validation.setOperationRef(stockEarlyPaymentRegisterTO);
				return validation;
			}	
			isExpiration = Boolean.TRUE;
		}				
		
		if(objParticipant != null && objSecuritySource != null && objHolderAccountTenedor != null
				&& objHolderAccountAcreedor != null && objHolderAccountBalance != null){			
			
			// CREAR NUEVO VALOR - VALOR DESTINO
								
			SecurityObjectTO objSecurityObjectTO = new SecurityObjectTO();
			
			securityObjectTO.setDpfInterface(stockEarlyPaymentRegisterTO.isDpfInterface());
			Object objResult = securitiesRemoteService.get().createSecuritiesRemoteService(loggerUser, securityObjectTO);	
			
			if(objResult instanceof RecordValidationType){
				RecordValidationType validation = (RecordValidationType) objResult;
				validation.setOperationRef(stockEarlyPaymentRegisterTO);
				return validation;
			} else if (objResult instanceof SecurityObjectTO){
				objSecurityObjectTO = (SecurityObjectTO) objResult;
			}			
			
			if(objSecurityObjectTO.getIdSecurityCode() != null){
				objSecurityTarget = find(Security.class, objSecurityObjectTO.getIdSecurityCode());				
			}
			
			// BUSSINES RENEWAL OPERATIONS
			
			SecuritiesRenewalOperation objSecuritiesRenewalOperation = new SecuritiesRenewalOperation();
			BigDecimal totalBalance = BigDecimal.ZERO;
			if(objSecurityTarget != null){	
				
				// VALIDATE STATUS SECURITY TARGET
				
				if(!SecurityStateType.REGISTERED.getCode().equals(objSecurityTarget.getStateSecurity())){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
							new Object[]{ SecurityStateType.lookup.get(objSecuritySource.getStateSecurity()).getValue() });
				}
				
				// OBJECT SECURITIES RENEWAL OPERATION
				
				objSecuritiesRenewalOperation.setMotive(SecuritiesRenewalMotiveType.SECURITIES_RENEWAL_DPF.getCode());
				objSecuritiesRenewalOperation.setRenewalOperationDetail(new ArrayList<RenewalOperationDetail>());
				objSecuritiesRenewalOperation.setSourceSecurity(objSecuritySource);
				objSecuritiesRenewalOperation.setTargetSecurity(objSecurityTarget);	
				if(isExpiration){
					objSecuritiesRenewalOperation.setSecurityExpiration(GeneralConstants.ONE_VALUE_INTEGER);
				} else {
					objSecuritiesRenewalOperation.setSecurityExpiration(GeneralConstants.ZERO_VALUE_INTEGER);
				}				
				
				// GET BALANCE BLOCKED
				
				List<TransferAccountBalanceTO> holderAccountsBalancesTOs = removeSecurityServiceFacade.getHolderAccountsBalances(
						objSecuritySource.getIdSecurityCodePk(), objHolderAccountBalance.getParticipant().getIdParticipantPk(), 
						objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk(), Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);	
				
				for(TransferAccountBalanceTO holderAccountBalanceTO : holderAccountsBalancesTOs){					
					holderAccountBalanceTO.setBlCheck(true);
					
					// OBJECT RENEWAL OPERATION DETAIL
					
					RenewalOperationDetail objRenewalOperationDetail = new RenewalOperationDetail();	
					if(holderAccountBalanceTO.getHolderAccountBalance().getTotalBalance().compareTo(BigDecimal.ZERO) == 0){						
						objRenewalOperationDetail.setAvailableBalance(BigDecimal.ONE);
					} else {						
						if(holderAccountBalanceTO.getHolderAccountBalance().getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXITS_AVAILABLE_BALANCE_SECURITY_EXPIRED, null);
						}
						objRenewalOperationDetail.setAvailableBalance(BigDecimal.ZERO);						
					}					
					objRenewalOperationDetail.setPawnBalance(objHolderAccountBalance.getPawnBalance());
					objRenewalOperationDetail.setBanBalance(objHolderAccountBalance.getBanBalance());
					objRenewalOperationDetail.setOtherBlockBalance(objHolderAccountBalance.getOtherBlockBalance());					
					objRenewalOperationDetail.setHolderAccount(objHolderAccountAcreedor);
					objRenewalOperationDetail.setParticipant(objParticipant);
					objRenewalOperationDetail.setSecuritiesRenewalOperation(objSecuritiesRenewalOperation);
					
					// BALANCE AVAILABLE
					
					if(objRenewalOperationDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
						if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){								
							objRenewalOperationDetail.setRenewalMarketfactOperation(new ArrayList<RenewalMarketfactOperation>());					
							RenewalMarketfactOperation objRenewalMarketfactOperation = new RenewalMarketfactOperation();
							objRenewalMarketfactOperation.setRenewalOperationDetail(objRenewalOperationDetail);
							objRenewalMarketfactOperation.setMarketDate(CommonsUtilities.currentDate());
							objRenewalMarketfactOperation.setMarketPrice(BigDecimal.ZERO);
							objRenewalMarketfactOperation.setMarketRate(objSecuritiesRenewalOperation.getTargetSecurity().getInterestRate());
							objRenewalMarketfactOperation.setOperationQuantity(objRenewalOperationDetail.getAvailableBalance());													
							objRenewalOperationDetail.getRenewalMarketfactOperation().add(objRenewalMarketfactOperation);								
						}
					}
					
					// BALANCE BLOCKED
					
					if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getBlockOperations())){
						isBlocked = true;
						objRenewalOperationDetail.setRenewalBlockOperation(new ArrayList<RenewalBlockOperation>());
						for (BlockOperation blockOperation : holderAccountBalanceTO.getBlockOperations()) {
							if(blockOperation.getSelected()){
								RenewalBlockOperation objRenewalBlockOperation = new RenewalBlockOperation();
								objRenewalBlockOperation.setBlockOperation(blockOperation);
								objRenewalBlockOperation.setRenewalOperationDetail(objRenewalOperationDetail);
								objRenewalBlockOperation.setBlockQuantity(blockOperation.getActualBlockBalance());
								objRenewalBlockOperation.setIndSourceTarget(GeneralConstants.ONE_VALUE_INTEGER);
								objRenewalBlockOperation.setRenewalMarketfactOperation(new ArrayList<RenewalMarketfactOperation>());								
								// markect fact balance
								RenewalMarketfactOperation objRenewalMarketfactOperation = new RenewalMarketfactOperation();
								objRenewalMarketfactOperation.setRenewalBlockOperation(objRenewalBlockOperation);
								objRenewalMarketfactOperation.setMarketDate(CommonsUtilities.currentDate());
								objRenewalMarketfactOperation.setMarketRate(objSecuritiesRenewalOperation.getTargetSecurity().getInterestRate());
								objRenewalMarketfactOperation.setMarketPrice(BigDecimal.ZERO);
								objRenewalMarketfactOperation.setOperationQuantity(blockOperation.getActualBlockBalance());
								objRenewalBlockOperation.getRenewalMarketfactOperation().add(objRenewalMarketfactOperation);								
								objRenewalOperationDetail.getRenewalBlockOperation().add(objRenewalBlockOperation);
							}
						}
					}	
					
					// TOTAL BALANCE OPERATIONS SECURITIES RENEWAL
					
					totalBalance = totalBalance.add(objRenewalOperationDetail.getTotalBalance());
					objRenewalOperationDetail.setTotalBalance(totalBalance);
					objSecuritiesRenewalOperation.getRenewalOperationDetail().add(objRenewalOperationDetail);					
				}
				
				objSecuritiesRenewalOperation.setOperationState(SecuritiesRenewalStateType.APPROVED.getCode());
				objSecuritiesRenewalOperation.setCustodyOperation(new CustodyOperation());		
				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
			}
			
			// INVOKE THE COMPONENT (ACCOUNT, SECURITIES, ISSUANCES)
			
			saveSecuritiesRenewalOperation(objSecuritiesRenewalOperation, loggerUser);
			changeStateRenewalOperation(objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk(), SecuritiesRenewalStateType.CONFIRMED.getCode(), loggerUser, isBlocked);									
			stockEarlyPaymentRegisterTO.setIdCustodyOperation(objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk());				
			stockEarlyPaymentRegisterTO.setIdSecurityCode(objSecuritiesRenewalOperation.getTargetSecurity().getIdSecurityCodePk());
			stockEarlyPaymentRegisterTO.setTransactionState(BooleanType.YES.getCode());
		}
		return stockEarlyPaymentRegisterTO;
	}
	
	/**
	 * Change the state renewal operation.
	 *
	 * @param idSecuritiesRenewal the id security renewal
	 * @param newState the new state
	 * @param loggerUser the logger user
	 * @param isBlocked the is blocked
	 * @throws ServiceException the service exception
	 */	
	public void changeStateRenewalOperation(Long idSecuritiesRenewal, Integer newState, LoggerUser loggerUser, 
			boolean isBlocked) throws ServiceException {
		
		SecuritiesRenewalOperation objSecuritiesRenewalOperation = find(SecuritiesRenewalOperation.class, idSecuritiesRenewal);
		if(SecuritiesRenewalStateType.CONFIRMED.getCode().equals(newState)){
			objSecuritiesRenewalOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
			objSecuritiesRenewalOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
			objSecuritiesRenewalOperation.setOperationState(RetirementOperationStateType.CONFIRMED.getCode());
		}
		objSecuritiesRenewalOperation.getCustodyOperation().setState(objSecuritiesRenewalOperation.getOperationState());		
		if(SecuritiesRenewalStateType.CONFIRMED.getCode().equals(newState)){
			
			// MOVIMIENTOS A NIVEL DE CUENTAS DEL VALOR DESTINO
			
			AccountsComponentTO accountsComponentTO = getAccountsComponentTO(objSecuritiesRenewalOperation, BusinessProcessType.RENEWAL_SECURITIES_REN.getCode());
			
			if(Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstSourceAccounts()) ||
					Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstTargetAccounts())){
				
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
				
			}							
			
			// MOVIMIENTOS A NIVEL DE VALOR Y EMISION DEL VALOR ORIGEN, SI ESQ EL VALOR ESTA BLOQUEADO
			
			SecuritiesComponentTO securitiesComponentTO = null;	
			if(isBlocked){
				
				// Ejecutar desbloqueos en el valor origen, y cuenta tendora
				executeBlockOperationsUnblock(objSecuritiesRenewalOperation, loggerUser);
				
				// Ingreso disponible al nuevo acreedor y salida del tenedor actual por renovacion de valores bloqueados
				accountsComponentTO = getAccountsComponentTO(objSecuritiesRenewalOperation, BusinessProcessType.RENEWAL_SECURITIES_BLOCK.getCode());				
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);	
				
				// Ejecutar bloqueos en el valor destino, y cuenta acreedora
				executeBlockOperationsBlock(objSecuritiesRenewalOperation);
				
				securitiesComponentTO = getSecuritiesComponentTO(objSecuritiesRenewalOperation, ComponentConstant.SOURCE, 
						BusinessProcessType.RENEWAL_SECURITIES_REN.getCode());			
				securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);		
				
				updatePaymentChronogram(securitiesComponentTO.getIdSecurityCode(), loggerUser);
			}
			
			// MOVIMIENTOS A NIVEL DE VALOR Y EMISION DEL VALOR DESTINO
											
			if(objSecuritiesRenewalOperation.getTargetSecurity() != null){
				
				securitiesComponentTO = getSecuritiesComponentTO(objSecuritiesRenewalOperation, ComponentConstant.TARGET, 
						BusinessProcessType.RENEWAL_SECURITIES_REN.getCode());				
				securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);
				
			}								
		}
	}
	
	/**
	 * Create the securities renewal operation.
	 *
	 * @param objSecuritiesRenewalOperation SecuritiesRenewalOperation
	 * @param loggerUser LoggerUser
	 * @throws ServiceException the service exception
	 */	
	public void saveSecuritiesRenewalOperation(SecuritiesRenewalOperation objSecuritiesRenewalOperation, LoggerUser loggerUser) 
			throws ServiceException{
		Map<String, Object> params = new HashMap<String, Object>();
		HolderAccount holderAccount = null;
		for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){
			params.put("idHolderAccountPkParam", objRenewalOperationDetail.getHolderAccount().getIdHolderAccountPk());
			holderAccount = (HolderAccount) findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE,params);			
			if(!HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount())){
				String strCode = holderAccount.getAccountNumber().toString();
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_NOT_REGISTERED,new Object[]{strCode});
			}
		}
		CustodyOperation custodyOperation = objSecuritiesRenewalOperation.getCustodyOperation();
		custodyOperation.setOperationType(ParameterOperationType.OPERATION_SECURITIES_RENEWAL_DPF.getCode());
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setState(objSecuritiesRenewalOperation.getOperationState());		
		Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_SECURITIES_RENEWAL);
        custodyOperation.setOperationNumber(operationNumber);        
        create(objSecuritiesRenewalOperation);        
        for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){      	     
			
			// CREATE OPERATION SECURITIES RENEW OPERATION
			
        	create(objRenewalOperationDetail);
			saveAll(objRenewalOperationDetail.getRenewalMarketfactOperation());
			if(Validations.validateListIsNotNullAndNotEmpty(objRenewalOperationDetail.getRenewalBlockOperation())){
				for (RenewalBlockOperation objRenewalBlockOperation : objRenewalOperationDetail.getRenewalBlockOperation()) {
					create(objRenewalBlockOperation);
					saveAll(objRenewalBlockOperation.getRenewalMarketfactOperation());
				}
			}
        }       
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param objSecuritiesRenewalOperation the securities renewal operation
	 * @param businessProcessType the business process type
	 * @return the accounts component to
	 * @throws ServiceException the service exception
	 */
	public AccountsComponentTO getAccountsComponentTO(SecuritiesRenewalOperation objSecuritiesRenewalOperation, Long businessProcessType) throws ServiceException{
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk());
		objAccountComponent.setLstSourceAccounts(new ArrayList<HolderAccountBalanceTO>());
		objAccountComponent.setLstTargetAccounts(new ArrayList<HolderAccountBalanceTO>());
		objAccountComponent.setIdOperationType(objSecuritiesRenewalOperation.getCustodyOperation().getOperationType());
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		if(BusinessProcessType.RENEWAL_SECURITIES_REN.getCode().equals(objAccountComponent.getIdBusinessProcess())){
			
			 for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){			 
				 
				 getListHolderAccountBalance(objRenewalOperationDetail, objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ONE, null);
				 
			 }
			 
		}		
		if(BusinessProcessType.RENEWAL_SECURITIES_BLOCK.getCode().equals(objAccountComponent.getIdBusinessProcess())){
			
			for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){
				
				for(RenewalBlockOperation objRenewalBlockOperation : objRenewalOperationDetail.getRenewalBlockOperation()){
					
					if(objRenewalBlockOperation.getBlockOperation().getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
						
						HolderMarketFactBalance holderMarketFactBalance = getLastHolderMarketfactBalance(
								objRenewalBlockOperation.getBlockOperation().getParticipant().getIdParticipantPk(), 
								objRenewalBlockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk(), 
								objSecuritiesRenewalOperation.getSourceSecurity().getIdSecurityCodePk());
						
						getListHolderAccountBalance(objRenewalOperationDetail, objAccountComponent, ComponentConstant.SOURCE, 
								ComponentConstant.ZERO, holderMarketFactBalance);
						
						getListHolderAccountBalance(objRenewalOperationDetail, objAccountComponent, ComponentConstant.TARGET, 
								ComponentConstant.ZERO, null);
						
					}
					
				}
				
			}
		}		
		return objAccountComponent;
	}

	/**
	 * Gets the securities component to.
	 *
	 * @param objSecuritiesRenewalOperation the securities renewal operation
	 * @param sourceOrTarget the source or target
	 * @param businessType the business type
	 * @return the securities component to
	 */
	private SecuritiesComponentTO getSecuritiesComponentTO(SecuritiesRenewalOperation objSecuritiesRenewalOperation, Long sourceOrTarget, Long businessType){
		Long operationType = null;
		if (BusinessProcessType.RENEWAL_SECURITIES_REN.getCode().equals(businessType)) {
			operationType = ParameterOperationType.SECURITIES_OPERATION_SECURITIES_RENEWAL_DPF.getCode();
		}		
		SecuritiesOperation objSecuritiesOperation = createSecuritiesOperation(objSecuritiesRenewalOperation, sourceOrTarget, operationType);		
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();		
		securitiesComponentTO.setIdBusinessProcess(businessType);
		securitiesComponentTO.setSecuritiesRenew(true);
		securitiesComponentTO.setIdOperationType(objSecuritiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(objSecuritiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setIdSecurityCode(objSecuritiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setIndSourceTarget(sourceOrTarget);		
		if(securitiesComponentTO.getIdOperationType().equals(ParameterOperationType.SECURITIES_OPERATION_SECURITIES_RENEWAL_DPF.getCode())){
			securitiesComponentTO.setDematerializedBalance(objSecuritiesOperation.getStockQuantity());
			securitiesComponentTO.setShareBalance(objSecuritiesOperation.getStockQuantity());	
			securitiesComponentTO.setCirculationBalance(objSecuritiesOperation.getStockQuantity());	
		}			 
		return securitiesComponentTO;
	}
	
	/**
	 * Gets the securities operation.
	 *
	 * @param objSecuritiesRenewalOperation the securities renewal operation
	 * @param sourceorTarget the sourceor target
	 * @param operationType the operation type
	 * @return the securities operation
	 */
	private SecuritiesOperation createSecuritiesOperation(SecuritiesRenewalOperation objSecuritiesRenewalOperation, Long sourceorTarget, Long operationType) {
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		
		securitiesOperation.setCustodyOperation(objSecuritiesRenewalOperation.getCustodyOperation());
		securitiesOperation.setOperationDate(CommonsUtilities.currentDateTime());		
		BigDecimal totalBalance = BigDecimal.ZERO;
		for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){
			totalBalance = totalBalance.add(objRenewalOperationDetail.getTotalBalance());
		}
		securitiesOperation.setStockQuantity(totalBalance);
		if(sourceorTarget.equals(ComponentConstant.SOURCE)){
			securitiesOperation.setSecurities(objSecuritiesRenewalOperation.getSourceSecurity());
			securitiesOperation.setCashAmount(totalBalance.multiply(objSecuritiesRenewalOperation.getSourceSecurity().getCurrentNominalValue()));
		}else if(sourceorTarget.equals(ComponentConstant.TARGET)){
			securitiesOperation.setSecurities(objSecuritiesRenewalOperation.getTargetSecurity());
			securitiesOperation.setCashAmount(totalBalance.multiply(objSecuritiesRenewalOperation.getTargetSecurity().getCurrentNominalValue()));
		}				
		securitiesOperation.setOperationType(operationType);				
		return create(securitiesOperation);
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @param objRenewalOperationDetail the renewal operation detail
	 * @param objAccountComponent account component
	 * @param sourceTarget the source target
	 * @param availableOrBlocked the available or blocked
	 * @param holderMarketFactBalance the holder market fact balance
	 * @return the holder account balance
	 */
	private void getListHolderAccountBalance(RenewalOperationDetail objRenewalOperationDetail, AccountsComponentTO objAccountComponent, 
			Long sourceTarget, Integer availableOrBlocked, HolderMarketFactBalance holderMarketFactBalance){
		
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();						
		holderAccountBalanceTO.setStockQuantity(BigDecimal.ZERO);
		holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
		
		if(ComponentConstant.ONE.equals(availableOrBlocked)){
			
			if(objRenewalOperationDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
				
				holderAccountBalanceTO.setIdHolderAccount(objRenewalOperationDetail.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(objRenewalOperationDetail.getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setStockQuantity(objRenewalOperationDetail.getAvailableBalance());
				if(ComponentConstant.ONE.equals(objAccountComponent.getIndMarketFact())){
					for(RenewalMarketfactOperation objRenewalMarketfactOperation : objRenewalOperationDetail.getRenewalMarketfactOperation()){
						MarketFactAccountTO marketAccountTO=new MarketFactAccountTO();
						if(sourceTarget.equals(ComponentConstant.TARGET)){
							marketAccountTO.setMarketDate(objRenewalMarketfactOperation.getMarketDate() );
							marketAccountTO.setMarketPrice(objRenewalMarketfactOperation.getMarketPrice() );
							marketAccountTO.setMarketRate(objRenewalMarketfactOperation.getMarketRate() );								
						} else if(sourceTarget.equals(ComponentConstant.SOURCE)) {
							marketAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate() );
							marketAccountTO.setMarketPrice(holderMarketFactBalance.getMarketPrice() );
							marketAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate() );								
						}
						marketAccountTO.setQuantity( objRenewalMarketfactOperation.getOperationQuantity() );
						holderAccountBalanceTO.getLstMarketFactAccounts().add(marketAccountTO);
					}
				}
			}
		}		
		
		if(ComponentConstant.ZERO.equals(availableOrBlocked)){
			
			if(objRenewalOperationDetail.getBanBalance().compareTo(BigDecimal.ZERO) > 0 || 
					objRenewalOperationDetail.getPawnBalance().compareTo(BigDecimal.ZERO) > 0 ||
					objRenewalOperationDetail.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){								
				
				for(RenewalBlockOperation objRenewalBlockOperation : objRenewalOperationDetail.getRenewalBlockOperation()){
					if(LevelAffectationType.BLOCK.getCode().equals(objRenewalBlockOperation.getBlockOperation().getBlockLevel())){
						for(RenewalMarketfactOperation objRenewalMarketfactOperation : objRenewalBlockOperation.getRenewalMarketfactOperation()){
							MarketFactAccountTO marketAccountTO=new MarketFactAccountTO();
							if(sourceTarget.equals(ComponentConstant.TARGET)){
								holderAccountBalanceTO.setIdHolderAccount(objRenewalOperationDetail.getHolderAccount().getIdHolderAccountPk());
								holderAccountBalanceTO.setIdParticipant(objRenewalOperationDetail.getParticipant().getIdParticipantPk());
								marketAccountTO.setMarketDate(objRenewalMarketfactOperation.getMarketDate());
								marketAccountTO.setMarketPrice(objRenewalMarketfactOperation.getMarketPrice());
								marketAccountTO.setMarketRate(objRenewalMarketfactOperation.getMarketRate());								
							} else if(sourceTarget.equals(ComponentConstant.SOURCE)) {
								holderAccountBalanceTO.setIdHolderAccount(objRenewalBlockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk());
								holderAccountBalanceTO.setIdParticipant(objRenewalBlockOperation.getBlockOperation().getParticipant().getIdParticipantPk());
								marketAccountTO.setMarketDate(holderMarketFactBalance.getMarketDate() );
								marketAccountTO.setMarketPrice(holderMarketFactBalance.getMarketPrice() );
								marketAccountTO.setMarketRate(holderMarketFactBalance.getMarketRate() );								
							}
							marketAccountTO.setQuantity(objRenewalMarketfactOperation.getOperationQuantity());
							holderAccountBalanceTO.getLstMarketFactAccounts().add(marketAccountTO);
						}						
						holderAccountBalanceTO.setStockQuantity(holderAccountBalanceTO.getStockQuantity().add(objRenewalBlockOperation.getBlockQuantity()));
					}
				}
			}
		}		
		if(holderAccountBalanceTO.getStockQuantity().compareTo(BigDecimal.ZERO) > 0){
			if(sourceTarget.equals(ComponentConstant.SOURCE)){
				objAccountComponent.getLstSourceAccounts().add(holderAccountBalanceTO);
				holderAccountBalanceTO.setIdSecurityCode(objRenewalOperationDetail.getSecuritiesRenewalOperation().getSourceSecurity().getIdSecurityCodePk());
			} else if(sourceTarget.equals(ComponentConstant.TARGET)){
				objAccountComponent.getLstTargetAccounts().add(holderAccountBalanceTO);
				holderAccountBalanceTO.setIdSecurityCode(objRenewalOperationDetail.getSecuritiesRenewalOperation().getTargetSecurity().getIdSecurityCodePk());				
			}
		}
	}
	
	/**
	 * Create the block operation unblock.
	 *
	 * @param objSecuritiesRenewalOperation the securities renewal operation
	 * @param loggerUser LoggerUser
	 * @return the list
	 * @throws ServiceException the service exception
	 */	
	private List<RequestReversalsTO> executeBlockOperationsUnblock(SecuritiesRenewalOperation objSecuritiesRenewalOperation, LoggerUser loggerUser) throws ServiceException {
		 List<RequestReversalsTO> lstRequestReversalsTO = null;
		for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){			
			if(Validations.validateListIsNotNullAndNotEmpty(objRenewalOperationDetail.getRenewalBlockOperation())){
				lstRequestReversalsTO = new ArrayList<RequestReversalsTO>();
				for(RenewalBlockOperation objRenewalBlockOperation : objRenewalOperationDetail.getRenewalBlockOperation()){

					// single block_operation for reversal
					BlockOpeForReversalsTO blockOpeForReversal = new BlockOpeForReversalsTO();
	        		blockOpeForReversal.setIdBlockOperationPk(objRenewalBlockOperation.getBlockOperation().getIdBlockOperationPk());
	        		blockOpeForReversal.setAmountUnblock(objRenewalBlockOperation.getBlockQuantity());
					
	        		// header for creating the unblock_request: one per block_operation
	        		RegisterReversalTO registerReversalTO = new RegisterReversalTO();
	        		registerReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
	            	registerReversalTO.setRegistryUser(objSecuritiesRenewalOperation.getCustodyOperation().getConfirmUser());
	            	if(objRenewalBlockOperation.getBlockOperation().getBlockRequest().getBlockNumber() != null){
	            		registerReversalTO.setDocumentNumber(objRenewalBlockOperation.getBlockOperation().getBlockRequest().getBlockNumber());
	            	} else {
	            		registerReversalTO.setDocumentNumber(GeneralConstants.STR_FOR_COMPLETE);
	            	}
	        		registerReversalTO.setIdHolderPk(objRenewalBlockOperation.getBlockOperation().getHolderAccount().getRepresentativeHolder().getIdHolderPk());
	            	registerReversalTO.setTotalUnblock(objRenewalBlockOperation.getBlockQuantity());
	            	registerReversalTO.setIdState(RequestReversalsStateType.APPROVED.getCode());
	            	registerReversalTO.getLstBlockOpeForReversals().add(blockOpeForReversal);
	            	
	            	HolderMarketFactBalance holderMarketFactBalance = getLastHolderMarketfactBalance(objRenewalBlockOperation.getBlockOperation().getParticipant().getIdParticipantPk(), 
	            			objRenewalBlockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk(), objRenewalBlockOperation.getBlockOperation().getSecurities().getIdSecurityCodePk());
	            	
	            	if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
	            		blockOpeForReversal.setReversalsMarkFactList(new ArrayList<ReversalsMarkFactTO>());
	    				for(RenewalMarketfactOperation objRenewalMarketfactOperation: objRenewalBlockOperation.getRenewalMarketfactOperation()){
	    					ReversalsMarkFactTO reversalMarketFact = new ReversalsMarkFactTO();
	    					reversalMarketFact.setMarketDate(holderMarketFactBalance.getMarketDate());
	    					reversalMarketFact.setMarketPrice(holderMarketFactBalance.getMarketPrice());
	    					reversalMarketFact.setMarketRate(holderMarketFactBalance.getMarketRate());
	    					reversalMarketFact.setBlockAmount(objRenewalMarketfactOperation.getOperationQuantity());
	    					blockOpeForReversal.getReversalsMarkFactList().add(reversalMarketFact);
	    				}
	    			}
	            	
	            	//TO for confirmation
	            	RequestReversalsTO requestReversalsTO = new RequestReversalsTO();
	                requestReversalsTO.setIdUnblockRequestPk(requestReversalsServiceBean.saveReversalRequest(registerReversalTO));
	                registerReversalTO.setConfirmationUser(objSecuritiesRenewalOperation.getCustodyOperation().getConfirmUser());
	                requestReversalsTO.setBusinessProcessTypeId(BusinessProcessType.RENEWAL_SECURITIES_BLOCK.getCode());
	                requestReversalsServiceBean.confirmReversalRequest(requestReversalsTO, loggerUser, Boolean.TRUE);
	                lstRequestReversalsTO.add(requestReversalsTO);
				}
			}			
        }
		return lstRequestReversalsTO;
	}
	
	/**
	 * Create the block operation.
	 *
	 * @param objSecuritiesRenewalOperation the securities renewal operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */	
	private List<AffectationTransferTO> executeBlockOperationsBlock(SecuritiesRenewalOperation objSecuritiesRenewalOperation) throws ServiceException {
		List<AffectationTransferTO> lstAffectationTransferTO = null;
		List<AffectationTransferTO> transfers = new ArrayList<AffectationTransferTO>();
		for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){
			if(Validations.validateListIsNotNullAndNotEmpty(objRenewalOperationDetail.getRenewalBlockOperation())){
				for(RenewalBlockOperation objRenewalBlockOperation : objRenewalOperationDetail.getRenewalBlockOperation()){
					AffectationTransferTO at = new AffectationTransferTO();
					at.setRenewalBlockOperation(objRenewalBlockOperation);
					at.setIdHolderPk(objRenewalBlockOperation.getBlockOperation().getHolderAccount().getRepresentativeHolder().getIdHolderPk());
					at.setIdHolderAccountPk(objRenewalOperationDetail.getHolderAccount().getIdHolderAccountPk());
					at.setIdParticipantPk(objRenewalOperationDetail.getParticipant().getIdParticipantPk());
					at.setIdSecurityCodePk(objSecuritiesRenewalOperation.getTargetSecurity().getIdSecurityCodePk());
					at.setIdBlockOperationPk(objRenewalBlockOperation.getBlockOperation().getIdBlockOperationPk());
					at.setQuantity(objRenewalBlockOperation.getBlockQuantity());
					if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
						at.setMarketFactAffectations(new ArrayList<MarketFactTO>());
						for(RenewalMarketfactOperation objRenewalMarketfactOperation : objRenewalBlockOperation.getRenewalMarketfactOperation()){
							MarketFactTO marketFactAffectationTO = new MarketFactTO();
							marketFactAffectationTO.setMarketDate(objRenewalMarketfactOperation.getMarketDate());
							marketFactAffectationTO.setMarketPrice(objRenewalMarketfactOperation.getMarketPrice());
							marketFactAffectationTO.setMarketRate(objRenewalMarketfactOperation.getMarketRate());
							marketFactAffectationTO.setQuantity(objRenewalMarketfactOperation.getOperationQuantity());
							at.getMarketFactAffectations().add(marketFactAffectationTO);
						}
					}
					transfers.add(at);
				}
			}
			
			affectationsServiceBean.transferBlockOperations(transfers, BusinessProcessType.RENEWAL_SECURITIES_BLOCK.getCode(), 
					objSecuritiesRenewalOperation.getCustodyOperation().getConfirmUser(),idepositarySetup.getIndMarketFact()) ;
			
			if(Validations.validateListIsNotNullAndNotEmpty(transfers)){
				lstAffectationTransferTO = new ArrayList<AffectationTransferTO>();
				for(AffectationTransferTO objAffectationTransferTO : transfers){					
					RenewalBlockOperation objRenewalBlockOperation = new RenewalBlockOperation();					
					BlockOperation objBlockOperation = new BlockOperation();
					objBlockOperation.setIdBlockOperationPk(objAffectationTransferTO.getDetachedBlockOperation().getIdBlockOperationPk());				
					objRenewalBlockOperation.setBlockOperation(objBlockOperation);
					objRenewalBlockOperation.setRenewalOperationDetail(objRenewalOperationDetail);
					objRenewalBlockOperation.setBlockQuantity(objAffectationTransferTO.getQuantity());
					objRenewalBlockOperation.setIndSourceTarget(GeneralConstants.TWO_VALUE_INTEGER);
					objRenewalBlockOperation.setRenewalMarketfactOperation(new ArrayList<RenewalMarketfactOperation>());										
					for(MarketFactTO objMarketFactTO : objAffectationTransferTO.getMarketFactAffectations()){											
						RenewalMarketfactOperation objRenewalMarketfactOperation = new RenewalMarketfactOperation();
						objRenewalMarketfactOperation.setRenewalBlockOperation(objRenewalBlockOperation);
						objRenewalMarketfactOperation.setMarketDate(objMarketFactTO.getMarketDate());
						objRenewalMarketfactOperation.setMarketRate(objMarketFactTO.getMarketRate());
						objRenewalMarketfactOperation.setMarketPrice(objMarketFactTO.getMarketPrice());
						objRenewalMarketfactOperation.setOperationQuantity(objMarketFactTO.getQuantity());
						objRenewalBlockOperation.getRenewalMarketfactOperation().add(objRenewalMarketfactOperation);
					}										
					create(objRenewalBlockOperation);
					if(objRenewalBlockOperation.getRenewalMarketfactOperation()!=null){
						saveAll(objRenewalBlockOperation.getRenewalMarketfactOperation());
					}	
					lstAffectationTransferTO.add(objAffectationTransferTO);
				}				
			}
		}	
		return lstAffectationTransferTO;
	}
	
	/**
	 * Gets list the market fact to.
	 *
	 * @param idSecurityCode the security code
	 * @param participantId the participant
	 * @param idHolderAccount the holder account
	 * @param isExpired the is expired
	 * @return list the market fact to
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MarketFactTO> getHolderMarketFacts(String idSecurityCode, Long participantId, Long idHolderAccount, boolean isExpired) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();		
		sbQuery.append("SELECT" );
		sbQuery.append("	hmb.IdMarketfactBalancePk, ");
		sbQuery.append("	hmb.marketRate, ");
		sbQuery.append("	hmb.marketPrice, " );		
		sbQuery.append("	hmb.marketDate, " );			
		sbQuery.append("	hmb.totalBalance," );	
		sbQuery.append("	hmb.availableBalance" );
		sbQuery.append("  FROM  HolderMarketFactBalance hmb ");
		sbQuery.append(" WHERE hmb.security.idSecurityCodePk = :idSecurityCode ");
		sbQuery.append("   AND hmb.holderAccount.idHolderAccountPk = :idHolderAccount ");
		sbQuery.append("   AND hmb.participant.idParticipantPk = :idParticipant ");
		sbQuery.append("   AND hmb.indActive= :indicatorOne" );
		if(!isExpired){
			sbQuery.append(" AND hmb.availableBalance > 0");
		}				
		parameters.put("idSecurityCode", idSecurityCode);
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", participantId);		
		parameters.put("indicatorOne", ComponentConstant.ONE);		
		sbQuery.append(" ORDER BY hmb.marketRate desc ");		
        List<Object[]> objectList = findListByQueryString(sbQuery.toString(), parameters);
		List<MarketFactTO> holderAccountBalanceTOs = new ArrayList<MarketFactTO>();
		for(Object[] objects  : objectList){
			MarketFactTO holderAccountBalanceTO = new MarketFactTO();
			holderAccountBalanceTO.setIdMarketFactPk((Long) objects[0] );
			holderAccountBalanceTO.setMarketRate((BigDecimal) objects[1]);
			holderAccountBalanceTO.setMarketPrice((BigDecimal) objects[2]);
			holderAccountBalanceTO.setMarketDate((Date) objects[3]);
			holderAccountBalanceTO.setQuantity((BigDecimal) objects[5]);
			holderAccountBalanceTOs.add(holderAccountBalanceTO);			
		}
		return holderAccountBalanceTOs;
	}
	
	/**
	 * Update the corporative operation state.
	 *
	 * @param lstIdCorporativeOperation the list corporative operation
	 * @param corporativeState the corporative state
	 * @param loggerUser the logger user
	 */
	private void updateCorporativeOperationState(List<Long> lstIdCorporativeOperation, Integer corporativeState, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE CorporativeOperation ");
		stringBuffer.append(" SET state = :corporativeState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idCorporativeOperationPk in (:lstIdCorporativeOperation) ");		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdCorporativeOperation", lstIdCorporativeOperation);
		query.setParameter("corporativeState", corporativeState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		query.executeUpdate();
	}
	
	/**
	 * Update the program interest state.
	 *
	 * @param lstIdProgramInterestCoupon the list program interest coupon
	 * @param couponState the coupon state
	 * @param loggerUser the logger user
	 */
	private void updateProgramInterestState(List<Long> lstIdProgramInterestCoupon, Integer couponState, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramInterestCoupon ");
		stringBuffer.append(" SET stateProgramInterest = :couponState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramInterestPk in (:lstIdProgramInterestCoupon) ");		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdProgramInterestCoupon", lstIdProgramInterestCoupon);
		query.setParameter("couponState", couponState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		query.executeUpdate();
	}
	
	/**
	 * Update the program amortization state.
	 *
	 * @param lstIdProgramAmortizationCoupon the lst id program amortization coupon
	 * @param couponState the coupon state
	 * @param couponSituation the coupon situation
	 * @param loggerUser the logger user
	 */
	private void updateProgramAmortizationState(List<Long> lstIdProgramAmortizationCoupon, Integer couponState, Integer couponSituation, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramAmortizationCoupon ");
		stringBuffer.append(" SET stateProgramAmortization = :couponState ");
		if(couponSituation!=null){
			stringBuffer.append(" , situationProgramAmortization = :couponSituation ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramAmortizationPk in (:lstIdProgramAmortizationCoupon) ");		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdProgramAmortizationCoupon", lstIdProgramAmortizationCoupon);
		query.setParameter("couponState", couponState);
		if(couponSituation!=null){
			query.setParameter("couponSituation", couponSituation);
		}
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());		
		query.executeUpdate();
	}				
	
	/**
	 * Validate securities renewal query information.
	 *
	 * @param securitiesRenewalQueryTO the Securities Renewal Query TO
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> validateSecuritiesRenewalQueryInformation(SecuritiesRenewalQueryTO securitiesRenewalQueryTO) throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_SECURITIES_RENEWAL_QUERY.equalsIgnoreCase(securitiesRenewalQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					securitiesRenewalQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(securitiesRenewalQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					securitiesRenewalQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			securitiesRenewalQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//SECURITY CLASS (DPF - DPA)
		boolean isDpfDpa= false;
		ParameterTableTO filter = new ParameterTableTO();
		filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		for (ParameterTable parameterTable: securityClasses) {
			if (parameterTable.getText1().equalsIgnoreCase(securitiesRenewalQueryTO.getSecurityClassDesc())) {
				isDpfDpa= true;
				securitiesRenewalQueryTO.setSecurityClass(parameterTable.getParameterTablePk());
				break;
			}
		}
		if (!isDpfDpa) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					securitiesRenewalQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
				
		return lstRecordValidationType;
	}
	
	/**
	 * Gets the securities renewal query to.
	 *
	 * @param securitiesRenewalQueryTO the securities renewal query to
	 * @return the securities renewal query to
	 */
	public SecuritiesRenewalQueryTO getSecuritiesRenewalQueryTO(SecuritiesRenewalQueryTO securitiesRenewalQueryTO) {
		List<SecurityObjectTO> lstSecurityObjectTO= new ArrayList<SecurityObjectTO>();
		List<Object[]> lstSecuritiesRenewalOperation= getListDpfSecuritiesRenewalOperation(securitiesRenewalQueryTO.getOperationDate(), 
																							securitiesRenewalQueryTO.getIdParticipant(),
																							securitiesRenewalQueryTO.getSecurityClass());
		if (Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesRenewalOperation)) {
			for (Object[] objSecuritiesRenewal: lstSecuritiesRenewalOperation) {
				Long operationNumber= new Long(objSecuritiesRenewal[0].toString());
				SecuritiesRenewalOperation securitiesRenewalOperation= (SecuritiesRenewalOperation) objSecuritiesRenewal[1];
				String idSerialSecurityCodeOlder= objSecuritiesRenewal[2].toString();
				SecurityObjectTO securityObjectTO= holderBalanceMovementsServiceBean.populateSecurityObjectTO(securitiesRenewalOperation.getSourceSecurity());
				securityObjectTO.setOperationNumber(operationNumber);
				securityObjectTO.setSecuritySerialPrevious(idSerialSecurityCodeOlder);
				lstSecurityObjectTO.add(securityObjectTO);
			}
			securitiesRenewalQueryTO.setLstSecurityObjectTO(lstSecurityObjectTO);
		}
		return securitiesRenewalQueryTO;
	}
	
	/**
	 * Gets the list dpf securities renewal operation.
	 *
	 * @param operationDate the operation date
	 * @param idParticipant the id participant
	 * @param securityClass the security class
	 * @return the list dpf securities renewal operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListDpfSecuritiesRenewalOperation(Date operationDate, Long idParticipant, Integer securityClass) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT.operationNumber, ");
		stringBuffer.append(" SRO, ");
		stringBuffer.append(" SRO.targetSecurity.idSecurityCodeOnly ");
		stringBuffer.append(" FROM InterfaceTransaction IT, SecuritiesRenewalOperation SRO ");
		stringBuffer.append(" INNER JOIN FETCH SRO.sourceSecurity SE_SOURCE ");
		stringBuffer.append(" WHERE SRO.idSecuritiesRenewalPk = IT.custodyOperation.idCustodyOperationPk ");
		stringBuffer.append(" and trunc(IT.interfaceProcess.processDate) = :operationDate ");
		stringBuffer.append(" and IT.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		stringBuffer.append(" and SRO.sourceSecurity.securityClass = :securityClass ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationDate", operationDate);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		query.setParameter("securityClass", securityClass);
		
		return query.getResultList();
	}
	
	/**
	 * Validate and save reverse renew securities tx.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveReverseRenewSecuritiesTx(ReverseRegisterTO reverseRegisterTO, LoggerUser loggerUser) 
			throws ServiceException {
		
		Object objResult = validateAndSaveReverseRenewSecurities(reverseRegisterTO, loggerUser);		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * Validate and save reverse renewal securities operation.
	 *
	 * @param reverseRegisterTO the Reverse register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */	
	public Object validateAndSaveReverseRenewSecurities(ReverseRegisterTO reverseRegisterTO, LoggerUser loggerUser) 
			throws ServiceException {
		
		SecurityObjectTO securityObjectTO = reverseRegisterTO.getSecurityObjectTO();
		
		// VALIDATE SECURITY CODE
		
		String idSecurityCodeTarget = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());		
		String idSecurityCodeSource = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerialPrevious());
		
		// VALIDATE PARTICIPANT
		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(reverseRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);		
		if(objParticipant == null){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			reverseRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		// CHECK OPERATION NUMBER EXITS
		
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getOperationNumber(), 
						reverseRegisterTO.getOperationCode());
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// VALIDATE OPERATION DATE IS TODAY
		
		Date operationDate = CommonsUtilities.truncateDateTime(reverseRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
		}

		// VALIDATE OPERATION TYPE
		
		if(!(ComponentConstant.INTERFACE_REVERSE_RENEW_SECURITIES_DPF.equals(reverseRegisterTO.getOperationCode()))){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// VALIDATE SECURITY CLASS
					
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filter.setText1(securityObjectTO.getSecurityClassDesc());		
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			} else {
				securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
			}			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}
		
		// VALIDATE SECURITY TARGET		
		
		Security objSecurityTarget = new Security();		 
		if(idSecurityCodeTarget != null){
			objSecurityTarget = find(Security.class, idSecurityCodeTarget);
		}		
		if(objSecurityTarget != null){
			
			// VALIDATE STATE SECURITY
			
			if(!SecurityStateType.REGISTERED.getCode().equals(objSecurityTarget.getStateSecurity())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
						new Object[]{ SecurityStateType.lookup.get(objSecurityTarget.getStateSecurity()).getValue() });
			}
			
			// VALIDATE ISSUANCE DATE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getIssuanceDate()) 
					&& Validations.validateIsNotNullAndNotEmpty(objSecurityTarget.getIssuanceDate())){
				if(CommonsUtilities.convertDatetoString(securityObjectTO.getIssuanceDate()).compareTo(
						CommonsUtilities.convertDatetoString(objSecurityTarget.getIssuanceDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
				}				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
			}						
			
			// VALIDATE EXPIRATION DATE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getExpirationDate())
					&& Validations.validateIsNotNullAndNotEmpty(objSecurityTarget.getExpirationDate())){
				if(CommonsUtilities.convertDatetoString(securityObjectTO.getExpirationDate()).compareTo(
						CommonsUtilities.convertDatetoString(objSecurityTarget.getExpirationDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
			// VALIDATE CURRENCY SECURITY
			
			ParameterTableTO objParameterCurrency = new ParameterTableTO();
			objParameterCurrency.setText1(securityObjectTO.getCurrencyMnemonic());		
			objParameterCurrency.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstCurrency = parameterServiceBean.getListParameterTableServiceBean(objParameterCurrency);			
			if(Validations.validateIsNotNullAndNotEmpty(lstCurrency)){
				ParameterTable objParameterTable = lstCurrency.get(0);
				if(Validations.validateIsNotNull(objSecurityTarget.getCurrency())
						&& !objSecurityTarget.getCurrency().equals(objParameterTable.getParameterTablePk())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
						
			// VALIDATE NOMINAL VALUE
						
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getNominalValue()) &&
					Validations.validateIsNotNullAndNotEmpty(objSecurityTarget.getCurrentNominalValue())) {
				if(objSecurityTarget.getCurrentNominalValue().compareTo(CommonsUtilities.truncateDecimal(securityObjectTO.getNominalValue(), 
						GeneralConstants.TWO_VALUE_INTEGER)) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, null);
				} 
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, null);
			}
			
			// VALIDATE INTEREST RATE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getInterestRate()) && 
					Validations.validateIsNotNullAndNotEmpty(objSecurityTarget.getInterestRate())) {
				if(objSecurityTarget.getInterestRate().compareTo(securityObjectTO.getInterestRate()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
			}
			
			// VALIDATE NUMBER COUPONS
			
			boolean blValidateCupons = false;
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
				blValidateCupons = true;
			} else {
				if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
					blValidateCupons = true;
				}
			}
			
			if(blValidateCupons){
				if(!objSecurityTarget.getNumberCoupons().equals(1)){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
				}
			} else {
				if(!objSecurityTarget.getNumberCoupons().equals(securityObjectTO.getNumberCoupons())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
				}
			}	
			
			// VALIDATE ALTERNATIVE CODE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getAlternativeCode()) && 
					Validations.validateIsNotNullAndNotEmpty(objSecurityTarget.getAlternativeCode())){
				if(objSecurityTarget.getAlternativeCode().trim().compareTo(securityObjectTO.getAlternativeCode().trim()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
			}	
			
			// VALIDATE SOURCE SECURITY SERIAL PREVIUS
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getSecuritySerialPrevious()) && 
					Validations.validateIsNotNullAndNotEmpty(objSecurityTarget.getIdSourceSecurityCodePk())){
				if(objSecurityTarget.getAlternativeCode().trim().compareTo(securityObjectTO.getAlternativeCode().trim()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
			}
			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
		}
		
		// VALIDATE SECURITY SOURCE EXITS
				
		Security objSecuritySource = new Security();		 
		if(idSecurityCodeSource != null){
			objSecuritySource = find(Security.class, idSecurityCodeSource);
		}		
		if(objSecuritySource == null){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
		} else {
			
			// VALIDATE STATUS SECURITY
			
			if(!SecurityStateType.EXPIRED.getCode().equals(objSecuritySource.getStateSecurity())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
						new Object[]{ SecurityStateType.lookup.get(objSecuritySource.getStateSecurity()).getValue() });
			}													
		}
		
		// VALIDATE HOLDER ACCOUNT TARGET AND HOLDER ACCOUNT BALANCE
		
		HolderAccountBalance objHolAccBalTarget = new HolderAccountBalance();
		HolderAccount objHolderAccountTarget = new HolderAccount();
		
		if(objParticipant != null && objSecurityTarget != null){	
			
			HolderAccountMovement objHolderAccountMovement = getHolderAccountMovementRenewal(objSecurityTarget.getIdSecurityCodePk());
			if (Validations.validateIsNullOrEmpty(objHolderAccountMovement)) {				
				List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
						objParticipant.getIdParticipantPk(), objSecurityTarget.getIdSecurityCodePk(), false);
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalances)) {
					objHolAccBalTarget = lstHolderAccountBalances.get(0);
				} else {
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE_PARTICIPANT, null);
				}								
			} else {
				objHolAccBalTarget = objHolderAccountMovement.getHolderAccountBalance();
				objHolderAccountTarget = find(HolderAccount.class, objHolAccBalTarget.getHolderAccount().getIdHolderAccountPk());
			}			
		}		
		
		// VALIDATE HOLDER ACCOUNT SOURCE AND HOLDER ACCOUNT BALANCE
		
		HolderAccountBalance objHolAccBalSource = new HolderAccountBalance();
		HolderAccount objHolderAccountSource = new HolderAccount();
		
		if(objParticipant != null && objSecuritySource != null){
			
			HolderAccountMovement objHolderAccountMovement = getHolderAccountMovementRenewal(objSecuritySource.getIdSecurityCodePk());
			if (Validations.validateIsNullOrEmpty(objHolderAccountMovement)) {				
				List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
						objParticipant.getIdParticipantPk(), objSecuritySource.getIdSecurityCodePk(), true);
				if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalances)) {
					objHolAccBalSource = lstHolderAccountBalances.get(0);
				} else {
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE_PARTICIPANT, null);
				}								
			} else {
				objHolAccBalSource = objHolderAccountMovement.getHolderAccountBalance();
				objHolderAccountSource = find(HolderAccount.class, objHolAccBalSource.getHolderAccount().getIdHolderAccountPk());
			}			
		}		
		
		if(objParticipant != null && objSecurityTarget != null && objHolderAccountTarget != null && objHolAccBalTarget != null &&
				objSecuritySource != null && objHolderAccountSource != null && objHolAccBalSource != null ){
			
			// VALIDATE BUSSINES REVERSE SECURITIES RENEWAL OPERATIONS
			
			if(Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getReverseOperationNumber())){
				
				// OBTENER TRANSACCION DE RENOVACION AUTOMATICA
				
				List<InterfaceTransaction> lstInterfaceTransactionReverse = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getReverseOperationNumber(), ComponentConstant.INTERFACE_RENEW_SECURITIES_DPF);
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionReverse) &&
						lstInterfaceTransactionReverse.size() == 1){
					
					InterfaceTransaction objInterfaceTransaction = lstInterfaceTransactionReverse.get(0);	
					
					// OBTENER OPERACION DE RENOVACION AUTOMATICA
					
					SecuritiesRenewalOperation objSecuritiesRenewalOperation = find(SecuritiesRenewalOperation.class, 
							objInterfaceTransaction.getCustodyOperation().getIdCustodyOperationPk());					
					
					if(objSecuritiesRenewalOperation == null){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RENEW_RECURRENT, null);
					} else {
						
						// VALIDATE OPERATION DATE CUSTODY
						
						if(CommonsUtilities.convertDatetoString(objSecuritiesRenewalOperation.getCustodyOperation().getOperationDate()).compareTo(
								CommonsUtilities.convertDatetoString(reverseRegisterTO.getOperationDate())) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_RENEWAL_DATE, null);
						}
						
						// VALIDATE SECURITY TARGET
						
						if(objSecuritiesRenewalOperation.getTargetSecurity().getIdSecurityCodePk().compareTo(objSecurityTarget.getIdSecurityCodePk()) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
						}
						
						// VALIDATE SECURITY SOURCE
						
						if(objSecuritiesRenewalOperation.getSourceSecurity().getIdSecurityCodePk().compareTo(objSecuritySource.getIdSecurityCodePk()) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
						}												
					}															
					
					BigDecimal bdTotalBalance = BigDecimal.ZERO;
					List<Long> lstIdCustodyOperation = new ArrayList<Long>();
					Boolean isBlocked = Boolean.FALSE;
					
					// ELIMINAR LAS OPERACIONES DE RENOVACION
					
					for(RenewalOperationDetail objRenewalOperationDetail : objSecuritiesRenewalOperation.getRenewalOperationDetail()){
						
						lstIdCustodyOperation.add(objSecuritiesRenewalOperation.getCustodyOperation().getIdCustodyOperationPk());				
						bdTotalBalance = bdTotalBalance.add(objRenewalOperationDetail.getTotalBalance());
						
						// VERIFICAR EXISTENCIA DE OPERACIONES DE BLOQUEO
						
						if(Validations.validateListIsNotNullAndNotEmpty(objRenewalOperationDetail.getRenewalBlockOperation())){
							
							for(RenewalBlockOperation objRenewalBlockOperation : objRenewalOperationDetail.getRenewalBlockOperation()){								
								BlockOperationTO blockOperationTO = new BlockOperationTO();
								blockOperationTO.setIdBlockOperationPk(objRenewalBlockOperation.getBlockOperation().getIdBlockOperationPk());
								blockOperationTO.setIdHolderAccountPk(objRenewalBlockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk());
								blockOperationTO.setIdParticipantPk(objRenewalBlockOperation.getBlockOperation().getParticipant().getIdParticipantPk());								
								if(GeneralConstants.TWO_VALUE_INTEGER.equals(objRenewalBlockOperation.getIndSourceTarget())){
									blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
									blockOperationTO.setSecurityCodePk(objSecuritiesRenewalOperation.getTargetSecurity().getIdSecurityCodePk());
								} else {
									blockOperationTO.setIdStateAffectation(AffectationStateType.UNBLOCKED.getCode());
									blockOperationTO.setSecurityCodePk(objSecuritiesRenewalOperation.getSourceSecurity().getIdSecurityCodePk());
								}
								
								// OBTIENE LISTA DE OPERACIONES DE BLOQUEO, VALOR ORIGEN / VALOR DESTINO
								
								List<BlockOperation> lstBlockOperation = affectationsServiceBean.findBlockOperationsByFilters(blockOperationTO);
								
								if(Validations.validateListIsNotNullAndNotEmpty(lstBlockOperation)){																		
									
									isBlocked = Boolean.TRUE;
									
									for(BlockOperation objBlockOperation : lstBlockOperation){										
										
										// VERIFICAR EXISTENCIA DE MOVIMIENTOS POSTERIORES A LA RENOVACION PARA EL VALOR VIGENTE (DESTINO)
																				 											 
										if(GeneralConstants.TWO_VALUE_INTEGER.equals(objRenewalBlockOperation.getIndSourceTarget())){											 
											lstIdCustodyOperation.add(objBlockOperation.getIdBlockOperationPk());											
											 
											// ENVIAR A ELIMINAR LOS MOVIMIENTOS DE AFECTACIONES DEL VALOR DESTINO
											 
											 deleteBlockMovementRenewal(objBlockOperation.getIdBlockOperationPk(), true);
											 
											 removeOperationsServiceBean.deleteSecuritiesMovementAndSecurityOperationRenewal(objBlockOperation.getCustodyOperation(), 
													 objBlockOperation.getSecurities());										 
											 
											 removeOperationsServiceBean.deleteHolderAccountMovementRenewal(objBlockOperation.getCustodyOperation());
											 
											 //	ENVIAR A ELIMINAR DOCUMENTOS DE AFECTACIONES
											 
											 deleteBlockMarketFactOperationRenewal(objBlockOperation);
											 deleteBlockRequestFileRenewal(objBlockOperation.getBlockRequest());											 
											 deleteBlockOperationRenewal(objBlockOperation);
											 deleteBlockRequestRenewal(objBlockOperation.getBlockRequest());
											 
											 removeOperationsServiceBean.updateInterfaceTransaction(objBlockOperation.getCustodyOperation(), loggerUser);
											 
											 removeOperationsServiceBean.deleteCustodyOperationRenewal(objBlockOperation.getCustodyOperation());											 											 
											 
										 } else {
											 
											 //	ENVIAR A ELIMINAR DOCUMENTOS DE DESAFECTACIONES DEL VALOR ORIGEN
											 
											 BlockOperationTO unblockOperationTO = new BlockOperationTO();
											 unblockOperationTO.setIdBlockOperationPk(objBlockOperation.getIdBlockOperationPk());
											 List<UnblockOperation> lstUnblockOperation = affectationsServiceBean.findUnblockOperationsByFilters(unblockOperationTO);											 																						 
											 
											 if(Validations.validateListIsNotNullAndNotEmpty(lstUnblockOperation)){
												 for(UnblockOperation objUnblockOperation : lstUnblockOperation){
													 
													 //	ENVIAR A ELIMINAR LOS MOVIMIENTOS DE DESAFECTACIONES
													 
													 deleteBlockMovementRenewal(objUnblockOperation.getIdUnblockOperationPk(), false);
													 
													 removeOperationsServiceBean.deleteSecuritiesMovementAndSecurityOperationRenewal(
															 objUnblockOperation.getCustodyOperation(), objBlockOperation.getSecurities());										 
													 
													 removeOperationsServiceBean.deleteHolderAccountMovementRenewal(objUnblockOperation.getCustodyOperation());
													 
													 deleteUnblockMarketFactOperationRenewal(objUnblockOperation);
													 deleteUnblockRequestFileRenewal(objUnblockOperation.getUnblockRequest());
													 deleteUnblockRequestRenewal(objUnblockOperation.getUnblockRequest());
													 deleteUnblockOperationRenewal(objUnblockOperation);
													 
													 removeOperationsServiceBean.updateInterfaceTransaction(objUnblockOperation.getCustodyOperation(), loggerUser);
													 
													 removeOperationsServiceBean.deleteCustodyOperationRenewal(objUnblockOperation.getCustodyOperation());
												 }
											 }	
											 
											 // CAMBIAR DE ESTADO A LAS OPERACIONES DE AFECTACIONES INICIALES DEL VALOR ORIGEN: DE DESBLOQUEADOS A BLOQUEADOS
											 
											 updateStateCustodyOperation(objBlockOperation, loggerUser);
											 updateStateBlockOperation(objBlockOperation, bdTotalBalance, loggerUser);
											 updateStateBlockMarketfactOperation(objBlockOperation, bdTotalBalance, loggerUser);
										 }
									}																										
								}
								
								// ELIMINAR MARKETFACT BLOCK OPERATION
								
								deleteRenewalMarketfactOperationBlock(objRenewalBlockOperation);
																
							}
							
							// VALIDAR SI HUBIERON MOVIMIENTOS DESPUES DE LA RENOVACION, RENOVACION CON BLOQUEOS
							
							List<HolderAccountMovement> lstHolderAccountMovement = removeOperationsServiceBean.getValidateHolderAccountMovement(
									objParticipant.getIdParticipantPk(), objHolderAccountTarget.getIdHolderAccountPk(),
									objSecuritiesRenewalOperation.getTargetSecurity().getIdSecurityCodePk(), lstIdCustodyOperation);											
							if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovement)){												
								 return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
											GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_MOVEMENTS_EXISTS, null);												 
							}
							
						} 
						
						// ELIMINAR BLOCK OPERATION
						
						deleteRenewalBlockOperation(objRenewalOperationDetail);
						
						// VALIDAR SI HUBIERON MOVIMIENTOS DESPUES DE LA RENOVACION, RENOVACION SOLO DISPONIBLES
						
						List<HolderAccountMovement> lstHolderAccountMovementRenew = removeOperationsServiceBean.getValidateHolderAccountMovement(objParticipant.getIdParticipantPk(), 
								objHolderAccountSource.getIdHolderAccountPk(), objSecuritiesRenewalOperation.getTargetSecurity().getIdSecurityCodePk(), 
								lstIdCustodyOperation);
						
						if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovementRenew)){												
							 return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_MOVEMENTS_EXISTS, null);												 
						 }
						
						// DELETE MOVEMENT TARGET SECURITY, COMPONENT SECURITIES AND ISSUANCE
						
						removeOperationsServiceBean.deleteSecuritiesMovementAndSecurityOperationRenewal(objSecuritiesRenewalOperation.getCustodyOperation(), 
								objSecuritiesRenewalOperation.getTargetSecurity());
						
						// DELETE MOVEMENT SORUCE SECURITY, COMPONENT SECURITIES AND ISSUANCE
						
						removeOperationsServiceBean.deleteSecuritiesMovementAndSecurityOperationRenewal(objSecuritiesRenewalOperation.getCustodyOperation(), 
								objSecuritiesRenewalOperation.getSourceSecurity());										
						
						// DELETE MOVEMENT SOURCE SECURITY AND TARGET SECURITY, COMPONENT ACCOUNT
						
						removeOperationsServiceBean.deleteHolderAccountMovementRenewal(objSecuritiesRenewalOperation.getCustodyOperation());
						
						// ELIMINAR MARKETFACT OPERATION
						
						deleteRenewalMarketfactOperationDetail(objRenewalOperationDetail);	
					}
					
					deleteRenewalOperationDetail(objSecuritiesRenewalOperation);
					deleteSecuritiesRenewalOperation(objSecuritiesRenewalOperation);	
					
					removeOperationsServiceBean.updateInterfaceTransaction(objSecuritiesRenewalOperation.getCustodyOperation(), loggerUser);
					removeOperationsServiceBean.deleteCustodyOperationRenewal(objSecuritiesRenewalOperation.getCustodyOperation());
					
					// OBTENEMOS DATOS DEL VALOR ANTERIOR
					
					BigDecimal bdCurrentNominalSource = BigDecimal.ZERO;
					SecurityHistoryBalance objSecurityHistoryBalance = getSecurityHistoryBalance(objSecuritiesRenewalOperation.getSourceSecurity().getIdSecurityCodePk());
					if(objSecurityHistoryBalance != null && 
							Validations.validateIsNotNullAndNotEmpty(objSecurityHistoryBalance.getNominalValue())){						
						bdCurrentNominalSource = objSecurityHistoryBalance.getNominalValue();
					}
					
					// SI EL VALOR A SIDO VENCIDO EN LA RENOVACION O ES BLOQUEADO
					
					if(GeneralConstants.ONE_VALUE_INTEGER.equals(objSecuritiesRenewalOperation.getSecurityExpiration()) || isBlocked){
						
						// MODIFICAR MONTOS DEL VALOR ORIGEN A NIVEL DE VALOR
						
						updateSecuritySource(objSecuritiesRenewalOperation.getSourceSecurity(), bdTotalBalance, bdCurrentNominalSource, loggerUser);						
						
						// MODIFICAR MONTOS DEL VALOR ORIGEN A NIVEL DE EMISION	
						
						updateIssuanceSource(objSecuritiesRenewalOperation.getSourceSecurity(), bdTotalBalance, bdCurrentNominalSource, loggerUser);						
						
						// MODIFICAR MONTOS DEL VALOR ORIGEN A NIVEL DE CUENTAS	
						
						updateHolderAccountBalanceSource(objHolAccBalSource, objHolAccBalTarget, loggerUser);
						updateHolderMarketFactBalanceSource(objHolAccBalSource, objHolAccBalTarget, loggerUser);
					}
					
					
					// ELIMINAR CUENTA MATRIZ DEL VALOR DESTINO
					
					removeOperationsServiceBean.deleteHolderAccountBalanceTarget(objHolAccBalTarget);
					removeOperationsServiceBean.deleteHolderMarketFactBalanceTarget(objHolAccBalTarget);
					
					// DELETE SECURITY AND  PAYMENT CHRONOGRAM TARGET
					
					removeOperationsServiceBean.deleteSecurityAndPaymentChronogramTarget(objSecuritiesRenewalOperation.getTargetSecurity());
					
					if(GeneralConstants.ONE_VALUE_INTEGER.equals(objSecuritiesRenewalOperation.getSecurityExpiration()) || isBlocked){
						
						// UPDATE SECURITY SOURCE				
						
						updateReverseRenewalPaymentChronogram(objSecuritiesRenewalOperation.getSourceSecurity(), loggerUser);
					}
					
					
				} else { 
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RENEW_RECURRENT, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RENEW_RECURRENT, null);
			}			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RENEW_RECURRENT, null);
		}
		return reverseRegisterTO;
	}
	
	
	
	
	
	/**
	 * Delete block movement renewal.
	 *
	 * @param idBlockUnblock the id block unblock
	 * @param isBlocked the is blocked
	 * @throws ServiceException the service exception
	 */
	public void deleteBlockMovementRenewal(Long idBlockUnblock, boolean isBlocked) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(idBlockUnblock)){
			sbQuery.append(" Delete BlockMovement bm  where ");
			if(isBlocked){
				sbQuery.append(" bm.blockOperation.idBlockOperationPk = :parIdBlockUnblock ");
			} else {
				sbQuery.append(" bm.unblockOperation.idUnblockOperationPk = :parIdBlockUnblock ");
			}						
			Query query = em.createQuery(sbQuery.toString());	
			if(isBlocked){
				query.setParameter("parIdBlockUnblock", idBlockUnblock);
			} else {
				query.setParameter("parIdBlockUnblock", idBlockUnblock);
			}			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete SecuritiesRenewalOperation.
	 *
	 * @param objSecuritiesRenewalOperation SecuritiesRenewalOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritiesRenewalOperation(SecuritiesRenewalOperation objSecuritiesRenewalOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk())){
			sbQuery.append(" Delete SecuritiesRenewalOperation sro  ");	
			sbQuery.append(" where sro.idSecuritiesRenewalPk = :parIdSecuritiesRenewal ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdSecuritiesRenewal", objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete RenewalOperationDetail.
	 *
	 * @param objSecuritiesRenewalOperation SecuritiesRenewalOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteRenewalOperationDetail(SecuritiesRenewalOperation objSecuritiesRenewalOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk())){
			sbQuery.append(" Delete RenewalOperationDetail rod  ");	
			sbQuery.append(" where rod.securitiesRenewalOperation.idSecuritiesRenewalPk = :parIdSecuritiesRenewal ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdSecuritiesRenewal", objSecuritiesRenewalOperation.getIdSecuritiesRenewalPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete RenewalBlockOperation.
	 *
	 * @param objRenewalOperationDetail RenewalOperationDetail
	 * @throws ServiceException the service exception
	 */
	public void deleteRenewalBlockOperation(RenewalOperationDetail objRenewalOperationDetail) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(objRenewalOperationDetail.getIdRenewalDetailPk())){
			sbQuery.append(" Delete RenewalBlockOperation rbo  ");
			sbQuery.append(" where rbo.renewalOperationDetail.idRenewalDetailPk = :parIdRenewalDetail ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdRenewalDetail", objRenewalOperationDetail.getIdRenewalDetailPk());
			query.executeUpdate();
		}								
	}
	
	/**
	 * The method is used to delete RenewalMarketfactOperation.
	 *
	 * @param objRenewalOperationDetail RenewalOperationDetail
	 * @throws ServiceException the service exception
	 */
	public void deleteRenewalMarketfactOperationDetail(RenewalOperationDetail objRenewalOperationDetail) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objRenewalOperationDetail.getIdRenewalDetailPk())){
			sbQuery.append(" Delete RenewalMarketfactOperation rm  ");
			sbQuery.append(" where rm.renewalOperationDetail.idRenewalDetailPk = :parIdRenewalDetail ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdRenewalDetail", objRenewalOperationDetail.getIdRenewalDetailPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete RenewalMarketfactOperation.
	 *
	 * @param objRenewalBlockOperation RenewalBlockOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteRenewalMarketfactOperationBlock(RenewalBlockOperation objRenewalBlockOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();				
		if(Validations.validateIsNotNullAndNotEmpty(objRenewalBlockOperation.getIdRenewalBlockPk())){
			sbQuery.append(" Delete RenewalMarketfactOperation rm  ");
			sbQuery.append(" where rm.renewalBlockOperation.idRenewalBlockPk = :parIdRenewalBlock ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdRenewalBlock", objRenewalBlockOperation.getIdRenewalBlockPk());
			query.executeUpdate();
		}		
	}
	
	
	
	/**
	 * The method is used to delete BlockOperation.
	 *
	 * @param objBlockOperation BlockOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteBlockOperationRenewal(BlockOperation objBlockOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objBlockOperation.getIdBlockOperationPk())){
			sbQuery.append(" Delete BlockOperation bo  ");
			sbQuery.append(" where bo.idBlockOperationPk = :parIdBlockOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdBlockOperation", objBlockOperation.getIdBlockOperationPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete BlockRequest.
	 *
	 * @param objBlockRequest BlockRequest
	 * @throws ServiceException the service exception
	 */
	public void deleteBlockRequestRenewal(BlockRequest objBlockRequest) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getIdBlockRequestPk())){
			sbQuery.append(" Delete BlockRequest br  ");
			sbQuery.append(" where br.idBlockRequestPk = :parIdBlockRequest ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdBlockRequest", objBlockRequest.getIdBlockRequestPk());
			query.executeUpdate();
		}						
	}
	
	/**
	 * The method is used to delete BlockRequestFile.
	 *
	 * @param objBlockRequest BlockRequest
	 * @throws ServiceException the service exception
	 */
	public void deleteBlockRequestFileRenewal(BlockRequest objBlockRequest) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objBlockRequest.getIdBlockRequestPk())){
			sbQuery.append(" Delete BlockRequestFile brf  ");
			sbQuery.append(" where brf.blockRequest.idBlockRequestPk = :parIdBlockRequest ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdBlockRequest", objBlockRequest.getIdBlockRequestPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete BlockMarketFactOperation.
	 *
	 * @param objBlockOperation BlockOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteBlockMarketFactOperationRenewal(BlockOperation objBlockOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objBlockOperation.getIdBlockOperationPk())){
			sbQuery.append(" Delete BlockMarketFactOperation bmfo  ");
			sbQuery.append(" where bmfo.blockOperation.idBlockOperationPk = :parIdBlockOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdBlockOperation", objBlockOperation.getIdBlockOperationPk());
			query.executeUpdate();
		}		
	}	
	
	/**
	 * The method is used to delete UnblockOperation.
	 *
	 * @param objUnblockOperation UnblockOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteUnblockOperationRenewal(UnblockOperation objUnblockOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockOperation.getIdUnblockOperationPk())){
			sbQuery.append(" Delete UnblockOperation uo  ");
			sbQuery.append(" where uo.idUnblockOperationPk = :parIdUnblockOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdUnblockOperation", objUnblockOperation.getIdUnblockOperationPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete UnblockRequest.
	 *
	 * @param objUnblockRequest UnblockRequest
	 * @throws ServiceException the service exception
	 */
	public void deleteUnblockRequestRenewal(UnblockRequest objUnblockRequest) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockRequest.getIdUnblockRequestPk())){
			sbQuery.append(" Delete UnblockRequest ur  ");
			sbQuery.append(" where ur.idUnblockRequestPk = :parIdUnblockRequest ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdUnblockRequest", objUnblockRequest.getIdUnblockRequestPk());
			query.executeUpdate();
		}						
	}
	
	/**
	 * The method is used to delete UnblockRequestFile.
	 *
	 * @param objUnblockRequest UnblockRequest
	 * @throws ServiceException the service exception
	 */
	public void deleteUnblockRequestFileRenewal(UnblockRequest objUnblockRequest) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockRequest.getIdUnblockRequestPk())){
			sbQuery.append(" Delete UnblockRequestFile urf  ");
			sbQuery.append(" where urf.unblockRequest.idUnblockRequestPk = :parIdUnblockRequest ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdUnblockRequest", objUnblockRequest.getIdUnblockRequestPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete UnblockMarketFactOperation.
	 *
	 * @param objUnblockOperation UnblockOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteUnblockMarketFactOperationRenewal(UnblockOperation objUnblockOperation ) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockOperation.getIdUnblockOperationPk())){
			sbQuery.append(" Delete UnblockMarketFactOperation umfo  ");
			sbQuery.append(" where umfo.unblockOperation.idUnblockOperationPk = :parIdUnblockOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdUnblockOperation", objUnblockOperation.getIdUnblockOperationPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to update state CustodyOperation.
	 *
	 * @param objBlockOperation BlockOperation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateCustodyOperation(BlockOperation objBlockOperation, LoggerUser loggerUser) throws ServiceException  {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update CustodyOperation custodyOperation set custodyOperation.state = :parState , "+
				" custodyOperation.lastModifyUser = :parLastModifyUser, custodyOperation.lastModifyDate = :parLastModifyDate, " +
				" custodyOperation.lastModifyIp = :parLastModifyIp, custodyOperation.lastModifyApp = :parLastModifyApp "+
				" where custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objBlockOperation.getCustodyOperation().getIdCustodyOperationPk());
		parameters.put("parState", AffectationStateType.BLOCKED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update state BlockOperation.
	 *
	 * @param objBlockOperation BlockOperation
	 * @param bdTotalBalance the bd total balance
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateBlockOperation(BlockOperation objBlockOperation, BigDecimal bdTotalBalance, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update BlockOperation blockOperation set blockOperation.blockState = :parState ," +
				" blockOperation.actualBlockBalance = :parTotalBalance, "+
				" blockOperation.lastModifyUser = :parLastModifyUser, blockOperation.lastModifyDate = :parLastModifyDate, " +
				" blockOperation.lastModifyIp = :parLastModifyIp, blockOperation.lastModifyApp = :parLastModifyApp " +
				" where blockOperation.custodyOperation.idCustodyOperationPk = :parIdCustodyOperationPk ");
		parameters.put("parIdCustodyOperationPk", objBlockOperation.getCustodyOperation().getIdCustodyOperationPk());
		parameters.put("parTotalBalance", bdTotalBalance);
		parameters.put("parState", AffectationStateType.BLOCKED.getCode());
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update state BlockMarketfactOperation.
	 *
	 * @param objBlockOperation BlockOperation
	 * @param bdTotalBalance the bd total balance
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateStateBlockMarketfactOperation(BlockOperation objBlockOperation, BigDecimal bdTotalBalance, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update BlockMarketFactOperation blockMarketFactOperation set " +
				" blockMarketFactOperation.actualBlockBalance = :parTotalBalance , " +
				" blockMarketFactOperation.lastModifyUser = :parLastModifyUser, blockMarketFactOperation.lastModifyDate = :parLastModifyDate, " +
				" blockMarketFactOperation.lastModifyIp = :parLastModifyIp, blockMarketFactOperation.lastModifyApp = :parLastModifyApp " +
				" where blockMarketFactOperation.blockOperation.idBlockOperationPk = :parIdCustodyOperationPk " +
				" and blockMarketFactOperation.indActive = :indicatorOne ");
		parameters.put("parIdCustodyOperationPk", objBlockOperation.getCustodyOperation().getIdCustodyOperationPk());
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("parTotalBalance", bdTotalBalance);		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update PaymentChronogram.
	 *
	 * @param objSecurity Security
	 * @param loggerUser LoggerUser
	 * @throws ServiceException the service exception
	 */
	private void updateReverseRenewalPaymentChronogram(Security objSecurity, LoggerUser loggerUser) throws ServiceException{		
		Object [] arrSecurityDetail = removeOperationsServiceBean.getSecurityDetails(objSecurity.getIdSecurityCodePk());
		Integer securityState = (Integer)arrSecurityDetail[0];
		Integer securityClass = (Integer)arrSecurityDetail[1];
		String strIdIssuanceCode = arrSecurityDetail[2].toString();
		if(securityState.equals(SecurityStateType.REGISTERED.getCode())){
			//we must reverse annul all the payment coupons and corporative events
			List<Long> lstIdProgramInterestCoupon= new ArrayList<Long>();
			List<Long> lstIdProgramAmortizationCoupon= new ArrayList<Long>();
			List<Long> lstIdCorporativeOperation= new ArrayList<Long>();
			List<CorporativeOperation> lstCorporativeOperation= getListReverseCorporativeOperation(objSecurity.getIdSecurityCodePk());
			if (Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)) {
				for (CorporativeOperation objCorporativeOperation: lstCorporativeOperation) {
					lstIdCorporativeOperation.add(objCorporativeOperation.getIdCorporativeOperationPk());
					if (Validations.validateIsNotNull(objCorporativeOperation.getProgramAmortizationCoupon())) {
						lstIdProgramAmortizationCoupon.add(objCorporativeOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
					} else if (Validations.validateIsNotNull(objCorporativeOperation.getProgramInterestCoupon())) {
						lstIdProgramInterestCoupon.add(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk());
					}
					
					// DELETE MOVEMENT SOURCE SECURITY, COMPONENT SECURITIES AND ISSUANCE					
					removeOperationsServiceBean.deleteSecuritiesMovementAndRenewalCorporative(objCorporativeOperation, objSecurity);
					
					// DELETE MOVEMENT SOURCE SECURITY AND TARGET SECURITY, COMPONENT ACCOUNT					
					removeOperationsServiceBean.deleteHolderAccountMovementRenewal(objCorporativeOperation);
				}
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdCorporativeOperation)) {
				updateCorporativeOperationState(lstIdCorporativeOperation, CorporateProcessStateType.REGISTERED.getCode(), loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramAmortizationCoupon)) {
				Integer state = null;
				Integer situation = null;
				if(securityClass.equals(SecurityClassType.DPF.getCode()) || securityClass.equals(SecurityClassType.DPA.getCode())){
					state = ProgramScheduleStateType.PENDING.getCode();
					situation = ProgramScheduleSituationType.PENDING.getCode();
				}else{
					state = ProgramScheduleStateType.PENDING.getCode();
				}				
				updateProgramAmortizationState(lstIdProgramAmortizationCoupon, state, situation , loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				updateProgramInterestState(lstIdProgramInterestCoupon, ProgramScheduleStateType.PENDING.getCode(), loggerUser);
			}
			updateIssuance(strIdIssuanceCode, IssuanceStateType.AUTHORIZED.getCode(), loggerUser);
		}
	}
	
	/**
	 * Gets the list reverse corporative operation.
	 *
	 * @param idSecurityCode the id security code
	 * @return the list reverse corporative operation
	 */
	private List<CorporativeOperation> getListReverseCorporativeOperation(String idSecurityCode) {
		List<CorporativeOperation> lstCorporativeOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT CO ");
		stringBuffer.append(" FROM CorporativeOperation CO ");
		stringBuffer.append(" INNER JOIN FETCH CO.corporativeEventType ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programInterestCoupon ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programAmortizationCoupon ");
		stringBuffer.append(" WHERE CO.securities.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" AND CO.state in (:lstState) ");		
		TypedQuery<CorporativeOperation> typedQuery= em.createQuery(stringBuffer.toString(),CorporativeOperation.class);
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		List<Integer> lstState= new ArrayList<Integer>();		
		lstState.add(CorporateProcessStateType.DEFINITIVE.getCode());	
		typedQuery.setParameter("lstState", lstState);		
		lstCorporativeOperation= typedQuery.getResultList();
		return lstCorporativeOperation;
	}
	
	/**
	 * The method is used to update security.
	 *
	 * @param objSecurity Security
	 * @param bdTotalBalance BigDecimal
	 * @param currentNominal the current nominal
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSecuritySource(Security objSecurity, BigDecimal bdTotalBalance, BigDecimal currentNominal, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update Security security set security.stateSecurity = :parState , "+
				" security.shareCapital = :parShareCapital, security.shareBalance = :parShareBalance , " +
				" security.circulationAmount = :parCirculationAmount, security.circulationBalance = :parCirculationBalance, " +
				" security.desmaterializedBalance = :parDesmaterializedBalance,  " +
				" security.currentNominalValue = :parcurrentNominalValue, " +
				" security.lastModifyUser = :parLastModifyUser, security.lastModifyDate = :parLastModifyDate, " +
				" security.lastModifyIp = :parLastModifyIp, security.lastModifyApp = :parLastModifyApp " +
				" where security.idSecurityCodePk = :parIdSecurityCode ");
		parameters.put("parIdSecurityCode", objSecurity.getIdSecurityCodePk());		
		parameters.put("parState", SecurityStateType.REGISTERED.getCode());
		parameters.put("parShareCapital", currentNominal.multiply(bdTotalBalance));
		parameters.put("parShareBalance", bdTotalBalance);
		parameters.put("parCirculationAmount", currentNominal.multiply(bdTotalBalance));
		parameters.put("parCirculationBalance", bdTotalBalance);
		parameters.put("parDesmaterializedBalance", bdTotalBalance);
		parameters.put("parcurrentNominalValue", currentNominal);		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	
	/**
	 * The method is used to update issuance source.
	 *
	 * @param objSecuritySource Security
	 * @param bdTotalBalance the bd total balance
	 * @param currentNominal the current nominal
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateIssuanceSource(Security objSecuritySource, BigDecimal bdTotalBalance, BigDecimal currentNominal, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update Issuance issuance set  "+				
				" issuance.circulationAmount = :parCirculationAmount , " +
				" issuance.amortizationAmount = :parAmortizationAmount , " +
				" issuance.stateIssuance = :parState , " +		
				" issuance.lastModifyUser = :parLastModifyUser , issuance.lastModifyDate = :parLastModifyDate , " +
				" issuance.lastModifyIp = :parLastModifyIp , issuance.lastModifyApp = :parLastModifyApp " +
				" where issuance.idIssuanceCodePk = :parIdIssuanceCode ");
		parameters.put("parIdIssuanceCode", objSecuritySource.getIssuance().getIdIssuanceCodePk());		
		parameters.put("parCirculationAmount", currentNominal.multiply(bdTotalBalance));
		parameters.put("parAmortizationAmount", BigDecimal.ZERO);	
		parameters.put("parState", IssuanceStateType.AUTHORIZED.getCode());		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * Gets the security history balance.
	 *
	 * @param idSecurityCode the id security code
	 * @return the security history balance
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public SecurityHistoryBalance getSecurityHistoryBalance(String idSecurityCode) throws ServiceException {
		SecurityHistoryBalance objSecurityHistoryBalance = new SecurityHistoryBalance();
		List<SecurityHistoryBalance> lstSecurityHistoryBalance = new ArrayList<SecurityHistoryBalance>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT shb ");
			sbQuery.append(" FROM SecurityHistoryBalance shb  ");			
			sbQuery.append(" WHERE shb.security.idSecurityCodePk = :parSecurityCode  ");
			sbQuery.append(" ORDER BY shb.idSecHistoryBalancePk desc  ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parSecurityCode", idSecurityCode);
			lstSecurityHistoryBalance = (List<SecurityHistoryBalance>) query.getResultList();
			if(Validations.validateListIsNotNullAndNotEmpty(lstSecurityHistoryBalance)){
				objSecurityHistoryBalance = lstSecurityHistoryBalance.get(0);
			} else {
				objSecurityHistoryBalance = null;
			}			
		} catch (NoResultException e) {
			return null;
		}				
		return objSecurityHistoryBalance;
	}
	
	/**
	 * The method is used to update holder account source.
	 *
	 * @param objHolderAccountBalanceSource the obj holder account balance source
	 * @param objHolderAccountBalanceTarget the obj holder account balance target
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateHolderAccountBalanceSource(HolderAccountBalance objHolderAccountBalanceSource, 
			HolderAccountBalance objHolderAccountBalanceTarget, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update HolderAccountBalance habal set  "+				
				" habal.totalBalance = :parTotalBalance , " +
				" habal.availableBalance = :parAvailableBalance , " +
				" habal.pawnBalance = :parPawnBalance , " +
				" habal.banBalance = :parBanBalance , " +
				" habal.otherBlockBalance = :parOtherBlockBalance , " +				
				" habal.lastModifyUser = :parLastModifyUser , habal.lastModifyDate = :parLastModifyDate , " +
				" habal.lastModifyIp = :parLastModifyIp , habal.lastModifyApp = :parLastModifyApp " +
				" where habal.holderAccount.idHolderAccountPk = :parIdHolderAccount " +
				" and habal.participant.idParticipantPk = :parIdParticipant " +
				" and habal.security.idSecurityCodePk = :parIdSecurityCode ");
		parameters.put("parIdHolderAccount", objHolderAccountBalanceSource.getHolderAccount().getIdHolderAccountPk());		
		parameters.put("parIdParticipant", objHolderAccountBalanceSource.getParticipant().getIdParticipantPk());
		parameters.put("parIdSecurityCode", objHolderAccountBalanceSource.getSecurity().getIdSecurityCodePk());	
		
		parameters.put("parTotalBalance", objHolderAccountBalanceTarget.getTotalBalance());	
		parameters.put("parAvailableBalance", objHolderAccountBalanceTarget.getAvailableBalance());	
		parameters.put("parPawnBalance", objHolderAccountBalanceTarget.getPawnBalance());	
		parameters.put("parBanBalance", objHolderAccountBalanceTarget.getBanBalance());
		parameters.put("parOtherBlockBalance", objHolderAccountBalanceTarget.getOtherBlockBalance());	
		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to update holder account market fact balance source.
	 *
	 * @param objHolderAccountBalanceSource the obj holder account balance source
	 * @param objHolderAccountBalanceTarget the obj holder account balance target
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateHolderMarketFactBalanceSource(HolderAccountBalance objHolderAccountBalanceSource, 
			HolderAccountBalance objHolderAccountBalanceTarget, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update HolderMarketFactBalance habal set  "+				
				" habal.totalBalance = :parTotalBalance , " +
				" habal.availableBalance = :parAvailableBalance , " +
				" habal.pawnBalance = :parPawnBalance , " +
				" habal.banBalance = :parBanBalance , " +
				" habal.otherBlockBalance = :parOtherBlockBalance , " +				
				" habal.lastModifyUser = :parLastModifyUser , habal.lastModifyDate = :parLastModifyDate , " +
				" habal.lastModifyIp = :parLastModifyIp , habal.lastModifyApp = :parLastModifyApp " +
				" where habal.holderAccount.idHolderAccountPk = :parIdHolderAccount " +
				" and habal.participant.idParticipantPk = :parIdParticipant " +
				" and habal.security.idSecurityCodePk = :parIdSecurityCode ");
		parameters.put("parIdHolderAccount", objHolderAccountBalanceSource.getHolderAccount().getIdHolderAccountPk());		
		parameters.put("parIdParticipant", objHolderAccountBalanceSource.getParticipant().getIdParticipantPk());
		parameters.put("parIdSecurityCode", objHolderAccountBalanceSource.getSecurity().getIdSecurityCodePk());	
		
		parameters.put("parTotalBalance", objHolderAccountBalanceTarget.getTotalBalance());	
		parameters.put("parAvailableBalance", objHolderAccountBalanceTarget.getAvailableBalance());	
		parameters.put("parPawnBalance", objHolderAccountBalanceTarget.getPawnBalance());	
		parameters.put("parBanBalance", objHolderAccountBalanceTarget.getBanBalance());
		parameters.put("parOtherBlockBalance", objHolderAccountBalanceTarget.getOtherBlockBalance());	
		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * Gets the holder market fact balance.
	 *
	 * @param idParticipant the participant
	 * @param idHolderAccount the holder account
	 * @param idSecurityCode the security
	 * @return holder market fact balance
	 * @throws ServiceException the service exception
	 */
	private HolderMarketFactBalance getLastHolderMarketfactBalance(Long idParticipant, Long idHolderAccount, String idSecurityCode) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT hmb from HolderMarketFactBalance hmb " );
		sbQuery.append("	where hmb.participant.idParticipantPk = :idParticipant " );
		sbQuery.append("	and   hmb.holderAccount.idHolderAccountPk = :idHolderAccount " );
		sbQuery.append("	and   hmb.security.idSecurityCodePk = :idSecurity " );
		sbQuery.append("	and   hmb.indActive= :indicatorOne" );
		sbQuery.append("	ORDER BY hmb.marketDate, hmb.lastModifyDate DESC");		
		parameters.put("idSecurity", idSecurityCode);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", idParticipant);	
		
		@SuppressWarnings("unchecked")
		List<HolderMarketFactBalance> listaHolderMarketFactBalance = findListByQueryString(sbQuery.toString(), parameters);
		
		HolderMarketFactBalance lstResult = null;
		
		if(listaHolderMarketFactBalance != null && listaHolderMarketFactBalance.size() > 0){
			lstResult = listaHolderMarketFactBalance.get(0);
		}		
		
		return lstResult;
	}
	
	/**
	 * Gets the holder account movement renewal.
	 *
	 * @param idSecurityCode the id security code
	 * @return the holder account movement renewal
	 */
	@SuppressWarnings("unchecked")
	public HolderAccountMovement getHolderAccountMovementRenewal(String idSecurityCode){
		HolderAccountMovement objHolderAccountMovement = new HolderAccountMovement();
		List<HolderAccountMovement> lstHolderAccountMovement = new ArrayList<HolderAccountMovement>();
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" SELECT ham ");
			sbQuery.append(" FROM HolderAccountMovement ham  ");
			sbQuery.append(" JOIN FETCH ham.holderAccountBalance hab ");
			sbQuery.append(" WHERE hab.security.idSecurityCodePk = :parSecurityCode  ");
			sbQuery.append(" ORDER BY ham.idHolderAccountMovementPk desc  ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parSecurityCode", idSecurityCode);
			lstHolderAccountMovement = (List<HolderAccountMovement>) query.getResultList();
			if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovement)){
				objHolderAccountMovement = lstHolderAccountMovement.get(0);
			} else {
				objHolderAccountMovement = null;
			}			
		} catch (NoResultException e) {
			return null;
		}				
		return objHolderAccountMovement;
	}
	
	/**
	 * Gets the lis source security code.
	 *
	 * @param strSourceSecurityPk the str source security pk
	 * @return the lis source security code
	 */
	public List<Security> getLisSourceSecurityCode(String strSourceSecurityPk)  {
		List<Security> lstSecurity = null;
		if(Validations.validateIsNotNullAndNotEmpty(strSourceSecurityPk)){			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT sec ");
			sbQuery.append("	FROM Security sec ");
			sbQuery.append("	WHERE trim(sec.idSourceSecurityCodePk) = trim(:idSecurityCodePkPrm) ");
			TypedQuery<Security> typedQuery= em.createQuery(sbQuery.toString(), Security.class);
			typedQuery.setParameter("idSecurityCodePkPrm", strSourceSecurityPk);
			lstSecurity = typedQuery.getResultList();
			return lstSecurity;
		}
		return lstSecurity;
	}
	
	/**
	 * Update the payment chronogram.
	 *
	 * @param idSecurityCode the security code
	 * @param loggerUser LoggerUser
	 * @throws ServiceException the service exception
	 */
	private void updatePaymentChronogram(String idSecurityCode, LoggerUser loggerUser) throws ServiceException{		
		Object[] arrSecurityDetail = removeOperationsServiceBean.getSecurityDetails(idSecurityCode);
		Integer securityState = (Integer)arrSecurityDetail[0];
		Integer securityClass = (Integer)arrSecurityDetail[1];		
		String strIdIssuanceCode = arrSecurityDetail[2].toString();
		if(securityState.equals(SecurityStateType.EXPIRED.getCode())){
			//we must annul all the payment coupons and corporative events
			List<Long> lstIdProgramInterestCoupon= new ArrayList<Long>();
			List<Long> lstIdProgramAmortizationCoupon= new ArrayList<Long>();
			List<Long> lstIdCorporativeOperation= new ArrayList<Long>();
			List<CorporativeOperation> lstCorporativeOperation = removeOperationsServiceBean.getListCorporativeOperation(idSecurityCode);
			if (Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)) {
				for (CorporativeOperation objCorporativeOperation: lstCorporativeOperation) {
					lstIdCorporativeOperation.add(objCorporativeOperation.getIdCorporativeOperationPk());
					if (Validations.validateIsNotNull(objCorporativeOperation.getProgramAmortizationCoupon())) {
						lstIdProgramAmortizationCoupon.add(objCorporativeOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
					} else if (Validations.validateIsNotNull(objCorporativeOperation.getProgramInterestCoupon())) {
						lstIdProgramInterestCoupon.add(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk());
					}
				}
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdCorporativeOperation)) {
				updateCorporativeOperationState(lstIdCorporativeOperation, CorporateProcessStateType.DEFINITIVE.getCode(), loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramAmortizationCoupon)) {
				Integer state = null;
				Integer situation = null;
				if(securityClass.equals(SecurityClassType.DPF.getCode()) || securityClass.equals(SecurityClassType.DPA.getCode())){
					state = ProgramScheduleStateType.EXPIRED.getCode();
					situation = ProgramScheduleSituationType.ISSUER_PAID.getCode();
				} else {
					state = ProgramScheduleStateType.EXPIRED.getCode();
				}				
				updateProgramAmortizationState(lstIdProgramAmortizationCoupon, state, situation , loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				updateProgramInterestState(lstIdProgramInterestCoupon, ProgramScheduleStateType.EXPIRED.getCode(), loggerUser);
			}
			updateIssuance(strIdIssuanceCode, IssuanceStateType.EXPIRED.getCode(), loggerUser);
		}
	}
		
    /**
     * Update issuance.
     *
     * @param idIssuanceCodePk the id issuance code pk
     * @param issuanceState the issuance state
     * @param loggerUser the logger user
     */
    public void updateIssuance(String idIssuanceCodePk, Integer issuanceState, LoggerUser loggerUser) {
    	StringBuffer stringBuffer = new StringBuffer();
    	stringBuffer.append(" UPDATE Issuance ");
    	stringBuffer.append(" SET stateIssuance = :issuanceState ");
    	stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
    	stringBuffer.append(" WHERE idIssuanceCodePk = :idIssuanceCodePk) ");    	
    	Query query= em.createQuery(stringBuffer.toString());
    	query.setParameter("issuanceState", issuanceState);
    	query.setParameter("idIssuanceCodePk", idIssuanceCodePk);
    	query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
    }
	
}
