package com.pradera.custody.vaultrules.servicebean;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.vaultrules.to.RegisterVaultRulesTO;
import com.pradera.custody.vaultrules.to.SearchVaultsRulesTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.model.component.VaultControlRules;

@Stateless
public class VaultsRulesServiceBean extends CrudDaoServiceBean {
	
	public List<Integer> searchCabinets() {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT DISTINCT(RB.ARMARIO) ");
		sbQuery.append(" FROM REGLAS_BOVEDA RB ORDER BY RB.ARMARIO ASC " );
		
		Query query = em.createNativeQuery(sbQuery.toString());
				
		return query.getResultList();		
	}
	
	public List<VaultControlRules> searchVaultRules(SearchVaultsRulesTO searchVaultsRulesTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT VCR FROM VaultControlRules VCR ");
		sbQuery.append(" WHERE 1 = 1");
		
		if(searchVaultsRulesTO.getCabinet() != null) {
			sbQuery.append(" AND VCR.cabinet = :pCabinet ");
		}
		
		if(searchVaultsRulesTO.getLevel() != null) {
			sbQuery.append(" AND VCR.level = :pLevel ");
		}
		
		if(searchVaultsRulesTO.getBox() != null) {
			sbQuery.append(" AND VCR.box = :pBox ");
		}
		
		sbQuery.append(" ORDER BY VCR.id ASC ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(searchVaultsRulesTO.getCabinet() != null) {
			query.setParameter("pCabinet", searchVaultsRulesTO.getCabinet());
		}
		
		if(searchVaultsRulesTO.getLevel() != null) {
			query.setParameter("pLevel", searchVaultsRulesTO.getLevel());
		}
		
		if(searchVaultsRulesTO.getBox() != null) {
			query.setParameter("pBox", searchVaultsRulesTO.getBox());
		}
		
		return (List<VaultControlRules>)query.getResultList();
	}

	public List<Integer> searchLevels(SearchVaultsRulesTO searchVaultsRulesTO) {
		// TODO Auto-generated method stub
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT DISTINCT(RB.NIVEL) ");
		sbQuery.append(" FROM REGLAS_BOVEDA RB ");
		sbQuery.append(" WHERE RB.ARMARIO = :pCabinet ");
		sbQuery.append(" ORDER BY NIVEL ASC ");
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("pCabinet", searchVaultsRulesTO.getCabinet());
				
		return query.getResultList();	
	}

	public List<Integer> searchBoxs(SearchVaultsRulesTO searchVaultsRulesTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT DISTINCT(RB.CAJA) ");
		sbQuery.append(" FROM REGLAS_BOVEDA RB ");
		sbQuery.append(" WHERE 1 = 1 ");
		
		if(searchVaultsRulesTO.getCabinet() != null) {
			sbQuery.append(" AND RB.ARMARIO = :pCabinet ");
		}
		
		if(searchVaultsRulesTO.getLevel() != null) {
			sbQuery.append(" AND RB.NIVEL= :pLevel ");
		}
		
		sbQuery.append(" ORDER BY CAJA ASC ");
		Query query = em.createNativeQuery(sbQuery.toString());
		
		if(searchVaultsRulesTO.getCabinet() != null) {
			query.setParameter("pCabinet", searchVaultsRulesTO.getCabinet());
		}
		
		if(searchVaultsRulesTO.getLevel() != null) {
			query.setParameter("pLevel", searchVaultsRulesTO.getLevel());		
		}
			
		return query.getResultList();	
	}

	public BigDecimal getLastCabinet() {
		// TODO Auto-generated method stub
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT MAX(RB.ARMARIO) ");
		sbQuery.append(" FROM REGLAS_BOVEDA RB ");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		return (BigDecimal) query.getSingleResult();
	}
	
	public void modifyBoxVaultControl(VaultControlRules vaultControlRules) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_MODIFY_BOX_VAULT_CONTROL(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:cabinet, ");
		stringBuffer.append(" 	:level, ");
		stringBuffer.append(" 	:box); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", vaultControlRules.getCertificatesPerBox());
		query.setParameter("cabinet", vaultControlRules.getCabinet());
	    query.setParameter("level", vaultControlRules.getLevel());
		query.setParameter("box", vaultControlRules.getBox());
		
		query.executeUpdate();
	}
	
	public void modifyBoxVaultControlForIssuers(VaultControlRules vaultControlRules) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_MODIFY_BOX_VAULT_FOR_ISSUER(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:issuersPerBox, ");
		stringBuffer.append(" 	:cabinet, ");
		stringBuffer.append(" 	:level, ");
		stringBuffer.append(" 	:box); ");
		//stringBuffer.append("   :securitiesClass, ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", vaultControlRules.getCertificatesPerBox());
		query.setParameter("issuersPerBox", vaultControlRules.getIssuersPerBox());
		query.setParameter("cabinet", vaultControlRules.getCabinet());
		query.setParameter("level", vaultControlRules.getLevel());
		query.setParameter("box", vaultControlRules.getBox());
		//query.setParameter("securitiesClass", vaultControlRules.getSecuritiesClass());
		
		query.executeUpdate();
	}
	
	public void modifyBoxVaultControForExpirationDate(VaultControlRules vaultControlRules) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_MODIFY_BOX_VAULT_FOR_DATES(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:cabinet, ");
		stringBuffer.append(" 	:level, ");
		stringBuffer.append(" 	:box, ");
		stringBuffer.append("   :securitiesClass); ");		
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", vaultControlRules.getCertificatesPerBox());
		query.setParameter("cabinet", vaultControlRules.getCabinet());
		query.setParameter("level", vaultControlRules.getLevel());
		query.setParameter("box", vaultControlRules.getBox());
		query.setParameter("securitiesClass", vaultControlRules.getSecuritiesClass());
		
		query.executeUpdate();
	}
	
	public void createCabinet(RegisterVaultRulesTO registerVaultRulesTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_ADD_CABINET_VAULT_CONTROL(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:boxPerLevel, ");
		stringBuffer.append(" 	:levelsPerCabinet); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", registerVaultRulesTO.getCapacityPerBox());
		query.setParameter("boxPerLevel", registerVaultRulesTO.getBoxPerLevel());
		query.setParameter("levelsPerCabinet", registerVaultRulesTO.getLevelsPerCabinet());

		query.executeUpdate();
		em.flush();
		em.clear();
	}
	
	public void createCabinetForActions(RegisterVaultRulesTO registerVaultRulesTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" BEGIN SP_ADD_CABINET_FOR_ACTIONS(");
		stringBuffer.append(" 	:certificatesPerBox, ");
		stringBuffer.append(" 	:boxPerLevel, ");
		stringBuffer.append(" 	:levelsPerCabinet, ");
		stringBuffer.append(" 	:issuersPerBox); ");
		stringBuffer.append(" 	END; ");

		Query query= em.createNativeQuery(stringBuffer.toString());
		
		query.setParameter("certificatesPerBox", registerVaultRulesTO.getCapacityPerBox());
		query.setParameter("boxPerLevel", registerVaultRulesTO.getBoxPerLevel());
		query.setParameter("levelsPerCabinet", registerVaultRulesTO.getLevelsPerCabinet());
		query.setParameter("issuersPerBox", registerVaultRulesTO.getIssuersPerBox());
		
		query.executeUpdate();
	}
	
	public String getIssuersPerBox(VaultControlRules filter) {
		StringBuffer stringBuffer = new StringBuffer();
		String issuers = "";
		stringBuffer.append(" SELECT DISTINCT(I.MNEMONIC) FROM CONTROL_BOVEDA CB INNER JOIN SECURITY SE ON ");
		stringBuffer.append(" CB.ID_SECURITY_CODE_FK = SE.ID_SECURITY_CODE_PK ");
		stringBuffer.append(" INNER JOIN ISSUER I ON ");
		stringBuffer.append(" I.ID_ISSUER_PK = SE.ID_ISSUER_FK  ");
		stringBuffer.append(" WHERE CB.ARMARIO = :cabinet AND CB.NIVEL = :nivel AND CB.CAJA = :caja ");
		
		Query query= em.createNativeQuery(stringBuffer.toString());
		query.setParameter("cabinet", filter.getCabinet());
		query.setParameter("nivel", filter.getLevel());
		query.setParameter("caja", filter.getBox());
		
		for(String vaultRule : (List<String>)query.getResultList()) {
			issuers = issuers.concat(vaultRule).concat(";");
		}
		
		return issuers;
	}
	
	public Long getFreeSpacePerBox(RegisterVaultRulesTO registerVaultRulesTO) {
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT count(*) FROM VaultControlComponent p ");
		stringBuffer.append(" WHERE p.armario = :cabinet ");
		stringBuffer.append(" AND p.nivel = :level ");
		stringBuffer.append(" AND p.caja = :box ");
		stringBuffer.append(" AND p.idSecurityCodeFk IS NULL ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("cabinet", registerVaultRulesTO.getCabinet().toString());
		query.setParameter("level", registerVaultRulesTO.getLevel());
		query.setParameter("box", registerVaultRulesTO.getBox());

		return (Long) query.getSingleResult();
		
	}

	public List<VaultControlRules> searchVaultRulesForModify(RegisterVaultRulesTO registerVaultRulesTO) {

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT VCR FROM VaultControlRules VCR ");
		sbQuery.append(" WHERE 1 = 1");
		
		if(registerVaultRulesTO.getCabinet() != null) {
			sbQuery.append(" AND VCR.cabinet = :pCabinet ");
		}
		
		if(registerVaultRulesTO.getLevel() != null) {
			sbQuery.append(" AND VCR.level = :pLevel ");
		}
		
		if(registerVaultRulesTO.getBox() != null) {
			sbQuery.append(" AND VCR.box = :pBox ");
		}
		
		sbQuery.append(" ORDER BY VCR.id ASC ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(registerVaultRulesTO.getCabinet() != null) {
			query.setParameter("pCabinet", registerVaultRulesTO.getCabinet());
		}
		
		if(registerVaultRulesTO.getLevel() != null) {
			query.setParameter("pLevel", registerVaultRulesTO.getLevel());
		}
		
		if(registerVaultRulesTO.getBox() != null) {
			query.setParameter("pBox", registerVaultRulesTO.getBox());
		}
		
		return (List<VaultControlRules>)query.getResultList();
	
	}
	
	public Long getFreeSpacePerBox(VaultControlRules vaultControlRules) {
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT count(*) FROM VaultControlComponent p ");
		stringBuffer.append(" WHERE p.armario = :cabinet ");
		stringBuffer.append(" AND p.nivel = :level ");
		stringBuffer.append(" AND p.caja = :box ");
		stringBuffer.append(" AND p.idSecurityCodeFk IS NULL ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("cabinet", vaultControlRules.getCabinet().toString());
		query.setParameter("level", vaultControlRules.getLevel());
		query.setParameter("box", vaultControlRules.getBox());

		return (Long) query.getSingleResult();
		
	}

	public Long getNumberIssuersPerBox(RegisterVaultRulesTO registerVaultRulesTO) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT p.issuersPerBox FROM VaultControlRules p ");
		stringBuffer.append(" WHERE p.armario = :cabinet ");
		stringBuffer.append(" AND p.nivel = :level ");
		stringBuffer.append(" AND p.caja = :box ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("cabinet", registerVaultRulesTO.getCabinet());
		query.setParameter("level", registerVaultRulesTO.getLevel());
		query.setParameter("box", registerVaultRulesTO.getBox());
		
		return (Long) query.getSingleResult();
	}

	public void updateExpirationDateOfBox(VaultControlRules vaultControlRules) {
		// TODO Auto-generated method stub
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE VaultControlRules p set ");
		stringBuffer.append(" p.expirationDate = :expirationDate ");
		stringBuffer.append(" WHERE p.cabinet = :cabinet ");
		stringBuffer.append(" AND p.level = :level ");
		stringBuffer.append(" AND p.box = :box ");		
		Query query = em.createQuery(stringBuffer.toString());
		
		query.setParameter("cabinet", vaultControlRules.getCabinet());
		query.setParameter("level", vaultControlRules.getLevel());
		query.setParameter("box", vaultControlRules.getBox());
		query.setParameter("expirationDate", vaultControlRules.getExpirationDate());
		
		query.executeUpdate();
	}

	public Long verifyCabinetRulesCreated(RegisterVaultRulesTO registerVaultRulesTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT count(*) FROM VaultControlRules p ");
		stringBuffer.append(" WHERE p.cabinet = :cabinet ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("cabinet", registerVaultRulesTO.getCabinet());

		return (Long) query.getSingleResult();
	}
	
	public Long verifyVaultControlCreated(RegisterVaultRulesTO registerVaultRulesTO) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT count(*) FROM VaultControlComponent p ");
		stringBuffer.append(" WHERE p.armario = :cabinet ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("cabinet", registerVaultRulesTO.getCabinet().toString());

		return (Long) query.getSingleResult();
	}
	
}