package com.pradera.custody.vaultrules.to;

import java.io.Serializable;

import com.pradera.model.generalparameter.ParameterTable;

public class RegisterVaultRulesTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer instrumentType;
	
	private Integer securityClass;
	
	private Integer cabinet;
	
	private Integer level;
	
	private Integer box;
	
	private Integer levelsPerCabinet;
	
	private Integer boxPerLevel;
	
	private Integer capacityPerBox;
	
	private Integer issuersPerBox;
	
	private Integer indBoxForIssuers;
	
	private Integer newCapacityPerBox;
	
	private Integer newIssuersPerBox;
	
	private Integer expirationYear;
	
	private ParameterTable selectedSecurityClass;

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getCabinet() {
		return cabinet;
	}

	public void setCabinet(Integer cabinet) {
		this.cabinet = cabinet;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getBox() {
		return box;
	}

	public void setBox(Integer box) {
		this.box = box;
	}

	public Integer getCapacityPerBox() {
		return capacityPerBox;
	}

	public void setCapacityPerBox(Integer capacityPerBox) {
		this.capacityPerBox = capacityPerBox;
	}

	public Integer getLevelsPerCabinet() {
		return levelsPerCabinet;
	}

	public void setLevelsPerCabinet(Integer levelsPerCabinet) {
		this.levelsPerCabinet = levelsPerCabinet;
	}

	public Integer getBoxPerLevel() {
		return boxPerLevel;
	}

	public void setBoxPerLevel(Integer boxPerLevel) {
		this.boxPerLevel = boxPerLevel;
	}

	public Integer getIssuersPerBox() {
		return issuersPerBox;
	}

	public void setIssuersPerBox(Integer issuersPerBox) {
		this.issuersPerBox = issuersPerBox;
	}

	public ParameterTable getSelectedSecurityClass() {
		return selectedSecurityClass;
	}

	public void setSelectedSecurityClass(ParameterTable selectedSecurityClass) {
		this.selectedSecurityClass = selectedSecurityClass;
	}

	public Integer getIndBoxForIssuers() {
		return indBoxForIssuers;
	}

	public void setIndBoxForIssuers(Integer indBoxForIssuers) {
		this.indBoxForIssuers = indBoxForIssuers;
	}

	public Integer getNewCapacityPerBox() {
		return newCapacityPerBox;
	}

	public void setNewCapacityPerBox(Integer newCapacityPerBox) {
		this.newCapacityPerBox = newCapacityPerBox;
	}

	public Integer getNewIssuersPerBox() {
		return newIssuersPerBox;
	}

	public void setNewIssuersPerBox(Integer newIssuersPerBox) {
		this.newIssuersPerBox = newIssuersPerBox;
	}

	public Integer getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(Integer expirationYear) {
		this.expirationYear = expirationYear;
	}
	
}