package com.pradera.custody.vaultrules.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.component.VaultControlRules;

public class SearchVaultsRulesTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer cabinet;
	
	private Integer level;
	
	private Integer box;
	
	private Date expirationDate;
	
	private Integer numberIssuerPerBox;
	
	private Integer securityClass;
	
	private Integer capacity;
	
	private String issuersPerBox;
	
	private List<VaultControlRules> lstVaultsRulesResult;
	
	private Boolean isSelected;

	public Integer getCabinet() {
		return cabinet;
	}

	public void setCabinet(Integer cabinet) {
		this.cabinet = cabinet;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getBox() {
		return box;
	}

	public void setBox(Integer box) {
		this.box = box;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getNumberIssuerPerBox() {
		return numberIssuerPerBox;
	}

	public void setNumberIssuerPerBox(Integer numberIssuerPerBox) {
		this.numberIssuerPerBox = numberIssuerPerBox;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getIssuersPerBox() {
		return issuersPerBox;
	}

	public void setIssuersPerBox(String issuersPerBox) {
		this.issuersPerBox = issuersPerBox;
	}

	public List<VaultControlRules> getLstVaultsRulesResult() {
		return lstVaultsRulesResult;
	}

	public void setLstVaultsRulesResult(List<VaultControlRules> lstVaultsRulesResult) {
		this.lstVaultsRulesResult = lstVaultsRulesResult;
	}

	public Boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
	
}