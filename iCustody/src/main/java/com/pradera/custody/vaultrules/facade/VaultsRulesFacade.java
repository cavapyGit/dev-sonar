package com.pradera.custody.vaultrules.facade;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.custody.vaultrules.servicebean.VaultsRulesServiceBean;
import com.pradera.custody.vaultrules.to.RegisterVaultRulesTO;
import com.pradera.custody.vaultrules.to.SearchVaultsRulesTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.VaultControlRules;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class VaultsRulesFacade {
	
	@EJB
	VaultsRulesServiceBean vaultsRulesServiceBean;
	
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	public List<Integer> searchCabinets() throws ServiceException {
		
		return vaultsRulesServiceBean.searchCabinets();
	}
	
	public List<VaultControlRules> searchVaultRules(SearchVaultsRulesTO searchVaultsRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.searchVaultRules(searchVaultsRulesTO);
	}
	
	public  List<Integer> searchLevels(SearchVaultsRulesTO searchVaultsRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.searchLevels(searchVaultsRulesTO);
	}
	
	public List<Integer> searchBoxs(SearchVaultsRulesTO searchVaultsRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.searchBoxs(searchVaultsRulesTO);
	}
	
	public Integer getLastCabinet() throws ServiceException {
		return vaultsRulesServiceBean.getLastCabinet().intValue();
	}
	
	public void createCabinet(RegisterVaultRulesTO registerVaultRulesTO) throws ServiceException {
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		vaultsRulesServiceBean.createCabinet(registerVaultRulesTO);
	}
	
	public void createCabinetForActions(RegisterVaultRulesTO registerVaultRulesTO) throws ServiceException {
		vaultsRulesServiceBean.createCabinetForActions(registerVaultRulesTO);
	}
	
	public String getIssuersPerBox(VaultControlRules vaultControlRules) throws ServiceException {
		return vaultsRulesServiceBean.getIssuersPerBox(vaultControlRules);
	}
	
	public Integer getFreeSpacePerBox(RegisterVaultRulesTO registerVaultRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.getFreeSpacePerBox(registerVaultRulesTO).intValue();
	}
	
	public void modifyBoxVaultControl(VaultControlRules vaultControlRules) throws ServiceException {
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		vaultsRulesServiceBean.modifyBoxVaultControl(vaultControlRules); 
	}
	
	public void modifyBoxVaultControlForIssuers(VaultControlRules vaultControlRules) throws ServiceException {
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		vaultsRulesServiceBean.modifyBoxVaultControlForIssuers(vaultControlRules);
	}
	
	public void modifyBoxVaultControForExpirationDate(VaultControlRules vaultControlRules) throws ServiceException {
		//LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		//loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		vaultsRulesServiceBean.modifyBoxVaultControForExpirationDate(vaultControlRules);
	}
	
	public List<VaultControlRules> searchVaultRulesForModify(RegisterVaultRulesTO registerVaultRulesTOg) throws ServiceException {
		return vaultsRulesServiceBean.searchVaultRulesForModify(registerVaultRulesTOg);
	}
	
	public Integer getFreeSpacePerBox(VaultControlRules vaultControlRules) throws ServiceException {
		return vaultsRulesServiceBean.getFreeSpacePerBox(vaultControlRules).intValue();
	}
	
	public Integer getNumberIssuersPerBox(RegisterVaultRulesTO registerVaultRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.getNumberIssuersPerBox(registerVaultRulesTO).intValue();
	}
	
	public void updateExpirationDateOfBox(VaultControlRules vaultControlRules) throws ServiceException {
		vaultsRulesServiceBean.updateExpirationDateOfBox(vaultControlRules);
	}
	
	public Integer verifyCabinetRulesCreated(RegisterVaultRulesTO registerVaultRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.verifyCabinetRulesCreated(registerVaultRulesTO).intValue();
	}
	
	public Integer verifyVaultControlCreated(RegisterVaultRulesTO registerVaultRulesTO) throws ServiceException {
		return vaultsRulesServiceBean.verifyVaultControlCreated(registerVaultRulesTO).intValue();
	}
	
}