package com.pradera.custody.vaultrules.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.custody.vaultrules.facade.VaultsRulesFacade;
import com.pradera.custody.vaultrules.to.RegisterVaultRulesTO;
import com.pradera.custody.vaultrules.to.SearchVaultsRulesTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.VaultControlRules;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.commons.sessionuser.UserInfo;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Date;


@DepositaryWebBean 
@LoggerCreateBean
public class VaultsRulesManageBean extends GenericBaseBean {

	private static final long serialVersionUID = 1L;
	
	private SearchVaultsRulesTO searchVaultsRulesTO;
	
	private RegisterVaultRulesTO modifyVaultRulesTO, createNewCabinetTO;
	
	private List<Integer> lstCabinetsSearch;
	
	private List<Integer> lstLevelsSearch;
	
	private List<Integer> lstBoxSearch;
	
	private List<ParameterTable> lstSecuritiesClass;
		
	private static final String CREATE_CABINET = "createCabinetVaultControl";
	
	private static final String SEARCH_VAULT_RULES = "searchVaultControlRules";
	
	private String title;
	
	private Boolean flagModifyCabinet = false;
	
	private Boolean flagModifyLevel = false;
	
	private Boolean flagModifyBox = false;
	
	private Boolean flagModifyCapacityPerBox = false;
	
	private Boolean flagModifyIssuersPerBox = false;
	
	private Boolean flagModifySecurityClass = false;
	
	private Boolean flagModifyExpirationDate = false;
	
	private List<Integer> yearsList;
			
	@Inject
	private UserInfo userInfo;
	
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	@EJB
	VaultsRulesFacade vaultsRulesFacade;
	
	@PostConstruct
	public void init(){
		createNewCabinetTO = new RegisterVaultRulesTO();
		modifyVaultRulesTO = new RegisterVaultRulesTO();
		searchVaultsRulesTO = new SearchVaultsRulesTO();
		lstCabinetsSearch = new ArrayList<Integer>();
		try {
			//clearSearch();
			fillParametersSearch();
			fillYearsList();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createCabinetDefault() throws ServiceException {
		Integer newCabinetNumber = vaultsRulesFacade.getLastCabinet() + 1;
		//
		createNewCabinetTO.setCabinet(newCabinetNumber);
	}
	
	private void fillParametersSearch() throws ServiceException {
		ParameterTableTO paramTabTO = new ParameterTableTO();
		List<ParameterTable> lstTemp = null;
		List<ParameterTable> instrumentType = null;
		
		lstCabinetsSearch = vaultsRulesFacade.searchCabinets();
		paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		
		lstTemp = lstTemp.stream().filter(param -> param.getLongInteger().intValue() == InstrumentType.FIXED_INCOME.getCode()).collect(Collectors.toList());

		paramTabTO = new ParameterTableTO();
		paramTabTO.setParameterTablePk(InstrumentType.VARIABLE_INCOME.getCode());
		instrumentType = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		
		lstSecuritiesClass = lstTemp;
		lstSecuritiesClass.addAll(instrumentType);
		
		//lstSecuritiesClass = lstTemp.stream().filter(paramTab -> String.valueOf(paramTab.getLongInteger()).equals("1")).collect(Collectors.toList());

	}
	
	private void fillYearsList() {
		yearsList = new ArrayList<>();
		int year = Calendar.getInstance().get(Calendar.YEAR);
		
        for (int i = year; i <= year + 20; i++) {
        	yearsList.add(i);
        }
		
	}
	
	public void clearSearch(){
		JSFUtilities.resetViewRoot();

		searchVaultsRulesTO.setCabinet(null);
		searchVaultsRulesTO.setBox(null);
		searchVaultsRulesTO.setLevel(null);
		searchVaultsRulesTO.setLstVaultsRulesResult(null);
		
		if(modifyVaultRulesTO != null) {
			modifyVaultRulesTO.setCabinet(null);
			modifyVaultRulesTO.setLevel(null);
			modifyVaultRulesTO.setBox(null);
		}
		
	}
	
	@LoggerAuditWeb
	public void searchVaultsRules() {
		try {
			
			List<VaultControlRules> vaultsRulesSearch = vaultsRulesFacade.searchVaultRules(searchVaultsRulesTO);
			
			for(int i = 0; i < vaultsRulesSearch.size(); i++) {
				
				VaultControlRules iterator = vaultsRulesSearch.get(i);
				
				if(iterator.getExpirationDate() != null) {
					Calendar cal = Calendar.getInstance();
					cal.setTime(iterator.getExpirationDate());
					iterator.setExpirationYear(cal.get(Calendar.YEAR));
				}
				
				if(iterator.getIndBoxForIssuers() != null 
						&& iterator.getIndBoxForIssuers() == 1) {
					String issuersDescription = vaultsRulesFacade.getIssuersPerBox(iterator);
					iterator.setIssuersPerBoxDescription(issuersDescription);
				}	
				
				Integer freeSpacePerBox = vaultsRulesFacade.getFreeSpacePerBox(iterator);
				
				if(iterator.getCertificatesPerBox() != null) {
					iterator.setUsedCapacity(iterator.getCertificatesPerBox() - freeSpacePerBox);
				}
				
				if(iterator.getSecuritiesClass() != null) {
					Optional<ParameterTable> paramTab = lstSecuritiesClass.stream()
							.filter(x -> x.getParameterTablePk().equals(iterator.getSecuritiesClass())).findFirst();
					
					if(paramTab.isPresent()) {
						iterator.setSecurityDescription(paramTab.get().getParameterName());
					}
				}
			}
			
			searchVaultsRulesTO.setLstVaultsRulesResult(vaultsRulesSearch);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			excepcion.fire( new ExceptionToCatchEvent(e));
		}
	}
	
	public void loadLevel() {
		
		if(searchVaultsRulesTO.getCabinet() != null) {
			try {
				lstLevelsSearch = vaultsRulesFacade.searchLevels(searchVaultsRulesTO);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		} else {
			searchVaultsRulesTO.setLevel(null);
			searchVaultsRulesTO.setBox(null);
		}

	}
	
	public void loadBox() {
		
		if(searchVaultsRulesTO.getLevel() != null) {
			try {
				lstBoxSearch = vaultsRulesFacade.searchBoxs(searchVaultsRulesTO);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		} else {
			searchVaultsRulesTO.setBox(null);
		}
		
	}

	public String createCabinet() {
		return CREATE_CABINET;
	}
	
	public String back() {
		return SEARCH_VAULT_RULES;
	}
	
	public void beforeOnCreateCabinet() {
		
		if(createNewCabinetTO.getCapacityPerBox() == 0 || createNewCabinetTO.getLevelsPerCabinet() == 0
				|| createNewCabinetTO.getBoxPerLevel() == 0) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Los datos ingresados no pueden ser 0");

			JSFUtilities.showSimpleValidationDialog();

			return;
		}
		
		/*if(createNewCabinetTO.getInstrumentType() == 399 && createNewCabinetTO.getIssuersPerBox() == 0) {
			Object[] bodyData = new Object[0];
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"La cantidad de emisores por caja no puede ser 0");

			JSFUtilities.showSimpleValidationDialog();

			return;
		}*/
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				"¿Esta seguro de crear un nuevo armario en boveda?");
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");

	}
	
	public void createCabinetOnVault() {
		
		try {
			/*
			if(createNewCabinetTO.getInstrumentType() == 399) {
				vaultsRulesFacade.createCabinetForActions(createNewCabinetTO);
			} else {
				vaultsRulesFacade.createCabinet(createNewCabinetTO);
			}
			*/
			
			vaultsRulesFacade.createCabinet(createNewCabinetTO);
			
			Integer rulesCreated = vaultsRulesFacade.verifyCabinetRulesCreated(createNewCabinetTO);
			
			Integer vaultControlRowsCreated = vaultsRulesFacade.verifyVaultControlCreated(createNewCabinetTO);
			
			fillParametersSearch();
			
			if( (createNewCabinetTO.getBoxPerLevel() * createNewCabinetTO.getLevelsPerCabinet() == rulesCreated)
					&& (createNewCabinetTO.getBoxPerLevel() * createNewCabinetTO.getLevelsPerCabinet() 
							* createNewCabinetTO.getCapacityPerBox() ==  vaultControlRowsCreated) )  {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Se creo correctamente un nuevo armario");

				//JSFUtilities.showSimpleValidationDialog();
				
				cleanData();
				
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"No se pudo crear el armario correctamente");

				//JSFUtilities.showSimpleValidationDialog();
				
				cleanData();
				
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			}
			

			
		} catch (ServiceException e) {
			// TODO: handle exception
		}
		
	}
	
	public String configModificationPage(Integer option) throws ServiceException {
		
		Integer freeSpacePerBox;
		List<VaultControlRules> listOfRules = new ArrayList<VaultControlRules>();
		RegisterVaultRulesTO tmp;
		
		switch (option) {
		case 1:
			this.setTitle("Modificar armario");		
			
			tmp = new RegisterVaultRulesTO();
			
			tmp.setCabinet(modifyVaultRulesTO.getCabinet());
			
			listOfRules = vaultsRulesFacade.searchVaultRulesForModify(tmp);
			
			for(int i = 0; i < listOfRules.size(); i++) {
				RegisterVaultRulesTO tmp2 = new RegisterVaultRulesTO();
				
				tmp2.setCabinet(listOfRules.get(i).getCabinet());
				tmp2.setLevel(listOfRules.get(i).getLevel());
				tmp2.setBox(listOfRules.get(i).getBox());
				
				freeSpacePerBox = vaultsRulesFacade.getFreeSpacePerBox(tmp2);
				
				if(freeSpacePerBox != listOfRules.get(i).getCertificatesPerBox()) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"El armario seleccionado no puede ser modificado debido a que existen certificados");

					JSFUtilities.showSimpleValidationDialog();
					
					return null;
				}
			}
			
			flagModifyCabinet = true;
			flagModifyLevel = false;
			flagModifyBox = false;
			break;
		case 2:
			this.setTitle("Modificar nivel");
						
			tmp = new RegisterVaultRulesTO();
			
			tmp.setCabinet(modifyVaultRulesTO.getCabinet());
			tmp.setLevel(modifyVaultRulesTO.getLevel());
			
			listOfRules = vaultsRulesFacade.searchVaultRulesForModify(tmp);
			
			for(int i = 0; i < listOfRules.size(); i++) {
				
				RegisterVaultRulesTO tmp2 = new RegisterVaultRulesTO();
				
				tmp2.setCabinet(listOfRules.get(i).getCabinet());
				tmp2.setLevel(listOfRules.get(i).getLevel());
				tmp2.setBox(listOfRules.get(i).getBox());
				
				freeSpacePerBox = vaultsRulesFacade.getFreeSpacePerBox(tmp2);
				
				if(freeSpacePerBox != listOfRules.get(i).getCertificatesPerBox()) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"El nivel seleccionado no puede ser modificado debido a que existen certificados");

					JSFUtilities.showSimpleValidationDialog();
					
					return null;
				}
				
			}
						
			flagModifyCabinet = false;
			flagModifyLevel = true;
			flagModifyBox = false;
			break;
		case 3:
			this.setTitle("Modificar caja");
			
			tmp = new RegisterVaultRulesTO();
			Boolean rowSelected = false;
			
			if(modifyVaultRulesTO.getCabinet() == null
					|| modifyVaultRulesTO.getLevel() == null
					|| modifyVaultRulesTO.getBox() == null) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Ningun registro se ha seleccionado. Verifique");

				JSFUtilities.showSimpleValidationDialog();
				
				return "";
			}
			
			tmp.setCabinet(modifyVaultRulesTO.getCabinet());
			tmp.setLevel(modifyVaultRulesTO.getLevel());
			tmp.setBox(modifyVaultRulesTO.getBox());
			
			freeSpacePerBox = vaultsRulesFacade.getFreeSpacePerBox(tmp);
			
			if (modifyVaultRulesTO.getIndBoxForIssuers() == null) {
				
				//SI NO TIENE NINGUNA REGLA ASIGNADA SE PUEDEN MODIFICAR TODAS LAS REGLAS
				
				flagModifyCapacityPerBox = true;
				flagModifySecurityClass = true;
				
			} else if (modifyVaultRulesTO.getIndBoxForIssuers() == 0) {
				
				//REGLA -> POR FECHA DE VENCIMIENTO
				
				//SI EXISTEN CERTIFICADOS SOLO SE PUEDE AUMENTAR LA CAPACIDAD
				
				if(freeSpacePerBox.equals(modifyVaultRulesTO.getCapacityPerBox())) {
					flagModifyCapacityPerBox = true;
					flagModifySecurityClass = true;
					flagModifyExpirationDate = true;
				} else {
					flagModifyCapacityPerBox = true;
					flagModifySecurityClass = false;
					flagModifyIssuersPerBox = false;
					flagModifyExpirationDate = false;
				}

				
			} else if (modifyVaultRulesTO.getIndBoxForIssuers() == 1) {
				
				//REGLA -> POR NUMERO DE EMISORES
				
				//SOLO SE PUEDE AUMENTAR EL NUMERO DE EMISORES SI EXISTEN CERTIFICADOS
				
				if(freeSpacePerBox.equals(modifyVaultRulesTO.getCapacityPerBox())) {
					flagModifyCapacityPerBox = true;
					flagModifySecurityClass = true;
					flagModifyIssuersPerBox = true;
				} else {
					flagModifyCapacityPerBox = false;
					flagModifySecurityClass = false;
					flagModifyIssuersPerBox = true;
				}
			}
			
			flagModifyCabinet = false;
			flagModifyLevel = false;
			flagModifyBox = true;
			break;
		}
		
		return "modifyVaultControlRules.xhtml?faces-redirect=true";
		
	}
	
	public String returnToSearch() {
		if(searchVaultsRulesTO.getLstVaultsRulesResult() != null) {
			for(VaultControlRules o : searchVaultsRulesTO.getLstVaultsRulesResult()) {
				o.setIsSelected(false);
			}
		}
		
		cleanData();

		return "searchVaultControlRules.xhtml?faces-redirect=true";
		
	}
	
	public void selectObject(VaultControlRules vaultControlRules) {
		for(VaultControlRules o : searchVaultsRulesTO.getLstVaultsRulesResult()) {
			o.setIsSelected(false);
		}
		
		vaultControlRules.setIsSelected(true);
		
		modifyVaultRulesTO = new RegisterVaultRulesTO();
		
		modifyVaultRulesTO.setCabinet(vaultControlRules.getCabinet());
		modifyVaultRulesTO.setLevel(vaultControlRules.getLevel());
		modifyVaultRulesTO.setBox(vaultControlRules.getBox());
		modifyVaultRulesTO.setCapacityPerBox(vaultControlRules.getCertificatesPerBox());
		modifyVaultRulesTO.setNewCapacityPerBox(vaultControlRules.getCertificatesPerBox());
				
		if(vaultControlRules.getIndBoxForIssuers() != null && vaultControlRules.getIndBoxForIssuers() == 1) {
			modifyVaultRulesTO.setSecurityClass(InstrumentType.VARIABLE_INCOME.getCode());
		} else {
			modifyVaultRulesTO.setSecurityClass(vaultControlRules.getSecuritiesClass());
		}
		
		
		modifyVaultRulesTO.setIndBoxForIssuers(vaultControlRules.getIndBoxForIssuers());
		modifyVaultRulesTO.setIssuersPerBox(vaultControlRules.getIssuersPerBox());
		modifyVaultRulesTO.setNewIssuersPerBox(vaultControlRules.getIssuersPerBox());
		modifyVaultRulesTO.setExpirationYear(vaultControlRules.getExpirationYear());
		
		if(vaultControlRules.getIndBoxForIssuers() != null ) {
			modifyVaultRulesTO.setInstrumentType(vaultControlRules.getIndBoxForIssuers() == 1 ? InstrumentType.VARIABLE_INCOME.getCode() : InstrumentType.FIXED_INCOME.getCode());
		}
	}
	
	public void beforeOnModify() {
		
		if(modifyVaultRulesTO.getNewCapacityPerBox() == 0) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"La capacidad por caja no puede ser 0. Verifique");
			JSFUtilities.showSimpleValidationDialog();			
			
			return;
		}
		
		if(modifyVaultRulesTO.getInstrumentType() != null && 
				modifyVaultRulesTO.getInstrumentType() == 399 && modifyVaultRulesTO.getNewIssuersPerBox() == 0) {
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"La cantidad de emisores no puede ser 0. Verifique");
			JSFUtilities.showSimpleValidationDialog();			
			
			return;	
		}
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				"Esta seguro de registrar las modificaciones de boveda?");
		
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		
	}
	
	public void modifyVaultRules() throws ServiceException {
		
		List<VaultControlRules> listOfRules = new ArrayList<VaultControlRules>();
		
		if(flagModifyLevel) {
			modifyVaultRulesTO.setBox(null);
		} else if(flagModifyCabinet) {
			modifyVaultRulesTO.setBox(null);
			modifyVaultRulesTO.setLevel(null);
		}
		
		listOfRules = vaultsRulesFacade.searchVaultRulesForModify(modifyVaultRulesTO);
		
		
		for(int i = 0; i < listOfRules.size(); i++) {
			listOfRules.get(i).setCertificatesPerBox(modifyVaultRulesTO.getNewCapacityPerBox());
			listOfRules.get(i).setIssuersPerBox(modifyVaultRulesTO.getNewIssuersPerBox());
			listOfRules.get(i).setSecuritiesClass(modifyVaultRulesTO.getSecurityClass());
			
			if(modifyVaultRulesTO.getExpirationYear() != null) {
				Calendar calendar = Calendar.getInstance();
		        calendar.set(Calendar.YEAR, modifyVaultRulesTO.getExpirationYear());
		        Date date = calendar.getTime();
				listOfRules.get(i).setExpirationDate(date);
			} else {
				listOfRules.get(i).setExpirationDate(null);
			}
			
			try {
			
				if(modifyVaultRulesTO.getInstrumentType() == null) {
					vaultsRulesFacade.modifyBoxVaultControl(listOfRules.get(i));
				} else if(modifyVaultRulesTO.getInstrumentType() == 124) {
					vaultsRulesFacade.modifyBoxVaultControForExpirationDate(listOfRules.get(i));
					
					if(flagModifyExpirationDate) {
						vaultsRulesFacade.updateExpirationDateOfBox(listOfRules.get(i));
					}
					
				} else if(modifyVaultRulesTO.getInstrumentType() == 399) {
					vaultsRulesFacade.modifyBoxVaultControlForIssuers(listOfRules.get(i));
				} 
			} catch (Exception e) {
				// TODO: handle exception
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Ocurrio un error al realizar la modificacion.");
				JSFUtilities.showSimpleValidationDialog();	
				return;
			}
		}
		
		cleanData();
		
		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
				"Se modifico correctamente la caja seleccionada");
		
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
		
	}
	
	public void changeSecurityType() throws ServiceException {
		
		modifyVaultRulesTO.setExpirationYear(null);
		
		if(modifyVaultRulesTO.getSecurityClass() == null) {
			modifyVaultRulesTO.setInstrumentType(null);
			return;
		}

		Optional<ParameterTable> parameterTable = lstSecuritiesClass.stream().filter(x -> x.getParameterTablePk().
				equals(modifyVaultRulesTO.getSecurityClass())).findFirst();
		
		if(parameterTable.isPresent()) {
			modifyVaultRulesTO.setInstrumentType(parameterTable.get().getLongInteger().intValue());
		}
		
		if(modifyVaultRulesTO.getInstrumentType() == 399) {
			flagModifyIssuersPerBox = true;
		} else {
			flagModifyExpirationDate = true;
		}
		
	}
	
	public void verifyCapacityPerBox() throws ServiceException {
		
		Integer freeSpacePerBox = vaultsRulesFacade.getFreeSpacePerBox(modifyVaultRulesTO);
		
		if(modifyVaultRulesTO.getNewCapacityPerBox() == null) {
			return;
		}

		if(!freeSpacePerBox.equals(modifyVaultRulesTO.getCapacityPerBox())) {
			
			if(modifyVaultRulesTO.getNewCapacityPerBox() < modifyVaultRulesTO.getCapacityPerBox()) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Solo se permite aumentar la capacidad debido a que en la caja seleccionada ya existen certificados");
				JSFUtilities.showSimpleValidationDialog();
				
				modifyVaultRulesTO.setNewCapacityPerBox(modifyVaultRulesTO.getCapacityPerBox());
			}
		}
		
	}
	
	public void verifyIssuersPerBox() throws ServiceException {
		
		//Integer issuersPerBox = vaultsRulesFacade.getNumberIssuersPerBox(modifyVaultRulesTO);
		Integer freeSpacePerBox = vaultsRulesFacade.getFreeSpacePerBox(modifyVaultRulesTO);
		
		if(modifyVaultRulesTO.getNewIssuersPerBox() == null) {
			return;
		}

		if(!freeSpacePerBox.equals(modifyVaultRulesTO.getCapacityPerBox())) {
			
			if(modifyVaultRulesTO.getNewIssuersPerBox() < modifyVaultRulesTO.getIssuersPerBox()) {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Solo se permite aumentar la cantidad de emisores debido a que en la caja seleccionada ya existen certificados");
				JSFUtilities.showSimpleValidationDialog();
				
				modifyVaultRulesTO.setNewIssuersPerBox(modifyVaultRulesTO.getIssuersPerBox());
			}
			
		}
		
	}
	
	private void cleanData() {
		searchVaultsRulesTO.setLstVaultsRulesResult(null);
		searchVaultsRulesTO.setCabinet(null);
		searchVaultsRulesTO.setLevel(null);
		searchVaultsRulesTO.setBox(null);
		createNewCabinetTO = new RegisterVaultRulesTO();
		if(modifyVaultRulesTO != null) {
			modifyVaultRulesTO.setCabinet(null);
			modifyVaultRulesTO.setLevel(null);
			modifyVaultRulesTO.setBox(null);
		}
	}
	
	public SearchVaultsRulesTO getSearchVaultsRulesTO() {
		return searchVaultsRulesTO;
	}

	public void setSearchVaultsRulesTO(SearchVaultsRulesTO searchVaultsRulesTO) {
		this.searchVaultsRulesTO = searchVaultsRulesTO;
	}

	public List<Integer> getLstCabinetsSearch() {
		return lstCabinetsSearch;
	}

	public void setLstCabinetsSearch(List<Integer> lstCabinetsSearch) {
		this.lstCabinetsSearch = lstCabinetsSearch;
	}

	public List<Integer> getLstLevelsSearch() {
		return lstLevelsSearch;
	}

	public void setLstLevelsSearch(List<Integer> lstLevelsSearch) {
		this.lstLevelsSearch = lstLevelsSearch;
	}

	public List<Integer> getLstBoxSearch() {
		return lstBoxSearch;
	}

	public void setLstBoxSearch(List<Integer> lstBoxSearch) {
		this.lstBoxSearch = lstBoxSearch;
	}

	public RegisterVaultRulesTO getCreateNewCabinetTO() {
		return createNewCabinetTO;
	}

	public void setCreateNewCabinetTO(RegisterVaultRulesTO createNewCabinetTO) {
		this.createNewCabinetTO = createNewCabinetTO;
	}

	public RegisterVaultRulesTO getModifyVaultRulesTO() {
		return modifyVaultRulesTO;
	}

	public void setModifyVaultRulesTO(RegisterVaultRulesTO modifyVaultRulesTO) {
		this.modifyVaultRulesTO = modifyVaultRulesTO;
	}

	public List<ParameterTable> getLstSecuritiesClass() {
		return lstSecuritiesClass;
	}

	public void setLstSecuritiesClass(List<ParameterTable> lstSecuritiesClass) {
		this.lstSecuritiesClass = lstSecuritiesClass;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getFlagModifyCabinet() {
		return flagModifyCabinet;
	}

	public void setFlagModifyCabinet(Boolean flagModifyCabinet) {
		this.flagModifyCabinet = flagModifyCabinet;
	}

	public Boolean getFlagModifyLevel() {
		return flagModifyLevel;
	}

	public void setFlagModifyLevel(Boolean flagModifyLevel) {
		this.flagModifyLevel = flagModifyLevel;
	}

	public Boolean getFlagModifyBox() {
		return flagModifyBox;
	}

	public void setFlagModifyBox(Boolean flagModifyBox) {
		this.flagModifyBox = flagModifyBox;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Boolean getFlagModifyCapacityPerBox() {
		return flagModifyCapacityPerBox;
	}

	public void setFlagModifyCapacityPerBox(Boolean flagModifyCapacityPerBox) {
		this.flagModifyCapacityPerBox = flagModifyCapacityPerBox;
	}

	public Boolean getFlagModifyIssuersPerBox() {
		return flagModifyIssuersPerBox;
	}

	public void setFlagModifyIssuersPerBox(Boolean flagModifyIssuersPerBox) {
		this.flagModifyIssuersPerBox = flagModifyIssuersPerBox;
	}

	public Boolean getFlagModifySecurityClass() {
		return flagModifySecurityClass;
	}

	public void setFlagModifySecurityClass(Boolean flagModifySecurityClass) {
		this.flagModifySecurityClass = flagModifySecurityClass;
	}

	public List<Integer> getYearsList() {
		return yearsList;
	}

	public void setYearsList(List<Integer> yearsList) {
		this.yearsList = yearsList;
	}

	public Boolean getFlagModifyExpirationDate() {
		return flagModifyExpirationDate;
	}

	public void setFlagModifyExpirationDate(Boolean flagModifyExpirationDate) {
		this.flagModifyExpirationDate = flagModifyExpirationDate;
	}
	
}