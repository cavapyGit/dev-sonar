package com.pradera.custody.ownershipexchange.facade;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.core.framework.batchprocess.service.BatchServiceBean;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.ownershipexchange.service.ChangeOwnershipServiceBean;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipDetailTO;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipOperationTO;
import com.pradera.custody.tranfersecurities.accounts.service.HolderAccountServiceBean;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.changeownership.ChangeOwnershipFile;
import com.pradera.model.custody.changeownership.ChangeOwnershipMarketFact;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.custody.type.ChangeOwnershipRejectMotiveType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ChangeOwnershipServiceFacade {
	
	/** The change ownership service bean. */
	@EJB
	ChangeOwnershipServiceBean changeOwnershipServiceBean;
	
	/** The holder account service bean. */
	@EJB
	HolderAccountServiceBean holderAccountServiceBean;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The holder account component service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/** The affectations service bean. */
	@EJB
	AffectationsServiceBean affectationsServiceBean;

	/** The batch process service bean. */
	@EJB
	BatchServiceBean batchProcessServiceBean;
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/**  The calculation Component Billing*. */
	@Inject 
	Instance<BillingConexionService> billingConexion;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;

	/**
	 * List holder account balances service facade.
	 *
	 * @param filters the filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> listHolderAccountBalancesServiceFacade(Map<String,Object> filters) throws ServiceException{
		
		List<Object[]> result = holderAccountServiceBean.listHolderAccountBalancesServiceBean(filters);
		
		return result; 
	}

	/**
	 * Save avail change owner ship service facade.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @return the change ownership operation
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 1440)
	@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_REGISTER)
	public ChangeOwnershipOperation saveAvailChangeOwnerShipServiceFacade(
			ChangeOwnershipOperation changeOwnershipOperation, Integer indMarketFact) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        return changeOwnershipServiceBean.registerChangeOwnershipOperation(changeOwnershipOperation, indMarketFact);
	}
	
	/**
	 * Save block change owner ship service facade.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @return the change ownership operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_REGISTER)
	public ChangeOwnershipOperation saveBlockChangeOwnerShipServiceFacade(
			ChangeOwnershipOperation changeOwnershipOperation) throws ServiceException {
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        return changeOwnershipServiceBean.registerChangeOwnershipOperation(changeOwnershipOperation, 0);
	}
	
	/**
	 * Confirm available ownership change.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @throws Exception the exception
	 */
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 1440)
	@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM)
	public void confirmAvailableOwnershipChange(ChangeOwnershipOperation changeOwnershipOperation, Integer indMarketFact ) throws Exception{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		changeOwnershipOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		changeOwnershipServiceBean.confirmChangeOwnershipOperation(changeOwnershipOperation,indMarketFact, loggerUser);
	}
	
	/**
	 * Reject available ownership change.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_REJECT)
	public void rejectAvailableOwnershipChange(ChangeOwnershipOperation changeOwnershipOperation, Integer indMarketFact) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		changeOwnershipOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		changeOwnershipServiceBean.rejectChangeOwnershipOperation(changeOwnershipOperation,indMarketFact);
	}
	
	/**
	 * Confirm blocked ownership change.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @throws Exception the exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM)
	public void confirmBlockedOwnershipChange(ChangeOwnershipOperation changeOwnershipOperation) throws Exception{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		changeOwnershipOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		changeOwnershipServiceBean.confirmChangeOwnershipOperation(changeOwnershipOperation,ComponentConstant.ONE, loggerUser);
	}
	
	/**
	 * Reject blocked ownership change.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_REJECT)
	public void rejectBlockedOwnershipChange(ChangeOwnershipOperation changeOwnershipOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		changeOwnershipOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		changeOwnershipServiceBean.rejectChangeOwnershipOperation(changeOwnershipOperation,1);
	}

	/**
	 * Search avail change ownership by filters.
	 *
	 * @param changeOwnershipOperationTO the change ownership operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_QUERY)
	public List<ChangeOwnershipDetailTO> searchAvailChangeOwnershipByFilters(
			ChangeOwnershipOperationTO changeOwnershipOperationTO) throws ServiceException {

		return changeOwnershipServiceBean.searchChangeOwnershipOperationsByFilters(changeOwnershipOperationTO);

	}
	
	/**
	 * Search blocked change ownership by filters.
	 *
	 * @param changeOwnershipOperationTO the change ownership operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_QUERY)
	public List<ChangeOwnershipDetailTO> searchBlockedChangeOwnershipByFilters(
			ChangeOwnershipOperationTO changeOwnershipOperationTO) throws ServiceException {
		return changeOwnershipServiceBean.searchBlockChangeOwnershipOperationsByFilters(changeOwnershipOperationTO);
	}
	

	/**
	 * Gets the change ownership operation by filters.
	 *
	 * @param changeOwnershipOperationTO the change ownership operation to
	 * @return the change ownership operation by filters
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 1440)
	public ChangeOwnershipOperation getChangeOwnershipOperationByFilters(
			ChangeOwnershipDetailTO changeOwnershipOperationTO)  throws ServiceException{
		List<ChangeOwnershipOperation> ops =  changeOwnershipServiceBean.getChangeOwnershipOperationByFilters(changeOwnershipOperationTO);
		ChangeOwnershipOperation chop = null;
		if(ops.size()>0){
			chop = ops.get(0);
			for(ChangeOwnershipDetail det : chop.getChangeOwnershipDetails()){
				
				HolderAccountTO holderAccountTO = new HolderAccountTO();
				holderAccountTO.setNeedHolder(true);
				holderAccountTO.setNeedParticipant(false);
				holderAccountTO.setNeedBanks(false);
				holderAccountTO.setIdHolderAccountPk(det.getHolderAccount().getIdHolderAccountPk());
				holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(holderAccountTO);
				if(chop.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
					det.setBlockChangeOwnerships(null);
					det.getTargetChangeOwnershipDetails().size();
					det.getSecurity().getIdSecurityCodePk();
					for(ChangeOwnershipDetail det2 : det.getTargetChangeOwnershipDetails()){
						det2.getChangeOwnershipMarketFacts().size();
						HolderAccountTO holderAccountTO2 = new HolderAccountTO();
						holderAccountTO2.setIdHolderAccountPk(det2.getHolderAccount().getIdHolderAccountPk());
						holderAccountTO2.setNeedHolder(true);
						holderAccountTO2.setNeedParticipant(true);
						holderAccountTO2.setNeedBanks(false);
						holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(holderAccountTO2);
					}
				}else{
					det.setTargetChangeOwnershipDetails(null);
					det.getBlockChangeOwnerships().size();
					det.getSecurity().getIdSecurityCodePk();
					for(BlockChangeOwnership blockdet : det.getBlockChangeOwnerships()){
						blockdet.getChangeOwnershipMarketFacts().size();
						if(blockdet.getRefBlockOperation()!=null){
							blockdet.getRefBlockOperation().getIdBlockOperationPk();
							blockdet.getRefBlockOperation().getBlockRequest().getIdBlockRequestPk();
						}
						blockdet.getBlockOperation().getIdBlockOperationPk();
						blockdet.getBlockOperation().getBlockRequest().getIdBlockRequestPk();
						HolderAccountTO holderAccountTO2 = new HolderAccountTO();
						holderAccountTO2.setIdHolderAccountPk(blockdet.getHolderAccount().getIdHolderAccountPk());
						holderAccountTO2.setNeedHolder(true);
						holderAccountTO2.setNeedParticipant(false);
						holderAccountTO2.setNeedBanks(false);
						holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(holderAccountTO2);
					}
				}
			}
		}
		
		return chop;
	}
	
	/**
	 * Gets the change ownership file content.
	 *
	 * @param filter the filter
	 * @return the change ownership file content
	 * @throws ServiceException the service exception
	 */
	public byte[] getChangeOwnershipFileContent(Map<String,Object> filter) 
			throws ServiceException {
		return changeOwnershipServiceBean.getChangeOwnershipFileContent(filter);
	}

	/**
	 * Update holder account.
	 *
	 * @param ha the ha
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount updateHolderAccount(HolderAccount ha) throws ServiceException {
		return crudDaoServiceBean.update(ha);		
	}

//	public List<HolderAccountTO> countAllAccounts(Map<String, Object> filters) {
//		return changeOwnershipServiceBean.countAllAccounts(filters);
//	}

	/**
 * Update holder.
 *
 * @param holder the holder
 * @return the holder
 * @throws ServiceException the service exception
 */
public Holder updateHolder(Holder holder) throws ServiceException {
		return crudDaoServiceBean.update(holder);	
	}

//	public String checkTransitBalances(Map<String,Object> filters) throws ServiceException {
//		HolderAccountBalance hab = holderAccountServiceBean.listHolderAccountBalancesServiceBean(filters).get(0);
//		
//		if( hab.getPawnBalance().longValue() > 0 )
//			return ParameterBalanceType.AVAILABLE_BALANCE.getDescripcion();
//		if( hab.getPawnBalance().longValue() > 0 )
//			return ParameterBalanceType.PAWN_BALANCE.getDescripcion();
//		if( hab.getBanBalance().longValue() > 0 )
//			return ParameterBalanceType.BAN_BALANCE.getDescripcion();
//		if( hab.getOtherBlockBalance().longValue() > 0 )
//			return ParameterBalanceType.OTHERS_BALANCE.getDescripcion();
//		if( hab.getTransitBalance().longValue() > 0 )
//			return ParameterBalanceType.TRANSIT_BALANCE.getDescripcion();
//		if( hab.getAccreditationBalance().longValue() > 0 )
//			return ParameterBalanceType.ACCREDITATION_BALANCE.getDescripcion();
//		
//		return null;
//	}
	
	/**
 * Find block operations by filters.
 *
 * @param blockOperationTO the block operation to
 * @return the list
 * @throws ServiceException the service exception
 */
public List<BlockOperation> findBlockOperationsByFilters(BlockOperationTO blockOperationTO) throws ServiceException {
		List<BlockOperation> list = affectationsServiceBean.findBlockOperationsByFilters(blockOperationTO);
		for(BlockOperation blockOperation : list){
			blockOperation.setCopiedActualBlockBalance(blockOperation.getActualBlockBalance());
		}
		return list;
	}

	/**
	 * Gets the change ownership file info.
	 *
	 * @param filter the filter
	 * @return the change ownership file info
	 * @throws ServiceException the service exception
	 */
	public List<ChangeOwnershipFile> getChangeOwnershipFileInfo(
			Map<String, Object> filter) throws ServiceException {
		return changeOwnershipServiceBean.getChangeOwnershipFileInfo(filter);
	}

	/**
	 * Count available balances by filters.
	 *
	 * @param filters the filters
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int countAvailableBalancesByFilters(Map<String, Object> filters) throws ServiceException {
		return holderAccountServiceBean.countAvailableBalancesServiceBean(filters);		
	}

	/**
	 * Count block operations by filters.
	 *
	 * @param filters the filters
	 * @return the int
	 * @throws ServiceException the service exception
	 */
	public int countBlockOperationsByFilters(Map<String, Object> filters) throws ServiceException {
		return changeOwnershipServiceBean.countBlockOperationsByFilters(filters);
	}

	/**
	 * Change ownership operations exists.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @return true, if successful
	 */
	public boolean changeOwnershipOperationsExists(
			ChangeOwnershipOperation changeOwnershipOperation) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Gets the holder accounts.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder accounts
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccounts (HolderAccountTO holderAccountTO) throws ServiceException {
		return holderAccountComponentServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO );
	}

	/**
	 * Automatic change ownership availables cancelations.
	 *
	 * @param finalDate the final date
	 * @param idepositarySetup the idepositary setup
	 * @throws ServiceException the service exception
	 */
	public void automaticChangeOwnershipAvailablesCancelations(Date finalDate, Integer idepositarySetup) throws ServiceException{						
		ChangeOwnershipOperationTO filter = new ChangeOwnershipOperationTO();
		filter.setIdBalanceType(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode());
		filter.setIdStatus(ChangeOwnershipStatusType.REGISTRADO.getCode());
		filter.setFinalDate(finalDate);
		List<ChangeOwnershipDetailTO> list = changeOwnershipServiceBean.searchChangeOwnershipOperationsByFilters(filter);
		ChangeOwnershipOperation choo = null;
		for (ChangeOwnershipDetailTO changeOwnershipOperationTO : list) {
			choo = getChangeOwnershipOperationByFilters(changeOwnershipOperationTO);			
			choo.getCustodyOperation().setRejectMotive(ChangeOwnershipRejectMotiveType.OTHER_MOTIVE_REJECT.getCode());
			choo.getCustodyOperation().setRejectMotiveOther("REVERSION NOCTURNA");
			rejectAvailableOwnershipChange(choo, idepositarySetup);
		}
	}
	
	/**
	 * Automatic change ownership blocked cancelations.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticChangeOwnershipBlockedCancelations(Date finalDate) throws ServiceException{						
		ChangeOwnershipOperationTO filter = new ChangeOwnershipOperationTO();
		filter.setIdBalanceType(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode());
		filter.setIdStatus(ChangeOwnershipStatusType.REGISTRADO.getCode());
		filter.setFinalDate(finalDate);
		List<ChangeOwnershipDetailTO> list = changeOwnershipServiceBean.searchBlockChangeOwnershipOperationsByFilters(filter);
		ChangeOwnershipOperation choo = null;
		for (ChangeOwnershipDetailTO changeOwnershipOperationTO : list) {	
			choo = getChangeOwnershipOperationByFilters(changeOwnershipOperationTO);
			choo.getCustodyOperation().setRejectMotive(ChangeOwnershipRejectMotiveType.OTHER_MOTIVE_REJECT.getCode());
			choo.getCustodyOperation().setRejectMotiveOther("REVERSION NOCTURNA");
			rejectBlockedOwnershipChange(choo);
		}
	}

	/**
	 * Gets the market fact change ownership.
	 *
	 * @param idChangeOwnershipDet the id change ownership det
	 * @param idBlockChangeOwnershipDet the id block change ownership det
	 * @return the market fact change ownership
	 */
	public List<ChangeOwnershipMarketFact> getMarketFactChangeOwnership(Long idChangeOwnershipDet, Long idBlockChangeOwnershipDet) {
		return changeOwnershipServiceBean.getMarketFactChangeOwnership(idChangeOwnershipDet, idBlockChangeOwnershipDet);
	}
	
	/**
	 * Gets the market fact balances.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param idHolderAccountPk the id holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @return the market fact balances
	 */
	public List<HolderMarketFactBalance> getMarketFactBalances(
			Long idParticipantPk, Long idHolderAccountPk,
			String idSecurityCodePk) {
		return changeOwnershipServiceBean.getMarketFactBalances(idParticipantPk,idHolderAccountPk,idSecurityCodePk);
	}
	
	/**
	 * Gets the market fact balances blocked.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param idHolderAccountPk the id holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @param idBlockOperationPk the id block operation pk
	 * @return the market fact balances blocked
	 */
	public List<BlockMarketFactOperation> getMarketFactBalancesBlocked(
			Long idParticipantPk, Long idHolderAccountPk, String idSecurityCodePk, Long idBlockOperationPk) {
		return changeOwnershipServiceBean.getMarketFactBalancesBlocked(idParticipantPk,idHolderAccountPk,idSecurityCodePk, idBlockOperationPk);
	}	
	
	/**
	 * Check exists other accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param accountType the account type
	 * @return true, if successful
	 */
	public boolean checkExistsOtherAccounts(Long idHolderPk, Integer accountType) {
		return changeOwnershipServiceBean.checkExistsOtherAccounts(idHolderPk, accountType);
	}
	
}
