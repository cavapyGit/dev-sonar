package com.pradera.custody.ownershipexchange.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class OfacMassiveRegisterProcess.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Mar 21, 2013
 */
@BatchProcess(name="ChangeOwnershipRegularizationBatch")
@RequestScoped
public class ChangeOwnershipRegularizationBatch implements JobExecution,Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The change ownership service bean. */
	@EJB
	ChangeOwnershipServiceBean changeOwnershipServiceBean;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		
		log.info(getClass().getName() + " being executed.");
		
		try {
		
//			Integer days = 3;
//			ChangeOwnershipDetailTO filter = new ChangeOwnershipDetailTO();
//			
//			List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();	
//			filter.setIdBalanceType(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode());
//			
//			for (ProcessLoggerDetail processLoggerDetail : processLoggerDetails) {
//				if(ComponentConstant.FINAL_DATE.equals(processLoggerDetail.getParameterName())){
//					filter.setInitialDate(CommonsUtilities.currentDateTime());
//				}else if(ComponentConstant.INITIAL_DATE.equals(processLoggerDetail.getParameterName())){
//					filter.setFinalDate(CommonsUtilities.addDaysToDate(filter.getInitialDate(), -days));
//				}
//			}
//			
//			List<ChangeOwnershipOperation> list = changeOwnershipServiceBean.getChangeOwnershipOperationByFilters(filter);
//			for (ChangeOwnershipOperation changeOwnershipOperation : list) {
//				changeOwnershipServiceBean.rejectChangeOwnershipOperation(changeOwnershipOperation, null);
//			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}