	package com.pradera.custody.ownershipexchange.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipDetailTO;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipOperationTO;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.component.settlements.to.SecuritiesTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.HolderHistoryState;
import com.pradera.model.accounts.HolderRequest;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestBlockMotiveType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequesterType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderBlockMotiveType;
import com.pradera.model.accounts.type.HolderRequestStateType;
import com.pradera.model.accounts.type.HolderRequestType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.accounts.type.StateHistoryStateHolderType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.changeownership.ChangeOwnershipFile;
import com.pradera.model.custody.changeownership.ChangeOwnershipMarketFact;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.custody.type.ChangeOwnershipMotiveType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
public class ChangeOwnershipServiceBean extends CrudDaoServiceBean {
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;

	
	/** The request reversals service bean. */
	@EJB
	RequestReversalsServiceBean requestReversalsServiceBean;
	
	/** The request affectation service bean. */
	@EJB
	RequestAffectationServiceBean requestAffectationServiceBean;
	
	/** The affectations service bean. */
	@EJB
	AffectationsServiceBean affectationsServiceBean;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The holder account component service bean. */
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The custody service consumer. */
	@EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;	
	
	/**
	 * Search change ownership operations by filters.
	 *
	 * @param changeOwnershipOperationTO the change ownership operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ChangeOwnershipDetailTO> searchChangeOwnershipOperationsByFilters(
			ChangeOwnershipOperationTO changeOwnershipOperationTO) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct 	");
		sbQuery.append("	co.operationNumber,	");  //0
		sbQuery.append("	co.registryDate,	");			 //1
		sbQuery.append("	(select parameterName from ParameterTable where parameterTablePk = chop.motive) as motive ,	");					 //2
		sbQuery.append("	(select parameterName from ParameterTable where parameterTablePk = co.state) as state ,	");						 //3
		sbQuery.append("	chop.motive, 	");  			 //4
		sbQuery.append("	co.state, 	");  				 //5
		
		//sbQuery.append("	sha,	");			 		     //6
		sbQuery.append("	spa.idParticipantPk,	");      //7
		sbQuery.append("	spa.mnemonic,	");      		 //8
		sbQuery.append("	spa.description,	");      	 //9
			 			 
		sbQuery.append("	tpa.idParticipantPk,	");      //10
		sbQuery.append("	tpa.mnemonic,	");      		 //11
		sbQuery.append("	tpa.description,	");          //12

		sbQuery.append("	chop.balanceType,	");		     //13
		sbQuery.append("	chop.changeOwnershipOperationRef.idChangeOwnershipPk, ");  //14
		sbQuery.append("	chop.idChangeOwnershipPk	");  //15

		//sbQuery.append(" from ChangeOwnershipDetail chod ");
		sbQuery.append(" from ChangeOwnershipOperation chop 	");
		//sbQuery.append(" inner join chod.holderAccount tha ");
		sbQuery.append(" inner join chop.custodyOperation co ");
		sbQuery.append(" inner join chop.sourceParticipant spa ");
		sbQuery.append(" inner join chop.targetParticipant tpa ");
		sbQuery.append(" inner join chop.sourceHolderAccount sha ");
		sbQuery.append(" inner join chop.changeOwnershipDetails valchod  ");
		sbQuery.append(" inner join valchod.holderAccount valha  ");
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk()) ||
				Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdHolderStatus()) || 
				Validations.validateListIsNotNullAndNotEmpty(changeOwnershipOperationTO.getLstStatus())){
			sbQuery.append(" inner join valha.holderAccountDetails valhad ");
			sbQuery.append(" inner join valhad.holder valho ");
		}
		
		sbQuery.append(" where 1 = 1	");
		
		//issue 1379: indicador para diferencia las solicitudes de cambio de titularidad de las solicitudesde Trasnferencia por Intercambio de Valores
		if(changeOwnershipOperationTO.isSearchByExchangeSecurities()){
			sbQuery.append(" and chop.indExchangeSecurities = 1 ");	
		}else{
			sbQuery.append(" and (chop.indExchangeSecurities is null or chop.indExchangeSecurities = 0)  ");
		}
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdOperationNumber())){
			sbQuery.append(" 	and co.operationNumber = :idOperationNumber ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdBalanceType())){
			sbQuery.append(" 	and chop.balanceType = :idBalanceType ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdReasonType())){
			sbQuery.append(" 	and chop.motive = :idMotiveType ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdStatus())){
			sbQuery.append(" 	and chop.state = :idState ");
		}			
		
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getInitialDate())){
			sbQuery.append(" 	and trunc(co.registryDate) >= :initialDate ");
		}
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getFinalDate())){
			sbQuery.append(" 	and trunc(co.registryDate) <= :finalDate ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolderAccount().getAccountNumber())){
			sbQuery.append(" 	and ( valha.accountNumber = :accountNumber ) ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdParticipantPk())){
			sbQuery.append(" 	and ( valchod.participant.idParticipantPk = :idParticipantPk ) ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk())){
			sbQuery.append(" 	and valho.idHolderPk = :idHolderPk ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdHolderStatus())){
			sbQuery.append(" 	and valho.stateHolder = :stateHolder  ");
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(changeOwnershipOperationTO.getLstStatus())){
			sbQuery.append(" 	and valho.stateHolder in (:lstState) ");
		}
		
		if(changeOwnershipOperationTO.isRegularizationCheck()){
			sbQuery.append("    and valchod.indOriginTarget = 1   ");
			sbQuery.append(" 	and (select count(*) from HolderAccountBalance hab where ");
			sbQuery.append(" 	   valchod.participant.idParticipantPk = hab.participant.idParticipantPk ");
			sbQuery.append(" 	   and valchod.holderAccount.idHolderAccountPk = hab.holderAccount.idHolderAccountPk ");
			sbQuery.append(" 	   and valchod.security.idSecurityCodePk = hab.security.idSecurityCodePk ");
			sbQuery.append(" 	   and hab.availableBalance > 0 ");
			sbQuery.append(" 	   and hab.totalBalance > 0 )  > 0 ");
			sbQuery.append("    and chop.changeOwnershipOperationRef is null ");
			sbQuery.append("    and chop.indLastRequest = 0 ");
		}
		
		//sbQuery.append(" and chod.indOriginTarget = 2 ");
		sbQuery.append(" order by co.operationNumber desc ");

		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdOperationNumber())){
			query.setParameter("idOperationNumber", changeOwnershipOperationTO.getIdOperationNumber());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdBalanceType())){
			query.setParameter("idBalanceType", changeOwnershipOperationTO.getIdBalanceType());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdReasonType())){
			query.setParameter("idMotiveType", changeOwnershipOperationTO.getIdReasonType());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdStatus())){
			query.setParameter("idState", changeOwnershipOperationTO.getIdStatus());
		}
		
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getInitialDate())){
			query.setParameter("initialDate", changeOwnershipOperationTO.getInitialDate());
		}
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getFinalDate())){
			query.setParameter("finalDate", changeOwnershipOperationTO.getFinalDate());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolderAccount().getAccountNumber())){
			query.setParameter("accountNumber", changeOwnershipOperationTO.getSourceTargetHolderAccount().getAccountNumber());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdParticipantPk())){
			query.setParameter("idParticipantPk", changeOwnershipOperationTO.getIdParticipantPk());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk())){
			query.setParameter("idHolderPk", changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdHolderStatus())){
			query.setParameter("stateHolder", changeOwnershipOperationTO.getIdHolderStatus());
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(changeOwnershipOperationTO.getLstStatus())){
			query.setParameter("lstState", changeOwnershipOperationTO.getLstStatus());
		}
		

		List<Object[]> resultList = query.getResultList();

		
		List<ChangeOwnershipDetailTO> chodResultList = new ArrayList<ChangeOwnershipDetailTO>();

		ChangeOwnershipDetailTO changeOwnershipDetailTO = new ChangeOwnershipDetailTO();
		
		int i = 0;
		while (i < resultList.size()) {
			Object[] object = resultList.get(i);
				
			changeOwnershipDetailTO = new ChangeOwnershipDetailTO();
			changeOwnershipDetailTO.setTargetHolderAccounts(new ArrayList<HolderAccount>());
			changeOwnershipDetailTO.setOperationNumber(Long.valueOf(object[0].toString()));
			changeOwnershipDetailTO.setIdChangeOwnershipPk((Long)object[14]);
			changeOwnershipDetailTO.setRegisterDate((Date)object[1]);
			changeOwnershipDetailTO.setMotiveDescription(object[2].toString());
			changeOwnershipDetailTO.setStatusDescription(object[3].toString());
			changeOwnershipDetailTO.setIdMotive((Integer)object[4]);
			changeOwnershipDetailTO.setIdStatus((Integer)object[5]);
			
			//HolderAccount ha = (HolderAccount)object[6];
			//for(HolderAccountDetail detail : ha.getHolderAccountDetails()){
			//	detail.getHolder().getIdHolderPk();
			//}
			//.setSourceHolderAccount(ha);
			
			changeOwnershipDetailTO.setIdSourceParticipantPk(Long.valueOf(object[6].toString()));
			changeOwnershipDetailTO.setSourceParticipantMnemonic(object[7].toString());
			changeOwnershipDetailTO.setSourceParticipantDescription(object[8].toString());
			
			changeOwnershipDetailTO.setIdTargetParticipantPk(Long.valueOf(object[9].toString()));
			changeOwnershipDetailTO.setTargetParticipantMnemonic(object[10].toString());
			changeOwnershipDetailTO.setTargetParticipantDescription(object[11].toString());
			
			changeOwnershipDetailTO.setIdBalanceType(Integer.valueOf(object[12].toString()));
			
			if(Validations.validateIsNotNull(object[14]))
				changeOwnershipDetailTO.setIdChangeOwnershipRefPk((Long)object[13]);
			
			chodResultList.add(changeOwnershipDetailTO);

			i++;
		}
		
		for(int j=0; j<chodResultList.size(); j++) {
			ChangeOwnershipDetailTO iterator = chodResultList.get(j);
			StringBuilder sbQuery2 = new StringBuilder();
			sbQuery2.append("select sha");
			sbQuery2.append(" from ChangeOwnershipOperation chop 	");
			//sbQuery.append(" inner join chod.holderAccount tha ");
			sbQuery2.append(" inner join chop.custodyOperation co ");
			sbQuery2.append(" inner join chop.sourceParticipant spa ");
			sbQuery2.append(" inner join chop.targetParticipant tpa ");
			sbQuery2.append(" inner join chop.sourceHolderAccount sha ");
			sbQuery2.append(" inner join chop.changeOwnershipDetails valchod  ");
			sbQuery2.append(" inner join valchod.holderAccount valha  ");
			if(iterator.getOperationNumber() != null){
				sbQuery2.append(" 	where co.operationNumber = :idOperationNumber ");
			}
			Query query2 = em.createQuery(sbQuery2.toString());
			
			if(iterator.getOperationNumber()!= null){
				query2.setParameter("idOperationNumber", iterator.getOperationNumber());
			}
			
			HolderAccount resultHolderAccount = (HolderAccount)query2.getSingleResult();
			
			for(HolderAccountDetail detail : resultHolderAccount.getHolderAccountDetails()){
				detail.getHolder().getIdHolderPk();
			}
			
			chodResultList.get(j).setSourceHolderAccount(resultHolderAccount);
			
		}
			
		return chodResultList;
		
	}
	
	/**
	 * Search block change ownership operations by filters.
	 *
	 * @param changeOwnershipOperationTO the change ownership operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ChangeOwnershipDetailTO> searchBlockChangeOwnershipOperationsByFilters(
			ChangeOwnershipOperationTO changeOwnershipOperationTO) throws ServiceException {
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select distinct 	");
		
		sbQuery.append("	co.operationNumber,	"); 	 //0
		sbQuery.append("	co.registryDate,	");				 //1
		sbQuery.append("	(select parameterName from ParameterTable where parameterTablePk = chop.motive) as motive ,	");					 //2
		sbQuery.append("	(select parameterName from ParameterTable where parameterTablePk = co.state) as state, 	");						 //3
		sbQuery.append("	chop.motive,	");					 //4
		sbQuery.append("	co.state,	");						 //5
		
		sbQuery.append("	sha,	");			 				 //6
		
		sbQuery.append("	spa.idParticipantPk,	");      	 //7
		sbQuery.append("	spa.mnemonic,	");      		 	 //8
		sbQuery.append("	spa.description,	"); 			 //9
		
		sbQuery.append("	tpa.idParticipantPk,	");      	 //10
		sbQuery.append("	tpa.mnemonic,	");      		 	 //11
		sbQuery.append("	tpa.description,	"); 			 //12
		sbQuery.append("	chop.balanceType,	");			     //13
		sbQuery.append("	chop.idChangeOwnershipPk	");			     //14
		
		sbQuery.append(" from ChangeOwnershipDetail chod ");
		sbQuery.append(" inner join chod.changeOwnershipOperation chop ");
		sbQuery.append(" inner join chop.custodyOperation co ");
		sbQuery.append(" inner join chop.sourceParticipant spa ");
		sbQuery.append(" inner join chop.targetParticipant tpa ");
		sbQuery.append(" inner join chop.sourceHolderAccount sha ");
		sbQuery.append(" inner join chod.blockChangeOwnerships bco ");
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk())){
			sbQuery.append(" inner join sha.holderAccountDetails shad ");
			sbQuery.append(" inner join shad.holder sho ");
		}
		
		sbQuery.append(" inner join bco.holderAccount tha ");
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk())){
			sbQuery.append(" inner join tha.holderAccountDetails thad ");
			sbQuery.append(" inner join thad.holder tho ");
		}
		
		sbQuery.append(" where 1 = 1	");
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdOperationNumber())){
			sbQuery.append(" 	and co.operationNumber = :idOperationNumber ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdBalanceType())){
			sbQuery.append(" 	and chop.balanceType = :idBalanceType ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdReasonType())){
			sbQuery.append(" 	and chop.motive = :idMotiveType ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdStatus())){
			sbQuery.append(" 	and chop.state = :idState ");
		}
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getInitialDate())){
			sbQuery.append(" 	and trunc(co.registryDate) >= :initialDate ");
		}
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getFinalDate())){
			sbQuery.append(" 	and trunc(co.registryDate) <= :finalDate ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolderAccount().getAccountNumber())){
			sbQuery.append(" 	and ( sha.accountNumber = :accountNumber or tha.accountNumber = :accountNumber ) ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdParticipantPk())){
			sbQuery.append(" 	and ( spa.idParticipantPk = :idParticipantPk or tpa.idParticipantPk = :idParticipantPk ) ");
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk())){
			sbQuery.append(" 	and ( sho.idHolderPk = :idHolderPk or tho.idHolderPk = :idHolderPk ) ");
		}
		
		sbQuery.append(" order by co.operationNumber desc");

		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdOperationNumber())){
			query.setParameter("idOperationNumber", changeOwnershipOperationTO.getIdOperationNumber());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdBalanceType())){
			query.setParameter("idBalanceType", changeOwnershipOperationTO.getIdBalanceType());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdReasonType())){
			query.setParameter("idMotiveType", changeOwnershipOperationTO.getIdReasonType());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdStatus())){
			query.setParameter("idState", changeOwnershipOperationTO.getIdStatus());
		}
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getInitialDate())){
			query.setParameter("initialDate", changeOwnershipOperationTO.getInitialDate());
		}
		if(Validations.validateIsNotNull(changeOwnershipOperationTO.getFinalDate())){
			query.setParameter("finalDate", changeOwnershipOperationTO.getFinalDate());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolderAccount().getAccountNumber())){
			query.setParameter("accountNumber", changeOwnershipOperationTO.getSourceTargetHolderAccount().getAccountNumber());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getIdParticipantPk())){
			query.setParameter("idParticipantPk", changeOwnershipOperationTO.getIdParticipantPk());
		}
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk())){
			query.setParameter("idHolderPk", changeOwnershipOperationTO.getSourceTargetHolder().getIdHolderPk());
		}

		List<Object[]> resultList = query.getResultList();
		
		List<ChangeOwnershipDetailTO> chodResultList = new ArrayList<ChangeOwnershipDetailTO>();

		ChangeOwnershipDetailTO changeOwnershipDetailTO = new ChangeOwnershipDetailTO();
		
		for (int i = 0; i < resultList.size(); i++) {
			Object[] object = resultList.get(i);
			
			changeOwnershipDetailTO = new ChangeOwnershipDetailTO();
			changeOwnershipDetailTO.setOperationNumber(Long.valueOf(object[0].toString()));
			changeOwnershipDetailTO.setIdChangeOwnershipPk((Long)object[14]);
			changeOwnershipDetailTO.setRegisterDate((Date)object[1]);
			
			changeOwnershipDetailTO.setMotiveDescription(object[2].toString());
			changeOwnershipDetailTO.setStatusDescription(object[3].toString());
			
			changeOwnershipDetailTO.setIdMotive(Integer.valueOf(object[4].toString()));
			changeOwnershipDetailTO.setIdStatus(Integer.valueOf(object[5].toString()));
			
			changeOwnershipDetailTO.setIdBalanceType(Integer.valueOf(object[13].toString()));
			
			HolderAccount ha1 = (HolderAccount)object[6];
			for(HolderAccountDetail detail : ha1.getHolderAccountDetails()){
				detail.getHolder().getIdHolderPk();
			}
			changeOwnershipDetailTO.setSourceHolderAccount(ha1);
			
			changeOwnershipDetailTO.setIdSourceParticipantPk(Long.valueOf(object[7].toString()));
			changeOwnershipDetailTO.setSourceParticipantMnemonic(object[8].toString());
			changeOwnershipDetailTO.setSourceParticipantDescription(object[9].toString());

			changeOwnershipDetailTO.setIdTargetParticipantPk(Long.valueOf(object[10].toString()));
			changeOwnershipDetailTO.setTargetParticipantMnemonic(object[11].toString());
			changeOwnershipDetailTO.setTargetParticipantDescription(object[12].toString());
							
			chodResultList.add(changeOwnershipDetailTO); 			                
		}
		
		return chodResultList;
	}

	/**
	 * Gets the change ownership operation by filters.
	 *
	 * @param changeOwnershipOperationTO the change ownership operation to
	 * @return the change ownership operation by filters
	 * @throws ServiceException the service exception
	 */
	public List<ChangeOwnershipOperation> getChangeOwnershipOperationByFilters(
			ChangeOwnershipDetailTO changeOwnershipOperationTO) throws ServiceException {
		
		Map<String,Object> parameters = new HashMap<String, Object>();
		StringBuilder sbQuery = new StringBuilder();
        sbQuery.append("select chop from ChangeOwnershipOperation chop ");
        sbQuery.append(" inner join fetch chop.custodyOperation co ");
        sbQuery.append(" inner join fetch chop.sourceHolderAccount sha ");
        sbQuery.append(" inner join fetch chop.sourceParticipant sp ");
        sbQuery.append(" inner join fetch chop.targetParticipant tp ");
        sbQuery.append(" left join fetch chop.changeOwnershipDetails chod ");
        sbQuery.append(" where 1=1 and chod.indOriginTarget = 1 ");
        
        if(Validations.validateIsNotNull(changeOwnershipOperationTO.getIdChangeOwnershipPk())){
        	sbQuery.append(" and chop.idChangeOwnershipPk = :idChangeOwnershipPk");
        	parameters.put("idChangeOwnershipPk",changeOwnershipOperationTO.getIdChangeOwnershipPk());
        }
        
        if(Validations.validateIsNotNull(changeOwnershipOperationTO.getIdBalanceType())){
        	sbQuery.append(" and chop.balanceType = :balanceType");
        	parameters.put("balanceType",changeOwnershipOperationTO.getIdBalanceType());
        }
        
        if(Validations.validateIsNotNull(changeOwnershipOperationTO.getRegisterDate())){
        	sbQuery.append(" and trunc(co.registryDate) = :registryDate");
        	parameters.put("registryDate",changeOwnershipOperationTO.getRegisterDate());
        }
        
        if(Validations.validateIsNotNull(changeOwnershipOperationTO.getOperationNumber())){
        	sbQuery.append(" and co.operationNumber = :operationNumber");
        	parameters.put("operationNumber",changeOwnershipOperationTO.getOperationNumber());
        }
        
        if(Validations.validateIsNotNull(changeOwnershipOperationTO.getListOperationType())){
        	sbQuery.append(" and co.operationType in :operationType");
        	parameters.put("operationType",changeOwnershipOperationTO.getListOperationType());
        }
        sbQuery.append(" order by chop.idChangeOwnershipPk ");
        
        
        return (List<ChangeOwnershipOperation>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the change ownership file info.
	 *
	 * @param filter the filter
	 * @return the change ownership file info
	 * @throws ServiceException the service exception
	 */
	public List<ChangeOwnershipFile> getChangeOwnershipFileInfo(Map<String,Object> filter) 
			throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select  ");
		sbQuery.append("   new ChangeOwnershipFile (chof.idChangeOwnershipFilePk, ");  //0
		sbQuery.append("   chof.fileName, ");	//01
		sbQuery.append("   chof.fileType, ");  // 02
		sbQuery.append("   (select pt.parameterName from ParameterTable pt where pt.parameterTablePk = chof.fileType )  ) ");  // 03
		sbQuery.append(" from ChangeOwnershipFile chof ");
		sbQuery.append(" where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.get("idChangeOwnershipPk"))){
			sbQuery.append(" 	and chof.changeOwnershipOperation.idChangeOwnershipPk = :idChangeOwnershipPk ");
		}
		
		TypedQuery<ChangeOwnershipFile> query = em.createQuery(sbQuery.toString(),ChangeOwnershipFile.class);
		
		if(Validations.validateIsNotNull(filter.get("idChangeOwnershipPk"))){
			query.setParameter("idChangeOwnershipPk",  (Long) filter.get("idChangeOwnershipPk"));
		}
		
		return (List<ChangeOwnershipFile>) query.getResultList();
	}
	
	/**
	 * Gets the change ownership file content.
	 *
	 * @param filter the filter
	 * @return the change ownership file content
	 * @throws ServiceException the service exception
	 */
	public byte[] getChangeOwnershipFileContent(Map<String,Object> filter) 
			throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" select  ");
		sbQuery.append("  documentFile ");  //0
		sbQuery.append(" from ChangeOwnershipFile changeOwnershipFile ");
		sbQuery.append(" where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.get("idChangeOwnershipFilePk"))){
			sbQuery.append(" 	and changeOwnershipFile.idChangeOwnershipFilePk = :idChangeOwnershipFilePk ");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(filter.get("idChangeOwnershipFilePk"))){
			query.setParameter("idChangeOwnershipFilePk",  (Long) filter.get("idChangeOwnershipFilePk"));
		}
		
		return (byte[])query.getSingleResult();
	}
	

	/**
	 * Gets the all change ownership files.
	 *
	 * @param idChangeOwnershipPk the id change ownership pk
	 * @return the all change ownership files
	 */
	public List<ChangeOwnershipFile> getAllChangeOwnershipFiles(
			Long idChangeOwnershipPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ");
		sbQuery.append("  file ");  
		sbQuery.append(" from ChangeOwnershipFile file ");
		sbQuery.append(" where 1 = 1 ");
		sbQuery.append("  and file.changeOwnershipOperation.idChangeOwnershipPk = :idChangeOwnershipPk ");
		TypedQuery<ChangeOwnershipFile> query = em.createQuery(sbQuery.toString(),ChangeOwnershipFile.class);
		query.setParameter("idChangeOwnershipPk", idChangeOwnershipPk);
		return query.getResultList();
	}

	/**
	 * Count block operations by filters.
	 *
	 * @param filters the filters
	 * @return the integer
	 */
	public Integer countBlockOperationsByFilters(Map<String,Object> filters) {
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select count ( bo.idBlockOperationPk ) as countBlock  ");
		sbQuery.append(" From BlockOperation bo ");
		sbQuery.append(" inner join bo.blockRequest br ");
		sbQuery.append(" Where 1 = 1 ");	
		
		if(Validations.validateIsNotNull(filters.get("idHolderAccountPk")))
			sbQuery.append(" and bo.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		
		if(Validations.validateIsNotNull(filters.get("idParticipantPk")))
			sbQuery.append(" and bo.participant.idParticipantPk= :idParticipantPk ");
		
		if(Validations.validateIsNotNull(filters.get("idBlockState")))
			sbQuery.append(" and bo.blockState = :idBlockState ");
		
		if(Validations.validateIsNotNull(filters.get("idBlockType")))
			sbQuery.append(" and br.blockType = :idBlockType ");
		
		TypedQuery<Long> query =  em.createQuery(sbQuery.toString(),Long.class); 

		if(Validations.validateIsNotNull(filters.get("idHolderAccountPk")))
			query.setParameter("idHolderAccountPk",filters.get("idHolderAccountPk"));
		
		if(Validations.validateIsNotNull(filters.get("idParticipantPk")))
			query.setParameter("idParticipantPk",filters.get("idParticipantPk"));

		if(Validations.validateIsNotNull(filters.get("idBlockState")))
			query.setParameter("idBlockState",filters.get("idBlockState"));
		
		if(Validations.validateIsNotNull(filters.get("idBlockType")))
			query.setParameter("idBlockType",filters.get("idBlockType"));
		
		int count = ((Long) query.getSingleResult()).intValue();
		
		return count;
	}

	/**
	 * Register change ownership operation.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @return the change ownership operation
	 * @throws ServiceException the service exception
	 */
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 1440)
	public ChangeOwnershipOperation registerChangeOwnershipOperation(
			ChangeOwnershipOperation changeOwnershipOperation,Integer indMarketFact) throws ServiceException {
		
		// validations
		validateOperationDetails(changeOwnershipOperation);
		
		Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_CHANGE_OWNER);
		changeOwnershipOperation.getCustodyOperation().setOperationNumber(operationNumber);
		
		if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
			
			if(changeOwnershipOperation.getRegularizationIndicator()){
	        	List<ChangeOwnershipFile> files = getAllChangeOwnershipFiles(
	        			changeOwnershipOperation.getChangeOwnershipOperationRef().getIdChangeOwnershipPk());
	        	changeOwnershipOperation.setChangeOwnershipFiles(new ArrayList<ChangeOwnershipFile>());
	        	for (ChangeOwnershipFile changeOwnershipFile : files) {
	        		em.detach(changeOwnershipFile);
					changeOwnershipFile.setIdChangeOwnershipFilePk(null);
					changeOwnershipFile.setChangeOwnershipOperation(changeOwnershipOperation);
					changeOwnershipOperation.getChangeOwnershipFiles().add(changeOwnershipFile);
				}
	        }
			create(changeOwnershipOperation);
			
			AccountsComponentTO objAccountComponent = generateAccountsComponent(changeOwnershipOperation,true);
			objAccountComponent.setIdBusinessProcess(BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_REGISTER.getCode());
			objAccountComponent.setIndMarketFact(indMarketFact);
			accountsComponentService.get().executeAccountsComponent(objAccountComponent);
			
		}else if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
			
			for (ChangeOwnershipDetail changeOwnershipDetail : changeOwnershipOperation.getChangeOwnershipDetails()) {
				BigDecimal totalBlockBalance = changeOwnershipDetail.getPawnBalance().add(changeOwnershipDetail.getBanBalance()).add(changeOwnershipDetail.getOtherBlockBalance());
				affectationsServiceBean.validateCurrentReblocks(changeOwnershipDetail.getParticipant().getIdParticipantPk(),
						changeOwnershipDetail.getHolderAccount().getIdHolderAccountPk(),
						changeOwnershipDetail.getSecurity().getIdSecurityCodePk(), totalBlockBalance);
			}
			
			create(changeOwnershipOperation);
		}
		
		return changeOwnershipOperation;
	}

	/**
	 * Validate operation details.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @throws ServiceException the service exception
	 */
	private void validateOperationDetails(
			ChangeOwnershipOperation changeOwnershipOperation) throws ServiceException {
		Map<String,Object> param = new HashMap<String,Object>();
		
		// verificar que ya exista una solicitud
		//existsChangeOwnershipRequest(changeOwnershipOperation);
		
		// participante origen solo reg y bloq
		param = new HashMap<String,Object>();
		param.put("idParticipantPkParam", changeOwnershipOperation.getSourceParticipant().getIdParticipantPk());
		Participant srcParticipant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
		if(!srcParticipant.getState().equals(ParticipantStateType.REGISTERED.getCode()) && 
				!srcParticipant.getState().equals(ParticipantStateType.BLOCKED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK_PARAM,
					new Object[] {srcParticipant.getIdParticipantPk()});
		}
		
		// participante destino solo reg y bloq
		param.put("idParticipantPkParam", changeOwnershipOperation.getTargetParticipant().getIdParticipantPk());
		Participant tgtParticipant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
		if(!tgtParticipant.getState().equals(ParticipantStateType.REGISTERED.getCode()) && 
				!tgtParticipant.getState().equals(ParticipantStateType.BLOCKED.getCode())){
			throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK_PARAM,
					new Object[] {tgtParticipant.getIdParticipantPk()});
		}
		
		for(ChangeOwnershipDetail detail : changeOwnershipOperation.getChangeOwnershipDetails()){
			
			// cuenta origen no cerrada
			param = new HashMap<String,Object>();
			param.put("idHolderAccountPkParam", detail.getHolderAccount().getIdHolderAccountPk());
			HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
			if(holderAccount.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
				throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_CLOSED_PARAM,
						new Object[]{detail.getHolderAccount().getAccountNumber().toString()});
			}
			
			// titular origen no bloqueada cuando es distinta a sucesion
			if (changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				//if(!changeOwnershipOperation.getSourceHolderAccount().getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
					for(HolderAccountDetail holderAccountDetail: detail.getHolderAccount().getHolderAccountDetails()){
						param = new HashMap<String,Object>();
						param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
						Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
						if(holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode()) &&
								!changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){
							changeOwnershipOperation.setSourceHolderAccount(null);
							throw new ServiceException(ErrorServiceType.HOLDER_BLOCK_PARAM,
									new Object[]{holder.getIdHolderPk().toString()});
						}
					}
				//}
					
				// titular destino no bloqueado cuando es distinto a sucesion
				for (ChangeOwnershipDetail detail2 : detail.getTargetChangeOwnershipDetails()) { // solo target
					
					param = new HashMap<String,Object>();
					param.put("idHolderAccountPkParam", detail2.getHolderAccount().getIdHolderAccountPk());
					HolderAccount holderAccount2 = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
					if(holderAccount2.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_CLOSED_PARAM,
								new Object[]{detail2.getHolderAccount().getAccountNumber().toString()});
					}
					
					for(HolderAccountDetail holderAccountDetail: detail2.getHolderAccount().getHolderAccountDetails()){
						param = new HashMap<String,Object>();
						param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
						Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
						if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.HOLDER_BLOCK_PARAM,
									new Object[]{holder.getIdHolderPk().toString()});
						}
					}
				}
				
			}else if (changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
				
				// titular destino no bloqueado cuando es distinto a sucesion
				for (BlockChangeOwnership blkDetail : detail.getBlockChangeOwnerships()) { // solo target
					
					param = new HashMap<String,Object>();
					param.put("idHolderAccountPkParam", blkDetail.getHolderAccount().getIdHolderAccountPk());
					HolderAccount holderAccount2 = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
					if(holderAccount2.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_CLOSED_PARAM,
								new Object[]{blkDetail.getHolderAccount().getAccountNumber().toString()});
					}
					
					for(HolderAccountDetail holderAccountDetail: blkDetail.getHolderAccount().getHolderAccountDetails()){
						param = new HashMap<String,Object>();
						param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
						Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
						if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
							throw new ServiceException(ErrorServiceType.HOLDER_BLOCK_PARAM,
									new Object[]{holder.getIdHolderPk().toString()});
						}
					}
				}
				
			}
			
			
		}
		
		//all is ok
		
	}
	
	/**
	 * Exists change ownership request.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 */
	private void existsChangeOwnershipRequest(
			ChangeOwnershipOperation changeOwnershipOperation) {
		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append(" select count(chod.id.idChangeOwnershipDetailPk) ");
		stringBuilderSql.append(" from ChangeOwnershipDetail chod inner join chod.changeOwnershipOperation cho ");
		stringBuilderSql.append(" where cho.sourceParticipant.idParticipantPk = :srcPart ");
		stringBuilderSql.append(" where cho.targetParticipant.idParticipantPk = :tgtPart ");
		stringBuilderSql.append(" where cho.sourceHolderAccount.idHolderAccountPk = :srcAccountPk ");
		stringBuilderSql.append(" and ( ");
		stringBuilderSql.append(" 	select count(*) ChangeOwnershipDetail tchod ");
		stringBuilderSql.append(" 	where tchod.idChangeOwnershipDetailPk = chod.idChangeOwnershipDetailPk ");
		stringBuilderSql.append(" 	tchod.security.idSecurityCodePk = :isinCode  ");
		stringBuilderSql.append(" 	tchod.holderAccount.idHolderAccountPk  = tgtAccountPk ");
		stringBuilderSql.append(" ) > 0 ");
  
		Query query = em.createQuery(stringBuilderSql.toString());
		
		query.setParameter("srcPart", changeOwnershipOperation.getSourceParticipant().getIdParticipantPk());
		query.setParameter("tgtPart", changeOwnershipOperation.getTargetParticipant().getIdParticipantPk());
		
		int count = ((Long) query.getSingleResult()).intValue();
		
		//return count;
	}

	/**
	 * Validate operation state.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @throws ServiceException the service exception
	 */
	private void validateOperationState(
			ChangeOwnershipOperation changeOwnershipOperation) throws ServiceException {
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("idChangeOwnershipPkParam", changeOwnershipOperation.getIdChangeOwnershipPk());
		ChangeOwnershipOperation operation = findObjectByNamedQuery(ChangeOwnershipOperation.OPERATION_STATE, param);
		if(!operation.getState().equals(changeOwnershipOperation.getState())){
			throw new ServiceException(ErrorServiceType.CHANGE_OWNERSHIP_STATE_MODIFIED);
		}
		
	}

	/**
	 * Confirm change ownership operation.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 1440)
	public void confirmChangeOwnershipOperation(ChangeOwnershipOperation changeOwnershipOperation, Integer indMarketFact,
			LoggerUser loggerUser) throws Exception {
		
		
		validateOperationState(changeOwnershipOperation);
		
		validateOperationDetails(changeOwnershipOperation);
		
		changeOwnershipOperation.setState(ChangeOwnershipStatusType.CONFIRMADO.getCode());
		changeOwnershipOperation.getCustodyOperation().setState(ChangeOwnershipStatusType.CONFIRMADO.getCode());
		changeOwnershipOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDate());
		
		if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
			AccountsComponentTO objAccountsComponentTO = generateAccountsComponent(changeOwnershipOperation,false);
			objAccountsComponentTO.setIdBusinessProcess(new Long(BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode()));
			objAccountsComponentTO.setIndMarketFact(indMarketFact);
			update(changeOwnershipOperation);
			accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
		}else if (changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
			
			for (ChangeOwnershipDetail changeOwnershipDetail : changeOwnershipOperation.getChangeOwnershipDetails()) {
				BigDecimal totalBlockBalance = changeOwnershipDetail.getPawnBalance().add(changeOwnershipDetail.getBanBalance()).add(changeOwnershipDetail.getOtherBlockBalance());
				affectationsServiceBean.validateCurrentReblocks(changeOwnershipDetail.getParticipant().getIdParticipantPk(),
						changeOwnershipDetail.getHolderAccount().getIdHolderAccountPk(),
						changeOwnershipDetail.getSecurity().getIdSecurityCodePk(), totalBlockBalance);
			}
			
			changeOwnershipOperation.setIndLastRequest(ComponentConstant.ONE);
			update(changeOwnershipOperation);
			executeConfirmActionsForBlocked(changeOwnershipOperation,indMarketFact, loggerUser);
		}
		
		if(changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){
			if(!changeOwnershipOperation.getRegularizationIndicator()){
				executeAccountActions(changeOwnershipOperation);
			}else{
				changeOwnershipOperation.setIndLastRequest(ComponentConstant.ONE);
			}
		}
		
		/* SENDING THE CHANGE OWNERSHIP BY WEB SERVICE */
		ParameterTable parameterTable= parameterServiceBean.getParameterDetail(changeOwnershipOperation.getMotive());
		if (Validations.validateIsNotNull(parameterTable.getText1()) 
					|| (Validations.validateIsNotNullAndPositive(changeOwnershipOperation.getMotive())
							&& changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.EXCHANGE_SECURITIES.getCode())
						)
			) {
			for (ChangeOwnershipDetail changeOwnershipDetail: changeOwnershipOperation.getChangeOwnershipDetails()) {
				if (SecurityClassType.DPF.getCode().equals(changeOwnershipDetail.getSecurity().getSecurityClass()) ||
					SecurityClassType.DPA.getCode().equals(changeOwnershipDetail.getSecurity().getSecurityClass()))
				{
					if (Validations.validateListIsNotNullAndNotEmpty(changeOwnershipDetail.getTargetChangeOwnershipDetails())) {
						for (ChangeOwnershipDetail targetChangeOwnershipDetail: changeOwnershipDetail.getTargetChangeOwnershipDetails()) {
							//web service just for DPFs and DPAs
							custodyServiceConsumer.sendSecuritiesTransferOperationWebClient(targetChangeOwnershipDetail, loggerUser);
						}
					}
				}
			}
		}
	}

	/**
	 * Reject change ownership operation.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @throws ServiceException the service exception
	 */
	public void rejectChangeOwnershipOperation(ChangeOwnershipOperation changeOwnershipOperation,Integer indMarketFact) throws ServiceException {
		
		validateOperationState(changeOwnershipOperation);
		
		changeOwnershipOperation.setState(ChangeOwnershipStatusType.RECHAZADO.getCode());
		changeOwnershipOperation.setIndLastRequest(1);
		changeOwnershipOperation.getCustodyOperation().setState(ChangeOwnershipStatusType.RECHAZADO.getCode());
		changeOwnershipOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDate());
		
		if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
			AccountsComponentTO objAccountsComponentTO = generateAccountsComponent(changeOwnershipOperation,true);
			objAccountsComponentTO.setIndMarketFact(indMarketFact);
			objAccountsComponentTO.setIdBusinessProcess(new Long(BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_REJECT.getCode()));
			accountsComponentService.get().executeAccountsComponent(objAccountsComponentTO);
		}else if (changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
			//TODO
		}
		update(changeOwnershipOperation);
	}
	
	/**
	 * Generate accounts component.
	 *
	 * @param cho the cho
	 * @param onlySource the only source
	 * @return the accounts component to
	 */
	public AccountsComponentTO generateAccountsComponent(ChangeOwnershipOperation cho,boolean onlySource){
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
		
		objAccountComponent.setIdOperationType(cho.getCustodyOperation().getOperationType());
		objAccountComponent.setIdCustodyOperation(cho.getIdChangeOwnershipPk());
		
		List<HolderAccountBalanceTO> lstSourceAccounts = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> lstTargetAccounts = new ArrayList<HolderAccountBalanceTO>();

		for(ChangeOwnershipDetail sourceDetail: cho.getChangeOwnershipDetails()){
			HolderAccountBalanceTO sHolderAccountBalanceTO = new HolderAccountBalanceTO();
			sHolderAccountBalanceTO.setIdHolderAccount(sourceDetail.getHolderAccount().getIdHolderAccountPk());
			sHolderAccountBalanceTO.setIdParticipant(sourceDetail.getParticipant().getIdParticipantPk());
			sHolderAccountBalanceTO.setIdSecurityCode(sourceDetail.getSecurity().getIdSecurityCodePk());
			sHolderAccountBalanceTO.setStockQuantity(sourceDetail.getAvailableBalance());
			sHolderAccountBalanceTO.setLstMarketFactAccounts( new ArrayList<MarketFactAccountTO>());
			
			if(sourceDetail.getChangeOwnershipMarketFacts()!=null){
				sHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				sourceDetail.setChangeOwnershipMarketFacts(getMarketFactChangeOwnership(sourceDetail.getIdChangeOwnershipDetailPk(), null));
				for(ChangeOwnershipMarketFact mktFactDetail : sourceDetail.getChangeOwnershipMarketFacts()){
					MarketFactAccountTO mktFact = new MarketFactAccountTO();
					mktFact.setMarketDate(mktFactDetail.getMarketDate());
					mktFact.setMarketPrice(mktFactDetail.getMarketPrice());
					mktFact.setMarketRate(mktFactDetail.getMarketRate());
					mktFact.setQuantity(mktFactDetail.getMarketBalance());
					sHolderAccountBalanceTO.getLstMarketFactAccounts().add(mktFact);
				}
			}
			
			lstSourceAccounts.add(sHolderAccountBalanceTO);
			
			if(!onlySource){
				for(ChangeOwnershipDetail targetDetail: sourceDetail.getTargetChangeOwnershipDetails()){
					HolderAccountBalanceTO tHolderAccountBalanceTO = new HolderAccountBalanceTO();
					tHolderAccountBalanceTO.setIdHolderAccount(targetDetail.getHolderAccount().getIdHolderAccountPk());
					tHolderAccountBalanceTO.setIdParticipant(targetDetail.getParticipant().getIdParticipantPk());
					tHolderAccountBalanceTO.setIdSecurityCode(targetDetail.getSecurity().getIdSecurityCodePk());
					tHolderAccountBalanceTO.setStockQuantity(targetDetail.getAvailableBalance());
					
					if(targetDetail.getChangeOwnershipMarketFacts()!=null){
						tHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
						targetDetail.setChangeOwnershipMarketFacts(getMarketFactChangeOwnership(targetDetail.getIdChangeOwnershipDetailPk(), null));
						for(ChangeOwnershipMarketFact mktFactDetail : targetDetail.getChangeOwnershipMarketFacts()){
							MarketFactAccountTO mktFact = new MarketFactAccountTO();
							mktFact.setMarketDate(mktFactDetail.getMarketDate());
							mktFact.setMarketPrice(mktFactDetail.getMarketPrice());
							mktFact.setMarketRate(mktFactDetail.getMarketRate());
							mktFact.setQuantity(mktFactDetail.getMarketBalance());
							tHolderAccountBalanceTO.getLstMarketFactAccounts().add(mktFact);
						}
					}
					
					lstTargetAccounts.add(tHolderAccountBalanceTO);
				}
			}
		}
		
		objAccountComponent.setLstSourceAccounts(lstSourceAccounts);
		objAccountComponent.setLstTargetAccounts(lstTargetAccounts);
		
		return objAccountComponent;
	}
	
	/**
	 * Execute account actions.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @throws ServiceException the service exception
	 */
	public void executeAccountActions(ChangeOwnershipOperation changeOwnershipOperation) throws ServiceException{
		boolean existsRemainingBalances;
		boolean existsHolderInAccount;
		Holder holder;
		HolderHistoryState holderHistoryState;
		HolderRequest holderRequest;
		
		//Issue 773 - tomando en cuenta cuentas mancomunadas
		
		//Cuenta Origend
		HolderAccount sourceHolderAccount = changeOwnershipOperation.getSourceHolderAccount();
		//Cuenta Destino
		HolderAccount targetHolderAccount = changeOwnershipOperation.getChangeOwnershipDetails().get(0).getTargetChangeOwnershipDetails().get(0).getHolderAccount();

		// Recorriendo las cuentas de origen
 		for(HolderAccountDetail holderAccountDetail : sourceHolderAccount.getHolderAccountDetails()){
 			
 			//obteniendo el holder
 			holder = holderAccountDetail.getHolder();
			holder.setHolderFile(null);
			holder.setPepPerson(null);
			holder.setRepresentedEntity(null);
			
			holderHistoryState = new HolderHistoryState();
			holderRequest = new HolderRequest();
			
			holderRequest.setHolderHistoryStates(new ArrayList<HolderHistoryState>());
			holderRequest.setParticipant(sourceHolderAccount.getParticipant());
			holderRequest.setRequestMotive(HolderBlockMotiveType.DECEASE.getCode());
			holderRequest.setRequestType(HolderRequestType.BLOCK.getCode());	
			holderRequest.setRequesterType(HolderAccountRequesterType.CAVAPY.getCode());
			holderRequest.setActionDate(CommonsUtilities.currentDate());
			holderRequest.setStateHolderRequest(HolderRequestStateType.CONFIRMED.getCode());
			holderRequest.setRegistryDate(CommonsUtilities.currentDate());
			holderRequest.setRegistryUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
			holderRequest.setApproveDate(CommonsUtilities.currentDate());
			holderRequest.setApproveUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
			holderRequest.setConfirmDate(CommonsUtilities.currentDate());
			holderRequest.setConfirmUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
			holderRequest.setHolder(holder);
			holderRequest.setRequestNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_MODIFICATION_RNT));
			
			holderHistoryState.setHolder(holder);
			holderHistoryState.setHolderRequest(holderRequest);
			holderHistoryState.setRegistryUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
			holderHistoryState.setMotive(holderRequest.getRequestMotive());
			holderHistoryState.setUpdateStateDate(CommonsUtilities.currentDateTime());
			
			//verificando si tiene saldo contable en otras cuentas
			existsRemainingBalances = checkExistsAccountsWithTotalBalance(holder.getIdHolderPk());
			
			//verificando si el cui se encuentra en la cuenta destino
			existsHolderInAccount = verifyHoldersInHolderAccountDetail(holder, targetHolderAccount.getHolderAccountDetails());
			
			//verificando el estado del holder
			if(holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				holderHistoryState.setOldState(StateHistoryStateHolderType.REGISTERED.getCode());
			}else if (holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
				holderHistoryState.setOldState(StateHistoryStateHolderType.BLOCKED.getCode());
			}
			
			//si no se encuentra en la cuenta de destino
			if(!existsHolderInAccount){
				//si tiene saldo contable
				if(existsRemainingBalances){
					//estado cambia a bloqueado
					holder.setStateHolder(HolderStateType.BLOCKED.getCode());
					holderHistoryState.setNewState(StateHistoryStateHolderType.BLOCKED.getCode());
					update(holder);
				}else{
					//estado cambia a transferido
					changeOwnershipOperation.setIndLastRequest(ComponentConstant.ONE);
					holder.setStateHolder(HolderStateType.TRANSFERED.getCode());
					holderHistoryState.setNewState(StateHistoryStateHolderType.TRANSFERED.getCode());
					update(holder);
				}
				
				//obteniendo las cuentas del holder y bloqueandolas
				HolderAccountTO holderAccountTO = new HolderAccountTO();
				holderAccountTO.setIdHolderPk(holder.getIdHolderPk());
				List<HolderAccount> listHolderAccount = holderAccountComponentServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO);
				HolderAccountRequest holderAccountRequestHy;
				
				for(HolderAccount ha: listHolderAccount){
					holderAccountRequestHy = new HolderAccountRequest();
					holderAccountRequestHy.setHolderAccount(ha);
					holderAccountRequestHy.setParticipant(ha.getParticipant());
					holderAccountRequestHy.setRequestType(HolderAccountRequestHysType.BLOCK.getCode());
					holderAccountRequestHy.setRequestMotive(HolderAccountRequestBlockMotiveType.DIED.getCode());
					holderAccountRequestHy.setRegistryDate(CommonsUtilities.currentDateTime());
					holderAccountRequestHy.setRegistryUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
					holderAccountRequestHy.setApproveDate(CommonsUtilities.currentDateTime());
					holderAccountRequestHy.setApproveUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
					holderAccountRequestHy.setHolderAccountGroup(ha.getAccountGroup());
					holderAccountRequestHy.setHolderAccountType(ha.getAccountType());
					holderAccountRequestHy.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());
					holderAccountRequestHy = holderAccountComponentServiceBean.regHoldAccountReqComponentServiceBean(holderAccountRequestHy);
					holderAccountRequestHy.setConfirmDate(CommonsUtilities.currentDateTime());
					holderAccountRequestHy.setConfirmUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
					holderAccountComponentServiceBean.blockHolderAccountServiceBean(holderAccountRequestHy);
				}
				
			}else{
				//se mantiene el estado inicial
				holderHistoryState.setNewState(holder.getStateHolder());
			}
			
			holderRequest.getHolderHistoryStates().add(holderHistoryState);
			holderRequest = create(holderRequest);
			holderRequest.setRequestNumber(holderRequest.getIdHolderRequestPk());
			update(holderRequest);

			if(existsRemainingBalances){
				
				HolderAccountRequest holderAccountRequestHy = new HolderAccountRequest();
				holderAccountRequestHy.setHolderAccount(changeOwnershipOperation.getSourceHolderAccount());
				holderAccountRequestHy.setParticipant(changeOwnershipOperation.getSourceHolderAccount().getParticipant());
				holderAccountRequestHy.setRequestType(HolderAccountRequestHysType.BLOCK.getCode());
				holderAccountRequestHy.setRequestMotive(HolderAccountRequestBlockMotiveType.DIED.getCode());
				holderAccountRequestHy.setRegistryDate(CommonsUtilities.currentDateTime());
				holderAccountRequestHy.setRegistryUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
				holderAccountRequestHy.setApproveDate(CommonsUtilities.currentDateTime());
				holderAccountRequestHy.setApproveUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
				holderAccountRequestHy.setHolderAccountGroup(changeOwnershipOperation.getSourceHolderAccount().getAccountGroup());
				holderAccountRequestHy.setHolderAccountType(changeOwnershipOperation.getSourceHolderAccount().getAccountType());
				holderAccountRequestHy.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());
				holderAccountRequestHy = holderAccountComponentServiceBean.regHoldAccountReqComponentServiceBean(holderAccountRequestHy);
				holderAccountRequestHy.setConfirmDate(CommonsUtilities.currentDateTime());
				holderAccountRequestHy.setConfirmUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
				holderAccountComponentServiceBean.blockHolderAccountServiceBean(holderAccountRequestHy);
			}
 		}
	}

	/**
	 * Verifying holder exist in a holder account detail list
	 * @param holderPk
	 * @param listHolderAccountDetail
	 * @return
	 */
	public boolean verifyHoldersInHolderAccountDetail(Holder holder, List<HolderAccountDetail> listHolderAccountDetail){
		boolean existsHolder = false;
		
		for(HolderAccountDetail holderaccountDetail: listHolderAccountDetail){
			if(holderaccountDetail.getHolder().getIdHolderPk() == holder.getIdHolderPk()){
				existsHolder = true;
			}
		}
		
		return existsHolder;
	}
	
	/**
	 * Execute confirm actions for blocked.
	 *
	 * @param changeOwnershipOperation the change ownership operation
	 * @param indMarketFact the ind market fact
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmActionsForBlocked(ChangeOwnershipOperation changeOwnershipOperation, Integer indMarketFact,
						LoggerUser loggerUser) throws ServiceException{
		
        // 1. getting and ordering block_operations for unblock.
        
        for(ChangeOwnershipDetail sourceChangeOwnership : changeOwnershipOperation.getChangeOwnershipDetails()){
				
			for(BlockChangeOwnership blockChangeOwnership : sourceChangeOwnership.getBlockChangeOwnerships()){

				// single block_operation for reversal
				BlockOpeForReversalsTO blockOpeForReversal = new BlockOpeForReversalsTO();
        		blockOpeForReversal.setIdBlockOperationPk(blockChangeOwnership.getBlockOperation().getIdBlockOperationPk());
        		blockOpeForReversal.setAmountUnblock(blockChangeOwnership.getBlockQuantity());
				
        		// header for creating the unblock_request: one per block_operation
        		RegisterReversalTO registerReversalTO = new RegisterReversalTO();
        		registerReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
            	registerReversalTO.setRegistryUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
            	registerReversalTO.setDocumentNumber(blockChangeOwnership.getBlockOperation().getBlockRequest().getBlockNumber()); // temporal
        		registerReversalTO.setIdHolderPk(sourceChangeOwnership.getHolderAccount().getRepresentativeHolder().getIdHolderPk());
            	registerReversalTO.setTotalUnblock(blockChangeOwnership.getBlockQuantity());
            	registerReversalTO.setIdState(RequestReversalsStateType.APPROVED.getCode());
            	registerReversalTO.getLstBlockOpeForReversals().add(blockOpeForReversal);
            	
            	if(indMarketFact.equals(BooleanType.YES.getCode())){
            		blockOpeForReversal.setReversalsMarkFactList(new ArrayList<ReversalsMarkFactTO>());
            		blockChangeOwnership.setChangeOwnershipMarketFacts(getMarketFactChangeOwnership(null, blockChangeOwnership.getIdBlockChangeOwnershipPk()));
    				for(ChangeOwnershipMarketFact blockMarketFact: blockChangeOwnership.getChangeOwnershipMarketFacts()){
    					ReversalsMarkFactTO reversalMarketFact = new ReversalsMarkFactTO();
    					reversalMarketFact.setMarketDate(blockMarketFact.getMarketDate());
    					reversalMarketFact.setMarketPrice(blockMarketFact.getMarketPrice());
    					reversalMarketFact.setMarketRate(blockMarketFact.getMarketRate());
    					reversalMarketFact.setBlockAmount(blockMarketFact.getMarketBalance());
    					blockOpeForReversal.getReversalsMarkFactList().add(reversalMarketFact);
    				}
    			}
            	
            	//TO for confirmation
            	RequestReversalsTO requestReversalsTO = new RequestReversalsTO();
                requestReversalsTO.setIdUnblockRequestPk(requestReversalsServiceBean.saveReversalRequest(registerReversalTO));
                registerReversalTO.setConfirmationUser(changeOwnershipOperation.getCustodyOperation().getConfirmUser());
                requestReversalsTO.setBusinessProcessTypeId(BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode());
                requestReversalsServiceBean.confirmReversalRequest(requestReversalsTO, loggerUser, Boolean.FALSE);
			}

        }
        
        // 2. registering change ownership operation and executing component
		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
		
		objAccountComponent.setIdBusinessProcess(BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode());
		objAccountComponent.setIdOperationType(changeOwnershipOperation.getCustodyOperation().getOperationType());
		objAccountComponent.setIdCustodyOperation(changeOwnershipOperation.getCustodyOperation().getIdCustodyOperationPk());
		objAccountComponent.setIndMarketFact(indMarketFact);
		
		List<HolderAccountBalanceTO> lstSourceAccounts = new ArrayList<HolderAccountBalanceTO>();
		List<HolderAccountBalanceTO> lstTargetAccounts = new ArrayList<HolderAccountBalanceTO>();
		
		for(ChangeOwnershipDetail detail: changeOwnershipOperation.getChangeOwnershipDetails()){

			BigDecimal totalStockQuantity = BigDecimal.ZERO;
			for(BlockChangeOwnership blockDetail: detail.getBlockChangeOwnerships()){
				if(blockDetail.getBlockOperation().getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
					HolderAccountBalanceTO tHolderAccountBalanceTO = new HolderAccountBalanceTO();
					tHolderAccountBalanceTO.setIdHolderAccount(blockDetail.getHolderAccount().getIdHolderAccountPk());
					tHolderAccountBalanceTO.setIdParticipant(blockDetail.getParticipant().getIdParticipantPk());
					tHolderAccountBalanceTO.setIdSecurityCode(blockDetail.getChangeOwnershipDetail().getSecurity().getIdSecurityCodePk());
					tHolderAccountBalanceTO.setStockQuantity(blockDetail.getBlockQuantity());
					lstTargetAccounts.add(tHolderAccountBalanceTO);
					totalStockQuantity=totalStockQuantity.add(blockDetail.getBlockQuantity());
					
					if(indMarketFact.equals(ComponentConstant.ONE)){
						tHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
						for(ChangeOwnershipMarketFact blockMarketFact: blockDetail.getChangeOwnershipMarketFacts()){
							MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
							marketFactAccountTO.setMarketDate(blockMarketFact.getMarketDate());
							marketFactAccountTO.setMarketPrice(blockMarketFact.getMarketPrice());
							marketFactAccountTO.setMarketRate(blockMarketFact.getMarketRate());
							marketFactAccountTO.setQuantity(blockMarketFact.getMarketBalance());
	    					tHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
	    				}
					}
					
				}
			}
			
			HolderAccountBalanceTO sHolderAccountBalanceTO = new HolderAccountBalanceTO();
			sHolderAccountBalanceTO.setIdHolderAccount(detail.getHolderAccount().getIdHolderAccountPk());
			sHolderAccountBalanceTO.setIdParticipant(detail.getParticipant().getIdParticipantPk());
			sHolderAccountBalanceTO.setIdSecurityCode(detail.getSecurity().getIdSecurityCodePk());
			sHolderAccountBalanceTO.setStockQuantity(totalStockQuantity);
			lstSourceAccounts.add(sHolderAccountBalanceTO);
			
			if(indMarketFact.equals(ComponentConstant.ONE)){
				sHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				detail.setChangeOwnershipMarketFacts(getMarketFactChangeOwnership(detail.getIdChangeOwnershipDetailPk(),null));
				for(ChangeOwnershipMarketFact detailMarketFact: detail.getChangeOwnershipMarketFacts()){
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setMarketDate(detailMarketFact.getMarketDate());
					marketFactAccountTO.setMarketPrice(detailMarketFact.getMarketPrice());
					marketFactAccountTO.setMarketRate(detailMarketFact.getMarketRate());
					marketFactAccountTO.setQuantity(detailMarketFact.getMarketBalance());
					sHolderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
				}
			}
			
		}
		
		objAccountComponent.setLstSourceAccounts(lstSourceAccounts);
		objAccountComponent.setLstTargetAccounts(lstTargetAccounts);
		
		accountsComponentService.get().executeAccountsComponent(objAccountComponent);
		
		// 3. build and save new block_operations for targets
		
		List<AffectationTransferTO> transfers = new ArrayList<AffectationTransferTO>();
		//por detalle origen
		for(ChangeOwnershipDetail detail : changeOwnershipOperation.getChangeOwnershipDetails()){
			Map<Integer,List<BlockChangeOwnership>> mapBlock =  detail.groupBlockDetailsByAffec();
			// por afectacion
			for (Map.Entry<Integer, List<BlockChangeOwnership>> entry : mapBlock.entrySet()) {
				// por c/detalle destino
				for(BlockChangeOwnership blockChangeOwnership : entry.getValue()){
					AffectationTransferTO at = new AffectationTransferTO();
					at.setBlockChangeOwnership(blockChangeOwnership);
					//at.setIdOperationType(blockDetail.getBlockOperation().getCustodyOperation().getOperationType());
					at.setIdHolderPk(blockChangeOwnership.getHolderAccount().getRepresentativeHolder().getIdHolderPk());
					at.setIdHolderAccountPk(blockChangeOwnership.getHolderAccount().getIdHolderAccountPk());
					at.setIdParticipantPk(blockChangeOwnership.getParticipant().getIdParticipantPk());
					at.setIdSecurityCodePk(blockChangeOwnership.getChangeOwnershipDetail().getSecurity().getIdSecurityCodePk());
					at.setIdBlockOperationPk(blockChangeOwnership.getBlockOperation().getIdBlockOperationPk());
					at.setQuantity(blockChangeOwnership.getBlockQuantity());
					if(indMarketFact.equals(ComponentConstant.ONE)){
						at.setMarketFactAffectations(new ArrayList<MarketFactTO>());
						for(ChangeOwnershipMarketFact changeOwnershipMktFact : blockChangeOwnership.getChangeOwnershipMarketFacts()){
							MarketFactTO marketFactAffectationTransferTO = new MarketFactTO();
							marketFactAffectationTransferTO.setMarketDate(changeOwnershipMktFact.getMarketDate());
							marketFactAffectationTransferTO.setMarketPrice(changeOwnershipMktFact.getMarketPrice());
							marketFactAffectationTransferTO.setMarketRate(changeOwnershipMktFact.getMarketRate());
							marketFactAffectationTransferTO.setQuantity(changeOwnershipMktFact.getMarketBalance());
							at.getMarketFactAffectations().add(marketFactAffectationTransferTO);
						}
					}
					transfers.add(at);
				}
			}
			
		}
		
		affectationsServiceBean.transferBlockOperations(transfers,
				BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode(), 
				changeOwnershipOperation.getCustodyOperation().getConfirmUser(),indMarketFact);
	
	}
	
	/**
	 * Gets the pending sell balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the pending sell balance
	 */
	public BigDecimal getPendingSellBalance(
			HolderAccountBalance holderAccountBalance) {
		
		StringBuilder stringBuilderSql = new StringBuilder();
		 
		stringBuilderSql.append(" select nvl(sum(hao.stockQuantity),0) from ");
		stringBuilderSql.append(" HolderAccountOperation hao inner join hao.holderAccount ha inner join hao.mechanismOperation mo ");
		stringBuilderSql.append(" where hao.role = :idRole ");
		stringBuilderSql.append(" and hao.operationPart = :idOperationPart ");
		stringBuilderSql.append(" and hao.stockReference is null ");				
		stringBuilderSql.append(" and mo.operationState in :operationStates ");	
		
		if(Validations.validateIsNotNull(holderAccountBalance.getSecurity())){
			stringBuilderSql.append(" and mo.security.idSecurityCodePk =  :idSecurityCodePk ");
		}
		stringBuilderSql.append(" and ha.idHolderAccountPk = :idHolderAccountPk ");
		
		Query query = em.createNativeQuery(stringBuilderSql.toString());
		query.setParameter("idRole", GeneralConstants.TWO_VALUE_INTEGER.intValue());
		query.setParameter("idOperationPart", GeneralConstants.ONE_VALUE_INTEGER.intValue());
		List<Integer> states = new ArrayList<Integer>();
		states.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		states.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("operationStates", states );
		
		if(Validations.validateIsNotNull(holderAccountBalance.getSecurity())){
			query.setParameter("idSecurityCodePk", holderAccountBalance.getSecurity().getIdSecurityCodePk() );
		}
		query.setParameter("idHolderAccountPk", holderAccountBalance.getHolderAccount().getIdHolderAccountPk() );
		
		BigDecimal sumResult  = (BigDecimal)query.getSingleResult();
		
		return sumResult;
	}

	/**
	 * Gets the market fact change ownership.
	 *
	 * @param idChangeOwnershipDet the id change ownership det
	 * @param idBlockChangeOwnershipDet the id block change ownership det
	 * @return the market fact change ownership
	 */
	public List<ChangeOwnershipMarketFact> getMarketFactChangeOwnership(Long idChangeOwnershipDet, Long idBlockChangeOwnershipDet) {
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" SELECT chom from ");
		sbQuery.append("  ChangeOwnershipMarketFact chom where  ");
		if(idChangeOwnershipDet !=null){
			sbQuery.append(" chom.changeOwnershipDetail.idChangeOwnershipDetailPk =  :idChangeOwnershipDet ");
			parameters.put("idChangeOwnershipDet", idChangeOwnershipDet);
		}else if(idBlockChangeOwnershipDet !=null){
			sbQuery.append(" chom.blockChangeOwnership.idBlockChangeOwnershipPk =  :idBlockChangeOwnershipDet ");
			parameters.put("idBlockChangeOwnershipDet", idBlockChangeOwnershipDet);
		}
		
		return (List<ChangeOwnershipMarketFact>)findListByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the market fact balances.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param idHolderAccountPk the id holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @return the market fact balances
	 */
	public List<HolderMarketFactBalance> getMarketFactBalances(Long idParticipantPk,
			Long idHolderAccountPk, String idSecurityCodePk) {
		
		List<HolderMarketFactBalance> holderMarketFactBalances = new ArrayList<HolderMarketFactBalance>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" SELECT hmb from ");
		sbQuery.append("  HolderMarketFactBalance hmb where  ");
		sbQuery.append("      hmb.holderAccount.idHolderAccountPk =  :idHolderAccountPk ");
		sbQuery.append("  and hmb.participant.idParticipantPk =  :idParticipantPk ");
		sbQuery.append("  and hmb.security.idSecurityCodePk =  :idSecurityCodePk");
		
		parameters.put("idHolderAccountPk", idHolderAccountPk);
		parameters.put("idParticipantPk", idParticipantPk);
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		
		holderMarketFactBalances =  findListByQueryString(sbQuery.toString(), parameters);
		return holderMarketFactBalances;
			
	}
	
	/**
	 * Gets the market fact balances blocked.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param idHolderAccountPk the id holder account pk
	 * @param idSecurityCodePk the id security code pk
	 * @param idBlockOperationPk the id block operation pk
	 * @return the market fact balances blocked
	 */
	@SuppressWarnings("unchecked")
	public List<BlockMarketFactOperation> getMarketFactBalancesBlocked(Long idParticipantPk,
			Long idHolderAccountPk, String idSecurityCodePk, Long idBlockOperationPk){
		List<BlockMarketFactOperation> blockMktFactOperations = new ArrayList<BlockMarketFactOperation>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT bmfo from ");
		sbQuery.append("  BlockMarketFactOperation bmfo  ");
		sbQuery.append("  Inner Join bmfo.blockOperation  bo ");
		sbQuery.append("where bo.holderAccount.idHolderAccountPk =  :idHolderAccountPk ");
		sbQuery.append("  and bo.participant.idParticipantPk =  :idParticipantPk ");
		sbQuery.append("  and bo.securities.idSecurityCodePk =  :idSecurityCodePk ");
		sbQuery.append("  and bo.idBlockOperationPk =  :idBlockOperation ");
		parameters.put("idHolderAccountPk", idHolderAccountPk);
		parameters.put("idParticipantPk", idParticipantPk);
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		parameters.put("idBlockOperation", idBlockOperationPk);		
		blockMktFactOperations =  findListByQueryString(sbQuery.toString(), parameters);
		return blockMktFactOperations;
	}
	
	/**
	 * Check exists other accounts.
	 *
	 * @param idHolder the id holder
	 * @param accountType the account type
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean checkExistsOtherAccounts(Long idHolder, Integer accountType) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select distinct ha.idHolderAccountPk from HolderAccount ha " );
		sbQuery.append("	inner join ha.holderAccountDetails had ");	
		sbQuery.append("	inner join had.holder ho ");	
		sbQuery.append(" where ha.stateAccount in :stateAccount ");
		sbQuery.append(" and ha.accountType = :accountType  ");
		sbQuery.append(" and ho.idHolderPk = :idHolder ");
		
		List<Integer> states = new ArrayList<Integer>();
		states.add(HolderAccountStatusType.ACTIVE.getCode());
		states.add(HolderAccountStatusType.BLOCK.getCode());
		parameters.put("stateAccount", states );
		
		parameters.put("idHolder", idHolder );
		parameters.put("accountType", accountType );
		
		List<Object[]> result =  findListByQueryString(sbQuery.toString(), parameters);
		if(result.size() > 0){
			return true;
		}
		
		return false;
	}

	/**
	 * Check exists accounts with total balance.
	 *
	 * @param idHolderPk the id holder pk
	 * @return true, if successful
	 */
	public boolean checkExistsAccountsWithTotalBalance(Long idHolderPk ) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append(" SELECT hab.holderAccount.idHolderAccountPk from HolderAccountBalance hab where  ");
		sbQuery.append("  exists ( ");
		sbQuery.append("     select had.holder.idHolderPk from HolderAccountDetail had  ");
		sbQuery.append("     where had.holder.idHolderPk = :idHolder ");
		sbQuery.append("     and   had.holderAccount.idHolderAccountPk = hab.holderAccount.idHolderAccountPk ");
		sbQuery.append("  ) ");
		sbQuery.append("  and hab.totalBalance > 0 ");
		//sbQuery.append("  and hab.holderAccount.idHolderAccountPk <> :idExcludedHolderAccount");
		
		parameters.put("idHolder", idHolderPk);
		//parameters.put("idExcludedHolderAccount", idExcludedHolderAccount);
		
		List<Object[]> resultList  =  findListByQueryString(sbQuery.toString(), parameters);
		if(resultList.size() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * Generate securities transfer register to.
	 *
	 * @param changeOwnershipDetail the change ownership detail
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateSecuritiesTransferRegisterTO(ChangeOwnershipDetail changeOwnershipDetail, Long idInterfaceProcess, 
																			LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		SecuritiesTransferRegisterTO securitiesTransferRegisterTO= getSecuritiesTransferRegisterTO(changeOwnershipDetail);
		securitiesTransferRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(securitiesTransferRegisterTO, 
																	GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransaction(securitiesTransferRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}
	
	/**
	 * Gets the securities transfer register to.
	 *
	 * @param changeOwnershipDetail the change ownership detail
	 * @return the securities transfer register to
	 */
	public SecuritiesTransferRegisterTO getSecuritiesTransferRegisterTO(ChangeOwnershipDetail changeOwnershipDetail) {
		ChangeOwnershipDetail changeOwnershipDetailTemp= getChangeOwnershipInformation(changeOwnershipDetail.getIdChangeOwnershipDetailPk());
		
		HolderAccountObjectTO holderAccountObjectTO= new HolderAccountObjectTO();
		holderAccountObjectTO.setAccountNumber(changeOwnershipDetailTemp.getHolderAccount().getAccountNumber());
		
		ParameterTable parameterTable= parameterServiceBean.getParameterDetail(changeOwnershipDetailTemp.getHolderAccount().getAccountType());
		holderAccountObjectTO.setAccountTypeCode(parameterTable.getText1());
		
		List<HolderAccountDetail> lstHolderAccountDetails= changeOwnershipDetailTemp.getHolderAccount().getHolderAccountDetails();
		//if there are a list of holders then is a co-ownership of natural persons
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails) && lstHolderAccountDetails.size() > 1) {
			holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
		} else {
			Holder holder= lstHolderAccountDetails.get(0).getHolder();
			if (PersonType.NATURAL.getCode().equals(holder.getHolderType())) {
				holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				holderAccountObjectTO.setHolderType(GeneralConstants.TWO_VALUE_INTEGER);
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails)) {
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTO= new ArrayList<NaturalHolderObjectTO>();
			for (HolderAccountDetail holderAccountDetail: lstHolderAccountDetails) {
				if (PersonType.JURIDIC.getCode().equals(holderAccountDetail.getHolder().getHolderType())) {
					JuridicHolderObjectTO juridicHolderObjectTO= holderBalanceMovementsServiceBean.populateJuridicHolderObjectTO(
																							holderAccountDetail.getHolder(), true);
					holderAccountObjectTO.setJuridicHolderObjectTO(juridicHolderObjectTO);
				} else {
					NaturalHolderObjectTO naturalHolderObjectTO= holderBalanceMovementsServiceBean.populateNaturalHolderObjectTO(
																							holderAccountDetail.getHolder(), true);
					lstNaturalHolderObjectTO.add(naturalHolderObjectTO);
				}
			}
			holderAccountObjectTO.setLstNaturalHolderObjectTOs(lstNaturalHolderObjectTO);
		}
		
		SecurityObjectTO securityObjectTO= holderBalanceMovementsServiceBean.populateSecurityObjectTO(changeOwnershipDetailTemp.getSecurity());
		holderAccountObjectTO.setConditions(securityObjectTO.getRestrictions());
		SecuritiesTransferRegisterTO securitiesTransferRegisterTO= new SecuritiesTransferRegisterTO();
		ParameterTable parameterTransferType = parameterServiceBean.getParameterDetail(changeOwnershipDetail.getChangeOwnershipOperation().getMotive());
		if(Validations.validateIsNotNull(changeOwnershipDetail.getChangeOwnershipOperation())
				&& Validations.validateIsNotNullAndPositive(changeOwnershipDetail.getChangeOwnershipOperation().getIndExchangeSecurities())
				&& changeOwnershipDetail.getChangeOwnershipOperation().getIndExchangeSecurities().equals(BooleanType.YES.getCode()) ){
			// Transferencia por Intercambio de Valores. Dado que es una transferencia especial le correcponde: COX
			securitiesTransferRegisterTO.setTransferType("COX");	
		}else{
			securitiesTransferRegisterTO.setTransferType(parameterTransferType.getText1());
		}
		securitiesTransferRegisterTO.setSecurityObjectTO(securityObjectTO);
		securitiesTransferRegisterTO.setHolderAccountObjectTO(holderAccountObjectTO);		
		securitiesTransferRegisterTO.setIdCustodyOperation(changeOwnershipDetail.getChangeOwnershipOperation().getIdChangeOwnershipPk());
		securitiesTransferRegisterTO.setOperationCode(ComponentConstant.INTERFACE_SECURITIES_TRANSFER_DPF);
		securitiesTransferRegisterTO.setOperationDate(CommonsUtilities.currentDate());
		
		Security security = this.find(Security.class, changeOwnershipDetailTemp.getSecurity().getIdSecurityCodePk());		
		Participant objParticipant = participantServiceBean.getParticipantIsIssuer(security.getIssuer().getIdIssuerPk());
		if(objParticipant != null){
			securitiesTransferRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			securitiesTransferRegisterTO.setEntityNemonic(objParticipant.getMnemonic());
		}
						
		securitiesTransferRegisterTO.setOperationNumber(changeOwnershipDetail.getChangeOwnershipOperation().getCustodyOperation().getOperationNumber());
		
		return securitiesTransferRegisterTO;
	}
	
	/**
	 * Gets the change ownership information.
	 *
	 * @param idChangeOwnershipDetail the id change ownership detail
	 * @return the change ownership information
	 */
	public ChangeOwnershipDetail getChangeOwnershipInformation(Long idChangeOwnershipDetail) {
		StringBuffer stringBuffer= new StringBuffer();
		ChangeOwnershipDetail changeOwnershipDetail= null;
		try {
			stringBuffer.append(" SELECT COD ");
			stringBuffer.append(" FROM ChangeOwnershipDetail COD ");
			stringBuffer.append(" INNER JOIN FETCH COD.holderAccount HA ");
			stringBuffer.append(" INNER JOIN FETCH HA.participant PA ");
			stringBuffer.append(" INNER JOIN FETCH HA.holderAccountDetails HAD ");
			stringBuffer.append(" INNER JOIN FETCH HAD.holder HO ");
			stringBuffer.append(" WHERE COD.idChangeOwnershipDetailPk = :idChangeOwnershipDetail ");
			stringBuffer.append(" and COD.indOriginTarget = :indOriginTarget ");
			
			TypedQuery<ChangeOwnershipDetail> typedQuery= em.createQuery(stringBuffer.toString(), ChangeOwnershipDetail.class);
			typedQuery.setParameter("idChangeOwnershipDetail", idChangeOwnershipDetail);
			typedQuery.setParameter("indOriginTarget", GeneralConstants.TWO_VALUE_LONG);
			changeOwnershipDetail= typedQuery.getSingleResult();
		} catch (NoResultException e) {
			
		}
		return changeOwnershipDetail;
	}

}
