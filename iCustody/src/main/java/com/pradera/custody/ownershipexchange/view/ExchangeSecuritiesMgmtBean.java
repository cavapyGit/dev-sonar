package com.pradera.custody.ownershipexchange.view;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.ownershipexchange.facade.ChangeOwnershipServiceFacade;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipDetailTO;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipOperationTO;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipTransferTO;
import com.pradera.custody.ownershipexchange.to.HolderAccountBlockBalTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.changeownership.ChangeOwnershipFile;
import com.pradera.model.custody.changeownership.ChangeOwnershipMarketFact;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.ChangeOwnershipBalanceTypeType;
import com.pradera.model.custody.type.ChangeOwnershipMotiveType;
import com.pradera.model.custody.type.ChangeOwnershipProcessingTypeType;
import com.pradera.model.custody.type.ChangeOwnershipRejectMotiveType;
import com.pradera.model.custody.type.ChangeOwnershipStatusType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class ExchangeSecuritiesMgmtBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The upload file types. */
	private static final String fUploadFileTypes="doc|docx|xls|xlsx|pdf";
	
	/** The Constant GENERIC_WARNING_DIALOG_ID. */
	private static final String GENERIC_WARNING_DIALOG_ID = ":frmOwnershipExchangeRegistry:warningDialog";
	
	/** The Constant CONFIRMATION_DIALOG_ID. */
	private static final String CONFIRMATION_DIALOG_ID = ":frmOwnershipExchangeRegistry:transferOwnershipCnf";
	
	/** The Constant CONFIRMATION_DIALOG_ID_OK. */
	private static final String CONFIRMATION_DIALOG_ID_OK = ":frmOwnershipExchangeRegistry:transferOwnershipOk";
	
	/** The Constant REGISTRY_FORM_ID. */
	private static final String REGISTRY_FORM_ID = ":frmOwnershipExchangeRegistry";
	
	/** The lst balance types. */
	private List<ParameterTable> lstBalanceTypes = new ArrayList<ParameterTable>(0);
	
	/** The lst ownership exchange reason types. */
	private List<ParameterTable> lstOwnershipExchangeReasonTypes = new ArrayList<ParameterTable>(0);
	
	/** The lst ownership exchange status types. */
	private List<ParameterTable> lstOwnershipExchangeStatusTypes = new ArrayList<ParameterTable>(0);
	
	/** The lst ownership exchange proc type. */
	private List<ParameterTable> lstOwnershipExchangeProcType = new ArrayList<ParameterTable>(0);
	
	/** The lst attachment types. */
	private LinkedHashMap<Integer,ParameterTable> lstAttachmentTypes = null;
	
	/** The map parameters. */
	private HashMap<Integer, ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();
	
	/** The lst reject motives. */
	private List<ParameterTable> lstRejectMotives = new ArrayList<ParameterTable>(0);
	
	/** The participant list. */
	private List<Participant> participantList;
	
	/** The lst instrument type. */
	private List<ParameterTable> lstSecurityClass = new ArrayList<ParameterTable>();
	
	/** The search participant. */
	private Participant searchParticipant;
	
	/** The change ownership operation to. */
	private ChangeOwnershipOperationTO changeOwnershipOperationTO;	
	
	/** The change ownership operation. */
	private ChangeOwnershipOperation changeOwnershipOperation;
	
	/** The holder account balances data model. */
	private GenericDataModel<HolderAccountBalance> holderAccountBalancesDataModel;
	
	/** The holder account bal block data model. */
	private GenericDataModel<HolderAccountBlockBalTO> holderAccountBalBlockDataModel;
	
	/** The change ownership details data model. */
	private GenericDataModel<ChangeOwnershipDetailTO> changeOwnershipDetailsDataModel;
	
	/** The change ownership regularization data model. */
	private GenericDataModel<ChangeOwnershipDetailTO> changeOwnershipRegularizationDataModel;
	
	/** The selected holder account. */
	private HolderAccount selectedHolderAccount;

	/** The selected holder account balance. */
	private HolderAccountBalance selectedHolderAccountBalance; // for availables
	
	/** The selected holder account block bal to. */
	private HolderAccountBlockBalTO selectedHolderAccountBlockBalTO; // for blocked 
	
	/** The selected change ownership operation. */
	private ChangeOwnershipOperation selectedChangeOwnershipOperation;
	
	/** The selected change ownership detail to. */
	private ChangeOwnershipDetailTO selectedChangeOwnershipDetailTO;
	
	/** The selected change ownership for reg to. */
	private ChangeOwnershipDetailTO selectedChangeOwnershipForRegTO;
	
	/** The current detail list. */
	private List<ChangeOwnershipTransferTO> currentDetailList;
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/** The selected transfer. */
	private ChangeOwnershipTransferTO selectedTransfer;
	
	/** The selected change ownership file display. */
	private String selectedChangeOwnershipFileDisplay;
	
	/** The dynamic balance. */
	private BigDecimal dynamicBalance = BigDecimal.ZERO;
	
	/** The selected change ownership file type. */
	private Integer selectedChangeOwnershipFileType;
	
	/** The all transfered. */
	private boolean allTransfered;
	
	/** The is all transferable. */
	private boolean isAllTransferable;
	
	/** The show motive text. */
	private boolean showMotiveText;
	
	/** The has other motive. */
	//private boolean indDisableAcceptMotiveReject;
	private boolean hasOtherMotive;
	
	/** The has motive type. */
	private boolean hasMotiveType;
	
	/** The has editable detail. */
	private boolean hasEditableDetail;
	
	/** The enable comments. */
	private boolean enableComments;
	
	/** The issuance helper search. */
	private Issuance issuanceHelperSearch;
	private boolean validIssuance;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The change ownership service facade. */
	@EJB
	ChangeOwnershipServiceFacade changeOwnershipServiceFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The user info. */
	@Inject
    UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;

	/** The help market fact balance view. */
	MarketFactBalanceHelpTO helpMarketFactBalanceView = new MarketFactBalanceHelpTO();
	
	/** The map security instrument type. */
	Map<Integer, String> mapSecurityInstrumentType = new HashMap<Integer,String>();
	
	/** The xls_extension. */
	private String xls_extension  = ".xls";
	
	/** The xlsx_extension. */
	private String xlsx_extension = ".xlsx";
	
	/** The doc_extension. */
	private String doc_extension  = ".doc";
	
	/** The docx_extension. */
	private String docx_extension = ".docx";
	
	/** The pdf_extension. */
	private String pdf_extension  = ".pdf";
	
	private Integer securityClass = 0;
	
	private String numberSecuritiesShow = "0";
	
	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	

	public String getNumberSecuritiesShow() {
		return numberSecuritiesShow;
	}

	public void setNumberSecuritiesShow(String numberSecuritiesShow) {
		this.numberSecuritiesShow = numberSecuritiesShow;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		
		changeOwnershipDetailsDataModel = null;
		hasOtherMotive = Boolean.FALSE;
		hasMotiveType = Boolean.FALSE;
		enableComments = Boolean.FALSE;
		changeOwnershipOperation = new ChangeOwnershipOperation();
		changeOwnershipOperationTO = new ChangeOwnershipOperationTO();
		changeOwnershipOperationTO.setIdBalanceType(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode());
		
		changeOwnershipOperation.setSourceHolderAccount(null);
		changeOwnershipOperation.setSourceParticipant(new Participant());
		changeOwnershipOperation.setTargetParticipant(new Participant());
		try {
			if(participantList == null || participantList.size() <= 0){
				List<Integer> states = new ArrayList<Integer>();
				states.add(ParticipantStateType.REGISTERED.getCode());
				states.add(ParticipantStateType.BLOCKED.getCode());
				Participant filter = new Participant();
				filter.setLstParticipantStates(states);
				setParticipantList(helperComponentFacade.getLisParticipantServiceFacade(filter));
			}
			
			loadLstBalanceTypes();
			loadLstMotiveTypes();
			loadLstStatusTypes();
			loadLstOwnershipExchangeProcType();
			loadParameters();
			loadInstrumentType();
			setValidButtonsListener();
			loadSecurityClass();
			
			//Check if the user is Depositary or participant for set the participant PK  
			if(!isDepositaryUser()){
				for(Participant participant : participantList ){
					if(participant.getIdParticipantPk().equals(userInfo.getUserAccountSession().getParticipantCode())){
						searchParticipant = participant;
					}
				}
				changeOwnershipOperationTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void validateIssuanceRegitered(){
		hideDialogues();
		validIssuance = true;
		
		if(Validations.validateIsNotNull(issuanceHelperSearch)
				&& Validations.validateIsNotNullAndNotEmpty(issuanceHelperSearch.getIdIssuanceCodePk())){
			// validar si el indicador de Intercambio de Valores de la emision esta permitido
			if(Validations.validateIsNullOrNotPositive(issuanceHelperSearch.getIndExchangeSecurities())
					|| issuanceHelperSearch.getIndExchangeSecurities().equals(BooleanType.NO.getCode()) ){
				//Lanzar mensaje de que la emision ingresada no permite el intercambio de valores. Verifique 
				showMessageOnDialog(PropertiesConstants.MSG_ERROR_TITLE, null, PropertiesConstants.MESSAGE_VALIDATION_EXCHANGESECURITIES_ISSUANCEINVALID_NOTALLOWEDEXCHAGESECURITIES, null);
				//JSFUtilities.showComponent("warningDialog");
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				issuanceHelperSearch=new Issuance();
				validIssuance = false;
				JSFUtilities.updateComponent(":frmOwnershipExchangeRegistry:opnlDialogs");
				return;
			}
			
			// TODO validar si el tramo de colocacion esta abierto
//			if(Validations.validateIsNullOrNotPositive(issuanceHelperSearch.getNumberTotalTranches())
//					|| Validations.validateIsNotNullAndPositive(issuanceHelperSearch.getNumberPlacementTranches())){
//				showMessageOnDialog(PropertiesConstants.MSG_ERROR_TITLE, null, PropertiesConstants.MESSAGE_VALIDATION_EXCHANGESECURITIES_ISSUANCEINVALID_NOFOUNDPLACEMENTTRANCHES, null);
//				//JSFUtilities.showComponent("warningDialog");
//				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
//				issuanceHelperSearch=new Issuance();
//				validIssuance = false;
//				JSFUtilities.updateComponent(":frmOwnershipExchangeRegistry:opnlDialogs");
//				return;
//			}
			
		}else{
			validIssuance = false;
		}
	}
	
	private void loadSecurityClass() throws ServiceException{
		lstSecurityClass = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		paramTable.setOrderByText1(1);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstSecurityClass.add(param);
		}		
	}
	
	/**
	 * Load instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadInstrumentType() throws ServiceException{
		List<ParameterTable> lstInstrumentType = new ArrayList<ParameterTable>();
		mapSecurityInstrumentType = new HashMap<Integer, String>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstInstrumentType.add(param);
			mapSecurityInstrumentType.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParameters() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
			masterPks.add(MasterTableType.ACCOUNTS_TYPE.getCode());
			masterPks.add(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
			masterPks.add(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_BALANCE_TYPE.getCode());
			masterPks.add(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_MOTIVE_TYPE.getCode());
			masterPks.add(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_REJECT_MOTIVE_TYPE.getCode());
			masterPks.add(MasterTableType.TYPE_AFFECTATION.getCode());
			masterPks.add(MasterTableType.LEVEL_AFFECTATION.getCode());
			masterPks.add(MasterTableType.HOLDER_STATE_TYPE.getCode());
			masterPks.add(MasterTableType.SECURITIES_CLASS.getCode());
		parameterTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt);
		}
		
	}
	
	/**
	 * Gets the parameter.
	 *
	 * @param parameterPk the parameter pk
	 * @return the parameter
	 */
	public ParameterTable getParameter(Integer parameterPk){
		return mapParameters.get(parameterPk);
	}

	/**
	 * Load lst ownership exchange proc type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstOwnershipExchangeProcType() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ComponentConstant.ONE);
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_PROC_TYPE.getCode());
		lstOwnershipExchangeProcType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load lst balance types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstBalanceTypes() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_BALANCE_TYPE.getCode());
		List<Integer> lstParamTabPkNotIn = new ArrayList<>();
		lstParamTabPkNotIn.add(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode());
		parameterTableTO.setLstParameterTablePkNotIn(lstParamTabPkNotIn);
		lstBalanceTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Load lst motive types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstMotiveTypes() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_MOTIVE_TYPE.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		lstOwnershipExchangeReasonTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		lstOwnershipExchangeReasonTypes = getOrderListReasonsType(lstOwnershipExchangeReasonTypes);
	}
	
	/**
	 * Gets the order list reasons type.
	 *
	 * @param lstOwnershipExchangeReasonTypes the lst ownership exchange reason types
	 * @return the order list reasons type
	 */
	private List<ParameterTable> getOrderListReasonsType(List<ParameterTable> lstOwnershipExchangeReasonTypes){
		Collections.sort(lstOwnershipExchangeReasonTypes, new Comparator<ParameterTable>() {

			@Override
			public int compare(ParameterTable p1, ParameterTable p2) {
				return p1.getParameterName().compareToIgnoreCase(p2.getParameterName());
			}
		});
		return lstOwnershipExchangeReasonTypes;
	}
	
	/**
	 * Load lst status types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadLstStatusTypes() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_STATUS_TYPE.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		lstOwnershipExchangeStatusTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		// qutando el estado rectificado xq aca NO Corresponde
		List<ParameterTable> lstAux = new ArrayList<>();
		for(ParameterTable pt:lstOwnershipExchangeStatusTypes){
			if(Validations.validateIsNotNull(pt) && Validations.validateIsNotNullAndPositive(pt.getParameterTablePk())
					&& pt.getParameterTablePk().equals(ChangeOwnershipStatusType.RECTIFIED.getCode()) ){
				continue;
			}
			lstAux.add(pt);
		}
		lstOwnershipExchangeStatusTypes = new ArrayList<>();
		lstOwnershipExchangeStatusTypes.addAll(lstAux);
	}
	
	/**
	 * On change search participant.
	 */
	public void onChangeSearchParticipant(){
		changeOwnershipOperationTO.setSourceTargetHolderAccount(new HolderAccount());
	}
	
	/**
	 * Load register information.
	 *
	 * @return the string
	 */
	public String loadRegisterInformation(){
		cleanAllRegistryFrm();	
		issuanceHelperSearch = new Issuance();
		validIssuance = false;
		return "newExchangeSecurities";
	}
	
	/**
	 * Gets the tamanio max on file.
	 *
	 * @return the tamanio max on file
	 */
	public String getTamanioMaxOnFile(){
		return PropertiesUtilities.getMessage(PropertiesConstants.LBL_CUSTODY_ALLOWED_PER_ONE_FILE_SIZE, new Object[]{GeneralConstants.TAMANIO_FILE_KBS});
	}
	
	/**
	 * On change balance type.
	 */
	public void onChangeBalanceType(){
		hideDialogues();
		//JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		JSFUtilities.resetComponent(REGISTRY_FORM_ID);
		//JSFUtilities.resetViewRoot();
		setHolderAccountBalancesDataModel(null);
		setHolderAccountBalBlockDataModel(null);
		allTransfered = false;
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		changeOwnershipOperation.setSourceHolderAccount(null);
	}

	/**
	 * On change motive type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void onChangeMotiveType() throws ServiceException {
		hideDialogues();
		setEnableComments(false);
		JSFUtilities.resetComponent(REGISTRY_FORM_ID);
		setHolderAccountBalancesDataModel(null);
		setHolderAccountBalBlockDataModel(null);
		allTransfered = false;
		lstAttachmentTypes = null;
		changeOwnershipOperationTO.setFiles(new HashMap<Integer,ChangeOwnershipFile>());
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		changeOwnershipOperation.setComments(null);
		changeOwnershipOperation.setSourceHolderAccount(null);
		
		ParameterTableTO filter = new ParameterTableTO();
		
		ParameterTable selMotive = mapParameters.get(changeOwnershipOperation.getMotive());
		if(selMotive!=null && selMotive.getShortInteger()!=null){ // short integer contains the reference to master table for attachments
			filter.setMasterTableFk(selMotive.getShortInteger());
			filter.setOrderbyParameterName(BooleanType.YES.getCode());
			List<ParameterTable> params =generalParametersFacade.getListParameterTableServiceBean(filter);
			if(params.size() > 0){
				lstAttachmentTypes = new LinkedHashMap<Integer, ParameterTable>();
				for (ParameterTable param : params) {
					lstAttachmentTypes.put(param.getParameterTablePk(),param);
				}
			}
		}

		if(ChangeOwnershipMotiveType.OTHERS.getCode().equals(changeOwnershipOperation.getMotive())){
			setEnableComments(true);
		}
	}
	
	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event){	
		String fDisplayName=fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
		
		if(fDisplayName!=null){
			long fileSize = event.getFile().getSize();
			String fullNameFile=event.getFile().getFileName();
			String extension = fullNameFile.substring(fullNameFile.lastIndexOf(".", fullNameFile.length()-4), fullNameFile.length());
			String tipoArchivo ="";
			long formatKB=((long)1024);

			if(Validations.validateIsNotNullAndNotEmpty(extension)){
				if(extension.trim().equalsIgnoreCase(xls_extension) || extension.equalsIgnoreCase(xlsx_extension)){
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_EXCEL_FILE);
				}else if(extension.trim().equalsIgnoreCase(doc_extension) || extension.equalsIgnoreCase(docx_extension)){
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_WORD_FILE);
				}else if(extension.trim().equalsIgnoreCase(pdf_extension)){
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_PDF_FILE);
				}else{
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_UNKNOW_FILE);
				}
			}else{
				tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_UNKNOW_FILE);
			}
			
			selectedChangeOwnershipFileDisplay=fDisplayName;
			
			ChangeOwnershipFile cttFile = new ChangeOwnershipFile();

			cttFile.setChangeOwnershipOperation(changeOwnershipOperation);
			cttFile.setFileName(fDisplayName);
			cttFile.setDocumentFile(event.getFile().getContents());
			//cttFile.setFileTypeDescription(lstAttachmentTypes.get(selectedChangeOwnershipFileType).getParameterName());
			cttFile.setFileTypeDescription("INTERCAMBIO DE VALORES");
			cttFile.setFileFormat(tipoArchivo);
			cttFile.setFileSize(fileSize/formatKB);
			//cttFile.setFileType(lstAttachmentTypes.get(selectedChangeOwnershipFileType).getParameterTablePk());
			cttFile.setFileType(0);
			
			if(changeOwnershipOperationTO.getFiles()==null){
				changeOwnershipOperationTO.setFiles(new HashMap<Integer,ChangeOwnershipFile>(0));
			}
			// el nro de adjuntos siempre es 1
			changeOwnershipOperationTO.setFiles(new HashMap<Integer,ChangeOwnershipFile>(0));
			changeOwnershipOperationTO.getFiles().put(cttFile.getFileType(), cttFile);
			
			if(changeOwnershipOperation.getChangeOwnershipFiles()==null){
				changeOwnershipOperation.setChangeOwnershipFiles(new ArrayList<ChangeOwnershipFile>());
			}//new ArrayList<ChangeOwnershipFile>(changeOwnershipOperationTO.getFiles().values())
			// el nro de adjuntos siempre es 1
			changeOwnershipOperation.setChangeOwnershipFiles(new ArrayList<ChangeOwnershipFile>());
			changeOwnershipOperation.getChangeOwnershipFiles().add(cttFile);
			
			selectedChangeOwnershipFileType = null;
		}
	}
	
	/**
	 * On change attachment type.
	 */
	public void onChangeAttachmentType(){
		setSelectedChangeOwnershipFileDisplay(null);
	}
	
	/**
	 * On change source participant.
	 */
	public void onChangeSourceParticipant(){
		JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		//JSFUtilities.resetViewRoot();
		changeOwnershipOperationTO.setSourceHolder(new Holder());
		changeOwnershipOperationTO.setSourceHolderAccounts(null);
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		changeOwnershipOperation.setSourceHolderAccount(null);
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
		allTransfered = false;
	}
	
	/**
	 * Validate source holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSourceHolder() throws ServiceException{
		hideDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		
		changeOwnershipOperationTO.setSourceHolderAccounts(null);
		changeOwnershipOperation.setSourceHolderAccount(null);
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
		
		if(changeOwnershipOperationTO.getSourceHolder().getIdHolderPk()!=null){
			
			// verify if is registered or blocked
			if(!changeOwnershipOperationTO.getSourceHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode()) && 
					!changeOwnershipOperationTO.getSourceHolder().getStateHolder().equals(HolderStateType.BLOCKED.getCode()) ){
				ParameterTable stateHolder = mapParameters.get(changeOwnershipOperationTO.getSourceHolder().getStateHolder());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_NOT_REGISTERED,
								new Object[]{changeOwnershipOperationTO.getSourceHolder().getIdHolderPk().toString(),
								stateHolder == null? null :stateHolder.getParameterName()}));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperationTO.setSourceHolder(new Holder());
				return;
			}
			// Issue 773: quitando la verificacion, se permite cambio de titularidad para cuenta mancomunada y motivo de sucesion
//			// verify: if it is heritage motive, the source CUI should not have any coproperty - account with total balance, in any broker.  
//			if(ChangeOwnershipMotiveType.SUCESION.getCode().equals(changeOwnershipOperation.getMotive())){
//				boolean exists = changeOwnershipServiceFacade.checkExistsOtherAccounts(changeOwnershipOperationTO.getSourceHolder().getIdHolderPk(), HolderAccountType.OWNERSHIP.getCode());
//				if(exists){
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
//							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_SUCESION_CONTAINS_OWNERSHIP));
//					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
//					changeOwnershipOperationTO.setSourceHolder(new Holder());
//					return;
//				}
//			}
						
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(changeOwnershipOperation.getSourceParticipant().getIdParticipantPk());
			holderAccountTO.setIdHolderPk(changeOwnershipOperationTO.getSourceHolder().getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);
			holderAccountTO.setHolderaccountGroupType(HolderAccountGroupType.INVERSTOR.getCode());
			List<HolderAccount> accountList = changeOwnershipServiceFacade.getHolderAccounts(holderAccountTO);
			if(accountList.size() > 0 ){
				
				changeOwnershipOperationTO.setSourceHolderAccounts(accountList);
				//check true when accountList return only one account
				if(changeOwnershipOperationTO.getSourceHolderAccounts().size() == ComponentConstant.ONE){
					changeOwnershipOperationTO.getSourceHolderAccounts().get(0).setSelected(true);
					HolderAccount ha = changeOwnershipOperationTO.getSourceHolderAccounts().get(0);
					ha.setSelected(true);
					changeOwnershipOperation.setSourceHolderAccount(ha);
					//call validation of holder accounts
					validateSourceHolderAccount();
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_CHANGE_OWNERSHIP_NO_ACCOUNTS));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperationTO.setSourceHolder(new Holder());
				return;
			}
		}
	}
	
	/**
	 * Validate source holder account.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSourceHolderAccount() throws ServiceException {
		hideDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
		allTransfered = false;
		
		if(Validations.validateIsNotNull(changeOwnershipOperation.getSourceHolderAccount())){
			
			if(Validations.validateIsNullOrNotPositive(changeOwnershipOperation.getBalanceType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_OWNERSHIP_BALANCE_TYPE_REQUIRED));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperation.setSourceHolderAccount(null);
				return;
			}
			
			if(Validations.validateIsNullOrNotPositive(changeOwnershipOperation.getMotive())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_OWNERSHIP_MOTIVE_REQUIRED));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperation.setSourceHolderAccount(null);
				return;
			}
			
			if(changeOwnershipOperation.getSourceHolderAccount().getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_ACCOUNT_CLOSED));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperation.setSourceHolderAccount(null);
				return;
			}
			// Issue 773, se permite cambio de titularidad por motivo de sucesion y cuenta mancomunada
//			// verificando si en tipo de cuenta es mancomunado y es sucesion             
//			if(changeOwnershipOperation.getSourceHolderAccount().getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())
//					&& changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){				
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
//						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_SUCESION_OWNERSHIP));
//				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
//				changeOwnershipOperation.setSourceHolderAccount(null);
//				return;
//			}
			//add		
			if(changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.DIVISION_COPROPIEDAD.getCode()) 
					&& !changeOwnershipOperation.getSourceHolderAccount().getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
				ParameterTable accountType = mapParameters.get(changeOwnershipOperation.getSourceHolderAccount().getAccountType());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_DIVISION_SOURCE,
								new Object[]{accountType == null? null :accountType.getParameterName()}));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperation.setSourceHolderAccount(null);
				return;
			}//end add
			
			// si esto esta validacion es cambiada, tambien se debe cambiar 
			// en el serviceBean en la validacion al momento de guardar solicitud. 
			if (changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				if(!changeOwnershipOperation.getSourceHolderAccount().getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
					Holder holder = changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails().get(0).getHolder();
					if(holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode()) && 
							!changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_SOURCE_BLOCKED_ENTIREDSTATE,
										new Object[]{holder.getIdHolderPk().toString()}));
						JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
						changeOwnershipOperation.setSourceHolderAccount(null);
						return;
					}
				}
			}
			
			if(Validations.validateIsNotNull(changeOwnershipOperationTO.getTargetHolderAccounts())){
				boolean equalsToSource = false;
				Long idSameHolderPk = null;
				
				//Issue 773: se quito esta verificacion
//				for(HolderAccount trgHa: changeOwnershipOperationTO.getTargetHolderAccounts()){
//					for(HolderAccountDetail trgHad : trgHa.getHolderAccountDetails()){
//						for(HolderAccountDetail srcHad : changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails()){
//							if(srcHad.getHolder().getIdHolderPk().equals(trgHad.getHolder().getIdHolderPk())){
//								equalsToSource = true;
//								idSameHolderPk = trgHad.getHolder().getIdHolderPk();
//								break;
//							}
//						}
//					}
//				}
				
				//Issue 773
				//Se permite el cambio de titularidad en tanto todos los holders no sean iguales
				
				// Recorriendo la lista de cuentas de destino seleccionadas
				for(HolderAccount trgHa: changeOwnershipOperationTO.getTargetHolderAccounts()){
					//verificando si la cantidad de CUIs de destino y origen son iguales
					if(trgHa.getHolderAccountDetails().size() == changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails().size()){
						int cantSameHolder = 0;
						// Recorriendo detalles de las cuentas de detino seleccionadas
						for(HolderAccountDetail trgHad : trgHa.getHolderAccountDetails()){
							//Recorriendo detalles de la cuenta de origen seleccionada
							for(HolderAccountDetail srcHad : changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails()){
								//si la cuenta de destino contiene el holder de la cuenta de origen
								if(trgHad.getHolder().getIdHolderPk().equals(srcHad.getHolder().getIdHolderPk())){
									//verificando que la cuenta no sea la misma
									idSameHolderPk = trgHad.getHolder().getIdHolderPk();
									cantSameHolder ++;
								}
							}
						}
						
						// si todos son iguales no se debe permitir el cambio
						if (cantSameHolder == changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails().size()){
							equalsToSource = true;
						}
					}
				}
				
				if(equalsToSource){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_EQUAL_SOURCE,
									new Object[]{idSameHolderPk.toString()}));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					changeOwnershipOperation.setSourceHolderAccount(null);
					return;
				}
				
			}
			
			Map<String,Object> filters = new HashMap<String, Object>();
			filters.put("idHolderAccountPk", changeOwnershipOperation.getSourceHolderAccount().getIdHolderAccountPk());
			filters.put("idParticipantPk", changeOwnershipOperation.getSourceParticipant().getIdParticipantPk());
			
			int count = 0;
			if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				count = changeOwnershipServiceFacade.countAvailableBalancesByFilters(filters);
				if(count <= 0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NO_BALANCES_AVAILABLE));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					changeOwnershipOperation.setSourceHolderAccount(null);
					return;
				}
			}else if (changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
				filters.put("idBlockState", AffectationStateType.BLOCKED.getCode());
				count = changeOwnershipServiceFacade.countBlockOperationsByFilters(filters);
				if(count <= 0){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NO_BLOCKED_BALANCES_AVAILABLE));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					changeOwnershipOperation.setSourceHolderAccount(null);
					return;
				}
			}
			
			// get documnt type
			for(HolderAccountDetail detail : changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails()){
				ParameterTable pt = mapParameters.get(detail.getHolder().getDocumentType());
				if(pt!=null){
					detail.getHolder().setDescriptionTypeDocument(pt.getParameterName());
				}
			}
			
		}
	}
	
	/**
	 * On change target participant.
	 */
	public void onChangeTargetParticipant(){
		JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		changeOwnershipOperationTO.setTargetHolder(new Holder());
		changeOwnershipOperationTO.setTargetHolderAccounts(new ArrayList<HolderAccount>(0));
	}
	
	/**
	 * Validate target holder.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateTargetHolder() throws ServiceException{
		hideDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		changeOwnershipOperationTO.setFoundTargetAccounts(new ArrayList<HolderAccount>());
		//cada vez que se cambie el CUI, se refrescara las cuentas seleccionadas
		changeOwnershipOperationTO.setTargetHolderAccounts(new ArrayList<HolderAccount>());
		if(changeOwnershipOperationTO.getTargetHolder().getIdHolderPk()!=null){
			
			if(!changeOwnershipOperationTO.getTargetHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				
				ParameterTable stateHolder = mapParameters.get(changeOwnershipOperationTO.getTargetHolder().getStateHolder());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_SOURCE_HOLDER_NOT_REGISTERED,
								new Object[]{changeOwnershipOperationTO.getTargetHolder().getIdHolderPk().toString(),
								stateHolder == null? null :stateHolder.getParameterName()}));
				
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperationTO.setTargetHolder(new Holder());
				return;
			}
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(changeOwnershipOperation.getTargetParticipant().getIdParticipantPk());
			holderAccountTO.setIdHolderPk(changeOwnershipOperationTO.getTargetHolder().getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.TRUE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);
			holderAccountTO.setHolderaccountGroupType(HolderAccountGroupType.INVERSTOR.getCode());
			List<HolderAccount> accountList = changeOwnershipServiceFacade.getHolderAccounts(holderAccountTO);
			if(accountList.size() > 0 ){
				changeOwnershipOperationTO.setFoundTargetAccounts(accountList);
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_CHANGE_OWNERSHIP_NO_ACCOUNTS));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				changeOwnershipOperationTO.setTargetHolder(new Holder());
				return;
			}
		}
	}
	
	
	/**
	 * Validate target holder account.
	 */
	public void validateTargetHolderAccount() {
		hideDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
		allTransfered = false;

		boolean exists = false;
		boolean fromSameHolder = false;
		boolean equalsToSource = false;
		Long idSameHolderPk = null;
		
		if(selectedHolderAccount.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_TARGET_ACCOUNT_CLOSED));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			selectedHolderAccount = null;
			return;
		}
		//add
		if(ChangeOwnershipMotiveType.DIVISION_COPROPIEDAD.getCode().equals(changeOwnershipOperation.getMotive()) 
				&& selectedHolderAccount.getAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
			ParameterTable accountType = mapParameters.get(selectedHolderAccount.getAccountType());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_DIVISION_DESTINATION,
							new Object[]{accountType == null? null :accountType.getParameterName()}));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			selectedHolderAccount = null;
			return;
		}//end add
				
		if(Validations.validateIsNotNull(changeOwnershipOperation.getSourceHolderAccount())){			
			//add
			boolean existHold = false;
			if(ChangeOwnershipMotiveType.DIVISION_COPROPIEDAD.getCode().equals(changeOwnershipOperation.getMotive())){
				for(HolderAccountDetail selHad : selectedHolderAccount.getHolderAccountDetails()){
					for(HolderAccountDetail srcHad : changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails()){
						if(srcHad.getHolder().getIdHolderPk().equals(selHad.getHolder().getIdHolderPk())){
							existHold = true;
							break;
						}
					}
				}
				if(!existHold){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_INCLUDE_HOLDER));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					selectedHolderAccount = null;
					return;
				} 
			} else {
				//Issue 773
				//Se permite el cambio de titularidad en tanto los holders no sean iguales
				
				//verificando si los CUIs de destino y origen son iguales
				if(selectedHolderAccount.getHolderAccountDetails().size() == changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails().size()){
					int cantSameHolder = 0;
					// recorriendo los holders de la cuenta de origen
					for(HolderAccountDetail srcHad : changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails()){
						//recorriendo la cuenta de destino
						for(HolderAccountDetail selHad : selectedHolderAccount.getHolderAccountDetails()){
							//si la cuenta de destino contiene el holder de la cuenta de origen
							if(srcHad.getHolder().getIdHolderPk().equals(selHad.getHolder().getIdHolderPk())){
								idSameHolderPk = selHad.getHolder().getIdHolderPk();
								cantSameHolder ++;
							}
						}
					}
					// si todos son iguales no se debe permitir el cambio
					if (cantSameHolder == changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails().size()){
						equalsToSource = true;
					}
				}
				//Issue 773: verificacion quitada, se permite la transferencia en tanto los cuis de destino sean diferentes
//				for(HolderAccountDetail selHad : selectedHolderAccount.getHolderAccountDetails()){
//					for(HolderAccountDetail srcHad : changeOwnershipOperation.getSourceHolderAccount().getHolderAccountDetails()){
//						if(srcHad.getHolder().getIdHolderPk().equals(selHad.getHolder().getIdHolderPk())){
//							equalsToSource = true;
//							idSameHolderPk = selHad.getHolder().getIdHolderPk();
//							break;
//						}
//					}
//				}
			}
		}
			
		for(HolderAccount ha: changeOwnershipOperationTO.getTargetHolderAccounts()){
			if(ha.getAlternateCode().equals(selectedHolderAccount.getAlternateCode())){
				exists = true;
				break;
			}
			for(HolderAccountDetail selHad : selectedHolderAccount.getHolderAccountDetails()){
				for(HolderAccountDetail had: ha.getHolderAccountDetails()){
					if(selHad.getHolder().getIdHolderPk().equals(had.getHolder().getIdHolderPk())){
						fromSameHolder = true;
						idSameHolderPk = selHad.getHolder().getIdHolderPk();
						break;
					}
				}
			}
		}
		
		if(!exists && !fromSameHolder && !equalsToSource){
			changeOwnershipOperationTO.getTargetHolderAccounts().add(selectedHolderAccount);
			changeOwnershipOperationTO.getFoundTargetAccounts().remove(selectedHolderAccount);
			if(changeOwnershipOperationTO.getFoundTargetAccounts().isEmpty()){
				//changeOwnershipOperationTO.setFoundTargetAccounts(null);
				changeOwnershipOperationTO.setTargetHolder(new Holder());
			}
			selectedHolderAccount = null;
		}else{	
			if(exists){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_ACCOUNT_ALREADY_ADDED));
			}
			if(fromSameHolder){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_ALREADY_EXISTS, 
								new Object[]{idSameHolderPk.toString()}));
			}
			if(equalsToSource){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_TARGET_HOLDER_EQUAL_SOURCE,
								new Object[]{idSameHolderPk.toString()}));
			}
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			selectedHolderAccount = null;
		}
		
	}
	
	/**
	 * Removes the target account.
	 *
	 * @param tha the tha
	 */
	public void removeTargetAccount(HolderAccount tha){
		changeOwnershipOperationTO.getTargetHolderAccounts().remove(tha);
		//al momento de eliminar una cuenta de destino, esta se anadira a las cuentas no seleccionadas
		changeOwnershipOperationTO.getFoundTargetAccounts().add(tha);
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
//		if(Validations.validateListIsNullOrEmpty(changeOwnershipOperationTO.getTargetHolderAccounts())){
//			changeOwnershipOperationTO.setTargetHolderAccounts(new ArrayList<HolderAccount>(0));
//		}
		allTransfered = false;
	}
	
	/**
	 * Search holder account balances.
	 */
	public void searchHolderAccountBalances(){
		hideDialogues();
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
		selectedHolderAccountBlockBalTO = null;
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
		allTransfered = false;
		
		if(Validations.validateIsNull(changeOwnershipOperation.getSourceHolderAccount())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_EMPTY_SOURCE_ACCOUNT));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			return;
		}
		
		if(Validations.validateListIsNullOrEmpty(changeOwnershipOperationTO.getTargetHolderAccounts())){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_EMPTY_TARGET_DATATABLE));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			return;
		}
				
		Map<String,Object> filters = new HashMap<String,Object>();
		
		filters.put("idParticipantPk",changeOwnershipOperation.getSourceParticipant().getIdParticipantPk());
		filters.put("idHolderAccountPk",changeOwnershipOperation.getSourceHolderAccount().getIdHolderAccountPk());
		filters.put("securityClass", securityClass);
		filters.put("numberSecuritiesShow", numberSecuritiesShow);
		
		try {
			if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				
				List<HolderAccountBalance> holderAccountBalancesList = listHolderAccountBalances(filters,mapParameters);
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalancesList)){
					setHolderAccountBalancesDataModel(new GenericDataModel<HolderAccountBalance>(holderAccountBalancesList));
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NO_BALANCES_AVAILABLE));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					return;
				}
				
			}else{
				List<HolderAccountBalance> holderAccountBals = listHolderAccountBalances(filters, mapParameters);
				
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBals)){
					
					List<HolderAccountBlockBalTO> list = new ArrayList<HolderAccountBlockBalTO>();
					
					for (HolderAccountBalance holderAccountBalance : holderAccountBals) {
						HolderAccountBlockBalTO blockBalTO = new HolderAccountBlockBalTO();
						blockBalTO.setHolderAccountBalance(holderAccountBalance);
						if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){
							if(holderAccountBalance.getPawnBalance().longValue() > 0 ){
								fillBlockOperationsByAffectation(AffectationType.PAWN.getCode(), blockBalTO);
							}else if (holderAccountBalance.getOtherBlockBalance().longValue() > 0 ){
								fillBlockOperationsByAffectation(AffectationType.OTHERS.getCode(), blockBalTO);
							}else if (holderAccountBalance.getBanBalance().longValue() > 0 ){
								fillBlockOperationsByAffectation(AffectationType.BAN.getCode(), blockBalTO);
							}
						}
						list.add(blockBalTO);
					}
					setHolderAccountBalBlockDataModel(new GenericDataModel<HolderAccountBlockBalTO>(list));
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NO_BLOCKED_BALANCES_AVAILABLE));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					return;
				}
				
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * List holder account balances.
	 *
	 * @param filters the filters
	 * @param mapParameters the map parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> listHolderAccountBalances(Map<String, Object> filters, HashMap<Integer, ParameterTable> mapParameters) throws ServiceException {
		
		isAllTransferable = false;
		List<HolderAccountBalance> balances = new ArrayList<HolderAccountBalance>();
		List<Object[]> result = changeOwnershipServiceFacade.listHolderAccountBalancesServiceFacade(filters);
		
		for (Object[] balObject : result) {
			
			HolderAccountBalance holderAccountBalance =  (HolderAccountBalance) balObject[0];
			Security security = new Security((String) balObject[1]);
			security.setDescription((String) balObject[2]);
			security.setSecurityClass((Integer) balObject[3]);
			security.setCfiCode((String) balObject[4]);
			security.setIdIsinCode((String) balObject[5]);
			
			ParameterTable securityClass = mapParameters.get(security.getSecurityClass());
			if(securityClass!=null){
				security.setSecurityClassDesc(securityClass.getText1());
			}
			
			holderAccountBalance.setSecurity(security);
			holderAccountBalance.setPendingSellBalance((BigDecimal)balObject[6]);
			
			BigDecimal availableToTransfer = holderAccountBalance.getAvailableBalance().subtract(holderAccountBalance.getPendingSellBalance());
			
			if(availableToTransfer.compareTo(BigDecimal.ZERO) > 0){
				isAllTransferable  = true;
				holderAccountBalance.setCopiedAvailableBalance(availableToTransfer);
			}else{
				holderAccountBalance.setCopiedAvailableBalance(BigDecimal.ZERO);
			}
			
			balances.add(holderAccountBalance);
		}
		return balances;
	}
	
	/**
	 * Clean all registry frm.
	 */
	public void cleanAllRegistryFrm(){
		selectedHolderAccountBalance = null;
		
		hideDialogues();
		//search data watwatwat
		changeOwnershipOperationTO = new ChangeOwnershipOperationTO();
		changeOwnershipDetailsDataModel = null;
		
		changeOwnershipOperation = new ChangeOwnershipOperation();
		
		holderAccountBalancesDataModel = null;
		holderAccountBalBlockDataModel = null;
		
		selectedHolderAccountBalance = null;
		selectedHolderAccountBlockBalTO = null;
		allTransfered = Boolean.FALSE;
		enableComments= Boolean.FALSE;
		
		changeOwnershipOperation.setSourceHolderAccount(null);
		changeOwnershipOperation.setSourceParticipant(new Participant());
		changeOwnershipOperation.setTargetParticipant(new Participant());
		// por defecto para isssue 1379
		changeOwnershipOperation.setBalanceType(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode());
		changeOwnershipOperation.setMotive(ChangeOwnershipMotiveType.EXCHANGE_SECURITIES.getCode());
		//setEnableComments(true);
		setEnableComments(false);

		lstAttachmentTypes = null;
		selectedChangeOwnershipFileDisplay = null;
		
		//JSFUtilities.setValidViewComponentAndChildrens(REGISTRY_FORM_ID);
		JSFUtilities.resetViewRoot();
		
	}
	
	/**
	 * Verify on back action.
	 *
	 * @return the string
	 */
	public String verifyOnBackAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("PF('transferOwnershipBackCnfWidget').show();");
			return null;
		}else{
			return backToSearch();
		}
	}

	/**
	 * Verify on clean action.
	 */
	public void verifyOnCleanAction(){
		hideDialogues();
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("PF('transferOwnershipClearCnfWidget').show();");
		}else{
			cleanAllRegistryFrm();
		}
	}
	
	
	/**
	 * Checks if is modified form.
	 *
	 * @return true, if is modified form
	 */
	public boolean isModifiedForm(){
		hideDialogues();
		if(Validations.validateIsNotNullAndPositive(changeOwnershipOperation.getBalanceType()) ||
				Validations.validateIsNotNullAndPositive(changeOwnershipOperation.getMotive()) ||
				Validations.validateIsNotNullAndPositive(changeOwnershipOperation.getSourceParticipant().getIdParticipantPk()) ||
				Validations.validateIsNotNullAndPositive(changeOwnershipOperation.getTargetParticipant().getIdParticipantPk()) ||
			    Validations.validateListIsNotNullAndNotEmpty(changeOwnershipOperation.getChangeOwnershipFiles()) ||
			    Validations.validateListIsNotNullAndNotEmpty(changeOwnershipOperation.getChangeOwnershipDetails())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Back to search.
	 *
	 * @return the string
	 */
	public String backToSearch(){
		selectedChangeOwnershipOperation = null;
		selectedChangeOwnershipDetailTO = null;
		setViewOperationType(ViewOperationsType.CONSULT.getCode());
		return "searchExchangeSecurities";
	}
	
	/**
	 * Validate transfer.
	 *
	 * @param hab the hab
	 */
	public void validateTransfer(HolderAccountBalance hab){
		
		hasEditableDetail = Boolean.TRUE;
		if(isValidAvailSingle(hab)){
			selectedHolderAccountBalance = hab;
			currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();
			dynamicBalance = selectedHolderAccountBalance.getAvailableBalance();
			
			for (HolderAccount targetHolderAccount : changeOwnershipOperationTO.getTargetHolderAccounts()){
				
				/* verify existing target */
				ChangeOwnershipDetail targetDetail = new ChangeOwnershipDetail();
				targetDetail.setChangeOwnershipOperation(changeOwnershipOperation); 
				targetDetail.setIndOriginTarget(ComponentConstant.TARGET);
				targetDetail.setSecurity(selectedHolderAccountBalance.getSecurity());
				targetDetail.setHolderAccount(targetHolderAccount);
				targetDetail.setParticipant(targetHolderAccount.getParticipant());
				
				ChangeOwnershipTransferTO detail = new ChangeOwnershipTransferTO();
				detail.setHolderAccount(targetHolderAccount);
				detail.setParticipant(targetHolderAccount.getParticipant());
				detail.setSecurity(selectedHolderAccountBalance.getSecurity());
				
				int idx  = changeOwnershipOperationTO.getAllTargetChangeOwnershipDetails().indexOf(targetDetail);
				if(idx >= 0){
					ChangeOwnershipDetail idxDetail = changeOwnershipOperationTO.getAllTargetChangeOwnershipDetails().get(idx);
					detail.setQuantityBalance(idxDetail.getAvailableBalance());
					dynamicBalance = dynamicBalance.subtract(detail.getQuantityBalance());
					detail.setSelected(true);
					detail.setDelete(true);
				}
				currentDetailList.add(detail);
			}
			
			JSFUtilities.executeJavascriptFunction("PF('ownershipExchangeAvailDialog').show();");
		}
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param transferObject the transfer object
	 */
	public void showMarketFactUI(ChangeOwnershipTransferTO transferObject){		
		selectedTransfer = transferObject;
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(selectedTransfer.getSecurity().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(selectedHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(selectedHolderAccountBalance.getParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
	}
	
	/**
	 * Show market fact ui detail.
	 *
	 * @param cod the cod
	 * @param flag the flag
	 */
	public void showMarketFactUIDetail(ChangeOwnershipDetail cod, boolean flag){	
		helpMarketFactBalanceView = new MarketFactBalanceHelpTO();

		if(cod!=null && cod.getIndOriginTarget()!=null && cod.getIndOriginTarget()>0){

			helpMarketFactBalanceView.setSecurityCodePk(cod.getSecurity().getIdSecurityCodePk());
			helpMarketFactBalanceView.setHolderAccountPk(cod.getHolderAccount().getIdHolderAccountPk());
			helpMarketFactBalanceView.setParticipantPk(cod.getParticipant().getIdParticipantPk());
			helpMarketFactBalanceView.setSecurityDescription(cod.getSecurity().getDescription());
			helpMarketFactBalanceView.setInstrumentType(cod.getSecurity().getInstrumentType());
			helpMarketFactBalanceView.setInstrumentDescription(mapSecurityInstrumentType.get(cod.getSecurity().getInstrumentType()));
			helpMarketFactBalanceView.setBalanceResult(BigDecimal.ZERO);
			helpMarketFactBalanceView.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());

			List<ChangeOwnershipMarketFact> lstMarketFact = changeOwnershipServiceFacade.getMarketFactChangeOwnership(cod.getIdChangeOwnershipDetailPk(),null);
			if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFact)){
				for (ChangeOwnershipMarketFact changeOwnershipMarketFact : lstMarketFact) {
					MarketFactDetailHelpTO marketFactBalanceHelpTO = new MarketFactDetailHelpTO();
					marketFactBalanceHelpTO.setTotalBalance(changeOwnershipMarketFact.getMarketBalance());
					marketFactBalanceHelpTO.setMarketDate(changeOwnershipMarketFact.getMarketDate());
					marketFactBalanceHelpTO.setMarketPrice(changeOwnershipMarketFact.getMarketPrice());
					marketFactBalanceHelpTO.setMarketRate(changeOwnershipMarketFact.getMarketRate());
					helpMarketFactBalanceView.getMarketFacBalances().add(marketFactBalanceHelpTO);
					
					helpMarketFactBalanceView.setBalanceResult(helpMarketFactBalanceView.getBalanceResult().add(marketFactBalanceHelpTO.getTotalBalance()));
				}
			}
			
		}
	}
	
	/**
	 * Show security detail.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityDetail(String securityCode){
		securityHelpBean.setSecurityCode(securityCode);
		securityHelpBean.setName("securityHelp");
		securityHelpBean.searchSecurityHandler();
	}
	
	/**
	 * Show market fact ui detail block.
	 *
	 * @param bco the bco
	 */
	public void showMarketFactUIDetailBlock(BlockChangeOwnership bco){	
		helpMarketFactBalanceView = new MarketFactBalanceHelpTO();
		ChangeOwnershipDetail cod= bco.getChangeOwnershipDetail();
		if(cod!=null && cod.getIndOriginTarget()!=null && cod.getIndOriginTarget()>0){

			helpMarketFactBalanceView.setSecurityCodePk(cod.getSecurity().getIdSecurityCodePk());
			helpMarketFactBalanceView.setHolderAccountPk(bco.getHolderAccount().getIdHolderAccountPk());
			helpMarketFactBalanceView.setParticipantPk(bco.getParticipant().getIdParticipantPk());
			helpMarketFactBalanceView.setSecurityDescription(cod.getSecurity().getDescription());
			helpMarketFactBalanceView.setInstrumentType(cod.getSecurity().getInstrumentType());
			helpMarketFactBalanceView.setInstrumentDescription(mapSecurityInstrumentType.get(cod.getSecurity().getInstrumentType()));
			helpMarketFactBalanceView.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
			helpMarketFactBalanceView.setBalanceResult(BigDecimal.ZERO);
			
			List<ChangeOwnershipMarketFact> lstMarketFact = changeOwnershipServiceFacade.getMarketFactChangeOwnership(null,bco.getIdBlockChangeOwnershipPk());
			if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFact)){
				for (ChangeOwnershipMarketFact changeOwnershipMarketFact : lstMarketFact) {
					MarketFactDetailHelpTO marketFactBalanceHelpTO = new MarketFactDetailHelpTO();
					marketFactBalanceHelpTO.setTotalBalance(changeOwnershipMarketFact.getMarketBalance());
					marketFactBalanceHelpTO.setMarketDate(changeOwnershipMarketFact.getMarketDate());
					marketFactBalanceHelpTO.setMarketPrice(changeOwnershipMarketFact.getMarketPrice());
					marketFactBalanceHelpTO.setMarketRate(changeOwnershipMarketFact.getMarketRate());
					helpMarketFactBalanceView.getMarketFacBalances().add(marketFactBalanceHelpTO);
					
					helpMarketFactBalanceView.setBalanceResult(helpMarketFactBalanceView.getBalanceResult().add(changeOwnershipMarketFact.getMarketBalance()));
				}
			}
		}		
	}
	
	/**
	 * Show block market fact ui.
	 *
	 * @param transferObject the transfer object
	 */
	public void showBlockMarketFactUI(ChangeOwnershipTransferTO transferObject){		
		selectedTransfer = transferObject;
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(selectedTransfer.getSecurity().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(selectedHolderAccountBlockBalTO.getHolderAccountBalance().getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(selectedHolderAccountBlockBalTO.getHolderAccountBalance().getParticipant().getIdParticipantPk());
		marketFactBalance.setIndBlockBalance(ComponentConstant.ONE);
		marketFactBalance.setIdBlockOperationPk(selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getIdBlockOperationPk());
		marketFactBalance.setBlockOperationType(selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getBlockRequest().getBlockType());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
//		BigDecimal total = BigDecimal.ZERO;
//		for(ChangeOwnershipMarketFact mktFact: selectedTransfer.getChangeOwnershipMarketFact()){
//			MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
//			marketDetail.setMarketFactBalancePk(mktFact.getHolderMarketFactBalance().getIdMarketfactBalancePk());
//			marketDetail.setEnteredBalance(mktFact.getAvailableBalance());
//			marketFactBalance.getMarketFacBalances().add(marketDetail);
//			total = total.add(marketDetail.getEnteredBalance());
//		}
//		
//		if(total.compareTo(BigDecimal.ZERO) > 0){
//			marketFactBalance.setBalanceResult(total);
//		}
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		//MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
//		BigDecimal balanceTemporal = BigDecimal.ZERO;
		if(Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance.getMarketFacBalances())){
//			if(Validations.validateIsNotNull(selectedTransfer.getQuantityBalance())){
//				balanceTemporal = selectedTransfer.getQuantityBalance();
//			}			
			selectedTransfer.setQuantityBalance(BigDecimal.ZERO);			
			for( MarketFactDetailHelpTO detail : holderMarketFactBalance.getMarketFacBalances() ){
				if(detail.getIsSelected() && detail.getEnteredBalance()!=null && detail.getEnteredBalance().compareTo(BigDecimal.ZERO) == 1){
					ChangeOwnershipMarketFact marketFactDetail = new ChangeOwnershipMarketFact();					
					marketFactDetail.setMarketDate(detail.getMarketDate());
					marketFactDetail.setMarketPrice(detail.getMarketPrice());
					marketFactDetail.setMarketRate(detail.getMarketRate());
					selectedTransfer.getChangeOwnershipMarketFact().add(marketFactDetail);
					selectedTransfer.setQuantityBalance(selectedTransfer.getQuantityBalance().add(detail.getEnteredBalance()));
					dynamicBalance = (dynamicBalance.subtract(detail.getEnteredBalance()));//.add(balanceTemporal);
					
					marketFactDetail.setMarketBalance(detail.getEnteredBalance());
					
				}
			}
		}
		
		if(holderMarketFactBalance.getIndBlockBalance() != null && holderMarketFactBalance.getIndBlockBalance().equals(ComponentConstant.ONE)){
			JSFUtilities.updateComponent("frmOwnershipExchangeBlockDialog:dtbBlockDetails");
			JSFUtilities.updateComponent("frmOwnershipExchangeBlockDialog:selectedBlockHABDetail");
		}else{
			JSFUtilities.updateComponent("frmOwnershipExchangeAvailDialog:dtbDetails");
			JSFUtilities.updateComponent("frmOwnershipExchangeAvailDialog:selectedHABDetail");
		}
	}
	
	/**
	 * Validate current transfer.
	 *
	 * @param maxBalance the max balance
	 * @return true, if successful
	 */
	public boolean validateCurrentTransfer(BigDecimal maxBalance){
		if(Validations.validateIsNull(currentDetailList)){
			currentDetailList = new ArrayList<ChangeOwnershipTransferTO>(0);
		}else{
			BigDecimal totalEntered = BigDecimal.ZERO;
			for(ChangeOwnershipTransferTO detail : currentDetailList){
				BigDecimal trasnferBalance = detail.getQuantityBalance() == null? BigDecimal.ZERO : detail.getQuantityBalance();
				if(detail.isSelected() && trasnferBalance.compareTo(BigDecimal.ZERO) > 0){
					
					totalEntered = totalEntered.add(trasnferBalance);
					if(totalEntered.compareTo(maxBalance) > 0){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_TOTAL_AMOUNT_EXCEEDED));
						JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
						return false;
					}
					
					detail.setSave(true);
				}else{
					detail.setSave(false);
					//implicit delete if it was unselected
				}
			}

		}
		return true;
	}
	
	/**
	 * Save current transfer.
	 *
	 * @param isTransferAll the is transfer all
	 */
	public void saveCurrentTransfer(boolean isTransferAll){
		BigDecimal maxBlockBalance = selectedHolderAccountBalance.getAvailableBalance();
		selectedHolderAccountBalance.setCopiedAvailableBalance(maxBlockBalance);
		if( isTransferAll || validateCurrentTransfer(maxBlockBalance) ){
			for(ChangeOwnershipTransferTO detail : currentDetailList){
				
				ChangeOwnershipDetail targetChangeOwnership = new ChangeOwnershipDetail();
				targetChangeOwnership.setSecurity(detail.getSecurity());
				targetChangeOwnership.setHolderAccount(detail.getHolderAccount());
				targetChangeOwnership.setParticipant(detail.getParticipant());
				targetChangeOwnership.setChangeOwnershipMarketFacts(detail.getChangeOwnershipMarketFact());
				
				if(detail.isSave()){
					
					/* add source */
					ChangeOwnershipDetail sourceChangeOwnership = new ChangeOwnershipDetail();
					sourceChangeOwnership.setChangeOwnershipMarketFacts(new ArrayList<ChangeOwnershipMarketFact>());
					sourceChangeOwnership.setTargetChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
					sourceChangeOwnership.setChangeOwnershipOperation(changeOwnershipOperation); // necessary for cascade persist
					sourceChangeOwnership.setHolderAccount(changeOwnershipOperation.getSourceHolderAccount());
					sourceChangeOwnership.setSecurity(selectedHolderAccountBalance.getSecurity());
					sourceChangeOwnership.setParticipant(changeOwnershipOperation.getSourceParticipant());
					sourceChangeOwnership.setIndOriginTarget(ComponentConstant.SOURCE);
					
					if(!changeOwnershipOperationTO.getChangeOwnershipDetails().contains(sourceChangeOwnership)){
						changeOwnershipOperationTO.getChangeOwnershipDetails().add(sourceChangeOwnership);
					}else{
						int index = changeOwnershipOperationTO.getChangeOwnershipDetails().indexOf(sourceChangeOwnership);
						sourceChangeOwnership = changeOwnershipOperationTO.getChangeOwnershipDetails().get(index);
					}
					
					BigDecimal trasnferBalance = detail.getQuantityBalance() == null? BigDecimal.ZERO : detail.getQuantityBalance();
					selectedHolderAccountBalance.setCopiedAvailableBalance(selectedHolderAccountBalance.getCopiedAvailableBalance().subtract(trasnferBalance));
					
					/* add target */
					targetChangeOwnership.setChangeOwnershipOperation(changeOwnershipOperation); 
					targetChangeOwnership.setIndOriginTarget(ComponentConstant.TARGET);
					targetChangeOwnership.setAvailableBalance(trasnferBalance);
					targetChangeOwnership.setSourceChangeOwnershipDetail(sourceChangeOwnership);
					
					sourceChangeOwnership.setAvailableBalance(sourceChangeOwnership.getAvailableBalance().add(trasnferBalance));
					sourceChangeOwnership.getTargetChangeOwnershipDetails().add(targetChangeOwnership);
					
					for(ChangeOwnershipMarketFact tgtMarketFact : detail.getChangeOwnershipMarketFact()){
						tgtMarketFact.setChangeOwnershipDetail(targetChangeOwnership);
						ChangeOwnershipMarketFact srcMarketFact = sourceChangeOwnership.containsMarketFact(tgtMarketFact);
						if(srcMarketFact!=null){
							srcMarketFact.setMarketBalance(srcMarketFact.getMarketBalance().add(tgtMarketFact.getMarketBalance()));
						}else{
							sourceChangeOwnership.getChangeOwnershipMarketFacts().add(creatCopyMktFact(
									tgtMarketFact,sourceChangeOwnership));
						}
					}
					
				}	
				if(detail.isDelete()){
					removeHolderAccountBalanceDetail(targetChangeOwnership);
				}
			}
			JSFUtilities.executeJavascriptFunction("PF('ownershipExchangeAvailDialog').hide();");
		}
	}
	
	/**
	 * Save current block transfer.
	 *
	 * @param isTransferAll the is transfer all
	 */
	public void saveCurrentBlockTransfer(boolean isTransferAll){
		BigDecimal maxBlockBalance = selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getActualBlockBalance();
		Integer blockType = selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getBlockRequest().getBlockType();
		Integer blockLevel = selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getBlockLevel();
		if( isTransferAll || validateCurrentTransfer(maxBlockBalance) ){
			for(ChangeOwnershipTransferTO detail : currentDetailList){

				BlockChangeOwnership blockChangeOwnership = new BlockChangeOwnership();
				blockChangeOwnership.setBlockOperation(selectedHolderAccountBlockBalTO.getSelectedBlockOperation());
				blockChangeOwnership.setHolderAccount(detail.getHolderAccount());
				blockChangeOwnership.setParticipant(detail.getParticipant());
				blockChangeOwnership.setChangeOwnershipMarketFacts(detail.getChangeOwnershipMarketFact());
				
				ChangeOwnershipDetail sourceChangeOwnership = new ChangeOwnershipDetail();
				sourceChangeOwnership.setHolderAccount(changeOwnershipOperation.getSourceHolderAccount());
				sourceChangeOwnership.setSecurity(selectedHolderAccountBlockBalTO.getHolderAccountBalance().getSecurity());
				sourceChangeOwnership.setParticipant(selectedHolderAccountBlockBalTO.getHolderAccountBalance().getParticipant());
				
				if(detail.isSave()){
					
					/* add source */
					sourceChangeOwnership.setChangeOwnershipMarketFacts(new ArrayList<ChangeOwnershipMarketFact>());
					sourceChangeOwnership.setBlockChangeOwnerships(new ArrayList<BlockChangeOwnership>());
					sourceChangeOwnership.setChangeOwnershipOperation(changeOwnershipOperation); // necessary for cascade persist
					sourceChangeOwnership.setIndOriginTarget(ComponentConstant.SOURCE);
					
					if(!changeOwnershipOperationTO.getChangeOwnershipDetails().contains(sourceChangeOwnership)){
						changeOwnershipOperationTO.getChangeOwnershipDetails().add(sourceChangeOwnership);
					}else{
						int index = changeOwnershipOperationTO.getChangeOwnershipDetails().indexOf(sourceChangeOwnership);
						sourceChangeOwnership = changeOwnershipOperationTO.getChangeOwnershipDetails().get(index);
					}
					
					
					BigDecimal trasnferBalance = detail.getQuantityBalance() == null? BigDecimal.ZERO : detail.getQuantityBalance();
					BigDecimal actualMaxBalance = selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getCopiedActualBlockBalance();
					BigDecimal newBalance = actualMaxBalance.subtract(trasnferBalance);
					selectedHolderAccountBlockBalTO.getSelectedBlockOperation().setCopiedActualBlockBalance(newBalance);
					if(newBalance.compareTo(BigDecimal.ZERO) <= 0){
						selectedHolderAccountBlockBalTO.getSelectedBlockOperation().setSelected(true);
					}
					
					if(LevelAffectationType.BLOCK.getCode().equals(blockLevel)){
						if(blockType.equals(AffectationType.BAN.getCode())){
							BigDecimal totalTransferedBalance = sourceChangeOwnership.getBanBalance().add(trasnferBalance);
							sourceChangeOwnership.setBanBalance(totalTransferedBalance);
						}else if(blockType.equals(AffectationType.PAWN.getCode())){
							BigDecimal totalTransferedBalance = sourceChangeOwnership.getPawnBalance().add(trasnferBalance);
							sourceChangeOwnership.setPawnBalance(totalTransferedBalance);
						}else if(blockType.equals(AffectationType.OTHERS.getCode())){
							BigDecimal totalTransferedBalance = sourceChangeOwnership.getOtherBlockBalance().add(trasnferBalance);
							sourceChangeOwnership.setOtherBlockBalance(totalTransferedBalance);
						}
					}
					
					sourceChangeOwnership.getBlockChangeOwnerships().add(blockChangeOwnership);
					
					blockChangeOwnership.setChangeOwnershipDetail(sourceChangeOwnership);
					blockChangeOwnership.setBlockQuantity(trasnferBalance);
					blockChangeOwnership.setAffectationType(blockType);
					
					for(ChangeOwnershipMarketFact blkMarketFact : detail.getChangeOwnershipMarketFact()){
						blkMarketFact.setBlockChangeOwnership(blockChangeOwnership);
						if(LevelAffectationType.BLOCK.getCode().equals(blockLevel)){
							ChangeOwnershipMarketFact srcMarketFact = sourceChangeOwnership.containsMarketFact(blkMarketFact);
							if(srcMarketFact!=null){
								srcMarketFact.setMarketBalance(srcMarketFact.getMarketBalance().add(blkMarketFact.getMarketBalance()));
							}else{
								sourceChangeOwnership.getChangeOwnershipMarketFacts().add(creatCopyMktFact(blkMarketFact,sourceChangeOwnership));
							}
						}
					}
					
				}
				if(detail.isDelete()){
					
					// get origin detail
					int index = changeOwnershipOperationTO.getChangeOwnershipDetails().indexOf(sourceChangeOwnership);
					sourceChangeOwnership = changeOwnershipOperationTO.getChangeOwnershipDetails().get(index);
					
					if(sourceChangeOwnership!=null){
						// remove block detail
						removeHolderAccountBalBlockDetail(sourceChangeOwnership,blockChangeOwnership);
						
						// remove all reblock details related to origin detail
						if(blockLevel.equals(LevelAffectationType.BLOCK.getCode())){
							List<BlockChangeOwnership> blockOqnershipsForRemoval = new ArrayList<BlockChangeOwnership>();
							for (BlockChangeOwnership reblockChangeOwnership : sourceChangeOwnership.getBlockChangeOwnerships()){
								if(reblockChangeOwnership.getBlockOperation().getBlockLevel().equals(LevelAffectationType.REBLOCK.getCode())){
									blockOqnershipsForRemoval.add(reblockChangeOwnership);
								}
							}
							
							for (BlockChangeOwnership blockChangeOwnership2 : blockOqnershipsForRemoval) {
								removeHolderAccountBalBlockDetail(sourceChangeOwnership,blockChangeOwnership2);
							}
						}
					}
				}
			
			}
			JSFUtilities.executeJavascriptFunction("PF('ownershipExchangeBlockDialog').hide();");
		}
	}

	/**
	 * Creat copy mkt fact.
	 *
	 * @param mktFact the mkt fact
	 * @param detail the detail
	 * @return the change ownership market fact
	 */
	private ChangeOwnershipMarketFact creatCopyMktFact(
			ChangeOwnershipMarketFact mktFact,ChangeOwnershipDetail detail) {
		ChangeOwnershipMarketFact newMktFact  = new  ChangeOwnershipMarketFact();
		newMktFact.setMarketDate(mktFact.getMarketDate());
		newMktFact.setMarketPrice(mktFact.getMarketPrice());
		newMktFact.setMarketRate(mktFact.getMarketRate());
		newMktFact.setMarketBalance(mktFact.getMarketBalance());
		newMktFact.setChangeOwnershipDetail(detail);
		return newMktFact;
	}

	/**
	 * On change select detail row.
	 *
	 * @param detail the detail
	 */
	public void onChangeSelectDetailRow(ChangeOwnershipTransferTO detail){
		if(detail.isSelected()){ // seleccionado
			
		}
		if(!detail.isSelected()){ // deseleccionado
			if(detail.getQuantityBalance()!=null){
				dynamicBalance  = dynamicBalance.add(detail.getQuantityBalance());
			}
			detail.setQuantityBalance(null);
			detail.setChangeOwnershipMarketFact(new ArrayList<ChangeOwnershipMarketFact>());
		}
	}
	
	
	/**
	 * Checks if is valid avail single.
	 *
	 * @param hab the hab
	 * @return true, if is valid avail single
	 */
	private boolean isValidAvailSingle(HolderAccountBalance hab) {
		if(changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())
				&& (hab.getAccreditationBalance().compareTo(BigDecimal.ZERO) > 0 || hab.getTransitBalance().compareTo(BigDecimal.ZERO) > 0 )){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_HAS_TRANSIT_ACCR));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			return false;
		}
		return true;
	}
	
	/**
	 * Transfer all.
	 */
	public void transferAll(){
		
		selectedHolderAccountBalance = null;
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
		
		for(HolderAccountBalance hab: holderAccountBalancesDataModel.getDataList()){
			selectedHolderAccountBalance = hab;
			currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();
			selectedHolderAccountBalance.setCopiedAvailableBalance(selectedHolderAccountBalance.getAvailableBalance());
			
			// - si es sucesion, y tiene accreditation o transito
			if(!isValidAvailSingle(hab)){
				selectedHolderAccountBalance = null;
				changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
				currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();
				return;
			}
			
			if(selectedHolderAccountBalance.getCopiedAvailableBalance().compareTo(BigDecimal.ZERO) > 0 ){
				
				// - solo va a ser a un solo titular destino.
				for(HolderAccount targetHolderAccount : changeOwnershipOperationTO.getTargetHolderAccounts()){
					
					ChangeOwnershipTransferTO detail = new ChangeOwnershipTransferTO();
					detail.setHolderAccount(targetHolderAccount);
					detail.setParticipant(targetHolderAccount.getParticipant());
					detail.setSecurity(selectedHolderAccountBalance.getSecurity());
					detail.setQuantityBalance(selectedHolderAccountBalance.getCopiedAvailableBalance());
					detail.setSave(true);
					
					List<HolderMarketFactBalance> mktBalances = changeOwnershipServiceFacade.getMarketFactBalances(
							selectedHolderAccountBalance.getParticipant().getIdParticipantPk(),
							selectedHolderAccountBalance.getHolderAccount().getIdHolderAccountPk(),
							selectedHolderAccountBalance.getSecurity().getIdSecurityCodePk());
					
					BigDecimal totalMktBalance = BigDecimal.ZERO;
					for(HolderMarketFactBalance holderMarketFactBalance : mktBalances ){
						ChangeOwnershipMarketFact marketFactDetail = new ChangeOwnershipMarketFact();
						marketFactDetail.setMarketDate(holderMarketFactBalance.getMarketDate());
						marketFactDetail.setMarketPrice(holderMarketFactBalance.getMarketPrice());
						marketFactDetail.setMarketRate(holderMarketFactBalance.getMarketRate());
						marketFactDetail.setMarketBalance(holderMarketFactBalance.getAvailableBalance());
						totalMktBalance = totalMktBalance.add(marketFactDetail.getMarketBalance());
						detail.getChangeOwnershipMarketFact().add(marketFactDetail);				
					}
					
					if(totalMktBalance.compareTo(detail.getQuantityBalance()) != 0){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
								"Inconsistencia entre saldos y hechos de mercado. Valor: "+selectedHolderAccountBalance.getSecurity().getIdSecurityCodePk());
						JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
						changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
						return;
					}
					
					currentDetailList.add(detail);
				}
				
				saveCurrentTransfer(true);
			}
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_CHANGE_OWNERSHIP_ALL_TRANSFERED_OK));
		JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID); 
		allTransfered = true;
		
	}

	/**
	 * Validate block transfer.
	 *
	 * @param blockOp the block op
	 */
	public void validateBlockTransfer(BlockOperation blockOp){
		BigDecimal balanceTotalBlocked = BigDecimal.ZERO;
		if(isValidBlockSingle(blockOp,false)){
			selectedHolderAccountBlockBalTO.setSelectedBlockOperation(blockOp);
			currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();
			dynamicBalance = blockOp.getActualBlockBalance();
							
			for (HolderAccount targetHolderAccount : changeOwnershipOperationTO.getTargetHolderAccounts()){
				
				BlockChangeOwnership blockChangeOwnership = new BlockChangeOwnership();
				blockChangeOwnership.setBlockOperation(selectedHolderAccountBlockBalTO.getSelectedBlockOperation());
				blockChangeOwnership.setHolderAccount(targetHolderAccount);
				blockChangeOwnership.setParticipant(targetHolderAccount.getParticipant());
				
				ChangeOwnershipTransferTO detail = new ChangeOwnershipTransferTO();
				detail.setHolderAccount(targetHolderAccount);
				detail.setParticipant(targetHolderAccount.getParticipant());
				detail.setSecurity(selectedHolderAccountBlockBalTO.getHolderAccountBalance().getSecurity());
				detail.setAffectationType(blockOp.getBlockRequest().getBlockType());
				detail.setIndBlockBalance(ComponentConstant.ONE);
				
				for(ChangeOwnershipDetail souceChangeOwnership : changeOwnershipOperationTO.getChangeOwnershipDetails()){
					blockChangeOwnership = souceChangeOwnership.containsBlockChangeOwnership(blockChangeOwnership);
					if(blockChangeOwnership!=null){
						detail.setQuantityBalance(blockChangeOwnership.getBlockQuantity());
						balanceTotalBlocked = balanceTotalBlocked.add(detail.getQuantityBalance());
						
						detail.setSelected(true);
						detail.setDelete(true);	
					}
				}				
				currentDetailList.add(detail);
			}
			dynamicBalance = dynamicBalance.subtract(balanceTotalBlocked);
			JSFUtilities.executeJavascriptFunction("PF('ownershipExchangeBlockDialog').show();");
			
		}
	}
	
	/**
	 * Checks if is valid block single.
	 *
	 * @param blockOp the block op
	 * @param transferAllFlag the transfer all flag
	 * @return true, if is valid block single
	 */
	private boolean isValidBlockSingle(BlockOperation blockOp, boolean transferAllFlag) {
		
		//validate if blockOperation is in accreditation 
		if(Validations.validateIsNotNull(blockOp.getIndAccreditation()) && 
				blockOp.getIndAccreditation().equals((ComponentConstant.ONE))){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_IN_ACCREDITATION));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
			return false;
		}
		
		//validate if reblock can be added		
		if(!transferAllFlag && blockOp.getBlockLevel().equals(LevelAffectationType.REBLOCK.getCode())){
			boolean canAddReblock = true;
			for(BlockOperation blockOperation : selectedHolderAccountBlockBalTO.getBlockOperationsInView()){
				if(blockOperation.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
					if(!blockOperation.getSelected()){ // is selected means all actual balance was transfered completely.
						canAddReblock = false;
						break;
					}
				}
			}
			if(!canAddReblock){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_UNABLE_ADD_REBLOCK));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Transfer all blocked new.
	 *
	 * @throws ServiceException the service exception
	 */
	public void transferAllBlockedNew() throws ServiceException{		
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
		for(HolderAccountBlockBalTO habTO : holderAccountBalBlockDataModel.getDataList()){
			if(habTO.getHolderAccountBalance().getPawnBalance().longValue() > 0){
				//fillBlockOperationsByAffectation(AffectationType.PAWN.getCode(), habTO);
				if(!fillBlockOperationsByAffectationBoolean(AffectationType.PAWN.getCode(), habTO)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NOT_FOUND_BLOCK_OPERATIONS,
							new Object[]{habTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk()}));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
					currentDetailList = null;	
					habTO.setBlockOperationsInView(null, null);
					return;
				}				
			}
			if(habTO.getHolderAccountBalance().getOtherBlockBalance().longValue() > 0 ){
				//fillBlockOperationsByAffectation(AffectationType.OTHERS.getCode(), habTO);
				if(!fillBlockOperationsByAffectationBoolean(AffectationType.OTHERS.getCode(), habTO)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NOT_FOUND_BLOCK_OPERATIONS,
							new Object[]{habTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk()}));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
					currentDetailList = null;	
					habTO.setBlockOperationsInView(null, null);
					return;
				}
			}
			if(habTO.getHolderAccountBalance().getBanBalance().longValue() > 0 ){
				//fillBlockOperationsByAffectation(AffectationType.BAN.getCode(), habTO);
				if(!fillBlockOperationsByAffectationBoolean(AffectationType.BAN.getCode(), habTO)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NOT_FOUND_BLOCK_OPERATIONS,
							new Object[]{habTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk()}));
					JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
					changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
					currentDetailList = null;	
					habTO.setBlockOperationsInView(null, null);
					return;
				}
			}
			selectedHolderAccountBlockBalTO = habTO;
			for(BlockOperation bo : habTO.getAllBlockOperations()){
				if(!isValidBlockSingle(bo, true)){					
					changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
					currentDetailList = null;				
					habTO.setBlockOperationsInView(null, null);
					return;
				}
				bo.setCopiedActualBlockBalance(bo.getActualBlockBalance());
				selectedHolderAccountBlockBalTO.setSelectedBlockOperation(bo);
				currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();				
				if(bo.getCopiedActualBlockBalance().compareTo(BigDecimal.ZERO) > 0){
					
					for(HolderAccount targetHolderAccount : changeOwnershipOperationTO.getTargetHolderAccounts()){
						
						ChangeOwnershipTransferTO detail = new ChangeOwnershipTransferTO();
						detail.setHolderAccount(targetHolderAccount);
						detail.setParticipant(targetHolderAccount.getParticipant());
						detail.setSecurity(selectedHolderAccountBlockBalTO.getHolderAccountBalance().getSecurity());
						detail.setAffectationType(bo.getBlockRequest().getBlockType());
						detail.setIndBlockBalance(ComponentConstant.ONE);
						detail.setQuantityBalance(bo.getCopiedActualBlockBalance());
						detail.setSave(true);
						
						BlockChangeOwnership blockChangeOwnership = new BlockChangeOwnership();
						blockChangeOwnership.setBlockOperation(selectedHolderAccountBlockBalTO.getSelectedBlockOperation());
						blockChangeOwnership.setHolderAccount(targetHolderAccount);
						blockChangeOwnership.setParticipant(targetHolderAccount.getParticipant());
						
						List<BlockMarketFactOperation> mktBalances = changeOwnershipServiceFacade.getMarketFactBalancesBlocked(
								selectedHolderAccountBlockBalTO.getHolderAccountBalance().getParticipant().getIdParticipantPk(),
								selectedHolderAccountBlockBalTO.getHolderAccountBalance().getHolderAccount().getIdHolderAccountPk(),
								selectedHolderAccountBlockBalTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk(),
								selectedHolderAccountBlockBalTO.getSelectedBlockOperation().getIdBlockOperationPk());
						
						BigDecimal totalMktBalance = BigDecimal.ZERO;
						for(BlockMarketFactOperation blockMarketFactOperation : mktBalances){
							ChangeOwnershipMarketFact marketFactDetail = new ChangeOwnershipMarketFact();
							marketFactDetail.setMarketDate(blockMarketFactOperation.getMarketDate());
							marketFactDetail.setMarketPrice(blockMarketFactOperation.getMarketPrice());
							marketFactDetail.setMarketRate(blockMarketFactOperation.getMarketRate());																					
							if(bo.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())){
								marketFactDetail.setMarketBalance(blockMarketFactOperation.getActualBlockBalance());
							}else if(bo.getBlockRequest().getBlockType().equals(AffectationType.OTHERS.getCode())){
								marketFactDetail.setMarketBalance(blockMarketFactOperation.getActualBlockBalance());
							}else if(bo.getBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())){
								marketFactDetail.setMarketBalance(blockMarketFactOperation.getActualBlockBalance());
							}							
							totalMktBalance = totalMktBalance.add(marketFactDetail.getMarketBalance());
							detail.getChangeOwnershipMarketFact().add(marketFactDetail);
						}						
						if(totalMktBalance.compareTo(detail.getQuantityBalance()) != 0){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
									"Inconsistencia entre saldos y hechos de mercado. Valor: "+selectedHolderAccountBlockBalTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk());
							JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);							
							changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>());
							currentDetailList = null;	
							habTO.setBlockOperationsInView(null, null);
							return;
						}												
						currentDetailList.add(detail);
					}				
				}				
				saveCurrentBlockTransfer(true);
			}
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_CHANGE_OWNERSHIP_ALL_BLOCKED_TRANSFERED_OK));
		JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID); 
		allTransfered = true;
	}
	
	/**
	 * Removes the holder account balance detail.
	 *
	 * @param chod the chod
	 */
	public void removeHolderAccountBalanceDetail(ChangeOwnershipDetail chod){
		boolean deleted = false;
		for(ChangeOwnershipDetail changeOwnershipDetail: changeOwnershipOperationTO.getChangeOwnershipDetails()){
		
			int idx  = changeOwnershipDetail.getTargetChangeOwnershipDetails().indexOf(chod);
			ChangeOwnershipDetail foundTarget = changeOwnershipDetail.getTargetChangeOwnershipDetails().get(idx);
			deleted = changeOwnershipDetail.getTargetChangeOwnershipDetails().remove(foundTarget);
			if(deleted){
				changeOwnershipDetail.setAvailableBalance(changeOwnershipDetail.getAvailableBalance().subtract(foundTarget.getAvailableBalance()));
				
				for(ChangeOwnershipMarketFact tgtMarketFact : foundTarget.getChangeOwnershipMarketFacts()){
					ChangeOwnershipMarketFact srcMarketFact = changeOwnershipDetail.containsMarketFact(tgtMarketFact);
					if(srcMarketFact!=null){
//						changeOwnershipDetail.getChangeOwnershipMarketFacts().remove(srcMarketFact);
					}
				}
				
				if(changeOwnershipDetail.getTargetChangeOwnershipDetails().isEmpty()){
					changeOwnershipOperationTO.getChangeOwnershipDetails().remove(changeOwnershipDetail);
				}
				break;
			}			
		}
		
//		search the correct selected holder account balance
		for(HolderAccountBalance hab: holderAccountBalancesDataModel.getDataList()){
			if(chod.getSecurity().getIdSecurityCodePk().equals(hab.getSecurity().getIdSecurityCodePk())){
				BigDecimal totalBalance = hab.getCopiedAvailableBalance();
				hab.setCopiedAvailableBalance(totalBalance.add(chod.getAvailableBalance()));
			}
		}
		
	}
	
	/**
	 * Removes the holder account bal block detail.
	 *
	 * @param changeOwnershipDetail the change ownership detail
	 * @param bco the bco
	 */
	public void removeHolderAccountBalBlockDetail(ChangeOwnershipDetail changeOwnershipDetail, BlockChangeOwnership bco){
		
		boolean deleted = false;
		BlockChangeOwnership foundTarget = null;
			
		foundTarget = changeOwnershipDetail.containsBlockChangeOwnership(bco);
		//remove blockChangeOwnership
		deleted = changeOwnershipDetail.getBlockChangeOwnerships().remove(foundTarget);
		
		if(deleted){
			
			if(LevelAffectationType.BLOCK.getCode().equals(bco.getBlockOperation().getBlockLevel())){
				if(foundTarget.getAffectationType().equals(AffectationType.BAN.getCode())){
					changeOwnershipDetail.setBanBalance(changeOwnershipDetail.getBanBalance().subtract(foundTarget.getBlockQuantity()));
				}else if(foundTarget.getAffectationType().equals(AffectationType.PAWN.getCode())){
					changeOwnershipDetail.setPawnBalance(changeOwnershipDetail.getPawnBalance().subtract(foundTarget.getBlockQuantity()));
				}else if(foundTarget.getAffectationType().equals(AffectationType.OTHERS.getCode())){
					changeOwnershipDetail.setOtherBlockBalance(changeOwnershipDetail.getOtherBlockBalance().subtract(foundTarget.getBlockQuantity()));
				}
				
				//remove market facts
				for(ChangeOwnershipMarketFact blkMarketFact : foundTarget.getChangeOwnershipMarketFacts()){
					ChangeOwnershipMarketFact srcMarketFact = changeOwnershipDetail.containsMarketFact(blkMarketFact);
					if(srcMarketFact!=null){
						changeOwnershipDetail.getChangeOwnershipMarketFacts().remove(srcMarketFact);
					}
				}
			}
		
			//remove if it is last
			if(changeOwnershipDetail.getBlockChangeOwnerships().size() <= 0){
				changeOwnershipOperationTO.getChangeOwnershipDetails().remove(changeOwnershipDetail);
			}
			
			// add copied balance
			for(HolderAccountBlockBalTO holdeAccountBalanceTO: holderAccountBalBlockDataModel.getDataList()){
				for(BlockOperation blockOperation : holdeAccountBalanceTO.getAllBlockOperations()){
					if(blockOperation.getIdBlockOperationPk().equals(foundTarget.getBlockOperation().getIdBlockOperationPk())){
						BigDecimal totalBalance = blockOperation.getCopiedActualBlockBalance();
						BigDecimal newBalance = totalBalance.add(foundTarget.getBlockQuantity());
						blockOperation.setCopiedActualBlockBalance(newBalance);
						if(newBalance.compareTo(BigDecimal.ZERO) > 0){
							selectedHolderAccountBlockBalTO.getSelectedBlockOperation().setSelected(false);
						}
					}
				}
			}
			
		}			
		
	}
	
	/**
	 * Before validate block transfer.
	 *
	 * @param rowIndex the row index
	 * @param affectationType the affectation type
	 */
	public void beforeValidateBlockTransfer(Integer rowIndex,Integer affectationType){		
		try {
			HolderAccountBlockBalTO holderAccountBlockTO = holderAccountBalBlockDataModel.getDataList().get(rowIndex);
			fillBlockOperationsByAffectation(affectationType,holderAccountBlockTO);
			selectedHolderAccountBlockBalTO = holderAccountBlockTO;			
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Fill block operations by affectation.
	 *
	 * @param affectationType the affectation type
	 * @param holderAccountBlockTO the holder account block to
	 * @throws ServiceException the service exception
	 */
	private void fillBlockOperationsByAffectation(Integer affectationType, HolderAccountBlockBalTO holderAccountBlockTO) throws ServiceException {			
		if(!holderAccountBlockTO.existsAffectationTypeList(affectationType)){
			BlockOperationTO blockOperationTO = new BlockOperationTO();
			blockOperationTO.setIdHolderAccountPk(holderAccountBlockTO.getHolderAccountBalance().getHolderAccount().getIdHolderAccountPk());
			blockOperationTO.setIdParticipantPk(holderAccountBlockTO.getHolderAccountBalance().getParticipant().getIdParticipantPk());
			blockOperationTO.setSecurityCodePk(holderAccountBlockTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk());
			blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
			blockOperationTO.setIdLevelAffectation(LevelAffectationType.BLOCK.getCode());
			blockOperationTO.setIdTypeAffectation(affectationType);
			List<BlockOperation> boList = changeOwnershipServiceFacade.findBlockOperationsByFilters(blockOperationTO);
			holderAccountBlockTO.setBlockOperationsInView(boList, affectationType);
			if(Validations.validateListIsNullOrEmpty(boList)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NOT_FOUND_BLOCK_OPERATIONS,
						new Object[]{holderAccountBlockTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk()}));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				return;
			}			
		}else{
			holderAccountBlockTO.swapBlockOperationsInView(affectationType);
		}
	}
	
	/**
	 * Fill block operations by affectation boolean.
	 *
	 * @param affectationType the affectation type
	 * @param holderAccountBlockTO the holder account block to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	private boolean fillBlockOperationsByAffectationBoolean(Integer affectationType, HolderAccountBlockBalTO holderAccountBlockTO) throws ServiceException {			
		if(!holderAccountBlockTO.existsAffectationTypeList(affectationType)){
			BlockOperationTO blockOperationTO = new BlockOperationTO();
			blockOperationTO.setIdHolderAccountPk(holderAccountBlockTO.getHolderAccountBalance().getHolderAccount().getIdHolderAccountPk());
			blockOperationTO.setIdParticipantPk(holderAccountBlockTO.getHolderAccountBalance().getParticipant().getIdParticipantPk());
			blockOperationTO.setSecurityCodePk(holderAccountBlockTO.getHolderAccountBalance().getSecurity().getIdSecurityCodePk());
			blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
			blockOperationTO.setIdTypeAffectation(affectationType);
			List<BlockOperation> boList = changeOwnershipServiceFacade.findBlockOperationsByFilters(blockOperationTO);
			holderAccountBlockTO.setBlockOperationsInView(boList, affectationType);
			if(Validations.validateListIsNullOrEmpty(boList)){				
				return false;
			}			
		}else{
			holderAccountBlockTO.swapBlockOperationsInView(affectationType);
		}
		return true;
	}
	
	/**
	 * Before save change owner ship operation handler.
	 *
	 * @throws ServiceException the service exception
	 */
	public void beforeSaveChangeOwnerShipOperationHandler() throws ServiceException{
		hideDialogues();
		if(changeOwnershipOperationTO.getChangeOwnershipDetails()!=null &&
				changeOwnershipOperationTO.getChangeOwnershipDetails().size() > 0 ){
			
			// validar que se haya cargado un archivo
			if(Validations.validateListIsNullOrEmpty(changeOwnershipOperation.getChangeOwnershipFiles())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_EXCHANGESECURITIES_NO_ATTACHMENTS_FOUND));
				JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
				return;						
			}
			
			if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				if(changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode()) ){
					for(HolderAccountBalance balance: holderAccountBalancesDataModel.getDataList()){
						if(balance.getCopiedAvailableBalance().longValue() > 0){ // this should be zero when all balance were transfer
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_REQUIRES_TOTAL, 
									new Object[]{ChangeOwnershipMotiveType.SUCESION.getValue(),
													ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getValue()}));
							JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID); 
							return;
						}
					}
				}
				
			}else if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){

				if(changeOwnershipOperation.getMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode())){
					for(HolderAccountBlockBalTO balance: holderAccountBalBlockDataModel.getDataList()){
						for(BlockOperation blockOperation : balance.getAllBlockOperations()){
							if(blockOperation.getCopiedActualBlockBalance().longValue() > 0){ // this should be zero when all balance were transfer
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
										PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_REQUIRES_TOTAL_BLOCK, 
										new Object[]{ChangeOwnershipMotiveType.SUCESION.getValue(),
														ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getValue()}));
								JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID); 
								return;
							}
						}
					}
				}
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EXCHANGESECURITIES_REGISTER_ALL));
			JSFUtilities.showComponent(CONFIRMATION_DIALOG_ID); 
		} else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_NO_DETAILS_FOUND));
			JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
		}
	}
	
	/**
	 * Save change owner ship operation handler.
	 */
	@LoggerAuditWeb
	public void saveChangeOwnerShipOperationHandler(){
		hideDialogues();
		try {
			BusinessProcess businessProcess = new BusinessProcess();
			
			CustodyOperation custodyOperation = new CustodyOperation();
			
			custodyOperation.setOperationType(ChangeOwnershipMotiveType.get(changeOwnershipOperation.getMotive()).getParameterOperationType());
			custodyOperation.setRegistryDate(CommonsUtilities.currentDate());
			custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
			custodyOperation.setState(ChangeOwnershipStatusType.REGISTRADO.getCode());
			custodyOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			
			changeOwnershipOperation.setCustodyOperation(custodyOperation);	
			changeOwnershipOperation.setState(ChangeOwnershipStatusType.REGISTRADO.getCode());
			changeOwnershipOperation.setIndLastRequest(0);
			changeOwnershipOperation.setChangeOwnershipDetails(changeOwnershipOperationTO.getChangeOwnershipDetails());
			
			if(changeOwnershipOperation.getRegularizationIndicator()){
				changeOwnershipOperation.setProcessingType(ChangeOwnershipProcessingTypeType.PENDING_BALANCE_TRANSFER.getCode());
			}else{
				changeOwnershipOperation.setProcessingType(ChangeOwnershipProcessingTypeType.NORMAL_OPERATION.getCode());
			}
			
			if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				changeOwnershipOperation.setIndExchangeSecurities(BooleanType.YES.getCode());
				changeOwnershipServiceFacade.saveAvailChangeOwnerShipServiceFacade(changeOwnershipOperation,idepositarySetup.getIndMarketFact());
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_REGISTER.getCode());
			}else if(changeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
				changeOwnershipServiceFacade.saveBlockChangeOwnerShipServiceFacade(changeOwnershipOperation);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_REGISTER.getCode());
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
					PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EXCHANGESECURITIES_REGISTER_OK, 
							new Object[]{changeOwnershipOperation.getCustodyOperation().getOperationNumber().toString()}));
			JSFUtilities.showComponent(CONFIRMATION_DIALOG_ID_OK); 
			
			// INICIO ENVIO DE NOTIFICACIONES
			Object[] parameters = {	changeOwnershipOperation.getCustodyOperation().getOperationNumber().toString() };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
//	ChangeOwnershipActionTO actions;
	
	/**
 * Before confirm change ownership handler.
 *
 * @return the string
 * @throws ServiceException the service exception
 */
public String beforeConfirmChangeOwnershipHandler() throws ServiceException{
		
		
		String action = null;
		
		if(Validations.validateIsNull(selectedChangeOwnershipDetailTO)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
    		return null;
		}
		
		if (!(selectedChangeOwnershipDetailTO.getIdStatus()
				.equals(ChangeOwnershipStatusType.REGISTRADO.getCode()))) {

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_NOT_REGISTERED));

			JSFUtilities.showComponent("cnfMsgCustomValidation");

		}else{
			
//			if(selectedChangeOwnershipDetailTO.getIdBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
//				if(selectedChangeOwnershipDetailTO.getIdMotive().equals(ChangeOwnershipMotiveType.SUCESION.getCode()) && 
//						Validations.validateIsNull(selectedChangeOwnershipDetailTO.getIdChangeOwnershipRefPk())){
//					
//					actions = new ChangeOwnershipActionTO();
//					// si el motivo es sucesion, la cuenta origen no puede ser una mancomunada o copropiedad.
//
//					List<HolderAccountTO> allAccounts = new ArrayList<HolderAccountTO>();
//					
//					List<HolderAccountDetail> details = selectedChangeOwnershipDetailTO.getSourceHolderAccount().getHolderAccountDetails();
//					Map<String,Object> filters = new HashMap<String, Object>();
//					for(HolderAccountDetail detail : details){
//						filters.put("idHolderPks", detail.getHolder().getIdHolderPk());
//						actions.setHolderToUpdate(detail.getHolder());
//					}
//					allAccounts = changeOwnershipServiceFacade.countAllAccounts(filters);
//					
//					boolean hasOwnershipAccounts = countOwnershipAccounts(allAccounts);
//					
//					if(!hasOwnershipAccounts){
//						if(allAccounts.size()==1){
//							actions.setChangeHolderStateToTransfered(true);
//						}else{
//							actions.setChangeHolderStateToBlocked(true);
//						}
//					}
//					
//					actions.setHolderAccountToUpdate(selectedChangeOwnershipDetailTO.getSourceHolderAccount());
//					actions.setChangeAccountStateToBlocked(true);
//
//				}
//			}
			
			obtainSelectedChangeOwnershipOperation(selectedChangeOwnershipDetailTO.getIdChangeOwnershipPk());
			
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
			action="viewExchangeSecurities";
		}
		return action;
	}

	/**
	 * Count ownership accounts.
	 *
	 * @param allAccounts the all accounts
	 * @return true, if successful
	 */
	private boolean countOwnershipAccounts(List<HolderAccountTO> allAccounts) {
		for(HolderAccountTO holderAccountTO : allAccounts){
			if(holderAccountTO.getHolderAccountType().equals(HolderAccountType.OWNERSHIP.getCode())){
				return true;
			}
		}
		return false;
	}

	/**
	 * Before reject change ownership handler.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String beforeRejectChangeOwnershipHandler() throws ServiceException{
		String action = null;
		
		if(Validations.validateIsNull(selectedChangeOwnershipDetailTO)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfMsgCustomValidation");
    		return null;
		}
		
		if (!(selectedChangeOwnershipDetailTO.getIdStatus()
				.equals(ChangeOwnershipStatusType.REGISTRADO.getCode()))) {

//			if (selectedChangeOwnershipDetailTO.getIdStatus().equals(
//					ChangeOwnershipStatusType.CONFIRMADO.getCode())) {
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_CONFIRM_INVALID,new Object[]{selectedChangeOwnershipDetailTO.getOperationNumber()}));
//			}
//			if (selectedChangeOwnershipDetailTO.getIdStatus().equals(
//					ChangeOwnershipStatusType.RECHAZADO.getCode())) {
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_REJECT_INVALID,new Object[]{selectedChangeOwnershipDetailTO.getOperationNumber()}));
//			}
//			if (selectedChangeOwnershipDetailTO.getIdStatus().equals(
//					ChangeOwnershipStatusType.ANULADO.getCode())) {
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//						PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_ANNULAR_INVALID,new Object[]{selectedChangeOwnershipDetailTO.getOperationNumber()}));
//			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_NOT_REGISTERED));

			JSFUtilities.showComponent("cnfMsgCustomValidation");

		}else{
			
			obtainSelectedChangeOwnershipOperation(selectedChangeOwnershipDetailTO.getIdChangeOwnershipPk());
			
			setViewOperationType(ViewOperationsType.REJECT.getCode());
			action="viewExchangeSecurities";
		}
		return action;
	}
	
	/**
	 * Details change ownership detail hanlder.
	 *
	 * @param idChangeOwnershipPk the id change ownership pk
	 * @return the string
	 */
	public String detailsChangeOwnershipDetailHanlder(Long idChangeOwnershipPk){
		
		try {
			
			obtainSelectedChangeOwnershipOperation(idChangeOwnershipPk);
			
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		return "viewExchangeSecurities";
		
	}
	
	/**
	 * Obtain selected change ownership operation.
	 *
	 * @param idChangeOwnershipPk the id change ownership pk
	 * @throws ServiceException the service exception
	 */
	private void obtainSelectedChangeOwnershipOperation(Long idChangeOwnershipPk) throws ServiceException {
		ChangeOwnershipDetailTO selectedDetail = new ChangeOwnershipDetailTO();
		selectedDetail.setIdChangeOwnershipPk(idChangeOwnershipPk);
		hasOtherMotive = false;
		hasMotiveType = false;
		selectedChangeOwnershipOperation = changeOwnershipServiceFacade.getChangeOwnershipOperationByFilters(selectedDetail);
		
		Map<String,Object> filter = new HashMap<String, Object>();
		filter.put("idChangeOwnershipPk", selectedChangeOwnershipOperation.getIdChangeOwnershipPk());
		selectedChangeOwnershipOperation.setChangeOwnershipFiles(changeOwnershipServiceFacade.getChangeOwnershipFileInfo(filter));
		
		ParameterTable objMotive = mapParameters.get(selectedChangeOwnershipOperation.getMotive());
		selectedChangeOwnershipOperation.setMotiveDescription(objMotive == null ? null : objMotive.getParameterName());
		
		ParameterTable objBalance = mapParameters.get(selectedChangeOwnershipOperation.getBalanceType());
		selectedChangeOwnershipOperation.setBalanceTypeDescription(objBalance == null ? null : objBalance.getParameterName());
		
		if(selectedChangeOwnershipOperation.getState().equals(ChangeOwnershipStatusType.RECHAZADO.getCode())){
			Integer rejMotive = selectedChangeOwnershipOperation.getCustodyOperation().getRejectMotive();
			if(rejMotive!=null){
				hasMotiveType = true;
				ParameterTable objRejMotive = mapParameters.get(selectedChangeOwnershipOperation.getCustodyOperation().getRejectMotive());
				selectedChangeOwnershipOperation.setRejectMotiveDescription(objRejMotive == null ? null : objRejMotive.getParameterName());
				
				if(rejMotive.equals(ChangeOwnershipRejectMotiveType.OTHER_MOTIVE_REJECT.getCode())){
					hasOtherMotive = true;
				}
				
			}
		}
//		if(Validations.validateIsNotNull(selectedChangeOwnershipDetailTO.getIdChangeOwnershipRefPk())){
//			selectedChangeOwnershipOperation.setRegularizationIndicator(true);
//		}
		
	}
	
	/**
	 * Gets the streamed content change ownership.
	 *
	 * @param changeOwnershipFile the change ownership file
	 * @return the streamed content change ownership
	 */
	public StreamedContent getStreamedContentChangeOwnership(ChangeOwnershipFile changeOwnershipFile){
		StreamedContent streamedContent = null;
		try {
			Map<String, Object> filter = new HashMap<String, Object>();
			filter.put("idChangeOwnershipFilePk", changeOwnershipFile.getIdChangeOwnershipFilePk());
			byte[] content = changeOwnershipServiceFacade.getChangeOwnershipFileContent(filter);
			
			streamedContent = getStreamedContentFromFile(content, null, changeOwnershipFile.getFileName());

		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		} catch (IOException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContent;
	}
	
	/**
	 * Begin confirm change ownership handler.
	 */
	public void beginConfirmChangeOwnershipHandler(){
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
				PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EXCHANGESECURITIES_CONFIRM_CONF,
						new Object[]{selectedChangeOwnershipOperation.getCustodyOperation().getOperationNumber()}));

		JSFUtilities.executeJavascriptFunction("PF('transferOwnershipActionCnfWidget').show();");
		
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmChangeOwnershipRejectMotive:dlgMotive");
		hideDialogues();
	}
	
	/**
	 * Accept reject motive.
	 */
	public void acceptRejectMotive(){
		JSFUtilities.setValidViewComponentAndChildrens("frmChangeOwnershipRejectMotive");
		
		int countValidationRequiredErrors = 0;
    	
    	if(Validations.validateIsNullOrNotPositive(selectedChangeOwnershipOperation.getCustodyOperation().getRejectMotive())){
    		JSFUtilities.addContextMessage("frmChangeOwnershipRejectMotive:cboMotive",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CUSTODY_OPERATION_REJECT_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CUSTODY_OPERATION_REJECT_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	if(showMotiveText &&
    			Validations.validateIsNullOrEmpty(selectedChangeOwnershipOperation.getCustodyOperation().getRejectMotiveOther())){
    		JSFUtilities.addContextMessage("frmChangeOwnershipRejectMotive:txtOtherMotiveReject",
    				FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CUSTODY_OPERATION_REJECT_OTHER_MOTIVE), 
    				PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CUSTODY_OPERATION_REJECT_OTHER_MOTIVE));
    		countValidationRequiredErrors++;
    	}
    	
    	
    	if(countValidationRequiredErrors > 0){
    		showMessageOnDialog(
    				null,
    				null,
    				PropertiesConstants.MESSAGE_DATA_REQUIRED,							
    				null);
    		JSFUtilities.showComponent("cnfGeneralMsgBusinessValidation");
    		return;
    	}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT),
				PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EXCHANGESECURITIES_REJECT_CONF,
				new Object[]{selectedChangeOwnershipOperation.getCustodyOperation().getOperationNumber()}));
		
		JSFUtilities.executeJavascriptFunction("PF('dlgWdgtMotive').show();");
		JSFUtilities.executeJavascriptFunction("PF('transferOwnershipActionCnfWidget').show();");
		
	}
	
	/**
	 * Begin reject change ownership handler.
	 *
	 * @throws ServiceException the service exception
	 */
	public void beginRejectChangeOwnershipHandler() throws ServiceException{
		
		hideDialogues();
		
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.MASTER_TABLE_CHANGE_OWNERSHIP_REJECT_MOTIVE_TYPE.getCode());
		lstRejectMotives = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		selectedChangeOwnershipOperation.getCustodyOperation().setRejectMotive(null);
		selectedChangeOwnershipOperation.getCustodyOperation().setRejectMotiveOther(null);
		
		showMotiveText = false;
		
		JSFUtilities.executeJavascriptFunction("PF('dlgWdgtMotive').show();");
	}
	
	/**
	 * Execute action change owner ship handler.
	 */
	@LoggerAuditWeb
	public void executeActionChangeOwnerShipHandler() {
		hideDialogues();
		try {
			
			BusinessProcess businessProcess = new BusinessProcess();
			
			if(selectedChangeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
					changeOwnershipServiceFacade.confirmAvailableOwnershipChange(selectedChangeOwnershipOperation,idepositarySetup.getIndMarketFact());
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode());
					
				}else if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
					changeOwnershipServiceFacade.rejectAvailableOwnershipChange(selectedChangeOwnershipOperation,idepositarySetup.getIndMarketFact());
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_BALANCE_HOLDER_OWNER_CHANGE_REJECT.getCode());
					
				}
			}else if(selectedChangeOwnershipOperation.getBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
				if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
					changeOwnershipServiceFacade.confirmBlockedOwnershipChange(selectedChangeOwnershipOperation);
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_CONFIRM.getCode());

				}else if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
					changeOwnershipServiceFacade.rejectBlockedOwnershipChange(selectedChangeOwnershipOperation);
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_BALANCE_HOLDER_OWNER_CHANGE_REJECT.getCode());
					
				}
			}

			if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EXCHANGESECURITIES_CONFIRM_OK,
								new Object[]{selectedChangeOwnershipOperation.getCustodyOperation().getOperationNumber().toString()}));
				JSFUtilities.executeJavascriptFunction("PF('transferOwnershipActionCnfOkWidget').show();");
			}
			if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_EXCHANGESECURITIES_REJECT_OK,
								new Object[]{selectedChangeOwnershipOperation.getCustodyOperation().getOperationNumber().toString()}));
				JSFUtilities.executeJavascriptFunction("PF('transferOwnershipActionCnfOkWidget').show();");
			}
			
			// INICIO ENVIO DE NOTIFICACIONES
			Object[] parameters = {	selectedChangeOwnershipOperation.getCustodyOperation().getOperationNumber().toString() };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES
			
			selectedChangeOwnershipDetailTO = null;			
			searchChangeOwnershipOperationsByFilters();
			
		} catch (Exception e) {
			if (e instanceof ServiceException) {
				ServiceException se= (ServiceException) e;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(),se.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}
		} 
		
	}
	
	/**
	 * On change reject motive.
	 */
	public void onChangeRejectMotive(){
		JSFUtilities.setValidViewComponentAndChildrens("frmMotiveRejectIssRequest");
		if(ChangeOwnershipRejectMotiveType.OTHER_MOTIVE_REJECT.getCode().equals(selectedChangeOwnershipOperation.getCustodyOperation().getRejectMotive())){
			this.setShowMotiveText(Boolean.TRUE);
			//this.setIndDisableAcceptMotiveReject(Boolean.TRUE);
		}else{
			//this.setIndDisableAcceptMotiveReject(Boolean.FALSE);
			this.setShowMotiveText(Boolean.FALSE);
			
		}
		selectedChangeOwnershipOperation.getCustodyOperation().setRejectMotiveOther(null);	
	}
	
//	public void onBlurOtherMotiveReject(){
//		if(Validations.validateIsNotNullAndNotEmpty(
//				selectedChangeOwnershipOperation.getCustodyOperation().getRejectMotiveOther())){
//			setIndDisableAcceptMotiveReject(Boolean.FALSE);
//		} else {
//			setIndDisableAcceptMotiveReject(Boolean.TRUE); 
//		}
//	}
//	
	/**
 * views.
 */
	public void hideDialogues() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent(GENERIC_WARNING_DIALOG_ID);
		JSFUtilities.hideComponent(CONFIRMATION_DIALOG_ID);	
//		JSFUtilities.hideComponent(CONFIRMATION_DIALOG_BACK);
//		JSFUtilities.hideComponent(CONFIRMATION_DIALOG_CLEAR);
	}

	
	/**
	 *  
	 *  search 
	 *  
	 *  */
	
	public void cleanAll(){
		changeOwnershipOperationTO = new ChangeOwnershipOperationTO();
		setChangeOwnershipDetailsDataModel(null);
		//JSFUtilities.setValidViewComponentAndChildrens(SEARCH_FORM_ID);
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Search change ownership operations by filters.
	 */
	public void searchChangeOwnershipOperationsByFilters(){
		try{
			
			firsRegisterDataTable = 0;
			selectedChangeOwnershipOperation = null;
			selectedChangeOwnershipDetailTO = null;
			List<ChangeOwnershipDetailTO> changeOwnershipDetailTOs = new ArrayList<ChangeOwnershipDetailTO>();
			if(changeOwnershipOperationTO.getIdBalanceType().equals(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode())){
				// issue 1379: indicador para diferenciar las solicitudes: cambio de titularidad de las Trasnferencias por intercambio de valor
				changeOwnershipOperationTO.setSearchByExchangeSecurities(true);
				changeOwnershipDetailTOs = changeOwnershipServiceFacade.searchAvailChangeOwnershipByFilters(changeOwnershipOperationTO);
			}else if(changeOwnershipOperationTO.getIdBalanceType().equals(ChangeOwnershipBalanceTypeType.BLOCKED_BALANCE.getCode())){
				changeOwnershipDetailTOs = changeOwnershipServiceFacade.searchBlockedChangeOwnershipByFilters(changeOwnershipOperationTO);
			}
			
			changeOwnershipDetailsDataModel = new GenericDataModel<ChangeOwnershipDetailTO>(changeOwnershipDetailTOs);
			
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Search change ownerships for regularization.
	 *
	 * @return the string
	 */
	public String searchChangeOwnershipsForRegularization(){
		try {			
			firsRegisterDataTable = 0;
			selectedChangeOwnershipOperation = null;
			selectedChangeOwnershipDetailTO = null;
			
			ChangeOwnershipOperationTO changeOwnershipOperationTO = new ChangeOwnershipOperationTO();			
			changeOwnershipOperationTO.setIdBalanceType(ChangeOwnershipBalanceTypeType.AVAILABLE_BALANCE.getCode());
			changeOwnershipOperationTO.setIdReasonType(ChangeOwnershipMotiveType.SUCESION.getCode());
			changeOwnershipOperationTO.setIdStatus(ChangeOwnershipStatusType.CONFIRMADO.getCode());
			changeOwnershipOperationTO.setInitialDate(null);
			changeOwnershipOperationTO.setFinalDate(getTomorrowSystemtDate());
			//changeOwnershipOperationTO.setIdHolderStatus(HolderStateType.TRANSFERED.getCode());
			List<Integer> lstStatus = new ArrayList<Integer>();
			lstStatus.add(HolderStateType.TRANSFERED.getCode());
			lstStatus.add(HolderStateType.BLOCKED.getCode());
			changeOwnershipOperationTO.setLstStatus(lstStatus);
			//changeOwnershipOperationTO.setNeedBalancesCheck(true);
			changeOwnershipOperationTO.setRegularizationCheck(true);			
			List<ChangeOwnershipDetailTO> changeOwnershipDetailTOs = changeOwnershipServiceFacade.searchAvailChangeOwnershipByFilters(changeOwnershipOperationTO);			
			changeOwnershipRegularizationDataModel = new GenericDataModel<ChangeOwnershipDetailTO>(changeOwnershipDetailTOs);			
			//setValidButtonsListener();			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
		return "searchRegularizationChangeOwnership";
				
	}
	
	
	/**
	 * Sets the valid buttons listener.
	 */
	public void setValidButtonsListener(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load regularization information.
	 *
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String loadRegularizationInformation() throws ServiceException{
		
		if(Validations.validateIsNull(selectedChangeOwnershipForRegTO)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showComponent("cnfGeneralMsgBusinessValidation");
    		return null;
		}
		
		changeOwnershipOperationTO = new ChangeOwnershipOperationTO();
		changeOwnershipOperationTO.setSourceHolderAccounts(new ArrayList<HolderAccount>());
		
		ChangeOwnershipDetailTO selectedDetail = new ChangeOwnershipDetailTO();
		selectedDetail.setIdChangeOwnershipPk(selectedChangeOwnershipForRegTO.getIdChangeOwnershipPk());
		changeOwnershipOperation = changeOwnershipServiceFacade.
				getChangeOwnershipOperationByFilters(selectedDetail);
		

		Map<String,Object> filter = new HashMap<String, Object>();
		filter.put("idChangeOwnershipPk", changeOwnershipOperation.getIdChangeOwnershipPk());
		changeOwnershipOperation.setChangeOwnershipFiles(changeOwnershipServiceFacade.getChangeOwnershipFileInfo(filter));
		
		
		for(ChangeOwnershipDetail detail : changeOwnershipOperation.getChangeOwnershipDetails()){
			changeOwnershipOperation.setSourceHolderAccount(detail.getHolderAccount());
			if(!changeOwnershipOperationTO.getSourceHolderAccounts().contains(detail.getHolderAccount())){
				changeOwnershipOperationTO.getSourceHolderAccounts().add(detail.getHolderAccount());
			}
			for(ChangeOwnershipDetail detail2 : detail.getTargetChangeOwnershipDetails()){
				if(!changeOwnershipOperationTO.getTargetHolderAccounts().contains(detail2.getHolderAccount())){
					changeOwnershipOperationTO.getTargetHolderAccounts().add(detail2.getHolderAccount());
				}
			}
		}
		changeOwnershipOperation.setRegularizationIndicator(true);
		changeOwnershipOperation.setChangeOwnershipDetails(null);
		changeOwnershipOperation.setChangeOwnershipOperationRef(new ChangeOwnershipOperation());
		changeOwnershipOperation.getChangeOwnershipOperationRef().setIdChangeOwnershipPk(
				changeOwnershipOperation.getIdChangeOwnershipPk());
		changeOwnershipOperation.setIdChangeOwnershipPk(null);

		selectedChangeOwnershipForRegTO = null;
		
		searchHolderAccountBalances();
		
		return "registerChangeOwnership";
	}
	
	/**
	 * Removes the file attached.
	 *
	 * @param cof the cof
	 */
	public void removeFileAttached(ChangeOwnershipFile cof) {
		changeOwnershipOperation.getChangeOwnershipFiles().remove(cof);
		selectedChangeOwnershipFileDisplay=null;
	}
	
	/**
	 * Getters and Setters.
	 *
	 * @return the participant list
	 */
	

	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the change ownership operation.
	 *
	 * @return the change ownership operation
	 */
	public ChangeOwnershipOperation getChangeOwnershipOperation() {
		return changeOwnershipOperation;
	}

	/**
	 * Sets the change ownership operation.
	 *
	 * @param changeOwnershipOperation the new change ownership operation
	 */
	public void setChangeOwnershipOperation(
			ChangeOwnershipOperation changeOwnershipOperation) {
		this.changeOwnershipOperation = changeOwnershipOperation;
	}

	/**
	 * Gets the change ownership operation to.
	 *
	 * @return the change ownership operation to
	 */
	public ChangeOwnershipOperationTO getChangeOwnershipOperationTO() {
		return changeOwnershipOperationTO;
	}

	/**
	 * Sets the change ownership operation to.
	 *
	 * @param changeOwnershipOperationTO the new change ownership operation to
	 */
	public void setChangeOwnershipOperationTO(ChangeOwnershipOperationTO changeOwnershipOperationTO) {
		this.changeOwnershipOperationTO = changeOwnershipOperationTO;
	}
	
	/**
	 * Gets the holder account balances data model.
	 *
	 * @return the holder account balances data model
	 */
	public GenericDataModel<HolderAccountBalance> getHolderAccountBalancesDataModel() {
		return holderAccountBalancesDataModel;
	}

	/**
	 * Sets the holder account balances data model.
	 *
	 * @param holderAccountBalancesDataModel the new holder account balances data model
	 */
	public void setHolderAccountBalancesDataModel(
			GenericDataModel<HolderAccountBalance> holderAccountBalancesDataModel) {
		this.holderAccountBalancesDataModel = holderAccountBalancesDataModel;
	}

	/**
	 * Gets the selected holder account balance.
	 *
	 * @return the selected holder account balance
	 */
	public HolderAccountBalance getSelectedHolderAccountBalance() {
		return selectedHolderAccountBalance;
	}

	/**
	 * Sets the selected holder account balance.
	 *
	 * @param selectedHolderAccountBalance the new selected holder account balance
	 */
	public void setSelectedHolderAccountBalance(
			HolderAccountBalance selectedHolderAccountBalance) {
		this.selectedHolderAccountBalance = selectedHolderAccountBalance;
	}

	/**
	 * Gets the selected change ownership operation.
	 *
	 * @return the selected change ownership operation
	 */
	public ChangeOwnershipOperation getSelectedChangeOwnershipOperation() {
		return selectedChangeOwnershipOperation;
	}

	/**
	 * Sets the selected change ownership operation.
	 *
	 * @param selectedChangeOwnershipOperation the new selected change ownership operation
	 */
	public void setSelectedChangeOwnershipOperation(
			ChangeOwnershipOperation selectedChangeOwnershipOperation) {
		this.selectedChangeOwnershipOperation = selectedChangeOwnershipOperation;
	}

	/**
	 * Checks if is all transfered.
	 *
	 * @return true, if is all transfered
	 */
	public boolean isAllTransfered() {
		return allTransfered;
	}

	/**
	 * Sets the all transfered.
	 *
	 * @param allTransfered the new all transfered
	 */
	public void setAllTransfered(boolean allTransfered) {
		this.allTransfered = allTransfered;
	}

	/**
	 * Gets the change ownership details data model.
	 *
	 * @return the change ownership details data model
	 */
	public GenericDataModel<ChangeOwnershipDetailTO> getChangeOwnershipDetailsDataModel() {
		return changeOwnershipDetailsDataModel;
	}

	/**
	 * Sets the change ownership details data model.
	 *
	 * @param changeOwnershipDetailsDataModel the new change ownership details data model
	 */
	public void setChangeOwnershipDetailsDataModel(
			GenericDataModel<ChangeOwnershipDetailTO> changeOwnershipDetailsDataModel) {
		this.changeOwnershipDetailsDataModel = changeOwnershipDetailsDataModel;
	}

	/**
	 * Gets the lst balance types.
	 *
	 * @return the lst balance types
	 */
	public List<ParameterTable> getLstBalanceTypes() {
		return lstBalanceTypes;
	}

	/**
	 * Sets the lst balance types.
	 *
	 * @param lstBalanceTypes the new lst balance types
	 */
	public void setLstBalanceTypes(List<ParameterTable> lstBalanceTypes) {
		this.lstBalanceTypes = lstBalanceTypes;
	}

	/**
	 * Gets the lst ownership exchange reason types.
	 *
	 * @return the lst ownership exchange reason types
	 */
	public List<ParameterTable> getLstOwnershipExchangeReasonTypes() {
		return lstOwnershipExchangeReasonTypes;
	}

	/**
	 * Sets the lst ownership exchange reason types.
	 *
	 * @param lstOwnershipExchangeReasonTypes the new lst ownership exchange reason types
	 */
	public void setLstOwnershipExchangeReasonTypes(
			List<ParameterTable> lstOwnershipExchangeReasonTypes) {
		this.lstOwnershipExchangeReasonTypes = lstOwnershipExchangeReasonTypes;
	}

	/**
	 * Gets the lst ownership exchange status types.
	 *
	 * @return the lst ownership exchange status types
	 */
	public List<ParameterTable> getLstOwnershipExchangeStatusTypes() {
		return lstOwnershipExchangeStatusTypes;
	}

	/**
	 * Sets the lst ownership exchange status types.
	 *
	 * @param lstOwnershipExchangeStatusTypes the new lst ownership exchange status types
	 */
	public void setLstOwnershipExchangeStatusTypes(
			List<ParameterTable> lstOwnershipExchangeStatusTypes) {
		this.lstOwnershipExchangeStatusTypes = lstOwnershipExchangeStatusTypes;
	}

	/**
	 * Gets the lst ownership exchange proc type.
	 *
	 * @return the lst ownership exchange proc type
	 */
	public List<ParameterTable> getLstOwnershipExchangeProcType() {
		return lstOwnershipExchangeProcType;
	}

	/**
	 * Sets the lst ownership exchange proc type.
	 *
	 * @param lstOwnershipExchangeProcType the new lst ownership exchange proc type
	 */
	public void setLstOwnershipExchangeProcType(
			List<ParameterTable> lstOwnershipExchangeProcType) {
		this.lstOwnershipExchangeProcType = lstOwnershipExchangeProcType;
	}

	/**
	 * Gets the checks if is all transferable.
	 *
	 * @return the checks if is all transferable
	 */
	public Boolean getIsAllTransferable() {
		return isAllTransferable;
	}

	/**
	 * Sets the checks if is all transferable.
	 *
	 * @param isAllTransferable the new checks if is all transferable
	 */
	public void setIsAllTransferable(Boolean isAllTransferable) {
		this.isAllTransferable = isAllTransferable;
	}

	/**
	 * Gets the holder account bal block data model.
	 *
	 * @return the holder account bal block data model
	 */
	public GenericDataModel<HolderAccountBlockBalTO> getHolderAccountBalBlockDataModel() {
		return holderAccountBalBlockDataModel;
	}

	/**
	 * Sets the holder account bal block data model.
	 *
	 * @param holderAccountBalBlockDataModel the new holder account bal block data model
	 */
	public void setHolderAccountBalBlockDataModel(
			GenericDataModel<HolderAccountBlockBalTO> holderAccountBalBlockDataModel) {
		this.holderAccountBalBlockDataModel = holderAccountBalBlockDataModel;
	}


	/**
	 * Gets the selected change ownership file display.
	 *
	 * @return the selected change ownership file display
	 */
	public String getSelectedChangeOwnershipFileDisplay() {
		return selectedChangeOwnershipFileDisplay;
	}

	/**
	 * Sets the selected change ownership file display.
	 *
	 * @param selectedChangeOwnershipFileDisplay the new selected change ownership file display
	 */
	public void setSelectedChangeOwnershipFileDisplay(
			String selectedChangeOwnershipFileDisplay) {
		this.selectedChangeOwnershipFileDisplay = selectedChangeOwnershipFileDisplay;
	}

	/**
	 * Gets the selected change ownership file type.
	 *
	 * @return the selected change ownership file type
	 */
	public Integer getSelectedChangeOwnershipFileType() {
		return selectedChangeOwnershipFileType;
	}

	/**
	 * Sets the selected change ownership file type.
	 *
	 * @param selectedChangeOwnershipFileType the new selected change ownership file type
	 */
	public void setSelectedChangeOwnershipFileType(
			Integer selectedChangeOwnershipFileType) {
		this.selectedChangeOwnershipFileType = selectedChangeOwnershipFileType;
	}

	/**
	 * Gets the selected change ownership detail to.
	 *
	 * @return the selected change ownership detail to
	 */
	public ChangeOwnershipDetailTO getSelectedChangeOwnershipDetailTO() {
		return selectedChangeOwnershipDetailTO;
	}

	/**
	 * Sets the selected change ownership detail to.
	 *
	 * @param selectedChangeOwnershipDetailTO the new selected change ownership detail to
	 */
	public void setSelectedChangeOwnershipDetailTO(
			ChangeOwnershipDetailTO selectedChangeOwnershipDetailTO) {
		this.selectedChangeOwnershipDetailTO = selectedChangeOwnershipDetailTO;
	}

	/**
	 * Gets the change ownership regularization data model.
	 *
	 * @return the change ownership regularization data model
	 */
	public GenericDataModel<ChangeOwnershipDetailTO> getChangeOwnershipRegularizationDataModel() {
		return changeOwnershipRegularizationDataModel;
	}

	/**
	 * Sets the change ownership regularization data model.
	 *
	 * @param changeOwnershipRegularizationDataModel the new change ownership regularization data model
	 */
	public void setChangeOwnershipRegularizationDataModel(
			GenericDataModel<ChangeOwnershipDetailTO> changeOwnershipRegularizationDataModel) {
		this.changeOwnershipRegularizationDataModel = changeOwnershipRegularizationDataModel;
	}

	/**
	 * Gets the selected change ownership for reg to.
	 *
	 * @return the selected change ownership for reg to
	 */
	public ChangeOwnershipDetailTO getSelectedChangeOwnershipForRegTO() {
		return selectedChangeOwnershipForRegTO;
	}

	/**
	 * Sets the selected change ownership for reg to.
	 *
	 * @param selectedChangeOwnershipForRegTO the new selected change ownership for reg to
	 */
	public void setSelectedChangeOwnershipForRegTO(
			ChangeOwnershipDetailTO selectedChangeOwnershipForRegTO) {
		this.selectedChangeOwnershipForRegTO = selectedChangeOwnershipForRegTO;
	}

	/**
	 * Gets the lst reject motives.
	 *
	 * @return the lst reject motives
	 */
	public List<ParameterTable> getLstRejectMotives() {
		return lstRejectMotives;
	}

	/**
	 * Sets the lst reject motives.
	 *
	 * @param lstRejectMotives the new lst reject motives
	 */
	public void setLstRejectMotives(List<ParameterTable> lstRejectMotives) {
		this.lstRejectMotives = lstRejectMotives;
	}

	/**
	 * Checks if is show motive text.
	 *
	 * @return true, if is show motive text
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	/**
	 * Sets the show motive text.
	 *
	 * @param showMotiveText the new show motive text
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	

	/**
	 * Gets the lst attachment types.
	 *
	 * @return the lst attachment types
	 */
	public LinkedHashMap<Integer, ParameterTable> getLstAttachmentTypes() {
		return lstAttachmentTypes;
	}

	/**
	 * Sets the lst attachment types.
	 *
	 * @param lstAttachmentTypes the lst attachment types
	 */
	public void setLstAttachmentTypes(
			LinkedHashMap<Integer, ParameterTable> lstAttachmentTypes) {
		this.lstAttachmentTypes = lstAttachmentTypes;
	}

	/**
	 * Checks if is depositary user.
	 *
	 * @return true, if is depositary user
	 */
	public boolean isDepositaryUser(){
		return userInfo.getUserAccountSession().isDepositaryInstitution();
	}

	/**
	 * Checks if is checks for other motive.
	 *
	 * @return the hasOtherMotive
	 */
	public boolean isHasOtherMotive() {
		return hasOtherMotive;
	}

	/**
	 * Sets the checks for other motive.
	 *
	 * @param hasOtherMotive the hasOtherMotive to set
	 */
	public void setHasOtherMotive(boolean hasOtherMotive) {
		this.hasOtherMotive = hasOtherMotive;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Gets the current detail list.
	 *
	 * @return the current detail list
	 */
	public List<ChangeOwnershipTransferTO> getCurrentDetailList() {
		return currentDetailList;
	}

	/**
	 * Sets the current detail list.
	 *
	 * @param currentDetailList the new current detail list
	 */
	public void setCurrentDetailList(
			List<ChangeOwnershipTransferTO> currentDetailList) {
		this.currentDetailList = currentDetailList;
	}

	/**
	 * Checks if is checks for editable detail.
	 *
	 * @return true, if is checks for editable detail
	 */
	public boolean isHasEditableDetail() {
		return hasEditableDetail;
	}

	/**
	 * Sets the checks for editable detail.
	 *
	 * @param hasEditableDetail the new checks for editable detail
	 */
	public void setHasEditableDetail(boolean hasEditableDetail) {
		this.hasEditableDetail = hasEditableDetail;
	}

	/**
	 * Gets the selected transfer.
	 *
	 * @return the selected transfer
	 */
	public ChangeOwnershipTransferTO getSelectedTransfer() {
		return selectedTransfer;
	}

	/**
	 * Sets the selected transfer.
	 *
	 * @param selectedTransfer the new selected transfer
	 */
	public void setSelectedTransfer(ChangeOwnershipTransferTO selectedTransfer) {
		this.selectedTransfer = selectedTransfer;
	}

	/**
	 * Checks if is checks for motive type.
	 *
	 * @return true, if is checks for motive type
	 */
	public boolean isHasMotiveType() {
		return hasMotiveType;
	}

	/**
	 * Sets the checks for motive type.
	 *
	 * @param hasMotiveType the new checks for motive type
	 */
	public void setHasMotiveType(boolean hasMotiveType) {
		this.hasMotiveType = hasMotiveType;
	}

	/**
	 * Gets the map parameters.
	 *
	 * @return the map parameters
	 */
	public HashMap<Integer, ParameterTable> getMapParameters() {
		return mapParameters;
	}

	/**
	 * Sets the map parameters.
	 *
	 * @param mapParameters the map parameters
	 */
	public void setMapParameters(HashMap<Integer, ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * Gets the selected holder account.
	 *
	 * @return the selected holder account
	 */
	public HolderAccount getSelectedHolderAccount() {
		return selectedHolderAccount;
	}

	/**
	 * Sets the selected holder account.
	 *
	 * @param selectedHolderAccount the new selected holder account
	 */
	public void setSelectedHolderAccount(HolderAccount selectedHolderAccount) {
		this.selectedHolderAccount = selectedHolderAccount;
	}

	/**
	 * Checks if is enable comments.
	 *
	 * @return true, if is enable comments
	 */
	public boolean isEnableComments() {
		return enableComments;
	}

	/**
	 * Sets the enable comments.
	 *
	 * @param enableComments the new enable comments
	 */
	public void setEnableComments(boolean enableComments) {
		this.enableComments = enableComments;
	}

	/**
	 * Gets the selected holder account block bal to.
	 *
	 * @return the selected holder account block bal to
	 */
	public HolderAccountBlockBalTO getSelectedHolderAccountBlockBalTO() {
		return selectedHolderAccountBlockBalTO;
	}

	/**
	 * Sets the selected holder account block bal to.
	 *
	 * @param selectedHolderAccountBlockBalTO the new selected holder account block bal to
	 */
	public void setSelectedHolderAccountBlockBalTO(
			HolderAccountBlockBalTO selectedHolderAccountBlockBalTO) {
		this.selectedHolderAccountBlockBalTO = selectedHolderAccountBlockBalTO;
	}

	/**
	 * Gets the dynamic balance.
	 *
	 * @return the dynamic balance
	 */
	public BigDecimal getDynamicBalance() {
		return dynamicBalance;
	}

	/**
	 * Sets the dynamic balance.
	 *
	 * @param dynamicBalance the new dynamic balance
	 */
	public void setDynamicBalance(BigDecimal dynamicBalance) {
		this.dynamicBalance = dynamicBalance;
	}

	/**
	 * Gets the search participant.
	 *
	 * @return the search participant
	 */
	public Participant getSearchParticipant() {
		return searchParticipant;
	}

	/**
	 * Sets the search participant.
	 *
	 * @param searchParticipant the new search participant
	 */
	public void setSearchParticipant(Participant searchParticipant) {
		this.searchParticipant = searchParticipant;
	}

	/**
	 * Gets the help market fact balance view.
	 *
	 * @return the help market fact balance view
	 */
	public MarketFactBalanceHelpTO getHelpMarketFactBalanceView() {
		return helpMarketFactBalanceView;
	}

	/**
	 * Sets the help market fact balance view.
	 *
	 * @param helpMarketFactBalanceView the new help market fact balance view
	 */
	public void setHelpMarketFactBalanceView(
			MarketFactBalanceHelpTO helpMarketFactBalanceView) {
		this.helpMarketFactBalanceView = helpMarketFactBalanceView;
	}
	
	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	public Issuance getIssuanceHelperSearch() {
		return issuanceHelperSearch;
	}

	public void setIssuanceHelperSearch(Issuance issuanceHelperSearch) {
		this.issuanceHelperSearch = issuanceHelperSearch;
	}

	public boolean isValidIssuance() {
		return validIssuance;
	}

	public void setValidIssuance(boolean validIssuance) {
		this.validIssuance = validIssuance;
	}
	
	
		
}