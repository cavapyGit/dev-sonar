package com.pradera.custody.ownershipexchange.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.OperationTypeCascade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipDetailTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class ChangeOwnershipDetailTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id change ownership pk. */
	private Long idChangeOwnershipPk;
	
	/** The register date. */
	private Date registerDate;
	
	/** The id source holder pk. */
	private Long idSourceHolderPk;
	
	/** The source holder description. */
	private String sourceHolderDescription;
	
	/** The id source holder account pk. */
	private Long idSourceHolderAccountPk;
	
	/** The id source account number. */
	private Long idSourceAccountNumber;
	
	/** The source holder account description. */
	private String sourceHolderAccountDescription;
	
	/** The id source holder account type. */
	private Integer idSourceHolderAccountType;
	
	/** The source holder account. */
	private HolderAccount sourceHolderAccount;
	
	/** The id target holder pk. */
	private Long idTargetHolderPk;
	
	/** The target holder description. */
	private String targetHolderDescription;
	
	/** The id target holder account pk. */
	private Long idTargetHolderAccountPk;
	
	/** The id target account number. */
	private String idTargetAccountNumber;
	
	/** The target holder account description. */
	private String targetHolderAccountDescription;
	
	/** The target holder accounts. */
	private List<HolderAccount> targetHolderAccounts;
	
	/** The id source participant pk. */
	private Long idSourceParticipantPk;
	
	/** The source participant mnemonic. */
	private String sourceParticipantMnemonic;
	
	/** The source participant description. */
	private String sourceParticipantDescription;
	
	/** The id target participant pk. */
	private Long idTargetParticipantPk;
	
	/** The target participant mnemonic. */
	private String targetParticipantMnemonic;
	
	/** The target participant description. */
	private String targetParticipantDescription;
	
	/** The id isin code pk. */
	private String idIsinCodePk;
	
	/** The id motive. */
	private Integer idMotive;
	
	/** The motive description. */
	private String motiveDescription;
	
	/** The id balance type. */
	private Integer idBalanceType;
	
	/** The balance type description. */
	private String balanceTypeDescription;
	
	/** The id status. */
	private Integer idStatus;
	
	/** The status description. */
	private String statusDescription;
	
	/** The need details. */
	private boolean needDetails = true;
	
	/** The list operation type. */
	List<OperationTypeCascade> listOperationType;
	
	/** The initial date. */
	/* cancelation or revertion search */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id change ownership ref pk. */
	private Long idChangeOwnershipRefPk;
	
	/** The target holder account. */
	private HolderAccount targetHolderAccount;

	
	/**
	 * Instantiates a new change ownership detail to.
	 *
	 * @param changeOwnershipDetailTO the change ownership detail to
	 */
	public ChangeOwnershipDetailTO(ChangeOwnershipDetailTO changeOwnershipDetailTO){
		this.idChangeOwnershipPk = changeOwnershipDetailTO.getIdChangeOwnershipPk();
		this.operationNumber = changeOwnershipDetailTO.getOperationNumber();
		this.registerDate = changeOwnershipDetailTO.getRegisterDate();
		
		this.idSourceHolderPk = changeOwnershipDetailTO.getIdSourceHolderPk();
		this.idSourceAccountNumber = changeOwnershipDetailTO.getIdSourceAccountNumber();
		this.sourceHolderDescription = changeOwnershipDetailTO.getSourceHolderDescription();
		this.idSourceHolderAccountPk = changeOwnershipDetailTO.getIdSourceHolderAccountPk();
		this.sourceHolderAccountDescription = changeOwnershipDetailTO.getSourceHolderAccountDescription();
		this.idSourceHolderAccountType = changeOwnershipDetailTO.getIdSourceHolderAccountType();
		this.sourceHolderAccount = changeOwnershipDetailTO.getSourceHolderAccount();
		
		this.idTargetHolderPk = changeOwnershipDetailTO.getIdTargetHolderPk();
		this.idTargetAccountNumber = changeOwnershipDetailTO.getIdTargetAccountNumber();
		this.targetHolderDescription = changeOwnershipDetailTO.getTargetHolderDescription();
		this.idTargetHolderAccountPk = changeOwnershipDetailTO.getIdTargetHolderAccountPk();
		this.targetHolderAccountDescription = changeOwnershipDetailTO.getTargetHolderAccountDescription();
		this.targetHolderAccounts = changeOwnershipDetailTO.getTargetHolderAccounts();
		
		this.idSourceParticipantPk = changeOwnershipDetailTO.getIdSourceParticipantPk();
		this.sourceParticipantMnemonic = changeOwnershipDetailTO.getSourceParticipantMnemonic();
		this.sourceParticipantDescription = changeOwnershipDetailTO.getSourceParticipantDescription();
		
		this.idTargetParticipantPk = changeOwnershipDetailTO.getIdTargetParticipantPk();
		this.targetParticipantMnemonic = changeOwnershipDetailTO.getTargetParticipantMnemonic();
		this.targetParticipantDescription = changeOwnershipDetailTO.getTargetParticipantDescription();
		
		this.idIsinCodePk = changeOwnershipDetailTO.getIdIsinCodePk();
		
		this.idMotive = changeOwnershipDetailTO.getIdMotive();
		this.motiveDescription = changeOwnershipDetailTO.getMotiveDescription();
		this.idBalanceType = changeOwnershipDetailTO.getIdBalanceType();
		this.balanceTypeDescription = changeOwnershipDetailTO.getBalanceTypeDescription();
		this.idStatus = changeOwnershipDetailTO.getIdStatus();
		this.statusDescription = changeOwnershipDetailTO.getStatusDescription();
		
		this.idChangeOwnershipRefPk = changeOwnershipDetailTO.getIdChangeOwnershipRefPk();

	}
	
	/**
	 * Instantiates a new change ownership detail to.
	 */
	public ChangeOwnershipDetailTO() {
		// TODO Auto-generated constructor stub
		targetHolderAccounts = new ArrayList<HolderAccount>();
	}
	
	/**
	 * Gets the target accounts description.
	 *
	 * @return the target accounts description
	 */
	public String getTargetAccountsDescription(){
		if(targetHolderAccountDescription==null){
			StringBuilder stringB = new StringBuilder();
			for(HolderAccount account : targetHolderAccounts){
				stringB.append(account.getAccountDescription());
				stringB.append("<br>");
			}
			targetHolderAccountDescription = stringB.toString();
		}
		return targetHolderAccountDescription;
	}
	
	/**
	 * Gets the target accounts numbers.
	 *
	 * @return the target accounts numbers
	 */
	public String getTargetAccountsNumbers(){
		if(idTargetAccountNumber==null){
			StringBuilder stringB = new StringBuilder();
			for(HolderAccount account : targetHolderAccounts){
				if(targetHolderAccounts.size()!=1){
					stringB.append("* &nbsp ");
				}
				stringB.append(account.getAccountNumber());
				stringB.append("<br>");
			}
			idTargetAccountNumber = stringB.toString();
		}
		return idTargetAccountNumber;
	}
	
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the id source holder pk.
	 *
	 * @return the idSourceHolderPk
	 */
	public Long getIdSourceHolderPk() {
		return idSourceHolderPk;
	}
	
	/**
	 * Sets the id source holder pk.
	 *
	 * @param idSourceHolderPk the idSourceHolderPk to set
	 */
	public void setIdSourceHolderPk(Long idSourceHolderPk) {
		this.idSourceHolderPk = idSourceHolderPk;
	}
	
	/**
	 * Gets the source holder description.
	 *
	 * @return the sourceHolderDescription
	 */
	public String getSourceHolderDescription() {
		return sourceHolderDescription;
	}
	
	/**
	 * Sets the source holder description.
	 *
	 * @param sourceHolderDescription the sourceHolderDescription to set
	 */
	public void setSourceHolderDescription(String sourceHolderDescription) {
		this.sourceHolderDescription = sourceHolderDescription;
	}
	
	/**
	 * Gets the id source holder account pk.
	 *
	 * @return the idSourceHolderAccountPk
	 */
	public Long getIdSourceHolderAccountPk() {
		return idSourceHolderAccountPk;
	}
	
	/**
	 * Sets the id source holder account pk.
	 *
	 * @param idSourceHolderAccountPk the idSourceHolderAccountPk to set
	 */
	public void setIdSourceHolderAccountPk(Long idSourceHolderAccountPk) {
		this.idSourceHolderAccountPk = idSourceHolderAccountPk;
	}
	
	/**
	 * Gets the source holder account description.
	 *
	 * @return the sourceHolderAccountDescription
	 */
	public String getSourceHolderAccountDescription() {
		return sourceHolderAccountDescription;
	}
	
	/**
	 * Sets the source holder account description.
	 *
	 * @param sourceHolderAccountDescription the sourceHolderAccountDescription to set
	 */
	public void setSourceHolderAccountDescription(
			String sourceHolderAccountDescription) {
		this.sourceHolderAccountDescription = sourceHolderAccountDescription;
	}
	
	/**
	 * Gets the id target holder pk.
	 *
	 * @return the idTargetHolderPk
	 */
	public Long getIdTargetHolderPk() {
		return idTargetHolderPk;
	}
	
	/**
	 * Sets the id target holder pk.
	 *
	 * @param idTargetHolderPk the idTargetHolderPk to set
	 */
	public void setIdTargetHolderPk(Long idTargetHolderPk) {
		this.idTargetHolderPk = idTargetHolderPk;
	}
	
	/**
	 * Gets the target holder description.
	 *
	 * @return the targetHolderDescription
	 */
	public String getTargetHolderDescription() {
		return targetHolderDescription;
	}
	
	/**
	 * Sets the target holder description.
	 *
	 * @param targetHolderDescription the targetHolderDescription to set
	 */
	public void setTargetHolderDescription(String targetHolderDescription) {
		this.targetHolderDescription = targetHolderDescription;
	}
	
	/**
	 * Gets the id target holder account pk.
	 *
	 * @return the idTargetHolderAccountPk
	 */
	public Long getIdTargetHolderAccountPk() {
		return idTargetHolderAccountPk;
	}
	
	/**
	 * Sets the id target holder account pk.
	 *
	 * @param idTargetHolderAccountPk the idTargetHolderAccountPk to set
	 */
	public void setIdTargetHolderAccountPk(Long idTargetHolderAccountPk) {
		this.idTargetHolderAccountPk = idTargetHolderAccountPk;
	}
	
	/**
	 * Gets the target holder account description.
	 *
	 * @return the targetHolderAccountDescription
	 */
	public String getTargetHolderAccountDescription() {
		return targetHolderAccountDescription;
	}
	
	/**
	 * Sets the target holder account description.
	 *
	 * @param targetHolderAccountDescription the targetHolderAccountDescription to set
	 */
	public void setTargetHolderAccountDescription(
			String targetHolderAccountDescription) {
		this.targetHolderAccountDescription = targetHolderAccountDescription;
	}
	
	/**
	 * Gets the id source participant pk.
	 *
	 * @return the idSourceParticipantPk
	 */
	public Long getIdSourceParticipantPk() {
		return idSourceParticipantPk;
	}
	
	/**
	 * Sets the id source participant pk.
	 *
	 * @param idSourceParticipantPk the idSourceParticipantPk to set
	 */
	public void setIdSourceParticipantPk(Long idSourceParticipantPk) {
		this.idSourceParticipantPk = idSourceParticipantPk;
	}
	
	/**
	 * Gets the source participant description.
	 *
	 * @return the sourceParticipantDescription
	 */
	public String getSourceParticipantDescription() {
		return sourceParticipantDescription;
	}
	
	/**
	 * Sets the source participant description.
	 *
	 * @param sourceParticipantDescription the sourceParticipantDescription to set
	 */
	public void setSourceParticipantDescription(String sourceParticipantDescription) {
		this.sourceParticipantDescription = sourceParticipantDescription;
	}
	
	/**
	 * Gets the id target participant pk.
	 *
	 * @return the idTargetParticipantPk
	 */
	public Long getIdTargetParticipantPk() {
		return idTargetParticipantPk;
	}
	
	/**
	 * Sets the id target participant pk.
	 *
	 * @param idTargetParticipantPk the idTargetParticipantPk to set
	 */
	public void setIdTargetParticipantPk(Long idTargetParticipantPk) {
		this.idTargetParticipantPk = idTargetParticipantPk;
	}
	
	/**
	 * Gets the target participant description.
	 *
	 * @return the targetParticipantDescription
	 */
	public String getTargetParticipantDescription() {
		return targetParticipantDescription;
	}
	
	/**
	 * Sets the target participant description.
	 *
	 * @param targetParticipantDescription the targetParticipantDescription to set
	 */
	public void setTargetParticipantDescription(String targetParticipantDescription) {
		this.targetParticipantDescription = targetParticipantDescription;
	}
//	/**
//	 * @return the motiveDescription
//	 */
//	public String getMotiveDescription() {
//		ChangeOwnershipMotiveType value = ChangeOwnershipMotiveType.lookup.get(idMotive);
//		return value==null?GeneralConstants.EMPTY_STRING:value.getValue();
//	}
//	/**
//	 * @return the statusDescription
//	 */
//	public String getStatusDescription() {
//		ChangeOwnershipStatusType value = ChangeOwnershipStatusType.lookup.get(idStatus);
//		return value==null?GeneralConstants.EMPTY_STRING:value.getValue();
//	}

	/**
 * Gets the id motive.
 *
 * @return the id motive
 */
public Integer getIdMotive() {
		return idMotive;
	}

	/**
	 * Sets the id motive.
	 *
	 * @param idMotive the new id motive
	 */
	public void setIdMotive(Integer idMotive) {
		this.idMotive = idMotive;
	}

	/**
	 * Gets the id status.
	 *
	 * @return the id status
	 */
	public Integer getIdStatus() {
		return idStatus;
	}

	/**
	 * Sets the id status.
	 *
	 * @param idStatus the new id status
	 */
	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	/**
	 * Gets the id source account number.
	 *
	 * @return the id source account number
	 */
	public Long getIdSourceAccountNumber() {
		return idSourceAccountNumber;
	}

	/**
	 * Sets the id source account number.
	 *
	 * @param idSourceAccountNumber the new id source account number
	 */
	public void setIdSourceAccountNumber(Long idSourceAccountNumber) {
		this.idSourceAccountNumber = idSourceAccountNumber;
	}

	/**
	 * Gets the id target account number.
	 *
	 * @return the id target account number
	 */
	public String getIdTargetAccountNumber() {
		return idTargetAccountNumber;
	}

	/**
	 * Sets the id target account number.
	 *
	 * @param idTargetAccountNumber the new id target account number
	 */
	public void setIdTargetAccountNumber(String idTargetAccountNumber) {
		this.idTargetAccountNumber = idTargetAccountNumber;
	}

	/**
	 * Gets the source participant mnemonic.
	 *
	 * @return the source participant mnemonic
	 */
	public String getSourceParticipantMnemonic() {
		return sourceParticipantMnemonic;
	}

	/**
	 * Sets the source participant mnemonic.
	 *
	 * @param sourceParticipantMnemonic the new source participant mnemonic
	 */
	public void setSourceParticipantMnemonic(String sourceParticipantMnemonic) {
		this.sourceParticipantMnemonic = sourceParticipantMnemonic;
	}

	/**
	 * Gets the target participant mnemonic.
	 *
	 * @return the target participant mnemonic
	 */
	public String getTargetParticipantMnemonic() {
		return targetParticipantMnemonic;
	}

	/**
	 * Sets the target participant mnemonic.
	 *
	 * @param targetParticipantMnemonic the new target participant mnemonic
	 */
	public void setTargetParticipantMnemonic(String targetParticipantMnemonic) {
		this.targetParticipantMnemonic = targetParticipantMnemonic;
	}

	/**
	 * Gets the id source holder account type.
	 *
	 * @return the id source holder account type
	 */
	public Integer getIdSourceHolderAccountType() {
		return idSourceHolderAccountType;
	}

	/**
	 * Sets the id source holder account type.
	 *
	 * @param idSourceHolderAccountType the new id source holder account type
	 */
	public void setIdSourceHolderAccountType(Integer idSourceHolderAccountType) {
		this.idSourceHolderAccountType = idSourceHolderAccountType;
	}

	/**
	 * Gets the source holder account.
	 *
	 * @return the source holder account
	 */
	public HolderAccount getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	/**
	 * Sets the source holder account.
	 *
	 * @param sourceHolderAccount the new source holder account
	 */
	public void setSourceHolderAccount(HolderAccount sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}

	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}

	/**
	 * Gets the target holder accounts.
	 *
	 * @return the target holder accounts
	 */
	public List<HolderAccount> getTargetHolderAccounts() {
		return targetHolderAccounts;
	}

	/**
	 * Sets the target holder accounts.
	 *
	 * @param targetHolderAccounts the new target holder accounts
	 */
	public void setTargetHolderAccounts(List<HolderAccount> targetHolderAccounts) {
		this.targetHolderAccounts = targetHolderAccounts;
	}

	/**
	 * Gets the id balance type.
	 *
	 * @return the id balance type
	 */
	public Integer getIdBalanceType() {
		return idBalanceType;
	}

	/**
	 * Sets the id balance type.
	 *
	 * @param idBalanceType the new id balance type
	 */
	public void setIdBalanceType(Integer idBalanceType) {
		this.idBalanceType = idBalanceType;
	}

	/**
	 * Checks if is need details.
	 *
	 * @return true, if is need details
	 */
	public boolean isNeedDetails() {
		return needDetails;
	}

	/**
	 * Sets the need details.
	 *
	 * @param needDetails the new need details
	 */
	public void setNeedDetails(boolean needDetails) {
		this.needDetails = needDetails;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the id change ownership ref pk.
	 *
	 * @return the id change ownership ref pk
	 */
	public Long getIdChangeOwnershipRefPk() {
		return idChangeOwnershipRefPk;
	}

	/**
	 * Sets the id change ownership ref pk.
	 *
	 * @param idChangeOwnershipRefPk the new id change ownership ref pk
	 */
	public void setIdChangeOwnershipRefPk(Long idChangeOwnershipRefPk) {
		this.idChangeOwnershipRefPk = idChangeOwnershipRefPk;
	}

	/**
	 * Gets the target holder account.
	 *
	 * @return the targetHolderAccount
	 */
	public HolderAccount getTargetHolderAccount() {
		return targetHolderAccount;
	}

	/**
	 * Sets the target holder account.
	 *
	 * @param targetHolderAccount the targetHolderAccount to set
	 */
	public void setTargetHolderAccount(HolderAccount targetHolderAccount) {
		this.targetHolderAccount = targetHolderAccount;
	}

	/**
	 * Gets the motive description.
	 *
	 * @return the motiveDescription
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}

	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the motiveDescription to set
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	/**
	 * Gets the balance type description.
	 *
	 * @return the balanceTypeDescription
	 */
	public String getBalanceTypeDescription() {
		return balanceTypeDescription;
	}

	/**
	 * Sets the balance type description.
	 *
	 * @param balanceTypeDescription the balanceTypeDescription to set
	 */
	public void setBalanceTypeDescription(String balanceTypeDescription) {
		this.balanceTypeDescription = balanceTypeDescription;
	}

	/**
	 * Gets the status description.
	 *
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}

	/**
	 * Sets the status description.
	 *
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/**
	 * Gets the id change ownership pk.
	 *
	 * @return the id change ownership pk
	 */
	public Long getIdChangeOwnershipPk() {
		return idChangeOwnershipPk;
	}

	/**
	 * Sets the id change ownership pk.
	 *
	 * @param idChangeOwnershipPk the new id change ownership pk
	 */
	public void setIdChangeOwnershipPk(Long idChangeOwnershipPk) {
		this.idChangeOwnershipPk = idChangeOwnershipPk;
	}

	/**
	 * Gets the list operation type.
	 *
	 * @return the list operation type
	 */
	public List<OperationTypeCascade> getListOperationType() {
		return listOperationType;
	}

	/**
	 * Sets the list operation type.
	 *
	 * @param listOperationType the new list operation type
	 */
	public void setListOperationType(List<OperationTypeCascade> listOperationType) {
		this.listOperationType = listOperationType;
	}
	
	

	
}
