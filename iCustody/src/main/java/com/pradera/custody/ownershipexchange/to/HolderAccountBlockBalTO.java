package com.pradera.custody.ownershipexchange.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderAccountBlockBalTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class HolderAccountBlockBalTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance;
	
	/** The others block operations. */
	private List<BlockOperation> othersBlockOperations;
	
	/** The pawn block operations. */
	private List<BlockOperation> pawnBlockOperations;
	
	/** The ban block operations. */
	private List<BlockOperation> banBlockOperations;
	
	/** The selected block operation. */
	private BlockOperation selectedBlockOperation;
	
	/** The block operations in view. */
	public GenericDataModel<BlockOperation> blockOperationsInView;
	
	/**
	 * Gets the block operations in view.
	 *
	 * @return the block operations in view
	 */
	public GenericDataModel<BlockOperation> getBlockOperationsInView() {
		return blockOperationsInView;
	}

	/**
	 * Sets the block operations in view.
	 *
	 * @param blockOperationsInView the new block operations in view
	 */
	public void setBlockOperationsInView(
			GenericDataModel<BlockOperation> blockOperationsInView) {
		this.blockOperationsInView = blockOperationsInView;
	}
	
	/**
	 * Sets the block operations in view.
	 *
	 * @param blockOperations the block operations
	 * @param affectationType the affectation type
	 */
	public void setBlockOperationsInView(
			List<BlockOperation> blockOperations, Integer affectationType) {
		if(Validations.validateIsNotNullAndPositive(affectationType) && 
				Validations.validateListIsNotNullAndNotEmpty(blockOperations)){
			if(affectationType.equals(AffectationType.BAN.getCode())){
				banBlockOperations  = blockOperations;
				blockOperationsInView = new GenericDataModel<BlockOperation>(banBlockOperations);
			}else if (affectationType.equals(AffectationType.PAWN.getCode())){
				pawnBlockOperations = blockOperations;
				blockOperationsInView = new GenericDataModel<BlockOperation>(pawnBlockOperations);
			}else if (affectationType.equals(AffectationType.OTHERS.getCode())){
				othersBlockOperations = blockOperations;
				blockOperationsInView = new GenericDataModel<BlockOperation>(othersBlockOperations);
			}			
		}else{
			blockOperationsInView = new GenericDataModel<BlockOperation>();
		}
	}
	
	/**
	 * Swap block operations in view.
	 *
	 * @param affectationType the affectation type
	 */
	public void swapBlockOperationsInView(Integer affectationType) {
		if(Validations.validateIsNotNullAndPositive(affectationType)){
			if(affectationType.equals(AffectationType.BAN.getCode())){
				blockOperationsInView = new GenericDataModel<BlockOperation>(banBlockOperations);
			}else if (affectationType.equals(AffectationType.PAWN.getCode())){
				blockOperationsInView = new GenericDataModel<BlockOperation>(pawnBlockOperations);
			}else if (affectationType.equals(AffectationType.OTHERS.getCode())){
				blockOperationsInView = new GenericDataModel<BlockOperation>(othersBlockOperations);
			}
			
		}else{
			blockOperationsInView = new GenericDataModel<BlockOperation>();
		}
	}
	
	/**
	 * Exists affectation type list.
	 *
	 * @param affectationType the affectation type
	 * @return true, if successful
	 */
	public boolean existsAffectationTypeList(Integer affectationType){
		boolean exists = false;
		if(Validations.validateIsNotNullAndPositive(affectationType)){
			if(affectationType.equals(AffectationType.BAN.getCode()) && Validations.validateIsNotNull(banBlockOperations)){
				exists=true;
			}else if (affectationType.equals(AffectationType.PAWN.getCode()) && Validations.validateIsNotNull(pawnBlockOperations)){
				exists=true;
			}else if (affectationType.equals(AffectationType.OTHERS.getCode()) && Validations.validateIsNotNull(othersBlockOperations)){
				exists=true;
			}
		}
		return exists;
	}
	
	/**
	 * Gets the all block operations.
	 *
	 * @return the all block operations
	 */
	public List<BlockOperation> getAllBlockOperations(){
		List<BlockOperation> allBlockOperations = new ArrayList<BlockOperation>();
		if(banBlockOperations!=null){
			allBlockOperations.addAll(banBlockOperations);
		}
		if(othersBlockOperations!=null){
			allBlockOperations.addAll(othersBlockOperations);
		}
		if(pawnBlockOperations!=null){
			allBlockOperations.addAll(pawnBlockOperations);
		}
		return allBlockOperations;
	}
	
	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}
	
	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}

	/**
	 * Gets the others block operations.
	 *
	 * @return the others block operations
	 */
	public List<BlockOperation> getOthersBlockOperations() {
		return othersBlockOperations;
	}

	/**
	 * Sets the others block operations.
	 *
	 * @param othersBlockOperations the new others block operations
	 */
	public void setOthersBlockOperations(List<BlockOperation> othersBlockOperations) {
		this.othersBlockOperations = othersBlockOperations;
	}

	/**
	 * Gets the pawn block operations.
	 *
	 * @return the pawn block operations
	 */
	public List<BlockOperation> getPawnBlockOperations() {
		return pawnBlockOperations;
	}

	/**
	 * Sets the pawn block operations.
	 *
	 * @param pawnBlockOperations the new pawn block operations
	 */
	public void setPawnBlockOperations(List<BlockOperation> pawnBlockOperations) {
		this.pawnBlockOperations = pawnBlockOperations;
	}

	/**
	 * Gets the ban block operations.
	 *
	 * @return the ban block operations
	 */
	public List<BlockOperation> getBanBlockOperations() {
		return banBlockOperations;
	}

	/**
	 * Sets the ban block operations.
	 *
	 * @param banBlockOperations the new ban block operations
	 */
	public void setBanBlockOperations(List<BlockOperation> banBlockOperations) {
		this.banBlockOperations = banBlockOperations;
	}

	/**
	 * Gets the selected block operation.
	 *
	 * @return the selected block operation
	 */
	public BlockOperation getSelectedBlockOperation() {
		return selectedBlockOperation;
	}

	/**
	 * Sets the selected block operation.
	 *
	 * @param selectedBlockOperation the new selected block operation
	 */
	public void setSelectedBlockOperation(BlockOperation selectedBlockOperation) {
		this.selectedBlockOperation = selectedBlockOperation;
	}

}
