package com.pradera.custody.ownershipexchange.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.changeownership.ChangeOwnershipMarketFact;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipTransferTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class ChangeOwnershipTransferTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The quantity balance. */
	private BigDecimal quantityBalance;
	
	/** The affectation type. */
	private Integer affectationType;
	
	/** The ind block balance. */
	private Integer indBlockBalance = ComponentConstant.ZERO;
	
	/** The block operation. */
	private BlockOperation blockOperation; // selected block op
	
	/** The participant. */
	private Participant participant;
	
	/** The security. */
	private Security security;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The selected. */
	private boolean selected;

	/** The disabled. */
	private boolean disabled;
	
	/** The save. */
	private boolean save;
	
	/** The delete. */
	private boolean delete;
	
	/** The change ownership market fact. */
	private List<ChangeOwnershipMarketFact> changeOwnershipMarketFact = new ArrayList<ChangeOwnershipMarketFact>();
	
	/**
	 * Gets the affectation type.
	 *
	 * @return the affectation type
	 */
	public Integer getAffectationType() {
		return affectationType;
	}

	/**
	 * Sets the affectation type.
	 *
	 * @param affectationType the new affectation type
	 */
	public void setAffectationType(Integer affectationType) {
		this.affectationType = affectationType;
	}

	/**
	 * Gets the quantity balance.
	 *
	 * @return the quantity balance
	 */
	public BigDecimal getQuantityBalance() {
		return quantityBalance;
	}

	/**
	 * Sets the quantity balance.
	 *
	 * @param quantityBalance the new quantity balance
	 */
	public void setQuantityBalance(BigDecimal quantityBalance) {
		this.quantityBalance = quantityBalance;
	}

	/**
	 * Gets the ind block balance.
	 *
	 * @return the ind block balance
	 */
	public Integer getIndBlockBalance() {
		return indBlockBalance;
	}

	/**
	 * Sets the ind block balance.
	 *
	 * @param indBlockBalance the new ind block balance
	 */
	public void setIndBlockBalance(Integer indBlockBalance) {
		this.indBlockBalance = indBlockBalance;
	}

	/**
	 * Gets the change ownership market fact.
	 *
	 * @return the change ownership market fact
	 */
	public List<ChangeOwnershipMarketFact> getChangeOwnershipMarketFact() {
		return changeOwnershipMarketFact;
	}

	/**
	 * Sets the change ownership market fact.
	 *
	 * @param changeOwnershipMarketFact the new change ownership market fact
	 */
	public void setChangeOwnershipMarketFact(
			List<ChangeOwnershipMarketFact> changeOwnershipMarketFact) {
		this.changeOwnershipMarketFact = changeOwnershipMarketFact;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Checks if is selected.
	 *
	 * @return true, if is selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * Checks if is disabled.
	 *
	 * @return true, if is disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * Sets the disabled.
	 *
	 * @param disabled the new disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * Checks if is save.
	 *
	 * @return true, if is save
	 */
	public boolean isSave() {
		return save;
	}

	/**
	 * Sets the save.
	 *
	 * @param save the new save
	 */
	public void setSave(boolean save) {
		this.save = save;
	}

	/**
	 * Checks if is delete.
	 *
	 * @return true, if is delete
	 */
	public boolean isDelete() {
		return delete;
	}

	/**
	 * Sets the delete.
	 *
	 * @param delete the new delete
	 */
	public void setDelete(boolean delete) {
		this.delete = delete;
	}


	/**
	 * Gets the block operation.
	 *
	 * @return the block operation
	 */
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	/**
	 * Sets the block operation.
	 *
	 * @param blockOperation the new block operation
	 */
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}
	
	

}
