package com.pradera.custody.ownershipexchange.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.changeownership.BlockChangeOwnership;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.changeownership.ChangeOwnershipFile;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ChangeOwnershipOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class ChangeOwnershipOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The source holder. */
	private Holder sourceHolder;
	
	/** The target holder. */
	private Holder targetHolder;
	
	/** The source holder accounts. */
	private List<HolderAccount> sourceHolderAccounts;
	
	/** The target holder accounts. */
	private List<HolderAccount> targetHolderAccounts;
	
	/** The found target accounts. */
	private List<HolderAccount> foundTargetAccounts;
	
	/** The change ownership details. */
	private List<ChangeOwnershipDetail> changeOwnershipDetails;
	
	/** The files. */
	private HashMap<Integer, ChangeOwnershipFile> files;
	
	/** For search purposes. */
	private Long idOperationNumber;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id source target holder pk. */
	private Long idSourceTargetHolderPk;
	
	/** The source target holder. */
	private Holder sourceTargetHolder;
	
	/** The id source target holder account pk. */
	private Long idSourceTargetHolderAccountPk;
	
	/** The source target holder account. */
	private HolderAccount sourceTargetHolderAccount;
	
	/** The id balance type. */
	private Integer idBalanceType;
	
	/** The id reason type. */
	private Integer idReasonType;
	
	/** The id status. */
	private Integer idStatus;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id holder status. */
	private Integer idHolderStatus;
	
	/** The regularization check. */
	private boolean regularizationCheck;
	
	private boolean searchByExchangeSecurities;
	
	private List<Integer> lstStatus;
	
	/**
	 * Instantiates a new change ownership operation to.
	 */
	public ChangeOwnershipOperationTO() {
		super();
		sourceHolder = new Holder();
		targetHolder = new Holder();
		changeOwnershipDetails = new ArrayList<ChangeOwnershipDetail>(0);
		initialDate = CommonsUtilities.currentDate();
		finalDate = CommonsUtilities.currentDate();
		
		sourceTargetHolder = new Holder();
		sourceTargetHolderAccount = new HolderAccount();
		targetHolderAccounts = new ArrayList<HolderAccount>(0);
		foundTargetAccounts = new ArrayList<HolderAccount>(0);
	}
	
	/** The all change ownership details. */
	private List<ChangeOwnershipDetail> allChangeOwnershipDetails;
	
	/** The all block ownership details. */
	private List<BlockChangeOwnership> allBlockOwnershipDetails;
	
	public boolean isSearchByExchangeSecurities() {
		return searchByExchangeSecurities;
	}

	public void setSearchByExchangeSecurities(boolean searchByExchangeSecurities) {
		this.searchByExchangeSecurities = searchByExchangeSecurities;
	}

	/**
	 * Gets the change ownership details.
	 *
	 * @return the change ownership details
	 */
	public List<ChangeOwnershipDetail> getChangeOwnershipDetails() {
		return changeOwnershipDetails;
	}

	/**
	 * Sets the change ownership details.
	 *
	 * @param changeOwnershipDetails the new change ownership details
	 */
	public void setChangeOwnershipDetails(
			List<ChangeOwnershipDetail> changeOwnershipDetails) {
		this.changeOwnershipDetails = changeOwnershipDetails;
	}

	/**
	 * Gets the id source target holder pk.
	 *
	 * @return the id source target holder pk
	 */
	public Long getIdSourceTargetHolderPk() {
		return idSourceTargetHolderPk;
	}

	/**
	 * Sets the id source target holder pk.
	 *
	 * @param idSourceTargetHolderPk the new id source target holder pk
	 */
	public void setIdSourceTargetHolderPk(Long idSourceTargetHolderPk) {
		this.idSourceTargetHolderPk = idSourceTargetHolderPk;
	}

	/**
	 * Gets the id source target holder account pk.
	 *
	 * @return the id source target holder account pk
	 */
	public Long getIdSourceTargetHolderAccountPk() {
		return idSourceTargetHolderAccountPk;
	}

	/**
	 * Sets the id source target holder account pk.
	 *
	 * @param idSourceTargetHolderAccountPk the new id source target holder account pk
	 */
	public void setIdSourceTargetHolderAccountPk(Long idSourceTargetHolderAccountPk) {
		this.idSourceTargetHolderAccountPk = idSourceTargetHolderAccountPk;
	}

	/**
	 * Gets the id balance type.
	 *
	 * @return the id balance type
	 */
	public Integer getIdBalanceType() {
		return idBalanceType;
	}

	/**
	 * Sets the id balance type.
	 *
	 * @param idBalanceType the new id balance type
	 */
	public void setIdBalanceType(Integer idBalanceType) {
		this.idBalanceType = idBalanceType;
	}

	/**
	 * Gets the id reason type.
	 *
	 * @return the id reason type
	 */
	public Integer getIdReasonType() {
		return idReasonType;
	}

	/**
	 * Sets the id reason type.
	 *
	 * @param idReasonType the new id reason type
	 */
	public void setIdReasonType(Integer idReasonType) {
		this.idReasonType = idReasonType;
	}

	/**
	 * Gets the id status.
	 *
	 * @return the id status
	 */
	public Integer getIdStatus() {
		return idStatus;
	}

	/**
	 * Sets the id status.
	 *
	 * @param idStatus the new id status
	 */
	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the id operation number.
	 *
	 * @return the id operation number
	 */
	public Long getIdOperationNumber() {
		return idOperationNumber;
	}

	/**
	 * Sets the id operation number.
	 *
	 * @param idOperationNumber the new id operation number
	 */
	public void setIdOperationNumber(Long idOperationNumber) {
		this.idOperationNumber = idOperationNumber;
	}
	
	/**
	 * Gets the all target change ownership details.
	 *
	 * @return the all target change ownership details
	 */
	public List<ChangeOwnershipDetail> getAllTargetChangeOwnershipDetails(){
		if(Validations.validateListIsNullOrEmpty(allChangeOwnershipDetails)){
			allChangeOwnershipDetails = new ArrayList<ChangeOwnershipDetail>();
			if(changeOwnershipDetails!=null){
				for(ChangeOwnershipDetail changeOwnershipDetail:changeOwnershipDetails){
					if(changeOwnershipDetail.getTargetChangeOwnershipDetails()!=null){
						allChangeOwnershipDetails.addAll(changeOwnershipDetail.getTargetChangeOwnershipDetails());
					}
				}
			}
		}else {
			allChangeOwnershipDetails = new ArrayList<ChangeOwnershipDetail>();
			if(changeOwnershipDetails!=null){
				for(ChangeOwnershipDetail changeOwnershipDetail:changeOwnershipDetails){
					if(changeOwnershipDetail.getTargetChangeOwnershipDetails()!=null){
						allChangeOwnershipDetails.addAll(changeOwnershipDetail.getTargetChangeOwnershipDetails());
					}
				}
			}
		}
		
		return allChangeOwnershipDetails;
	}
	
	/**
	 * Gets the all block change ownerships.
	 *
	 * @return the all block change ownerships
	 */
	public List<BlockChangeOwnership> getAllBlockChangeOwnerships(){
		if(allBlockOwnershipDetails==null){
			allBlockOwnershipDetails = new ArrayList<BlockChangeOwnership>();
			if(changeOwnershipDetails!=null){
				for(ChangeOwnershipDetail changeOwnershipDetail:changeOwnershipDetails){
					if(changeOwnershipDetail.getBlockChangeOwnerships()!=null){
						allBlockOwnershipDetails.addAll(changeOwnershipDetail.getBlockChangeOwnerships());
					}
				}
			}
		}
		return allBlockOwnershipDetails;
		
	}

	/**
	 * Gets the source holder.
	 *
	 * @return the source holder
	 */
	public Holder getSourceHolder() {
		return sourceHolder;
	}


	/**
	 * Sets the source holder.
	 *
	 * @param sourceHolder the new source holder
	 */
	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}


	/**
	 * Gets the target holder.
	 *
	 * @return the target holder
	 */
	public Holder getTargetHolder() {
		return targetHolder;
	}


	/**
	 * Sets the target holder.
	 *
	 * @param targetHolder the new target holder
	 */
	public void setTargetHolder(Holder targetHolder) {
		this.targetHolder = targetHolder;
	}


	/**
	 * Gets the target holder accounts.
	 *
	 * @return the target holder accounts
	 */
	public List<HolderAccount> getTargetHolderAccounts() {
		return targetHolderAccounts;
	}


	/**
	 * Sets the target holder accounts.
	 *
	 * @param targetHolderAccounts the new target holder accounts
	 */
	public void setTargetHolderAccounts(List<HolderAccount> targetHolderAccounts) {
		this.targetHolderAccounts = targetHolderAccounts;
	}

	/**
	 * Gets the source target holder.
	 *
	 * @return the source target holder
	 */
	public Holder getSourceTargetHolder() {
		return sourceTargetHolder;
	}

	/**
	 * Sets the source target holder.
	 *
	 * @param sourceTargetHolder the new source target holder
	 */
	public void setSourceTargetHolder(Holder sourceTargetHolder) {
		this.sourceTargetHolder = sourceTargetHolder;
	}

	/**
	 * Gets the source target holder account.
	 *
	 * @return the source target holder account
	 */
	public HolderAccount getSourceTargetHolderAccount() {
		return sourceTargetHolderAccount;
	}

	/**
	 * Sets the source target holder account.
	 *
	 * @param sourceTargetHolderAccount the new source target holder account
	 */
	public void setSourceTargetHolderAccount(HolderAccount sourceTargetHolderAccount) {
		this.sourceTargetHolderAccount = sourceTargetHolderAccount;
	}

	/**
	 * Gets the id holder status.
	 *
	 * @return the id holder status
	 */
	public Integer getIdHolderStatus() {
		return idHolderStatus;
	}

	/**
	 * Sets the id holder status.
	 *
	 * @param idHolderStatus the new id holder status
	 */
	public void setIdHolderStatus(Integer idHolderStatus) {
		this.idHolderStatus = idHolderStatus;
	}

	/**
	 * Checks if is regularization check.
	 *
	 * @return the regularizationCheck
	 */
	public boolean isRegularizationCheck() {
		return regularizationCheck;
	}

	/**
	 * Sets the regularization check.
	 *
	 * @param regularizationCheck the regularizationCheck to set
	 */
	public void setRegularizationCheck(boolean regularizationCheck) {
		this.regularizationCheck = regularizationCheck;
	}

	/**
	 * Gets the files.
	 *
	 * @return the files
	 */
	public HashMap<Integer, ChangeOwnershipFile> getFiles() {
		return files;
	}

	/**
	 * Sets the files.
	 *
	 * @param files the files to set
	 */
	public void setFiles(HashMap<Integer, ChangeOwnershipFile> files) {
		this.files = files;
	}


	/**
	 * Gets the source holder accounts.
	 *
	 * @return the source holder accounts
	 */
	public List<HolderAccount> getSourceHolderAccounts() {
		return sourceHolderAccounts;
	}


	/**
	 * Sets the source holder accounts.
	 *
	 * @param sourceHolderAccounts the new source holder accounts
	 */
	public void setSourceHolderAccounts(List<HolderAccount> sourceHolderAccounts) {
		this.sourceHolderAccounts = sourceHolderAccounts;
	}

	/**
	 * Gets the found target accounts.
	 *
	 * @return the found target accounts
	 */
	public List<HolderAccount> getFoundTargetAccounts() {
		return foundTargetAccounts;
	}

	/**
	 * Sets the found target accounts.
	 *
	 * @param foundTargetAccounts the new found target accounts
	 */
	public void setFoundTargetAccounts(List<HolderAccount> foundTargetAccounts) {
		this.foundTargetAccounts = foundTargetAccounts;
	}

	/**
	 * @return the lstStatus
	 */
	public List<Integer> getLstStatus() {
		return lstStatus;
	}

	/**
	 * @param lstStatus the lstStatus to set
	 */
	public void setLstStatus(List<Integer> lstStatus) {
		this.lstStatus = lstStatus;
	}

}
