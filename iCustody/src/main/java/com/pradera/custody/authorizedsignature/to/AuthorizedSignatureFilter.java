package com.pradera.custody.authorizedsignature.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 * The Class AuthorizeSignatureTO.
 *
 * @author equinajo.
 * @version 1.0 , 18/08/2020
 */
public class AuthorizedSignatureFilter implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Integer state;
	private Integer situation;
	private String institution;
	private String searchCriteria;
	private Integer institutionType;
	private Date initialDate;
	private Date finalDate;
	private String documentNumber;
	private Long idAuthorizedSignaturePk;
	private List<Integer> listNotInState;
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getSituation() {
		return situation;
	}
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	public String getInstitution() {
		return institution;
	}
	public void setInstitution(String institution) {
		this.institution = institution;
	}
	public String getSearchCriteria() {
		return searchCriteria;
	}
	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}
	public Integer getInstitutionType() {
		return institutionType;
	}
	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Long getIdAuthorizedSignaturePk() {
		return idAuthorizedSignaturePk;
	}
	public void setIdAuthorizedSignaturePk(Long idAuthorizedSignaturePk) {
		this.idAuthorizedSignaturePk = idAuthorizedSignaturePk;
	}
	public List<Integer> getListNotInState() {
		return listNotInState;
	}
	public void setListNotInState(List<Integer> listNotInState) {
		this.listNotInState = listNotInState;
	}

}
