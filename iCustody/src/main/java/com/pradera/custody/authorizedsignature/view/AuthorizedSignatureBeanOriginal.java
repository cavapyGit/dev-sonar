package com.pradera.custody.authorizedsignature.view;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.custody.authorizedsignature.facade.AuthorizedSignatureServiceFacade;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureFilter;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.AuthorizeSignatureParameterType;
import com.pradera.model.generalparameter.type.AuthorizeSignatureStateType;
import com.pradera.model.generalparameter.type.MasterTableStatusType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.generalparameter.type.SignatureType;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * 
 * The Class AuthorizedSignatureBean.
 *
 * @author equinajo.
 * @version 1.0 , 17/08/2020
 */
@DepositaryWebBean
@LoggerCreateBean
public class AuthorizedSignatureBeanOriginal extends GenericBaseBean implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/** The entity manager*/
	@Inject @DepositaryDataBase
	protected EntityManager em;
	/** The country residence. */
    @Inject @Configurable Integer countryResidence;
	
    /** The general parameters facade*/
    @EJB
    private GeneralParametersFacade generalParametersFacade;
    /** The helper component facade */
    @EJB
    private HelperComponentFacade helperComponentFacade;
    /** The authorized signature service facade*/
    @EJB
	private AuthorizedSignatureServiceFacade authorizedSignatureServiceFacade;
   
    /** The user info. */
	@Inject
	private UserInfo userInfo;
	/** The user privilege*/
	@Inject
	private UserPrivilege userPrivilege;
	
	/** Permisos */
	private Long participantLogin;
	private String issuerLogin;
	
	/** Variables genericas*/	
	private boolean isBcr;
	private boolean flagActionRegister;
	private boolean flagActionModify;
	private boolean flagDepositary;
	private boolean isParticipant;
	private boolean isIssuer;
	private boolean rejectConfirmEnabled;
	private boolean operationConfirm;
	private boolean operationReject;
	private Long participantSelected;
	private boolean privilegeRegister;
	private boolean privilegeSearch;
	private Participant participant;
	private Issuer issuer;
	
	/** Variables para los criterios de busqueda */
	private Date currentDate;
	private Date initialDate;
	private Date finalDate;
	private String searchCriteria;
	private Integer statusSelected;
	private Integer situationSelected;
	private String institutionSelected;
	private Integer institutionTypeSelected;
	private List<SelectItem> situationList;
	private List<SelectItem> institutionList;
	private List<SelectItem> institutionTypeList;
	private List<ParameterTable> statusAutorizedSignatureList;
	private AuthorizedSignatureDataModel searchAuthorizedSignatureDataModel;
	
	/** Variables para la busqueda de firmas autorizadas */
	private  AuthorizedSignatureTO authorizedSignatureTO;
	private  AuthorizedSignatureTO authorizedSignatureTOAux;
	private AuthorizedSignatureTO selectAuthorizeSignatureTO;
	private List<AuthorizedSignatureTO> authorizedSignatureSearchList;
	
	/** Variables para la navegacion de paginas */
	private final String PAGE_SEARCH = "searchAuthorizedSignature";
	private final String PAGE_REGISTER = "newAuthorizedSignature";
	private final String PAGE_VIEW = "viewAuthorizedSignature";
	
	/** Variables para el registro de firma autorizada */
	private String signatureTypeSelected;
	private AuthorizedSignatureTO authorizedSignatureTOReg;
	private List<SelectItem> signatureTypeList;
	
	/** Variables para la firma */
	private String flagDisplayName;
	private String flagSizeSignature;
	private String flagDimensionToSignature;
	private String flagUploadImgTypesImgDisplayToSignature;
	private ParameterTable parameterMaxSize;
	private ParameterTable parameterAllowed;
	private ParameterTable parameterDimension;
	
	
	private String reasonSelected;
	private List<SelectItem> reasonTypeList;
	private String rejectOther;
	
	/**
	 * Post construc.
	 */
	@PostConstruct
	public void init(){
		loadSearchCriteria();
    }
	
	/*********************************************************************
	 **************** Metodos searAuthorizedSignature ********************
	 *********************************************************************/
    
	/** Metodo para un numero registro */
    public String newAuthorizedSignature(){
		loadRegiter();
		loadParameterImgSignature();
		return PAGE_REGISTER;
    }
    
    /** Metodo para la busqueda de registros */
	 public void searchAuthorizedSignatures(){
    	try {
	    	AuthorizedSignatureFilter authorizedSignatureFilter = new AuthorizedSignatureFilter();
	    	if(Validations.validateIsNotNullAndNotEmpty(searchCriteria))
	    		authorizedSignatureFilter.setSearchCriteria(searchCriteria);
	    	if(Validations.validateIsNotNullAndNotEmpty(statusSelected))
	    		authorizedSignatureFilter.setState(statusSelected);
	    	/*if(Validations.validateIsNotNullAndNotEmpty(institutionTypeSelected))
	    		authorizedSignatureFilter.setInstitutionType(institutionTypeSelected);*/
	    	if(Validations.validateIsNotNullAndNotEmpty(institutionSelected))
	    		authorizedSignatureFilter.setInstitution(institutionSelected);
	    	if(Validations.validateIsNotNullAndNotEmpty(situationSelected))
	    		authorizedSignatureFilter.setSituation(situationSelected);
	    	if(Validations.validateIsNotNull(initialDate))
	    		authorizedSignatureFilter.setInitialDate(initialDate);
	    	if(Validations.validateIsNotNull(finalDate))
	    		authorizedSignatureFilter.setFinalDate(finalDate);
	    	authorizedSignatureSearchList = authorizedSignatureServiceFacade.getAuthorizedSignatureSearchList(authorizedSignatureFilter);
    		if(Validations.validateIsNotNull(authorizedSignatureSearchList)){
    			searchAuthorizedSignatureDataModel = new AuthorizedSignatureDataModel(authorizedSignatureSearchList);
    		}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
    }
	
	/** Metodo para limpiar los criterios de busqueda */
    public void clearSearchCriteria(){
    	initialDate = null;
    	finalDate = null;
    	searchCriteria = null;
    	statusSelected = null;
     	situationSelected = null;
    	institutionSelected = null;
    	signatureTypeSelected = null;
    	institutionTypeSelected = null;
    	selectAuthorizeSignatureTO = null;
    	authorizedSignatureSearchList = new ArrayList<AuthorizedSignatureTO>();
    	searchAuthorizedSignatureDataModel = new AuthorizedSignatureDataModel();
		securityEvents();
    }

    /** Metodo para modificar el registro */
    public String modifyAuthorizedSignature(){
    	if(Validations.validateIsNullOrEmpty(selectAuthorizeSignatureTO)){
    		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
    		JSFUtilities.updateComponent("opnlDialogs");
    		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    		return null;
    	}
    	loadModify();
    	loadParameterImgSignature();
    	
    	authorizedSignatureTOAux = new AuthorizedSignatureTO();
    	authorizedSignatureTOAux.setIdAuthorizedSignaturePk(selectAuthorizeSignatureTO.getIdAuthorizedSignaturePk());
    	authorizedSignatureTOAux.setDocumentNumber(selectAuthorizeSignatureTO.getDocumentNumber());
    	authorizedSignatureTOAux.setFirtsLastName(selectAuthorizeSignatureTO.getFirtsLastName());
    	authorizedSignatureTOAux.setSecondLastName(selectAuthorizeSignatureTO.getSecondLastName());
    	authorizedSignatureTOAux.setName(selectAuthorizeSignatureTO.getName());
    	authorizedSignatureTOAux.setJobPosition(selectAuthorizeSignatureTO.getJobPosition());
    	authorizedSignatureTOAux.setSituation(selectAuthorizeSignatureTO.getSituation());
    	authorizedSignatureTOAux.setSignatureType(selectAuthorizeSignatureTO.getSignatureType());
    	authorizedSignatureTOAux.setInstitutionType(selectAuthorizeSignatureTO.getInstitutionType());
    	authorizedSignatureTOAux.setIdInstitutionFk(selectAuthorizeSignatureTO.getIdInstitutionFk());
    	
    	authorizedSignatureTOAux.setSignatureImg(selectAuthorizeSignatureTO.getSignatureImg());
    	authorizedSignatureTOAux.setSignatureName(selectAuthorizeSignatureTO.getSignatureName());
    	authorizedSignatureTOAux.setSignatureSize(selectAuthorizeSignatureTO.getSignatureSize());
    	authorizedSignatureTOAux.setSignatureExtension(selectAuthorizeSignatureTO.getSignatureExtension());
    	
    	authorizedSignatureTOAux.setFileContent(selectAuthorizeSignatureTO.getFileContent());
    	authorizedSignatureTOAux.setFileName(selectAuthorizeSignatureTO.getFileName());
    	authorizedSignatureTOAux.setFileSize(selectAuthorizeSignatureTO.getFileSize());
    	authorizedSignatureTOAux.setFileExtension(selectAuthorizeSignatureTO.getFileExtension());
    	
		return PAGE_REGISTER;
    }
    
    /** Metodo para confirmar la solicitud */
    public String confirmAuthorizedSignature(){
    	if(Validations.validateIsNullOrEmpty(selectAuthorizeSignatureTO)){
    		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
    		JSFUtilities.updateComponent("opnlDialogs");
    		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    		return null;
    	}
    	authorizedSignatureTO = selectAuthorizeSignatureTO;
    	if(!authorizedSignatureTO.getState().equals(AuthorizeSignatureStateType.REGISTERED.getCode())){
    		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_AUTHORIZED_SIGNATURE_REQUIRED_STATE_REGISTER, null);
    		JSFUtilities.updateComponent("opnlDialogs");
    		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    		return null;
    	}
    	disabledButtons();
    	renderFalseAllOperation();
    	operationConfirm = true;
		JSFUtilities.putSessionMap("sessStreamedImage",authorizedSignatureTO.getSignatureImg());
		return PAGE_VIEW;
    }
    
    /** Metodo para rechazar la solicitud */
    public String rejectAuthorizedSignature(){
    	if(Validations.validateIsNullOrEmpty(selectAuthorizeSignatureTO)){
    		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
    		JSFUtilities.updateComponent("opnlDialogs");
    		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    		return null;
    	}
    	authorizedSignatureTO = selectAuthorizeSignatureTO;
    	if(!authorizedSignatureTO.getState().equals(AuthorizeSignatureStateType.REGISTERED.getCode())){
    		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.MSG_ERROR_AUTHORIZED_SIGNATURE_REQUIRED_STATE_REGISTER, null);
    		JSFUtilities.updateComponent("opnlDialogs");
    		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    		return null;
    	}
    	disabledButtons();
    	renderFalseAllOperation();
    	operationReject = true;
    	JSFUtilities.putSessionMap("sessStreamedImage",authorizedSignatureTO.getSignatureImg());
		return PAGE_VIEW;
    }
    
    /** Metodo para ver el detalle */
    public String viewAuthorizedSignatureDetail(AuthorizedSignatureTO rowAuthorizeSignatureTO){
    	if(Validations.validateIsNullOrEmpty(rowAuthorizeSignatureTO)){
    		showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
    		JSFUtilities.updateComponent("opnlDialogs");
    		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    		return null;
    	}
    	authorizedSignatureTO = rowAuthorizeSignatureTO;
    	disabledButtons();
    	renderFalseAllOperation();
    	JSFUtilities.putSessionMap("sessStreamedImage",authorizedSignatureTO.getSignatureImg());
    	return PAGE_VIEW;
    }
    
	/*********************************************************************
	 ******************* Metodos newAuthorizedSignature ******************
	 *********************************************************************/
    
    /** Metodo para validar antes de guardar el registro */
	public void beforeSave() {
		//Realizando las validaciones correspondientes 
		if(!authorizedSignatureTOReg.getDocumentNumber().matches(".*[0-9].*")){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_AUTHORIZED_SIGNATURE_DOCUMENT_NUMBER, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return;
        }
		if (!authorizedSignatureTOReg.getJobPosition().matches(".*[a-zA-Z].*")) { 
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_AUTHORIZED_SIGNATURE_JOB_POSITION, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return;
		}
		if(Validations.validateIsNullOrEmpty(authorizedSignatureTOReg.getSignatureImg())){
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_SIGNATURE_IMG, null);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
			return;
		}
		//Verificando que el firmante no se encuentre registrado anteriormente
		AuthorizedSignatureFilter authorizedSignatureFilterAux = new AuthorizedSignatureFilter();
		authorizedSignatureFilterAux.setDocumentNumber(authorizedSignatureTOReg.getDocumentNumber());
		authorizedSignatureFilterAux.setIdAuthorizedSignaturePk(authorizedSignatureTOReg.getIdAuthorizedSignaturePk());		
		try {
			//Verificando si se encuentra en alguna solicitud pendiente de confirmacion
			authorizedSignatureFilterAux.setState(AuthorizeSignatureStateType.REGISTERED.getCode());
			List<AuthorizedSignatureTO> listAuthorizedSignatureTOAux=authorizedSignatureServiceFacade.getAuthorizedSignatureSearchListValidation(authorizedSignatureFilterAux);
			if(Validations.validateListIsNotNullAndNotEmpty(listAuthorizedSignatureTOAux)){
				AuthorizedSignatureTO authorizedSignatureTOAux = listAuthorizedSignatureTOAux.get(0);
				Object[] arrBodyData = { authorizedSignatureTOAux.getInstitution(),authorizedSignatureTOAux.getStateDesc(),authorizedSignatureTOAux.getSituationDesc()};
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_EXIST_AUTHORIZED_SIGNATURE_REGISTER, arrBodyData);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
				return;
			}
			//Verificando si se encuentra en una solicitud confirmada y activo
			authorizedSignatureFilterAux.setState(AuthorizeSignatureStateType.CONFIRMED.getCode());
			authorizedSignatureFilterAux.setSituation(MasterTableStatusType.ACTIVE.getCode());
			listAuthorizedSignatureTOAux=authorizedSignatureServiceFacade.getAuthorizedSignatureSearchListValidation(authorizedSignatureFilterAux);
			if(Validations.validateListIsNotNullAndNotEmpty(listAuthorizedSignatureTOAux)){
				AuthorizedSignatureTO authorizedSignatureTOAux = listAuthorizedSignatureTOAux.get(0);
				Object[] arrBodyData = { authorizedSignatureTOAux.getInstitution(),authorizedSignatureTOAux.getStateDesc(),authorizedSignatureTOAux.getSituationDesc()};
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_EXIST_AUTHORIZED_SIGNATURE_CONFIRM, arrBodyData);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
				return;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		String msgConfirmAction="";
		if(Validations.validateIsNullOrEmpty(authorizedSignatureTOReg.getIdAuthorizedSignaturePk())){
			msgConfirmAction = PropertiesConstants.LBL_MSG_CONFIRM_REGISTER_AUTHORIZED_SIGNATURE;
		}else{
			msgConfirmAction = PropertiesConstants.LBL_MSG_CONFIRM_MODIFY_AUTHORIZED_SIGNATURE;
		}
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM,null, msgConfirmAction,null);
		JSFUtilities.showComponent("cnfAuthorizedSignatureRegiter");
		JSFUtilities.hideComponent("validationAuthorizedSignature");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/** Persiste el registro de la firma autorizada */
	public void saveRegisterAuthorizedSignature() {
		try {
			for(SelectItem itemTemp:institutionList){
				if(itemTemp.getValue().equals(authorizedSignatureTOReg.getIdInstitutionFk())){
					authorizedSignatureTOReg.setInstitutionType(Integer.parseInt(itemTemp.getDescription()));
					break;
				}
			}
			if(Validations.validateIsNullOrEmpty(authorizedSignatureTOReg.getInstitutionType())){
				showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_AUTHORIZED_SIGNATURE_INSTITUTION_TYPE, null);
				JSFUtilities.updateComponent("opnlDialogs");
				JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
				return;
			}
			hideDialogsListener(null);
			authorizedSignatureTOReg.setRegisterUser(userInfo.getUserAccountSession().getUserName());
			authorizedSignatureTOReg.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			authorizedSignatureTOReg.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			authorizedSignatureTOReg.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeRegister().intValue());
			String msgSuccesfull="";
			if(Validations.validateIsNullOrEmpty(authorizedSignatureTOReg.getIdAuthorizedSignaturePk()))
				msgSuccesfull = PropertiesConstants.MSG_SUCCESFULL_REGISTER_AUTHORIZED_SIGNATURE;
			else
				msgSuccesfull = PropertiesConstants.MSG_SUCCESFULL_MODIFY_AUTHORIZED_SIGNATURE;
			authorizedSignatureServiceFacade.persistenAuthorizerSignature(authorizedSignatureTOReg);
			enabledButtons();
			clearSearchCriteria();
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL,null,msgSuccesfull,null);
			JSFUtilities.showComponent("cnfEndTransactionAuthorizedSignature");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}	
    
	/** Metodo para inicializar los registros */
	public void btnClear() {
		if(flagActionRegister)
		loadRegiter();
		else{
			selectAuthorizeSignatureTO.setIdAuthorizedSignaturePk(authorizedSignatureTOAux.getIdAuthorizedSignaturePk());
			selectAuthorizeSignatureTO.setDocumentNumber(authorizedSignatureTOAux.getDocumentNumber());
			selectAuthorizeSignatureTO.setFirtsLastName(authorizedSignatureTOAux.getFirtsLastName());
			selectAuthorizeSignatureTO.setSecondLastName(authorizedSignatureTOAux.getSecondLastName());
			selectAuthorizeSignatureTO.setName(authorizedSignatureTOAux.getName());
			selectAuthorizeSignatureTO.setJobPosition(authorizedSignatureTOAux.getJobPosition());
			selectAuthorizeSignatureTO.setSituation(authorizedSignatureTOAux.getSituation());
			selectAuthorizeSignatureTO.setSignatureType(authorizedSignatureTOAux.getSignatureType());
			selectAuthorizeSignatureTO.setInstitutionType(authorizedSignatureTOAux.getInstitutionType());
			selectAuthorizeSignatureTO.setIdInstitutionFk(authorizedSignatureTOAux.getIdInstitutionFk());
	    	
			selectAuthorizeSignatureTO.setSignatureImg(authorizedSignatureTOAux.getSignatureImg());
			selectAuthorizeSignatureTO.setSignatureName(authorizedSignatureTOAux.getSignatureName());
			selectAuthorizeSignatureTO.setSignatureSize(authorizedSignatureTOAux.getSignatureSize());
			selectAuthorizeSignatureTO.setSignatureExtension(authorizedSignatureTOAux.getSignatureExtension());
			
			selectAuthorizeSignatureTO.setFileContent(authorizedSignatureTOAux.getFileContent());
			selectAuthorizeSignatureTO.setFileName(authorizedSignatureTOAux.getFileName());
			selectAuthorizeSignatureTO.setFileSize(authorizedSignatureTOAux.getFileSize());
			selectAuthorizeSignatureTO.setFileExtension(authorizedSignatureTOAux.getFileExtension());
	    	
	    	loadModify();
		}
		
		
		loadParameterImgSignature();
		JSFUtilities.resetViewRoot();
	}
    
	/** Metodo para regresar a la pagina de busqueda*/
	public String goPageSearch() {
		flagActionRegister = false;
		flagActionModify = false;
		enabledButtons();
		renderFalseAllOperation();
		clearSearchCriteria();
		return PAGE_SEARCH;
	}
	
	/*********************************************************************
	 ************ Metodos viewAuthorizationSignature *********************
	 *********************************************************************/
	
	/** Muestra la ventana de confirmacion para confirmar */
	public void beforeConfirm() {
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_CONFIRM_AUTHORIZED_SIGNATURE, null);
		JSFUtilities.showComponent("cnfAuthorizedSignatureView");
	}
		
	/** Muestra la ventana para detallar el motivo de rechazo */
	public void showDlgSelectReason() {
		cancelAlertAnullOrReject();
		loadReasonTypeList();
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('alterRejectWidget').show();");
	}
	
	/** Mestra la ventana de confirmacion de rechazo */
	public void beforeReject() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('alterRejectWidget').hide();");
		showMessageOnDialog(PropertiesConstants.MESSAGES_CONFIRM, null, PropertiesConstants.LBL_MSG_REJECT_AUTHORIZED_SIGNATURE, null);
		JSFUtilities.showComponent("cnfAuthorizedSignatureView");
	}
	
	/** Clasifica la accion tomada */
	public void saveOperationChangeState() throws Exception {
		if (operationConfirm) {
			saveConfirmAuthorizedSignature();
			return;
		}
		if (operationReject) {
			saveRejectAuthorizedSignature();
			return;
		}
	}
	
	/** Cambia el estado a confirmado */ 
	public void saveConfirmAuthorizedSignature() throws Exception {
		try {
			hideDialogsListener(null);
			authorizedSignatureTO.setConfirmationUser(userInfo.getUserAccountSession().getUserName());
			authorizedSignatureTO.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			authorizedSignatureTO.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			authorizedSignatureTO.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeConfirm().intValue());
			
			authorizedSignatureTO.setState(AuthorizeSignatureStateType.CONFIRMED.getCode());
			authorizedSignatureServiceFacade.changeStateAuthorizedSignature(authorizedSignatureTO);
			enabledButtons();
			searchAuthorizedSignatures();
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_CONFIRM_AUTHORIZED_SIGNATURE, null);
			JSFUtilities.showComponent("cnfEndTransactionAuthorizedSignature");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** Cambia el estado a rechazado */
	public void saveRejectAuthorizedSignature() throws Exception {
		try {
			hideDialogsListener(null);
			authorizedSignatureTO.setRejectUser(userInfo.getUserAccountSession().getUserName());
			authorizedSignatureTO.setLastModifyUser(userInfo.getUserAccountSession().getUserName());
			authorizedSignatureTO.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
			authorizedSignatureTO.setAuditIdPrivilege(userPrivilege.getUserAcctions().getIdPrivilegeConfirm().intValue());
			
			String reasonReject="";
			if(Validations.validateIsNotNullAndNotEmpty(reasonSelected) && !reasonSelected.equals("-1")){
				if(reasonSelected.equals("716")){
					reasonReject = rejectOther;
				}else{
					for (int i = 0; i < reasonTypeList.size(); i++) {
						if(reasonTypeList.get(i).getValue().equals(reasonSelected)){
							reasonReject = reasonTypeList.get(i).getLabel();
							break;
						}
					}
				}
			}
 			authorizedSignatureTO.setReasonReject(reasonReject);
			authorizedSignatureTO.setState(AuthorizeSignatureStateType.REJECTED.getCode());
			authorizedSignatureServiceFacade.changeStateAuthorizedSignature(authorizedSignatureTO);
			enabledButtons();
			searchAuthorizedSignatures();
			showMessageOnDialog(PropertiesConstants.MESSAGES_SUCCESFUL, null, PropertiesConstants.MSG_SUCCESFUL_REJECT_AUTHORIZED_SIGNATURE, null);
			JSFUtilities.showComponent("cnfEndTransactionAuthorizedSignature");
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/** Retorna a la ventana de busqueda desde la ventana de detalle */
	public String backToSearchFromView() {
		flagActionRegister = false;
		flagActionModify = false;
		enabledButtons();
		renderFalseAllOperation();
		securityEvents();
		searchAuthorizedSignatures();
		return PAGE_SEARCH;
	}

    /*********************************************************************
	 ************ Metodos para carga de datos **********************
	 *********************************************************************/
	
	/** Cierra los dialogos */
	public void hideDialogsListener(ActionEvent actionEvent) {
		JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.hideComponent("cnfAuthorizedSignatureRegiter");
	}
	
	/** Cancela las operaciones */
	private void renderFalseAllOperation() {
		operationConfirm = false;
		operationReject = false;
	}
	
	/** Metodo para cargar las variables necesarias para la modificacion */
	public void loadModify(){
		disabledButtons();
		flagActionRegister= false;
		flagActionModify = true;
		authorizedSignatureTOReg = selectAuthorizeSignatureTO;
		flagDisplayName = authorizedSignatureTOReg.getSignatureName();
		JSFUtilities.putSessionMap("sessStreamedImage",authorizedSignatureTOReg.getSignatureImg());
		loadSearchCriteria();
	}
	
	/** Metodo para inicializar las variables para el registro */
    public void loadRegiter() {
    	disabledButtons();
    	flagActionRegister= true;
		flagActionModify = false;
		flagDisplayName = null;
		authorizedSignatureTOReg = new AuthorizedSignatureTO();
		JSFUtilities.putSessionMap("sessStreamedImage",null);
		loadSearchCriteria();
	}
	
	/** Metodo que carga los criterios de busqueda */
	public void loadSearchCriteria(){
		try {
			currentDate = new Date();
			loadStatusList();
			loadSituationList();
			loadSignatureTypeList();
			//loadInstitutionTypeList();
			loadInstitutionList();
			securityEvents();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
    /** Metodo para cargar los estados */
    public void loadStatusList() throws ServiceException {
    	statusSelected = null;
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.AUTHORIZED_SIGNATURE_STATE.getCode());
		statusAutorizedSignatureList = generalParametersFacade.getListParameterTableExcludePkServiceFacade(parameterTableTO);
	}
    
    /** Metodo para cargar los tipos de instituciones */
    public void loadInstitutionTypeList(){
    	institutionTypeSelected = null;
    	institutionTypeList = new ArrayList<>();
    	institutionTypeList.add(new SelectItem(InstitutionType.PARTICIPANT.getCode(), InstitutionType.PARTICIPANT.getValue()));
    	institutionTypeList.add(new SelectItem(InstitutionType.ISSUER.getCode(), InstitutionType.ISSUER.getValue()));
    }
    
    /** Metodo para cargar las situaciones */
    public void loadSituationList(){
    	situationSelected = null;
    	situationList = new ArrayList<>();
    	situationList.add(new SelectItem(MasterTableStatusType.ACTIVE.getCode(), MasterTableStatusType.ACTIVE.getValue()));
    	situationList.add(new SelectItem(MasterTableStatusType.INACTIVE.getCode(), MasterTableStatusType.INACTIVE.getValue()));  
    }

    /** Metodo para cargar los tipos de firmas */
    public void loadSignatureTypeList(){
    	signatureTypeSelected = null;
    	signatureTypeList = new ArrayList<>();
    	signatureTypeList.add(new SelectItem(SignatureType.IND.toString(), SignatureType.IND.getValue()));
    	signatureTypeList.add(new SelectItem(SignatureType.CON.toString(), SignatureType.CON.getValue()));
    }
    
    /** Metodo para cargar las instituciones */
	public void loadInstitutionList() throws ServiceException {
		institutionSelected = null;
		institutionList = new ArrayList<>();
		// Recuperando listado de participantes
		Map<String, Object> filter = new HashMap<String, Object>();
		List<Integer> states = new ArrayList<Integer>();
		states.add(ParticipantStateType.REGISTERED.getCode());
		filter.put("states", states);
		List<Participant> participantList = helperComponentFacade.getComboParticipantsByMapFilter(filter);
		if (Validations.validateListIsNotNullAndNotEmpty(participantList)) {
			for (Participant participantTemp : participantList) {
				institutionList.add(new SelectItem(participantTemp.getIdParticipantPk().toString(), participantTemp.getMnemonic() + GeneralConstants.DASH + participantTemp.getDescription(), InstitutionType.PARTICIPANT.getCode().toString()));
			}
		}
		// Recuperando listado de emisores
		List<Issuer> listIssuer = authorizedSignatureServiceFacade.issuerList();
		if (Validations.validateListIsNotNullAndNotEmpty(listIssuer)) {
			for (Issuer issuerTemp : listIssuer) {
				institutionList.add(new SelectItem(issuerTemp.getIdIssuerPk().toString(), issuerTemp.getMnemonic() + GeneralConstants.DASH + issuerTemp.getBusinessName(), InstitutionType.ISSUER.getCode().toString()));
			}
		}
	}
   
    /** Cargando para metros para la firma autorizada*/
    public void loadParameterImgSignature(){
    	parameterMaxSize=authorizedSignatureServiceFacade.getParameterTableSignature(AuthorizeSignatureParameterType.MAX_SIZE.getCode());
    	parameterAllowed=authorizedSignatureServiceFacade.getParameterTableSignature(AuthorizeSignatureParameterType.ALLOWED.getCode());
    	parameterDimension=authorizedSignatureServiceFacade.getParameterTableSignature(AuthorizeSignatureParameterType.DIMENSION.getCode());
    	if(Validations.validateIsNotNullAndNotEmpty(parameterMaxSize))
    	flagSizeSignature=CommonsUtilities.sizeFileString(parameterMaxSize.getLongInteger());
    	if(Validations.validateIsNotNullAndNotEmpty(parameterAllowed))
    	flagUploadImgTypesImgDisplayToSignature=CommonsUtilities.allowedFileString(parameterAllowed.getText1());
    	if(Validations.validateIsNotNullAndNotEmpty(parameterDimension))
    	flagDimensionToSignature=CommonsUtilities.dimensionImageString(parameterDimension.getText1());
    }
    
    /** Metodo para cargar los tipos de razones de rechazo
     * @throws ServiceException */
    public void loadReasonTypeList(){
    	reasonSelected = "-1";
    	reasonTypeList = new ArrayList<>();
    	
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.REASON_REJECT_ANULLED_REQUEST.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<ParameterTable> lstParameterTable;
		try {
			lstParameterTable = generalParametersFacade.getComboParameterTable(parameterTableTO);
			for (ParameterTable param : lstParameterTable) {
				reasonTypeList.add(new SelectItem(param.getParameterTablePk().toString(), param.getParameterName()));
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
	 * Cancel alert anull or reject.
	 */
	public void cancelAlertAnullOrReject(){
		reasonSelected = "-1";
		rejectOther = "";
		JSFUtilities.updateComponent(":alterReject:formPopUp");
		JSFUtilities.resetViewRoot();
	}
   
	/*********************************************************************
	 ************ Metodos para la persistencia de la firma ***************
	 *********************************************************************/
	
	/** Metodo para cargar la firma */
	public void imgAttachSignatureImg(FileUploadEvent event) throws IOException{
		JSFUtilities.updateComponent("opUploadFile");
		JSFUtilities.executeJavascriptFunction("PF('dlgwFileUploadInvFile').hide();");
		//Validando extension
		List<String> listaArchivosPermitidos = CommonsUtilities.toList(parameterAllowed.getText1());
		String extensionArchivo = CommonsUtilities.getExtension(event.getFile().getFileName());
		if(!listaArchivosPermitidos.contains(extensionArchivo.toLowerCase())){
			Object[] arrBodyData = { flagUploadImgTypesImgDisplayToSignature };
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_EXTENSION_SIGNATURE_IMG, arrBodyData);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
			return;
		}
		//Validando dimensiones
		InputStream is = event.getFile().getInputstream();
        BufferedImage img = ImageIO.read(is);
        int width = img.getWidth();
        int height = img.getHeight();
        String[] parts = parameterDimension.getText1().split(",");
        if(width != Integer.parseInt(parts[1]) || height != Integer.parseInt(parts[0])){
        	Object[] arrBodyData = { flagDimensionToSignature };
        	showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_DIMENSION_SIGNATURE_IMG, arrBodyData);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
			return;
        }
        //Validando tamanio
        Long tamanioFilePermitido = parameterMaxSize.getLongInteger();
        if (event.getFile().getSize() > tamanioFilePermitido) {
        	Object[] arrBodyData = { flagSizeSignature };
        	showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_SIZE_SIGNATURE_IMG, arrBodyData);
      		JSFUtilities.updateComponent("opnlDialogs");
      		JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
      		return;
      	}
		String fDisplayNameTemp=getDisplayName(event.getFile());		 
		 if(fDisplayNameTemp!=null){
			 flagDisplayName=fDisplayNameTemp;
		 }
		authorizedSignatureTOReg.setSignatureImg(event.getFile().getContents());	
		authorizedSignatureTOReg.setSignatureName(event.getFile().getFileName());
		authorizedSignatureTOReg.setSignatureSize(event.getFile().getSize());
		authorizedSignatureTOReg.setSignatureExtension(event.getFile().getContentType());
		JSFUtilities.putSessionMap("sessStreamedImage",authorizedSignatureTOReg.getSignatureImg());
	}

	/** Metodo para eliminar la firma */
	public void removeSignatureImg(){
		authorizedSignatureTOReg.setSignatureImg(null);
		authorizedSignatureTOReg.setSignatureName(null);
		authorizedSignatureTOReg.setSignatureSize(null);
		authorizedSignatureTOReg.setSignatureExtension(null);
		flagDisplayName = null;
		JSFUtilities.putSessionMap("sessStreamedImage",null);
	}

	/** Metodo para obtener el nombre de la firma */
	private String getDisplayName(UploadedFile fileUploaded){
		String fDisplayName = null;
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			fDisplayName = sbFileName.toString();
		}else{
			fDisplayName =  fileUploaded.getFileName();
		}
		return fDisplayName;
	}
	
	/** metodo para realizar la carga archivo adjunto
	 * 
	 * @param event */
	public void uploadAttachedFile(FileUploadEvent event) throws Exception {
		JSFUtilities.updateComponent("opUploadFile");
		JSFUtilities.executeJavascriptFunction("PF('dlgwFileUploadInvFile').hide();");
		//Validando extension
		List<String> listaArchivosPermitidos = CommonsUtilities.toList(this.getPdfFileUploadFiltTypes());
		String extensionArchivo = CommonsUtilities.getExtension(event.getFile().getFileName());
		if(!listaArchivosPermitidos.contains(extensionArchivo.toLowerCase())){
			Object[] arrBodyData = { this.getPdfFileUploadFiltTypes() };
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_EXTENSION_SIGNATURE_IMG, arrBodyData);
			JSFUtilities.updateComponent("opnlDialogs");
			JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
			return;
		}
        //Validando tamanio
        Long tamanioFilePermitido = parameterMaxSize.getLongInteger();
        if (event.getFile().getSize() > tamanioFilePermitido) {
        	Object[] arrBodyData = { flagSizeSignature };
        	showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null, PropertiesConstants.VALIDATION_SIZE_SIGNATURE_IMG, arrBodyData);
      		JSFUtilities.updateComponent("opnlDialogs");
      		JSFUtilities.executeJavascriptFunction("PF('alterValidationImgWidget').show();");
      		return;
      	}
		authorizedSignatureTOReg.setFileContent(event.getFile().getContents());	
		authorizedSignatureTOReg.setFileName(event.getFile().getFileName());
		authorizedSignatureTOReg.setFileSize(event.getFile().getSize());
		authorizedSignatureTOReg.setFileExtension(event.getFile().getContentType());
	}
	
	/** Metodo para eliminar la firma */
	public void removeFile(){
		authorizedSignatureTOReg.setFileContent(null);
		authorizedSignatureTOReg.setFileName(null);
		authorizedSignatureTOReg.setFileSize(null);
		authorizedSignatureTOReg.setFileExtension(null);
	}
	
    /*********************************************************************
   	 ********************** Metodos Genericos ****************************
   	 *********************************************************************/
    
    /** Enabled buttons */
    public void enabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
    	if(userPrivilege.getUserAcctions().isModify())
			privilegeComponent.setBtnModifyView(true);
    	if(userPrivilege.getUserAcctions().isConfirm())
			privilegeComponent.setBtnConfirmView(true);
		if(userPrivilege.getUserAcctions().isReject())
			privilegeComponent.setBtnRejectView(true);
		if(flagDepositary)
			rejectConfirmEnabled = true;
		userPrivilege.setPrivilegeComponent(privilegeComponent);	
    }
    
    /** Disabled buttons */
    public void disabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(false);
		privilegeComponent.setBtnConfirmView(false);
		privilegeComponent.setBtnRejectView(false);
		rejectConfirmEnabled = false;
		userPrivilege.setPrivilegeComponent(privilegeComponent);
    }
    
    /** Security events. */
    public void securityEvents(){
		try {
			if (userPrivilege.getUserAcctions().isRegister()) {
				privilegeRegister = true;
			}
			if (userPrivilege.getUserAcctions().isSearch()) {
				privilegeSearch = true;
			}
			isParticipant = false;
			isIssuer = false;
			flagDepositary = false;
			participantLogin = userInfo.getUserAccountSession().getParticipantCode();
			issuerLogin = userInfo.getUserAccountSession().getIssuerCode();
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isIssuerInstitucion()){
				if(userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
					isParticipant = true;
					participant = authorizedSignatureServiceFacade.findParticipant(participantLogin);
					if(Validations.validateIsNotNullAndNotEmpty(participant)){
						if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode())) {
							privilegeRegister = false;
						}
						institutionTypeSelected = InstitutionType.PARTICIPANT.getCode();
						institutionSelected = participant.getIdParticipantPk().toString();
						if(flagActionRegister){
							authorizedSignatureTOReg.setInstitutionType(institutionTypeSelected);
							authorizedSignatureTOReg.setIdInstitutionFk(institutionSelected);
						}
					}
				}
				if(userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
					isIssuer = true;
					issuer = authorizedSignatureServiceFacade.findIssuer(issuerLogin);
					if(Validations.validateIsNotNullAndNotEmpty(issuer)){
						if (issuer.getStateIssuer().equals(ParticipantStateType.BLOCKED.getCode())) {
							privilegeRegister = false;
						}
						institutionTypeSelected = InstitutionType.ISSUER.getCode();
						institutionSelected = issuer.getIdIssuerPk();
						if(flagActionRegister){
							authorizedSignatureTOReg.setInstitutionType(institutionTypeSelected);
							authorizedSignatureTOReg.setIdInstitutionFk(institutionSelected);
						}
					}
				}
			}else{
				flagDepositary = true;
			}
			RequestContext.getCurrentInstance().update("cboparticipant");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
    
}
