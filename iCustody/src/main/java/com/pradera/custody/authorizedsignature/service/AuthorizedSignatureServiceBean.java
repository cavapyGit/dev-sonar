package com.pradera.custody.authorizedsignature.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureFilter;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.authorizedSignature.AuthorizedSignature;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;

/**
 * 
 * The Class AuthorizedSignatureServiceBean.
 *
 * @author equinajo.
 * @version 1.0 , 18/08/2020
 */

@Stateless
public class AuthorizedSignatureServiceBean extends CrudDaoServiceBean{
	
	/**
	 * Find participant.
	 * @param participantPk the participant pk
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipant(Long participantPk)throws ServiceException{
		String sql = "Select p from Participant p where p.idParticipantPk = :idParticipantPk";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idParticipantPk", participantPk);
		return (Participant)findObjectByQueryString(sql, parameters);
	}

	/**
	 * Find Issuer list
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Issuer> issuerList() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append(" Select su ");		
		sbQuery.append(" From Issuer su ");
		sbQuery.append(" Where su.stateIssuer=:stateIs ");
		sbQuery.append(" Order by su.mnemonic ");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("stateIs", IssuerStateType.REGISTERED.getCode());
		return (List<Issuer>) query.getResultList();
	}
	
	/**
	 * Find Issuer
	 * @param idIssuerPk
	 * @return
	 */
	public Issuer findIssuer(String idIssuerPk){
		return find(Issuer.class, idIssuerPk);
	}
	
	/**
	 * Persisten authorized signature register modify
	 * @param authorizedSignature
	 * @return
	 * @throws ServiceException
	 */
	public AuthorizedSignature persistenAuthorizedSignature(AuthorizedSignature authorizedSignature) throws ServiceException{
		if(Validations.validateIsNullOrEmpty(authorizedSignature.getIdAuthorizedSignaturePk()))
			return createWithFlush(authorizedSignature);
		else
			return update(authorizedSignature);
	}
	
	/**
	 * Get authorized signature list	
	 * @param authorizedSignatureFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<AuthorizedSignatureTO> getAuthorizedSignatureList(AuthorizedSignatureFilter authorizedSignatureFilter)throws ServiceException{
		
		List<AuthorizedSignatureTO> authorizedSignatureTOList = new ArrayList<AuthorizedSignatureTO>();
		String searchCriteria="";
		if (Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getSearchCriteria())) {
			searchCriteria = authorizedSignatureFilter.getSearchCriteria().trim().toUpperCase();
			searchCriteria = "%" + searchCriteria + "%";
	    }
		//Creating query
		StringBuffer sbQuery = new StringBuffer("");
		sbQuery.append(" Select asi ");
		sbQuery.append(" From AuthorizedSignature asi");
		sbQuery.append(" Where 1=1 ");
		if(Validations.validateIsNotNull(authorizedSignatureFilter.getInitialDate())
				&& Validations.validateIsNotNull(authorizedSignatureFilter.getFinalDate())){
			sbQuery.append(" and trunc(asi.registerDate) between :initialDate and :finalDate");	
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getState()))
			sbQuery.append(" And asi.state = :parState ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getInstitutionType()))
			sbQuery.append(" And asi.institutionType = :parInstitutionType ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getInstitution()))
			sbQuery.append(" And asi.idInstitutionFk = :parInstitution ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getSituation()))
			sbQuery.append(" And asi.situation = :parSituation ");
		if(Validations.validateIsNotNullAndNotEmpty(searchCriteria)){
			sbQuery.append(" And (" +
	                   " upper(asi.firstLastName ||' '|| asi.secondLastName ||' '|| asi.name) LIKE :parSearchCriteria " +
	                   " Or upper(asi.documentNumber) Like :parSearchCriteria " +
	                   " Or upper(asi.jobPosition) Like :parSearchCriteria " +
	                   " ) ");
		}
		sbQuery.append(" Order by asi.registerDate desc"); 
		// Setting parameter
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNull(authorizedSignatureFilter.getInitialDate())
				&& Validations.validateIsNotNull(authorizedSignatureFilter.getFinalDate())){
			query.setParameter("initialDate", authorizedSignatureFilter.getInitialDate());
			query.setParameter("finalDate", authorizedSignatureFilter.getFinalDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getState()))
			query.setParameter("parState",authorizedSignatureFilter.getState());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getInstitutionType()))
			query.setParameter("parInstitutionType",authorizedSignatureFilter.getInstitutionType());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getInstitution()))
			query.setParameter("parInstitution",authorizedSignatureFilter.getInstitution());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getSituation()))
			query.setParameter("parSituation",authorizedSignatureFilter.getSituation());
		if(Validations.validateIsNotNullAndNotEmpty(searchCriteria))
			query.setParameter("parSearchCriteria",searchCriteria);
		List<AuthorizedSignature> list = query.getResultList();
		// Convirtiendo a pojo
		if(list != null){
			AuthorizedSignatureTO authorizedSignatureTO = null;
			for (AuthorizedSignature authorizedSignature : list) {
				authorizedSignatureTO = new AuthorizedSignatureTO();
				authorizedSignatureTO.setIdAuthorizedSignaturePk(authorizedSignature.getIdAuthorizedSignaturePk());
				authorizedSignatureTO.setDocumentNumber(authorizedSignature.getDocumentNumber());
				authorizedSignatureTO.setFirtsLastName(authorizedSignature.getFirstLastName());
				authorizedSignatureTO.setSecondLastName(authorizedSignature.getSecondLastName());
				authorizedSignatureTO.setName(authorizedSignature.getName());				
				authorizedSignatureTO.setJobPosition(authorizedSignature.getJobPosition());				
				authorizedSignatureTO.setName(authorizedSignature.getName());				
				authorizedSignatureTO.setSituation(authorizedSignature.getSituation());				
				authorizedSignatureTO.setSignatureType(authorizedSignature.getSignatureType());	
				authorizedSignatureTO.setInstitutionType(authorizedSignature.getInstitutionType());				
				authorizedSignatureTO.setIdInstitutionFk(authorizedSignature.getIdInstitutionFk());
				authorizedSignatureTO.setReasonReject(authorizedSignature.getReasonReject());
				
				authorizedSignatureTO.setSignatureImg(authorizedSignature.getSignatureImg());
				authorizedSignatureTO.setSignatureName(authorizedSignature.getSignatureName());
				authorizedSignatureTO.setSignatureSize(authorizedSignature.getSignatureSize());
			    authorizedSignatureTO.setSignatureExtension(authorizedSignature.getSignatureExtension());
			    
			    authorizedSignatureTO.setFileContent(authorizedSignature.getFileContent());
				authorizedSignatureTO.setFileName(authorizedSignature.getFileName());
				authorizedSignatureTO.setFileSize(authorizedSignature.getFileSize());
			    authorizedSignatureTO.setFileExtension(authorizedSignature.getFileExtension());
								
				authorizedSignatureTO.setState(authorizedSignature.getState());				
				authorizedSignatureTO.setRegisterDate(authorizedSignature.getRegisterDate());				
				authorizedSignatureTO.setRegisterUser(authorizedSignature.getRegisterUser());
				authorizedSignatureTO.setConfirmationDate(authorizedSignature.getConfirmationDate());				
				authorizedSignatureTO.setConfirmationUser(authorizedSignature.getConfirmationUser());
				authorizedSignatureTO.setRejectDate(authorizedSignature.getRejectDate());				
				authorizedSignatureTO.setRejectUser(authorizedSignature.getRejectUser());
				authorizedSignatureTO.setLastModifyDate(authorizedSignature.getLastModifyDate());				
				authorizedSignatureTO.setLastModifyUser(authorizedSignature.getLastModifyUser());				
				authorizedSignatureTO.setLastModifyIp(authorizedSignature.getLastModifyIp());				
				authorizedSignatureTO.setLastModifyApp(authorizedSignature.getLastModifyApp());	
				
				authorizedSignatureTOList.add(authorizedSignatureTO);
			}
		}
		return authorizedSignatureTOList;
	}
		
	/**
	 * Get authorized signature list	
	 * @param authorizedSignatureFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<AuthorizedSignatureTO> getAuthorizedSignatureListValidation(AuthorizedSignatureFilter authorizedSignatureFilter)throws ServiceException{
		
		List<AuthorizedSignatureTO> authorizedSignatureTOList = new ArrayList<AuthorizedSignatureTO>();
		//Creating query
		StringBuffer sbQuery = new StringBuffer("");
		sbQuery.append(" Select asi ");
		sbQuery.append(" From AuthorizedSignature asi");
		sbQuery.append(" Where 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getState()))
			sbQuery.append(" And asi.state = :parState ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getSituation()))
			sbQuery.append(" And asi.situation = :parSituation ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getDocumentNumber()))
			sbQuery.append(" And asi.documentNumber = :parDocumentNumber ");
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getIdAuthorizedSignaturePk()))
			sbQuery.append(" And asi.idAuthorizedSignaturePk <> :parIdAuthorizedSignaturePk ");
		if(Validations.validateListIsNotNullAndNotEmpty(authorizedSignatureFilter.getListNotInState()))
			sbQuery.append(" And asi.state not in :parListNotInState ");
		// Setting parameter
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getState()))
			query.setParameter("parState",authorizedSignatureFilter.getState());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getSituation()))
			query.setParameter("parSituation",authorizedSignatureFilter.getSituation());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getDocumentNumber()))
			query.setParameter("parDocumentNumber",authorizedSignatureFilter.getDocumentNumber());
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureFilter.getIdAuthorizedSignaturePk()))
			query.setParameter("parIdAuthorizedSignaturePk",authorizedSignatureFilter.getIdAuthorizedSignaturePk());
		if(Validations.validateListIsNotNullAndNotEmpty(authorizedSignatureFilter.getListNotInState()))
			query.setParameter("parListNotInState",authorizedSignatureFilter.getListNotInState());
		List<AuthorizedSignature> list = query.getResultList();
		// Convirtiendo a pojo
		if(list != null){
			AuthorizedSignatureTO authorizedSignatureTO = null;
			for (AuthorizedSignature authorizedSignature : list) {
				authorizedSignatureTO = new AuthorizedSignatureTO();
				authorizedSignatureTO.setIdAuthorizedSignaturePk(authorizedSignature.getIdAuthorizedSignaturePk());
				authorizedSignatureTO.setDocumentNumber(authorizedSignature.getDocumentNumber());
				authorizedSignatureTO.setFirtsLastName(authorizedSignature.getFirstLastName());
				authorizedSignatureTO.setSecondLastName(authorizedSignature.getSecondLastName());
				authorizedSignatureTO.setName(authorizedSignature.getName());				
				authorizedSignatureTO.setJobPosition(authorizedSignature.getJobPosition());				
				authorizedSignatureTO.setName(authorizedSignature.getName());				
				authorizedSignatureTO.setSituation(authorizedSignature.getSituation());				
				authorizedSignatureTO.setSignatureType(authorizedSignature.getSignatureType());	
				authorizedSignatureTO.setInstitutionType(authorizedSignature.getInstitutionType());				
				authorizedSignatureTO.setIdInstitutionFk(authorizedSignature.getIdInstitutionFk());
				authorizedSignatureTO.setReasonReject(authorizedSignature.getReasonReject());
								
				authorizedSignatureTO.setState(authorizedSignature.getState());				
				authorizedSignatureTO.setRegisterDate(authorizedSignature.getRegisterDate());				
				authorizedSignatureTO.setRegisterUser(authorizedSignature.getRegisterUser());
				authorizedSignatureTO.setConfirmationDate(authorizedSignature.getConfirmationDate());				
				authorizedSignatureTO.setConfirmationUser(authorizedSignature.getConfirmationUser());
				authorizedSignatureTO.setRejectDate(authorizedSignature.getRejectDate());				
				authorizedSignatureTO.setRejectUser(authorizedSignature.getRejectUser());
				authorizedSignatureTO.setLastModifyDate(authorizedSignature.getLastModifyDate());				
				authorizedSignatureTO.setLastModifyUser(authorizedSignature.getLastModifyUser());				
				authorizedSignatureTO.setLastModifyIp(authorizedSignature.getLastModifyIp());				
				authorizedSignatureTO.setLastModifyApp(authorizedSignature.getLastModifyApp());	
				
				authorizedSignatureTOList.add(authorizedSignatureTO);
			}
		}
		return authorizedSignatureTOList;
	}
	
}
