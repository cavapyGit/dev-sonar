package com.pradera.custody.authorizedsignature.facade;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.authorizedsignature.service.AuthorizedSignatureServiceBean;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureFilter;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.authorizedSignature.AuthorizedSignature;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.AuthorizeSignatureStateType;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * 
 * The Class AuthorizedSignatureServiceFacade.
 *
 * @author equinajo.
 * @version 1.0 , 18/08/2020
 */

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class AuthorizedSignatureServiceFacade{
	
	/** The crud dato service bean. */
	@EJB
	CrudDaoServiceBean crudDatoServiceBean;
	
	/** Holds current logger user details. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** Service bean authorized signature*/
	@EJB
	AuthorizedSignatureServiceBean authorizedSignatureServiceBean;
	
	/**
	 * Find participant
	 * @param participantPk
	 * @return
	 * @throws ServiceException
	 */
	public Participant findParticipant(Long participantPk)throws ServiceException{
		return authorizedSignatureServiceBean.findParticipant(participantPk);
	}
	
	/**
	 * Find issuer
	 * @param idIssuerPk
	 * @return
	 */
	public Issuer findIssuer(String idIssuerPk){
		return authorizedSignatureServiceBean.findIssuer(idIssuerPk);
	}
	
	/**
	 * Get parameter table signature
	 * @param parameterTablePk
	 * @return
	 */
	public ParameterTable getParameterTableSignature(Integer parameterTablePk) {
		return crudDatoServiceBean.find(ParameterTable.class, parameterTablePk);
	}
	
	/**
	 * List issuer
	 * @return
	 * @throws ServiceException
	 */
	public List<Issuer> issuerList()throws ServiceException{
		return authorizedSignatureServiceBean.issuerList();
	}
	
	/**
	 * Get list search authorized signature
	 * @param authorizedSignatureFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<AuthorizedSignatureTO> getAuthorizedSignatureSearchList(AuthorizedSignatureFilter authorizedSignatureFilter)
			throws ServiceException {
		List<AuthorizedSignatureTO> authorizedSignatureTOList = authorizedSignatureServiceBean.getAuthorizedSignatureList(authorizedSignatureFilter);
		if (Validations.validateListIsNotNullAndNotEmpty(authorizedSignatureTOList)) {
			authorizedSignatureTOList = completeDateAutorizedSignatureTOList(authorizedSignatureTOList);
		}
		return authorizedSignatureTOList;
	}

	/**
	 * Complete information authorized signature to list
	 * @param authorizedSignatureTOList
	 * @return
	 * @throws ServiceException
	 */
	public List<AuthorizedSignatureTO> completeDateAutorizedSignatureTOList(
			List<AuthorizedSignatureTO> authorizedSignatureTOList)
			throws ServiceException {
		for (AuthorizedSignatureTO authorizedSignatureTOTemp : authorizedSignatureTOList) {
			if (Validations.validateIsNotNullAndNotEmpty(authorizedSignatureTOTemp.getInstitutionType())) {
				if (authorizedSignatureTOTemp.getInstitutionType().equals(
						InstitutionType.PARTICIPANT.getCode())) {
					Participant participantTemp = crudDatoServiceBean.find(Participant.class,new Long(authorizedSignatureTOTemp.getIdInstitutionFk()));
					if (Validations.validateIsNotNullAndNotEmpty(participantTemp)) {
						authorizedSignatureTOTemp.setInstitution(participantTemp.getMnemonic()+ GeneralConstants.DASH+ participantTemp.getDescription());
					}
				}
				if (authorizedSignatureTOTemp.getInstitutionType().equals(InstitutionType.ISSUER.getCode())) {
					Issuer issuerTemp = crudDatoServiceBean.find(Issuer.class,authorizedSignatureTOTemp.getIdInstitutionFk());
					if (Validations.validateIsNotNullAndNotEmpty(issuerTemp)) {
						authorizedSignatureTOTemp.setInstitution(issuerTemp.getMnemonic()+ GeneralConstants.DASH+ issuerTemp.getDescription());
					}
				}
			}
			if (Validations.validateIsNotNullAndNotEmpty(authorizedSignatureTOTemp.getState())) {
				ParameterTable parameterState = crudDatoServiceBean.find(ParameterTable.class,authorizedSignatureTOTemp.getState());
				if (Validations.validateIsNotNullAndNotEmpty(parameterState)) {
					authorizedSignatureTOTemp.setStateDesc(parameterState.getDescription());
				}
			}
		}
		return authorizedSignatureTOList;
	}

	/**
	 * Persisten authorized signature register and modify
	 * @param authorizedSignatureTO
	 * @return
	 * @throws ServiceException
	 */
	public AuthorizedSignature persistenAuthorizerSignature(AuthorizedSignatureTO authorizedSignatureTO)
			throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo AuthorizedSignatureServiceFacade.persistenAuthorizerSignature. Se procedera a setear las pistas de auditoria manualemnte");
		}
		AuthorizedSignature authorizedSignature;
		if (Validations.validateIsNullOrEmpty(authorizedSignatureTO.getIdAuthorizedSignaturePk())) {
			authorizedSignature = new AuthorizedSignature();
			authorizedSignature.setRegisterDate(new Date());
			authorizedSignature.setRegisterUser(authorizedSignatureTO.getRegisterUser());
		} else {
			authorizedSignature = crudDatoServiceBean.find(AuthorizedSignature.class,authorizedSignatureTO.getIdAuthorizedSignaturePk());
			authorizedSignature.setLastModifyDate(new Date());
		}

		authorizedSignature.setDocumentNumber(authorizedSignatureTO.getDocumentNumber());
		authorizedSignature.setFirstLastName(authorizedSignatureTO.getFirtsLastName());
		authorizedSignature.setSecondLastName(authorizedSignatureTO.getSecondLastName());
		authorizedSignature.setName(authorizedSignatureTO.getName());
		authorizedSignature.setJobPosition(authorizedSignatureTO.getJobPosition());
		authorizedSignature.setSituation(authorizedSignatureTO.getSituation());
		authorizedSignature.setSignatureType(authorizedSignatureTO.getSignatureType());
		authorizedSignature.setInstitutionType(authorizedSignatureTO.getInstitutionType());
		authorizedSignature.setIdInstitutionFk(authorizedSignatureTO.getIdInstitutionFk());

		authorizedSignature.setSignatureImg(authorizedSignatureTO.getSignatureImg());
		authorizedSignature.setSignatureName(authorizedSignatureTO.getSignatureName());
		authorizedSignature.setSignatureSize(authorizedSignatureTO.getSignatureSize());
		authorizedSignature.setSignatureExtension(authorizedSignatureTO.getSignatureExtension());
		
		authorizedSignature.setFileContent(authorizedSignatureTO.getFileContent());
		authorizedSignature.setFileName(authorizedSignatureTO.getFileName());
		authorizedSignature.setFileSize(authorizedSignatureTO.getFileSize());
		authorizedSignature.setFileExtension(authorizedSignatureTO.getFileExtension());

		authorizedSignature.setState(AuthorizeSignatureStateType.REGISTERED.getCode());
		authorizedSignature.setConfirmationDate(null);
		authorizedSignature.setConfirmationUser(null);
		authorizedSignature.setRejectDate(null);
		authorizedSignature.setRejectUser(null);
		authorizedSignature.setReasonReject(null);
		
		if (Validations.validateIsNotNullAndNotEmpty(authorizedSignatureTO.getIdAuthorizedSignaturePk()))
		authorizedSignature.setLastModifyUser(authorizedSignatureTO.getLastModifyUser());
		authorizedSignature.setLastModifyIp(authorizedSignatureTO.getLastModifyIp());
		authorizedSignature.setLastModifyApp(authorizedSignatureTO.getAuditIdPrivilege());
			
		return authorizedSignatureServiceBean.persistenAuthorizedSignature(authorizedSignature);
	}

	/**
	 * Change state authorized signature confirm and reject
	 * @param authorizedSignatureTO
	 * @return
	 * @throws ServiceException
	 */
	public AuthorizedSignature changeStateAuthorizedSignature(AuthorizedSignatureTO authorizedSignatureTO)throws ServiceException {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		try {
			if (authorizedSignatureTO.getState().equals(AuthorizeSignatureStateType.CONFIRMED.getCode()))
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			if (authorizedSignatureTO.getState().equals(AuthorizeSignatureStateType.REJECTED.getCode()))
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		} catch (Exception e) {
			System.out.println("::: no se pudo obtener el privilegio en el metodo AuthorizedSignatureServiceFacade.changeStateAuthorizedSignature. Se procedera a setear las pistas de auditoria manualemnte");
		}
		AuthorizedSignature authorizedSignature = crudDatoServiceBean.find(AuthorizedSignature.class,authorizedSignatureTO.getIdAuthorizedSignaturePk());
		authorizedSignature.setState(authorizedSignatureTO.getState());
		if (authorizedSignatureTO.getState().equals(AuthorizeSignatureStateType.CONFIRMED.getCode())){
			authorizedSignature.setConfirmationDate(new Date());
			authorizedSignature.setConfirmationUser(authorizedSignatureTO.getConfirmationUser());
		}
		if (authorizedSignatureTO.getState().equals(AuthorizeSignatureStateType.REJECTED.getCode())){
			authorizedSignature.setReasonReject(authorizedSignatureTO.getReasonReject());
			authorizedSignature.setRejectDate(new Date());
			authorizedSignature.setRejectUser(authorizedSignatureTO.getRejectUser());
		}
			
		authorizedSignature.setLastModifyDate(new Date());
		authorizedSignature.setLastModifyUser(authorizedSignatureTO.getLastModifyUser());
		authorizedSignature.setLastModifyIp(authorizedSignatureTO.getLastModifyIp());
		authorizedSignature.setLastModifyApp(authorizedSignatureTO.getAuditIdPrivilege());
		
		return authorizedSignatureServiceBean.persistenAuthorizedSignature(authorizedSignature);
	}


	/**
	 * Get list search authorized signature
	 * @param authorizedSignatureFilter
	 * @return
	 * @throws ServiceException
	 */
	public List<AuthorizedSignatureTO> getAuthorizedSignatureSearchListValidation(AuthorizedSignatureFilter authorizedSignatureFilter)
			throws ServiceException {
		List<AuthorizedSignatureTO> authorizedSignatureTOList = authorizedSignatureServiceBean.getAuthorizedSignatureListValidation(authorizedSignatureFilter);
		if (Validations.validateListIsNotNullAndNotEmpty(authorizedSignatureTOList)) {
			authorizedSignatureTOList = completeDateAutorizedSignatureTOList(authorizedSignatureTOList);
		}
		return authorizedSignatureTOList;
	}
	
}