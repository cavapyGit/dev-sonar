package com.pradera.custody.authorizedsignature.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.generalparameter.type.MasterTableStatusType;
import com.pradera.model.generalparameter.type.SignatureType;

/**
 * 
 * The Class AuthorizeSignatureTO.
 *
 * @author equinajo.
 * @version 1.0 , 18/08/2020
 */
public class AuthorizedSignatureTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Long idAuthorizedSignaturePk;
	private String documentNumber;
	private String firtsLastName;
	private String secondLastName;
	private String name;
	private String jobPosition;
	private Integer situation;
	private String signatureType;
	private Integer institutionType;
	private String idInstitutionFk;
	private String reasonReject;
	
	private byte[] signatureImg;
	private String signatureName;
	private Long signatureSize;
	private String signatureExtension;
	
	private byte[] fileContent;
	private String fileName;
	private Long fileSize;
	private String fileExtension;
	
	private Integer state;
	private Date registerDate;
	private String registerUser;
	private Date confirmationDate;
	private String confirmationUser;
	private Date rejectDate;
	private String rejectUser;
	private Date lastModifyDate;
	private String lastModifyUser;
	private String lastModifyIp;
	private Integer lastModifyApp;
	
	//Variable temporal
	private String institution;
	private String stateDesc;
	private Integer auditIdPrivilege;

	/**
	 * Obtiene el nombre completo
	 * @return
	 */
	public String getFullName(){
	      StringBuilder sb=new StringBuilder("");
	       if(Validations.validateIsNotNullAndNotEmpty(this.firtsLastName)){
	           sb.append(this.firtsLastName);
	       }
	       if(Validations.validateIsNotNullAndNotEmpty(this.secondLastName)){
	           if(Validations.validateIsNotNullAndNotEmpty(sb.toString())){
	               sb.append(" ");
	           }
	           sb.append(this.secondLastName);
	       }
	       if(Validations.validateIsNotNullAndNotEmpty(this.name)){
	           sb.append(" ");
	           sb.append(this.name);
	       }
	       return sb.toString();
	}
	
	/**
	 * Obtiene la descripcion de la situacion
	 * @return
	 */
	public String getSituationDesc(){
		String result = "";
		if(Validations.validateIsNotNullAndNotEmpty(this.situation)){
			if(this.situation.equals(MasterTableStatusType.ACTIVE.getCode())){
				result = MasterTableStatusType.ACTIVE.getValue();
			}
			if(this.situation.equals(MasterTableStatusType.INACTIVE.getCode())){
				result = MasterTableStatusType.INACTIVE.getValue();
			}
		}
		return result;
	}
	
	/**
	 * Obtiene la descripcion del tipo de firma
	 * @return
	 */
	public String getSignatureTypeDesc(){
		String result = "";
		if(Validations.validateIsNotNullAndNotEmpty(this.signatureType)){
			if(this.signatureType.equals(SignatureType.IND.toString())){
				result = SignatureType.IND.getValue();
			}
			if(this.signatureType.equals(SignatureType.CON.toString())){
				result = SignatureType.CON.getValue();
			}
		}
		return result;
	}
	
	/**
	 * Obtiene la descripcion del tipo de institucion
	 * @return
	 */
	public String getInstitutionTypeDesc(){
		String result = "";
		if(Validations.validateIsNotNullAndNotEmpty(this.institutionType)){
			if(this.institutionType.equals(InstitutionType.PARTICIPANT.getCode())){
				result = InstitutionType.PARTICIPANT.getValue();
			}
			if(this.institutionType.equals(InstitutionType.ISSUER.getCode())){
				result = InstitutionType.ISSUER.getValue();
			}
		}
		return result;
	}
	
	/**
	 * Obtiene la fecha de registro con formato
	 * @return
	 */
	public String getRegisterDateString(){
		return CommonsUtilities.convertDatetoStringForTemplate(this.registerDate,CommonsUtilities.DATE_TIME_PATTERN);
    }
	
	/**
	 * Obtiene la fecha de registro con formato
	 * @return
	 */
	public String getRegisterDateStringNotTime(){
		return CommonsUtilities.convertDatetoStringForTemplate(this.registerDate,CommonsUtilities.DATE_PATTERN);
    }
	
	/**
	 * Obtiene la fecha de modificacion con formato
	 * @return
	 */
	public String getLastModifyDateString(){
		if(this.lastModifyDate!=null)
			return CommonsUtilities.convertDatetoStringForTemplate(this.lastModifyDate,CommonsUtilities.DATE_TIME_PATTERN);
		else
			return "";
    }
	
	/**
	 * Obtiene la fecha de confirmacion con formato
	 * @return
	 */
	public String getConfirmationDateString(){
		if(this.confirmationDate!=null)
			return CommonsUtilities.convertDatetoStringForTemplate(this.confirmationDate,CommonsUtilities.DATE_TIME_PATTERN);
		else
			return "";
    }
	
	/**
	 * Obtiene la fecha de rechazo con formato
	 * @return
	 */
	public String getRejectDateString(){
		if(this.rejectDate!=null)
			return CommonsUtilities.convertDatetoStringForTemplate(this.rejectDate,CommonsUtilities.DATE_TIME_PATTERN);
		else
			return "";
    }

	public Long getIdAuthorizedSignaturePk() {
		return idAuthorizedSignaturePk;
	}

	public void setIdAuthorizedSignaturePk(Long idAuthorizedSignaturePk) {
		this.idAuthorizedSignaturePk = idAuthorizedSignaturePk;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getFirtsLastName() {
		return firtsLastName;
	}

	public void setFirtsLastName(String firtsLastName) {
		this.firtsLastName = firtsLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public Integer getSituation() {
		return situation;
	}

	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	public String getSignatureType() {
		return signatureType;
	}

	public void setSignatureType(String signatureType) {
		this.signatureType = signatureType;
	}

	public Integer getInstitutionType() {
		return institutionType;
	}

	public void setInstitutionType(Integer institutionType) {
		this.institutionType = institutionType;
	}

	public String getIdInstitutionFk() {
		return idInstitutionFk;
	}

	public void setIdInstitutionFk(String idInstitutionFk) {
		this.idInstitutionFk = idInstitutionFk;
	}

	public String getReasonReject() {
		return reasonReject;
	}

	public void setReasonReject(String reasonReject) {
		this.reasonReject = reasonReject;
	}

	public byte[] getSignatureImg() {
		return signatureImg;
	}

	public void setSignatureImg(byte[] signatureImg) {
		this.signatureImg = signatureImg;
	}

	public String getSignatureName() {
		return signatureName;
	}

	public void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}

	public Long getSignatureSize() {
		return signatureSize;
	}

	public void setSignatureSize(Long signatureSize) {
		this.signatureSize = signatureSize;
	}

	public String getSignatureExtension() {
		return signatureExtension;
	}

	public void setSignatureExtension(String signatureExtension) {
		this.signatureExtension = signatureExtension;
	}

	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getRegisterUser() {
		return registerUser;
	}

	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getConfirmationUser() {
		return confirmationUser;
	}

	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getRejectUser() {
		return rejectUser;
	}

	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	public String getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getLastModifyIp() {
		return lastModifyIp;
	}

	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public Integer getAuditIdPrivilege() {
		return auditIdPrivilege;
	}

	public void setAuditIdPrivilege(Integer auditIdPrivilege) {
		this.auditIdPrivilege = auditIdPrivilege;
	}

}
