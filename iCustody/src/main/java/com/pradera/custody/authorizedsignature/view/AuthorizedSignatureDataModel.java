package com.pradera.custody.authorizedsignature.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.custody.authorizedsignature.to.AuthorizedSignatureTO;

/**
 * DataModel de la clase AuthorizedSignatureTO
 * @author equinajo
 *
 */
public class AuthorizedSignatureDataModel extends ListDataModel<AuthorizedSignatureTO> implements SelectableDataModel<AuthorizedSignatureTO>, Serializable {

	private static final long serialVersionUID = 1L;
	
	// constructor
	public AuthorizedSignatureDataModel (){}
	
	//2do constructor
	public AuthorizedSignatureDataModel (List<AuthorizedSignatureTO> lstAuthorizedSignature){
		super(lstAuthorizedSignature);
	}

	@Override
	public AuthorizedSignatureTO getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<AuthorizedSignatureTO> lst= (List<AuthorizedSignatureTO>) getWrappedData();
		for(AuthorizedSignatureTO authSignature:lst){
			if(authSignature.getIdAuthorizedSignaturePk().toString().equals(rowKey))
				return authSignature;
		}
		return null;
	}

	@Override
	public Object getRowKey(AuthorizedSignatureTO authSignature) {
		// TODO Auto-generated method stub
		return authSignature.getIdAuthorizedSignaturePk();
	}

}
