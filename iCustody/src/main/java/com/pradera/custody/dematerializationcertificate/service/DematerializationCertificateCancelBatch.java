package com.pradera.custody.dematerializationcertificate.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.ProcessLogger;
 

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DematerializationCertificateCancelBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@RequestScoped
@BatchProcess(name="DematerializationCertificateCancelBatch")
public class DematerializationCertificateCancelBatch implements JobExecution {
	
	/** The dematerialization certificate facade. */
	@Inject
	DematerializationCertificateFacade dematerializationCertificateFacade;
	
	/** The dematerialization certificate service bean. */
	@Inject
	DematerializationCertificateServiceBean dematerializationCertificateServiceBean;
	
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		try {
			Integer days = generalParametersFacade.getMaxDayForRequestsToCancell(MasterTableType.MAX_DAYS_DEMATERIALIZATION.getCode()); 
			if(days > 0){
				this.cancellDematerialization(days);
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
		} 		
	}
	
 	/**
	  * Cancell dematerialization.
	  *
	  * @param days the days
	  * @throws ServiceException the service exception
	  */
	 public void cancellDematerialization(Integer days) throws ServiceException{
		List<Integer> paraamStates = new ArrayList<Integer>();
		paraamStates.add(DematerializationStateType.REIGSTERED.getCode());
		paraamStates.add(DematerializationStateType.APPROVED.getCode());
		List<AccountAnnotationOperation> listAccountAnnotationOperation = dematerializationCertificateServiceBean.searchAccountAnnotationOperationForBatchProcess(paraamStates);
 		
	  for(AccountAnnotationOperation accountAnnotationOperation: listAccountAnnotationOperation){
		  dematerializationCertificateFacade.annulAnnotationOperation(accountAnnotationOperation);		  
	  }
	 
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}



	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
