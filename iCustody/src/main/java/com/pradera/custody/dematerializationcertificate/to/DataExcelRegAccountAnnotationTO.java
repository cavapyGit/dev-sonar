package com.pradera.custody.dematerializationcertificate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.validation.Validations;

/** issue 1058 clase para almacenar los datos obtenidos del archivo excel para el registro ACV
 * 
 * @author rlarico */
public class DataExcelRegAccountAnnotationTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer codeResult;
	private String msgResult;

	// es string
	private String idHolder;
	private Integer accountNumber;
	private String idSecurityCode;
	private Date dateMarketfact;
	private BigDecimal rateMarkefact;
	private BigDecimal priceMarkefact;
	private String motive;
	private Integer motiveId;
	private String numberCertificate;
	private BigDecimal quantity;
	private String cellReferenceHolder;
	private String cellReferenceAccount;
	private String cellReferenceSecurity;
	private String cellReferenceQuantity;
	private BigDecimal price;
	
	private boolean containsTax;
	private String creditor;

	private List<DataExcelRegAccountAnnotationTO> lstDataExcelReult;

	private List<String> lstErrorReadExcel;

	public DataExcelRegAccountAnnotationTO() {
		this.lstDataExcelReult = new ArrayList<>();
		this.lstErrorReadExcel = new ArrayList<>();
	}

	public String getIdHolder() {
		return idHolder;
	}

	public void setIdHolder(String idHolder) {
		if(Validations.validateIsNotNull(idHolder)){
			this.idHolder = idHolder.trim();
		}else{
			this.idHolder = idHolder;
		}
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public List<String> getLstErrorReadExcel() {
		return lstErrorReadExcel;
	}

	public void setLstErrorReadExcel(List<String> lstErrorReadExcel) {
		this.lstErrorReadExcel = lstErrorReadExcel;
	}

	public String getMsgResult() {
		return msgResult;
	}

	public String getCellReferenceHolder() {
		return cellReferenceHolder;
	}

	public boolean isContainsTax() {
		return containsTax;
	}

	public void setContainsTax(boolean containsTax) {
		this.containsTax = containsTax;
	}

	public String getCreditor() {
		return creditor;
	}

	public void setCreditor(String creditor) {
		if(Validations.validateIsNotNull(creditor)){
			this.creditor = creditor.trim();
		}else{
			this.creditor = creditor;
		}
	}

	public String getCellReferenceQuantity() {
		return cellReferenceQuantity;
	}

	public void setCellReferenceQuantity(String cellReferenceQuantity) {
		this.cellReferenceQuantity = cellReferenceQuantity;
	}

	public void setCellReferenceHolder(String cellReferenceHolder) {
		this.cellReferenceHolder = cellReferenceHolder;
	}

	public String getCellReferenceAccount() {
		return cellReferenceAccount;
	}

	public void setCellReferenceAccount(String cellReferenceAccount) {
		this.cellReferenceAccount = cellReferenceAccount;
	}

	public String getCellReferenceSecurity() {
		return cellReferenceSecurity;
	}

	public void setCellReferenceSecurity(String cellReferenceSecurity) {
		this.cellReferenceSecurity = cellReferenceSecurity;
	}

	public void setMsgResult(String msgResult) {
		this.msgResult = msgResult;
	}

	public Integer getMotiveId() {
		return motiveId;
	}

	public void setMotiveId(Integer motiveId) {
		this.motiveId = motiveId;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getCodeResult() {
		return codeResult;
	}

	public void setCodeResult(Integer codeResult) {
		this.codeResult = codeResult;
	}

	public List<DataExcelRegAccountAnnotationTO> getLstDataExcelReult() {
		return lstDataExcelReult;
	}

	public void setLstDataExcelReult(List<DataExcelRegAccountAnnotationTO> lstDataExcelReult) {
		this.lstDataExcelReult = lstDataExcelReult;
	}

	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	public void setIdSecurityCode(String idSecurityCode) {
		if(Validations.validateIsNotNull(idSecurityCode)){
			this.idSecurityCode = idSecurityCode.trim();
		}else{
			this.idSecurityCode = idSecurityCode;
		}
	}

	public Date getDateMarketfact() {
		return dateMarketfact;
	}

	public void setDateMarketfact(Date dateMarketfact) {
		this.dateMarketfact = dateMarketfact;
	}

	public BigDecimal getRateMarkefact() {
		return rateMarkefact;
	}

	public void setRateMarkefact(BigDecimal rateMarkefact) {
		this.rateMarkefact = rateMarkefact;
	}

	public BigDecimal getPriceMarkefact() {
		return priceMarkefact;
	}

	public void setPriceMarkefact(BigDecimal priceMarkefact) {
		this.priceMarkefact = priceMarkefact;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		if(Validations.validateIsNotNull(motive)){
			this.motive = motive.trim();
		}else{
			this.motive = motive;
		}
	}

	public String getNumberCertificate() {
		return numberCertificate;
	}

	public void setNumberCertificate(String numberCertificate) {
		if(Validations.validateIsNotNull(numberCertificate)){
			this.numberCertificate = numberCertificate.trim();
		}else{
			this.numberCertificate = numberCertificate;
		}
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "DataExcelRegAccountAnnotationTO [idHolder=" + idHolder + ", accountNumber=" + accountNumber + ", idSecurityCode=" + idSecurityCode + ", dateMarketfact="
				+ dateMarketfact + ", rateMarkefact=" + rateMarkefact + ", priceMarkefact=" + priceMarkefact + ", motive=" + motive + ", numberCertificate="
				+ numberCertificate + ", quantity=" + quantity + "]";
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
