package com.pradera.custody.dematerializationcertificate.view;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.dematerializationcertificate.to.DataExcelRegAccountAnnotationTO;
import com.pradera.custody.dematerializationcertificate.to.RegisterAccountAnnotationTO;
import com.pradera.custody.dematerializationcertificate.to.SearchAccountAnnotationTO;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.custody.to.XmlRespuestaActivoFinanciero;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.securitiesannotation.AccAnnotationOperationFile;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationRejectMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DematerializationCertificateBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class DematerializationCertificateBeanOriginal extends GenericBaseBean {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private String titleLabel;
	
	/** The search account annotation to. */
	private SearchAccountAnnotationTO searchAccountAnnotationTO;
	
	/** The register account annotation to. */
	private RegisterAccountAnnotationTO registerAccountAnnotationTO;
	//este objeto solo se lo utiliza para las validaciones
	private RegisterAccountAnnotationTO regAnnotationTOAux;
	
	/** The selected account annotation opertaion. */
//	private AccountAnnotationOperation selectedAccountAnnotationOpertaion;
	private String massiveInterfaceType = ComponentConstant.INTERFACE_ACCOUNT_ENTRY_MASSIVE;
	
	/** The check account anotation. */
	private AccountAnnotationOperation[] checkAccountAnotation;
	
	/** The bl issuer. */
	private boolean blParticipant, blIssuer, blBlocIssuer;
	
	/** The bl reject. */
	private boolean blApprove, blAnnul, blConfirm, blReject, blReview, blCertify, blCancel;
	
	/** The bl certificate disable. */
	private boolean blCertificateDisable;
	
	/** The bl disabled fields. */
	private boolean blDisabledFields ;
	
	/** The ind alternative code. */
	private boolean indAlternativeCode;
	
	/** The bl rendered motives. */
	private boolean blRenderedMotives;
	
	/** The ind lien. */
	private boolean indValidator,indLien;
	
	/** The bl error. */
	private boolean blError = Boolean.FALSE;
	
	/** The Constant VIEW_MAPPING. */
	private static final String VIEW_MAPPING = "viewDematerializacionSecurities";
	
	/** The Constant REGISTER_MAPPING. */
	private static final String REGISTER_MAPPING = "registerDematerializacionSecurities";
	
	/** The mp mnemonic class sec. */
	private Map<Integer, String> mpStates, mpMotive, mpMnemonicClassSec;
	
	/** The list geographic location dpto. */
	private List<GeographicLocation>  listGeographicLocationDpto;
	
	/** The list geographic location province. */
	private List<GeographicLocation>  listGeographicLocationProvince;
	
	/** The list geographic location municipality. */
	private List<GeographicLocation>  listGeographicLocationMunicipality;
	
	/** The reg lst physical cert marketfacts. from data base */
	private List<HolderMarketFactBalance> listHolderMarketFactBalance = new ArrayList<HolderMarketFactBalance>();
	
	/** The reg lst physical cert marketfacts. from data base */
	private HolderMarketFactBalance holderMarketFactBalance = new HolderMarketFactBalance();
	
	private List<RegisterAccountAnnotationTO> lstRegisterAccountAnnotationTOExcel;
	
	private AccAnnotationOperationFile fileExcelPersist;
	
	private List<String> lstErrorReadExcel;
	
	/** issue 1398: de acuerdo a requerimiento de usuario, obliga llenar el campo 'Nro de certificado' solo cuando el motivo es 'Desmaterializacion de un Fisico' */
	private boolean requiredCertificateNumber;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;	
	
	/** The department residence. */
	@Inject @Configurable 
	Integer departmentDefault;
	
	/** The province residence. */
	@Inject @Configurable 
	Integer provinceDefault;
	
	/** The municipality residence. */
	@Inject @Configurable 
	Integer districtDefault;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The dematerialization certificate facade. */
	@Inject
	DematerializationCertificateFacade dematerializationCertificateFacade;
	
	/** The accounts facade. */
	@Inject
	AccountsFacade accountsFacade;
	
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The security. */
	private Security security;
	
	/** The param service bean. */
	@EJB
	private ParameterServiceBean paramServiceBean;
	
	/** The certificate deposite bean facade. */
	@EJB
	private CertificateDepositeServiceFacade certificateDepositeBeanFacade;

	/** The issuer search pk. */
	private String issuerSearchPK;

	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	private boolean broker;
	
	private boolean validDematerialized;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		
		// issue 1398: a requerimiento de usuario ya no se valida el estado CERTIFICADO para DPF o DPA 
		userPrivilege.getUserAcctions().setReview(false);
		userPrivilege.getUserAcctions().setCertify(false);
		userPrivilege.getUserAcctions().setCancel(false);
		
		lstErrorReadExcel=new ArrayList<>();
		searchAccountAnnotationTO = new SearchAccountAnnotationTO();
		searchAccountAnnotationTO.setSearchType(BooleanType.YES.getCode());
		searchAccountAnnotationTO.setBlDefaultSearch(true);
		searchAccountAnnotationTO.setSecurity(new Security());
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		searchAccountAnnotationTO.setHolder(new Holder());
		searchAccountAnnotationTO.setIssuer(new Issuer());
		try {
			searchAccountAnnotationTO.setLstIssuers(dematerializationCertificateFacade.lstIssuersStateRegistered());
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				blParticipant = true;
				Participant participant = dematerializationCertificateFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				searchAccountAnnotationTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				List<Participant> lstParticipant = new ArrayList<Participant>();
				lstParticipant.add(participant);
				searchAccountAnnotationTO.setLstParticipants(lstParticipant);
			}else{
				if(userInfo.getUserAccountSession().isIssuerInstitucion()){
					blIssuer = true;
					searchAccountAnnotationTO.setIssuer(dematerializationCertificateFacade.getIssuer(userInfo.getUserAccountSession().getIssuerCode()));
					searchAccountAnnotationTO.setLstIssuers(new ArrayList<Issuer>());
				}
				Participant filterPar = new Participant();
				filterPar.setState(ParticipantStateType.REGISTERED.getCode());
				searchAccountAnnotationTO.setLstParticipants(accountsFacade.getLisParticipantServiceBean(filterPar));
			}
			fillCombos();
			loadCboFilterCurency();
			listHolidays();
			showPrivilegeButtons();
			blError = Boolean.FALSE;
			broker=true;
			lstRegisterAccountAnnotationTOExcel=new ArrayList<>();
			fileExcelPersist = new AccAnnotationOperationFile();
			this.titleLabel = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_MASSIVE_LABEL);
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * issue 1398: Si el motivo es DESMA POR FISICO, entonces solo en ese caso el campo 'nro de certificdo es requerido'
	 */
	public void changeMotive(){
		requiredCertificateNumber = false;
		if( Validations.validateIsNotNull(registerAccountAnnotationTO) 
			&& Validations.validateIsNotNull(registerAccountAnnotationTO.getAccountAnnotationOperation())
			&& Validations.validateIsNotNullAndPositive(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive())
			&& registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode()) ){
			requiredCertificateNumber = true;
		}
	}
	
	/**
	 * Prellenado de combos LaPaz-Murillo-NuestraSenoraDeLaPaz.
	 *
	 * @throws ServiceException the service exception
	 */
	public void ubicacionGeografica() throws ServiceException {
		
		registerAccountAnnotationTO.setDepartment(departmentDefault);
		registerAccountAnnotationTO.setProvince(provinceDefault);
		registerAccountAnnotationTO.setMunicipality(districtDefault);
		
		GeographicLocationTO filter = new GeographicLocationTO();
		
		filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		filter.setIdLocationReferenceFk(countryResidence);
		listGeographicLocationDpto = new ArrayList<GeographicLocation>();
		listGeographicLocationDpto = dematerializationCertificateFacade.listaUbiccioGeografica(filter);

		filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		filter.setIdLocationReferenceFk(departmentDefault);
		listGeographicLocationProvince = new ArrayList<GeographicLocation>();
		listGeographicLocationProvince = dematerializationCertificateFacade.listaUbiccioGeografica(filter);

		filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		filter.setIdLocationReferenceFk(provinceDefault);
		listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
		listGeographicLocationMunicipality = dematerializationCertificateFacade.listaUbiccioGeografica(filter);

	}
	
	/**
	 * Metodo ejecutado al cambiar de departamento.
	 *
	 * @throws ServiceException the service exception
	 */
	public void ubicacionGeograficaByProvince() throws ServiceException {
		
		if (registerAccountAnnotationTO.getDepartment()==null){
			listGeographicLocationProvince = new ArrayList<GeographicLocation>();
			listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
		}else{
			registerAccountAnnotationTO.setProvince(null);
			registerAccountAnnotationTO.setMunicipality(null);
			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
			filter.setIdLocationReferenceFk(registerAccountAnnotationTO.getDepartment());
			
			listGeographicLocationProvince = new ArrayList<GeographicLocation>();
			listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
			listGeographicLocationProvince = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		}
	}
	
	/**
	 * Metodo ejecutado al cambiar de provincia.
	 *
	 * @throws ServiceException the service exception
	 */
	public void ubicacionGeograficaByMunicipality() throws ServiceException {
		registerAccountAnnotationTO.setMunicipality(null);
		GeographicLocationTO filter = new GeographicLocationTO();
		filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		filter.setIdLocationReferenceFk(registerAccountAnnotationTO.getProvince());
		listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
		listGeographicLocationMunicipality = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
	}
	
	
	/**
	 * Clear search.
	 */
	public void clearSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchAccountAnnotationTO.setSearchType(BooleanType.YES.getCode());
		searchAccountAnnotationTO.setBlDefaultSearch(true);
		searchAccountAnnotationTO.setAnnotationMotive(null);
		searchAccountAnnotationTO.setAnnotationState(null);
		searchAccountAnnotationTO.setCertificateNumber(null);
		searchAccountAnnotationTO.setSerialNumber(null);
		searchAccountAnnotationTO.setRequestNumber(null);
		searchAccountAnnotationTO.setStartDate(getCurrentSystemDate());
		searchAccountAnnotationTO.setEndDate(getCurrentSystemDate());
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		searchAccountAnnotationTO.setHolder(new Holder());
		searchAccountAnnotationTO.setSecurity(new Security());
		if(!blParticipant){
			searchAccountAnnotationTO.setIdParticipant(null);
		}
	}
	
	/**
	 * Change participant search.
	 */
	public void changeParticipantSearch(){
		searchAccountAnnotationTO.setHolder(new Holder());
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		cleanResults();
	}
	
	/**
	 * Change holder.
	 */
	public void changeHolder(){
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
	}

	/**
	 * Clean results.
	 */
	public void cleanResults(){
		checkAccountAnotation = null;
		searchAccountAnnotationTO.setLstAccountAnnotationOperations(null);
		searchAccountAnnotationTO.setBlNoResult(false);
	}
	
	/**
	 * Search by.
	 */
	public void searchBy(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		if(BooleanType.YES.getCode().equals(searchAccountAnnotationTO.getSearchType())){
			searchAccountAnnotationTO.setBlDefaultSearch(true);
			searchAccountAnnotationTO.setRequestNumber(null);
		}else{
			searchAccountAnnotationTO.setBlDefaultSearch(false);
			if(!blParticipant)
				searchAccountAnnotationTO.setIdParticipant(null);
			searchAccountAnnotationTO.setStartDate(getCurrentSystemDate());
			searchAccountAnnotationTO.setEndDate(getCurrentSystemDate());
			searchAccountAnnotationTO.setAnnotationMotive(null);
			searchAccountAnnotationTO.setAnnotationState(null);
			searchAccountAnnotationTO.setCertificateNumber(null);
			searchAccountAnnotationTO.setSerialNumber(null);
			searchAccountAnnotationTO.setRequestNumber(null);
			searchAccountAnnotationTO.setSecurity(new Security());
			searchAccountAnnotationTO.setHolder(new Holder());
			searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		}
	}
	
	/**
	 * Search request.
	 */
	@LoggerAuditWeb 
	public void searchRequest(){
		try{
			cleanResults();
			searchAccountAnnotationTO.setBlNoResult(true);
			List<AccountAnnotationOperation> lstResult = dematerializationCertificateFacade.searchAccountAnnotationOperationOriginal(searchAccountAnnotationTO, blParticipant, blIssuer);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for (int i = 0; i < lstResult.size(); i++) {
					BigDecimal physicalAvailable=BigDecimal.ZERO;
					
					if(IssuanceType.DEMATERIALIZED.getCode().equals(lstResult.get(i).getSecurity().getIssuanceForm())){
						physicalAvailable=lstResult.get(i).getSecurity().getPhysicalBalance();
					}else{
						physicalAvailable=CommonsUtilities.subtractTwoDecimal(lstResult.get(i).getSecurity().getPhysicalBalance(), lstResult.get(i).getSecurity().getPhysicalDepositBalance(),1);
					}
					
					String classType = paramServiceBean.getParameterTableById(lstResult.get(i).getSecurity().getSecurityClass()).getParameterName();
					lstResult.get(i).getSecurity().setClassTypeDescription(classType);
					lstResult.get(i).getSecurity().setPhysicalAvailable(physicalAvailable);
					
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(lstResult.get(i).getHolderAccount().getIdHolderAccountPk());
					holderAccount = dematerializationCertificateFacade.getHolderAccount(holderAccount);
					lstResult.get(i).setHolderAccount(holderAccount);
				}
				searchAccountAnnotationTO.setLstAccountAnnotationOperations(new GenericDataModel<AccountAnnotationOperation>(setDescriptions(lstResult)));
				searchAccountAnnotationTO.setBlNoResult(false);
			}
		}catch(ServiceException se){
			excepcion.fire( new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * New request.
	 *
	 * @return the string
	 */
	public String newRequest(){
		try {
			lstErrorReadExcel = new ArrayList<>();
			fileExcelPersist = new AccAnnotationOperationFile();
			lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
			registerAccountAnnotationTO = new RegisterAccountAnnotationTO();
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
				registerAccountAnnotationTO.setBlMarketFact(true);
			registerAccountAnnotationTO.setAccountAnnotationOperation(new AccountAnnotationOperation());
			registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
			registerAccountAnnotationTO.setHolder(new Holder());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(new HolderAccount());
			blBlocIssuer = false;
			if(blParticipant){
				registerAccountAnnotationTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			} else if(blIssuer) {				
				String strIssuer = userInfo.getUserAccountSession().getIssuerCode();
				Participant objParticipant = dematerializationCertificateFacade.geParticipantByIssuer(strIssuer);
				if(objParticipant != null){
					blBlocIssuer = true;
					registerAccountAnnotationTO.setIdParticipantPk(objParticipant.getIdParticipantPk());
				} 
			}				
			registerAccountAnnotationTO.setLstParticipants(searchAccountAnnotationTO.getLstParticipants());
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setIndicator1("DESMATERIA");
			paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			registerAccountAnnotationTO.setLstMotives(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			
			paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			registerAccountAnnotationTO.setLstSecurityClass(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
			registerAccountAnnotationTO.setBlCertificateNumber(false);

			ubicacionGeografica();
//			ubicacionGeograficaByDpto();
//			ubicacionGeograficaByProvince();
//			ubicacionGeograficaByMunicipality();
			blError = Boolean.FALSE;
			if (userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.PARTICIPANT.getCode())){
				broker=true;
			}else{
				broker=false;
			}
			
			return REGISTER_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Change participant.
	 */
	public void changeParticipant(){
		registerAccountAnnotationTO.setHolder(new Holder());
		registerAccountAnnotationTO.setLstHolderAccount(null);
	}
	
	/**
	 * issue 1058: metodo para calcular capital social de carga masiva Excel ACV este metodo esta basado en el metodo: calculateShareCapital
	 * @param cellReferences
	 * @param registerAccountAnnotationTO
	 * @return true si tiene alguna observacion
	 */
	public boolean calculateShareCapitalFromExcel(String cellReferences){
		regAnnotationTOAux.setShareCapital(null);
		if(Validations.validateIsNullOrEmpty(regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk())){
			regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_EMPTY));
			JSFUtilities.showSimpleValidationDialog();
			return true;
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance())
				&& regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance().compareTo(BigDecimal.ZERO) == 1){
			if(!regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getIssuanceForm()
					.equals(IssuanceType.DEMATERIALIZED.getCode())){
				BigDecimal physicalBalance = regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getPhysicalBalance();
				if(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance().compareTo(physicalBalance) == 1){
					regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage("alt.accountannotation.physical.balance.not.enough.from.excel", new Object[] { cellReferences } ));
//					JSFUtilities.showSimpleValidationDialog();
					blError = true;
//					return true;
					
					lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.physical.balance.not.enough.from.excel", new Object[] { cellReferences } ));
				}else{
					BigDecimal nominalValue = regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getCurrentNominalValue();
					regAnnotationTOAux.setShareCapital(nominalValue.multiply(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance()));
					if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
						regAnnotationTOAux.getAccountAnnotationMarketFact().setOperationQuantity(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance());
				}
			}else{
				BigDecimal nominalValue = regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getCurrentNominalValue();
				Issuance issuance = dematerializationCertificateFacade.findIssuanceAndIssuer(regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
				
				List<AccountAnnotationOperation> lstAccountAnnotationOperation = dematerializationCertificateFacade.getAccountAnnotationOperationValidate(
						regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
				BigDecimal bigAcumulateTotal = BigDecimal.ZERO;
				if(Validations.validateIsNotNullAndNotEmpty(lstAccountAnnotationOperation)){
					for(AccountAnnotationOperation objAccountAnnotationOperation : lstAccountAnnotationOperation){
						if(Validations.validateIsNotNullAndNotEmpty(objAccountAnnotationOperation.getTotalBalance())){
							bigAcumulateTotal = bigAcumulateTotal.add(objAccountAnnotationOperation.getTotalBalance());
						}						
					}
				}
				
				if(nominalValue.multiply(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance()).compareTo(issuance.getIssuanceAmount()) > 0){
					regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage("alt.accountannotation.issuance.balance.not.enough.from.excel", new Object[] { cellReferences }));
//					JSFUtilities.showSimpleValidationDialog();
					blError = true;
//					return true;
					lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.issuance.balance.not.enough.from.excel", new Object[] { cellReferences }));
				} else {
					BigDecimal bigQuantity = regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance().add(bigAcumulateTotal);
					if(nominalValue.multiply(bigQuantity).compareTo(issuance.getIssuanceAmount()) > 0){
						regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//								, PropertiesUtilities.getMessage("alt.accountannotation.issuance.balance.not.enough.acumulate.from.excel", new Object[] { cellReferences }));
//						JSFUtilities.showSimpleValidationDialog();
						blError = true;
//						return true;
						lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.issuance.balance.not.enough.acumulate.from.excel", new Object[] { cellReferences }));
					} else {
						regAnnotationTOAux.setShareCapital(nominalValue.multiply(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance()));
						if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
							regAnnotationTOAux.getAccountAnnotationMarketFact().setOperationQuantity(regAnnotationTOAux.getAccountAnnotationOperation().getTotalBalance());
					}										
				}								
			}			
		}else{
			regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
		}
		return false;
	}
	
	/**
	 * Calculate share capital.
	 */
	public void calculateShareCapital(){
		registerAccountAnnotationTO.setShareCapital(null);
		if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk())){
			registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_EMPTY));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance())
				&& registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().compareTo(BigDecimal.ZERO) == 1){
			if(!registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIssuanceForm()
					.equals(IssuanceType.DEMATERIALIZED.getCode())){
				BigDecimal physicalBalance = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPhysicalBalance();
				/*if(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().compareTo(physicalBalance) == 1){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PHYSICAL_NOT_ENOUGH));
					JSFUtilities.showSimpleValidationDialog();
					blError = true;
				}else*/{
					BigDecimal nominalValue = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getCurrentNominalValue();
					registerAccountAnnotationTO.setShareCapital(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()));
					if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
						registerAccountAnnotationTO.getAccountAnnotationMarketFact().setOperationQuantity(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
				}
			}else{
				BigDecimal nominalValue = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getCurrentNominalValue();
				Issuance issuance = dematerializationCertificateFacade.findIssuanceAndIssuer(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
				
				List<AccountAnnotationOperation> lstAccountAnnotationOperation = dematerializationCertificateFacade.getAccountAnnotationOperationValidate(
						registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
				BigDecimal bigAcumulateTotal = BigDecimal.ZERO;
				if(Validations.validateIsNotNullAndNotEmpty(lstAccountAnnotationOperation)){
					for(AccountAnnotationOperation objAccountAnnotationOperation : lstAccountAnnotationOperation){
						if(Validations.validateIsNotNullAndNotEmpty(objAccountAnnotationOperation.getTotalBalance())){
							bigAcumulateTotal = bigAcumulateTotal.add(objAccountAnnotationOperation.getTotalBalance());
						}						
					}
				}
				
				if(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()).compareTo(issuance.getIssuanceAmount()) > 0){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH));
					JSFUtilities.showSimpleValidationDialog();
					blError = true;
				} else {
					BigDecimal bigQuantity = registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().add(bigAcumulateTotal);
					if(nominalValue.multiply(bigQuantity).compareTo(issuance.getIssuanceAmount()) > 0){
						registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH_ACCUMULATE));
						JSFUtilities.showSimpleValidationDialog();
						blError = true;
					} else {
						registerAccountAnnotationTO.setShareCapital(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()));
						if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
							registerAccountAnnotationTO.getAccountAnnotationMarketFact().setOperationQuantity(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
					}										
				}								
			}			
		}else{
			registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
		}
	}
	
	/**
	 * Validate certificate aprobe.
	 *
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean ValidateCertificateAprobe() throws ServiceException{
		AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
			PhysicalBalanceDetail physicalBalanceDetail = 
				dematerializationCertificateFacade.validateSecurityCertificate(selectedAAO.getSecurity().getIdSecurityCodePk(), 
															selectedAAO.getCertificateNumber());
			if (Validations.validateIsNotNull(physicalBalanceDetail)){
			System.out.println("=====================SI EXISTE EN FISICA=======================");
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, 
							new Object[]{selectedAAO.getCertificateNumber()}));
			JSFUtilities.showSimpleValidationDialog();
			return true;	
			}else{
			return false;
			}
			
			
	}
	
	
	/**
	 * Validate holder.
	 */
	
	public void validateHolder(){
		try {
			registerAccountAnnotationTO.setLstHolderAccount(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(new HolderAccount());
			
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getHolder()) && Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getHolder().getIdHolderPk())){		
				if(!HolderStateType.REGISTERED.getCode().equals(registerAccountAnnotationTO.getHolder().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_INVALID_STATE));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolder(new Holder());
					return;
				}
				if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getIdParticipantPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolder(new Holder());				
				}else{
					List<HolderAccount> lstResult = dematerializationCertificateFacade.findHolderAccounts(registerAccountAnnotationTO.getHolder().getIdHolderPk()
																			, registerAccountAnnotationTO.getIdParticipantPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						if(lstResult.size()==GeneralConstants.ONE_VALUE_INTEGER){
							HolderAccount holderAccount=lstResult.get(0);
							registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(holderAccount);
							
						}
						registerAccountAnnotationTO.setLstHolderAccount(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerAccountAnnotationTO.setHolder(new Holder());	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Validate Duplication Certificate.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateDuplicationOfCertificate() throws ServiceException{
//	    String securityPk= registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk();
//	    List<AccountAnnotationOperation> lstAccountsAO= dematerializationCertificateFacade.findAccountAnnotationOperationBySecurity(securityPk);
//	    if(lstAccountsAO!=null && lstAccountsAO.size()>0){
//	    	for(AccountAnnotationOperation annotation: lstAccountsAO){
//	    		if(annotation.getCertificateNumber().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber())){
//	    			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
//	    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
//							PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_SAME_SECURITY, new Object[]{securityPk}));
//	    			JSFUtilities.showSimpleValidationDialog();
//	    			break;
//	    		}
//	    	}
//	    }
		
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber())){
			PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
												registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk(), 
												registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber());
				if (Validations.validateIsNotNull(physicalBalanceDetail)){
					System.out.println("=====================SI EXISTE EN FISICA=======================");
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, 
									new Object[]{registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber(), 
									registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk()}));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
					return;	
				} else {
					System.out.println("=================NO EXISTE EN FISICA==================");	

				    List<Integer> states = new ArrayList<Integer>();
					states.add(DematerializationStateType.REIGSTERED.getCode());
					states.add(DematerializationStateType.APPROVED.getCode());
					states.add(DematerializationStateType.CONFIRMED.getCode());	
					states.add(DematerializationStateType.CERTIFIED.getCode());
					states.add(DematerializationStateType.REVIEWED.getCode());
					Integer requestState = dematerializationCertificateFacade.existOtherRequest(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity(),states,
							registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber());
					if(Validations.validateIsNotNullAndNotEmpty(requestState) && 
							(DematerializationStateType.REIGSTERED.getCode().equals(requestState)
							||	DematerializationStateType.APPROVED.getCode().equals(requestState)
							||	DematerializationStateType.CONFIRMED.getCode().equals(requestState)
							||	DematerializationStateType.CERTIFIED.getCode().equals(requestState)
							|| DematerializationStateType.REVIEWED.getCode().equals(requestState))){
						Object[] bodyData = new Object[2];
						   bodyData[0] = registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber();
						   bodyData[1] = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk();   
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_SAME_SECURITY, 
										bodyData));
						registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
						JSFUtilities.showSimpleValidationDialog();
						return;	
					}
			}
		}		
			
	}
	
	/**
	 * issue 1058: metodo para realizar validaciones del valor (carga masiva desde excel AVC) este metodo esta basda en el metodo: validateSecurity
	 * @param cellReferencesSecurity
	 * @param cellRefQuantity
	 * @param registerAccountAnnotationTO
	 * @return true si tiene alguna observacion
	 * @throws ServiceException
	 */
	public boolean validateSecurityFromExcel(String cellReferencesSecurity, String cellRefQuantity) throws ServiceException{
		regAnnotationTOAux.setBlDPF(false);
		regAnnotationTOAux.setBlFixedIncome(false);
		regAnnotationTOAux.setBlCertificateNumber(false);
		regAnnotationTOAux.getAccountAnnotationOperation().setExpeditionDate(null);
		regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
		regAnnotationTOAux.setShareCapital(null);
		//filling market fact data
		regAnnotationTOAux.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		Security security = regAnnotationTOAux.getAccountAnnotationOperation().getSecurity();
		
		//validate dematerialized 
		if(Validations.validateIsNotNull(security)
				&& Validations.validateIsNotNull(security.getIssuanceForm())){
			
			BigDecimal physicalAvailable=BigDecimal.ZERO;
			if(IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuanceForm())){
				physicalAvailable=security.getPhysicalBalance();
			}else{
				physicalAvailable=CommonsUtilities.subtractTwoDecimal(security.getPhysicalBalance(), security.getPhysicalDepositBalance(),1);
			}
			security.setPhysicalAvailable(physicalAvailable);
			if(security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
				blCertificateDisable=true;
			}
			else{
				blCertificateDisable=false;
			}
		}
		else{
			blCertificateDisable=false;
		}
			
		
		//valdar si esta depositado en la 
		
		//JSFUtilities.resetComponent("frmDematerializationCertificate:txtExpedition");
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass())||SecurityClassType.DPA.getCode().equals(security.getSecurityClass())){
				List<Integer> states = new ArrayList<Integer>();
				states.add(DematerializationStateType.REIGSTERED.getCode());
				states.add(DematerializationStateType.APPROVED.getCode());
				states.add(DematerializationStateType.CONFIRMED.getCode());	
				states.add(DematerializationStateType.CERTIFIED.getCode());
				states.add(DematerializationStateType.REVIEWED.getCode());
				Integer requestState = dematerializationCertificateFacade.existOtherRequest(security,states,null);
				if(Validations.validateIsNotNullAndNotEmpty(requestState) && 
						(DematerializationStateType.REIGSTERED.getCode().equals(requestState)
						||	DematerializationStateType.APPROVED.getCode().equals(requestState)
						||	DematerializationStateType.CERTIFIED.getCode().equals(requestState)
						|| DematerializationStateType.REVIEWED.getCode().equals(requestState))){
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage("alt.accountannotation.dpf.exist.from.excel", new Object[] { cellReferencesSecurity }));
//					JSFUtilities.showSimpleValidationDialog();
//					regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
//					return true;	
					
					lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.dpf.exist.from.excel", new Object[] { cellReferencesSecurity }));
				}
				else if(DematerializationStateType.CONFIRMED.getCode().equals(requestState)){
//					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//							, PropertiesUtilities.getMessage("alt.accountannotation.dpf.exist.dematerialized.from.excel", new Object[] { cellReferencesSecurity }));
//					JSFUtilities.showSimpleValidationDialog();
//					regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
//					return true;
					
					lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.dpf.exist.dematerialized.from.excel", new Object[] { cellReferencesSecurity }));
				}							
			}
			if(!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getMessage("alt.accountannotation.security.blocked.from.excel", new Object[] { cellReferencesSecurity }));
//				JSFUtilities.showSimpleValidationDialog();
//				regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
//				return true;
				lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.security.blocked.from.excel", new Object[] { cellReferencesSecurity }));
			}
			if(SecuritySourceType.FOREIGN.getCode().equals(security.getSecuritySource())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getMessage("alt.accountannotation.security.foreign.from.excel", new Object[] { cellReferencesSecurity }));
//				JSFUtilities.showSimpleValidationDialog();
//				regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
//				return true;
				lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.security.foreign.from.excel", new Object[] { cellReferencesSecurity }));
			}
			Issuance issuance = dematerializationCertificateFacade.findIssuanceAndIssuer(security.getIdSecurityCodePk());
			security.setIssuance(issuance);
			security.setIssuer(issuance.getIssuer());
			regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().setIssuer(issuance.getIssuer());
			if(IssuanceType.PHYSICAL.getCode().equals(issuance.getIssuanceType())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getMessage("alt.accountannotation.security.dematerialized.from.excel", new Object[] { cellReferencesSecurity }));
//				JSFUtilities.showSimpleValidationDialog();
//				regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
//				return true;
				lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.security.dematerialized.from.excel", new Object[] { cellReferencesSecurity }));
			}
			if(!IssuerStateType.REGISTERED.getCode().equals(issuance.getIssuer().getStateIssuer())){
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						, PropertiesUtilities.getMessage("alt.accountannotation.issuer.blocked.from.excel", new Object[] { cellReferencesSecurity }));
//				JSFUtilities.showSimpleValidationDialog();
//				regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
//				return true;
				lstErrorReadExcel.add(PropertiesUtilities.getMessage("alt.accountannotation.issuer.blocked.from.excel", new Object[] { cellReferencesSecurity }));
			}
			if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType())){
				regAnnotationTOAux.setBlFixedIncome(true);
//				regAnnotationTOAux.setBlVariableIncome(true);
			}
//			else if(InstrumentType.VARIABLE_INCOME.getCode().equals(security.getInstrumentType())){
//				regAnnotationTOAux.setBlVariableIncome(true);
//			}
				
			if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass()) || SecurityClassType.DPA.getCode().equals(security.getSecurityClass())){
				regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(BigDecimal.ONE);
				regAnnotationTOAux.getAccountAnnotationOperation().setSerialNumber(security.getSecuritySerial());
				regAnnotationTOAux.setBlDPF(true);	
				regAnnotationTOAux.setBlCertificateNumber(true);
				calculateShareCapitalFromExcel(cellRefQuantity);
				if(blError){
					regAnnotationTOAux.getAccountAnnotationOperation().setSecurity(new Security());
					regAnnotationTOAux.getAccountAnnotationOperation().setTotalBalance(null);
					regAnnotationTOAux.getAccountAnnotationOperation().setSerialNumber(null);
					regAnnotationTOAux.setBlDPF(false);	
					regAnnotationTOAux.setBlCertificateNumber(false);
					blError = Boolean.FALSE;
					return true;
				}
				//get the positions from 5 to 10 and if there are zeros forward then delete them
				String certificateNumber=null;
				if(security.getIdSecurityCodePk().length()>=10)
				{
					certificateNumber= security.getIdSecurityCodeOnly().substring(4,10); 
					if(Validations.validateIsNumeric(certificateNumber))
					{
						Integer iCertificateNumber=Integer.parseInt(certificateNumber);
						certificateNumber=String.valueOf(iCertificateNumber);
					}
				}
				else{
					certificateNumber= security.getIdSecurityCodeOnly().substring(4,security.getIdSecurityCodePk().length()); 
				}
				regAnnotationTOAux.getAccountAnnotationOperation().setCertificateNumber(certificateNumber);
			}else{
				regAnnotationTOAux.getAccountAnnotationOperation().setCertificateNumber(null);
			}
			
			//VALIDAR SI EXISTE UN HECHO DE MERCADO ASOCIADO AL VALOR
			List<HolderMarketFactBalance> lstMarketFact =null;
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				lstMarketFact = dematerializationCertificateFacade.getMarketFactFromSecurity(security.getIdSecurityCodePk());
				
				if(lstMarketFact.size()==GeneralConstants.ONE_VALUE_INTEGER){
					HolderMarketFactBalance hmfb= lstMarketFact.get(0);
					regAnnotationTOAux.getAccountAnnotationMarketFact().setMarketDate(hmfb.getMarketDate());
					regAnnotationTOAux.getAccountAnnotationMarketFact().setMarketPrice(hmfb.getMarketPrice());
					regAnnotationTOAux.getAccountAnnotationMarketFact().setMarketRate(hmfb.getMarketRate());
				} else {
					//issue 188
					holderMarketFactBalance = dematerializationCertificateFacade.getHolderMarketFacts(regAnnotationTOAux.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk(),
							                  regAnnotationTOAux.getIdParticipantPk(), regAnnotationTOAux.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk());
					if (Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance)) {
						regAnnotationTOAux.getAccountAnnotationMarketFact().setMarketDate(holderMarketFactBalance.getMarketDate());
						regAnnotationTOAux.getAccountAnnotationMarketFact().setMarketPrice(holderMarketFactBalance.getMarketPrice());
						regAnnotationTOAux.getAccountAnnotationMarketFact().setMarketRate(holderMarketFactBalance.getMarketRate());
					}
					//--
				}
			}
			
			List<ParameterTable> lstMotivesPerSecurity = dematerializationCertificateFacade.getAllowedDematerializationMotives(security);
			 
			regAnnotationTOAux.setLstMotives(lstMotivesPerSecurity);
			return false;
		}
		return true;
	}
	
	/**
	 * Validate security.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateSecurity() throws ServiceException{
		blCertificateDisable=false;
		registerAccountAnnotationTO.setBlDPF(false);
		registerAccountAnnotationTO.setBlFixedIncome(false);
		registerAccountAnnotationTO.setBlCertificateNumber(false);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setExpeditionDate(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
		registerAccountAnnotationTO.setShareCapital(null);
		//filling market fact data
		//registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		try {
			Security secAux=dematerializationCertificateFacade.getSecurityFromId(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
			if(Validations.validateIsNotNull(secAux)
					&& Validations.validateIsNotNull(secAux.getIdSecurityCodePk())){
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(secAux);
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALT_ACCOUNTANNOTATION_SECURITY_NO_EXIST));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
			}
		} catch (Exception e) {
			// si sale error significa que el valor ingresado no existe
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ALT_ACCOUNTANNOTATION_SECURITY_NO_EXIST));
			JSFUtilities.showSimpleValidationDialog();
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
		}
		
		Security security = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity();
		
		//validate dematerialized 
		/*if(Validations.validateIsNotNull(security)
				&& Validations.validateIsNotNull(security.getIssuanceForm())){
			
			BigDecimal physicalAvailable=BigDecimal.ZERO;
			if(IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuanceForm())){
				physicalAvailable=security.getPhysicalBalance();
			}else{
				physicalAvailable=CommonsUtilities.subtractTwoDecimal(security.getPhysicalBalance(), security.getPhysicalDepositBalance(),1);
			}
			security.setPhysicalAvailable(physicalAvailable);
			if(security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
				blCertificateDisable=true;
			}
			else{
				blCertificateDisable=false;
			}
		}
		else{
			blCertificateDisable=false;
		}*/
		blCertificateDisable=false;
			
		
		//valdar si esta depositado en la 
		
		JSFUtilities.resetComponent("frmDematerializationCertificate:txtExpedition");
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass())||SecurityClassType.DPA.getCode().equals(security.getSecurityClass())){
				List<Integer> states = new ArrayList<Integer>();
				states.add(DematerializationStateType.REIGSTERED.getCode());
				states.add(DematerializationStateType.APPROVED.getCode());
				states.add(DematerializationStateType.CONFIRMED.getCode());	
				states.add(DematerializationStateType.CERTIFIED.getCode());
				states.add(DematerializationStateType.REVIEWED.getCode());
				Integer requestState = dematerializationCertificateFacade.existOtherRequest(security,states,null);
				if(Validations.validateIsNotNullAndNotEmpty(requestState) && 
						(DematerializationStateType.REIGSTERED.getCode().equals(requestState)
						||	DematerializationStateType.APPROVED.getCode().equals(requestState)
						||	DematerializationStateType.CERTIFIED.getCode().equals(requestState)
						|| DematerializationStateType.REVIEWED.getCode().equals(requestState))){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_DPF_EXIST));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					return;	
				}
				else if(DematerializationStateType.CONFIRMED.getCode().equals(requestState)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_DPF_EXIST_DEMATERIALIZED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					return;	
				}							
			}
			if(!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			
			// issue 891: validando si el valor esta autorizado para entrar a cartera
			if (Validations.validateIsNotNull(security.getIndAuthorized())) {
				if (security.getIndAuthorized().equals(BooleanType.NO.getCode())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							PropertiesUtilities.getMessage(PropertiesConstants.MSG_ALERT_VALIDATION_REQUIRED_AUTHORIZEDSECURITY));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					return;
				}
			}
			
			if(SecuritySourceType.FOREIGN.getCode().equals(security.getSecuritySource())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_FOREIGN));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			Issuance issuance = dematerializationCertificateFacade.findIssuanceAndIssuer(security.getIdSecurityCodePk());
			security.setIssuance(issuance);
			security.setIssuer(issuance.getIssuer());
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setIssuer(issuance.getIssuer());
			
			//debe permitir fisico y desmaterializado
			/*if(IssuanceType.PHYSICAL.getCode().equals(issuance.getIssuanceType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_DEMATERIALIZED));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}*/
			
			if(!IssuerStateType.REGISTERED.getCode().equals(issuance.getIssuer().getStateIssuer())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ISSUER_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType())){
				registerAccountAnnotationTO.setBlFixedIncome(true);
//				registerAccountAnnotationTO.setBlVariableIncome(true);
			}
//			else if(InstrumentType.VARIABLE_INCOME.getCode().equals(security.getInstrumentType())){
//				registerAccountAnnotationTO.setBlVariableIncome(true);
//			}
				
			if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass()) || SecurityClassType.DPA.getCode().equals(security.getSecurityClass())){
				registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(BigDecimal.ONE);
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(security.getSecuritySerial());
				registerAccountAnnotationTO.setBlDPF(true);	
				registerAccountAnnotationTO.setBlCertificateNumber(true);
				calculateShareCapital();
				if(blError){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(null);
					registerAccountAnnotationTO.setBlDPF(false);	
					registerAccountAnnotationTO.setBlCertificateNumber(false);
					blError = Boolean.FALSE;
					return;
				}
				//get the positions from 5 to 10 and if there are zeros forward then delete them
				String certificateNumber=null;
				if(security.getIdSecurityCodePk().length()>=10)
				{
					/*certificateNumber= security.getIdSecurityCodeOnly().substring(4,10); 
					if(Validations.validateIsNumeric(certificateNumber))
					{
						Integer iCertificateNumber=Integer.parseInt(certificateNumber);
						certificateNumber=String.valueOf(iCertificateNumber);
						certificateNumber="";
					}*/
					certificateNumber="";
				}
				else{
					certificateNumber= "";//security.getIdSecurityCodeOnly().substring(4,security.getIdSecurityCodePk().length()); 
				}
				registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(certificateNumber);
			}else{
				registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
			}
			
			registerAccountAnnotationTO.getAccountAnnotationOperation().setOperationPrice(security.getCurrentNominalValue());
			
			//VALIDAR SI EXISTE UN HECHO DE MERCADO ASOCIADO AL VALOR
			/*List<HolderMarketFactBalance> lstMarketFact =null;
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				lstMarketFact = dematerializationCertificateFacade.getMarketFactFromSecurity(security.getIdSecurityCodePk());
				
				if(lstMarketFact.size()==GeneralConstants.ONE_VALUE_INTEGER){
					HolderMarketFactBalance hmfb= lstMarketFact.get(0);
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketDate(hmfb.getMarketDate());
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketPrice(hmfb.getMarketPrice());
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketRate(hmfb.getMarketRate());
				} else {
					//issue 188
					holderMarketFactBalance = dematerializationCertificateFacade.getHolderMarketFacts(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk(),
							                  registerAccountAnnotationTO.getIdParticipantPk(), registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk());
					if (Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance)) {
						registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketDate(holderMarketFactBalance.getMarketDate());
						registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketPrice(holderMarketFactBalance.getMarketPrice());
						registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketRate(holderMarketFactBalance.getMarketRate());
					}
					//--
				}
			}*/
			
			//List<ParameterTable> lstMotivesPerSecurity = dematerializationCertificateFacade.getAllowedDematerializationMotivesOriginal(security);
			 
			//registerAccountAnnotationTO.setLstMotives(lstMotivesPerSecurity);
		}		
	}
//	ISSUE 188 trae toda de lista de hechos de mercado activos
	public void holderMarketFactList() throws ServiceException {
    	security = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity();
		String security = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk();
		Long idParticipantPk = registerAccountAnnotationTO.getIdParticipantPk();		
		Long idHolderAccountPk = registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk();
		listHolderMarketFactBalance = dematerializationCertificateFacade.getListHolderMarketFacts(security, idParticipantPk, idHolderAccountPk);
    }
//	cambia el estado activo a inactivo
	public void changeStatusHolderMarketFactBalance(Long idMarketfactBalancePk) {
//		dematerializationCertificateFacade.changeStatusHolderMarketFactBalance(idMarketfactBalancePk);
	}	
//	--
	/**
	 * Before save.
	 */
	public void beforeSave(){
		
		if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount())
				|| Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}/*else if((!IssuanceType.DEMATERIALIZED.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIssuanceForm()))&&
				registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPhysicalAvailable().compareTo(
				registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance())<0){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT));
			JSFUtilities.showSimpleValidationDialog();
		}*/else{
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				
				// modificando el mensaje de confirmacion en base a si hay o no numero de certificado
				if(!blCertificateDisable
						&& Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber()) ){
					// mensaje modificado
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALT_ACCOUNTANNOTATION_ASKREGISTER_BANK_SHARES_EMPTY_NUMCERTIFICATE));
				}else{
					// mensaje normal
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER_BANK_SHARES));
				}
				
				// modificando el mensaje de confirmacion en base a si hay o no archivo adjunto
				if(Validations.validateIsNotNull(fileExcelPersist)
						&& Validations.validateIsNotNull(fileExcelPersist.getAttachedContent())){
					// mensaje normal
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER_BANK_SHARES));
				}else{
					// mensaje modificado
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALT_ACCOUNTANNOTATION_ASKREGISTER_BANK_SHARES_NO_ATTACHMENT));
				}
				
			}
			else{
				/*
				// modificando el mensaje de confirmacion en base a si hay o no numero de certificado
				if(!blCertificateDisable
						&& Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber()) ){
					// mensaje modificado
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALT_ACCOUNTANNOTATION_ASKREGISTER_EMPTY_NUMCERTIFICATE));
				}else{
					// mensaje normal
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER));
				}
				*/
				// modificando el mensaje de confirmacion en base a si hay o no archivo adjunto
				if(Validations.validateIsNotNull(fileExcelPersist)
						&& Validations.validateIsNotNull(fileExcelPersist.getAttachedContent())){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER));
				}else{
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALT_ACCOUNTANNOTATION_ASKREGISTER_NO_ATTACHMENT));
				}
			}
			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
	}
	
	/**
	 * Show request.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @return the string
	 */
	public String showRequest(AccountAnnotationOperation accountAnnotationOperation){
		try {
			setFlagsFalse();
			indAlternativeCode=false;
			if(DematerializationStateType.ANNULLED.getCode().equals(accountAnnotationOperation.getState()) ||
			   DematerializationStateType.CANCELLED.getCode().equals(accountAnnotationOperation.getState()) ||
			   DematerializationStateType.REJECTED.getCode().equals(accountAnnotationOperation.getState())){
				blRenderedMotives = true;
			}
			fillView(accountAnnotationOperation);
			registerAccountAnnotationTO.setIndGrav(BooleanType.YES.getBooleanValue());
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	private static final int READ_EXCEL_SUCCESS = 1;
	private static final int READ_EXCEL_ERROR = 0;
	
	/**
	 * metodo para extraer los datos del archivo excel
	 * @param entryFile
	 * @return
	 */
	public DataExcelRegAccountAnnotationTO readDesmaterializationDataMasive(InputStream entryFile) {

		DataExcelRegAccountAnnotationTO result = new DataExcelRegAccountAnnotationTO();

		try {
			XSSFWorkbook book = new XSSFWorkbook(entryFile);
			XSSFSheet sheet;
			XSSFRow row;
			XSSFCell cell;

			sheet = book.getSheetAt(0);

			List<DataExcelRegAccountAnnotationTO> lstDataExcel = new ArrayList<>();
			DataExcelRegAccountAnnotationTO dataExcel;

			// posion de las columnas de las q se extraeran los datos
			int colCui0 = 0;
			int colHolderAccountNumber1 = 1;
			int colIdSecurotyCode2 = 2;
			int colDateMarketfact3 = 3;
			int colRateMarketfact4 = 4;
			int colPriceMarketfact5 = 5;
			int colMotive6 = 6;
			int colNumCertificate7 = 7;
			int colQuantity8 = 8;
			// columnasde contiene gravamen y acreedor
			int colContainsTax9 = 9;
			int colCreditor10 = 10;

			// primera y ultima fila del archivo
			int rowFirstAcv = 1;
			int lastRowAcv = sheet.getLastRowNum();

			String idHolder = null;
			Integer accountNumber = null;
			String idSecurityCode = null;
			Date dateMarketfact = null;
			BigDecimal rateMarketfact = null;
			BigDecimal priceMarketfact = null;
			String motive = null;
			Integer motiveId = null;
			String numberCertificate = null;
			// contieneGravamen
			String containsTax = null; 
			boolean containsTaxBoolean = false;
			String creditor = null;
			BigDecimal quantity = null;
			BigDecimal oneHundred = new BigDecimal("100");
			BigDecimal oneHundredNegative = new BigDecimal("-100");

			// esto para comparar los motivos de registro
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setIndicator1("DESMATERIA");
			paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			List<ParameterTable> lstAllMotives=generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
			// filtrando los motivios permitidos en esta carga masiva
			List<ParameterTable> lstMotives = new ArrayList<>();
			for (ParameterTable pt : lstAllMotives)
				if (	pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.SUSCRIPTION.getCode()) 
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode()) 
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode())
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_BCB.getCode()) 
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode()) 
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.AUCTION_PURCHASE.getCode())
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode())
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.INCOME_PURCHASE_PRIVATE_STOCK.getCode()) 
						|| pt.getParameterTablePk().equals(DematerializationCertificateMotiveType.INCOME_PURCHASE_PRIMARY.getCode()) )
					lstMotives.add(pt);

			for (int filaExtrac = rowFirstAcv; filaExtrac <= lastRowAcv; filaExtrac++) {
				row = sheet.getRow(filaExtrac);
				if (row != null) {
					
					dataExcel = new DataExcelRegAccountAnnotationTO();

					// extracion 0: CUI (solo se admite celda de tipo: numerico o texto)
					cell = row.getCell(colCui0);
					if (cell != null){
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
							idHolder = String.valueOf(new BigDecimal(String.valueOf(cell.getNumericCellValue())).longValue());
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							idHolder = cell.getStringCellValue();
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
						dataExcel.setCellReferenceHolder(cell.getReference());
					} else {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cui.required"));
						entryFile.close();
						return result;
					}

					// extracion 1: cuenta titular (solo se admite celda de tipo: numerico o texto)
					cell = row.getCell(colHolderAccountNumber1);
					if (cell != null){
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
							accountNumber = new BigDecimal(String.valueOf(cell.getNumericCellValue())).intValue();
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							accountNumber = new BigDecimal(cell.getStringCellValue()).intValue();
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
						dataExcel.setCellReferenceAccount(cell.getReference());
					} else {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.account.number.required"));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.account.number.required"));
					}

					// extracion 2: clase - clave de valor (solo se admite celda de tipo: texto)
					cell = row.getCell(colIdSecurotyCode2);
					if (cell != null){
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							idSecurityCode = cell.getStringCellValue().trim();
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
						}
						dataExcel.setCellReferenceSecurity(cell.getReference());
					} else {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.security.required"));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.security.required"));
					}

					// extracion 3: fecha de Hecho de Mercado(solo se admite celda de tipo: numeric(fecha en excel) o formula)
					cell = row.getCell(colDateMarketfact3);
					if (cell != null)
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA)
							dateMarketfact = cell.getDateCellValue();
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.date.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.date.value", new Object[] { cell.getReference() }));
						}
					else {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.date.marketfact.required"));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.date.marketfact.required"));
					}

					// extracion 4: tasa de Hecho de Mercado (solo se admite celda de tipo: numerica o formula)
					cell = row.getCell(colRateMarketfact4);
					if (cell != null)
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA
								|| cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
							if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
								// BigDecimal.valueOf(cell.getNumericCellValue()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
								rateMarketfact = BigDecimal.valueOf(cell.getNumericCellValue());
							else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK)
								rateMarketfact = null;
							else if (cell.getCachedFormulaResultType() == XSSFCell.CELL_TYPE_NUMERIC)
								// BigDecimal.valueOf(cell.getNumericCellValue()).multiply(BigDecimal.TEN).multiply(BigDecimal.TEN);
								rateMarketfact = BigDecimal.valueOf(cell.getNumericCellValue());
						} else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
					else
						rateMarketfact = null;
					
					// validando que la tasa de mercado esta dentro del rango [0 - 100]
					if( Validations.validateIsNotNull(rateMarketfact) 
							&& (rateMarketfact.compareTo(oneHundredNegative) < 0 || rateMarketfact.compareTo(oneHundred) > 0) ){
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.rate.marketfact.out.of.range", new Object[] { cell.getReference() }));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.rate.marketfact.out.of.range", new Object[] { cell.getReference() }));
					}

					// extracion 5: precio de mercado (solo se admite celda de tipo: numerico o texto)
					cell = row.getCell(colPriceMarketfact5);
					if (cell != null)
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
							priceMarketfact = new BigDecimal(String.valueOf(cell.getNumericCellValue()));
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							priceMarketfact = new BigDecimal(cell.getStringCellValue());
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK)
							priceMarketfact = null;
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
					else
						priceMarketfact = null;

					// extracion 6: motivo (solo se admite celda de tipo: texto)
					cell = row.getCell(colMotive6);
					if (cell != null)
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
							// validando que sea un motivo dentro de los parametros establecidos
							motive = cell.getStringCellValue();
							motive = motive.toUpperCase();
							motiveId = null;
							StringBuilder validMotives = new StringBuilder();
							for (ParameterTable pt : lstMotives)
								if (motive.equals(pt.getDescription())) {
									motiveId = pt.getParameterTablePk();
									break;
								} else {
									validMotives.append("<br/>");
									validMotives.append(pt.getDescription());
								}

							if (motiveId == null) {
								result.setCodeResult(READ_EXCEL_ERROR);
								result.setMsgResult(PropertiesUtilities.getMessage("msg.error.motive.excel",
										new Object[] { cell.getReference(), validMotives.toString() }));
//								entryFile.close();
//								return result;
								result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.motive.excel",
										new Object[] { cell.getReference(), validMotives.toString() }));
							}
						} else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
						}
					else {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.motive.required"));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.motive.required"));
					}

					// extracion 7: nro de certificado (solo se admite celda de tipo: numerico o texto)
					cell = row.getCell(colNumCertificate7);
					if (cell != null)
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
							numberCertificate = String.valueOf(new BigDecimal(String.valueOf(cell.getNumericCellValue())).intValue());
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							numberCertificate = cell.getStringCellValue();
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK)
							numberCertificate = "";
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
					else
						numberCertificate = "";

					// extracion 8: de Cantidad (solo se admite celda de tipo: numerico o texto)
					cell = row.getCell(colQuantity8);
					if (cell != null){
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
							quantity = new BigDecimal(String.valueOf(cell.getNumericCellValue()));
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							quantity = new BigDecimal(cell.getStringCellValue());
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
					dataExcel.setCellReferenceQuantity(cell.getReference());
					} else {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.quantity.required"));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.quantity.required"));
					}
					
					// validacion de que la cantidad debe ser mayor a cero
					if (quantity.compareTo(BigDecimal.ZERO) <= 0) {
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.quantity.required.greater.zero", new Object[] { cell.getReference() }));
//						entryFile.close();
//						return result;
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.quantity.required.greater.zero", new Object[] { cell.getReference() }));
					}
					
					// extracion 9: ContieneGravamen (solo se admite celda de tipo: texto (SI/NO))
					cell = row.getCell(colContainsTax9);
					if (cell != null)
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING){
							containsTax = cell.getStringCellValue();
							if((!containsTax.equalsIgnoreCase("SI") && !containsTax.equalsIgnoreCase("NO") ) ){
								result.setCodeResult(READ_EXCEL_ERROR);
								result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.yes.or.not", new Object[] { cell.getReference() }));
								result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.yes.or.not", new Object[] { cell.getReference() }));
							}else{
								if(containsTax.equalsIgnoreCase("NO")){
									containsTaxBoolean = false;
								}else{
									containsTaxBoolean = true;
								}
							}
						} else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK){
							containsTaxBoolean = false;
						} else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
						}
					else{
						result.setCodeResult(READ_EXCEL_ERROR);
						result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
						result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
					}
					
					// extracion 10: Acreedor (solo se admite celda de tipo: texto)
					creditor = "";
					cell = row.getCell(colCreditor10);
					if (cell != null){
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING){
							creditor = cell.getStringCellValue().trim().toUpperCase();
						} else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK){
							// si ContieneGravamen es SI entonces Acreedor no puede ser blanco o nulo
							if(containsTaxBoolean){
								// lanzando error
								result.setCodeResult(READ_EXCEL_ERROR);
								result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.creditor.if.gravamenYes", new Object[] { cell.getReference() }));
								result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.creditor.if.gravamenYes", new Object[] { cell.getReference() }));
							}
						}
						else {
							result.setCodeResult(READ_EXCEL_ERROR);
							result.setMsgResult(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
//							entryFile.close();
//							return result;
							result.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
						}
					}
					
					dataExcel.setIdHolder(idHolder);
					dataExcel.setAccountNumber(accountNumber);
					dataExcel.setIdSecurityCode(idSecurityCode);
					dataExcel.setDateMarketfact(dateMarketfact);
					dataExcel.setRateMarkefact(rateMarketfact);
					dataExcel.setPriceMarkefact(priceMarketfact);
					dataExcel.setMotive(motive);
					dataExcel.setNumberCertificate(numberCertificate);
					dataExcel.setQuantity(quantity);
					dataExcel.setMotiveId(motiveId);
					dataExcel.setContainsTax(containsTaxBoolean);
					dataExcel.setCreditor(creditor);

					lstDataExcel.add(dataExcel);
				}
			}
			
			if(Validations.validateIsNull(result.getCodeResult())){
				//si llega aca, significa una lectura exitosa del archivo excel
				result.setCodeResult(READ_EXCEL_SUCCESS);
				result.setMsgResult(PropertiesUtilities.getMessage("msg.success.read.excel"));
				result.setLstDataExcelReult(lstDataExcel);
				System.out.println("::: los datos extraidos del archivo excel son: ");
				for (DataExcelRegAccountAnnotationTO dtExcel : lstDataExcel) {
					System.out.println(dtExcel.toString());
				}
			}
			
			entryFile.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error : " + e.getMessage());
			result.setCodeResult(READ_EXCEL_ERROR);
			result.setMsgResult(PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage().toString() }));
		}

		return result;
	}
	
	/** metodo para mostrar un alert por pantalla
	 * 
	 * @param cabeceraMensaje
	 * @param cuerpoMensaje */
	private static void showDialog(String cabeceraMensaje, String cuerpoMensaje) {
		JSFUtilities.putViewMap("bodyMessageView", cuerpoMensaje);
		JSFUtilities.putViewMap("headerMessageView", cabeceraMensaje);
	}
	
	/** metodo para realizar la carga archivo adjunto
	 * 
	 * @param event */
	public void uploadAttachedFile(FileUploadEvent event) throws Exception {
		UploadedFile uploadedFile = event.getFile();

		if (fileExcelPersist.isMassiveRegister()) {
			// validando q primero se haya cargado el archivo excel
			if (Validations.validateIsNotNull(fileExcelPersist.getFileName())) {
				fileExcelPersist.setAttachedName("Archivo Adjunto");
				fileExcelPersist.setAttachedType(getFileExtension(uploadedFile.getFileName()));
				fileExcelPersist.setAttachedContent(uploadedFile.getContents());
				fileExcelPersist.setNameFileAttached(uploadedFile.getFileName());
			} else {
				// error de que primero debe cargar el archivo excel para el registro masivo
				showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage("msg.accountannotation.register.masive.first.require.excel"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		} else {
			// se realiza la carga normal de un adjunto
			fileExcelPersist.setAttachedName("Archivo Adjunto");
			fileExcelPersist.setAttachedType(getFileExtension(uploadedFile.getFileName()));
			fileExcelPersist.setAttachedContent(uploadedFile.getContents());
			fileExcelPersist.setNameFileAttached(uploadedFile.getFileName());
		}
		
		//mensaje de exito
		showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), PropertiesUtilities.getMessage("msg.success.uploaded.attached"));
		JSFUtilities.showSimpleValidationDialog();
	}
	
	/** metodo para realizar lectura y validacion de los datos del archivo excel para el registro masivo de ACV
	 * (Desmaterializacion)
	 * 
	 * @param event */
	public void readFileExcel(FileUploadEvent event) {
		
		UploadedFile archivoExcel = event.getFile();

		DataExcelRegAccountAnnotationTO result;
		lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
		RegisterAccountAnnotationTO regAnnotationTO;
		fileExcelPersist = new AccAnnotationOperationFile();
		fileExcelPersist.setMassiveRegister(true);

		// el Participante es obtenido por defecto al cargar el bean identifica al usuario participante
		if (registerAccountAnnotationTO.getIdParticipantPk() == null) {
			showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), PropertiesUtilities.getMessage("msg.error.participant.required"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		AccountAnnotationOperation acAnnotationOperation;
		AccountAnnotationMarketFact acAnnotationMarketFact;
		HolderAccount holderAccount;
		Long idHolderPk;
		Security security;
		String[] idHolders;

		try {
			archivoExcel.getInputstream();
			result = readDesmaterializationDataMasive(archivoExcel.getInputstream());
			switch (result.getCodeResult()) {
			case READ_EXCEL_SUCCESS:
				
				// preparando archivo excel para su respectiva persistencia
				fileExcelPersist.setFileName("Archivo Excel para Registro Masivo ACV");
				fileExcelPersist.setFileType(getFileExtension(archivoExcel.getFileName()));
				fileExcelPersist.setFileContent(archivoExcel.getContents());
				fileExcelPersist.setNameFileExcel(archivoExcel.getFileName());
				
				lstErrorReadExcel = new ArrayList<>();
				
				
				// ahora se valida si los datos son coherentes
				for (DataExcelRegAccountAnnotationTO dataExcel : result.getLstDataExcelReult()) {
					
					// en este objeto se carga la data necesaria para realizar la persisistencia de datos
					regAnnotationTO = new RegisterAccountAnnotationTO();
					regAnnotationTO.setIdParticipantPk(registerAccountAnnotationTO.getIdParticipantPk());

					// en este objeto se carga gran parte de la data para el registro de solicitude de desmaterializacion
					acAnnotationOperation = new AccountAnnotationOperation();

					// validando el numero de cuenta titular (segun restriccion sql deberia encontrar un unico valor al ingresar participante y nroCuentaTitular)
					holderAccount = dematerializationCertificateFacade.getHolderAccountFromParticipantAndHoldAccountNumber(regAnnotationTO.getIdParticipantPk(),
							dataExcel.getAccountNumber());
					if (holderAccount != null) {
						
						// validando que la cuenta titular tenga estado REGISTRADO
						if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
							// limpiando lista
//							lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
//							fileExcelPersist = new AccAnnotationOperationFile();
//							fileExcelPersist.setMassiveRegister(true);
//							showDialog(
//									PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//									PropertiesUtilities.getMessage("msg.error.account.number.state.invalid", new Object[] { holderAccount.getAccountNumber().toString(),
//											dataExcel.getCellReferenceHolder() }));
//							JSFUtilities.showSimpleValidationDialog();
//							return;
							
							lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.account.number.state.invalid", new Object[] { holderAccount.getAccountNumber().toString(),
											dataExcel.getCellReferenceHolder() }));
						}
						
						acAnnotationOperation.setHolderAccount(holderAccount);

						// validando si el CUI tiene relacion con la cuenta titular encontrada
						// extraer el CUI o CUI's (en caso de ser mancomunados)
						idHolders = dataExcel.getIdHolder().split(",");
						for (String idHolder : idHolders)
							try {
								idHolderPk = Long.parseLong(idHolder.trim());
								if (!dematerializationCertificateFacade.verifyRelationshipHolderAndHolderAccount(holderAccount.getIdHolderAccountPk(), idHolderPk)) {
									// como noexiste relacion lanzar mensaje de error limpiando lista
//									lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
//									fileExcelPersist = new AccAnnotationOperationFile();
//									fileExcelPersist.setMassiveRegister(true);
//									showDialog(
//											PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//											PropertiesUtilities.getMessage("msg.error.account.number.norelationship.cui", new Object[] { idHolder,
//													dataExcel.getAccountNumber().toString(), dataExcel.getCellReferenceHolder() }));
//									JSFUtilities.showSimpleValidationDialog();
//									return;
									lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.account.number.norelationship.cui", new Object[] { idHolder,
											dataExcel.getAccountNumber().toString(), dataExcel.getCellReferenceHolder() }));
								}
							} catch (Exception e) {
								e.printStackTrace();
								// limpiando lista
//								lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
//								showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//										PropertiesUtilities.getMessage("msg.error.cui.invalid.from.excel", new Object[] { dataExcel.getCellReferenceHolder() }));
//								JSFUtilities.showSimpleValidationDialog();
//								return;
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.cui.invalid.from.excel", new Object[] { dataExcel.getCellReferenceHolder() }));
							}
					} else {
						// limpiando lista
//						lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
//						fileExcelPersist = new AccAnnotationOperationFile();
//						fileExcelPersist.setMassiveRegister(true);
//						showDialog(
//								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
//								PropertiesUtilities.getMessage("msg.error.account.number.invalid",
//										new Object[] { dataExcel.getAccountNumber().toString(), dataExcel.getCellReferenceAccount() }));
//						JSFUtilities.showSimpleValidationDialog();
//						return;
						lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.account.number.invalid",
								new Object[] { dataExcel.getAccountNumber().toString(), dataExcel.getCellReferenceAccount() }));
					}

					// validando el valor (Security)
					security = dematerializationCertificateFacade.getSecurityFromId(dataExcel.getIdSecurityCode());
					if (security != null){
						acAnnotationOperation.setSecurity(security);

						// issue 1263: de acuerdo al tipo de renta del valor
						// si es RF: precio = null, tasa != null
						// si es RV: precio != null, tasa = null
						if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
							if(Validations.validateIsNotNull(dataExcel.getPriceMarkefact())){
								// lanzamos validacion de que el precio debe ser vacio
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.rf.noRequired.priceHM", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
							if(Validations.validateIsNull(dataExcel.getRateMarkefact())){
								// lanzamos validacion de que la tasa no puede ser vacia
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.rf.required.rateHM", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
						}else if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
							if(Validations.validateIsNull(dataExcel.getPriceMarkefact())){
								// lanzamos validacion de que el precio no debe estar vacio
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.rv.required.priceHM", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
							if(Validations.validateIsNotNull(dataExcel.getRateMarkefact())){
								// lanzamos validacion de que la tasa debe estar vacia
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.rv.noRequired.rateHM", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
						}
						
						// tambien se valida:
						// si gravamen = true: acreedor obligatorio
						// si gravamen = false: acreedor vacio estricto
						if(dataExcel.isContainsTax()){
							if(Validations.validateIsNullOrEmpty(dataExcel.getCreditor())){
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.ifContainsTax.required.creditor", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
						}else{
							if(Validations.validateIsNotNullAndNotEmpty(dataExcel.getCreditor())){
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.ifNoContainsTax.noRequired.creditor", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
						}
						
						// issue 1263: validando que el numero de certificado sea obligatorio para Desmaterializaciones de un fisico 
						if(dataExcel.getMotiveId().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())){
							if(Validations.validateIsNullOrEmpty(dataExcel.getNumberCertificate())){
								lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.acv.massive.ifPhysicalMotive.required.numberCertified", new Object[] { security.getIdSecurityCodePk()+" de la celda: "+ dataExcel.getCellReferenceSecurity() }));
							}
						}
						
					}else {
						// limpiando lista
//						lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
//						fileExcelPersist = new AccAnnotationOperationFile();
//						fileExcelPersist.setMassiveRegister(true);
//						showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
//								PropertiesUtilities.getMessage("msg.error.security.code.invalid", new Object[] { dataExcel.getCellReferenceSecurity() }));
//						JSFUtilities.showSimpleValidationDialog();
//						return;
						lstErrorReadExcel.add(PropertiesUtilities.getMessage("msg.error.security.code.invalid", new Object[] { dataExcel.getCellReferenceSecurity() }));
					}

					// seteando el motivo
					// seteando el nro de certificado
					// setenado la cantidad
					acAnnotationOperation.setMotive(dataExcel.getMotiveId());
					acAnnotationOperation.setCertificateNumber(dataExcel.getNumberCertificate());
					acAnnotationOperation.setTotalBalance(dataExcel.getQuantity());
					regAnnotationTO.setAccountAnnotationOperation(acAnnotationOperation);

					// setenado la fecha de Hecho de mercado
					// setenado la tasa de Hecho de mercado
					// setenado el precio de Hecho de mercado
					acAnnotationMarketFact = new AccountAnnotationMarketFact();
					acAnnotationMarketFact.setMarketDate(dataExcel.getDateMarketfact());
					acAnnotationMarketFact.setMarketRate(dataExcel.getRateMarkefact());
					acAnnotationMarketFact.setMarketPrice(dataExcel.getPriceMarkefact());
					acAnnotationMarketFact.setOperationQuantity(dataExcel.getQuantity());
					regAnnotationTO.setAccountAnnotationMarketFact(acAnnotationMarketFact);
					
					//realizando validaciones para el valor y la cantidad que se quuiere registrar
					regAnnotationTOAux = (RegisterAccountAnnotationTO) regAnnotationTO.clone();
					
					if (security != null && holderAccount != null) {
						// metodos para aplicar las validaciones que se aplican cuando se hace manualmente el registro
						// (registro individual)
						if (!calculateShareCapitalFromExcel(dataExcel.getCellReferenceQuantity())) {
							validateSecurityFromExcel(dataExcel.getCellReferenceSecurity(), dataExcel.getCellReferenceQuantity());
						}
					}
					
					// SE VUELVEN A COPIAR LAS MISMOS SETEOS DEBIDO A QUE EL POJO SUFRIO CAMBIOS EN LAS VALIDACIONES
					// seteando el motivo
					// seteando el nro de certificado
					// setenado la cantidad
					acAnnotationOperation.setMotive(dataExcel.getMotiveId());
					acAnnotationOperation.setCertificateNumber(dataExcel.getNumberCertificate());
					acAnnotationOperation.setTotalBalance(dataExcel.getQuantity());
					
					acAnnotationOperation.setIndLien(BooleanType.NO.getCode());
					acAnnotationOperation.setCreditorSource(null);
					regAnnotationTO.setBlLien(false);
					if(dataExcel.isContainsTax()){
						acAnnotationOperation.setIndLien(BooleanType.YES.getCode());
						acAnnotationOperation.setCreditorSource(dataExcel.getCreditor());
						regAnnotationTO.setBlLien(true);
					}
					
					regAnnotationTO.setAccountAnnotationOperation(acAnnotationOperation);

					// setenado la fecha de Hecho de mercado
					// setenado la tasa de Hecho de mercado
					// setenado el precio de Hecho de mercado
					acAnnotationMarketFact = new AccountAnnotationMarketFact();
					acAnnotationMarketFact.setMarketDate(dataExcel.getDateMarketfact());
					acAnnotationMarketFact.setMarketRate(dataExcel.getRateMarkefact());
					acAnnotationMarketFact.setMarketPrice(dataExcel.getPriceMarkefact());
					acAnnotationMarketFact.setOperationQuantity(dataExcel.getQuantity());
					regAnnotationTO.setAccountAnnotationMarketFact(acAnnotationMarketFact);
					
					lstRegisterAccountAnnotationTOExcel.add(regAnnotationTO);
				}
				
				if(lstErrorReadExcel.isEmpty()){
					// si todo fue exito se notifica al usuario, (tambien significa que todo esta listo para el registro masivo)
					showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), result.getMsgResult());
					JSFUtilities.showSimpleValidationDialog();
				}else{
					// limpiando lista
					lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
					fileExcelPersist = new AccAnnotationOperationFile();
					fileExcelPersist.setMassiveRegister(true);
					showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage("msg.error.read.file.excel"));
					JSFUtilities.showSimpleValidationDialog();
				}
				
				break;
			case READ_EXCEL_ERROR:
				
				// limpiando lista
				lstRegisterAccountAnnotationTOExcel=new ArrayList<>();
				fileExcelPersist = new AccAnnotationOperationFile();
				fileExcelPersist.setMassiveRegister(true);
				
				if(result.getLstErrorReadExcel().isEmpty()){
					showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), result.getMsgResult());
					JSFUtilities.showSimpleValidationDialog();
				}else{
					//depositando los errores encontrados
					lstErrorReadExcel = new ArrayList<>(result.getLstErrorReadExcel());
					showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage("msg.error.read.file.excel"));
					JSFUtilities.showSimpleValidationDialog();
				}
				
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
			// limpiando lista
			lstRegisterAccountAnnotationTOExcel=new ArrayList<>();
			fileExcelPersist = new AccAnnotationOperationFile();
			fileExcelPersist.setMassiveRegister(true);
			showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage() != null ? e.getMessage().toString() : "?" }));
			JSFUtilities.showSimpleValidationDialog();
		} catch (Exception e) {
			e.printStackTrace();
			// limpiando lista
			lstRegisterAccountAnnotationTOExcel=new ArrayList<>();
			fileExcelPersist = new AccAnnotationOperationFile();
			fileExcelPersist.setMassiveRegister(true);
			showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage() != null ? e.getMessage().toString() : "?" }));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/** Metodo para extraer la extension de un archivo
	 * 
	 * @param fileName
	 * @return */
	private String getFileExtension(String fileName) {
		String[] partition = fileName.split("\\.");
		// verificando que se haya realizado la separacion del nombre del archivo-extension
		if (partition.length > 1) {
			return partition[partition.length - 1].toLowerCase();
		} else {
			// si no tiene extension se retorna una cadena vacia
			return "";
		}
	}
	
	/** Gets the file stream content.
	 * 
	 * @param mcnFile the mcn file
	 * @return the file stream content
	 * @throws IOException Signals that an I/O exception has occurred. */
	public StreamedContent getFileStreamContent(AccAnnotationOperationFile accAnnotationFile) throws IOException {
		if (accAnnotationFile != null)
			if (accAnnotationFile.getAttachedContent() != null) {
				InputStream inputStream = new ByteArrayInputStream((byte[]) accAnnotationFile.getAttachedContent());
				StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream, null, accAnnotationFile.getAttachedName() + "."
						+ accAnnotationFile.getAttachedType());
				inputStream.close();
				return streamedContentFile;
			} else
				return null;
		return null;
	}
	
	
	/**
	 * metodo para realizar el registro masivo de ACV (Desmaterializacion de valores) mediante archivo excel
	 */
	@LoggerAuditWeb
	public void saveRequestMasive() {
		try {
			if (lstRegisterAccountAnnotationTOExcel != null && !lstRegisterAccountAnnotationTOExcel.isEmpty()) {
				//realizando persistencia de la entidad q almacena los archivos excel
				fileExcelPersist = dematerializationCertificateFacade.registryAccAnnotationFile(fileExcelPersist);
				
				AccountAnnotationOperation accountAnnotationOperation;
				List<Long> lstIdAccAnnotatonOperation = new ArrayList<>();
				for (RegisterAccountAnnotationTO regAccountAnnotationTO : lstRegisterAccountAnnotationTOExcel) {
					regAccountAnnotationTO.getAccountAnnotationOperation().setIdAccAnnotationFileFk(fileExcelPersist);
					accountAnnotationOperation = dematerializationCertificateFacade.registerAccountAnnotationOperationOriginal(regAccountAnnotationTO);
					lstIdAccAnnotatonOperation.add(accountAnnotationOperation.getCustodyOperation().getOperationNumber());
				}
				lstRegisterAccountAnnotationTOExcel = new ArrayList<>();
				fileExcelPersist = new AccAnnotationOperationFile();
				fileExcelPersist.setMassiveRegister(true);

				// si el registro masivo es exitoso entonces se muestra el mensaje de exito
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_REGISTER_MASIVE_SUCCESS));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog1').show();");

				for (Long idAccAnnotationOperation : lstIdAccAnnotatonOperation) {
					// INICIO ENVIO DE NOTIFICACIONES
					BusinessProcess businessProcess = new BusinessProcess();
					businessProcess.setIdBusinessProcessPk(BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REGISTER.getCode());
					Object[] parameters = { idAccAnnotationOperation.toString() };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
				}
			} else {
				showDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_REGISTER_MASIVE_REQUIRE_EXCEL));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog1').show();");
		}
	}
	
	/**
	 * Save request.
	 */
	@LoggerAuditWeb
	public void saveRequest(){
		try {
			//si se realizo la carga de archivo adjunto primero se lo registra
			if(Validations.validateIsNotNull(fileExcelPersist.getAttachedName())){
				fileExcelPersist.setMassiveRegister(false);
				fileExcelPersist = dematerializationCertificateFacade.registryAccAnnotationFile(fileExcelPersist);
				registerAccountAnnotationTO.getAccountAnnotationOperation().setIdAccAnnotationFileFk(fileExcelPersist);
			}
			
			// issue 878: el objeto this.holderMarketFactBalance no deja modificar marketDate, MarketPrice, marketRate
			// por eso se lo evalua y si tiene datos se lo setea en el objeto (this.registerAccountAnnotationTO) 
			// que realiza la persistencia de datos en la BBDD
			if (Validations.validateIsNotNull(holderMarketFactBalance)) {
				if (Validations.validateIsNotNull(holderMarketFactBalance.getMarketDate()))
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketDate(holderMarketFactBalance.getMarketDate());
				if (Validations.validateIsNotNull(holderMarketFactBalance.getMarketPrice()))
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketPrice(holderMarketFactBalance.getMarketPrice());
				if (Validations.validateIsNotNull(holderMarketFactBalance.getMarketRate()))
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketRate(holderMarketFactBalance.getMarketRate());
			}
			
			AccountAnnotationOperation accountAnnotationOperation = dematerializationCertificateFacade.registerAccountAnnotationOperationOriginal(registerAccountAnnotationTO);
			Long idAccountAnnotationOperationPk = accountAnnotationOperation.getCustodyOperation().getOperationNumber();
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_REGISTER_BANK_SHARES, new Object[]{idAccountAnnotationOperationPk.toString()}));
			}
			else{
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REGISTER, new Object[]{idAccountAnnotationOperationPk.toString()}));
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			fileExcelPersist=new AccAnnotationOperationFile();
			
			// INICIO ENVIO DE NOTIFICACIONES
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REGISTER.getCode());
			Object[] parameters = {	idAccountAnnotationOperationPk.toString() };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES	
			
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}
	
	/**
	 * Clean register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void cleanRegister() throws ServiceException{
		JSFUtilities.resetViewRoot();
		registerAccountAnnotationTO.setAccountAnnotationOperation(new AccountAnnotationOperation());
		registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(new HolderAccount());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setCreditorSource(null);
		registerAccountAnnotationTO.setHolder(new Holder());
		registerAccountAnnotationTO.setLstHolderAccount(null);
		ubicacionGeografica();
		if(!blParticipant)
			registerAccountAnnotationTO.setIdParticipantPk(null);
		else
			registerAccountAnnotationTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		registerAccountAnnotationTO.setSecurityClass(null);
		registerAccountAnnotationTO.setShareCapital(null);
		registerAccountAnnotationTO.setBlFixedIncome(false);
		registerAccountAnnotationTO.setBlOthers(false);
		registerAccountAnnotationTO.setBlRejected(false);
		registerAccountAnnotationTO.setBlLien(false);
		
		blCertificateDisable=false;
		
		fileExcelPersist = new AccAnnotationOperationFile();
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	public String validateApprove(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())&&
							Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
						PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
																				selectedAAO.getSecurity().getIdSecurityCodePk(), 
																				selectedAAO.getCertificateNumber());
						Object[] bodyData = new Object[2];
						   bodyData[0] = selectedAAO.getCertificateNumber();   
						   bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
						if (Validations.validateIsNotNull(physicalBalanceDetail)){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}

						if(DematerializationStateType.REIGSTERED.getCode().equals(selectedAAO.getState())){
							blApprove = true;
							blDisabledFields=false;
							indAlternativeCode=true;
//							indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
							JSFUtilities.showSimpleValidationDialog();
						}
				}
				else{
					String strOperationNumber= GeneralConstants.EMPTY_STRING;
					for(AccountAnnotationOperation objAao : checkAccountAnotation)
					{
						PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
																								objAao.getSecurity().getIdSecurityCodePk(), 
																								objAao.getCertificateNumber());
						Object[] bodyData = new Object[2];
						bodyData[0] = objAao.getCertificateNumber();   
						bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
						if (Validations.validateIsNotNull(physicalBalanceDetail)) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
						if (DematerializationStateType.REIGSTERED.getCode().equals(objAao.getState())) {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}	
					}
					blApprove = true;
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE), 
										PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_APPROVE, 
													new Object[]{strOperationNumber}));
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate annul.
	 *
	 * @return the string
	 */
	public String validateAnnul(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER) {
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(DematerializationStateType.REIGSTERED.getCode().equals(selectedAAO.getState())){
						blAnnul = true;
						blDisabledFields=false;
						indAlternativeCode=false;
						fillView(selectedAAO);
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
						JSFUtilities.showSimpleValidationDialog();
					}
				} else {
					for(AccountAnnotationOperation aao: checkAccountAnotation){
						if(!DematerializationStateType.REIGSTERED.getCode().equals(aao.getState())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blAnnul = true;
					indAlternativeCode=false;
					selectedMotive();
				}
			} else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())
							&& Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
					PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
																				selectedAAO.getSecurity().getIdSecurityCodePk(), 
																				selectedAAO.getCertificateNumber());
						if (Validations.validateIsNotNull(physicalBalanceDetail)){
							   Object[] bodyData = new Object[2];
							   bodyData[0] = selectedAAO.getCertificateNumber();   
							   bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())){
						// issue 1398: a requerimiento de usuario ya no se valida el estado CERTIFICADO para DPF o DPA
//						if(SecurityClassType.DPF.getCode().equals(selectedAAO.getSecurity().getSecurityClass())||SecurityClassType.DPA.getCode().equals(selectedAAO.getSecurity().getSecurityClass())){
//							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
//									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNTANNOTATION_INVALID_CONFIRM_DPF));
//							JSFUtilities.showSimpleValidationDialog();
//							return null;
//						}else{
							blConfirm = true;
							blDisabledFields = false;
							indAlternativeCode=true;
//							indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
//						}
					}else if(DematerializationStateType.CERTIFIED.getCode().equals(selectedAAO.getState())){
						blConfirm = true;
						indAlternativeCode=true;
						fillView(selectedAAO);
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
					}
			} else {
				String strOperationNumber= GeneralConstants.EMPTY_STRING;
				int countDematerializationPhysical = 0;
				for(AccountAnnotationOperation objAao : checkAccountAnotation){
					PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
																							objAao.getSecurity().getIdSecurityCodePk(), 
																							objAao.getCertificateNumber());
					
					if(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(objAao.getMotive())){
						countDematerializationPhysical = countDematerializationPhysical + 1;
					}
					Object[] bodyData = new Object[2];
					bodyData[0] = objAao.getCertificateNumber();   
					bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
					if (Validations.validateIsNotNull(physicalBalanceDetail)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
					
					if (DematerializationStateType.APPROVED.getCode().equals(objAao.getState())){
						// issue 1398: a requerimiento de usuario ya no se valida el estado CERTIFICADO para DPF o DPA
//						if(SecurityClassType.DPF.getCode().equals(objAao.getSecurity().getSecurityClass())
//								|| SecurityClassType.DPA.getCode().equals(objAao.getSecurity().getSecurityClass())){
//							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
//									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNTANNOTATION_INVALID_CONFIRM_DPF));
//							JSFUtilities.showSimpleValidationDialog();
//							return null;
//						} else {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
//						}
					} else if(DematerializationStateType.CERTIFIED.getCode().equals(objAao.getState())){
						if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
							strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
						} else {
							strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
						}
					} else {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}	
				}
				//validando envio AYNI
				if(countDematerializationPhysical != 0 && countDematerializationPhysical != checkAccountAnotation.length){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_MOTIVE));
					JSFUtilities.showSimpleValidationDialog();
					return null;
				}
				if(countDematerializationPhysical == checkAccountAnotation.length)validDematerialized = true;
				else validDematerialized = false;
				
				blConfirm = true;
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CONFIRM, 
												new Object[]{strOperationNumber}));
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate certify.
	 *
	 * @return the string
	 */
	public String validateCertify(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())
							&& Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
							PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
																						selectedAAO.getSecurity().getIdSecurityCodePk(), 
																						selectedAAO.getCertificateNumber());
							Object[] bodyData = new Object[2];
							   bodyData[0] = selectedAAO.getCertificateNumber();   
							   bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
							if (Validations.validateIsNotNull(physicalBalanceDetail)){
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return null;
							}
					}
				if(selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())){	
						if(DematerializationStateType.REVIEWED.getCode().equals(selectedAAO.getState())){
							blCertify = true;
							blDisabledFields = false;
							indAlternativeCode=true;
//							indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_CERTIFIED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
			} else {
				String strOperationNumber= GeneralConstants.EMPTY_STRING;
				for(AccountAnnotationOperation objAao : checkAccountAnotation)
				{
					PhysicalBalanceDetail physicalBalanceDetail= dematerializationCertificateFacade.validateSecurityCertificate(
																							objAao.getSecurity().getIdSecurityCodePk(), 
																							objAao.getCertificateNumber());
					Object[] bodyData = new Object[2];
					bodyData[0] = objAao.getCertificateNumber();   
					bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
					if (Validations.validateIsNotNull(physicalBalanceDetail)) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
					
					if(objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
					   objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())) 
					{
						if (DematerializationStateType.REVIEWED.getCode().equals(objAao.getState())) {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_CERTIFIED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					} else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}	
				}
				blCertify = true;
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CERTIFY), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CERTIFY, 
												new Object[]{strOperationNumber}));
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		return null;
	}
	
	/**
	 * Validate review.
	 *
	 * @return the string
	 */
	public String validateReview(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())
							&& Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
						PhysicalBalanceDetail physicalBalanceDetail= dematerializationCertificateFacade.validateSecurityCertificate(
																				selectedAAO.getSecurity().getIdSecurityCodePk(), 
																				selectedAAO.getCertificateNumber());
						Object[] bodyData = new Object[2];
						   bodyData[0] = selectedAAO.getCertificateNumber();   
						   bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
						if (Validations.validateIsNotNull(physicalBalanceDetail)){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					if(selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())|| 
					   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
					{
						if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())){
							blReview = true;
							blDisabledFields = false;
							indAlternativeCode=true;
//								indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_REVIEW));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
			} else{
				String strOperationNumber= GeneralConstants.EMPTY_STRING;
				for(AccountAnnotationOperation objAao : checkAccountAnotation)
				{
					PhysicalBalanceDetail physicalBalanceDetail= dematerializationCertificateFacade.validateSecurityCertificate(
																							objAao.getSecurity().getIdSecurityCodePk(), 
																							objAao.getCertificateNumber());
					Object[] bodyData = new Object[2];
					bodyData[0] = objAao.getCertificateNumber();   
					bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
					if (Validations.validateIsNotNull(physicalBalanceDetail)) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
					if(objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())|| 
					   objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
					{
						if (DematerializationStateType.APPROVED.getCode().equals(objAao.getState())) {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					} else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_REVIEW));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}	
				}
				blReview = true;
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REVIEW, 
												new Object[]{strOperationNumber}));
				}		
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		return null;
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER) {
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					// issue 1398: a requerimiento de usuario ya no se valida el estado CERTIFICADO para DPF o DPA
//					if(selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
//					   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
//					{
//						if(!DematerializationStateType.CERTIFIED.getCode().equals(selectedAAO.getState())){
//							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
//							JSFUtilities.showSimpleValidationDialog();
//							return null;
//						}
//					} else {
						if(!DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
//					}
					blReject = true;
					blDisabledFields=false;
					indAlternativeCode=false;
					fillView(selectedAAO);
					return VIEW_MAPPING;
				} else {
					for(AccountAnnotationOperation aao: checkAccountAnotation){
						// issue 1398: a requerimiento de usuario ya no se valida el estado CERTIFICADO para DPF o DPA
//						if(aao.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||aao.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
//						{
//							if(DematerializationStateType.CERTIFIED.getCode().equals(aao.getState())){
//								blReject = true;
//								indAlternativeCode=false;
//								selectedMotive();
//							}else{
//								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//										, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
//								JSFUtilities.showSimpleValidationDialog();
//								return GeneralConstants.EMPTY_STRING;
//							}
//						}
//						else
//						{	
							if(DematerializationStateType.APPROVED.getCode().equals(aao.getState()))
							{
								blReject = true;
								indAlternativeCode=false;
								selectedMotive();
							}
							else
							{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
//						}
					}
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate cancel.
	 *
	 * @return the string
	 */
	public String validateCancel(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
					   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
					{
						if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState()) || 
						   DematerializationStateType.REVIEWED.getCode().equals(selectedAAO.getState()))
						{
							blCancel = true;
							indAlternativeCode=false;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
						}
					 }else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CANCEL));
							JSFUtilities.showSimpleValidationDialog();
							return null;
					}	
				}
				else{
					for(AccountAnnotationOperation selectedAAO: checkAccountAnotation){
						if(selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
						   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
						{
							if(!DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState()) && 
							   !DematerializationStateType.REVIEWED.getCode().equals(selectedAAO.getState()))
							{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
													PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
								JSFUtilities.showSimpleValidationDialog();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CANCEL));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blCancel = true;
					indAlternativeCode=false;
					selectedMotive();
				}		
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	/**
	 * Reject others.
	 */
	public void rejectOthers(){
		registerAccountAnnotationTO.getAccountAnnotationOperation().setComments(null);
		registerAccountAnnotationTO.setBlOthers(false);
		if(DematerializationRejectMotiveType.OTHERS.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject()))
			registerAccountAnnotationTO.setBlOthers(true);		
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		String bodyMessage = null, headerMessage = null;
		String strOperation=GeneralConstants.EMPTY_STRING;
		boolean blValidate=BooleanType.YES.getBooleanValue();
		if(blApprove){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_APPROVE;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_APPROVE_BANK_SHARES;
			}
			else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_APPROVE;
			}
			
		}else if(blAnnul){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_ANNUL;
			for(AccountAnnotationOperation objAao : checkAccountAnotation)
			{
				strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				if(!objAao.getSecurity().isBankStockExchange()){
					blValidate=BooleanType.NO.getBooleanValue();
				}
			}
			if(blValidate==BooleanType.YES.getBooleanValue()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_ANNUL_BANK_SHARES;
			}
			else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_ANNUL;
			}
			if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
				strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), 
								PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
			blValidate=BooleanType.YES.getBooleanValue();
			return;
		}else if(blConfirm){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CONFIRM_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CONFIRM;
			}
		}else if(blReject){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REJECT;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT;
			}
			
			for(AccountAnnotationOperation objAao : checkAccountAnotation)
			{
				strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				if(!objAao.getSecurity().isBankStockExchange()){
					blValidate=BooleanType.NO.getBooleanValue();
				}
			}
			if(blValidate==BooleanType.YES.getBooleanValue()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT_BANK_SHARES;
			}
			else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT;
			}
			if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
				strOperation=strOperation.substring(0,strOperation.length()-1);
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), 
								PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
			blValidate=BooleanType.YES.getBooleanValue();
			return;
		}else if(blReview){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REVIEW;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REVIEW_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REVIEW;
			}			
		}else if(blCertify){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CERTIFY;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CERTIFY_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CERTIFY;
			}
		}else if(blCancel){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CANCEL;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL;
			}			
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), 
							PropertiesUtilities.getMessage(bodyMessage, new Object[]{registerAccountAnnotationTO.getAccountAnnotationOperation().
									getCustodyOperation().getOperationNumber().toString()}));
	}

	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try {
			String bodyMessage = null,strOperation=GeneralConstants.EMPTY_STRING;
			String bodyMessageDesc="";
			BusinessProcess businessProcess = new BusinessProcess();
			Long idBusinessProcess = null;
			if(blApprove){
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_APPROVE.getCode();
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.approveAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_APPROVE_BANK_SHARES;
					}
					else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_APPROVE;
					}
				} else {
					strOperation= dematerializationCertificateFacade.approveMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_APPROVE;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					
					// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
					businessProcess.setIdBusinessProcessPk(idBusinessProcess);
					Object[] parameters = {	strOperation };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					return;
				}
			}else if(blAnnul){
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_ANNUL.getCode();
				for(AccountAnnotationOperation acco : checkAccountAnotation){
					acco.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					acco.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					dematerializationCertificateFacade.annulAnnotationOperation(acco);
					strOperation+=acco.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.substring(0,strOperation.length()-1);
				}
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_ANNUL_BANK_SHARES;
				}
				else{
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_ANNUL;
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
									PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
				searchRequest();
				
				// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
				businessProcess.setIdBusinessProcessPk(idBusinessProcess);
				Object[] parameters = {	strOperation };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
				// FIN ENVIO DE NOTIFICACIONES
				
				return;
			}else if(blConfirm){
				
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM.getCode();
				
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.confirmAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM;
					}
					/*
					if(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive())){
						checkAccountAnotation[0] = registerAccountAnnotationTO.getAccountAnnotationOperation();
						XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero =dematerializationCertificateFacade.sendDepositeDematerializedRecord(checkAccountAnotation);
						if(xmlRespuestaActivoFinanciero.getCodResp().equalsIgnoreCase("0000")){
							bodyMessageDesc = ". " + PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_AYNI_0000);
						}else{
							bodyMessageDesc = ". " + PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_AYNI_1000);
							bodyMessageDesc = bodyMessageDesc + " " + xmlRespuestaActivoFinanciero.getDescResp();
						}
						checkAccountAnotation = null;
					}*/
					
				} else {
					strOperation= dematerializationCertificateFacade.confirmMultipleAnnotationOperation(checkAccountAnotation);
					
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM;
					/*if(validDematerialized){
						XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero =dematerializationCertificateFacade.sendDepositeDematerializedRecord(checkAccountAnotation);
						if(xmlRespuestaActivoFinanciero.getCodResp().equalsIgnoreCase("0000")){
							bodyMessageDesc = ". " + PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_AYNI_0000);
						}else{
							bodyMessageDesc = ". " + PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_AYNI_1000);
							bodyMessageDesc = bodyMessageDesc + " " + xmlRespuestaActivoFinanciero.getDescResp();
						}
					}*/
					
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation})+bodyMessageDesc);
					searchRequest();
					
					// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
					businessProcess.setIdBusinessProcessPk(idBusinessProcess);
					Object[] parameters = {	strOperation };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					return;
				}
			}else if(blReject){
				
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REJECT.getCode();
				
				for(AccountAnnotationOperation acco : checkAccountAnotation){
					acco.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					acco.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					dematerializationCertificateFacade.rejectAnnotationOperation(acco);
					strOperation+=acco.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.substring(0,strOperation.length()-1);
				}
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REJECT_BANK_SHARES;
				}else{
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REJECT;
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
									PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
				searchRequest();
				
				// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
				businessProcess.setIdBusinessProcessPk(idBusinessProcess);
				Object[] parameters = {	strOperation };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
				// FIN ENVIO DE NOTIFICACIONES
				
				return;
			}else if(blReview){
				
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REVIEW.getCode();
				
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.reviewAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REVIEW_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REVIEW;
					}
				} else {
					strOperation= dematerializationCertificateFacade.reviewMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REVIEW;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					
					// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
					businessProcess.setIdBusinessProcessPk(idBusinessProcess);
					Object[] parameters = {	strOperation };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					return;
				}
			}else if(blCertify){
				
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CERTIFY.getCode();
				
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.certifyAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CERTIFY_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CERTIFY;
					}
				} else {
					strOperation= dematerializationCertificateFacade.certifyMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CERTIFY;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					
					// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
					businessProcess.setIdBusinessProcessPk(idBusinessProcess);
					Object[] parameters = {	strOperation };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					return;
				}
			} else if(blCancel){
				
				idBusinessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CANCEL.getCode();
				
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];	
					selectedAAO.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					selectedAAO.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());			
					dematerializationCertificateFacade.cancelAnnotationOperation(selectedAAO);
					if(selectedAAO.getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CANCEL_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CANCEL;
					}
					
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{selectedAAO.getCustodyOperation().getOperationNumber().toString()}));
					searchRequest();
					
					return;
					
				} else {
					for (AccountAnnotationOperation selectedAAO: checkAccountAnotation) {
						selectedAAO.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
						selectedAAO.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					}
					strOperation= dematerializationCertificateFacade.cancelMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CANCEL;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					
					// INICIO ENVIO DE NOTIFICACIONES MULTIPLE					
					businessProcess.setIdBusinessProcessPk(idBusinessProcess);
					Object[] parameters = {	strOperation };
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
					// FIN ENVIO DE NOTIFICACIONES
					
					return;
				}
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
								PropertiesUtilities.getMessage(bodyMessage, new Object[]{registerAccountAnnotationTO.getAccountAnnotationOperation().
																getCustodyOperation().getOperationNumber().toString()})+bodyMessageDesc);
			
			// INICIO ENVIO DE NOTIFICACIONES					
			businessProcess.setIdBusinessProcessPk(idBusinessProcess);
			Object[] parameters = {	registerAccountAnnotationTO.getAccountAnnotationOperation().
					getCustodyOperation().getOperationNumber().toString() };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES
			
			searchRequest();
			
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}		
	}
	
	/**
	 * Fill view.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @throws ServiceException the service exception
	 */
	
	
	private void fillView(AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		
		registerAccountAnnotationTO = new RegisterAccountAnnotationTO();
		registerAccountAnnotationTO.setAccountAnnotationOperation(accountAnnotationOperation);
		GeographicLocationTO filter = new GeographicLocationTO();
		filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		filter.setIdLocationReferenceFk(countryResidence);
		listGeographicLocationDpto = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		filter.setIdLocationReferenceFk(accountAnnotationOperation.getDepartment());
		listGeographicLocationProvince = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		filter.setIdLocationReferenceFk(accountAnnotationOperation.getProvince());
		listGeographicLocationMunicipality = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		if(accountAnnotationOperation.getDepartment()==null){
			listGeographicLocationDpto=null;
		}else{ 
			registerAccountAnnotationTO.setDepartment(accountAnnotationOperation.getDepartment());
		}
		if(accountAnnotationOperation.getProvince()==null){
			listGeographicLocationProvince=null;
		}else{ 
			registerAccountAnnotationTO.setProvince(accountAnnotationOperation.getProvince());
		}
		if(accountAnnotationOperation.getMunicipality()==null){
			listGeographicLocationMunicipality=null;
		}else{
			registerAccountAnnotationTO.setMunicipality(accountAnnotationOperation.getMunicipality());
		}
		
		accountAnnotationOperation.setHolderAccount(dematerializationCertificateFacade.findHolderAccount(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk()));
		for(HolderAccountDetail holderAccDeta:accountAnnotationOperation.getHolderAccount().getHolderAccountDetails()){
			if(BooleanType.YES.getCode().equals(holderAccDeta.getIndRepresentative())){
				registerAccountAnnotationTO.setHolder(holderAccDeta.getHolder());
				break;
			}
		}
		registerAccountAnnotationTO.setIdParticipantPk(accountAnnotationOperation.getHolderAccount().getParticipant().getIdParticipantPk());
		registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
			registerAccountAnnotationTO.setBlMarketFact(true);
			AccountAnnotationMarketFact accAnnMarketFact = dematerializationCertificateFacade.findMarketFact(accountAnnotationOperation.getIdAnnotationOperationPk());
			if(Validations.validateIsNotNullAndNotEmpty(accAnnMarketFact))
				registerAccountAnnotationTO.setAccountAnnotationMarketFact(accAnnMarketFact);			
		}
		registerAccountAnnotationTO.setShareCapital(accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurity().getCurrentNominalValue()));
		registerAccountAnnotationTO.setSecurityClass(accountAnnotationOperation.getSecurity().getSecurityClass());		
		if(InstrumentType.FIXED_INCOME.getCode().equals(accountAnnotationOperation.getSecurity().getInstrumentType()))
			registerAccountAnnotationTO.setBlFixedIncome(true);
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setIndicator1("DESMATERIA");
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		registerAccountAnnotationTO.setLstMotives(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		
		paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		registerAccountAnnotationTO.setLstSecurityClass(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.MOTIVE_REJECT_DEMATERIALIZATION.getCode());
		registerAccountAnnotationTO.setLstRejectMotive(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		if(DematerializationStateType.REJECTED.getCode().equals(accountAnnotationOperation.getState())){
			registerAccountAnnotationTO.setBlRejected(true);
		}
		registerAccountAnnotationTO.getAccountAnnotationOperation().setMotiveDescReject(accountAnnotationOperation.getCustodyOperation().getRejectMotiveOther());
		if(blConfirm){
			if(SecurityClassType.ACC.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) || 
					SecurityClassType.ACE.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass())){
				registerAccountAnnotationTO.setBlPrimarySettlement(true);
			}
		}
		if(blCancel || blAnnul || blReject){
			registerAccountAnnotationTO.setIndGrav(BooleanType.YES.getBooleanValue());
		}	
		if(blApprove || blConfirm || blCertify){
			registerAccountAnnotationTO.setIndGrav(BooleanType.NO.getBooleanValue());
		}
		if (SecurityClassType.DPF.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) || SecurityClassType.DPA.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) ){			
			if(blConfirm){
				registerAccountAnnotationTO.setBlDPF(BooleanType.YES.getBooleanValue());
				indValidator = BooleanType.YES.getBooleanValue();
			}			
			if(BooleanType.YES.getCode().equals(accountAnnotationOperation.getIndLien())){
				registerAccountAnnotationTO.setBlLien(BooleanType.YES.getBooleanValue());
				registerAccountAnnotationTO.getAccountAnnotationOperation().setCreditorSource(accountAnnotationOperation.getCreditorSource());
				indLien=true;
			}else{
					registerAccountAnnotationTO.setBlLien(BooleanType.NO.getBooleanValue());
				}
		}else{
			registerAccountAnnotationTO.setBlDPF(BooleanType.NO.getBooleanValue());
			registerAccountAnnotationTO.setIndGrav(BooleanType.YES.getBooleanValue());
			registerAccountAnnotationTO.setBlLien(BooleanType.NO.getBooleanValue());
			indAlternativeCode=false;
			if(blConfirm){
				indValidator = BooleanType.NO.getBooleanValue();
			}			
		}
		
		// issue 1263: evaluando el indicador de gravamen, NO solo para DPF sino para cualquier valor (a requerimiento del usuario)
		if(Validations.validateIsNotNull(accountAnnotationOperation.getIndLien())
				&& accountAnnotationOperation.getIndLien().equals(BooleanType.YES.getCode()) ){
			registerAccountAnnotationTO.setBlLien(BooleanType.YES.getBooleanValue());
			indLien = true;
		}else{
			registerAccountAnnotationTO.setBlLien(BooleanType.NO.getBooleanValue());
			indLien = false;
		}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnReview(true);
		privilegeComponent.setBtnCertifyView(true);
		privilegeComponent.setBtnCancel(true);
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		mpStates = new HashMap<>();
		mpMotive = new HashMap<>();
		mpMnemonicClassSec = new HashMap<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_STATE.getCode());
		List<ParameterTable> lstStates = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstStates){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				searchAccountAnnotationTO.getStateList().add(paramTab);
			}
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		paramTabTO = new ParameterTableTO();
		paramTabTO.setIndicator1("DESMATERIA");
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		//paramTabTO.setIndicator4(ComponentConstant.ONE);
		paramTabTO.setOrderbyParameterTableCd(ComponentConstant.ONE);
		List<ParameterTable> lstMotives = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstMotives){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				searchAccountAnnotationTO.getMotiveList().add(paramTab);
			}
			mpMotive.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
			
		//filling securityClass list
		paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		List<ParameterTable> lstSecClasses = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstSecClasses){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				searchAccountAnnotationTO.getSecurityClassList().add(paramTab);
			}
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			mpMnemonicClassSec.put(paramTab.getParameterTablePk(), paramTab.getText1());
		}
		
	}
	
	/**
	 * Sets the flags false.
	 */
	private void setFlagsFalse(){
		blApprove = false;
		blAnnul = false;
		blConfirm = false;
		blReject = false;
		blReview = false;
		blCertify = false;
		blCancel = false;
		blDisabledFields= true;
		blRenderedMotives= false;
		
	}
	
	/**
	 * Sets the descriptions.
	 *
	 * @param lstResult the lst result
	 * @return the list
	 */
	private List<AccountAnnotationOperation> setDescriptions(List<AccountAnnotationOperation> lstResult){
		for(AccountAnnotationOperation accAnnOpe:lstResult){
			accAnnOpe.setMotiveDescription(mpMotive.get(accAnnOpe.getMotive()));
			accAnnOpe.setStateDescription(mpStates.get(accAnnOpe.getState()));
		}
		return lstResult;
	}
	
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler(){
		if (registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject() != null) {
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject().equals(DematerializationRejectMotiveType.OTHERS.getCode())){
				registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.TRUE);
			}else{
				registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.FALSE);
			}
		}else{
			registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.FALSE);
		}
	}
	
	/**
	 * Selected motive.
	 */
	public void selectedMotive(){
		try{
				JSFUtilities.resetComponent(":frmMotive:dialogMotive");
				executeAction();
		
				JSFUtilities.hideGeneralDialogues();
				hideLocalComponents();
				newRequest();
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO.setMasterTableFk(MasterTableType.MOTIVE_REJECT_DEMATERIALIZATION.getCode());
				paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
				registerAccountAnnotationTO.setLstRejectMotive(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
				if(registerAccountAnnotationTO.getAccountAnnotationOperation()!=null){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setMotiveReject(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.FALSE);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setMotiveDescReject(null);
				}
		
				JSFUtilities.showComponent(":frmMotive:dialogMotive");
			} catch (Exception e) {
				e.printStackTrace();
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveHandler(){
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		String strOperation=GeneralConstants.EMPTY_STRING;
		if (registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject() == null || 
			!Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject())))) {
			
			JSFUtilities.addContextMessage("frmMotive:cboMotive",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE),
										PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE));
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}	
		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject().equals(DematerializationRejectMotiveType.OTHERS.getCode())){
			if (Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject())) {			
				JSFUtilities.addContextMessage(
						"frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE),
						PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE));
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
						JSFUtilities.showValidationDialog();
						return;
			}else{
				if(blAnnul){
					for(AccountAnnotationOperation objAao : checkAccountAnotation){
						strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					}
					if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
						strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
					}
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ANNUL_REQUEST, 
							new Object[]{strOperation});
				}else if(blReject){
					for(AccountAnnotationOperation objAao : checkAccountAnotation){
						strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					}
					if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
						strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
					}
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_REJECT_REQUEST, 
							new Object[]{strOperation});
				}else if(blCancel){
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL, 
							new Object[]{checkAccountAnotation[0].getCustodyOperation().getOperationNumber().toString()});
				}
			}
		}else{
			if(blAnnul){
				for(AccountAnnotationOperation objAao : checkAccountAnotation){
					strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
				}
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ANNUL_REQUEST, 
						new Object[]{strOperation});
			}else if(blReject){
				for(AccountAnnotationOperation objAao : checkAccountAnotation){
					strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
				}
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_REJECT_REQUEST, 
						new Object[]{strOperation});
			}else if(blCancel){
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL, 
						new Object[]{checkAccountAnotation[0].getCustodyOperation().getOperationNumber().toString()});
			}
		}
		JSFUtilities.showComponent(":frmBlock:cnfblock");
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		hideLocalComponents();
	}
	

	
	/**
	 * Hide local components.
	 */
	public void hideLocalComponents(){
		JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
		//JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
		JSFUtilities.hideGeneralDialogues();
	}
	
	/**
	 * Validate issuer search.
	 */
	public void validateIssuerSearch() {
		if ( !(Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer()) 
				&& Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer().getIdIssuerPk())) ) {
			return;
		}
		issuerSearchPK = searchAccountAnnotationTO.getIssuer().getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);
		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			searchAccountAnnotationTO.setIssuer(new Issuer());
		}
		cleanResults();
	}
	
	/**
	 * Validate issuer register.
	 */
	public void validateIssuerRegister() {
		if ( !(Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer()) 
				&& Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer().getIdIssuerPk())) ) {
			return;
		}
		issuerSearchPK = searchAccountAnnotationTO.getIssuer().getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);
		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			searchAccountAnnotationTO.setIssuer(new Issuer());
		}
		cleanResults();
	}
	
	/**
	 * Alert.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */
	public void alert(String headerMessage, String bodyMessage) {
		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency = generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );		
	}

	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Gets the search account annotation to.
	 *
	 * @return the search account annotation to
	 */
	public SearchAccountAnnotationTO getSearchAccountAnnotationTO() {
		return searchAccountAnnotationTO;
	}
	
	/**
	 * Sets the search account annotation to.
	 *
	 * @param searchAccountAnnotationTO the new search account annotation to
	 */
	public void setSearchAccountAnnotationTO(
			SearchAccountAnnotationTO searchAccountAnnotationTO) {
		this.searchAccountAnnotationTO = searchAccountAnnotationTO;
	}
	
	/**
	 * Gets the register account annotation to.
	 *
	 * @return the register account annotation to
	 */
	public RegisterAccountAnnotationTO getRegisterAccountAnnotationTO() {
		return registerAccountAnnotationTO;
	}
	
	/**
	 * Sets the register account annotation to.
	 *
	 * @param registerAccountAnnotationTO the new register account annotation to
	 */
	public void setRegisterAccountAnnotationTO(
			RegisterAccountAnnotationTO registerAccountAnnotationTO) {
		this.registerAccountAnnotationTO = registerAccountAnnotationTO;
	}
	
	/**
	 * Checks if is bl approve.
	 *
	 * @return true, if is bl approve
	 */
	public boolean isBlApprove() {
		return blApprove;
	}
	
	/**
	 * Sets the bl approve.
	 *
	 * @param blApprove the new bl approve
	 */
	public void setBlApprove(boolean blApprove) {
		this.blApprove = blApprove;
	}
	
	/**
	 * Checks if is bl annul.
	 *
	 * @return true, if is bl annul
	 */
	public boolean isBlAnnul() {
		return blAnnul;
	}
	
	/**
	 * Sets the bl annul.
	 *
	 * @param blAnnul the new bl annul
	 */
	public void setBlAnnul(boolean blAnnul) {
		this.blAnnul = blAnnul;
	}
	
	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}
	
	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}
	
	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}
	
	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}
	
	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}
	
	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}


	/**
	 * Checks if is bl cancel.
	 *
	 * @return true, if is bl cancel
	 */
	public boolean isBlCancel() {
		return blCancel;
	}

	/**
	 * Sets the bl cancel.
	 *
	 * @param blCancel the new bl cancel
	 */
	public void setBlCancel(boolean blCancel) {
		this.blCancel = blCancel;
	}

	/**
	 * Checks if is bl review.
	 *
	 * @return true, if is bl review
	 */
	public boolean isBlReview() {
		return blReview;
	}

	/**
	 * Sets the bl review.
	 *
	 * @param blReview the new bl review
	 */
	public void setBlReview(boolean blReview) {
		this.blReview = blReview;
	}

	/**
	 * Checks if is bl certify.
	 *
	 * @return true, if is bl certify
	 */
	public boolean isBlCertify() {
		return blCertify;
	}

	/**
	 * Sets the bl certify.
	 *
	 * @param blCertify the new bl certify
	 */
	public void setBlCertify(boolean blCertify) {
		this.blCertify = blCertify;
	}

	/**
	 * Checks if is bl disabled fields.
	 *
	 * @return true, if is bl disabled fields
	 */
	public boolean isBlDisabledFields() {
		return blDisabledFields;
	}

	public AccAnnotationOperationFile getFileExcelPersist() {
		return fileExcelPersist;
	}

	public void setFileExcelPersist(AccAnnotationOperationFile fileExcelPersist) {
		this.fileExcelPersist = fileExcelPersist;
	}

	/**
	 * Sets the bl disabled fields.
	 *
	 * @param blDisabledFields the new bl disabled fields
	 */
	public void setBlDisabledFields(boolean blDisabledFields) {
		this.blDisabledFields = blDisabledFields;
	}

	/**
	 * Checks if is bl rendered motives.
	 *
	 * @return true, if is bl rendered motives
	 */
	public boolean isBlRenderedMotives() {
		return blRenderedMotives;
	}

	/**
	 * Sets the bl rendered motives.
	 *
	 * @param blRenderedMotives the new bl rendered motives
	 */
	public void setBlRenderedMotives(boolean blRenderedMotives) {
		this.blRenderedMotives = blRenderedMotives;
	}

	/**
	 * Gets the param service bean.
	 *
	 * @return the param service bean
	 */
	public ParameterServiceBean getParamServiceBean() {
		return paramServiceBean;
	}

	/**
	 * Sets the param service bean.
	 *
	 * @param paramServiceBean the new param service bean
	 */
	public void setParamServiceBean(ParameterServiceBean paramServiceBean) {
		this.paramServiceBean = paramServiceBean;
	}

	/**
	 * Gets the mp mnemonic class sec.
	 *
	 * @return the mp mnemonic class sec
	 */
	public Map<Integer, String> getMpMnemonicClassSec() {
		return mpMnemonicClassSec;
	}

	/**
	 * Sets the mp mnemonic class sec.
	 *
	 * @param mpMnemonicClassSec the mp mnemonic class sec
	 */
	public void setMpMnemonicClassSec(Map<Integer, String> mpMnemonicClassSec) {
		this.mpMnemonicClassSec = mpMnemonicClassSec;
	}

	/**
	 * Gets the check account anotation.
	 *
	 * @return the check account anotation
	 */
	public AccountAnnotationOperation[] getCheckAccountAnotation() {
		return checkAccountAnotation;
	}

	public List<String> getLstErrorReadExcel() {
		return lstErrorReadExcel;
	}

	public void setLstErrorReadExcel(List<String> lstErrorReadExcel) {
		this.lstErrorReadExcel = lstErrorReadExcel;
	}

	/**
	 * Sets the check account anotation.
	 *
	 * @param checkAccountAnotation the new check account anotation
	 */
	public void setCheckAccountAnotation(
			AccountAnnotationOperation[] checkAccountAnotation) {
		this.checkAccountAnotation = checkAccountAnotation;
	}

	/**
	 * Gets the list geographic location.
	 *
	 * @return the list geographic location
	 */
	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocationDpto;
	}

	/**
	 * Sets the list geographic location.
	 *
	 * @param listGeographicLocation the new list geographic location
	 */
	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocationDpto = listGeographicLocation;
	}

	/**
	 * Gets the list geographic location dpto.
	 *
	 * @return the list geographic location dpto
	 */
	public List<GeographicLocation> getListGeographicLocationDpto() {
		return listGeographicLocationDpto;
	}

	/**
	 * Sets the list geographic location dpto.
	 *
	 * @param listGeographicLocationDpto the new list geographic location dpto
	 */
	public void setListGeographicLocationDpto(
			List<GeographicLocation> listGeographicLocationDpto) {
		this.listGeographicLocationDpto = listGeographicLocationDpto;
	}


	/**
	 * Gets the list geographic location province.
	 *
	 * @return the list geographic location province
	 */
	public List<GeographicLocation> getListGeographicLocationProvince() {
		return listGeographicLocationProvince;
	}


	/**
	 * Sets the list geographic location province.
	 *
	 * @param listGeographicLocationProvince the new list geographic location province
	 */
	public void setListGeographicLocationProvince(
			List<GeographicLocation> listGeographicLocationProvince) {
		this.listGeographicLocationProvince = listGeographicLocationProvince;
	}


	/**
	 * Gets the list geographic location municipality.
	 *
	 * @return the list geographic location municipality
	 */
	public List<GeographicLocation> getListGeographicLocationMunicipality() {
		return listGeographicLocationMunicipality;
	}


	/**
	 * Sets the list geographic location municipality.
	 *
	 * @param listGeographicLocationMunicipality the new list geographic location municipality
	 */
	public void setListGeographicLocationMunicipality(
			List<GeographicLocation> listGeographicLocationMunicipality) {
		this.listGeographicLocationMunicipality = listGeographicLocationMunicipality;
	}

	/**
	 * Checks if is ind alternative code.
	 *
	 * @return true, if is ind alternative code
	 */
	public boolean isIndAlternativeCode() {
		return indAlternativeCode;
	}

	/**
	 * Sets the ind alternative code.
	 *
	 * @param indAlternativeCode the new ind alternative code
	 */
	public void setIndAlternativeCode(boolean indAlternativeCode) {
		this.indAlternativeCode = indAlternativeCode;
	}

	/**
	 * Checks if is ind validator.
	 *
	 * @return true, if is ind validator
	 */
	public boolean isIndValidator() {
		return indValidator;
	}

	/**
	 * Sets the ind validator.
	 *
	 * @param indValidator the new ind validator
	 */
	public void setIndValidator(boolean indValidator) {
		this.indValidator = indValidator;
	}

	/**
	 * Checks if is ind lien.
	 *
	 * @return true, if is ind lien
	 */
	public boolean isIndLien() {
		return indLien;
	}

	/**
	 * Sets the ind lien.
	 *
	 * @param indLien the new ind lien
	 */
	public void setIndLien(boolean indLien) {
		this.indLien = indLien;
	}

	/**
	 * Checks if is bl certificate disable.
	 *
	 * @return true, if is bl certificate disable
	 */
	public boolean isBlCertificateDisable() {
		return blCertificateDisable;
	}

	/**
	 * Sets the bl certificate disable.
	 *
	 * @param blCertificateDisable the new bl certificate disable
	 */
	public void setBlCertificateDisable(boolean blCertificateDisable) {
		this.blCertificateDisable = blCertificateDisable;
	}

	/**
	 * Gets the issuer search pk.
	 *
	 * @return the issuerSearchPK
	 */
	public String getIssuerSearchPK() {
		return issuerSearchPK;
	}

	/**
	 * Sets the issuer search pk.
	 *
	 * @param issuerSearchPK the issuerSearchPK to set
	 */
	public void setIssuerSearchPK(String issuerSearchPK) {
		this.issuerSearchPK = issuerSearchPK;
	}

	/**
	 * Gets the lst cbo filter currency.
	 *
	 * @return the lstCboFilterCurrency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	/**
	 * Sets the lst cbo filter currency.
	 *
	 * @param lstCboFilterCurrency the lstCboFilterCurrency to set
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	/**
	 * Checks if is bl bloc issuer.
	 *
	 * @return the blBlocIssuer
	 */
	public boolean isBlBlocIssuer() {
		return blBlocIssuer;
	}

	/**
	 * Sets the bl bloc issuer.
	 *
	 * @param blBlocIssuer the blBlocIssuer to set
	 */
	public void setBlBlocIssuer(boolean blBlocIssuer) {
		this.blBlocIssuer = blBlocIssuer;
	}

	public String getTitleLabel() {
		return titleLabel;
	}

	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

	/**
	 * Gets the massive interface type.
	 *
	 * @return the massive interface type
	 */
	public String getMassiveInterfaceType() {
		return massiveInterfaceType;
	}

	/**
	 * Sets the massive interface type.
	 *
	 * @param massiveInterfaceType the new massive interface type
	 */
	public void setMassiveInterfaceType(String massiveInterfaceType) {
		this.massiveInterfaceType = massiveInterfaceType;
	}

	public List<HolderMarketFactBalance> getListHolderMarketFactBalance() {
		return listHolderMarketFactBalance;
	}

	public void setListHolderMarketFactBalance(
			List<HolderMarketFactBalance> listHolderMarketFactBalance) {
		this.listHolderMarketFactBalance = listHolderMarketFactBalance;
	}
	
	public HolderMarketFactBalance getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	public void setHolderMarketFactBalance(
			HolderMarketFactBalance holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Checks if is bl error.
	 *
	 * @return the blError
	 */
	public boolean isBlError() {
		return blError;
	}

	/**
	 * Sets the bl error.
	 *
	 * @param blError the blError to set
	 */
	public void setBlError(boolean blError) {
		this.blError = blError;
	}

	public boolean isBroker() {
		return broker;
	}

	public void setBroker(boolean broker) {
		this.broker = broker;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/** issue 1398: de acuerdo a requerimiento de usuario, obliga llenar el campo 'Nro de certificado' solo cuando el motivo es 'Desmaterializacion de un Fisico' */
	public boolean isRequiredCertificateNumber() {
		return requiredCertificateNumber;
	}

	/** issue 1398: de acuerdo a requerimiento de usuario, obliga llenar el campo 'Nro de certificado' solo cuando el motivo es 'Desmaterializacion de un Fisico' */
	public void setRequiredCertificateNumber(boolean requiredCertificateNumber) {
		this.requiredCertificateNumber = requiredCertificateNumber;
	}
}
