package com.pradera.custody.dematerializationcertificate.service;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.codec.binary.Base64;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.Permutations;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.component.helperui.to.SecuritySearchTO;
import com.pradera.core.component.securities.service.SecuritiesServiceBean;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.custody.authorizedsignature.service.AuthorizedSignatureServiceBean;
import com.pradera.custody.channel.opening.service.ChannelOpeningServiceBean;
import com.pradera.custody.channel.opening.to.ChannelOpeningModelTO;
import com.pradera.custody.dematerializationcertificate.to.SearchAccountAnnotationTO;
import com.pradera.custody.operations.remove.service.RemoveOperationsServiceBean;
import com.pradera.custody.retirementoperation.service.RetirementOperationServiceBean;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.tranfersecurities.accounts.service.HolderAccountServiceBean;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.accounts.to.JuridicHolderObjectTO;
import com.pradera.integration.component.accounts.to.NaturalHolderObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.QueryParametersConsultCdpf;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.business.to.SecurityConsultationTO;
import com.pradera.integration.component.custody.to.AccountEntryQueryTO;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.AuthorizedSignatureQueryResponseTO;
import com.pradera.integration.component.custody.to.AuthorizedSignatureQueryTO;
import com.pradera.integration.component.custody.to.AuthorizedSignatureTO;
import com.pradera.integration.component.custody.to.InstitutionQueryResponseTO;
import com.pradera.integration.component.custody.to.PhysicalCertificateQueryTO;
import com.pradera.integration.component.custody.to.PhysicalCertificateResponseTO;
import com.pradera.integration.component.custody.to.PhysicalCertificateTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.custody.to.SecondaryAccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.SecurityEntryQueryTO;
import com.pradera.integration.component.custody.to.XmlActivoFinanciero;
import com.pradera.integration.component.custody.to.XmlSolicitudActivoFinanciero;
import com.pradera.integration.component.securities.service.SecuritiesRemoteService;
import com.pradera.integration.component.securities.to.AmountLocationObjectBbxBtxTO;
import com.pradera.integration.component.securities.to.CouponObjectTO;
import com.pradera.integration.component.securities.to.HolderAccountObjectBbXBtxTO;
import com.pradera.integration.component.securities.to.PlacementObjectBbxBtxTO;
import com.pradera.integration.component.securities.to.SecurityObjectBbxBtxTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.HolderMarketFactMovement;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.MovementType;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.authorizedSignature.AuthorizedSignature;
import com.pradera.model.custody.channelopening.type.ChannelOpeningStateType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccAnnotationOperationFile;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationRejectMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceProcess;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.AuthorizeSignatureStateType;
import com.pradera.model.generalparameter.type.MasterTableStatusType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.placementsegment.PlacementSegmentDetail;
import com.pradera.model.issuancesecuritie.placementsegment.type.PlacementSegmentStateType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DematerializationCertificateServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Lock(LockType.READ)
public class DematerializationCertificateServiceBean extends CrudDaoServiceBean {
	

	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;	
	
	/** The securities component service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentService;	
	
	/** The accounts remote service. */
	@Inject
	private Instance<AccountsRemoteService> accountsRemoteService;
	
	/** The securities remote service. */
	@Inject
	private Instance<SecuritiesRemoteService> securitiesRemoteService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The participant service bean. */
	@EJB
	ParticipantServiceBean participantServiceBean;	
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;

	/** The parameter service bean. */
	@EJB
	HolderAccountServiceBean holderAccountServiceBean;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The custody service consumer. */
	@EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	/** The remove operations service bean. */
	@EJB
	private RemoveOperationsServiceBean removeOperationsServiceBean;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalPametersFacade;
	 
	/** The securities query service. */
	@EJB
	private SecuritiesQueryServiceBean securitiesQueryService;
	
	/** The Custody query service */
	@EJB
	private RetirementOperationServiceBean retirementOperationServiceBean;
	
	@EJB
	private ChannelOpeningServiceBean channelOpeningServiceBean;
	
	@EJB
	private AuthorizedSignatureServiceBean authorizedSignatureServiceBean;
	
	@EJB
	SecuritiesServiceBean securitiesServiceBean;

	@EJB 
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The Constant logger. */
	@Inject  
	PraderaLogger logger;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The country residence. */
	@Inject
	@Configurable("countryResidence")
	private Integer countryResidence;
	
	/** The id participant bc. */
	@Inject @Configurable Long idParticipantBC;
	
	/** The id participant vun. */
	@Inject @Configurable Long idParticipantVUN;
	
	/** The document type description. */
	private Map<Integer,String> documentTypeDescription = null;
	
	public List<Issuer> lstIssuersStateRegistered() throws ServiceException{
		StringBuilder sd=new StringBuilder();
		sd.append(" select i");
		sd.append(" from Issuer i");
		sd.append(" where i.stateIssuer = ").append(IssuerStateType.REGISTERED.getCode());
		sd.append(" order by i.mnemonic asc");
		return findListByQueryString(sd.toString());
	}
	
	/**
	 * Find account annotation operation by filter.
	 *
	 * @param searchAccountAnnotationTO the search account annotation to
	 * @return the account annotation operation
	 * @throws ServiceException the service exception
	 */
	public AccountAnnotationOperation findAccountAnnotationOperationByFilter(SearchAccountAnnotationTO searchAccountAnnotationTO)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		sbQuery.append("Select aao From AccountAnnotationOperation aao  ");
		sbQuery.append(" join fetch aao.holderAccount ");
		sbQuery.append(" join fetch aao.security ");
		sbQuery.append(" join fetch aao.custodyOperation co");
		sbQuery.append(" Where aao.indPrimarySettlement != 1 ");
		
		if(Validations.validateIsNotNull(searchAccountAnnotationTO.getOperationDate())){
			sbQuery.append(" and trunc(co.registryDate) =:registryDate");
			parameters.put("registryDate", searchAccountAnnotationTO.getOperationDate());
		}
		
		if(Validations.validateIsNotNull(searchAccountAnnotationTO.getAnnotationState())){
			sbQuery.append(" and aao.state =:astate");
			parameters.put("astate", searchAccountAnnotationTO.getAnnotationState());
		}

		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getRequestNumber())){
			sbQuery.append(" and co.operationNumber=:operationNumber");
			parameters.put("operationNumber", searchAccountAnnotationTO.getRequestNumber());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getListOperationTypePk())){
			sbQuery.append(" and co.operationType in :operationType");
			parameters.put("operationType",searchAccountAnnotationTO.getListOperationTypePk());
		}

		return (AccountAnnotationOperation) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> searchAccountAnnotationOperation(Date dateToCancel) throws ServiceException{

		List<AccountAnnotationOperation> lstAccountAnnotationOperation = new ArrayList<AccountAnnotationOperation>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();  
		
		sbQuery.append("	SELECT distinct aao FROM AccountAnnotationOperation aao");
		sbQuery.append("	JOIN FETCH aao.custodyOperation co");
		sbQuery.append("	WHERE trunc(co.registryDate) <= :endDate");
		sbQuery.append("	AND aao.indImmobilization = :indImmobilization");
		sbQuery.append("	AND aao.state in (:lstInState) ");
			
		List<Integer> lstAnnotationStates = new ArrayList<Integer>();
		lstAnnotationStates.add(DematerializationStateType.REIGSTERED.getCode());
		lstAnnotationStates.add(DematerializationStateType.APPROVED.getCode());
		
		parameters.put("endDate", dateToCancel);
		parameters.put("indImmobilization", 1);
		parameters.put("lstInState", lstAnnotationStates);
		
		return (List<AccountAnnotationOperation>) findListByQueryString(sbQuery.toString(),parameters);
		
	}
	
	/**
	 * Search account annotation operation.
	 *
	 * @param searchAccountAnnotationTO the search account annotation to
	 * @param blParticipant the bl participant
	 * @param blIssuer the bl issuer
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> searchAccountAnnotationOperation(SearchAccountAnnotationTO searchAccountAnnotationTO, 
								boolean blParticipant, boolean blIssuer) throws ServiceException{

		List<AccountAnnotationOperation> lstAccountAnnotationOperation = new ArrayList<AccountAnnotationOperation>();
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();  
		
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao");
		sbQuery.append("	LEFT JOIN FETCH aao.issuer");
		sbQuery.append("	LEFT JOIN FETCH aao.securitiesManager");
		sbQuery.append("	INNER JOIN  aao.custodyOperation co");
		sbQuery.append("	INNER JOIN FETCH aao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH ha.participant pa");		
		sbQuery.append(" 	LEFT JOIN FETCH aao.security sec");
		sbQuery.append(" 	LEFT JOIN FETCH sec.issuer isu");	
		sbQuery.append(" 	LEFT JOIN FETCH sec.issuance ");
		if(BooleanType.NO.getCode().equals(searchAccountAnnotationTO.getSearchType())){
			sbQuery.append("	WHERE co.operationNumber = :requestNumber");
			parameters.put("requestNumber", searchAccountAnnotationTO.getRequestNumber());
			
			if(blParticipant){
				sbQuery.append(" 	AND pa.idParticipantPk = :participantPk");
				parameters.put("participantPk", searchAccountAnnotationTO.getIdParticipant());
			}
		}else{
			sbQuery.append("	WHERE trunc(co.registryDate) between :starDate and :endDate");
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getIdParticipant())){
				sbQuery.append(" AND pa.idParticipantPk = :participantPk");
				parameters.put("participantPk", searchAccountAnnotationTO.getIdParticipant());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getAnnotationState())){
				sbQuery.append("	AND aao.state = :annotationState");
				parameters.put("annotationState", searchAccountAnnotationTO.getAnnotationState());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getHolderAccount().getAccountNumber())){
				sbQuery.append(" 	AND ha.accountNumber = :accountNumber");
				parameters.put("accountNumber", searchAccountAnnotationTO.getHolderAccount().getAccountNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getCertificateNumber())){
				sbQuery.append("	AND aao.certificateNumber = :certNumber");
				parameters.put("certNumber", searchAccountAnnotationTO.getCertificateNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSerialNumber())){
				sbQuery.append("	AND aao.serialNumber = :serlNumber");
				parameters.put("serlNumber", searchAccountAnnotationTO.getSerialNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSecurity().getIdSecurityCodePk())){
				sbQuery.append("	AND sec.idSecurityCodePk=:isinCode");
				parameters.put("isinCode", searchAccountAnnotationTO.getSecurity().getIdSecurityCodePk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getHolder().getIdHolderPk())){
				sbQuery.append(" AND (SELECT count(had) FROM HolderAccountDetail had where had.holderAccount.idHolderAccountPk=ha.idHolderAccountPk");
				sbQuery.append(" AND had.holder.idHolderPk = :idHolderPk");
				sbQuery.append(" AND had.indRepresentative = :indYes ) > 0");
				parameters.put("idHolderPk", searchAccountAnnotationTO.getHolder().getIdHolderPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getAnnotationMotive())){
				sbQuery.append("	AND aao.motive = :idMotive");
				parameters.put("idMotive", searchAccountAnnotationTO.getAnnotationMotive());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSecurity().getSecurityClass())){
				sbQuery.append("	AND sec.securityClass = :idSecurityClass");
				parameters.put("idSecurityClass", searchAccountAnnotationTO.getSecurity().getSecurityClass());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSecurityCurrency())){
				sbQuery.append("	AND sec.currency = :idCurrency");
				parameters.put("idCurrency", searchAccountAnnotationTO.getSecurityCurrency());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getIssuer().getIdIssuerPk())){
				sbQuery.append("	AND isu.idIssuerPk = :idIssuerPk");
				parameters.put("idIssuerPk", searchAccountAnnotationTO.getIssuer().getIdIssuerPk());
			}			
			parameters.put("starDate", searchAccountAnnotationTO.getStartDate());
			parameters.put("endDate", searchAccountAnnotationTO.getEndDate());

			sbQuery.append("	AND aao.indImmobilization = :indImmobilization");
			parameters.put("indImmobilization", 1);
			
		}
		if(blIssuer){
			List<Integer> lstClass = new ArrayList<Integer>();
			lstClass.add(SecurityClassType.DPF.getCode());
			lstClass.add(SecurityClassType.DPA.getCode());
			
			sbQuery.append("	AND sec.securityClass in (:securityClass) ");
			sbQuery.append("	AND isu.idIssuerPk = :idIssuerPk");
			
			parameters.put("securityClass", lstClass);
			parameters.put("idIssuerPk", searchAccountAnnotationTO.getIssuer().getIdIssuerPk());
		}
		sbQuery.append(" 	ORDER BY co.operationNumber desc ");
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getHolder().getIdHolderPk())){
			parameters.put("indYes", BooleanType.YES.getCode());
		}
		lstAccountAnnotationOperation = (List<AccountAnnotationOperation>) findListByQueryString(sbQuery.toString(),parameters);
		
		return lstAccountAnnotationOperation;
	
	}
	
	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> searchAccountAnnotationOperationOriginal(SearchAccountAnnotationTO searchAccountAnnotationTO, 
								boolean blParticipant, boolean blIssuer) throws ServiceException{
		List<AccountAnnotationOperation> lstAccountAnnotationOperation = new ArrayList<AccountAnnotationOperation>();
		//List<AccountAnnotationOperation> lstReturn = null;
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();  
		
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao");
		sbQuery.append("	INNER JOIN  aao.custodyOperation co");
		sbQuery.append("	INNER JOIN FETCH aao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH ha.participant pa");		
		sbQuery.append(" 	INNER JOIN FETCH aao.security sec");
		sbQuery.append(" 	INNER JOIN FETCH sec.issuer isu");	
		sbQuery.append(" 	INNER JOIN FETCH sec.issuance ");
		sbQuery.append(" 	LEFT JOIN FETCH aao.idAccAnnotationFileFk ");
		sbQuery.append(" 	WHERE aao.indDematerialization = :indDematerialization ");

		parameters.put("indDematerialization", 1);
		
		if(BooleanType.NO.getCode().equals(searchAccountAnnotationTO.getSearchType())){
			sbQuery.append("	AND co.operationNumber = :requestNumber");
			parameters.put("requestNumber", searchAccountAnnotationTO.getRequestNumber());
			
			if(blParticipant){
				sbQuery.append(" 	AND pa.idParticipantPk = :participantPk");
				parameters.put("participantPk", searchAccountAnnotationTO.getIdParticipant());
			}
		}else{
			sbQuery.append("	AND trunc(co.registryDate) between :starDate and :endDate");
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getIdParticipant())){
				sbQuery.append(" AND pa.idParticipantPk = :participantPk");
				parameters.put("participantPk", searchAccountAnnotationTO.getIdParticipant());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getAnnotationState())){
				sbQuery.append("	AND aao.state = :annotationState");
				parameters.put("annotationState", searchAccountAnnotationTO.getAnnotationState());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getHolderAccount().getAccountNumber())){
				sbQuery.append(" 	AND ha.accountNumber = :accountNumber");
				parameters.put("accountNumber", searchAccountAnnotationTO.getHolderAccount().getAccountNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getCertificateNumber())){
				sbQuery.append("	AND aao.certificateNumber = :certNumber");
				parameters.put("certNumber", searchAccountAnnotationTO.getCertificateNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSerialNumber())){
				sbQuery.append("	AND aao.serialNumber = :serlNumber");
				parameters.put("serlNumber", searchAccountAnnotationTO.getSerialNumber());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSecurity().getIdSecurityCodePk())){
				sbQuery.append("	AND sec.idSecurityCodePk=:isinCode");
				parameters.put("isinCode", searchAccountAnnotationTO.getSecurity().getIdSecurityCodePk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getHolder().getIdHolderPk())){
				sbQuery.append(" AND (SELECT count(had) FROM HolderAccountDetail had where had.holderAccount.idHolderAccountPk=ha.idHolderAccountPk");
				sbQuery.append(" AND had.holder.idHolderPk = :idHolderPk");
				sbQuery.append(" AND had.indRepresentative = :indYes ) > 0");
				parameters.put("idHolderPk", searchAccountAnnotationTO.getHolder().getIdHolderPk());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getAnnotationMotive())){
				sbQuery.append("	AND aao.motive = :idMotive");
				parameters.put("idMotive", searchAccountAnnotationTO.getAnnotationMotive());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSecurity().getSecurityClass())){
				sbQuery.append("	AND sec.securityClass = :idSecurityClass");
				parameters.put("idSecurityClass", searchAccountAnnotationTO.getSecurity().getSecurityClass());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getSecurityCurrency())){
				sbQuery.append("	AND sec.currency = :idCurrency");
				parameters.put("idCurrency", searchAccountAnnotationTO.getSecurityCurrency());
			}
			if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getIssuer().getIdIssuerPk())){
				sbQuery.append("	AND isu.idIssuerPk = :idIssuerPk");
				parameters.put("idIssuerPk", searchAccountAnnotationTO.getIssuer().getIdIssuerPk());
			}			
			parameters.put("starDate", searchAccountAnnotationTO.getStartDate());
			parameters.put("endDate", searchAccountAnnotationTO.getEndDate());
		}
		if(blIssuer){
			List<Integer> lstClass = new ArrayList<Integer>();
			lstClass.add(SecurityClassType.DPF.getCode());
			lstClass.add(SecurityClassType.DPA.getCode());
			
			sbQuery.append("	AND sec.securityClass in (:securityClass) ");
			sbQuery.append("	AND isu.idIssuerPk = :idIssuerPk");
			
			parameters.put("securityClass", lstClass);
			parameters.put("idIssuerPk", searchAccountAnnotationTO.getIssuer().getIdIssuerPk());
		}
		sbQuery.append(" 	ORDER BY co.operationNumber desc ");
		if(Validations.validateIsNotNullAndNotEmpty(searchAccountAnnotationTO.getHolder().getIdHolderPk())){
			parameters.put("indYes", BooleanType.YES.getCode());
		}
		lstAccountAnnotationOperation = (List<AccountAnnotationOperation>) findListByQueryString(sbQuery.toString(),parameters);
		
		return lstAccountAnnotationOperation;
	
	}
	/**
	 * Gets the holder account.
	 *
	 * @param holderAcc the holder acc
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountAnnotation(HolderAccount holderAcc, Boolean onlyInvestoGroup) throws ServiceException{
		HolderAccount holderAccount = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			
		sbQuery.append(" select ha from HolderAccount ha join fetch ha.participant pa join fetch ha.holderAccountDetails had join fetch had.holder ho ");
		sbQuery.append(" where 1 = 1 ");
		
		if(onlyInvestoGroup) {
			sbQuery.append(" and ha.accountGroup= :investor");	
		}
		if(Validations.validateIsNotNullAndPositive(holderAcc.getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :idHolderAccountPkPrm " );
		}else{
			
			if(Validations.validateIsNotNullAndPositive(holderAcc.getAccountNumber())){
				sbQuery.append(" and ha.accountNumber = :accountNumberPrm " );
			}			
			if(Validations.validateIsNotNull(holderAcc.getParticipant()) &&
			   Validations.validateIsNotNullAndPositive(holderAcc.getParticipant().getIdParticipantPk()) ){
				sbQuery.append(" and pa.idParticipantPk = :idParticipantPkPrm " );
			}
			
		}				
		TypedQuery<HolderAccount> typedQuery = em.createQuery(sbQuery.toString(),HolderAccount.class); 
		if(Validations.validateIsNotNullAndPositive(holderAcc.getIdHolderAccountPk())){
			typedQuery.setParameter("idHolderAccountPkPrm", holderAcc.getIdHolderAccountPk());
		}else{

			if(Validations.validateIsNotNullAndPositive(holderAcc.getAccountNumber())){
				typedQuery.setParameter("accountNumberPrm",holderAcc.getAccountNumber());
			}
			
			if(Validations.validateIsNotNull(holderAcc.getParticipant()) &&
			   Validations.validateIsNotNullAndPositive(holderAcc.getParticipant().getIdParticipantPk()) ){
				typedQuery.setParameter("idParticipantPkPrm",holderAcc.getParticipant().getIdParticipantPk());
			}						
		}
		if(onlyInvestoGroup) {
			typedQuery.setParameter("investor",HolderAccountGroupType.INVERSTOR.getCode());
		}
		
		holderAccount = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			logger.info("No result found for named query: " + sbQuery.toString());
		} catch (Exception e) {
			logger.info("Error while running query: " + e.getMessage());
			e.printStackTrace();
		}		
		return holderAccount;
	}
	
	/**
	 * Search account annotation operation for batch process.
	 *
	 * @param lstState the lst state
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> searchAccountAnnotationOperationForBatchProcess(List<Integer> lstState)throws ServiceException{		
		Query query = em.createQuery("select aao FROM AccountAnnotationOperation aao where aao.state in :stateList ");
		query.setParameter("stateList", lstState);
		return (List<AccountAnnotationOperation>)query.getResultList();
	}

	/**
	 * Find account annotation.
	 *
	 * @param idAccountAnnotationOperationPk the id account annotation operation pk
	 * @return the account annotation operation
	 * @throws ServiceException the service exception
	 */
	public AccountAnnotationOperation findAccountAnnotation(Long idAccountAnnotationOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao");
			sbQuery.append("	INNER JOIN FETCH aao.holderAccount ha");
			sbQuery.append(" 	LEFT JOIN FETCH aao.security se");
			sbQuery.append("	WHERE aao.idAnnotationOperationPk = :idAnnotationOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idAnnotationOperationPk", idAccountAnnotationOperationPk);
			return (AccountAnnotationOperation)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	public List<AccountAnnotationCupon> findAccountAnnotationCupon(Long idAccountAnnotationOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aac FROM AccountAnnotationCupon aac ");
		sbQuery.append("	LEFT JOIN FETCH aac.accountAnnotationOperation aao ");
		sbQuery.append("	WHERE aao.idAnnotationOperationPk = :idAnnotationOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAnnotationOperationPk", idAccountAnnotationOperationPk);
		return query.getResultList();
	}
	/**
	 * Find issuance and issuer.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuance
	 */
	public Issuance findIssuanceAndIssuer(String idSecurityCodePk){
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ice FROM Issuance ice");
			sbQuery.append("	INNER JOIN FETCH ice.issuer");
			sbQuery.append("	WHERE ice.idIssuanceCodePk in ( ");
			sbQuery.append("		SELECT se.issuance.idIssuanceCodePk FROM Security se");
			sbQuery.append("		WHERE se.idSecurityCodePk = :idSecurityCodePk");
			sbQuery.append("	) ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			return (Issuance)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	INNER JOIN FETCH ha.participant");
			sbQuery.append("	WHERE ha.idHolderAccountPk = :idHolderAccountPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			return (HolderAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * Find holder accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> findHolderAccounts(Long idHolderPk, Long idParticipantPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT distinct had.holderAccount.idHolderAccountPk ");
		sbQuery.append("	FROM HolderAccountDetail had ");
		sbQuery.append("	WHERE had.holder.idHolderPk = :idHolderPk ");
		sbQuery.append("	AND had.holderAccount.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append("	AND had.holderAccount.stateAccount = :state ");
		sbQuery.append("	AND had.holderAccount.accountGroup = :accountGroup ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPk", idHolderPk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("state", HolderAccountStatusType.ACTIVE.getCode());
		query.setParameter("accountGroup", HolderAccountGroupType.INVERSTOR.getCode());
		List<Long> lstIdHolderAccountPk = (List<Long>)query.getResultList();
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdHolderAccountPk)){
			sbQuery = new StringBuilder();
			sbQuery.append("	SELECT distinct ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder");
			sbQuery.append("	INNER JOIN FETCH ha.participant");
			sbQuery.append("	WHERE ha.idHolderAccountPk in (:lstIdHolderAccountPk)");
			query = em.createQuery(sbQuery.toString());
			query.setParameter("lstIdHolderAccountPk", lstIdHolderAccountPk);
			return (List<HolderAccount>)query.getResultList();
		}else
			return null;
	}

	/**
	 * Find market fact.
	 *
	 * @param idAnnotationOperationPk the id annotation operation pk
	 * @return the account annotation market fact
	 */
	public AccountAnnotationMarketFact findMarketFact(Long idAnnotationOperationPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aamf");
			sbQuery.append("	FROM AccountAnnotationMarketFact aamf");
			sbQuery.append("	WHERE aamf.accountAnnotationOperation.idAnnotationOperationPk = :idAnnotationOperationPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
			return (AccountAnnotationMarketFact)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Search account annotation operation by revert.
	 *
	 * @param searchAccountAnnotationTO the search account annotation to
	 * @param lstStates the lst states
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> searchAccountAnnotationOperationByRevert(SearchAccountAnnotationTO searchAccountAnnotationTO , List<Integer> lstStates) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao	");
		sbQuery.append("	WHERE aao.state IN :annotationState AND indDematerialization = :indDematerialization ");
		//sbQuery.append("	trunc(aao.custodyOperation.registryDate) = :endDate	AND	");			
		Query query = em.createQuery(sbQuery.toString());				
		//query.setParameter("endDate", searchAccountAnnotationTO.getEndDate());
		query.setParameter("annotationState", lstStates);
		query.setParameter("indDematerialization", 1);
		return (List<AccountAnnotationOperation>) query.getResultList();
	}

	/**
	 * Find account annotation operation by security.
	 *
	 * @param idSecurityCode the id security code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> findAccountAnnotationOperationBySecurity(String idSecurityCode) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao");
		sbQuery.append("	WHERE aao.security.idSecurityCodePk = :idSecurity	");

		Query query = em.createQuery(sbQuery.toString());	
		
		query.setParameter("idSecurity", idSecurityCode);

		return (List<AccountAnnotationOperation>) query.getResultList();
	}
	
	/**
	 * Exist other request.
	 *
	 * @param security the security
	 * @param states the states
	 * @param certificateNumber the certificate number
	 * @return the integer
	 */
	public Integer existOtherRequest(Security security,List<Integer> states,String certificateNumber) {
		
		Integer result=null;
		
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aao.state");
			sbQuery.append("	FROM AccountAnnotationOperation aao");
			sbQuery.append("	WHERE aao.security.idSecurityCodePk = :idSecurityCodePk");
			sbQuery.append("	and   aao.custodyOperation.indRectification != :indicatorOne");
			if(states!=null && states.size()>0){
				sbQuery.append(" and aao.state in :listState");
			}
			if(Validations.validateIsNotNullAndNotEmpty(certificateNumber)){
				sbQuery.append("	AND aao.certificateNumber = :certificateNumber");
			}
			Query query = em.createQuery(sbQuery.toString());
			if(states!=null && states.size()>0){
				query.setParameter("listState",states);
			}
			if(Validations.validateIsNotNullAndNotEmpty(certificateNumber)){
				query.setParameter("certificateNumber",certificateNumber);
			}
			query.setParameter("idSecurityCodePk", security.getIdSecurityCodePk());
			query.setParameter("indicatorOne", ComponentConstant.ONE);
			
			if(query.getResultList().size()>0){
				result = (Integer)query.getResultList().get(0);	
			}
			
		} catch (NoResultException e) {
			result = null;
		}		return result;
	}
	
	/**
	 * Gets the market fact from security.
	 *
	 * @param idSecurity the id security
	 * @return the market fact from security
	 */
	@SuppressWarnings("unchecked")
	public List<HolderMarketFactBalance> getMarketFactFromSecurity(String idSecurity){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hmfb ");
		sbQuery.append("	FROM HolderMarketFactBalance hmfb ");
		sbQuery.append("	WHERE hmfb.security.idSecurityCodePk = :idSec ");
		sbQuery.append("	AND hmfb.indActive=:indActive ");
		sbQuery.append("	ORDER BY hmfb.marketDate DESC ");
		Query query = em.createQuery(sbQuery.toString());				
		query.setParameter("idSec", idSecurity);
		query.setParameter("indActive", GeneralConstants.ONE_VALUE_INTEGER);
		
		return (List<HolderMarketFactBalance>) query.getResultList();
	}
		
	/**
	 * Validate security certificate.
	 *
	 * @param idSecurityCode the id security code
	 * @param certificate the certificate
	 * @return the physical balance detail
	 */
	public PhysicalBalanceDetail validateSecurityCertificate(String idSecurityCode, String certificate){
		StringBuilder sbQuery = new StringBuilder();
		PhysicalBalanceDetail physicalBalanceDetail= null;
		try {
			sbQuery.append(" SELECT PBD FROM PhysicalBalanceDetail PBD ");
			sbQuery.append(" WHERE ");
			sbQuery.append(" PBD.physicalCertificate.securities.idSecurityCodePk = :idSecurityCode ");
			sbQuery.append(" and PBD.state = :depositedState ");
			if(Validations.validateIsNotNullAndNotEmpty(certificate) && Validations.validateIsNumeric(certificate)){
				sbQuery.append(" and PBD.physicalCertificate.certificateNumber = :certificate ");
			}			
			TypedQuery<PhysicalBalanceDetail> query = em.createQuery(sbQuery.toString(), PhysicalBalanceDetail.class);	
			if(Validations.validateIsNotNullAndNotEmpty(certificate) && Validations.validateIsNumeric(certificate)){
				query.setParameter("certificate", certificate);
			}			
			query.setParameter("idSecurityCode", idSecurityCode);
			query.setParameter("depositedState", StateType.DEPOSITED.getCode());
			physicalBalanceDetail= query.getSingleResult();
		} catch (NoResultException e){
			
		}
		
		return physicalBalanceDetail;
	}
	
	/**
	 * Get Participant by Issuer.
	 *
	 * @param strIssuer the str issuer
	 * @return Participant
	 */
	public Participant geParticipantByIssuer(String strIssuer){		
		Participant objParticipant = null;
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select par from Participant par ");
			sbQuery.append(" where par.issuer.idIssuerPk = :parIssuer ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIssuer", strIssuer);
			return (Participant) query.getSingleResult();
		} catch (NoResultException e){			
		}
		return objParticipant;
	}
	
	
	/**
	 * Gets the annotation operation type.
	 *
	 * @param motive the motive
	 * @return the annotation operation type
	 */
	private Long getAnnotationOperationType(Integer motive) {
		if(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode().equals(motive)){//INGRESO POR ANOTACION
			return ParameterOperationType.SECURITIES_DEMATERIALIZATION.getCode();
			
		}else if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(motive)){
			return ParameterOperationType.SECURITIES_DEMATERIALIZATION_GUARDA_MANAGED.getCode();
			
		}else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(motive)){
			return ParameterOperationType.SECURITIES_DEMATERIALIZATION_GUARDA_EXCLUSIVE.getCode();
			
		}
		
		
		else if(DematerializationCertificateMotiveType.SUSCRIPTION.getCode().equals(motive)){//SUSCRIPCION PREFERENTE
			return ParameterOperationType.SECURITIES_SUSCRIPTION.getCode();
			
		} else if(DematerializationCertificateMotiveType.DIRECT_PURCHASE_BCB.getCode().equals(motive)){//VENTA DIRECTA
			return ParameterOperationType.SECURITIES_DIRECT_PURCHASE_BCB.getCode();
			
		} else if(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode().equals(motive)){//TESORO DIRECTO TGN
			return ParameterOperationType.SECURITIES_DIRECT_PURCHASE_TGN.getCode();
			
		} else if(DematerializationCertificateMotiveType.AUCTION_PURCHASE.getCode().equals(motive)){//SUBASTA
			return ParameterOperationType.SECURITIES_AUCTION_PURCHASE.getCode();
			
		} else if(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode().equals(motive)){//MESA DE DINERO
			return ParameterOperationType.SECURITIES_MONEY_PURCHASE.getCode();
			
		} else if(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode().equals(motive)){//ADQUISICION DIRECTA
			return ParameterOperationType.SECURITIES_DIRECT_PURCHASE_PRIVATE.getCode();
			
		} else if(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode().equals(motive)){//COMPRA ACCIONES BANCARIAS
			return ParameterOperationType.SECURITIES_BANK_SHARES_PURCHASE.getCode();
			
		} else if(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(motive)){//DESMATERIALIZACION
			return ParameterOperationType.SECURITIES_DEMATERIALIZATION.getCode();
			
		} else if(DematerializationCertificateMotiveType.INCOME_PURCHASE_PRIVATE_STOCK.getCode().equals(motive)){//COMPRA ACCIONES PRIVADAS
			return ParameterOperationType.SECURITIES_DIRECT_PURCHASE_PRIVATE.getCode();
			
		} else if(DematerializationCertificateMotiveType.DONATION.getCode().equals(motive)){//DESMATERIALIZACION
			return ParameterOperationType.SECURITIES_DEMATERIALIZATION.getCode();
			
		} else if(DematerializationCertificateMotiveType.INCOME_PURCHASE_PRIMARY.getCode().equals(motive)){//COMPRA EN PRIMARIO
			return ParameterOperationType.SECURITIES_DEMATERIALIZATION.getCode();
			
		}
			
		return null;
	}
	
	/** metodo para obtener un valor desde el id ingresado como parametro
	 * 
	 * @param idSecurityCode
	 * @return */
	public Security getSecurityFromId(String idSecurityCode) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select s");
		sd.append(" from Security s");
		sd.append(" inner join fetch s.issuer i");
		sd.append(" where s.idSecurityCodePk = :idSecurityCode");
		try {
			List<Security> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idSecurityCode", idSecurityCode);
			lst = q.getResultList();
			if (!lst.isEmpty())
				return lst.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
	/** metodo para obtener un HolderAccount desde un particiapnte y cuentaTitular ingresados como parametros
	 * 
	 * @param idParticipant
	 * @param holderAccountNumber
	 * @return */
	public HolderAccount getHolderAccountFromParticipantAndHoldAccountNumber(Long idParticipant, Integer holderAccountNumber) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select ha");
		sd.append(" from HolderAccount ha");
		sd.append(" inner join fetch ha.participant p");
		sd.append(" where p.idParticipantPk = :idParticipant");
		sd.append(" and ha.accountNumber = :holderAccountNumber");
		try {
			List<HolderAccount> lst = new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idParticipant", idParticipant);
			q.setParameter("holderAccountNumber", holderAccountNumber);
			lst = q.getResultList();
			if (!lst.isEmpty())
				return lst.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}

	/** metodo para verificar si un Holder tiene relacion con el holderAccount ingresados como parametros
	 * 
	 * @param idHolderAccount
	 * @param idHolderPk
	 * @return true si tiene relacion , false si no tiene relacion */
	public boolean verifyRelationshipHolderAndHolderAccount(Long idHolderAccount, Long idHolderPk) {
		List<HolderAccountDetail> lst = new ArrayList<>();
		StringBuilder sd = new StringBuilder();
		sd.append(" select had");
		sd.append(" from HolderAccountDetail had");
		sd.append(" where had.holderAccount.idHolderAccountPk = :idHolderAccount ");
		sd.append(" and had.holder.idHolderPk = :idHolderPk ");
		try {
			Query q = em.createQuery(sd.toString());
			q.setParameter("idHolderAccount", idHolderAccount);
			q.setParameter("idHolderPk", idHolderPk);
			lst = q.getResultList();
			if (lst.isEmpty())
				return false;
			else
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/** metodo para registrar en la BBDD un AccAccountAnnotationFile
	 * 
	 * @param entity
	 * @return */
	public AccAnnotationOperationFile registryAccAnnotationFile(AccAnnotationOperationFile entity) {
		try {
			em.persist(entity);
			em.flush();
			return entity;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Register annotation operation.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @param loggerUser the logger user
	 * @return the account annotation operation
	 * @throws ServiceException the service exception
	 */
	
	public String securityIsinGenerate(Security security) throws ServiceException{
        return securitiesServiceBean.securityIsinGenerate(security);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public AccountAnnotationOperation registerAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {
		
		ErrorServiceType entitiesStates = validateEntities(accountAnnotationOperation);
		
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates)) {
			Object[] param= new Object[1];
			if (ErrorServiceType.ISSUER_BLOCK_PARAM.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getSecurity().getIssuer().getMnemonic();
			} else if (ErrorServiceType.SECURITY_CODE_BLOCK.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getSecurity().getIdSecurityCodePk();
			} else if (ErrorServiceType.HOLDER_ACCOUNT_NO_REGISTERED.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getHolderAccount().getAlternateCode();
			} else if (ErrorServiceType.PARTICIPANT_BLOCK_PARAM.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getHolderAccount().getParticipant().getMnemonic();
			}
			throw new ServiceException(entitiesStates, param);
		}
		
		if(BooleanType.YES.getCode().equals(accountAnnotationOperation.getIndSerializable()))
			accountAnnotationOperation.setExpeditionDate(accountAnnotationOperation.getSecurity().getIssuanceDate());
		accountAnnotationOperation.setIndPrimarySettlement(BooleanType.NO.getCode());
		
		List<AccountAnnotationCupon> lstAccountAnnotationCupon= accountAnnotationOperation.getAccountAnnotationCupons();
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
		custodyOperation.setState(accountAnnotationOperation.getState());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		if (DematerializationStateType.APPROVED.getCode().equals(accountAnnotationOperation.getState())) {
			custodyOperation.setApprovalDate(CommonsUtilities.currentDateTime());
			custodyOperation.setApprovalUser(loggerUser.getUserName());
		}
		custodyOperation.setOperationNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_DEMATERIALIZATION));
		
		//Long operationType = ( accountAnnotationOperation.getIndGuarda().equals(1) )? ParameterOperationType.SECURITIES_DEMATERIALIZATION_GUARDA.getCode() : getAnnotationOperationType(accountAnnotationOperation.getMotive());
		Long operationType = getAnnotationOperationType(accountAnnotationOperation.getMotive());
		
		custodyOperation.setOperationType(operationType);
		accountAnnotationOperation.setCustodyOperation(custodyOperation);
		custodyOperation.setAudit(loggerUser);
		accountAnnotationOperation.setAudit(loggerUser);
		
		Security tmpSecurity = null;
		String annotationIsinCode = "";
		/*try {
			tmpSecurity = accountAnnotationOperation.getSecurity();
			annotationIsinCode = securityIsinGenerate(tmpSecurity);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		if(BooleanType.NO.getCode().equals(accountAnnotationOperation.getIndSerializable())) {
			accountAnnotationOperation.setAccountAnnotationCupons(null);
			accountAnnotationOperation.setSecuritiesManager(null);
			accountAnnotationOperation.setSecurity(null);
			accountAnnotationOperation.setPlacementParticipant(null);
			accountAnnotationOperation.setIdAccAnnotationFileFk(null);
			accountAnnotationOperation.setPhysicalCertificate(null);
		}

		Integer lastCupon = 0;
		Date lastPaymentDateCupon = null;
		Date lasRegistryDateCupon = null;
		Date lastBeginingDateCupon = null;
		if(lstAccountAnnotationCupon!=null && lstAccountAnnotationCupon.size()>0) {
			for(AccountAnnotationCupon cupon: lstAccountAnnotationCupon){
				if(cupon.getCuponNumber() > lastCupon) {
					lastPaymentDateCupon = cupon.getPaymentDate();
					lasRegistryDateCupon = cupon.getRegistryDate();
					lastBeginingDateCupon = cupon.getBeginingDate();
				}
			}
			
			accountAnnotationOperation.setPaymentDate(lastPaymentDateCupon);
			accountAnnotationOperation.setRegistryDate(lasRegistryDateCupon);
			accountAnnotationOperation.setBeginingDate(lastBeginingDateCupon);
			
		}else {
			
			if(accountAnnotationOperation.getSecurityExpirationDate()!=null) {
				
				Date previusDate = CommonsUtilities.removeDaysHabilsToDate(accountAnnotationOperation.getSecurityExpirationDate(), 1);
				accountAnnotationOperation.setPaymentDate(accountAnnotationOperation.getSecurityExpirationDate());
				accountAnnotationOperation.setRegistryDate(previusDate);
				accountAnnotationOperation.setBeginingDate(previusDate);
				
			}
			
		}
		
		accountAnnotationOperation.setAnnotationIsinCode(annotationIsinCode);
		if(accountAnnotationOperation.getSerialNumber()!=null) {
			accountAnnotationOperation.setSerialNumber(accountAnnotationOperation.getSerialNumber().toUpperCase());
		}
		if(accountAnnotationOperation.getCertificateNumber()!=null) {
			accountAnnotationOperation.setCertificateNumber(accountAnnotationOperation.getCertificateNumber().toUpperCase());
		}
		create(accountAnnotationOperation,AccountAnnotationOperation.class);
		

		if(lstAccountAnnotationCupon!=null && lstAccountAnnotationCupon.size()>0) {
			for(AccountAnnotationCupon cupon: lstAccountAnnotationCupon){
				cupon.setAudit(loggerUser);
				cupon.setAccountAnnotationOperation(accountAnnotationOperation);
				cupon.setPhysicalCertificate(null);
				cupon.setIdAccAnnotationFileFk(null);
				create(cupon,AccountAnnotationCupon.class);
			}
		}
		
		/*for (AccountAnnotationMarketFact accountOperationMarketFact : accountAnnotationOperation.getAccountAnnotationMarketFact()) {
			accountOperationMarketFact.setAccountAnnotationOperation(accountAnnotationOperation);
			if(accountAnnotationOperation.getSecurity().getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
				if(accountOperationMarketFact.getMarketPrice()==null){
					accountOperationMarketFact.setMarketPrice(BigDecimal.ZERO);
				}
			}
			create(accountOperationMarketFact);
		}*/

		return accountAnnotationOperation;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public AccountAnnotationOperation registerAnnotationOperationOriginal(
			AccountAnnotationOperation accountAnnotationOperation,LoggerUser loggerUser) throws ServiceException {
		
		ErrorServiceType entitiesStates = validateEntities(accountAnnotationOperation);
		
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates)) {
			Object[] param= new Object[1];
			if (ErrorServiceType.ISSUER_BLOCK_PARAM.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getSecurity().getIssuer().getMnemonic();
			} else if (ErrorServiceType.SECURITY_CODE_BLOCK.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getSecurity().getIdSecurityCodePk();
			} else if (ErrorServiceType.HOLDER_ACCOUNT_NO_REGISTERED.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getHolderAccount().getAlternateCode();
			} else if (ErrorServiceType.PARTICIPANT_BLOCK_PARAM.equals(entitiesStates)) {
				param[0] = accountAnnotationOperation.getHolderAccount().getParticipant().getMnemonic();
			}
			throw new ServiceException(entitiesStates, param);
		}
		
		if(SecurityClassType.DPF.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) || 
				SecurityClassType.DPA.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass())){
			List<Integer> state = new ArrayList<Integer>();
			state.add(DematerializationStateType.REIGSTERED.getCode());
			state.add(DematerializationStateType.APPROVED.getCode());
			state.add(DematerializationStateType.CONFIRMED.getCode());			
			Integer requestState = existOtherRequest(accountAnnotationOperation.getSecurity(),state,null);
			if(Validations.validateIsNotNullAndNotEmpty(requestState) && 
					(DematerializationStateType.REIGSTERED.getCode().equals(requestState)
					||	DematerializationStateType.APPROVED.getCode().equals(requestState)
					||	DematerializationStateType.CONFIRMED.getCode().equals(requestState))){
				throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_DPF_EXIST);	
			}
		}
		
		accountAnnotationOperation.setExpeditionDate(accountAnnotationOperation.getSecurity().getIssuanceDate());
		accountAnnotationOperation.setIndPrimarySettlement(BooleanType.NO.getCode());
		accountAnnotationOperation.setAudit(loggerUser);
		
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
		custodyOperation.setState(accountAnnotationOperation.getState());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		if (DematerializationStateType.APPROVED.getCode().equals(accountAnnotationOperation.getState())) {
			custodyOperation.setApprovalDate(CommonsUtilities.currentDateTime());
			custodyOperation.setApprovalUser(loggerUser.getUserName());
		}
		custodyOperation.setOperationNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_DEMATERIALIZATION));
		// 30289L
		Long operationType =  ParameterOperationType.SECURITIES_DEMATERIALIZATION_ANNOTATION.getCode(); //getAnnotationOperationType(accountAnnotationOperation.getMotive());
		
		custodyOperation.setOperationType(operationType);
		accountAnnotationOperation.setCustodyOperation(custodyOperation);
		custodyOperation.setAudit(loggerUser);	
		create(accountAnnotationOperation);
		
		/*if(Validations.validateIsNotNull(accountAnnotationOperation.getAccountAnnotationMarketFact())) {
			for (AccountAnnotationMarketFact accountOperationMarketFact : accountAnnotationOperation.getAccountAnnotationMarketFact()) {
				accountOperationMarketFact.setAccountAnnotationOperation(accountAnnotationOperation);
				if(accountAnnotationOperation.getSecurity().getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
					// issue 1263: siempre q se trate de renta fija el precioHM siempre debe ser cero
					accountOperationMarketFact.setMarketPrice(BigDecimal.ZERO);
				}else if(accountAnnotationOperation.getSecurity().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					// issue 1263: siempre q se trate de renta variable la tasaHM siempre debe ser nulo
					accountOperationMarketFact.setMarketRate(null);
				}
				create(accountOperationMarketFact);
			}
		}*/
		
		return accountAnnotationOperation;
	}
	
	/**
	 * Validate and save account entry tx.
	 *
	 * @param accountEntryRegisterTO the account entry register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)	
	public RecordValidationType validateAndSaveAccountEntryTx(AccountEntryRegisterTO accountEntryRegisterTO , LoggerUser loggerUser) throws ServiceException {		
		Object objResult = validateAndSaveAccountEntry(accountEntryRegisterTO, loggerUser);		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * Validate and save account entry.
	 *
	 * @param accountEntryRegisterTO the account entry register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveAccountEntry(AccountEntryRegisterTO accountEntryRegisterTO,LoggerUser loggerUser) throws ServiceException {
		
		logger.info("ENTRANDO A VALIDAR ::::::::::::::::::::; ");
		
		//issue 1398: A peticion de usuario: solo cuando es un BTX el motivo de la desma debe ser si o si: [2163 - INGRESO POR TESORO DIRECTO TGN]
		// si viene con otro motivo, se infiere automaticamente
		SecurityObjectTO securityObjectTOAux = accountEntryRegisterTO.getSecurityObjectTO();
		if( Validations.validateIsNotNull(securityObjectTOAux) 
			&& Validations.validateIsNotNullAndPositive(securityObjectTOAux.getSecurityClass())
			&& securityObjectTOAux.getSecurityClass().equals(SecurityClassType.BTX.getCode()) ){
			accountEntryRegisterTO.setMotive(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode());
		}
				
		// check operation date is today
		Date operationDate = CommonsUtilities.truncateDateTime(accountEntryRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date)!=0){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE,null);
		}
		
		if(Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO().getIssuanceDate())){
			// issue 1177: la fecha de emision no debe ser mayor a la fecha de operacion
			Date issuanceDateDate = CommonsUtilities.truncateDateTime(accountEntryRegisterTO.getSecurityObjectTO().getIssuanceDate());
			if(issuanceDateDate.compareTo(operationDate)>0){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE_MUST_BE_LESS_OPERATION_DATE,null);
			}
		}
		
		// issue 892 aca aplicamos la validacion para la paertura de canal
		// issue 892 la diferencia entre FechaEmision y FechaOperacion no debe ser mayor a 4 dias
		// validandoque l fecha de emision no sea nulo
		if(Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO())
				&& Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO().getIssuanceDate())){
			Date issuanceDateDate = CommonsUtilities.truncateDateTime(accountEntryRegisterTO.getSecurityObjectTO().getIssuanceDate());
			int differenceDays=CommonsUtilities.getDaysBetween(issuanceDateDate,operationDate);
			if(differenceDays>4){
				
				// verificando si se trata de un fraccionamiento
				boolean isFraction=false;
				if ( Validations.validateIsNotNull(accountEntryRegisterTO)
						&& accountEntryRegisterTO.isDpfInterface()
						&& Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO())
						&& Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity())
						&& accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity().length() > 0) {

					// buscando serie a fraccionar
					SecurityTO securityTO = new SecurityTO();
					securityTO.setIdSecurityCodePk("DPF-" + accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity());
					Security objFractionableSecurity = securitiesQueryService.findSecuritiesByDpf(securityTO);

					// VALIDACIONES SOLO PARA VALORES QUE SON PARTE DE UN FRACCIONAMIENTO
					if (Validations.validateIsNotNull(objFractionableSecurity)) {
						// validando que el valor a fraccionar esta en estador redimido
						if (SecurityStateType.REDEEMED.getCode().equals(objFractionableSecurity.getStateSecurity())) {
							isFraction=true;
							logger.info("::: Se trata de una apertura de canal, por lo tanto se omite la validacion de apertura de canal");
						}
					}
				}
				
				if (!isFraction) {
					// si se encuentra una apertura de canal pues omitimos la validacion de los 4 dias
					ChannelOpeningModelTO filter = new ChannelOpeningModelTO();
					filter.setMnemonicPart(accountEntryRegisterTO.getEntityNemonic());
					filter.setDateChannelOpening(operationDate);
					filter.setStateChannelOpening(ChannelOpeningStateType.CONFIRMADO.getCode());
					logger.info("::: verificando si existe apertura de canal para el participante " + accountEntryRegisterTO.getIdParticipant());
					logger.info("::: se verificarA si la [fecha de opracion XML] sea igual a la [fecha de Apertura de Canal] ademas su estado de Apertura de canal tendrA que ser CONFIRMADO");
					List<ChannelOpeningModelTO> lstChannelOpeningModelTO = channelOpeningServiceBean.getLstChannelOpeningTO(filter);
					if (lstChannelOpeningModelTO.isEmpty() 
							|| lstChannelOpeningModelTO.size() == 0) {
						// LANZANDO MENSAJE DE VALIDACION
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_DAYS_EXCEEDED_AFTER_DATE_ISSUANCE,null);
					} else {
						// validando el que la hora de registro este dendtro del horario de la apertura de canal
						int size = lstChannelOpeningModelTO.size();
						ChannelOpeningModelTO channelOpeEvaluated = lstChannelOpeningModelTO.get(size - 1);
						Calendar currentHour = Calendar.getInstance();
						currentHour.set(1970, Calendar.JANUARY, 1);
						if (currentHour.getTime().compareTo(channelOpeEvaluated.getInitialHour()) < 0
								|| currentHour.getTime().compareTo(channelOpeEvaluated.getFinalHour()) > 0) {
							// LANZANDO MENSAJE DE VALIDACION
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_VALIDATION_EXCEEDED_HOURALLOWED,null);
						}
					}
				}
			}	
		}
		
		// validate if participant entity is allowed for ACV interface
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(accountEntryRegisterTO.getEntityNemonic());		
		objParticipant = participantServiceBean.getParticipantByPk(objParticipant);
		
		if(objParticipant == null  || (!accountEntryRegisterTO.isDpfInterface() && 
				!objParticipant.getIdParticipantPk().equals(idParticipantBC) && !objParticipant.getIdParticipantPk().equals(idParticipantVUN)) ){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		}
		
		if(!ParticipantStateType.REGISTERED.getCode().equals(objParticipant.getState())){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_STATUS_CODE, null);
		}
		
		// check if operation code is correct for ACV
		if((!accountEntryRegisterTO.isDpfInterface() && !ComponentConstant.INTERFACE_ACCOUNT_ENTRY.equals(accountEntryRegisterTO.getOperationCode())) ||
				(accountEntryRegisterTO.isDpfInterface() && !ComponentConstant.INTERFACE_ACCOUNT_ENTRY_DPF.equals(accountEntryRegisterTO.getOperationCode()))){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// check if operation was successfully registered previously
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
				objParticipant.getIdParticipantPk(), accountEntryRegisterTO.getOperationNumber(), 
				accountEntryRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// validate security class, only BBX and BTX is allowed when is not dpf
		SecurityObjectTO securityObjectTO = accountEntryRegisterTO.getSecurityObjectTO();
		
		if(!accountEntryRegisterTO.isDpfInterface()){
			Integer securityClass = securityObjectTO.getSecurityClass();
			
			ParameterTableTO filter = new ParameterTableTO();
			filter.setParameterTablePk(securityClass);
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
				ParameterTable securityClassParam = securityClasses.get(0);
				if(!ComponentConstant.ONE.equals(securityClassParam.getIndicator6())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
				}else{
					securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
					securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
				}
			}else{
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
			}
		}else{ //DPA issue 158
			if (securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				securityObjectTO.setSecurityClass(SecurityClassType.DPA.getCode());
				securityObjectTO.setSecurityClassDesc(SecurityClassType.DPA.getText1());
			}else{
				securityObjectTO.setSecurityClass(SecurityClassType.DPF.getCode());
				securityObjectTO.setSecurityClassDesc(SecurityClassType.DPF.getText1());
			}
		}
		
		//tasaIntPas en caso de ser DPA debe ser 0 //issue 158
		if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getInstrumentTypeMnemonic())){
			if(securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				if(!securityObjectTO.getPassiveInterestRate().equals(GeneralConstants.ZERO_VALUE_BIGDECIMAL)){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE, null);
				}
			}
		}
		
		//periodoPago en caso de ser DPA debe null //issue 158
		if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getInstrumentTypeMnemonic())){
			if(securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				if(!(securityObjectTO.getAmortizationPeriodicity() == null)){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PERIODICITY, null);
				}
			}
		}
		
		logger.debug("DPF(RA)(633):::::::::::::::::: " + accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity());
		// Issue 633
		// Validar que cuando se trate de un fraccionamiento solo para DPF
		if(accountEntryRegisterTO.isDpfInterface()
				&& Validations.validateIsNotNull(accountEntryRegisterTO) 
				&& Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO()) 
				&& Validations.validateIsNotNull(accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity())
				&& accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity().length() > 0){
			
			// buscando serie a fraccionar
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk("DPF-" + accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity());
			Security objFractionableSecurity = securitiesQueryService.findSecuritiesByDpf(securityTO);
			
			// VALIDACIONES SOLO PARA VALORES QUE SON PARTE DE UN FRACCIONAMIENTO
			if(Validations.validateIsNotNull(objFractionableSecurity)){
				// validando que el valor a fraccionar esta en estador redimido
				if(SecurityStateType.REDEEMED.getCode().equals(objFractionableSecurity.getStateSecurity())){
					
					//ISSUE 1123
					//Validando que solo el partipante que lo redimio lo pueda fraccionar
					if(retirementOperationServiceBean.searchHolderAccountBalanceExpByParticipantAndSecurity(objParticipant.getIdParticipantPk(), securityTO.getIdSecurityCodePk()).isEmpty()){
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
					}
					
					RetirementOperationTO retirementOperationTO = new RetirementOperationTO();
					
					retirementOperationTO.setInitDate(accountEntryRegisterTO.getOperationDate());
					retirementOperationTO.setEndDate(accountEntryRegisterTO.getOperationDate());			
					retirementOperationTO.setState(RetirementOperationStateType.CONFIRMED.getCode());
					retirementOperationTO.setMotive(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode());
					
					retirementOperationTO.setSecurity(new Security("DPF-" + accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity()));
					// buscando operacion de Redencion anticipada
					List<RetirementOperationTO> listaOperaciones = retirementOperationServiceBean.searchRetirementOperationsAccount(retirementOperationTO);
					
					//VALIDANDO LA CUENTA TITULAR issue 1123
					//
					if(accountEntryRegisterTO.getSecurityObjectTO().getCuiNumber() != null){
						//En caso de que el tag CUI no este vacio: Validando que la cuenta titular sea la misma en la que se redimio
						if(retirementOperationServiceBean.searchHolderAccountBalanceExpByParticipantAndSecurityAndHolderAccount(objParticipant.getIdParticipantPk(), securityTO.getIdSecurityCodePk(),accountEntryRegisterTO.getSecurityObjectTO().getCuiNumber()).isEmpty()){
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_NOTEXITS_ACCOUNT_NUMBER, null);
						}
					}else{
						//En caso de que el tag CUI este vacio: Seteando la cuenta titular donde se redimio el valor
						HolderAccount holderAccount = findHolderAccount(listaOperaciones.get(0).getIdHolderAccountPk().get(0));
						accountEntryRegisterTO.getSecurityObjectTO().setCuiNumber(Long.valueOf(holderAccount.getAccountNumber()));
					}
					
					// validando que la fecha de operacion del Nuevo DPF fraccionable, sea la misma que la 
					// del valor que fue Redimido Anticipadamente y el que modifico ese usuario modificado fue WEBSERVICES
					if(Validations.validateIsNotNull(listaOperaciones) && listaOperaciones.size() > 0
							 && Validations.validateIsNotNull(listaOperaciones.get(0).getInitDate())
							 && CommonsUtilities.truncateDateTime(listaOperaciones.get(0).getInitDate())
							 		.equals(CommonsUtilities.truncateDateTime(accountEntryRegisterTO.getOperationDate()))){
						// validando que el nuevo valor a registrar
						// tenga la misma fecha de emision
						if(!CommonsUtilities.isEqualDate(objFractionableSecurity.getIssuanceDate(), securityObjectTO.getIssuanceDate())){

							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_ISSUANCE_DATE_NOT_EQUALS,null);
						}
						// tenga la misma ffecha de vencimiento
						if(!CommonsUtilities.isEqualDate(objFractionableSecurity.getExpirationDate(), securityObjectTO.getExpirationDate())){

							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_EXPIRATION_DATE_NOT_EQUALS,null);
						}
						// tenga la misma tasa
						if(objFractionableSecurity.getInterestRate().compareTo(securityObjectTO.getInterestRate()) != 0){
							
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_INTERES_RATE_NOT_EQUALS,null);
						}
						// buscando PK de moneda a partir de Nemonico
						ParameterTableTO filterCurrency = new ParameterTableTO();
						filterCurrency.setMasterTableFk(MasterTableType.CURRENCY.getCode());
						filterCurrency.setText1(securityObjectTO.getCurrencyMnemonic() == null ? "" : securityObjectTO.getCurrencyMnemonic());
						List<ParameterTable> currencyDpf = parameterServiceBean.getListParameterTableServiceBean(filterCurrency);
						
						// verificando que la lista devuelva exactamente un solo registro
						ParameterTable currency = currencyDpf == null ? null : currencyDpf.size() == 1 ? currencyDpf.get(0) : null; 
						
						// validando la existencia del Nemonico de la moneda que se envio en el DPF
						if(currency == null){
							
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_CURRENCY_NOT_EQUALS,null);
						}
						
						// validando que tenga la misma moneda la fraccion y el DPF fraccionado
						if(currency != null && !objFractionableSecurity.getCurrency().equals(currency.getParameterTablePk())){
							
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_CURRENCY_NOT_EQUALS,null);
						}
						
						// buscando series que fueron fraccionados a partir de un determinado DPF
						securityTO = new SecurityTO();
						securityTO.setIdFractionSecurityCodePk(securityObjectTO.getSecurityClassDesc() + "-" + securityObjectTO.getFractionableSecurity());
						List<Security> objFractionSecurityList = securitiesQueryService.findSecuritiesDpfForFatherFraction(securityTO);
						
						// capturando el monto del nuevo DPF para que se valide el monto
						BigDecimal totalNominalValue = accountEntryRegisterTO.getSecurityObjectTO().getNominalValue();
						logger.info("Monto DPF(" + accountEntryRegisterTO.getSecurityObjectTO().getSecuritySerial() + "): " + totalNominalValue);
						
						if(Validations.validateIsNotNull(objFractionSecurityList) && objFractionSecurityList.size() > 0 ){
														
							// sumando el valor nominal de todos los DPF's que fueron parte del fraccionamiento 
							for(Security security : objFractionSecurityList){
								
								if(Validations.validateIsNotNull(security.getCurrentNominalValue())){
									totalNominalValue = security.getCurrentNominalValue().add(totalNominalValue);
									logger.info(security.getIdSecurityCodePk() + " VN: " + security.getCurrentNominalValue());
								}
							}
							
						}
						
						// validando que la suma de los valores nominales de las fracciones no supere a
						if(objFractionableSecurity != null){
							
							logger.info("Suma de los DPF Fraccionados: " + totalNominalValue + " == " + objFractionableSecurity.getCurrentNominalValue());
							// validando que el monto de todas las fracciones no sea mayor al DPF Redimido
							if(objFractionableSecurity.getCurrentNominalValue().compareTo(totalNominalValue) < 0){
								
								return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_TOTAL_NOMINAL_VALUE,null);
							}
						} 
						
						// validando que los nuevos DPF que son parte de una fracccionamiento tengan el o los mismos titulares, numero de Cedula de Identidad
						List<Long> listIdHolderAccountPk = listaOperaciones.get(0).getIdHolderAccountPk();
						if(listIdHolderAccountPk != null && listIdHolderAccountPk.size() > 0){
							
							HolderAccountTO holderAccountTO = new HolderAccountTO();
							holderAccountTO.setIdHolderAccountPk(listIdHolderAccountPk.get(0));							
							
							// buscando lista de numeros de documentos 
							List<String[]> listDocumentNumber = getListDocumentHolderAccount(holderAccountTO);
							
							// verificando si se trata de una cuenta Natural o Juridica:   
							// 1: Natural
							// 2: Juridica
							if(Integer.valueOf("1").equals(accountEntryRegisterTO.getHolderAccountObjectTO().getHolderTypeAlt())){
								// cuenta natural
								if(accountEntryRegisterTO.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs() != null
										&& accountEntryRegisterTO.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs().size() > 0
										&& listDocumentNumber.size() > 0){
									
									int nroTitularesEncontrados = 0;
									// verificando que sean los mismos numeros de documentos del valor redimido 
									// y el nuevo valor a crear
									String documentNumberInmutable="";
									for(String[] documentNumber : listDocumentNumber){
										// verificando que el numero de documento se encuentre en la lista de titularess enviados
										for(NaturalHolderObjectTO naturalHolderObjectTO : accountEntryRegisterTO.getHolderAccountObjectTO().getLstNaturalHolderObjectTOs()){
											
											naturalHolderObjectTO.setDuplicateCod(naturalHolderObjectTO.getDuplicateCod() != null ? naturalHolderObjectTO.getDuplicateCod().trim() : null);
											documentNumberInmutable = naturalHolderObjectTO.getDocumentNumber();
											if(naturalHolderObjectTO.getDuplicateCod() != null 
													&& !naturalHolderObjectTO.getDuplicateCod().equalsIgnoreCase("-")
													&& !naturalHolderObjectTO.getDuplicateCod().equalsIgnoreCase("")){
												documentNumberInmutable=naturalHolderObjectTO.getDocumentNumber()+naturalHolderObjectTO.getDuplicateCod();
											}
											//VERIFICANDO SI ES DE EXTRANJERO
											if(naturalHolderObjectTO.getDocumentType() == 3 && !naturalHolderObjectTO.getDocumentNumber().contains("E-")){
												documentNumberInmutable = "E-" + documentNumberInmutable;
											}
											//TOMANDO EN CUENTA LOS TIPOS 12,16 Y 17
											if(naturalHolderObjectTO.getDocumentType()==12) documentNumberInmutable="DCD"+documentNumberInmutable+naturalHolderObjectTO.getDocumentSource();
											if(naturalHolderObjectTO.getDocumentType()==16) documentNumberInmutable="DCC"+documentNumberInmutable+naturalHolderObjectTO.getDocumentSource();
											if(naturalHolderObjectTO.getDocumentType()==17) documentNumberInmutable="DCR"+documentNumberInmutable+naturalHolderObjectTO.getDocumentSource();
											
											
											if(documentNumberInmutable.equals(documentNumber[0])){
												nroTitularesEncontrados++;
											}
										}
									}
									// verificando que todos los numeros de documentos se encuentren en el nuevo DPF
									if(!Integer.valueOf(nroTitularesEncontrados).equals(listDocumentNumber.size())){
										return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_FRACTIONS,null);
									}									
									
								} else {
									return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
								}
								// verificando si se trata de una cuenta Natural o Juridica:   
								// 1: Natural
								// 2: Juridica
							} else if(Integer.valueOf("2").equals(accountEntryRegisterTO.getHolderAccountObjectTO().getHolderTypeAlt())){
								// cuenta Juridica
								if(accountEntryRegisterTO.getHolderAccountObjectTO().getJuridicHolderObjectTO() != null
										&& accountEntryRegisterTO.getHolderAccountObjectTO().getJuridicHolderObjectTO().getDocumentNumber() != null
										&& listDocumentNumber.size() > 0){
									
									int nroTitularesEncontrados = 0;
									// verificando que sean los mismos numeros de documentos del valor redimido 
									// y el nuevo valor a crear
									for(String[] documentNumber : listDocumentNumber){
										// verificando que el numero de documento se encuentre en la lista de titularess enviados
										if(accountEntryRegisterTO.getHolderAccountObjectTO().getJuridicHolderObjectTO().getDocumentNumber().equals(documentNumber[0])){
											nroTitularesEncontrados++;
										}										
									}
									// verificando que todos los numeros de documentos se encuentren en el nuevo DPF
									if(Integer.valueOf("0").equals(nroTitularesEncontrados)){
										// TODO: Lanzar el error  msg.interface.invalid.holder.fraction
										return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_FRACTIONS,null);
									}	
									
								} else {
									return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularJuridico,null);
								}								
							}
							
						} else {
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_seccTitularNatural,null);
						}
						
					} else {
						//
						// ENviar mensaje de Error El valor que se esta fraccionando no fue Redimido por Anticipado en el mismo dia
						// que la creacion del o los nuevos valores
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_INVALID_DATE_RA,null);					
					}
					
				} else {
					// ENviar mensaje de Error de que el valro no se encuentra en estado redimido
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_STATE_NOT_EQUALS,null);					
				}
			} else {
				// ENviar mensaje de Error de que el valro no se encuentra registrado

				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_FRACTIONABLE_NOT_EXIST,null);
			}
			
		}	
		
		if(!accountEntryRegisterTO.isDpfInterface()){
			// check if geographic location combination exists in bd
			boolean exists = geographicDptProviMunicipality(
					accountEntryRegisterTO.getDepartament(), accountEntryRegisterTO.getProvince(), accountEntryRegisterTO.getMunicipality());
			if(!exists){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_CITY,null);
			}
		}
		
		Security objSecurity = null;
		if(securityObjectTO.getSecurityClassDesc()!=null && securityObjectTO.getSecuritySerial()!=null){
			String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
			///objSecurity = find(Security.class, idSecurityCode);
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk(idSecurityCode);
			objSecurity = securitiesQueryService.findSecuritiesByDpf(securityTO);
			
			if(objSecurity == null){
				if(accountEntryRegisterTO.isDpfInterface()){
					securityObjectTO.setDpfInterface(accountEntryRegisterTO.isDpfInterface());	
					securityObjectTO.setRestrictions(accountEntryRegisterTO.getRestrictions());
					Object objResult = securitiesRemoteService.get().createSecuritiesRemoteService(loggerUser, securityObjectTO);
					
					if(objResult instanceof RecordValidationType){
						RecordValidationType validation= (RecordValidationType) objResult;
						validation.setOperationRef(accountEntryRegisterTO);
						return validation;
					} else if(objResult instanceof SecurityObjectTO){
						securityObjectTO = (SecurityObjectTO) objResult;
						//objSecurity = find(Security.class, securityObjectTO.getIdSecurityCode());
						SecurityTO objSecurityTO = new SecurityTO();
						objSecurityTO.setIdSecurityCodePk(securityObjectTO.getIdSecurityCode());
						objSecurity = securitiesQueryService.findSecuritiesByDpf(objSecurityTO);
					}
				} else {
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL,null);
				}
				
			}else{
				if(accountEntryRegisterTO.isDpfInterface()){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_OPERATION_DUPLICATE_TAG_SERIAL,null);	
				}
			}
		}
		
		// validate allowed motives for ACV
		if(!accountEntryRegisterTO.isDpfInterface()){
			
			if(objSecurity == null){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL,null);
			}
			
			boolean isMotiveOk = false;
			List<ParameterTable> allowedMotives = getAllowedDematerializationMotives(objSecurity);
			
			if(Validations.validateListIsNotNullAndNotEmpty(allowedMotives)){
				for(ParameterTable paramMotive : allowedMotives){
					if(accountEntryRegisterTO.getMotive().equals(paramMotive.getParameterTablePk()) && ComponentConstant.ONE.equals(paramMotive.getIndicator6())){
						isMotiveOk = true;
						break;
					}
				}
			}
			
			if(!isMotiveOk){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, null, PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_MOTIVE_FOR_SECURITY);
			}
			
		}else{
			accountEntryRegisterTO.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
			accountEntryRegisterTO.setStockQuantity(BigDecimal.ONE);
			accountEntryRegisterTO.setPrice(objSecurity.getCurrentNominalValue());
		}
     	
     	if(SecuritySourceType.FOREIGN.getCode().equals(objSecurity.getSecuritySource())){
     		return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, null, PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_FOREIGN);
		}		
     	
		HolderAccountObjectTO holderAccountObjectTO = accountEntryRegisterTO.getHolderAccountObjectTO();
		holderAccountObjectTO.setIdParticipant(objParticipant.getIdParticipantPk());
		holderAccountObjectTO.setCreateObject(true);
		holderAccountObjectTO.setDpfInterface(accountEntryRegisterTO.isDpfInterface());
		
		//	validate holder account and CUI holder     			  		
		if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getCuiNumber())){    			
			holderAccountObjectTO.setAccountNumber(securityObjectTO.getCuiNumber().intValue());
			holderAccountObjectTO.setValidateAccount(BooleanType.YES.getBooleanValue());    					
		} else {			
			holderAccountObjectTO.setValidateAccount(BooleanType.NO.getBooleanValue());
		}				
		
		logger.info(":::: ENVIA DATOS DE LA CUENTA ::::");
		logger.info("Participante: " + holderAccountObjectTO.getIdParticipant());
		logger.info("Participante dpf: " + holderAccountObjectTO.getAccountTypeCode());
		logger.info("Participante interface: " + holderAccountObjectTO.getAccountType());
		
		//transfomando el encoding a utf-8 issue 978 (se corrige solo a nivel pojo para no afectar los metodos core de commons)
		String textRead;
		for(NaturalHolderObjectTO naturalHolder:holderAccountObjectTO.getLstNaturalHolderObjectTOs()){
			try{
				
				textRead=naturalHolder.getFirstLastName();
				naturalHolder.setFirstLastName(new String(textRead.getBytes(),StandardCharsets.UTF_8));	
				
				textRead=naturalHolder.getSecondLastName();
				naturalHolder.setSecondLastName(new String(textRead.getBytes(),StandardCharsets.UTF_8));
				
				textRead=naturalHolder.getName();
				naturalHolder.setName(new String(textRead.getBytes(),StandardCharsets.UTF_8));
				
				textRead=naturalHolder.getLegalAddress();
				naturalHolder.setLegalAddress(new String(textRead.getBytes(),StandardCharsets.UTF_8));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		Object objResponse = accountsRemoteService.get().createHolderRemoteService(loggerUser, holderAccountObjectTO);
			
		if(objResponse instanceof RecordValidationType){
			RecordValidationType validation = (RecordValidationType) objResponse;
			validation.setOperationRef(accountEntryRegisterTO);			
			return validation;
		} else if (objResponse instanceof HolderAccountObjectTO){
			holderAccountObjectTO = (HolderAccountObjectTO) objResponse;
		}
		
		try {			
			HolderAccount objHolderAccount = null;			
			if(holderAccountObjectTO.getIdHolderAccount() != null){
												
				//objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());	
				HolderAccountTO objHolderAccountTO = new HolderAccountTO();
				objHolderAccountTO.setIdHolderAccountPk(holderAccountObjectTO.getIdHolderAccount());
				objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountByDpf(objHolderAccountTO);				
				
				accountEntryRegisterTO.getHolderAccountObjectTO().setAccountNumber(objHolderAccount.getAccountNumber());
				logger.info(":::: CUENTA OBTENIDA PARA ENVIAR AL ACV (CHEK-ERROR) ::::");
				logger.info("Valor: " + objSecurity.getIdSecurityCodePk());
				logger.info("Id Cuenta: " + objHolderAccount.getIdHolderAccountPk());
				logger.info("Numero de Cuenta: " + objHolderAccount.getAccountNumber());
				logger.info("Participante: " + objHolderAccount.getParticipant().getIdParticipantPk());
				
				if(!objParticipant.getIdParticipantPk().equals(objHolderAccount.getParticipant().getIdParticipantPk())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PARTICIPANT_BALANCE_INSERT, null);
				}
			}						
			
			if(objHolderAccount == null){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT, null);
			}
			
			AccountAnnotationOperation accountAnnotationOperation = new AccountAnnotationOperation();
			accountAnnotationOperation.setHolder(objHolderAccount.getRepresentativeHolder());
			accountAnnotationOperation.setHolderAccount(objHolderAccount);
			accountAnnotationOperation.setSecurity(objSecurity);
			accountAnnotationOperation.setMotive(accountEntryRegisterTO.getMotive());
			accountAnnotationOperation.setTotalBalance(accountEntryRegisterTO.getStockQuantity());
			accountAnnotationOperation.setExpeditionDate(accountAnnotationOperation.getSecurity().getIssuanceDate());
			accountAnnotationOperation.setSerialNumber(objSecurity.getSecuritySerial());
			accountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
			accountAnnotationOperation.setIndPrimarySettlement(ComponentConstant.ZERO);
			accountAnnotationOperation.setIndLien(ComponentConstant.ZERO);
			accountAnnotationOperation.setDepartment(accountEntryRegisterTO.getDepartament());
			accountAnnotationOperation.setProvince(accountEntryRegisterTO.getProvince());
			accountAnnotationOperation.setObligationNumber(accountEntryRegisterTO.getObligationNumber());
			accountAnnotationOperation.setAlternativeCode(objSecurity.getAlternativeCode());
			accountAnnotationOperation.setPlacementParticipant(objParticipant);
			
			if (accountEntryRegisterTO.getMunicipality()!=null){
				accountAnnotationOperation.setMunicipality(accountEntryRegisterTO.getMunicipality());
			}
							
			AccountAnnotationMarketFact accountAnnotationMarketFact = new AccountAnnotationMarketFact();
			accountAnnotationMarketFact.setAccountAnnotationOperation(accountAnnotationOperation);
			accountAnnotationMarketFact.setMarketPrice(accountEntryRegisterTO.getPrice());
			accountAnnotationMarketFact.setMarketRate(accountAnnotationOperation.getSecurity().getInterestRate());
			
			//issue 787
			if(objSecurity.getSecurityClass().equals(SecurityClassType.DPA.getCode())
					|| objSecurity.getSecurityClass().equals(SecurityClassType.DPF.getCode())){
				if(!CommonsUtilities.truncateDateTime(accountAnnotationOperation.getSecurity().getIssuanceDate()).equals(CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))){
					accountAnnotationMarketFact.setMarketDate(accountAnnotationOperation.getSecurity().getIssuanceDate());
				}else{
					accountAnnotationMarketFact.setMarketDate(CommonsUtilities.currentDate());
				}
			}else{
				accountAnnotationMarketFact.setMarketDate(CommonsUtilities.currentDate());
			}
			
			accountAnnotationMarketFact.setOperationQuantity(accountEntryRegisterTO.getStockQuantity());
			accountAnnotationOperation.setAccountAnnotationMarketFact(new ArrayList<AccountAnnotationMarketFact>());
			accountAnnotationOperation.getAccountAnnotationMarketFact().add(accountAnnotationMarketFact);

			//registerAnnotationOperation(accountAnnotationOperation, loggerUser);//ingreso provicional
			registerAnnotationOperationOriginal(accountAnnotationOperation, loggerUser);//desmaterializacion normal
			
			confirmAnnotationOperation(accountAnnotationOperation, loggerUser, true);
			
			accountEntryRegisterTO.setIdCustodyOperation(accountAnnotationOperation.getIdAnnotationOperationPk());
			accountEntryRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			accountEntryRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
			
		} catch (Exception e) {
			throw e;
		} 		
		
		return accountEntryRegisterTO;
	}
	
	/**
	 * Validate and save account entry new tx.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param accountEntryRegisterTO the account entry register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)	
	public RecordValidationType validateAndSaveAccountEntryNewTx(AccountEntryRegisterTO accountEntryRegisterTO , LoggerUser loggerUser) throws ServiceException {
		logger.info(":::::::::: validateAndSaveAccountEntryNewTx ::::::::::; ");
		Object objResult = validateAndSaveAccountEntryNew(accountEntryRegisterTO, loggerUser);		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * Validate and save account entry new structure.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param accountEntryRegisterTO the account entry register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveAccountEntryNew(AccountEntryRegisterTO accountEntryRegisterTO,LoggerUser loggerUser) throws ServiceException {
		
		logger.info("ENTRANDO A VALIDAR ::::::::::::::::::::; ");
		logger.info("::: validateAndSaveAccountEntryNew ::::; ");
		
		//issue 1398: A peticion de usuario: solo cuando es un BTX el motivo de la desma debe ser si o si: [2163 - INGRESO POR TESORO DIRECTO TGN]
		// si viene con otro motivo, se infiere automaticamente
		SecurityObjectTO securityObjectTOAux = accountEntryRegisterTO.getSecurityObjectTO();
		if( Validations.validateIsNotNull(securityObjectTOAux) 
			&& Validations.validateIsNotNullAndPositive(securityObjectTOAux.getSecurityClass())
			&& securityObjectTOAux.getSecurityClass().equals(SecurityClassType.BTX.getCode()) ){
			accountEntryRegisterTO.setMotive(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode());
		}
				
		// check operation date is today
		Date operationDate = CommonsUtilities.truncateDateTime(accountEntryRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date)!=0){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE,null);
		}
		
		// validate if participant entity is allowed for ACV interface
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(accountEntryRegisterTO.getEntityNemonic());		
		objParticipant = participantServiceBean.getParticipantByPk(objParticipant);
		
		if(objParticipant == null  || (!accountEntryRegisterTO.isDpfInterface() && 
				!objParticipant.getIdParticipantPk().equals(idParticipantBC) && !objParticipant.getIdParticipantPk().equals(idParticipantVUN)) ){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		}
		
		if(!ParticipantStateType.REGISTERED.getCode().equals(objParticipant.getState())){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_STATUS_CODE, null);
		}
		
		// check if operation code is correct for ACV
		if((!accountEntryRegisterTO.isDpfInterface() && !ComponentConstant.INTERFACE_ACCOUNT_ENTRY.equals(accountEntryRegisterTO.getOperationCode())) ||
				(accountEntryRegisterTO.isDpfInterface() && !ComponentConstant.INTERFACE_ACCOUNT_ENTRY_DPF.equals(accountEntryRegisterTO.getOperationCode()))){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// check if operation was successfully registered previously
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
				objParticipant.getIdParticipantPk(), accountEntryRegisterTO.getOperationNumber(), 
				accountEntryRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		//Verificando que el numero de obligacion no se haya procesado
		List<InterfaceTransaction> lstInterfaceTransactionBefore = holderAccountServiceBean.getListInterfaceTransactionByObligationNumber(
				objParticipant.getIdParticipantPk(), accountEntryRegisterTO.getObligationNumber(), 
				accountEntryRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionBefore)){
			//Verificando si se realizo la reversion 
			List<InterfaceTransaction> lstInterfaceTransactionRevert = holderAccountServiceBean.getListInterfaceTransactionByObligationNumber(
					objParticipant.getIdParticipantPk(), accountEntryRegisterTO.getObligationNumber(), 
					ComponentConstant.INTERFACE_EARLY_PAYMENT_OF_PLACEMENT);
			if(Validations.validateListIsNullOrEmpty(lstInterfaceTransactionRevert)){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OBLIGATION_NUMBER, null);
			}else{
				if(lstInterfaceTransactionBefore.size()!=lstInterfaceTransactionRevert.size()){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OBLIGATION_NUMBER, null);
				}
			}
		}
		
		// validate security class, only BBX and BTX is allowed when is not dpf
		SecurityObjectTO securityObjectTO = accountEntryRegisterTO.getSecurityObjectTO();
		
		if(!accountEntryRegisterTO.isDpfInterface()){
			Integer securityClass = securityObjectTO.getSecurityClass();
			
			ParameterTableTO filter = new ParameterTableTO();
			filter.setParameterTablePk(securityClass);
			filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
			if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
				ParameterTable securityClassParam = securityClasses.get(0);
				if(!ComponentConstant.ONE.equals(securityClassParam.getIndicator6())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
				}else{
					securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
					securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
				}
			}else{
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
			}
		}else{ //DPA issue 158
			if (securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				securityObjectTO.setSecurityClass(SecurityClassType.DPA.getCode());
				securityObjectTO.setSecurityClassDesc(SecurityClassType.DPA.getText1());
			}else{
				securityObjectTO.setSecurityClass(SecurityClassType.DPF.getCode());
				securityObjectTO.setSecurityClassDesc(SecurityClassType.DPF.getText1());
			}
		}
		
		//tasaIntPas en caso de ser DPA debe ser 0 //issue 158
		if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getInstrumentTypeMnemonic())){
			if(securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				if(!securityObjectTO.getPassiveInterestRate().equals(GeneralConstants.ZERO_VALUE_BIGDECIMAL)){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE, null);
				}
			}
		}
		
		//periodoPago en caso de ser DPA debe null //issue 158
		if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getInstrumentTypeMnemonic())){
			if(securityObjectTO.getInstrumentTypeMnemonic().equals(SecurityClassType.DPA.getText1())){
				if(!(securityObjectTO.getAmortizationPeriodicity() == null)){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_AMORTIZATION_PERIODICITY, null);
				}
			}
		}
			
		logger.debug("DPF(RA)(633):::::::::::::::::: " + accountEntryRegisterTO.getSecurityObjectTO().getFractionableSecurity());
			
		if(!accountEntryRegisterTO.isDpfInterface()){			
			// check if geographic location combination exists in bd
			boolean exists = geographicDptProviMunicipalityNew(
					accountEntryRegisterTO.getDepartament(), accountEntryRegisterTO.getProvince(), accountEntryRegisterTO.getMunicipality());
			if(!exists){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_RESIDENCE_CITY,null);
			}
		}
		
		Security objSecurity = null;
		if(securityObjectTO.getSecurityClassDesc()!=null && securityObjectTO.getSecuritySerial()!=null){
			String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
			///objSecurity = find(Security.class, idSecurityCode);
			SecurityTO securityTO = new SecurityTO();
			securityTO.setIdSecurityCodePk(idSecurityCode);
			objSecurity = securitiesQueryService.findSecuritiesByDpf(securityTO);
			
			if(objSecurity == null){
				if(accountEntryRegisterTO.isDpfInterface()){
					securityObjectTO.setDpfInterface(accountEntryRegisterTO.isDpfInterface());	
					securityObjectTO.setRestrictions(accountEntryRegisterTO.getRestrictions());
					Object objResult = securitiesRemoteService.get().createSecuritiesRemoteService(loggerUser, securityObjectTO);
					
					if(objResult instanceof RecordValidationType){
						RecordValidationType validation= (RecordValidationType) objResult;
						validation.setOperationRef(accountEntryRegisterTO);
						return validation;
					} else if(objResult instanceof SecurityObjectTO){
						securityObjectTO = (SecurityObjectTO) objResult;
						//objSecurity = find(Security.class, securityObjectTO.getIdSecurityCode());
						SecurityTO objSecurityTO = new SecurityTO();
						objSecurityTO.setIdSecurityCodePk(securityObjectTO.getIdSecurityCode());
						objSecurity = securitiesQueryService.findSecuritiesByDpf(objSecurityTO);
					}
				} else {
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL,null);
				}
				
			}else{
				if(accountEntryRegisterTO.isDpfInterface()){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_OPERATION_DUPLICATE_TAG_SERIAL,null);	
				}
			}
		}
		
		// validate allowed motives for ACV		
		if(!accountEntryRegisterTO.isDpfInterface()){
			
			if(objSecurity == null){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL,null);
			}
			//Validando que la entidad pertenesca a la clase de valor solo para BCB
			if(accountEntryRegisterTO.getEntityNemonic().equals("BCB")){
				if(!objSecurity.getSecurityClass().equals(SecurityClassType.BBX.getCode())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE_PLACEMENT,null);
				}
			}
			//Validando que la entidad pertenesca a la clase de valor solo para VUN
			if(accountEntryRegisterTO.getEntityNemonic().equals("VUN")){
				if(!objSecurity.getSecurityClass().equals(SecurityClassType.BTX.getCode())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE_PLACEMENT,null);	
				}
			}
			
			//Validando el motivo si es ingreso por venta directa entonces tiene que ser un valor BBX
			if(accountEntryRegisterTO.getMotive().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_BCB.getCode())){
				if(!objSecurity.getSecurityClass().equals(SecurityClassType.BBX.getCode())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, null, PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_MOTIVE_FOR_SECURITY);	
				}
			}
			
			//Validando el motivo si es ingreso por tesoro directo entonces tiene que ser un valor BTX
			if(accountEntryRegisterTO.getMotive().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode())){
				if(!objSecurity.getSecurityClass().equals(SecurityClassType.BTX.getCode())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, null, PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_MOTIVE_FOR_SECURITY);	
				}
			}
						
			boolean isMotiveOk = false;
			List<ParameterTable> allowedMotives = getAllowedDematerializationMotives(objSecurity);
			
			if(Validations.validateListIsNotNullAndNotEmpty(allowedMotives)){
				for(ParameterTable paramMotive : allowedMotives){
					if(accountEntryRegisterTO.getMotive().equals(paramMotive.getParameterTablePk()) && ComponentConstant.ONE.equals(paramMotive.getIndicator6())){
						isMotiveOk = true;
						break;
					}
				}
			}
			
			if(!isMotiveOk){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, null, PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_MOTIVE_FOR_SECURITY);
			}
			
			//Validando los hechos de mercado
			if(Validations.validateIsNotNullAndNotEmpty(accountEntryRegisterTO.getMarketFactAccountTO())){
				MarketFactAccountTO marketFactAccountTO = accountEntryRegisterTO.getMarketFactAccountTO();
				
				Date marketFactDate = CommonsUtilities.truncateDateTime(marketFactAccountTO.getMarketDate());
				if(objSecurity.getSecurityClass().equals(SecurityClassType.BBX.getCode())|| objSecurity.getSecurityClass().equals(SecurityClassType.BTX.getCode())){
					// Verificando la fecha de hecho de mercado
					Date issuanceDate = CommonsUtilities.truncateDateTime(objSecurity.getIssuanceDate());
					if (marketFactDate.compareTo(issuanceDate) != 0) {
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_DATE_HM,null);
					}
					//Verificando que la taza y precio sean igual a la emision del valor
					if(!objSecurity.getInitialNominalValue().equals(marketFactAccountTO.getMarketPrice())){
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_PRICE_HM, null);
					}
					if(!objSecurity.getInterestRate().equals(marketFactAccountTO.getMarketRate())){
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE_HM, null);
					}
				}else{
					// Verificando la fecha de hecho de mercado
					if (marketFactDate.compareTo(operationDate) != 0) {
						return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_DATE_HM,null);
					}
					if(marketFactAccountTO.getMarketPrice()!=null){
						String marketPriceString = marketFactAccountTO.getMarketPrice().toString();
						String[] partPriceString = marketPriceString.split("\\.");
						//Validando longitud de precio de hm
						if(partPriceString[0]!= null && partPriceString[0].length()>10){
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_PRICE_HM, null);
						}
						if(partPriceString.length>1){
							if(partPriceString[1]!= null && partPriceString[1].length()>2){
								return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_PRICE_HM, null);
							}
						}
					}
					
					if(marketFactAccountTO.getMarketRate()!=null){
						String marketRateString = marketFactAccountTO.getMarketRate().toString();
						String[] partRateString = marketRateString.split("\\.");
						//Validando longitud de tasa de hm
						if(partRateString[0]!= null && partRateString[0].length()>4){
							return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE_HM, null);
						}
						if(partRateString.length>1){
							if(partRateString[1]!= null && partRateString[1].length()>8){
								return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PASSIVE_INTEREST_RATE_HM, null);
							}	
						}	
					}
				}
			}
			
			//Validando el estado del Valor
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("idSecurityCodePkParam", objSecurity.getIdSecurityCodePk().toString());
			Security security = findObjectByNamedQuery(Security.SECURITY_STATE, params);
			if (!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())){
				ParameterTable parameterTable = find(ParameterTable.class, security.getStateSecurity());
				Object parameter[] = {parameterTable.getDescription()};
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_STATE, null, parameter);
			}
			
		}else{
			accountEntryRegisterTO.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
			accountEntryRegisterTO.setStockQuantity(BigDecimal.ONE);
			accountEntryRegisterTO.setPrice(objSecurity.getCurrentNominalValue());
		}
     	
     	if(SecuritySourceType.FOREIGN.getCode().equals(objSecurity.getSecuritySource())){
     		return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, null, PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_FOREIGN);
		}		
     	
		HolderAccountObjectTO holderAccountObjectTO = accountEntryRegisterTO.getHolderAccountObjectTO();
		holderAccountObjectTO.setIdParticipant(objParticipant.getIdParticipantPk());
		holderAccountObjectTO.setCreateObject(true);
		holderAccountObjectTO.setDpfInterface(accountEntryRegisterTO.isDpfInterface());
				
		//	validate holder account and CUI holder     			  		
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountObjectTO.getAccountNumber())){    			
			holderAccountObjectTO.setAccountNumber(holderAccountObjectTO.getAccountNumber().intValue());
			holderAccountObjectTO.setValidateAccount(BooleanType.YES.getBooleanValue());    					
		} else {			
			holderAccountObjectTO.setValidateAccount(BooleanType.NO.getBooleanValue());
		}				
		
		logger.info(":::: ENVIA DATOS DE LA CUENTA ::::");
		logger.info("Participante: " + holderAccountObjectTO.getIdParticipant());
		logger.info("Participante dpf: " + holderAccountObjectTO.getAccountTypeCode());
		logger.info("Participante interface: " + holderAccountObjectTO.getAccountType());
		
		//transfomando el encoding a utf-8 issue 978 (se corrige solo a nivel pojo para no afectar los metodos core de commons)
		String textRead;
			for(NaturalHolderObjectTO naturalHolder:holderAccountObjectTO.getLstNaturalHolderObjectTOs()){
				try{
					
					textRead=naturalHolder.getFirstLastName();
					naturalHolder.setFirstLastName(new String(textRead.getBytes(),StandardCharsets.UTF_8));	
					
					textRead=naturalHolder.getSecondLastName();
					naturalHolder.setSecondLastName(new String(textRead.getBytes(),StandardCharsets.UTF_8));
					
					textRead=naturalHolder.getName();
					naturalHolder.setName(new String(textRead.getBytes(),StandardCharsets.UTF_8));
					
					textRead=naturalHolder.getLegalAddress();
					naturalHolder.setLegalAddress(new String(textRead.getBytes(),StandardCharsets.UTF_8));
				}catch(Exception e){
					e.printStackTrace();
				}
			}			
		
		Object objResponse = accountsRemoteService.get().createHolderNewRemoteService(loggerUser, holderAccountObjectTO);
			
		if(objResponse instanceof RecordValidationType){
			RecordValidationType validation = (RecordValidationType) objResponse;
			validation.setOperationRef(accountEntryRegisterTO);			
			return validation;
		} else if (objResponse instanceof HolderAccountObjectTO){
			holderAccountObjectTO = (HolderAccountObjectTO) objResponse;
		}
		
		try {			
			HolderAccount objHolderAccount = null;			
			if(holderAccountObjectTO.getIdHolderAccount() != null){
												
				//objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());	
				HolderAccountTO objHolderAccountTO = new HolderAccountTO();
				objHolderAccountTO.setIdHolderAccountPk(holderAccountObjectTO.getIdHolderAccount());
				objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountByDpf(objHolderAccountTO);				
				
				accountEntryRegisterTO.getHolderAccountObjectTO().setAccountNumber(objHolderAccount.getAccountNumber());
				logger.info(":::: CUENTA OBTENIDA PARA ENVIAR AL ACV (CHEK-ERROR) ::::");
				logger.info("Valor: " + objSecurity.getIdSecurityCodePk());
				logger.info("Id Cuenta: " + objHolderAccount.getIdHolderAccountPk());
				logger.info("Numero de Cuenta: " + objHolderAccount.getAccountNumber());
				logger.info("Participante: " + objHolderAccount.getParticipant().getIdParticipantPk());
				
				if(!objParticipant.getIdParticipantPk().equals(objHolderAccount.getParticipant().getIdParticipantPk())){
					return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_PARTICIPANT_BALANCE_INSERT, null);
				}
			}						
			
			if(objHolderAccount == null){
				return CommonsUtilities.populateRecordCodeValidation(accountEntryRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT, null);
			}
			
			AccountAnnotationOperation accountAnnotationOperation = new AccountAnnotationOperation();
			accountAnnotationOperation.setHolder(objHolderAccount.getRepresentativeHolder());
			accountAnnotationOperation.setHolderAccount(objHolderAccount);
			accountAnnotationOperation.setSecurity(objSecurity);
			accountAnnotationOperation.setMotive(accountEntryRegisterTO.getMotive());
			accountAnnotationOperation.setTotalBalance(accountEntryRegisterTO.getStockQuantity());
			accountAnnotationOperation.setExpeditionDate(accountAnnotationOperation.getSecurity().getIssuanceDate());
			accountAnnotationOperation.setSerialNumber(objSecurity.getSecuritySerial());
			accountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
			accountAnnotationOperation.setIndPrimarySettlement(ComponentConstant.ZERO);
			accountAnnotationOperation.setIndLien(ComponentConstant.ZERO);
			accountAnnotationOperation.setDepartment(accountEntryRegisterTO.getDepartament());
			accountAnnotationOperation.setProvince(accountEntryRegisterTO.getProvince());
			accountAnnotationOperation.setObligationNumber(accountEntryRegisterTO.getObligationNumber());
			accountAnnotationOperation.setAlternativeCode(objSecurity.getAlternativeCode());
			// issue 1398: asignando el participante que esta realizando la colocacion en venta directa
			if( Validations.validateIsNotNull(objParticipant)
				&& Validations.validateIsNotNullAndPositive(objParticipant.getIdParticipantPk())){
				accountAnnotationOperation.setPlacementParticipant(objParticipant);
			}
			//Se registro el numero de operacion 
			accountAnnotationOperation.setOperationNumber(accountEntryRegisterTO.getOperationNumber());
			
			if (accountEntryRegisterTO.getMunicipality()!=null){
				accountAnnotationOperation.setMunicipality(accountEntryRegisterTO.getMunicipality());
			}
							
			//Seteando datos por defecto para el departamento de la paz
			if(accountAnnotationOperation.getDepartment() != null){
				if(accountAnnotationOperation.getDepartment().equals(new Integer(3515))){
					accountAnnotationOperation.setProvince(new Integer(17367));
					accountAnnotationOperation.setMunicipality(new Integer(17497));
				}
			}
			
			AccountAnnotationMarketFact accountAnnotationMarketFact = new AccountAnnotationMarketFact();
			accountAnnotationMarketFact.setAccountAnnotationOperation(accountAnnotationOperation);
			//accountAnnotationMarketFact.setMarketPrice(accountEntryRegisterTO.getPrice());
			accountAnnotationMarketFact.setMarketPrice(accountEntryRegisterTO.getMarketFactAccountTO().getMarketPrice());
			//accountAnnotationMarketFact.setMarketRate(accountAnnotationOperation.getSecurity().getInterestRate());
			if(accountEntryRegisterTO.getMarketFactAccountTO().getMarketRate()!=null){
				accountAnnotationMarketFact.setMarketRate(accountEntryRegisterTO.getMarketFactAccountTO().getMarketRate());	
			}else{
				accountAnnotationMarketFact.setMarketRate(accountAnnotationOperation.getSecurity().getInterestRate());
			}
			
			
			//issue 787
			if(objSecurity.getSecurityClass().equals(SecurityClassType.DPA.getCode())
					|| objSecurity.getSecurityClass().equals(SecurityClassType.DPF.getCode())){
				if(!CommonsUtilities.truncateDateTime(accountAnnotationOperation.getSecurity().getIssuanceDate()).equals(CommonsUtilities.truncateDateTime(CommonsUtilities.currentDate()))){
					accountAnnotationMarketFact.setMarketDate(accountAnnotationOperation.getSecurity().getIssuanceDate());
				}else{
					accountAnnotationMarketFact.setMarketDate(CommonsUtilities.currentDate());
				}
			}else{
				//accountAnnotationMarketFact.setMarketDate(CommonsUtilities.currentDate());
				accountAnnotationMarketFact.setMarketDate(accountEntryRegisterTO.getMarketFactAccountTO().getMarketDate());
			}
			
			accountAnnotationMarketFact.setOperationQuantity(accountEntryRegisterTO.getStockQuantity());
			accountAnnotationOperation.setAccountAnnotationMarketFact(new ArrayList<AccountAnnotationMarketFact>());
			accountAnnotationOperation.getAccountAnnotationMarketFact().add(accountAnnotationMarketFact);
					
			//registerAnnotationOperation(accountAnnotationOperation, loggerUser);//ingreso provicional
			registerAnnotationOperationOriginal(accountAnnotationOperation, loggerUser);//desmaterializacion normal
			
			
			confirmAnnotationOperation(accountAnnotationOperation, loggerUser, true);
			
			accountEntryRegisterTO.setIdCustodyOperation(accountAnnotationOperation.getIdAnnotationOperationPk());
			accountEntryRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			accountEntryRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
			
		} catch (Exception e) {
			throw e;
		} 		
		
		return accountEntryRegisterTO;
	}
	
	/**
	 * Validate and save reverse registration placement tx.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveReverseRegistrationPlacementTx(ReverseRegisterTO reverseRegisterTO, LoggerUser loggerUser) throws ServiceException {
		
		Object objResult = validateAndSaveReverseRegistrationPlacement(reverseRegisterTO, loggerUser);
		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * Validate and save reverse registration placement.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveReverseRegistrationPlacement(ReverseRegisterTO reverseRegisterTO, LoggerUser loggerUser) throws ServiceException {
		
		SecurityObjectTO securityObjectTO = reverseRegisterTO.getSecurityObjectTO();
		
		// validate if participant entity is allowed for ACV interface
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(reverseRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		
		if(objParticipant == null){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			reverseRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		// check operation date is today
		Date operationDate = CommonsUtilities.truncateDateTime(reverseRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date)!=0){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENVIO_DATE,null);
		}
				
		// check if operation code is correct for ACV
		if(!ComponentConstant.INTERFACE_EARLY_PAYMENT_OF_PLACEMENT.equals(reverseRegisterTO.getOperationCode())){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// check if operation was successfully registered previously
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
				objParticipant.getIdParticipantPk(), reverseRegisterTO.getOperationNumber(), 
				reverseRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// Verificando que el numero de obligacion no se haya procesado
		List<InterfaceTransaction> lstInterfaceTransactionBefore = holderAccountServiceBean.getListInterfaceTransactionByObligationNumber(
						objParticipant.getIdParticipantPk(),reverseRegisterTO.getObligationNumber(),reverseRegisterTO.getOperationCode());
		if (Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionBefore)) {
			//Verificando si ha realizado la colocacion
			List<InterfaceTransaction> lstInterfaceTransactionPlacement = holderAccountServiceBean.getListInterfaceTransactionByObligationNumber(
					objParticipant.getIdParticipantPk(), reverseRegisterTO.getObligationNumber(), 
					ComponentConstant.INTERFACE_SECURITY_PLACEMENT_RCO);
			if(Validations.validateListIsNullOrEmpty(lstInterfaceTransactionPlacement)){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OBLIGATION_NUMBER,null);
			}else{
				if(lstInterfaceTransactionBefore.size()==lstInterfaceTransactionPlacement.size()){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OBLIGATION_NUMBER,null);
				}
			}
			
		}
		
		// VALIDATE SECURITY CLASS
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filter.setParameterTablePk(securityObjectTO.getSecurityClass());
		//filter.setText1(securityObjectTO.getSecurityClassDesc());		
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.BBX.getCode()) //DPA - issue 158
					&& !securityClassParam.getParameterTablePk().equals(SecurityClassType.BTX.getCode())
					){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			} else {
				securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
			}			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}
		
		//VALIDATE SECURITY CODE
		SecuritySearchTO securitySearchTO= new SecuritySearchTO();
		securitySearchTO.setDescription(securityObjectTO.getSecuritySerial());
		List<Security> listSecurityAux = holderAccountServiceBean.findSecuritiesByDescriptionForBbxBtx(securitySearchTO);
		if(Validations.validateListIsNullOrEmpty(listSecurityAux)){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE, null);
		}
		
		String idSecurityCode= CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
		
		// VALIDATE SECURITY TARGET		
		Security objSecurity = new Security();
		
		if(idSecurityCode != null){
			objSecurity = find(Security.class, idSecurityCode);
		}	
		
		if(objSecurity != null){
			
			// VALIDATE STATE SECURITY
			if(!SecurityStateType.REGISTERED.getCode().equals(objSecurity.getStateSecurity())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
						new Object[]{ SecurityStateType.lookup.get(objSecurity.getStateSecurity()).getValue() });
			}
			
			// VALIDATE ISSUANCE DATE
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getIssuanceDate()) 
					&& Validations.validateIsNotNullAndNotEmpty(objSecurity.getIssuanceDate())){
				if(CommonsUtilities.convertDatetoString(securityObjectTO.getIssuanceDate()).compareTo(
						CommonsUtilities.convertDatetoString(objSecurity.getIssuanceDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
				}				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
			}						
			
			// VALIDATE EXPIRATION DATE
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getExpirationDate())
					&& Validations.validateIsNotNullAndNotEmpty(objSecurity.getExpirationDate())){
				if(CommonsUtilities.convertDatetoString(securityObjectTO.getExpirationDate()).compareTo(
						CommonsUtilities.convertDatetoString(objSecurity.getExpirationDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_PLACEMENT, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_PLACEMENT, null);
			}
			
			// VALIDATE CURRENCY SECURITY
			ParameterTableTO objParameterCurrency = new ParameterTableTO();
			objParameterCurrency.setText1(securityObjectTO.getCurrencyMnemonic());		
			objParameterCurrency.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstCurrency = parameterServiceBean.getListParameterTableServiceBean(objParameterCurrency);			
			if(Validations.validateIsNotNullAndNotEmpty(lstCurrency)){
				ParameterTable objParameterTable = lstCurrency.get(0);
				if(Validations.validateIsNotNull(objSecurity.getCurrency())
						&& !objSecurity.getCurrency().equals(objParameterTable.getParameterTablePk())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
				
			// VALIDATE INTEREST RATE
			BigDecimal BdInterestRate = CommonsUtilities.truncateDecimal(securityObjectTO.getInterestRate(), GeneralConstants.FOUR_VALUE_INTEGER);
			if(Validations.validateIsNotNullAndNotEmpty(BdInterestRate) && 
					Validations.validateIsNotNullAndNotEmpty(objSecurity.getInterestRate())) {
				if(objSecurity.getInterestRate().compareTo(BdInterestRate) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
			}
						
			//VALIDATE NUMBER COUPONS
			boolean blValidateCupons = false;
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
				blValidateCupons = true;
			} else {
				if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
					blValidateCupons = true;
				}
			}
			if(blValidateCupons){
				if(!objSecurity.getNumberCoupons().equals(0)){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_REVERSE_PLACEMENT, null);
				}
			} else {
				if(!objSecurity.getNumberCoupons().equals(securityObjectTO.getNumberCoupons())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_REVERSE_PLACEMENT, null);
				}
			}
			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}
		
		// VALIDATE HOLDER ACCOUNT AND HOLDER ACCOUNT BALANCE
		HolderAccount objHolderAccount = new HolderAccount();
		//Variables para cartera
		HolderAccountBalance objHolAccBalance = new HolderAccountBalance();
		HolderMarketFactBalance objHolMarketFactBalance = null;
		//Variables para ACV
		AccountAnnotationOperation objAccAnnOperation=null;
		AccountAnnotationMarketFact objAccAnnMarketFact= null;
		
		if(objParticipant != null && objSecurity != null){
			
			Long idHolderAccountPk = null;
			//Obteniendo el AccountAnnotationOperation por el numero de operacion reversion 
			objAccAnnOperation = holderAccountServiceBean.getAccountAnnotationOperation(
					objSecurity.getIdSecurityCodePk(),reverseRegisterTO.getReverseOperationNumber());
					
			//Verificando que se encuentre Account Annotation Operation para el numero de operacion origen
			if(Validations.validateIsNullOrEmpty(objAccAnnOperation)){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
			}
			
			idHolderAccountPk = objAccAnnOperation.getHolderAccount().getIdHolderAccountPk();	
				
			//Verificando si se envio el numero de cuenta titular
			HolderAccount holderAccountAux = null;
			if(reverseRegisterTO.getHolderAccountObjectTO()!=null && 
					Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getHolderAccountObjectTO().getAccountNumber())){											
				holderAccountAux = holderAccountServiceBean.getIdHolderAccountByAccountNumber(
						objParticipant.getIdParticipantPk(), 
						reverseRegisterTO.getHolderAccountObjectTO().getAccountNumber());
				//Si no encuentra la cuenta titular
				if(Validations.validateIsNullOrEmpty(holderAccountAux)){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ACCOUNT_NUMBER, null);
				} 
				//Verificando que la cuenta titular sea igual a la encontrada
				if(!idHolderAccountPk.equals(holderAccountAux.getIdHolderAccountPk())){
					return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ACCOUNT_NUMBER, null);
				}
			}
			
			if(!HolderAccountStatusType.ACTIVE.getCode().equals(objAccAnnOperation.getHolderAccount().getStateAccount())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDERACCOUNT_STATUS_NOT_REGISTERED, null);
			}
			
			//Verificando que el Account Annotation Operation obtenido no este revertido
			if(objAccAnnOperation.getState().equals(DematerializationStateType.REVERSED.getCode())){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION_ORIG, null);
			}
			
			//Obteniendo AccountAnnotationMarketFact
			objAccAnnMarketFact = holderAccountServiceBean.getHolderAccountMarketFact(objAccAnnOperation);
			if(Validations.validateIsNullOrEmpty(objAccAnnMarketFact)){
				return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
			}
			
			//Obteniendo el HolderAccountBalance por la cuenta titular
			List<HolderAccountBalance> lstHolderAccountBalances = holderAccountServiceBean.getListHolderAccountBalanceByIdHolderAccount(
					objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(),idHolderAccountPk, Boolean.FALSE);				
			if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
			} else {
				objHolAccBalance = lstHolderAccountBalances.get(0);	
				if(Validations.validateIsNotNullAndNotEmpty(objHolAccBalance)){
					objHolderAccount = find(HolderAccount.class, objHolAccBalance.getHolderAccount().getIdHolderAccountPk());				
					HolderAccountObjectTO objHolderAccountObjectTO = reverseRegisterTO.getHolderAccountObjectTO();				
					if(Validations.validateIsNotNullAndNotEmpty(objHolderAccount.getAccountNumber()) &&
							Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTO.getAccountNumber())){
						if(!objHolderAccount.getAccountNumber().equals(objHolderAccountObjectTO.getAccountNumber())){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_ACCOUNTNUMBER, null);
						}
					}
				}				
			}
		}
				
		if(objParticipant != null && objSecurity != null && objHolderAccount != null && objHolAccBalance != null){
			
			// VALIDATE BUSSINES REVERSE ACV OPERATIONS
			if(Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getReverseOperationNumber())){
							
				// OBTENER TRANSACCION ACV
				List<InterfaceTransaction> lstInterfaceTransactionReverse = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getReverseOperationNumber(), ComponentConstant.INTERFACE_SECURITY_PLACEMENT_RCO);
				
				if(Validations.validateListIsNullOrEmpty(lstInterfaceTransactionReverse)){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
				}
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionReverse) &&
						lstInterfaceTransactionReverse.size() == 1){
					
					//check the operation date is equal to the placement date
					InterfaceProcess interfaceProcessAux = lstInterfaceTransactionReverse.get(0).getInterfaceProcess();
					Date operationDateReverse = CommonsUtilities.truncateDateTime(interfaceProcessAux.getProcessDate()); 
					if(operationDateReverse.compareTo(date)!=0){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE_PLACEMENT,null);
					}
					
					InterfaceTransaction objInterfaceTransaction = lstInterfaceTransactionReverse.get(0);
					
					if(objInterfaceTransaction.getCustodyOperation()!=null && !objInterfaceTransaction.getCustodyOperation().getIdCustodyOperationPk().equals(objAccAnnOperation.getIdAnnotationOperationPk())){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
					}
					
					if(Validations.validateIsNotNullAndNotEmpty(objAccAnnOperation)){
						// VALIDATE OPERATION DATE CUSTODY
						if(CommonsUtilities.convertDatetoString(objAccAnnOperation.getCustodyOperation().getOperationDate()).compareTo(
								CommonsUtilities.convertDatetoString(reverseRegisterTO.getOperationDate())) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_ACCOUNTENTRY_DATE, null);
						}
						
						// VALIDATE SECURITY TARGET
						if(objAccAnnOperation.getSecurity().getIdSecurityCodePk().compareTo(objSecurity.getIdSecurityCodePk()) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
						}												
					}
					
					List<Long> lstIdCustodyOperation = new ArrayList<Long>();										
					lstIdCustodyOperation.add(objAccAnnOperation.getCustodyOperation().getIdCustodyOperationPk());	
					
					// VALIDAR SI HUBIERON MOVIMIENTOS DESPUES DEL ACV
					List<HolderAccountMovement> lstHolderAccountMovementRenew = holderAccountServiceBean.getValidateHolderAccountMovementNotPlacement(objParticipant.getIdParticipantPk(), 
							objHolderAccount.getIdHolderAccountPk(), objAccAnnOperation.getSecurity().getIdSecurityCodePk(), 
							lstIdCustodyOperation);
					
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovementRenew)){												
						 return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_MOVEMENTS_EXISTS_REVERSE, null);												 
					 }
					
					//Recuperando Holder MarketFact Balance
					objHolMarketFactBalance = holderAccountServiceBean.getHolderMarketFactBalance(objHolAccBalance,objAccAnnMarketFact);
					if(Validations.validateIsNullOrEmpty(objHolMarketFactBalance)){
						return CommonsUtilities.populateRecordCodeValidation(null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
					}
					
					//REGISTER HOLDER ACCOUNT MOVEMENT REVERSAL AND CHANGING STATE OF ACCOUNT ANNOTATION OPERATION 
					creatingHolderAccountMovementReversal(objSecurity,objAccAnnOperation,objHolAccBalance,objAccAnnMarketFact);
					changingSatateAccountAnnotationOperation(objAccAnnOperation);
										
					//SUBTRACTING THE AMOUNT OF TOTAL VALANCE IN CARTERA AND UPDATE HOLDER MARKET FACT BALANCE
					subtractingAmountTotalBalanceHolderAccountBalance(objHolAccBalance,objAccAnnMarketFact);
					changeStatusAndUpdateHolMarketFactBalance(objHolMarketFactBalance,objAccAnnOperation);
					
					reverseRegisterTO.setIdCustodyOperation(objAccAnnOperation.getIdAnnotationOperationPk());
					reverseRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
															
				} else { 
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
				}				
			}
			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
		}		
		return reverseRegisterTO;
	}
	
	/**
	 * Geographic dpt provi municipality.
	 *
	 * @param departament the departament
	 * @param province the province
	 * @param municipality the municipality
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean geographicDptProviMunicipality(Integer departament, Integer province, Integer municipality){
		boolean exist=false;
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT gl0 FROM GeographicLocation gl0 ");
		sbQuery.append(" inner join gl0.geographicLocation gl1 ");
		sbQuery.append(" inner join gl1.geographicLocation gl2 ");
		
		sbQuery.append(" WHERE gl1.geographicLocation.idGeographicLocationPk = :departament");
		sbQuery.append(" AND gl1.idGeographicLocationPk = :province");
		sbQuery.append(" AND gl2.geographicLocation.idGeographicLocationPk = :country");
		if (municipality!=null){
			sbQuery.append(" AND gl0.idGeographicLocationPk = :municipality");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		if (municipality!=null){
			query.setParameter("municipality", municipality);
		}
		query.setParameter("departament", departament);
		query.setParameter("province", province);
		query.setParameter("country", countryResidence);
		
		List<GeographicLocation> listGeographicLocation = query.getResultList();
		if (listGeographicLocation.size()>0){
			exist=true;
		}
		return exist;
	}
	
	/**
	 * Confirm multiple annotation operation.
	 *
	 * @param arrAccountAnnotationOperation the arr account annotation operation
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	
	
	public String confirmMultipleAnnotationOperation(
			AccountAnnotationOperation[] arrAccountAnnotationOperation, LoggerUser loggerUser) throws ServiceException {
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (int i=0; i<arrAccountAnnotationOperation.length; ++i)
		{
			confirmAnnotationOperation(arrAccountAnnotationOperation[i], loggerUser, false);
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			}
		}
		return strOperation;
	}
	
	public String confirmMultipleAnnotationOperationWithOutCertificate(
			AccountAnnotationOperation[] arrAccountAnnotationOperation, LoggerUser loggerUser) throws ServiceException {
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (int i=0; i<arrAccountAnnotationOperation.length; ++i)
		{
			confirmAnnotationOperationWithOutCertificate(arrAccountAnnotationOperation[i], loggerUser, false);
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			}
		}
		
		return strOperation;
	}
	
	
	
	//ingreso de titulos
	public void confirmAnnotationOperationWithOutCertificate(
			AccountAnnotationOperation accountAnnotationOperationTemp, LoggerUser loggerUser, boolean sendFromDpf) throws ServiceException {
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(accountAnnotationOperationTemp.getIdAnnotationOperationPk());
		if(!DematerializationStateType.APPROVED.getCode().equals(accountAnnotationOperation.getState())/*&&
				!DematerializationStateType.CERTIFIED.getCode().equals(accountAnnotationOperation.getState())*/){
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE, new Object[]{accountAnnotationOperationTemp.getIdAnnotationOperationPk().toString()});
		}
		
		ErrorServiceType entitiesStates = validateEntities(accountAnnotationOperation);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates)){
			throw new ServiceException(entitiesStates);
		}
		
		accountAnnotationOperation.setState(DematerializationStateType.CONFIRMED.getCode());
		accountAnnotationOperation.setAlternativeCode(accountAnnotationOperationTemp.getAlternativeCode());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.CONFIRMED.getCode());
		accountAnnotationOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDate());
		accountAnnotationOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		
		
		List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(accountAnnotationOperation);
		
		Long businessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM.getCode();
		
		Integer motive = accountAnnotationOperation.getMotive(); 	
		if(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode().equals(motive)){
			businessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM.getCode();
			
		}else if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(motive)){
			businessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_GUARDA_MANAGED_CONFIRM.getCode();
			
		}else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(motive)){
			businessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_GUARDA_EXCLUSIVE_CONFIRM.getCode();
		}	

		accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(accountAnnotationOperation.getIdAnnotationOperationPk(),
																 businessProcess, 
																 accountBalanceTOs, accountAnnotationOperation.getCustodyOperation().getOperationType()));

			SecuritiesOperation securitiesOperation = registerSecuritiesOperationInPhysicalCertificate(accountAnnotationOperation);
			SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
			securitiesComponentTO.setIdBusinessProcess(businessProcess); 
			securitiesComponentTO.setIdSecuritiesOperation(securitiesOperation.getIdSecuritiesOperationPk());
			
			Security newSecurity = find(securitiesOperation.getSecurities().getIdSecurityCodePk(), Security.class);
			BigDecimal quantMultVN = securitiesOperation.getStockQuantity();//.multiply(newSecurity.getCurrentNominalValue());
			
			securitiesComponentTO.setIdSecurityCode(newSecurity.getIdSecurityCodePk());
			if(accountAnnotationOperation.getIndSerializable().equals(0)) {
				securitiesComponentTO.setIdOperationType(getAnnotationPhysicalSecurityOperationType(accountAnnotationOperation));		
				securitiesComponentTO.setDematerializedBalance(quantMultVN);
				securitiesComponentTO.setShareBalance(quantMultVN);
				securitiesComponentTO.setCirculationBalance(quantMultVN);
				securitiesComponentTO.setPhysicalBalance(quantMultVN);
				securitiesComponentTO.setPlacedBalance(quantMultVN);
				securitiesComponentTO.setIssuanceExecute(true);
			}else {
				if(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode().equals(motive) || DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(motive)){
					businessProcess = BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_GUARDA_MANAGED_CONFIRM.getCode();
					securitiesComponentTO.setIdBusinessProcess(businessProcess);
				}
					
				securitiesComponentTO.setIdOperationType(getAnnotationPhysicalSerializedSecurityOperationType(accountAnnotationOperation));
				securitiesComponentTO.setDematerializedBalance(quantMultVN);
				securitiesComponentTO.setIssuanceExecute(false);
			}
			
			securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);
	}
	
	//confirmar desmaterializacion
	public void confirmAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperationTemp, LoggerUser loggerUser, boolean sendFromDpf) throws ServiceException {
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(accountAnnotationOperationTemp.getIdAnnotationOperationPk());
		if(!DematerializationStateType.APPROVED.getCode().equals(accountAnnotationOperation.getState())&&
				!DematerializationStateType.CERTIFIED.getCode().equals(accountAnnotationOperation.getState())){
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE, new Object[]{accountAnnotationOperationTemp.getIdAnnotationOperationPk().toString()});
		}

		ErrorServiceType entitiesStates = validateEntities(accountAnnotationOperation);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates)){
			throw new ServiceException(entitiesStates);
		}
				
		accountAnnotationOperation.setState(DematerializationStateType.CONFIRMED.getCode());
		accountAnnotationOperation.setAlternativeCode(accountAnnotationOperationTemp.getAlternativeCode());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.CONFIRMED.getCode());
		accountAnnotationOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDate());
		accountAnnotationOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		accountAnnotationOperation.setCreditorSource(accountAnnotationOperationTemp.getCreditorSource());
		accountAnnotationOperation.setIndDematerialization(1);
		accountAnnotationOperation.setIndImmobilization(0);
		
		
		List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(accountAnnotationOperation);
		accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(accountAnnotationOperation.getIdAnnotationOperationPk(),
																 BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM.getCode(), 
																 accountBalanceTOs, accountAnnotationOperation.getCustodyOperation().getOperationType()));
		
		SecuritiesOperation securitiesOperation = registerSecuritiesOperationDematerialization(accountAnnotationOperation);
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();
		securitiesComponentTO.setIdBusinessProcess(BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM.getCode());
		securitiesComponentTO.setIdOperationType(securitiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(securitiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setIdSecurityCode(securitiesOperation.getSecurities().getIdSecurityCodePk());		
		securitiesComponentTO.setDematerializedBalance(securitiesOperation.getStockQuantity());
		securitiesComponentTO.setShareBalance(securitiesOperation.getStockQuantity());
		securitiesComponentTO.setCirculationBalance(securitiesOperation.getStockQuantity());
		//securitiesComponentTO.setPhysicalBalance(securitiesOperation.getStockQuantity());
		securitiesComponentTO.setIssuanceExecute(false);
		
		//Execute securities component
		securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);
	}
	
	/**
	 * Validate entities.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @return the error service type
	 */
	public ErrorServiceType validateEntities(AccountAnnotationOperation accountAnnotationOperation){
		Map<String, Object> mpEntities = new HashMap<String, Object>();	
		mpEntities.put(parameterServiceBean.HOLDER_ACCOUNT_KEY, accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk());
		mpEntities.put(parameterServiceBean.PARTICIPANT_KEY, accountAnnotationOperation.getHolderAccount().getParticipant().getIdParticipantPk());
		if(accountAnnotationOperation.getSecurity() != null){
			mpEntities.put(parameterServiceBean.SECURITY_KEY, accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
			mpEntities.put(parameterServiceBean.ISSUER_KEY, accountAnnotationOperation.getSecurity().getIssuer().getIdIssuerPk());
		}
		return parameterServiceBean.validateEntities(mpEntities);
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idAnnotationOperationPk the id annotation operation pk
	 * @param businessProcess the business process
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idAnnotationOperationPk, Long businessProcess, 
													List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType) {
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcess);
		objAccountComponent.setIdCustodyOperation(idAnnotationOperationPk);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		//objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		return objAccountComponent;
	}
			
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @return the holder account balance t os
	 */
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(AccountAnnotationOperation accountAnnotationOperation) {
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(accountAnnotationOperation.getHolderAccount().getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(accountAnnotationOperation.getTotalBalance());
		holderAccountBalanceTO.setCashPrice(accountAnnotationOperation.getCashPrice());
		if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation.getOperationPrice()))
			holderAccountBalanceTO.setOperationPrice(accountAnnotationOperation.getOperationPrice());
		//holderAccountBalanceTO.setLstMarketFactAccounts(null);
		
		/*if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())){
			AccountAnnotationMarketFact accountAnnotationMarketfact = findMarketFact(accountAnnotationOperation.getIdAnnotationOperationPk());
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			
			MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
			marketFactAccountTO.setMarketDate(accountAnnotationMarketfact.getMarketDate());
			marketFactAccountTO.setMarketPrice(accountAnnotationMarketfact.getMarketPrice());
			marketFactAccountTO.setMarketRate(accountAnnotationMarketfact.getMarketRate());
			marketFactAccountTO.setQuantity(accountAnnotationMarketfact.getOperationQuantity());
			holderAccountBalanceTO.getLstMarketFactAccounts().add(marketFactAccountTO);
		}*/
		
			
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the annotation security operation type.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @param issuanceForm the issuance form
	 * @return the annotation security operation type
	 */
	private Long getAnnotationPhysicalSecurityOperationType(AccountAnnotationOperation accountAnnotationOperation) {
		
		if(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode().equals(accountAnnotationOperation.getMotive()) || 
				DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(accountAnnotationOperation.getMotive())){
			return ParameterOperationType.SEC_SECURITIES_DEMATERIALIZATION.getCode();	
		} else if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(accountAnnotationOperation.getMotive())){
			return ParameterOperationType.ISSUANCE_DEMATERIALIZATION_GUARDA_MANAGED.getCode();
		} else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(accountAnnotationOperation.getMotive())){
			return ParameterOperationType.ISSUANCE_DEMATERIALIZATION_GUARDA_ECLUSIVE.getCode();
		}

		return 10003L;		
	}
	
	private Long getAnnotationPhysicalSerializedSecurityOperationType(AccountAnnotationOperation accountAnnotationOperation) {
		
		if( DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode().equals(accountAnnotationOperation.getMotive()) || 
			DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(accountAnnotationOperation.getMotive()) || 
			DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(accountAnnotationOperation.getMotive())){
			return ParameterOperationType.ISSUANCE_DEMATERIALIZATION_GUARDA_MANAGED_SERIALIZED.getCode();
		} else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(accountAnnotationOperation.getMotive())){
			return ParameterOperationType.ISSUANCE_DEMATERIALIZATION_GUARDA_ECLUSIVE_SERIALIZED.getCode();
		}

		return 0L;		
	}

	private Long getAnnotationSecurityOperationType(AccountAnnotationOperation accountAnnotationOperation,Integer issuanceForm) {
		
		if(DematerializationCertificateMotiveType.SUSCRIPTION.getCode().equals(accountAnnotationOperation.getMotive())){//SUSCRIPCION PREFERENTE
			return ParameterOperationType.SEC_SECURITIES_SUSCRIPTION.getCode();
			
		} else if(DematerializationCertificateMotiveType.DIRECT_PURCHASE_BCB.getCode().equals(accountAnnotationOperation.getMotive())){//VENTA DIRECTA
			return ParameterOperationType.SEC_SECURITIES_DIRECT_PURCHASE_BCB.getCode();
			
		} else if(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode().equals(accountAnnotationOperation.getMotive())){//TESORO DIRECTO TGN
			return ParameterOperationType.SEC_SECURITIES_DIRECT_PURCHASE_TGN.getCode();
			
		} else if(DematerializationCertificateMotiveType.AUCTION_PURCHASE.getCode().equals(accountAnnotationOperation.getMotive())){//SUBASTA
			return ParameterOperationType.SEC_SECURITIES_AUCTION.getCode();
			
		} else if(DematerializationCertificateMotiveType.MONEY_PURCHASE.getCode().equals(accountAnnotationOperation.getMotive())){//MESA DE DINERO
			return ParameterOperationType.SEC_SECURITIES_MONEY_PURCHASE.getCode();
			
		} else if(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode().equals(accountAnnotationOperation.getMotive()) || 
				DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(accountAnnotationOperation.getMotive())){	//DESMATERIALIZACION
			
			if(issuanceForm.equals(IssuanceType.DEMATERIALIZED.getCode())){
				return ParameterOperationType.SEC_SECURITIES_DEMATERIALIZATION.getCode();	
			}else{
				return ParameterOperationType.SEC_SECURITIES_DEMATERIALIZATION_MIXED.getCode();
			}
			
		} else if(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode().equals(accountAnnotationOperation.getMotive())				//ADQUISICION DIRECTA
				|| DematerializationCertificateMotiveType.INCOME_PURCHASE_PRIVATE_STOCK.getCode().equals(accountAnnotationOperation.getMotive())
				|| DematerializationCertificateMotiveType.INCOME_PURCHASE_PRIMARY.getCode().equals(accountAnnotationOperation.getMotive())){		//COMPRA ACCIONES PRIVADAS
			
			if(issuanceForm.equals(IssuanceType.DEMATERIALIZED.getCode())){
				return ParameterOperationType.SEC_SECURITIES_DIRECT_PURCHASE_PRIVATE_DEMATERIALIZED.getCode();	
			}else{
				return ParameterOperationType.SEC_SECURITIES_DIRECT_PURCHASE_PRIVATE_MIXED.getCode();
			}
			
		} else if(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode().equals(accountAnnotationOperation.getMotive())){//COMPRA ACCIONES BANCARIAS
			if(BooleanType.YES.getCode().equals(accountAnnotationOperation.getIndPrimarySettlement()) || issuanceForm.equals(IssuanceType.DEMATERIALIZED.getCode())){
				return ParameterOperationType.SEC_SECURITIES_PRIMARY_PLACEMENT_DEMATERIALIZED.getCode();	
			}else{
				return ParameterOperationType.SEC_SECURITIES_PRIMARY_PLACEMENT_MIXED.getCode();
			}	
			
		}else if(DematerializationCertificateMotiveType.DONATION.getCode().equals(accountAnnotationOperation.getMotive())){//DONACION
			return ParameterOperationType.SEC_SECURITIES_DEMATERIALIZATION.getCode();
			
		}
//		else if(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode().equals(accountAnnotationOperation.getMotive())){
//			return ParameterOperationType.SEC_SECURITIES_DEMATERIALIZATION_PHYSICAL.getCode();
//		}
		return null;
	}
	
	/**
	 * Register securities operation.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @return the securities operation
	 * @throws ServiceException the service exception
	 */
	private SecuritiesOperation registerSecuritiesOperationInPhysicalCertificate(AccountAnnotationOperation accountAnnotationOperation)throws ServiceException{

		BigDecimal cashAmount = null;
		
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		securitiesOperation.setSecurities(accountAnnotationOperation.getSecurity());
		securitiesOperation.setCustodyOperation(accountAnnotationOperation.getCustodyOperation());
		securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
		//* Cash Amount
		if(Validations.validateIsNotNull(accountAnnotationOperation.getTotalBalance())){
			if(accountAnnotationOperation.getTotalBalance().compareTo(BigDecimal.ZERO)>=0){
				cashAmount = accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurity().getCurrentNominalValue());
			}
		}	
		securitiesOperation.setCashAmount(cashAmount);
		securitiesOperation.setStockQuantity(accountAnnotationOperation.getTotalBalance());
		
		Long operationType =  getAnnotationPhysicalSecurityOperationType(accountAnnotationOperation);
		securitiesOperation.setOperationType(operationType);
		
		//securitiesOperation.setOperationType(ParameterOperationType.SEC_SECURITIES_DEMATERIALIZATION.getCode());
		
		return create(securitiesOperation);
	}
	
	private SecuritiesOperation registerSecuritiesOperation(AccountAnnotationOperation accountAnnotationOperation)throws ServiceException{

		BigDecimal cashAmount = null;
		
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		securitiesOperation.setSecurities(accountAnnotationOperation.getSecurity());
		securitiesOperation.setCustodyOperation(accountAnnotationOperation.getCustodyOperation());
		securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
		//* Cash Amount
		if(Validations.validateIsNotNull(accountAnnotationOperation.getTotalBalance())){
			if(accountAnnotationOperation.getTotalBalance().compareTo(BigDecimal.ZERO)>=0){
				cashAmount = accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurity().getCurrentNominalValue());
			}
		}	
		securitiesOperation.setCashAmount(cashAmount);
		securitiesOperation.setStockQuantity(accountAnnotationOperation.getTotalBalance());
		/*Long operationType =  (accountAnnotationOperation.getIndGuarda().equals(0))?getAnnotationSecurityOperationType(accountAnnotationOperation,securitiesOperation.getSecurities().getIssuanceForm())
																				   :ParameterOperationType.ISSUANCE_DEMATERIALIZATION_GUARDA.getCode();
		*/
		Long operationType =  getAnnotationSecurityOperationType(accountAnnotationOperation,securitiesOperation.getSecurities().getIssuanceForm());
		
		securitiesOperation.setOperationType(operationType);
		
		return create(securitiesOperation);
	}
	
	private SecuritiesOperation registerSecuritiesOperationDematerialization(AccountAnnotationOperation accountAnnotationOperation)throws ServiceException{

		BigDecimal cashAmount = null;
		
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		securitiesOperation.setSecurities(accountAnnotationOperation.getSecurity());
		securitiesOperation.setCustodyOperation(accountAnnotationOperation.getCustodyOperation());
		securitiesOperation.setOperationDate(CommonsUtilities.currentDate());
		//* Cash Amount
		if(Validations.validateIsNotNull(accountAnnotationOperation.getTotalBalance())){
			if(accountAnnotationOperation.getTotalBalance().compareTo(BigDecimal.ZERO)>=0){
				cashAmount = accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurity().getCurrentNominalValue());
			}
		}	
		securitiesOperation.setCashAmount(cashAmount);
		securitiesOperation.setStockQuantity(accountAnnotationOperation.getTotalBalance());
		/*Long operationType =  (accountAnnotationOperation.getIndGuarda().equals(0))?getAnnotationSecurityOperationType(accountAnnotationOperation,securitiesOperation.getSecurities().getIssuanceForm())
																				   :ParameterOperationType.ISSUANCE_DEMATERIALIZATION_GUARDA.getCode();
		*/
		
		Long operationType =  getAnnotationSecurityOperationType(accountAnnotationOperation,securitiesOperation.getSecurities().getIssuanceForm());
		securitiesOperation.setOperationType(operationType);
		
		return create(securitiesOperation);
	}
	
	/**
	 * Return List Account Annotation Operation.
	 *
	 * @param strIdSecurityCode the security code
	 * @return the Account Annotation Operation
	 */
	@SuppressWarnings("unchecked")
	public List<AccountAnnotationOperation> getAccountAnnotationOperationValidate(String strIdSecurityCode) {
		List<Integer> lstStates = new ArrayList<Integer>();
		lstStates.add(DematerializationStateType.REIGSTERED.getCode());
		lstStates.add(DematerializationStateType.APPROVED.getCode());
		lstStates.add(DematerializationStateType.CONFIRMED.getCode());
		lstStates.add(DematerializationStateType.REVIEWED.getCode());
		lstStates.add(DematerializationStateType.CERTIFIED.getCode());
		try{
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select AAO from AccountAnnotationOperation AAO ");
			sbQuery.append(" where AAO.security.idSecurityCodePk = :parIdSecurityCode ");
			sbQuery.append(" and AAO.state in (:parLstStatus) ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdSecurityCode", strIdSecurityCode);
			query.setParameter("parLstStatus", lstStates);
			return (List<AccountAnnotationOperation>) query.getResultList();			
		} catch (NoResultException e){				
		}
		return null;
	}

	/**
	 * Gets the allowed dematerialization motives.
	 *
	 * @param security the security
	 * @return the allowed dematerialization motives
	 */

	public List<ParameterTable> getAllowedDematerializationMotivesOriginal(Security security) {

		//dematerialization type for security
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setIndicator1("DESMATERIA");
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		List<ParameterTable> lstMotives = parameterServiceBean.getListParameterTableServiceBean(paramTabTO);
		List<ParameterTable> lstMotivesPerSecurity=new ArrayList<ParameterTable>();
		if(security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
				if(security.getSecurityType().equals(SecurityType.PUBLIC.getCode())){
					if(security.getIssuer().getEconomicSector().equals(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode())){
						for(ParameterTable objParameterTable:lstMotives){
							if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())){
								lstMotivesPerSecurity.add(objParameterTable);
								//break;
							}
						}
					} else {
						for(ParameterTable objParameterTable:lstMotives){
							if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_BCB.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.AUCTION_PURCHASE.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode())){
								lstMotivesPerSecurity.add(objParameterTable);
							}
						}
					}						
				}
				else if(security.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
							break;
						}
					}
				}
				
			}
			else if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode()) ||
								objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode()) ||
								objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
						}
					}
			}
		}
		if(security.getIssuanceForm().equals(IssuanceType.MIXED.getCode())){
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
				if(security.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
							break;
						}
					}
				}
			}
			else if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				if(security.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode()) || 
								objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
						}
					}
				}
				else{
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
							break;
						}
					}
				}
			}
			for(ParameterTable objParameterTable:lstMotives){ 
				if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.SUSCRIPTION.getCode()) ||
						objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode()) ||
						objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode())){
					lstMotivesPerSecurity.add(objParameterTable);
				}
			}
		}
		if(lstMotivesPerSecurity.size() <= 0 ){
			lstMotivesPerSecurity = lstMotives;
		}
	
		//issue 1263: habilitar para todas la clase de valor el motivo INGRESO POR DESMATERIALIZACION
		for(ParameterTable objParameterTable:lstMotives){ 
			if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode())){
				if(!lstMotivesPerSecurity.contains(objParameterTable)){
					lstMotivesPerSecurity.add(objParameterTable);
					break;
				}
			}
		}
		
		return lstMotivesPerSecurity;
	
		
	}
	public List<ParameterTable> getAllowedDematerializationMotives(Security security) {

		//dematerialization type for security
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setIndicator1("FISICO");
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		List<ParameterTable> lstMotives = parameterServiceBean.getListParameterTableServiceBean(paramTabTO);
		List<ParameterTable> lstMotivesPerSecurity=new ArrayList<ParameterTable>();
		if(security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
				if(security.getSecurityType().equals(SecurityType.PUBLIC.getCode())){
					if(security.getIssuer().getEconomicSector().equals(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode())){
						for(ParameterTable objParameterTable:lstMotives){
							if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())){
								lstMotivesPerSecurity.add(objParameterTable);
								//break;
							}
						}
					} else {
						for(ParameterTable objParameterTable:lstMotives){
							if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_BCB.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_TGN.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.AUCTION_PURCHASE.getCode()) ||
									objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode())){
								lstMotivesPerSecurity.add(objParameterTable);
							}
						}
					}						
				}
				else if(security.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
							break;
						}
					}
				}
				
			}
			else if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode()) ||
								objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode()) ||
								objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
						}
					}
			}
		}
		if(security.getIssuanceForm().equals(IssuanceType.MIXED.getCode())){
			if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
				if(security.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
							break;
						}
					}
				}
			}
			else if(security.getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())){
				if(security.getSecurityType().equals(SecurityType.PRIVATE.getCode())){
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode()) || 
								objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DIRECT_PURCHASE_PRIVATE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
						}
					}
				}
				else{
					for(ParameterTable objParameterTable:lstMotives){
						if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode())){
							lstMotivesPerSecurity.add(objParameterTable);
							break;
						}
					}
				}
			}
			for(ParameterTable objParameterTable:lstMotives){ 
				if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.SUSCRIPTION.getCode()) ||
						objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode()) ||
						objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode())){
					lstMotivesPerSecurity.add(objParameterTable);
				}
			}
		}
		if(lstMotivesPerSecurity.size() <= 0 ){
			lstMotivesPerSecurity = lstMotives;
		}
	
		//issue 1263: habilitar para todas la clase de valor el motivo INGRESO POR DESMATERIALIZACION
		for(ParameterTable objParameterTable:lstMotives){ 
			if(objParameterTable.getParameterTablePk().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode())){
				if(!lstMotivesPerSecurity.contains(objParameterTable)){
					lstMotivesPerSecurity.add(objParameterTable);
					break;
				}
			}
		}
		
		return lstMotivesPerSecurity;
	}
	
	
	/**
	 * Validate account entry query information.
	 *
	 * @param accountEntryQueryTO the account entry query to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> validateAccountEntryQueryInformation(AccountEntryQueryTO accountEntryQueryTO) throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_ACCOUNT_ENTRY_QUERY.equalsIgnoreCase(accountEntryQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
									accountEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(accountEntryQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
									accountEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			accountEntryQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//SECURITY CLASS (DPF - DPA)
		boolean isDpfDpa= false;
		ParameterTableTO filter = new ParameterTableTO();
		filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		for (ParameterTable parameterTable: securityClasses) {
			if (parameterTable.getText1().equalsIgnoreCase(accountEntryQueryTO.getSecurityClassDesc())) {
				isDpfDpa= true;
				accountEntryQueryTO.setSecurityClass(parameterTable.getParameterTablePk());
				break;
			}
		}
		if (!isDpfDpa) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					accountEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		return lstRecordValidationType;
	}

	
	/**
	 * Gets the account entry query to.
	 *
	 * @param accountEntryQueryTO the account entry query to
	 * @return the account entry query to
	 */
	public AccountEntryQueryTO getAccountEntryQueryTO(AccountEntryQueryTO accountEntryQueryTO) {
		List<HolderAccountObjectTO> lstHolderAccountObjectTO= new ArrayList<HolderAccountObjectTO>();
		List<Object[]> lstAccountEntryOperations= getListDpfAccountEntryOperation(accountEntryQueryTO.getOperationDate(), 
																					accountEntryQueryTO.getIdParticipant(),
																					accountEntryQueryTO.getSecurityClass());
		if (Validations.validateListIsNotNullAndNotEmpty(lstAccountEntryOperations)) {
			for (Object[] objAccountEntry: lstAccountEntryOperations) {
				Long operationNumber= new Long(objAccountEntry[0].toString());
				HolderAccountBalance holderAccountBalance= (HolderAccountBalance) objAccountEntry[1];
				Long idHolderAccountPk = new Long(objAccountEntry[2].toString());
				HolderAccount objHolderAccount = new HolderAccount();
				objHolderAccount.setIdHolderAccountPk(idHolderAccountPk);
				objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountForCustody(objHolderAccount);
				holderAccountBalance.setHolderAccount(objHolderAccount);
				HolderAccountObjectTO holderAccountObjectTO= holderBalanceMovementsServiceBean.populateHolderAccountObjectTO(holderAccountBalance);
				holderAccountObjectTO.setOperationNumber(operationNumber);
				lstHolderAccountObjectTO.add(holderAccountObjectTO);
			}
			accountEntryQueryTO.setLstHolderAccountObjectTO(lstHolderAccountObjectTO);
		}
		return accountEntryQueryTO;
	}
	
	/**
	 * Gets the list dpf account entry operation.
	 *
	 * @param operationDate the operation date
	 * @param idParticipant the id participant
	 * @param securityClass the security class
	 * @return the list dpf account entry operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListDpfAccountEntryOperation(Date operationDate, Long idParticipant, Integer securityClass) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT.operationNumber, ");
		stringBuffer.append(" HAB, ");
		stringBuffer.append(" HA.idHolderAccountPk ");
		stringBuffer.append(" FROM InterfaceTransaction IT, HolderAccountBalance HAB, AccountAnnotationOperation AAO ");
		stringBuffer.append(" INNER JOIN HAB.holderAccount HA ");
		stringBuffer.append(" INNER JOIN FETCH HAB.security SE ");
		//stringBuffer.append(" INNER JOIN FETCH HA.holderAccountDetails HAD ");
		//stringBuffer.append(" INNER JOIN FETCH HAD.holder HO ");
		stringBuffer.append(" WHERE AAO.idAnnotationOperationPk = IT.custodyOperation.idCustodyOperationPk ");
		stringBuffer.append(" and trunc(IT.interfaceProcess.processDate) = :operationDate ");
		stringBuffer.append(" and IT.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and AAO.holderAccount.idHolderAccountPk = HAB.holderAccount.idHolderAccountPk ");
		stringBuffer.append(" and AAO.security.idSecurityCodePk = HAB.security.idSecurityCodePk ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		stringBuffer.append(" and AAO.security.securityClass = :securityClass ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationDate", operationDate);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		query.setParameter("securityClass", securityClass);
		
		return query.getResultList();
	}
	
	/**
	 * Search account annotation operation.
	 *
	 * @param idAnnotationOperationPk the id annotation operation pk
	 * @return the account annotation operation
	 * @throws ServiceException the service exception
	 */
	public AccountAnnotationOperation searchAccountAnnotationOperation(Long idAnnotationOperationPk) throws ServiceException{		
		AccountAnnotationOperation accountAnnotationOperation=null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aao FROM AccountAnnotationOperation aao");
		sbQuery.append("	INNER JOIN  aao.custodyOperation co");
		sbQuery.append("	INNER JOIN FETCH aao.holderAccount ha");
		sbQuery.append("	INNER JOIN FETCH ha.participant pa");		
		sbQuery.append(" 	INNER JOIN FETCH aao.security sec");
		sbQuery.append(" 	INNER JOIN FETCH sec.issuer isu");	
		sbQuery.append(" 	INNER JOIN FETCH sec.issuance ");
		sbQuery.append(" 	WHERE aao.idAnnotationOperationPk=:idAnnotationOperationPk ");
		sbQuery.append(" 	ORDER BY co.operationNumber desc ");
		TypedQuery<AccountAnnotationOperation> typedQuery= em.createQuery(sbQuery.toString(), AccountAnnotationOperation.class);
		typedQuery.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
		accountAnnotationOperation= typedQuery.getSingleResult();
		return accountAnnotationOperation;
}
	
	/**
	 * Generate secondary account entry register to.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateSecondaryAccountEntryRegisterTO(AccountAnnotationOperation accountAnnotationOperation, 
																	Long idInterfaceProcess, LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		SecondaryAccountEntryRegisterTO secondaryAccountEntryRegisterTO= getSecondaryAccountEntryRegisterTO(accountAnnotationOperation);
		secondaryAccountEntryRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(secondaryAccountEntryRegisterTO, 
																	GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransactionTx(secondaryAccountEntryRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}
	
	
	/**
	 * Gets the secondary account entry register to.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @return the secondary account entry register to
	 * @throws ServiceException the service exception
	 */
	public SecondaryAccountEntryRegisterTO getSecondaryAccountEntryRegisterTO(AccountAnnotationOperation accountAnnotationOperation) throws ServiceException {
		SecondaryAccountEntryRegisterTO secondaryAccountEntryRegisterTO= new SecondaryAccountEntryRegisterTO();
		
		AccountAnnotationOperation accountAnnotationOperationResult=
				searchAccountAnnotationOperation(accountAnnotationOperation.getIdAnnotationOperationPk());
		
		HolderAccountObjectTO holderAccountObjectTO= new HolderAccountObjectTO();
		holderAccountObjectTO.setAccountNumber(accountAnnotationOperationResult.getHolderAccount().getAccountNumber());
		
		ParameterTable parameterTable= parameterServiceBean.getParameterDetail(accountAnnotationOperationResult.getHolderAccount().getAccountType());
		holderAccountObjectTO.setAccountTypeCode(parameterTable.getText1());
		
		List<HolderAccountDetail> lstHolderAccountDetails= accountAnnotationOperationResult.getHolderAccount().getHolderAccountDetails();
		//if there are a list of holders then is a co-ownership of natural persons
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails) && lstHolderAccountDetails.size() > 1) {
			holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
		} else {
			Holder holder= lstHolderAccountDetails.get(0).getHolder();
			if (PersonType.NATURAL.getCode().equals(holder.getHolderType())) {
				holderAccountObjectTO.setHolderType(GeneralConstants.ONE_VALUE_INTEGER);
			} else {
				holderAccountObjectTO.setHolderType(GeneralConstants.TWO_VALUE_INTEGER);
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountDetails)) {
			List<NaturalHolderObjectTO> lstNaturalHolderObjectTO= new ArrayList<NaturalHolderObjectTO>();
			for (HolderAccountDetail holderAccountDetail: lstHolderAccountDetails) {
				if (PersonType.JURIDIC.getCode().equals(holderAccountDetail.getHolder().getHolderType())) {
					JuridicHolderObjectTO juridicHolderObjectTO= holderBalanceMovementsServiceBean.populateJuridicHolderObjectTO(
																							holderAccountDetail.getHolder(), true);
					holderAccountObjectTO.setJuridicHolderObjectTO(juridicHolderObjectTO);
				} else {
					NaturalHolderObjectTO naturalHolderObjectTO= holderBalanceMovementsServiceBean.populateNaturalHolderObjectTO(
																							holderAccountDetail.getHolder(), true);
					lstNaturalHolderObjectTO.add(naturalHolderObjectTO);
				}
			}
			holderAccountObjectTO.setLstNaturalHolderObjectTOs(lstNaturalHolderObjectTO);
		}
		SecurityObjectTO securityObjectTO= holderBalanceMovementsServiceBean.populateSecurityObjectTO(accountAnnotationOperationResult.getSecurity());
		
		secondaryAccountEntryRegisterTO.setTransferType(ComponentConstant.MOTIVE_SECONDARY_ACCOUNT_ENTRY);
		secondaryAccountEntryRegisterTO.setSecurityObjectTO(securityObjectTO);
		secondaryAccountEntryRegisterTO.setHolderAccountObjectTO(holderAccountObjectTO);		
		secondaryAccountEntryRegisterTO.setIdCustodyOperation(accountAnnotationOperationResult.getIdAnnotationOperationPk());
		secondaryAccountEntryRegisterTO.setOperationCode(ComponentConstant.INTERFACE_SECONDARY_ACCOUNT_ENTRY_DPF);
		secondaryAccountEntryRegisterTO.setOperationDate(CommonsUtilities.currentDate());
		
		Security security = this.find(Security.class, accountAnnotationOperationResult.getSecurity().getIdSecurityCodePk());
		Participant objParticipant = participantServiceBean.getParticipantIsIssuer(security.getIssuer().getIdIssuerPk());
		if(objParticipant != null){
			secondaryAccountEntryRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			secondaryAccountEntryRegisterTO.setEntityNemonic(objParticipant.getMnemonic());			
		}
						
		secondaryAccountEntryRegisterTO.setOperationNumber(accountAnnotationOperationResult.getIdAnnotationOperationPk());
				
		return secondaryAccountEntryRegisterTO;
	}
	
	
	/**
	 * Validate and save reverse account entry tx.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveReverseAccountEntryTx(ReverseRegisterTO reverseRegisterTO, LoggerUser loggerUser) throws ServiceException {
		
		Object objResult = validateAndSaveReverseAccountEntry(reverseRegisterTO, loggerUser);
		
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	} 
	
	/**
	 * Validate and save reverse account entry.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveReverseAccountEntry(ReverseRegisterTO reverseRegisterTO, LoggerUser loggerUser) throws ServiceException {
		
		SecurityObjectTO securityObjectTO = reverseRegisterTO.getSecurityObjectTO();
		
		String idSecurityCode= CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
		
		// validate if participant entity is allowed for ACV interface
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(reverseRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		
		if(objParticipant == null){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			reverseRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		// check operation date is today
		Date operationDate = CommonsUtilities.truncateDateTime(reverseRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date)!=0){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE,null);
		}
			
		// check if operation code is correct for ACV
		if(!ComponentConstant.INTERFACE_REVERSE_ACCOUNT_ENTRY_DPF.equals(reverseRegisterTO.getOperationCode())){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// check if operation was successfully registered previously
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
				objParticipant.getIdParticipantPk(), reverseRegisterTO.getOperationNumber(), 
				reverseRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// VALIDATE SECURITY CLASS
		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filter.setText1(securityObjectTO.getSecurityClassDesc());		
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode()) //DPA - issue 158
					&& !securityClassParam.getParameterTablePk().equals(SecurityClassType.DPA.getCode())
					){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			} else {
				securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
			}			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}
		
		// VALIDATE SECURITY TARGET		
		
		Security objSecurity = new Security();
		
		if(idSecurityCode != null){
			objSecurity = find(Security.class, idSecurityCode);
		}	
		
		if(objSecurity != null){
			
			// VALIDATE STATE SECURITY
			
			if(!SecurityStateType.REGISTERED.getCode().equals(objSecurity.getStateSecurity())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
						new Object[]{ SecurityStateType.lookup.get(objSecurity.getStateSecurity()).getValue() });
			}
			
			// VALIDATE ISSUANCE DATE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getIssuanceDate()) 
					&& Validations.validateIsNotNullAndNotEmpty(objSecurity.getIssuanceDate())){
				if(CommonsUtilities.convertDatetoString(securityObjectTO.getIssuanceDate()).compareTo(
						CommonsUtilities.convertDatetoString(objSecurity.getIssuanceDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
				}				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
			}						
			
			// VALIDATE EXPIRATION DATE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getExpirationDate())
					&& Validations.validateIsNotNullAndNotEmpty(objSecurity.getExpirationDate())){
				if(CommonsUtilities.convertDatetoString(securityObjectTO.getExpirationDate()).compareTo(
						CommonsUtilities.convertDatetoString(objSecurity.getExpirationDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
			// VALIDATE CURRENCY SECURITY
			
			ParameterTableTO objParameterCurrency = new ParameterTableTO();
			objParameterCurrency.setText1(securityObjectTO.getCurrencyMnemonic());		
			objParameterCurrency.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstCurrency = parameterServiceBean.getListParameterTableServiceBean(objParameterCurrency);			
			if(Validations.validateIsNotNullAndNotEmpty(lstCurrency)){
				ParameterTable objParameterTable = lstCurrency.get(0);
				if(Validations.validateIsNotNull(objSecurity.getCurrency())
						&& !objSecurity.getCurrency().equals(objParameterTable.getParameterTablePk())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
						
			// VALIDATE NOMINAL VALUE
			BigDecimal BdNominalValue = CommonsUtilities.truncateDecimal(securityObjectTO.getNominalValue(), GeneralConstants.TWO_VALUE_INTEGER);
			if(Validations.validateIsNotNullAndNotEmpty(BdNominalValue) &&
					Validations.validateIsNotNullAndNotEmpty(objSecurity.getCurrentNominalValue())) {
				if(objSecurity.getCurrentNominalValue().compareTo(BdNominalValue) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, null);
				} 
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, null);
			}
			
			// VALIDATE INTEREST RATE
			
			BigDecimal BdInterestRate = CommonsUtilities.truncateDecimal(securityObjectTO.getInterestRate(), GeneralConstants.FOUR_VALUE_INTEGER);
			if(Validations.validateIsNotNullAndNotEmpty(BdInterestRate) && 
					Validations.validateIsNotNullAndNotEmpty(objSecurity.getInterestRate())) {
				if(objSecurity.getInterestRate().compareTo(BdInterestRate) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
			}
			
			// VALIDATE NUMBER COUPONS
			
			// VALIDATE NUMBER COUPONS
			
			boolean blValidateCupons = false;
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
				blValidateCupons = true;
			} else {
				if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
					blValidateCupons = true;
				}
			}//DPA - issue 158
			if(!securityObjectTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				if(blValidateCupons){
					if(!objSecurity.getNumberCoupons().equals(1)){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
					}
				} else {
					if(!objSecurity.getNumberCoupons().equals(securityObjectTO.getNumberCoupons())){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
					}
				}
			}
			
			// VALIDATE ALTERNATIVE CODE
			
			if(Validations.validateIsNotNullAndNotEmpty(securityObjectTO.getAlternativeCode()) && 
					Validations.validateIsNotNullAndNotEmpty(objSecurity.getAlternativeCode())){
				if(objSecurity.getAlternativeCode().trim().compareTo(securityObjectTO.getAlternativeCode().trim()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
			}
			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
		}
		
		// VALIDATE HOLDER ACCOUNT AND HOLDER ACCOUNT BALANCE
		
		HolderAccountBalance objHolAccBalance = new HolderAccountBalance();
		HolderAccount objHolderAccount = new HolderAccount();
		
		if(objParticipant != null && objSecurity != null){					
			List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
					objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(), Boolean.FALSE);				
			if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
			} else {
				objHolAccBalance = lstHolderAccountBalances.get(0);	
				if(objHolAccBalance != null){
					objHolderAccount = find(HolderAccount.class, objHolAccBalance.getHolderAccount().getIdHolderAccountPk());				
					HolderAccountObjectTO objHolderAccountObjectTO = reverseRegisterTO.getHolderAccountObjectTO();				
					if(Validations.validateIsNotNullAndNotEmpty(objHolderAccount.getAccountNumber()) &&
							Validations.validateIsNotNullAndNotEmpty(objHolderAccountObjectTO.getAccountNumber())){
						if(!objHolderAccount.getAccountNumber().equals(objHolderAccountObjectTO.getAccountNumber())){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_ACCOUNTNUMBER, null);
						}
					}
				}				
			}
		}					
		
		if(objParticipant != null && objSecurity != null && objHolderAccount != null && objHolAccBalance != null){
			
			// VALIDATE BUSSINES REVERSE ACV OPERATIONS
			
			if(Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getReverseOperationNumber())){

				// OBTENER TRANSACCION ACV
				
				List<InterfaceTransaction> lstInterfaceTransactionReverse = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getReverseOperationNumber(), ComponentConstant.INTERFACE_ACCOUNT_ENTRY_DPF);
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionReverse) &&
						lstInterfaceTransactionReverse.size() == 1){
					
					InterfaceTransaction objInterfaceTransaction = lstInterfaceTransactionReverse.get(0);
					AccountAnnotationOperation objAccountAnnotationOperation = find(AccountAnnotationOperation.class, 
							objInterfaceTransaction.getCustodyOperation().getIdCustodyOperationPk());
					
					if(objAccountAnnotationOperation == null){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_ACCOUNTRY_RECURRENT, null);
					} else {
						
						// VALIDATE OPERATION DATE CUSTODY
						
						if(CommonsUtilities.convertDatetoString(objAccountAnnotationOperation.getCustodyOperation().getOperationDate()).compareTo(
								CommonsUtilities.convertDatetoString(reverseRegisterTO.getOperationDate())) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_ACCOUNTENTRY_DATE, null);
						}
						
						// VALIDATE SECURITY TARGET
						
						if(objAccountAnnotationOperation.getSecurity().getIdSecurityCodePk().compareTo(objSecurity.getIdSecurityCodePk()) != 0){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
						}												
					}
					
					List<Long> lstIdCustodyOperation = new ArrayList<Long>();										
					lstIdCustodyOperation.add(objAccountAnnotationOperation.getCustodyOperation().getIdCustodyOperationPk());	
					
					// VALIDAR SI HUBIERON MOVIMIENTOS DESPUES DEL ACV
					
					List<HolderAccountMovement> lstHolderAccountMovementRenew = removeOperationsServiceBean.getValidateHolderAccountMovement(objParticipant.getIdParticipantPk(), 
							objHolderAccount.getIdHolderAccountPk(), objAccountAnnotationOperation.getSecurity().getIdSecurityCodePk(), 
							lstIdCustodyOperation);
					
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountMovementRenew)){												
						 return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_MOVEMENTS_EXISTS, null);												 
					 }
					
					// DELETE MOVEMENT SECURITY, COMPONENT SECURITIES AND ISSUANCE
					
					removeOperationsServiceBean.deleteSecuritiesMovementAndSecurityOperationRenewal(objAccountAnnotationOperation.getCustodyOperation(), 
							objAccountAnnotationOperation.getSecurity());
					
					// DELETE MOVEMENT SECURITY, COMPONENT ACCOUNT
					
					removeOperationsServiceBean.deleteHolderAccountMovementRenewal(objAccountAnnotationOperation.getCustodyOperation());					
					deleteAccountAnnotationMarketFact(objAccountAnnotationOperation);					
					deleteAccountAnnotationOperation(objAccountAnnotationOperation);					
					removeOperationsServiceBean.updateInterfaceTransaction(objAccountAnnotationOperation.getCustodyOperation(), loggerUser);
					removeOperationsServiceBean.deleteCustodyOperationRenewal(objAccountAnnotationOperation.getCustodyOperation());	
					
					// ELIMINAR CUENTA MATRIZ DEL VALOR CREADO
					
					removeOperationsServiceBean.deleteHolderAccountBalanceTarget(objHolAccBalance);
					removeOperationsServiceBean.deleteHolderMarketFactBalanceTarget(objHolAccBalance);
					
					// DELETE SECURITY AND  PAYMENT CHRONOGRAM
					
					removeOperationsServiceBean.deleteSecurityAndPaymentChronogramTarget(objAccountAnnotationOperation.getSecurity());
					
				} else { 
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
				}				
			}
			
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_PLACEMENT_RECURRENT, null);
		}		
		return reverseRegisterTO;
	}
	
	/**
	 * The method is used to delete AccountAnnotationMarketFact.
	 *
	 * @param objAccountAnnotationOperation the obj account annotation operation
	 * @throws ServiceException the service exception
	 */
	public void deleteAccountAnnotationMarketFact(AccountAnnotationOperation objAccountAnnotationOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objAccountAnnotationOperation.getIdAnnotationOperationPk())){
			sbQuery.append(" Delete AccountAnnotationMarketFact aamf  ");
			sbQuery.append(" where aamf.accountAnnotationOperation.idAnnotationOperationPk = :parIdAnnotationOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdAnnotationOperation", objAccountAnnotationOperation.getIdAnnotationOperationPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete SecuritiesRenewalOperation.
	 *
	 * @param objAccountAnnotationOperation the obj account annotation operation
	 * @throws ServiceException the service exception
	 */
	public void deleteAccountAnnotationOperation(AccountAnnotationOperation objAccountAnnotationOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(objAccountAnnotationOperation.getIdAnnotationOperationPk())){
			sbQuery.append(" Delete AccountAnnotationOperation aao  ");	
			sbQuery.append(" where aao.idAnnotationOperationPk = :parIdAnnotationOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdAnnotationOperation", objAccountAnnotationOperation.getIdAnnotationOperationPk());
			query.executeUpdate();
		}
		
		
	}
			
	/**
	 * Update Request Balance in the Placement Segment.
	 *
	 * @param securityCode the security code
	 * @param participantPk the participant pk
	 * @param stockQuantity the stock quantity
	 * @throws ServiceException the service exception
	 */
	public void updatePlacementSegment(String securityCode, Long participantPk, BigDecimal stockQuantity) throws ServiceException{
		PlacementSegmentDetail placementSegmentDetail=getPlacementDetail(securityCode,participantPk);
		BigDecimal requestBalance=CommonsUtilities.addTwoDecimal(placementSegmentDetail.getRequestBalance(), stockQuantity, 4);
		placementSegmentDetail.setRequestBalance(requestBalance);
	}
	
	/**
	 * Gets the placement detail.
	 *
	 * @param securityCode the security code
	 * @param participantPk the participant pk
	 * @return the placement detail
	 * @throws ServiceException the service exception
	 */
	private PlacementSegmentDetail getPlacementDetail(String securityCode, Long participantPk) throws ServiceException{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" Select det From PlacementSegmentDetail det					");
		stringBuffer.append(" Inner Join det.placementSegment placement						");
		stringBuffer.append(" Inner Join placement.placementSegParticipaStructs part		");
		stringBuffer.append(" Where det.security.idSecurityCodePk = :securityCode 	  And	");
		stringBuffer.append(" 		part.participant.idParticipantPk = :participantPk And	");
		stringBuffer.append(" 		placement.placementSegmentState = :openState			");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("securityCode", securityCode);
		parameters.put("participantPk", participantPk);
		parameters.put("openState", PlacementSegmentStateType.OPENED.getCode());		
		
		return (PlacementSegmentDetail) findObjectByQueryString(stringBuffer.toString(), parameters);
	}
	
	/**
	 * Checks if is exists issuer.
	 *
	 * @param consultCdpf the consult cdpf
	 * @return true, if is exists issuer
	 */
	@SuppressWarnings("unchecked")
	public boolean isExistsIssuer(QueryParametersConsultCdpf consultCdpf) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select MNEMONIC from Issuer where MNEMONIC='"+consultCdpf.getCodEm()+"'");
		Query query = em.createNativeQuery(sbQuery.toString());
		List<Object[]> list = null;
		try{
			list = (List<Object[]>)query.getResultList();
			if(list.size()>0)
				return true;
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Checks if is exists cod serie.
	 *
	 * @param cSerie the c serie
	 * @return true, if is exists cod serie
	 */
	@SuppressWarnings("unchecked")
	public boolean isExistsCodSerie(String cSerie) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("select id_security_code_only from security where id_security_code_only='"+cSerie+"'");
		Query query = em.createNativeQuery(sbQuery.toString());
		List<Object[]> list = null;
		try{
			list = (List<Object[]>)query.getResultList();
			if(list.size()>0)
				return true;
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Gets the dematerialized securities.
	 *
	 * @param filters the filters
	 * @return the dematerialized securities
	 */
	@SuppressWarnings("unchecked")
	public List<SecurityConsultationTO> getDematerializedSecurities(QueryParametersConsultCdpf filters){
		String DATE_FORMAT = "dd/MM/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select * from( select id_security_code_only AS cserie, "
				+ " (case when security_class = 420 then 'DPF' END) AS codInst, "
				+ " (SELECT MNEMONIC FROM IDEPOSITARY.ISSUER WHERE ID_ISSUER_PK=ID_ISSUER_FK) AS codEm, "
				+ " SUBSTR (id_security_code_only,4,1) AS monedaGen, DECODE(CURRENCY,1304,'ECU',1734,'UFV',127,'BOB',430,'USD',1853,'DMV') As moneda, "
				+ " (CASE WHEN NUMBER_COUPONS > 1 THEN 'CUP' ELSE 'IAV' END) As metodoValuacion, TO_CHAR (ISSUANCE_DATE,'dd/MM/yyyy') As fechaEmision, "
				+ " (EXPIRATION_DATE-ISSUANCE_DATE)As Plazo, TO_CHAR (EXPIRATION_DATE,'dd/MM/yyyy') As fechaVenci,INTEREST_RATE as trem, null as tDesc, "
				+ " DECODE (number_coupons, 1, '0',PERIODICITY_DAYS)  As plazo_amortiza, (CASE WHEN NUMBER_COUPONS > 1 THEN NUMBER_COUPONS ELSE 0 END) As nroCupones, "
				+ " INITIAL_NOMINAL_VALUE As valorEmision,INITIAL_NOMINAL_VALUE As valorNominal, SUBSTR (id_security_code_only,1,3) As agenteRegistrador, "
				+ " (CASE WHEN STATE_SECURITY =2033 THEN 'VE' WHEN STATE_SECURITY =1372 THEN 'SU' WHEN STATE_SECURITY =641 THEN 'RE' WHEN STATE_SECURITY = 131 THEN 'VI'  END) As status, "
				+ " NULL  As diasRedimidos,(CASE WHEN NUMBER_COUPONS > 1 THEN 7 ELSE 6 END)  As tipoGenericoValor, 'F' As lCupNegociable,'F' As lFciNeg,'F' As prepago,'F' As lSubor,'T' As lTasaFija, "
				+ " null As tipoBono,NULL As ltasaDif, null As tir,ID_SECURITY_CODE_PK FROM idepositary.SECURITY WHERE  SECURITY_CLASS = 420)"
				+ "  where 1=1");
				if(Validations.validateIsNotNullAndNotEmpty(filters.getCodInst()))
					sbQuery.append(" and codInst = '"+filters.getCodInst()+"'");
				if(Validations.validateIsNotNullAndNotEmpty(filters.getcSerie()))
					sbQuery.append(" and cSerie = '"+filters.getcSerie()+"'");
				if(Validations.validateIsNotNullAndNotEmpty(filters.getCodEm()))
					sbQuery.append(" and codEm = '"+filters.getCodEm()+"'");
				if(Validations.validateIsNotNullAndNotEmpty(filters.getFechaEmi())){
				    String fecha= sdf.format(filters.getFechaEmi());
					sbQuery.append(" and fechaEmision = '"+fecha+"'");
				}
		Query query = em.createNativeQuery(sbQuery.toString());		
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)query.getResultList();
		}catch(NoResultException ex){
			return null;
		}
		List<SecurityConsultationTO> list=new ArrayList<>();
		for (Object[] obj : objectData) {
			SecurityConsultationTO to=new SecurityConsultationTO();
			to.setCserie((obj[0]!=null)?String.valueOf(obj[0]):"");
			to.setCodInst((obj[1]!=null)?String.valueOf(obj[1]):"");
			to.setCodEm((obj[2]!=null)?String.valueOf(obj[2]):"");
			to.setMonedaGen((obj[3]!=null)?String.valueOf(obj[3]):"");
			to.setMoneda((obj[4]!=null)?String.valueOf(obj[4]):"");
			to.setMetodoValuacion((obj[5]!=null)?String.valueOf(obj[5]):"");
			Date date;
			try {
				date = sdf.parse(String.valueOf(obj[6]));
				to.setFechaEmision(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			to.setPlazo((obj[7]!=null)?String.valueOf(obj[7]):"");
			try {
				date = sdf.parse(String.valueOf(obj[8]));
				to.setFechaVenci(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			to.setTrem(((BigDecimal)(obj[9]==null?BigDecimal.ZERO:obj[9])));
			to.settDesc(((BigDecimal)(obj[10]==null?BigDecimal.ZERO:obj[10])));
			to.setPlazoAmortiza(Long.parseLong((obj[11]!=null)?String.valueOf(obj[11]):"0"));
			to.setNroCupones(Long.parseLong((obj[12]!=null)?String.valueOf(obj[12]):"0"));
			to.setValorEmision(((BigDecimal)(obj[13]==null?BigDecimal.ZERO:obj[13])));
			to.setValorNominal(((BigDecimal)(obj[14]==null?BigDecimal.ZERO:obj[14])));
			to.setAgenteRegistrador((obj[15]!=null)?String.valueOf(obj[15]):"");
			to.setStatus((obj[16]!=null)?String.valueOf(obj[16]):"");
			to.setDiasRedimidos(Long.parseLong((obj[17]!=null)?String.valueOf(obj[17]):"0"));
			to.setTipoGenericoValor(Long.parseLong((obj[18]!=null)?String.valueOf(obj[18]):"0"));
			to.setlCupNegociable((obj[19]!=null)?String.valueOf(obj[19]):"");
			to.setlFciNeg((obj[20]!=null)?String.valueOf(obj[20]):"");
			to.setPrepago((obj[21]!=null)?String.valueOf(obj[21]):"");
			to.setlSubor((obj[22]!=null)?String.valueOf(obj[22]):"");
			to.setlTasaFija((obj[23]!=null)?String.valueOf(obj[23]):"");
			to.setTipoBono((obj[24]!=null)?String.valueOf(obj[24]):"");
			to.setLtasaDif((obj[25]!=null)?String.valueOf(obj[25]):"");
			to.setTir(Long.parseLong((obj[26]!=null)?String.valueOf(obj[26]):"0"));
			if(to.getNroCupones()!=0L){
				List<CouponObjectTO> cupones=getListcupones(String.valueOf(obj[27]));
				for (CouponObjectTO couponObjectTO : cupones) 
					to.getLcupones().add(couponObjectTO);
			}
			list.add(to);
		}
		return list;
	}
	
	/**
	 * Gets the listcupones.
	 *
	 * @param idSecurityCodePK the id security code pk
	 * @return the listcupones
	 */
	@SuppressWarnings("unchecked")
	public List<CouponObjectTO> getListcupones(String idSecurityCodePK){
		if(!Validations.validateIsNotNullAndNotEmpty(idSecurityCodePK))
			return new ArrayList<>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select COUPON_NUMBER numeroCupon, "
				+ " to_char(EXPERITATION_DATE,'dd/MM/yyyy') "
				+ " fechaVenc from idepositary.program_interest_coupon a where ID_INT_PAYMENT_SCHEDULE_FK in (select ID_INT_PAYMENT_SCHEDULE_PK"
				+ " from idepositary.interest_payment_schedule where "
				+ " ID_SECURITY_CODE_FK ='"+idSecurityCodePK+"') "
				+ " order by ID_INT_PAYMENT_SCHEDULE_FK,ID_PROGRAM_INTEREST_PK ");
		System.out.println("CONSULTA CUPON:"+sbQuery);
		Query query = em.createNativeQuery(sbQuery.toString());		
		List<Object[]> objectData = null;
		try{
			objectData = (List<Object[]>)query.getResultList();
		}catch(NoResultException ex){
			return null;
		}
		List<CouponObjectTO> list=new ArrayList<>();
		CouponObjectTO to;
		String DATE_FORMAT = "dd/MM/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		for (Object[] obj : objectData) {
			to=new CouponObjectTO();
			to.setCouponNumber(Integer.parseInt((obj[1]!=null)?String.valueOf(obj[0]):"0"));
			Date date;
			try {
				date = sdf.parse(String.valueOf(obj[1]));
				to.setExpirationDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			list.add(to);
		}
		return list;
	}
	
/**
 * Validate security entry query information.
 *
 * @param securityEntryQueryTO the security entry query to
 * @return the list
 * @throws ServiceException the service exception
 */
public List<RecordValidationType> validateSecurityEntryQueryInformation(SecurityEntryQueryTO securityEntryQueryTO) throws ServiceException {
		
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_BCB_BTX_SECURITY_ENTRY_QUERY.equalsIgnoreCase(securityEntryQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(securityEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(securityEntryQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(securityEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			securityEntryQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		
		
		//VALIDAMOS LA FECHA INICIO DE CONSULTA
		if (!(CommonsUtilities.currentDate().compareTo(securityEntryQueryTO.getInitialDate())==0) || (securityEntryQueryTO.getInitialDate().compareTo(CommonsUtilities.currentDate())<0)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(securityEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//VALIDAMOS LA FINAL DE CONSULTA
		if (securityEntryQueryTO.getFinalDate().compareTo(securityEntryQueryTO.getInitialDate())<0) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(securityEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//SECURITY CLASS (DPF - DPA)
//		boolean isDpfDpa= false;
//		ParameterTableTO filter = new ParameterTableTO();
//		filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
//		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
//		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
//		for (ParameterTable parameterTable: securityClasses) {
//			if (parameterTable.getText1().equalsIgnoreCase(accountEntryQueryTO.getSecurityClassDesc())) {
//				isDpfDpa= true;
//				accountEntryQueryTO.setSecurityClass(parameterTable.getParameterTablePk());
//				break;
//			}
//		}
//		if (!isDpfDpa) {
//			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
//					accountEntryQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
//			lstRecordValidationType.add(recordValidationType);
//			return lstRecordValidationType;
//		}
//		
		return lstRecordValidationType;
	}

/**
 * Gets the security entry query to.
 *
 * @param securityEntryQueryTO the security entry query to
 * @return the security entry query to
 */
public SecurityEntryQueryTO getSecurityEntryQueryTO(SecurityEntryQueryTO securityEntryQueryTO) {
	
	ParameterTableTO  parameterTableTO = new ParameterTableTO();
	parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
	documentTypeDescription = new HashMap<Integer,String>(); 
    
	try {
		for(ParameterTable param : generalPametersFacade.getListParameterTableServiceBean(parameterTableTO)){
			documentTypeDescription.put(param.getParameterTablePk(), param.getIndicator1());
		}
	} catch (ServiceException e) {
		e.printStackTrace();
	}
	
	List<Object[]> lstAccountEntryOperations= getListbbxBtxSecurityEntryOperation(securityEntryQueryTO.getInitialDate(),securityEntryQueryTO.getFinalDate(), 
																				securityEntryQueryTO.getSecurityClass());
	if (Validations.validateListIsNotNullAndNotEmpty(lstAccountEntryOperations)) {
		
		Map<String, Object> mapSecurity = new HashMap<String, Object>();
		for (Object[] objAccountEntry: lstAccountEntryOperations) {
			HolderAccountBalance holderAccountBalance= (HolderAccountBalance) objAccountEntry[1];
			mapSecurity.put(holderAccountBalance.getSecurity().getIdSecurityCodePk(), holderAccountBalance.getSecurity());
			
		}
		
		List<PlacementObjectBbxBtxTO> lstPlacementObjectBbxBtxTO;
		PlacementObjectBbxBtxTO placementObjectBbxBtxTO;
		List<SecurityObjectBbxBtxTO> listSecurityObjectBbxBtxTO = new ArrayList<>();
		SecurityObjectBbxBtxTO securityObjectBbxBtxTo = null;
		List<HolderAccountObjectBbXBtxTO> listHolderAccountObjectBbXBtxTO;
		
		for (Map.Entry<String, Object> entry : mapSecurity.entrySet()) {
			securityObjectBbxBtxTo = new SecurityObjectBbxBtxTO();
		    String key = entry.getKey().toString();
		    Security security = (Security)entry.getValue();
		    
		    securityObjectBbxBtxTo = new SecurityObjectBbxBtxTO();
		    securityObjectBbxBtxTo.setSecurityClass(security.getSecurityClass());
		    securityObjectBbxBtxTo.setSecuritySerial(security.getIdSecurityCodeOnly());
		    securityObjectBbxBtxTo.setIssuanceDate(security.getIssuanceDate());
		    securityObjectBbxBtxTo.setExpirationDate(security.getExpirationDate());
		    securityObjectBbxBtxTo.setCurrencyMnemonic(security.getCurrency().toString());
		    securityObjectBbxBtxTo.setInterestRate(security.getInterestRate());
		    
		    lstPlacementObjectBbxBtxTO = new ArrayList<PlacementObjectBbxBtxTO>();
		    
		    HolderAccountObjectBbXBtxTO holderAccountObjectBbXBtxTO;
		    AmountLocationObjectBbxBtxTO amountLocationObjectBbxBtxTO;
		    for (Object[] objAccountEntry: lstAccountEntryOperations) {
		    	HolderAccountBalance holderAccountBalance= (HolderAccountBalance) objAccountEntry[1];
				AccountAnnotationOperation accountAnnotationOperation = (AccountAnnotationOperation) objAccountEntry[2];
				
				placementObjectBbxBtxTO = new PlacementObjectBbxBtxTO();
				listHolderAccountObjectBbXBtxTO = new ArrayList<HolderAccountObjectBbXBtxTO>();
				
		    	if (key.equals(holderAccountBalance.getSecurity().getIdSecurityCodePk())){
		    		
		    		// lista de titulares
//					HolderAccountObjectTO holderAccountObjectTO= holderBalanceMovementsServiceBean.populateHolderAccountObjectTO(holderAccountBalance);
					List<HolderAccountDetail> listHolderAccountDetail = getListHolderEntryOperation(holderAccountBalance.getHolderAccount().getIdHolderAccountPk());
					for (HolderAccountDetail holderDetail : listHolderAccountDetail){
						
						holderAccountObjectBbXBtxTO = new HolderAccountObjectBbXBtxTO();
						holderAccountObjectBbXBtxTO.setAccountNumber(holderAccountBalance.getHolderAccount().getAccountNumber());
					    holderAccountObjectBbXBtxTO.setCui(holderDetail.getHolder().getIdHolderPk());
					    holderAccountObjectBbXBtxTO.setAccountType(holderAccountBalance.getHolderAccount().getAccountType());
					    holderAccountObjectBbXBtxTO.setDocumentType(documentTypeDescription.get(holderDetail.getHolder().getDocumentType()).toString());
					    holderAccountObjectBbXBtxTO.setDocumentNumber(holderDetail.getHolder().getDocumentNumber());
					    holderAccountObjectBbXBtxTO.setExpended(holderDetail.getHolder().getDocumentSource().toString());
					    holderAccountObjectBbXBtxTO.setIndResidence(holderDetail.getHolder().getIndResidence());
					    listHolderAccountObjectBbXBtxTO.add(holderAccountObjectBbXBtxTO);
					}
				    placementObjectBbxBtxTO.setListAccountObjectBbXBtxTO(listHolderAccountObjectBbXBtxTO);
				    
				    // desmaterializacion
				    amountLocationObjectBbxBtxTO = new AmountLocationObjectBbxBtxTO();
			    	if (accountAnnotationOperation.getObligationNumber()!=null){
			    		amountLocationObjectBbxBtxTO.setNroObligationNumber(accountAnnotationOperation.getObligationNumber());
			    	}else{
			    		amountLocationObjectBbxBtxTO.setNroObligationNumber(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk().toString());
			    	}
				    
				    amountLocationObjectBbxBtxTO.setCantVal(holderAccountBalance.getAvailableBalance());
				    amountLocationObjectBbxBtxTO.setAmount(holderAccountBalance.getAvailableBalance().multiply(holderAccountBalance.getSecurity().getCurrentNominalValue()));
				    amountLocationObjectBbxBtxTO.setDepartment(accountAnnotationOperation.getDepartment());
				    amountLocationObjectBbxBtxTO.setProvince(accountAnnotationOperation.getProvince());
				    amountLocationObjectBbxBtxTO.setMunicipality(accountAnnotationOperation.getMunicipality());
				    placementObjectBbxBtxTO.setAmountLocationObjectBbxBtxTO(amountLocationObjectBbxBtxTO);
				    
				    lstPlacementObjectBbxBtxTO.add(placementObjectBbxBtxTO);
				    
		    	}
		    }
		    
		    //adicionamos en el valor
		    securityObjectBbxBtxTo.setListPlacementObjectBbxBtxTO(lstPlacementObjectBbxBtxTO);
		    listSecurityObjectBbxBtxTO.add(securityObjectBbxBtxTo);
		}
		securityEntryQueryTO.setListSecurityObjectBbxBtxTO(listSecurityObjectBbxBtxTO);
	}
	return securityEntryQueryTO;
}

/**
 * Gets the listbbx btx security entry operation.
 *
 * @param initialDate the initial date
 * @param finalDate the final date
 * @param securityClass the security class
 * @return the listbbx btx security entry operation
 */
@SuppressWarnings("unchecked")
public List<Object[]> getListbbxBtxSecurityEntryOperation(Date initialDate,Date finalDate, Integer securityClass) {
	StringBuffer stringBuffer= new StringBuffer();
	stringBuffer.append(" SELECT SE.idSecurityCodePk,HAB,AAO ");
	stringBuffer.append(" FROM HolderAccountBalance HAB, AccountAnnotationOperation AAO ");
	stringBuffer.append(" INNER JOIN FETCH HAB.holderAccount HA ");
	stringBuffer.append(" INNER JOIN FETCH HAB.security SE ");
	stringBuffer.append(" INNER JOIN FETCH HA.holderAccountDetails HAD ");
	stringBuffer.append(" INNER JOIN FETCH HAD.holder HO ");
	
	stringBuffer.append(" where SE.issuer.idIssuerPk in (select issuer.idIssuerPk from Participant where mnemonic = 'BCB') ");
	stringBuffer.append(" and trunc(SE.expirationDate) BETWEEN :initialDate and :finalDate ");
	stringBuffer.append(" and SE.securityClass = :securityClass ");
	
	stringBuffer.append(" and AAO.holderAccount.idHolderAccountPk = HAB.holderAccount.idHolderAccountPk ");
	stringBuffer.append(" and AAO.security.idSecurityCodePk = HAB.security.idSecurityCodePk ");
	stringBuffer.append(" order by SE.idSecurityCodePk asc,HA.idHolderAccountPk asc ");
	
	Query query= em.createQuery(stringBuffer.toString());
	query.setParameter("initialDate", initialDate);
	query.setParameter("finalDate", finalDate);
	query.setParameter("securityClass", securityClass);
	
	return query.getResultList();
}

/**
 * Gets the list holder entry operation.
 *
 * @param idHolderAccountPk the id holder account pk
 * @return the list holder entry operation
 */
@SuppressWarnings("unchecked")
public List<HolderAccountDetail> getListHolderEntryOperation(Long idHolderAccountPk) {
	StringBuffer stringBuffer= new StringBuffer();
	stringBuffer.append(" SELECT had ");
	stringBuffer.append(" FROM HolderAccountDetail had ");
	stringBuffer.append(" INNER JOIN FETCH had.holder h ");
	stringBuffer.append(" where had.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
	
	Query query= em.createQuery(stringBuffer.toString());
	query.setParameter("idHolderAccountPk", idHolderAccountPk);
	return query.getResultList();
}

//issue 188 
@SuppressWarnings("unchecked")
public List<HolderMarketFactBalance> getListHolderMarketFacts( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) {
	StringBuilder sbQuery = new StringBuilder();
	sbQuery.append("SELECT a ");
	sbQuery.append("FROM HolderMarketFactBalance a ");
	sbQuery.append("WHERE 1=1 AND a.security.idSecurityCodePk = :idSecurityCodePk ");
	sbQuery.append("AND a.participant.idParticipantPk = :idParticipantPk ");
	sbQuery.append("AND a.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
	sbQuery.append("AND a.indActive = :indActive ");	
	Query query = em.createQuery(sbQuery.toString());
	query.setParameter("idSecurityCodePk", idSecurityCodePk);
	query.setParameter("idParticipantPk", idParticipantPk);
	query.setParameter("idHolderAccountPk", idHolderAccountPk);
	query.setParameter("indActive", GeneralConstants.ONE_VALUE_INTEGER);
	return (List<HolderMarketFactBalance>)query.getResultList();	
}
@SuppressWarnings("unchecked")
public HolderMarketFactBalance getHolderMarketFact( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) throws ServiceException {
	StringBuilder sbQuery = new StringBuilder();
	sbQuery.append("SELECT a ");
	sbQuery.append("FROM HolderMarketFactBalance a ");
	sbQuery.append("WHERE 1=1 ");
	sbQuery.append("AND a.security.idSecurityCodePk = :idSecurityCodePk ");
	sbQuery.append("AND a.participant.idParticipantPk = :idParticipantPk ");
	sbQuery.append("AND a.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
	sbQuery.append("AND a.indActive = :indActive ");
	Query query = em.createQuery(sbQuery.toString());
	query.setParameter("idSecurityCodePk", idSecurityCodePk);
	query.setParameter("idParticipantPk", idParticipantPk);
	query.setParameter("idHolderAccountPk", idHolderAccountPk);
	query.setParameter("indActive", GeneralConstants.ONE_VALUE_INTEGER);
	try {
		return (HolderMarketFactBalance)query.getSingleResult();
	} catch (Exception e) {
		return null;
	}		
}
@SuppressWarnings("unchecked")
public void changeStatusHolderMarketFactBalance(Long idMarketfactBalancePk) {
	StringBuilder sbQuery = new StringBuilder();
	sbQuery.append(" update HolderMarketFactBalance set indActive = 0");
	sbQuery.append(" where IdMarketfactBalancePk = :idMarketfactBalancePk");
	Query query=em.createQuery( sbQuery.toString() );
	query.setParameter("idMarketfactBalancePk", idMarketfactBalancePk);
	int deleted = query.executeUpdate();
}
//--
	/**
	 * Metodo que permite buscar el o los numeros de documentos que los titulares de una determinada Cuenta Titular
	 * @param idHolderAccountPk
	 * @return
	 */
	public List<String[]> getListDocumentHolderAccount(HolderAccountTO holderAccountTO){
		
		StringBuilder query = new StringBuilder();
		
		query.append(" select ");
		query.append(" h.DOCUMENT_NUMBER NUM_DOC, ");
		query.append(" (select INDICATOR1 from PARAMETER_TABLE where PARAMETER_TABLE_PK = h.DOCUMENT_TYPE) TIPO_DOC ");		
		query.append(" from HOLDER_ACCOUNT_DETAIL had ");
		query.append(" inner join HOLDER_ACCOUNT ha on ha.ID_HOLDER_ACCOUNT_PK = had.ID_HOLDER_ACCOUNT_FK ");
		query.append(" inner join HOLDER h on h.ID_HOLDER_PK = had.ID_HOLDER_FK ");
		query.append(" inner join PARTICIPANT P on P.ID_PARTICIPANT_PK = ha.ID_PARTICIPANT_FK ");
		query.append(" where 1 =1 ");
		
		if(holderAccountTO.getIdHolderAccountPk() != null){
			query.append(" and ha.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk ");
		}		

		Query consult = em.createNativeQuery(query.toString());
		
		if(holderAccountTO.getIdHolderAccountPk() != null){
			consult.setParameter("idHolderAccountPk", holderAccountTO.getIdHolderAccountPk());
		}
		
		List<Object[]> listObject = consult.getResultList();
		List<String[]> listDocumentNumber = new ArrayList<>();
		String[] record = null;
		
		for(Object[] object : listObject){
			record = new String[2];			
			record[0] = (String) object[0];
			record[1] = object[1] == null ? null : (String) object[1];
			
			listDocumentNumber.add(record);
		}
		
		return listDocumentNumber;
		
	}
	
	/**
	 * QUERY PARA EL WEBSERVICE DE CONSULTA DE CUI FIRMAS AUTORIZADAS
	 * 
	 * @param otcOperationTO the otc operation to
	 * @return the sirtex operations service bean
	 * @throws ServiceException the service exception
	 */
	public AuthorizedSignatureQueryResponseTO getListAuthorizedSignatures(AuthorizedSignatureQueryTO authorizedSignatureQueryTO) throws ServiceException {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" select                                                                                                                          ");
		querySql.append(" asi.INSTITUTION_TYPE                                                                                                            ");
		querySql.append(" ,asi.ID_INSTITUTION_FK                                                                                                          ");
		querySql.append(" ,(CASE WHEN asi.INSTITUTION_TYPE = 21 THEN P.description WHEN asi.INSTITUTION_TYPE = 22 THEN i.BUSINESS_NAME end) INSTITUCION   ");
		querySql.append(" ,(CASE WHEN asi.INSTITUTION_TYPE = 21 THEN P.MNEMONIC WHEN asi.INSTITUTION_TYPE = 22 THEN i.MNEMONIC end) MNEMONICO             ");
		querySql.append(" ,asi.ID_AUTHORIZED_SIGNATURE_PK                                                                                                 ");
		querySql.append(" ,(asi.FIRST_LAST_NAME||' '||asi.SECOND_LAST_NAME||', '||asi.NAME) nombre_completo                                               ");
		querySql.append(" ,asi.DOCUMENT_NUMBER                                                                                                            ");
		querySql.append(" ,(CASE WHEN asi.SIGNATURE_TYPE like 'IND' then 'INDIVIDUAL' WHEN asi.SIGNATURE_TYPE like 'CON' then 'CONJUNTA' end) tipo_firma  ");
		//querySql.append(" ,asi.SIGNATURE_IMG                                                                                                              ");
		querySql.append(" ,asi.SIGNATURE_EXTENSION                                                                                                        ");
		querySql.append(" from AUTHORIZED_SIGNATURE asi                                                                                                   ");
		querySql.append(" left join PARTICIPANT p on p.ID_PARTICIPANT_PK = (case when asi.INSTITUTION_TYPE = 21 then asi.ID_INSTITUTION_FK else null end) ");
		querySql.append(" left join ISSUER i on i.ID_ISSUER_PK = (case when asi.INSTITUTION_TYPE = 22 then asi.ID_INSTITUTION_FK else null end)           ");
		querySql.append(" where 1 = 1                                                                                                                     ");
		querySql.append(" and asi.state = :state and asi.situation=:situation                                                        					  ");
		
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getJuridicHolderObjectTO().getFullName())){
		querySql.append(" and ((asi.INSTITUTION_TYPE = 21 and p.DESCRIPTION like upper('%'||:institution||'%')) or                                        ");
		querySql.append("      (asi.INSTITUTION_TYPE = 22 and i.BUSINESS_NAME like upper('%'||:institution||'%')))                                        ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getJuridicHolderObjectTO().getMnemonic())){
		querySql.append(" and ((asi.INSTITUTION_TYPE = 21 and p.MNEMONIC like upper('%'||:mnemonic||'%')) or                                              ");
		querySql.append("      (asi.INSTITUTION_TYPE = 22 and i.MNEMONIC like upper('%'||:mnemonic||'%')))                                                ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getNaturalHolderObjectTO().getDocumentNumber())){
		querySql.append(" and asi.DOCUMENT_NUMBER like :document_number                                                                                   ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getNaturalHolderObjectTO().getFullName())){
		querySql.append(" and (TRANSLATE(REPLACE(asi.NAME,' ','')||REPLACE(asi.FIRST_LAST_NAME,' ','')||REPLACE(asi.SECOND_LAST_NAME,' ','')              ");
		querySql.append("      ,'\u00c1\u00c9\u00cd\u00d3\u00da\u00d1\u00e1\u00e9\u00ed\u00f3\u00fa\u00f1','AEIOUNaeioun')) IN (:fullName)                ");
		}
		querySql.append("	order by asi.FIRST_LAST_NAME,asi.SECOND_LAST_NAME,asi.NAME                                                                    ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("state", AuthorizeSignatureStateType.CONFIRMED.getCode());
		query.setParameter("situation", MasterTableStatusType.ACTIVE.getCode());
		
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getJuridicHolderObjectTO().getFullName())){
			query.setParameter("institution",authorizedSignatureQueryTO.getJuridicHolderObjectTO().getFullName());
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getJuridicHolderObjectTO().getMnemonic())){
			query.setParameter("mnemonic",authorizedSignatureQueryTO.getJuridicHolderObjectTO().getMnemonic());
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getNaturalHolderObjectTO().getDocumentNumber())){
			query.setParameter("document_number",authorizedSignatureQueryTO.getNaturalHolderObjectTO().getDocumentNumber());
		}
		if(Validations.validateIsNotNullAndNotEmpty(authorizedSignatureQueryTO.getNaturalHolderObjectTO().getFullName())){
	    	String temp = authorizedSignatureQueryTO.getNaturalHolderObjectTO().getFullName().toUpperCase();
			if(temp != null && temp.contains(" ")){
				List<String> lista = Permutations.getPermutationsWordsNoSpace(temp);
				query.setParameter("fullName", lista);
			} else {
				query.setParameter("fullName", "%" + temp + "%");
			}
			query.setMaxResults(100);				
		}
		
		List<Object[]> result = query.getResultList();
		AuthorizedSignatureQueryResponseTO authorizedSignatureQueryResponseTO = new AuthorizedSignatureQueryResponseTO();
		Map<String,JuridicHolderObjectTO> mapInstitution = new HashMap<String,JuridicHolderObjectTO>();
		JuridicHolderObjectTO juridicHolderObjectTO;
		AuthorizedSignatureTO authorizedSignatureTO;
		
		try {
			for(Object[] objectData: result){
				authorizedSignatureTO = new AuthorizedSignatureTO();
				authorizedSignatureTO.setIdAuthorizedSignaturePk(Long.parseLong(objectData[4].toString()));
				authorizedSignatureTO.setFullName((String)objectData[5]);
				authorizedSignatureTO.setDocumentNumber((String)objectData[6]);
				authorizedSignatureTO.setSignatureType((String)objectData[7]);
				byte[] img = authorizedSignatureServiceBean.find(AuthorizedSignature.class,authorizedSignatureTO.getIdAuthorizedSignaturePk()).getSignatureImg();
				String base64String = Base64.encodeBase64String(img);
				
				if(Validations.validateIsNotNullAndNotEmpty(img))
				authorizedSignatureTO.setSignatureImg(base64String);
				authorizedSignatureTO.setSignatureExtension((String)objectData[8]);
				
				if(mapInstitution.containsKey((String)objectData[1])){
					mapInstitution.get((String)objectData[1]).getLstAuthorizedSignature().add(authorizedSignatureTO);
				}else{
					juridicHolderObjectTO = new JuridicHolderObjectTO();
					juridicHolderObjectTO.setFullName((String)objectData[2]);
					juridicHolderObjectTO.setMnemonic((String)objectData[3]);
					juridicHolderObjectTO.setLstAuthorizedSignature(new ArrayList<AuthorizedSignatureTO>());
					juridicHolderObjectTO.getLstAuthorizedSignature().add(authorizedSignatureTO);
					mapInstitution.put((String)objectData[1], juridicHolderObjectTO);
				}
			}
			authorizedSignatureQueryResponseTO.setLstJuridicHolderObjectTOs(new ArrayList<JuridicHolderObjectTO>(mapInstitution.values()));
			return authorizedSignatureQueryResponseTO;
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	
	public InstitutionQueryResponseTO getListInstitution () throws ServiceException {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" select DISTINCT ");
		querySql.append(" (CASE WHEN asi.INSTITUTION_TYPE = 21 THEN P.MNEMONIC WHEN asi.INSTITUTION_TYPE = 22 THEN i.MNEMONIC end) MNEMONICO ");
		querySql.append(" ,(CASE WHEN asi.INSTITUTION_TYPE = 21 THEN P.description WHEN asi.INSTITUTION_TYPE = 22 THEN i.BUSINESS_NAME end) BUSINESS_NAME ");
		querySql.append(" from AUTHORIZED_SIGNATURE ASI ");
		querySql.append(" left join PARTICIPANT p on p.ID_PARTICIPANT_PK = (case when asi.INSTITUTION_TYPE = 21 then asi.ID_INSTITUTION_FK else null end) ");
		querySql.append(" left join ISSUER i on i.ID_ISSUER_PK = (case when asi.INSTITUTION_TYPE = 22 then asi.ID_INSTITUTION_FK else null end) ");
		querySql.append(" where 1 = 1                                                                                                                     ");
		querySql.append(" and asi.state = :state and asi.situation=:situation                                                        					  ");
		querySql.append(" ORDER BY 1 ASC ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("state", AuthorizeSignatureStateType.CONFIRMED.getCode());
		query.setParameter("situation", MasterTableStatusType.ACTIVE.getCode());
		
		List<Object[]> result = query.getResultList();
		InstitutionQueryResponseTO institutionQueryResponseTO = new InstitutionQueryResponseTO();
		List<JuridicHolderObjectTO> listJuridicHolder = new ArrayList<JuridicHolderObjectTO>();
		JuridicHolderObjectTO juridicHolderObjectTO;
		
		try {
			for(Object[] objectData: result){
				juridicHolderObjectTO = new JuridicHolderObjectTO();
				juridicHolderObjectTO.setMnemonic((String)objectData[0]);
				juridicHolderObjectTO.setFullName((String)objectData[1]);
				listJuridicHolder.add(juridicHolderObjectTO);
			}
			institutionQueryResponseTO.setLstJuridicHolderObjectTOs(listJuridicHolder);
			return institutionQueryResponseTO;
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	public PhysicalCertificateResponseTO consultPhysicalCertificate(PhysicalCertificateQueryTO physicalCertificateQueryTO) throws ServiceException {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT                                                                                                         ");
		querySql.append(" distinct (co.OPERATION_NUMBER),                                                                                ");
		querySql.append(" TO_CHAR(pc.CERTIFICATE_NUMBER),                                                                                ");
		querySql.append(" i.MNEMONIC,                                                                                                    ");
		querySql.append(" TO_CHAR(pod.REGISTRY_DATE, 'DD/MM/YYYY HH:MI:SS') as REGISTRY_DATE,                                            ");
		querySql.append(" (select TEXT1 from PARAMETER_TABLE where PARAMETER_TABLE_PK = pc.SECURITY_CLASS) SECURITY_CLASS_DESC,          ");
		querySql.append(" pc.ID_SECURITY_CODE_FK,                                                                                        ");
		querySql.append(" pc.NOMINAL_VALUE,                                                                                              ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = pc.CURRENCY) as CURRENCY_DESC,             ");
		querySql.append(" p.MNEMONIC || ' - ' ||p.ID_PARTICIPANT_PK|| ' - ' ||p.DESCRIPTION as participant_DESC,                         ");
		querySql.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||CHR(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                    ");
		querySql.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                ");
		querySql.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = pco.ID_HOLDER_ACCOUNT_FK) HOLDERS,                                           ");
		querySql.append(" TO_CHAR(pc.ISSUE_DATE, 'DD/MM/YYYY') as ISSUE_DATE,                                                            ");
		querySql.append(" TO_CHAR(pc.EXPIRATION_DATE, 'DD/MM/YYYY') as EXPIRATION_DATE,                                                  ");
		querySql.append(" TO_CHAR(co.LAST_MODIFY_DATE, 'DD/MM/YYYY HH:MI') as CONFIRMATION_DATE,                						 ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = pc.STATE) as STATE_DESC,                   ");
		querySql.append(" TO_CHAR(pod.IND_APPLIED),                                                                                      ");
		querySql.append(" 'FISICO' AS TIPO                                                                                       		 ");
		querySql.append(" FROM PHYSICAL_CERTIFICATE pc                                                                                   ");
		querySql.append(" INNER JOIN PHYSICAL_OPERATION_DETAIL pod on pod.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK     ");
		querySql.append(" INNER JOIN PHYSICAL_CERTIFICATE_OPERATION pco on pco.ID_PHYSICAL_OPERATION_PK = pod.ID_PHYSICAL_OPERATION_FK   ");
		querySql.append(" INNER JOIN CUSTODY_OPERATION co on co.ID_CUSTODY_OPERATION_PK = pco.ID_PHYSICAL_OPERATION_PK                   ");
		querySql.append(" INNER JOIN SECURITY s on s.ID_SECURITY_CODE_PK = pc.ID_SECURITY_CODE_FK                                        ");
		querySql.append(" INNER JOIN ISSUER i  ON i.ID_ISSUER_PK = s.ID_ISSUER_FK                                                        ");
		querySql.append(" INNER JOIN PARTICIPANT p on p.ID_PARTICIPANT_PK = pco.ID_PARTICIPANT_FK                                        ");
		querySql.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had on had.ID_HOLDER_ACCOUNT_FK = pco.ID_HOLDER_ACCOUNT_FK                    ");
		querySql.append(" where 1=1                                                                                                      ");
		querySql.append(" and pod.IND_APPLIED = 1                                                                                        ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getParticipantMnemonic()))
		querySql.append(" and p.MNEMONIC like upper(:participantMnemonic)                                                            ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIdHolderPk()))
		querySql.append(" and had.ID_HOLDER_FK = :idHolderPk                                                                         ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIssuerMnemonic()))
		querySql.append(" and i.MNEMONIC like upper(:issuerMnemonic)                                                                 ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getOperationNumberCertificate()))
		querySql.append(" and co.OPERATION_NUMBER = :operationNumber                                                                 ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getCertificateNumber()))
		querySql.append(" and pc.CERTIFICATE_NUMBER = :certificateNumber                                                             ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIdSecurityCodePk()))
		querySql.append(" and pc.ID_SECURITY_CODE_FK like :idSecurityCodePk                                                          ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getTipo()) && physicalCertificateQueryTO.getTipo().equalsIgnoreCase("DESMA"))
		querySql.append(" and pc.ID_SECURITY_CODE_FK is null                                                             			 ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getFinalDate()))
		querySql.append(" and trunc(pod.REGISTRY_DATE) between trunc(:initialDate) and trunc(:finalDate)                                ");
		querySql.append("                                                                                                                ");
		querySql.append(" UNION                                                                                                          ");
		querySql.append("                                                                                                                ");
		querySql.append(" SELECT                                                                                                         ");
		querySql.append(" distinct (co.OPERATION_NUMBER),                                                                                ");
		querySql.append(" TO_CHAR(AAO.certificate_number),                                                                               ");
		querySql.append(" i.MNEMONIC,                                                                                                    ");
		querySql.append(" TO_CHAR(co.REGISTRY_DATE, 'DD/MM/YYYY HH:MI') as REGISTRY_DATE,                                           	 ");
		querySql.append(" (select TEXT1 from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.SECURITY_CLASS) as SECURITY_CLASS_DESC,        ");
		querySql.append(" s.ID_SECURITY_CODE_pK,                                                                                         ");
		querySql.append(" s.CURRENT_NOMINAL_VALUE,                                                                                       ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.CURRENCY) as CURRENCY_DESC,              ");
		querySql.append(" p.MNEMONIC || ' - ' ||p.ID_PARTICIPANT_PK|| ' - ' ||p.DESCRIPTION as participant_DESC,                         ");
		querySql.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||CHR(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                    ");
		querySql.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                                ");
		querySql.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_pK) HOLDERS,                                            ");
		querySql.append(" TO_CHAR(s.issuance_date, 'DD/MM/YYYY') as ISSUE_DATE,                                                          ");
		querySql.append(" TO_CHAR(s.EXPIRATION_DATE, 'DD/MM/YYYY') as EXPIRATION_DATE,                                                   ");
		querySql.append(" TO_CHAR(co.LAST_MODIFY_DATE, 'DD/MM/YYYY HH:MI') as CONFIRMATION_DATE,                                         ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = co.operation_state) as STATE_DESC,         ");
		querySql.append(" '' as IND_APPLIED,                                                                                             ");
		querySql.append(" 'DESMA' as TIPO                                                                                                ");
		querySql.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO                                                                          ");
		querySql.append(" INNER JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK                 ");
		querySql.append(" INNER JOIN SECURITY s on s.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK                                       ");
		querySql.append(" INNER JOIN ISSUER i  ON i.ID_ISSUER_PK = s.ID_ISSUER_FK                                                        ");
		querySql.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK                             ");
		querySql.append(" INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK                                         ");
		querySql.append(" WHERE AAO.MOTIVE = 2357                                                                                        ");
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getParticipantMnemonic()))
		querySql.append(" and p.MNEMONIC like upper(:participantMnemonic)                                                               ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIdHolderPk())){
		querySql.append(" and((select count(*)                                                                                          ");
		querySql.append(" 	   from HOLDER_ACCOUNT_DETAIL had                                                                           ");
		querySql.append(" 	   where had.id_holder_account_fk=ha.id_holder_account_pk and had.ID_HOLDER_FK = :idHolderPk)>0)            ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIssuerMnemonic()))
		querySql.append(" and i.MNEMONIC like upper(:issuerMnemonic)                                                                    ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getOperationNumberCertificate()))
		querySql.append(" and co.OPERATION_NUMBER = :operationNumber                                                                    ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getCertificateNumber()))
		querySql.append(" and aao.CERTIFICATE_NUMBER = :certificateNumber                                                               ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIdSecurityCodePk()))
		querySql.append(" and s.ID_SECURITY_CODE_pK like :idSecurityCodePk                                                                ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getTipo()) && physicalCertificateQueryTO.getTipo().equalsIgnoreCase("FISICO"))
		querySql.append(" and s.ID_SECURITY_CODE_pK is null                                                             			 	 ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getInitialDate()) && Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getFinalDate()))
		querySql.append(" and trunc(co.REGISTRY_DATE) between trunc(:initialDate) and trunc(:finalDate)                ");
		querySql.append(" and co.operation_state = 858                                                                                   ");
		querySql.append(" order by 1 desc                                                                                                ");
		
		Query query = em.createNativeQuery(querySql.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getParticipantMnemonic()))
			query.setParameter("participantMnemonic", physicalCertificateQueryTO.getParticipantMnemonic());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIdHolderPk()))
			query.setParameter("idHolderPk", physicalCertificateQueryTO.getIdHolderPk());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIssuerMnemonic()))
			query.setParameter("issuerMnemonic", physicalCertificateQueryTO.getIssuerMnemonic());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getOperationNumberCertificate()))
			query.setParameter("operationNumber", physicalCertificateQueryTO.getOperationNumberCertificate());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getCertificateNumber()))
			query.setParameter("certificateNumber", physicalCertificateQueryTO.getCertificateNumber());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", physicalCertificateQueryTO.getIdSecurityCodePk());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getInitialDate()))
			query.setParameter("initialDate", physicalCertificateQueryTO.getInitialDate());
		
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateQueryTO.getFinalDate()))
			query.setParameter("finalDate", physicalCertificateQueryTO.getFinalDate());
		
		List<Object[]> result = query.getResultList();
		
		//Objeto de Rspuesta
		PhysicalCertificateTO responseTO;
		PhysicalCertificateResponseTO physicalCertificateResponseTO = new PhysicalCertificateResponseTO();
		List<PhysicalCertificateTO> listResponseTO = new ArrayList<PhysicalCertificateTO>();
		
		try {
			//recorriendo la lista de respuesta
			for(Object[] objectData: result){
				responseTO = new PhysicalCertificateTO();
				responseTO.setOperationNumberCertificate(Long.parseLong(objectData[0].toString()));
				if(Validations.validateIsNotNullAndNotEmpty(objectData[1]))
				responseTO.setCertificateNumber(Long.parseLong(objectData[1].toString()));
				responseTO.setIssuerMnemonic((String) objectData[2]);
				responseTO.setRegistryDate((String) objectData[3]);
				responseTO.setSecurityClassDesc((String) objectData[4]);
				responseTO.setIdSecurityCodePk((String) objectData[5]);
				responseTO.setNominalValue((BigDecimal) objectData[6]);
				responseTO.setCurrencyDesc((String) objectData[7]);
				responseTO.setParticipantDesc((String) objectData[8]);
				responseTO.setIdHolderPk((String) objectData[9]);
				responseTO.setIssueDate((String) objectData[10]);
				responseTO.setExpirationDate((String) objectData[11]);
				responseTO.setConfDate((String) objectData[12]);
				responseTO.setStateDesc((String) objectData[13]);
				if(objectData[14] != null){
					responseTO.setIndApplied(Integer.parseInt(objectData[14].toString()));
				}
				responseTO.setTipo((String) objectData[15]);
				listResponseTO.add(responseTO);
			}
			
			physicalCertificateResponseTO.setListPhysicarCertificate(listResponseTO);
			return physicalCertificateResponseTO;
			
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	/**
	 * Gets xmlSolicitudActivoFinanciero
	 * @param listPhysicalCertificates
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public XmlSolicitudActivoFinanciero getSolicitudDepositoDesmaterializacion(AccountAnnotationOperation[] accountAnnotationOperations, LoggerUser loggerUser) throws ServiceException{
		XmlSolicitudActivoFinanciero solicitudActivoFinanciero = new XmlSolicitudActivoFinanciero();
		List<XmlActivoFinanciero> xmlActivoFinancieros = new ArrayList<>();
		XmlActivoFinanciero xmlActivoFinanciero;
		
		for(AccountAnnotationOperation aao: accountAnnotationOperations){
			xmlActivoFinanciero = getDematerializeCertificate(aao.getIdAnnotationOperationPk());
			xmlActivoFinancieros.add(xmlActivoFinanciero);
		}
		
		solicitudActivoFinanciero.setTipoSolicitud("DEP");
		solicitudActivoFinanciero.setMotivo(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getValue());
		solicitudActivoFinanciero.setUsuarioSolicitante(loggerUser.getUserName());
		solicitudActivoFinanciero.setIpAlta(loggerUser.getIpAddress());
		solicitudActivoFinanciero.setListActivoFinanciero(xmlActivoFinancieros);
		
		return solicitudActivoFinanciero;
	}
	
	/**
	 * Gets data physical certificate by ids
	 */
	public XmlActivoFinanciero getDematerializeCertificate(long annotationOperationPk) throws ServiceException {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT                                                                                                   ");
		querySql.append(" distinct (co.OPERATION_NUMBER),                                                                          ");
		querySql.append(" AAO.certificate_number,                                                                                  ");
		querySql.append(" i.MNEMONIC,                                                                                              ");
		querySql.append(" TO_CHAR(co.REGISTRY_DATE, 'DD/MM/YYYY HH:MI') as REGISTRY_DATE,                                          ");
		querySql.append(" (select TEXT1 from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.SECURITY_CLASS) as SECURITY_CLASS_DESC,  ");
		querySql.append(" s.ID_SECURITY_CODE_pK,                                                                                   ");
		querySql.append(" s.CURRENT_NOMINAL_VALUE,                                                                                 ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.CURRENCY) as CURRENCY_DESC,        ");
		querySql.append(" p.MNEMONIC || ' - ' ||p.ID_PARTICIPANT_PK|| ' - ' ||p.DESCRIPTION as participant_DESC,                   ");
		querySql.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||CHR(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)              ");
		querySql.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                          ");
		querySql.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_pK) HOLDERS,                                      ");
		querySql.append(" TO_CHAR(s.issuance_date, 'DD/MM/YYYY') as ISSUE_DATE,                                                    ");
		querySql.append(" TO_CHAR(s.EXPIRATION_DATE, 'DD/MM/YYYY') as EXPIRATION_DATE,                                             ");
		querySql.append(" TO_CHAR(co.LAST_MODIFY_DATE, 'DD/MM/YYYY HH:MI') as CONFIRMATION_DATE,                                   ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = co.operation_state) as STATE_DESC,   ");
		querySql.append(" '' as IND_APPLIED,                                                                                       ");
		querySql.append(" 'DESMA' as TIPO                                                                                     	   ");
		querySql.append(" FROM ACCOUNT_ANNOTATION_OPERATION AAO                                                                    ");
		querySql.append(" INNER JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK = AAO.ID_ANNOTATION_OPERATION_PK           ");
		querySql.append(" INNER JOIN SECURITY s on s.ID_SECURITY_CODE_PK = AAO.ID_SECURITY_CODE_FK                                 ");
		querySql.append(" INNER JOIN ISSUER i  ON i.ID_ISSUER_PK = s.ID_ISSUER_FK                                                  ");
		querySql.append(" INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = AAO.ID_HOLDER_ACCOUNT_FK                       ");
		querySql.append(" INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HA.ID_PARTICIPANT_FK                                   ");
		querySql.append(" and AAO.ID_ANNOTATION_OPERATION_PK = :id_annotation_operation                                            ");
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("id_annotation_operation", annotationOperationPk);
		Object[] result = (Object[]) query.getSingleResult();
		
		XmlActivoFinanciero xmlActivoFinanciero = new XmlActivoFinanciero();
		xmlActivoFinanciero.setNroSolicitud(Long.parseLong(result[0].toString()));
		if(Validations.validateIsNotNullAndNotEmpty(result[1]))
		xmlActivoFinanciero.setNroTitulo(Long.parseLong(result[1].toString()));
		xmlActivoFinanciero.setEmisor((String)result[2]);
		xmlActivoFinanciero.setFechaSolicitud((String)result[3]);
		xmlActivoFinanciero.setClaseValor((String)result[4]);
		xmlActivoFinanciero.setClaseClaveValor((String)result[5]);
		xmlActivoFinanciero.setValorNominal((BigDecimal)result[6]);
		xmlActivoFinanciero.setMoneda((String)result[7]);
		xmlActivoFinanciero.setParticipante((String)result[8]);
		xmlActivoFinanciero.setCui((String)result[9]);
		xmlActivoFinanciero.setFechaEmision((String)result[10]);
		xmlActivoFinanciero.setFechaVencimiento((String)result[11]);
		xmlActivoFinanciero.setFechaConfirmacion((String)result[12]);
		xmlActivoFinanciero.setEstado((String)result[13]);
		xmlActivoFinanciero.setTipo((String)result[15]);
		
		return xmlActivoFinanciero;
			
	}
	
	/**
	 * The method is used to changing state of account annotation operation
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param objAccountAnnotationOperation the obj account annotation operation
	 */
	@SuppressWarnings("unchecked")
	public void changingSatateAccountAnnotationOperation(AccountAnnotationOperation objAccountAnnotationOperation) {
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(objAccountAnnotationOperation)){
			sbQuery.append(" update AccountAnnotationOperation ");	
			sbQuery.append(" set state = :parState ");
			sbQuery.append(" where idAnnotationOperationPk = :parIdAnnotationOperation ");
			Query query=em.createQuery( sbQuery.toString() );
			query.setParameter("parState", DematerializationStateType.REVERSED.getCode());
			query.setParameter("parIdAnnotationOperation", objAccountAnnotationOperation.getIdAnnotationOperationPk());
			query.executeUpdate();		
		}
	}
	
	/**
	 * Geographic dpt provi municipality.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param departament the departament
	 * @param province the province
	 * @param municipality the municipality
	 * @return true, if successful
	 */
	@SuppressWarnings("unchecked")
	public boolean geographicDptProviMunicipalityNew(Integer departament, Integer province, Integer municipality){
		boolean exist=false;
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT gl0 FROM GeographicLocation gl0 ");
		sbQuery.append(" inner join gl0.geographicLocation gl1 ");
		sbQuery.append(" inner join gl1.geographicLocation gl2 ");
		
		sbQuery.append(" WHERE gl1.geographicLocation.idGeographicLocationPk = :departament");
		//sbQuery.append(" AND gl1.idGeographicLocationPk = :province");
		sbQuery.append(" AND gl2.geographicLocation.idGeographicLocationPk = :country");
		if (municipality!=null){
			sbQuery.append(" AND gl0.idGeographicLocationPk = :municipality");
		}
		
		Query query = em.createQuery(sbQuery.toString());
		if (municipality!=null){
			query.setParameter("municipality", municipality);
		}
		query.setParameter("departament", departament);
		//query.setParameter("province", province);
		query.setParameter("country", countryResidence);
		
		List<GeographicLocation> listGeographicLocation = query.getResultList();
		if (listGeographicLocation.size()>0){
			exist=true;
		}
		return exist;
	}
	
	/**
	 * The method is used to creating holder account movement for reversal placement
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param reverseRegisterTO
	 * @param loggerUser
	 */
	public void creatingHolderAccountMovementReversal(Security security,AccountAnnotationOperation accountAnnotationOperation
			,HolderAccountBalance objHolderAccountBalance,AccountAnnotationMarketFact accountAnnotationMarketFact) throws ServiceException{
		//Movement type salida de reversion de valores
		MovementType movementTypeAux = find(MovementType.class,new Long(200066));
		
		//Movement to be inserted
		HolderAccountMovement objHolderAccountMovement = new HolderAccountMovement();
		objHolderAccountMovement.setHolderAccountBalance(objHolderAccountBalance);
		objHolderAccountMovement.setMovementDate(CommonsUtilities.currentDateTime());
		objHolderAccountMovement.setMovementQuantity(accountAnnotationMarketFact.getOperationQuantity());
		objHolderAccountMovement.setMovementType(movementTypeAux);
		objHolderAccountMovement.setCustodyOperation(accountAnnotationOperation.getCustodyOperation());
		objHolderAccountMovement.setOperationPrice(BigDecimal.ZERO); //default value
		objHolderAccountMovement.setNominalValue(security.getInitialNominalValue());
		objHolderAccountMovement.setOperationDate(CommonsUtilities.currentDateTime());
		objHolderAccountMovement.setOperationNumber(getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_DEMATERIALIZATION));
		objHolderAccountMovement.setIdSourceParticipant(objHolderAccountBalance.getParticipant().getIdParticipantPk());
		objHolderAccountMovement.setIdTargetParticipant(objHolderAccountBalance.getParticipant().getIdParticipantPk());
		this.create(objHolderAccountMovement);
		
		//Insert HolderMarketfactMovemen
		HolderMarketFactMovement objHolderMarketFactMovement = new HolderMarketFactMovement();
		objHolderMarketFactMovement.setHolderAccount(accountAnnotationOperation.getHolderAccount());
		objHolderMarketFactMovement.setMarketDate(accountAnnotationMarketFact.getMarketDate());
		objHolderMarketFactMovement.setMarketPrice(accountAnnotationMarketFact.getMarketPrice());
		objHolderMarketFactMovement.setMarketRate(accountAnnotationMarketFact.getMarketRate());
		objHolderMarketFactMovement.setMovementQuantity(objHolderAccountMovement.getMovementQuantity());
		objHolderMarketFactMovement.setMovementType(movementTypeAux);
		objHolderMarketFactMovement.setParticipant(objHolderAccountBalance.getParticipant());
		objHolderMarketFactMovement.setSecurities(accountAnnotationOperation.getSecurity());
		if(objHolderAccountMovement!=null){
			objHolderMarketFactMovement.setHolderAccountMovement(objHolderAccountMovement);
			objHolderMarketFactMovement.setMovementDate(objHolderAccountMovement.getMovementDate());
		}else{
			objHolderMarketFactMovement.setMovementDate(CommonsUtilities.currentDateTime());
		}
		this.create(objHolderMarketFactMovement);
		
		//Actualizando estado de custodyOperation a anulado
		if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation.getCustodyOperation())){
			StringBuilder sbQuery=new StringBuilder();		
			sbQuery.append(" update CustodyOperation set ");
			sbQuery.append(" state = :parState ");
			sbQuery.append(" where 1=1 ");
			sbQuery.append(" and idCustodyOperationPk = :parIdCustodyOperation ");	
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdCustodyOperation", accountAnnotationOperation.getCustodyOperation().getIdCustodyOperationPk());
			query.setParameter("parState", DematerializationStateType.REVERSED.getCode());
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to subtracting amount total balance holder account balance.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param objHolderAccountBalance HolderAccountBalance
	 * @throws ServiceException the service exception
	 */
	public void subtractingAmountTotalBalanceHolderAccountBalance(HolderAccountBalance objHolderAccountBalance,AccountAnnotationMarketFact objAccAnnMarketFact) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getHolderAccount())
				&& Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getSecurity())
				&& Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getParticipant())
				&& Validations.validateIsNotNullAndNotEmpty(objAccAnnMarketFact)){
			sbQuery.append(" update HolderAccountBalance set ");
			sbQuery.append(" totalBalance = (totalBalance - :parTotalBalanceMF), ");
			sbQuery.append(" availableBalance = (availableBalance - :parAvailableBalanceMF) ");
			sbQuery.append(" where 1=1 ");
			sbQuery.append(" and holderAccount.idHolderAccountPk = :parIdHolderAccount ");
			sbQuery.append(" and participant.idParticipantPk = :parIdParticipant   ");	
			sbQuery.append(" and security.idSecurityCodePk = :parIdSecurityCode  ");	
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdHolderAccount", objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
			query.setParameter("parIdParticipant", objHolderAccountBalance.getParticipant().getIdParticipantPk());
			query.setParameter("parIdSecurityCode", objHolderAccountBalance.getSecurity().getIdSecurityCodePk());
			query.setParameter("parTotalBalanceMF", objAccAnnMarketFact.getOperationQuantity());
			query.setParameter("parAvailableBalanceMF", objAccAnnMarketFact.getOperationQuantity());
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to change status and update holder marketfact balance.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param idMarketfactBalancePk
	 */
	@SuppressWarnings("unchecked")
	public void changeStatusAndUpdateHolMarketFactBalance(HolderMarketFactBalance marketfactBalance
			, AccountAnnotationOperation accountAnnotationOperation) {
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(marketfactBalance) && Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation)){
			sbQuery.append(" update HolderMarketFactBalance");
			sbQuery.append(" set ");
			sbQuery.append(" totalBalance = (totalBalance - :parCantVal)");
			sbQuery.append(" ,availableBalance = (availableBalance -:parCantVal)");
			sbQuery.append(" where IdMarketfactBalancePk = :idMarketfactBalancePk");
			Query query=em.createQuery( sbQuery.toString() );
			query.setParameter("idMarketfactBalancePk", marketfactBalance.getIdMarketfactBalancePk());
			query.setParameter("parCantVal", accountAnnotationOperation.getTotalBalance());
			query.executeUpdate();	
		}
	}

	public Security findSecurity(AccountAnnotationOperation accountAnnotationOperation){
		try {
			Security securityResult = null;
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT se FROM Security se");
			sbQuery.append("	WHERE se.issuer.idIssuerPk = :idIssuerPk");
			sbQuery.append("	AND se.securityClass = :securityClass");
			sbQuery.append("	AND se.issuanceDate = :issuanceDate");
			sbQuery.append("	AND se.expirationDate = :expirationDate");
			//sbQuery.append("	AND se.yieldRate = :yieldRate");
			sbQuery.append("	AND se.currency = :currency");
			sbQuery.append("	AND se.interestType = :interestType");
			sbQuery.append("	AND se.interestRate = :interestRate");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idIssuerPk", accountAnnotationOperation.getIssuer().getIdIssuerPk());
			query.setParameter("securityClass", accountAnnotationOperation.getSecurityClass());
			query.setParameter("issuanceDate", accountAnnotationOperation.getExpeditionDate());
			query.setParameter("expirationDate", accountAnnotationOperation.getSecurityExpirationDate());
			//query.setParameter("yieldRate", accountAnnotationOperation.getSecurityYieldRate());
			query.setParameter("currency", accountAnnotationOperation.getSecurityCurrency());
			query.setParameter("interestType", accountAnnotationOperation.getSecurityInterestType());
			query.setParameter("interestRate", accountAnnotationOperation.getSecurityInterestRate());
			
			if(query.getResultList() !=null && query.getResultList().size()>0 ){
				securityResult = (Security) query.getResultList().get(0);
			}
			return securityResult;
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public List<SecuritySerialRange> findSecuritySerialRange(String idSecurityCodePk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" SELECT s ");
		sd.append(" FROM SecuritySerialRange s ");
		sd.append(" WHERE s.security.idSecurityCodePk = :idSecurityCodePk AND s.stateRange = :stateRange ");
		
		Query q = em.createQuery(sd.toString());
		q.setParameter("idSecurityCodePk", idSecurityCodePk);
		q.setParameter("stateRange", 1);
		
		return q.getResultList();
	}

	public PayrollHeader rejectAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation)throws ServiceException{
		
		accountAnnotationOperation.setState(DematerializationStateType.REJECTED.getCode());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REJECTED.getCode());
		
		PayrollHeader payrollHeader = getPayrollHeaderAndDetailEnabled(accountAnnotationOperation.getIdAnnotationOperationPk());
		if(payrollHeader != null && payrollHeader.getLstPayrollDetail()!=null && payrollHeader.getLstPayrollDetail().size()>0) {
			for(PayrollDetail pd: payrollHeader.getLstPayrollDetail()) {
				if( pd.getAccountAnnotationOperation().getIdAnnotationOperationPk().equals(accountAnnotationOperation.getIdAnnotationOperationPk()) ){
					pd.setPayrollState(PayrollDetailStateType.REJECT.getCode());
					update(pd);
				}
			}
		}
		
		return payrollHeader;
	}
	
	public void rejectPayrollWithAllRejected(PayrollHeader payrollHeader) throws ServiceException {

		List<PayrollDetail> lstPayrollDetails = getAllPayrollDetail(payrollHeader.getIdPayrollHeaderPk());
		Boolean allPayrollDetailRejectOrAnull = true;
		
		for(PayrollDetail pd: lstPayrollDetails) {
			if( !(pd.getPayrollState().equals(PayrollDetailStateType.REJECT.getCode()) || 
				  pd.getPayrollState().equals(PayrollDetailStateType.ANNULATE.getCode()) || 
				  pd.getPayrollState().equals(PayrollDetailStateType.VAULT.getCode()) )  
			) {
				allPayrollDetailRejectOrAnull = false;
			}
		}
		
		if(allPayrollDetailRejectOrAnull) {
			payrollHeader.setPayrollState(PayrollHeaderStateType.FINISHED.getCode());
			update(payrollHeader);
		}
		
	}
	
	public PayrollHeader getPayrollHeaderAndDetail(Long idAnnotationOperationPk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select ph ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" join fetch ph.lstPayrollDetail pd ");
		sd.append(" where pd.accountAnnotationOperation.idAnnotationOperationPk = :idAnnotationOperationPk ");
		try {
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
			lst = q.getResultList();
			if (!lst.isEmpty())
				return lst.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
	public PayrollDetail getPayrollDetailOnliCapitalValid(Long idAnnotationOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT pd ");
			sbQuery.append("	FROM PayrollDetail pd ");
			sbQuery.append("	WHERE pd.accountAnnotationOperation.idAnnotationOperationPk = :idAnnotationOperationPk AND pd.indCupon = :indCupon AND pd.payrollState not in (:lstNotIn) ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
			query.setParameter("indCupon", 0);
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollDetailStateType.REJECT.getCode());
			lstNotIn.add(PayrollDetailStateType.ANNULATE.getCode());
			query.setParameter("lstNotIn", lstNotIn);
			
			PayrollDetail lst = (PayrollDetail)query.getSingleResult();
			return lst;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public PayrollHeader getPayrollHeaderAndDetailEnabled(List<Long> lstIdAnnotationOperationPk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select ph ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" join fetch ph.lstPayrollDetail pd ");
		sd.append(" WHERE pd.accountAnnotationOperation.idAnnotationOperationPk in (:idAnnotationOperationPk) ");
		sd.append(" AND pd.accountAnnotationOperation.state not in (:stateNotIn) ");
		
		try {
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			lstNotIn.add(PayrollHeaderStateType.FINISHED.getCode());
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idAnnotationOperationPk", lstIdAnnotationOperationPk);
			q.setParameter("stateNotIn", lstNotIn);
			lst = q.getResultList();
			if (!lst.isEmpty())
				return lst.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
public PayrollHeader getPayrollHeaderWithAnnotationOperation(Long idAnnotationOperationPk) {
		
		StringBuilder sd = new StringBuilder();
		sd.append(" select distinct ph.idPayrollHeaderPk, ph.payrollType, ph.payrollState   ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" inner join ph.lstPayrollDetail pd ");
		sd.append(" WHERE pd.accountAnnotationOperation.idAnnotationOperationPk = :idAnnotationOperationPk ");
		sd.append(" AND ph.payrollState not in (:stateNotIn) ");
		
		try {
			
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstNotIn.add(PayrollHeaderStateType.PROCESSED.getCode());
			lstNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			lstNotIn.add(PayrollHeaderStateType.FINISHED.getCode());
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
			q.setParameter("stateNotIn", lstNotIn);
			
			List<PayrollHeader> lstPayrollHead = new ArrayList<PayrollHeader>();
			PayrollHeader payrollHeader;
			List<Object> objectList = q.getResultList();
			
			if(objectList!=null && objectList.size()>0) {
				for (int i = 0; i < objectList.size(); i++) {
					Object[] obj =  (Object[]) objectList.get(i);
					payrollHeader = new PayrollHeader();
					payrollHeader.setIdPayrollHeaderPk((Long)obj[0]);
					payrollHeader.setPayrollType((Integer)obj[1]);
					payrollHeader.setPayrollState((Integer)obj[2]);
					lstPayrollHead.add(payrollHeader);
				}
			}
			
			if(lstPayrollHead!=null && lstPayrollHead.size()>0) {
				return lstPayrollHead.get(0);
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
		
	}

	public PayrollHeader getPayrollHeaderWithRetirementOperation(Long idRetirementOperationPk) {
		
		StringBuilder sd = new StringBuilder();
		sd.append(" select distinct ph.idPayrollHeaderPk, ph.payrollType, ph.payrollState   ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" inner join ph.lstPayrollDetail pd ");
		sd.append(" WHERE pd.retirementOperation.idRetirementOperationPk = :idRetirementOperationPk ");
		sd.append(" AND ph.payrollState not in (:stateNotIn) ");
		
		try {
			
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstNotIn.add(PayrollHeaderStateType.PROCESSED.getCode());
			lstNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			lstNotIn.add(PayrollHeaderStateType.FINISHED.getCode());
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			q.setParameter("stateNotIn", lstNotIn);
			
			List<PayrollHeader> lstPayrollHead = new ArrayList<PayrollHeader>();
			PayrollHeader payrollHeader;
			List<Object> objectList = q.getResultList();
			
			if(objectList!=null && objectList.size()>0) {
				for (int i = 0; i < objectList.size(); i++) {
					Object[] obj =  (Object[]) objectList.get(i);
					payrollHeader = new PayrollHeader();
					payrollHeader.setIdPayrollHeaderPk((Long)obj[0]);
					payrollHeader.setPayrollType((Integer)obj[1]);
					payrollHeader.setPayrollState((Integer)obj[2]);
					lstPayrollHead.add(payrollHeader);
				}
			}
			
			if(lstPayrollHead!=null && lstPayrollHead.size()>0) {
				return lstPayrollHead.get(0);
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
		
	}
	
	public PayrollHeader getPayrollHeaderAndDetailEnabled(Long idAnnotationOperationPk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select ph ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" join fetch ph.lstPayrollDetail pd ");
		sd.append(" WHERE pd.accountAnnotationOperation.idAnnotationOperationPk = :idAnnotationOperationPk ");
		sd.append(" AND ph.payrollState not in (:lstPayrollStateNotIn) ");
		sd.append(" AND pd.payrollState not in (:lstPayrollDetailStateNotIn) ");
		
		try {
			List<Integer> lstPayrollStateNotIn = new ArrayList<Integer>();
			lstPayrollStateNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstPayrollStateNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			lstPayrollStateNotIn.add(PayrollHeaderStateType.FINISHED.getCode());

			List<Integer> lstPayrollDetailStateNotIn = new ArrayList<Integer>();
			lstPayrollDetailStateNotIn.add(PayrollDetailStateType.ANNULATE.getCode());
			lstPayrollDetailStateNotIn.add(PayrollDetailStateType.REJECT.getCode());
			lstPayrollDetailStateNotIn.add(PayrollDetailStateType.VAULT.getCode());
			
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idAnnotationOperationPk", idAnnotationOperationPk);
			q.setParameter("lstPayrollStateNotIn", lstPayrollStateNotIn);
			q.setParameter("lstPayrollDetailStateNotIn", lstPayrollDetailStateNotIn);
			
			lst = q.getResultList();
			if (!lst.isEmpty())
				return lst.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
	public List<PayrollDetail> getAllPayrollDetail(Long idPayrollHeaderPk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select prd ");
		sd.append(" from PayrollDetail prd ");
		sd.append(" where prd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk ");
		try {
			
			TypedQuery<PayrollDetail> q = em.createQuery(sd.toString(), PayrollDetail.class);
			q.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
			return q.getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
	public void annulateCustodyOperation(CustodyOperation custodyOperation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update CustodyOperation ");
		sbQuery.append(" set state = :state ");
		sbQuery.append(" , rejectMotive = :rejectMotive ");
		sbQuery.append(" , rejectMotiveOther = :rejectMotiveOther ");
		sbQuery.append(" , annulationDate = :annulationDate ");
		sbQuery.append(" , annulationUser = :annulationUser ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" where idCustodyOperationPk = :idCustodyOperationPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("state", custodyOperation.getState());
		query.setParameter("rejectMotive", custodyOperation.getRejectMotive());
		query.setParameter("rejectMotiveOther", custodyOperation.getRejectMotiveOther());
		query.setParameter("annulationDate", custodyOperation.getAnnulationDate());
		query.setParameter("annulationUser", custodyOperation.getAnnulationUser());
		query.setParameter("lastModifyDate", custodyOperation.getLastModifyDate());
		query.setParameter("lastModifyUser", custodyOperation.getLastModifyUser());
		query.setParameter("idCustodyOperationPk", custodyOperation.getIdCustodyOperationPk());
		
		query.executeUpdate();	
	} 
	
	public void annulateAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update AccountAnnotationOperation ");
		sbQuery.append(" set state = :state ");
		sbQuery.append(" , motiveReject = :motiveReject ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" where idAnnotationOperationPk = :idAnnotationOperationPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("state", accountAnnotationOperation.getState());
		query.setParameter("motiveReject", accountAnnotationOperation.getMotiveReject());
		query.setParameter("lastModifyDate", accountAnnotationOperation.getLastModifyDate());
		query.setParameter("lastModifyUser", accountAnnotationOperation.getLastModifyUser());
		query.setParameter("idAnnotationOperationPk", accountAnnotationOperation.getIdAnnotationOperationPk());
		
		query.executeUpdate();	
	} 

	
	public void annulatePayrollDetail(Long idPayrollHeaderPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update PayrollDetail ");
		sbQuery.append(" set payrollState = :payrollState ");
		sbQuery.append(" where payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("payrollState", PayrollDetailStateType.ANNULATE.getCode());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.executeUpdate();	
	} 
	
	public void annulatePayrollHeader(Long idPayrollHeaderPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update PayrollHeader ");
		sbQuery.append(" set payrollState = :payrollState ");
		sbQuery.append(" where idPayrollHeaderPk = :idPayrollHeaderPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("payrollState", PayrollHeaderStateType.ANNULATE.getCode());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.executeUpdate();	
	} 
	
}
