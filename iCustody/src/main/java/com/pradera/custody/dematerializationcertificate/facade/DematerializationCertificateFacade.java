package com.pradera.custody.dematerializationcertificate.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.hibernate.sql.Update;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.dematerializationcertificate.service.DematerializationCertificateServiceBean;
import com.pradera.custody.dematerializationcertificate.to.RegisterAccountAnnotationTO;
import com.pradera.custody.dematerializationcertificate.to.SearchAccountAnnotationTO;
import com.pradera.custody.payroll.facade.GeneratePayrollFacade;
import com.pradera.custody.tranfersecurities.accounts.service.CustodyOperationServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.to.PrimaryPlacementComponentTO;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.XmlRespuestaActivoFinanciero;
import com.pradera.integration.component.custody.to.XmlSolicitudActivoFinanciero;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.securitiesannotation.AccAnnotationOperationFile;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationRejectMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DematerializationCertificateFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class DematerializationCertificateFacade {
	
	/** The securities component service. */
	@Inject
	Instance<SecuritiesComponentService> securitiesComponentService;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The dematerialization certificate service bean. */
	@EJB
	DematerializationCertificateServiceBean dematerializationCertificateServiceBean;	
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The general parameters facade. */
	@EJB
	ParameterServiceBean parameterServiceBean;
	
	/** The custody operation service bean. */
	@EJB
	CustodyOperationServiceBean custodyOperationServiceBean;
	
	/** The geographic location service bean. */
	@EJB
	private GeographicLocationServiceBean geographicLocationServiceBean;
	
	@EJB
	private GeneratePayrollFacade generatePayrollFacade;
	
	
	/** The holiday query service bean	 */
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	@Inject
	private ClientRestService clientRestService;
	
	/** The depositary setup. */
	@Inject
	@DepositarySetup
	private IdepositarySetup depositarySetup;

	/** The log. */
	@Inject
	PraderaLogger log;

	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipant(Long participantCode) throws ServiceException{
		return dematerializationCertificateServiceBean.find(Participant.class, participantCode);
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @param issuerCode the issuer code
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuer(String issuerCode) throws ServiceException{
		return dematerializationCertificateServiceBean.find(Issuer.class, issuerCode);
	}
	
	public List<Issuer> lstIssuersStateRegistered() throws ServiceException{
		return dematerializationCertificateServiceBean.lstIssuersStateRegistered();
	}

	private void annulateAnnotationAndCustodyOperation(AccountAnnotationOperation accountAnnotationOperation) {

		CustodyOperation custodyOperation = accountAnnotationOperation.getCustodyOperation();
		custodyOperation.setState(DematerializationStateType.ANNULLED.getCode());
		custodyOperation.setRejectMotive(DematerializationRejectMotiveType.OTHERS.getCode());
		custodyOperation.setRejectMotiveOther("REGISTRO MAYOR A FECHA DE VENCIMIENTO ");
		custodyOperation.setAnnulationDate(CommonsUtilities.currentDate());
		custodyOperation.setAnnulationUser("BATCH");
		custodyOperation.setLastModifyDate(CommonsUtilities.currentDate());
		custodyOperation.setLastModifyUser("BATCH");
		log.info(getClass().getName() + " update CustodyOperation: "+custodyOperation.toString());
		dematerializationCertificateServiceBean.annulateCustodyOperation(custodyOperation);
		
		AccountAnnotationOperation aao = accountAnnotationOperation;
		aao.setState(DematerializationStateType.ANNULLED.getCode());
		aao.setMotiveReject(DematerializationRejectMotiveType.OTHERS.getCode());
		aao.setLastModifyUser("BATCH");
		aao.setLastModifyDate(CommonsUtilities.currentDate());
		aao.setIdAnnotationOperationPk(custodyOperation.getIdCustodyOperationPk());
		log.info(getClass().getName() + " update AccountAnnotationOperation: "+accountAnnotationOperation.toString());
		dematerializationCertificateServiceBean.annulateAccountAnnotationOperation(aao);
		
	}
	

	public void automaticDepositCancelations(Date finalDate) throws ServiceException{		
		log.info(getClass().getName() + " automaticDepositCancelations => finalDate: "+finalDate.toString());
		
		List<AccountAnnotationOperation> lstAccountAnnotationOperation = dematerializationCertificateServiceBean.searchAccountAnnotationOperation(finalDate);
		if(lstAccountAnnotationOperation!=null) {
			log.info(getClass().getName() + " automaticDepositCancelations => lstAccountAnnotationOperation size: "+lstAccountAnnotationOperation.toString());
		}
		
		List<Long> lstIdPayrollHeader = new ArrayList<Long>();
		
		if(lstAccountAnnotationOperation != null && lstAccountAnnotationOperation.size()>0 ) {
			for(AccountAnnotationOperation accountAnnotationOperation : lstAccountAnnotationOperation) {
				if( accountAnnotationOperation.getState().equals(DematerializationStateType.REIGSTERED.getCode()) ) {
					annulateAnnotationAndCustodyOperation(accountAnnotationOperation);
				}else {
					PayrollHeader payrollHeader = dematerializationCertificateServiceBean.getPayrollHeaderWithAnnotationOperation(accountAnnotationOperation.getIdAnnotationOperationPk());
					if( payrollHeader!=null) {
						if( !lstIdPayrollHeader.contains(payrollHeader.getIdPayrollHeaderPk()) ) {
							lstIdPayrollHeader.add(payrollHeader.getIdPayrollHeaderPk());
							log.info(getClass().getName() + " idPayrollHeader => add: "+payrollHeader.getIdPayrollHeaderPk());
						}
						annulateAnnotationAndCustodyOperation(accountAnnotationOperation);
					}
				}
			}

			if( lstIdPayrollHeader.size() >0) {
				for(Long idPayrollHeaderPk : lstIdPayrollHeader) {
					log.info(getClass().getName() + " update PayrollHeader/Detail: "+idPayrollHeaderPk);
					dematerializationCertificateServiceBean.annulatePayrollDetail(idPayrollHeaderPk);
					dematerializationCertificateServiceBean.annulatePayrollHeader(idPayrollHeaderPk);
				}
			}
		}
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_QUERY)
	public List<AccountAnnotationOperation> searchAccountAnnotationOperation(SearchAccountAnnotationTO searchAccountAnnotationTO, boolean blParticipant, boolean blIssuer)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<AccountAnnotationOperation> lstReturnData = dematerializationCertificateServiceBean.searchAccountAnnotationOperation(searchAccountAnnotationTO, blParticipant, blIssuer);
		
		if(lstReturnData!=null) {
			PayrollHeader payrollHeader = null;
			for(AccountAnnotationOperation current: lstReturnData) {
				payrollHeader = searchPayrollHeaderWithAccountAnnotation(current.getIdAnnotationOperationPk());
				current.setPayrollHeader(payrollHeader);
			}
		}
		
		return lstReturnData;
	}
	
	public PayrollHeader searchPayrollHeaderWithAccountAnnotation(Long idAccountAnnotationOperationPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return generatePayrollFacade.searchPayrollHeaderWithAccountAnnotation(idAccountAnnotationOperationPk);
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_QUERY)
	public List<AccountAnnotationOperation> searchAccountAnnotationOperationOriginal(SearchAccountAnnotationTO searchAccountAnnotationTO, boolean blParticipant, boolean blIssuer)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return dematerializationCertificateServiceBean.searchAccountAnnotationOperationOriginal(searchAccountAnnotationTO, blParticipant, blIssuer);
	}
	
	/**
	 * Lista ubiccio geografica.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<GeographicLocation> listaUbiccioGeografica(GeographicLocationTO filter) throws ServiceException {
		return geographicLocationServiceBean.getListGeographicLocation(filter);
	}
	
	/**
	 * Approve multiple annotation operation.
	 *
	 * @param arrAccountAnnotationOperation the arr account annotation operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_APPROVE)
	public String approveMultipleAnnotationOperation(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (int i=0; i<arrAccountAnnotationOperation.length; ++i) {
			AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(arrAccountAnnotationOperation[i].getIdAnnotationOperationPk());
			if(!DematerializationStateType.REIGSTERED.getCode().equals(accountAnnotationOperation.getState()))
				throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
			ErrorServiceType entitiesStates = dematerializationCertificateServiceBean.validateEntities(accountAnnotationOperation);
			if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
				throw new ServiceException(entitiesStates);
			accountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
			accountAnnotationOperation.setAlternativeCode(arrAccountAnnotationOperation[i].getAlternativeCode());
			accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.APPROVED.getCode());
			accountAnnotationOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDate());
			accountAnnotationOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
			accountAnnotationOperation.setCreditorSource(arrAccountAnnotationOperation[i].getCreditorSource());
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			}
			dematerializationCertificateServiceBean.update(accountAnnotationOperation);
		}
		return strOperation;
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_APPROVE)
	public String indApproveMultipleAnnotationOperation(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (int i=0; i<arrAccountAnnotationOperation.length; ++i) {
			AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(arrAccountAnnotationOperation[i].getIdAnnotationOperationPk());
			if(!DematerializationStateType.REIGSTERED.getCode().equals(accountAnnotationOperation.getState()))
				throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
			ErrorServiceType entitiesStates = dematerializationCertificateServiceBean.validateEntities(accountAnnotationOperation);
			if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
				throw new ServiceException(entitiesStates);
			accountAnnotationOperation.setIndApprove(1);
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			}
		}
		return strOperation;
	}
	
	/**
	 * Approve annotation operation.
	 * Aprobar ACV
	 *
	 * @param aaoTO the aao to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_APPROVE)
	public void approveAnnotationOperation(AccountAnnotationOperation aaoTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(aaoTO.getIdAnnotationOperationPk());
		if(!DematerializationStateType.REIGSTERED.getCode().equals(accountAnnotationOperation.getState()))
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		ErrorServiceType entitiesStates = dematerializationCertificateServiceBean.validateEntities(accountAnnotationOperation);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		accountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
		accountAnnotationOperation.setAlternativeCode(aaoTO.getAlternativeCode());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.APPROVED.getCode());
		accountAnnotationOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDate());
		accountAnnotationOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
		accountAnnotationOperation.setCreditorSource(aaoTO.getCreditorSource());
	}

	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_APPROVE)
	public void indApproveAnnotationOperation(AccountAnnotationOperation aaoTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(aaoTO.getIdAnnotationOperationPk());
		if(!DematerializationStateType.REIGSTERED.getCode().equals(accountAnnotationOperation.getState()))
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		ErrorServiceType entitiesStates = dematerializationCertificateServiceBean.validateEntities(accountAnnotationOperation);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		accountAnnotationOperation.setIndApprove(1);
	}
	
	/**
	 * Annulate annotation operation.
	 * Anular ACV
	 *
	 * @param aaoTO the aao to
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_ANNUL)
	public void annulAnnotationOperation(AccountAnnotationOperation aaoTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(aaoTO.getIdAnnotationOperationPk());
		if(!DematerializationStateType.REIGSTERED.getCode().equals(accountAnnotationOperation.getState()))
			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
		/*ErrorServiceType entitiesStates = validateEntities(accountAnnotationOperation);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);*/
		accountAnnotationOperation.setState(DematerializationStateType.ANNULLED.getCode());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.ANNULLED.getCode());
		accountAnnotationOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDate());
		accountAnnotationOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		accountAnnotationOperation.getCustodyOperation().setRejectMotive(aaoTO.getMotiveReject());
		accountAnnotationOperation.setMotiveReject(aaoTO.getMotiveReject());
		if(Validations.validateIsNotNullAndNotEmpty(aaoTO.getMotiveDescReject())){
			accountAnnotationOperation.getCustodyOperation().setRejectMotiveOther(aaoTO.getMotiveDescReject());
		}
	}
	

	/**
	 * Confirm multiple annotation operation.
	 *
	 * @param arrAccountAnnotationOperation the arr account annotation operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM)
	public String confirmMultipleAnnotationOperation(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		return dematerializationCertificateServiceBean.confirmMultipleAnnotationOperation(arrAccountAnnotationOperation, loggerUser);
	}
	/**
	 * Confirm annotation operation.
	 *
	 * @param accountAnnotationOperationTemp the account annotation operation temp
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM)
	public void confirmAnnotationOperation(AccountAnnotationOperation accountAnnotationOperationTemp) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		dematerializationCertificateServiceBean.confirmAnnotationOperation(accountAnnotationOperationTemp,loggerUser, false);
	}
	
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM)
	public void confirmAnnotationOperationWithOutCertificate(AccountAnnotationOperation accountAnnotationOperationTemp) throws ServiceException{		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		dematerializationCertificateServiceBean.confirmAnnotationOperationWithOutCertificate(accountAnnotationOperationTemp,loggerUser, false);
	}
	
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CONFIRM)
	public String confirmMultipleAnnotationOperationWithOutCertificate(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		return dematerializationCertificateServiceBean.confirmMultipleAnnotationOperationWithOutCertificate(arrAccountAnnotationOperation, loggerUser);
	}
	
	
	/**
	 * Review multiple annotation operation.
	 *
	 * @param arrAccountAnnotationOperation the arr account annotation operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REVIEW)
	public String reviewMultipleAnnotationOperation(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (int i=0; i<arrAccountAnnotationOperation.length; ++i) {
			AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(arrAccountAnnotationOperation[i].getIdAnnotationOperationPk());
			accountAnnotationOperation.setState(DematerializationStateType.REVIEWED.getCode());
			accountAnnotationOperation.setAlternativeCode(arrAccountAnnotationOperation[i].getAlternativeCode());
			accountAnnotationOperation.setCreditorSource(arrAccountAnnotationOperation[i].getCreditorSource());
			accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REVIEWED.getCode());
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			}
		}
		return strOperation;
	}
	
	/**
	 *  Review Annotation Operation
	 * Revisr ACV.
	 *
	 * @param accountAnnotationOperationTemp the account annotation operation temp
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REVIEW)
	public void reviewAnnotationOperation(AccountAnnotationOperation accountAnnotationOperationTemp)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(accountAnnotationOperationTemp.getIdAnnotationOperationPk());
//		if(!DematerializationStateType.APPROVED.getCode().equals(accountAnnotationOperation.getState())){
//			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
//		}
		accountAnnotationOperation.setState(DematerializationStateType.REVIEWED.getCode());
		accountAnnotationOperation.setAlternativeCode(accountAnnotationOperationTemp.getAlternativeCode());
		accountAnnotationOperation.setCreditorSource(accountAnnotationOperationTemp.getCreditorSource());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REVIEWED.getCode());
	}
	
	
	/**
	 * Certify multiple annotation operation.
	 *
	 * @param arrAccountAnnotationOperation the arr account annotation operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CERTIFY)
	public String certifyMultipleAnnotationOperation(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (int i=0; i<arrAccountAnnotationOperation.length; ++i) {
			certifyAnnotationOperation(arrAccountAnnotationOperation[i]);
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + arrAccountAnnotationOperation[i].getCustodyOperation().getOperationNumber();
			}
		}
		return strOperation;
	}
	
	/**
	 * Certificate Annotation Operation
	 * Certificar ACV.
	 *
	 * @param accountAnnotationOperationTemp the account annotation operation temp
	 * @throws ServiceException the service exception
	 */
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CERTIFY)
	public void certifyAnnotationOperation(AccountAnnotationOperation accountAnnotationOperationTemp)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(accountAnnotationOperationTemp.getIdAnnotationOperationPk());
		accountAnnotationOperation.setState(DematerializationStateType.CERTIFIED.getCode());
		accountAnnotationOperation.setAlternativeCode(accountAnnotationOperationTemp.getAlternativeCode());
		accountAnnotationOperation.setCreditorSource(accountAnnotationOperationTemp.getCreditorSource());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.CERTIFIED.getCode());
	}
	
	
	/**
	 * Cancel multiple annotation operation.
	 *
	 * @param arrAccountAnnotationOperation the arr account annotation operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CANCEL)
	public String cancelMultipleAnnotationOperation(AccountAnnotationOperation[] arrAccountAnnotationOperation) throws ServiceException
	{
		String strOperation= GeneralConstants.EMPTY_STRING;
		for (AccountAnnotationOperation objAccountAnnotationOperation: arrAccountAnnotationOperation) {
			cancelAnnotationOperation(objAccountAnnotationOperation);
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
				strOperation+=objAccountAnnotationOperation.getCustodyOperation().getOperationNumber();
			} else {
				strOperation+=GeneralConstants.STR_COMMA + objAccountAnnotationOperation.getCustodyOperation().getOperationNumber();
			}
		}
		return strOperation;
	}
	
	/**
	 * Cancel Annotation Operation
	 * Cancelar ACV.
	 *
	 * @param accountAnnotationOperationTemp the account annotation operation temp
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_CANCEL)
	public void cancelAnnotationOperation(AccountAnnotationOperation accountAnnotationOperationTemp)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(accountAnnotationOperationTemp.getIdAnnotationOperationPk());
		accountAnnotationOperation.setState(DematerializationStateType.CANCELLED.getCode());
		accountAnnotationOperation.setAlternativeCode(accountAnnotationOperationTemp.getAlternativeCode());
		accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.CANCELLED.getCode());
	
		accountAnnotationOperation.setMotiveReject(accountAnnotationOperationTemp.getMotiveReject());
		accountAnnotationOperation.setComments(accountAnnotationOperationTemp.getComments());
		accountAnnotationOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDate());
		accountAnnotationOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		accountAnnotationOperation.getCustodyOperation().setRejectMotive(accountAnnotationOperationTemp.getMotiveReject());
	
		if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperationTemp.getMotiveDescReject())){
			accountAnnotationOperation.getCustodyOperation().setRejectMotiveOther(accountAnnotationOperationTemp.getMotiveDescReject());
		}else{		
			accountAnnotationOperation.getCustodyOperation().setRejectMotiveOther(accountAnnotationOperationTemp.getComments());
		}
	}
	/**
	 * Reject annotation operation.
	 *
	 * @param accountAnnotationOperationTemp the id annotation operation pk
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REJECT)
	public void rejectAnnotationOperation(AccountAnnotationOperation accountAnnotationOperationTemp)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		AccountAnnotationOperation accountAnnotationOperation = findAccountAnnotation(accountAnnotationOperationTemp.getIdAnnotationOperationPk());
//		if(!DematerializationStateType.APPROVED.getCode().equals(accountAnnotationOperation.getState())){
//			throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
//		}
		
		accountAnnotationOperation.setMotiveReject(accountAnnotationOperationTemp.getMotiveReject());
		accountAnnotationOperation.setComments(accountAnnotationOperationTemp.getComments());
		if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperationTemp.getMotiveDescReject())){
			accountAnnotationOperation.getCustodyOperation().setRejectMotiveOther(accountAnnotationOperationTemp.getMotiveDescReject());
		}else{		
			accountAnnotationOperation.getCustodyOperation().setRejectMotiveOther(accountAnnotationOperationTemp.getComments());
		}
		accountAnnotationOperation.getCustodyOperation().setRejectMotive(accountAnnotationOperationTemp.getMotiveReject());
		accountAnnotationOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDate());
		accountAnnotationOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
		
		PayrollHeader payrollHeader = dematerializationCertificateServiceBean.rejectAnnotationOperation(accountAnnotationOperation);
		
		if(payrollHeader!=null) {
			dematerializationCertificateServiceBean.rejectPayrollWithAllRejected(payrollHeader);
		}
		//dematerializationCertificateServiceBean.update(payrollHeader);
		/*
		PayrollHeader payrollHeaderAndDetails = dematerializationCertificateServiceBean.getPayrollHeaderAndAllDetail(payrollHeader.getIdPayrollHeaderPk());
		Boolean allPayrollDetailRejectOrAnull = true;
		
		for(PayrollDetail pd: payrollHeaderAndDetails.getLstPayrollDetail()) {
			if( !(pd.getPayrollState().equals(PayrollDetailStateType.REJECT.getCode()) || 
				  pd.getPayrollState().equals(PayrollDetailStateType.ANNULATE.getCode()) )  
			) {
				allPayrollDetailRejectOrAnull = false;
			}
		}
		
		if(allPayrollDetailRejectOrAnull) {
			payrollHeaderAndDetails.setPayrollState(PayrollHeaderStateType.FINISHED.getCode());
			dematerializationCertificateServiceBean.update(payrollHeaderAndDetails);
		}
		*/	
		
		
		/*
		if(ComponentConstant.ONE.equals(accountAnnotationOperation.getIndPrimarySettlement())){
			
			PrimaryPlacementComponentTO primaryPlacementComponentTO = new PrimaryPlacementComponentTO();
	    	primaryPlacementComponentTO.setIdSecurityCode(accountAnnotationOperation.getSecurity().getIdSecurityCodePk());
	    	primaryPlacementComponentTO.setIdPlacementParticipant(accountAnnotationOperation.getPlacementParticipant().getIdParticipantPk());
	    	primaryPlacementComponentTO.setMode(ComponentConstant.SUBTRACTION);
	    	primaryPlacementComponentTO.setStockQuantity(accountAnnotationOperation.getTotalBalance());
	    	primaryPlacementComponentTO.setValidateOpenPlacement(false);
	    	primaryPlacementComponentTO.setUpdateDate(accountAnnotationOperation.getCustodyOperation().getRegistryDate());
			securitiesComponentService.get().updatePrimaryPlacement(primaryPlacementComponentTO);
		}*/
		
	}
	
	/** metodo para obtener un valor desde el id ingresado como parametro
	 * 
	 * @param idSecurityCode
	 * @return */
	public Security getSecurityFromId(String idSecurityCode) {
		Security sec = dematerializationCertificateServiceBean.getSecurityFromId(idSecurityCode);
		return sec;
	}
	
	/** metodo para obtener un HolderAccount desde un particiapnte y cuentaTitular ingresados como parametros
	 * 
	 * @param idParticipant
	 * @param holderAccountNumber
	 * @return null si no se encuntran datos */
	public HolderAccount getHolderAccountFromParticipantAndHoldAccountNumber(Long idParticipant, Integer holderAccountNumber) {
		HolderAccount ha = null;
		ha = dematerializationCertificateServiceBean.getHolderAccountFromParticipantAndHoldAccountNumber(idParticipant, holderAccountNumber);
		return ha;
	}
	
	/** metodo para verificar si un Holder tiene relacion con el holderAccount ingresados como parametros
	 * 
	 * @param idHolderAccount
	 * @param idHolderPk
	 * @return true si tiene relacion , false si no tiene relacion */
	public boolean verifyRelationshipHolderAndHolderAccount(Long idHolderAccount, Long idHolderPk) {
		return dematerializationCertificateServiceBean.verifyRelationshipHolderAndHolderAccount(idHolderAccount, idHolderPk);
	}
	
	/**
	 * metodo para registrar en la BBDD un AccAccountAnnotationFile
	 * @param accAnnotationOperationFile
	 * @return si retorna null es porque hubo ocurrio un error
	 */
	public AccAnnotationOperationFile registryAccAnnotationFile(AccAnnotationOperationFile accAnnotationOperationFile) {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		if (accAnnotationOperationFile.isMassiveRegister()) {
			// pistas de auditoria
			accAnnotationOperationFile.setRegistryUser(loggerUser.getUserName());
			accAnnotationOperationFile.setRegistryDate(CommonsUtilities.currentDateTime());
			accAnnotationOperationFile.setRegistryIp(loggerUser.getIpAddress());

			// validando si se ha realizado la carga de un archivo adjunto
			if (Validations.validateIsNotNull(accAnnotationOperationFile.getAttachedName())) {
				// pistas de auditoria para archivo adjunto
				accAnnotationOperationFile.setRegistryUserAtt(loggerUser.getUserName());
				accAnnotationOperationFile.setRegistryDateAtt(CommonsUtilities.currentDateTime());
				accAnnotationOperationFile.setRegistryIpAtt(loggerUser.getIpAddress());
			}
		} else {
			// pistas de auditoria para archivo adjunto
			accAnnotationOperationFile.setRegistryUserAtt(loggerUser.getUserName());
			accAnnotationOperationFile.setRegistryDateAtt(CommonsUtilities.currentDateTime());
			accAnnotationOperationFile.setRegistryIpAtt(loggerUser.getIpAddress());
		}

		AccAnnotationOperationFile result = null;
		result = dematerializationCertificateServiceBean.registryAccAnnotationFile(accAnnotationOperationFile);
		return result;
	}

	/**
	 * Register account annotation operation.
	 * Registrar ACV
	 * @param registerAccountOperationTO the register account operation to
	 * @return the long
	 * @throws ServiceException the service exception
	 */

	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REGISTER)
	public List<AccountAnnotationOperation> registerListAccountAnnotationOperation(List<RegisterAccountAnnotationTO> lstRegisterAccountOperationTO)throws ServiceException{//Ingreso provicional
		List<AccountAnnotationOperation> listReturn = new ArrayList<AccountAnnotationOperation>();
		for (RegisterAccountAnnotationTO registerAccountAnnotationTO : lstRegisterAccountOperationTO) {
			listReturn.add(registerAccountAnnotationOperation(registerAccountAnnotationTO));
		}
		return listReturn;
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REGISTER)
	public AccountAnnotationOperation registerAccountAnnotationOperation(RegisterAccountAnnotationTO registerAccountOperationTO)throws ServiceException{//Ingreso provicional
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		AccountAnnotationOperation accountAnnotationOperation = registerAccountOperationTO.getAccountAnnotationOperation();
		if(ComponentConstant.ONE.equals(depositarySetup.getIndMarketFact())){
			List<AccountAnnotationMarketFact> lst = new ArrayList<>();
			lst.add(registerAccountOperationTO.getAccountAnnotationMarketFact());
			accountAnnotationOperation.setAccountAnnotationMarketFact(lst);
		}
		if(BooleanType.YES.getCode().equals(accountAnnotationOperation.getIndSerializable())){
			accountAnnotationOperation.setSecuritiesManager(null);
			//accountAnnotationOperation.setIssuer(null);
		}
		if(Validations.validateIsNullOrEmpty(accountAnnotationOperation.getSecurity().getIdSecurityCodePk()))
			accountAnnotationOperation.setSecurity(null);
		accountAnnotationOperation.setDepartment(registerAccountOperationTO.getDepartment());
		accountAnnotationOperation.setProvince(registerAccountOperationTO.getProvince());
		accountAnnotationOperation.setMunicipality(registerAccountOperationTO.getMunicipality());
		accountAnnotationOperation.setState(DematerializationStateType.REIGSTERED.getCode());
		if(registerAccountOperationTO.isBlLien()){
			accountAnnotationOperation.setIndLien(BooleanType.YES.getCode());
			accountAnnotationOperation.setCreditorSource(registerAccountOperationTO.getAccountAnnotationOperation().getCreditorSource());
		}else{
			accountAnnotationOperation.setIndLien(BooleanType.NO.getCode());
		}
		accountAnnotationOperation.setIndDematerialization(0);
		accountAnnotationOperation.setIndImmobilization(1);
		
		if(!accountAnnotationOperation.getSecurityClass().equals(SecurityClassType.PGS.getCode())) {
			accountAnnotationOperation.setHolderAccountBenef(null);
			accountAnnotationOperation.setHolderAccountDebtor(null);
		}
		
		List<AccountAnnotationCupon> lstCurrentCupons = accountAnnotationOperation.getAccountAnnotationCupons();
		
		if(lstCurrentCupons!=null && lstCurrentCupons.size()>0) {
			List<AccountAnnotationCupon> lstSelectedCupons = new ArrayList<AccountAnnotationCupon>();
			for(int i=0; i< lstCurrentCupons.size(); i++) {
				if(lstCurrentCupons.get(i).getStateCupon() == Boolean.TRUE) {
					lstCurrentCupons.get(i).setSerialNumber(accountAnnotationOperation.getSerialNumber().toUpperCase());
					lstCurrentCupons.get(i).setCertificateNumber(accountAnnotationOperation.getCertificateNumber().toUpperCase());
					lstCurrentCupons.get(i).setSecurityNominalValue(accountAnnotationOperation.getSecurityNominalValue());
					lstCurrentCupons.get(i).setSecurityTotalBalance(accountAnnotationOperation.getTotalBalance());
					lstSelectedCupons.add(lstCurrentCupons.get(i));
				}
			}
			accountAnnotationOperation.setAccountAnnotationCupons(lstSelectedCupons);
		}
		
		
		return dematerializationCertificateServiceBean.registerAnnotationOperation(accountAnnotationOperation, loggerUser);
		
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.CERTIFICATE_DEMATERIALIZATION_REGISTER)
	public AccountAnnotationOperation registerAccountAnnotationOperationOriginal(RegisterAccountAnnotationTO registerAccountOperationTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		AccountAnnotationOperation accountAnnotationOperation = registerAccountOperationTO.getAccountAnnotationOperation();
		
		if(ComponentConstant.ONE.equals(depositarySetup.getIndMarketFact())){
			List<AccountAnnotationMarketFact> lst = new ArrayList<>();
			lst.add(registerAccountOperationTO.getAccountAnnotationMarketFact());
			accountAnnotationOperation.setAccountAnnotationMarketFact(lst);
		}
		accountAnnotationOperation.setPlacementParticipant(new Participant(registerAccountOperationTO.getIdParticipantPk()));
		accountAnnotationOperation.setDepartment(registerAccountOperationTO.getDepartment());
		accountAnnotationOperation.setProvince(registerAccountOperationTO.getProvince());
		accountAnnotationOperation.setMunicipality(registerAccountOperationTO.getMunicipality());
		accountAnnotationOperation.setState(DematerializationStateType.REIGSTERED.getCode());
		accountAnnotationOperation.setSecurityNominalValue(accountAnnotationOperation.getSecurity().getCurrentNominalValue());
		accountAnnotationOperation.setIndDematerialization(1);
		accountAnnotationOperation.setIndImmobilization(0);
		if(registerAccountOperationTO.isBlLien()){
			accountAnnotationOperation.setIndLien(BooleanType.YES.getCode());
			accountAnnotationOperation.setCreditorSource(registerAccountOperationTO.getAccountAnnotationOperation().getCreditorSource());
		}else{
			accountAnnotationOperation.setIndLien(BooleanType.NO.getCode());
		}
		accountAnnotationOperation.setIndDematerialization(1);
		accountAnnotationOperation.setIndImmobilization(0);
		accountAnnotationOperation.setOperationPrice(accountAnnotationOperation.getSecurityNominalValue());
		
		return dematerializationCertificateServiceBean.registerAnnotationOperationOriginal(accountAnnotationOperation, loggerUser);
	
	}
	/**
	 * Find account annotation.
	 *
	 * @param idAccountAnnotationOperationPk the id account annotation operation pk
	 * @return the account annotation operation
	 * @throws ServiceException the service exception
	 */
	private AccountAnnotationOperation findAccountAnnotation(Long idAccountAnnotationOperationPk) throws ServiceException{
		return dematerializationCertificateServiceBean.findAccountAnnotation(idAccountAnnotationOperationPk);
	}

	public List<AccountAnnotationCupon> findAccountAnnotationCupon(Long idAccountAnnotationOperationPk) throws ServiceException{
		return dematerializationCertificateServiceBean.findAccountAnnotationCupon(idAccountAnnotationOperationPk);
	}
	/**
	 * Find issuance and issuer.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuance
	 */
	public Issuance findIssuanceAndIssuer(String idSecurityCodePk){
		return dematerializationCertificateServiceBean.findIssuanceAndIssuer(idSecurityCodePk);
	}

	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		return dematerializationCertificateServiceBean.findHolderAccount(idHolderAccountPk);
	}

	/**
	 * Find holder accounts.
	 *
	 * @param idHolderPk the id holder pk
	 * @param idParticipantPk the id participant pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> findHolderAccounts(Long idHolderPk, Long idParticipantPk) throws ServiceException{
		List<HolderAccount> lstResult = dematerializationCertificateServiceBean.findHolderAccounts(idHolderPk, idParticipantPk);
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());			
			Map<Integer, String> mpAccountType = new HashMap<Integer, String>();
			for(ParameterTable paramTab:parameterServiceBean.getListParameterTableServiceBean(paramTabTO)){
				mpAccountType.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			}
			for(HolderAccount holderAccount:lstResult){
				holderAccount.setAccountDescription(mpAccountType.get(holderAccount.getAccountType()));
			}
			return lstResult;
		}else
			return null;
	}

	/**
	 * Find market fact.
	 *
	 * @param idAnnotationOperationPk the id annotation operation pk
	 * @return the account annotation market fact
	 */
	public AccountAnnotationMarketFact findMarketFact(Long idAnnotationOperationPk) {
		return dematerializationCertificateServiceBean.findMarketFact(idAnnotationOperationPk);
	}
	
	/**
	 * Automatic securities dematerialisation cancelations.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticSecuritiesDematerialisationCancelations(Date finalDate) throws ServiceException{								
		String COMMENTS_REJECT = "REVERSION NOCTURNA";
		SearchAccountAnnotationTO searchAccountAnnotationTO = new SearchAccountAnnotationTO();
		searchAccountAnnotationTO.setEndDate(finalDate);
		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(DematerializationStateType.REIGSTERED.getCode());
		lstStates.add(DematerializationStateType.APPROVED.getCode());
		lstStates.add(DematerializationStateType.CERTIFIED.getCode());		
		lstStates.add(DematerializationStateType.REVIEWED.getCode());		
		List<AccountAnnotationOperation> lstAccountAnnotationOperation = dematerializationCertificateServiceBean.searchAccountAnnotationOperationByRevert(searchAccountAnnotationTO, lstStates);				
		for (AccountAnnotationOperation objAccountAnnotationOperation : lstAccountAnnotationOperation){							
			
			//ISSUE 1111
			//Los ANR Aprobados, no los toca
			if(!(objAccountAnnotationOperation.getSecurity().getSecurityClass().equals(SecurityClassType.ANR.getCode()) 
					&& objAccountAnnotationOperation.getState().equals(DematerializationStateType.APPROVED.getCode()))){
				
				//ISSUE 653
				if(objAccountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_PHISYCAL.getCode())){
					
					//es un dato parametrizable en ptable pk 2367
					Date endDate = CommonsUtilities.truncateDateTime(calculateDays(objAccountAnnotationOperation.getCustodyOperation().getRegistryDate()));
					
					if(finalDate.equals(endDate)){
						if(objAccountAnnotationOperation.getState().equals(DematerializationStateType.APPROVED.getCode())
								|| objAccountAnnotationOperation.getState().equals(DematerializationStateType.REVIEWED.getCode())
								|| objAccountAnnotationOperation.getState().equals(DematerializationStateType.CERTIFIED.getCode())
								){
							objAccountAnnotationOperation.setMotiveReject(DematerializationRejectMotiveType.OTHERS.getCode());
							objAccountAnnotationOperation.setComments(COMMENTS_REJECT);
							rejectAnnotationOperation(objAccountAnnotationOperation);
						}else{
							if(objAccountAnnotationOperation.getState().equals(DematerializationStateType.REIGSTERED.getCode())){
								annulAnnotationOperation(objAccountAnnotationOperation);
							}
						}
					}
				}else{
					if (objAccountAnnotationOperation.getState().equals(DematerializationStateType.REIGSTERED.getCode())) {				
						annulAnnotationOperation(objAccountAnnotationOperation);
					}else if (!DematerializationCertificateMotiveType.BANK_SHARES_PRIMARY_COLOCATION_PURCHASE.getCode().equals(objAccountAnnotationOperation.getMotive()) 
							&& objAccountAnnotationOperation.getState().equals(DematerializationStateType.APPROVED.getCode())) {	
						objAccountAnnotationOperation.setMotiveReject(DematerializationRejectMotiveType.OTHERS.getCode());
						objAccountAnnotationOperation.setComments(COMMENTS_REJECT);
						rejectAnnotationOperation(objAccountAnnotationOperation);
					}		
				}
			}
		}
	}

	/**
	 * Metodo para calcular +2 dias habiles 
	 * @param initialDate
	 * @return
	 */
	
	public Date calculateDays (Date initialDate){
		
		//Parametrizable
		ParameterTableTO filter = new ParameterTableTO();
		filter.setParameterTablePk(2367);
		List<ParameterTable> param = parameterServiceBean.getListParameterTableServiceBean(filter);
		
		Date endDate = initialDate;
		Integer contador = 0;
				
		while (contador != param.get(0).getShortInteger()) {
			
			 endDate = CommonsUtilities.addDaysToDate(endDate, GeneralConstants.ONE_VALUE_INTEGER);
			
			while(holidayQueryServiceBean.isNonWorkingDayServiceBean(endDate,true)){
				endDate = CommonsUtilities.addDaysToDate(endDate, GeneralConstants.ONE_VALUE_INTEGER);
			}
			
			contador = contador + 1;
		}
		
		return endDate;
	}
	/**
	 * Exist other request.
	 *
	 * @param security the security
	 * @param states the states
	 * @param certificateNumber the certificate number
	 * @return the integer
	 */
	public Integer existOtherRequest(Security security,List<Integer> states,String certificateNumber) {
		return dematerializationCertificateServiceBean.existOtherRequest(security,states,certificateNumber);
	}
	
	/**
	 * Gets the market fact from security.
	 *
	 * @param idSecurity the id security
	 * @return the market fact from security
	 */
	public List<HolderMarketFactBalance> getMarketFactFromSecurity(String idSecurity){
		return dematerializationCertificateServiceBean.getMarketFactFromSecurity(idSecurity);
	}
	
	/**
	 * Find account annotation operation by security.
	 *
	 * @param idSecurityCode the id security code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccountAnnotationOperation> findAccountAnnotationOperationBySecurity(String idSecurityCode) throws ServiceException{
		return dematerializationCertificateServiceBean.findAccountAnnotationOperationBySecurity(idSecurityCode);
	}
	
	/**
	 * Validate security certificate.
	 *
	 * @param idSecurityCode the id security code
	 * @param certificate the certificate
	 * @return the physical balance detail
	 */
	public PhysicalBalanceDetail validateSecurityCertificate(String idSecurityCode, String certificate){
		return dematerializationCertificateServiceBean.validateSecurityCertificate(idSecurityCode, certificate);
		
	}
	
	public PayrollDetail getPayrollDetailOnliCapitalValid(Long idAnnotationOperationPk) throws ServiceException{
		return dematerializationCertificateServiceBean.getPayrollDetailOnliCapitalValid(idAnnotationOperationPk);
	}
	
	/**
	 * Get Participant by Issuer.
	 *
	 * @param strIssuer the str issuer
	 * @return Participant
	 * @throws ServiceException the service exception
	 */
	public Participant geParticipantByIssuer(String strIssuer) throws ServiceException{
		return dematerializationCertificateServiceBean.geParticipantByIssuer(strIssuer);
	}
	
	/**
	 * Proceso que es llamado desde el batch de recepcion de interfaces.
	 *
	 * @param processFileTO the process file to
	 * @throws ServiceException the service exception
	 */
	public void saveMassiveAccountAnnotationOperation(ProcessFileTO processFileTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        ValidationProcessTO validationTO = processFileTO.getValidationProcessTO();
        
        // #1 separate transaction: save file
        Long idProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(processFileTO, 
        														ExternalInterfaceReceptionType.MANUAL.getCode(), loggerUser);
        
        // #2 separate transaction: save operations independently
		for(ValidationOperationType operation : validationTO.getLstOperations()){

			AccountEntryRegisterTO accountEntryRegisterTO = (AccountEntryRegisterTO) operation;
			accountEntryRegisterTO.setIdInterfaceProcess(idProcess);
			
			try{
				
				validationTO.getLstValidations().add(dematerializationCertificateServiceBean.validateAndSaveAccountEntryTx(accountEntryRegisterTO,loggerUser));
				
				interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryRegisterTO, BooleanType.YES.getCode(), loggerUser);
				
				
			} catch (Exception e) {
				validationTO.getLstValidations().addAll(parameterServiceBean.generateErrorValidationProcess(accountEntryRegisterTO, e).getLstValidations());
				
				interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryRegisterTO, BooleanType.NO.getCode(), loggerUser);
				
			} 
		}
		
		Integer processState= ExternalInterfaceReceptionStateType.CORRECT.getCode();
		//#3 separate transaction: update file independently
		interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(processFileTO.getIdProcessFilePk(),
				validationTO.getLstValidations(), processFileTO.getStreamResponseFileDir(), 
				ComponentConstant.REG_PLACEMENT_TAG, ComponentConstant.OPERATION_PLACEMENT_TAG, processState, loggerUser);
	}

	/**
	 * Gets the allowed dematerialization motives.
	 *
	 * @param security the security
	 * @return the allowed dematerialization motives
	 */
	public List<ParameterTable> getAllowedDematerializationMotives(Security security) {
		return dematerializationCertificateServiceBean.getAllowedDematerializationMotives(security);
	}
	public List<ParameterTable> getAllowedDematerializationMotivesOriginal(Security security) {
		return dematerializationCertificateServiceBean.getAllowedDematerializationMotivesOriginal(security);
	}
	
	/**
	 * Return List Account Annotation Operation.
	 *
	 * @param strIdSecurityCode the security code
	 * @return the Account Annotation Operation
	 */
	public List<AccountAnnotationOperation> getAccountAnnotationOperationValidate(String strIdSecurityCode) {
		return dematerializationCertificateServiceBean.getAccountAnnotationOperationValidate(strIdSecurityCode);
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param holderAccount the holder account
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccount holderAccount) throws ServiceException{
		return dematerializationCertificateServiceBean.getHolderAccountAnnotation(holderAccount,false);
	}
	
	public HolderAccount getHolderAccountInvestor(HolderAccount holderAccount) throws ServiceException{
		return dematerializationCertificateServiceBean.getHolderAccountAnnotation(holderAccount,true);
	}

	public Security findSecurity(AccountAnnotationOperation accountAnnotationOperation) {
		return dematerializationCertificateServiceBean.findSecurity(accountAnnotationOperation);
	}
	
	
//	issue 188 - obtiene lista de hechos de mercado activos 
	public List<HolderMarketFactBalance> getListHolderMarketFacts( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) throws ServiceException {
		return dematerializationCertificateServiceBean.getListHolderMarketFacts(idSecurityCodePk, idParticipantPk, idHolderAccountPk);
	}
//	obtiene hecho de mercado activo
	public HolderMarketFactBalance getHolderMarketFacts( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) throws ServiceException {
		return dematerializationCertificateServiceBean.getHolderMarketFact(idSecurityCodePk, idParticipantPk, idHolderAccountPk);
	}
//	cambia estado de hecho de mercado de activo a inactivo
	public void changeStatusHolderMarketFactBalance(Long idMarketfactBalancePk) {
		dematerializationCertificateServiceBean.changeStatusHolderMarketFactBalance(idMarketfactBalancePk);
	}
//	--
	
	/**
	 * Certificate action application.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @param physicalCertificates the physical certificates
	 * @param operationType the operation type
	 * @throws ServiceException the service exception
	 */
	public XmlRespuestaActivoFinanciero sendDepositeDematerializedRecord(AccountAnnotationOperation[] accountAnnotationOperations)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		
		XmlSolicitudActivoFinanciero xmlSolicitudActivoFinanciero;
		XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero;
		ParameterTable parameterTable = dematerializationCertificateServiceBean.find(ParameterTable.class, 2561);
		xmlSolicitudActivoFinanciero = dematerializationCertificateServiceBean.getSolicitudDepositoDesmaterializacion(accountAnnotationOperations, loggerUser);
		xmlRespuestaActivoFinanciero = clientRestService.envioSolicitud(xmlSolicitudActivoFinanciero,parameterTable.getDescription());
		
		return xmlRespuestaActivoFinanciero;
	}


	public List<SecuritySerialRange> findSecuritySerialRange(String idSecurityCodePk) {
		return dematerializationCertificateServiceBean.findSecuritySerialRange(idSecurityCodePk);
	}
}
