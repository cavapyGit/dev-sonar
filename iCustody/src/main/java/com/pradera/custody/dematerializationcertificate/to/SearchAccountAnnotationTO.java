package com.pradera.custody.dematerializationcertificate.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.OperationTypeCascade;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SearchAccountAnnotationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class SearchAccountAnnotationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;	
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The holder. */
	private Holder holder;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The security. */
	private Security security;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The certificate number. */
	private String certificateNumber;
	
	/** The serial number. */
	private String serialNumber;
	
	/** The annotation state. */
	private Integer annotationMotive, annotationState;
	
	/** The start date. */
	private Date startDate = CommonsUtilities.currentDate();
	
	/** The end date. */
	private Date endDate = CommonsUtilities.currentDate();
	
	/** The max date*/
	private Date maxDate = CommonsUtilities.currentDate();
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	private List<Issuer> lstIssuers;
	
	/** The motive list. */
	private List<ParameterTable> stateList = new ArrayList<ParameterTable>();
	
	/** The motive list. */
	private List<ParameterTable> motiveList = new ArrayList<ParameterTable>();
	
	/** The security class list. */
	private List<ParameterTable> securityClassList = new ArrayList<ParameterTable>();
	
	/** The lst account annotation operations. */
	private GenericDataModel<AccountAnnotationOperation> lstAccountAnnotationOperations;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The search type. */
	private Integer searchType;
	
	/** The bl default search. */
	private boolean blDefaultSearch = true;
	
	/** The list operation type pk. */
	private List<OperationTypeCascade> listOperationTypePk;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The security currency. */
	private Integer securityCurrency;
	
	/**
	 * Gets the state list.
	 *
	 * @return the state list
	 */
	public List<ParameterTable> getStateList() {
		return stateList;
	}
	
	/**
	 * Sets the state list.
	 *
	 * @param stateList the new state list
	 */
	public void setStateList(List<ParameterTable> stateList) {
		this.stateList = stateList;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}
	
	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the annotation state.
	 *
	 * @return the annotation state
	 */
	public Integer getAnnotationState() {
		return annotationState;
	}
	
	/**
	 * Sets the annotation state.
	 *
	 * @param annotationState the new annotation state
	 */
	public void setAnnotationState(Integer annotationState) {
		this.annotationState = annotationState;
	}
	
	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}
	
	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	/**
	 * Gets the certificate number.
	 *
	 * @return the certificate number
	 */
	public String getCertificateNumber() {
		return certificateNumber;
	}
	
	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the new certificate number
	 */
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	
	/**
	 * Gets the serial number.
	 *
	 * @return the serial number
	 */
	public String getSerialNumber() {
		return serialNumber;
	}
	
	/**
	 * Sets the serial number.
	 *
	 * @param serialNumber the new serial number
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	/**
	 * Gets the annotation motive.
	 *
	 * @return the annotation motive
	 */
	public Integer getAnnotationMotive() {
		return annotationMotive;
	}
	
	/**
	 * Sets the annotation motive.
	 *
	 * @param annotationMotive the new annotation motive
	 */
	public void setAnnotationMotive(Integer annotationMotive) {
		this.annotationMotive = annotationMotive;
	}
	
	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}
	
	/**
	 * Sets the start date.
	 *
	 * @param startDate the new start date
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	
	
	public List<Issuer> getLstIssuers() {
		return lstIssuers;
	}

	public void setLstIssuers(List<Issuer> lstIssuers) {
		this.lstIssuers = lstIssuers;
	}

	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the motive list.
	 *
	 * @return the motive list
	 */
	public List<ParameterTable> getMotiveList() {
		return motiveList;
	}
	
	/**
	 * Sets the motive list.
	 *
	 * @param motiveList the new motive list
	 */
	public void setMotiveList(List<ParameterTable> motiveList) {
		this.motiveList = motiveList;
	}
	
	/**
	 * Gets the lst account annotation operations.
	 *
	 * @return the lst account annotation operations
	 */
	public GenericDataModel<AccountAnnotationOperation> getLstAccountAnnotationOperations() {
		return lstAccountAnnotationOperations;
	}
	
	/**
	 * Sets the lst account annotation operations.
	 *
	 * @param lstAccountAnnotationOperations the new lst account annotation operations
	 */
	public void setLstAccountAnnotationOperations(
			GenericDataModel<AccountAnnotationOperation> lstAccountAnnotationOperations) {
		this.lstAccountAnnotationOperations = lstAccountAnnotationOperations;
	}
	
	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}
	
	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the search type.
	 *
	 * @return the search type
	 */
	public Integer getSearchType() {
		return searchType;
	}
	
	/**
	 * Sets the search type.
	 *
	 * @param searchType the new search type
	 */
	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}
	
	/**
	 * Checks if is bl default search.
	 *
	 * @return true, if is bl default search
	 */
	public boolean isBlDefaultSearch() {
		return blDefaultSearch;
	}
	
	/**
	 * Sets the bl default search.
	 *
	 * @param blDefaultSearch the new bl default search
	 */
	public void setBlDefaultSearch(boolean blDefaultSearch) {
		this.blDefaultSearch = blDefaultSearch;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the list operation type pk.
	 *
	 * @return the list operation type pk
	 */
	public List<OperationTypeCascade> getListOperationTypePk() {
		return listOperationTypePk;
	}

	/**
	 * Sets the list operation type pk.
	 *
	 * @param listOperationTypePk the new list operation type pk
	 */
	public void setListOperationTypePk(
			List<OperationTypeCascade> listOperationTypePk) {
		this.listOperationTypePk = listOperationTypePk;
	}

	/**
	 * Gets the security class list.
	 *
	 * @return the security class list
	 */
	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}

	/**
	 * Sets the security class list.
	 *
	 * @param securityClassList the new security class list
	 */
	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the security currency.
	 *
	 * @return the securityCurrency
	 */
	public Integer getSecurityCurrency() {
		return securityCurrency;
	}

	/**
	 * Sets the security currency.
	 *
	 * @param securityCurrency the securityCurrency to set
	 */
	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}

	/**
	 * @return the maxDate
	 */
	public Date getMaxDate() {
		return maxDate;
	}

	/**
	 * @param maxDate the maxDate to set
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
	
}
