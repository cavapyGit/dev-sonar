
package com.pradera.custody.dematerializationcertificate.view;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.dematerializationcertificate.to.RegisterAccountAnnotationTO;
import com.pradera.custody.dematerializationcertificate.to.SearchAccountAnnotationTO;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationCupon;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationOperation;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationRejectMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.generalparameter.type.PeriodType;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.SecuritiesManager;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecuritySourceType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.NegotiationModality;
import com.pradera.model.negotiation.type.NegotiationMechanismType;
import com.pradera.model.process.BusinessProcess;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;

import io.undertow.client.ClientResponse;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright ArkinSoftware 2014.</li>
 * </ul>
 * 
 * The Class DematerializationCertificateBean.
 *
 * @author ArkinSoftware.
 * @version 1.0 , 04-abr-2014
 */



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class DematerializationCertificateBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class DematerializationCertificateBean extends GenericBaseBean {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search account annotation to. */
	private SearchAccountAnnotationTO searchAccountAnnotationTO;
	
	/** The register account annotation to. */
	private RegisterAccountAnnotationTO registerAccountAnnotationTO;
	
	/** The selected account annotation opertaion. */
//	private AccountAnnotationOperation selectedAccountAnnotationOpertaion;
	private String massiveInterfaceType = ComponentConstant.INTERFACE_ACCOUNT_ENTRY;
	
	private AccountAnnotationOperation[] checkAccountAnotation;
	
	/** The bl issuer. */
	private boolean blParticipant, blIssuer, blBlocIssuer;
	
	/** The bl reject. */
	private boolean blApprove, blAnnul, blConfirm, blReject, blReview, blCertify, blCancel;
	
	private boolean blCertificateDisable;
	
	private boolean blDisabledFields ;
	private boolean indAlternativeCode;
	private boolean blRenderedMotives;
	private boolean indValidator,indLien;
	
	private boolean blError = Boolean.FALSE;
	
	/** The Constant VIEW_MAPPING. */
	private static final String VIEW_MAPPING = "viewAccountAnnotation";
	
	/** The Constant REGISTER_MAPPING. */
	private static final String REGISTER_MAPPING = "registerAccountAnnotation";

	private static final String MODIFY_MAPPING = "modifyAccountAnnotation";
	
	private Map<Integer, String> mpStates, mpMotive, mpMnemonicClassSec;
	
	private List<GeographicLocation>  listGeographicLocationDpto;
	private List<GeographicLocation>  listGeographicLocationProvince;
	private List<GeographicLocation>  listGeographicLocationMunicipality;
	
	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;	
	
	/** The department residence. */
	@Inject @Configurable 
	Integer departmentDefault;
	
	/** The province residence. */
	@Inject @Configurable 
	Integer provinceDefault;
	
	/** The municipality residence. */
	@Inject @Configurable 
	Integer districtDefault;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	@Inject
	UserPrivilege userPrivilege;
	
	/** The dematerialization certificate facade. */
	@Inject
	DematerializationCertificateFacade dematerializationCertificateFacade;
	
	/** The accounts facade. */
	@Inject
	AccountsFacade accountsFacade;
	 
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	
	@EJB
	private ParameterServiceBean paramServiceBean;
	
	@EJB
	private CertificateDepositeServiceFacade certificateDepositeBeanFacade;

	@EJB 
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	private String issuerSearchPK;

	/** The lst cbo filter currency. */
	private List<ParameterTable> lstCboFilterCurrency;
	
	private String currentMotive, otherMotive;

	List<NegotiationModality> lstNegotiationModality = new ArrayList<NegotiationModality>();
	
	private Date minExpirationDate;
	
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		searchAccountAnnotationTO = new SearchAccountAnnotationTO();
		searchAccountAnnotationTO.setSearchType(BooleanType.YES.getCode());
		searchAccountAnnotationTO.setBlDefaultSearch(true);
		searchAccountAnnotationTO.setSecurity(new Security());
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		searchAccountAnnotationTO.setHolder(new Holder());
		searchAccountAnnotationTO.setIssuer(new Issuer());
		try {
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				blParticipant = true;
				Participant participant = dematerializationCertificateFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				searchAccountAnnotationTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				List<Participant> lstParticipant = new ArrayList<Participant>();
				lstParticipant.add(participant);
				searchAccountAnnotationTO.setLstParticipants(lstParticipant);
			}else{
				if(userInfo.getUserAccountSession().isIssuerInstitucion()){
					blIssuer = true;
					searchAccountAnnotationTO.setIssuer(dematerializationCertificateFacade.getIssuer(userInfo.getUserAccountSession().getIssuerCode()));
				}
				Participant filterPar = new Participant();
				filterPar.setState(ParticipantStateType.REGISTERED.getCode());
				searchAccountAnnotationTO.setLstParticipants(accountsFacade.getLisParticipantServiceBean(filterPar));
			}
			fillCombos();
			loadCboFilterCurency();
			listHolidays();
			showPrivilegeButtons();
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	public void loadModalityOTC() {
		Integer instrumentType = InstrumentType.FIXED_INCOME.getCode();
		Long mechanismPk = NegotiationMechanismType.OTC.getCode();
		try {
			lstNegotiationModality = accountsFacade.getModalityByInstrumentType(mechanismPk, instrumentType, null);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public void changeNegotiationModality() {
		Long idModalitySelected = null;//mechanismOperationOtcSession.getMechanisnModality().getId().getIdNegotiationModalityPk();
		Long idMechanismSelected = NegotiationMechanismType.OTC.getCode();
	}
	
	/**
	 * Prellenado de combos LaPaz-Murillo-NuestraSenoraDeLaPaz
	 * @throws ServiceException
	 */
	public void ubicacionGeografica() throws ServiceException {
		
		registerAccountAnnotationTO.setDepartment(departmentDefault);
		registerAccountAnnotationTO.setProvince(provinceDefault);
		registerAccountAnnotationTO.setMunicipality(districtDefault);
		
		GeographicLocationTO filter = new GeographicLocationTO();
		
		filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		filter.setIdLocationReferenceFk(countryResidence);
		listGeographicLocationDpto = new ArrayList<GeographicLocation>();
		listGeographicLocationDpto = dematerializationCertificateFacade.listaUbiccioGeografica(filter);

		filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		filter.setIdLocationReferenceFk(departmentDefault);
		listGeographicLocationProvince = new ArrayList<GeographicLocation>();
		listGeographicLocationProvince = dematerializationCertificateFacade.listaUbiccioGeografica(filter);

		filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		filter.setIdLocationReferenceFk(provinceDefault);
		listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
		listGeographicLocationMunicipality = dematerializationCertificateFacade.listaUbiccioGeografica(filter);

	}
	
	/**
	 * Metodo ejecutado al cambiar de departamento
	 * @throws ServiceException
	 */
	public void ubicacionGeograficaByProvince() throws ServiceException {
		
		if (registerAccountAnnotationTO.getDepartment()==null){
			listGeographicLocationProvince = new ArrayList<GeographicLocation>();
			listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
		}else{
			registerAccountAnnotationTO.setProvince(null);
			registerAccountAnnotationTO.setMunicipality(null);
			GeographicLocationTO filter = new GeographicLocationTO();
			filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
			filter.setIdLocationReferenceFk(registerAccountAnnotationTO.getDepartment());
			
			listGeographicLocationProvince = new ArrayList<GeographicLocation>();
			listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
			listGeographicLocationProvince = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		}
	}
	
	/**
	 * Metodo ejecutado al cambiar de provincia
	 * @throws ServiceException
	 */
	public void ubicacionGeograficaByMunicipality() throws ServiceException {
		registerAccountAnnotationTO.setMunicipality(null);
		GeographicLocationTO filter = new GeographicLocationTO();
		filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		filter.setIdLocationReferenceFk(registerAccountAnnotationTO.getProvince());
		listGeographicLocationMunicipality = new ArrayList<GeographicLocation>();
		listGeographicLocationMunicipality = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
	}
	
	
	/**
	 * Clear search.
	 */
	public void clearSearch(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		searchAccountAnnotationTO.setSearchType(BooleanType.YES.getCode());
		searchAccountAnnotationTO.setBlDefaultSearch(true);
		searchAccountAnnotationTO.setAnnotationMotive(null);
		searchAccountAnnotationTO.setAnnotationState(null);
		searchAccountAnnotationTO.setCertificateNumber(null);
		searchAccountAnnotationTO.setSerialNumber(null);
		searchAccountAnnotationTO.setRequestNumber(null);
		searchAccountAnnotationTO.setStartDate(getCurrentSystemDate());
		searchAccountAnnotationTO.setEndDate(getCurrentSystemDate());
		searchAccountAnnotationTO.setMaxDate(getCurrentSystemDate());
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		searchAccountAnnotationTO.setHolder(new Holder());
		searchAccountAnnotationTO.setSecurity(new Security());
		if(!blParticipant){
			searchAccountAnnotationTO.setIdParticipant(null);
		}
	}
	
	public void changeParticipantSearch(){
		searchAccountAnnotationTO.setHolder(new Holder());
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		cleanResults();
	}
	
	public void changeHolder(){
		searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
	}

	/**
	 * Clean results.
	 */
	public void cleanResults(){
		checkAccountAnotation = null;
		searchAccountAnnotationTO.setLstAccountAnnotationOperations(null);
		searchAccountAnnotationTO.setBlNoResult(false);
	}
	
	/**
	 * Search by.
	 */
	public void searchBy(){
		JSFUtilities.resetViewRoot();
		cleanResults();
		if(BooleanType.YES.getCode().equals(searchAccountAnnotationTO.getSearchType())){
			searchAccountAnnotationTO.setBlDefaultSearch(true);
			searchAccountAnnotationTO.setRequestNumber(null);
		}else{
			searchAccountAnnotationTO.setBlDefaultSearch(false);
			if(!blParticipant)
				searchAccountAnnotationTO.setIdParticipant(null);
			searchAccountAnnotationTO.setStartDate(getCurrentSystemDate());
			searchAccountAnnotationTO.setEndDate(getCurrentSystemDate());
			searchAccountAnnotationTO.setAnnotationMotive(null);
			searchAccountAnnotationTO.setAnnotationState(null);
			searchAccountAnnotationTO.setCertificateNumber(null);
			searchAccountAnnotationTO.setSerialNumber(null);
			searchAccountAnnotationTO.setRequestNumber(null);
			searchAccountAnnotationTO.setSecurity(new Security());
			searchAccountAnnotationTO.setHolder(new Holder());
			searchAccountAnnotationTO.setHolderAccount(new HolderAccount());
		}
	}
	
	/**
	 * Search request.
	 */
	@LoggerAuditWeb 
	public void searchRequest(){
		try{
			cleanResults();
			searchAccountAnnotationTO.setBlNoResult(true);
			List<AccountAnnotationOperation> lstResult = dematerializationCertificateFacade.searchAccountAnnotationOperation(searchAccountAnnotationTO, blParticipant, blIssuer);
			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for (int i = 0; i < lstResult.size(); i++) {
					BigDecimal physicalAvailable=BigDecimal.ZERO;
					if(lstResult.get(i).getSecurity() != null){
						if(IssuanceType.DEMATERIALIZED.getCode().equals(lstResult.get(i).getSecurity().getIssuanceForm())){
							physicalAvailable=lstResult.get(i).getSecurity().getPhysicalBalance();
						}else{
							physicalAvailable=CommonsUtilities.subtractTwoDecimal(lstResult.get(i).getSecurity().getPhysicalBalance(), lstResult.get(i).getSecurity().getPhysicalDepositBalance(),1);
						}
						
						String classType = paramServiceBean.getParameterTableById(lstResult.get(i).getSecurity().getSecurityClass()).getParameterName();
						lstResult.get(i).getSecurity().setClassTypeDescription(classType);
						lstResult.get(i).getSecurity().setPhysicalAvailable(physicalAvailable);
					}
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(lstResult.get(i).getHolderAccount().getIdHolderAccountPk());
					holderAccount = dematerializationCertificateFacade.getHolderAccount(holderAccount);
					lstResult.get(i).setHolderAccount(holderAccount);
				}
				searchAccountAnnotationTO.setLstAccountAnnotationOperations(new GenericDataModel<AccountAnnotationOperation>(setDescriptions(lstResult)));
				searchAccountAnnotationTO.setBlNoResult(false);
			}
		}catch(ServiceException se){
			excepcion.fire( new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * New request.
	 *
	 * @return the string
	 */
	public void enabledExpirationData(){

		minExpirationDate = CommonsUtilities.convertStringtoDate("01/01/1900");
	}
	
	public String newRequest(){
		try {
			minExpirationDate = getCurrentSystemDate();
			tmpProcessFile = null;
			registerAccountAnnotationTO = new RegisterAccountAnnotationTO();
			//registerAccountAnnotationTO.getAccountAnnotationOperation().setIndSerializable(0);
			if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
				registerAccountAnnotationTO.setBlMarketFact(true);
			registerAccountAnnotationTO.setAccountAnnotationOperation(new AccountAnnotationOperation());
			registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecuritiesManager(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setIssuer(new Issuer());
			registerAccountAnnotationTO.setHolder(new Holder());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(new HolderAccount());
			

			registerAccountAnnotationTO.setHolderBeneficiario(new Holder());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountBenef(new HolderAccount());

			registerAccountAnnotationTO.setHolderDebtor(new Holder());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountDebtor(new HolderAccount());
			
			
			registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(BigDecimal.ONE);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setBranchOffice(2735);//oficina asuncion
			//registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityNominalValue(BigDecimal.ONE);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityInterestType(InterestType.FIXED.getCode());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setIndTaxExoneration(0);
			
			blBlocIssuer = false;
			if(blParticipant){
				registerAccountAnnotationTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
			} else if(blIssuer) {				
				String strIssuer = userInfo.getUserAccountSession().getIssuerCode();
				Participant objParticipant = dematerializationCertificateFacade.geParticipantByIssuer(strIssuer);
				if(objParticipant != null){
					blBlocIssuer = true;
					registerAccountAnnotationTO.setIdParticipantPk(objParticipant.getIdParticipantPk());
				} 
			}				
			registerAccountAnnotationTO.setLstParticipants(searchAccountAnnotationTO.getLstParticipants());
			registerAccountAnnotationTO.setLstSecuritiesManager(accountsFacade.getSecuritiesManagers());
			ParameterTableTO paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setIndicator1("FISICO");
			paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
			registerAccountAnnotationTO.setLstMotives(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			
			paramTabTO = new ParameterTableTO();
			paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
			paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
			List<ParameterTable> lstSecClass = new ArrayList<>();
			for(ParameterTable paramTab:lstTemp){
				if(InstrumentType.FIXED_INCOME.getCode().equals(paramTab.getShortInteger()) && paramTab.getIndicator2()!=null && paramTab.getIndicator2().equals("1") )
					lstSecClass.add(paramTab);
			}
			registerAccountAnnotationTO.setLstSecurityClass(lstSecClass);
			paramTabTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			registerAccountAnnotationTO.setLstCurrency(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			paramTabTO.setMasterTableFk(MasterTableType.INTEREST_TYPE.getCode());
			registerAccountAnnotationTO.setLstInterestType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
			registerAccountAnnotationTO.setLstBranchOffice(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
			
			registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
			
			registerAccountAnnotationTO.setBlCertificateNumber(false);
			
			loadCboInterestPeriodicity();
			loadCboIndElectronicCupon();
			ubicacionGeografica();
			loadModalityOTC();
//			ubicacionGeograficaByDpto();
//			ubicacionGeograficaByProvince();
//			ubicacionGeograficaByMunicipality();

			registerAccountAnnotationTO.getAccountAnnotationOperation().setIndSerializable(0);
			
			return REGISTER_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}

	public void changeParticipant(){
		registerAccountAnnotationTO.setHolder(new Holder());
		registerAccountAnnotationTO.setLstHolderAccount(null);
	}

	public void changeParticipantSeller(){
		registerAccountAnnotationTO.setHolderSeller(new Holder());
		registerAccountAnnotationTO.setLstHolderAccountSeller(null);
	}

	public void changeMotive(){
		registerAccountAnnotationTO.setIdParticipantSellerPk(null);
		registerAccountAnnotationTO.setHolderSeller(null);
		registerAccountAnnotationTO.setLstHolderAccountSeller(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountSeller(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setAmountRate(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setCashPrice(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setIndExpirationCupons(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setAccountAnnotationCupons(null);
	}
	
	/**
	 * Calculate share capital.
	 */
	public void calculateShareCapital(){
		registerAccountAnnotationTO.setShareCapital(null);
		if(BooleanType.YES.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getIndSerializable())){
			if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk())){
				registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_EMPTY));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
		}else if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue())){
			registerAccountAnnotationTO.setShareCapital(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "Para un registro NO SERIALIZADO se debe ingresar un valor nominal");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance())
				&& registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().compareTo(BigDecimal.ZERO) == 1){
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk() != null &&
					!IssuanceType.DEMATERIALIZED.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIssuanceForm())){
				BigDecimal physicalBalance = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPhysicalBalance();
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().compareTo(physicalBalance) == 1){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PHYSICAL_NOT_ENOUGH));
					JSFUtilities.showSimpleValidationDialog();
					blError = true;
				}else{
					BigDecimal nominalValue = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getCurrentNominalValue();
					registerAccountAnnotationTO.setShareCapital(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()));
					if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
						registerAccountAnnotationTO.getAccountAnnotationMarketFact().setOperationQuantity(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
				}
			}else{
				BigDecimal nominalValue = BigDecimal.ZERO;
				if(BooleanType.YES.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getIndSerializable()))
					nominalValue = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getCurrentNominalValue();
				else
					nominalValue = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue();
				
				Issuance issuance = dematerializationCertificateFacade.findIssuanceAndIssuer(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
				List<AccountAnnotationOperation> lstAccountAnnotationOperation = dematerializationCertificateFacade.getAccountAnnotationOperationValidate(
						registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
				BigDecimal bigAcumulateTotal = BigDecimal.ZERO;
				if(Validations.validateIsNotNullAndNotEmpty(lstAccountAnnotationOperation)){
					for(AccountAnnotationOperation objAccountAnnotationOperation : lstAccountAnnotationOperation){
						if(Validations.validateIsNotNullAndNotEmpty(objAccountAnnotationOperation.getTotalBalance())){
							bigAcumulateTotal = bigAcumulateTotal.add(objAccountAnnotationOperation.getTotalBalance());
						}						
					}
				}
				
				if(issuance != null){
					if(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()).compareTo(issuance.getIssuanceAmount()) > 0){
						registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH));
						JSFUtilities.showSimpleValidationDialog();
						blError = true;
					} else {
						BigDecimal bigQuantity = registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().add(bigAcumulateTotal);
						if(nominalValue.multiply(bigQuantity).compareTo(issuance.getIssuanceAmount()) > 0){
							registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ISSUANCE_NOT_ENOUGH_ACCUMULATE));
							JSFUtilities.showSimpleValidationDialog();
							blError = true;
						} else {
							registerAccountAnnotationTO.setShareCapital(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()));
							if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact()))
								registerAccountAnnotationTO.getAccountAnnotationMarketFact().setOperationQuantity(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
						}										
					}
				}else{
					registerAccountAnnotationTO.setShareCapital(nominalValue.multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()));
				}
			}			
		}else{
			registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
		}
	}
	
	public void validateInterestPeriodicity(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(value!=null && registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getSecurityMonthsTerm()!=null){
			Integer periodictyId=Integer.valueOf( value.toString());
			Integer periodityMonth=InterestPeriodicityType.get(periodictyId).getIndicator1();
			Object[] argObj=new Object[2];
			if(periodityMonth.intValue()>registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getSecurityMonthsTerm().intValue()){
				try {
					argObj[0]=PropertiesUtilities.getMessage("security.lbl.interest.payment.periodicity");
					argObj[1]=PropertiesUtilities.getMessage("security.lbl.months.terms.value");
				} catch (Exception e) {
					e.printStackTrace();
				}

				String strMsg =  "Tiene que ser mayor o igual";//PropertiesUtilities.getMessage(PropertiesConstants.ERROR_LESS_THAN_OR_EQUAL_TO, argObj);
				FacesMessage msg = new FacesMessage(strMsg);
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}
	}
	
	public void calculateNroCouponsAndFirstPayment() throws Exception{
		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isAtMaturityIntPaymentModality()){
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setNumberCoupons(1);
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setCouponFirstExpirationDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getExpirationDate());
		}else if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isByCouponIntPaymentModality()){
			BigDecimal coupons = getNroCouponsForInterest(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getSecurityMonthsTerm(),registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getSecurityDaysTerm());
			if(Validations.validateIsNotNullAndNotEmpty(coupons)){
				if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIssuance())
						&& Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicity())){
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setNumberCoupons(new Integer(coupons.toString()));
					Date expirationDate=null;
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isByDaysInterestPeriodicity()){
						if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicityDays()!=null && registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicityDays()>0){
							expirationDate = CommonsUtilities.addDaysToDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIssuanceDate(), 
									registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicityDays());
						}
					}else{
						expirationDate = CommonsUtilities.addMonthsToDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIssuanceDate(), 
								   InterestPeriodicityType.get(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicity()).getIndicator1());
					}

					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setCouponFirstExpirationDate(expirationDate);
				}else{
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setNumberCoupons(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setCouponFirstExpirationDate(null);
				}
			}else{
				registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setNumberCoupons(null);
				registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setCouponFirstExpirationDate(null);
			}
		}
	}
	
	public void changeCboInterestPeriodicityHandler(){
		try {
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setCouponFirstExpirationDate(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setNumberCoupons(null);
			
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity().equals(InterestPaymentModalityType.AT_MATURITY.getCode())) {
				registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setInterestPaymentModality(InterestPaymentModalityType.AT_MATURITY.getCode());
			}else {
				registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setInterestPaymentModality(InterestPaymentModalityType.BY_COUPON.getCode());	
			}
			calculateNroCouponsAndFirstPayment();
		} catch (Exception e) {
			excepcion.fire( new ExceptionToCatchEvent( e ) );
		}
	}
	
	private BigDecimal getNroCouponsForInterest(Integer securityMonths, Integer securityDays) throws Exception{
		if(Validations.validateIsNotNullAndNotEmpty(securityMonths) &&
				Validations.validateIsNotNullAndNotEmpty(securityDays) &&		
				Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicity())){
			BigDecimal coupons = null;
			BigDecimal monthsTerm = new BigDecimal(securityMonths);
			BigDecimal daysTerm = new BigDecimal(securityDays);
			BigDecimal periodicity=null;
			
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isByDaysInterestPeriodicity() && (registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicityDays()==null || registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicityDays()<=0)){
				return null;
			}
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isByDaysInterestPeriodicity()){
				periodicity=BigDecimal.valueOf(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicityDays());
				coupons = daysTerm.divide(periodicity,RoundingMode.DOWN);
			}else if(!registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isIndistinctPeriodicity()){
				periodicity = new BigDecimal(InterestPeriodicityType.get(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getPeriodicity().intValue()).getIndicator1());
				coupons = monthsTerm.divide(periodicity,RoundingMode.DOWN);
			}	
			return coupons;
		}
		return null;
	}

	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The tmp process file. */
	private ProcessFileTO tmpProcessFile; // temporal bean on upload
	
	
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateXlsFile(event.getFile(), null, null,null);//fUploadFileTypes
			if (fDisplayName != null) {
				tmpProcessFile = new ProcessFileTO();
				tmpProcessFile.setFileName(fDisplayName);
				tmpProcessFile.setProcessFile(event.getFile().getContents());
				tmpProcessFile.setProcessInputStream(event.getFile().getInputstream());
				//tmpProcessFile.setTempProcessFile(File.createTempFile("tempMcnfile", ".tmp"));
				//FileUtils.writeByteArrayToFile(tmpProcessFile.getTempProcessFile(), tmpProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	public void removefileHandler(ActionEvent actionEvent) {
		tmpProcessFile.setProcessFile(null);
		tmpProcessFile.setFileName(null);
		if(tmpProcessFile.getTempProcessFile()!=null){
			tmpProcessFile.getTempProcessFile().delete();
			tmpProcessFile.setTempProcessFile(null);
		}
	}
	
	public boolean validationIfExistCupon() {
		
		return true;
	}
	
	
	public void validateInterfaceFile() 
			throws BeanIOConfigurationException, IllegalArgumentException, IOException, ServiceException, ParserConfigurationException {
		
		try {
			//JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveMCN");
			int countValidationErrors = 0;
			
			if(Validations.validateIsNull(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate())) {
				 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						 ,"Por favor ingrese la fecha de vencimiento");
				 JSFUtilities.showSimpleValidationDialog();
				 return;
			}
					
			if(Validations.validateIsNull(tmpProcessFile.getProcessInputStream())){
	    		JSFUtilities.addContextMessage("frmMassiveMCN:fuplMcnFile", FacesMessage.SEVERITY_ERROR,
	    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL), 
	    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL));
	    		
				countValidationErrors++;
	    	}
			
			if(countValidationErrors > 0) {
	    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
	    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
	    		return;
	    	}
			/*errorsQuantity = 0;
			sucessQuantity = 0;
			*/
			 List<ValidationAccountAnnotationCupon>  lstCupons = CommonsUtilities.setXLSXToEntityAccountAnnotationCupon(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber(),registerAccountAnnotationTO.getAccountAnnotationOperation().getSerialNumber(),tmpProcessFile.getProcessInputStream());
			 Date expiriationDate = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate();
			 for(ValidationAccountAnnotationCupon i : lstCupons) {
				 Date beginningDateFile = new SimpleDateFormat("dd/MM/yyyy").parse(i.getBeginingDate());
				 Date expirDateFile = new SimpleDateFormat("dd/MM/yyyy").parse(i.getExpirationDate());
				 Date paymentDateFile = new SimpleDateFormat("dd/MM/yyyy").parse(i.getPaymentDate());
				 if(beginningDateFile.after(expiriationDate)) {
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							 ,"La fecha de inicio del archivo no puede ser mayor a la fecha de vencimiento del título");
					 JSFUtilities.showSimpleValidationDialog();
					 tmpProcessFile.setFileName(null);
					 return;
				 } else if (expirDateFile.after(expiriationDate)) {
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							 ,"La fecha de vencimiento del archivo no puede ser mayor a la fecha de vencimiento del título");
					 JSFUtilities.showSimpleValidationDialog();
					 tmpProcessFile.setFileName(null);
					 return;
				 } else if(paymentDateFile.after(expiriationDate)) {
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							 ,"La fecha de pago del archivo no puede ser mayor a la fecha de vencimiento del título");
					 JSFUtilities.showSimpleValidationDialog();
					 tmpProcessFile.setFileName(null);
					 return; 
				 }
			 }
			 
			 setAccountAnnotationCupon(registerAccountAnnotationTO.getAccountAnnotationOperation(), lstCupons);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					,"Ocurrio un error inesperado");
			JSFUtilities.showSimpleValidationDialog();
		}
		//processFile = tmpProcessFile;
	}
	
	public void setAccountAnnotationCupon(AccountAnnotationOperation accountAnnotationOperation, List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon) {
		if( lstValidationAccountAnnotationCupon!=null && lstValidationAccountAnnotationCupon.size()>0 ) {
			List<AccountAnnotationCupon> lstAccountAnnotationCupon = new ArrayList<AccountAnnotationCupon>();
			AccountAnnotationCupon accountAnnotationCupon = null;
			String messagesError = "";
			Integer nroCupon = 0;
			
			for(ValidationAccountAnnotationCupon validationCupon: lstValidationAccountAnnotationCupon) {
				accountAnnotationCupon = new AccountAnnotationCupon();
				messagesError = "";
				nroCupon = 0;
				
				try {
					if( validationCupon.getNroCupon()!=null && !validationCupon.getNroCupon().equals("") ) {
						nroCupon = Integer.valueOf(validationCupon.getNroCupon());
						accountAnnotationCupon.setCuponNumber(nroCupon);
					}
				} catch (Exception e) {
					messagesError = messagesError+", Nro Cupon";
				}
				
				try {
					if( validationCupon.getBeginingDate()!=null && !validationCupon.getBeginingDate().equals("") ) {
						accountAnnotationCupon.setBeginingDate(CommonsUtilities.convertStringtoDate(validationCupon.getBeginingDate()));
						if(nroCupon == 1) {
							accountAnnotationOperation.setCouponFirstExpirationDate(accountAnnotationCupon.getBeginingDate());
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Inicio";
					accountAnnotationOperation.setMsgMassiveErrors(accountAnnotationOperation.getMsgMassiveErrors().concat(""));
				}
				
				try {
					if( validationCupon.getExpirationDate() !=null && !validationCupon.getExpirationDate().equals("") ) {
						accountAnnotationCupon.setExpirationDate(CommonsUtilities.convertStringtoDate(validationCupon.getExpirationDate()));
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Vencimiento";
				}
				
				try {
					if( validationCupon.getPaymentDate() !=null && !validationCupon.getPaymentDate().equals("") ) {
						accountAnnotationCupon.setPaymentDate(CommonsUtilities.convertStringtoDate(validationCupon.getPaymentDate()));
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Pago";
				}

				try {
					if( validationCupon.getInterestRate() !=null && !validationCupon.getInterestRate().equals("") ) {
						accountAnnotationCupon.setInterestRate(new BigDecimal(validationCupon.getInterestRate()));
					}
				} catch (Exception e) {
					messagesError = messagesError+", Tasa";
				}

				try {
					if( validationCupon.getCuponAmount() !=null && !validationCupon.getCuponAmount().equals("") ) {
						accountAnnotationCupon.setCuponAmount(new BigDecimal(validationCupon.getCuponAmount()));
					}
				} catch (Exception e) {
					messagesError = messagesError+", Monto Cupon";
				}
				
				try {
					if( validationCupon.getExpirationDate() !=null && !validationCupon.getExpirationDate().equals("") ) {
						accountAnnotationCupon.setRegistryDate(CommonsUtilities.addDaysToDate(accountAnnotationCupon.getExpirationDate(), 1*-1));
						//accountAnnotationCupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(accountAnnotationCupon.getRegistryDate(), -1));
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Registro";
				}
				
				if(messagesError!=null && !messagesError.equals("")) {
					messagesError = "Hubo Problemas en los siguientes campos" + messagesError;
				}


				try {
					Integer paymentDays=null;
					if(true){
						paymentDays=CommonsUtilities.getDaysBetween(accountAnnotationCupon.getBeginingDate(),accountAnnotationCupon.getPaymentDate());
					}
					accountAnnotationCupon.setPaymentDays(paymentDays);
				} catch (Exception e) {
					messagesError = messagesError+", Cantidad de dias";
				}
				
				accountAnnotationCupon.setStateCupon(true);
				accountAnnotationCupon.setInterestFactor( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate() );
				accountAnnotationCupon.setCertificateNumber(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber()+"-"+accountAnnotationCupon.getCuponNumber());
				accountAnnotationCupon.setSerialNumber(registerAccountAnnotationTO.getAccountAnnotationOperation().getSerialNumber().toUpperCase());
				accountAnnotationCupon.setIndElectronicCupon(registerAccountAnnotationTO.getAccountAnnotationOperation().getIndElectronicCupon());
				accountAnnotationCupon.setMessagesError(messagesError);
				
				lstAccountAnnotationCupon.add(accountAnnotationCupon);
			}
			
			accountAnnotationOperation.setAccountAnnotationCupons(lstAccountAnnotationCupon);
		}
		
	}
	
	public void selectCheckboxWithExpirationCupons(AccountAnnotationCupon accountAnnotationCupon) {
		System.out.println("se activo! "+accountAnnotationCupon.getExpirationDate());
		if( registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons()!=null && 
			registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons().equals(1)) {
			 
			for(AccountAnnotationCupon cupon : registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons()) {
				if( cupon.getExpirationDate().after(accountAnnotationCupon.getExpirationDate()) ) {
					cupon.setStateCupon(true);
				}else if( cupon.getExpirationDate().equals(accountAnnotationCupon.getExpirationDate()) ) {
					cupon.setStateCupon(true);
				}else {
					cupon.setStateCupon(false);
				}
			}
			
		}
	}
	
	public void generateAccountAnnotationCupon() {
		try {
			//periodicity
			Integer numberCupons = null;
			Integer factorPeriodicity = null;
			
			factorPeriodicity = CommonsUtilities.factorPeriodicityType(registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity());
			
			
			Integer daysDiff = CommonsUtilities.diffDatesInDays(registerAccountAnnotationTO.getAccountAnnotationOperation().getExpeditionDate(), 
											registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate());
			BigDecimal anioDecimal = new BigDecimal("365.00");
			BigDecimal dayDiffDecimal = new BigDecimal(daysDiff+".00");
			
			BigDecimal yearFactor = dayDiffDecimal.divide(anioDecimal,2); 
			
			numberCupons = (yearFactor.multiply(new BigDecimal(factorPeriodicity))).intValue();
			numberCupons = (numberCupons <= 1)?1:numberCupons;
			
			Integer inNumberCupons =  (registerAccountAnnotationTO.getCuponQuantity() == null) ? numberCupons : registerAccountAnnotationTO.getCuponQuantity();
			
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setNumberCoupons(inNumberCupons);//numberCupons
			
		}catch (Exception e){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					,"Ocurrio un error al calcular los cupones, existen campos obligatorios incompletos");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

		try {
			
			registerAccountAnnotationTO.getAccountAnnotationOperation().setAccountAnnotationCupons(new ArrayList<AccountAnnotationCupon>());
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity() != null &&
				registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getNumberCoupons() !=null) {
				
				AccountAnnotationCupon current = null;
				Date previousExpirationDate=null;
				
				for(int i=1; i<=registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getNumberCoupons(); i++) {
					current = new AccountAnnotationCupon();
					current = generateCuponWithData(i, previousExpirationDate,null, current);
					previousExpirationDate=current.getExpirationDate();
					registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons().add(current);
					
				}
			}
			
			Integer diasHabiles = 3;
			
			ParameterTableTO filter = new ParameterTableTO();
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive())) {
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
					filter.setParameterTablePk(2911);
				else
					filter.setParameterTablePk(2912);
			}else
				filter.setParameterTablePk(2912);
			List<ParameterTable> param = paramServiceBean.getListParameterTableServiceBean(filter);
			if(param!=null && param.size()>0) {
				diasHabiles = param.get(0).getShortInteger();
			}
			
			
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons()!=null) {
				List<AccountAnnotationCupon> lstCuponsAvailables = new ArrayList<AccountAnnotationCupon>();
				Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles); 	//generalParameterFacade.workingDateCalculateServiceFacade(CommonsUtilities.currentDate(), 3);
				for(AccountAnnotationCupon cupon: registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons()) {

					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons() == null || 
					  ( registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons() != null &&
						registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons().equals(0) ) ) {
						
						if( cupon.getExpirationDate().after(maxAvailableDate) ) {
							lstCuponsAvailables.add(cupon);
						}
						
					}else {

						lstCuponsAvailables.add(cupon);
					}
				}
				registerAccountAnnotationTO.getAccountAnnotationOperation().setAccountAnnotationCupons(lstCuponsAvailables);
				if( registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons().size() == 0 ) {

					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,"No puede registrarse un cupon cuya fecha vencimiento este dentro de los "+diasHabiles+" dias siguientes");
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			
		
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					,"Existen campos obligatorios incompletos");
			JSFUtilities.showSimpleValidationDialog();
		}
		
	}
	
	public AccountAnnotationCupon generateCuponWithData(Integer i, Date previousExpirationDate, Date forceExpirationDate, AccountAnnotationCupon current ) throws ServiceException {
		
		current.setCuponNumber(i);

		if(previousExpirationDate==null){
			current.setBeginingDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getExpeditionDate() );	
		}else{
			current.setBeginingDate( previousExpirationDate );
		}
		if( i == registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getNumberCoupons() ){//si es el ultimo cupon
			current.setExpirationDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate());
		}else{
			//Expiration Date
			if(current.getCuponNumber().equals( GeneralConstants.ONE_VALUE_INTEGER )){
				current.setExpirationDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getCouponFirstExpirationDate());
			}else{
				if(InterestPeriodicityType.BY_DAYS.getCode().equals( registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity() )){
					current.setExpirationDate(  CommonsUtilities.addDaysToDate(current.getBeginingDate(), 
							365 ) );//security.getPeriodicityDays()	
				}else{
					current.setExpirationDate(  CommonsUtilities.addMonthsToDate(current.getBeginingDate(), 
							InterestPeriodicityType.get(registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity()).getIndicator1() ) );
				}
			}
		}
		
		if(forceExpirationDate != null) {
			current.setExpirationDate(forceExpirationDate);
		}
		
		previousExpirationDate=current.getExpirationDate();
		
		current.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(current.getExpirationDate(), 
				   GeneralConstants.ONE_VALUE_INTEGER) );
		Integer paymentDays=null;
		if(true){//security.isExtendedTerm()
			paymentDays=CommonsUtilities.getDaysBetween(current.getBeginingDate(),current.getExpirationDate());
		}/*else{
			paymentDays=CommonsUtilities.getDaysBetween(current.getBeginingDate(),current.getExpirationDate());
		}*/
		current.setPaymentDays(paymentDays);
		current.setRegistryDate(CommonsUtilities.addDaysToDate(current.getExpirationDate(), 1*-1));//security.getStockRegistryDays()
		current.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(current.getRegistryDate(), -1));
		current.setInterestFactor( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate() );

		
		BigDecimal rate=registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate().divide(BigDecimal.valueOf(100),MathContext.DECIMAL128);
		BigDecimal factor=null;
		factor=rate.divide( new BigDecimal(CalendarDayType._365.getIntegerValue()),MathContext.DECIMAL128 );
		factor=factor.multiply( BigDecimal.valueOf(paymentDays) );
		factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
		current.setInterestRate(factor);
		
		BigDecimal cuponAmount = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue().multiply(current.getInterestRate()).divide(BigDecimal.valueOf(100)  , MathContext.DECIMAL128);
		
		current.setCuponAmount(cuponAmount);
		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber()!=null) {
			current.setCertificateNumber(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber().toUpperCase()+"-"+current.getCuponNumber());
		}
		
		current.setSerialNumber(registerAccountAnnotationTO.getAccountAnnotationOperation().getSerialNumber().toUpperCase());//+"-"+current.getCuponNumber()
		current.setIndElectronicCupon(registerAccountAnnotationTO.getAccountAnnotationOperation().getIndElectronicCupon());
		current.setStateCupon(true);
		current.setSecurityTotalBalance(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
		current.setSecurityNominalValue(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue());
		
		//Date securityExpirationAddOneDay = CommonsUtilities.addDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate(), 1);
		
		/*if( //current.getExpirationDate().after(CommonsUtilities.currentDate()) &&
			//registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate() 
			securityExpirationAddOneDay.after(current.getExpirationDate())
		) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons().add(current);
		}*/
		
		return current;
	}
	
	/*
	public void generateAutomaticSchedule() {

		AccountAnnotationCupon cupon = new AccountAnnotationCupon();
		
		ProgramInterestCoupon programInterestCoupon=null;
		Date previousExpirationDate=null;
		
		for(int i=1;i<=registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getNumberCoupons();i++){
			programInterestCoupon=new ProgramInterestCoupon();
			programInterestCoupon.setCouponNumber(Integer.valueOf(i));
			programInterestCoupon.setCurrency( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityCurrency() );
			//Begin date
			if(previousExpirationDate==null){
				programInterestCoupon.setBeginingDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getExpeditionDate() );	
			}else{
				programInterestCoupon.setBeginingDate( previousExpirationDate );
			}
			
			if(i==registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getNumberCoupons()){
				//Expiration Date
				programInterestCoupon.setExperitationDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate());
				
			}else{
				//Expiration Date
				if(programInterestCoupon.getCouponNumber().equals( GeneralConstants.ONE_VALUE_INTEGER )){
					programInterestCoupon.setExperitationDate( registerAccountAnnotationTO.getAccountAnnotationOperation().getCouponFirstExpirationDate() );
				}else{
					if(InterestPeriodicityType.BY_DAYS.getCode().equals( registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity() )){
						programInterestCoupon.setExperitationDate(  CommonsUtilities.addDaysToDate(programInterestCoupon.getBeginingDate(), 
								364 ) );//registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicityDays()	
					}else{
						programInterestCoupon.setExperitationDate(  CommonsUtilities.addMonthsToDate(programInterestCoupon.getBeginingDate(), 
								InterestPeriodicityType.get(registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity()).getIndicator1() ) );
					}
				}
			}
			previousExpirationDate=programInterestCoupon.getExperitationDate();
			//Payment Date
			programInterestCoupon.setPaymentDate( generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getExperitationDate(), 
					   GeneralConstants.ONE_VALUE_INTEGER) );
			Integer paymentDays=null;
			if(security.isExtendedTerm()){
				paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getPaymentDate());
			}else{
				paymentDays=CommonsUtilities.getDaysBetween(programInterestCoupon.getBeginingDate(),programInterestCoupon.getExperitationDate());
			}
			programInterestCoupon.setPaymentDays(paymentDays);
			
			if(programInterestCoupon.getCurrency().equals(CurrencyType.UFV.getCode()) || 
					programInterestCoupon.getCurrency().equals(CurrencyType.DMV.getCode())){
				programInterestCoupon.setExchangeDateRate(programInterestCoupon.getPaymentDate());
				programInterestCoupon.setCurrencyPayment(CurrencyType.PYG.getCode());
			}
			
			//Corporative Process Date
//					programInterestCoupon.setCorporativeDate( CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), 
//							security.getCorporativeProcessDays() * -1) );
			programInterestCoupon.setCorporativeDate( CommonsUtilities.currentDate() );
			
			//Registry Date
			programInterestCoupon.setRegistryDate(CommonsUtilities.addDaysToDate(programInterestCoupon.getExperitationDate(), security.getStockRegistryDays()*-1));
			programInterestCoupon.setRegistryDate(generalParameterFacade.workingDateCalculateServiceFacade(programInterestCoupon.getRegistryDate(), -1));
			//Cut Off Date
			programInterestCoupon.setCutoffDate( programInterestCoupon.getRegistryDate() );
			
			programInterestCoupon.setInterestPaymentCronogram( security.getInterestPaymentSchedule() );
			
			programInterestCoupon.setInterestFactor( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate() );
			programInterestCoupon.setStateProgramInterest( ProgramScheduleStateType.PENDING.getCode() );
			programInterestCoupon.setSituationProgramInterest( ProgramScheduleSituationType.PENDING.getCode() );
			
			if(programInterestCoupon.isPendingState()){
				programInterestCoupon.setInterestRate(securitiesUtils.interestRateCalculate(security, 
																							security.getPeriodicity(), 
																							security.getInterestRate(), 
																							programInterestCoupon.getBeginingDate(), 
																							programInterestCoupon.getPaymentDays(),
																							programInterestCoupon.getCouponNumber(),
																							septemberLeap,
																							nextYear));
			}	

			programInterestCoupon.setIndRounding(BooleanType.NO.getCode()); 
			programInterestCoupon.setPayedAmount(BigDecimal.ZERO);
			programInterestCoupon.setLastModifyApp( Integer.valueOf(1) );
			programInterestCoupon.setLastModifyDate( CommonsUtilities.currentDateTime() );
			programInterestCoupon.setLastModifyIp( userInfo.getUserAccountSession().getIpAddress()  );
			programInterestCoupon.setLastModifyUser(userInfo.getUserAccountSession().getUserName() );
			if(programInterestCoupon.getCouponNumber().compareTo( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getNumberCoupons() )==0){
				programInterestCoupon.setLastCoupon(true);
			}
			
			security.getInterestPaymentSchedule().getProgramInterestCoupons().add( programInterestCoupon );
		}
	
	}*/
	
	
	public void loadCboInterestPeriodicity() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.INTEREST_PERIODICITY.getCode() );
		parameterTableTO.setOrderbyParameterTableCd( BooleanType.YES.getCode() );
		registerAccountAnnotationTO.setLstCboInterestPeriodicity(generalParametersFacade.getListParameterTableServiceBean(parameterTableTO));
		
		parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setParameterTablePk(InterestPaymentModalityType.AT_MATURITY.getCode());
		List<ParameterTable> lstParam = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		registerAccountAnnotationTO.getLstCboInterestPeriodicity().addAll(lstParam);
	}

	public void loadCboIndElectronicCupon() throws ServiceException{
		ParameterTable param = null;
		List<ParameterTable> lstParameterTable = new ArrayList<ParameterTable>();
		param = new ParameterTable();
		param.setParameterTablePk(1);
		param.setParameterName("SI");
		param.setDescription("SI");
		lstParameterTable.add(param);
		
		param = new ParameterTable();
		param.setParameterTablePk(0);
		param.setParameterName("NO");
		param.setDescription("NO");
		lstParameterTable.add(param);
		
		
		registerAccountAnnotationTO.setLstIndElectronicCupon(lstParameterTable);
		if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getIndElectronicCupon())) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setIndElectronicCupon(0);
		}
	}
	
	
	public boolean ValidateCertificateAprobe() throws ServiceException{
		AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
			PhysicalBalanceDetail physicalBalanceDetail = null;
			if(selectedAAO.getSecurity()!=null){
				physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(selectedAAO.getSecurity().getIdSecurityCodePk(), 
						selectedAAO.getCertificateNumber());
			}				
			if (Validations.validateIsNotNull(physicalBalanceDetail)){
			System.out.println("=====================SI EXISTE EN FISICA=======================");
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, 
							new Object[]{selectedAAO.getCertificateNumber()}));
			JSFUtilities.showSimpleValidationDialog();
			return true;	
			}else{
			return false;
			}
			
			
	}
	
	
	/**
	 * 
	 */
	
	public void validateHolder(){
		try {
			registerAccountAnnotationTO.setLstHolderAccount(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(new HolderAccount());
			
			
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getHolder().getIdHolderPk())){		
				if(!HolderStateType.REGISTERED.getCode().equals(registerAccountAnnotationTO.getHolder().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_INVALID_STATE));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolder(new Holder());
					return;
				}
				if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getIdParticipantPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolder(new Holder());				
				}else{
					List<HolderAccount> lstResult = dematerializationCertificateFacade.findHolderAccounts(registerAccountAnnotationTO.getHolder().getIdHolderPk()
																			, registerAccountAnnotationTO.getIdParticipantPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						if(lstResult.size()==GeneralConstants.ONE_VALUE_INTEGER){
							HolderAccount holderAccount=lstResult.get(0);
							registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(holderAccount);
							
						}
						registerAccountAnnotationTO.setLstHolderAccount(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerAccountAnnotationTO.setHolder(new Holder());	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	public void validateHolderDebtor(){
		try {
			registerAccountAnnotationTO.setLstHolderAccountDebtor(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountDebtor(new HolderAccount());
			
			
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getHolderDebtor().getIdHolderPk())){		
				if(!HolderStateType.REGISTERED.getCode().equals(registerAccountAnnotationTO.getHolderDebtor().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_INVALID_STATE));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolderDebtor(new Holder());
					return;
				}
				if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getIdParticipantPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolder(new Holder());				
				}else{
					List<HolderAccount> lstResult = dematerializationCertificateFacade.findHolderAccounts(registerAccountAnnotationTO.getHolderDebtor().getIdHolderPk()
																			, registerAccountAnnotationTO.getIdParticipantPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						if(lstResult.size()==GeneralConstants.ONE_VALUE_INTEGER){
							HolderAccount holderAccount=lstResult.get(0);
							registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountDebtor(holderAccount);
						}
						registerAccountAnnotationTO.setLstHolderAccountDebtor(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerAccountAnnotationTO.setHolderDebtor(new Holder());	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	public void validateHolderBeneficiario(){
		try {
			registerAccountAnnotationTO.setLstHolderAccountBeneficiario(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountBenef(new HolderAccount());
			
			
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getHolderBeneficiario().getIdHolderPk())){		
				if(!HolderStateType.REGISTERED.getCode().equals(registerAccountAnnotationTO.getHolderBeneficiario().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_INVALID_STATE));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolderBeneficiario(new Holder());
					return;
				}
				if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getIdParticipantPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolder(new Holder());				
				}else{
					List<HolderAccount> lstResult = dematerializationCertificateFacade.findHolderAccounts(registerAccountAnnotationTO.getHolderBeneficiario().getIdHolderPk()
																			, registerAccountAnnotationTO.getIdParticipantPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						if(lstResult.size()==GeneralConstants.ONE_VALUE_INTEGER){
							HolderAccount holderAccount=lstResult.get(0);
							registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountBenef(holderAccount);
						}
						registerAccountAnnotationTO.setLstHolderAccountBeneficiario(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerAccountAnnotationTO.setHolderBeneficiario(new Holder());	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	public void validateHolderSeller(){
		try {
			registerAccountAnnotationTO.setLstHolderAccountSeller(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountSeller(new HolderAccount());
			
			
			if( registerAccountAnnotationTO.getHolderSeller() != null &&  
				Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getHolderSeller().getIdHolderPk()) ){		
				if(!HolderStateType.REGISTERED.getCode().equals(registerAccountAnnotationTO.getHolderSeller().getStateHolder())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.SPLIT_COUPON_HOLDER_INVALID_STATE));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolderSeller(new Holder());
					return;
				}
				if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getIdParticipantSellerPk())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_PARTICIPANT_REQUIRED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.setHolderSeller(new Holder());				
				}else{
					List<HolderAccount> lstResult = dematerializationCertificateFacade.findHolderAccounts(registerAccountAnnotationTO.getHolderSeller().getIdHolderPk()
																			, registerAccountAnnotationTO.getIdParticipantSellerPk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
						if(lstResult.size()==GeneralConstants.ONE_VALUE_INTEGER){
							HolderAccount holderAccount=lstResult.get(0);
							registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccountSeller(holderAccount);
							
						}
						registerAccountAnnotationTO.setLstHolderAccountSeller(lstResult);
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_HOLDER_NOT_ACCOUNTS));
						JSFUtilities.showSimpleValidationDialog();
						registerAccountAnnotationTO.setHolderSeller(new Holder());	
					}
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Validate Duplication Certificate
	 * @throws ServiceException
	 */

	public void validateSecuritySerie() {
		String serialNumber = "";
		//buscar que la serie exista en la tabla securitySerialRange
		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && registerAccountAnnotationTO.getAccountAnnotationOperation().getSerialNumber() != null) {
			List<SecuritySerialRange> lstSecuritySerialRange = dematerializationCertificateFacade.findSecuritySerialRange(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk());
			if(lstSecuritySerialRange!=null) {
				
				List<SecuritySerialRange> lstRangeInSerie = new ArrayList<SecuritySerialRange>();
				for(SecuritySerialRange current: lstSecuritySerialRange) {
					if(current.getSerialRange().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSerialNumber())) {
						lstRangeInSerie.add(current);
					}
				}
				
				if(lstRangeInSerie.size()>0) {
					registerAccountAnnotationTO.getAccountAnnotationOperation().setLstSecuritySerialRange(lstRangeInSerie);
				}else {
					serialNumber = registerAccountAnnotationTO.getAccountAnnotationOperation().getSerialNumber();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setLstSecuritySerialRange(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							"El Valor "+registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk()+", no tiene registrado una serie "+serialNumber);
					JSFUtilities.showSimpleValidationDialog();
				}
				
			}else {
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						"El Valor "+registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk()+", no tiene registrado una serie/rango. Verifique ");
				JSFUtilities.showSimpleValidationDialog();
			}
			
		}else {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(null);
			registerAccountAnnotationTO.getAccountAnnotationOperation().setLstSecuritySerialRange(null);
		}
	}
	
	public void validateSecuritySubclase() {
		
	}
	
	public void validateDuplicationOfCertificate() throws ServiceException{
//	    String securityPk= registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk();
//	    List<AccountAnnotationOperation> lstAccountsAO= dematerializationCertificateFacade.findAccountAnnotationOperationBySecurity(securityPk);
//	    if(lstAccountsAO!=null && lstAccountsAO.size()>0){
//	    	for(AccountAnnotationOperation annotation: lstAccountsAO){
//	    		if(annotation.getCertificateNumber().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber())){
//	    			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
//	    			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
//							PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_SAME_SECURITY, new Object[]{securityPk}));
//	    			JSFUtilities.showSimpleValidationDialog();
//	    			break;
//	    		}
//	    	}
//	    }
		PhysicalBalanceDetail physicalBalanceDetail = null;
		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null){
			physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk(), 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber());
		}
				
			if (Validations.validateIsNotNull(physicalBalanceDetail)){
			System.out.println("=====================SI EXISTE EN FISICA=======================");
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, 
							new Object[]{registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber(), 
							registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk()}));
			JSFUtilities.showSimpleValidationDialog();
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
			return;	
				}else{
				System.out.println("=================NO EXISTE EN FISICA==================");	

			    List<Integer> states = new ArrayList<Integer>();
				states.add(DematerializationStateType.REIGSTERED.getCode());
				states.add(DematerializationStateType.APPROVED.getCode());
				states.add(DematerializationStateType.CONFIRMED.getCode());	
				states.add(DematerializationStateType.CERTIFIED.getCode());
				states.add(DematerializationStateType.REVIEWED.getCode());
				Integer requestState = null;
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null){
					requestState = dematerializationCertificateFacade.existOtherRequest(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity(),states,
							registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber());
				}
				
				if(Validations.validateIsNotNullAndNotEmpty(requestState) && 
						(DematerializationStateType.REIGSTERED.getCode().equals(requestState)
						||	DematerializationStateType.APPROVED.getCode().equals(requestState)
						||	DematerializationStateType.CONFIRMED.getCode().equals(requestState)
						||	DematerializationStateType.CERTIFIED.getCode().equals(requestState)
						|| DematerializationStateType.REVIEWED.getCode().equals(requestState))){
					Object[] bodyData = new Object[2];
					   bodyData[0] = registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateNumber();
					   bodyData[1] = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().getIdSecurityCodePk();   
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_SAME_SECURITY, 
									bodyData));
					registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
					JSFUtilities.showSimpleValidationDialog();
					return;	
				}
			}	
	}
	
	public void validateCertificateFrom() throws ServiceException{

		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() == null) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateFrom(null);
			return;
		}

		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getLstSecuritySerialRange() == null) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateFrom(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					"El valor no tiene registrado una Inversion minima");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		Boolean validateInRange = false;
		for (SecuritySerialRange current : registerAccountAnnotationTO.getAccountAnnotationOperation().getLstSecuritySerialRange()) { 
			if(	current.getNumberShareInitial() <= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom()  && 
				current.getNumberShareFinal() >= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() 	
			) {
				if( registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() != null ) {
					
					if(	current.getNumberShareInitial() <= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() && 
						current.getNumberShareFinal() >= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() 	
					) {

						BigDecimal totalBalance = new BigDecimal((registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() - registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom()) + 1 );
						registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(totalBalance);
		
						BigDecimal shareCapital = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue().multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
						registerAccountAnnotationTO.setShareCapital(shareCapital);
					}
				}
				validateInRange = true;
				break;
			}
		}
		
		
		
		if(!validateInRange){
			Integer fromTmp = registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom().intValue(); 
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateFrom(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					" la cantidad "+fromTmp+" desde, no coincide dentro del valor Inversion minima/maxima");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}

	}
	
	public void validateCertificateTo() throws ServiceException{

		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() == null) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateTo(null);
			return;
		}

		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getLstSecuritySerialRange() == null) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateTo(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					"El valor no tiene registrado una Inversion maxima");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		

		Boolean validateInRange = false;
		for (SecuritySerialRange current : registerAccountAnnotationTO.getAccountAnnotationOperation().getLstSecuritySerialRange()) { 
			if(	current.getNumberShareInitial() <= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() && 
				current.getNumberShareFinal() >= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() 	
			) {
				if( registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() != null ) {
					if(	current.getNumberShareInitial() <= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom()  && 
						current.getNumberShareFinal() >= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() 	
					) {
						BigDecimal totalBalance = new BigDecimal((registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() - registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom()) + 1 );
						registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(totalBalance);
						
						BigDecimal shareCapital = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue().multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance());
						registerAccountAnnotationTO.setShareCapital(shareCapital);
						
						validateInRange = true;
						break;
					}
				}
				
			}
		}
		

		if(!validateInRange){
			Integer toTmp = registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo().intValue(); 
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateTo(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					" la cantidad "+toTmp+" hasta, no coincide dentro del valor Inversion minima/maxima");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
	}
	
	public void validateSecurity() throws ServiceException{
		registerAccountAnnotationTO.setBlDPF(false);
		registerAccountAnnotationTO.setBlFixedIncome(false);
		registerAccountAnnotationTO.setBlCertificateNumber(false);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setExpeditionDate(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
		registerAccountAnnotationTO.setShareCapital(null);
		//filling market fact data
		registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		Security security = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity();

		registerAccountAnnotationTO.setSecurityClass(security.getSecurityClass());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityExpirationDate(security.getExpirationDate());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityCurrency(security.getCurrency());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityClass(security.getSecurityClass());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityNominalValue(security.getInitialNominalValue());

		registerAccountAnnotationTO.getAccountAnnotationOperation().setExpeditionDate(security.getIssuanceDate());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(security.getPhysicalBalance());
		
		//validate dematerialized 
		if(Validations.validateIsNotNull(security)
				&& Validations.validateIsNotNull(security.getIssuanceForm()))
			if(Validations.validateIsNullOrEmpty(security.getPhysicalBalance()) || 
					security.getPhysicalBalance().compareTo(BigDecimal.ZERO) <= 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "El valor seleccionado no tiene saldo Fisico, Por Favor Verifique");
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			security.setPhysicalAvailable(security.getPhysicalBalance());

			//registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(security.getSecuritySerial());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(security.getSubClass());
			
		/*	BigDecimal physicalAvailable=BigDecimal.ZERO;
			if(IssuanceType.DEMATERIALIZED.getCode().equals(security.getIssuanceForm())){
				physicalAvailable=security.getPhysicalBalance();
			}else{
				physicalAvailable=CommonsUtilities.subtractTwoDecimal(security.getPhysicalBalance(), security.getPhysicalDepositBalance(),1);
			}
			security.setPhysicalAvailable(physicalAvailable);
			if(security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
				blCertificateDisable=true;
			}
			else{
				blCertificateDisable=false;
			}
		}
		else{
			blCertificateDisable=false;
		}*/
			
		
		//valdar si esta depositado en la 
		
		JSFUtilities.resetComponent("frmDematerializationCertificate:txtExpedition");
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass())||SecurityClassType.DPA.getCode().equals(security.getSecurityClass())){
				List<Integer> states = new ArrayList<Integer>();
				states.add(DematerializationStateType.REIGSTERED.getCode());
				states.add(DematerializationStateType.APPROVED.getCode());
				states.add(DematerializationStateType.CONFIRMED.getCode());	
				states.add(DematerializationStateType.CERTIFIED.getCode());
				states.add(DematerializationStateType.REVIEWED.getCode());
				Integer requestState = dematerializationCertificateFacade.existOtherRequest(security,states,null);
				if(Validations.validateIsNotNullAndNotEmpty(requestState) && 
						(DematerializationStateType.REIGSTERED.getCode().equals(requestState)
						||	DematerializationStateType.APPROVED.getCode().equals(requestState)
						||	DematerializationStateType.CERTIFIED.getCode().equals(requestState)
						|| DematerializationStateType.REVIEWED.getCode().equals(requestState))){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_DPF_EXIST));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					return;	
				}
				else if(DematerializationStateType.CONFIRMED.getCode().equals(requestState)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_DPF_EXIST_DEMATERIALIZED));
					JSFUtilities.showSimpleValidationDialog();
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					return;	
				}							
			}
			if(security.getIndAuthorized().equals(0)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "El valor no se encuentra autorizado, Verifique por favor.");
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			if( !(SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity()) || 
					SecurityStateType.GUARDA_EXCLUSIVE.getCode().equals(security.getStateSecurity()) || 
					SecurityStateType.GUARDA_MIXTA.getCode().equals(security.getStateSecurity()) )
			){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			if(SecuritySourceType.FOREIGN.getCode().equals(security.getSecuritySource())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_FOREIGN));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			Issuance issuance = dematerializationCertificateFacade.findIssuanceAndIssuer(security.getIdSecurityCodePk());
			if(issuance == null) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), "Erro de Inconsistencia, el valor no tiene una emision");
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			security.setIssuance(issuance);
			security.setIssuer(issuance.getIssuer());
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().setIssuer(issuance.getIssuer());
			registerAccountAnnotationTO.getAccountAnnotationOperation().setIssuer(issuance.getIssuer());
			
			/*if(IssuanceType.PHYSICAL.getCode().equals(issuance.getIssuanceType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SECURITY_DEMATERIALIZED)
				);
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			*/
			
			if(!IssuerStateType.REGISTERED.getCode().equals(issuance.getIssuer().getStateIssuer())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ISSUER_BLOCKED));
				JSFUtilities.showSimpleValidationDialog();
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
				return;
			}
			if(InstrumentType.FIXED_INCOME.getCode().equals(security.getInstrumentType())){
				registerAccountAnnotationTO.setBlFixedIncome(true);
//				registerAccountAnnotationTO.setBlVariableIncome(true);
			}
//			else if(InstrumentType.VARIABLE_INCOME.getCode().equals(security.getInstrumentType())){
//				registerAccountAnnotationTO.setBlVariableIncome(true);
//			}
				
			//comentado temporalmente
			/*
			if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass()) || SecurityClassType.DPA.getCode().equals(security.getSecurityClass())){
				registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(BigDecimal.ONE);
				registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(security.getSecuritySerial());
				registerAccountAnnotationTO.setBlDPF(true);	
				registerAccountAnnotationTO.setBlCertificateNumber(true);
				calculateShareCapital();
				if(blError){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					registerAccountAnnotationTO.getAccountAnnotationOperation().setTotalBalance(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSerialNumber(null);
					registerAccountAnnotationTO.setBlDPF(false);	
					registerAccountAnnotationTO.setBlCertificateNumber(false);
					return;
				}
				//get the positions from 5 to 10 and if there are zeros forward then delete them
				String certificateNumber=null;
				if(security.getIdSecurityCodePk().length()>=10)
				{
					certificateNumber= security.getIdSecurityCodeOnly().substring(4,10); 
					if(Validations.validateIsNumeric(certificateNumber))
					{
						Integer iCertificateNumber=Integer.parseInt(certificateNumber);
						certificateNumber=String.valueOf(iCertificateNumber);
					}
				}
				else{
					certificateNumber= security.getIdSecurityCodeOnly().substring(4,security.getIdSecurityCodePk().length()); 
				}
				registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(certificateNumber);
			}else{
				registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
			}
			*/
			
			
			//VALIDAR SI EXISTE UN HECHO DE MERCADO ASOCIADO AL VALOR
			List<HolderMarketFactBalance> lstMarketFact =null;
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				lstMarketFact = dematerializationCertificateFacade.getMarketFactFromSecurity(security.getIdSecurityCodePk());
				
				if(lstMarketFact.size()==GeneralConstants.ONE_VALUE_INTEGER){
					HolderMarketFactBalance hmfb= lstMarketFact.get(0);
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketDate(hmfb.getMarketDate());
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketPrice(hmfb.getMarketPrice());
					registerAccountAnnotationTO.getAccountAnnotationMarketFact().setMarketRate(hmfb.getMarketRate());
				}
			}
			
			List<ParameterTable> lstMotivesPerSecurity = dematerializationCertificateFacade.getAllowedDematerializationMotives(security);
			 
			registerAccountAnnotationTO.setLstMotives(lstMotivesPerSecurity);
			
			if( security.getStateSecurity().equals(SecurityStateType.GUARDA_EXCLUSIVE.getCode()) ||
				security.getStateSecurity().equals(SecurityStateType.GUARDA_MIXTA.getCode()) ) {
				registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
			}else {
				registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
			}
			
		}
	}
	
	/**
	 * Before save.
	 */
	public void beforeSave(){
		
		Integer diasHabiles = 3;
		
		ParameterTableTO filter = new ParameterTableTO();
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive())) {
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
				filter.setParameterTablePk(2911);
			else
				filter.setParameterTablePk(2912);
		}else
			filter.setParameterTablePk(2912);
		List<ParameterTable> param = paramServiceBean.getListParameterTableServiceBean(filter);
		if(param!=null && param.size()>0) {
			diasHabiles = param.get(0).getShortInteger();
		}
		
		Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles); 	//generalParameterFacade.workingDateCalculateServiceFacade(CommonsUtilities.currentDate(), 3);
	
		if( ( registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons() == null || 
			  registerAccountAnnotationTO.getAccountAnnotationOperation().getIndExpirationCupons().equals(0) ) && 
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate() !=null && 
			maxAvailableDate.after(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate()) ) {
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "No puede registrarse titulos cuya fecha vencimiento este dentro de los "+diasHabiles+" dias siguientes");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if( registerAccountAnnotationTO.getAccountAnnotationOperation().getIndSerializable().equals(0) && //NO SERIALIZADO
			!(registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons() != null &&
			registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons().size() > 0) ) {
			

			if(!registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityClass().equals(SecurityClassType.CHQ.getCode())) {
				
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "No se a ingresado cupones, Verifique por favor");
			JSFUtilities.showSimpleValidationDialog();
			return;

			}
		}
		
		AccountAnnotationOperation accountAnnotationOperation = certificateDepositeBeanFacade.findAccountAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
		if(accountAnnotationOperation != null) {
			
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, "Ya se encuentra registrado un titulo con las mismas caracteristicas, Verifique por favor");
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		
		if(Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount())
				|| Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ACCOUNT_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else{
			if (!registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode())) {
				boolean blTypeCurrency=BooleanType.NO.getBooleanValue();
						if(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIndPdd()== null || registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIndPdd()== 0) {
							List<HolderAccountBank> holderAccount = certificateDepositeBeanFacade.findHolderAccountBank(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk(), HolderAccountStateBankType.REGISTERED.getCode());
							if(Validations.validateIsNotNull(holderAccount)) {
								for (HolderAccountBank holderAccountBank : holderAccount) {
									if (holderAccountBank.getCurrency().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityCurrency())) {
										blTypeCurrency = BooleanType.YES.getBooleanValue();
									}
								}
							}
						}else {
							List<InstitutionBankAccount> lsInstitutionBankAccount = certificateDepositeBeanFacade.findInstitutionBankAccount(registerAccountAnnotationTO.getIdParticipantPk(), InstitutionBankAccountsType.STATE.getCode());
							if(Validations.validateIsNotNull(lsInstitutionBankAccount)) {
								for (InstitutionBankAccount institutionBank : lsInstitutionBankAccount) {
									if (institutionBank.getCurrency().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityCurrency())) {
										blTypeCurrency = BooleanType.YES.getBooleanValue();
									}
								}
							}
						}
				if (!blTypeCurrency) {
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_BANK_NOT_FOUND));
				JSFUtilities.executeJavascriptFunction("PF('cnfWnotAccountDialog').show();");
				return;
				}	
			}
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER_BANK_SHARES));
			}
			else{			
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER));
			}	
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
	}
	
	
	public void showConfirmDialog() {
		if (registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity() != null
				&& registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()) {
			JSFUtilities.showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER_BANK_SHARES));
		} else {
			JSFUtilities.showMessageOnDialog(
					PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM),
					PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REGISTER));
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}

	/**
	 * Show request.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @return the string
	 */
	public String showRequest(AccountAnnotationOperation accountAnnotationOperation){
		try {
			setFlagsFalse();
			indAlternativeCode=false;
			if(DematerializationStateType.ANNULLED.getCode().equals(accountAnnotationOperation.getState()) ||
			   DematerializationStateType.CANCELLED.getCode().equals(accountAnnotationOperation.getState()) ||
			   DematerializationStateType.REJECTED.getCode().equals(accountAnnotationOperation.getState())){
				blRenderedMotives = true;
			}
			fillView(accountAnnotationOperation);
			registerAccountAnnotationTO.setIndGrav(BooleanType.YES.getBooleanValue());
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Save request.
	 */
	@LoggerAuditWeb
	public void saveRequest(){
		try {
			AccountAnnotationOperation accountAnnotationOperation = dematerializationCertificateFacade.registerAccountAnnotationOperation(registerAccountAnnotationTO);

			Long idAccountAnnotationOperationPk = accountAnnotationOperation.getCustodyOperation().getOperationNumber();

		    BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REGISTER.getCode());
			if(accountAnnotationOperation!=null) {
				Object[] parameters = { idAccountAnnotationOperationPk };
				
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,registerAccountAnnotationTO.getIdParticipantPk() ,parameters);
			}
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REGISTER, new Object[]{idAccountAnnotationOperationPk.toString()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}
	}
	
	public void modifyCurrentExpirationDate(AccountAnnotationCupon currentAccountAnnotationCupon) {
		Date previousExpirationDate = null;
		//currentAccountAnnotationCupon.getBeginingDate();
		
		List<AccountAnnotationCupon> lstAccountAnnotationCupon = registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons();
		AccountAnnotationCupon lastEnableCupon = null;
		if(currentAccountAnnotationCupon.getCuponNumber() > 0){
			for(int i=0; i<lstAccountAnnotationCupon.size(); i++) {
				if( lstAccountAnnotationCupon.get(i).getCuponNumber().equals(currentAccountAnnotationCupon.getCuponNumber()) ) {
					break;
				}
				lastEnableCupon = 	lstAccountAnnotationCupon.get(i);
			}
		}
		
		if(lastEnableCupon!=null) {
			previousExpirationDate = lastEnableCupon.getExpirationDate();
		}else {
			previousExpirationDate = currentAccountAnnotationCupon.getBeginingDate();
		}
		
		try {
			
			currentAccountAnnotationCupon = generateCuponWithData(currentAccountAnnotationCupon.getCuponNumber(), previousExpirationDate,currentAccountAnnotationCupon.getExpirationDate(),currentAccountAnnotationCupon);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Clean register.
	 * @throws ServiceException 
	 */
	public void cleanRegister() throws ServiceException{
		JSFUtilities.resetViewRoot();
		registerAccountAnnotationTO.setAccountAnnotationOperation(new AccountAnnotationOperation());
		registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setHolderAccount(new HolderAccount());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecuritiesManager(new SecuritiesManager());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setIssuer(new Issuer());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setCreditorSource(null);
		registerAccountAnnotationTO.setHolder(new Holder());		
		registerAccountAnnotationTO.setLstHolderAccount(null);
		ubicacionGeografica();
		if(!blParticipant)
			registerAccountAnnotationTO.setIdParticipantPk(null);
		else
			registerAccountAnnotationTO.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());
		registerAccountAnnotationTO.setSecurityClass(null);
		registerAccountAnnotationTO.setShareCapital(null);
		registerAccountAnnotationTO.setBlFixedIncome(false);
		registerAccountAnnotationTO.setBlOthers(false);
		registerAccountAnnotationTO.setBlRejected(false);
		registerAccountAnnotationTO.setBlLien(false);
		tmpProcessFile = null;
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	public String validateApprove(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())&&
							Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
						if(selectedAAO.getSecurity() != null){
							PhysicalBalanceDetail physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
																					selectedAAO.getSecurity().getIdSecurityCodePk(), 
																					selectedAAO.getCertificateNumber());
							Object[] bodyData = new Object[2];
							   bodyData[0] = selectedAAO.getCertificateNumber();   
							   bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
							if (Validations.validateIsNotNull(physicalBalanceDetail)){
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return null;
							}
						}
					}

						if(DematerializationStateType.REIGSTERED.getCode().equals(selectedAAO.getState())){
							blApprove = true;
							blDisabledFields=false;
							indAlternativeCode=true;
//							indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
							JSFUtilities.showSimpleValidationDialog();
						}
				}
				else{
					String strOperationNumber= GeneralConstants.EMPTY_STRING;
					for(AccountAnnotationOperation objAao : checkAccountAnotation)
					{
						PhysicalBalanceDetail physicalBalanceDetail = null;
						
						Object[] bodyData = new Object[2];
						
						if(objAao.getSecurity()!=null){
							physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
									objAao.getSecurity().getIdSecurityCodePk(), 
									objAao.getCertificateNumber());							
						
							bodyData[0] = objAao.getCertificateNumber();   
							bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
						}						
						
						if (Validations.validateIsNotNull(physicalBalanceDetail)) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
						if (DematerializationStateType.REIGSTERED.getCode().equals(objAao.getState())) {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}	
					}
					blApprove = true;
					JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
										PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_APPROVE, 
													new Object[]{strOperationNumber}));
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Validate annul.
	 *
	 * @return the string
	 */
	public String validateAnnul(){
			currentMotive = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_LBL_MOTIVE_ANULL);
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER) {
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(DematerializationStateType.REIGSTERED.getCode().equals(selectedAAO.getState())){
						blAnnul = true;
						blDisabledFields=false;
						indAlternativeCode=false;
						fillView(selectedAAO);
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
						JSFUtilities.showSimpleValidationDialog();
					}
				} else {
					for(AccountAnnotationOperation aao: checkAccountAnotation){
						if(!DematerializationStateType.REIGSTERED.getCode().equals(aao.getState())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REGISTER));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blAnnul = true;
					indAlternativeCode=false;
					selectedMotive();
				}
			} else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	public String validateModify() {
		
		currentMotive = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_LBL_MOTIVE_ANULL);
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER) {
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if( DematerializationStateType.REIGSTERED.getCode().equals(selectedAAO.getState()) ||
						DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())
						){
						blAnnul = true;
						blDisabledFields=false;
						indAlternativeCode=false;
						fillView(selectedAAO);
						return MODIFY_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REJECT));
						JSFUtilities.showSimpleValidationDialog();
					}
				} else {
					for(AccountAnnotationOperation aao: checkAccountAnotation){
						if(		DematerializationStateType.REIGSTERED.getCode().equals(aao.getState()) ||
								DematerializationStateType.APPROVED.getCode().equals(aao.getState())
						){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REJECT));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blAnnul = true;
					indAlternativeCode=false;
					selectedMotive();
				}
			} else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
			return null;
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())
							&& Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
					PhysicalBalanceDetail physicalBalanceDetail = null;
					if(selectedAAO.getSecurity()!=null){
						physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
								selectedAAO.getSecurity().getIdSecurityCodePk(), 
								selectedAAO.getCertificateNumber());
					}
					
						if (Validations.validateIsNotNull(physicalBalanceDetail)){
							   Object[] bodyData = new Object[2];
							   bodyData[0] = selectedAAO.getCertificateNumber();   
							   bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					//--TMP COMMENT
					if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())){
					//if(DematerializationStateType.CONFIRMED.getCode().equals(selectedAAO.getState())){
						/*if(selectedAAO.getSecurity()!=null && (SecurityClassType.DPF.getCode().equals(selectedAAO.getSecurity().getSecurityClass())||SecurityClassType.DPA.getCode().equals(selectedAAO.getSecurity().getSecurityClass()))){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNTANNOTATION_INVALID_CONFIRM_DPF));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}else{*/
						PayrollDetail payrollDetail = dematerializationCertificateFacade.getPayrollDetailOnliCapitalValid(selectedAAO.getIdAnnotationOperationPk());
						if(payrollDetail == null || payrollDetail.getPayrollState() == null || !payrollDetail.getPayrollState().equals(PayrollDetailStateType.VAULT.getCode())) {//RECEIVED
			
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
									, "No se a completado el flujo de retiro para ser Confirmado");
							JSFUtilities.showValidationDialog();
							setViewOperationType(ViewOperationsType.CONSULT.getCode());
							return null;
						}
							blConfirm = true;
							blDisabledFields = false;
							indAlternativeCode=true;
//							indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						//}
					}else if(DematerializationStateType.CERTIFIED.getCode().equals(selectedAAO.getState())){
						blConfirm = true;
						indAlternativeCode=true;
						fillView(selectedAAO);
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
					}
			} else {
				String strOperationNumber= GeneralConstants.EMPTY_STRING;
				for(AccountAnnotationOperation objAao : checkAccountAnotation)
				{
					PhysicalBalanceDetail physicalBalanceDetail = null;
					Object[] bodyData = new Object[2];
					if(objAao.getSecurity()!=null){
						physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
								objAao.getSecurity().getIdSecurityCodePk(), 
								objAao.getCertificateNumber());
						
						bodyData[0] = objAao.getCertificateNumber();   
						bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
					}
									
					if (Validations.validateIsNotNull(physicalBalanceDetail)){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
					
					if (DematerializationStateType.APPROVED.getCode().equals(objAao.getState())){
						/*if(objAao.getSecurity()!=null && (SecurityClassType.DPF.getCode().equals(objAao.getSecurity().getSecurityClass())
								|| SecurityClassType.DPA.getCode().equals(objAao.getSecurity().getSecurityClass()))){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNTANNOTATION_INVALID_CONFIRM_DPF));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						} else*/ //{
						PayrollDetail payrollDetail = dematerializationCertificateFacade.getPayrollDetailOnliCapitalValid(objAao.getIdAnnotationOperationPk());
						if(payrollDetail == null || payrollDetail.getPayrollState() == null || !payrollDetail.getPayrollState().equals(PayrollDetailStateType.VAULT.getCode())) {//RECEIVED
			
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
									, "No se a completado el flujo de retiro para ser Confirmado");
							JSFUtilities.showValidationDialog();
							setViewOperationType(ViewOperationsType.CONSULT.getCode());
							return null;
						}
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						//}
					} else if(DematerializationStateType.CERTIFIED.getCode().equals(objAao.getState())){
						if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
							strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
						} else {
							strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
						}
					} else {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}	
				}
				blConfirm = true;
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CONFIRM, 
												new Object[]{strOperationNumber}));
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	public String validateCertify(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())
							&& Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
							PhysicalBalanceDetail physicalBalanceDetail = null;
							Object[] bodyData = new Object[2];
							if(selectedAAO.getSecurity()!=null){
								physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
										selectedAAO.getSecurity().getIdSecurityCodePk(), 
										selectedAAO.getCertificateNumber());
								
								 bodyData[0] = selectedAAO.getCertificateNumber();   
								 bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
							}													
							  
							if (Validations.validateIsNotNull(physicalBalanceDetail)){
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return null;
							}
					}
				if(selectedAAO.getSecurity()!=null && (selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))){	
						if(DematerializationStateType.REVIEWED.getCode().equals(selectedAAO.getState())){
							blCertify = true;
							blDisabledFields = false;
							indAlternativeCode=true;
//							indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_CERTIFIED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
			} else {
				String strOperationNumber= GeneralConstants.EMPTY_STRING;
				for(AccountAnnotationOperation objAao : checkAccountAnotation)
				{
					PhysicalBalanceDetail physicalBalanceDetail= null; 
					Object[] bodyData = new Object[2];
					if(objAao.getSecurity()!=null){
						physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
								objAao.getSecurity().getIdSecurityCodePk(), 
								objAao.getCertificateNumber());
						
						bodyData[0] = objAao.getCertificateNumber();   
						bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
					}
									
					if (Validations.validateIsNotNull(physicalBalanceDetail)) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
					
					if(objAao.getSecurity()!=null && (objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
					   objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))) 
					{
						if (DematerializationStateType.REVIEWED.getCode().equals(objAao.getState())) {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_CERTIFIED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					} else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}	
				}
				blCertify = true;
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CERTIFY), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CERTIFY, 
												new Object[]{strOperationNumber}));
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		return null;
	}
	
	public String validateReview(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(Validations.validateIsNotNull(selectedAAO.getCertificateNumber())
							&& Validations.validateIsNumeric(selectedAAO.getCertificateNumber())){
						PhysicalBalanceDetail physicalBalanceDetail= null;
						Object[] bodyData = new Object[2];
						if(selectedAAO.getSecurity()!=null){
							physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
									selectedAAO.getSecurity().getIdSecurityCodePk(), 
									selectedAAO.getCertificateNumber());
							
							bodyData[0] = selectedAAO.getCertificateNumber();   
							bodyData[1] = selectedAAO.getSecurity().getIdSecurityCodePk();
						}
						
						if (Validations.validateIsNotNull(physicalBalanceDetail)){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					if(selectedAAO.getSecurity()!=null && (selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())|| 
					   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())))
					{
						if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())){
							blReview = true;
							blDisabledFields = false;
							indAlternativeCode=true;
//								indLien=true;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_REVIEW));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
			} else{
				String strOperationNumber= GeneralConstants.EMPTY_STRING;
				for(AccountAnnotationOperation objAao : checkAccountAnotation)
				{
					PhysicalBalanceDetail physicalBalanceDetail= null;
					Object[] bodyData = new Object[2];
					if(objAao.getSecurity()!=null){
						physicalBalanceDetail = dematerializationCertificateFacade.validateSecurityCertificate(
								objAao.getSecurity().getIdSecurityCodePk(), 
								objAao.getCertificateNumber());
						
						bodyData[0] = objAao.getCertificateNumber();   
						bodyData[1] = objAao.getSecurity().getIdSecurityCodePk();
					}
									
					if (Validations.validateIsNotNull(physicalBalanceDetail)) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											PropertiesUtilities.getMessage(PropertiesConstants.ALERT_DUPLICATE_CERTIFICATE_NUMBER_PHYSICAL, bodyData));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
					if(objAao.getSecurity()!=null && (objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())|| 
					   objAao.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())))
					{
						if (DematerializationStateType.APPROVED.getCode().equals(objAao.getState())) {
							if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
								strOperationNumber+=objAao.getCustodyOperation().getOperationNumber();
							} else {
								strOperationNumber+=GeneralConstants.STR_COMMA + objAao.getCustodyOperation().getOperationNumber();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					} else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_REVIEW));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}	
				}
				blReview = true;
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REVIEW, 
												new Object[]{strOperationNumber}));
				}		
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
		return null;
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){

		currentMotive = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_LBL_MOTIVE_ANULL);
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER) {
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())){
						blReject = true;
						blDisabledFields=false;
						indAlternativeCode=false;
						fillView(selectedAAO);
						return VIEW_MAPPING;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
						JSFUtilities.showSimpleValidationDialog();
					}
				} else {
					for(AccountAnnotationOperation aao: checkAccountAnotation){
						if(!DematerializationStateType.APPROVED.getCode().equals(aao.getState())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blReject = true;
					indAlternativeCode=false;
					selectedMotive();
				}
			} else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;

		/*currentMotive = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_LBL_MOTIVE_REJECT);
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER) {
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(selectedAAO.getSecurity()!=null && (selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
					   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())))
					{
						if(!DematerializationStateType.CERTIFIED.getCode().equals(selectedAAO.getState())){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					} else {
						if(!DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState())) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM)
									, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blReject = true;
					blDisabledFields=false;
					indAlternativeCode=false;
					fillView(selectedAAO);
					return VIEW_MAPPING;
				} else {
					for(AccountAnnotationOperation aao: checkAccountAnotation){
						if(aao.getSecurity()!=null && (aao.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||aao.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())))
						{
							if(DematerializationStateType.CERTIFIED.getCode().equals(aao.getState())){
								blReject = true;
								indAlternativeCode=false;
								selectedMotive();
							}else{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_APPROVED_OR_CERTIFIED));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
						else
						{	
							if(DematerializationStateType.APPROVED.getCode().equals(aao.getState()))
							{
								blReject = true;
								indAlternativeCode=false;
								selectedMotive();
							}
							else
							{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;*/
	}
	
	public String validateCancel(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNotNullAndNotEmpty(checkAccountAnotation) && checkAccountAnotation.length > 0){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];
					if(selectedAAO.getSecurity()!=null && (selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
					   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())))
					{
						if(DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState()) || 
						   DematerializationStateType.REVIEWED.getCode().equals(selectedAAO.getState()))
						{
							blCancel = true;
							indAlternativeCode=false;
							fillView(selectedAAO);
							return VIEW_MAPPING;
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
							JSFUtilities.showSimpleValidationDialog();
						}
					 }else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CANCEL));
							JSFUtilities.showSimpleValidationDialog();
							return null;
					}	
				}
				else{
					for(AccountAnnotationOperation selectedAAO: checkAccountAnotation){
						if(selectedAAO.getSecurity()!=null && (selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())||
						   selectedAAO.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())))
						{
							if(!DematerializationStateType.APPROVED.getCode().equals(selectedAAO.getState()) && 
							   !DematerializationStateType.REVIEWED.getCode().equals(selectedAAO.getState()))
							{
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
													PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_INVALID_STATE_REVIEWED));
								JSFUtilities.showSimpleValidationDialog();
							}
						} else {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_ANNOTATION_DPF_STATE_CANCEL));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					blCancel = true;
					indAlternativeCode=false;
					selectedMotive();
				}		
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	/**
	 * Reject others.
	 */
	public void rejectOthers(){
		registerAccountAnnotationTO.getAccountAnnotationOperation().setComments(null);
		registerAccountAnnotationTO.setBlOthers(false);
		if(DematerializationRejectMotiveType.OTHERS.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject()))
			registerAccountAnnotationTO.setBlOthers(true);		
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		String bodyMessage = null, headerMessage = null;
		String strOperation=GeneralConstants.EMPTY_STRING;
		boolean blValidate=BooleanType.YES.getBooleanValue();
		if(blApprove){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_APPROVE;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_APPROVE_BANK_SHARES;
			}
			else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_APPROVE;
			}
			
		}else if(blAnnul){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_ANNUL;
			for(AccountAnnotationOperation objAao : checkAccountAnotation)
			{
				strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				if(objAao.getSecurity()!=null && !objAao.getSecurity().isBankStockExchange()){
					blValidate=BooleanType.NO.getBooleanValue();
				}
			}
			if(blValidate==BooleanType.YES.getBooleanValue()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_ANNUL_BANK_SHARES;
			}
			else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_ANNUL;
			}
			if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
				strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), 
								PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
			blValidate=BooleanType.YES.getBooleanValue();
			return;
		}else if(blConfirm){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CONFIRM_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CONFIRM;
			}
		}else if(blReject){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REJECT;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT;
			}
			
			for(AccountAnnotationOperation objAao : checkAccountAnotation)
			{
				strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				if(objAao.getSecurity()!=null && !objAao.getSecurity().isBankStockExchange()){
					blValidate=BooleanType.NO.getBooleanValue();
				}
			}
			if(blValidate==BooleanType.YES.getBooleanValue()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT_BANK_SHARES;
			}
			else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REJECT;
			}
			if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
				strOperation=strOperation.substring(0,strOperation.length()-1);
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), 
								PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
			blValidate=BooleanType.YES.getBooleanValue();
			return;
		}else if(blReview){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REVIEW;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REVIEW_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_REVIEW;
			}			
		}else if(blCertify){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CERTIFY;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CERTIFY_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CERTIFY;
			}
		}else if(blCancel){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CANCEL;
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
					registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL_BANK_SHARES;
			}else{
				bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL;
			}			
		}
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), 
							PropertiesUtilities.getMessage(bodyMessage, new Object[]{registerAccountAnnotationTO.getAccountAnnotationOperation().
									getCustodyOperation().getOperationNumber().toString()}));
	}
	
	private Client createClientJerser() throws NoSuchAlgorithmException,KeyManagementException{
		return Client.create();
	}

	@LoggerAuditWeb
	public void executeBatchCreateOtcOperationWithAccountAnnotation(String listIdAccountAnnotationPk ) throws KeyManagementException, UniformInterfaceException, ClientHandlerException, NoSuchAlgorithmException {
		
    	StringBuilder urlResource = new StringBuilder();
        urlResource.append(serverPath).append("/").append(ModuleWarType.SETTLEMENT.getValue()).
        append("/resources/ExecOtcOperationResource/createOtcOperationController").
        append("/").append(userInfo.getUserAccountSession().getUserName()).append("/").append(userInfo.getUserAccountSession().getIpAddress()).append("/").append(listIdAccountAnnotationPk);
        createClientJerser().resource(urlResource.toString()).type("application/json").get(ClientResponse.class);
        
	}
	
	/**
	 * Do action.
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	
	public String descriptionMotiveReject(Integer motiveParameterTablePk) {
		String motiveDescription = "";
		if(registerAccountAnnotationTO.getLstRejectMotive() !=null && registerAccountAnnotationTO.getLstRejectMotive().size()>0) {
			for (ParameterTable parameter : registerAccountAnnotationTO.getLstRejectMotive()) {
				if(parameter.getParameterTablePk().equals(motiveParameterTablePk)) {
					return parameter.getDescription();
				}
			}
		}
		
		return motiveDescription;
	}
	
	@LoggerAuditWeb
	public void doAction() throws KeyManagementException, NoSuchAlgorithmException{
		try {
			String bodyMessage = null,strOperation=GeneralConstants.EMPTY_STRING;
			if(blApprove){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.indApproveAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
							registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_APPROVE_BANK_SHARES;
					}
					else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_APPROVE;
					}
				} else {
					strOperation= dematerializationCertificateFacade.indApproveMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_APPROVE;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					return;
				}
			}else if(blAnnul){
				for(AccountAnnotationOperation acco : checkAccountAnotation){
					acco.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					acco.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					dematerializationCertificateFacade.annulAnnotationOperation(acco);
					strOperation+=acco.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.substring(0,strOperation.length()-1);
				}
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
						registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_ANNUL_BANK_SHARES;
				}
				else{
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_ANNUL;
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
									PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
				searchRequest();
				return;
			}else if(blConfirm){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					//--TMP COMMENT
					dematerializationCertificateFacade.confirmAnnotationOperationWithOutCertificate(registerAccountAnnotationTO.getAccountAnnotationOperation());
					
				    strOperation = registerAccountAnnotationTO.getAccountAnnotationOperation().getIdAnnotationOperationPk().toString();
				    //ejecutar solo cuando es ingreso por otc
				    //executeBatchCreateOtcOperationWithAccountAnnotation(strOperation);
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
							registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM;
					}
					
				} else {
					
					strOperation= dematerializationCertificateFacade.confirmMultipleAnnotationOperationWithOutCertificate(checkAccountAnotation);
					//ejecutar solo cuando es ingreso por otc
				    //executeBatchCreateOtcOperationWithAccountAnnotation(strOperation);
				    
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CONFIRM;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					return;
				}
				
				
			}else if(blReject){
				for(AccountAnnotationOperation acco : checkAccountAnotation){
					acco.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					acco.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					dematerializationCertificateFacade.rejectAnnotationOperation(acco);
					strOperation+=acco.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					
					
					try {
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REJECT.getCode());//CERTIFICATE_DEMATERIALIZATION_REJECT
	
						Object[] parameters = new Object[2];
						parameters[0] = acco.getCustodyOperation().getOperationNumber();
						parameters[1] = descriptionMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
						
						
						//----------------------
						
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, acco.getHolderAccount().getParticipant().getIdParticipantPk() ,parameters);
						/*
						notificationServiceFacade.sendNotificationEmailParticipantNoConfig(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, 
								   "RECHAZO DE TITULO",
								   "La solicitud Nro %s ha sido rechazada por motivo %s ",
								   acco.getHolderAccount().getParticipant().getIdParticipantPk(), 
								   parameters);
						*/
						
					}catch (Exception e) {
						System.out.println("*** no se pudo enviar la notificacion:: "+e.getMessage());
					}
					
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.substring(0,strOperation.length()-1);
				}
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
						registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REJECT_BANK_SHARES;
				}
				else{
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REJECT;
				}
				
				
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
									PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
				searchRequest();
				return;
				
				
				/*
				for(AccountAnnotationOperation acco : checkAccountAnotation){
					acco.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					acco.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					dematerializationCertificateFacade.rejectAnnotationOperation(acco);
					strOperation+=acco.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.substring(0,strOperation.length()-1);
				}
				if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
						registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REJECT_BANK_SHARES;
				}else{
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REJECT;
				}
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
									PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
				searchRequest();
				return;
				*/
				
			}else if(blReview){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.reviewAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
							registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REVIEW_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REVIEW;
					}
				} else {
					strOperation= dematerializationCertificateFacade.reviewMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_REVIEW;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					return;
				}
			}else if(blCertify){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					dematerializationCertificateFacade.certifyAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity()!=null && 
							registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CERTIFY_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CERTIFY;
					}
				} else {
					strOperation= dematerializationCertificateFacade.certifyMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CERTIFY;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					return;
				}
			}else if(blCancel){
				if(checkAccountAnotation.length==GeneralConstants.ONE_VALUE_INTEGER){
					AccountAnnotationOperation selectedAAO=checkAccountAnotation[0];	
					selectedAAO.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
					selectedAAO.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());			
					dematerializationCertificateFacade.cancelAnnotationOperation(selectedAAO);
					if(selectedAAO.getSecurity()!=null && selectedAAO.getSecurity().isBankStockExchange()){
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CANCEL_BANK_SHARES;
					}else{
						bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CANCEL;
					}
				} else {
					for (AccountAnnotationOperation selectedAAO: checkAccountAnotation) {
						selectedAAO.setMotiveReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject());
						selectedAAO.setMotiveDescReject(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject());
					}
					strOperation= dematerializationCertificateFacade.cancelMultipleAnnotationOperation(checkAccountAnotation);
					bodyMessage = PropertiesConstants.ACCOUNT_ANNOTATION_SUCCESS_CANCEL;
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
										PropertiesUtilities.getMessage(bodyMessage, new Object[]{strOperation}));
					searchRequest();
					return;
				}
			}
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
								PropertiesUtilities.getMessage(bodyMessage, new Object[]{registerAccountAnnotationTO.getAccountAnnotationOperation().
																getCustodyOperation().getOperationNumber().toString()}));
			searchRequest();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		}		
	}
	
	/**
	 * Fill view.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @throws ServiceException the service exception
	 */
	
	
	private void fillView(AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		registerAccountAnnotationTO = new RegisterAccountAnnotationTO();
		registerAccountAnnotationTO.setLstSecuritiesManager(accountsFacade.getSecuritiesManagers());
		registerAccountAnnotationTO.setAccountAnnotationOperation(accountAnnotationOperation);
		GeographicLocationTO filter = new GeographicLocationTO();
		filter.setGeographicLocationType(GeographicLocationType.DEPARTMENT.getCode());
		filter.setIdLocationReferenceFk(countryResidence);
		listGeographicLocationDpto = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		filter.setGeographicLocationType(GeographicLocationType.PROVINCE.getCode());
		filter.setIdLocationReferenceFk(accountAnnotationOperation.getDepartment());
		listGeographicLocationProvince = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		filter.setGeographicLocationType(GeographicLocationType.DISTRICT.getCode());
		filter.setIdLocationReferenceFk(accountAnnotationOperation.getProvince());
		listGeographicLocationMunicipality = dematerializationCertificateFacade.listaUbiccioGeografica(filter);
		if(accountAnnotationOperation.getDepartment()==null){
			listGeographicLocationDpto=null;
		}else{ 
			registerAccountAnnotationTO.setDepartment(accountAnnotationOperation.getDepartment());
		}
		if(accountAnnotationOperation.getProvince()==null){
			listGeographicLocationProvince=null;
		}else{ 
			registerAccountAnnotationTO.setProvince(accountAnnotationOperation.getProvince());
		}
		if(accountAnnotationOperation.getMunicipality()==null){
			listGeographicLocationMunicipality=null;
		}else{
			registerAccountAnnotationTO.setMunicipality(accountAnnotationOperation.getMunicipality());
		}
		

		accountAnnotationOperation.setHolderAccount(dematerializationCertificateFacade.findHolderAccount(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk()));
		for(HolderAccountDetail holderAccDeta:accountAnnotationOperation.getHolderAccount().getHolderAccountDetails()){
			if(BooleanType.YES.getCode().equals(holderAccDeta.getIndRepresentative())){
				registerAccountAnnotationTO.setHolder(holderAccDeta.getHolder());
				break;
			}
		}
		
		if(accountAnnotationOperation.getHolderAccountBenef()!=null) {
			accountAnnotationOperation.setHolderAccountBenef(dematerializationCertificateFacade.findHolderAccount(accountAnnotationOperation.getHolderAccountBenef().getIdHolderAccountPk()));
			for(HolderAccountDetail holderAccDeta:accountAnnotationOperation.getHolderAccountBenef().getHolderAccountDetails()){
				if(BooleanType.YES.getCode().equals(holderAccDeta.getIndRepresentative())){
					registerAccountAnnotationTO.setHolderBeneficiario(holderAccDeta.getHolder());
					break;
				}
			}
		}
		
		if(accountAnnotationOperation.getHolderAccountDebtor()!=null) {
			accountAnnotationOperation.setHolderAccountDebtor(dematerializationCertificateFacade.findHolderAccount(accountAnnotationOperation.getHolderAccountDebtor().getIdHolderAccountPk()));
			for(HolderAccountDetail holderAccDeta:accountAnnotationOperation.getHolderAccountDebtor().getHolderAccountDetails()){
				if(BooleanType.YES.getCode().equals(holderAccDeta.getIndRepresentative())){
					registerAccountAnnotationTO.setHolderDebtor(holderAccDeta.getHolder());
					break;
				}
			}
		}
		
		
		registerAccountAnnotationTO.setIdParticipantPk(accountAnnotationOperation.getHolderAccount().getParticipant().getIdParticipantPk());
		registerAccountAnnotationTO.setAccountAnnotationMarketFact(new AccountAnnotationMarketFact());
		if(BooleanType.YES.getCode().equals(idepositarySetup.getIndMarketFact())){
			registerAccountAnnotationTO.setBlMarketFact(true);
			AccountAnnotationMarketFact accAnnMarketFact = dematerializationCertificateFacade.findMarketFact(accountAnnotationOperation.getIdAnnotationOperationPk());
			if(Validations.validateIsNotNullAndNotEmpty(accAnnMarketFact))
				registerAccountAnnotationTO.setAccountAnnotationMarketFact(accAnnMarketFact);			
		}
		if(BooleanType.YES.getCode().equals(accountAnnotationOperation.getIndSerializable())){
			registerAccountAnnotationTO.setShareCapital(accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurity().getCurrentNominalValue()));
			registerAccountAnnotationTO.setSecurityClass(accountAnnotationOperation.getSecurity().getSecurityClass());		
			if(InstrumentType.FIXED_INCOME.getCode().equals(accountAnnotationOperation.getSecurity().getInstrumentType()))
				registerAccountAnnotationTO.setBlFixedIncome(true);
		}else{
			registerAccountAnnotationTO.setShareCapital(accountAnnotationOperation.getTotalBalance().multiply(accountAnnotationOperation.getSecurityNominalValue()));
			registerAccountAnnotationTO.setSecurityClass(accountAnnotationOperation.getSecurityClass());	
		}
		
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setIndicator1("FISICO");
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		registerAccountAnnotationTO.setLstMotives(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		
		paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		registerAccountAnnotationTO.setLstSecurityClass(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.MOTIVE_REJECT_DEMATERIALIZATION.getCode());
		registerAccountAnnotationTO.setLstRejectMotive(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		if(DematerializationStateType.REJECTED.getCode().equals(accountAnnotationOperation.getState())){
			registerAccountAnnotationTO.setBlRejected(true);
		}
		registerAccountAnnotationTO.getAccountAnnotationOperation().setMotiveDescReject(accountAnnotationOperation.getCustodyOperation().getRejectMotiveOther());
		paramTabTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		registerAccountAnnotationTO.setLstCurrency(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.INTEREST_TYPE.getCode());
		registerAccountAnnotationTO.setLstInterestType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		registerAccountAnnotationTO.setLstBranchOffice(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));

		loadCboInterestPeriodicity();
		loadCboIndElectronicCupon();
		/*if(blConfirm){
			if(SecurityClassType.ACC.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) || 
					SecurityClassType.ACE.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass())){
				registerAccountAnnotationTO.setBlPrimarySettlement(true);
			}
		}*/
		if(blCancel || blAnnul || blReject){
			registerAccountAnnotationTO.setIndGrav(BooleanType.YES.getBooleanValue());
		}	
		if(blApprove || blConfirm || blCertify){
			registerAccountAnnotationTO.setIndGrav(BooleanType.NO.getBooleanValue());
		}

		List<AccountAnnotationCupon> lstAccountAnnotationCupon = dematerializationCertificateFacade.findAccountAnnotationCupon(accountAnnotationOperation.getIdAnnotationOperationPk());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setAccountAnnotationCupons(lstAccountAnnotationCupon);
		
		/*if (SecurityClassType.DPF.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) || SecurityClassType.DPA.getCode().equals(accountAnnotationOperation.getSecurity().getSecurityClass()) ){			
			if(blConfirm){
				registerAccountAnnotationTO.setBlDPF(BooleanType.YES.getBooleanValue());
				indValidator = BooleanType.YES.getBooleanValue();
			}			
			if(BooleanType.YES.getCode()==accountAnnotationOperation.getIndLien()){
				registerAccountAnnotationTO.setBlLien(BooleanType.YES.getBooleanValue());
				registerAccountAnnotationTO.getAccountAnnotationOperation().setCreditorSource(accountAnnotationOperation.getCreditorSource());
				indLien=true;
			}else{
					registerAccountAnnotationTO.setBlLien(BooleanType.NO.getBooleanValue());
				}
		}else{
			registerAccountAnnotationTO.setBlDPF(BooleanType.NO.getBooleanValue());
			registerAccountAnnotationTO.setIndGrav(BooleanType.YES.getBooleanValue());
			registerAccountAnnotationTO.setBlLien(BooleanType.NO.getBooleanValue());
			indAlternativeCode=false;
			if(blConfirm){
				indValidator = BooleanType.NO.getBooleanValue();
			}			
		}*/
		
		
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		//privilegeComponent.setBtnApproveView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		//privilegeComponent.setBtnReview(true);
		//privilegeComponent.setBtnCertifyView(true);
		//privilegeComponent.setBtnCancel(true);
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		mpStates = new HashMap<>();
		mpMotive = new HashMap<>();
		mpMnemonicClassSec = new HashMap<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_STATE.getCode());
		List<ParameterTable> lstStates = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstStates){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				searchAccountAnnotationTO.getStateList().add(paramTab);
			}
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		paramTabTO = new ParameterTableTO();
		paramTabTO.setIndicator1("FISICO");
		paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
		paramTabTO.setOrderbyParameterTableCd(ComponentConstant.ONE);
		List<ParameterTable> lstMotives = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstMotives){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				searchAccountAnnotationTO.getMotiveList().add(paramTab);
			}
			mpMotive.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
			
		//filling securityClass list
		paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
		List<ParameterTable> lstSecClasses = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstSecClasses){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				searchAccountAnnotationTO.getSecurityClassList().add(paramTab);
			}
			//mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
			mpMnemonicClassSec.put(paramTab.getParameterTablePk(), paramTab.getText1());
		}
		
	}
	
	/**
	 * Sets the flags false.
	 */
	private void setFlagsFalse(){
		blApprove = false;
		blAnnul = false;
		blConfirm = false;
		blReject = false;
		blReview = false;
		blCertify = false;
		blCancel = false;
		blDisabledFields= true;
		blRenderedMotives= false;
		
	}
	
	private List<AccountAnnotationOperation> setDescriptions(List<AccountAnnotationOperation> lstResult){
		for(AccountAnnotationOperation accAnnOpe:lstResult){
			accAnnOpe.setMotiveDescription(mpMotive.get(accAnnOpe.getMotive()));
			accAnnOpe.setStateDescription(mpStates.get(accAnnOpe.getState()));
			if(accAnnOpe.getSecurityCurrency()!=null) {
				CurrencyType currencyType = CurrencyType.get(accAnnOpe.getSecurityCurrency());
				if(currencyType!=null) {
					accAnnOpe.setSecurityCurrencyDesc(currencyType.getCodeIso());
				}
			}
		}
		return lstResult;
	}
	
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler(){
		otherMotive = null;
		if (registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject() != null) {
			if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject().equals(DematerializationRejectMotiveType.OTHERS.getCode())){
				registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.TRUE);
				otherMotive = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_LBL_MOTIVE_OTHER);
			}else{
				registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.FALSE);
			}
		}else{
			registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.FALSE);
		}
	}
	

	public void executeRateToPrice() {
		if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue()!=null) {
			BigDecimal nominalValue = registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue();
			BigDecimal rate = registerAccountAnnotationTO.getAccountAnnotationOperation().getAmountRate();
			registerAccountAnnotationTO.getAccountAnnotationOperation().setCashPrice(rate.multiply(nominalValue));
		}
	}
	
	public void selectedMotive(){
		try{
				JSFUtilities.resetComponent(":frmMotive:dialogMotive");
				executeAction();
		
				JSFUtilities.hideGeneralDialogues();
				hideLocalComponents();
				newRequest();
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO.setMasterTableFk(MasterTableType.MOTIVE_REJECT_DEMATERIALIZATION.getCode());
				registerAccountAnnotationTO.setLstRejectMotive(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
				if(registerAccountAnnotationTO.getAccountAnnotationOperation()!=null){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setMotiveReject(null);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setShowMotiveText(Boolean.FALSE);
					registerAccountAnnotationTO.getAccountAnnotationOperation().setMotiveDescReject(null);
				}
		
				JSFUtilities.showComponent(":frmMotive:dialogMotive");
			} catch (Exception e) {
				e.printStackTrace();
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
	
	
	/**
	 * Confirm motive handler.
	 */
	public void confirmMotiveHandler(){
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		String strOperation=GeneralConstants.EMPTY_STRING;
		if (registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject() == null || 
			!Validations.validateIsPositiveNumber(Integer.parseInt(String.valueOf(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject())))) {
			
			JSFUtilities.addContextMessage("frmMotive:cboMotive",
										FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE),
										PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE));
			
			showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
			JSFUtilities.showValidationDialog();
			return;
		}	
		/*if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveReject().equals(DematerializationRejectMotiveType.OTHERS.getCode())){
			if (Validations.validateIsNullOrEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotiveDescReject())) {			
				JSFUtilities.addContextMessage(
						"frmMotive:txtOtherMotive",FacesMessage.SEVERITY_ERROR,
						PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE),
						PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_OTHER_MOTIVE));
						showMessageOnDialog(PropertiesConstants.MESSAGES_ALERT, null,PropertiesConstants.MESSAGE_DATA_REQUIRED, null);
						JSFUtilities.showValidationDialog();
						return;
			}else{
				if(blAnnul){
					for(AccountAnnotationOperation objAao : checkAccountAnotation){
						strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					}
					if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
						strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
					}
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ANNUL_REQUEST, 
							new Object[]{strOperation});
				}else if(blReject){
					for(AccountAnnotationOperation objAao : checkAccountAnotation){
						strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					}
					if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
						strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
					}
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_REJECT_REQUEST, 
							new Object[]{strOperation});
				}else if(blCancel){
					showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL, 
							new Object[]{checkAccountAnotation[0].getCustodyOperation().getOperationNumber().toString()});
				}
			}
		}else*/{
			if(blAnnul){
				for(AccountAnnotationOperation objAao : checkAccountAnotation){
					strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
				}
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ANNUL_REQUEST, 
						new Object[]{strOperation});
			}else if(blReject){
				for(AccountAnnotationOperation objAao : checkAccountAnotation){
					strOperation+=objAao.getCustodyOperation().getOperationNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				if(strOperation.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
					strOperation=strOperation.trim().substring(0,strOperation.trim().length()-1);
				}
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_REJECT_REQUEST, 
						new Object[]{strOperation});
			}else if(blCancel){
				showMessageOnDialog(PropertiesConstants.MESSAGES_WARNING, null, PropertiesConstants.ACCOUNT_ANNOTATION_ASK_CANCEL, 
						new Object[]{checkAccountAnotation[0].getCustodyOperation().getOperationNumber().toString()});
			}
		}
		JSFUtilities.showComponent(":frmBlock:cnfblock");
	}
	
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
		hideLocalComponents();
	}
	
	public void changeIndSerializable() {
		cleanPanel();
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityInterestType(InterestType.FIXED.getCode());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setCertificateNumber(null);
		if( registerAccountAnnotationTO.getAccountAnnotationOperation().getIndSerializable().equals(1) ) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
		}
	}
	
	public void cleanPanel(){
		JSFUtilities.resetComponent(":frmDematerializationCertificate:opnlNoSerialize");
		JSFUtilities.resetComponent(":frmDematerializationCertificate:txtExpedition");
		JSFUtilities.resetComponent(":frmDematerializationCertificate:securityHelper:securityHelper");
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setIssuer(new Issuer());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecuritiesManager(new SecuritiesManager());
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityClass(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setExpeditionDate(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityExpirationDate(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityYieldRate(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityCurrency(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityInterestType(null);
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityInterestRate(null);
	}
	
	public void resetInterestRate(){
		JSFUtilities.resetComponent(":frmDematerializationCertificate:txtInterestRate");
		registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityInterestRate(null);
		if(InterestType.CERO.getCode().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestType()))
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityInterestRate(BigDecimal.ZERO);
		//searchSecurity();
	}
	
	public void calculateNominal(){
		registerAccountAnnotationTO.setShareCapital(null);
		if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance()))
			registerAccountAnnotationTO.setShareCapital(registerAccountAnnotationTO.getAccountAnnotationOperation().getTotalBalance().multiply(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue()));
		
		searchSecurity();
		
		if( registerAccountAnnotationTO.getAccountAnnotationOperation() != null &&
			registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive() != null && 
			registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode()) ){
			executeRateToPrice();
		}
		
	}
	
	public void searchSecurity(){
		
		if( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityClass() !=null &&
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityClass().equals(SecurityClassType.DPF.getCode())) {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setIndTaxExoneration(1);
		}

		if( registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityClass() !=null &&
			registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityClass().equals(SecurityClassType.CHQ.getCode())) {
			registerAccountAnnotationTO.setCuponQuantity(0);
		}else {
			registerAccountAnnotationTO.setCuponQuantity(null);
		}
		
		try {
			registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurityYieldRate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate());
			
			if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getIssuer().getIdIssuerPk()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityClass()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getExpeditionDate()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityExpirationDate()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityCurrency()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestType()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate()) &&
					Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityNominalValue())){
					registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(new Security());
					Security security = dematerializationCertificateFacade.findSecurity(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(security != null){
						if(SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())){
							registerAccountAnnotationTO.getAccountAnnotationOperation().setSecurity(security);
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), "Se encontro un valor pero con estado inválido");
							JSFUtilities.showSimpleValidationDialog();
						}
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	public void hideLocalComponents(){
		JSFUtilities.hideComponent(":frmMotive:dialogMotive");
		JSFUtilities.hideComponent(":frmBlock:cnfblock");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfHolderAccountModifyMgmt");
		//JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfCleanHolderAccountMgmt");
		JSFUtilities.hideComponent(":frmHolderAccountMgmt:cnfBackAdvancedAccount");
		JSFUtilities.hideGeneralDialogues();
	}
	
	public void validateIssuerSearch() {
		if ( !(Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer()) 
				&& Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer().getIdIssuerPk())) ) {
			return;
		}
		issuerSearchPK = searchAccountAnnotationTO.getIssuer().getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);
		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			searchAccountAnnotationTO.setIssuer(new Issuer());
		}
		cleanResults();
	}
	
	public void validateIssuerRegister() {
		if ( !(Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer()) 
				&& Validations.validateIsNotNull(searchAccountAnnotationTO.getIssuer().getIdIssuerPk())) ) {
			return;
		}
		issuerSearchPK = searchAccountAnnotationTO.getIssuer().getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);
		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			searchAccountAnnotationTO.setIssuer(new Issuer());
		}
		cleanResults();
	}
	
	public void alert(String headerMessage, String bodyMessage) {
		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Load cbo filter curency.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCboFilterCurency() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState( ParameterTableStateType.REGISTERED.getCode() );
		parameterTableTO.setMasterTableFk( MasterTableType.CURRENCY.getCode() );
		lstCboFilterCurrency = generalParametersFacade.getListParameterTableServiceBean( parameterTableTO );		
	}

	public void validateDate() {
		if ((CommonsUtilities.addMonthsToDate(searchAccountAnnotationTO.getStartDate(), 4)
				.compareTo(searchAccountAnnotationTO.getEndDate())) < 0) {
			searchAccountAnnotationTO
					.setEndDate(CommonsUtilities.addMonthsToDate(searchAccountAnnotationTO.getStartDate(), 4));
			searchAccountAnnotationTO
					.setMaxDate(CommonsUtilities.addMonthsToDate(searchAccountAnnotationTO.getStartDate(), 4));
		}
	}
	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}
	
	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}
	
	/**
	 * Gets the search account annotation to.
	 *
	 * @return the search account annotation to
	 */
	public SearchAccountAnnotationTO getSearchAccountAnnotationTO() {
		return searchAccountAnnotationTO;
	}
	
	/**
	 * Sets the search account annotation to.
	 *
	 * @param searchAccountAnnotationTO the new search account annotation to
	 */
	public void setSearchAccountAnnotationTO(
			SearchAccountAnnotationTO searchAccountAnnotationTO) {
		this.searchAccountAnnotationTO = searchAccountAnnotationTO;
	}
	
	/**
	 * Gets the register account annotation to.
	 *
	 * @return the register account annotation to
	 */
	public RegisterAccountAnnotationTO getRegisterAccountAnnotationTO() {
		return registerAccountAnnotationTO;
	}
	
	/**
	 * Sets the register account annotation to.
	 *
	 * @param registerAccountAnnotationTO the new register account annotation to
	 */
	public void setRegisterAccountAnnotationTO(
			RegisterAccountAnnotationTO registerAccountAnnotationTO) {
		this.registerAccountAnnotationTO = registerAccountAnnotationTO;
	}
	
	/**
	 * Checks if is bl approve.
	 *
	 * @return true, if is bl approve
	 */
	public boolean isBlApprove() {
		return blApprove;
	}
	
	/**
	 * Sets the bl approve.
	 *
	 * @param blApprove the new bl approve
	 */
	public void setBlApprove(boolean blApprove) {
		this.blApprove = blApprove;
	}
	
	/**
	 * Checks if is bl annul.
	 *
	 * @return true, if is bl annul
	 */
	public boolean isBlAnnul() {
		return blAnnul;
	}
	
	/**
	 * Sets the bl annul.
	 *
	 * @param blAnnul the new bl annul
	 */
	public void setBlAnnul(boolean blAnnul) {
		this.blAnnul = blAnnul;
	}
	
	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}
	
	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}
	
	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}
	
	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}
	
	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}
	
	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}


	public boolean isBlCancel() {
		return blCancel;
	}

	public void setBlCancel(boolean blCancel) {
		this.blCancel = blCancel;
	}

	public boolean isBlReview() {
		return blReview;
	}

	public void setBlReview(boolean blReview) {
		this.blReview = blReview;
	}

	public boolean isBlCertify() {
		return blCertify;
	}

	public void setBlCertify(boolean blCertify) {
		this.blCertify = blCertify;
	}

	public boolean isBlDisabledFields() {
		return blDisabledFields;
	}

	public void setBlDisabledFields(boolean blDisabledFields) {
		this.blDisabledFields = blDisabledFields;
	}

	public boolean isBlRenderedMotives() {
		return blRenderedMotives;
	}

	public void setBlRenderedMotives(boolean blRenderedMotives) {
		this.blRenderedMotives = blRenderedMotives;
	}

	public ParameterServiceBean getParamServiceBean() {
		return paramServiceBean;
	}

	public void setParamServiceBean(ParameterServiceBean paramServiceBean) {
		this.paramServiceBean = paramServiceBean;
	}

	public Map<Integer, String> getMpMnemonicClassSec() {
		return mpMnemonicClassSec;
	}

	public void setMpMnemonicClassSec(Map<Integer, String> mpMnemonicClassSec) {
		this.mpMnemonicClassSec = mpMnemonicClassSec;
	}

	public AccountAnnotationOperation[] getCheckAccountAnotation() {
		return checkAccountAnotation;
	}

	public void setCheckAccountAnotation(
			AccountAnnotationOperation[] checkAccountAnotation) {
		this.checkAccountAnotation = checkAccountAnotation;
	}

	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocationDpto;
	}

	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocationDpto = listGeographicLocation;
	}

	public List<GeographicLocation> getListGeographicLocationDpto() {
		return listGeographicLocationDpto;
	}

	public void setListGeographicLocationDpto(
			List<GeographicLocation> listGeographicLocationDpto) {
		this.listGeographicLocationDpto = listGeographicLocationDpto;
	}


	public List<GeographicLocation> getListGeographicLocationProvince() {
		return listGeographicLocationProvince;
	}


	public void setListGeographicLocationProvince(
			List<GeographicLocation> listGeographicLocationProvince) {
		this.listGeographicLocationProvince = listGeographicLocationProvince;
	}


	public List<GeographicLocation> getListGeographicLocationMunicipality() {
		return listGeographicLocationMunicipality;
	}


	public void setListGeographicLocationMunicipality(
			List<GeographicLocation> listGeographicLocationMunicipality) {
		this.listGeographicLocationMunicipality = listGeographicLocationMunicipality;
	}

	public boolean isIndAlternativeCode() {
		return indAlternativeCode;
	}

	public void setIndAlternativeCode(boolean indAlternativeCode) {
		this.indAlternativeCode = indAlternativeCode;
	}

	public boolean isIndValidator() {
		return indValidator;
	}

	public void setIndValidator(boolean indValidator) {
		this.indValidator = indValidator;
	}

	public boolean isIndLien() {
		return indLien;
	}

	public void setIndLien(boolean indLien) {
		this.indLien = indLien;
	}

	public boolean isBlCertificateDisable() {
		return blCertificateDisable;
	}

	public void setBlCertificateDisable(boolean blCertificateDisable) {
		this.blCertificateDisable = blCertificateDisable;
	}

	/**
	 * @return the issuerSearchPK
	 */
	public String getIssuerSearchPK() {
		return issuerSearchPK;
	}

	/**
	 * @param issuerSearchPK the issuerSearchPK to set
	 */
	public void setIssuerSearchPK(String issuerSearchPK) {
		this.issuerSearchPK = issuerSearchPK;
	}

	/**
	 * @return the lstCboFilterCurrency
	 */
	public List<ParameterTable> getLstCboFilterCurrency() {
		return lstCboFilterCurrency;
	}

	/**
	 * @param lstCboFilterCurrency the lstCboFilterCurrency to set
	 */
	public void setLstCboFilterCurrency(List<ParameterTable> lstCboFilterCurrency) {
		this.lstCboFilterCurrency = lstCboFilterCurrency;
	}

	/**
	 * @return the blBlocIssuer
	 */
	public boolean isBlBlocIssuer() {
		return blBlocIssuer;
	}

	/**
	 * @param blBlocIssuer the blBlocIssuer to set
	 */
	public void setBlBlocIssuer(boolean blBlocIssuer) {
		this.blBlocIssuer = blBlocIssuer;
	}

	public String getMassiveInterfaceType() {
		return massiveInterfaceType;
	}

	public void setMassiveInterfaceType(String massiveInterfaceType) {
		this.massiveInterfaceType = massiveInterfaceType;
	}

	/**
	 * @return the blError
	 */
	public boolean isBlError() {
		return blError;
	}

	/**
	 * @param blError the blError to set
	 */
	public void setBlError(boolean blError) {
		this.blError = blError;
	}

	
	public String getCurrentMotive() {
		return currentMotive;
	}

	public void setCurrentMotive(String currentMotive) {
		this.currentMotive = currentMotive;
	}

	public String getOtherMotive() {
		return otherMotive;
	}

	public void setOtherMotive(String otherMotive) {
		this.otherMotive = otherMotive;
	}

	public ProcessFileTO getTmpProcessFile() {
		return tmpProcessFile;
	}

	public void setTmpProcessFile(ProcessFileTO tmpProcessFile) {
		this.tmpProcessFile = tmpProcessFile;
	}	
	
	public String hashMotive(Integer motive) {
		return DematerializationCertificateMotiveType.get(motive).getValue();
	}

	public Date getMinExpirationDate() {
		return minExpirationDate;
	}

	public void setMinExpirationDate(Date minExpirationDate) {
		this.minExpirationDate = minExpirationDate;
	}
	
}