package com.pradera.custody.dematerializationcertificate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.core.component.generalparameter.service.GeographicLocationServiceBean;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.SecuritiesManager;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RegisterAccountAnnotationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class RegisterAccountAnnotationTO implements Serializable, Cloneable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The account annotation operation. */
	private AccountAnnotationOperation accountAnnotationOperation;
	
	/** The account annotation market fact. */
	private AccountAnnotationMarketFact accountAnnotationMarketFact;
	
	/** The lst security class. */
	private List<ParameterTable> lstMotives, lstSecurityClass, lstCurrency, lstInterestType, lstBranchOffice, lstCboInterestPeriodicity, lstIndElectronicCupon;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The share capital. */
	private BigDecimal shareCapital;
	
	/** The lst participants. */
	private List<Participant> lstParticipants;
	
	private List<SecuritiesManager> lstSecuritiesManager;
	
	/** The lst reject motive. */
	private List<ParameterTable> lstRejectMotive;
	
	/**  The lst geographic location. */
	private List<GeographicLocationServiceBean> lstGeographicLocation;
	
	/** The bl rejected. */
	private boolean blOthers, blRejected ;
	
	/** The bl fixed income. */
	private boolean blMarketFact, blFixedIncome, blVariableIncome;
	
	/** The holder. */
	private Holder holder, holderSeller, holderBeneficiario, holderDebtor;
	private List<HolderAccount> lstHolderAccount,  lstHolderAccountSeller, lstHolderAccountBeneficiario, lstHolderAccountDebtor;
	private Long idParticipantPk, idParticipantSellerPk;
	private boolean blDPF;
	
	/** The bl certificate number. */
	private boolean blCertificateNumber;
	private boolean blPrimarySettlement;
	public boolean blLien;
	private boolean indGrav,indSecurityDPFDPA;
	
	/** The province. */
	private Integer department,municipality,province;

	private Participant participant, participantSeller;

	private Integer cuponQuantity;

	private BigDecimal amountRate;
	
	private HolderAccount holderAccountSeller;
	
	//constrictor
	public RegisterAccountAnnotationTO(){}
	
	/**
	 * Gets the account annotation operation.
	 *
	 * @return the account annotation operation
	 */
	public AccountAnnotationOperation getAccountAnnotationOperation() {
		return accountAnnotationOperation;
	}
	
	/**
	 * Sets the account annotation operation.
	 *
	 * @param accountAnnotationOperation the new account annotation operation
	 */
	public void setAccountAnnotationOperation(
			AccountAnnotationOperation accountAnnotationOperation) {
		this.accountAnnotationOperation = accountAnnotationOperation;
	}
	
	/**
	 * Gets the lst motives.
	 *
	 * @return the lst motives
	 */
	public List<ParameterTable> getLstMotives() {
		return lstMotives;
	}
	
	/**
	 * Sets the lst motives.
	 *
	 * @param lstMotives the new lst motives
	 */
	public void setLstMotives(List<ParameterTable> lstMotives) {
		this.lstMotives = lstMotives;
	}
	
	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}
	
	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	
	/**
	 * Gets the share capital.
	 *
	 * @return the share capital
	 */
	public BigDecimal getShareCapital() {
		return shareCapital;
	}
	
	/**
	 * Sets the share capital.
	 *
	 * @param shareCapital the new share capital
	 */
	public void setShareCapital(BigDecimal shareCapital) {
		this.shareCapital = shareCapital;
	}
	
	/**
	 * Gets the lst participants.
	 *
	 * @return the lst participants
	 */
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	
	/**
	 * Sets the lst participants.
	 *
	 * @param lstParticipants the new lst participants
	 */
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Gets the lst reject motive.
	 *
	 * @return the lst reject motive
	 */
	public List<ParameterTable> getLstRejectMotive() {
		return lstRejectMotive;
	}
	
	/**
	 * Sets the lst reject motive.
	 *
	 * @param lstRejectMotive the new lst reject motive
	 */
	public void setLstRejectMotive(List<ParameterTable> lstRejectMotive) {
		this.lstRejectMotive = lstRejectMotive;
	}
	
	/**
	 * Checks if is bl others.
	 *
	 * @return true, if is bl others
	 */
	public boolean isBlOthers() {
		return blOthers;
	}
	
	/**
	 * Sets the bl others.
	 *
	 * @param blOthers the new bl others
	 */
	public void setBlOthers(boolean blOthers) {
		this.blOthers = blOthers;
	}
	
	/**
	 * Checks if is bl rejected.
	 *
	 * @return true, if is bl rejected
	 */
	public boolean isBlRejected() {
		return blRejected;
	}
	
	/**
	 * Sets the bl rejected.
	 *
	 * @param blRejected the new bl rejected
	 */
	public void setBlRejected(boolean blRejected) {
		this.blRejected = blRejected;
	}
	
	/**
	 * Checks if is bl fixed income.
	 *
	 * @return true, if is bl fixed income
	 */
	public boolean isBlFixedIncome() {
		return blFixedIncome;
	}
	
	/**
	 * Sets the bl fixed income.
	 *
	 * @param blFixedIncome the new bl fixed income
	 */
	public void setBlFixedIncome(boolean blFixedIncome) {
		this.blFixedIncome = blFixedIncome;
	}
	
	/**
	 * Gets the account annotation market fact.
	 *
	 * @return the account annotation market fact
	 */
	public AccountAnnotationMarketFact getAccountAnnotationMarketFact() {
		return accountAnnotationMarketFact;
	}
	
	/**
	 * Sets the account annotation market fact.
	 *
	 * @param accountAnnotationMarketFact the new account annotation market fact
	 */
	public void setAccountAnnotationMarketFact(
			AccountAnnotationMarketFact accountAnnotationMarketFact) {
		this.accountAnnotationMarketFact = accountAnnotationMarketFact;
	}
	
	/**
	 * Checks if is bl market fact.
	 *
	 * @return true, if is bl market fact
	 */
	public boolean isBlMarketFact() {
		return blMarketFact;
	}
	
	/**
	 * Sets the bl market fact.
	 *
	 * @param blMarketFact the new bl market fact
	 */
	public void setBlMarketFact(boolean blMarketFact) {
		this.blMarketFact = blMarketFact;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}

	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Checks if is bl dpf.
	 *
	 * @return true, if is bl dpf
	 */
	public boolean isBlDPF() {
		return blDPF;
	}

	/**
	 * Sets the bl dpf.
	 *
	 * @param blDPF the new bl dpf
	 */
	public void setBlDPF(boolean blDPF) {
		this.blDPF = blDPF;
	}

	/**
	 * Checks if is bl variable income.
	 *
	 * @return true, if is bl variable income
	 */
	public boolean isBlVariableIncome() {
		return blVariableIncome;
	}

	/**
	 * Sets the bl variable income.
	 *
	 * @param blVariableIncome the new bl variable income
	 */
	public void setBlVariableIncome(boolean blVariableIncome) {
		this.blVariableIncome = blVariableIncome;
	}

	/**
	 * Checks if is bl certificate number.
	 *
	 * @return true, if is bl certificate number
	 */
	public boolean isBlCertificateNumber() {
		return blCertificateNumber;
	}

	/**
	 * Sets the bl certificate number.
	 *
	 * @param blCertificateNumber the new bl certificate number
	 */
	public void setBlCertificateNumber(boolean blCertificateNumber) {
		this.blCertificateNumber = blCertificateNumber;
	}

	/**
	 * Checks if is bl primary settlement.
	 *
	 * @return true, if is bl primary settlement
	 */
	public boolean isBlPrimarySettlement() {
		return blPrimarySettlement;
	}

	/**
	 * Sets the bl primary settlement.
	 *
	 * @param blPrimarySettlement the new bl primary settlement
	 */
	public void setBlPrimarySettlement(boolean blPrimarySettlement) {
		this.blPrimarySettlement = blPrimarySettlement;
	}

	/**
	 * Checks if is bl lien.
	 *
	 * @return true, if is bl lien
	 */
	public boolean isBlLien() {
		return blLien;
	}

	/**
	 * Sets the bl lien.
	 *
	 * @param blLien the new bl lien
	 */
	public void setBlLien(boolean blLien) {
		this.blLien = blLien;
	}

	/**
	 * Gets the lst geographic location.
	 *
	 * @return the lst geographic location
	 */
	public List<GeographicLocationServiceBean> getLstGeographicLocation() {
		return lstGeographicLocation;
	}

	/**
	 * Sets the lst geographic location.
	 *
	 * @param lstGeographicLocation the new lst geographic location
	 */
	public void setLstGeographicLocation(
			List<GeographicLocationServiceBean> lstGeographicLocation) {
		this.lstGeographicLocation = lstGeographicLocation;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Integer getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(Integer department) {
		this.department = department;
	}

	/**
	 * Gets the municipality.
	 *
	 * @return the municipality
	 */
	public Integer getMunicipality() {
		return municipality;
	}

	/**
	 * Sets the municipality.
	 *
	 * @param municipality the new municipality
	 */
	public void setMunicipality(Integer municipality) {
		this.municipality = municipality;
	}

	/**
	 * Gets the province.
	 *
	 * @return the province
	 */
	public Integer getProvince() {
		return province;
	}

	/**
	 * Sets the province.
	 *
	 * @param province the new province
	 */
	public void setProvince(Integer province) {
		this.province = province;
	}

	/**
	 * Checks if is ind grav.
	 *
	 * @return true, if is ind grav
	 */
	public boolean isIndGrav() {
		return indGrav;
	}

	/**
	 * Sets the ind grav.
	 *
	 * @param indGrav the new ind grav
	 */
	public void setIndGrav(boolean indGrav) {
		this.indGrav = indGrav;
	}

	/**
	 * Checks if is ind security dpfdpa.
	 *
	 * @return true, if is ind security dpfdpa
	 */
	public boolean isIndSecurityDPFDPA() {
		return indSecurityDPFDPA;
	}

	/**
	 * Sets the ind security dpfdpa.
	 *
	 * @param indSecurityDPFDPA the new ind security dpfdpa
	 */
	public void setIndSecurityDPFDPA(boolean indSecurityDPFDPA) {
		this.indSecurityDPFDPA = indSecurityDPFDPA;
	}
	
	public List<ParameterTable> getLstCurrency() {
		return lstCurrency;
	}

	public void setLstCurrency(List<ParameterTable> lstCurrency) {
		this.lstCurrency = lstCurrency;
	}

	public List<ParameterTable> getLstInterestType() {
		return lstInterestType;
	}

	public void setLstInterestType(List<ParameterTable> lstInterestType) {
		this.lstInterestType = lstInterestType;
	}

	public List<ParameterTable> getLstBranchOffice() {
		return lstBranchOffice;
	}

	public void setLstBranchOffice(List<ParameterTable> lstBranchOffice) {
		this.lstBranchOffice = lstBranchOffice;
	}

	public List<SecuritiesManager> getLstSecuritiesManager() {
		return lstSecuritiesManager;
	}

	public void setLstSecuritiesManager(List<SecuritiesManager> lstSecuritiesManager) {
		this.lstSecuritiesManager = lstSecuritiesManager;
	}

	public List<ParameterTable> getLstCboInterestPeriodicity() {
		return lstCboInterestPeriodicity;
	}

	public void setLstCboInterestPeriodicity(List<ParameterTable> lstCboInterestPeriodicity) {
		this.lstCboInterestPeriodicity = lstCboInterestPeriodicity;
	}

	public List<ParameterTable> getLstIndElectronicCupon() {
		return lstIndElectronicCupon;
	}

	public void setLstIndElectronicCupon(List<ParameterTable> lstIndElectronicCupon) {
		this.lstIndElectronicCupon = lstIndElectronicCupon;
	}

	public Participant getParticipant() {
		return participant;
	}

	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Holder getHolderSeller() {
		return holderSeller;
	}

	public void setHolderSeller(Holder holderSeller) {
		this.holderSeller = holderSeller;
	}

	public List<HolderAccount> getLstHolderAccountSeller() {
		return lstHolderAccountSeller;
	}

	public void setLstHolderAccountSeller(List<HolderAccount> lstHolderAccountSeller) {
		this.lstHolderAccountSeller = lstHolderAccountSeller;
	}

	public Long getIdParticipantSellerPk() {
		return idParticipantSellerPk;
	}

	public void setIdParticipantSellerPk(Long idParticipantSellerPk) {
		this.idParticipantSellerPk = idParticipantSellerPk;
	}

	public Integer getCuponQuantity() {
		return cuponQuantity;
	}

	public void setCuponQuantity(Integer cuponQuantity) {
		this.cuponQuantity = cuponQuantity;
	}

	public Participant getParticipantSeller() {
		return participantSeller;
	}

	public void setParticipantSeller(Participant participantSeller) {
		this.participantSeller = participantSeller;
	}

	public BigDecimal getAmountRate() {
		return amountRate;
	}

	public void setAmountRate(BigDecimal amountRate) {
		this.amountRate = amountRate;
	}

	public HolderAccount getHolderAccountSeller() {
		return holderAccountSeller;
	}

	public void setHolderAccountSeller(HolderAccount holderAccountSeller) {
		this.holderAccountSeller = holderAccountSeller;
	}
	
	public Holder getHolderBeneficiario() {
		return holderBeneficiario;
	}

	public void setHolderBeneficiario(Holder holderBeneficiario) {
		this.holderBeneficiario = holderBeneficiario;
	}

	public List<HolderAccount> getLstHolderAccountBeneficiario() {
		return lstHolderAccountBeneficiario;
	}

	public void setLstHolderAccountBeneficiario(List<HolderAccount> lstHolderAccountBeneficiario) {
		this.lstHolderAccountBeneficiario = lstHolderAccountBeneficiario;
	}

	public Holder getHolderDebtor() {
		return holderDebtor;
	}

	public void setHolderDebtor(Holder holderDebtor) {
		this.holderDebtor = holderDebtor;
	}

	public List<HolderAccount> getLstHolderAccountDebtor() {
		return lstHolderAccountDebtor;
	}

	public void setLstHolderAccountDebtor(List<HolderAccount> lstHolderAccountDebtor) {
		this.lstHolderAccountDebtor = lstHolderAccountDebtor;
	}

	/** metodo para clonar objeto */
	@Override
	public Object clone() {
		Object obj = null;
		try {
			obj = super.clone();
		} catch (CloneNotSupportedException ex) {
			System.out.println("No se puede clonar Objeto");
		}
		return obj;
	}
}