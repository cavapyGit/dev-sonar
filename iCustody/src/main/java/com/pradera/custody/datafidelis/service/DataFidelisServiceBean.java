package com.pradera.custody.datafidelis.service;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
//import com.pradera.custody.datafidelis.service.DATAFIDELIS.REGISTROS;
//import com.pradera.custody.datafidelis.service.DATAFIDELIS.REGISTROS.REGISTRO;
import com.pradera.custody.datafidelis.service.ENTIDADDEVALORES.REGISTROS;
import com.pradera.custody.datafidelis.service.ENTIDADDEVALORES.REGISTROS.REGISTRO;

@Stateless
@Lock(LockType.READ)
public class DataFidelisServiceBean extends CrudDaoServiceBean {
	/**
	 * Search data fidelis object list.
	 */
	public List<Object[]> getDataFidelisReports () {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select s.ID_SECURITY_CODE_ONLY SERIE_TITULO,"
				+"   sc.TEXT1 CLASE, "
				+"   i.MNEMONIC EMISOR,"
				+"   s.ID_ISIN_CODE ISIN, "
				+"   s.CFI_CODE CFI, "
				+"   vcv.VALUATOR_CODE COD_VALORACION, "
				+"   sc.PARAMETER_NAME DESC_TITULO, "
				+"   TO_CHAR(mfv.CUT_DATE,'DD/MM/YYYY') FECHA_INFORMACION, "
				+"   cu.DESCRIPTION MONEDA, "
				+"   round(mfv.MARKET_PRICE,2) PRECIO "
				+" from SECURITY s "
				+"   left join VALUATOR_CODE_VIEW vcv on vcv.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK "
				+"   inner join PARAMETER_TABLE sc on sc.PARAMETER_TABLE_PK = s.SECURITY_CLASS "
				+"   inner join PARAMETER_TABLE cu on cu.PARAMETER_TABLE_PK = s.CURRENCY "
				+"   inner join ISSUER i on i.ID_ISSUER_PK = s.ID_ISSUER_FK "
				+"   inner join MARKET_FACT_VIEW mfv on mfv.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK "
				+" where 1=1 "
				+"   and s.STATE_SECURITY = 131 "
				+"   and s.SECURITY_CLASS not in (420, 415, 414, 1954) "
				+"   and s.DESMATERIALIZED_BALANCE > 0 "
				+"   and TO_CHAR(mfv.CUT_DATE,'DD/MM/YYYY') = TO_CHAR(:date,'DD/MM/YYYY')"
				+"   and TO_CHAR(s.EXPIRATION_DATE,'DD/MM/YYYY') >= TO_CHAR(:date,'DD/MM/YYYY') "
				+"   and mfv.TOTAL_BALANCE > 0 "
				+" group by s.ID_SECURITY_CODE_ONLY, sc.TEXT1, i.MNEMONIC, s.ID_ISIN_CODE, s.CFI_CODE, vcv.VALUATOR_CODE, "
				+" sc.PARAMETER_NAME,mfv.CUT_DATE, cu.DESCRIPTION, mfv.MARKET_PRICE ");
								
		Query query = em.createNativeQuery(sbQuery.toString());		
		//query.setParameter("date",CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(), -1));
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(CommonsUtilities.currentDate());
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			query.setParameter("date",CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(), -3));
		} else {
			query.setParameter("date",CommonsUtilities.addDaysToDate(CommonsUtilities.currentDate(), -1));
		}
		return (List<Object[]>)query.getResultList();
	}
	
	/**
	 * 
	 * @return
	 */
	/*public REGISTROS getDataFidelisReport () {
		List<Object[]> objectData = getDataFidelisReports();
		
		REGISTROS registros = new REGISTROS();		
		for (Object[] obj : objectData) {			
			REGISTRO registro = new REGISTRO();			
			registro.setCLAVEVALOR((obj[0] != null) ? String.valueOf(obj[0]) : "");
			registro.setCLASEVALOR((obj[1] != null) ? String.valueOf(obj[1]) : "");
			registro.setEMISOR((obj[2] != null) ? String.valueOf(obj[2]) : "");
			registro.setISIN((obj[3] != null) ? String.valueOf(obj[3]) : "");
			registro.setCFI((obj[4] != null) ? String.valueOf(obj[4]) : "");
			registro.setCODVALORACION((obj[5] != null) ? String.valueOf(obj[5]) : "");
			registro.setDESCTITULO((obj[6] != null) ? String.valueOf(obj[6]) : "");
		    registro.setFECHAINFORMACION((obj[7] != null) ? String.valueOf(obj[7]) : "");
			registro.setMONEDA((obj[8] != null) ? String.valueOf(obj[8]) : "");
			registro.setPRECIO((obj[9] != null) ? String.valueOf(obj[9]) : "");
		
			registros.getREGISTRO().add(registro);
		}
		return registros;
	}*/
	public REGISTROS getDataFidelisReport () {
		List<Object[]> objectData = getDataFidelisReports();
		
		REGISTROS registros = new REGISTROS();		
		for (Object[] obj : objectData) {			
			REGISTRO registro = new REGISTRO();			
			registro.setSERIETITULO((obj[0] != null) ? String.valueOf(obj[0]) : "");
			registro.setISIN((obj[3] != null) ? String.valueOf(obj[3]) : "");
			registro.setMONEDA((obj[8] != null) ? String.valueOf(obj[8]) : "");
			registro.setFECHA((obj[7] != null) ? String.valueOf(obj[7]) : "");
			registro.setPRECIO((obj[9] != null) ? String.valueOf(obj[9]) : "");
		
			registros.getREGISTRO().add(registro);
		}
		return registros;
	}
}
