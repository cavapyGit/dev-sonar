//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.07.29 at 10:35:06 AM BOT 
//


package com.pradera.custody.datafidelis.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="REGISTROS">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="REGISTRO" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ISIN">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="^[a-zA-Z0.9-]*$"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="SERIE_TITULO">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="^[a-zA-Z0-9-]*$"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="MONEDA">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="8"/>
 *                                   &lt;pattern value="^[A-Z]*$"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="FECHA">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="[0-3][0-9]/[01][0-9]/[0-9]{4}"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PRECIO">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;maxLength value="15"/>
 *                                   &lt;pattern value="^[0.9-]*$"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "registros"
})
@XmlRootElement(name = "ENTIDAD_DE_VALORES")
public class ENTIDADDEVALORES {

    @XmlElement(name = "REGISTROS", required = true)
    protected ENTIDADDEVALORES.REGISTROS registros;

    /**
     * Gets the value of the registros property.
     * 
     * @return
     *     possible object is
     *     {@link ENTIDADDEVALORES.REGISTROS }
     *     
     */
    public ENTIDADDEVALORES.REGISTROS getREGISTROS() {
        return registros;
    }

    /**
     * Sets the value of the registros property.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTIDADDEVALORES.REGISTROS }
     *     
     */
    public void setREGISTROS(ENTIDADDEVALORES.REGISTROS value) {
        this.registros = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="REGISTRO" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ISIN">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;pattern value="^[a-zA-Z0.9-]*$"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="SERIE_TITULO">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;pattern value="^[a-zA-Z0-9-]*$"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="MONEDA">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="8"/>
     *                         &lt;pattern value="^[A-Z]*$"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="FECHA">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;pattern value="[0-3][0-9]/[01][0-9]/[0-9]{4}"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PRECIO">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;maxLength value="15"/>
     *                         &lt;pattern value="^[0.9-]*$"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "registro"
    })
    public static class REGISTROS {

        @XmlElement(name = "REGISTRO", required = true)
        protected List<ENTIDADDEVALORES.REGISTROS.REGISTRO> registro;

        /**
         * Gets the value of the registro property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the registro property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getREGISTRO().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ENTIDADDEVALORES.REGISTROS.REGISTRO }
         * 
         * 
         */
        public List<ENTIDADDEVALORES.REGISTROS.REGISTRO> getREGISTRO() {
            if (registro == null) {
                registro = new ArrayList<ENTIDADDEVALORES.REGISTROS.REGISTRO>();
            }
            return this.registro;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ISIN">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;pattern value="^[a-zA-Z0.9-]*$"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="SERIE_TITULO">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;pattern value="^[a-zA-Z0-9-]*$"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="MONEDA">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="8"/>
         *               &lt;pattern value="^[A-Z]*$"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="FECHA">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;pattern value="[0-3][0-9]/[01][0-9]/[0-9]{4}"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PRECIO">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;maxLength value="15"/>
         *               &lt;pattern value="^[0.9-]*$"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "isin",
            "serietitulo",
            "moneda",
            "fecha",
            "precio"
        })
        public static class REGISTRO {

            @XmlElement(name = "ISIN", required = true)
            protected String isin;
            @XmlElement(name = "SERIE_TITULO", required = true)
            protected String serietitulo;
            @XmlElement(name = "MONEDA", required = true, nillable = true)
            protected String moneda;
            @XmlElement(name = "FECHA", required = true, nillable = true)
            protected String fecha;
            @XmlElement(name = "PRECIO", required = true, nillable = true)
            protected String precio;

            /**
             * Gets the value of the isin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getISIN() {
                return isin;
            }

            /**
             * Sets the value of the isin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setISIN(String value) {
                this.isin = value;
            }

            /**
             * Gets the value of the serietitulo property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSERIETITULO() {
                return serietitulo;
            }

            /**
             * Sets the value of the serietitulo property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSERIETITULO(String value) {
                this.serietitulo = value;
            }

            /**
             * Gets the value of the moneda property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMONEDA() {
                return moneda;
            }

            /**
             * Sets the value of the moneda property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMONEDA(String value) {
                this.moneda = value;
            }

            /**
             * Gets the value of the fecha property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFECHA() {
                return fecha;
            }

            /**
             * Sets the value of the fecha property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFECHA(String value) {
                this.fecha = value;
            }

            /**
             * Gets the value of the precio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPRECIO() {
                return precio;
            }

            /**
             * Sets the value of the precio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPRECIO(String value) {
                this.precio = value;
            }

        }

    }

}
