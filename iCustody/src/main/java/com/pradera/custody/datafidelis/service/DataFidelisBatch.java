package com.pradera.custody.datafidelis.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.datafidelis.facade.DataFidelisFacade;
//import com.pradera.custody.datafidelis.service.DATAFIDELIS.REGISTROS;
import com.pradera.custody.datafidelis.service.ENTIDADDEVALORES.REGISTROS;
import com.pradera.model.process.ProcessLogger;
/**
 * BATCH GENERACION Y SUBIDA XML DATAFIDELIS
 */
@BatchProcess(name="DataFidelisBatch")
@RequestScoped
public class DataFidelisBatch implements Serializable, JobExecution {
	
	@EJB
	DataFidelisFacade dataFidelisFacade;
		
	@Inject
	private PraderaLogger log;
		
	@Override
	public void startJob(ProcessLogger processLogger) {
		StringBuilder xmlResponse = new StringBuilder();
		REGISTROS dataFidelisTO = new REGISTROS();
		dataFidelisTO = dataFidelisFacade.getDataFidelis();		
		if(!dataFidelisTO.getREGISTRO().isEmpty()) {
			//DATAFIDELIS datafidelis = new DATAFIDELIS();
			ENTIDADDEVALORES datafidelis = new ENTIDADDEVALORES();
			datafidelis.setREGISTROS(dataFidelisTO);
			String DATE_FORMAT = "YYMMdd";
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			String fecha = sdf.format(CommonsUtilities.currentDate());
			try {
				String filename = "EDV"+fecha+".xml";
				String server = "200.105.203.154";
				int port = 21, reply;
				String user = "edv_uploader";
				String pass = "3dv";
				String rute="/home/jboss/datafidelis/";//produccion
				//String rute=("D:\\DATAFIDELIS\\");//pruebas
				
				File file = new File(rute+filename);
	            JAXBContext jaxbContext = JAXBContext.newInstance(ENTIDADDEVALORES.class);
	            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	            jaxbMarshaller.marshal(datafidelis, file);
	            //jaxbMarshaller.marshal(datafidelis, System.out);
	            FTPClient ftpClient = new FTPClient();
	            try {
	            	ftpClient.connect(server, port);
	            	reply = ftpClient.getReplyCode();
	            	if (!FTPReply.isPositiveCompletion(reply)) {
	                    ftpClient.disconnect();
	                    log.info("::::EXCEPCION CONECTANDO AL FTP::::");
	                }
					ftpClient.login(user, pass);
					ftpClient.enterLocalPassiveMode();
					ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
					FileInputStream inputStream = new FileInputStream(file);
					boolean done = ftpClient.storeFile(filename, inputStream);
					if (done) {
						log.info("::::GENERACION DATAFIDELIS EXITOSA::::");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (JAXBException e) {
	            e.printStackTrace();
	        }
		} else {
			xmlResponse.append("NO HAY DATOS A ESA FECHA");
			log.info("::::NO HAY DATOS A ESA FECHA::::");
		}
		log.info(":::: FIN GENERACION ARCHIVO DE RESPUESTA DATAFIDELIS::::");
	}
	
	@Override
	public Object[] getParametersNotification() {
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		return null;
	}

	@Override
	public boolean sendNotification() {
		return false;
	}
}
