package com.pradera.custody.datafidelis.facade;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
//import com.pradera.custody.datafidelis.service.DATAFIDELIS.REGISTROS;
import com.pradera.custody.datafidelis.service.ENTIDADDEVALORES.REGISTROS;
import com.pradera.custody.datafidelis.service.DataFidelisServiceBean;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class DataFidelisFacade {
	
	/** DATAFIDELIS SERVICE BEAN */
	@EJB
	DataFidelisServiceBean dataFidelisServiceBean;	
	
	public REGISTROS getDataFidelis() {
		return dataFidelisServiceBean.getDataFidelisReport();
	}
}
