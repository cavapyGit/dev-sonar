package com.pradera.custody.certificateoperation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.custody.certificateoperation.service.CertificateOperationServiceBean;
import com.pradera.custody.certificateoperation.view.RegisterCertificateOperationTO;
import com.pradera.custody.certificateoperation.view.SearchCertificateOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.CertificateOperation;
import com.pradera.model.custody.physical.CertificateOperationStateType;
import com.pradera.model.custody.physical.CertificateOperationType;
import com.pradera.model.custody.physical.FractionDetail;
import com.pradera.model.custody.physical.GlobalizeDetail;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.operations.RequestCorrelativeType;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class CertificateOperationFacade {

	@Inject
	CertificateOperationServiceBean certificateOperationServiceBean;
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	public List<PhysicalCertificate> getPhysicalCertificates(RegisterCertificateOperationTO registerCertificateOperationTO) throws ServiceException{
		Map<String, String> mpCertificatesRelated = new HashMap<>();
		List<PhysicalCertificate> lstTemp = certificateOperationServiceBean.getPhysicalCertificates(registerCertificateOperationTO);
		List<PhysicalCertificate> lstResult = new ArrayList<>();
		if(!mpCertificatesRelated.isEmpty()){
			for(PhysicalCertificate physicalCertificate:lstTemp){
				if(Validations.validateIsNull(mpCertificatesRelated.get(physicalCertificate.getCertificateNumber().toString())))
					lstResult.add(physicalCertificate);
			}
			return lstResult;
		}
		return lstTemp;
	}

	public void saveOperation(RegisterCertificateOperationTO registerCertificateOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setState(CertificateOperationStateType.REGISTERED.getCode());
		//TODO CAMBIAR
		custodyOperation.setOperationType(ParameterOperationType.SECURITIES_DEMATERIALIZATION.getCode());
		custodyOperation.setOperationNumber(certificateOperationServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_DEMATERIALIZATION));
		CertificateOperation certificateOperation = registerCertificateOperationTO.getCertificateOperation();
		certificateOperation.setOperationState(CertificateOperationStateType.REGISTERED.getCode());
		certificateOperation.setCustodyOperation(custodyOperation);
		if(CertificateOperationType.FRACTION.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){			
			for(FractionDetail fraction:registerCertificateOperationTO.getLstFraction()){
				fraction.setCertificateOperation(certificateOperation);
				certificateOperationServiceBean.create(fraction);
			}
			PhysicalCertificate certificate = certificateOperationServiceBean.find(PhysicalCertificate.class, registerCertificateOperationTO.getPhysicalCertificate().getIdPhysicalCertificatePk()); 
			certificate.setState(PhysicalCertificateStateType.ANNULED.getCode());
			certificateOperation.setPhysicalCertificate(certificate);
		}else if(CertificateOperationType.GLOBALIZE.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			for(PhysicalCertificate physicalCertificate:registerCertificateOperationTO.getLstPhysicalCertificates()){
				if(physicalCertificate.isBlSelected()){
					GlobalizeDetail globalizeDetail = new GlobalizeDetail();
					globalizeDetail.setCertificateOperation(certificateOperation);
					PhysicalCertificate certificate = certificateOperationServiceBean.find(PhysicalCertificate.class, physicalCertificate.getIdPhysicalCertificatePk()); 
					certificate.setState(PhysicalCertificateStateType.ANNULED.getCode());
					globalizeDetail.setPhysicalCertificate(certificate);
					certificateOperationServiceBean.create(globalizeDetail);
				}
			}
			certificateOperation.setGlobalizeQuantity(registerCertificateOperationTO.getGlobalizeQuantity());			
		}else if(CertificateOperationType.TRANSFER.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			PhysicalCertificate certificate = certificateOperationServiceBean.find(PhysicalCertificate.class, registerCertificateOperationTO.getPhysicalCertificate().getIdPhysicalCertificatePk()); 
			certificate.setState(PhysicalCertificateStateType.ANNULED.getCode());
			certificateOperation.setPhysicalCertificate(certificate);
		}else if(CertificateOperationType.REPLACE.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			PhysicalCertificate certificate = certificateOperationServiceBean.find(PhysicalCertificate.class, registerCertificateOperationTO.getPhysicalCertificate().getIdPhysicalCertificatePk()); 
			certificate.setState(PhysicalCertificateStateType.ANNULED.getCode());
			certificateOperation.setPhysicalCertificate(certificate);
		}	
		certificateOperationServiceBean.create(certificateOperation);
	}

	public List<CertificateOperation> getCertificateOperations(SearchCertificateOperationTO searchCertificateOperationTO) throws ServiceException{
		return certificateOperationServiceBean.getCertificateOperations(searchCertificateOperationTO); 
	}

	public CertificateOperation getCertificateOperation(Long idCertificateOperationPk) throws ServiceException{
		return certificateOperationServiceBean.getCertificateOperation(idCertificateOperationPk);
	}

	public List<PhysicalCertificate> getCertificatesRelated(CertificateOperation certificateOperation) throws ServiceException{
		return certificateOperationServiceBean.getCertificatesForGlobalize(certificateOperation.getIdCertificateOperationPk());
	}

	public List<FractionDetail> getFractions(Long idCertificateOperationPk) throws ServiceException{
		return certificateOperationServiceBean.getFractions(idCertificateOperationPk);
	}

	public void rejectCertificateOperation(Long idCertificateOperationPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		CertificateOperation certificateOperation = getCertificateOperation(idCertificateOperationPk);
		certificateOperation.setOperationState(CertificateOperationStateType.REJECTED.getCode());
		certificateOperation.getCustodyOperation().setState(CertificateOperationStateType.REJECTED.getCode());
		certificateOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
		certificateOperation.getCustodyOperation().setRegistryUser(loggerUser.getUserName());
		if(CertificateOperationType.GLOBALIZE.getCode().equals(certificateOperation.getCertificateOperationType())){
			List<PhysicalCertificate> lstCertificates = getCertificatesRelated(certificateOperation);
			for(PhysicalCertificate physicalCertificate:lstCertificates)
				physicalCertificate.setState(PhysicalCertificateStateType.REGISTERED.getCode());
		}else
			certificateOperation.getPhysicalCertificate().setState(PhysicalCertificateStateType.REGISTERED.getCode());
	}

	public void confirmCertificateOperation(Long idCertificateOperationPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		CertificateOperation certificateOperation = getCertificateOperation(idCertificateOperationPk);
		certificateOperation.setOperationState(CertificateOperationStateType.CONFIRMED.getCode());
		certificateOperation.getCustodyOperation().setState(CertificateOperationStateType.CONFIRMED.getCode());
		certificateOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		certificateOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		Issuer issuer = null;
		BigDecimal totalStock = BigDecimal.ZERO;
		Integer branchOffice = null;
		if(CertificateOperationType.GLOBALIZE.getCode().equals(certificateOperation.getCertificateOperationType())){
			List<PhysicalCertificate> lstCertificates = getCertificatesRelated(certificateOperation);
			for(PhysicalCertificate physicalCertificate:lstCertificates){
				physicalCertificate.setState(PhysicalCertificateStateType.REGISTERED.getCode());
				physicalCertificate.setSituation(PayrollDetailStateType.TRANSIT.getCode());	
				issuer = physicalCertificate.getIssuer();
				branchOffice = physicalCertificate.getBranchOffice();
				totalStock = totalStock.add(physicalCertificate.getCertificateQuantity());
			}
		}else{
			certificateOperation.getPhysicalCertificate().setState(PhysicalCertificateStateType.REGISTERED.getCode());
			certificateOperation.getPhysicalCertificate().setSituation(PayrollDetailStateType.TRANSIT.getCode());	
			issuer = certificateOperation.getPhysicalCertificate().getIssuer();
			totalStock = certificateOperation.getPhysicalCertificate().getCertificateQuantity();
			branchOffice = certificateOperation.getPhysicalCertificate().getBranchOffice();
		}
		PayrollHeader payrollHeader = new PayrollHeader();		
		payrollHeader.setPayrollNumber(new Long(13));
		payrollHeader.setPayrollState(PayrollHeaderStateType.PROCESSED.getCode());
		payrollHeader.setRegisterDate(CommonsUtilities.currentDate());
		payrollHeader.setRegisterUser(loggerUser.getUserName());
		payrollHeader.setPayrollType(PayrollType.CERTIFICATE_OPERATION.getCode());
		payrollHeader.setBranchOffice(branchOffice);
		payrollHeader.setIssuer(issuer);
		payrollHeader.setTotalStock(totalStock);
		PayrollDetail payrollDetail = new PayrollDetail();
		payrollDetail.setBranchOffice(certificateOperation.getPhysicalCertificate().getBranchOffice());
		payrollDetail.setPayrollQuantity(totalStock);
		payrollDetail.setPayrollState(PayrollDetailStateType.TRANSIT.getCode());
		payrollDetail.setCertificateOperation(certificateOperation);
		payrollDetail.setPayrollHeader(payrollHeader);
		certificateOperationServiceBean.create(payrollDetail);
		certificateOperationServiceBean.create(payrollHeader);
	}
}
