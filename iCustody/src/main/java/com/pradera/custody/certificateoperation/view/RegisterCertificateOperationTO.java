package com.pradera.custody.certificateoperation.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.custody.physical.CertificateOperation;
import com.pradera.model.custody.physical.FractionDetail;
import com.pradera.model.custody.physical.PhysicalCertificate;

public class RegisterCertificateOperationTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private GenericDataModel<PhysicalCertificate> lstPhysicalCertificates;
	private PhysicalCertificate physicalCertificate;
	private String certificateNumber;
	private Integer part;
	private CertificateOperation certificateOperation;
	private BigDecimal globalizeQuantity = BigDecimal.ZERO;
	private BigDecimal fractionPendingQuantity = BigDecimal.ZERO;
	private boolean blNoResult;
	private List<FractionDetail> lstFraction;
	
	public GenericDataModel<PhysicalCertificate> getLstPhysicalCertificates() {
		return lstPhysicalCertificates;
	}
	public void setLstPhysicalCertificates(
			GenericDataModel<PhysicalCertificate> lstPhysicalCertificates) {
		this.lstPhysicalCertificates = lstPhysicalCertificates;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public CertificateOperation getCertificateOperation() {
		return certificateOperation;
	}
	public void setCertificateOperation(CertificateOperation certificateOperation) {
		this.certificateOperation = certificateOperation;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}
	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}
	public List<FractionDetail> getLstFraction() {
		return lstFraction;
	}
	public void setLstFraction(List<FractionDetail> lstFraction) {
		this.lstFraction = lstFraction;
	}
	public Integer getPart() {
		return part;
	}
	public void setPart(Integer part) {
		this.part = part;
	}
	public BigDecimal getGlobalizeQuantity() {
		return globalizeQuantity;
	}
	public void setGlobalizeQuantity(BigDecimal globalizeQuantity) {
		this.globalizeQuantity = globalizeQuantity;
	}
	public BigDecimal getFractionPendingQuantity() {
		return fractionPendingQuantity;
	}
	public void setFractionPendingQuantity(BigDecimal fractionPendingQuantity) {
		this.fractionPendingQuantity = fractionPendingQuantity;
	}
}
