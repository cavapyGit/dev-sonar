package com.pradera.custody.certificateoperation.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.certificateoperation.view.RegisterCertificateOperationTO;
import com.pradera.custody.certificateoperation.view.SearchCertificateOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.physical.CertificateOperation;
import com.pradera.model.custody.physical.FractionDetail;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.type.PhysicalCertificateStateType;

@Stateless
public class CertificateOperationServiceBean extends CrudDaoServiceBean{
	
	public List<PhysicalCertificate> getPhysicalCertificates(RegisterCertificateOperationTO registerCertificateOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pc");
		sbQuery.append("	FROM PhysicalCertificate pc");
		sbQuery.append("	WHERE pc.branchOffice = :branchOffice");
		sbQuery.append("	AND pc.situation = :vault");
		sbQuery.append("	AND pc.state = :registered");
		if(Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getCertificateOperation().getSecurity().getIdSecurityCodePk()))
			sbQuery.append("	AND pc.securities.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getCertificateNumber()))
			sbQuery.append("	AND pc.certificateNumber = :certificateNumber");
		sbQuery.append("	ORDER BY pc.idPhysicalCertificatePk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("branchOffice", registerCertificateOperationTO.getCertificateOperation().getBranchOffice());
		if(Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getCertificateOperation().getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", registerCertificateOperationTO.getCertificateOperation().getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getCertificateNumber()))
			query.setParameter("certificateNumber", registerCertificateOperationTO.getCertificateNumber());
		query.setParameter("vault", PayrollDetailStateType.VAULT.getCode());
		query.setParameter("registered", PhysicalCertificateStateType.CONFIRMED.getCode());//REGISTERED
		return query.getResultList();
	}

	public List<CertificateOperation> getCertificateOperations(SearchCertificateOperationTO searchCertificateOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co");
		sbQuery.append("	FROM CertificateOperation co");
		sbQuery.append("	LEFT JOIN FETCH co.physicalCertificate");
		sbQuery.append("	JOIN FETCH co.security");
		sbQuery.append("	WHERE co.branchOffice = :branchOffice");
		sbQuery.append("	AND TRUNC(co.custodyOperation.registryDate) BETWEEN :initialDate AND :endDate");
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateOperationTO.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("	AND co.securities.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateOperationTO.getState()))
			sbQuery.append("	AND co.operationState = :state");
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateOperationTO.getOperationType()))
			sbQuery.append("	AND co.certificateOperationType = :certificateOperationType");
		sbQuery.append("	ORDER BY co.idCertificateOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("branchOffice", searchCertificateOperationTO.getBranchOffice());
		query.setParameter("initialDate", searchCertificateOperationTO.getInitialDate());
		query.setParameter("endDate", searchCertificateOperationTO.getEndDate());
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateOperationTO.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", searchCertificateOperationTO.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateOperationTO.getState()))
			query.setParameter("state", searchCertificateOperationTO.getState());
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateOperationTO.getOperationType()))
			query.setParameter("certificateOperationType", searchCertificateOperationTO.getOperationType());		
		return query.getResultList();
	}

	public List<PhysicalCertificate> getPhysicalCertificate(Long idCertificateOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co.physicalCertificate");
		sbQuery.append("	FROM CertificateOperation co");
		sbQuery.append("	WHERE co.idCertificateOperationPk = :idCertificateOperationPk");
		sbQuery.append("	ORDER BY co.physicalCertificate.idPhysicalCertificatePk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idCertificateOperationPk", idCertificateOperationPk);
		return query.getResultList();
	}

	public List<PhysicalCertificate> getCertificatesForGlobalize(Long idCertificateOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT gd.physicalCertificate");
		sbQuery.append("	FROM GlobalizeDetail gd");
		sbQuery.append("	WHERE gd.certificateOperation.idCertificateOperationPk = :idCertificateOperationPk");
		sbQuery.append("	ORDER BY gd.physicalCertificate.idPhysicalCertificatePk DESC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idCertificateOperationPk", idCertificateOperationPk);
		return query.getResultList();
	}

	public List<FractionDetail> getFractions(Long idCertificateOperationPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT fd");
		sbQuery.append("	FROM FractionDetail fd");
		sbQuery.append("	WHERE fd.certificateOperation.idCertificateOperationPk = :idCertificateOperationPk");
		sbQuery.append("	ORDER BY fd.partCertificate ASC");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idCertificateOperationPk", idCertificateOperationPk);
		return query.getResultList();
	}

	public CertificateOperation getCertificateOperation(Long idCertificateOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT co");
		sbQuery.append("	FROM CertificateOperation co");
		sbQuery.append("	JOIN FETCH co.security");
		sbQuery.append("	LEFT JOIN FETCH co.physicalCertificate");
		sbQuery.append("	WHERE co.idCertificateOperationPk = :idCertificateOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idCertificateOperationPk", idCertificateOperationPk);
		return (CertificateOperation) query.getSingleResult();
	}
}
