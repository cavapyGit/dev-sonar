package com.pradera.custody.certificateoperation.view;

import java.io.Serializable;
import java.util.Date;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.custody.physical.CertificateOperation;
import com.pradera.model.issuancesecuritie.Security;

public class SearchCertificateOperationTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Date initialDate, endDate;
	private boolean blNoResult;
	private GenericDataModel<CertificateOperation> lstResult;
	private Integer operationType;
	private Integer state;
	private Security security;
	private Integer branchOffice;
	private CertificateOperation selected;
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public Integer getOperationType() {
		return operationType;
	}
	public void setOperationType(Integer operationType) {
		this.operationType = operationType;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public Integer getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
	public GenericDataModel<CertificateOperation> getLstResult() {
		return lstResult;
	}
	public void setLstResult(GenericDataModel<CertificateOperation> lstResult) {
		this.lstResult = lstResult;
	}
	public CertificateOperation getSelected() {
		return selected;
	}
	public void setSelected(CertificateOperation selected) {
		this.selected = selected;
	}
}
