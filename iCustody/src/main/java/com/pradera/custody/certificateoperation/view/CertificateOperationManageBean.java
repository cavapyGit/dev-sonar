package com.pradera.custody.certificateoperation.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

//import org.jboss.solder.exception.control.ExceptionToCatch;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.custody.certificateoperation.facade.CertificateOperationFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.physical.CertificateOperation;
import com.pradera.model.custody.physical.CertificateOperationStateType;
import com.pradera.model.custody.physical.CertificateOperationType;
import com.pradera.model.custody.physical.FractionDetail;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;

@DepositaryWebBean
@LoggerCreateBean
public class CertificateOperationManageBean extends GenericBaseBean {
	
	private static final long serialVersionUID = 1L;
	private SearchCertificateOperationTO searchCertificateOperationTO;
	private Map<Integer, String> mpStates, mpOperationTypes, mpBranchOffices, mpVaults;
	private List<ParameterTable> lstStates, lstOperationTypes, lstBranchOffices, lstVaults;
	private static final String REGISTER_MAPPING = "registerCertificateOperation";
	private static final String SEARCH_MAPPING = "searchCertificateOperation";
	private static final String VIEW_MAPPING = "viewCertificateOperation";	
	private RegisterCertificateOperationTO registerCertificateOperationTO;
	private boolean blReject, blConfirm;
	@Inject
	UserPrivilege userPrivilege;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@Inject
	CertificateOperationFacade certificateOperationFacade;
	
	@PostConstruct
	public void init(){
		try {
			showPrivilegeButtons();
			fillMaps();
			searchCertificateOperationTO = new SearchCertificateOperationTO();
			searchCertificateOperationTO.setSecurity(new Security());
			searchCertificateOperationTO.setInitialDate(getCurrentSystemDate());
			searchCertificateOperationTO.setEndDate(getCurrentSystemDate());
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	public String back(){
		return SEARCH_MAPPING;
	}
	
	public void makeParts(){
		registerCertificateOperationTO.setLstFraction(null);		
		JSFUtilities.resetComponent(":frmCertificateOperation:part");
		if(Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getCertificateOperation().getFractionType())
				&& Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getPart())){
			if(registerCertificateOperationTO.getPart().equals(GeneralConstants.ONE_VALUE_INTEGER)
					|| registerCertificateOperationTO.getPart().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "Las n° del fraccionamiento deben ser mayores a 1");
				JSFUtilities.showSimpleValidationAltDialog();
				registerCertificateOperationTO.setPart(null);
				return;
			}
			BigDecimal proportionalQuantity = BigDecimal.ZERO;
			if(registerCertificateOperationTO.getCertificateOperation().getFractionType().equals(1)){
				try {
					proportionalQuantity = registerCertificateOperationTO.getPhysicalCertificate().getCertificateQuantity().divide(new BigDecimal(registerCertificateOperationTO.getPart()));	
				} catch (ArithmeticException e) {
					e.printStackTrace();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, "La cantidad debe ser un número entero.");
					JSFUtilities.showSimpleValidationAltDialog();
					registerCertificateOperationTO.setPart(null);
					return;
				}				
				if(proportionalQuantity.toString().contains(GeneralConstants.DOT)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, "La cantidad debe ser un número entero.");
					JSFUtilities.showSimpleValidationAltDialog();
					registerCertificateOperationTO.setPart(null);
					return;
				}
			}
			BigDecimal pendingQuantity = registerCertificateOperationTO.getPhysicalCertificate().getCertificateQuantity();
			List<FractionDetail> lstFraction = new ArrayList<>();
			for(int i=0;i<registerCertificateOperationTO.getPart().intValue();i++){
				FractionDetail fraction = new FractionDetail();
				fraction.setPartCertificate(new Integer(i + 1));
				fraction.setPartQuantity(null);
				if(proportionalQuantity.compareTo(BigDecimal.ZERO) != 0){
					fraction.setPartQuantity(proportionalQuantity);
					pendingQuantity = pendingQuantity.subtract(proportionalQuantity);
				}
				lstFraction.add(fraction);
			}
			registerCertificateOperationTO.setFractionPendingQuantity(pendingQuantity);
			registerCertificateOperationTO.setLstFraction(lstFraction);
		}
	}
	
	public void subtracPendingQuantity(FractionDetail fraction){
		if(Validations.validateIsNotNullAndNotEmpty(fraction.getPartQuantity())){
			BigDecimal pending = BigDecimal.ZERO;
			pending = registerCertificateOperationTO.getPhysicalCertificate().getCertificateQuantity().subtract(getRealCalculate());
			if(pending.compareTo(BigDecimal.ZERO) < 0){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, "La canitdad a superado la cantidad pendiente del certificado.");
				JSFUtilities.showSimpleValidationAltDialog();
				fraction.setPartQuantity(null);
				pending = registerCertificateOperationTO.getPhysicalCertificate().getCertificateQuantity().subtract(getRealCalculate());
			}
			registerCertificateOperationTO.setFractionPendingQuantity(pending);			
		}else{
			BigDecimal actualTotal = getRealCalculate();
			registerCertificateOperationTO.setFractionPendingQuantity(registerCertificateOperationTO.getPhysicalCertificate().getCertificateQuantity().subtract(actualTotal));			
		}
	}
	
	private BigDecimal getRealCalculate(){
		BigDecimal realCalculate = BigDecimal.ZERO;
		for(FractionDetail detail:registerCertificateOperationTO.getLstFraction())
			if(Validations.validateIsNotNullAndNotEmpty(detail.getPartQuantity()))
				realCalculate = realCalculate.add(detail.getPartQuantity());
		return realCalculate;
	}
	
	public String viewDetail(CertificateOperation certificateOperation){
		try {
			fillView(certificateOperation);
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}
	
	private void fillView(CertificateOperation certificateOperation) throws ServiceException{
		blConfirm = false;
		blReject = false;
		registerCertificateOperationTO = new RegisterCertificateOperationTO();
		registerCertificateOperationTO.setCertificateOperation(certificateOperationFacade.getCertificateOperation(certificateOperation.getIdCertificateOperationPk()));
		List<PhysicalCertificate> lstResult = new ArrayList<>();
		if(CertificateOperationType.GLOBALIZE.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType()))
			lstResult = certificateOperationFacade.getCertificatesRelated(certificateOperation);
		else
			lstResult.add(registerCertificateOperationTO.getCertificateOperation().getPhysicalCertificate());
		registerCertificateOperationTO.setLstPhysicalCertificates(new GenericDataModel<>(lstResult));
		if(CertificateOperationType.FRACTION.getCode().equals(certificateOperation.getCertificateOperationType())){
			registerCertificateOperationTO.setLstFraction(certificateOperationFacade.getFractions(certificateOperation.getIdCertificateOperationPk()));
			registerCertificateOperationTO.setPart(new Integer(registerCertificateOperationTO.getLstFraction().size()));
		}else if(CertificateOperationType.GLOBALIZE.getCode().equals(certificateOperation.getCertificateOperationType())){
			registerCertificateOperationTO.setGlobalizeQuantity(certificateOperation.getGlobalizeQuantity());
		}else if(CertificateOperationType.TRANSFER.getCode().equals(certificateOperation.getCertificateOperationType())){
			PhysicalCertificate physicalCertificate = registerCertificateOperationTO.getLstPhysicalCertificates().getDataList().get(0);
			physicalCertificate.setVaultLocationDesc(mpVaults.get(physicalCertificate.getVaultLocation()));
			registerCertificateOperationTO.getCertificateOperation().setVaultLocationTargetDesc(mpVaults.get(certificateOperation.getVaultLocationTarget()));
			registerCertificateOperationTO.setPhysicalCertificate(physicalCertificate);
		}else if(CertificateOperationType.REPLACE.getCode().equals(certificateOperation.getCertificateOperationType())){
			
		}
	}
	
	public void clean(){
		JSFUtilities.resetViewRoot();
		searchCertificateOperationTO = new SearchCertificateOperationTO();
		searchCertificateOperationTO.setSecurity(new Security());
		searchCertificateOperationTO.setInitialDate(getCurrentSystemDate());
		searchCertificateOperationTO.setEndDate(getCurrentSystemDate());
	}
	
	public void calculateTotalGlobalize(PhysicalCertificate certificate){
		if(certificate.isBlSelected())
			registerCertificateOperationTO.setGlobalizeQuantity(registerCertificateOperationTO.getGlobalizeQuantity().add(certificate.getCertificateQuantity()));
		else
			registerCertificateOperationTO.setGlobalizeQuantity(registerCertificateOperationTO.getGlobalizeQuantity().subtract(certificate.getCertificateQuantity()));		
	}
	
	public void beforeSave(){
		boolean blProcess = false;
		String bodyText = GeneralConstants.EMPTY_STRING;
		if(CertificateOperationType.FRACTION.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			if(registerCertificateOperationTO.getFractionPendingQuantity().compareTo(BigDecimal.ZERO) > 0)
				bodyText = "El fraccionamiento debe ser por toda la cantidad exacta.";
			else if(registerCertificateOperationTO.getCertificateOperation().getFractionType().equals(2)){
				boolean blEmpty = false;
				for(FractionDetail detail:registerCertificateOperationTO.getLstFraction())
					if(Validations.validateIsNullOrEmpty(detail.getPartQuantity())){
						blEmpty = true;
						break;
					}				
				if(blEmpty)
					bodyText = "Existen cantidades vacias.";
				else
					blProcess = true;
			}else
				blProcess = true;
		}else if(CertificateOperationType.GLOBALIZE.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			BigDecimal globalize = BigDecimal.ZERO;
			for(PhysicalCertificate certificate:registerCertificateOperationTO.getLstPhysicalCertificates()){
				if(certificate.isBlSelected()){
					globalize = globalize.add(BigDecimal.ONE);					
				}
			}
			if(globalize.compareTo(BigDecimal.ONE) <= 0)
				bodyText = "Para realizar la globalización es necesario más de un certificado.";
			else
				blProcess = true;			
		}else if(CertificateOperationType.TRANSFER.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			if(registerCertificateOperationTO.getPhysicalCertificate().getVaultLocation().equals(registerCertificateOperationTO.getCertificateOperation().getVaultLocationTarget())){
				bodyText = "La bóveda destino no puede ser igual a la bóveda origen";
				registerCertificateOperationTO.getCertificateOperation().setVaultLocationTarget(null);
			}else
				blProcess = true;
		}else if(CertificateOperationType.REPLACE.getCode().equals(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType())){
			
		}
		if(blProcess){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					"¿Está seguro que desea registrar la operación?");			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}else{			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), bodyText);
			JSFUtilities.showSimpleValidationDialog();
		}
		
	}
	
	public String validateReject(){
		try {
			if(Validations.validateIsNull(searchCertificateOperationTO.getSelected())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), "Debe seleccionar un registro");
				JSFUtilities.showSimpleValidationDialog();
			}else{
				if(CertificateOperationStateType.REGISTERED.getCode().equals(searchCertificateOperationTO.getSelected().getOperationState())){
					fillView(searchCertificateOperationTO.getSelected());
					blReject = true;
					return VIEW_MAPPING;
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), "La operacion debe tener el en estado REGISTRADO");
					JSFUtilities.showSimpleValidationDialog();
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}
	
	public String validateConfirm(){
		try {
			if(Validations.validateIsNull(searchCertificateOperationTO.getSelected())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), "Debe seleccionar un registro");
				JSFUtilities.showSimpleValidationDialog();
			}else{
				if(CertificateOperationStateType.REGISTERED.getCode().equals(searchCertificateOperationTO.getSelected().getOperationState())){
					fillView(searchCertificateOperationTO.getSelected());
					blConfirm = true;
					return VIEW_MAPPING;
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), "La operacion debe tener el en estado REGISTRADO");
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}
	
	public void beforeAction(){
		String headerText = GeneralConstants.EMPTY_STRING, bodyText = GeneralConstants.EMPTY_STRING;
		if(blReject){
			headerText = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT);
			bodyText = "¿Está seguro que desea RECHAZAR la operación?";
		}else if(blConfirm){
			headerText = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
			bodyText = "¿Está seguro que desea CONFIRMAR la operación?"; 
		}
		showMessageOnDialog(headerText, bodyText);			
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	@LoggerAuditWeb
	public void doAction(){
		try {
			String bodyText = GeneralConstants.EMPTY_STRING;
			if(blReject){
				certificateOperationFacade.rejectCertificateOperation(registerCertificateOperationTO.getCertificateOperation().getIdCertificateOperationPk());
				bodyText = "Se rechazó la operación";
			}else if(blConfirm){
				certificateOperationFacade.confirmCertificateOperation(registerCertificateOperationTO.getCertificateOperation().getIdCertificateOperationPk());
				bodyText = "Se confirmó la operación";
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), bodyText);			
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			search();
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	@LoggerAuditWeb
	public void save(){
		try {
			certificateOperationFacade.saveOperation(registerCertificateOperationTO);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
					"Se registro la " + mpOperationTypes.get(registerCertificateOperationTO.getCertificateOperation().getCertificateOperationType()) + ".");			
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void changeBranchOfficeTarget(){
		try {
			lstVaults = new ArrayList<>();
			registerCertificateOperationTO.getCertificateOperation().setVaultLocationTarget(null);			
			JSFUtilities.resetComponent(":frmCertificateOperation:vaultTarget");
			if(Validations.validateIsNotNullAndNotEmpty(registerCertificateOperationTO.getCertificateOperation().getBranchOfficeTarget())){				
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO.setMasterTableFk(MasterTableType.VAULTS_FOR_BRANCH.getCode());
				paramTabTO.setIdRelatedParameterFk(registerCertificateOperationTO.getCertificateOperation().getBranchOfficeTarget());
				List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
				for(ParameterTable paramTab:lstTemp){
					if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()))
						lstVaults.add(paramTab);
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	public void setDescriptions(){
		JSFUtilities.resetComponent(":frmCertificateOperation:opnlProcessed");
		registerCertificateOperationTO.getPhysicalCertificate().setVaultLocationDesc(mpVaults.get(registerCertificateOperationTO.getPhysicalCertificate().getVaultLocation()));;
		cleanProcessed();
	}
	
	public void search(){
		try {
			searchCertificateOperationTO.setBlNoResult(true);
			searchCertificateOperationTO.setLstResult(null);
			List<CertificateOperation> lstResult = certificateOperationFacade.getCertificateOperations(searchCertificateOperationTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				for(CertificateOperation certificateOperation:lstResult){
					certificateOperation.setOperationTypeDesc(mpOperationTypes.get(certificateOperation.getCertificateOperationType()));
					certificateOperation.setOperationStateDesc(mpStates.get(certificateOperation.getOperationState()));
				}
				searchCertificateOperationTO.setBlNoResult(false);
				searchCertificateOperationTO.setLstResult(new GenericDataModel<>(lstResult));
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	public void searchCertificates(){
		try {
			JSFUtilities.resetViewRoot();
			registerCertificateOperationTO.setBlNoResult(true);
			registerCertificateOperationTO.setLstPhysicalCertificates(null);
			registerCertificateOperationTO.setPhysicalCertificate(null);
			List<PhysicalCertificate> lstResult = certificateOperationFacade.getPhysicalCertificates(registerCertificateOperationTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				registerCertificateOperationTO.setBlNoResult(false);
				registerCertificateOperationTO.setLstPhysicalCertificates(new GenericDataModel<>(lstResult));
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public String register(){
		registerCertificateOperationTO = new RegisterCertificateOperationTO();
		registerCertificateOperationTO.setCertificateOperation(new CertificateOperation());		
		registerCertificateOperationTO.getCertificateOperation().setSecurity(new Security());
		return REGISTER_MAPPING;
	}
	
	public void cleanRegister(){
		JSFUtilities.resetViewRoot();
		registerCertificateOperationTO = new RegisterCertificateOperationTO();
		registerCertificateOperationTO.setCertificateOperation(new CertificateOperation());		
		registerCertificateOperationTO.getCertificateOperation().setSecurity(new Security());
	}
	
	public void changeBranchOffice(){
		JSFUtilities.resetComponent(":frmCertificateOperation:certificateOperationType");
		registerCertificateOperationTO.setBlNoResult(false);
		registerCertificateOperationTO.setLstPhysicalCertificates(null);
		registerCertificateOperationTO.setPhysicalCertificate(null);
		cleanProcessed();
	}
	
	public void changeOperationType(){
		registerCertificateOperationTO.setBlNoResult(false);
		registerCertificateOperationTO.setLstPhysicalCertificates(null);
		registerCertificateOperationTO.setPhysicalCertificate(null);
		cleanProcessed();
	}
	
	public void cleanProcessed(){
		registerCertificateOperationTO.getCertificateOperation().setFractionType(null);
		registerCertificateOperationTO.setGlobalizeQuantity(BigDecimal.ZERO);
		registerCertificateOperationTO.setPart(null);
		registerCertificateOperationTO.setLstFraction(null);
		registerCertificateOperationTO.getCertificateOperation().setBranchOfficeTarget(null);
		registerCertificateOperationTO.getCertificateOperation().setVaultLocationTarget(null);
	}
	
	public void cleanResult(){
		searchCertificateOperationTO.setLstResult(null);
		searchCertificateOperationTO.setBlNoResult(false);
	}
	
	private void fillMaps() throws ServiceException{
		mpStates = new HashMap<>();
		mpOperationTypes = new HashMap<>();
		mpBranchOffices = new HashMap<>();
		mpVaults = new HashMap<>();
		lstStates = new ArrayList<>();
		lstOperationTypes = new ArrayList<>();
		lstBranchOffices = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.CERTFICIATE_OPERATION_STATE.getCode());
		List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstStates.add(paramTab);
			}	
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.CERTIFICATE_OPERATIONS.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstOperationTypes.add(paramTab);
			}
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.VAULTS_FOR_BRANCH.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			mpVaults.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstBranchOffices.add(paramTab);
			}
			mpBranchOffices.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
	}
	
	private void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnConfirmView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	public SearchCertificateOperationTO getSearchCertificateOperationTO() {
		return searchCertificateOperationTO;
	}

	public void setSearchCertificateOperationTO(
			SearchCertificateOperationTO searchCertificateOperationTO) {
		this.searchCertificateOperationTO = searchCertificateOperationTO;
	}

	public Map<Integer, String> getMpStates() {
		return mpStates;
	}

	public void setMpStates(Map<Integer, String> mpStates) {
		this.mpStates = mpStates;
	}

	public Map<Integer, String> getMpOperationTypes() {
		return mpOperationTypes;
	}

	public void setMpOperationTypes(Map<Integer, String> mpOperationTypes) {
		this.mpOperationTypes = mpOperationTypes;
	}

	public Map<Integer, String> getMpBranchOffices() {
		return mpBranchOffices;
	}

	public void setMpBranchOffices(Map<Integer, String> mpBranchOffices) {
		this.mpBranchOffices = mpBranchOffices;
	}

	public Map<Integer, String> getMpVaults() {
		return mpVaults;
	}

	public void setMpVaults(Map<Integer, String> mpVaults) {
		this.mpVaults = mpVaults;
	}

	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	public List<ParameterTable> getLstOperationTypes() {
		return lstOperationTypes;
	}

	public void setLstOperationTypes(List<ParameterTable> lstOperationTypes) {
		this.lstOperationTypes = lstOperationTypes;
	}

	public List<ParameterTable> getLstBranchOffices() {
		return lstBranchOffices;
	}

	public void setLstBranchOffices(List<ParameterTable> lstBranchOffices) {
		this.lstBranchOffices = lstBranchOffices;
	}

	public RegisterCertificateOperationTO getRegisterCertificateOperationTO() {
		return registerCertificateOperationTO;
	}

	public void setRegisterCertificateOperationTO(
			RegisterCertificateOperationTO registerCertificateOperationTO) {
		this.registerCertificateOperationTO = registerCertificateOperationTO;
	}

	public List<ParameterTable> getLstVaults() {
		return lstVaults;
	}

	public void setLstVaults(List<ParameterTable> lstVaults) {
		this.lstVaults = lstVaults;
	}

	public boolean isBlConfirm() {
		return blConfirm;
	}

	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}

	public boolean isBlReject() {
		return blReject;
	}

	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}	
	
}
