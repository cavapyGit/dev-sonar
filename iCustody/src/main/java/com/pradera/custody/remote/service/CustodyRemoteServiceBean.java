package com.pradera.custody.remote.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.AsynchronousLoggerInterceptor;
import com.pradera.commons.contextholder.interceptor.RemoteLoggerUserInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.custody.dematerializationcertificate.service.DematerializationCertificateServiceBean;
import com.pradera.custody.retirementoperation.service.RetirementOperationServiceBean;
import com.pradera.custody.securitiesrenewal.service.SecuritiesRenewalServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.business.to.QueryParametersConsultCdpf;
import com.pradera.integration.component.business.to.SecurityConsultationTO;
import com.pradera.integration.component.custody.service.CustodyRemoteService;
import com.pradera.integration.component.custody.to.AccountBalanceQueryTO;
import com.pradera.integration.component.custody.to.AccountEntryQueryTO;
import com.pradera.integration.component.custody.to.AccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.AuthorizedSignatureQueryResponseTO;
import com.pradera.integration.component.custody.to.AuthorizedSignatureQueryTO;
import com.pradera.integration.component.custody.to.InstitutionQueryResponseTO;
import com.pradera.integration.component.custody.to.PhysicalCertificateQueryTO;
import com.pradera.integration.component.custody.to.PhysicalCertificateResponseTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.custody.to.SecuritiesRenewalQueryTO;
import com.pradera.integration.component.custody.to.SecurityEntryQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CustodyRemoteServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
public class CustodyRemoteServiceBean implements CustodyRemoteService {

	/** The transaction registry. */
	@Resource private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The dematerialization certificate service bean. */
	@EJB
	private DematerializationCertificateServiceBean dematerializationCertificateServiceBean;
	
	/** The retirement operation service bean. */
	@EJB
	private RetirementOperationServiceBean retirementOperationServiceBean;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The securities renewal service bean. */
	@EJB
	private SecuritiesRenewalServiceBean securitiesRenewalServiceBean;
	
	/** The session context. */
	@Resource private SessionContext sessionContext;
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The remove security service bean. */
	@Inject
	private RetirementOperationServiceBean removeSecurityServiceBean;

	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#createAccountEntryOperation(com.pradera.integration.component.custody.to.AccountEntryRegisterTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public Object createAccountEntryOperation(AccountEntryRegisterTO accountEntryRegisterTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return dematerializationCertificateServiceBean.validateAndSaveAccountEntry(accountEntryRegisterTO, loggerUser);
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#createAccountEntryOperationTx(com.pradera.integration.component.custody.to.AccountEntryRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType createAccountEntryOperationTx(LoggerUser loggerUser, AccountEntryRegisterTO accountEntryRegisterTO) throws ServiceException {        
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);		
		try{
        	RecordValidationType recordValidationType = dematerializationCertificateServiceBean.validateAndSaveAccountEntryTx(accountEntryRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}  
	}
		
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#renewSecuritiesOperationTx(com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */	
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType renewSecuritiesOperationTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        try {					
        	RecordValidationType recordValidationType = securitiesRenewalServiceBean.validateAndSaveRenewSecuritiesTx(stockEarlyPaymentRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#createEarlyPaymentOperation(com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public Object createEarlyPaymentOperation(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return retirementOperationServiceBean.validateAndSaveEarlyPayment(stockEarlyPaymentRegisterTO, loggerUser);
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#createEarlyPaymentOperationTx(com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType createEarlyPaymentOperationTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        try {					
        	RecordValidationType recordValidationType = retirementOperationServiceBean.validateAndSaveEarlyPaymentTx(stockEarlyPaymentRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}        
		
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#reverseEarlyPaymentOperationTx(com.pradera.integration.component.custody.to.ReverseRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType reverseEarlyPaymentOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException {		
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);        
        try{
        	RecordValidationType recordValidationType = retirementOperationServiceBean.validateAndSaveReverseEarlyPaymentTx(reverseRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}
	
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType revreverseEarlyPaymentNoDpfTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException {		
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);        
        try{
        	RecordValidationType recordValidationType = retirementOperationServiceBean.validateAndSaveReverseEarlyPaymentNoDpfTx(reverseRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#reverseAccountEntryOperationTx(com.pradera.integration.component.custody.to.ReverseRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType reverseAccountEntryOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException {
		
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);        
        try{
        	RecordValidationType recordValidationType = dematerializationCertificateServiceBean.validateAndSaveReverseAccountEntryTx(reverseRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}
		
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#reverseRenewSecuritiesOperationTx(com.pradera.integration.component.custody.to.ReverseRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType reverseRenewSecuritiesOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);        
        try{
        	RecordValidationType recordValidationType = securitiesRenewalServiceBean.validateAndSaveReverseRenewSecuritiesTx(reverseRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			//e.printStackTrace();
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#getListDpfHolderAccountBalance(com.pradera.integration.component.custody.to.AccountBalanceQueryTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListDpfHolderAccountBalance(AccountBalanceQueryTO accountBalanceQueryTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        accountBalanceQueryTO.setOperationNumber(accountBalanceQueryTO.getQueryNumber());
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	lstRecordValidationTypes= holderBalanceMovementsServiceBean.validateAccountQueryInformation(accountBalanceQueryTO);
            if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
            	accountBalanceQueryTO= holderBalanceMovementsServiceBean.getAccountBalanceQuery(accountBalanceQueryTO);
            	
            	RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(accountBalanceQueryTO, 
    																	GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
    			lstRecordValidationTypes.add(objRecordValidationType);
    			transactionState= BooleanType.YES.getCode();
            }
            interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountBalanceQueryTO, transactionState, loggerUser);
        } catch (Exception e) {
        	//e.printStackTrace();
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountBalanceQueryTO, transactionState, loggerUser);
        	throw e;
        }
		return lstRecordValidationTypes;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#getListDpfAccountEntryOperation(com.pradera.integration.component.custody.to.AccountEntryQueryTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListDpfAccountEntryOperation(AccountEntryQueryTO accountEntryQueryTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        accountEntryQueryTO.setOperationNumber(accountEntryQueryTO.getQueryNumber());
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	lstRecordValidationTypes= dematerializationCertificateServiceBean.validateAccountEntryQueryInformation(accountEntryQueryTO);
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		accountEntryQueryTO= dematerializationCertificateServiceBean.getAccountEntryQueryTO(accountEntryQueryTO);
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(accountEntryQueryTO, 
																		GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryQueryTO, transactionState, loggerUser);
        } catch (Exception e) {
        	//e.printStackTrace();
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryQueryTO, transactionState, loggerUser);
        	throw e;
        }
		return lstRecordValidationTypes;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#getListDpfSecuritiesRenewalOperation(com.pradera.integration.component.custody.to.SecuritiesRenewalQueryTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListDpfSecuritiesRenewalOperation(SecuritiesRenewalQueryTO securitiesRenewalQueryTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        securitiesRenewalQueryTO.setOperationNumber(securitiesRenewalQueryTO.getQueryNumber());
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	lstRecordValidationTypes= securitiesRenewalServiceBean.validateSecuritiesRenewalQueryInformation(securitiesRenewalQueryTO);
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		securitiesRenewalQueryTO= securitiesRenewalServiceBean.getSecuritiesRenewalQueryTO(securitiesRenewalQueryTO);
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(securitiesRenewalQueryTO, 
										GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(securitiesRenewalQueryTO, transactionState, loggerUser);
        } catch (Exception e) {
        	//e.printStackTrace();
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(securitiesRenewalQueryTO, transactionState, loggerUser);
        	throw e;
        }
		return lstRecordValidationTypes;
	}

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#getListDpfEarlyPaymentOperation(com.pradera.integration.component.custody.to.StockEarlyPaymentQueryTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListDpfEarlyPaymentOperation(StockEarlyPaymentQueryTO stockEarlyPaymentQueryTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        stockEarlyPaymentQueryTO.setOperationNumber(stockEarlyPaymentQueryTO.getQueryNumber());
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	lstRecordValidationTypes= retirementOperationServiceBean.validateEarlyPaymentQueryInformation(stockEarlyPaymentQueryTO);
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		stockEarlyPaymentQueryTO= retirementOperationServiceBean.getStockEarlyPaymentQueryTO(stockEarlyPaymentQueryTO);
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentQueryTO, 
										GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentQueryTO, transactionState, loggerUser);
        } catch (Exception e) {
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentQueryTO, transactionState, loggerUser);
        	throw e;
        }
		return lstRecordValidationTypes;
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#createEarlyPaymentOperationBcbTgnTx(com.pradera.integration.contextholder.LoggerUser, com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType createEarlyPaymentOperationBcbTgnTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        try {					
        	RecordValidationType recordValidationType = removeSecurityServiceBean.validateAndSaveEarlyPaymentTx(stockEarlyPaymentRegisterTO, loggerUser);     
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {		
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}	
	}
	
	
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType createEarlyPaymentNoDPFOperationTx(LoggerUser loggerUser, StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO) throws ServiceException {
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);
        try {					
        	RecordValidationType recordValidationType = removeSecurityServiceBean.validateAndSaveEarlyPaymentNoDPFTx(stockEarlyPaymentRegisterTO, loggerUser);     
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {		
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}	
	}
	
	//validateAndSaveEarlyPaymentNoDPFTx

	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#getListBbxBtxSecuritiesQueryOperation(com.pradera.integration.component.custody.to.SecurityEntryQueryTO)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListBbxBtxSecuritiesQueryOperation(SecurityEntryQueryTO securityEntryQueryTO) throws ServiceException {		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        securityEntryQueryTO.setOperationNumber(securityEntryQueryTO.getQueryNumber());
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {        	
        	lstRecordValidationTypes= dematerializationCertificateServiceBean.validateSecurityEntryQueryInformation(securityEntryQueryTO);        	
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		securityEntryQueryTO= dematerializationCertificateServiceBean.getSecurityEntryQueryTO(securityEntryQueryTO);
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(securityEntryQueryTO, 
																		GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(securityEntryQueryTO, transactionState, loggerUser);        	        	        	
        } catch (Exception e) {
        	//e.printStackTrace();
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(securityEntryQueryTO, transactionState, loggerUser);
        	throw e;
        }
		return lstRecordValidationTypes;				
	}

	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#getListRegisteredDpf(com.pradera.integration.component.business.to.QueryParametersConsultCdpf)
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getListRegisteredDpf(QueryParametersConsultCdpf consultCdpf){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        List<RecordValidationType> lstRecordValidationTypes = new ArrayList<>();
        //Validacion para el emisor
		boolean existsIssuer = dematerializationCertificateServiceBean.isExistsIssuer(consultCdpf);
		if (!existsIssuer){
			RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(consultCdpf,GenericPropertiesConstants.MSG_DISPLAY_LBL_ISSUER_NO_VALIDE, null);
			objRecordValidationType.setErrorCode("50000");
			lstRecordValidationTypes.add(objRecordValidationType);
			transactionState= BooleanType.YES.getCode();
			consultCdpf.setError(true);
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(consultCdpf, transactionState, loggerUser);
			return lstRecordValidationTypes;
		}
		//validacion para el instrumento
		if (!consultCdpf.getCodInst().equals("DPF")){
			RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(consultCdpf,GenericPropertiesConstants.MSG_DISPLAY_LBL_INSTRUMENT_NO_VALIDE, null);
			objRecordValidationType.setErrorCode("5001");
			lstRecordValidationTypes.add(objRecordValidationType);
			transactionState= BooleanType.YES.getCode();
			consultCdpf.setError(true);
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(consultCdpf, transactionState, loggerUser);
			return lstRecordValidationTypes;
		}
		//validacion de cod serie
		boolean errorCodSerie;
		if(consultCdpf.getcSerie().length()==0)
			errorCodSerie= true;
		else
			errorCodSerie=dematerializationCertificateServiceBean.isExistsCodSerie(consultCdpf.getcSerie());
		
		if (!errorCodSerie){
			RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(consultCdpf,GenericPropertiesConstants.MSG_DISPLAY_LBL_SERIE_NO_VALIDE, null);
			objRecordValidationType.setErrorCode("0016");
			lstRecordValidationTypes.add(objRecordValidationType);
			transactionState= BooleanType.YES.getCode();
			consultCdpf.setError(true);
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(consultCdpf, transactionState, loggerUser);
			return lstRecordValidationTypes;
		}
		
		
        List<SecurityConsultationTO> list= dematerializationCertificateServiceBean.getDematerializedSecurities(consultCdpf);
        for (SecurityConsultationTO securityConsultationTO : list) {
        	consultCdpf.getConsultedSecurities().add(securityConsultationTO);
		}
        RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(consultCdpf,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		transactionState= BooleanType.YES.getCode();
		interfaceComponentServiceBean.get().saveInterfaceTransactionTx(consultCdpf, transactionState, loggerUser);
		return lstRecordValidationTypes;
	}
	/***
	 * 
	 * @param authorizedSignatureQueryTO
	 * @return
	 * @throws ServiceException
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getAuthorizedSignatureList(AuthorizedSignatureQueryTO authorizedSignatureQueryTO) throws ServiceException {	
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        AuthorizedSignatureQueryResponseTO authorizedSignatureQueryResponseTO = null;
        List<RecordValidationType> lstRecordValidationTypes = new ArrayList<RecordValidationType>();
        try {
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		authorizedSignatureQueryResponseTO = dematerializationCertificateServiceBean.getListAuthorizedSignatures(authorizedSignatureQueryTO);
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(authorizedSignatureQueryResponseTO,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        	throw e;
        }
		return lstRecordValidationTypes;				
	}
	
	/**
	 * Gets List Institutions
	 * @return
	 * @throws ServiceException
	 */
	@Override
	@Interceptors(RemoteLoggerUserInterceptor.class)
	public List<RecordValidationType> getInstitutionList() throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER.name());
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        Integer transactionState= BooleanType.NO.getCode();
        InstitutionQueryResponseTO institutionQueryResponseTO = null;
        List<RecordValidationType> lstRecordValidationTypes = new ArrayList<RecordValidationType>();
        try {
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		institutionQueryResponseTO = dematerializationCertificateServiceBean.getListInstitution();
        		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(institutionQueryResponseTO,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        	throw e;
        }
		return lstRecordValidationTypes;				
	}
	
	
	@Override
	public List<RecordValidationType> getListPhysicalCertificate(PhysicalCertificateQueryTO physicalCertificateQueryTO) throws ServiceException {	
        Integer transactionState= BooleanType.NO.getCode();
        PhysicalCertificateResponseTO physicalCertificateResponseTO = null;
        
        List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
        try {
        	if (Validations.validateListIsNullOrEmpty(lstRecordValidationTypes)) {
        		//ejecutamos el query con los parametros enviados
        		physicalCertificateResponseTO = dematerializationCertificateServiceBean.consultPhysicalCertificate(physicalCertificateQueryTO);
        		physicalCertificateResponseTO.setOperationNumber(physicalCertificateQueryTO.getOperationNumber());
        		physicalCertificateResponseTO.setOperationCode(physicalCertificateQueryTO.getOperationCode());
        		physicalCertificateResponseTO.setOperationDate(physicalCertificateQueryTO.getOperationDate());
                                      		
        		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(physicalCertificateResponseTO,GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
				lstRecordValidationTypes.add(objRecordValidationType);
				transactionState= BooleanType.YES.getCode();
        	}
        	
        } catch (Exception e) {
        	e.printStackTrace();
        	throw e;
        }
		return lstRecordValidationTypes;				
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#createAccountEntryNewOperationTx(com.pradera.integration.component.custody.to.AccountEntryRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType createAccountEntryNewOperationTx(LoggerUser loggerUser,AccountEntryRegisterTO accountEntryRegisterTO) throws ServiceException {        
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);		
		try{
        	RecordValidationType recordValidationType = dematerializationCertificateServiceBean.validateAndSaveAccountEntryNewTx(accountEntryRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(accountEntryRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}  
	}
	
	/* (non-Javadoc)
	 * @see com.pradera.integration.component.custody.service.CustodyRemoteService#reverseRegistrationPlacementOperationTx(com.pradera.integration.component.custody.to.ReverseRegisterTO, com.pradera.integration.contextholder.LoggerUser)
	 */
	@Override
	@Interceptors(AsynchronousLoggerInterceptor.class)
	public RecordValidationType reverseRegistrationPlacementOperationTx(LoggerUser loggerUser, ReverseRegisterTO reverseRegisterTO) throws ServiceException {
		
		ThreadLocalContextHolder.put(RegistryContextHolderType.LOGGER_USER.name(), loggerUser);        
        try{
        	RecordValidationType recordValidationType = dematerializationCertificateServiceBean.validateAndSaveReverseRegistrationPlacementTx(reverseRegisterTO, loggerUser);
        	interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.YES.getCode(), loggerUser);
        	return recordValidationType;
		} catch (Exception e) {
			interfaceComponentServiceBean.get().saveInterfaceTransactionTx(reverseRegisterTO, BooleanType.NO.getCode(), loggerUser);
			throw e;
		}
	}
	
}
