package com.pradera.custody.reversals.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RequestReversalsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class RequestReversalsTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id unblock request pk. */
	private Long idUnblockRequestPk;
	
	/** The number record. */
	private String numberRecord;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The holder description. */
	private String holderDescription;	
	
	/** The registry date. */
	private Date registryDate;
	
	/** The confirmation user. */
	private String confirmationUser;
	
	/** The state. */
	private String state;
	
	/** The id state. */
	private Integer idState;
	
	/** The motive. */
	private String motive;
	
	/** The amount unblock. */
	private String amountUnblock;
	
	/** The business process type id. */
	private Long businessProcessTypeId;
	
	/** The id issuer. */
	private String idIssuer;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The motive reject. */
	private Integer motiveReject;
	
	/** The motive reject other. */
	private String motiveRejectOther;
	
	/** The approve user. */
	private String approveUser;
	
	/** The register user. */
	private String registerUser;
	
	/** The annul user. */
	private String annulUser;
	
	
	/**
	 * Gets the id unblock request pk.
	 *
	 * @return the id unblock request pk
	 */
	public Long getIdUnblockRequestPk() {
		return idUnblockRequestPk;
	}
	
	/**
	 * Sets the id unblock request pk.
	 *
	 * @param idUnblockRequestPk the new id unblock request pk
	 */
	public void setIdUnblockRequestPk(Long idUnblockRequestPk) {
		this.idUnblockRequestPk = idUnblockRequestPk;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public String getMotive() {
		return motive;
	}
	
	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(String motive) {
		this.motive = motive;
	}
	
	/**
	 * Gets the amount unblock.
	 *
	 * @return the amount unblock
	 */
	public String getAmountUnblock() {
		return amountUnblock;
	}
	
	/**
	 * Sets the amount unblock.
	 *
	 * @param amountUnblock the new amount unblock
	 */
	public void setAmountUnblock(String amountUnblock) {
		this.amountUnblock = amountUnblock;
	}
	
	/**
	 * Gets the id state.
	 *
	 * @return the id state
	 */
	public Integer getIdState() {
		return idState;
	}
	
	/**
	 * Sets the id state.
	 *
	 * @param idState the new id state
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}
	
	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}
	
	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}
	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}
	
	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}
	
	/**
	 * Gets the business process type id.
	 *
	 * @return the businessProcessTypeId
	 */
	public Long getBusinessProcessTypeId() {
		return businessProcessTypeId;
	}
	
	/**
	 * Sets the business process type id.
	 *
	 * @param businessProcessTypeId the businessProcessTypeId to set
	 */
	public void setBusinessProcessTypeId(Long businessProcessTypeId) {
		this.businessProcessTypeId = businessProcessTypeId;
	}
	
	/**
	 * Gets the id issuer.
	 *
	 * @return the id issuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}
	
	/**
	 * Sets the id issuer.
	 *
	 * @param idIssuer the new id issuer
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the motive reject.
	 *
	 * @return the motive reject
	 */
	public Integer getMotiveReject() {
		return motiveReject;
	}
	
	/**
	 * Sets the motive reject.
	 *
	 * @param motiveReject the new motive reject
	 */
	public void setMotiveReject(Integer motiveReject) {
		this.motiveReject = motiveReject;
	}
	
	/**
	 * Gets the motive reject other.
	 *
	 * @return the motive reject other
	 */
	public String getMotiveRejectOther() {
		return motiveRejectOther;
	}
	
	/**
	 * Sets the motive reject other.
	 *
	 * @param motiveRejectOther the new motive reject other
	 */
	public void setMotiveRejectOther(String motiveRejectOther) {
		this.motiveRejectOther = motiveRejectOther;
	}
	
	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}
	
	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}
	
	/**
	 * Gets the annul user.
	 *
	 * @return the annul user
	 */
	public String getAnnulUser() {
		return annulUser;
	}
	
	/**
	 * Sets the annul user.
	 *
	 * @param annulUser the new annul user
	 */
	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}
	
	/**
	 * Gets the register user.
	 *
	 * @return the register user
	 */
	public String getRegisterUser() {
		return registerUser;
	}
	
	/**
	 * Sets the register user.
	 *
	 * @param registerUser the new register user
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}
	
}
