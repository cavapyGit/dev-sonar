package com.pradera.custody.reversals.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockOpeForReversalsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class BlockOpeForReversalsTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id block operation pk. */
	private Long idBlockOperationPk;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The id unblock operation pk. */
	private Long idUnblockOperationPk;
	
	/** The number record. */
	private String numberRecord;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The confirm date. */
	private Date confirmDate;
	
	/** The id block level. */
	private Integer idBlockLevel;
	
	/** The block level. */
	private String blockLevel;
	
	/** The block type. */
	private String blockType;
	
	/** The id block type. */
	private Integer idBlockType;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The account number. */
	private String accountNumber;
	
	/** The alternate code. */
	private String alternateCode;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The id isin code. */
	private String idIsinCode;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The isin description. */
	private String isinDescription;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The holder description. */
	private String holderDescription;
	
	/** The amount. */
	private BigDecimal amount;
	
	/** The current amount. */
	private BigDecimal currentAmount;
	
	/** The form affectation. */
	private String formAffectation;
	
	/** The lst benefits. */
	private List<ParameterTable> lstBenefits;
	
	/** The id block entity pk. */
	private Long idBlockEntityPk;
	
	/** The block entity name. */
	private String blockEntityName;
	
	/** The state. */
	private String state;
	
	/** The amount unblock. */
	private BigDecimal amountUnblock;
	
	/** The bl check. */
	private boolean blCheck;
	
	/** The max size. */
	private Integer maxSize;
	
	/** The id change ownership detail fk. */
	private Long idChangeOwnershipDetailFk;
	
	/** The id operation enforce detail fk. */
	private Long idOperationEnforceDetailFk;
	
	/** The block date. */
	private Date blockDate;
	
	/** The check auto settled. */
	private boolean checkAutoSettled;
	
	/** The check authority restricted. */
	private boolean checkAuthorityRestricted;
	
	/** The bl pawn. */
	private boolean blPawn;
	
	/** The bl ban. */
	private boolean blBan;
	
	/** The reversals mark fact list. */
	private List<ReversalsMarkFactTO> reversalsMarkFactList;
	
	/** The market lock. */
	private boolean marketLock;
	
	/** The confirm user. */
	private String confirmUser;
	
	/** The holders name. */
	private String holdersName;
	
	/**
	 * Gets the id change ownership detail fk.
	 *
	 * @return the id change ownership detail fk
	 */
	public Long getIdChangeOwnershipDetailFk() {
		return idChangeOwnershipDetailFk;
	}
	
	/**
	 * Sets the id change ownership detail fk.
	 *
	 * @param idChangeOwnershipDetailFk the new id change ownership detail fk
	 */
	public void setIdChangeOwnershipDetailFk(Long idChangeOwnershipDetailFk) {
		this.idChangeOwnershipDetailFk = idChangeOwnershipDetailFk;
	}
	
	/**
	 * Gets the id operation enforce detail fk.
	 *
	 * @return the id operation enforce detail fk
	 */
	public Long getIdOperationEnforceDetailFk() {
		return idOperationEnforceDetailFk;
	}
	
	/**
	 * Sets the id operation enforce detail fk.
	 *
	 * @param idOperationEnforceDetailFk the new id operation enforce detail fk
	 */
	public void setIdOperationEnforceDetailFk(Long idOperationEnforceDetailFk) {
		this.idOperationEnforceDetailFk = idOperationEnforceDetailFk;
	}
	
	/**
	 * Gets the id block operation pk.
	 *
	 * @return the id block operation pk
	 */
	public Long getIdBlockOperationPk() {
		return idBlockOperationPk;
	}
	
	/**
	 * Sets the id block operation pk.
	 *
	 * @param idBlockOperationPk the new id block operation pk
	 */
	public void setIdBlockOperationPk(Long idBlockOperationPk) {
		this.idBlockOperationPk = idBlockOperationPk;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the block level.
	 *
	 * @return the block level
	 */
	public String getBlockLevel() {
		return blockLevel;
	}
	
	/**
	 * Sets the block level.
	 *
	 * @param blockLevel the new block level
	 */
	public void setBlockLevel(String blockLevel) {
		this.blockLevel = blockLevel;
	}
	
	/**
	 * Gets the block type.
	 *
	 * @return the block type
	 */
	public String getBlockType() {
		return blockType;
	}
	
	/**
	 * Sets the block type.
	 *
	 * @param blockType the new block type
	 */
	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}
	
	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}
	
	/**
	 * Gets the isin description.
	 *
	 * @return the isin description
	 */
	public String getIsinDescription() {
		return isinDescription;
	}
	
	/**
	 * Sets the isin description.
	 *
	 * @param isinDescription the new isin description
	 */
	public void setIsinDescription(String isinDescription) {
		this.isinDescription = isinDescription;
	}
	
	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	
	/**
	 * Sets the amount.
	 *
	 * @param amount the new amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the amount unblock.
	 *
	 * @return the amount unblock
	 */
	public BigDecimal getAmountUnblock() {
		return amountUnblock;
	}
	
	/**
	 * Sets the amount unblock.
	 *
	 * @param amountUnblock the new amount unblock
	 */
	public void setAmountUnblock(BigDecimal amountUnblock) {
		this.amountUnblock = amountUnblock;
	}
	
	/**
	 * Checks if is bl check.
	 *
	 * @return true, if is bl check
	 */
	public boolean isBlCheck() {
		return blCheck;
	}
	
	/**
	 * Sets the bl check.
	 *
	 * @param blCheck the new bl check
	 */
	public void setBlCheck(boolean blCheck) {
		this.blCheck = blCheck;
	}
	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}
	
	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}
	
	/**
	 * Gets the current amount.
	 *
	 * @return the current amount
	 */
	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}
	
	/**
	 * Sets the current amount.
	 *
	 * @param currentAmount the new current amount
	 */
	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}
	
	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}
	
	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the form affectation.
	 *
	 * @return the form affectation
	 */
	public String getFormAffectation() {
		return formAffectation;
	}
	
	/**
	 * Sets the form affectation.
	 *
	 * @param formAffectation the new form affectation
	 */
	public void setFormAffectation(String formAffectation) {
		this.formAffectation = formAffectation;
	}
	
	/**
	 * Gets the lst benefits.
	 *
	 * @return the lst benefits
	 */
	public List<ParameterTable> getLstBenefits() {
		return lstBenefits;
	}
	
	/**
	 * Sets the lst benefits.
	 *
	 * @param lstBenefits the new lst benefits
	 */
	public void setLstBenefits(List<ParameterTable> lstBenefits) {
		this.lstBenefits = lstBenefits;
	}
	
	/**
	 * Gets the id block entity pk.
	 *
	 * @return the id block entity pk
	 */
	public Long getIdBlockEntityPk() {
		return idBlockEntityPk;
	}
	
	/**
	 * Sets the id block entity pk.
	 *
	 * @param idBlockEntityPk the new id block entity pk
	 */
	public void setIdBlockEntityPk(Long idBlockEntityPk) {
		this.idBlockEntityPk = idBlockEntityPk;
	}
	
	/**
	 * Gets the block entity name.
	 *
	 * @return the block entity name
	 */
	public String getBlockEntityName() {
		return blockEntityName;
	}
	
	/**
	 * Sets the block entity name.
	 *
	 * @param blockEntityName the new block entity name
	 */
	public void setBlockEntityName(String blockEntityName) {
		this.blockEntityName = blockEntityName;
	}
	
	/**
	 * Gets the alternate code.
	 *
	 * @return the alternate code
	 */
	public String getAlternateCode() {
		return alternateCode;
	}
	
	/**
	 * Sets the alternate code.
	 *
	 * @param alternateCode the new alternate code
	 */
	public void setAlternateCode(String alternateCode) {
		this.alternateCode = alternateCode;
	}
	
	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}
	
	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}
	
	/**
	 * Gets the max size.
	 *
	 * @return the max size
	 */
	public Integer getMaxSize() {
		return maxSize;
	}
	
	/**
	 * Sets the max size.
	 *
	 * @param maxSize the new max size
	 */
	public void setMaxSize(Integer maxSize) {
		this.maxSize = maxSize;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	
	/**
	 * Gets the id block level.
	 *
	 * @return the id block level
	 */
	public Integer getIdBlockLevel() {
		return idBlockLevel;
	}
	
	/**
	 * Sets the id block level.
	 *
	 * @param idBlockLevel the new id block level
	 */
	public void setIdBlockLevel(Integer idBlockLevel) {
		this.idBlockLevel = idBlockLevel;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id unblock operation pk.
	 *
	 * @return the id unblock operation pk
	 */
	public Long getIdUnblockOperationPk() {
		return idUnblockOperationPk;
	}
	
	/**
	 * Sets the id unblock operation pk.
	 *
	 * @param idUnblockOperationPk the new id unblock operation pk
	 */
	public void setIdUnblockOperationPk(Long idUnblockOperationPk) {
		this.idUnblockOperationPk = idUnblockOperationPk;
	}
	
	/**
	 * Gets the id isin code.
	 *
	 * @return the id isin code
	 */
	public String getIdIsinCode() {
		return idIsinCode;
	}
	
	/**
	 * Sets the id isin code.
	 *
	 * @param idIsinCode the new id isin code
	 */
	public void setIdIsinCode(String idIsinCode) {
		this.idIsinCode = idIsinCode;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the reversals mark fact list.
	 *
	 * @return the reversals mark fact list
	 */
	public List<ReversalsMarkFactTO> getReversalsMarkFactList() {
		return reversalsMarkFactList;
	}
	
	/**
	 * Sets the reversals mark fact list.
	 *
	 * @param reversalsMarkFactList the new reversals mark fact list
	 */
	public void setReversalsMarkFactList(
			List<ReversalsMarkFactTO> reversalsMarkFactList) {
		this.reversalsMarkFactList = reversalsMarkFactList;
	}
	
	/**
	 * Gets the id block type.
	 *
	 * @return the id block type
	 */
	public Integer getIdBlockType() {
		return idBlockType;
	}
	
	/**
	 * Sets the id block type.
	 *
	 * @param idBlockType the new id block type
	 */
	public void setIdBlockType(Integer idBlockType) {
		this.idBlockType = idBlockType;
	}
	
	/**
	 * Gets the block date.
	 *
	 * @return the block date
	 */
	public Date getBlockDate() {
		return blockDate;
	}
	
	/**
	 * Sets the block date.
	 *
	 * @param blockDate the new block date
	 */
	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}
	
	/**
	 * Checks if is market lock.
	 *
	 * @return true, if is market lock
	 */
	public boolean isMarketLock() {
		return marketLock;
	}
	
	/**
	 * Sets the market lock.
	 *
	 * @param marketLock the new market lock
	 */
	public void setMarketLock(boolean marketLock) {
		this.marketLock = marketLock;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Checks if is check auto settled.
	 *
	 * @return true, if is check auto settled
	 */
	public boolean isCheckAutoSettled() {
		return checkAutoSettled;
	}
	
	/**
	 * Sets the check auto settled.
	 *
	 * @param checkAutoSettled the new check auto settled
	 */
	public void setCheckAutoSettled(boolean checkAutoSettled) {
		this.checkAutoSettled = checkAutoSettled;
	}
	
	/**
	 * Checks if is check authority restricted.
	 *
	 * @return true, if is check authority restricted
	 */
	public boolean isCheckAuthorityRestricted() {
		return checkAuthorityRestricted;
	}
	
	/**
	 * Sets the check authority restricted.
	 *
	 * @param checkAuthorityRestricted the new check authority restricted
	 */
	public void setCheckAuthorityRestricted(boolean checkAuthorityRestricted) {
		this.checkAuthorityRestricted = checkAuthorityRestricted;
	}
	
	/**
	 * Checks if is bl pawn.
	 *
	 * @return true, if is bl pawn
	 */
	public boolean isBlPawn() {
		return blPawn;
	}
	
	/**
	 * Sets the bl pawn.
	 *
	 * @param blPawn the new bl pawn
	 */
	public void setBlPawn(boolean blPawn) {
		this.blPawn = blPawn;
	}
	
	/**
	 * Checks if is bl ban.
	 *
	 * @return true, if is bl ban
	 */
	public boolean isBlBan() {
		return blBan;
	}
	
	/**
	 * Sets the bl ban.
	 *
	 * @param blBan the new bl ban
	 */
	public void setBlBan(boolean blBan) {
		this.blBan = blBan;
	}
	
	/**
	 * Gets the confirm user.
	 *
	 * @return the confirmUser
	 */
	public String getConfirmUser() {
		return confirmUser;
	}
	
	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the confirmUser to set
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the holders name.
	 *
	 * @return the holdersName
	 */
	public String getHoldersName() {
		return holdersName;
	}

	/**
	 * Sets the holders name.
	 *
	 * @param holdersName the holdersName to set
	 */
	public void setHoldersName(String holdersName) {
		this.holdersName = holdersName;
	}
	
	
	
}