package com.pradera.custody.reversals.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.integration.common.type.BooleanType;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterReversalTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class RegisterReversalTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id unblock request pk. */
	private Long idUnblockRequestPk;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The holder description. */
	private String holderDescription;
	
	/** The document number. */
	private String documentNumber;
	
	/** The lst motive unblock. */
	private List<ParameterTable> lstMotiveUnblock;
	
	/** The lst motive reject. */
	private List<ParameterTable> lstMotiveReject;
	
	/** The param table motive unblock. */
	private ParameterTable paramTableMotiveUnblock;
	
	/** The total unblock. */
	private BigDecimal totalUnblock;
	
	/** The lst block ope for reversals. */
	private List<BlockOpeForReversalsTO> lstBlockOpeForReversals;
	
	/** The document file1. */
	private byte[] documentFile1;
	
	/** The file name1. */
	private String fileName1;
	
	/** The document file2. */
	private byte[] documentFile2;
	
	/** The file name2. */
	private String fileName2;
	
	/** The bl save request. */
	private boolean blSaveRequest;
	
	/** The state. */
	private String state;
	
	/** The id state. */
	private Integer idState;
	
	/** The motive unblock. */
	private String motiveUnblock;
	
	/** The registry user. */
	private String registryUser;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The approve user. */
	private String approveUser;
	
	/** The approve date. */
	private Date approveDate;
	
	/** The annul user. */
	private String annulUser;
	
	/** The annul date. */
	private Date annulDate;
	
	/** The confirmation user. */
	private String confirmationUser;
	
	/** The confirmation date. */
	private Date confirmationDate;
	
	/** The reject user. */
	private String rejectUser;
	
	/** The reject date. */
	private Date rejectDate;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject other motive. */
	private String rejectOtherMotive;
	
	/** The reject motive desc. */
	private String rejectMotiveDesc;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The obj unblock request. */
	UnblockRequest objUnblockRequest;
	
	/** The ind validate block document. */
	private Integer indValidateBlockDocument = BooleanType.YES.getCode();
	
	/**
	 * Gets the id unblock request pk.
	 *
	 * @return the id unblock request pk
	 */
	public Long getIdUnblockRequestPk() {
		return idUnblockRequestPk;
	}
	
	/**
	 * Sets the id unblock request pk.
	 *
	 * @param idUnblockRequestPk the new id unblock request pk
	 */
	public void setIdUnblockRequestPk(Long idUnblockRequestPk) {
		this.idUnblockRequestPk = idUnblockRequestPk;
	}
	
	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	/**
	 * Gets the lst motive unblock.
	 *
	 * @return the lst motive unblock
	 */
	public List<ParameterTable> getLstMotiveUnblock() {
		return lstMotiveUnblock;
	}
	
	/**
	 * Sets the lst motive unblock.
	 *
	 * @param lstMotiveUnblock the new lst motive unblock
	 */
	public void setLstMotiveUnblock(List<ParameterTable> lstMotiveUnblock) {
		this.lstMotiveUnblock = lstMotiveUnblock;
	}
	
	/**
	 * Gets the param table motive unblock.
	 *
	 * @return the param table motive unblock
	 */
	public ParameterTable getParamTableMotiveUnblock() {
		return paramTableMotiveUnblock;
	}
	
	/**
	 * Sets the param table motive unblock.
	 *
	 * @param paramTableMotiveUnblock the new param table motive unblock
	 */
	public void setParamTableMotiveUnblock(ParameterTable paramTableMotiveUnblock) {
		this.paramTableMotiveUnblock = paramTableMotiveUnblock;
	}
	
	/**
	 * Gets the total unblock.
	 *
	 * @return the total unblock
	 */
	public BigDecimal getTotalUnblock() {
		return totalUnblock;
	}
	
	/**
	 * Sets the total unblock.
	 *
	 * @param totalUnblock the new total unblock
	 */
	public void setTotalUnblock(BigDecimal totalUnblock) {
		this.totalUnblock = totalUnblock;
	}
	
	/**
	 * Gets the lst block ope for reversals.
	 *
	 * @return the lst block ope for reversals
	 */
	public List<BlockOpeForReversalsTO> getLstBlockOpeForReversals() {
		return lstBlockOpeForReversals;
	}
	
	/**
	 * Sets the lst block ope for reversals.
	 *
	 * @param lstBlockOpeForReversals the new lst block ope for reversals
	 */
	public void setLstBlockOpeForReversals(
			List<BlockOpeForReversalsTO> lstBlockOpeForReversals) {
		this.lstBlockOpeForReversals = lstBlockOpeForReversals;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Checks if is bl save request.
	 *
	 * @return true, if is bl save request
	 */
	public boolean isBlSaveRequest() {
		return blSaveRequest;
	}
	
	/**
	 * Sets the bl save request.
	 *
	 * @param blSaveRequest the new bl save request
	 */
	public void setBlSaveRequest(boolean blSaveRequest) {
		this.blSaveRequest = blSaveRequest;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the motive unblock.
	 *
	 * @return the motive unblock
	 */
	public String getMotiveUnblock() {
		return motiveUnblock;
	}
	
	/**
	 * Sets the motive unblock.
	 *
	 * @param motiveUnblock the new motive unblock
	 */
	public void setMotiveUnblock(String motiveUnblock) {
		this.motiveUnblock = motiveUnblock;
	}
	
	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return registryUser;
	}
	
	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}
	
	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	
	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	
	/**
	 * Gets the confirmation user.
	 *
	 * @return the confirmation user
	 */
	public String getConfirmationUser() {
		return confirmationUser;
	}
	
	/**
	 * Sets the confirmation user.
	 *
	 * @param confirmationUser the new confirmation user
	 */
	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}
	
	/**
	 * Gets the confirmation date.
	 *
	 * @return the confirmation date
	 */
	public Date getConfirmationDate() {
		return confirmationDate;
	}
	
	/**
	 * Sets the confirmation date.
	 *
	 * @param confirmationDate the new confirmation date
	 */
	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}
	
	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return rejectUser;
	}
	
	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}
	
	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return rejectDate;
	}
	
	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}
	
	/**
	 * Gets the document file1.
	 *
	 * @return the document file1
	 */
	public byte[] getDocumentFile1() {
		return documentFile1;
	}
	
	/**
	 * Sets the document file1.
	 *
	 * @param documentFile1 the new document file1
	 */
	public void setDocumentFile1(byte[] documentFile1) {
		this.documentFile1 = documentFile1;
	}
	
	/**
	 * Gets the document file2.
	 *
	 * @return the document file2
	 */
	public byte[] getDocumentFile2() {
		return documentFile2;
	}
	
	/**
	 * Sets the document file2.
	 *
	 * @param documentFile2 the new document file2
	 */
	public void setDocumentFile2(byte[] documentFile2) {
		this.documentFile2 = documentFile2;
	}
	
	/**
	 * Gets the file name1.
	 *
	 * @return the file name1
	 */
	public String getFileName1() {
		return fileName1;
	}
	
	/**
	 * Sets the file name1.
	 *
	 * @param fileName1 the new file name1
	 */
	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}
	
	/**
	 * Gets the file name2.
	 *
	 * @return the file name2
	 */
	public String getFileName2() {
		return fileName2;
	}
	
	/**
	 * Sets the file name2.
	 *
	 * @param fileName2 the new file name2
	 */
	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}
	
	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}
	
	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}
	
	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}
	
	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}
	
	/**
	 * Gets the reject other motive.
	 *
	 * @return the reject other motive
	 */
	public String getRejectOtherMotive() {
		return rejectOtherMotive;
	}
	
	/**
	 * Sets the reject other motive.
	 *
	 * @param rejectOtherMotive the new reject other motive
	 */
	public void setRejectOtherMotive(String rejectOtherMotive) {
		this.rejectOtherMotive = rejectOtherMotive;
	}
	
	/**
	 * Gets the lst motive reject.
	 *
	 * @return the lst motive reject
	 */
	public List<ParameterTable> getLstMotiveReject() {
		return lstMotiveReject;
	}
	
	/**
	 * Sets the lst motive reject.
	 *
	 * @param lstMotiveReject the new lst motive reject
	 */
	public void setLstMotiveReject(List<ParameterTable> lstMotiveReject) {
		this.lstMotiveReject = lstMotiveReject;
	}
	
	/**
	 * Gets the obj unblock request.
	 *
	 * @return the obj unblock request
	 */
	public UnblockRequest getObjUnblockRequest() {
		return objUnblockRequest;
	}
	
	/**
	 * Sets the obj unblock request.
	 *
	 * @param objUnblockRequest the new obj unblock request
	 */
	public void setObjUnblockRequest(UnblockRequest objUnblockRequest) {
		this.objUnblockRequest = objUnblockRequest;
	}
	
	/**
	 * Gets the id state.
	 *
	 * @return the id state
	 */
	public Integer getIdState() {
		return idState;
	}
	
	/**
	 * Sets the id state.
	 *
	 * @param idState the new id state
	 */
	public void setIdState(Integer idState) {
		this.idState = idState;
	}
	
	/**
	 * Gets the reject motive desc.
	 *
	 * @return the reject motive desc
	 */
	public String getRejectMotiveDesc() {
		return rejectMotiveDesc;
	}
	
	/**
	 * Sets the reject motive desc.
	 *
	 * @param rejectMotiveDesc the new reject motive desc
	 */
	public void setRejectMotiveDesc(String rejectMotiveDesc) {
		this.rejectMotiveDesc = rejectMotiveDesc;
	}
	
	/**
	 * Gets the approve user.
	 *
	 * @return the approve user
	 */
	public String getApproveUser() {
		return approveUser;
	}
	
	/**
	 * Sets the approve user.
	 *
	 * @param approveUser the new approve user
	 */
	public void setApproveUser(String approveUser) {
		this.approveUser = approveUser;
	}
	
	/**
	 * Gets the approve date.
	 *
	 * @return the approve date
	 */
	public Date getApproveDate() {
		return approveDate;
	}
	
	/**
	 * Sets the approve date.
	 *
	 * @param approveDate the new approve date
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}
	
	/**
	 * Gets the annul user.
	 *
	 * @return the annul user
	 */
	public String getAnnulUser() {
		return annulUser;
	}
	
	/**
	 * Sets the annul user.
	 *
	 * @param annulUser the new annul user
	 */
	public void setAnnulUser(String annulUser) {
		this.annulUser = annulUser;
	}
	
	/**
	 * Gets the annul date.
	 *
	 * @return the annul date
	 */
	public Date getAnnulDate() {
		return annulDate;
	}
	
	/**
	 * Sets the annul date.
	 *
	 * @param annulDate the new annul date
	 */
	public void setAnnulDate(Date annulDate) {
		this.annulDate = annulDate;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the ind validate block document.
	 *
	 * @return the ind validate block document
	 */
	public Integer getIndValidateBlockDocument() {
		return indValidateBlockDocument;
	}
	
	/**
	 * Sets the ind validate block document.
	 *
	 * @param indValidateBlockDocument the new ind validate block document
	 */
	public void setIndValidateBlockDocument(Integer indValidateBlockDocument) {
		this.indValidateBlockDocument = indValidateBlockDocument;
	}
}