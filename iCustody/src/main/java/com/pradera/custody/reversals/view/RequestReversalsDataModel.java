package com.pradera.custody.reversals.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.custody.reversals.to.RequestReversalsTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RequestReversalsDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class RequestReversalsDataModel extends ListDataModel<RequestReversalsTO> implements SelectableDataModel<RequestReversalsTO>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new request reversals data model.
	 *
	 * @param lstRequestReversals the lst request reversals
	 */
	public RequestReversalsDataModel(List<RequestReversalsTO> lstRequestReversals) {
		super(lstRequestReversals);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public RequestReversalsTO getRowData(String rowKey) {
		List<RequestReversalsTO> lstRequestReversals = (List<RequestReversalsTO>)getWrappedData();
		for(RequestReversalsTO requestReversal : lstRequestReversals){
			if(requestReversal.getIdUnblockRequestPk().toString().equals(rowKey))
				return requestReversal;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	public Object getRowKey(RequestReversalsTO requestReversal) {
		return requestReversal.getIdUnblockRequestPk();
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	@SuppressWarnings("unchecked")
	public Integer getSize(){
		List<RequestReversalsTO> lstRequestReversals = (List<RequestReversalsTO>)getWrappedData();
		return lstRequestReversals.size();
	}
	
}
