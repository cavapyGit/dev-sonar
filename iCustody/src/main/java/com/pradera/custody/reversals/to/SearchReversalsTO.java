package com.pradera.custody.reversals.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class SearchReversalsTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class SearchReversalsTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The holder. */
	private Holder holder;	
	
	/** The block entity. */
	private BlockEntity blockEntity;
	
	/** The number record. */
	private String numberRecord;
	
	/** The number request. */
	private Long numberRequest;
	
	/** The action. */
	private String action;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The parameter table affectation type. */
	private ParameterTable parameterTableAffectationType;	
	
	/** The lst affectation type. */
	private List<ParameterTable> lstAffectationType;
	
	/** The parameter table affectation level. */
	private ParameterTable parameterTableAffectationLevel;	
	
	/** The lst affectation level. */
	private List<ParameterTable> lstAffectationLevel;
	
	/** The parameter table request reversal state. */
	private ParameterTable parameterTableRequestReversalState;
	
	/** The lst request reversal state. */
	private List<ParameterTable> lstRequestReversalState;
	
	/** The lst motive reject. */
	private List<ParameterTable> lstMotiveReject;
	
	/** The parameter table motive reversal. */
	private ParameterTable parameterTableMotiveReversal;
	
	/** The lst motive reversal. */
	private List<ParameterTable> lstMotiveReversal;
	
	/** The lst block ope for reversals. */
	private List<BlockOpeForReversalsTO> lstBlockOpeForReversals;
	
	/** The block ope for rever to. */
	private BlockOpeForReversalsTO blockOpeForReverTO;
	
	/** The bl reversals. */
	private boolean blReversals;
	
	/** The bl search request. */
	private boolean blSearchRequest;
	
	/** The search type. */
	private Integer searchType;
	
	/** The bl default search. */
	private boolean blDefaultSearch;
	
	/** The security. */
	private Security security;
	
	/** The id issuer. */
	private String idIssuer;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The lst status. */
	private List<Integer> lstStatus;
	
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
	
	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public BlockEntity getBlockEntity() {
		return blockEntity;
	}
	
	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}
	
	/**
	 * Gets the number record.
	 *
	 * @return the number record
	 */
	public String getNumberRecord() {
		return numberRecord;
	}
	
	/**
	 * Sets the number record.
	 *
	 * @param numberRecord the new number record
	 */
	public void setNumberRecord(String numberRecord) {
		this.numberRecord = numberRecord;
	}
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the parameter table affectation type.
	 *
	 * @return the parameter table affectation type
	 */
	public ParameterTable getParameterTableAffectationType() {
		return parameterTableAffectationType;
	}
	
	/**
	 * Sets the parameter table affectation type.
	 *
	 * @param parameterTableAffectationType the new parameter table affectation type
	 */
	public void setParameterTableAffectationType(
			ParameterTable parameterTableAffectationType) {
		this.parameterTableAffectationType = parameterTableAffectationType;
	}
	
	/**
	 * Gets the lst affectation type.
	 *
	 * @return the lst affectation type
	 */
	public List<ParameterTable> getLstAffectationType() {
		return lstAffectationType;
	}
	
	/**
	 * Sets the lst affectation type.
	 *
	 * @param lstAffectationType the new lst affectation type
	 */
	public void setLstAffectationType(List<ParameterTable> lstAffectationType) {
		this.lstAffectationType = lstAffectationType;
	}
	
	/**
	 * Gets the parameter table affectation level.
	 *
	 * @return the parameter table affectation level
	 */
	public ParameterTable getParameterTableAffectationLevel() {
		return parameterTableAffectationLevel;
	}
	
	/**
	 * Sets the parameter table affectation level.
	 *
	 * @param parameterTableAffectationLevel the new parameter table affectation level
	 */
	public void setParameterTableAffectationLevel(
			ParameterTable parameterTableAffectationLevel) {
		this.parameterTableAffectationLevel = parameterTableAffectationLevel;
	}
	
	/**
	 * Gets the lst affectation level.
	 *
	 * @return the lst affectation level
	 */
	public List<ParameterTable> getLstAffectationLevel() {
		return lstAffectationLevel;
	}
	
	/**
	 * Sets the lst affectation level.
	 *
	 * @param lstAffectationLevel the new lst affectation level
	 */
	public void setLstAffectationLevel(List<ParameterTable> lstAffectationLevel) {
		this.lstAffectationLevel = lstAffectationLevel;
	}
	
	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	
	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(String action) {
		this.action = action;
	}
	
	/**
	 * Gets the lst block ope for reversals.
	 *
	 * @return the lst block ope for reversals
	 */
	public List<BlockOpeForReversalsTO> getLstBlockOpeForReversals() {
		return lstBlockOpeForReversals;
	}
	
	/**
	 * Sets the lst block ope for reversals.
	 *
	 * @param lstBlockOpeForReversals the new lst block ope for reversals
	 */
	public void setLstBlockOpeForReversals(
			List<BlockOpeForReversalsTO> lstBlockOpeForReversals) {
		this.lstBlockOpeForReversals = lstBlockOpeForReversals;
	}	
	
	/**
	 * Checks if is bl search request.
	 *
	 * @return true, if is bl search request
	 */
	public boolean isBlSearchRequest() {
		return blSearchRequest;
	}
	
	/**
	 * Sets the bl search request.
	 *
	 * @param blSearchRequest the new bl search request
	 */
	public void setBlSearchRequest(boolean blSearchRequest) {
		this.blSearchRequest = blSearchRequest;
	}
	
	/**
	 * Checks if is bl reversals.
	 *
	 * @return true, if is bl reversals
	 */
	public boolean isBlReversals() {
		return blReversals;
	}
	
	/**
	 * Sets the bl reversals.
	 *
	 * @param blReversals the new bl reversals
	 */
	public void setBlReversals(boolean blReversals) {
		this.blReversals = blReversals;
	}	
	
	/**
	 * Gets the parameter table request reversal state.
	 *
	 * @return the parameter table request reversal state
	 */
	public ParameterTable getParameterTableRequestReversalState() {
		return parameterTableRequestReversalState;
	}
	
	/**
	 * Sets the parameter table request reversal state.
	 *
	 * @param parameterTableRequestReversalState the new parameter table request reversal state
	 */
	public void setParameterTableRequestReversalState(
			ParameterTable parameterTableRequestReversalState) {
		this.parameterTableRequestReversalState = parameterTableRequestReversalState;
	}
	
	/**
	 * Gets the lst request reversal state.
	 *
	 * @return the lst request reversal state
	 */
	public List<ParameterTable> getLstRequestReversalState() {
		return lstRequestReversalState;
	}
	
	/**
	 * Sets the lst request reversal state.
	 *
	 * @param lstRequestReversalState the new lst request reversal state
	 */
	public void setLstRequestReversalState(
			List<ParameterTable> lstRequestReversalState) {
		this.lstRequestReversalState = lstRequestReversalState;
	}
	
	/**
	 * Gets the parameter table motive reversal.
	 *
	 * @return the parameter table motive reversal
	 */
	public ParameterTable getParameterTableMotiveReversal() {
		return parameterTableMotiveReversal;
	}
	
	/**
	 * Sets the parameter table motive reversal.
	 *
	 * @param parameterTableMotiveReversal the new parameter table motive reversal
	 */
	public void setParameterTableMotiveReversal(
			ParameterTable parameterTableMotiveReversal) {
		this.parameterTableMotiveReversal = parameterTableMotiveReversal;
	}
	
	/**
	 * Gets the lst motive reversal.
	 *
	 * @return the lst motive reversal
	 */
	public List<ParameterTable> getLstMotiveReversal() {
		return lstMotiveReversal;
	}
	
	/**
	 * Sets the lst motive reversal.
	 *
	 * @param lstMotiveReversal the new lst motive reversal
	 */
	public void setLstMotiveReversal(List<ParameterTable> lstMotiveReversal) {
		this.lstMotiveReversal = lstMotiveReversal;
	}
	
	/**
	 * Gets the block ope for rever to.
	 *
	 * @return the block ope for rever to
	 */
	public BlockOpeForReversalsTO getBlockOpeForReverTO() {
		return blockOpeForReverTO;
	}
	
	/**
	 * Sets the block ope for rever to.
	 *
	 * @param blockOpeForReverTO the new block ope for rever to
	 */
	public void setBlockOpeForReverTO(BlockOpeForReversalsTO blockOpeForReverTO) {
		this.blockOpeForReverTO = blockOpeForReverTO;
	}
	
	/**
	 * Gets the number request.
	 *
	 * @return the number request
	 */
	public Long getNumberRequest() {
		return numberRequest;
	}
	
	/**
	 * Sets the number request.
	 *
	 * @param numberRequest the new number request
	 */
	public void setNumberRequest(Long numberRequest) {
		this.numberRequest = numberRequest;
	}
	
	/**
	 * Gets the search type.
	 *
	 * @return the search type
	 */
	public Integer getSearchType() {
		return searchType;
	}
	
	/**
	 * Sets the search type.
	 *
	 * @param searchType the new search type
	 */
	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}
	
	/**
	 * Checks if is bl default search.
	 *
	 * @return true, if is bl default search
	 */
	public boolean isBlDefaultSearch() {
		return blDefaultSearch;
	}
	
	/**
	 * Sets the bl default search.
	 *
	 * @param blDefaultSearch the new bl default search
	 */
	public void setBlDefaultSearch(boolean blDefaultSearch) {
		this.blDefaultSearch = blDefaultSearch;
	}
	
	/**
	 * Gets the lst motive reject.
	 *
	 * @return the lst motive reject
	 */
	public List<ParameterTable> getLstMotiveReject() {
		return lstMotiveReject;
	}
	
	/**
	 * Sets the lst motive reject.
	 *
	 * @param lstMotiveReject the new lst motive reject
	 */
	public void setLstMotiveReject(List<ParameterTable> lstMotiveReject) {
		this.lstMotiveReject = lstMotiveReject;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	
	/**
	 * Gets the id issuer.
	 *
	 * @return the id issuer
	 */
	public String getIdIssuer() {
		return idIssuer;
	}
	
	/**
	 * Sets the id issuer.
	 *
	 * @param idIssuer the new id issuer
	 */
	public void setIdIssuer(String idIssuer) {
		this.idIssuer = idIssuer;
	}
	
	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}
	
	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}
	
	/**
	 * Gets the lst status.
	 *
	 * @return the lstStatus
	 */
	public List<Integer> getLstStatus() {
		return lstStatus;
	}
	
	/**
	 * Sets the lst status.
	 *
	 * @param lstStatus the lstStatus to set
	 */
	public void setLstStatus(List<Integer> lstStatus) {
		this.lstStatus = lstStatus;
	}
	
	
}