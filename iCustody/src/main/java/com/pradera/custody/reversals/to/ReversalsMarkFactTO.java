package com.pradera.custody.reversals.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReversalsMarkFactTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class ReversalsMarkFactTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id holder market pk. */
	private Long idHolderMarketPk;
	
	/** The block amount. */
	private BigDecimal blockAmount;
	
	/** The market price. */
	private BigDecimal marketPrice;
	
	/** The market rate. */
	private BigDecimal marketRate;
	
	/** The market date. */
	private Date marketDate;

	/**
	 * Gets the id holder market pk.
	 *
	 * @return the id holder market pk
	 */
	public Long getIdHolderMarketPk() {
		return idHolderMarketPk;
	}

	/**
	 * Sets the id holder market pk.
	 *
	 * @param idHolderMarketPk the new id holder market pk
	 */
	public void setIdHolderMarketPk(Long idHolderMarketPk) {
		this.idHolderMarketPk = idHolderMarketPk;
	}

	/**
	 * Gets the block amount.
	 *
	 * @return the block amount
	 */
	public BigDecimal getBlockAmount() {
		return blockAmount;
	}

	/**
	 * Sets the block amount.
	 *
	 * @param blockAmount the new block amount
	 */
	public void setBlockAmount(BigDecimal blockAmount) {
		this.blockAmount = blockAmount;
	}

	/**
	 * Gets the market price.
	 *
	 * @return the market price
	 */
	public BigDecimal getMarketPrice() {
		return marketPrice;
	}

	/**
	 * Sets the market price.
	 *
	 * @param marketPrice the new market price
	 */
	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}

	/**
	 * Gets the market rate.
	 *
	 * @return the market rate
	 */
	public BigDecimal getMarketRate() {
		return marketRate;
	}

	/**
	 * Sets the market rate.
	 *
	 * @param marketRate the new market rate
	 */
	public void setMarketRate(BigDecimal marketRate) {
		this.marketRate = marketRate;
	}

	/**
	 * Gets the market date.
	 *
	 * @return the market date
	 */
	public Date getMarketDate() {
		return marketDate;
	}

	/**
	 * Sets the market date.
	 *
	 * @param marketDate the new market date
	 */
	public void setMarketDate(Date marketDate) {
		this.marketDate = marketDate;
	}
}