package com.pradera.custody.reversals.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.SearchReversalsTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.ReversalMotiveRejectType;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.type.InstrumentType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RequestReversalsServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class RequestReversalsServiceFacade {

	/** The request reversals service bean. */
	@EJB
	private RequestReversalsServiceBean requestReversalsServiceBean;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;	
	
	/** The parameter service bean. */
	@EJB
	ParameterServiceBean parameterServiceBean;
		
	/** The transaction registry. */
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	
	/** The billing conexion. */
	@Inject 
	Instance<BillingConexionService> billingConexion;
	
	//@ProcessAuditLogger(process=BusinessProcessType.SECURITY_BLOCK_QUERY)
	/**
	 * Find block operation.
	 *
	 * @param searchReversalsTO the search reversals to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOpeForReversalsTO> findBlockOperation(SearchReversalsTO searchReversalsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<BlockOpeForReversalsTO> lstBlockOpeForReversals = new ArrayList<BlockOpeForReversalsTO>();
		List<Object[]> lstResult = requestReversalsServiceBean.findBlockOperation(searchReversalsTO);
		for (Object[] objResult:lstResult) {
			BlockOpeForReversalsTO objBlockOpeForReverTO = new BlockOpeForReversalsTO();
			objBlockOpeForReverTO.setIdBlockOperationPk((Long)objResult[0]);
			objBlockOpeForReverTO.setBlockType(objResult[1].toString());
			objBlockOpeForReverTO.setRegistryDate((Date) objResult[2]);
			if(Validations.validateIsNotNullAndNotEmpty(objResult[3]))
				objBlockOpeForReverTO.setNumberRecord(objResult[3].toString());
			objBlockOpeForReverTO.setAccountNumber(objResult[4].toString());
			objBlockOpeForReverTO.setIdParticipantPk((Long) objResult[5]);
			objBlockOpeForReverTO.setParticipantDescription(objResult[6].toString());
			objBlockOpeForReverTO.setIdSecurityCodePk(objResult[7].toString());
			if(Validations.validateIsNotNullAndNotEmpty(objResult[8])){
				objBlockOpeForReverTO.setIsinDescription(objResult[8].toString());
			} else {
				objBlockOpeForReverTO.setIsinDescription(null);
			}			
			objBlockOpeForReverTO.setCurrentAmount(new BigDecimal(objResult[9].toString()));
			objBlockOpeForReverTO.setBlockLevel(objResult[10].toString());
			objBlockOpeForReverTO.setState(objResult[11].toString());
			objBlockOpeForReverTO.setAmount(new BigDecimal(objResult[12].toString()));
			objBlockOpeForReverTO.setIdBlockEntityPk(new Long(objResult[13].toString()));
			if(Validations.validateIsNotNullAndNotEmpty(objResult[14]))
				objBlockOpeForReverTO.setBlockEntityName(objResult[21].toString() + GeneralConstants.BLANK_SPACE
							+ (Validations.validateIsNotNullAndNotEmpty(objResult[22])?objResult[22].toString():GeneralConstants.EMPTY_STRING) 
							+ GeneralConstants.BLANK_SPACE + objResult[14].toString());
			else
				objBlockOpeForReverTO.setBlockEntityName(objResult[20].toString());			
			if(Validations.validateIsNotNullAndNotEmpty(objResult[15]))
				objBlockOpeForReverTO.setFormAffectation(objResult[15].toString());
			objBlockOpeForReverTO.setAlternateCode(objResult[16].toString());
			objBlockOpeForReverTO.setIdHolderPk(new Long(objResult[17].toString()));
			objBlockOpeForReverTO.setParticipantMnemonic(objResult[18].toString());
			objBlockOpeForReverTO.setRegistryUser(objResult[19].toString());
			objBlockOpeForReverTO.setConfirmUser(objResult[30].toString());
			objBlockOpeForReverTO.setIdBlockType((Integer) objResult[23]);
			objBlockOpeForReverTO.setIdHolderAccountPk((Long) objResult[24]);
			objBlockOpeForReverTO.setBlockDate((Date)objResult[25]);
			objBlockOpeForReverTO.setHolderDescription(objResult[26].toString());
			objBlockOpeForReverTO.setOperationNumber(new Long(objResult[27].toString()));
			objBlockOpeForReverTO.setMaxSize(objBlockOpeForReverTO.getCurrentAmount().toString().length());
			if(Validations.validateIsNotNull(objResult[28])){
				if(objResult[28].toString().equals(GeneralConstants.ONE_VALUE_STRING))
					objBlockOpeForReverTO.setCheckAutoSettled(true);
				else
					objBlockOpeForReverTO.setCheckAutoSettled(false);
			}
			if(Validations.validateIsNotNull(objResult[29])){
				if(objResult[29].toString().equals(GeneralConstants.ONE_VALUE_STRING))
					objBlockOpeForReverTO.setCheckAuthorityRestricted(true);
				else
					objBlockOpeForReverTO.setCheckAuthorityRestricted(false);
			}
			if(objBlockOpeForReverTO.getIdBlockType().equals(AffectationType.PAWN.getCode())){
				objBlockOpeForReverTO.setBlPawn(true);
				objBlockOpeForReverTO.setBlBan(false);
			}
			else if(objBlockOpeForReverTO.getIdBlockType().equals(AffectationType.BAN.getCode())){
				objBlockOpeForReverTO.setBlPawn(false);
				objBlockOpeForReverTO.setBlBan(true);
			}else{
				objBlockOpeForReverTO.setBlPawn(false);
				objBlockOpeForReverTO.setBlBan(false);
			}
			
			//Obtener los titulares de la cuenta
			HolderAccount objHolderAccount = requestReversalsServiceBean.find(HolderAccount.class, objBlockOpeForReverTO.getIdHolderAccountPk());
			objBlockOpeForReverTO.setHoldersName(objHolderAccount.getAccountRntAndDescription());
			
			lstBlockOpeForReversals.add(objBlockOpeForReverTO);				
		}
		return lstBlockOpeForReversals;
	}

	//@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_REGISTER)
	/**
	 * Save reversal request.
	 *
	 * @param registerReversalTO the register reversal to
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_CONFIRM)
	public Long saveReversalRequest(RegisterReversalTO registerReversalTO) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		    
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		registerReversalTO.setRegistryUser(loggerUser.getUserName());
		
		return requestReversalsServiceBean.saveReversalRequest(registerReversalTO);
	}

	//@ProcessAuditLogger(process=BusinessProcessType.BLOCK_EXECUTION_QUERY)
	/**
	 * Find reversal requests.
	 *
	 * @param searchReversalsTO the search reversals to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RequestReversalsTO> findReversalRequests(SearchReversalsTO searchReversalsTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List<Object[]> lstResult = requestReversalsServiceBean.findReversalRequests(searchReversalsTO);
		List<RequestReversalsTO> lstRequestReversals = new ArrayList<RequestReversalsTO>();
		for(Object[] objResult:lstResult){
			RequestReversalsTO requestReversalTO = new RequestReversalsTO();
			requestReversalTO.setIdUnblockRequestPk((Long)objResult[0]);
			requestReversalTO.setRegistryDate((Date) objResult[1]);
			requestReversalTO.setAmountUnblock(objResult[2].toString());
			requestReversalTO.setMotive(objResult[3].toString());
			requestReversalTO.setState(objResult[4].toString());
			requestReversalTO.setIdHolderPk(new Long(objResult[5].toString()));
			requestReversalTO.setHolderDescription(objResult[8].toString());
			requestReversalTO.setIdState(new Integer(objResult[7].toString()));
			requestReversalTO.setNumberRecord((objResult[11] != null? objResult[11].toString(): null));
			requestReversalTO.setRegisterUser(objResult[12].toString());
			//requestReversalTO.setIdParticipant((Long)objResult[13]);
			lstRequestReversals.add(requestReversalTO);
		}
		return lstRequestReversals;
	}
	
	/**
	 * Find request reversal detail.
	 *
	 * @param requeReversalTO the reque reversal to
	 * @return the register reversal to
	 * @throws ServiceException the service exception
	 */
	public RegisterReversalTO findRequestReversalDetail(RequestReversalsTO requeReversalTO) throws ServiceException{
		RegisterReversalTO registerReversalTO = new RegisterReversalTO();	
		List<ParameterTable> lstPTAffecType = null;
		List<ParameterTable> lstPTAffecLevel = null;
		List<ParameterTable> lstPTAffecState = null;
		List<ParameterTable> lstPTMotiveUnblock = null;
		List<ParameterTable> lstPTStateReversalReq = null;
		List<ParameterTable> lstPTAffecForm = null;
		List<ParameterTable> lstRejectMotive = null;
		UnblockRequest objUnblockRequest = requestReversalsServiceBean.getUnblockRequest(requeReversalTO);
		objUnblockRequest.getUnblockOperation().size();
		registerReversalTO.setObjUnblockRequest(objUnblockRequest);
		List<BlockOpeForReversalsTO> lstBlockOperForReversals = new ArrayList<BlockOpeForReversalsTO>();			
		
		//Se carga los parameter tables necesarios para los detalles
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		lstPTAffecType = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		lstPTAffecLevel = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
		lstPTAffecState = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.MOTIVE_REVERSALS.getCode());
		lstPTMotiveUnblock = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_REVERSALS_REQUEST.getCode());
		lstPTStateReversalReq = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
		lstPTAffecForm = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.MOTIVE_REJECT_REVERSALS.getCode());
		lstRejectMotive = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable paramTable:lstPTMotiveUnblock){
			if(paramTable.getParameterTablePk().equals(objUnblockRequest.getMotive()))
				registerReversalTO.setMotiveUnblock(paramTable.getParameterName());
		}
		for(ParameterTable paramTable:lstPTStateReversalReq){
			if(paramTable.getParameterTablePk().equals(objUnblockRequest.getUnblockRequestState()))
				registerReversalTO.setState(paramTable.getParameterName());
		}
		if(Validations.validateListIsNotNullAndNotEmpty(objUnblockRequest.getUnblockRequestFile())){
			if(objUnblockRequest.getUnblockRequestFile().size()>1){
				registerReversalTO.setFileName1(objUnblockRequest.getUnblockRequestFile().get(0).getFileName());
				registerReversalTO.setDocumentFile1(objUnblockRequest.getUnblockRequestFile().get(0).getDocumentFile());
				registerReversalTO.setFileName2(objUnblockRequest.getUnblockRequestFile().get(1).getFileName());
				registerReversalTO.setDocumentFile2(objUnblockRequest.getUnblockRequestFile().get(1).getDocumentFile());
			}else{
				registerReversalTO.setFileName1(objUnblockRequest.getUnblockRequestFile().get(0).getFileName());
				registerReversalTO.setDocumentFile1(objUnblockRequest.getUnblockRequestFile().get(0).getDocumentFile());
			}
		}
		registerReversalTO.setRegistryUser(objUnblockRequest.getRegistryUser());
		registerReversalTO.setRegistryDate(objUnblockRequest.getRegistryDate());
		registerReversalTO.setIdHolderPk(objUnblockRequest.getHolder().getIdHolderPk());
		registerReversalTO.setHolderDescription(objUnblockRequest.getHolder().getDescriptionHolder());
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockRequest.getConfirmationUser()))
			registerReversalTO.setConfirmationUser(objUnblockRequest.getConfirmationUser());
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockRequest.getConfirmationDate()))
			registerReversalTO.setConfirmationDate(objUnblockRequest.getConfirmationDate());
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockRequest.getRejectUser()))
			registerReversalTO.setRejectUser(objUnblockRequest.getRejectUser());
		if(Validations.validateIsNotNullAndNotEmpty(objUnblockRequest.getRejectDate()))
			registerReversalTO.setRejectDate(objUnblockRequest.getRejectDate());
		registerReversalTO.setApproveUser(objUnblockRequest.getApproveUser());
		registerReversalTO.setApproveDate(objUnblockRequest.getApproveDate());
		registerReversalTO.setAnnulUser(objUnblockRequest.getAnnulUser());
		registerReversalTO.setAnnulDate(objUnblockRequest.getAnnulDate());
		registerReversalTO.setDocumentNumber(objUnblockRequest.getDocumentNumber());
		registerReversalTO.setIdUnblockRequestPk(objUnblockRequest.getIdUnblockRequestPk());
		registerReversalTO.setTotalUnblock(objUnblockRequest.getRequestQuantity());
		for(UnblockOperation objUnblockOperation:objUnblockRequest.getUnblockOperation()){
			BlockOpeForReversalsTO objBlockOpeForReverTO = new BlockOpeForReversalsTO();
			objBlockOpeForReverTO.setIdUnblockOperationPk(objUnblockOperation.getIdUnblockOperationPk());
			objBlockOpeForReverTO.setAmountUnblock(objUnblockOperation.getUnblockQuantity());
			objBlockOpeForReverTO.setIdBlockOperationPk(objUnblockOperation.getBlockOperation().getIdBlockOperationPk());
			objBlockOpeForReverTO.setOperationNumber(objUnblockOperation.getBlockOperation().getCustodyOperation().getOperationNumber());
			for(ParameterTable paramTable:lstPTAffecType){
				if(paramTable.getParameterTablePk().equals(objUnblockOperation.getBlockOperation().getBlockRequest().getBlockType()))
					objBlockOpeForReverTO.setBlockType(paramTable.getParameterName());
			}
			objBlockOpeForReverTO.setRegistryDate(objUnblockOperation.getBlockOperation().getBlockRequest().getRegistryDate());
			if(Validations.validateIsNotNullAndNotEmpty(objUnblockOperation.getBlockOperation().getBlockRequest().getBlockNumber()))
				objBlockOpeForReverTO.setNumberRecord(objUnblockOperation.getBlockOperation().getBlockRequest().getBlockNumber());
			objBlockOpeForReverTO.setIdHolderAccountPk(objUnblockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk());
			objBlockOpeForReverTO.setAccountNumber(objUnblockOperation.getBlockOperation().getHolderAccount().getAccountNumber().toString());
			objBlockOpeForReverTO.setIdParticipantPk(objUnblockOperation.getBlockOperation().getParticipant().getIdParticipantPk());
			objBlockOpeForReverTO.setParticipantDescription(objUnblockOperation.getBlockOperation().getParticipant().getDescription());
			objBlockOpeForReverTO.setIdSecurityCodePk(objUnblockOperation.getBlockOperation().getSecurities().getIdSecurityCodePk());
			objBlockOpeForReverTO.setIsinDescription(objUnblockOperation.getBlockOperation().getSecurities().getDescription());
			objBlockOpeForReverTO.setAmount(objUnblockOperation.getBlockOperation().getOriginalBlockBalance());
			objBlockOpeForReverTO.setCurrentAmount(objUnblockOperation.getBlockOperation().getActualBlockBalance());
			objBlockOpeForReverTO.setIdBlockLevel(objUnblockOperation.getBlockOperation().getBlockLevel());
			for(ParameterTable paramTable:lstPTAffecLevel){
				if(paramTable.getParameterTablePk().equals(objUnblockOperation.getBlockOperation().getBlockLevel()))
					objBlockOpeForReverTO.setBlockLevel(paramTable.getParameterName());
			}	
			for(ParameterTable paramTable:lstPTAffecState){
				if(paramTable.getParameterTablePk().equals(objUnblockOperation.getBlockOperation().getBlockState()))
					objBlockOpeForReverTO.setState(paramTable.getParameterName());
			}
			for(ParameterTable paramTable:lstPTAffecForm){
				if(paramTable.getParameterTablePk().equals(objUnblockOperation.getBlockOperation().getBlockRequest().getBlockForm()))
					objBlockOpeForReverTO.setFormAffectation(paramTable.getParameterName());
			}
			objBlockOpeForReverTO.setAlternateCode(objUnblockOperation.getBlockOperation().getHolderAccount().getAlternateCode());
			objBlockOpeForReverTO.setRegistryUser(objUnblockOperation.getBlockOperation().getCustodyOperation().getRegistryUser());
			objBlockOpeForReverTO.setRegistryDate(objUnblockOperation.getBlockOperation().getCustodyOperation().getRegistryDate());
			objBlockOpeForReverTO.setConfirmDate(objUnblockOperation.getBlockOperation().getCustodyOperation().getConfirmDate());
			objBlockOpeForReverTO.setIdBlockEntityPk(objUnblockOperation.getBlockOperation().getBlockRequest().getBlockEntity().getIdBlockEntityPk());
			objBlockOpeForReverTO.setBlockEntityName(objUnblockOperation.getBlockOperation().getBlockRequest().getBlockEntity().getName());
			objBlockOpeForReverTO.setBlockDate(objUnblockOperation.getBlockOperation().getCustodyOperation().getOperationDate());
			if (Validations.validateIsNotNull(objUnblockOperation.getCustodyOperation().getRejectMotiveOther())) {
				registerReversalTO.setRejectMotiveDesc(objUnblockOperation.getCustodyOperation().getRejectMotiveOther());
			} else if (Validations.validateIsNotNull(objUnblockOperation.getCustodyOperation().getRejectMotive())) {
				for (ParameterTable parameterTable:lstRejectMotive) {
					if (parameterTable.getParameterTablePk().equals(objUnblockOperation.getCustodyOperation().getRejectMotive())) {
						registerReversalTO.setRejectMotiveDesc(parameterTable.getDescription());
						break;
					}
				}
			}
			
			//Obtener los titulares de la cuenta
			HolderAccount objHolderAccount = requestReversalsServiceBean.find(HolderAccount.class, objBlockOpeForReverTO.getIdHolderAccountPk());
			objBlockOpeForReverTO.setHoldersName(objHolderAccount.getAccountRntAndDescription());
			
			lstBlockOperForReversals.add(objBlockOpeForReverTO);				
		}
		registerReversalTO.setLstBlockOpeForReversals(lstBlockOperForReversals);
		return registerReversalTO;
	}	
	
	/**
	 * Find block operation detail.
	 *
	 * @param blockOpeForReveTO the block ope for reve to
	 * @return the block ope for reversals to
	 * @throws ServiceException the service exception
	 */
	public BlockOpeForReversalsTO findBlockOperationDetail(BlockOpeForReversalsTO blockOpeForReveTO) throws ServiceException{
		BlockOpeForReversalsTO blockOpeForReverTO = new BlockOpeForReversalsTO();
		List<ParameterTable> lstPTAffecType = null;
		List<ParameterTable> lstPTAffecForm = null;
		List<ParameterTable> lstPTAffecLevel = null;
		List<ParameterTable> lstPTAffecState = null;
		List<ParameterTable> lstBenefits = null;
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		lstPTAffecType = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		lstPTAffecLevel = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
		parameterTableTO.setMasterTableFk(MasterTableType.STATE_AFFECTATION.getCode());
		lstPTAffecState = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);			
		BlockOperation blockOpe = crudDaoServiceBean.find(BlockOperation.class, new Long(blockOpeForReveTO.getIdBlockOperationPk()));
		blockOpeForReverTO.setIdBlockOperationPk((Long)blockOpe.getIdBlockOperationPk());
		blockOpeForReverTO.setOperationNumber(blockOpe.getCustodyOperation().getOperationNumber());
		for(ParameterTable paramTable:lstPTAffecType){
			if(paramTable.getParameterTablePk().equals(blockOpe.getBlockRequest().getBlockType()))
				blockOpeForReverTO.setBlockType(paramTable.getParameterName());
		}
		blockOpeForReverTO.setRegistryDate(blockOpe.getBlockRequest().getRegistryDate());
		if(Validations.validateIsNotNullAndNotEmpty(blockOpe.getBlockRequest().getBlockNumber()))
			blockOpeForReverTO.setNumberRecord(blockOpe.getBlockRequest().getBlockNumber());
		blockOpeForReverTO.setAccountNumber(blockOpe.getHolderAccount().getAccountNumber().toString());
		blockOpeForReverTO.setAlternateCode(blockOpe.getHolderAccount().getAlternateCode());
		blockOpeForReverTO.setIdParticipantPk(blockOpe.getParticipant().getIdParticipantPk());
		blockOpeForReverTO.setParticipantDescription(blockOpe.getParticipant().getDescription());
		blockOpeForReverTO.setIdSecurityCodePk(blockOpe.getSecurities().getIdSecurityCodePk());
		blockOpeForReverTO.setIsinDescription(blockOpe.getSecurities().getDescription());
		blockOpeForReverTO.setAmount(blockOpe.getOriginalBlockBalance());
		blockOpeForReverTO.setCurrentAmount(blockOpe.getActualBlockBalance());
		for(ParameterTable paramTable:lstPTAffecLevel){
			if(paramTable.getParameterTablePk().equals(blockOpe.getBlockLevel()))
				blockOpeForReverTO.setBlockLevel(paramTable.getParameterName());
		}	
		for(ParameterTable paramTable:lstPTAffecState){
			if(paramTable.getParameterTablePk().equals(blockOpe.getBlockState()))
				blockOpeForReverTO.setState(paramTable.getParameterName());
		}	
		for(ParameterTable paramTable:lstPTAffecType){
			if(paramTable.getParameterTablePk().equals(blockOpe.getBlockRequest().getBlockType()))
				blockOpeForReverTO.setBlockType(paramTable.getParameterName());
		}
		if(Validations.validateIsNotNullAndNotEmpty(blockOpe.getBlockRequest().getBlockForm())){
			parameterTableTO.setMasterTableFk(MasterTableType.AFFECTATION_FORM.getCode());
			lstPTAffecForm = parameterServiceBean.getListParameterTableServiceBean(parameterTableTO);
			for(ParameterTable paramTable:lstPTAffecForm){
				if(paramTable.getParameterTablePk().equals(blockOpe.getBlockRequest().getBlockForm()))
					blockOpeForReverTO.setFormAffectation(paramTable.getParameterName());
			}
		}
		blockOpeForReverTO.setIdHolderPk(blockOpe.getBlockRequest().getHolder().getIdHolderPk());
		blockOpeForReverTO.setRegistryUser(blockOpe.getCustodyOperation().getRegistryUser());
		blockOpeForReverTO.setHolderDescription(blockOpe.getBlockRequest().getHolder().getDescriptionHolder());
		//RENTA FIJA
		if(InstrumentType.FIXED_INCOME.getCode().equals(blockOpe.getSecurities().getInstrumentType())) {
			lstBenefits = new ArrayList<ParameterTable>();
			if(BooleanType.YES.getCode().equals(blockOpe.getIndInterest()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.INTEREST_PAYMENT.getCode(), ImportanceEventType.INTEREST_PAYMENT.getValue()));
			if(BooleanType.YES.getCode().equals(blockOpe.getIndAmortization()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.CAPITAL_AMORTIZATION.getCode(), ImportanceEventType.CAPITAL_AMORTIZATION.getValue()));		
			if(BooleanType.YES.getCode().equals(blockOpe.getIndRescue()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.EARLY_PAYMENT.getCode(), ImportanceEventType.EARLY_PAYMENT.getValue()));
		}else{//RENTA VARIABLE
			lstBenefits = new ArrayList<ParameterTable>();
			if(BooleanType.YES.getCode().equals(blockOpe.getIndCashDividend()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.CASH_DIVIDENDS.getCode(), ImportanceEventType.CASH_DIVIDENDS.getValue()));
			if(BooleanType.YES.getCode().equals(blockOpe.getIndStockDividend()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.ACTION_DIVIDENDS.getCode(), ImportanceEventType.ACTION_DIVIDENDS.getValue()));
			if(BooleanType.YES.getCode().equals(blockOpe.getIndSuscription()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.PREFERRED_SUSCRIPTION.getCode(), ImportanceEventType.PREFERRED_SUSCRIPTION.getValue()));
			if(BooleanType.YES.getCode().equals(blockOpe.getIndReturnContributions()))
				lstBenefits.add(createPTBenefit(ImportanceEventType.RETURN_OF_CONTRIBUTION.getCode(), ImportanceEventType.RETURN_OF_CONTRIBUTION.getValue()));
		}
		blockOpeForReverTO.setLstBenefits(lstBenefits);
		blockOpeForReverTO.setConfirmDate(blockOpe.getCustodyOperation().getConfirmDate());
		blockOpeForReverTO.setIdBlockEntityPk(blockOpe.getBlockRequest().getBlockEntity().getIdBlockEntityPk());
		blockOpeForReverTO.setBlockEntityName(blockOpe.getBlockRequest().getBlockEntity().getName());
		return blockOpeForReverTO;
	}
	
	
	/**
	 * Approve reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_APPROVE)
	public boolean approveReversalRequest(RequestReversalsTO requestReversal) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		    
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		requestReversal.setApproveUser(loggerUser.getUserName());
		requestReversal.setBusinessProcessTypeId(BusinessProcessType.SECURITY_UNBLOCK_APPROVE.getCode());
		return requestReversalsServiceBean.approveReversalRequest(requestReversal);
	}
	
	/**
	 * Annul reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_ANNUL)
	public boolean annulReversalRequest(RequestReversalsTO requestReversal) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		    
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		requestReversal.setAnnulUser(loggerUser.getUserName());
		requestReversal.setBusinessProcessTypeId(BusinessProcessType.SECURITY_UNBLOCK_ANNUL.getCode());
		return requestReversalsServiceBean.annulReversalRequest(requestReversal);
	}
	
	/**
	 * Confirm reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @param loggerUser the logger user
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_CONFIRM)
	public boolean confirmReversalRequest(RequestReversalsTO requestReversal,LoggerUser loggerUser) throws ServiceException{
		
		if(Validations.validateIsNull(loggerUser)){
			loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		    
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		}	
		
		requestReversal.setConfirmationUser(loggerUser.getUserName());
		requestReversal.setBusinessProcessTypeId(BusinessProcessType.SECURITY_UNBLOCK_CONFIRM.getCode());
		return requestReversalsServiceBean.confirmReversalRequest(requestReversal, loggerUser, Boolean.FALSE);
	}

	//@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_REJECT)
	/**
	 * Reject reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_UNBLOCK_REGISTER)
	public boolean rejectReversalRequest(RequestReversalsTO requestReversal) throws ServiceException{		
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		requestReversal.setConfirmationUser(loggerUser.getUserName());
		
		return requestReversalsServiceBean.rejectReversalRequest(requestReversal);
	}
	
	/**
	 * Creates the pt benefit.
	 *
	 * @param paramTabPk the param tab pk
	 * @param paramName the param name
	 * @return the parameter table
	 */
	private ParameterTable createPTBenefit(Integer paramTabPk, String paramName){
		ParameterTable paramTab = new ParameterTable();
		paramTab.setParameterTablePk(paramTabPk);
		paramTab.setParameterName(paramName);
		return paramTab;
	}
	
	
	/**
	 * Cancela o Anula las solicitudes de desafectaciones
	 * Dependiendo el estado en que se encuentre.
	 *
	 * @param finalDate the final date
	 * @param strIssuer the str issuer
	 * @throws ServiceException the service exception
	 */
	
	public void automaticReversalsCancelations( Date finalDate, String strIssuer) throws ServiceException {
		SearchReversalsTO searchReversalsTO = new SearchReversalsTO();
		List<Integer> statesPhysical = new ArrayList<Integer>();
		statesPhysical.add(RequestReversalsStateType.REGISTERED.getCode());
		statesPhysical.add(RequestReversalsStateType.APPROVED.getCode());
		searchReversalsTO.setLstStatus(statesPhysical);
		searchReversalsTO.setInitialDate(finalDate);		
		List<UnblockRequest> lstBlockOperation = requestReversalsServiceBean.findUnblockRequestReversion(searchReversalsTO);
		if(lstBlockOperation != null && lstBlockOperation.size() > 0){
			RequestReversalsTO requestReversal = null;
			for(UnblockRequest objUnblockRequest : lstBlockOperation){
				requestReversal = new RequestReversalsTO();
				if(RequestReversalsStateType.REGISTERED.getCode().equals(objUnblockRequest.getUnblockRequestState())){
					requestReversal.setIdUnblockRequestPk(objUnblockRequest.getIdUnblockRequestPk());
					requestReversal.setIdIssuer(strIssuer);					
					requestReversal.setMotiveReject(ReversalMotiveRejectType.OTHER.getCode());
					requestReversal.setMotiveRejectOther("REVERSION NOCTURNA");
					annulReversalRequest(requestReversal);
				} else if(RequestReversalsStateType.APPROVED.getCode().equals(objUnblockRequest.getUnblockRequestState())){
					requestReversal.setIdUnblockRequestPk(objUnblockRequest.getIdUnblockRequestPk());
					requestReversal.setIdIssuer(strIssuer);
					requestReversal.setMotiveReject(ReversalMotiveRejectType.OTHER.getCode());
					requestReversal.setMotiveRejectOther("REVERSION NOCTURNA");
					rejectReversalRequest(requestReversal);
				}				
			}			
		}
	}
	
	/**
	 * Identify unblock operations to send.
	 *
	 * @param sendDate the send date
	 * @throws ServiceException the service exception
	 */
	public void identifyUnblockOperationsToSend(Date sendDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		    
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		requestReversalsServiceBean.identifyUnblockOperationsToSend(sendDate, loggerUser);
	}
}
