package com.pradera.custody.reversals.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.reversals.to.SearchReversalsTO;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockenforce.BlockEnforceDetail;
import com.pradera.model.custody.blockoperation.UnblockMarketFactOperation;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.blockoperation.UnblockRequestFile;
import com.pradera.model.custody.reversals.type.MotiveReverlsasType;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.UnblockMarketOperationStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RequestReversalsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class RequestReversalsServiceBean extends CrudDaoServiceBean{

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(RequestReversalsServiceBean.class);
	
    /** The accounts component service. */
    @Inject
    Instance<AccountsComponentService> accountsComponentService;
    
    /** The request affectation service bean. */
    @EJB
    RequestAffectationServiceBean requestAffectationServiceBean;
    
    /** The idepositary setup. */
    @Inject @DepositarySetup
    IdepositarySetup idepositarySetup;

    /** The custody service consumer. */
    @EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	/**
	 * Find block operation.
	 *
	 * @param searchReversalsTO the search reversals to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findBlockOperation(SearchReversalsTO searchReversalsTO) throws ServiceException {		
		String strQuery = "		SELECT BO.idBlockOperationPk," +					//0
							"	(SELECT parameterName FROM ParameterTable where parameterTablePk = BR.blockType)," +//1
							"	BO.custodyOperation.registryDate," +		//2
							"	BR.blockNumber," +							//3
							"	BO.holderAccount.accountNumber," +			//4
							"	BO.participant.idParticipantPk," +			//5
							"	BO.participant.description," +				//6
							"	BO.securities.idSecurityCodePk," +			//7
							"	BO.securities.description," +				//8
							"	BO.actualBlockBalance," +					//9
							"	(SELECT parameterName FROM ParameterTable where parameterTablePk = BO.blockLevel)," +//10
							"	(SELECT parameterName FROM ParameterTable where parameterTablePk = BO.blockState)," +//11
							"	BO.originalBlockBalance," +					//12
							"	BR.blockEntity.idBlockEntityPk," +			//13
							"	BR.blockEntity.name," +						//14
							"	(SELECT parameterName FROM ParameterTable where parameterTablePk = BR.blockForm)," +//15
							"	BO.holderAccount.alternateCode," +			//16
							"	BR.holder.idHolderPk," +					//17
							"	BO.participant.mnemonic," +					//18
							"	BO.custodyOperation.registryUser," +		//19
							"	BR.blockEntity.fullName," +					//20
							"	BR.blockEntity.firstLastName," +			//21
							"	BR.blockEntity.secondLastName," +			//22
							"	BR.blockType," +							//23
							"	BO.holderAccount.idHolderAccountPk," +		//24
							"   BR.blockNumberDate, " +						//25
							"	BR.holder.fullName, " + 					//26
							"	BO.custodyOperation.operationNumber, " +		//27
							"	BR.indAutoSettled," +						//28
							"	BR.indAuthorityRestricted, " +				//29
							"	BO.custodyOperation.confirmUser " +		    //30
							"	FROM BlockRequest BR, BlockOperation BO" +
							"	WHERE BR.holder.idHolderPk = :idHolderPk" +
							"	AND BO.blockState in (:lstBlockOperationState)" +
							"	AND BR.idBlockRequestPk = BO.blockRequest.idBlockRequestPk" +
							"	AND BO.actualBlockBalance > 0" +							
						//	"	AND trunc(BO.custodyOperation.registryDate) >= :initialDate" +
							"	AND trunc(BO.custodyOperation.registryDate) <= :endDate";
		
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getInitialDate())){
			strQuery +="	AND trunc(BO.custodyOperation.registryDate) >= :initialDate" ;
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableAffectationType().getParameterTablePk()))
			strQuery += "	AND BR.blockType = :blockType";
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableAffectationLevel().getParameterTablePk()))
			strQuery += "	AND BO.blockLevel = :blockLevel";
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getNumberRecord()))
			strQuery += "	AND BR.blockNumber = :numberRecord";
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getBlockEntity().getIdBlockEntityPk()))
			strQuery += "	AND BR.blockEntity.idBlockEntityPk = :idEntityBlockPk";
		if(Validations.validateIsNotNull(searchReversalsTO.getSecurity()) &&
				Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getSecurity().getIdSecurityCodePk()))
			strQuery += "	AND BO.securities.idSecurityCodePk = :idSecurityCodePk";
		List<Integer> lstBlockOperationState = new ArrayList<Integer>();
		lstBlockOperationState.add(AffectationStateType.BLOCKED.getCode());		
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdIssuer())) {
			strQuery += "	and BO.securities.issuer.idIssuerPk = :idIssuer ";
		}
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdParticipant())) {
			strQuery += "	and BO.participant.idParticipantPk = :idParticipant ";
		}
		
		strQuery += "	ORDER BY BO.idBlockOperationPk DESC";
		
		Query query = em.createQuery(strQuery);
		query.setParameter("idHolderPk", searchReversalsTO.getHolder().getIdHolderPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getInitialDate())){
			query.setParameter("initialDate", searchReversalsTO.getInitialDate());
		}
		query.setParameter("endDate", searchReversalsTO.getEndDate());
		query.setParameter("lstBlockOperationState", lstBlockOperationState);
		
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableAffectationType().getParameterTablePk()))
			query.setParameter("blockType", searchReversalsTO.getParameterTableAffectationType().getParameterTablePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableAffectationLevel().getParameterTablePk()))
			query.setParameter("blockLevel", searchReversalsTO.getParameterTableAffectationLevel().getParameterTablePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getNumberRecord()))
			query.setParameter("numberRecord", searchReversalsTO.getNumberRecord());		
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getBlockEntity().getIdBlockEntityPk()))
			query.setParameter("idEntityBlockPk", searchReversalsTO.getBlockEntity().getIdBlockEntityPk());
		if(Validations.validateIsNotNull(searchReversalsTO.getSecurity()) &&
				Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", searchReversalsTO.getSecurity().getIdSecurityCodePk());
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdIssuer())) {
			query.setParameter("idIssuer", searchReversalsTO.getIdIssuer());
		}
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdParticipant())) {
			query.setParameter("idParticipant", searchReversalsTO.getIdParticipant());
		}
		
		List<Object[]> lstResult  = query.getResultList();
		return lstResult;
	}

	/**
	 * Find reversal requests.
	 *
	 * @param searchReversalsTO the search reversals to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> findReversalRequests(SearchReversalsTO searchReversalsTO) throws ServiceException{
		String strQuery = 	"	SELECT DISTINCT UR.idUnblockRequestPk," +															//0
							"			UR.registryDate," +																	//1
							"			UR.requestQuantity," +																//2
							"			(SELECT parameterName FROM ParameterTable where parameterTablePk = UR.motive)," +	//3
							"			(SELECT parameterName FROM ParameterTable " +
							"				where parameterTablePk = UR.unblockRequestState)," +							//4
							"			UR.holder.idHolderPk," +															//5
							"			UR.holder.name," +																	//6
							"			UR.unblockRequestState," +															//7
							"			UR.holder.fullName," +																//8
							"			UR.holder.firstLastName," +															//9
							"			UR.holder.secondLastName," +														//10
							"			UR.documentNumber," +																//11
							"			UR.registryUser" +																    //12
		//					"			UR.participant.idParticipantPk" +													//13
							"	FROM UnblockRequest UR " ;
		if (Validations.validateIsNotNull(searchReversalsTO.getIdIssuer())) {
			strQuery += " ,UnblockOperation UBO ";
		}
		strQuery += "	WHERE 1=1 ";
		
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableRequestReversalState().getParameterTablePk()))
			strQuery += "	AND UR.unblockRequestState = :state";
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getHolder().getIdHolderPk()))
			strQuery += "	AND UR.holder.idHolderPk = :idHolderPk";
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableMotiveReversal().getParameterTablePk()))
			strQuery += "	AND UR.motive = :motive";
		if(Validations.validateIsNullOrEmpty(searchReversalsTO.getNumberRequest())){
			if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getInitialDate()))
				strQuery += "	AND trunc(UR.registryDate) >= :initialDate";			
			if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getEndDate()))
				strQuery += "	AND trunc(UR.registryDate) <= :endDate";
		}else{
			strQuery += "	AND UR.idUnblockRequestPk = :idUnblockRequestPk";
		}
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdIssuer())) {
			strQuery += "	AND UR.idUnblockRequestPk = UBO.unblockRequest.idUnblockRequestPk ";
			strQuery += "	AND UBO.blockOperation.securities.issuer.idIssuerPk = :idIssuer ";
		}
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdParticipant())) {
			strQuery += "	and UR.participant.idParticipantPk = :idParticipant ";
		}
		
		strQuery += "	ORDER BY UR.idUnblockRequestPk DESC";	
		
		Query query = em.createQuery(strQuery);	
		if(Validations.validateIsNullOrEmpty(searchReversalsTO.getNumberRequest())){
			if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getInitialDate()))
				query.setParameter("initialDate", searchReversalsTO.getInitialDate());
			if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getEndDate()))
				query.setParameter("endDate", searchReversalsTO.getEndDate());
		}else{
			query.setParameter("idUnblockRequestPk", searchReversalsTO.getNumberRequest());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getHolder().getIdHolderPk()))
			query.setParameter("idHolderPk", searchReversalsTO.getHolder().getIdHolderPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableRequestReversalState().getParameterTablePk()))
			query.setParameter("state", searchReversalsTO.getParameterTableRequestReversalState().getParameterTablePk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getParameterTableMotiveReversal().getParameterTablePk()))
			query.setParameter("motive", searchReversalsTO.getParameterTableMotiveReversal().getParameterTablePk());
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdIssuer())) {
			query.setParameter("idIssuer", searchReversalsTO.getIdIssuer());
		}
		
		if (Validations.validateIsNotNull(searchReversalsTO.getIdParticipant())) {
			query.setParameter("idParticipant", searchReversalsTO.getIdParticipant());
		}
		
		List<Object[]> lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Gets the unblock request.
	 *
	 * @param requestReversalsTO the request reversals to
	 * @return the unblock request
	 */
	@SuppressWarnings("unchecked")
	public UnblockRequest getUnblockRequest(RequestReversalsTO requestReversalsTO){
		
		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append(" SELECT UR ");
		stringBuilderSql.append(" FROM UnblockRequest UR ");
		stringBuilderSql.append(" INNER JOIN FETCH UR.unblockOperation UBO ");
		stringBuilderSql.append(" INNER JOIN FETCH UBO.custodyOperation ");
		stringBuilderSql.append(" INNER JOIN FETCH UBO.blockOperation BO ");
		stringBuilderSql.append(" INNER JOIN FETCH BO.securities SE ");
		stringBuilderSql.append(" INNER JOIN FETCH SE.issuer ");
		stringBuilderSql.append(" Where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndNotEmpty(requestReversalsTO.getIdUnblockRequestPk())){
			stringBuilderSql.append(" and UR.idUnblockRequestPk = :idUnblockRequestPk ");
		}
		
		if (Validations.validateIsNotNull(requestReversalsTO.getIdIssuer())) {
			stringBuilderSql.append(" and BO.securities.issuer.idIssuerPk = :idIssuer ");
		}
  
		TypedQuery<UnblockRequest> query = (TypedQuery<UnblockRequest>) em.createQuery(stringBuilderSql.toString());
		
		if(Validations.validateIsNotNullAndNotEmpty(requestReversalsTO.getIdUnblockRequestPk())){
			query.setParameter("idUnblockRequestPk", new Long(requestReversalsTO.getIdUnblockRequestPk()));
		}
		
		if (Validations.validateIsNotNull(requestReversalsTO.getIdIssuer())) {
			query.setParameter("idIssuer", requestReversalsTO.getIdIssuer());
		}
		
		return query.getSingleResult();
	}
	
	/**
	 * Validate entities states.
	 *
	 * @param unblockRequest the unblock request
	 * @return the error service type
	 */
	private ErrorServiceType validateEntitiesStates(UnblockRequest unblockRequest){
		/****** DEBERIA VALIDARSE EN EL FACADE PREVIO A ESTE *********/
		/** En caso sea la depositaria quien ejecuto el proceso, estas validaciones son incorrectas **/
		/*Map<String, Object> params = new HashMap<String, Object>();
		params.put("idHolderPkParam", unblockRequest.getHolder().getIdHolderPk());
		Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, params);
		if(!HolderStateType.REGISTERED.getCode().equals(holder.getStateHolder()))
			return ErrorServiceType.HOLDER_BLOCK;		*/
		return null;
	}

	/**
	 * Validate entities states.
	 *
	 * @param blockOperation the block operation
	 * @param blHABlocked the bl ha blocked
	 * @return the error service type
	 */
	private ErrorServiceType validateEntitiesStates(BlockOperation blockOperation, boolean blHABlocked) {
		/****** DEBERIA VALIDARSE EN EL FACADE PREVIO A ESTE *********/
		/** En caso sea la depositaria quien ejecuto el proceso, estas validaciones son incorrectas **/
		/*Map<String, Object> params = new HashMap<String, Object>();
		params.put("idHolderAccountPkParam", blockOperation.getHolderAccount().getIdHolderAccountPk());
		HolderAccount holderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, params);
		if(blHABlocked){
			List<Integer> lstHAStates = new ArrayList<Integer>();
			lstHAStates.add(HolderAccountStatusType.ACTIVE.getCode());
			lstHAStates.add(HolderAccountStatusType.BLOCK.getCode());
			if(!lstHAStates.contains(holderAccount.getStateAccount()))
				return ErrorServiceType.HOLDER_ACCOUNT_BLOCK;
		}else{
			if(!HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()))
				return ErrorServiceType.HOLDER_ACCOUNT_BLOCK;
		}
		params = new HashMap<String, Object>();
		params.put("idIsinCodePkParam", blockOperation.getSecurities().getIdSecurityCodePk());
		Security security = findObjectByNamedQuery(Security.SECURITY_STATE, params);
		if(!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity()))
			return ErrorServiceType.SECURITY_BLOCK;
		params = new HashMap<String, Object>();
		params.put("idParticipantPkParam", blockOperation.getParticipant().getIdParticipantPk());
		Participant participant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, params);
		if(!ParticipantStateType.REGISTERED.getCode().equals(participant.getState()))
			return ErrorServiceType.PARTICIPANT_BLOCK;*/
		return null;
	}
	
	/**
	 * Save reversal request.
	 *
	 * @param registerReversalTO the register reversal to
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long saveReversalRequest(RegisterReversalTO registerReversalTO) throws ServiceException{
		
		UnblockRequest objUnblockRequest = new UnblockRequest();
		if (Validations.validateIsNotNull(registerReversalTO.getIdParticipant())) {
			Participant objParticipant= new Participant();
			objParticipant.setIdParticipantPk(registerReversalTO.getIdParticipant());
			objUnblockRequest.setParticipant(objParticipant);
		}
		objUnblockRequest.setUnblockOperation(new ArrayList<UnblockOperation>());
		objUnblockRequest.setUnblockRequestFile(new ArrayList<UnblockRequestFile>());
		objUnblockRequest.setRegistryDate(CommonsUtilities.currentDateTime()); 
		objUnblockRequest.setRegistryUser(registerReversalTO.getRegistryUser());
		objUnblockRequest.setDocumentNumber(registerReversalTO.getDocumentNumber());
		Holder objHolder = new Holder();
		objHolder.setIdHolderPk(registerReversalTO.getIdHolderPk());
		objUnblockRequest.setHolder(objHolder);
		objUnblockRequest.setMotive(MotiveReverlsasType.NORMAL.getCode());
		if(registerReversalTO.getIdState()!=null){
			objUnblockRequest.setUnblockRequestState(registerReversalTO.getIdState());
		}else{
			objUnblockRequest.setUnblockRequestState(RequestReversalsStateType.REGISTERED.getCode());
		}
		
		objUnblockRequest.setRequestQuantity(registerReversalTO.getTotalUnblock());
		
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
				
		ErrorServiceType entitiesStates = validateEntitiesStates(objUnblockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		
		create(objUnblockRequest);
		
		if(Validations.validateIsNotNull(registerReversalTO.getDocumentFile1())){
			UnblockRequestFile objUnblockRequestFile1 = new UnblockRequestFile();
			objUnblockRequestFile1.setRegistryUser(registerReversalTO.getRegistryUser());
			objUnblockRequestFile1.setRegistryDate(CommonsUtilities.currentDate());
			objUnblockRequestFile1.setFileName(registerReversalTO.getFileName1());
			objUnblockRequestFile1.setDocumentFile(registerReversalTO.getDocumentFile1());	
			objUnblockRequestFile1.setUnblockRequest(objUnblockRequest);
			objUnblockRequest.getUnblockRequestFile().add(objUnblockRequestFile1);
			create(objUnblockRequestFile1);
		}
		
		if(Validations.validateIsNotNull(registerReversalTO.getDocumentFile2())){
			UnblockRequestFile objUnblockRequestFile2 = new UnblockRequestFile();
			objUnblockRequestFile2.setRegistryUser(registerReversalTO.getRegistryUser());
			objUnblockRequestFile2.setRegistryDate(CommonsUtilities.currentDate());
			objUnblockRequestFile2.setFileName(registerReversalTO.getFileName2());
			objUnblockRequestFile2.setDocumentFile(registerReversalTO.getDocumentFile2());			
			objUnblockRequestFile2.setUnblockRequest(objUnblockRequest);
			objUnblockRequest.getUnblockRequestFile().add(objUnblockRequestFile2);
			create(objUnblockRequestFile2);
		}
		
		for(BlockOpeForReversalsTO objBlockOpeForReversal:registerReversalTO.getLstBlockOpeForReversals()){
			//Se busca la operacion de bloqueo
			BlockOperation objBlockOperation = find(BlockOperation.class, objBlockOpeForReversal.getIdBlockOperationPk());
			//Se verifica que este en estado bloqueado
			if(AffectationStateType.BLOCKED.getCode().equals(objBlockOperation.getBlockState())){
				
				if(registerReversalTO.getIndValidateBlockDocument().equals(BooleanType.YES.getCode())){
					//we verify if the block operation exists at any custody request
					requestAffectationServiceBean.verifyBlockOperationAtOtherRequests(objBlockOperation.getIdBlockOperationPk(), 
																					objBlockOperation.getDocumentNumber().toString());
				}
				
				CustodyOperation objCustodyOperation = new CustodyOperation();
				objCustodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				objCustodyOperation.setState(RequestReversalsStateType.REGISTERED.getCode());
				objCustodyOperation.setRegistryUser(registerReversalTO.getRegistryUser());	
				objCustodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
				//Segun el tipo y nivel de bloqueo se coloca el tipo de operacion para el desbloqueo
				if(AffectationType.PAWN.getCode().equals(objBlockOperation.getBlockRequest().getBlockType())){
					if(LevelAffectationType.BLOCK.getCode().equals(objBlockOperation.getBlockLevel()))
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_UNBLOCK.getCode());
					else
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_PAWN_UNBLOCK_REBLOCK.getCode());
				}else if(AffectationType.RESERVE.getCode().equals(objBlockOperation.getBlockRequest().getBlockType())){
					if(LevelAffectationType.BLOCK.getCode().equals(objBlockOperation.getBlockLevel()))
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_UNBLOCK.getCode());
					else
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_RESERVE_UNBLOCK_REBLOCK.getCode());
				}else if(AffectationType.BAN.getCode().equals(objBlockOperation.getBlockRequest().getBlockType())){
					if(LevelAffectationType.BLOCK.getCode().equals(objBlockOperation.getBlockLevel()))
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_UNBLOCK.getCode());
					else
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_BAN_UNBLOCK_REBLOCK.getCode());
				}else if(AffectationType.OTHERS.getCode().equals(objBlockOperation.getBlockRequest().getBlockType())){
					if(LevelAffectationType.BLOCK.getCode().equals(objBlockOperation.getBlockLevel()))
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_UNBLOCK.getCode());
					else
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OTHERS_UNBLOCK_REBLOCK.getCode());
				}else{
					if(LevelAffectationType.BLOCK.getCode().equals(objBlockOperation.getBlockLevel()))
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_UNBLOCK.getCode());
					else
						objCustodyOperation.setOperationType(ParameterOperationType.SECURITIES_OPPOSITION_UNBLOCK_REBLOCK.getCode());
				}
				Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_UNBLOCKING);
				objCustodyOperation.setOperationNumber(operationNumber);
				
				UnblockOperation objUnblockOperation = new UnblockOperation();
				objUnblockOperation.setUnblockState(RequestReversalsStateType.REGISTERED.getCode());
				objUnblockOperation.setUnblockRequest(objUnblockRequest);
				objUnblockOperation.setBlockOperation(objBlockOperation);
				objUnblockOperation.setUnblockQuantity(objBlockOpeForReversal.getAmountUnblock());
				objUnblockOperation.setCustodyOperation(objCustodyOperation);
				objUnblockOperation.setUnblockMarketFactOperations(new ArrayList<UnblockMarketFactOperation>());
				objUnblockOperation.setDocumentNumber(operationNumber);
				
				if(objBlockOpeForReversal.getIdOperationEnforceDetailFk()!=null){
					objUnblockOperation.setBlockEnforceDetail(new BlockEnforceDetail());
					objUnblockOperation.getBlockEnforceDetail().setIdEnforceDetailPk(objBlockOpeForReversal.getIdOperationEnforceDetailFk());
				}
				
				create(objUnblockOperation);					
				update(objCustodyOperation);
				
				if(lstAffectationType.contains(objBlockOperation.getBlockRequest().getBlockType()))
					entitiesStates = validateEntitiesStates(objBlockOperation, true);
				else
					entitiesStates = validateEntitiesStates(objBlockOperation, false);
				if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
					throw new ServiceException(entitiesStates);
				
				objUnblockRequest.getUnblockOperation().add(objUnblockOperation);
				
//				/*ITERAMOS, LOS HECHOS DE MERCADO PARA LAS DESAFECTACIONES*/ 
				//solo si la desafectacion es parcial habra hechos del mercado
				if(Validations.validateIsNotNull(objBlockOpeForReversal.getReversalsMarkFactList()) && objBlockOpeForReversal.getReversalsMarkFactList().size()>0){
					for(ReversalsMarkFactTO reversalMarketFact: objBlockOpeForReversal.getReversalsMarkFactList()){
	
						HolderMarketFactBalance holderMarketFact = null;
						if(reversalMarketFact.getIdHolderMarketPk()!=null){
							holderMarketFact = em.find(HolderMarketFactBalance.class, reversalMarketFact.getIdHolderMarketPk());
						}else if(reversalMarketFact.getMarketDate()!=null && (reversalMarketFact.getMarketRate()!=null || reversalMarketFact.getMarketPrice()!=null)){
							Long holderMarketFactID = findHolderMarketFactID(reversalMarketFact.getMarketPrice(),
																			 	reversalMarketFact.getMarketRate(),
																			 	reversalMarketFact.getMarketDate(),
																objBlockOperation.getParticipant().getIdParticipantPk(), 
																objBlockOperation.getHolderAccount().getIdHolderAccountPk(), 
																objBlockOperation.getSecurities().getIdSecurityCodePk());
							holderMarketFact = new HolderMarketFactBalance();
							holderMarketFact.setIdMarketfactBalancePk(holderMarketFactID);
							holderMarketFact.setMarketDate(reversalMarketFact.getMarketDate());
							holderMarketFact.setMarketPrice(reversalMarketFact.getMarketPrice());
							holderMarketFact.setMarketRate(reversalMarketFact.getMarketRate());
						}
						
						UnblockMarketFactOperation unblockMarketFact = new UnblockMarketFactOperation();
						unblockMarketFact.setUnblockOperation(objUnblockOperation);
//						unblockMarketFact.setHolderMarketFactBalance(holderMarketFact);
						unblockMarketFact.setUnblockQuantity(reversalMarketFact.getBlockAmount());
						unblockMarketFact.setUnblockMarketState(UnblockMarketOperationStateType.REGISTER.getCode());
						unblockMarketFact.setRegistryUser(registerReversalTO.getRegistryUser());
						unblockMarketFact.setRegistryDate(CommonsUtilities.currentDateTime());
						
						if(holderMarketFact != null){
							/*COLUMNS FOR NOW IT'S NOT AVAILABLE*/
							unblockMarketFact.setMarketDate(holderMarketFact.getMarketDate());
							unblockMarketFact.setMarketPrice(holderMarketFact.getMarketPrice());
							unblockMarketFact.setMarketRate(holderMarketFact.getMarketRate());
						}						
						
						create(unblockMarketFact);
						
						objUnblockOperation.getUnblockMarketFactOperations().add(unblockMarketFact);
					}
				}
//				/*********************************************************************/
			}else{
				logger.error("block_operation: "+objBlockOperation.getIdBlockOperationPk()+" not in blocked state.");
				throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_NOT_BLOCKED,new Object[]{objBlockOperation.getDocumentNumber()});
			}
		}				
		return objUnblockRequest.getIdUnblockRequestPk();
	}

	/**
	 * Approve reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean approveReversalRequest(RequestReversalsTO requestReversal) throws ServiceException 
	{
		UnblockRequest objUnblockRequest = getUnblockRequest(requestReversal);
		
		if(!RequestReversalsStateType.REGISTERED.getCode().equals(objUnblockRequest.getUnblockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objUnblockRequest.getIdUnblockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.UNBLOCK_REQUEST_STATE_MODIFY, bodyData);		
		}
		ErrorServiceType entitiesStates = validateEntitiesStates(objUnblockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		
		objUnblockRequest.getUnblockOperation().size();
		objUnblockRequest.setApproveDate(CommonsUtilities.currentDateTime());
		objUnblockRequest.setApproveUser(requestReversal.getApproveUser());
		objUnblockRequest.setUnblockRequestState(RequestReversalsStateType.APPROVED.getCode());
		
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
		
		for(UnblockOperation objUnblockOperation:objUnblockRequest.getUnblockOperation()){
			UnblockOperation objUnblockOpeTemp = new UnblockOperation();
			objUnblockOpeTemp = objUnblockOperation;
			//Si la operacion de bloqueo ya fue desbloqueada se genera la excepcion de negocio
			if(AffectationStateType.UNBLOCKED.getCode().equals(objUnblockOpeTemp.getBlockOperation().getBlockState()))
			{
				Object[] param= new Object[1];
				param[0] = objUnblockOpeTemp.getBlockOperation().getDocumentNumber();
				throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_NOT_BLOCKED);
			}
			
			if(lstAffectationType.contains(objUnblockOpeTemp.getBlockOperation().getBlockRequest().getBlockType()))
				entitiesStates = validateEntitiesStates(objUnblockOpeTemp.getBlockOperation(), true);
			else
				entitiesStates = validateEntitiesStates(objUnblockOpeTemp.getBlockOperation(), false);
			if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
				throw new ServiceException(entitiesStates);			
			
			objUnblockOpeTemp.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
			objUnblockOpeTemp.getCustodyOperation().setApprovalUser(requestReversal.getApproveUser());				
			objUnblockOpeTemp.getCustodyOperation().setState(RequestReversalsStateType.APPROVED.getCode());	
			objUnblockOpeTemp.setUnblockState(RequestReversalsStateType.APPROVED.getCode());
		}			
		update(objUnblockRequest);	
		return true;
	}
	
	
	/**
	 * Annul reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean annulReversalRequest(RequestReversalsTO requestReversal) throws ServiceException 
	{
		UnblockRequest objUnblockRequest = getUnblockRequest(requestReversal);
		
		if(!RequestReversalsStateType.REGISTERED.getCode().equals(objUnblockRequest.getUnblockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objUnblockRequest.getIdUnblockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.UNBLOCK_REQUEST_STATE_MODIFY, bodyData);		
		}
		
		objUnblockRequest.getUnblockOperation().size();
		objUnblockRequest.setAnnulDate(CommonsUtilities.currentDateTime());
		objUnblockRequest.setAnnulUser(requestReversal.getAnnulUser());
		objUnblockRequest.setUnblockRequestState(RequestReversalsStateType.ANNULED.getCode());
		
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
		
		for(UnblockOperation objUnblockOperation:objUnblockRequest.getUnblockOperation()){
			UnblockOperation objUnblockOpeTemp = new UnblockOperation();
			objUnblockOpeTemp = objUnblockOperation;
			objUnblockOpeTemp.getCustodyOperation().setAnnulationDate(CommonsUtilities.currentDateTime());
			objUnblockOpeTemp.getCustodyOperation().setAnnulationUser(requestReversal.getAnnulUser());				
			objUnblockOpeTemp.getCustodyOperation().setState(RequestReversalsStateType.ANNULED.getCode());
			objUnblockOpeTemp.getCustodyOperation().setRejectMotive(requestReversal.getMotiveReject());
			objUnblockOpeTemp.getCustodyOperation().setRejectMotiveOther(requestReversal.getMotiveRejectOther());
			objUnblockOpeTemp.setUnblockState(RequestReversalsStateType.ANNULED.getCode());
		}			
		update(objUnblockRequest);	
		return true;
	}
	
	/**
	 * Confirm reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @param loggerUser the logger user
	 * @param isSecuritiesRenew the is securities renew
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean confirmReversalRequest(RequestReversalsTO requestReversal, LoggerUser loggerUser, boolean isSecuritiesRenew) throws ServiceException {
		
		UnblockRequest objUnblockRequest = getUnblockRequest(requestReversal);
		
		/*if(!RequestReversalsStateType.APPROVED.getCode().equals(objUnblockRequest.getUnblockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objUnblockRequest.getIdUnblockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.UNBLOCK_REQUEST_STATE_MODIFY, bodyData);		
		}*/
		
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
				
		ErrorServiceType entitiesStates = validateEntitiesStates(objUnblockRequest);
		if(Validations.validateIsNotNullAndNotEmpty(entitiesStates))
			throw new ServiceException(entitiesStates);
		
		objUnblockRequest.setConfirmationDate(CommonsUtilities.currentDateTime());
		objUnblockRequest.setConfirmationUser(requestReversal.getConfirmationUser());
		objUnblockRequest.setUnblockRequestState(RequestReversalsStateType.CONFIRMED.getCode());
		updateUnblockRequest(objUnblockRequest.getIdUnblockRequestPk(), loggerUser);
		for(UnblockOperation objUnblockOperation:objUnblockRequest.getUnblockOperation()){
			UnblockOperation objUnblockOpeTemp = new UnblockOperation();
			objUnblockOpeTemp = objUnblockOperation;
			//Si la operacion de bloqueo ya fue desbloqueada se genera la excepcion de negocio
			if(AffectationStateType.UNBLOCKED.getCode().equals(objUnblockOpeTemp.getBlockOperation().getBlockState())){
				throw new ServiceException(ErrorServiceType.BLOCK_OPERATION_NOT_BLOCKED,
						new Object[]{objUnblockOpeTemp.getBlockOperation().getIdBlockOperationPk().toString()});
			}
			//La cantidad a desbloquear es mayor a la cantidad actual del documento de bloqueo
			if(objUnblockOpeTemp.getUnblockQuantity().compareTo(objUnblockOpeTemp.getBlockOperation().getActualBlockBalance()) > 0){
				throw new ServiceException(ErrorServiceType.UNBLOCK_OPERATION_NOT_ENOUGH_AMOUNT,
						new Object[]{objUnblockOpeTemp.getBlockOperation().getIdBlockOperationPk().toString()});
			}
			
			if(lstAffectationType.contains(objUnblockOpeTemp.getBlockOperation().getBlockRequest().getBlockType()))
				entitiesStates = validateEntitiesStates(objUnblockOpeTemp.getBlockOperation(), true);
			else
				entitiesStates = validateEntitiesStates(objUnblockOpeTemp.getBlockOperation(), false);
			if(Validations.validateIsNotNullAndNotEmpty(entitiesStates)){
				throw new ServiceException(entitiesStates);
			}
			
			objUnblockOpeTemp.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
			objUnblockOpeTemp.getCustodyOperation().setConfirmUser(requestReversal.getConfirmationUser());
			objUnblockOpeTemp.getCustodyOperation().setState(RequestReversalsStateType.CONFIRMED.getCode());
			objUnblockOpeTemp.setUnblockState(RequestReversalsStateType.CONFIRMED.getCode());
			objUnblockOpeTemp.getBlockOperation().setActualBlockBalance(
					objUnblockOpeTemp.getBlockOperation().getActualBlockBalance().subtract(objUnblockOpeTemp.getUnblockQuantity()));
			
			for(UnblockMarketFactOperation unblockMarketFact: objUnblockOpeTemp.getUnblockMarketFactOperations()){
				for(BlockMarketFactOperation blockMarketFact: objUnblockOpeTemp.getBlockOperation().getBlockMarketFactOperations()){
					if(unblockMarketFact.getMarketDate().equals(blockMarketFact.getMarketDate()) &&
					  (unblockMarketFact.getMarketPrice()!=null && unblockMarketFact.getMarketPrice().equals(blockMarketFact.getMarketPrice()) ||
					   unblockMarketFact.getMarketRate()!=null && unblockMarketFact.getMarketRate().equals(blockMarketFact.getMarketRate()))){
						
						if((blockMarketFact.getActualBlockBalance().subtract(unblockMarketFact.getUnblockQuantity())).compareTo(BigDecimal.ZERO)>=0){
							blockMarketFact.setActualBlockBalance(blockMarketFact.getActualBlockBalance().subtract(unblockMarketFact.getUnblockQuantity()));
						}else{
							throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
						}
					}
				}
			}
			if(objUnblockOpeTemp.getBlockOperation().getActualBlockBalance().equals(new BigDecimal(0)))
				objUnblockOpeTemp.getBlockOperation().setBlockState(AffectationStateType.UNBLOCKED.getCode());	
			
			List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = 
					getHolderAccountBalanceTOs(objUnblockOpeTemp.getBlockOperation(),objUnblockOpeTemp.getUnblockQuantity());
			
			/*SETEAMOS LOS HECHOS DE MERCADO PARA LAS DESAFECTACIONES*/
			if(Validations.validateIsNotNull(objUnblockOpeTemp.getUnblockMarketFactOperations()) &&
				objUnblockOpeTemp.getUnblockMarketFactOperations().size()>0 ){
				setAccountBalanceMarketFact(accountBalanceTOs, objUnblockOpeTemp);	
			}
			
			update(objUnblockOpeTemp, loggerUser);	
			
			accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(objUnblockOpeTemp.getIdUnblockOperationPk(),
																objUnblockOpeTemp.getBlockOperation().getIdBlockOperationPk(),
																requestReversal.getBusinessProcessTypeId(), 
																accountBalanceTOs,objUnblockOpeTemp.getCustodyOperation().getOperationType()));
			
			/*
			 * CALLING THE CLIENT WEB SERVICE
			 */
			if(objUnblockOperation.getBlockOperation().getSecurities().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
					objUnblockOperation.getBlockOperation().getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
				
				if(objUnblockOperation.getBlockOperation().getSecurities().getExpirationDate().compareTo(CommonsUtilities.currentDate()) <= 0 
						&& SecurityStateType.SUSPENDED.getCode().equals(objUnblockOperation.getBlockOperation().getSecurities().getStateSecurity())){
					HolderAccountBalanceTO objHolderAccountBalanceTO = new HolderAccountBalanceTO();
					objHolderAccountBalanceTO.setIdHolderAccount(objUnblockOpeTemp.getBlockOperation().getHolderAccount().getIdHolderAccountPk());
					objHolderAccountBalanceTO.setIdParticipant(objUnblockOpeTemp.getBlockOperation().getParticipant().getIdParticipantPk());
					objHolderAccountBalanceTO.setIdSecurityCode(objUnblockOpeTemp.getBlockOperation().getSecurities().getIdSecurityCodePk());
					boolean hasReblock = haveReblocksWithOutUnblockRequest(objHolderAccountBalanceTO);
					if(!hasReblock){
						updateSecurityDpfSuspend(objUnblockOpeTemp.getBlockOperation().getSecurities().getIdSecurityCodePk(), loggerUser);
					}
				}								
				
				if(!isSecuritiesRenew){
					custodyServiceConsumer.sendUnblockOperationWebClient(objUnblockOperation, loggerUser);
				}				
			}

		}			
		
		return true;
		
	}

	/**
	 * Update unblock request.
	 *
	 * @param idUnblockRequest the id unblock request
	 * @param loggerUser the logger user
	 */
	public void updateUnblockRequest(Long idUnblockRequest, LoggerUser loggerUser)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE UnblockRequest ");
		stringBuffer.append(" SET unblockRequestState = :unblockRequestState ");
		stringBuffer.append(" , confirmationUser = :confirmationUser ");
		stringBuffer.append(" , confirmationDate = :confirmationDate ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idUnblockRequestPk = :idUnblockRequest ");
		
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idUnblockRequest", idUnblockRequest);
		query.setParameter("unblockRequestState", RequestReversalsStateType.CONFIRMED.getCode());
		query.setParameter("confirmationUser", loggerUser.getUserName());
		query.setParameter("confirmationDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Reject reversal request.
	 *
	 * @param requestReversal the request reversal
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean rejectReversalRequest(RequestReversalsTO requestReversal) throws ServiceException {
		
		UnblockRequest objUnblockRequest = find(UnblockRequest.class, new Long(requestReversal.getIdUnblockRequestPk()));
		if(!RequestReversalsStateType.APPROVED.getCode().equals(objUnblockRequest.getUnblockRequestState())){
			Object[] bodyData = new Object[1];
			bodyData[0] = objUnblockRequest.getIdUnblockRequestPk().toString();
			throw new ServiceException(ErrorServiceType.UNBLOCK_REQUEST_STATE_MODIFY, bodyData);
		}
				
		List<Integer> lstAffectationType = new ArrayList<Integer>();
		lstAffectationType.add(AffectationType.BAN.getCode());
		lstAffectationType.add(AffectationType.OPPOSITION.getCode());
		lstAffectationType.add(AffectationType.OTHERS.getCode());
				
		objUnblockRequest.getUnblockOperation().size();
		objUnblockRequest.setRejectDate(CommonsUtilities.currentDateTime());
		objUnblockRequest.setRejectUser(requestReversal.getConfirmationUser());
		objUnblockRequest.setUnblockRequestState(RequestReversalsStateType.REJECTED.getCode());
		for(UnblockOperation objUnblockOperation:objUnblockRequest.getUnblockOperation()){
			UnblockOperation objUnblockOpeTemp = new UnblockOperation();
			objUnblockOpeTemp = objUnblockOperation;
			objUnblockOpeTemp.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			objUnblockOpeTemp.getCustodyOperation().setRejectUser(requestReversal.getConfirmationUser());				
			objUnblockOpeTemp.getCustodyOperation().setState(RequestReversalsStateType.REJECTED.getCode());
			objUnblockOpeTemp.setUnblockState(RequestReversalsStateType.REJECTED.getCode());
			objUnblockOpeTemp.getCustodyOperation().setRejectMotive(requestReversal.getMotiveReject());
			objUnblockOpeTemp.getCustodyOperation().setRejectMotiveOther(requestReversal.getMotiveRejectOther());
			//Cuando hay rechazo de solicitud de desafectacion no se mueven saldos
						
		}			
		update(objUnblockRequest);	
		return true;
	}

	/**
	 * Gets the holder account balance t os.
	 *
	 * @param objBlockOperation the obj block operation
	 * @param unblock the unblock
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	private List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(BlockOperation objBlockOperation, BigDecimal unblock) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(objBlockOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(objBlockOperation.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setIdSecurityCode(objBlockOperation.getSecurities().getIdSecurityCodePk());
		holderAccountBalanceTO.setStockQuantity(unblock);
		accountBalanceTOs.add(holderAccountBalanceTO);
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param idCustodyOperationRef the id custody operation ref
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long idCustodyOperationRef, Long businessProcessType, List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType){		
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setIdRefCustodyOperation(idCustodyOperationRef);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		return objAccountComponent;
	}
	
	/**
	 * Sets the account balance market fact.
	 *
	 * @param accountBalanceTOs the account balance t os
	 * @param unBlockOperation the un block operation
	 */
	private void setAccountBalanceMarketFact(List<HolderAccountBalanceTO> accountBalanceTOs, UnblockOperation unBlockOperation){
		// TODO Auto-generated method stub
		for(HolderAccountBalanceTO hab: accountBalanceTOs){
			hab.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			if(hab.getIdParticipant().equals(unBlockOperation.getBlockOperation().getParticipant().getIdParticipantPk()) && 
			   hab.getIdSecurityCode().equals(unBlockOperation.getBlockOperation().getSecurities().getIdSecurityCodePk() )&&
			   hab.getIdHolderAccount().equals(unBlockOperation.getBlockOperation().getHolderAccount().getIdHolderAccountPk())){
				for(UnblockMarketFactOperation unblockHab: unBlockOperation.getUnblockMarketFactOperations()){
					MarketFactAccountTO marketFactAccountTO = new MarketFactAccountTO();
					marketFactAccountTO.setQuantity(unblockHab.getUnblockQuantity());
					marketFactAccountTO.setMarketDate(unblockHab.getMarketDate());
					marketFactAccountTO.setMarketPrice(unblockHab.getMarketPrice());
					marketFactAccountTO.setMarketRate(unblockHab.getMarketRate());
					hab.getLstMarketFactAccounts().add(marketFactAccountTO);
				}
			}
		}
	}
	
	/**
	 * Find holder market fact id.
	 *
	 * @param marketPrice the market price
	 * @param marketRate the market rate
	 * @param marketDate the market date
	 * @param participantID the participant id
	 * @param accountID the account id
	 * @param securityCode the security code
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long findHolderMarketFactID(BigDecimal marketPrice, BigDecimal marketRate, Date marketDate, Long participantID, Long accountID, String securityCode) throws ServiceException{
		Map<String,Object> parameters = new HashMap<String,Object>();
		StringBuilder stringSQL = new StringBuilder();
		stringSQL.append("SELECT hmb.IdMarketfactBalancePk			"); 
		stringSQL.append(" FROM HolderMarketFactBalance hmb			");
		stringSQL.append(" WHERE hmb.participant.idParticipantPk = :participantID And hmb.holderAccount.idHolderAccountPk = :accountID And");
		stringSQL.append("       hmb.security.idSecurityCodePk = :securityID And TRUNC(hmb.marketDate) = TRUNC(:marketDate)");
		if(marketRate!=null){
			stringSQL.append("       AND hmb.marketRate = :marketRate");
			parameters.put("marketRate",marketRate);
		}
		if(marketPrice!=null){
			stringSQL.append("       AND hmb.marketPrice = :marketPrice");
			parameters.put("marketPrice", marketPrice);
		}
		parameters.put("participantID", participantID);
		parameters.put("accountID", accountID);
		parameters.put("securityID", securityCode);
		parameters.put("marketDate", marketDate);
		
		try{
			return (Long) findObjectByQueryString(stringSQL.toString(), parameters);
		}catch(NoResultException ex){
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
	}
		
	/**
	 * Find unblock request reversion.
	 *
	 * @param searchReversalsTO the search reversals to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UnblockRequest> findUnblockRequestReversion(SearchReversalsTO searchReversalsTO) throws ServiceException {		
		String strQuery = 	"	SELECT DISTINCT UR " +			
				"	FROM UnblockRequest UR " +
				"   where UR.unblockRequestState in (:lstBlockOperationState) " +
				"   and trunc(UR.registryDate) = :initialDate " ;		
		Query query = em.createQuery(strQuery);		
		query.setParameter("initialDate", searchReversalsTO.getInitialDate());		
		query.setParameter("lstBlockOperationState", searchReversalsTO.getLstStatus());		
		return  (List<UnblockRequest>) query.getResultList();				
	}
	
	/**
	 * Identify unblock operations to send.
	 *
	 * @param sendDate the send date
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void identifyUnblockOperationsToSend(Date sendDate,LoggerUser loggerUser) throws ServiceException{
		/** OBTENEMOS LA LISTA DE OP. DE BLOQUEO A ENVIAR **/
		List<UnblockOperation> lstUnblockOperation = getListUnblockOperationWebService(sendDate);
		if(Validations.validateListIsNotNullAndNotEmpty(lstUnblockOperation)){
			/** CALLING THE CLIENT WEB SERVICE **/
			for (UnblockOperation objUnblockOperation : lstUnblockOperation){				
				custodyServiceConsumer.sendUnblockOperationWebClient(objUnblockOperation, loggerUser);
			}
		}
	}
	
	/**
	 * Gets the list unblock operation web service.
	 *
	 * @param sendDate the send date
	 * @return the list unblock operation web service
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UnblockOperation> getListUnblockOperationWebService(Date sendDate) throws ServiceException{
		List<UnblockOperation> lstBlockOperation = null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT UO FROM UnblockOperation UO ");
		stringBuffer.append(" 	inner join fetch UO.blockOperation bo ");
		stringBuffer.append(" 	inner join fetch bo.securities sec ");
		stringBuffer.append(" 	inner join fetch UO.unblockRequest ur ");
		stringBuffer.append(" 	inner join fetch UO.custodyOperation co ");
		stringBuffer.append(" 	inner join fetch bo.participant p ");
		stringBuffer.append(" 	inner join fetch bo.holderAccount ha ");
		stringBuffer.append(" 	inner join fetch bo.blockRequest br ");
		stringBuffer.append(" 	inner join fetch br.blockEntity be ");
		stringBuffer.append(" WHERE trunc(ur.confirmationDate) = trunc(:date) ");
		stringBuffer.append(" 	and ur.unblockRequestState = :unblockRequestSt ");
		stringBuffer.append(" 	and co.indWebservice = :indWebService ");
		stringBuffer.append(" 	and bo.idBlockOperationPk not in ");
		stringBuffer.append("		(select rbo.blockOperation.idBlockOperationPk from RenewalBlockOperation rbo ");
		stringBuffer.append("		where bo.idBlockOperationPk = rbo.blockOperation.idBlockOperationPk) ");
		stringBuffer.append(" 	and sec.securityClass in (:classDPF,:classDPA) ");
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("date", sendDate);
		query.setParameter("unblockRequestSt", RequestReversalsStateType.CONFIRMED.getCode());
		query.setParameter("indWebService", ComponentConstant.ZERO);
		query.setParameter("classDPF", SecurityClassType.DPF.getCode());
		query.setParameter("classDPA", SecurityClassType.DPA.getCode());
		lstBlockOperation  = (List<UnblockOperation>) query.getResultList();
		return lstBlockOperation;
	}
	

	/**
	 * Have reblocks with out unblock request.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean haveReblocksWithOutUnblockRequest(HolderAccountBalanceTO objHolderAccountBalanceTO)throws ServiceException{
		List<Long> lstResult = haveReblocks(objHolderAccountBalanceTO);
		boolean blExist = false;
		for(Long idBlockOperation:lstResult){
			if(!haveUnblockOperationRegister(idBlockOperation)){
				blExist = true;
				break;
			}				
		}
		return blExist;
	}
	
	/**
	 * Have reblocks.
	 *
	 * @param objHolderAccountBalanceTO the obj holder account balance to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> haveReblocks(HolderAccountBalanceTO objHolderAccountBalanceTO) throws ServiceException{
		String strQuery = "			SELECT BO.idBlockOperationPk" +
				"		FROM BlockOperation BO" +
				"		WHERE BO.holderAccount.idHolderAccountPk = :idHolderAccountPk" +
				"		AND BO.participant.idParticipantPk = :idParticipantPk" +
				"		AND BO.securities.idSecurityCodePk = :idSecurityCodePk" +
				"		AND BO.blockLevel = :level" +
				"		AND BO.actualBlockBalance > 0" +
				"		AND BO.blockState = :state";	
		Query query = em.createQuery(strQuery);
		query.setParameter("idHolderAccountPk", objHolderAccountBalanceTO.getIdHolderAccount());
		query.setParameter("idParticipantPk", objHolderAccountBalanceTO.getIdParticipant());
		query.setParameter("idSecurityCodePk", objHolderAccountBalanceTO.getIdSecurityCode());
		query.setParameter("state", AffectationStateType.BLOCKED.getCode());
		query.setParameter("level", LevelAffectationType.REBLOCK.getCode());
		List<Long> lstResult = query.getResultList();
		return lstResult;
	}
	
	/**
	 * Have unblock operation register.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return true, if successful
	 */
	public boolean haveUnblockOperationRegister(Long idBlockOperationPk){
		try{
			String strQuery = 	"	SELECT UO.blockOperation.idBlockOperationPk" +
								"	FROM UnblockOperation UO" +
								"	WHERE UO.blockOperation.idBlockOperationPk = :idBlockOperationPk" +
								"	AND UO.custodyOperation.state = :state";
			Query query = em.createQuery(strQuery);
			query.setParameter("idBlockOperationPk", idBlockOperationPk);
			query.setParameter("state", RequestReversalsStateType.REGISTERED.getCode());
			if(Validations.validateIsNotNullAndNotEmpty(query.getSingleResult()))
				return true;
		}catch (NoResultException e) {
			return false;
		}
		return false;
	}
	
	
	/**
	 * Update security dpf suspend.
	 *
	 * @param idSecurityPk the id security pk
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityDpfSuspend(String idSecurityPk, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update Security security set security.stateSecurity = :parState , "+				
				" security.lastModifyUser = :parLastModifyUser, security.lastModifyDate = :parLastModifyDate, " +
				" security.lastModifyIp = :parLastModifyIp, security.lastModifyApp = :parLastModifyApp " +
				" where security.idSecurityCodePk = :parIdSecurityCode ");
		parameters.put("parIdSecurityCode", idSecurityPk);		
		parameters.put("parState", SecurityStateType.REGISTERED.getCode());		
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
}
