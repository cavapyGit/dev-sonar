package com.pradera.custody.reversals.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;
import com.pradera.custody.affectation.to.ResultSearchTO;
import com.pradera.custody.reversals.facade.RequestReversalsServiceFacade;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.FileReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.reversals.to.SearchReversalsTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.reversals.type.ReversalsActionType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.notification.ProcessNotification;
import com.pradera.model.process.BusinessProcess;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RequestReversalsMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@LoggerCreateBean
@DepositaryWebBean
public class RequestReversalsMgmBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The search reversals to. */
	private SearchReversalsTO searchReversalsTO;
	
	/** The register reversal to. */
	private RegisterReversalTO registerReversalTO;
	
	/** The lst reversal action. */
	private List<ParameterTable> lstReversalAction;
	
	/** The general pameters facade. */
	@EJB
	GeneralParametersFacade generalPametersFacade;
	
	/** The request reversals service facade. */
	@EJB
	RequestReversalsServiceFacade requestReversalsServiceFacade;
	
	/** The request affectation service facade. */
	@EJB
	RequestAffectationServiceFacade requestAffectationServiceFacade;
	
	/** The Constant STR_NEW_REVERSAL_MAPPING. */
	private static final String STR_NEW_REVERSAL_MAPPING = "newReversal";
	
	/** The request reversal to. */
	private RequestReversalsTO requestReversalTO;
	
	/** The lst request reversals. */
	private RequestReversalsDataModel lstRequestReversals;
	
	/** The bl approve. */
	private boolean blApprove;
	
	/** The bl annul. */
	private boolean blAnnul;
	
	/** The bl confirm. */
	private boolean blConfirm;
	
	/** The bl reject. */
	private boolean blReject;
	
	/** The bl no result. */
	private boolean blNoResult;
	
	/** The bl view detail. */
	private boolean blViewDetail;
	/** The flag issuer block. */
	private boolean flagIssuerBlock;
	
	/** The Constant STR_VIEW_REVERSAL_MAPPING. */
	private static final String STR_VIEW_REVERSAL_MAPPING = "viewRequestReversal";
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The batch service facade. */
	@EJB
	BatchProcessServiceFacade batchServiceFacade;
	
	/** The max days of custody request. */
	@Inject @Configurable
	Integer maxDaysOfCustodyRequest;
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;

	/** The log. */
	Logger log = Logger.getLogger(RequestReversalsMgmBean.class.getName());
	
	/** The list file reversals to. */
	private List<FileReversalsTO> listFileReversalsTO;
	
	/** The support selected. */
	private Integer supportSelected;
	
	/** The list support name. */
	private Map<String,Integer> listSupportName;
	
	private Integer idReversals = ReversalsActionType.REVERSALS.getCode();
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		searchReversalsTO = new SearchReversalsTO();	
		searchReversalsTO.setAction(null);
		searchReversalsTO.setHolder(new Holder());
		searchReversalsTO.setSecurity(new Security());
		searchReversalsTO.setBlockEntity(new BlockEntity());
		searchReversalsTO.setParameterTableAffectationLevel(new ParameterTable());
		searchReversalsTO.setParameterTableAffectationType(new ParameterTable());
		searchReversalsTO.setParameterTableRequestReversalState(new ParameterTable());
		searchReversalsTO.setParameterTableMotiveReversal(new ParameterTable());
		searchReversalsTO.setInitialDate(getCurrentSystemDate());
		searchReversalsTO.setEndDate(getCurrentSystemDate());
		lstRequestReversals = null;
		registerReversalTO = null;
		blNoResult = false;
		listFileReversalsTO = new ArrayList<FileReversalsTO>();
		lstReversalAction= new ArrayList<ParameterTable>();
		listSupportName = new HashMap<String,Integer>();
		listSupportName.put(PropertiesUtilities.getMessage(JSFUtilities.getCurrentLocale(),"lbl.reversal.register.unblockRequest.file1").toUpperCase(),new Integer(1));
		listSupportName.put(PropertiesUtilities.getMessage(JSFUtilities.getCurrentLocale(), "lbl.reversal.register.unblockRequest.file2").toUpperCase(),new Integer(2));
		fillCombos();
		flagIssuerBlock = false;
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			flagIssuerBlock = true;
		}
	}
	
	/**
	 * Sets the flags false.
	 */
	private void setFlagsFalse(){
		blApprove = false;
		blAnnul = false;
		blConfirm = false;
		blReject = false;
		blViewDetail= false;
//		blDisabledFields= true;
//		blRenderedMotives= false;
	}
	
	/**
	 * Search by.
	 */
	public void searchBy(){
		JSFUtilities.resetViewRoot();
		if(GeneralConstants.TWO_VALUE_INTEGER.equals(searchReversalsTO.getSearchType())){
			searchReversalsTO.setBlDefaultSearch(true);			
			searchReversalsTO.setHolder(new Holder());
			searchReversalsTO.getParameterTableMotiveReversal().setParameterTablePk(null);
			searchReversalsTO.getParameterTableRequestReversalState().setParameterTablePk(null);
			searchReversalsTO.setInitialDate(CommonsUtilities.currentDate());
			searchReversalsTO.setEndDate(CommonsUtilities.currentDate());
		}else{
			searchReversalsTO.setBlDefaultSearch(false);
			searchReversalsTO.setNumberRequest(null);
		}
	}
	
	/**
	 * Search by action.
	 */
	@LoggerAuditWeb
	public void searchByAction(){
		try{
			if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getAction())){
				List<BlockOpeForReversalsTO> lstBlockOpeForReversals = new ArrayList<BlockOpeForReversalsTO>();
				List<RequestReversalsTO> lstReqReversals = new ArrayList<RequestReversalsTO>();
				searchReversalsTO.setIdIssuer(userInfo.getUserAccountSession().getIssuerCode());
				searchReversalsTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				
				if(ReversalsActionType.REVERSALS.getCode().equals(Integer.valueOf(searchReversalsTO.getAction()))){
					if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getHolder().getIdHolderPk())){
							lstBlockOpeForReversals = requestReversalsServiceFacade.findBlockOperation(searchReversalsTO);
							for(BlockOpeForReversalsTO objBlockOpeForReversalsTO :  lstBlockOpeForReversals){
								
								/*MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
								marketFactBalance.setSecurityCodePk(objBlockOpeForReversalsTO.getIdSecurityCodePk());
								marketFactBalance.setHolderAccountPk(objBlockOpeForReversalsTO.getIdHolderAccountPk());
								marketFactBalance.setParticipantPk(objBlockOpeForReversalsTO.getIdParticipantPk());
								
								holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
							    
								if(holderMarketFactBalance.getMarketFacBalances().size()>1){
									objBlockOpeForReversalsTO.setMarketLock(true);
								}*/
								objBlockOpeForReversalsTO.setMarketLock(Boolean.FALSE);
							}
							if(Validations.validateListIsNotNullAndNotEmpty(lstBlockOpeForReversals)){
								searchReversalsTO.setLstBlockOpeForReversals(lstBlockOpeForReversals);
								blNoResult = false;
							}else{
								searchReversalsTO.setLstBlockOpeForReversals(null);
								blNoResult = true;
							}
					}else{											
						JSFUtilities.addContextMessage(
								":frmActionUnlock:helperRNT:helperRNT",FacesMessage.SEVERITY_ERROR,
								PropertiesUtilities.getMessage("alt.reversal.needRNT"),
								PropertiesUtilities.getMessage("alt.reversal.needRNT"));
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
								, PropertiesUtilities.getMessage("message.data.required"));
						JSFUtilities.showSimpleValidationDialog();						
					}				
				}else if(ReversalsActionType.SEARCH_REQUEST.getCode().equals(Integer.valueOf(searchReversalsTO.getAction()))){					
						//Seteamos a null el objeto seleccionado de la lista
						requestReversalTO = null;
						lstReqReversals = requestReversalsServiceFacade.findReversalRequests(searchReversalsTO);
						if(Validations.validateListIsNotNullAndNotEmpty(lstReqReversals)){
							lstRequestReversals = new RequestReversalsDataModel(lstReqReversals);
							showPrivilegeButtons();
							blNoResult = false;
						}else{
							lstRequestReversals = null;
							blNoResult = true;
						}
				}else{
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage("alt.reversal.needAction"));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clear block opes.
	 */
	public void clearBlockOpes(){
		if(Validations.validateIsNotNullAndNotEmpty(searchReversalsTO.getHolder().getIdHolderPk())){
			if(ReversalsActionType.REVERSALS.getCode().equals(Integer.valueOf(searchReversalsTO.getAction()))){
				searchReversalsTO.setLstBlockOpeForReversals(null);
				blNoResult = false;
			}
		}
	}
	
	/**
	 * Show privilege buttons.
	 */
	public void showPrivilegeButtons(){		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();		
		privilegeComponent.setBtnConfirmView(true);		
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnApproveView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * View request detail.
	 *
	 * @param event the event
	 */
	public void viewRequestDetail(javax.faces.event.ActionEvent event){
		try{
			RequestReversalsTO requeReversalTO = new RequestReversalsTO();
			requeReversalTO = (RequestReversalsTO) event.getComponent().getAttributes().get("requestReversal");
			this.requestReversalTO= requeReversalTO;
			setFlagsFalse();
			requeReversalTO.setIdIssuer(userInfo.getUserAccountSession().getIssuerCode());
			registerReversalTO = requestReversalsServiceFacade.findRequestReversalDetail(requeReversalTO);
		    this.blViewDetail= true;
			listFileReversalsTO = new ArrayList<FileReversalsTO>();
			
			if(registerReversalTO.getDocumentFile1()!=null){
				FileReversalsTO fileReversalsTO = new FileReversalsTO();
				fileReversalsTO.setDocumentFile(registerReversalTO.getDocumentFile1());
				fileReversalsTO.setFileName(registerReversalTO.getFileName1());
				String extensionFile = registerReversalTO.getFileName1().toLowerCase().substring(registerReversalTO.getFileName1().lastIndexOf(".")+1,registerReversalTO.getFileName1().length());
				fileReversalsTO.setFileExtension(extensionFile.toUpperCase());
				fileReversalsTO.setFileSize(Integer.valueOf(registerReversalTO.getDocumentFile1().length).longValue()/1000L);
				listFileReversalsTO.add(fileReversalsTO);
			}
			
			if(registerReversalTO.getDocumentFile2()!=null){
				FileReversalsTO fileReversalsTO = new FileReversalsTO();
				fileReversalsTO.setDocumentFile(registerReversalTO.getDocumentFile2());
				fileReversalsTO.setFileName(registerReversalTO.getFileName2());
				String extensionFile = registerReversalTO.getFileName2().toLowerCase().substring(registerReversalTO.getFileName2().lastIndexOf(".")+1,registerReversalTO.getFileName2().length());
				fileReversalsTO.setFileExtension(extensionFile.toUpperCase());
				fileReversalsTO.setFileSize(Integer.valueOf(registerReversalTO.getDocumentFile2().length).longValue()/1000L);
				listFileReversalsTO.add(fileReversalsTO);		
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	

	/**
	 * Validate is valid user.
	 *
	 * @param requestReversalTO the request reversal to
	 * @return the list
	 */
	private List<String> validateIsValidUser(RequestReversalsTO requestReversalTO) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;	
		OperationUserTO operationUserTO = new OperationUserTO();  
		operationUserTO.setOperNumber(requestReversalTO.getIdUnblockRequestPk().toString());
		operationUserTO.setUserName(requestReversalTO.getRegisterUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	public void documentFile(){
		//Document File Information
		FileReversalsTO fileReversalsTO = new FileReversalsTO();
		fileReversalsTO.setDocumentFile(registerReversalTO.getDocumentFile1());
		fileReversalsTO.setFileName(registerReversalTO.getFileName1());
		String extensionFile = registerReversalTO.getFileName1().toLowerCase().substring(registerReversalTO.getFileName1().lastIndexOf(".")+1,registerReversalTO.getFileName1().length());
		fileReversalsTO.setFileExtension(extensionFile.toUpperCase());
		fileReversalsTO.setFileSize(Integer.valueOf(registerReversalTO.getDocumentFile1().length).longValue()/1000L);
		fileReversalsTO.setPosition(new Integer(1));
		boolean exist=false;
		int position=0;				
	
		for(int i=0;i<listFileReversalsTO.size();i++){
			if(listFileReversalsTO.get(i).getPosition().equals(new Integer(1))){
				exist=true;
				position=i;
			}
		}
		if(exist){
			listFileReversalsTO.set(position,fileReversalsTO);
		}else{
			listFileReversalsTO.add(fileReversalsTO);	
		}
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	public String validateApprove(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestReversalTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestReversalsStateType.REGISTERED.getCode().equals(requestReversalTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestReversalTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					
					registerReversalTO = requestReversalsServiceFacade.findRequestReversalDetail(requestReversalTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (UnblockOperation objUnblockOperation: registerReversalTO.getObjUnblockRequest().getUnblockOperation()) {
							//the issuer shall confirm their own DPA and DPF securities only
							if (!issuerCode.equalsIgnoreCase(objUnblockOperation.getBlockOperation().getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = registerReversalTO.getIdUnblockRequestPk().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blApprove = true;
					return STR_VIEW_REVERSAL_MAPPING; 
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = requestReversalTO.getIdUnblockRequestPk();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_REGISTERED,bodyData));
					JSFUtilities.showSimpleValidationDialog();					
					return GeneralConstants.EMPTY_STRING;
				}					
			}	
		} catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}		
	}
	
	/**
	 * Validate annul.
	 *
	 * @return the string
	 */
	public String validateAnnul(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestReversalTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestReversalsStateType.REGISTERED.getCode().equals(requestReversalTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestReversalTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  
					registerReversalTO = requestReversalsServiceFacade.findRequestReversalDetail(requestReversalTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (UnblockOperation objUnblockOperation: registerReversalTO.getObjUnblockRequest().getUnblockOperation()) {
							//the issuer shall confirm their own DPA and DPF securities only
							if (!issuerCode.equalsIgnoreCase(objUnblockOperation.getBlockOperation().getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = registerReversalTO.getIdUnblockRequestPk().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blAnnul = true;
					return STR_VIEW_REVERSAL_MAPPING; 
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = requestReversalTO.getIdUnblockRequestPk();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_REGISTERED,bodyData));
					JSFUtilities.showSimpleValidationDialog();					
					return GeneralConstants.EMPTY_STRING;
				}					
			}	
		} catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}		
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm(){
		try {
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestReversalTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestReversalsStateType.APPROVED.getCode().equals(requestReversalTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestReversalTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  
					registerReversalTO = requestReversalsServiceFacade.findRequestReversalDetail(requestReversalTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (UnblockOperation objUnblockOperation: registerReversalTO.getObjUnblockRequest().getUnblockOperation()) {
							//the issuer shall confirm their own DPA and DPF securities only
							if (!issuerCode.equalsIgnoreCase(objUnblockOperation.getBlockOperation().getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = registerReversalTO.getIdUnblockRequestPk().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
							if (!SecurityClassType.DPF.getCode().equals(objUnblockOperation.getBlockOperation().getSecurities().getSecurityClass()) &&
								!SecurityClassType.DPA.getCode().equals(objUnblockOperation.getBlockOperation().getSecurities().getSecurityClass())) 
							{
								Object[] bodyData = new Object[1];
								bodyData[0] = registerReversalTO.getIdUnblockRequestPk().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_SECURITY_CLASS_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blConfirm = true;
					return STR_VIEW_REVERSAL_MAPPING; 
				}else{
					Object[] bodyData = new Object[1];
					bodyData[0] = requestReversalTO.getIdUnblockRequestPk();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_APPROVED,bodyData));
					JSFUtilities.showSimpleValidationDialog();					
					return GeneralConstants.EMPTY_STRING;
				}					
			}	
		} catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}		
	}	
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject(){
		try{
			setFlagsFalse();
			if(Validations.validateIsNullOrEmpty(requestReversalTO)){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}else{
				if(RequestReversalsStateType.APPROVED.getCode().equals(requestReversalTO.getIdState())){
					
					List<String> lstOperationUser = validateIsValidUser(requestReversalTO);
					  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
						  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
							String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT,new Object[]{StringUtils.join(lstOperationUser,",")});
							showMessageOnDialog(headerMessage, bodyMessage);
							JSFUtilities.showSimpleValidationDialog();
							return "";
					  }
					  
					registerReversalTO = requestReversalsServiceFacade.findRequestReversalDetail(requestReversalTO);
					//Document File Information
					documentFile();
					//now we validate according the user
					String issuerCode= userInfo.getUserAccountSession().getIssuerCode();
					if (Validations.validateIsNotNull(issuerCode)) {
						for (UnblockOperation objUnblockOperation: registerReversalTO.getObjUnblockRequest().getUnblockOperation()) {
							//the issuer shall confirm their own DPA and DPF securities only
							if (!issuerCode.equalsIgnoreCase(objUnblockOperation.getBlockOperation().getSecurities().getIssuer().getIdIssuerPk())) {
								Object[] bodyData = new Object[1];
								bodyData[0] = registerReversalTO.getIdUnblockRequestPk().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_ISSUER_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
							if (!SecurityClassType.DPF.getCode().equals(objUnblockOperation.getBlockOperation().getSecurities().getSecurityClass()) &&
								!SecurityClassType.DPA.getCode().equals(objUnblockOperation.getBlockOperation().getSecurities().getSecurityClass())) 
							{
								Object[] bodyData = new Object[1];
								bodyData[0] = registerReversalTO.getIdUnblockRequestPk().toString();
								showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
										, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_SECURITY_CLASS_CONFIFM,bodyData));
								JSFUtilities.showSimpleValidationDialog();
								return GeneralConstants.EMPTY_STRING;
							}
						}
					}
					blReject = true;
					return STR_VIEW_REVERSAL_MAPPING;				
				}else{					
					Object[] bodyData = new Object[1];					
					bodyData[0] = requestReversalTO.getIdUnblockRequestPk().toString();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
							, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_INVALID_NO_APPROVED,bodyData));
					JSFUtilities.showSimpleValidationDialog();
					return GeneralConstants.EMPTY_STRING;
				}				
			}	
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
			return GeneralConstants.EMPTY_STRING;
		}
	}
	
	/**
	 * Before action.
	 */
	@LoggerAuditWeb
	public void beforeAction(){
		if (blApprove) {
			Object[] bodyData = new Object[1];
			bodyData[0] = registerReversalTO.getIdUnblockRequestPk();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_APPROVE,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		} else if (blAnnul) {
			Object[] bodyData = new Object[1];
			bodyData[0] = registerReversalTO.getIdUnblockRequestPk();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_ANNUL,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		} else if(blConfirm){
			Object[] bodyData = new Object[1];
			bodyData[0] = registerReversalTO.getIdUnblockRequestPk();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_CONFIRM,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}
		else if(blReject){
			Object[] bodyData = new Object[1];
			bodyData[0] = registerReversalTO.getIdUnblockRequestPk();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT), 
					PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ASK_REJECT,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
		}
	}	
	
	/**
	 * Hide dialogs.
	 */
	public void hideDialogs(){
	}
	
	/**
	 * View block ope detail.
	 *
	 * @param event the event
	 */
	public void viewBlockOpeDetail(javax.faces.event.ActionEvent event){
		try{
			BlockOpeForReversalsTO blockOpeForReveTO = new BlockOpeForReversalsTO();
			blockOpeForReveTO = (BlockOpeForReversalsTO) event.getComponent().getAttributes().get("blockOpe");		
			searchReversalsTO.setBlockOpeForReverTO(requestReversalsServiceFacade.findBlockOperationDetail(blockOpeForReveTO));
			
			JSFUtilities.executeJavascriptFunction("PF('dlgWBlockOpe').show()");
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Show fields for action.
	 */
	public void showFieldsForAction(){
		if(Validations.validateIsNullOrEmpty(searchReversalsTO.getAction())){
			searchReversalsTO.setBlReversals(false);
			searchReversalsTO.setBlSearchRequest(false);
			searchReversalsTO.setInitialDate(getCurrentSystemDate());
			clearForAction();
		}else if(ReversalsActionType.REVERSALS.getCode().equals(Integer.valueOf(searchReversalsTO.getAction()))){
			searchReversalsTO.setInitialDate(null);
			//searchReversalsTO.setBlDefaultSearch(true);
			searchReversalsTO.setBlReversals(true);		
			searchReversalsTO.setBlSearchRequest(false);
			clearForAction();
		}else if(ReversalsActionType.SEARCH_REQUEST.getCode().equals(Integer.valueOf(searchReversalsTO.getAction()))){
			searchReversalsTO.setInitialDate(getCurrentSystemDate());
			searchReversalsTO.setBlDefaultSearch(false);
			searchReversalsTO.setSearchType(GeneralConstants.ONE_VALUE_INTEGER);
			searchReversalsTO.setBlReversals(false);			
			searchReversalsTO.setBlSearchRequest(true);
			clearForAction();
		}
	}
	
	/**
	 * Clear for action.
	 */
	private void clearForAction(){
		searchReversalsTO.setHolder(new Holder());
		searchReversalsTO.setLstBlockOpeForReversals(null);
		searchReversalsTO.setBlockEntity(new BlockEntity());
		lstRequestReversals = null;
		blNoResult = false;
		requestReversalTO = null;
		JSFUtilities.resetComponent(":frmActionUnlock:helperRNT:helperRNT");
	}
	
	/**
	 * Validate amount unblock with market fact.
	 *
	 * @param objBlockOpeForRevTO the obj block ope for rev to
	 */
	public void validateAmountUnblockWithMarketFact(BlockOpeForReversalsTO objBlockOpeForRevTO){
		validateAmountUnblock(objBlockOpeForRevTO);
		/*if(objBlockOpeForRevTO.getAmountUnblock()!=null){
		   setMarketFactUI(objBlockOpeForRevTO);
		}*/
	}
	
	/**
	 * Validate amount unblock.
	 *
	 * @param objBlockOpeForRevTO the obj block ope for rev to
	 */
	public void validateAmountUnblock(BlockOpeForReversalsTO objBlockOpeForRevTO){		
		if(Validations.validateIsNotNullAndNotEmpty(objBlockOpeForRevTO.getAmountUnblock())){
			BigDecimal totalBlock = objBlockOpeForRevTO.getCurrentAmount();
			BigDecimal quantityUnblock = objBlockOpeForRevTO.getAmountUnblock();
			if(quantityUnblock.compareTo(totalBlock) == 1){
				clearQuantitiesUnblocks(objBlockOpeForRevTO);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
						PropertiesUtilities.getMessage("alt.reversal.tooMuchQuantity"));
				JSFUtilities.showSimpleValidationDialog();
			}	
			
		}else{
			clearQuantitiesUnblocks(objBlockOpeForRevTO);
		}
		
	}
	
	/**
	 * Clean reversal.
	 */
	public void cleanReversal(){
		JSFUtilities.resetViewRoot();
		init();
	}
	
	/**
	 * Clear quantities unblocks.
	 *
	 * @param objBlockOpeForRevTO the obj block ope for rev to
	 */
	public void clearQuantitiesUnblocks(BlockOpeForReversalsTO objBlockOpeForRevTO){
		objBlockOpeForRevTO.setAmountUnblock(null);
		if(objBlockOpeForRevTO.isBlCheck()){
			if (BigDecimal.ONE.equals(objBlockOpeForRevTO.getCurrentAmount())) {
				objBlockOpeForRevTO.setAmountUnblock(objBlockOpeForRevTO.getCurrentAmount());
				validateAmountUnblockWithMarketFact(objBlockOpeForRevTO);
			}
		}
	}
	
	/**
	 * Hide dialogs for reversal.
	 */
	public void hideDialogsForReversal(){
	}

	/**
	 * Ask for save reversal.
	 */
	public void askForSaveReversal(){
		try{
			
			// validando si se adjunto archivo de repaldo
			if(listFileReversalsTO == null || (listFileReversalsTO != null && listFileReversalsTO.size() == 0)){				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage("fupl.error.empty.reversal"));					
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
//			if(Validations.validateIsNullOrEmpty(validaFieldsForReversalRequest())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
						, PropertiesUtilities.getMessage("alt.reversal.askRegister"));
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
//			}
//			else{
//				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
//						, validaFieldsForReversalRequest());
//				JSFUtilities.showSimpleValidationDialog();
//			}
		}catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Save reversal request.
	 */
	@LoggerAuditWeb
	public void saveReversalRequest(){
		try{
			hideDialogsForReversal();
			Long strResult = requestReversalsServiceFacade.saveReversalRequest(registerReversalTO);
			if(Validations.validateIsNotNullAndNotEmpty(strResult)){
				Object[] bodyData = new Object[1];
				bodyData[0] = strResult.toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage("alt.reversal.register",bodyData));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
								
				// INICIO ENVIO NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_UNBLOCK_REGISTER.getCode());
				Object[] parameters = {	strResult.toString() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters );
				// FIN NOTIFICACIONES
				
				init();
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
		    showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));			
		    JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Valida fields for reversal request.
	 *
	 * @return the string
	 */
	private String validaFieldsForReversalRequest(){
		if(Validations.validateIsNullOrEmpty(registerReversalTO.getFileName1()))
			return PropertiesUtilities.getMessage("alt.reversal.needFile1");
		else{
			return GeneralConstants.EMPTY_STRING;
		}		
	}
	
	/**
	 * Creates the new reversal.
	 *
	 * @return the string
	 */
	public String createNewReversal(){
		if(Validations.validateListIsNotNullAndNotEmpty(searchReversalsTO.getLstBlockOpeForReversals())){
			boolean blHaveOne = false;
			boolean blEmptyUnblockAmounnt = false;
			for(BlockOpeForReversalsTO objBlockOpeForReve:searchReversalsTO.getLstBlockOpeForReversals()){
				if(objBlockOpeForReve.isBlCheck()){
					Object[] param= new Object[1];
					param[0] = objBlockOpeForReve.getOperationNumber().toString();
					//Issuer user just can unblock their own block operations
					if (!userInfo.getUserAccountSession().isDepositaryInstitution()) {
						if (requestAffectationServiceFacade.verifyBlockOperationRegisteredByEDV(objBlockOpeForReve.getIdBlockOperationPk())) {
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
												PropertiesUtilities.getMessage("alt.reversal.no.issuer.user.block", param));
							JSFUtilities.showSimpleValidationDialog();
							return null;
						}
					}
					try {
						requestAffectationServiceFacade.verifyBlockOperationAtOtherRequests(
														objBlockOpeForReve.getIdBlockOperationPk(), 
														objBlockOpeForReve.getOperationNumber().toString());
					} catch (ServiceException e) {
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
											PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));			
						JSFUtilities.showSimpleValidationDialog();
					    return null;
					}
					blHaveOne = true;
				}
			}
			for(BlockOpeForReversalsTO objBlockOpeForReve:searchReversalsTO.getLstBlockOpeForReversals()){
				if(Validations.validateIsNullOrEmpty(objBlockOpeForReve.getAmountUnblock()) && objBlockOpeForReve.isBlCheck()){
					blEmptyUnblockAmounnt = true;
					break;
				}
			}
			if(blHaveOne && !blEmptyUnblockAmounnt){
				registerReversalTO = new RegisterReversalTO();
				registerReversalTO.setIdParticipant(userInfo.getUserAccountSession().getParticipantCode());
				registerReversalTO.setParamTableMotiveUnblock(new ParameterTable());
				registerReversalTO.setLstMotiveUnblock(searchReversalsTO.getLstMotiveReversal());
				BigDecimal totalUnblock = BigDecimal.ZERO;
				List<BlockOpeForReversalsTO> lstTemp = new ArrayList<BlockOpeForReversalsTO>();
				for(BlockOpeForReversalsTO objBlockOpeForReve:searchReversalsTO.getLstBlockOpeForReversals()){
					if(objBlockOpeForReve.isBlCheck()){
						totalUnblock = totalUnblock.add(objBlockOpeForReve.getAmountUnblock());
						lstTemp.add(objBlockOpeForReve);
					}
				}
				supportSelected=null;
				listFileReversalsTO = new ArrayList<FileReversalsTO>();
				registerReversalTO.setTotalUnblock(totalUnblock);
				registerReversalTO.setLstBlockOpeForReversals(lstTemp);
				registerReversalTO.setIdHolderPk(searchReversalsTO.getHolder().getIdHolderPk());
				registerReversalTO.setHolderDescription(searchReversalsTO.getHolder().getDescriptionHolder());
				return STR_NEW_REVERSAL_MAPPING;
			}else{
				if(!blHaveOne){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
								,PropertiesUtilities.getMessage("alt.reversal.noSelectedUnblock"));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}else{
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
								, PropertiesUtilities.getMessage("alt.reversal.emptyQuantity"));
						JSFUtilities.showSimpleValidationDialog();
						return null;
					}
			}
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getMessage("alt.reversal.noSelectedUnblock"));
			JSFUtilities.showSimpleValidationDialog();
			return null;
		}
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		if (blApprove) {
			approveReversalRequest();
		} else if (blAnnul) {
			annulReversalRequest();
		} else if(blConfirm){
			confirmReversalRequest();
		}else if(blReject){
			rejectReversalRequest();
		}
	}
	
	/**
	 * Before show reject.
	 */
	public void beforeShowReject(){
		requestReversalTO.setMotiveReject(null);
		requestReversalTO.setMotiveRejectOther(null);
		JSFUtilities.resetComponent(":frmMotiveReject:dialogMotiveReject");
		JSFUtilities.showComponent(":frmMotiveReject:dialogMotiveReject");
	}
		
	/**
	 * Approve reversal request.
	 */
	public void approveReversalRequest(){	
		try{
			boolean blResult = requestReversalsServiceFacade.approveReversalRequest(requestReversalTO);
			if(blResult){
				Object[] bodyData = new Object[1];
				bodyData[0] = requestReversalTO.getIdUnblockRequestPk().toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_APPROVE, bodyData));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");				
				
				// INICIO ENVIO NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_UNBLOCK_APPROVE.getCode());
				Object[] parameters = {	requestReversalTO.getIdUnblockRequestPk().toString() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
				// FIN NOTIFICACIONES
				
				searchByAction();				
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
		}
	}
	
	/**
	 * Annul reversal request.
	 */
	public void annulReversalRequest(){	
		try{
			boolean blResult = requestReversalsServiceFacade.annulReversalRequest(requestReversalTO);
			if(blResult){
				Object[] bodyData = new Object[1];
				bodyData[0] = requestReversalTO.getIdUnblockRequestPk().toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_ANNUL, bodyData));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");				
				
				// INICIO ENVIO NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_UNBLOCK_ANNUL.getCode());
				Object[] parameters = {	requestReversalTO.getIdUnblockRequestPk().toString() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
				// FIN NOTIFICACIONES
				
				searchByAction();				
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
		}
	}
	
	/**
	 * Confirm reversal request.
	 */
	public void confirmReversalRequest(){	
		try{
			boolean blResult = requestReversalsServiceFacade.confirmReversalRequest(requestReversalTO,null);
			if(blResult){
				Object[] bodyData = new Object[1];
				bodyData[0] = requestReversalTO.getIdUnblockRequestPk().toString();				
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_CONFIRM, bodyData));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");				
				
				// INICIO ENVIO NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_UNBLOCK_CONFIRM.getCode());
//				Object[] parameters = {	requestReversalTO.getIdUnblockRequestPk().toString() };
//				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
//						businessProcess, null, parameters);
				sendNotificactionsEmail(requestReversalTO.getIdUnblockRequestPk(),businessProcess);
				// FIN NOTIFICACIONES
				searchByAction();				
			}
		}catch (Exception e) {
			log.info(e.getMessage());
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
						, PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(), se.getParams()));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			}			
		}
	}
	

	/**
	 * Reject reversal request.
	 */
	public void rejectReversalRequest(){
		try{
			boolean blResult = requestReversalsServiceFacade.rejectReversalRequest(requestReversalTO);
			if(blResult){
				Object[] bodyData = new Object[1];
				bodyData[0] = requestReversalTO.getIdUnblockRequestPk().toString();
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AFFECTATION_REJECT, bodyData));
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");				
				
				// INICIO ENVIO NOTIFICACIONES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.SECURITY_UNBLOCK_REJECT.getCode());
				Object[] parameters = {	requestReversalTO.getIdUnblockRequestPk().toString() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
				// FIN NOTIFICACIONES
				
				searchByAction();
			}
		}catch (ServiceException e) {
			log.info(e.getMessage());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
		}
	}
	
	/**
	 * Ask for back.
	 *
	 * @return the string
	 */
	public String askForBack(){
		if(validateRequiredFields()){
			return backFromRegister();
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					, PropertiesUtilities.getGenericMessage("conf.msg.back"));
			JSFUtilities.executeJavascriptFunction("cnfwBackDialog.show()");
		}
		return GeneralConstants.EMPTY_STRING;
	}
	
	/**
	 * Back from register.
	 *
	 * @return the string
	 */
	public String backFromRegister(){
		init();
		return "backFromNewReversal";
	}
	
	/**
	 * Validate required fields.
	 *
	 * @return true, if successful
	 */
	private boolean validateRequiredFields(){
		if(Validations.validateIsNullOrEmpty(registerReversalTO.getDocumentNumber())
				&& Validations.validateIsNullOrEmpty(registerReversalTO.getFileName1())
				&& Validations.validateIsNullOrEmpty(registerReversalTO.getHolderDescription())){
			return true;
		}
		return false;
	}
	
	/**
	 * Fill combos.
	 */
	private void fillCombos(){
		try{
			ParameterTableTO parameterTableTO = new ParameterTableTO();			
			//Lista de motivos de desbloqueo				
			parameterTableTO.setMasterTableFk(MasterTableType.MOTIVE_REVERSALS.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());			
			searchReversalsTO.setLstMotiveReversal(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));			
			//Lista de tipos de afectacion				
			parameterTableTO.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchReversalsTO.setLstAffectationType(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de niveles de afectacion						
			parameterTableTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchReversalsTO.setLstAffectationLevel(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista de los estados de las desafectaciones
			parameterTableTO.setMasterTableFk(MasterTableType.STATE_REVERSALS_REQUEST.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchReversalsTO.setLstRequestReversalState(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			//Lista motivo de rechazo
			parameterTableTO.setMasterTableFk(MasterTableType.MOTIVE_REJECT_REVERSALS.getCode());
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			searchReversalsTO.setLstMotiveReject(generalPametersFacade.getListParameterTableServiceBean(parameterTableTO));
			
			//reversal action list
			ParameterTable parameterTableSearching= new ParameterTable();
			parameterTableSearching.setParameterTablePk(ReversalsActionType.SEARCH_REQUEST.getCode());
			parameterTableSearching.setParameterName(PropertiesUtilities.getMessage(PropertiesConstants.REVERSAL_ACTION_SEARCHING));
			lstReversalAction.add(parameterTableSearching);
			if (userPrivilege.getUserAcctions().isRegister()) {
				ParameterTable parameterTableRegister= new ParameterTable();
				parameterTableRegister.setParameterTablePk(ReversalsActionType.REVERSALS.getCode());
				parameterTableRegister.setParameterName(PropertiesUtilities.getMessage(PropertiesConstants.REVERSAL_ACTION_REGISTER));
				lstReversalAction.add(parameterTableRegister);
			} else {
				searchReversalsTO.setAction(ReversalsActionType.SEARCH_REQUEST.getCode().toString());
				showFieldsForAction();
			}
		}
		catch (ServiceException e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the streamed content file.
	 *
	 * @param position the position
	 * @return the streamed content file
	 */
	public StreamedContent getStreamedContentFile(Integer position){
		StreamedContent streamedContent = null;
		
		if(position.equals(new Integer(1))){
			streamedContent = getStreamedContentFile1();
		}
		else if(position.equals(new Integer(2))){
			streamedContent = getStreamedContentFile2();
		}
		
		return streamedContent;
	}
	
	/**
	 * Gets the streamed content file1.
	 *
	 * @return the streamed content file1
	 */
	public StreamedContent getStreamedContentFile1(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(registerReversalTO.getDocumentFile1());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	registerReversalTO.getFileName1());
			inputStream.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContentFile;
	}
	
	/**
	 * Gets the streamed content file2.
	 *
	 * @return the streamed content file2
	 */
	public StreamedContent getStreamedContentFile2(){
		StreamedContent streamedContentFile = null;
		try {
			InputStream inputStream = null;
			inputStream = new ByteArrayInputStream(registerReversalTO.getDocumentFile2());
			streamedContentFile = new DefaultStreamedContent(inputStream, null,	registerReversalTO.getFileName2());
			inputStream.close();
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return streamedContentFile;
	}
	
	/**
	 * Document file handler.
	 *
	 * @param event the event
	 */
	public void documentFileHandler(FileUploadEvent event){
		if(supportSelected!=null && supportSelected.equals(new Integer(1))){
			documentFile1Handler(event);
		}
		else if (supportSelected!=null && supportSelected.equals(new Integer(2))){
			documentFile2Handler(event);
		}
	}
	
	/**
	 * Document file1 handler.
	 *
	 * @param event the event
	 */
	public void documentFile1Handler(FileUploadEvent event) {
		try {
			 String fDisplayName= fUploadValidateFile(event.getFile(),null, null,super.getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				registerReversalTO.setFileName1(fDisplayName);
				registerReversalTO.setDocumentFile1(event.getFile().getContents());
				
				FileReversalsTO fileReversalsTO = new FileReversalsTO();
				fileReversalsTO.setDocumentFile(registerReversalTO.getDocumentFile1());
				fileReversalsTO.setFileName(registerReversalTO.getFileName1());
				String extensionFile = registerReversalTO.getFileName1().toLowerCase().substring(registerReversalTO.getFileName1().lastIndexOf(".")+1,registerReversalTO.getFileName1().length());
				fileReversalsTO.setFileExtension(extensionFile.toUpperCase());
				fileReversalsTO.setFileSize(Integer.valueOf(registerReversalTO.getDocumentFile1().length).longValue()/1000L);
				fileReversalsTO.setPosition(new Integer(1));
				boolean exist=false;
				int position=0;				
			
				for(int i=0;i<listFileReversalsTO.size();i++){
					if(listFileReversalsTO.get(i).getPosition().equals(new Integer(1))){
						exist=true;
						position=i;
					}
				}
				if(exist){
					listFileReversalsTO.set(position,fileReversalsTO);
				}else{
					listFileReversalsTO.add(fileReversalsTO);	
				}			
				
			 }
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the uploaded file handler.
	 *
	 * @param position the position
	 */
	public void removeUploadedFileHandler(Integer position){
		if(position.equals(new Integer(1))){
			removeUploadedFile1Handler();
			int pos=0;				
		
			for(int i=0;i<listFileReversalsTO.size();i++){
				if(listFileReversalsTO.get(i).getPosition().equals(position)){
					pos=i;
				}
			}
			listFileReversalsTO.remove(pos);
		
		}
		else if(position.equals(new Integer(2))){
			removeUploadedFile2Handler();
			int pos=0;				
			for(int i=0;i<listFileReversalsTO.size();i++){
				if(listFileReversalsTO.get(i).getPosition().equals(position)){
					pos=i;
				}
			}
			listFileReversalsTO.remove(pos);
		}
	}
	
	/**
	 * Removes the uploaded file1 handler.
	 */
	public void removeUploadedFile1Handler(){
		try {
			registerReversalTO.setDocumentFile1(null); 
			registerReversalTO.setFileName1(null); 
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Document file2 handler.
	 *
	 * @param event the event
	 */
	public void documentFile2Handler(FileUploadEvent event) {
		try {
			 String fDisplayName= fUploadValidateFile(event.getFile(),null, null,super.getfUploadFileDocumentsTypes());			 
			 if(Validations.validateIsNotNullAndNotEmpty(fDisplayName)){
				registerReversalTO.setFileName2(fDisplayName);
				registerReversalTO.setDocumentFile2(event.getFile().getContents());
				
				FileReversalsTO fileReversalsTO = new FileReversalsTO();
				fileReversalsTO.setDocumentFile(registerReversalTO.getDocumentFile2());
				fileReversalsTO.setFileName(registerReversalTO.getFileName2());
				String extensionFile = registerReversalTO.getFileName2().toLowerCase().substring(registerReversalTO.getFileName2().lastIndexOf(".")+1,registerReversalTO.getFileName2().length());
				fileReversalsTO.setFileExtension(extensionFile.toUpperCase());
				fileReversalsTO.setFileSize(Integer.valueOf(registerReversalTO.getDocumentFile2().length).longValue()/1000L);
				fileReversalsTO.setPosition(new Integer(2));
				boolean exist=false;
				int position=0;				
			
				for(int i=0;i<listFileReversalsTO.size();i++){
					if(listFileReversalsTO.get(i).getPosition().equals(new Integer(2))){
						exist=true;
						position=i;
					}
				}
				if(exist){
					listFileReversalsTO.set(position,fileReversalsTO);
				}else{
					listFileReversalsTO.add(fileReversalsTO);	
				}
			 }
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Removes the uploaded file2 handler.
	 */
	public void removeUploadedFile2Handler(){
		try {
			registerReversalTO.setDocumentFile2(null); 
			registerReversalTO.setFileName2(null); 
		} catch (Exception e) {
			log.info(e.getMessage());
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Sets the market fact ui.
	 *
	 * @param blockReversalTO the new market fact ui
	 */
	public void setMarketFactUI(BlockOpeForReversalsTO blockReversalTO){
		
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(blockReversalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(blockReversalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(blockReversalTO.getIdParticipantPk());
		marketFactBalance.setIndBlockBalance(BooleanType.YES.getCode());
		marketFactBalance.setIdBlockOperationPk(blockReversalTO.getIdBlockOperationPk());
		marketFactBalance.setBlockOperationType(blockReversalTO.getIdBlockType());
		
		blockReversalTO.getAmountUnblock();
		
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());

		if(blockReversalTO.getReversalsMarkFactList()!=null && !blockReversalTO.getReversalsMarkFactList().isEmpty()){
			for(ReversalsMarkFactTO blockMarketFact: blockReversalTO.getReversalsMarkFactList()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(blockMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(blockReversalTO.getAmountUnblock());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}
		else{
			if(blockReversalTO.getAmountUnblock()!=null && !blockReversalTO.getAmountUnblock().equals(BigDecimal.ZERO)){
				marketFactBalance.setBalanceResult(blockReversalTO.getAmountUnblock());
			}
		}
		try {
			holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		selectMarketFact();
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param blockReversalTO the block reversal to
	 */
	public void showMarketFactUI(BlockOpeForReversalsTO blockReversalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(blockReversalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(blockReversalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(blockReversalTO.getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setIndBlockBalance(BooleanType.YES.getCode());
		marketFactBalance.setIdBlockOperationPk(blockReversalTO.getIdBlockOperationPk());
		marketFactBalance.setBlockOperationType(blockReversalTO.getIdBlockType());
		
		blockReversalTO.getAmountUnblock();
		
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());

		if(blockReversalTO.getReversalsMarkFactList()!=null && !blockReversalTO.getReversalsMarkFactList().isEmpty()){
			for(ReversalsMarkFactTO blockMarketFact: blockReversalTO.getReversalsMarkFactList()){
				MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
				marketDetail.setMarketFactBalancePk(blockMarketFact.getIdHolderMarketPk());
				marketDetail.setEnteredBalance(blockReversalTO.getAmountUnblock());
				marketFactBalance.getMarketFacBalances().add(marketDetail);
			}
		}
		else{
			if(blockReversalTO.getAmountUnblock()!=null && !blockReversalTO.getAmountUnblock().equals(BigDecimal.ZERO)){
				marketFactBalance.setBalanceResult(blockReversalTO.getAmountUnblock());
			}
		}
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance);
	}
	
	/**
	 * Select market fact.
	 */
	public void selectMarketFact(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		for(BlockOpeForReversalsTO blockReversalReq: this.searchReversalsTO.getLstBlockOpeForReversals()){
			if(blockReversalReq.getIdBlockOperationPk().equals(marketFactBalance.getIdBlockOperationPk()) &&
			   !marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
				
				blockReversalReq.setAmountUnblock(marketFactBalance.getBalanceResult());
				blockReversalReq.setReversalsMarkFactList(new ArrayList<ReversalsMarkFactTO>());
				
				for(MarketFactDetailHelpTO markFatDet: marketFactBalance.getMarketFacBalances()){
					ReversalsMarkFactTO reversalBalancDet = new ReversalsMarkFactTO();
					reversalBalancDet.setMarketDate(markFatDet.getMarketDate());
					reversalBalancDet.setMarketPrice(markFatDet.getMarketPrice());
					reversalBalancDet.setMarketRate(markFatDet.getMarketRate());
					reversalBalancDet.setIdHolderMarketPk(markFatDet.getMarketFactBalancePk());
					reversalBalancDet.setBlockAmount(markFatDet.getEnteredBalance());
					
					blockReversalReq.getReversalsMarkFactList().add(reversalBalancDet);
				}
				validateAmountUnblock(blockReversalReq);
				JSFUtilities.updateComponent("frmActionUnlock:outPutPanelResult");
				return;
			}
		}
	}

	/**
	 * Gets the search reversals to.
	 *
	 * @return the search reversals to
	 */
	public SearchReversalsTO getSearchReversalsTO() {
		return searchReversalsTO;
	}

	/**
	 * Sets the search reversals to.
	 *
	 * @param searchReversalsTO the new search reversals to
	 */
	public void setSearchReversalsTO(SearchReversalsTO searchReversalsTO) {
		this.searchReversalsTO = searchReversalsTO;
	}

	/**
	 * Gets the register reversal to.
	 *
	 * @return the register reversal to
	 */
	public RegisterReversalTO getRegisterReversalTO() {
		return registerReversalTO;
	}

	/**
	 * Sets the register reversal to.
	 *
	 * @param registerReversalTO the new register reversal to
	 */
	public void setRegisterReversalTO(RegisterReversalTO registerReversalTO) {
		this.registerReversalTO = registerReversalTO;
	}

	/**
	 * Gets the request reversal to.
	 *
	 * @return the request reversal to
	 */
	public RequestReversalsTO getRequestReversalTO() {
		return requestReversalTO;
	}

	/**
	 * Sets the request reversal to.
	 *
	 * @param requestReversalTO the new request reversal to
	 */
	public void setRequestReversalTO(RequestReversalsTO requestReversalTO) {
		this.requestReversalTO = requestReversalTO;
	}

	/**
	 * Gets the lst request reversals.
	 *
	 * @return the lst request reversals
	 */
	public RequestReversalsDataModel getLstRequestReversals() {
		return lstRequestReversals;
	}

	/**
	 * Sets the lst request reversals.
	 *
	 * @param lstRequestReversals the new lst request reversals
	 */
	public void setLstRequestReversals(RequestReversalsDataModel lstRequestReversals) {
		this.lstRequestReversals = lstRequestReversals;
	}

	/**
	 * Checks if is bl confirm.
	 *
	 * @return true, if is bl confirm
	 */
	public boolean isBlConfirm() {
		return blConfirm;
	}

	/**
	 * Sets the bl confirm.
	 *
	 * @param blConfirm the new bl confirm
	 */
	public void setBlConfirm(boolean blConfirm) {
		this.blConfirm = blConfirm;
	}

	/**
	 * Checks if is bl reject.
	 *
	 * @return true, if is bl reject
	 */
	public boolean isBlReject() {
		return blReject;
	}

	/**
	 * Sets the bl reject.
	 *
	 * @param blReject the new bl reject
	 */
	public void setBlReject(boolean blReject) {
		this.blReject = blReject;
	}

	/**
	 * Checks if is bl no result.
	 *
	 * @return true, if is bl no result
	 */
	public boolean isBlNoResult() {
		return blNoResult;
	}

	/**
	 * Sets the bl no result.
	 *
	 * @param blNoResult the new bl no result
	 */
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotiveReject:dialogMotiveReject");
		JSFUtilities.hideComponent(":frmMotiveReject:dialogMotiveReject");	
	}
	/*Notificacion para desbloqueo*/
	public void sendNotificactionsEmail(Long idUnblockRequestPk,BusinessProcess businessProcess){
		List<ResultSearchTO> searchTOs=new  ArrayList<>();
		try {
			List<UserAccountSession> accountSessions=new ArrayList<>();
			accountSessions=requestAffectationServiceFacade.getListUsersOfIssuerUnLock(idUnblockRequestPk);
			
			Integer idUserAccountPk=userInfo.getUserAccountSession().getIdUserAccountPk();
			//Verificamos si el usuario se encuentra dentro de la lista de los detinatarios
			for (UserAccountSession userAccountSession : accountSessions) {
				if(idUserAccountPk==userAccountSession.getIdUserAccountPk())
					return;
			}
			
			if(accountSessions.size()==0)
				return;
			searchTOs=requestAffectationServiceFacade.obtainDataForMessagesList(idUnblockRequestPk);
			ProcessNotification notification=requestAffectationServiceFacade.getProcessNotification(BusinessProcessType.SECURITY_UNBLOCK_CONFIRM.getCode());
			if(!Validations.validateIsNotNullAndNotEmpty(notification))
				return;
			String messageNotification=null;
			for (ResultSearchTO result : searchTOs) {
				Object[] parameters =new Object[4];
				parameters[0]=idUnblockRequestPk;
				parameters[1]=result.getmNemonicParticipant();
				parameters[2]=result.getAccountNumber();
				parameters[3]=result.getIdSecurityCodePk();
				String message=notification.getNotificationMessage();
				if(message.contains("%s")){
					messageNotification=String.format(message, parameters);
					//notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					//		businessProcess, null, parameters);
					
					notificationServiceFacade.sendNotificationManual(userInfo.getUserAccountSession().getUserName(), businessProcess, accountSessions,
							notification.getNotificationSubject(), messageNotification, NotificationType.EMAIL);
				}else{
					messageNotification=message;
					//notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
					//		businessProcess, null, parameters);
					
					notificationServiceFacade.sendNotificationManual(userInfo.getUserAccountSession().getUserName(), businessProcess, accountSessions,
							notification.getNotificationSubject(), messageNotification, NotificationType.EMAIL);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the list file reversals to.
	 *
	 * @return the list file reversals to
	 */
	public List<FileReversalsTO> getListFileReversalsTO() {
		return listFileReversalsTO;
	}

	/**
	 * Sets the list file reversals to.
	 *
	 * @param listFileReversalsTO the new list file reversals to
	 */
	public void setListFileReversalsTO(List<FileReversalsTO> listFileReversalsTO) {
		this.listFileReversalsTO = listFileReversalsTO;
	}

	/**
	 * Gets the support selected.
	 *
	 * @return the support selected
	 */
	public Integer getSupportSelected() {
		return supportSelected;
	}

	/**
	 * Sets the support selected.
	 *
	 * @param supportSelected the new support selected
	 */
	public void setSupportSelected(Integer supportSelected) {
		this.supportSelected = supportSelected;
	}

	/**
	 * Gets the list support name.
	 *
	 * @return the list support name
	 */
	public Map<String, Integer> getListSupportName() {
		return listSupportName;
	}

	/**
	 * Sets the list support name.
	 *
	 * @param listSupportName the list support name
	 */
	public void setListSupportName(Map<String, Integer> listSupportName) {
		this.listSupportName = listSupportName;
	}

	/**
	 * Checks if is bl approve.
	 *
	 * @return true, if is bl approve
	 */
	public boolean isBlApprove() {
		return blApprove;
	}

	/**
	 * Sets the bl approve.
	 *
	 * @param blApprove the new bl approve
	 */
	public void setBlApprove(boolean blApprove) {
		this.blApprove = blApprove;
	}

	/**
	 * Checks if is bl annul.
	 *
	 * @return true, if is bl annul
	 */
	public boolean isBlAnnul() {
		return blAnnul;
	}

	/**
	 * Sets the bl annul.
	 *
	 * @param blAnnul the new bl annul
	 */
	public void setBlAnnul(boolean blAnnul) {
		this.blAnnul = blAnnul;
	}

	/**
	 * Checks if is bl view detail.
	 *
	 * @return true, if is bl view detail
	 */
	public boolean isBlViewDetail() {
		return blViewDetail;
	}

	/**
	 * Sets the bl view detail.
	 *
	 * @param blViewDetail the new bl view detail
	 */
	public void setBlViewDetail(boolean blViewDetail) {
		this.blViewDetail = blViewDetail;
	}

	/**
	 * Gets the lst reversal action.
	 *
	 * @return the lst reversal action
	 */
	public List<ParameterTable> getLstReversalAction() {
		return lstReversalAction;
	}

	/**
	 * Sets the lst reversal action.
	 *
	 * @param lstReversalAction the new lst reversal action
	 */
	public void setLstReversalAction(List<ParameterTable> lstReversalAction) {
		this.lstReversalAction = lstReversalAction;
	}

	/**
	 * Checks if is flag issuer block.
	 *
	 * @return true, if is flag issuer block
	 */
	public boolean isFlagIssuerBlock() {
		return flagIssuerBlock;
	}

	/**
	 * Sets the flag issuer block.
	 *
	 * @param flagIssuerBlock the new flag issuer block
	 */
	public void setFlagIssuerBlock(boolean flagIssuerBlock) {
		this.flagIssuerBlock = flagIssuerBlock;
	}

	public Integer getIdReversals() {
		return idReversals;
	}

	public void setIdReversals(Integer idReversals) {
		this.idReversals = idReversals;
	}

	
	
	
}