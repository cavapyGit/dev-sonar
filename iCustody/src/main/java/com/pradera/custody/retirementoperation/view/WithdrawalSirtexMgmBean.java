package com.pradera.custody.retirementoperation.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.custody.retirementoperation.facade.RetirementOperationServiceFacade;
import com.pradera.custody.retirementoperation.to.RegisterWithdrawalSiterxTO;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementMarketfact;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class WithdrawalSirtexMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class WithdrawalSirtexMgmBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The obj register withdrawal siterx. */
	private RegisterWithdrawalSiterxTO objRegisterWithdrawalSiterx;
	
	/** The obj filter withdrawal sirtex. */
	private RetirementOperationTO objFilterWithdrawalSirtex;
	
	/** The lst states. */
	private List<ParameterTable> lstStates = new ArrayList<ParameterTable>();
	
	/** The lst bcb securitie class. */
	private List<ParameterTable> lstBCBSecuritieClass = new ArrayList<ParameterTable>();
	
	/** The lst issuer. */
	private List<Issuer> lstIssuer;
	
	/** The lst withdrawal sirtex. */
	private GenericDataModel<RetirementOperationTO> lstWithdrawalSirtex;
	
	/** The lst selected withdrawal sirtex. */
	private List<RetirementOperationTO> lstSelectedWithdrawalSirtex;
	
	/** The map parameters. */
	private HashMap<Integer, String> mapParameters = new HashMap<Integer,String>();
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The withdrawal market fact balance. */
	private MarketFactBalanceHelpTO withdrawalMarketFactBalance;
	
	/** The help market fact balance view. */
	private MarketFactBalanceHelpTO helpMarketFactBalanceView;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The id participant bc. */
	@Inject @Configurable String idParticipantBC;
	
	/** The retirement operation service facade. */
	@EJB
	RetirementOperationServiceFacade retirementOperationServiceFacade;
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The Constant REGISTER_VIEW_MAPPING. */
	private static final String REGISTER_VIEW_MAPPING = "registerWithdrawalSirtex";
	
	/** The Constant DETAIL_VIEW_MAPPING. */
	private static final String DETAIL_VIEW_MAPPING = "viewWithdrawalSirtex";
	
	/** The Constant ISSUER_BCB. */
	private static final String ISSUER_BCB = "BCB";
	
	/** The Constant ISSUER_TGN. */
	private static final String ISSUER_TGN = "TGN";
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init()
	{
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			objFilterWithdrawalSirtex= new RetirementOperationTO();
			List<String> mnemonics= new ArrayList<String>();
			mnemonics.add(ISSUER_BCB);
			mnemonics.add(ISSUER_TGN);
			lstIssuer= retirementOperationServiceFacade.getListIssuerByMnemonic(mnemonics);
			objFilterWithdrawalSirtex.setIndWithdrawalBCB(BooleanType.YES.getCode());
			loadParameters();
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParameters() throws ServiceException{
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setLstMasterTableFk(new ArrayList<Integer>());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_MOTIVE_STATE.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.SECURITIES_CLASS.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.ACCOUNTS_TYPE.getCode());
		
		List<ParameterTable> lstState = generalPametersFacade.getListParameterTableServiceBean(parameterFilter);
		for (ParameterTable parameterTable : lstState) 
		{
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.RETIREMENT_MOTIVE_STATE.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState())){
					if (!RetirementOperationStateType.ANNULLED.getCode().equals(parameterTable.getParameterTablePk()) &&
						!RetirementOperationStateType.APPROVED.getCode().equals(parameterTable.getParameterTablePk())) 
					{
						lstStates.add(parameterTable);
					}
				}
			}
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.SECURITIES_CLASS.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState())){
					if (Validations.validateIsNotNull(parameterTable.getText3())) {//BCB class code
						parameterTable.setDescription(parameterTable.getText3() + GeneralConstants.HYPHEN + parameterTable.getDescription());
						lstBCBSecuritieClass.add(parameterTable);
					}
				}
			}
			mapParameters.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){
		PrivilegeComponent privilegeComp = new PrivilegeComponent();
		privilegeComp.setBtnConfirmView(true);
		privilegeComp.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	
	/**
	 * New request.
	 *
	 * @return the string
	 */
	public String newRequest(){
		try {
			objRegisterWithdrawalSiterx= new RegisterWithdrawalSiterxTO();
			objRegisterWithdrawalSiterx.setLstHolderAccountBalance(null);
			Participant objParticipant= retirementOperationServiceFacade.findParticipant(new Long(idParticipantBC));
			objRegisterWithdrawalSiterx.setParticipant(objParticipant);
			objRegisterWithdrawalSiterx.getHolder().setIdHolderPk(objParticipant.getHolder().getIdHolderPk());
			
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
			holderAccountTO.setIdHolderPk(objParticipant.getHolder().getIdHolderPk());
			holderAccountTO.setNeedHolder(Boolean.TRUE);
			holderAccountTO.setNeedParticipant(Boolean.FALSE);
			holderAccountTO.setNeedBanks(Boolean.FALSE);
			List<HolderAccount> lstHolderAccount = retirementOperationServiceFacade.getHolderAccounts(holderAccountTO);
			if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccount)) {
				objRegisterWithdrawalSiterx.setLstHolderAccount(lstHolderAccount);
				if (lstHolderAccount.size() == GeneralConstants.ONE_VALUE_INTEGER.intValue()) {
					objRegisterWithdrawalSiterx.setHolderAccount(lstHolderAccount.get(0));
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.ALERT_BCB_HOLDER_ACCOUNT_NOT_EXISTS));
				JSFUtilities.showSimpleValidationDialog();
				return GeneralConstants.EMPTY_STRING;
			}
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, e.getMessage());
			JSFUtilities.showSimpleValidationDialog();
		}
		
		return REGISTER_VIEW_MAPPING;
	}

	/**
	 * Search secuties sirtex.
	 */
	public void searchSecutiesSirtex() {
		objRegisterWithdrawalSiterx.setLstHolderAccountBalance(null);
		List<TransferAccountBalanceTO> lstHolderAccountBalanceTO= retirementOperationServiceFacade.getListHolderAccountBalance(
																					objRegisterWithdrawalSiterx, lstIssuer, getParametersTableMap());
		objRegisterWithdrawalSiterx.setLstHolderAccountBalance(new  GenericDataModel<>(lstHolderAccountBalanceTO));
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void showMarketFactUI(TransferAccountBalanceTO objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBalTO.getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
		
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		
		for(TransferAccountBalanceTO hab : objRegisterWithdrawalSiterx.getLstHolderAccountBalance()){
			if(hab.getIdSecurityCodePk().equals( withdrawalMarketFactBalance.getSecurityCodePk() ) &&
			   hab.getIdHolderAccountPk().equals( withdrawalMarketFactBalance.getHolderAccountPk() ))
			{
				hab.setMarketFactDetails(new ArrayList<MarketFactTO>());
				hab.setQuantityRemoveBalance(BigDecimal.ZERO);
				for(MarketFactDetailHelpTO detail : withdrawalMarketFactBalance.getMarketFacBalances() ){
					if(detail.getIsSelected() && detail.getEnteredBalance()!=null && 
					   detail.getEnteredBalance().compareTo(BigDecimal.ZERO) > 0)
					{
						MarketFactTO marketFactDetail = new MarketFactTO();					
						marketFactDetail.setMarketDate(detail.getMarketDate());
						marketFactDetail.setMarketPrice(detail.getMarketPrice());
						marketFactDetail.setMarketRate(detail.getMarketRate());
						marketFactDetail.setQuantity(detail.getEnteredBalance());
						hab.setQuantityRemoveBalance(hab.getQuantityRemoveBalance().add(marketFactDetail.getQuantity()));
						hab.getMarketFactDetails().add(marketFactDetail);
					}
				}
			}
		}
		JSFUtilities.updateComponent("frmRegWithdrawalSirtex:dtbResult");
	}
	
	/**
	 * Update holder account withdrawal.
	 *
	 * @param objTransferAccountBalanceTO the obj transfer account balance to
	 */
	public void updateHolderAccountWithdrawal(TransferAccountBalanceTO objTransferAccountBalanceTO) {
		if (Validations.validateIsNotNull(objTransferAccountBalanceTO)) {
			if (objTransferAccountBalanceTO.isBlCheck()) {
				objTransferAccountBalanceTO.setBlCheck(true);
			} else {
				objTransferAccountBalanceTO.setQuantityRemoveBalance(BigDecimal.ZERO);
				objTransferAccountBalanceTO.setMarketFactDetails(new ArrayList<MarketFactTO>());
				objTransferAccountBalanceTO.setBlCheck(false);
			}
		}
	}
	
	
	/**
	 * Before save.
	 */
	public void beforeSave() {
		boolean hasSelected= false; //flag to knows if we selected any holder account to withdrawal
		for (TransferAccountBalanceTO objTransferAccountBalanceTO: objRegisterWithdrawalSiterx.getLstHolderAccountBalance()) {
			if (objTransferAccountBalanceTO.isBlCheck()) {
				if (objTransferAccountBalanceTO.getQuantityRemoveBalance() != null && 
					objTransferAccountBalanceTO.getQuantityRemoveBalance().compareTo(BigDecimal.ZERO) > 0) {
					hasSelected= true;
				} else {
					Object[] param= new Object[1];
					param[0] = objTransferAccountBalanceTO.getIdSecurityCodePk();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WITHDRAWAL_SIRTEX_REMOVE_QUANTITY_ZERO, param));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
		}
		if (!hasSelected) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WITHDRAWAL_SIRTEX_SECURITIES_NO_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_REGISTER_ASK));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Save withdrawal sirtex.
	 */
	@LoggerAuditWeb
	public void saveWithdrawalSirtex() {
		try {
			Object[] param= new Object[1];
			param[0] = retirementOperationServiceFacade.saveWithdrawalSirtexOperation(objRegisterWithdrawalSiterx);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_REGISTER_SUCCESS,param));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (ServiceException e) {
			if(e.getMessage()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	/**
	 * Clean registration.
	 */
	public void cleanRegistration() {
		JSFUtilities.resetViewRoot();
		newRequest();
	}
	
	
	/**
	 * Search request.
	 */
	public void SearchRequest() {
		showPrivilegeButtons();
		try {
			/* Setting on 1 when it's  sirtex's operation*/ 			
			objFilterWithdrawalSirtex.setIndWithdrawalBCB(1);
			List<RetirementOperationTO> lstRetirementOperationTO = retirementOperationServiceFacade.getListSirtexWithdrawalOperation(objFilterWithdrawalSirtex);
			lstWithdrawalSirtex= new GenericDataModel<>(lstRetirementOperationTO);
			lstSelectedWithdrawalSirtex= null;
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getExceptionMessage(e.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Clean searching.
	 */
	public void cleanSearching() {
		objFilterWithdrawalSirtex= new RetirementOperationTO();
		lstWithdrawalSirtex= null;
		lstSelectedWithdrawalSirtex= null;
	}
	
	/**
	 * Load sirtex withdrawal operation.
	 *
	 * @param objRetirementOperationTO the obj retirement operation to
	 * @return the string
	 */
	public String loadSirtexWithdrawalOperation(RetirementOperationTO objRetirementOperationTO) {
		try {
			objRegisterWithdrawalSiterx= new RegisterWithdrawalSiterxTO();
			objRegisterWithdrawalSiterx.setLstHolderAccountBalance(null);
			Participant objParticipant= retirementOperationServiceFacade.findParticipant(new Long(idParticipantBC));
			objRegisterWithdrawalSiterx.setParticipant(objParticipant);
			objRegisterWithdrawalSiterx.getHolder().setIdHolderPk(objParticipant.getHolder().getIdHolderPk());
			RetirementOperation sirtexWithdrawalOperation= retirementOperationServiceFacade.findRetirmentOperation(
																			objRetirementOperationTO.getIdRetirementOperationPk());
			objRegisterWithdrawalSiterx.setSirtexWithdrawalOperation(sirtexWithdrawalOperation);
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getExceptionMessage(e.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
		return DETAIL_VIEW_MAPPING;
	}
	
	/**
	 * Show market fact ui detail.
	 *
	 * @param retirementDetail the retirement detail
	 */
	public void showMarketFactUIDetail(RetirementDetail retirementDetail){
		helpMarketFactBalanceView = new MarketFactBalanceHelpTO();

		helpMarketFactBalanceView.setSecurityCodePk(objRegisterWithdrawalSiterx.getSirtexWithdrawalOperation().getSecurity().getIdSecurityCodePk());
		helpMarketFactBalanceView.setHolderAccountPk(retirementDetail.getHolderAccount().getIdHolderAccountPk());
		helpMarketFactBalanceView.setParticipantPk(retirementDetail.getParticipant().getIdParticipantPk());
		helpMarketFactBalanceView.setSecurityDescription(objRegisterWithdrawalSiterx.getSirtexWithdrawalOperation().getSecurity().getDescription());
		helpMarketFactBalanceView.setInstrumentType(objRegisterWithdrawalSiterx.getSirtexWithdrawalOperation().getSecurity().getInstrumentType());
		helpMarketFactBalanceView.setInstrumentDescription((String) getParametersTableMap().get(helpMarketFactBalanceView.getInstrumentType()));
		helpMarketFactBalanceView.setBalanceResult(BigDecimal.ZERO);
		helpMarketFactBalanceView.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		helpMarketFactBalanceView.setIndHandleDetails(ComponentConstant.ONE);

		List<RetirementMarketfact> lstMarketFact = retirementOperationServiceFacade.findRetirementMarketFactByRetirement(retirementDetail.getIdRetirementDetailPk());
		if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFact)){
			for (RetirementMarketfact marketFact : lstMarketFact) {
				MarketFactDetailHelpTO marketFactBalanceHelpTO = new MarketFactDetailHelpTO();
				marketFactBalanceHelpTO.setTotalBalance(marketFact.getOperationQuantity());
				marketFactBalanceHelpTO.setMarketDate(marketFact.getMarketDate());
				marketFactBalanceHelpTO.setMarketPrice(marketFact.getMarketPrice());
				marketFactBalanceHelpTO.setMarketRate(marketFact.getMarketRate());
				helpMarketFactBalanceView.getMarketFacBalances().add(marketFactBalanceHelpTO);
				helpMarketFactBalanceView.setBalanceResult(helpMarketFactBalanceView.getBalanceResult().add(marketFactBalanceHelpTO.getTotalBalance()));
			}
		}
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	public String validateConfirm() {
		return validateAction(ViewOperationsType.CONFIRM.getCode(), PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_CONFIRM_ASK);
	}
	
	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	public String validateReject() {
		return validateAction(ViewOperationsType.REJECT.getCode(), PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_REJECT_ASK);
	}
	
	/**
	 * Validate action.
	 *
	 * @param action the action
	 * @param strMessage the str message
	 * @return the string
	 */
	public String validateAction(Integer action, String strMessage) {
		if (Validations.validateListIsNotNullAndNotEmpty(lstSelectedWithdrawalSirtex)) {
			String strOperationNumber= GeneralConstants.EMPTY_STRING;
			for(RetirementOperationTO objRetirementOperationTO: lstSelectedWithdrawalSirtex) {
				if (!RetirementOperationStateType.REGISTERED.getCode().equals(objRetirementOperationTO.getState())) {
					Object[] param= new Object[1];
					param[0] = objRetirementOperationTO.getOperationNumber();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
										PropertiesUtilities.getMessage(PropertiesConstants.ALERT_WITHDRAWAL_SIRTEX_REQUEST_NO_REGISTERED, param));
					JSFUtilities.showSimpleValidationDialog();
					return null;
				} else {
					if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperationNumber)) {
						strOperationNumber += objRetirementOperationTO.getOperationNumber();
					} else {
						strOperationNumber += GeneralConstants.STR_COMMA + objRetirementOperationTO.getOperationNumber();
					}
				}
			}
			if (GeneralConstants.ONE_VALUE_INTEGER.intValue() == lstSelectedWithdrawalSirtex.size()) {
				loadSirtexWithdrawalOperation(lstSelectedWithdrawalSirtex.get(0));
				setViewOperationType(action);
				return DETAIL_VIEW_MAPPING;
			} else {
				Object[] param= new Object[1];
				param[0] = strOperationNumber;
				//JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
									PropertiesUtilities.getMessage(strMessage, param));
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
				//JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
			}
			setViewOperationType(action);
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
		
		return null;
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		String headerMessage = null;
		String bodyMessage = null;
		if(isViewOperationConfirm()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			bodyMessage = PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_CONFIRM_ASK;
		}else if(isViewOperationReject()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REJECT;
			bodyMessage = PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_REJECT_ASK;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage)
				, PropertiesUtilities.getMessage(bodyMessage, new Object[]{
						objRegisterWithdrawalSiterx.getSirtexWithdrawalOperation().getCustodyOperation().getOperationNumber().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try {
			String bodyMessage= null;
			Object[] param= new Object[1];
			if(getViewOperationType().equals(ViewOperationsType.CONFIRM.getCode())){
				param[0]= retirementOperationServiceFacade.confirmSirtexWithdrawalOperation(lstSelectedWithdrawalSirtex);
				bodyMessage= PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_CONFIRM_SUCCESS;
			} else if(getViewOperationType().equals(ViewOperationsType.REJECT.getCode())){
				param[0]= retirementOperationServiceFacade.rejectSirtexWithdrawalOperation(lstSelectedWithdrawalSirtex);
				bodyMessage= PropertiesConstants.MSG_WITHDRAWAL_SIRTEX_REJECT_SUCCESS;
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
																	PropertiesUtilities.getMessage(bodyMessage, param));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			SearchRequest();
			objRegisterWithdrawalSiterx= null;
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
												, PropertiesUtilities.getExceptionMessage(e.getMessage()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Show security financial data.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialData(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);
	}
	
	/**
	 * Validate amount in sirtex redemption.
	 *
	 * @param accountBalanceTO the account balance to
	 */
	public void validateAmountInSirtexRedemption(TransferAccountBalanceTO accountBalanceTO){
		BigDecimal quantityToRemove = accountBalanceTO.getQuantityRemoveBalance();
		BigDecimal availableBalance = accountBalanceTO.getHolderAccountBalance().getAvailableBalance();
		
		if(quantityToRemove.compareTo(availableBalance)>0 || quantityToRemove.equals(BigDecimal.ZERO)){
			accountBalanceTO.setQuantityRemoveBalance(null);
		}else{
			for(MarketFactTO marketFactTO: accountBalanceTO.getMarketFactDetails()){
				marketFactTO.setQuantity(quantityToRemove);
			}
		}
	}
	
	/**
	 * Gets the obj register withdrawal siterx.
	 *
	 * @return the obj register withdrawal siterx
	 */
	public RegisterWithdrawalSiterxTO getObjRegisterWithdrawalSiterx() {
		return objRegisterWithdrawalSiterx;
	}

	/**
	 * Sets the obj register withdrawal siterx.
	 *
	 * @param objRegisterWithdrawalSiterx the new obj register withdrawal siterx
	 */
	public void setObjRegisterWithdrawalSiterx(
			RegisterWithdrawalSiterxTO objRegisterWithdrawalSiterx) {
		this.objRegisterWithdrawalSiterx = objRegisterWithdrawalSiterx;
	}

	/**
	 * Gets the obj filter withdrawal sirtex.
	 *
	 * @return the obj filter withdrawal sirtex
	 */
	public RetirementOperationTO getObjFilterWithdrawalSirtex() {
		return objFilterWithdrawalSirtex;
	}

	/**
	 * Sets the obj filter withdrawal sirtex.
	 *
	 * @param objFilterWithdrawalSirtex the new obj filter withdrawal sirtex
	 */
	public void setObjFilterWithdrawalSirtex(
			RetirementOperationTO objFilterWithdrawalSirtex) {
		this.objFilterWithdrawalSirtex = objFilterWithdrawalSirtex;
	}

	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	/**
	 * Gets the lst bcb securitie class.
	 *
	 * @return the lst bcb securitie class
	 */
	public List<ParameterTable> getLstBCBSecuritieClass() {
		return lstBCBSecuritieClass;
	}

	/**
	 * Sets the lst bcb securitie class.
	 *
	 * @param lstBCBSecuritieClass the new lst bcb securitie class
	 */
	public void setLstBCBSecuritieClass(List<ParameterTable> lstBCBSecuritieClass) {
		this.lstBCBSecuritieClass = lstBCBSecuritieClass;
	}

	/**
	 * Gets the lst withdrawal sirtex.
	 *
	 * @return the lst withdrawal sirtex
	 */
	public GenericDataModel<RetirementOperationTO> getLstWithdrawalSirtex() {
		return lstWithdrawalSirtex;
	}

	/**
	 * Sets the lst withdrawal sirtex.
	 *
	 * @param lstWithdrawalSirtex the new lst withdrawal sirtex
	 */
	public void setLstWithdrawalSirtex(
			GenericDataModel<RetirementOperationTO> lstWithdrawalSirtex) {
		this.lstWithdrawalSirtex = lstWithdrawalSirtex;
	}

	/**
	 * Gets the lst selected withdrawal sirtex.
	 *
	 * @return the lst selected withdrawal sirtex
	 */
	public List<RetirementOperationTO> getLstSelectedWithdrawalSirtex() {
		return lstSelectedWithdrawalSirtex;
	}

	/**
	 * Sets the lst selected withdrawal sirtex.
	 *
	 * @param lstSelectedWithdrawalSirtex the new lst selected withdrawal sirtex
	 */
	public void setLstSelectedWithdrawalSirtex(
			List<RetirementOperationTO> lstSelectedWithdrawalSirtex) {
		this.lstSelectedWithdrawalSirtex = lstSelectedWithdrawalSirtex;
	}

	/**
	 * Gets the map parameters.
	 *
	 * @return the map parameters
	 */
	public HashMap<Integer, String> getMapParameters() {
		return mapParameters;
	}

	/**
	 * Sets the map parameters.
	 *
	 * @param mapParameters the map parameters
	 */
	public void setMapParameters(HashMap<Integer, String> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * Gets the withdrawal market fact balance.
	 *
	 * @return the withdrawal market fact balance
	 */
	public MarketFactBalanceHelpTO getWithdrawalMarketFactBalance() {
		return withdrawalMarketFactBalance;
	}

	/**
	 * Sets the withdrawal market fact balance.
	 *
	 * @param withdrawalMarketFactBalance the new withdrawal market fact balance
	 */
	public void setWithdrawalMarketFactBalance(
			MarketFactBalanceHelpTO withdrawalMarketFactBalance) {
		this.withdrawalMarketFactBalance = withdrawalMarketFactBalance;
	}

	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}

	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	/**
	 * Gets the help market fact balance view.
	 *
	 * @return the help market fact balance view
	 */
	public MarketFactBalanceHelpTO getHelpMarketFactBalanceView() {
		return helpMarketFactBalanceView;
	}

	/**
	 * Sets the help market fact balance view.
	 *
	 * @param helpMarketFactBalanceView the new help market fact balance view
	 */
	public void setHelpMarketFactBalanceView(
			MarketFactBalanceHelpTO helpMarketFactBalanceView) {
		this.helpMarketFactBalanceView = helpMarketFactBalanceView;
	}
}
