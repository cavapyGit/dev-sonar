package com.pradera.custody.retirementoperation.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.Column;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.service.IssuerQueryServiceBean;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.custody.accreditationcertificates.facade.AccreditationCertificatesServiceFacade;
import com.pradera.custody.accreditationcertificates.service.AccreditationCertificatesServiceBean;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.custody.retirementoperation.to.RegisterWithdrawalSiterxTO;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.accounts.service.AccountsRemoteService;
import com.pradera.integration.component.accounts.to.HolderAccountObjectTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.service.SecuritiesComponentService;
import com.pradera.integration.component.business.service.VaultControlService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.component.business.to.SecuritiesComponentTO;
import com.pradera.integration.component.custody.to.ReverseRegisterTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentQueryTO;
import com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.CorporativeProcessResult;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementMarketfact;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationMotive;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationRedemptionType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.RetirementStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleSituationType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementOperationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
public class RetirementOperationServiceBean extends CrudDaoServiceBean {
	
	/** The Constant logger. */
	private static final  Logger logger = LoggerFactory.getLogger(RetirementOperationServiceBean.class);

	/** The accounts remote service bean. */
	@Inject
    private Instance<AccountsRemoteService> accountsRemoteServiceBean;
	
	/** The executor component service bean. */
	@Inject
    private Instance<AccountsComponentService> accountsComponentService;
	
	/** The securities component service. */
	@Inject
    private Instance<SecuritiesComponentService> securitiesComponentService;
	
	@Inject
	private AccreditationCertificatesServiceBean beanService;
	
	/** The idepositary setup. */
	@Inject
	@DepositarySetup
    IdepositarySetup idepositarySetup;
	
	/** The request reversals service bean. */
	@EJB
	private RequestReversalsServiceBean requestReversalsServiceBean;
	
	/** The affectations service bean. */
	@EJB
	private AffectationsServiceBean affectationsServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;	
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	@EJB
	private SecuritiesQueryServiceBean securitiesQueryService;
	
	/** The issuer query service. */
	/***/
	@EJB
	private IssuerQueryServiceBean issuerQueryService;
	
	/** The holder balance movements service bean. */
	@EJB
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;
	
	/** The accreditation facade bean. */	
	@Inject
	private AccreditationCertificatesServiceFacade accreditationfacadeBean;
	 
	@Inject
	Instance<VaultControlService> vaultControlService;
	
	/** The id participant bc. */
	@Inject @Configurable Long idParticipantBC;
	
	/** The id participant vun. */
	@Inject @Configurable Long idParticipantVUN;
	
	/**
	 * Search holders.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountDetail> searchHolders(Long idHolderAccountPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("  SELECT " );	
		sbQuery.append("         HAD.holder.idHolderPk," );
		sbQuery.append("         HAD.holder.fullName" );	
		sbQuery.append("   FROM  HolderAccountDetail HAD");
		sbQuery.append("  WHERE  HAD.holderAccount.idHolderAccountPk=:idHolderAccountPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		
        List<Object> objects =(List<Object>) query.getResultList();	
		List<HolderAccountDetail> holderAccountDetails = new ArrayList<HolderAccountDetail>();
		for(int i=0;i<objects.size();++i){
			Object [] object = (Object[]) objects.get(i);			
			HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
			Holder holder = new Holder();
			
			holder.setIdHolderPk((Long)object[0]);
			holder.setName((String)object[1]);
			
			holderAccountDetail.setHolder(holder);
			holderAccountDetails.add(holderAccountDetail);
		}
		return holderAccountDetails;
	}
	
	public List<RetirementOperation> searchRetirementOperationNoInExpirationCupon(Date dateToCancel) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT ro ");
		sbQuery.append(" FROM RetirementOperation ro ");
		sbQuery.append(" JOIN FETCH ro.custodyOperation co ");
		sbQuery.append(" WHERE trunc(co.registryDate) <= :endDate");
		sbQuery.append(" AND ro.state in (:lstInState) ");
		sbQuery.append(" AND ro.motive not in (:motive) ");
			
		List<Integer> lstInState = new ArrayList<Integer>();
		lstInState.add(RetirementOperationStateType.REGISTERED.getCode());
		lstInState.add(RetirementOperationStateType.APPROVED.getCode());
		
		TypedQuery<RetirementOperation> query = em.createQuery(sbQuery.toString(), RetirementOperation.class);
		query.setParameter("endDate", dateToCancel);
		query.setParameter("lstInState", lstInState);
		query.setParameter("motive", RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode());
		
		return query.getResultList();
	}
	

	public List<RetirementOperation> searchRetirementOperationWithoutCorporative(Date dateToCancel) {
		List<RetirementOperation> lstRetirementOperations = new ArrayList<RetirementOperation>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT ro ");
		sbQuery.append(" FROM RetirementOperation ro ");
		sbQuery.append(" JOIN FETCH ro.custodyOperation co ");
		sbQuery.append(" WHERE trunc(co.registryDate) < :endDate");
		sbQuery.append(" AND ro.state in (:lstInState) ");
		sbQuery.append(" AND ro.motive not in (:motive) ");
			
		List<Integer> lstInState = new ArrayList<Integer>();
		lstInState.add(RetirementOperationStateType.REGISTERED.getCode());
		lstInState.add(RetirementOperationStateType.APPROVED.getCode());
		
		TypedQuery<RetirementOperation> query = em.createQuery(sbQuery.toString(), RetirementOperation.class);
		query.setParameter("endDate", dateToCancel);
		query.setParameter("lstInState", lstInState);
		query.setParameter("motive", RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode());
		
		lstRetirementOperations = query.getResultList();
		if(lstRetirementOperations != null && lstRetirementOperations.size() > 0) {
			for(RetirementOperation ro : lstRetirementOperations) {
				PayrollHeader ph = getPayrollHeaderAndDetailEnabled(ro.getIdRetirementOperationPk());
				ro.setPayrollHeader(ph);
			}
		}
		
		
		return lstRetirementOperations;
	}
	
	public List<RetirementOperation> searchRetirementOperationWithCorporative(Date dateToCancel) {
		List<RetirementOperation> lstRetirementOperations = new ArrayList<RetirementOperation>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT ro ");
		sbQuery.append(" FROM RetirementOperation ro ");
		sbQuery.append(" JOIN FETCH ro.custodyOperation co ");
		sbQuery.append(" WHERE trunc(co.registryDate) < :endDate");
		sbQuery.append(" AND ro.state in (:lstInState) ");
		sbQuery.append(" AND ro.motive in (:motive) ");
			
		List<Integer> lstInState = new ArrayList<Integer>();
		lstInState.add(RetirementOperationStateType.REGISTERED.getCode());
		lstInState.add(RetirementOperationStateType.APPROVED.getCode());
		
		TypedQuery<RetirementOperation> query = em.createQuery(sbQuery.toString(), RetirementOperation.class);
		query.setParameter("endDate", dateToCancel);
		query.setParameter("lstInState", lstInState);
		query.setParameter("motive", RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode());
		
		lstRetirementOperations = query.getResultList();
		if(lstRetirementOperations!=null && lstRetirementOperations.size() > 0) {
			for(RetirementOperation ro : lstRetirementOperations) {
				PayrollHeader ph = getPayrollHeaderAndDetailEnabled(ro.getIdRetirementOperationPk());
				ro.setPayrollHeader(ph);
			}
		}
		
		
		return lstRetirementOperations;
	}
	
	public PayrollHeader getPayrollHeaderWithRetirementOperation(Long idRetirementOperationPk) {
		
		StringBuilder sd = new StringBuilder();
		sd.append(" select distinct ph.idPayrollHeaderPk, ph.payrollType, ph.payrollState   ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" inner join ph.lstPayrollDetail pd ");
		sd.append(" WHERE pd.retirementOperation.idRetirementOperationPk = :idRetirementOperationPk AND ph.indApprove = 0 ");
		sd.append(" AND ph.payrollState not in (:stateNotIn) ");
		
		try {
			
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstNotIn.add(PayrollHeaderStateType.PROCESSED.getCode());
			lstNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			lstNotIn.add(PayrollHeaderStateType.FINISHED.getCode());
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			q.setParameter("stateNotIn", lstNotIn);
			
			List<PayrollHeader> lstPayrollHead = new ArrayList<PayrollHeader>();
			PayrollHeader payrollHeader;
			List<Object> objectList = q.getResultList();
			
			if(objectList!=null && objectList.size()>0) {
				for (int i = 0; i < objectList.size(); i++) {
					Object[] obj =  (Object[]) objectList.get(i);
					payrollHeader = new PayrollHeader();
					payrollHeader.setIdPayrollHeaderPk((Long)obj[0]);
					payrollHeader.setPayrollType((Integer)obj[1]);
					payrollHeader.setPayrollState((Integer)obj[2]);
					lstPayrollHead.add(payrollHeader);
				}
			}
			
			if(lstPayrollHead!=null && lstPayrollHead.size()>0) {
				return lstPayrollHead.get(0);
			}
			
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
		
	}
	
	public PayrollHeader getPayrollHeaderAndDetailEnabled(Long idRetirementOperationPk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select ph ");
		sd.append(" from PayrollHeader  ph ");
		sd.append(" join fetch ph.lstPayrollDetail pd ");
		sd.append(" WHERE pd.retirementOperation.idRetirementOperationPk = :idRetirementOperationPk ");
		sd.append(" AND ph.payrollState not in (:stateNotIn) ");
		//sd.append(" AND ph.indApprove != :indApprove ");
		
		try {
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollHeaderStateType.ANNULATE.getCode());
			lstNotIn.add(PayrollHeaderStateType.PROCESSED.getCode());
			lstNotIn.add(PayrollHeaderStateType.REJECT.getCode());
			lstNotIn.add(PayrollHeaderStateType.FINISHED.getCode());
			List<PayrollHeader> lst= new ArrayList<>();
			Query q = em.createQuery(sd.toString());
			q.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			q.setParameter("stateNotIn", lstNotIn);
			//q.setParameter("indApprove", 1);
			lst = q.getResultList();
			if (!lst.isEmpty())
				return lst.get(0);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
		
	}
	
	public List<PayrollDetail> getAllPayrollDetail(Long idPayrollHeaderPk) {
		StringBuilder sd = new StringBuilder();
		sd.append(" select prd ");
		sd.append(" from PayrollDetail prd ");
		sd.append(" where prd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk ");
		try {
			
			TypedQuery<PayrollDetail> q = em.createQuery(sd.toString(), PayrollDetail.class);
			q.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
			return q.getResultList();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error: " + e.getMessage());
			return null;
		}
	}
	
	/**
	 * Search retirement operations.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<RetirementOperationTO> searchRetirementOperations(RetirementOperationTO retirementOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
//		Map<String,Object> parameters = new HashMap<String, Object>();
//		sbQuery.append("	SELECT distinct ro.idRetirementOperationPk, " );
//		sbQuery.append("		ro.custodyOperation.registryDate, " );
//		sbQuery.append("		ro.security.idSecurityCodePk, " );
//		sbQuery.append("		ro.totalBalance, " );
//		sbQuery.append("		ro.state, " );
//		sbQuery.append("		( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.state ), " );
//		sbQuery.append("		( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.motive ), " );
//		sbQuery.append("		ro.custodyOperation.operationNumber, " );
//		sbQuery.append("		ro.security.idSecurityBcbCode, " );
//		sbQuery.append("		ro.security.securityClass, " );
//		sbQuery.append("		ro.motive " );//10
////		sbQuery.append("		rd.holderAccount.idHolderAccountPk " );
//		sbQuery.append("	FROM RetirementDetail rd inner join rd.retirementOperation ro" );
//		sbQuery.append("	WHERE TRUNC(ro.custodyOperation.registryDate) between :initialDate AND :endDate");
		
		sbQuery.append("	SELECT distinct ro.ID_RETIREMENT_OPERATION_PK, " );//0
		sbQuery.append("	co.REGISTRY_DATE, " );//1
		sbQuery.append("	ro.ID_SECURITY_CODE_FK, " );//2
		sbQuery.append("	ro.TOTAL_BALANCE, " );//3
		sbQuery.append("	ro.STATE, " );//4
		sbQuery.append("	(select  pt.PARAMETER_NAME from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK=ro.STATE), " );//5
		sbQuery.append("	(select  pt.PARAMETER_NAME from PARAMETER_TABLE pt where  pt.PARAMETER_TABLE_PK=ro.MOTIVE), " );//6
		sbQuery.append("	co.OPERATION_NUMBER , " );//7
		sbQuery.append("	s.ID_SECURITY_BCB_CODE , " );//8
		sbQuery.append("	s.SECURITY_CLASS , " );//9
		sbQuery.append("	ro.MOTIVE, " );//10
		sbQuery.append("	p.DESCRIPTION, " );//11
		sbQuery.append("	(CASE WHEN ro.MOTIVE = " + RetirementOperationMotiveType.REVERSION.getCode() + " THEN " );
		sbQuery.append("	(SELECT pc.CUPON_NUMBER FROM PHYSICAL_CERTIFICATE pc WHERE pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK " );
		sbQuery.append("	AND pc.IND_CUPON = 0) ELSE " );
		sbQuery.append("	(SELECT pc.CUPON_NUMBER FROM PHYSICAL_CERTIFICATE pc WHERE pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK) " );
		sbQuery.append("	END) AS CUPON, " );	//12
		
		sbQuery.append("	(CASE WHEN " );
		sbQuery.append("	(CASE WHEN ro.MOTIVE = " + RetirementOperationMotiveType.REVERSION.getCode() + " THEN " );
		sbQuery.append("	(SELECT pc.CUPON_NUMBER FROM PHYSICAL_CERTIFICATE pc WHERE pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK " );
		sbQuery.append("	AND pc.IND_CUPON = 0) ELSE " );
		sbQuery.append("	(SELECT pc.CUPON_NUMBER FROM PHYSICAL_CERTIFICATE pc WHERE pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK) " );
		sbQuery.append("	END) IS NULL THEN " );
		sbQuery.append("	(CASE WHEN ro.MOTIVE = " + RetirementOperationMotiveType.REVERSION.getCode() + "THEN  " );
		sbQuery.append("	(SELECT pac.COUPON_AMOUNT FROM AMORTIZATION_PAYMENT_SCHEDULE aps " );
		sbQuery.append("	INNER JOIN PROGRAM_AMORTIZATION_COUPON pac ON aps.ID_AMO_PAYMENT_SCHEDULE_PK = pac.ID_AMO_PAYMENT_SCHEDULE_FK " );
		sbQuery.append("	WHERE aps.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK) " );
		sbQuery.append("	ELSE (SELECT pc2.NOMINAL_VALUE FROM PHYSICAL_CERTIFICATE pc2 WHERE pc2.ID_PHYSICAL_CERTIFICATE_PK = ro.ID_PHYSICAL_CERTIFICATE_FK) " );
		sbQuery.append("	END ) " );
		sbQuery.append("	ELSE " );
		sbQuery.append("	(SELECT pic.COUPON_AMOUNT FROM INTEREST_PAYMENT_SCHEDULE ips " );
		sbQuery.append("	INNER JOIN PROGRAM_INTEREST_COUPON pic ON ips.ID_INT_PAYMENT_SCHEDULE_PK = pic.ID_INT_PAYMENT_SCHEDULE_FK " );
		sbQuery.append("	WHERE ips.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK AND pic.COUPON_NUMBER =  " );
		sbQuery.append("	(SELECT pc.CUPON_NUMBER FROM PHYSICAL_CERTIFICATE pc  " );
		sbQuery.append("	WHERE pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK)) end) AS NOMINAL_VALUE, " ); //13
		
		sbQuery.append("	(SELECT DISTINCT  " );
		sbQuery.append("	ph.ID_PAYROLL_HEADER_PK FROM PAYROLL_DETAIL pd INNER JOIN PAYROLL_HEADER ph ON pd.ID_PAYROLL_HEADER_FK = ph.ID_PAYROLL_HEADER_PK " );
		sbQuery.append("	WHERE pd.ID_RETIREMENT_OPERATION_FK = ro.ID_RETIREMENT_OPERATION_PK AND pd.PAYROLL_STATE NOT IN ("
				+ PayrollDetailStateType.ANNULATE.getCode() + "," + PayrollDetailStateType.REJECT.getCode() + " ) " );
		sbQuery.append("	AND ph.PAYROLL_STATE NOT IN ("
				+ PayrollHeaderStateType.ANNULATE.getCode() + "," + PayrollHeaderStateType.REJECT.getCode() + ")) AS ID_PAYROLL_HEADER, " ); //14
		
		sbQuery.append("	(CASE WHEN  " );
		sbQuery.append("	(SELECT DISTINCT  " );
		sbQuery.append("	pd.PAYROLL_STATE FROM PAYROLL_DETAIL pd INNER JOIN PAYROLL_HEADER ph ON pd.ID_PAYROLL_HEADER_FK = ph.ID_PAYROLL_HEADER_PK " );
		sbQuery.append("	WHERE pd.ID_RETIREMENT_OPERATION_FK = ro.ID_RETIREMENT_OPERATION_PK AND pd.PAYROLL_STATE NOT IN ("
				+ PayrollDetailStateType.ANNULATE.getCode() + "," + PayrollDetailStateType.REJECT.getCode() + " ) " );
		sbQuery.append("	AND ph.PAYROLL_STATE NOT IN ("
				+ PayrollHeaderStateType.ANNULATE.getCode() + "," + PayrollHeaderStateType.REJECT.getCode() + ")) = "
				+ PayrollDetailStateType.DELIVERED.getCode() + " THEN 'SI' ELSE 'NO' END ) AS PAYROLL_STATE, " ); //15
		
		sbQuery.append("	(CASE WHEN aao.CERTIFICATE_FROM IS NOT NULL AND aao.CERTIFICATE_TO IS NOT NULL THEN " );
		sbQuery.append("	TO_CHAR(aao.CERTIFICATE_FROM) || ' - ' || TO_CHAR(aao.CERTIFICATE_TO) " );
		sbQuery.append("	ELSE NULL END) AS CERTIFICATE_FROM_TO " ); //16
		sbQuery.append("	from  RETIREMENT_DETAIL rd  " );
		sbQuery.append("	inner join RETIREMENT_OPERATION ro on rd.ID_RETIREMENT_OPERATION_FK=ro.ID_RETIREMENT_OPERATION_PK cross " );
		sbQuery.append("	join CUSTODY_OPERATION co cross " );
		sbQuery.append("	join SECURITY s " );
		sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_ANNOTATION_OPERATION_PK = ro.ID_ANNOTATION_OPERATION_FK " );
		sbQuery.append("	INNER JOIN PARTICIPANT p ON rd.ID_PARTICIPANT_FK = p.ID_PARTICIPANT_PK " );
		sbQuery.append("	where " );
		sbQuery.append("	ro.ID_RETIREMENT_OPERATION_PK = co.ID_CUSTODY_OPERATION_PK " );
		sbQuery.append("	and ro.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK " );
		sbQuery.append("	and TRUNC(co.REGISTRY_DATE) between :initialDate AND :endDate " );
				
		/*
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getIndWithdrawalBCB())){
			sbQuery.append("	 AND ro.indWithdrawalBCB = :indWithdrawalBCB ");
			parameters.put("indWithdrawalBCB", retirementOperationTO.getIndWithdrawalBCB());
		}*/
		
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getSecurityClass())){
			sbQuery.append("	AND s.SECURITY_CLASS = :securityClass");
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getIdParticipantPk())){
			sbQuery.append("	AND p.ID_PARTICIPANT_PK =  :idParticipantPk");
		}
		
		if(Validations.validateIsNotNull(retirementOperationTO.getIdHolderPk())){
			sbQuery.append("  AND exists ( ");
			sbQuery.append("			SELECT FROM HOLDER_ACCOUNT ha INNER JOIN HOLDER_ACCOUNT_DETAIL had ON had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK ");
			sbQuery.append("			WHERE had.ID_HOLDER_FK = :idHolder ");
			sbQuery.append("			AND had.ID_HOLDER_ACCOUNT_FK = rd.ID_HOLDER_ACCOUNT_FK ) ");
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getState())){
			sbQuery.append("	AND ro.STATE = :state");
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getOperationNumber())){
			sbQuery.append("	AND co.OPERATION_NUMBER = :operationNumber");
		}
			
		if(Validations.validateIsNotNull(retirementOperationTO.getSecurity()) && Validations.validateIsNotNull(retirementOperationTO.getSecurity().getIdSecurityCodePk())){
			sbQuery.append("	AND ( ro.ID_SECURITY_CODE_FK = :idSecurityCodePrm ) ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getMotive())){
			sbQuery.append("	AND ro.MOTIVE = :motive");
		}
		
		if (Validations.validateIsNotNull(retirementOperationTO.getIndWithdrawalBCB())) {
			sbQuery.append("	AND ro.IND_WITHDRAWAL_BCB = :indWithdrawalBCB");
		}
		
		sbQuery.append("	ORDER BY ro.ID_RETIREMENT_OPERATION_PK DESC");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("initialDate", retirementOperationTO.getInitDate(),TemporalType.DATE);
		query.setParameter("endDate", retirementOperationTO.getEndDate(),TemporalType.DATE);
		
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getSecurityClass())){
			query.setParameter("securityClass", retirementOperationTO.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getIdParticipantPk())){
			query.setParameter("idParticipantPk", retirementOperationTO.getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(retirementOperationTO.getIdHolderPk())){
			query.setParameter("idHolder", retirementOperationTO.getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getState())){
			query.setParameter("state", retirementOperationTO.getState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getOperationNumber())){
			query.setParameter("operationNumber", retirementOperationTO.getOperationNumber());
		}
		if(Validations.validateIsNotNull(retirementOperationTO.getSecurity()) && Validations.validateIsNotNull(retirementOperationTO.getSecurity().getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePrm", retirementOperationTO.getSecurity().getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getMotive())){
			query.setParameter("motive", retirementOperationTO.getMotive());
		}
		if (Validations.validateIsNotNull(retirementOperationTO.getIndWithdrawalBCB())) {
			query.setParameter("indWithdrawalBCB", retirementOperationTO.getIndWithdrawalBCB());
		}
		
		List<Object[]> lstResult = query.getResultList();
		
		//List<Object[]> lstResult = findListByQueryString(sbQuery.toString(), parameters);
		List<RetirementOperationTO> lstRetireOperation = new ArrayList<RetirementOperationTO>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){			
			for(Object[] arrObjects : lstResult){
				RetirementOperationTO retirementOperation = new RetirementOperationTO();
				retirementOperation.setIdRetirementOperationPk(((BigDecimal) arrObjects[0]).longValue());
				retirementOperation.setInitDate((Date)arrObjects[1]);
				retirementOperation.setIdSecurityCodePk((String)arrObjects[2]);
				retirementOperation.setTotalBalance((BigDecimal)arrObjects[3]);
				retirementOperation.setState(((BigDecimal)arrObjects[4]).intValue());
				retirementOperation.setStateDescription((String)arrObjects[5]);
				retirementOperation.setMotiveDescription((String)arrObjects[6]);
				retirementOperation.setOperationNumber(((BigDecimal)arrObjects[7]).longValue());
				if (Validations.validateIsNotNull(arrObjects[8])) {
					retirementOperation.setIdSecurityBCBcode(arrObjects[8].toString());
				}
				retirementOperation.setSecurityClass(new Integer(arrObjects[9].toString()));
				retirementOperation.setMotive(new Integer(arrObjects[10].toString()));
				retirementOperation.setParticipantMnemonic((String)arrObjects[11]);
				if (Validations.validateIsNotNull(arrObjects[12])) {
					retirementOperation.setCoupon(arrObjects[12].toString());
				}else {
					retirementOperation.setCoupon("");
				}
				retirementOperation.setNominalValue((BigDecimal)arrObjects[13]);
				if (Validations.validateIsNotNull(arrObjects[14])) {
					retirementOperation.setPayroll(arrObjects[14].toString());
				}else {
					retirementOperation.setPayroll("");
				}
				retirementOperation.setStateDesc((String)arrObjects[15]);
				if (Validations.validateIsNotNull(arrObjects[16])) {
					retirementOperation.setCertificateFromTo(arrObjects[16].toString());
				}else {
					retirementOperation.setCertificateFromTo("");
				}
				
				retirementOperation.setHolderName(getListDescrptionHolderFromIdRetirementOperation(retirementOperation.getIdRetirementOperationPk()));
				
//				if(Validations.validateIsNotNull(arrObjects[10])){
//					List<Long> list = new ArrayList<>();
//					list.add(new Long(arrObjects[10].toString()));
//					retirementOperation.setIdHolderAccountPk(list);	
//				}				
				lstRetireOperation.add(retirementOperation);
			}
		}		
		return lstRetireOperation;				
	}
	
	/**
	 * Search retirement operations.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<RetirementOperationTO> searchRetirementOperationsAccount(RetirementOperationTO retirementOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append("	SELECT distinct ro.idRetirementOperationPk, " );
		sbQuery.append("		ro.custodyOperation.registryDate, " );
		sbQuery.append("		ro.security.idSecurityCodePk, " );
		sbQuery.append("		ro.totalBalance, " );
		sbQuery.append("		ro.state, " );
		sbQuery.append("		( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.state ), " );
		sbQuery.append("		( select pt.parameterName from ParameterTable pt where pt.parameterTablePk = ro.motive ), " );
		sbQuery.append("		ro.custodyOperation.operationNumber, " );
		sbQuery.append("		ro.security.idSecurityBcbCode, " );
		sbQuery.append("		ro.security.securityClass, " );
		sbQuery.append("		rd.holderAccount.idHolderAccountPk " );
		sbQuery.append("	FROM RetirementDetail rd inner join rd.retirementOperation ro" );
		sbQuery.append("	WHERE TRUNC(ro.custodyOperation.registryDate) between :initialDate AND :endDate");
				
		/*
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getIndWithdrawalBCB())){
			sbQuery.append("	 AND ro.indWithdrawalBCB = :indWithdrawalBCB ");
			parameters.put("indWithdrawalBCB", retirementOperationTO.getIndWithdrawalBCB());
		}*/
		
		if(Validations.validateIsNotNullAndPositive(retirementOperationTO.getSecurityClass())){
			sbQuery.append("	AND ro.security.securityClass = :securityClass");
			parameters.put("securityClass", retirementOperationTO.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getIdParticipantPk())){
			sbQuery.append("	AND rd.participant.idParticipantPk = :idParticipantPk");
			parameters.put("idParticipantPk", retirementOperationTO.getIdParticipantPk());
		}
		
		if(Validations.validateIsNotNull(retirementOperationTO.getIdHolderPk())){
			sbQuery.append("	exists ( ");
			sbQuery.append("			select had.holder.idHolderPk from HolderAccount ha inner join ha.holderAccountDetails ");
			sbQuery.append("			where had.holder.idHolderPk = :idHolder ");
			sbQuery.append("			and had.holderAccount.idHolderAccountPk = rd.holderAccount.idHolderAccountPk ) ");
			parameters.put("idHolder", retirementOperationTO.getIdHolderPk());
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getState())){
			sbQuery.append("	AND ro.state = :state");
			parameters.put("state", retirementOperationTO.getState());
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getOperationNumber())){
			sbQuery.append("	AND ro.custodyOperation.operationNumber = :operationNumber");
			parameters.put("operationNumber", retirementOperationTO.getOperationNumber());
		}
			
		if(Validations.validateIsNotNull(retirementOperationTO.getSecurity()) && Validations.validateIsNotNull(retirementOperationTO.getSecurity().getIdSecurityCodePk())){
			sbQuery.append("	AND ( ro.security.idSecurityCodePk = :idSecurityCodePrm or ro.targetSecurity.idSecurityCodePk = :idSecurityCodePrm ) ");
			parameters.put("idSecurityCodePrm", retirementOperationTO.getSecurity().getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(retirementOperationTO.getMotive())){
			sbQuery.append("	AND ro.motive = :motive");
			parameters.put("motive", retirementOperationTO.getMotive());
		}
		
		if (Validations.validateIsNotNull(retirementOperationTO.getIndWithdrawalBCB())) {
			sbQuery.append("	AND ro.indWithdrawalBCB = :indWithdrawalBCB");
			parameters.put("indWithdrawalBCB", retirementOperationTO.getIndWithdrawalBCB());
		}
		
		sbQuery.append("	ORDER BY ro.idRetirementOperationPk DESC");
		
		parameters.put("initialDate", retirementOperationTO.getInitDate());
		parameters.put("endDate", retirementOperationTO.getEndDate());
		
		List<Object[]> lstResult = findListByQueryString(sbQuery.toString(), parameters);
		List<RetirementOperationTO> lstRetireOperation = new ArrayList<RetirementOperationTO>();
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){			
			for(Object[] arrObjects : lstResult){
				RetirementOperationTO retirementOperation = new RetirementOperationTO();
				retirementOperation.setIdRetirementOperationPk((Long)arrObjects[0]);
				retirementOperation.setInitDate((Date)arrObjects[1]);
				retirementOperation.setIdSecurityCodePk((String)arrObjects[2]);
				retirementOperation.setTotalBalance((BigDecimal)arrObjects[3]);
				retirementOperation.setState((Integer)arrObjects[4]);
				retirementOperation.setStateDescription((String)arrObjects[5]);
				retirementOperation.setMotiveDescription((String)arrObjects[6]);
				retirementOperation.setOperationNumber((Long)arrObjects[7]);
				if (Validations.validateIsNotNull(arrObjects[8])) {
					retirementOperation.setIdSecurityBCBcode(arrObjects[8].toString());
				}
				retirementOperation.setSecurityClass(new Integer(arrObjects[9].toString()));
				
				retirementOperation.setHolderName(getListDescrptionHolderFromIdRetirementOperation(retirementOperation.getIdRetirementOperationPk()));
				
				if(Validations.validateIsNotNull(arrObjects[10])){
					List<Long> list = new ArrayList<>();
					list.add(new Long(arrObjects[10].toString()));
					retirementOperation.setIdHolderAccountPk(list);	
				}				
				lstRetireOperation.add(retirementOperation);
			}
		}		
		return lstRetireOperation;				
	}
	
	/**
	 * Metodo para buscar valor redimido
	 * @param idHolderAccountPks
	 * @param security
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalanceExp> searchHolderAccountBalanceExpByParticipantAndSecurity (Long idParticipant,String idSecurity){

		List<HolderAccountBalanceExp> lstAccountBalanceExps = new ArrayList<>();
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  	SELECT                                                                                     ");
		querySql.append("  	HABE.ID_HOLDER_EXP_PK,                                                                     ");
		querySql.append("  	HABE.ID_PARTICIPANT_PK,                                                                    ");
		querySql.append("  	HABE.ID_HOLDER_ACCOUNT_PK,                                                                 ");
		querySql.append("  	HABE.ID_SECURITY_CODE_PK,                                                                  ");
		querySql.append("  	HABE.TOTAL_BALANCE,                                                                        ");
		querySql.append("  	HABE.AVAILABLE_BALANCE,                                                                    ");
		querySql.append("  	HABE.PAWN_BALANCE,                                                                         ");
		querySql.append("  	HABE.BAN_BALANCE,                                                                          ");
		querySql.append("  	HABE.OTHER_BLOCK_BALANCE,                                                                  ");
		querySql.append("  	HABE.ACCREDITATION_BALANCE,                                                                ");
		querySql.append("  	TO_CHAR(P.MNEMONIC || ' - ' || P.ID_PARTICIPANT_PK) PART,                                  ");
		querySql.append("  	HA.ACCOUNT_NUMBER,                                                                         ");
		querySql.append("  	(SELECT                                                                                    ");
		querySql.append("  			LISTAGG(HAD.ID_HOLDER_FK                                                           ");
		querySql.append("  			||' - '                                                                            ");
		querySql.append("  			||(select h.full_name from holder h                                                ");
		querySql.append("  			   where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK)                                          ");
		querySql.append("  			||chr(13)                                                                          ");
		querySql.append("  			)                                                                                  ");
		querySql.append("  	WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  												   ");
		querySql.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD  														   ");
		querySql.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  								   ");
		querySql.append("  	) HOLDERS                                                                                  ");
		querySql.append("  	FROM HOLDER_ACCOUNT_BALANCE_EXP HABE                                                       ");
		querySql.append("  	INNER JOIN SECURITY SEC ON HABE.ID_SECURITY_CODE_PK = SEC.ID_SECURITY_CODE_PK              ");
		querySql.append("  	INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HABE.ID_PARTICIPANT_PK                   ");
		querySql.append("  	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HABE.ID_HOLDER_ACCOUNT_PK        ");
		querySql.append("  	WHERE 1 = 1                                                                                ");
		
		if(Validations.validateIsNotNull(idParticipant)){
			querySql.append("  	AND P.ID_PARTICIPANT_PK = :idParticipant                   				                   ");
		}	
		if(Validations.validateIsNotNull(idSecurity)){
			querySql.append("  	AND SEC.ID_SECURITY_CODE_PK = :securityPk                                                  ");
		}
		
		querySql.append(" order by SEC.ID_SECURITY_CODE_PK asc ");	
		
		Query query = em.createNativeQuery(querySql.toString());
		
		if(Validations.validateIsNotNull(idSecurity)){
			query.setParameter("securityPk", idSecurity);
		}
		if(Validations.validateIsNotNull(idParticipant)){
			query.setParameter("idParticipant", idParticipant);
		}
		
		List<Object[]> lstRestul = query.getResultList();
		
		if (lstRestul.size()>0){
			for (Object[] lstFiltr : lstRestul){
				HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
				
				accountBalanceExp.setIdHolderExpPk(Long.valueOf(lstFiltr[0].toString()));
				accountBalanceExp.setParticipant(Long.valueOf(lstFiltr[1].toString()));
				accountBalanceExp.setHolderAccount(Long.valueOf(lstFiltr[2].toString()));
				accountBalanceExp.setSecurity(lstFiltr[3].toString());
				accountBalanceExp.setTotalBalance((BigDecimal)lstFiltr[4]);
				accountBalanceExp.setAvailableBalance((BigDecimal)lstFiltr[5]);
				accountBalanceExp.setPawnBalance((BigDecimal)lstFiltr[6]);
				accountBalanceExp.setBanBalance((BigDecimal)lstFiltr[7]);
				accountBalanceExp.setOtherBlockBalance((BigDecimal)lstFiltr[8]);
				accountBalanceExp.setAccreditationBalance((BigDecimal)lstFiltr[9]);
				accountBalanceExp.setParticipantDesc((String)lstFiltr[10].toString());
				accountBalanceExp.setAccountNumberDesc(lstFiltr[11].toString());
				accountBalanceExp.setHolderDesc(lstFiltr[12].toString());
				lstAccountBalanceExps.add(accountBalanceExp);
			}
		}	
		return lstAccountBalanceExps;
	}
	
	/**
	 * Metodo para buscar valor redimido
	 * @param idHolderAccountPks
	 * @param security
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalanceExp> searchHolderAccountBalanceExpByParticipantAndSecurityAndHolderAccount (Long idParticipant,String idSecurity,Long accountNumber){

		List<HolderAccountBalanceExp> lstAccountBalanceExps = new ArrayList<>();
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  	SELECT                                                                                     ");
		querySql.append("  	HABE.ID_HOLDER_EXP_PK,                                                                     ");
		querySql.append("  	HABE.ID_PARTICIPANT_PK,                                                                    ");
		querySql.append("  	HABE.ID_HOLDER_ACCOUNT_PK,                                                                 ");
		querySql.append("  	HABE.ID_SECURITY_CODE_PK,                                                                  ");
		querySql.append("  	HABE.TOTAL_BALANCE,                                                                        ");
		querySql.append("  	HABE.AVAILABLE_BALANCE,                                                                    ");
		querySql.append("  	HABE.PAWN_BALANCE,                                                                         ");
		querySql.append("  	HABE.BAN_BALANCE,                                                                          ");
		querySql.append("  	HABE.OTHER_BLOCK_BALANCE,                                                                  ");
		querySql.append("  	HABE.ACCREDITATION_BALANCE,                                                                ");
		querySql.append("  	TO_CHAR(P.MNEMONIC || ' - ' || P.ID_PARTICIPANT_PK) PART,                                  ");
		querySql.append("  	HA.ACCOUNT_NUMBER,                                                                         ");
		querySql.append("  	(SELECT                                                                                    ");
		querySql.append("  			LISTAGG(HAD.ID_HOLDER_FK                                                           ");
		querySql.append("  			||' - '                                                                            ");
		querySql.append("  			||(select h.full_name from holder h                                                ");
		querySql.append("  			   where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK)                                          ");
		querySql.append("  			||chr(13)                                                                          ");
		querySql.append("  			)                                                                                  ");
		querySql.append("  	WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  												   ");
		querySql.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD  														   ");
		querySql.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  								   ");
		querySql.append("  	) HOLDERS                                                                                  ");
		querySql.append("  	FROM HOLDER_ACCOUNT_BALANCE_EXP HABE                                                       ");
		querySql.append("  	INNER JOIN SECURITY SEC ON HABE.ID_SECURITY_CODE_PK = SEC.ID_SECURITY_CODE_PK              ");
		querySql.append("  	INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HABE.ID_PARTICIPANT_PK                   ");
		querySql.append("  	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HABE.ID_HOLDER_ACCOUNT_PK        ");
		querySql.append("  	WHERE 1 = 1                                                                                ");
		
		if(Validations.validateIsNotNull(idParticipant)){
			querySql.append("  	AND P.ID_PARTICIPANT_PK = :idParticipant                   				                   ");
		}	
		if(Validations.validateIsNotNull(idSecurity)){
			querySql.append("  	AND SEC.ID_SECURITY_CODE_PK = :securityPk                                                  ");
		}
		if(Validations.validateIsNotNull(accountNumber)){
			querySql.append("  	AND HA.ACCOUNT_NUMBER = :accountNumber                                                  ");
		}
		
		querySql.append(" order by SEC.ID_SECURITY_CODE_PK asc ");	
		
		Query query = em.createNativeQuery(querySql.toString());
		
		if(Validations.validateIsNotNull(idSecurity)){
			query.setParameter("securityPk", idSecurity);
		}
		if(Validations.validateIsNotNull(idParticipant)){
			query.setParameter("idParticipant", idParticipant);
		}
		if(Validations.validateIsNotNull(accountNumber)){
			query.setParameter("accountNumber", accountNumber);
		}
		
		List<Object[]> lstRestul = query.getResultList();
		
		if (lstRestul.size()>0){
			for (Object[] lstFiltr : lstRestul){
				HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
				
				accountBalanceExp.setIdHolderExpPk(Long.valueOf(lstFiltr[0].toString()));
				accountBalanceExp.setParticipant(Long.valueOf(lstFiltr[1].toString()));
				accountBalanceExp.setHolderAccount(Long.valueOf(lstFiltr[2].toString()));
				accountBalanceExp.setSecurity(lstFiltr[3].toString());
				accountBalanceExp.setTotalBalance((BigDecimal)lstFiltr[4]);
				accountBalanceExp.setAvailableBalance((BigDecimal)lstFiltr[5]);
				accountBalanceExp.setPawnBalance((BigDecimal)lstFiltr[6]);
				accountBalanceExp.setBanBalance((BigDecimal)lstFiltr[7]);
				accountBalanceExp.setOtherBlockBalance((BigDecimal)lstFiltr[8]);
				accountBalanceExp.setAccreditationBalance((BigDecimal)lstFiltr[9]);
				accountBalanceExp.setParticipantDesc((String)lstFiltr[10].toString());
				accountBalanceExp.setAccountNumberDesc(lstFiltr[11].toString());
				accountBalanceExp.setHolderDesc(lstFiltr[12].toString());
				lstAccountBalanceExps.add(accountBalanceExp);
			}
		}	
		return lstAccountBalanceExps;
	}
	

	
	/**
	 * metodo para obtener la descripcion del/los titulare/s desde el id de un RetirementOperation(Operacion de retiro)
	 * @param idRetirementOperationPk
	 * @return null si no hay resultados
	 */
	private String getListDescrptionHolderFromIdRetirementOperation(Long idRetirementOperationPk){
		String result=null;
		StringBuilder sd=new StringBuilder();
		sd.append(" select listagg((select h.ID_HOLDER_PK || '-' || h.FULL_NAME from HOLDER h where h.ID_HOLDER_PK = HAD.ID_HOLDER_FK), '<br/> ')");
		sd.append(" within group (order by had.ID_HOLDER_FK) ");
		sd.append(" from HOLDER_ACCOUNT_DETAIL had   ");
		sd.append(" where ");
		sd.append(" had.ID_HOLDER_ACCOUNT_FK in ");
		sd.append(" 	( select ID_HOLDER_ACCOUNT_FK from RETIREMENT_DETAIL");
		sd.append("  	  where ID_RETIREMENT_OPERATION_FK = :idRetirementOperationPk )");
		try {
			Query q=em.createNativeQuery(sd.toString());
			q.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			Object obj=q.getSingleResult();
			if(obj!=null)
				result=obj.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
		
		
		
	/**
	 * Find all participants.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> findAllParticipants() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p.idParticipantPk,p.description, p.state from Participant p order by p.description");
		Query query = em.createQuery(sbQuery.toString());		
		List<Object> oList = query.getResultList();
		List<Participant> participantList = new ArrayList<Participant>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating Participant
			Participant participant = new Participant();
			participant.setDescription((String) objects[1]);
			participant.setIdParticipantPk((Long) objects[0]);
			participant.setState((Integer) objects[2]);
			participantList.add(participant);
		}
		return participantList;
	}
	
	/**
	 * Gets the holder market facts.
	 *
	 * @param idSecurityCode the id security code
	 * @param participantId the participant id
	 * @param idHolderAccount the id holder account
	 * @return the holder market facts
	 */
	@SuppressWarnings("unchecked")
	public List<MarketFactTO> getHolderMarketFacts(String idSecurityCode, Long participantId, Long idHolderAccount){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();			
		
		sbQuery.append("SELECT" );
		sbQuery.append("	hmb.IdMarketfactBalancePk, ");
		sbQuery.append("	hmb.marketRate, ");
		sbQuery.append("	hmb.marketPrice, " );		
		sbQuery.append("	hmb.marketDate, " );			
		sbQuery.append("	hmb.totalBalance," );	
		sbQuery.append("	hmb.availableBalance" );
		sbQuery.append("  FROM  HolderMarketFactBalance hmb ");
		sbQuery.append(" WHERE hmb.security.idSecurityCodePk = :idSecurityCode ");
		sbQuery.append("   AND hmb.holderAccount.idHolderAccountPk = :idHolderAccount ");
		sbQuery.append("   AND hmb.participant.idParticipantPk = :idParticipant ");
		sbQuery.append(" AND hmb.availableBalance > 0");		

		parameters.put("idSecurityCode", idSecurityCode);
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", participantId);
		
		sbQuery.append(" ORDER BY hmb.marketRate desc ");
		
        List<Object[]> objectList = findListByQueryString(sbQuery.toString(), parameters);
		List<MarketFactTO> holderAccountBalanceTOs = new ArrayList<MarketFactTO>();

		for(Object[] objects  : objectList){
			MarketFactTO holderAccountBalanceTO = new MarketFactTO();
			holderAccountBalanceTO.setIdMarketFactPk((Long) objects[0] );
			holderAccountBalanceTO.setMarketRate((BigDecimal) objects[1]);
			holderAccountBalanceTO.setMarketPrice((BigDecimal) objects[2]);
			holderAccountBalanceTO.setMarketDate((Date) objects[3]);
			holderAccountBalanceTO.setQuantity((BigDecimal) objects[5]);
			holderAccountBalanceTOs.add(holderAccountBalanceTO);			
		}
		return holderAccountBalanceTOs;
	}
	
	
	/**
	 * Gets the holder accounts balances.
	 *
	 * @param idSecurityCode the id security code
	 * @param participantId the participant id
	 * @param idHolderAccount the id holder account
	 * @param validateTotal the validate total
	 * @return the holder accounts balances
	 */
	public List<PhysicalCertificate> getListPhysicalCertificate(String idSecurityCodePk, Date expirationDate) {
		List<PhysicalCertificate> lstPhysicalCertificate= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT pc ");
		stringBuffer.append(" FROM PhysicalCertificate pc ");
		stringBuffer.append(" JOIN FETCH pc.securities s ");
		stringBuffer.append(" JOIN FETCH s.issuer i ");
		stringBuffer.append(" WHERE s.idSecurityCodePk = :idSecurityCodePk   ");
		stringBuffer.append(" AND pc.situation = :situation ");
		stringBuffer.append(" AND pc.state = :state ");
		if(expirationDate!=null) {
			stringBuffer.append(" AND pc.expirationDate = :expirationDate ");
		}
		
		TypedQuery<PhysicalCertificate> typedQuery= em.createQuery(stringBuffer.toString(), PhysicalCertificate.class);
		typedQuery.setParameter("idSecurityCodePk", idSecurityCodePk);
		typedQuery.setParameter("situation", PayrollDetailStateType.VAULT.getCode());//SituationType.CUSTODY_VAULT.getCode()
		typedQuery.setParameter("state", PhysicalCertificateStateType.CONFIRMED.getCode());
		if(expirationDate!=null) {
			typedQuery.setParameter("expirationDate", expirationDate);
		}
		
		lstPhysicalCertificate = typedQuery.getResultList();
		return lstPhysicalCertificate;
	}
	
public CorporativeProcessResult CorporativeProcessResultWithAmortizationCupon(String idSecurityCodePk, Date corporativeDeliveryDate) {
		
		try {
			
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT cpr ");
			stringBuffer.append(" FROM CorporativeProcessResult cpr  ");
			stringBuffer.append(" JOIN FETCH cpr.corporativeOperation co ");
			stringBuffer.append(" JOIN FETCH co.programAmortizationCoupon pac ");
			stringBuffer.append(" JOIN FETCH cpr.participant p ");
			stringBuffer.append(" JOIN FETCH cpr.holderAccount ha ");
			stringBuffer.append(" JOIN FETCH cpr.security s ");
			stringBuffer.append(" WHERE co.securities.idSecurityCodePk = :idSecurityCodePk  ");
			stringBuffer.append(" AND co.state = :corpState  ");
			stringBuffer.append(" AND co.corporativeEventType.idCorporativeEventTypePk = :idCorporativeEventTypePk ");
			stringBuffer.append(" AND co.deliveryDate = :corporativeDeliveryDate ");
	
			TypedQuery<CorporativeProcessResult> query= em.createQuery(stringBuffer.toString(), CorporativeProcessResult.class);
			
			query.setParameter("corporativeDeliveryDate", corporativeDeliveryDate);
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			query.setParameter("idCorporativeEventTypePk", 15L);
			query.setParameter("corpState", CorporateProcessStateType.DEFINITIVE.getCode());
		
			return query.getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		}
		
	}

	public CorporativeProcessResult CorporativeProcessResultWithInterestCupon(String idSecurityCodePk, Date corporativeDeliveryDate) {
		
		try {
			
			StringBuffer stringBuffer= new StringBuffer();
			stringBuffer.append(" SELECT cpr ");
			stringBuffer.append(" FROM CorporativeProcessResult cpr  ");
			stringBuffer.append(" JOIN FETCH cpr.corporativeOperation co ");
			stringBuffer.append(" JOIN FETCH co.programInterestCoupon pic ");
			stringBuffer.append(" JOIN FETCH cpr.participant p ");
			stringBuffer.append(" JOIN FETCH cpr.holderAccount ha ");
			stringBuffer.append(" JOIN FETCH cpr.security s ");
			stringBuffer.append(" WHERE co.securities.idSecurityCodePk = :idSecurityCodePk  ");
			stringBuffer.append(" AND co.state = :corpState  ");
			stringBuffer.append(" AND co.corporativeEventType.idCorporativeEventTypePk = :idCorporativeEventTypePk ");
			stringBuffer.append(" AND co.deliveryDate = :corporativeDeliveryDate ");
	
			TypedQuery<CorporativeProcessResult> query= em.createQuery(stringBuffer.toString(), CorporativeProcessResult.class);
			
			query.setParameter("corporativeDeliveryDate", corporativeDeliveryDate);
			query.setParameter("idCorporativeEventTypePk", 14L);
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			query.setParameter("corpState", CorporateProcessStateType.DEFINITIVE.getCode());
			
			return query.getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		}
		
	}

	public List<PhysicalCertificate> getPhysicalCertificateCorporative(String idIssuerPk, String idSecurityCodePk, Integer currency, Date corporativeDeliveryDate) throws CloneNotSupportedException{
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT pc ");
		stringBuffer.append(" FROM PhysicalCertificate pc ");
		stringBuffer.append(" JOIN FETCH pc.securities s ");
		stringBuffer.append(" JOIN FETCH s.issuer i ");
		stringBuffer.append(" WHERE 1 = 1 ");
		if(Validations.validateIsNotNullAndNotEmpty(idSecurityCodePk)) {
			stringBuffer.append(" AND s.idSecurityCodePk = :idSecurityCodePk   ");
		}
		stringBuffer.append(" AND pc.situation = :situation ");
		stringBuffer.append(" AND pc.state = :state ");
		if(Validations.validateIsNotNullAndNotEmpty(currency)) {
			stringBuffer.append(" AND pc.currency = :currency ");
		}
		
		if(corporativeDeliveryDate!=null) {
			stringBuffer.append(" AND pc.expirationDate = :expirationDate ");
		}
		
		TypedQuery<PhysicalCertificate> query= em.createQuery(stringBuffer.toString(), PhysicalCertificate.class);
		if(Validations.validateIsNotNullAndNotEmpty(idSecurityCodePk)) {
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
		}

		if(Validations.validateIsNotNullAndNotEmpty(currency)) {
			query.setParameter("currency", currency);
		}
		
		query.setParameter("situation", PayrollDetailStateType.VAULT.getCode());//SituationType.CUSTODY_VAULT.getCode()
		query.setParameter("state", PhysicalCertificateStateType.CONFIRMED.getCode());
		
		if(corporativeDeliveryDate!=null) {
			query.setParameter("expirationDate", corporativeDeliveryDate);
		}
		
		List<PhysicalCertificate> objectList  = query.getResultList();		
		List<PhysicalCertificate> lstPhysicalCertificate = null;
		
		if(objectList!=null && objectList.size()>0) {
			lstPhysicalCertificate = new ArrayList<PhysicalCertificate>();
			for(int i = 0; i < objectList.size();++i){
				PhysicalCertificate physicalCertificate = objectList.get(i);
				
				if(physicalCertificate.getIndCupon() == 1) {
					CorporativeProcessResult cprInterest = CorporativeProcessResultWithInterestCupon(physicalCertificate.getSecurities().getIdSecurityCodePk(), corporativeDeliveryDate);
					if(cprInterest!=null) {
						PhysicalCertificate physicalCertificateInterest = physicalCertificate.clone();
						physicalCertificateInterest.setHolderAccount(cprInterest.getHolderAccount());
						physicalCertificateInterest.setParticipant(cprInterest.getParticipant());
						lstPhysicalCertificate.add(physicalCertificateInterest);
					}
				}
				
				if(physicalCertificate.getIndCupon() == 0) {
					CorporativeProcessResult cprAmortization = CorporativeProcessResultWithAmortizationCupon(physicalCertificate.getSecurities().getIdSecurityCodePk(), corporativeDeliveryDate);
					if(cprAmortization!=null) {
						PhysicalCertificate physicalCertificateAmortization = physicalCertificate.clone();
						physicalCertificateAmortization.setHolderAccount(cprAmortization.getHolderAccount());
						physicalCertificateAmortization.setParticipant(cprAmortization.getParticipant());
						lstPhysicalCertificate.add(physicalCertificateAmortization);
					}
				}
				
			}
		}
		
		return lstPhysicalCertificate;
		
	}

	@SuppressWarnings("unchecked")
	public List<TransferAccountBalanceTO> getHolderAccountsBalances(String idSecurityCode, Long participantId, Long idHolderAccount, boolean validateTotal){
		StringBuilder sbQuery = new StringBuilder();
		//Param's map
		Map<String,Object> mapParam = new HashMap<String,Object>();			
		
		sbQuery.append("SELECT" );
		sbQuery.append("	pa.idParticipantPk," );				// [0]
		sbQuery.append("	pa.mnemonic,	" );				// [1]
		sbQuery.append("	hab.id.idHolderAccountPk, " );		// [2]
		sbQuery.append("	ha, " );							// [3]
		sbQuery.append("	hab.security.idSecurityCodePk," );	// [4]
		sbQuery.append("	hab, " );							// [5]
		sbQuery.append("	sec.securityClass " );				// [6]
		sbQuery.append("  FROM  HolderAccountBalance hab ");
		sbQuery.append("  inner join hab.holderAccount ha ");
		sbQuery.append("  inner join hab.participant pa ");
		sbQuery.append("  inner join hab.security sec ");
		/*
		sbQuery.append("  left join fetch ha.holderAccountDetails had ");   //issue 116
		sbQuery.append("  left join fetch had.holder hol ");
		*/
		sbQuery.append(" WHERE hab.security.idSecurityCodePk = :idSecurityCode ");
		sbQuery.append(" AND ( select count(1) from PhysicalCertificate pc where pc.securities.idSecurityCodePk = hab.security.idSecurityCodePk ) > 0 ");
		
		if(validateTotal){
			sbQuery.append(" AND hab.totalBalance > 0 ");
		}
		mapParam.put("idSecurityCode", idSecurityCode);
		
		if(Validations.validateIsNotNullAndNotEmpty(idHolderAccount)){
			sbQuery.append("   AND  hab.id.idHolderAccountPk=:idHolderAccount");
			mapParam.put("idHolderAccount", idHolderAccount);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(participantId)){
			sbQuery.append(" AND hab.id.idParticipantPk=:idParticipantPkPrm");
			mapParam.put("idParticipantPkPrm", participantId);
		}
		
		sbQuery.append(" ORDER BY hab.totalBalance DESC");
		
        List<Object> objectList = findListByQueryString(sbQuery.toString(), mapParam);
		List<TransferAccountBalanceTO> holderAccountBalanceTOs = new ArrayList<TransferAccountBalanceTO>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			TransferAccountBalanceTO holderAccountBalanceTO = new TransferAccountBalanceTO();
			holderAccountBalanceTO.setIdParticipantPk((Long)objects[0]);
			holderAccountBalanceTO.setParticipantMnemonic((String)objects[1]);
			
			holderAccountBalanceTO.setIdHolderAccountPk((Long)objects[2]);
			
			HolderAccount holderAccount = (HolderAccount) objects[3];
			holderAccountBalanceTO.setAccountNumber(holderAccount.getAccountNumber());						
//			holderAccountBalanceTO.setHolderAccountState(holderAccount.getStateAccount());
			holderAccountBalanceTO.setAlternateCode(holderAccount.getAlternateCode());
			
			holderAccountBalanceTO.setIdSecurityCodePk((String)objects[4]);
			holderAccountBalanceTO.setHolderAccountBalance((HolderAccountBalance)objects[5]);
			
			List<PhysicalCertificate> lstPhysicalCertificate = getListPhysicalCertificate(holderAccountBalanceTO.getIdSecurityCodePk(),null);
			holderAccountBalanceTO.setLstPhysicalCertificate(lstPhysicalCertificate);
			
			/*if(holderAccountBalanceTO.getHolderAccountBalance() != null){
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance())){
					if(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance().size() > 1){
						holderAccountBalanceTO.setMarketLock(true);
					} else 
						holderAccountBalanceTO.setMarketLock(false);
				}				
			}*/
			
			holderAccountBalanceTO.setSecurityClass((Integer) objects[6]);
			
			holderAccountBalanceTOs.add(holderAccountBalanceTO);			
		}
		return holderAccountBalanceTOs;
	}
	
	@SuppressWarnings("unchecked")
	public List<TransferAccountBalanceTO> getHolderAccountsBalancesCorporative(String idIssuerPk, String idSecurityCode, Integer currency, Date corporativeDeliveryDate){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			
		//hay un par de corporativos mas que pueden retirar dinero
		//corporativo por aumento de capital + este retira el cupon/titulo y te dan nuevos para ser ingresados
		sbQuery.append("SELECT" );
		sbQuery.append("	pa.idParticipantPk," );				// [0]
		sbQuery.append("	pa.mnemonic,	" );				// [1]
		sbQuery.append("	hab.id.idHolderAccountPk, " );		// [2]
		sbQuery.append("	ha, " );							// [3]
		sbQuery.append("	hab.security.idSecurityCodePk," );	// [4]
		sbQuery.append("	hab, " );							// [5]
		sbQuery.append("	sec.securityClass " );				// [6]
		sbQuery.append("  FROM  HolderAccountBalance hab ");
		sbQuery.append("  inner join hab.holderAccount ha ");
		sbQuery.append("  inner join hab.participant pa ");
		sbQuery.append("  inner join hab.security sec ");
		sbQuery.append("  inner join sec.issuer i ");
		sbQuery.append(" WHERE ");
		sbQuery.append("		( select count(1) from CorporativeOperation co ");
		sbQuery.append(" 			where co.securities.idSecurityCodePk = hab.security.idSecurityCodePk and co.state = :corpState and co.corporativeEventType.idCorporativeEventTypePk in (lstidCorporativeEventTypePk) ");
		if(corporativeDeliveryDate != null) {
		sbQuery.append(" 			and co.deliveryDate = :deliveryDate ");
		}
		sbQuery.append("   		) > 0 ");
		sbQuery.append(" AND  ( select count(1) from PhysicalCertificate pc where pc.securities.idSecurityCodePk = hab.security.idSecurityCodePk ) > 0 ");
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND i.idIssuerPk = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.security.idSecurityCodePk = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND hab.security.currency = :currency ");
		}
		
		sbQuery.append(" ORDER BY hab.security.idSecurityCodePk DESC ");
		
		if(corporativeDeliveryDate != null) {
			mapParam.put("deliveryDate", corporativeDeliveryDate);
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		
		List<Long> lstidCorporativeEventTypePk = new ArrayList<Long>();
		lstidCorporativeEventTypePk.add(14L);
		lstidCorporativeEventTypePk.add(15L);
		mapParam.put("lstidCorporativeEventTypePk", lstidCorporativeEventTypePk);
		
		mapParam.put("corpState", CorporateProcessStateType.DEFINITIVE.getCode());
		
		
		
        List<Object> objectList = findListByQueryString(sbQuery.toString(), mapParam);
		List<TransferAccountBalanceTO> holderAccountBalanceTOs = new ArrayList<TransferAccountBalanceTO>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			TransferAccountBalanceTO holderAccountBalanceTO = new TransferAccountBalanceTO();
			holderAccountBalanceTO.setIdParticipantPk((Long)objects[0]);
			holderAccountBalanceTO.setParticipantMnemonic((String)objects[1]);
			
			holderAccountBalanceTO.setIdHolderAccountPk((Long)objects[2]);
			
			HolderAccount holderAccount = (HolderAccount) objects[3];
			holderAccountBalanceTO.setAccountNumber(holderAccount.getAccountNumber());						
//			holderAccountBalanceTO.setHolderAccountState(holderAccount.getStateAccount());
			holderAccountBalanceTO.setAlternateCode(holderAccount.getAlternateCode());
			
			holderAccountBalanceTO.setIdSecurityCodePk((String)objects[4]);
			holderAccountBalanceTO.setHolderAccountBalance((HolderAccountBalance)objects[5]);
			
			List<PhysicalCertificate> lstPhysicalCertificate = getListPhysicalCertificate(holderAccountBalanceTO.getIdSecurityCodePk(),corporativeDeliveryDate);
			holderAccountBalanceTO.setLstPhysicalCertificate(lstPhysicalCertificate);
			
			/*if(holderAccountBalanceTO.getHolderAccountBalance() != null){
				if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance())){
					if(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance().size() > 1){
						holderAccountBalanceTO.setMarketLock(true);
					} else 
						holderAccountBalanceTO.setMarketLock(false);
				}				
			}*/
			
			holderAccountBalanceTO.setSecurityClass((Integer) objects[6]);
			
			holderAccountBalanceTOs.add(holderAccountBalanceTO);			
		}
		return holderAccountBalanceTOs;
	}

	public CorporativeOperation findCorporativeOperationById(Long idCorporativeOperationPk){
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT co " );
		sbQuery.append(" FROM com.pradera.model.corporatives.CorporativeOperation co ");
		sbQuery.append(" LEFT JOIN FETCH co.programInterestCoupon pic ");
		sbQuery.append(" LEFT JOIN FETCH co.programAmortizationCoupon pac ");
		sbQuery.append(" JOIN FETCH co.securities s ");
		sbQuery.append(" WHERE co.idCorporativeOperationPk = :idCorporativeOperationPk ");
		
		TypedQuery<CorporativeOperation> query = em.createQuery(sbQuery.toString(), CorporativeOperation.class);
		query.setParameter("idCorporativeOperationPk", idCorporativeOperationPk);
		
		return query.getSingleResult();
	}
	
	public PhysicalCertificate  findPhysicalCertificateByIdProgramInterestPk(Long idProgramInterestPk){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT pc ");
		sbQuery.append(" FROM PhysicalCertificate pc, VaultControlComponent vc ");
		sbQuery.append(" WHERE vc.idPhysicalCertificateFk = pc.idPhysicalCertificatePk AND pc.idProgramInterestFk = :idProgramInterestFk ");
		sbQuery.append(" ORDER BY pc.idPhysicalCertificatePk DESC ");
		
		TypedQuery<PhysicalCertificate> query = em.createQuery(sbQuery.toString(), PhysicalCertificate.class);
		query.setParameter("idProgramInterestFk", idProgramInterestPk);
		
		List<PhysicalCertificate> lstReturn = query.getResultList();
		if(lstReturn!=null && lstReturn.size()>0) {
			return lstReturn.get(0);	
		}
		return null;
	}
	
	public PhysicalCertificate  findPhysicalCertificateAmortization(String idSecurityCodePk){
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT pc " );
		sbQuery.append(" FROM PhysicalCertificate pc, VaultControlComponent vc ");
		sbQuery.append(" WHERE vc.idPhysicalCertificateFk = pc.idPhysicalCertificatePk AND pc.securities.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" AND pc.idProgramInterestFk IS NULL ");
		sbQuery.append(" ORDER BY pc.idPhysicalCertificatePk DESC ");
		
		TypedQuery<PhysicalCertificate> query = em.createQuery(sbQuery.toString(), PhysicalCertificate.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);

		List<PhysicalCertificate> lstReturn = query.getResultList();
		if(lstReturn!=null && lstReturn.size()>0) {
			return lstReturn.get(0);	
		}
		return null;
	}

	public RetirementOperation findRetirementOperationWithPhysicalCertificate(Long idPhysicalCertificatePk) {
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ro ");
		sbQuery.append(" FROM RetirementOperation ro ");
		sbQuery.append(" WHERE ro.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
		
		TypedQuery<RetirementOperation> query = em.createQuery(sbQuery.toString(), RetirementOperation.class);
		query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
		
		return query.getSingleResult();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateRetirementDetailCorporativeInterest(Long idCorporativeOperationPk){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER, " );//10
		sbQuery.append(" co.DELIVERY_DATE,  " );//11
		sbQuery.append(" co.ID_CORPORATIVE_OPERATION_PK, " );//12
		sbQuery.append(" co.STATE " );//13
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) ) " );		
		sbQuery.append(" INNER JOIN CONTROL_BOVEDA cb ON cb.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK ");
		sbQuery.append(" INNER JOIN PROGRAM_INTEREST_COUPON pic ON pic.ID_PROGRAM_INTEREST_PK = pc.ID_PROGRAM_INTEREST_FK " ); 
		sbQuery.append(" INNER JOIN CORPORATIVE_OPERATION co ON co.ID_ORIGIN_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND co.ID_PROGRAM_INTEREST_FK = pc.ID_PROGRAM_INTEREST_FK " );
		sbQuery.append(" INNER JOIN CORPORATIVE_PROCESS_RESULT cpr ON cpr.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK AND cpr.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN CORPORATIVE_EVENT_TYPE cet ON cet.ID_CORPORATIVE_EVENT_TYPE_PK IN (14) ");
		sbQuery.append(" WHERE hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 AND hab.TOTAL_BALANCE > 0 " );//TOTAL_BALANCE
		
		if(idCorporativeOperationPk!=null) {
			sbQuery.append(" AND co.ID_CORPORATIVE_OPERATION_PK = :idCorporativeOperationPk " );
		}
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		mapParam.put("idCorporativeOperationPk", idCorporativeOperationPk);
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();

			Date deliveryDate = (Date)objects[11];
			Long idCoporativeOperationPk = ((BigDecimal)objects[12]).longValue();
			Integer CorporativeState = ((BigDecimal)objects[13]).intValue();
			
			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			corporativeOperation.setIdCorporativeOperationPk(idCoporativeOperationPk);
			corporativeOperation.setDeliveryDate(deliveryDate);
			corporativeOperation.setState(CorporativeState);
			corporativeOperation.setEventTypeDescription("PAGO DE INTERES");
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			retirementDetail.setCorporativeOperation(corporativeOperation);
			
			lstRetirementDetail.add(retirementDetail);			
		}
		return lstRetirementDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateRetirementDetailCorporativeInterest(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, Date corporativeDeliveryDate){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER, " );//10
		sbQuery.append(" co.DELIVERY_DATE,  " );//11
		sbQuery.append(" co.ID_CORPORATIVE_OPERATION_PK, " );//12
		sbQuery.append(" co.STATE " );//13
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) ) " );																//AND pc.SITUATION NOT IN (2843,2844)
		sbQuery.append(" INNER JOIN PROGRAM_INTEREST_COUPON pic ON pic.ID_PROGRAM_INTEREST_PK = pc.ID_PROGRAM_INTEREST_FK " ); 
		sbQuery.append(" INNER JOIN CORPORATIVE_OPERATION co ON co.ID_ORIGIN_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND co.ID_PROGRAM_INTEREST_FK = pc.ID_PROGRAM_INTEREST_FK " );
		sbQuery.append(" INNER JOIN CORPORATIVE_PROCESS_RESULT cpr ON cpr.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK AND cpr.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN CORPORATIVE_EVENT_TYPE cet ON cet.ID_CORPORATIVE_EVENT_TYPE_PK IN (14) ");
		if(idHolderPk!=null) {
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had " ); 
			sbQuery.append(" ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK " ); 
			sbQuery.append(" INNER JOIN HOLDER h " );
			sbQuery.append(" ON h.ID_HOLDER_PK = had.ID_HOLDER_FK " ); 
		}
		sbQuery.append(" WHERE hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 " );// AND hab.TOTAL_BALANCE > 0  - en el caso que se amortice capital e interes, no apareceria ya que su total es 0, ver otra forma de validar
		//sbQuery.append(" AND pc.IND_ELECTRONIC_CUPON = 0 " );
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND pc.ID_ISSUER_FK = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND pc.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.ID_PARTICIPANT_PK = :idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.ID_HOLDER_PK = :idHolderPk " );
		}
		sbQuery.append(" AND co.DELIVERY_DATE = :corporativeDeliveryDate " );
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		//List<Integer> lstSituationNoIn = new ArrayList<Integer>();
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		mapParam.put("corporativeDeliveryDate", corporativeDeliveryDate);
		
		
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();

			Date deliveryDate = (Date)objects[11];
			Long idCoporativeOperationPk = ((BigDecimal)objects[12]).longValue();
			Integer CorporativeState = ((BigDecimal)objects[13]).intValue();
			
			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			corporativeOperation.setIdCorporativeOperationPk(idCoporativeOperationPk);
			corporativeOperation.setDeliveryDate(deliveryDate);
			corporativeOperation.setState(CorporativeState);
			corporativeOperation.setEventTypeDescription("PAGO DE INTERES");
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			retirementDetail.setCorporativeOperation(corporativeOperation);
			
			lstRetirementDetail.add(retirementDetail);			
		}
		return lstRetirementDetail;
	}
	

	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateRetirementGuardExclusiveInterest(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, Date expirationPaymentDate){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER, " );//10
		sbQuery.append(" pic.EXPERITATION_DATE " );//11
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) AND pc.IND_CUPON = 1 ) " );
		sbQuery.append(" INNER JOIN PROGRAM_INTEREST_COUPON pic ON pic.ID_PROGRAM_INTEREST_PK = pc.ID_PROGRAM_INTEREST_FK ");
		sbQuery.append(" INNER JOIN SECURITY s ON s.ID_SECURITY_CODE_PK = pc.ID_SECURITY_CODE_FK " ); 

		if(idHolderPk!=null) {
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had " ); 
			sbQuery.append(" ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK " ); 
			sbQuery.append(" INNER JOIN HOLDER h " );
			sbQuery.append(" ON h.ID_HOLDER_PK = had.ID_HOLDER_FK " ); 
		}
		sbQuery.append(" WHERE s.STATE_SECURITY in (:stateSecurity) " );// hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 AND hab.TOTAL_BALANCE > 0  - en el caso que se amortice capital e interes, no apareceria ya que su total es 0, ver otra forma de validar
		//sbQuery.append(" AND pc.IND_ELECTRONIC_CUPON = 0 " );
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND pc.ID_ISSUER_FK = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND pc.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.ID_PARTICIPANT_PK = :idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.ID_HOLDER_PK = :idHolderPk " );
		}
		sbQuery.append(" AND pic.PAYMENT_DATE = :corporativeDeliveryDate " );
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		//List<Integer> lstSituationNoIn = new ArrayList<Integer>();
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		mapParam.put("corporativeDeliveryDate", expirationPaymentDate);
		

		List<Integer> lstStateSecurity = new ArrayList<Integer>();
		lstStateSecurity.add(SecurityStateType.GUARDA_EXCLUSIVE.getCode());
		lstStateSecurity.add(SecurityStateType.GUARDA_EXCLUSIVE_EXPIRATION.getCode());
		mapParam.put("stateSecurity", lstStateSecurity);
		
		
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();

			Date deliveryDate = (Date)objects[11];
			
			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			corporativeOperation.setDeliveryDate(deliveryDate);
			corporativeOperation.setEventTypeDescription("PAGO DE INTERES");
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			retirementDetail.setCorporativeOperation(corporativeOperation);
			
			
			lstRetirementDetail.add(retirementDetail);			
		}
		return lstRetirementDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateRetirementDetailCorporativeAmoritzation(Long idCorporativeOperationPk){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER, " );//10
		sbQuery.append(" co.DELIVERY_DATE,  " );//11
		sbQuery.append(" co.ID_CORPORATIVE_OPERATION_PK, " );//12
		sbQuery.append(" co.STATE " );//13
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) ) " );		 	
		sbQuery.append(" INNER JOIN CONTROL_BOVEDA cb ON cb.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK ");
		sbQuery.append(" INNER JOIN CORPORATIVE_OPERATION co ON co.ID_ORIGIN_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.ID_PROGRAM_INTEREST_FK IS NULL AND co.ID_PROGRAM_INTEREST_FK IS NULL AND co.ID_PROGRAM_AMORTIZATION_FK IS NOT NULL " );
		sbQuery.append(" INNER JOIN CORPORATIVE_PROCESS_RESULT cpr ON cpr.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK AND cpr.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK "); 
		sbQuery.append(" INNER JOIN CORPORATIVE_EVENT_TYPE cet ON cet.ID_CORPORATIVE_EVENT_TYPE_PK IN (15) ");
		sbQuery.append(" WHERE 1 = 1 " );//hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 AND hab.TOTAL_BALANCE > 0
		
		if(idCorporativeOperationPk!=null) {
			sbQuery.append(" AND co.ID_CORPORATIVE_OPERATION_PK = :idCorporativeOperationPk " );
		}
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		if(idCorporativeOperationPk!=null) {
			mapParam.put("idCorporativeOperationPk", idCorporativeOperationPk);
		}
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();
			

			Date deliveryDate = (Date)objects[11];
			Long idCoporativeOperationPk = ((BigDecimal)objects[12]).longValue();
			Integer CorporativeState = ((BigDecimal)objects[13]).intValue();
			

			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			corporativeOperation.setIdCorporativeOperationPk(idCoporativeOperationPk);
			corporativeOperation.setDeliveryDate(deliveryDate);
			corporativeOperation.setState(CorporativeState);
			corporativeOperation.setEventTypeDescription("AMORTIZACION DE CAPITAL");
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setCorporativeOperation(corporativeOperation);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			
			lstRetirementDetail.add(retirementDetail);			
		}
		return lstRetirementDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateRetirementDetailCorporativeAmoritzation(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, Date corporativeDeliveryDate){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER, " );//10
		sbQuery.append(" co.DELIVERY_DATE,  " );//11
		sbQuery.append(" co.ID_CORPORATIVE_OPERATION_PK, " );//12
		sbQuery.append(" co.STATE " );//13
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) ) " );		 
		sbQuery.append(" INNER JOIN CORPORATIVE_OPERATION co ON co.ID_ORIGIN_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.ID_PROGRAM_INTEREST_FK IS NULL AND co.ID_PROGRAM_INTEREST_FK IS NULL AND co.ID_PROGRAM_AMORTIZATION_FK IS NOT NULL " );
		sbQuery.append(" INNER JOIN CORPORATIVE_PROCESS_RESULT cpr ON cpr.ID_CORPORATIVE_OPERATION_FK = co.ID_CORPORATIVE_OPERATION_PK AND cpr.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK "); 
		sbQuery.append(" INNER JOIN CORPORATIVE_EVENT_TYPE cet ON cet.ID_CORPORATIVE_EVENT_TYPE_PK IN (15) ");
		if(idHolderPk!=null) {
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had " ); 
			sbQuery.append(" ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK " ); 
			sbQuery.append(" INNER JOIN HOLDER h " );
			sbQuery.append(" ON h.ID_HOLDER_PK = had.ID_HOLDER_FK " ); 
		}
		sbQuery.append(" WHERE 1 = 1 " );//hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 AND hab.TOTAL_BALANCE > 0
		//sbQuery.append(" AND pc.IND_ELECTRONIC_CUPON = 0 " );
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND pc.ID_ISSUER_FK = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND pc.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.ID_PARTICIPANT_PK = :idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.ID_HOLDER_PK = :idHolderPk " );
		}
		sbQuery.append(" AND co.DELIVERY_DATE = :corporativeDeliveryDate " );
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		//List<Integer> lstSituationNoIn = new ArrayList<Integer>();
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		mapParam.put("corporativeDeliveryDate", corporativeDeliveryDate);
		
		
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();
			

			Date deliveryDate = (Date)objects[11];
			Long idCoporativeOperationPk = ((BigDecimal)objects[12]).longValue();
			Integer CorporativeState = ((BigDecimal)objects[13]).intValue();
			

			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			corporativeOperation.setIdCorporativeOperationPk(idCoporativeOperationPk);
			corporativeOperation.setDeliveryDate(deliveryDate);
			corporativeOperation.setState(CorporativeState);
			corporativeOperation.setEventTypeDescription("AMORTIZACION DE CAPITAL");
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setCorporativeOperation(corporativeOperation);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			
			lstRetirementDetail.add(retirementDetail);			
		}
		return lstRetirementDetail;
	}
	

	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateRetirementGuardExclusiveAmoritzation(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, Date expirationPaymentDate){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER, " );//10
		sbQuery.append(" pac.PAYMENT_DATE  " );//11
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) AND pc.IND_CUPON = 0 ) " );		 												
		sbQuery.append(" INNER JOIN AMORTIZATION_PAYMENT_SCHEDULE aps ON pc.ID_SECURITY_CODE_FK = aps.ID_SECURITY_CODE_FK " );
		sbQuery.append(" INNER JOIN PROGRAM_AMORTIZATION_COUPON pac ON pac.ID_AMO_PAYMENT_SCHEDULE_FK = aps.ID_AMO_PAYMENT_SCHEDULE_PK " );
		sbQuery.append(" INNER JOIN SECURITY s ON s.ID_SECURITY_CODE_PK = pc.ID_SECURITY_CODE_FK " ); 
		
		if(idHolderPk!=null) {
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had " ); 
			sbQuery.append(" ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK " ); 
			sbQuery.append(" INNER JOIN HOLDER h " );
			sbQuery.append(" ON h.ID_HOLDER_PK = had.ID_HOLDER_FK " ); 
		}
		sbQuery.append(" WHERE 1 = 1 AND s.STATE_SECURITY in (:stateSecurity) " );
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND pc.ID_ISSUER_FK = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND pc.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.ID_PARTICIPANT_PK = :idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.ID_HOLDER_PK = :idHolderPk " );
		}
		sbQuery.append(" AND pac.PAYMENT_DATE = :corporativeDeliveryDate " );
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		List<Integer> lstSecurityStateIn = new ArrayList<Integer>();
		lstSecurityStateIn.add(SecurityStateType.GUARDA_EXCLUSIVE.getCode());
		lstSecurityStateIn.add(SecurityStateType.GUARDA_EXCLUSIVE_EXPIRATION.getCode());
		mapParam.put("stateSecurity", lstSecurityStateIn);
		
		//List<Integer> lstSituationNoIn = new ArrayList<Integer>();
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		mapParam.put("corporativeDeliveryDate", expirationPaymentDate);
		
		
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();
			Date deliveryDate = (Date)objects[11];
			

			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			corporativeOperation.setDeliveryDate(deliveryDate);
			corporativeOperation.setEventTypeDescription("AMORTIZACION DE CAPITAL");
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setCorporativeOperation(corporativeOperation);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			
			lstRetirementDetail.add(retirementDetail);			
		}
		return lstRetirementDetail;
	}
	

	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateVariableIncomeRetirementDetailAvailable(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER,  " );//10
		sbQuery.append(" hab.TRANSIT_BALANCE,  " );//11
		sbQuery.append(" aao.TOTAL_BALANCE as total_balance_annotation,  " );//12
		sbQuery.append(" aao.CERTIFICATE_FROM,  " );//13
		sbQuery.append(" aao.CERTIFICATE_TO  " );//14
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) ) " );	//AND pc.SITUATION NOT IN (2843,2844)
		sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao " );
		sbQuery.append(" on (aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK AND aao.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK ) " );
		
		if(idHolderPk!=null) {
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had " ); 
			sbQuery.append(" ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK " ); 
			sbQuery.append(" INNER JOIN HOLDER h " );
			sbQuery.append(" ON h.ID_HOLDER_PK = had.ID_HOLDER_FK " ); 
		}
		sbQuery.append(" WHERE hab.TOTAL_BALANCE > 0 " );//hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 AND TOTAL_BALANCE
		//sbQuery.append(" AND pc.IND_ELECTRONIC_CUPON = 0 " ); // aca puede ser el problema
		sbQuery.append(" AND pc.INSTRUMENT_TYPE = :instrumentType " );
		sbQuery.append(" AND aao.STATE_ANNOTATION = :aaoState " );
		
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND pc.ID_ISSUER_FK = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND pc.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.ID_PARTICIPANT_PK = :idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.ID_HOLDER_PK = :idHolderPk " );
		}
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		//List<Integer> lstSituationNoIn = new ArrayList<Integer>();
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}

		mapParam.put("instrumentType", InstrumentType.VARIABLE_INCOME.getCode());
		mapParam.put("aaoState", DematerializationStateType.CONFIRMED.getCode());
		
		
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			BigDecimal transitBalance = (BigDecimal)objects[11];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();
			BigDecimal totalBalanceAnnotation = (BigDecimal)objects[12];
			Integer annotationFrom = (objects[13]!=null)?((BigDecimal)objects[13]).intValue():0;
			Integer annotationTo = (objects[14]!=null)?((BigDecimal)objects[14]).intValue():0;

			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setParticipantBuyer(new Participant());
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			retirementDetail.setTotalBalanceAnnotation(totalBalanceAnnotation);
			retirementDetail.setAnnotationFromTo(annotationFrom+" - "+annotationTo);
			
			if(availableBalance.compareTo(physicalCertificate.getCertificateQuantity()) >=0) {
				lstRetirementDetail.add(retirementDetail);			
			}
		}
		return lstRetirementDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<RetirementDetail> generateFixedIncomeRetirementDetailAvailable(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			

		sbQuery.append(" SELECT DISTINCT " );
		sbQuery.append(" hab.TOTAL_BALANCE, " );//0
		sbQuery.append(" hab.AVAILABLE_BALANCE, " );//1
		sbQuery.append(" hab.PAWN_BALANCE, " );//2
		sbQuery.append(" hab.OTHER_BLOCK_BALANCE, " );//3
		sbQuery.append(" hab.BAN_BALANCE, " );//4
		sbQuery.append(" hab.ID_PARTICIPANT_PK , " );//5
		sbQuery.append(" hab.ID_HOLDER_ACCOUNT_PK , " );//6
		sbQuery.append(" hab.ID_SECURITY_CODE_PK , " );//7
		sbQuery.append(" pc.ID_ISSUER_FK, " );//8
		sbQuery.append(" pc.ID_PHYSICAL_CERTIFICATE_PK,  " );//9
		sbQuery.append(" pc.CUPON_NUMBER,  " );//10
		sbQuery.append(" hab.TRANSIT_BALANCE  " );//11
		sbQuery.append(" FROM HOLDER_ACCOUNT_BALANCE hab " );
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc " );
		sbQuery.append(" on ( pc.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK AND pc.STATE NOT IN (:lstStateNoIn) ) " );	//AND pc.SITUATION NOT IN (2843,2844)
		if(idHolderPk!=null) {
			sbQuery.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had " ); 
			sbQuery.append(" ON had.ID_HOLDER_ACCOUNT_FK = hab.ID_HOLDER_ACCOUNT_PK " ); 
			sbQuery.append(" INNER JOIN HOLDER h " );
			sbQuery.append(" ON h.ID_HOLDER_PK = had.ID_HOLDER_FK " ); 
		}
		sbQuery.append(" WHERE hab.TRANSIT_BALANCE = 0 AND hab.TOTAL_BALANCE > 0 " );//hab.PAWN_BALANCE = 0 AND hab.OTHER_BLOCK_BALANCE = 0 AND hab.BAN_BALANCE = 0 AND hab.TRANSIT_BALANCE = 0 AND TOTAL_BALANCE
		//sbQuery.append(" AND pc.IND_ELECTRONIC_CUPON = 0 " ); // aca puede ser el problema
		sbQuery.append(" AND pc.INSTRUMENT_TYPE = :instrumentType " );
		
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND pc.ID_ISSUER_FK = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.ID_SECURITY_CODE_PK = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND pc.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.ID_PARTICIPANT_PK = :idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.ID_HOLDER_PK = :idHolderPk " );
		}
		 
		sbQuery.append(" ORDER BY hab.ID_SECURITY_CODE_PK, pc.CUPON_NUMBER DESC ");

		List<Integer> lstStateNoIn = new ArrayList<Integer>();
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
		lstStateNoIn.add(PhysicalCertificateStateType.RETIREMENT.getCode());
		mapParam.put("lstStateNoIn", lstStateNoIn);
		
		//List<Integer> lstSituationNoIn = new ArrayList<Integer>();
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		
		mapParam.put("instrumentType", InstrumentType.FIXED_INCOME.getCode());
		
		
		
        List<Object> objectList = findByNativeQuery(sbQuery.toString(), mapParam);
		List<RetirementDetail> lstRetirementDetail = new ArrayList<RetirementDetail>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			
			//BigDecimal totalBalance = (BigDecimal)objects[0];
			BigDecimal availableBalance = (BigDecimal)objects[1];
			BigDecimal pawnBalance = (BigDecimal)objects[2];
			BigDecimal otherBlockBalance = (BigDecimal)objects[3];
			BigDecimal banBalance = (BigDecimal)objects[4];
			BigDecimal transitBalance = (BigDecimal)objects[11];
			Long idParticipantQueryPk = ((BigDecimal)objects[5]).longValue();
			Long idHolderAccountQueryPk = ((BigDecimal)objects[6]).longValue();
			String idSecurityQueryPk = (String)objects[7];
			String idIssuerQueryPk = (String)objects[8];
			Long idPhysicalCertificateQueryPk = ((BigDecimal)objects[9]).longValue();

			Participant participant = find(Participant.class, idParticipantQueryPk);
			Security security = find(Security.class, idSecurityQueryPk);
			Issuer issuer =find(Issuer.class, idIssuerQueryPk);
			PhysicalCertificate physicalCertificate = find(PhysicalCertificate.class, idPhysicalCertificateQueryPk);
			HolderAccount holderAccount = getHolderAccountAndChilds(idHolderAccountQueryPk);
			
			RetirementDetail retirementDetail = new RetirementDetail();
			retirementDetail.setAvailableBalance(availableBalance);
			retirementDetail.setPawnBalance(pawnBalance);
			retirementDetail.setOtherBlockBalance(otherBlockBalance);
			retirementDetail.setBanBalance(banBalance);
			retirementDetail.setParticipant(participant);
			retirementDetail.setParticipantBuyer(new Participant());
			retirementDetail.setHolderAccount(holderAccount);
			retirementDetail.setSecurity(security);
			retirementDetail.setIssuer(issuer);
			retirementDetail.setPhysicalCertificate(physicalCertificate);
			
			lstRetirementDetail.add(retirementDetail);			
			
		}
		return lstRetirementDetail;
	}
	
	public HolderAccount getHolderAccountAndChilds(Long idHolderAccountPk) {
		
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();	

		sbQuery.append(" SELECT ha FROM HolderAccount ha ");
		sbQuery.append(" JOIN FETCH ha.holderAccountDetails had ");
		sbQuery.append(" JOIN FETCH had.holder h ");
		sbQuery.append(" WHERE ");
		sbQuery.append(" ha.idHolderAccountPk =: idHolderAccountPk " );
		mapParam.put("idHolderAccountPk", idHolderAccountPk);
		
		return (HolderAccount) findObjectByQueryString(sbQuery.toString(), mapParam);
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<TransferAccountBalanceTO> getHolderAccountsBalancesWithRetirement(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			
		
		sbQuery.append("SELECT" );
		sbQuery.append("	pa.idParticipantPk," );				// [0]
		sbQuery.append("	pa.mnemonic,	" );				// [1]
		sbQuery.append("	hab.id.idHolderAccountPk, " );		// [2]
		sbQuery.append("	ha, " );							// [3]
		sbQuery.append("	hab.security.idSecurityCodePk," );	// [4]
		sbQuery.append("	hab, " );							// [5]
		sbQuery.append("	sec.securityClass " );				// [6]
		sbQuery.append("  FROM  HolderAccountBalance hab ");
		sbQuery.append("  inner join hab.holderAccount ha ");
		if(idHolderPk!=null) {
			sbQuery.append("  inner join ha.holderAccountDetails had ");
			sbQuery.append("  inner join had.holder h ");
		}
		sbQuery.append("  inner join hab.participant pa ");
		sbQuery.append("  inner join hab.security sec ");
		sbQuery.append("  inner join sec.issuer i ");
		sbQuery.append(" WHERE ");
		sbQuery.append(" hab.availableBalance > 0 ");
		sbQuery.append(" AND ( select count(1) from PhysicalCertificate pc where pc.securities.idSecurityCodePk = hab.security.idSecurityCodePk ) > 0 ");
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND i.idIssuerPk = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.security.idSecurityCodePk = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND hab.security.currency = :currency ");
		}

		if(idHolderAccountPk!=null) {
			sbQuery.append(" AND hab.id.idHolderAccountPk =: idHolderAccountPk " );
		}
		if(idParticipantPk!=null) {
			sbQuery.append(" AND hab.id.idParticipantPk =: idParticipantPk " );
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.idHolderPk =: idHolderPk " );
		}
		
		sbQuery.append(" ORDER BY hab.security.idSecurityCodePk DESC ");
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderAccountPk!=null) {
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		if(idParticipantPk!=null) {
			mapParam.put("idParticipantPk", idParticipantPk);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		
		
		
        List<Object> objectList = findListByQueryString(sbQuery.toString(), mapParam);
		List<TransferAccountBalanceTO> holderAccountBalanceTOs = new ArrayList<TransferAccountBalanceTO>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			TransferAccountBalanceTO holderAccountBalanceTO = new TransferAccountBalanceTO();
			holderAccountBalanceTO.setIdParticipantPk((Long)objects[0]);
			holderAccountBalanceTO.setParticipantMnemonic((String)objects[1]);
			
			holderAccountBalanceTO.setIdHolderAccountPk((Long)objects[2]);
			
			HolderAccount holderAccount = (HolderAccount) objects[3];
			holderAccountBalanceTO.setAccountNumber(holderAccount.getAccountNumber());						
//			holderAccountBalanceTO.setHolderAccountState(holderAccount.getStateAccount());
			holderAccountBalanceTO.setAlternateCode(holderAccount.getAlternateCode());
			
			holderAccountBalanceTO.setIdSecurityCodePk((String)objects[4]);
			holderAccountBalanceTO.setHolderAccountBalance((HolderAccountBalance)objects[5]);
			
			List<PhysicalCertificate> lstPhysicalCertificate = getListPhysicalCertificate(holderAccountBalanceTO.getIdSecurityCodePk(),null);
			holderAccountBalanceTO.setLstPhysicalCertificate(lstPhysicalCertificate);
			
			if(holderAccountBalanceTO.getHolderAccountBalance() != null){
				/*if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance())){
					if(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance().size() > 1){
						holderAccountBalanceTO.setMarketLock(true);
					} else 
						holderAccountBalanceTO.setMarketLock(false);
				}*/				
			}
			
			holderAccountBalanceTO.setSecurityClass((Integer) objects[6]);
			
			holderAccountBalanceTOs.add(holderAccountBalanceTO);			
		}
		return holderAccountBalanceTOs;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TransferAccountBalanceTO> getHolderAccountsBalances(String idIssuerPk, String idSecurityCode, Integer currency, Long idHolderPk){
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();			
		
		sbQuery.append("SELECT" );
		sbQuery.append("	pa.idParticipantPk," );				// [0]
		sbQuery.append("	pa.mnemonic,	" );				// [1]
		sbQuery.append("	hab.id.idHolderAccountPk, " );		// [2]
		sbQuery.append("	ha, " );							// [3]
		sbQuery.append("	hab.security.idSecurityCodePk," );	// [4]
		sbQuery.append("	hab, " );							// [5]
		sbQuery.append("	sec.securityClass " );				// [6]
		sbQuery.append("  FROM  HolderAccountBalance hab ");
		sbQuery.append("  inner join hab.holderAccount ha ");
		if(idHolderPk!=null) {
			sbQuery.append("  inner join ha.holderAccountDetails had ");
			sbQuery.append("  inner join had.holder h ");
		}
		sbQuery.append("  inner join hab.participant pa ");
		sbQuery.append("  inner join hab.security sec ");
		sbQuery.append("  inner join sec.issuer i ");
		sbQuery.append(" WHERE ");
		sbQuery.append(" hab.availableBalance > 0 ");
		sbQuery.append(" AND ( select count(1) from PhysicalCertificate pc where pc.securities.idSecurityCodePk = hab.security.idSecurityCodePk ) > 0 ");
		
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			sbQuery.append(" AND i.idIssuerPk = :idIssuerPk ");
		}
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			sbQuery.append(" AND hab.security.idSecurityCodePk = :idSecurityCode ");
		}
		if(currency!=null) {
			sbQuery.append(" AND hab.security.currency = :currency ");
		}
		if(idHolderPk!=null) {
			sbQuery.append(" AND h.idHolderPk =: idHolderPk " );
		}
		
		sbQuery.append(" ORDER BY hab.security.idSecurityCodePk DESC ");
		
		if(idSecurityCode!=null && !idSecurityCode.equals("")) {
			mapParam.put("idSecurityCode", idSecurityCode);
		}
		if(idIssuerPk!=null && !idIssuerPk.equals("")) {
			mapParam.put("idIssuerPk", idIssuerPk);
		}
		if(currency!=null) {
			mapParam.put("currency", currency);
		}
		if(idHolderPk!=null) {
			mapParam.put("idHolderPk", idHolderPk);
		}
		
		
        List<Object> objectList = findListByQueryString(sbQuery.toString(), mapParam);
		List<TransferAccountBalanceTO> holderAccountBalanceTOs = new ArrayList<TransferAccountBalanceTO>();
		//Parsing the result
		for(int i = 0; i < objectList.size();++i){
			Object[] objects =  (Object[]) objectList.get(i);
			TransferAccountBalanceTO holderAccountBalanceTO = new TransferAccountBalanceTO();
			holderAccountBalanceTO.setIdParticipantPk((Long)objects[0]);
			holderAccountBalanceTO.setParticipantMnemonic((String)objects[1]);
			
			holderAccountBalanceTO.setIdHolderAccountPk((Long)objects[2]);
			
			HolderAccount holderAccount = (HolderAccount) objects[3];
			holderAccountBalanceTO.setAccountNumber(holderAccount.getAccountNumber());						
//			holderAccountBalanceTO.setHolderAccountState(holderAccount.getStateAccount());
			holderAccountBalanceTO.setAlternateCode(holderAccount.getAlternateCode());
			
			holderAccountBalanceTO.setIdSecurityCodePk((String)objects[4]);
			holderAccountBalanceTO.setHolderAccountBalance((HolderAccountBalance)objects[5]);
			
			List<PhysicalCertificate> lstPhysicalCertificate = getListPhysicalCertificate(holderAccountBalanceTO.getIdSecurityCodePk(),null);
			holderAccountBalanceTO.setLstPhysicalCertificate(lstPhysicalCertificate);
			
			if(holderAccountBalanceTO.getHolderAccountBalance() != null){
				/*if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance())){
					if(holderAccountBalanceTO.getHolderAccountBalance().getHolderMarketfactBalance().size() > 1){
						holderAccountBalanceTO.setMarketLock(true);
					} else 
						holderAccountBalanceTO.setMarketLock(false);
				}*/				
			}
			
			holderAccountBalanceTO.setSecurityClass((Integer) objects[6]);
			
			holderAccountBalanceTOs.add(holderAccountBalanceTO);			
		}
		return holderAccountBalanceTOs;
	}
	/**
	 * Search retirement operation for batch.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<RetirementOperation> searchRetirementOperationForBatch(RetirementOperationTO retirementOperationTO){
		Map<String,Object> mapParam = new HashMap<String,Object>();
		//String builder
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("SELECT ");
		sbQuery.append("       RO.idRetirementOperationPk, ");		
		sbQuery.append("       RO.custodyOperation.registryDate,");		
		sbQuery.append("       RO.motive,");		
		sbQuery.append("       RO.state " );
		sbQuery.append("  FROM RetirementOperation RO");
		sbQuery.append(" WHERE 1=1");
		
		if(Validations.validateIsNotNull(retirementOperationTO.getEndDate())){
			sbQuery.append(" AND RO.custodyOperation.registryDate <=:endDate ");
			mapParam.put("endDate", retirementOperationTO.getEndDate());
		}
		
		if(Validations.validateIsNotNull(retirementOperationTO.getState())){
			sbQuery.append(" AND RO.state =:registeredState or RO.state =:approvedState");
			mapParam.put("registeredState", RetirementOperationStateType.REGISTERED.getCode());
			mapParam.put("approvedState", RetirementOperationStateType.APPROVED.getCode());
		}
		
		if(Validations.validateIsNotNull(retirementOperationTO.getMotive())){
			sbQuery.append("AND RO.motive =:motive");
			mapParam.put("motive", retirementOperationTO.getMotive());
		}
		
		
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        
        List<RetirementOperation> retirementOperationLst = new ArrayList<RetirementOperation>();
		List<Object> objectsList = query.getResultList();
		
		// Parsing the result
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			RetirementOperation tmpRetirementOperation = new RetirementOperation();
			
			tmpRetirementOperation.setIdRetirementOperationPk((Long)objects[0]);
			
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setIdCustodyOperationPk((Long)objects[0]);
			custodyOperation.setRegistryDate((Date)objects[1]);
			
			tmpRetirementOperation.setCustodyOperation(custodyOperation);
			tmpRetirementOperation.setMotive((Integer)objects[3]);
			tmpRetirementOperation.setState((Integer)objects[4]);
			retirementOperationLst.add(tmpRetirementOperation);						
		}
		return retirementOperationLst;
	}

	/**
	 * Find retirment operation.
	 *
	 * @param idRetirementOperationPk the id retirement operation pk
	 * @return the retirement operation
	 * @throws ServiceException the service exception
	 */
	public RetirementOperation findRetirmentOperation(Long idRetirementOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro ");
			sbQuery.append("	FROM RetirementOperation ro");
			sbQuery.append("	INNER JOIN FETCH ro.security");
			sbQuery.append("	left JOIN FETCH ro.targetSecurity");
			sbQuery.append("	INNER JOIN FETCH ro.retirementDetails rds");
			sbQuery.append("	INNER JOIN FETCH ro.accountAnnotationOperation aao");
			sbQuery.append("	INNER JOIN FETCH rds.participant pa");
			sbQuery.append("	INNER JOIN FETCH rds.holderAccount ha");
			sbQuery.append("	INNER JOIN FETCH rds.physicalCertificate pc");
			sbQuery.append("	INNER JOIN FETCH ro.holderAccount ha2");
			sbQuery.append("	WHERE ro.idRetirementOperationPk = :idRetirementOperationPk");
			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			RetirementOperation result= (RetirementOperation)query.getSingleResult();

			return result;
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	/**
	 * metodo para obtener la descripcion del holder(cui-fullname) desde el id: HolderAccount(cuenta titular)
	 * @param idHolderAccountPk
	 * @return null si no se encotraron resultados
	 */
	public String getDescriptionHolderFromIdHolderAccount(Long idHolderAccountPk){
		String result=null;
		StringBuilder sd=new StringBuilder();
		sd.append(" select listagg((select h.ID_HOLDER_PK || '-' || h.FULL_NAME from HOLDER h where h.ID_HOLDER_PK = HAD.ID_HOLDER_FK), '<br/> ')");
		sd.append(" within group (order by had.ID_HOLDER_FK) ");
		sd.append(" from HOLDER_ACCOUNT_DETAIL had   ");
		sd.append(" where ");
		sd.append(" had.ID_HOLDER_ACCOUNT_FK = :idHolderAccountPk ");
		try {
			Query q=em.createNativeQuery(sd.toString());
			q.setParameter("idHolderAccountPk", idHolderAccountPk);
			Object obj=q.getSingleResult();
			if(obj!=null)
				result=obj.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Find retirement market fact by retirement.
	 *
	 * @param idRetirementDetail the id retirement detail
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<RetirementMarketfact> findRetirementMarketFactByRetirement(long idRetirementDetail){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idRetiementDetailPrm", idRetirementDetail);
		return (List<RetirementMarketfact>)findWithNamedQuery(RetirementMarketfact.RETIREMENT_MARKETFACT_BY_RETIREMENT_DET,parameters);
	}

	
	/**
	 * Search retirement operation by revert.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @param lstStates the lst states
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<RetirementOperation> searchRetirementOperationByRevert(RetirementOperationTO retirementOperationTO, List<Integer> lstStates){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT RO	");		
		sbQuery.append("	FROM RetirementOperation RO	");
		sbQuery.append(" 	INNER JOIN FETCH RO.retirementDetails RTD	");
		sbQuery.append(" 	INNER JOIN FETCH RO.security SC	");
		sbQuery.append(" 	WHERE 1=1	");
		sbQuery.append(" 	AND trunc(RO.custodyOperation.registryDate) = :endDate ");
		sbQuery.append(" 	AND RO.state IN :lststate ");		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("endDate", retirementOperationTO.getEndDate());
		query.setParameter("lststate", lstStates);
		return (List<RetirementOperation>)query.getResultList();	
	}
	
	/**
	 * isue 1239: este metodo es replica del metodo validateAndSaveEarlyPaymentTx, la diferencia esque tiene modificaciones de acuerso a los requerimientos del issue
	 * @param stockEarlyPaymentRegisterTO
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveEarlyPaymentNoDPFTx(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO, LoggerUser loggerUser) throws ServiceException{
		
		Object objResult = validateAndSaveEarlyPaymentNoDPF(stockEarlyPaymentRegisterTO, loggerUser);
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * Issue 1239: replica del metodo validateAndSaveEarlyPayment con la diferencia que se hace modificaciones  de acuerdo a requerimiento del issue
	 * @param stockEarlyPaymentRegisterTO
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public Object validateAndSaveEarlyPaymentNoDPF(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO, LoggerUser loggerUser) 
			throws ServiceException {
		
		//CHECK OPERATION TYPE
		AccountAnnotationOperation accountAnnotationOperation = null;
		
		if(!(ComponentConstant.INTERFACE_EARLY_PAYMENT_NO_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode()) ||
				ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode()))){			
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		//VALIDATE PARTICIPANT
		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(stockEarlyPaymentRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);		
		if(stockEarlyPaymentRegisterTO.isDpfInterface()){
			if (objParticipant == null ) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			} else {
				stockEarlyPaymentRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			}
		} else {
			if(objParticipant == null  || ( !objParticipant.getIdParticipantPk().equals(idParticipantBC) 
					&& !objParticipant.getIdParticipantPk().equals(idParticipantVUN)) ){
				objParticipant = null;
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			} else {
				
				// Validar por numero de obligacion, si es que viene en el archivo
				
				if(Validations.validateIsNotNullAndNotEmpty(stockEarlyPaymentRegisterTO.getObligationNumber())){
					accountAnnotationOperation = findOriginalAccountAnnotation(stockEarlyPaymentRegisterTO.getObligationNumber(),objParticipant.getIdParticipantPk());
					if(accountAnnotationOperation == null){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_OBLIGATION_NUMBER_ORIGIN, null);
					}
				} 								
				stockEarlyPaymentRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			}
		}	
		
		//CHECK OPERATION NUMBER EXITS
		
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), stockEarlyPaymentRegisterTO.getOperationNumber(), 
						stockEarlyPaymentRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		//CHECK OPERATION DATE IS TODAY
		
		Date operationDate = CommonsUtilities.truncateDateTime(stockEarlyPaymentRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SEND_DATE, null);
		}

		// VALIDATE SECURITY CLASS
		
		SecurityObjectTO securityObjectTO = stockEarlyPaymentRegisterTO.getSecurityObjectTO();		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
			filter.setText1(securityObjectTO.getSecurityClassDesc().trim());
		} else {
			filter.setParameterTablePk(securityObjectTO.getSecurityClass());
		}				
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){
				if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode())//DPA - issue 158
						&&!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPA.getCode())
						){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
				} else {
					securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
					securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
				}
			} else {
				if(!ComponentConstant.ONE.equals(securityClassParam.getIndicator6())){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);	
				} else {
					securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
					securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
				}
			}			
		} else {								
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
		}

		//VALIDATE SECURITY EXISTS
		
		String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
		Security objSecurity = new Security();		 
		if(idSecurityCode != null){
			objSecurity = find(Security.class, idSecurityCode);
			logger.debug("Id Security Code: " + idSecurityCode);
		}		
		if(objSecurity == null){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE, null);
		} else {
			stockEarlyPaymentRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
			
			// VALIDATE EARLY PAYMENT DPF
			
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){								
				
				// VALIDATE STATE SECURITY
				
				if(!SecurityStateType.REGISTERED.getCode().equals(objSecurity.getStateSecurity())){				
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
							new Object[]{SecurityStateType.lookup.get(objSecurity.getStateSecurity()).getValue()});
				}
				
				// VALIDATE INDICATOR EARLY REDEMTION
				
				if(!GeneralConstants.ONE_VALUE_INTEGER.equals(objSecurity.getIndEarlyRedemption())){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INDICATOR_EARLY_PAYMENT, null);
				}
				
				// VALIDATE EXPIRATION DATE SECURITY
				
				if(Validations.validateIsNotNull(securityObjectTO.getExpirationDate()) && 
						Validations.validateIsNotNull(objSecurity.getExpirationDate())){
					if(CommonsUtilities.convertDatetoString(objSecurity.getExpirationDate()).compareTo(
							CommonsUtilities.convertDatetoString(securityObjectTO.getExpirationDate())) != 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
					}
				} else  {
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
				}
				
				// VALIDATE NUMBER COUPONS
				
				boolean blValidateCupons = false;
				if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
					blValidateCupons = true;
				} else {
					if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
						blValidateCupons = true;
					}
				}
				//DPA - issue 158
				if(!securityObjectTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
					if(blValidateCupons){
						if(!objSecurity.getNumberCoupons().equals(1)){
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
						}
					} else {
						if(!objSecurity.getNumberCoupons().equals(securityObjectTO.getNumberCoupons())){
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
						}
					}
				}
				// EXPIRATION OPERATION ACCREDITATION
				
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
				Security objSecurityAccreditation = new Security();
				objSecurityAccreditation.setIdSecurityCodePk(objSecurity.getIdSecurityCodePk());
				accreditationOperationTO.setSecurity(objSecurityAccreditation);						
				accreditationfacadeBean.expirationAcreditationByDpf(accreditationOperationTO);
				
			}
			
			// VALIDATE EXPIRATION DATE SECURIIES
			
			if(Validations.validateIsNotNull(objSecurity.getExpirationDate())){
				if(CommonsUtilities.currentDate().compareTo(objSecurity.getExpirationDate()) == 0){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_RETIREMENT, null);
				}
				// issue 1239: validando que la fecha de vencimiento no sea menor a la fecha actual
				if(CommonsUtilities.currentDate().compareTo(objSecurity.getExpirationDate()) > 0){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_RETIREMENT_MASSIVE, null);
				}
			} else  {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
		}				
		
		// VALIDATE HOLDER ACCOUNT
		
		HolderAccount objHolderAccount = new HolderAccount();
		HolderAccountBalance objHolderAccountBalance = new HolderAccountBalance();
		BigDecimal bigAvailableBalance = BigDecimal.ZERO;		
		if(objParticipant != null && objSecurity != null){			
			HolderAccountObjectTO holderAccountObjectTO = new HolderAccountObjectTO();						
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){	
				List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
						objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(), Boolean.FALSE);				
				if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
					holderAccountObjectTO.setIdHolderAccount(null);
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
					
				} else {
					objHolderAccountBalance = lstHolderAccountBalances.get(0);				
					holderAccountObjectTO.setIdHolderAccount(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
				}								
			} else {
				holderAccountObjectTO = stockEarlyPaymentRegisterTO.getHolderAccountObjectTO();
				holderAccountObjectTO.setIdParticipant(objParticipant.getIdParticipantPk());
				holderAccountObjectTO.setCreateObject(false);
				//Buscar por numero de cuenta
				if(Validations.validateIsNotNullAndNotEmpty(holderAccountObjectTO.getAccountNumber())){					
					holderAccountObjectTO.setValidateAccount(BooleanType.YES.getBooleanValue());
				} else {
					holderAccountObjectTO.setValidateAccount(BooleanType.NO.getBooleanValue());
				}
				
				SecurityObjectTO secAux=new SecurityObjectTO();
				secAux.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
				secAux.setSecurityClass(objSecurity.getSecurityClass());
				holderAccountObjectTO.setSecurityObjectTO(secAux);
				holderAccountObjectTO.setEarlyPaymentNoDpf(true);
				Object objResult = accountsRemoteServiceBean.get().createHolderRemoteService(loggerUser, holderAccountObjectTO);
				
				if(objResult instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType)objResult;
					validation.setOperationRef(stockEarlyPaymentRegisterTO);
					return validation;
				}else if (objResult instanceof HolderAccountObjectTO){
					holderAccountObjectTO = (HolderAccountObjectTO) objResult;
				}
			}
			
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){
				
				if(holderAccountObjectTO.getIdHolderAccount() != null){
					objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());
				}
				
				if(objHolderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
					bigAvailableBalance = objHolderAccountBalance.getAvailableBalance();
				} else {
					
					//VALIDATE ACREDITATION OPERATIONS
					
					if(objHolderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO) > 0) {
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_ACCREDITATION_OPERATION_EXISTS, null);												
					}
					
					//VALIDATE BLOCKED OPERATIONS
					
					if(objHolderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) > 0
							|| objHolderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO) > 0
							|| objHolderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_BLOCKED_OPERATIONS, null);				
					} 
					
					//VALIDATE REPORTO OPERATIONS
					
					if(objHolderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO) > 0
							|| objHolderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_REPORT_OPERATION_EXISTS, null);				
					}
					
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_BALANCE_AVAILABLE, null);
				}
				
			} else {
				boolean isTotalType = false;
				
				if(securityObjectTO.getStockQuantity().compareTo(BigDecimal.ZERO) <= 0){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_ZERO_RCA, null);
				}
				
				if(GeneralConstants.REDEMTION_TYPE_PARTIAL.equals(stockEarlyPaymentRegisterTO.getRedemptionTypeCode())){
					stockEarlyPaymentRegisterTO.setRedemptionType(RetirementOperationRedemptionType.PARTIAL.getCode());
				} else if(GeneralConstants.REDEMTION_TYPE_TOTAL.equals(stockEarlyPaymentRegisterTO.getRedemptionTypeCode())){
					isTotalType = true;
					stockEarlyPaymentRegisterTO.setRedemptionType(RetirementOperationRedemptionType.TOTAL.getCode());
				}

				// ACV's by security, Participant y holder account									
				
				List<Object[]> lstAccountAnnotationOperation = findOriginalAccountAnnotation(objSecurity.getIdSecurityCodePk(),
						objParticipant.getIdParticipantPk(), holderAccountObjectTO.getIdHolderAccount());
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstAccountAnnotationOperation)){
					boolean blExitsAcv = false;					
					for(Object[] objAccAnnOper : lstAccountAnnotationOperation){						
						AccountAnnotationOperation objAccountAnnotationOperation = (AccountAnnotationOperation) objAccAnnOper[0];
						BigDecimal bdQuantityRet = (BigDecimal) objAccAnnOper[1];						
						BigDecimal bdAmountAvailable = objAccountAnnotationOperation.getTotalBalance().subtract(bdQuantityRet);	
						if(isTotalType){
							if(bdAmountAvailable.compareTo(securityObjectTO.getStockQuantity()) == 0){
								accountAnnotationOperation = objAccountAnnotationOperation;
								blExitsAcv = true;
								break;
							}
						} else 	{
							if(securityObjectTO.getStockQuantity().compareTo(bdAmountAvailable) <= 0){
								accountAnnotationOperation = objAccountAnnotationOperation;
								blExitsAcv = true;
								break;
							}
						}
					}	
					
					if(isTotalType && !blExitsAcv){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_TOTAL_RCA, null);
					}
					
					if(blExitsAcv){
						
						objHolderAccountBalance = participantServiceBean.getHolderAccountBalanceByRetirement(objSecurity.getIdSecurityCodePk(), 
								objParticipant.getIdParticipantPk(), holderAccountObjectTO.getIdHolderAccount());
						
						if(objHolderAccountBalance != null){
							
							if(objHolderAccountBalance.getTotalBalance().compareTo(BigDecimal.ZERO) <= 0){
								return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
							}														
							
							if(securityObjectTO.getStockQuantity().compareTo(objHolderAccountBalance.getAvailableBalance()) > 0){
								return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
							}
							
							holderAccountObjectTO.setIdHolderAccount(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
							if(holderAccountObjectTO.getIdHolderAccount() != null){
								objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());
							}
						} else {
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);							
						}
						
					} else {
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_RCA, null);
					}
				} else {
					objHolderAccountBalance = participantServiceBean.getHolderAccountBalanceByRetirement(objSecurity.getIdSecurityCodePk(), 
							objParticipant.getIdParticipantPk(), holderAccountObjectTO.getIdHolderAccount());
					if(objHolderAccountBalance != null){
						if(holderAccountObjectTO.getIdHolderAccount() != null){
							objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());
						}
					} else {
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
					}
					
				}
			}			
		}
		
		if(objParticipant != null && objSecurity != null && objHolderAccount != null && objHolderAccountBalance != null){			
			
			//CREATE RETIREMENT OPERATION
			
			RetirementOperation retirementOperation = new RetirementOperation();
			retirementOperation.setMotive(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode());
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){
				retirementOperation.setRetirementType(RetirementOperationRedemptionType.TOTAL.getCode());
			} else {
				retirementOperation.setRetirementType(stockEarlyPaymentRegisterTO.getRedemptionType());
			}			
			retirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
			retirementOperation.setSecurity(objSecurity);
			retirementOperation.setAccountAnnotationOperation(accountAnnotationOperation);
			retirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
			retirementOperation.setIndRetirementExpiration(BooleanType.NO.getCode());
			RetirementDetail retirementDetail = new RetirementDetail();
			if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
				retirementOperation.setTotalBalance(bigAvailableBalance);
				
				retirementDetail.setAvailableBalance(bigAvailableBalance);
			} else {
				retirementOperation.setTotalBalance(securityObjectTO.getStockQuantity());
				
				retirementDetail.setAvailableBalance(securityObjectTO.getStockQuantity());
			}			
			retirementDetail.setHolderAccount(objHolderAccount);
			retirementDetail.setParticipant(objParticipant);
			retirementDetail.setRetirementOperation(retirementOperation);
			
			HolderMarketFactBalance holderMarketFactBalance = new HolderMarketFactBalance();			
			try {
				holderMarketFactBalance = getLastHolderMarketfactBalance(objParticipant.getIdParticipantPk(), 
						objHolderAccount.getIdHolderAccountPk(), objSecurity.getIdSecurityCodePk());				
			} catch (NonUniqueResultException nuex) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_MULTIPLE_MARKETFACTS, null);
			}
			
			if(holderMarketFactBalance != null){				
				if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
					if(holderMarketFactBalance.getAvailableBalance().compareTo(bigAvailableBalance) < 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
					}
				} else {
					if(holderMarketFactBalance.getAvailableBalance().compareTo(securityObjectTO.getStockQuantity()) < 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
					}
				}				
				retirementDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());				
				RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
				retirementMarketfact.setRetirementDetail(retirementDetail);
				retirementMarketfact.setMarketRate(holderMarketFactBalance.getMarketRate());
				retirementMarketfact.setMarketPrice(holderMarketFactBalance.getMarketPrice());
				retirementMarketfact.setMarketDate(holderMarketFactBalance.getMarketDate());
				if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
					retirementMarketfact.setOperationQuantity(bigAvailableBalance);
				} else {
					retirementMarketfact.setOperationQuantity(securityObjectTO.getStockQuantity());
				}				
				retirementDetail.getRetirementMarketfacts().add(retirementMarketfact);				
				retirementOperation.getRetirementDetails().add(retirementDetail);
				retirementOperation.setState(RetirementOperationStateType.APPROVED.getCode());
				retirementOperation.setCustodyOperation(new CustodyOperation());				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NO_MKTFACT_BALANCES, null);
			}		
			
			saveRetirementOperation(retirementOperation, loggerUser, false);				
			changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.CONFIRMED.getCode());				
			stockEarlyPaymentRegisterTO.setIdCustodyOperation(retirementOperation.getIdRetirementOperationPk());				 				
		}		
		return stockEarlyPaymentRegisterTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveEarlyPaymentTx(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO, LoggerUser loggerUser) throws ServiceException{
		
		Object objResult = validateAndSaveEarlyPayment(stockEarlyPaymentRegisterTO, loggerUser);
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	} 
	
	/**
	 * Validate and save early payment.
	 *
	 * @param stockEarlyPaymentRegisterTO the stock early payment register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveEarlyPayment(StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO, LoggerUser loggerUser) 
			throws ServiceException {
		
		//CHECK OPERATION TYPE
		AccountAnnotationOperation accountAnnotationOperation = null;
		
		if(!(ComponentConstant.INTERFACE_EARLY_PAYMENT.equals(stockEarlyPaymentRegisterTO.getOperationCode()) ||
				ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode()))){			
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		//VALIDATE PARTICIPANT
		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(stockEarlyPaymentRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);		
		if(stockEarlyPaymentRegisterTO.isDpfInterface()){
			if (objParticipant == null ) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			} else {
				stockEarlyPaymentRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			}
		} else {
			if(objParticipant == null  || ( !objParticipant.getIdParticipantPk().equals(idParticipantBC) 
					&& !objParticipant.getIdParticipantPk().equals(idParticipantVUN)) ){
				objParticipant = null;
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			} else {
				
				// Validar por numero de obligacion, si es que viene en el archivo
				
				if(Validations.validateIsNotNullAndNotEmpty(stockEarlyPaymentRegisterTO.getObligationNumber())){
					accountAnnotationOperation = findOriginalAccountAnnotation(stockEarlyPaymentRegisterTO.getObligationNumber(),objParticipant.getIdParticipantPk());
					if(accountAnnotationOperation == null){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_OBLIGATION_NUMBER_ORIGIN, null);
					}
				} 								
				stockEarlyPaymentRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
			}
		}	
		
		//CHECK OPERATION NUMBER EXITS
		
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), stockEarlyPaymentRegisterTO.getOperationNumber(), 
						stockEarlyPaymentRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		//CHECK OPERATION DATE IS TODAY
		
		Date operationDate = CommonsUtilities.truncateDateTime(stockEarlyPaymentRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
		}

		// VALIDATE SECURITY CLASS
		
		SecurityObjectTO securityObjectTO = stockEarlyPaymentRegisterTO.getSecurityObjectTO();		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
			filter.setText1(securityObjectTO.getSecurityClassDesc().trim());
		} else {
			filter.setParameterTablePk(securityObjectTO.getSecurityClass());
		}				
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){
				if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode())//DPA - issue 158
						&&!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPA.getCode())
						){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
				} else {
					securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
					securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
				}
			} else {
				if(!ComponentConstant.ONE.equals(securityClassParam.getIndicator6())){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);	
				} else {
					securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
					securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
				}
			}			
		} else {								
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}

		//VALIDATE SECURITY EXISTS
		
		String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityObjectTO.getSecurityClassDesc(), securityObjectTO.getSecuritySerial());
		Security objSecurity = new Security();		 
		if(idSecurityCode != null){
			objSecurity = find(Security.class, idSecurityCode);
			logger.debug("Id Security Code: " + idSecurityCode);
		}		
		if(objSecurity == null){
			return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
		} else {
			stockEarlyPaymentRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
			
			// VALIDATE EARLY PAYMENT DPF
			
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){								
				
				// VALIDATE STATE SECURITY
				
				if(!SecurityStateType.REGISTERED.getCode().equals(objSecurity.getStateSecurity())){				
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
							new Object[]{SecurityStateType.lookup.get(objSecurity.getStateSecurity()).getValue()});
				}
				
				// VALIDATE INDICATOR EARLY REDEMTION
				
				if(!GeneralConstants.ONE_VALUE_INTEGER.equals(objSecurity.getIndEarlyRedemption())){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INDICATOR_EARLY_PAYMENT, null);
				}
				
				// VALIDATE EXPIRATION DATE SECURITY
				
				if(Validations.validateIsNotNull(securityObjectTO.getExpirationDate()) && 
						Validations.validateIsNotNull(objSecurity.getExpirationDate())){
					if(CommonsUtilities.convertDatetoString(objSecurity.getExpirationDate()).compareTo(
							CommonsUtilities.convertDatetoString(securityObjectTO.getExpirationDate())) != 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
					}
				} else  {
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
				}
				
				// VALIDATE NUMBER COUPONS
				
				boolean blValidateCupons = false;
				if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
					blValidateCupons = true;
				} else {
					if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
						blValidateCupons = true;
					}
				}
				//DPA - issue 158
				if(!securityObjectTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
					if(blValidateCupons){
						if(!objSecurity.getNumberCoupons().equals(1)){
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
						}
					} else {
						if(!objSecurity.getNumberCoupons().equals(securityObjectTO.getNumberCoupons())){
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
						}
					}
				}
				// EXPIRATION OPERATION ACCREDITATION
				
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
				Security objSecurityAccreditation = new Security();
				objSecurityAccreditation.setIdSecurityCodePk(objSecurity.getIdSecurityCodePk());
				accreditationOperationTO.setSecurity(objSecurityAccreditation);	
				
				//Validando motivo de accreditation issue 1363
				List<AccreditationOperation> accreditationOperationLstAux = beanService.searchAccreditationOperationByUnblock(accreditationOperationTO);
				if (Validations.validateListIsNotNullAndNotEmpty(accreditationOperationLstAux)) {
					for (AccreditationOperation tmpAccreditationOperationAux : accreditationOperationLstAux) {
						if (ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())) {
							if (!AccreditationOperationMotive.EARLY_REDEMPTION.getCode().equals(tmpAccreditationOperationAux.getMotive())) {
								return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_BALANCE_AVAILABLE, null);
							}
						} 
					}
				}
				
				accreditationfacadeBean.expirationAcreditationByDpf(accreditationOperationTO);
				
			}
			
			// VALIDATE EXPIRATION DATE SECURIIES
			
			if(Validations.validateIsNotNull(objSecurity.getExpirationDate())){
				if(CommonsUtilities.currentDate().compareTo(objSecurity.getExpirationDate()) == 0){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE_RETIREMENT, null);
				}
			} else  {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
		}				
		
		// VALIDATE HOLDER ACCOUNT
		
		HolderAccount objHolderAccount = new HolderAccount();
		HolderAccountBalance objHolderAccountBalance = new HolderAccountBalance();
		BigDecimal bigAvailableBalance = BigDecimal.ZERO;		
		if(objParticipant != null && objSecurity != null){			
			HolderAccountObjectTO holderAccountObjectTO = new HolderAccountObjectTO();						
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){	
				List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
						objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(), Boolean.FALSE);				
				if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
					holderAccountObjectTO.setIdHolderAccount(null);
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
					
				} else {
					objHolderAccountBalance = lstHolderAccountBalances.get(0);				
					holderAccountObjectTO.setIdHolderAccount(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
				}								
			} else {
				holderAccountObjectTO = stockEarlyPaymentRegisterTO.getHolderAccountObjectTO();
				holderAccountObjectTO.setIdParticipant(objParticipant.getIdParticipantPk());
				holderAccountObjectTO.setCreateObject(false);
				//Buscar por numero de cuenta
				if(Validations.validateIsNotNullAndNotEmpty(holderAccountObjectTO.getAccountNumber())){					
					holderAccountObjectTO.setValidateAccount(BooleanType.YES.getBooleanValue());
				} else {
					holderAccountObjectTO.setValidateAccount(BooleanType.NO.getBooleanValue());
				}
				Object objResult = accountsRemoteServiceBean.get().createHolderRemoteService(loggerUser, holderAccountObjectTO);
				
				if(objResult instanceof RecordValidationType){
					RecordValidationType validation = (RecordValidationType)objResult;
					validation.setOperationRef(stockEarlyPaymentRegisterTO);
					return validation;
				}else if (objResult instanceof HolderAccountObjectTO){
					holderAccountObjectTO = (HolderAccountObjectTO) objResult;
				}
			}
			
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){
				
				if(holderAccountObjectTO.getIdHolderAccount() != null){
					objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());
				}
				
				if(objHolderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
					bigAvailableBalance = objHolderAccountBalance.getAvailableBalance();
				} else {
					
					//VALIDATE ACREDITATION OPERATIONS
					
					if(objHolderAccountBalance.getAccreditationBalance().compareTo(BigDecimal.ZERO) > 0) {
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_ACCREDITATION_OPERATION_EXISTS, null);												
					}
					
					//VALIDATE BLOCKED OPERATIONS
					
					if(objHolderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) > 0
							|| objHolderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO) > 0
							|| objHolderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_BLOCKED_OPERATIONS, null);				
					} 
					
					//VALIDATE REPORTO OPERATIONS
					
					if(objHolderAccountBalance.getReportedBalance().compareTo(BigDecimal.ZERO) > 0
							|| objHolderAccountBalance.getReportingBalance().compareTo(BigDecimal.ZERO) > 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_SECURITY_CODE_REPORT_OPERATION_EXISTS, null);				
					}
					
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_BALANCE_AVAILABLE, null);
				}
				
			} else {
				boolean isTotalType = false;
				
				if(securityObjectTO.getStockQuantity().compareTo(BigDecimal.ZERO) <= 0){
					return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_TOTAL_ZERO, null);
				}
				
				if(GeneralConstants.ONE_VALUE_INTEGER.equals(stockEarlyPaymentRegisterTO.getRedemptionType())){
					stockEarlyPaymentRegisterTO.setRedemptionType(RetirementOperationRedemptionType.PARTIAL.getCode());
				} else if(GeneralConstants.TWO_VALUE_INTEGER.equals(stockEarlyPaymentRegisterTO.getRedemptionType())){
					isTotalType = true;
					stockEarlyPaymentRegisterTO.setRedemptionType(RetirementOperationRedemptionType.TOTAL.getCode());
				}

				// ACV's by security, Participant y holder account									
				
				List<Object[]> lstAccountAnnotationOperation = findOriginalAccountAnnotation(objSecurity.getIdSecurityCodePk(),
						objParticipant.getIdParticipantPk(), holderAccountObjectTO.getIdHolderAccount());
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstAccountAnnotationOperation)){
					boolean blExitsAcv = false;					
					for(Object[] objAccAnnOper : lstAccountAnnotationOperation){						
						AccountAnnotationOperation objAccountAnnotationOperation = (AccountAnnotationOperation) objAccAnnOper[0];
						BigDecimal bdQuantityRet = (BigDecimal) objAccAnnOper[1];						
						BigDecimal bdAmountAvailable = objAccountAnnotationOperation.getTotalBalance().subtract(bdQuantityRet);	
						if(isTotalType){
							if(bdAmountAvailable.compareTo(securityObjectTO.getStockQuantity()) == 0){
								accountAnnotationOperation = objAccountAnnotationOperation;
								blExitsAcv = true;
								break;
							}
						} else 	{
							if(securityObjectTO.getStockQuantity().compareTo(bdAmountAvailable) <= 0){
								accountAnnotationOperation = objAccountAnnotationOperation;
								blExitsAcv = true;
								break;
							}
						}
					}	
					
					if(isTotalType && !blExitsAcv){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY_TOTAL, null);
					}
					
					if(blExitsAcv){
						
						objHolderAccountBalance = participantServiceBean.getHolderAccountBalanceByRetirement(objSecurity.getIdSecurityCodePk(), 
								objParticipant.getIdParticipantPk(), holderAccountObjectTO.getIdHolderAccount());
						
						if(objHolderAccountBalance != null){
							
							if(objHolderAccountBalance.getTotalBalance().compareTo(BigDecimal.ZERO) <= 0){
								return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
							}														
							
							if(securityObjectTO.getStockQuantity().compareTo(objHolderAccountBalance.getAvailableBalance()) > 0){
								return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
							}
							
							holderAccountObjectTO.setIdHolderAccount(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
							if(holderAccountObjectTO.getIdHolderAccount() != null){
								objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());
							}
						} else {
							return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);							
						}
						
					} else {
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_STOCK_BALANCES_EARLY, null);
					}
				} else {
					objHolderAccountBalance = participantServiceBean.getHolderAccountBalanceByRetirement(objSecurity.getIdSecurityCodePk(), 
							objParticipant.getIdParticipantPk(), holderAccountObjectTO.getIdHolderAccount());
					if(objHolderAccountBalance != null){
						if(holderAccountObjectTO.getIdHolderAccount() != null){
							objHolderAccount = find(HolderAccount.class, holderAccountObjectTO.getIdHolderAccount());
						}
					} else {
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
					}
					
				}
			}			
		}
		
		if(objParticipant != null && objSecurity != null && objHolderAccount != null && objHolderAccountBalance != null){			
			
			//CREATE RETIREMENT OPERATION
			
			RetirementOperation retirementOperation = new RetirementOperation();
			retirementOperation.setMotive(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode());
			if(stockEarlyPaymentRegisterTO.isDpfInterface()){
				retirementOperation.setRetirementType(RetirementOperationRedemptionType.TOTAL.getCode());
			} else {
				retirementOperation.setRetirementType(stockEarlyPaymentRegisterTO.getRedemptionType());
			}			
			retirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
			retirementOperation.setSecurity(objSecurity);
			retirementOperation.setAccountAnnotationOperation(accountAnnotationOperation);
			retirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
			retirementOperation.setIndRetirementExpiration(BooleanType.NO.getCode());
			RetirementDetail retirementDetail = new RetirementDetail();
			if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
				retirementOperation.setTotalBalance(bigAvailableBalance);
				
				retirementDetail.setAvailableBalance(bigAvailableBalance);
			} else {
				retirementOperation.setTotalBalance(securityObjectTO.getStockQuantity());
				
				retirementDetail.setAvailableBalance(securityObjectTO.getStockQuantity());
			}			
			retirementDetail.setHolderAccount(objHolderAccount);
			retirementDetail.setParticipant(objParticipant);
			retirementDetail.setRetirementOperation(retirementOperation);
			
			HolderMarketFactBalance holderMarketFactBalance = new HolderMarketFactBalance();			
			try {
				holderMarketFactBalance = getLastHolderMarketfactBalance(objParticipant.getIdParticipantPk(), 
						objHolderAccount.getIdHolderAccountPk(), objSecurity.getIdSecurityCodePk());				
			} catch (NonUniqueResultException nuex) {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_MULTIPLE_MARKETFACTS, null);
			}
			
			if(holderMarketFactBalance != null){				
				if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
					if(holderMarketFactBalance.getAvailableBalance().compareTo(bigAvailableBalance) < 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
					}
				} else {
					if(holderMarketFactBalance.getAvailableBalance().compareTo(securityObjectTO.getStockQuantity()) < 0){
						return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSUFFICIENT_BALANCES, null);
					}
				}				
				retirementDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());				
				RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
				retirementMarketfact.setRetirementDetail(retirementDetail);
				retirementMarketfact.setMarketRate(holderMarketFactBalance.getMarketRate());
				retirementMarketfact.setMarketPrice(holderMarketFactBalance.getMarketPrice());
				retirementMarketfact.setMarketDate(holderMarketFactBalance.getMarketDate());
				if(ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF.equals(stockEarlyPaymentRegisterTO.getOperationCode())){
					retirementMarketfact.setOperationQuantity(bigAvailableBalance);
				} else {
					retirementMarketfact.setOperationQuantity(securityObjectTO.getStockQuantity());
				}				
				retirementDetail.getRetirementMarketfacts().add(retirementMarketfact);				
				retirementOperation.getRetirementDetails().add(retirementDetail);
				retirementOperation.setState(RetirementOperationStateType.APPROVED.getCode());
				retirementOperation.setCustodyOperation(new CustodyOperation());				
			} else {
				return CommonsUtilities.populateRecordCodeValidation(stockEarlyPaymentRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NO_MKTFACT_BALANCES, null);
			}		
			
			saveRetirementOperation(retirementOperation, loggerUser, false);				
			changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.CONFIRMED.getCode());				
			stockEarlyPaymentRegisterTO.setIdCustodyOperation(retirementOperation.getIdRetirementOperationPk());				 				
		}		
		return stockEarlyPaymentRegisterTO;
	}
	
	/**
	 * Find original account annotation.
	 *
	 * @param obligationNumber the obligation number
	 * @param idParticipant the id participant
	 * @return the account annotation operation
	 */
	private AccountAnnotationOperation findOriginalAccountAnnotation(
			String obligationNumber, Long idParticipant) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT aao from AccountAnnotationOperation aao " );
		sbQuery.append("	where aao.holderAccount.participant.idParticipantPk = :idParticipant " );
		sbQuery.append("	and   aao.obligationNumber = :obligationNumber " );
		
		parameters.put("obligationNumber", obligationNumber);
		parameters.put("idParticipant", idParticipant);
		
		return (AccountAnnotationOperation) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Find original account annotation.
	 *
	 * @param strIdSecurityCode the str id security code
	 * @param lngIdParticipant the lng id participant
	 * @param lngIdHolderAccount the lng id holder account
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	private List<Object[]> findOriginalAccountAnnotation(String strIdSecurityCode,
			Long lngIdParticipant, Long lngIdHolderAccount){		
		Map<String,Object> mapParameters = new HashMap<String,Object>();
		List<Integer> lstStateRetirement = new ArrayList<Integer>();
		List<Integer> lstStateAcv = new ArrayList<Integer>();
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT aao ,  ");
		stringBuffer.append(" nvl ( (SELECT sum(ro.totalBalance) FROM RetirementOperation ro ");
		stringBuffer.append(" where ro.accountAnnotationOperation.idAnnotationOperationPk = aao.idAnnotationOperationPk ");
		stringBuffer.append(" and ro.state in (:parLstStateRetirement) ), 0 )   ");
		stringBuffer.append(" FROM AccountAnnotationOperation aao ");
		stringBuffer.append(" WHERE aao.holderAccount.participant.idParticipantPk = :parIdParticipant " );
		stringBuffer.append(" AND aao.holderAccount.idHolderAccountPk = :parIdHolderAccount " );
		stringBuffer.append(" AND aao.security.idSecurityCodePk = :parIdSecurityCode " );
		stringBuffer.append(" AND aao.state in (:parLstStateAcv) " );
		lstStateRetirement.add(RetirementOperationStateType.CONFIRMED.getCode());
		lstStateAcv.add(DematerializationStateType.CONFIRMED.getCode());
		mapParameters.put("parLstStateRetirement", lstStateRetirement);
		mapParameters.put("parIdParticipant", lngIdParticipant);
		mapParameters.put("parIdHolderAccount", lngIdHolderAccount);
		mapParameters.put("parIdSecurityCode", strIdSecurityCode);
		mapParameters.put("parLstStateAcv", lstStateAcv);
		List<Object[]> objectListTemp = null;
        try {
        	objectListTemp =  findListByQueryString(stringBuffer.toString(), mapParameters);
        } catch(NoResultException ex){
        	return null;
        }        
        return objectListTemp;
	}

	/**
	 * Gets the last holder marketfact balance.
	 *
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @return the last holder marketfact balance
	 */
	private HolderMarketFactBalance getLastHolderMarketfactBalance(
			Long idParticipant, Long idHolderAccount, String idSecurityCode) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT hmb from HolderMarketFactBalance hmb " );
		sbQuery.append("	where hmb.participant.idParticipantPk = :idParticipant " );
		sbQuery.append("	and   hmb.holderAccount.idHolderAccountPk = :idHolderAccount " );
		sbQuery.append("	and   hmb.security.idSecurityCodePk = :idSecurity " );
		sbQuery.append("	and   hmb.indActive= :indicatorOne" );		
		sbQuery.append("    and   hmb.availableBalance > 0 ");
		
		sbQuery.append("	ORDER BY hmb.marketDate DESC");
		
		parameters.put("idSecurity", idSecurityCode);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", idParticipant);
		
		HolderMarketFactBalance lstResult = (HolderMarketFactBalance) findObjectByQueryString(sbQuery.toString(), parameters);
		return lstResult;
	}
	
	/**
	 * obteniendo el la cartera de HM a devolver el saldo por concepto de PagoAnticipado. No Aplica a DPF
	 * @param idParticipant
	 * @param idHolderAccount
	 * @param idSecurityCode
	 * @return
	 */
	private HolderMarketFactBalance getLastHolderMarketfactBalanceEarlyNoDpf(
			Long idParticipant, Long idHolderAccount, String idSecurityCode) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT hmb from HolderMarketFactBalance hmb " );
		sbQuery.append("	where hmb.participant.idParticipantPk = :idParticipant " );
		sbQuery.append("	and   hmb.holderAccount.idHolderAccountPk = :idHolderAccount " );
		sbQuery.append("	and   hmb.security.idSecurityCodePk = :idSecurity " );
		sbQuery.append("	and   hmb.indActive= :indicatorOne" );		
		
		sbQuery.append("	ORDER BY hmb.marketDate DESC");
		
		parameters.put("idSecurity", idSecurityCode);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", idParticipant);
		
		HolderMarketFactBalance lstResult = (HolderMarketFactBalance) findObjectByQueryString(sbQuery.toString(), parameters);
		return lstResult;
	}

	public List<CorporativeOperation> CorporativeInterest(Long idProgramInterestPk) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT co FROM CorporativeOperation co  ");
		strbQuery.append(" JOIN FETCH co.programInterestCoupon pic ");
		strbQuery.append(" WHERE pic.idProgramInterestPk = :idProgramInterestPk and co.state = :coState ");
		
		TypedQuery<CorporativeOperation> query=em.createQuery(strbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idProgramInterestPk", idProgramInterestPk);
		query.setParameter("coState", CorporateProcessStateType.REGISTERED.getCode());

		try {
			return query.getResultList();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public List<CorporativeOperation> CorporativeInterest(String idSecurityCodePk) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT co FROM CorporativeOperation co  ");
		strbQuery.append(" JOIN FETCH co.programInterestCoupon pic ");
		strbQuery.append(" WHERE co.securities.idSecurityCodePk = :idSecurityCodePk and co.state = :coState ");
		
		TypedQuery<CorporativeOperation> query=em.createQuery(strbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("coState", CorporateProcessStateType.REGISTERED.getCode());

		try {
			return query.getResultList();
		}catch (NoResultException e) {
			return null;
		}
	}

	public CorporativeOperation CorporativeAmortization(String idSecurityCodePk) throws ServiceException {
		StringBuilder strbQuery = new StringBuilder();
		strbQuery.append(" SELECT co FROM CorporativeOperation co  ");
		strbQuery.append(" JOIN FETCH co.programAmortizationCoupon pac ");
		strbQuery.append(" WHERE co.securities.idSecurityCodePk = :idSecurityCodePk and co.state = :coState ");
		
		TypedQuery<CorporativeOperation> query=em.createQuery(strbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("coState", CorporateProcessStateType.REGISTERED.getCode());
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the last holder marketfact balance early redemption.
	 *
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCode the id security code
	 * @return the last holder marketfact balance
	 */
	private HolderMarketFactBalance getLastHolderMarketfactBalanceEarlyRedemption(
			Long idParticipant, Long idHolderAccount, String idSecurityCode) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" SELECT hmb from HolderMarketFactBalance hmb " );
		sbQuery.append("	where hmb.participant.idParticipantPk = :idParticipant " );
		sbQuery.append("	and   hmb.holderAccount.idHolderAccountPk = :idHolderAccount " );
		sbQuery.append("	and   hmb.security.idSecurityCodePk = :idSecurity " );
		sbQuery.append("	and   hmb.indActive= :indicatorOne" );		
		sbQuery.append("    and   hmb.availableBalance = 0 ");
		
		sbQuery.append("	ORDER BY hmb.marketDate DESC");
		
		parameters.put("idSecurity", idSecurityCode);
		parameters.put("indicatorOne", ComponentConstant.ONE);
		parameters.put("idHolderAccount", idHolderAccount);
		parameters.put("idParticipant", idParticipant);
		
		HolderMarketFactBalance lstResult = (HolderMarketFactBalance) findObjectByQueryString(sbQuery.toString(), parameters);
		return lstResult;
	}



	/**
	 * Save retirement operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @param loggerUser the logger user
	 * @param totalRemoval the total removal
	 * @throws ServiceException the service exception
	 */
	public CustodyOperation saveCustodyOperation(CustodyOperation custodyOp) throws ServiceException {
		Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_SECURITIES_WITHDRAWAL);
		custodyOp.setOperationNumber(operationNumber);
		return create(custodyOp);
	}
	public void saveRetirementOperation(RetirementOperation retirementOperation, LoggerUser loggerUser, boolean totalRemoval) throws ServiceException {
		System.out.println("********* saveRetirementOperation::"+retirementOperation.toString());
		retirementOperation.setAudit(loggerUser);
		create(retirementOperation);

		if(retirementOperation.getRetirementDetails()!=null) {
			
			for (RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()) {
				
				PhysicalCertificate physicalCertificate = retirementDetail.getPhysicalCertificate();
				retirementDetail.setRetirementOperation(retirementOperation);
				retirementDetail.setAudit(loggerUser);
				create(retirementDetail);
				
				physicalCertificate.setState(PhysicalCertificateStateType.RETIREMENT_REQUEST.getCode());
				physicalCertificate.setAudit(loggerUser);
				update(physicalCertificate);
				
				
			}
		}
		
		if(retirementOperation.getRetirementDetails()!=null && retirementOperation.getRetirementDetails().size()>0) {
			
			if( retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) ||  
				retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ||
				retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_OTC.getCode()) ||  
				( retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) && retirementOperation.getPhysicalCertificate().getIdProgramInterestFk() == null )
				
			){
				
				Long idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_REGISTER.getCode();
				
				if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(retirementOperation.getAccountAnnotationOperation().getMotive())){
					idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_MANAGED_REGISTER.getCode();
				}else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(retirementOperation.getAccountAnnotationOperation().getMotive())){
					idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_REGISTER.getCode();
				}
				
				
				AccountsComponentTO accountsComponentTO = getAccountsComponentTO(retirementOperation,idBusinessProcessPk);
				if(Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstSourceAccounts()) || 
						Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstTargetAccounts())){
					System.out.println(" ************** executeAccountsComponent:: "+accountsComponentTO.toString());
					accountsComponentService.get().executeAccountsComponent(accountsComponentTO);	
				}
			}
			
		}
		
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param retirementOperation the retirement operation
	 * @param businessProcessType the business process type
	 * @return the accounts component to
	 */
	private AccountsComponentTO getAccountsComponentTO(RetirementOperation retirementOperation, Long businessProcessType){
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(retirementOperation.getIdRetirementOperationPk());
		objAccountComponent.setLstSourceAccounts(new ArrayList<HolderAccountBalanceTO>());
		objAccountComponent.setLstTargetAccounts(new ArrayList<HolderAccountBalanceTO>());
		objAccountComponent.setIdOperationType(retirementOperation.getCustodyOperation().getOperationType());
		objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
		
		if(objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_REGISTER.getCode()) ||
				objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_CANCEL.getCode()) || 
				objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_REJECT.getCode()) || 
				objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_MANAGED_REGISTER.getCode()) || 
				objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_MANAGED_CANCEL.getCode()) || 
				objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_REGISTER.getCode()) || 
				objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_CANCEL.getCode()) 
				){
			/*for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
				getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
			}*/
			getHolderAccountBalance(retirementOperation, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
			//getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
		}
		
		
		
		if( objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode()) ||
			objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_MANAGED.getCode())  ||
			objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode()) 
			){
			/*for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
				getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
				if(objAccountComponent.getIdOperationType().equals(ParameterOperationType.SECURITIES_REDEMPTION_FIXED_INCOME.getCode())
						&& retirementOperation.getTargetSecurity()!=null){
					getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ONE);
				}
			}*/
			getHolderAccountBalance(retirementOperation, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
			//getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
			if(objAccountComponent.getIdOperationType().equals(ParameterOperationType.SECURITIES_REDEMPTION_FIXED_INCOME.getCode())
					&& retirementOperation.getTargetSecurity()!=null){
				getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ONE);
			}
		}
		
		if(objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.SECURITY_RETIREMENT_CONFIRM_BLOCKED.getCode())){
			/*for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
				getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ZERO);
				if(objAccountComponent.getIdOperationType().equals(ParameterOperationType.SECURITIES_REDEMPTION_FIXED_INCOME.getCode())
						&& retirementOperation.getTargetSecurity()!=null){
					getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ZERO);
				}
				
			}*/
			getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ZERO);
			if(objAccountComponent.getIdOperationType().equals(ParameterOperationType.SECURITIES_REDEMPTION_FIXED_INCOME.getCode())
					&& retirementOperation.getTargetSecurity()!=null){
				getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ZERO);
			}
		}
		
		if(objAccountComponent.getIdBusinessProcess().equals(BusinessProcessType.REVERSE_EARLY_PAYMENT_RRA.getCode())){
			/*for(RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()){
				getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
				if(objAccountComponent.getIdOperationType().equals(ParameterOperationType.REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode())
						&& retirementOperation.getTargetSecurity()!=null){
					getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ONE);
				}
			}*/
			getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
			if(objAccountComponent.getIdOperationType().equals(ParameterOperationType.REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode())
					&& retirementOperation.getTargetSecurity()!=null){
				getHolderAccountBalance(retirementOperation.getRetirementDetails().get(0), objAccountComponent, ComponentConstant.TARGET, ComponentConstant.ONE);
			}
		}
		
		return objAccountComponent;
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @param retirementDetail the retirement detail
	 * @param objAccountComponent the obj account component
	 * @param sourceTarget the source target
	 * @param availableOrBlocked the available or blocked
	 * @return the holder account balance
	 */
	private void getHolderAccountBalance(RetirementDetail retirementDetail,AccountsComponentTO objAccountComponent, Long sourceTarget, Integer availableOrBlocked){
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(retirementDetail.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(retirementDetail.getParticipant().getIdParticipantPk());
		if(retirementDetail.getTotalBalanceAnnotation() == null) {
			holderAccountBalanceTO.setStockQuantity(retirementDetail.getTotalBalance());
		}else {
			holderAccountBalanceTO.setStockQuantity(retirementDetail.getTotalBalanceAnnotation());
		}
		holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
		
		/*if(ComponentConstant.ONE.equals(availableOrBlocked)){
			if(retirementDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
				holderAccountBalanceTO.setStockQuantity(retirementDetail.getAvailableBalance());
				if(ComponentConstant.ONE.equals(objAccountComponent.getIndMarketFact())){
					for(RetirementMarketfact retirementMarketFact : retirementDetail.getRetirementMarketfacts()){
						MarketFactAccountTO marketAccountTO=new MarketFactAccountTO();
						marketAccountTO.setMarketDate( retirementMarketFact.getMarketDate() );
						marketAccountTO.setMarketPrice( retirementMarketFact.getMarketPrice() );
						marketAccountTO.setMarketRate( retirementMarketFact.getMarketRate() );
						marketAccountTO.setQuantity( retirementMarketFact.getOperationQuantity() );
						holderAccountBalanceTO.getLstMarketFactAccounts().add(marketAccountTO);
					}
				}
			}
		}
		
		if(ComponentConstant.ZERO.equals(availableOrBlocked)){
			if(retirementDetail.getBanBalance().compareTo(BigDecimal.ZERO) > 0 || 
					retirementDetail.getPawnBalance().compareTo(BigDecimal.ZERO) > 0 ||
						retirementDetail.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){
				for(RetirementBlockDetail retirementBlockDetail : retirementDetail.getRetirementBlockDetails()){
					if(LevelAffectationType.BLOCK.getCode().equals(retirementBlockDetail.getBlockOperation().getBlockLevel())){
						for(RetirementMarketfact retirementMarketFact : retirementBlockDetail.getRetirementMarketfacts()){
							MarketFactAccountTO marketAccountTO=new MarketFactAccountTO();
							marketAccountTO.setMarketDate( retirementMarketFact.getMarketDate() );
							marketAccountTO.setMarketPrice( retirementMarketFact.getMarketPrice() );
							marketAccountTO.setMarketRate( retirementMarketFact.getMarketRate() );
							marketAccountTO.setQuantity( retirementMarketFact.getOperationQuantity() );
							holderAccountBalanceTO.getLstMarketFactAccounts().add(marketAccountTO);
						}
						
						holderAccountBalanceTO.setStockQuantity(holderAccountBalanceTO.getStockQuantity().add(retirementBlockDetail.getBlockQuantity()));
					}
				}
			}
		}*/
		
		if(holderAccountBalanceTO.getStockQuantity().compareTo(BigDecimal.ZERO) > 0){
			if(sourceTarget.equals(ComponentConstant.SOURCE)){
				objAccountComponent.getLstSourceAccounts().add(holderAccountBalanceTO);
				holderAccountBalanceTO.setIdSecurityCode(retirementDetail.getRetirementOperation().getSecurity().getIdSecurityCodePk());
			}else if(sourceTarget.equals(ComponentConstant.TARGET)){
				holderAccountBalanceTO.setIdSecurityCode(retirementDetail.getRetirementOperation().getTargetSecurity().getIdSecurityCodePk());
				objAccountComponent.getLstTargetAccounts().add(holderAccountBalanceTO);
			}
		}
	}

	
	private void getHolderAccountBalance(RetirementOperation retirementOperation,AccountsComponentTO objAccountComponent, Long sourceTarget, Integer availableOrBlocked){
		HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
		holderAccountBalanceTO.setIdHolderAccount(retirementOperation.getHolderAccount().getIdHolderAccountPk());
		holderAccountBalanceTO.setIdParticipant(retirementOperation.getParticipant().getIdParticipantPk());
		holderAccountBalanceTO.setStockQuantity(retirementOperation.getTotalBalance());		
		holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
		
		if(holderAccountBalanceTO.getStockQuantity().compareTo(BigDecimal.ZERO) > 0){
			if(sourceTarget.equals(ComponentConstant.SOURCE)){
				objAccountComponent.getLstSourceAccounts().add(holderAccountBalanceTO);
				holderAccountBalanceTO.setIdSecurityCode(retirementOperation.getSecurity().getIdSecurityCodePk());
			}else if(sourceTarget.equals(ComponentConstant.TARGET)){
				holderAccountBalanceTO.setIdSecurityCode(retirementOperation.getTargetSecurity().getIdSecurityCodePk());
				objAccountComponent.getLstTargetAccounts().add(holderAccountBalanceTO);
			}
		}
	}

		
	/**
	 * Change state retirement operation.
	 *
	 * @param objRetirementOperation the obj retirement operation
	 * @param loggerUser the logger user
	 * @param newState the new state
	 * @throws ServiceException the service exception
	 */
	public void confirmRetirementCorporative(RetirementOperation objRetirementOperation, LoggerUser loggerUser) throws ServiceException {

		RetirementOperation retirementOperation = find(RetirementOperation.class, objRetirementOperation.getIdRetirementOperationPk());
		retirementOperation.setAudit(loggerUser);
		retirementOperation.setState(RetirementOperationStateType.CONFIRMED.getCode());
		retirementOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
		retirementOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
		retirementOperation.getCustodyOperation().setAudit(loggerUser);
		retirementOperation.getCustodyOperation().setState(retirementOperation.getState());

		update(retirementOperation);
	}
	
	public void cancelCorporativeCuponOrAmortization(String idSecurityCodePk, PhysicalCertificate physicalCertificate) throws ServiceException {
		
		if(physicalCertificate.getIdProgramInterestFk()!=null) {
			
			List<CorporativeOperation> lstCorporativeInterest = CorporativeInterest(physicalCertificate.getIdProgramInterestFk());
			if(lstCorporativeInterest!=null && lstCorporativeInterest.size()>0) {
				ProgramInterestCoupon programInterestCupon = null;
				for(CorporativeOperation corporativeInteres : lstCorporativeInterest) {
					
					corporativeInteres.setState(CorporateProcessStateType.ANNULLED.getCode());
					programInterestCupon = corporativeInteres.getProgramInterestCoupon();
					programInterestCupon.setStateProgramInterest(ProgramScheduleStateType.CANCELED.getCode());
	
					update(programInterestCupon);
					update(corporativeInteres);
				}
			}
			
		}else {
			
			CorporativeOperation corporativeAmortization = CorporativeAmortization(idSecurityCodePk);
			if(corporativeAmortization!=null ) {
				corporativeAmortization.setState(CorporateProcessStateType.ANNULLED.getCode());
				ProgramAmortizationCoupon programAmortization = corporativeAmortization.getProgramAmortizationCoupon();
				programAmortization.setStateProgramAmortization(ProgramScheduleStateType.CANCELED.getCode());

				update(programAmortization);
				update(corporativeAmortization);
			}
		}
		
	}
	
	public void cancelCorporativeCuponAmortization(String idSecurityCodePk) throws ServiceException {
		List<CorporativeOperation> lstCorporativeInterest = CorporativeInterest(idSecurityCodePk);
		if(lstCorporativeInterest!=null && lstCorporativeInterest.size()>0) {
			ProgramInterestCoupon programInterestCupon = null;
			for(CorporativeOperation corporativeInteres : lstCorporativeInterest) {
				
				corporativeInteres.setState(CorporateProcessStateType.ANNULLED.getCode());
				programInterestCupon = corporativeInteres.getProgramInterestCoupon();
				programInterestCupon.setStateProgramInterest(ProgramScheduleStateType.CANCELED.getCode());

				update(programInterestCupon);
				update(corporativeInteres);
			}
		}
		
		CorporativeOperation corporativeAmortization = CorporativeAmortization(idSecurityCodePk);
		if(corporativeAmortization!=null ) {
			corporativeAmortization.setState(CorporateProcessStateType.ANNULLED.getCode());
			ProgramAmortizationCoupon programAmortization = corporativeAmortization.getProgramAmortizationCoupon();
			programAmortization.setStateProgramAmortization(ProgramScheduleStateType.CANCELED.getCode());

			update(programAmortization);
			update(corporativeAmortization);
		}
	}
	
	public void physicalBalanceZeroWithRetirement(RetirementOperation objRetirementOperation) throws ServiceException {
		RetirementOperation retirementOperation = find(RetirementOperation.class, objRetirementOperation.getIdRetirementOperationPk());
		Security retSecurity = retirementOperation.getSecurity();
		Security currentSecurity = find(Security.class, retSecurity.getIdSecurityCodePk());
	
		if( currentSecurity.getShareBalance().compareTo(BigDecimal.ZERO) == 0 &&
			currentSecurity.getCirculationBalance().compareTo(BigDecimal.ZERO) == 0 &&
			currentSecurity.getPlacedBalance().compareTo(BigDecimal.ZERO) == 0 
		) {
			currentSecurity.setPhysicalBalance(BigDecimal.ZERO);	
			update(currentSecurity);
		}
	}

	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void changeStateRetirementOperation(RetirementOperation objRetirementOperation, LoggerUser loggerUser, Integer newState) throws ServiceException {
		RetirementOperation retirementOperation = find(RetirementOperation.class, objRetirementOperation.getIdRetirementOperationPk());
		
		if(RetirementOperationStateType.APPROVED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.APPROVED.getCode());
		} else if(RetirementOperationStateType.ANNULLED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.ANNULLED.getCode());
			retirementOperation.getCustodyOperation().setRejectMotive(objRetirementOperation.getRejectMotive());
			retirementOperation.getCustodyOperation().setRejectMotiveOther(objRetirementOperation.getRejectMotiveOther());
		} else if(RetirementOperationStateType.CONFIRMED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.CONFIRMED.getCode());
		} else if(RetirementOperationStateType.REJECTED.getCode().equals(newState)){
			retirementOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
			retirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			retirementOperation.setState(RetirementOperationStateType.REJECTED.getCode());
			retirementOperation.getCustodyOperation().setRejectMotive(objRetirementOperation.getRejectMotive());
			retirementOperation.getCustodyOperation().setRejectMotiveOther(objRetirementOperation.getRejectMotiveOther());
		} else if (RetirementOperationStateType.REVERSED.getCode().equals(newState)){
			retirementOperation.setState(RetirementOperationStateType.REVERSED.getCode());
			retirementOperation.getCustodyOperation().setOperationType(ParameterOperationType.REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode());
		}
		retirementOperation.getCustodyOperation().setState(retirementOperation.getState());
			
		//Lista que vamos a guardar en HolderAccountBalanceExp
		List<HolderAccountBalanceExp> accountBalanceExps = new ArrayList<>();
		
		if(RetirementOperationStateType.CONFIRMED.getCode().equals(newState)){
			
			if( retirementOperation.getMotive()!=null && 
				( retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) || 
				  //retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ||
				  retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_OTC.getCode()) ) 
			) {
				
				//accountBalanceExps = holderAccountBal(retirementOperation);

				Long idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode();

				AccountAnnotationOperation accountAnnotationOperation = getAccountAnnotationOperationWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());

				if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(accountAnnotationOperation.getMotive())){
					idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_MANAGED.getCode();
				}else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(accountAnnotationOperation.getMotive())){
					idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode();
				}

				AccountsComponentTO accountsComponentTO = getAccountsComponentTO(retirementOperation, idBusinessProcessPk);
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);


				//if( retirementOperation.getIndRetirementExpiration().equals(0) ) {
				Security currentSecurity = retirementOperation.getSecurity();
				//if(currentSecurity.getExpirationDate()!=null) {//solo los valores que tenga fecha de expiracion se cambia el estado (RF)
					currentSecurity.setStateSecurity(SecurityStateType.RETIREMENT.getCode());
				//}
				update(currentSecurity);
				
				accountAnnotationOperation.setState(DematerializationStateType.REVERSED.getCode());					
				update(accountAnnotationOperation);
				cancelCorporativeCuponAmortization(currentSecurity.getIdSecurityCodePk());
				//}
				

				try {
					vaultControlService.get().deleteCertificates(retirementOperation.getSecurity().getIdSecurityCodePk());
				}catch (Exception e) {
					System.out.println("***** ocurrio un error al retirar el valor "+retirementOperation.getSecurity().getIdSecurityCodePk()+" de boveda :: "+e.getMessage());
				}
				
			}else if( retirementOperation.getMotive()!=null && 
					( retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) || 
					  retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) ) 
			){

				RetirementOperation retirementOperationAndDependency = getRetirementOperationAndPhysicalCertificate(retirementOperation.getIdRetirementOperationPk());
				String idSecurityCodePk = retirementOperation.getSecurity().getIdSecurityCodePk();
				
				//En el caso de RETIREMENT_EXPIRATION_CUPON no se ejecuta el movimiento de saldos del valores, ya que esto viene por un proceso corporativo, que ya movio los saldos del valor
				if( retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) ) {

					try {
						if(retirementOperationAndDependency.getPhysicalCertificate().getIdProgramInterestFk() == null) {
							Long idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode();
							AccountsComponentTO accountsComponentTO = getAccountsComponentTO(retirementOperation, idBusinessProcessPk);
							accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
						}
						
						cancelCorporativeCuponOrAmortization(idSecurityCodePk, retirementOperationAndDependency.getPhysicalCertificate());
					}catch (Exception e) {
						System.out.println("*** ocurrio un error al ejecutar el componente de valores para guarda exclusiva en la amortizacion del capital "+e.getMessage());					
					}
					
				}
				
				//ahora siempre va a retirar una posicion de boveda, ya que ahora se le pasara como parametro el pk del titulo tambien
				Security currentSecurity = getSecurity(idSecurityCodePk);
				AccountAnnotationOperation accountAnnotationOperation = getAccountAnnotationOperationWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());

				try {
					vaultControlService.get().deleteCertificates(retirementOperationAndDependency.getPhysicalCertificate().getIdPhysicalCertificatePk()); //currentSecurity.getIdSecurityCodePk() , 
				}catch (Exception e) {
					System.out.println("*** ocurrio un error al retirar el valor/certificado "+idSecurityCodePk+" de boveda :: "+e.getMessage());
				}
				
				/*
				PhysicalCertificate physicalCertificate = retirementOperationAndDependency.getPhysicalCertificate();
				physicalCertificate.setSituation(SituationType.PARTICIPANT.getCode());
				update(physicalCertificate);
				*/
				
				if(retirementOperationAndDependency.getPhysicalCertificate().getIdProgramInterestFk() == null) {
					//si es nulo es porque es una amortizacion de capital	
					currentSecurity.setStateSecurity(SecurityStateType.RETIREMENT.getCode());
					update(currentSecurity);

					accountAnnotationOperation.setState(DematerializationStateType.REVERSED.getCode());					
					update(accountAnnotationOperation);
					
					cancelCorporativeCuponAmortization(idSecurityCodePk);

				}
				
			}else if( retirementOperation.getMotive()!=null && 
					( retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ) 
			){

				RetirementOperation retirementOperationAndDependency = getRetirementOperationAndPhysicalCertificate(retirementOperation.getIdRetirementOperationPk());
				String idSecurityCodePk = retirementOperation.getSecurity().getIdSecurityCodePk();
				Long idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode();
				Security currentSecurity = getSecurity(idSecurityCodePk);
				
				AccountAnnotationOperation accountAnnotationOperation = getAccountAnnotationOperationWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());

				AccountsComponentTO accountsComponentTO = getAccountsComponentTO(retirementOperation, idBusinessProcessPk);
				
				if( currentSecurity.getStateSecurity()!=null && 
					currentSecurity.getStateSecurity().equals(SecurityStateType.GUARDA_EXCLUSIVE.getCode()) 
				){
					accountsComponentTO.setIdBusinessProcess(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode());
					accountsComponentTO.setIdOperationType(ParameterOperationType.SECURITIES_RETIREMENT_GUARDA_EXCLUSIVE.getCode());
				}else {
					accountsComponentTO.setIdBusinessProcess(BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_MANAGED.getCode());
					accountsComponentTO.setIdOperationType(ParameterOperationType.SECURITIES_RETIREMENT_GUARDA_MANAGED.getCode());
				}
				
				accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
				
				
				//ahora siempre va a retirar una posicion de boveda, ya que ahora se le pasara como parametro el pk del titulo tambien
				try {
					vaultControlService.get().deleteCertificates(retirementOperationAndDependency.getPhysicalCertificate().getIdPhysicalCertificatePk()); //currentSecurity.getIdSecurityCodePk() , 
				}catch (Exception e) {
					System.out.println("*** ocurrio un error al retirar el valor/certificado "+idSecurityCodePk+" de boveda :: "+e.getMessage());
				}
				
				accountAnnotationOperation.setState(DematerializationStateType.REVERSED.getCode());					
				update(accountAnnotationOperation);
			
			}
			
			if( retirementOperation.getMotive()!=null && 
				( retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) 				|| 
				  retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) 				||
				  retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_OTC.getCode()) 			|| 
				  retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) 				||
				  retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) )
			) {

				AccountAnnotationOperation accountAnnotationOperation = getAccountAnnotationOperationWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());
				
				Long idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode();
				
				/*if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(accountAnnotationOperation.getMotive())){
					idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_MANAGED.getCode();
				}else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(accountAnnotationOperation.getMotive())){
					idBusinessProcessPk = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode();
				}*/
				
					SecuritiesComponentTO securitiesComponentTO = getRetirementSecuritiesComponentTO(retirementOperation,ComponentConstant.SOURCE,idBusinessProcessPk, false);
					
					if(accountAnnotationOperation.getIndSerializable().equals(0)) {
						securitiesComponentTO.setIssuanceExecute(true);
					}else {
						securitiesComponentTO.setIdOperationType(ParameterOperationType.SEC_SECURITIES_SERIALIZED_REVERSION.getCode());
						securitiesComponentTO.setIssuanceExecute(false);
						securitiesComponentTO.setShareBalance(BigDecimal.ZERO);	
						securitiesComponentTO.setCirculationBalance(BigDecimal.ZERO);	
						securitiesComponentTO.setPhysicalBalance(BigDecimal.ZERO);	
						securitiesComponentTO.setPlacedBalance(BigDecimal.ZERO);	
					}
					
					if( accountAnnotationOperation.getIndSerializable().equals(0) && retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) ) {
						RetirementOperation retirementOperationAndDependency = getRetirementOperationAndPhysicalCertificate(retirementOperation.getIdRetirementOperationPk());
						if(retirementOperationAndDependency.getPhysicalCertificate().getIdProgramInterestFk() == null) {
							securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);	
						}
					}else {
						securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);	
					}
							

			}

			/*	
			//Validamos que la lista no esta vacia 
			if(Validations.validateIsNotNullAndNotEmpty(accountBalanceExps)){
				for(HolderAccountBalanceExp accountBalanceExp : accountBalanceExps){
					//Insertamos en HolderAccountBalanceExp
					create(accountBalanceExp);
				}
			}
			*/
			
			
		} 
		
		else if(RetirementOperationStateType.ANNULLED.getCode().equals(newState) || RetirementOperationStateType.REJECTED.getCode().equals(newState)){
			Long idBusinessProcess = null;
			
			/*
			if(retirementOperation.getAccountAnnotationOperation()!=null && retirementOperation.getAccountAnnotationOperation().getIndGuarda().equals(1)) {
				idBusinessProcess = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CANCEL.getCode();
			}else {*/
			//}
			List<RetirementDetail> lstRetirementDetail = getRetirementDetailWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());
			for(RetirementDetail current: lstRetirementDetail) {
				PhysicalCertificate pc = current.getPhysicalCertificate();
				pc.setState(PhysicalCertificateStateType.CONFIRMED.getCode());
				
				update(pc);
			}
			
			AccountAnnotationOperation accountAnnotationOperation = getAccountAnnotationOperationWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());
			if(accountAnnotationOperation !=null && DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(accountAnnotationOperation.getMotive())){
				idBusinessProcess = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_MANAGED_CANCEL.getCode();
			}else if(accountAnnotationOperation !=null && DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(accountAnnotationOperation.getMotive())){
				idBusinessProcess = BusinessProcessType.SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_CANCEL.getCode();
			}else {
				
				if(RetirementOperationStateType.ANNULLED.getCode().equals(newState)){
					idBusinessProcess = BusinessProcessType.SECURITY_RETIREMENT_CANCEL.getCode();
				}else if(RetirementOperationStateType.REJECTED.getCode().equals(newState)){
					idBusinessProcess = BusinessProcessType.SECURITY_RETIREMENT_REJECT.getCode();
				}
				
			}
			
			if( retirementOperation.getIndRetirementExpiration().equals(0) || (retirementOperation.getIndRetirementExpiration().equals(1) && !retirementOperation.getPhysicalCertificate().getBlIsCupon()) ) {
				AccountsComponentTO accountsComponentTO = getAccountsComponentTO(retirementOperation, idBusinessProcess);
				if(Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstSourceAccounts()) ||
						Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstTargetAccounts())){
					accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
				}
			}
			
			
		}
		
		else if(RetirementOperationStateType.REVERSED.getCode().equals(newState)){
			if(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(retirementOperation.getMotive())){				
				AccountsComponentTO accountsComponentTO = getAccountsComponentTO(retirementOperation, BusinessProcessType.REVERSE_EARLY_PAYMENT_RRA.getCode());
				if(Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstSourceAccounts()) ||
						Validations.validateListIsNotNullAndNotEmpty(accountsComponentTO.getLstTargetAccounts())){
					accountsComponentService.get().executeAccountsComponent(accountsComponentTO);
				}						
				SecuritiesComponentTO securitiesComponentTO = null;				
				securitiesComponentTO = getSecuritiesComponentTO(retirementOperation, ComponentConstant.SOURCE,	
						BusinessProcessType.REVERSE_EARLY_PAYMENT_RRA.getCode(), true);
				securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);	
				updateReversePaymentChronogram(securitiesComponentTO.getIdSecurityCode(), loggerUser);
				
				if(retirementOperation.getTargetSecurity()!=null){
					securitiesComponentTO = getSecuritiesComponentTO(retirementOperation, ComponentConstant.TARGET,
							BusinessProcessType.REVERSE_EARLY_PAYMENT_RRA.getCode(), true);
					securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);	
				}				
			}
		}
		update(retirementOperation);		
	}
	
	
	/**
	 * Metodo para generar HolderAccountBalanceExp
	 * @param retirementOperation
	 * @return
	 */
	public List<HolderAccountBalanceExp> holderAccountBal (RetirementOperation retirementOperation){
		
		//Lista que vamos a guardar en HolderAccountBalanceExp
		List<HolderAccountBalanceExp> accountBalanceExps = new ArrayList<>();
		
		//Saldo disponible que vamos a guardar
		Integer total = 0 ;
		
		// Recorremos para sumar y luego comparar que el disponible sea igual al saldo total
		/*for (RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()){
			total = total + retirementDetail.getAvailableBalance().intValue();
		}
		
		if(retirementOperation.getTotalBalance().intValue() == total ){
			//Recorremos todos los saldos seleccionados
			for (RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()){
				
				HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
				
				accountBalanceExp.setHolderAccount(retirementDetail.getHolderAccount().getIdHolderAccountPk());
				accountBalanceExp.setParticipant(retirementDetail.getParticipant().getIdParticipantPk());
				accountBalanceExp.setSecurity(retirementOperation.getSecurity().getIdSecurityCodePk());
				accountBalanceExp.setTotalBalance(retirementDetail.getAvailableBalance());
				accountBalanceExp.setAvailableBalance(retirementDetail.getAvailableBalance());
				accountBalanceExp.setPawnBalance(GeneralConstants.ZERO);
				accountBalanceExp.setOtherBlockBalance(GeneralConstants.ZERO);
				accountBalanceExp.setBanBalance(GeneralConstants.ZERO);
				
				//agregamos a la lista
				accountBalanceExps.add(accountBalanceExp);
			}
		}
		*/

		total = retirementOperation.getTotalBalance().intValue();
		
		if(retirementOperation.getTotalBalance().intValue() == total ){
				HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
				
				accountBalanceExp.setHolderAccount(retirementOperation.getRetirementDetails().get(0).getHolderAccount().getIdHolderAccountPk());
				accountBalanceExp.setParticipant(retirementOperation.getRetirementDetails().get(0).getParticipant().getIdParticipantPk());
				accountBalanceExp.setSecurity(retirementOperation.getSecurity().getIdSecurityCodePk());
				accountBalanceExp.setTotalBalance(retirementOperation.getRetirementDetails().get(0).getAvailableBalance());
				accountBalanceExp.setAvailableBalance(retirementOperation.getRetirementDetails().get(0).getAvailableBalance());
				accountBalanceExp.setPawnBalance(GeneralConstants.ZERO);
				accountBalanceExp.setOtherBlockBalance(GeneralConstants.ZERO);
				accountBalanceExp.setBanBalance(GeneralConstants.ZERO);
				
				accountBalanceExps.add(accountBalanceExp);
		}
		
		return accountBalanceExps;
	}
	
	
	/**
	 * Execute block operations block.
	 *
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	private void executeBlockOperationsBlock(RetirementOperation retirementOperation) throws ServiceException {
		List<AffectationTransferTO> transfers = new ArrayList<AffectationTransferTO>();
		if(retirementOperation.getTargetSecurity() != null){
			for(RetirementDetail detail : retirementOperation.getRetirementDetails()){
				if(Validations.validateListIsNotNullAndNotEmpty(detail.getRetirementBlockDetails())){
					for(RetirementBlockDetail retirementBlockDetail : detail.getRetirementBlockDetails()){
						AffectationTransferTO at = new AffectationTransferTO();
						at.setRetirementBlockDetail(retirementBlockDetail);
						at.setIdHolderPk(detail.getHolderAccount().getRepresentativeHolder().getIdHolderPk());
						at.setIdHolderAccountPk(detail.getHolderAccount().getIdHolderAccountPk());
						at.setIdParticipantPk(detail.getParticipant().getIdParticipantPk());
						at.setIdSecurityCodePk(retirementOperation.getTargetSecurity().getIdSecurityCodePk());
						at.setIdBlockOperationPk(retirementBlockDetail.getBlockOperation().getIdBlockOperationPk());
						at.setQuantity(retirementBlockDetail.getBlockQuantity());
						if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
							at.setMarketFactAffectations(new ArrayList<MarketFactTO>());
							for(RetirementMarketfact retirementMktFact : retirementBlockDetail.getRetirementMarketfacts()){
								MarketFactTO marketFactAffectationTO = new MarketFactTO();
								marketFactAffectationTO.setMarketDate(retirementMktFact.getMarketDate());
								marketFactAffectationTO.setMarketPrice(retirementMktFact.getMarketPrice());
								marketFactAffectationTO.setMarketRate(retirementMktFact.getMarketRate());
								marketFactAffectationTO.setQuantity(retirementMktFact.getOperationQuantity());
								at.getMarketFactAffectations().add(marketFactAffectationTO);
							}
						}
						transfers.add(at);
					}
				}
			}
			
			affectationsServiceBean.transferBlockOperations(transfers,
					BusinessProcessType.SECURITY_RETIREMENT_CONFIRM_BLOCKED.getCode(), 
					retirementOperation.getCustodyOperation().getConfirmUser(),idepositarySetup.getIndMarketFact()) ;
		}		
	}

	/**
	 * Execute block operations unblock.
	 *
	 * @param retirementOperation the retirement operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void executeBlockOperationsUnblock(RetirementOperation retirementOperation, LoggerUser loggerUser) throws ServiceException {
		for(RetirementDetail retirementDetail : retirementOperation.getRetirementDetails()){
			
			if(Validations.validateListIsNotNullAndNotEmpty(retirementDetail.getRetirementBlockDetails())){
				for(RetirementBlockDetail retirementBlockDetail : retirementDetail.getRetirementBlockDetails()){

					// single block_operation for reversal
					BlockOpeForReversalsTO blockOpeForReversal = new BlockOpeForReversalsTO();
	        		blockOpeForReversal.setIdBlockOperationPk(retirementBlockDetail.getBlockOperation().getIdBlockOperationPk());
	        		blockOpeForReversal.setAmountUnblock(retirementBlockDetail.getBlockQuantity());
					
	        		// header for creating the unblock_request: one per block_operation
	        		RegisterReversalTO registerReversalTO = new RegisterReversalTO();
	        		registerReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
	            	registerReversalTO.setRegistryUser(retirementOperation.getCustodyOperation().getConfirmUser());
	            	registerReversalTO.setDocumentNumber(retirementBlockDetail.getBlockOperation().getBlockRequest().getBlockNumber()); // temporal
	        		registerReversalTO.setIdHolderPk(retirementDetail.getHolderAccount().getRepresentativeHolder().getIdHolderPk());
	            	registerReversalTO.setTotalUnblock(retirementBlockDetail.getBlockQuantity());
	            	registerReversalTO.setIdState(RequestReversalsStateType.APPROVED.getCode());
	            	registerReversalTO.getLstBlockOpeForReversals().add(blockOpeForReversal);
	            	
	            	if(idepositarySetup.getIndMarketFact().equals(ComponentConstant.ONE)){
	            		blockOpeForReversal.setReversalsMarkFactList(new ArrayList<ReversalsMarkFactTO>());
	    				for(RetirementMarketfact retirementMarketFact: retirementBlockDetail.getRetirementMarketfacts()){
	    					ReversalsMarkFactTO reversalMarketFact = new ReversalsMarkFactTO();
	    					reversalMarketFact.setMarketDate(retirementMarketFact.getMarketDate());
	    					reversalMarketFact.setMarketPrice(retirementMarketFact.getMarketPrice());
	    					reversalMarketFact.setMarketRate(retirementMarketFact.getMarketRate());
	    					reversalMarketFact.setBlockAmount(retirementMarketFact.getOperationQuantity());
	    					blockOpeForReversal.getReversalsMarkFactList().add(reversalMarketFact);
	    				}
	    			}
	            	
	            	//TO for confirmation
	            	RequestReversalsTO requestReversalsTO = new RequestReversalsTO();
	                requestReversalsTO.setIdUnblockRequestPk(requestReversalsServiceBean.saveReversalRequest(registerReversalTO));
	                registerReversalTO.setConfirmationUser(retirementOperation.getCustodyOperation().getConfirmUser());
	                requestReversalsTO.setBusinessProcessTypeId(BusinessProcessType.SECURITY_RETIREMENT_CONFIRM_BLOCKED.getCode());
	                requestReversalsServiceBean.confirmReversalRequest(requestReversalsTO, loggerUser, Boolean.FALSE);
				}
			}
			
        }
	}



	/**
	 * Creates the sec operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @param sourceorTarget the sourceor target
	 * @param operationType the operation type
	 * @return the securities operation
	 */
	private SecuritiesOperation createSecuritiesOperation(RetirementOperation retirementOperation, Long sourceorTarget, Long operationType) {
		SecuritiesOperation securitiesOperation = new SecuritiesOperation();
		if(sourceorTarget.equals(ComponentConstant.SOURCE)){
			securitiesOperation.setSecurities(retirementOperation.getSecurity());
		}else if(sourceorTarget.equals(ComponentConstant.TARGET)){
			securitiesOperation.setSecurities(retirementOperation.getTargetSecurity());
		}
		securitiesOperation.setCustodyOperation(retirementOperation.getCustodyOperation());
		securitiesOperation.setOperationDate(CommonsUtilities.currentDateTime());
		securitiesOperation.setStockQuantity(retirementOperation.getTotalBalance());
		securitiesOperation.setCashAmount(retirementOperation.getTotalBalance().multiply(retirementOperation.getSecurity().getCurrentNominalValue()));
		securitiesOperation.setOperationType(operationType);
				
		return create(securitiesOperation);
	}
	
	/**
	 * Gets the securities component to.
	 *
	 * @param retirementOperation the retirement operation
	 * @param sourceOrTarget the source or target
	 * @param businessType the business type
	 * @param isReversed the is reversed
	 * @return the securities component to
	 */
	private SecuritiesComponentTO getSecuritiesComponentTO(RetirementOperation retirementOperation, Long sourceOrTarget, Long businessType, boolean isReversed){
		Long operationType= null;
		if (BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode().equals(businessType)) {
			if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode())  || retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ){
				operationType= ParameterOperationType.SEC_SECURITIES_REVERSION.getCode();
			}else if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode())){
				operationType= ParameterOperationType.SEC_SECURITIES_REDEMPTION_EQUITIES.getCode();	
			}else if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode())){
				operationType= ParameterOperationType.SEC_SECURITIES_REDEMPTION_FIXED_INCOME.getCode();	
			}
		}if (BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_MANAGED.getCode().equals(businessType)) {
			operationType= ParameterOperationType.SECURITY_RETIREMENT_GUARDA_MANAGED_CONFIRM.getCode();
			
		}else if (BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode().equals(businessType)) {
			operationType= ParameterOperationType.SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_CONFIRM.getCode();
			
		} else if (BusinessProcessType.SIRTEX_EARLY_PAYMENT_CONFIRM.getCode().equals(businessType)) {
			operationType= ParameterOperationType.SEC_SECURITIES_ANTICIPATED_REDEMPTION.getCode();
		} else if(BusinessProcessType.REVERSE_EARLY_PAYMENT_RRA.getCode().equals(businessType)){
			operationType= ParameterOperationType.SECURITIES_REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode();
		}
		
		SecuritiesOperation objSecuritiesOperation = createSecuritiesOperation(retirementOperation,sourceOrTarget, operationType);
		
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();		
		securitiesComponentTO.setIdBusinessProcess(businessType);
		if(isReversed){
			securitiesComponentTO.setReverseEarlyPayment(true);
		} else {
			securitiesComponentTO.setEarlyPayment(true);
		}
		securitiesComponentTO.setIdOperationType(objSecuritiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(objSecuritiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setIdSecurityCode(objSecuritiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setIndSourceTarget(sourceOrTarget);
		
		if(securitiesComponentTO.getIdOperationType().equals(ParameterOperationType.SEC_SECURITIES_REDEMPTION_EQUITIES.getCode()) ||
			securitiesComponentTO.getIdOperationType().equals(ParameterOperationType.SEC_SECURITIES_REDEMPTION_FIXED_INCOME.getCode()) ||
			securitiesComponentTO.getIdOperationType().equals(ParameterOperationType.SEC_SECURITIES_ANTICIPATED_REDEMPTION.getCode()))
		{
			securitiesComponentTO.setDematerializedBalance(objSecuritiesOperation.getStockQuantity());
			securitiesComponentTO.setShareBalance(objSecuritiesOperation.getStockQuantity());	
			securitiesComponentTO.setCirculationBalance(objSecuritiesOperation.getStockQuantity());	
		}else if(securitiesComponentTO.getIdOperationType().equals(ParameterOperationType.SEC_SECURITIES_REVERSION.getCode())){
			securitiesComponentTO.setDematerializedBalance(objSecuritiesOperation.getStockQuantity());
			securitiesComponentTO.setPhysicalBalance(objSecuritiesOperation.getStockQuantity());	
		} else if(securitiesComponentTO.getIdOperationType().equals(ParameterOperationType.SECURITIES_REVERSION_SECURITIES_REDEMPTION_FIXED_INCOME.getCode())){
			securitiesComponentTO.setDematerializedBalance(objSecuritiesOperation.getStockQuantity());
			securitiesComponentTO.setShareBalance(objSecuritiesOperation.getStockQuantity());	
			securitiesComponentTO.setCirculationBalance(objSecuritiesOperation.getStockQuantity());	
		}
			 
		return securitiesComponentTO;
	}

	private SecuritiesComponentTO getRetirementSecuritiesComponentTO(RetirementOperation retirementOperation, Long sourceOrTarget, Long businessType, boolean isReversed){
		Long operationType= ParameterOperationType.SEC_SECURITIES_REVERSION.getCode();
		/*
		if (BusinessProcessType.SECURITY_RETIREMENT_CONFIRM.getCode().equals(businessType)) {
			operationType= ParameterOperationType.SEC_SECURITIES_REVERSION.getCode();
		}else if (BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_MANAGED.getCode().equals(businessType)) {
			operationType= ParameterOperationType.SECURITY_RETIREMENT_GUARDA_MANAGED_CONFIRM.getCode();
		}else if (BusinessProcessType.SECURITY_RETIREMENT_GUARDA_CONFIRM_EXCLUSIVE.getCode().equals(businessType)) {
			operationType= ParameterOperationType.SECURITY_RETIREMENT_GUARDA_EXCLUSIVE_CONFIRM.getCode();
		} */
		
		SecuritiesOperation objSecuritiesOperation = createSecuritiesOperation(retirementOperation,sourceOrTarget, operationType);
		
		SecuritiesComponentTO securitiesComponentTO = new SecuritiesComponentTO();		
		securitiesComponentTO.setIdBusinessProcess(businessType);
		/*if(isReversed){
			securitiesComponentTO.setReverseEarlyPayment(true);
		} else {
			securitiesComponentTO.setEarlyPayment(true);
		}*/
		securitiesComponentTO.setIdOperationType(objSecuritiesOperation.getOperationType());
		securitiesComponentTO.setIdSecuritiesOperation(objSecuritiesOperation.getIdSecuritiesOperationPk());
		securitiesComponentTO.setIdSecurityCode(objSecuritiesOperation.getSecurities().getIdSecurityCodePk());
		securitiesComponentTO.setIndSourceTarget(sourceOrTarget);

		securitiesComponentTO.setDematerializedBalance(objSecuritiesOperation.getStockQuantity());
		securitiesComponentTO.setShareBalance(objSecuritiesOperation.getStockQuantity());	
		securitiesComponentTO.setCirculationBalance(objSecuritiesOperation.getStockQuantity());	
		securitiesComponentTO.setPhysicalBalance(objSecuritiesOperation.getStockQuantity());	
		securitiesComponentTO.setPlacedBalance(objSecuritiesOperation.getStockQuantity());	
			 
		return securitiesComponentTO;
	}


	/**
	 * Gets the prepaid security.
	 *
	 * @param idSecurityCode the id security code
	 * @return the prepaid security
	 */
	public Security getPrepaidSecurity(String idSecurityCode) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();			
		
		sbQuery.append("SELECT 	sec " );
		sbQuery.append("  FROM  Security sec ");
		sbQuery.append(" WHERE 	sec.securityOrigin.idSecurityCodePk = :idSecurityCode ");
		sbQuery.append("   and  sec.stateSecurity  in  ( :registeredState ) ");
		sbQuery.append("   and  sec.indPrepaid= :indPrepaid");

		parameters.put("idSecurityCode", idSecurityCode);
		parameters.put("indPrepaid", ComponentConstant.ONE);
		
		//parameters.put("registeredState", SecurityStateType.REGISTERED.getCode());
		
		List<Integer> lstState= new ArrayList<Integer>();
		lstState.add(SecurityStateType.REGISTERED.getCode());
		lstState.add(SecurityStateType.EXPIRED.getCode());
		parameters.put("registeredState", lstState);
		
        Security objectList = (Security) findObjectByQueryString(sbQuery.toString(), parameters);
        return objectList;
	}

	/**
	 * Gets the issuer security.
	 *
	 * @param idSecurityCode the id security code
	 * @return the prepaid security
	 */
	/*public Security getSecurityWithIssuer(String idSecurityCode) {
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String,Object>();			
		
		sbQuery.append("SELECT 	sec " );
		sbQuery.append("  FROM  Security sec ");
		sbQuery.append("  inner join fetch  sec.issuer iss ");
		sbQuery.append("  left join fetch  iss.holder hold ");
		sbQuery.append(" WHERE 	sec.idSecurityCodePk = :idSecurityCode ");
		sbQuery.append("   and  sec.stateSecurity  in  ( :registeredState ) ");

		parameters.put("idSecurityCode", idSecurityCode);
	
		List<Integer> lstState= new ArrayList<Integer>();
		lstState.add(SecurityStateType.REGISTERED.getCode());
		parameters.put("registeredState", lstState);
		
        Security objectList = (Security) findObjectByQueryString(sbQuery.toString(), parameters);
        return objectList;
	}
	*/
	
	/**
	 * Gets the retirement block details.
	 *
	 * @param idRetirementDetailPk the id retirement detail pk
	 * @return the retirement block details
	 */
	@SuppressWarnings("unchecked")
	public List<RetirementBlockDetail> getRetirementBlockDetails(Long idRetirementDetailPk) {
		
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select rbd From RetirementBlockDetail rbd ");
		sbQuery.append("   inner join fetch rbd.blockOperation bo ");
		sbQuery.append("   inner join fetch bo.blockRequest br ");
		sbQuery.append("   inner join fetch br.blockEntity be ");
		sbQuery.append("   Where rbd.retirementDetail.idRetirementDetailPk = :idRetirementDetail ");
		
		parameters.put("idRetirementDetail", idRetirementDetailPk);
		
		return findListByQueryString(sbQuery.toString(), parameters);
	}



	/**
	 * Gets the issuer pk.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuer pk
	 */
	public String getIssuerPk(String idSecurityCodePk) {
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select sec.issuer.idIssuerPk From Security sec");
		sbQuery.append("   Where sec.idSecurityCodePk = :idSecurityCodePk ");
		
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		
		return (String)findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Gets the list holder account.
	 *
	 * @param idParticipant the id participant
	 * @param idHolder the id holder
	 * @return the list holder account
	 */
	public List<HolderAccount> getListHolderAccount(Long idParticipant, Long idHolder) {
		List<HolderAccount> lstHolderAccount= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HA ");
		stringBuffer.append(" FROM HolderAccount HA, HolderAccountDetail HAD ");
		stringBuffer.append(" WHERE HA.idHolderAccountPk = HAD.holderAccount.idHolderAccountPk ");
		stringBuffer.append(" and HA.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and HAD.holder.idHolderPk = :idHolder ");
		stringBuffer.append(" and HA.stateAccount = :accountState ");
		
		TypedQuery<HolderAccount> typedQuery= em.createQuery(stringBuffer.toString(), HolderAccount.class);
		typedQuery.setParameter("idParticipant", idParticipant);
		typedQuery.setParameter("idHolder", idHolder);
		typedQuery.setParameter("accountState", HolderAccountStatusType.ACTIVE.getCode());
		lstHolderAccount = typedQuery.getResultList();
		return lstHolderAccount;
	}
	
	/**
	 * Gets the list holder account balance.
	 *
	 * @param idHolderAccount the id holder account
	 * @param lstIssuer the lst issuer
	 * @param secutiyClass the secutiy class
	 * @return the list holder account balance
	 */
	public List<HolderAccountBalance> getListHolderAccountBalance(Long idHolderAccount, List<Issuer> lstIssuer, Integer secutiyClass) {
		List<HolderAccountBalance> lstHolderAccountBalance= null;
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT HAB ");
		stringBuffer.append(" FROM HolderAccountBalance HAB 								");
		stringBuffer.append(" INNER JOIN FETCH HAB.security SE 								");
		stringBuffer.append(" INNER JOIN FETCH HAB.holderMarketfactBalance hmb 				");
		stringBuffer.append(" WHERE HAB.holderAccount.idHolderAccountPk = :idHolderAccount 	");
		stringBuffer.append(" and HAB.availableBalance > 0 									");
		stringBuffer.append(" and hmb.availableBalance > 0 									");
		stringBuffer.append(" and SE.issuer.idIssuerPk in (:lstIdIssuer) 					");	
		if (Validations.validateIsNotNull(secutiyClass)) {
			stringBuffer.append(" and SE.securityClass = :secutiyClass ");
		}
		
		TypedQuery<HolderAccountBalance> typedQuery= em.createQuery(stringBuffer.toString(),HolderAccountBalance.class);
		typedQuery.setParameter("idHolderAccount", idHolderAccount);
		List<String> lstIdIssuer= new ArrayList<String>();
		for (Issuer issuer: lstIssuer) {
			lstIdIssuer.add(issuer.getIdIssuerPk());
		}
		typedQuery.setParameter("lstIdIssuer", lstIdIssuer);
		if (Validations.validateIsNotNull(secutiyClass)) {
			typedQuery.setParameter("secutiyClass", secutiyClass);
		}
		lstHolderAccountBalance= typedQuery.getResultList();
		return lstHolderAccountBalance;
	}
	
	/**
	 * Save withdrawal sirtex operation.
	 *
	 * @param objRegisterWithdrawalSiterx the obj register withdrawal siterx
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String saveWithdrawalSirtexOperation(RegisterWithdrawalSiterxTO objRegisterWithdrawalSiterx, LoggerUser loggerUser) throws ServiceException{
		String strNroSolicitudes= GeneralConstants.EMPTY_STRING;
		for (TransferAccountBalanceTO objTransferAccountBalanceTO: objRegisterWithdrawalSiterx.getLstHolderAccountBalance()) {
			if(Validations.validateIsNotNull(objTransferAccountBalanceTO.getQuantityRemoveBalance()) && 
			   objTransferAccountBalanceTO.getQuantityRemoveBalance().compareTo(BigDecimal.ZERO) > 0)
			{
				RetirementOperation objRetirementOperation= new RetirementOperation();
				objRetirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
				Security securities= new Security();
				securities.setIdSecurityCodePk(objTransferAccountBalanceTO.getIdSecurityCodePk());
				objRetirementOperation.setSecurity(securities);
				objRetirementOperation.setIndWithdrawalBCB(BooleanType.YES.getCode());
				objRetirementOperation.setIndRetirementExpiration(BooleanType.NO.getCode());
				objRetirementOperation.setTotalBalance(objTransferAccountBalanceTO.getQuantityRemoveBalance());
				objRetirementOperation.setMotive(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode());
				objRetirementOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
				
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_EARLY_PAYMENT.getCode());
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				custodyOperation.setRegistryUser(loggerUser.getUserName());
				custodyOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
				objRetirementOperation.setCustodyOperation(custodyOperation);
		        
				Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_SECURITIES_WITHDRAWAL);
		        custodyOperation.setOperationNumber(operationNumber);
		        if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strNroSolicitudes)) {
		        	strNroSolicitudes += operationNumber;
		        } else {
		        	strNroSolicitudes += GeneralConstants.STR_COMMA + operationNumber;
		        }
				create(objRetirementOperation);
				
				RetirementDetail retirementDetail = new RetirementDetail();
				retirementDetail.setAvailableBalance(objTransferAccountBalanceTO.getQuantityRemoveBalance());
				retirementDetail.setHolderAccount(new HolderAccount(objTransferAccountBalanceTO.getIdHolderAccountPk()));
				retirementDetail.setParticipant(new Participant(objTransferAccountBalanceTO.getIdParticipantPk()));
				retirementDetail.setRetirementOperation(objRetirementOperation);
				
				if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact())) {
					retirementDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());
					for(MarketFactTO marketFactTO : objTransferAccountBalanceTO.getMarketFactDetails()){
						RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
						retirementMarketfact.setRetirementDetail(retirementDetail);
						retirementMarketfact.setMarketDate(marketFactTO.getMarketDate());
						retirementMarketfact.setMarketRate(marketFactTO.getMarketRate());
						retirementMarketfact.setMarketPrice(marketFactTO.getMarketPrice());
						retirementMarketfact.setOperationQuantity(marketFactTO.getQuantity());
						retirementDetail.getRetirementMarketfacts().add(retirementMarketfact);
					}
				}
				create(retirementDetail);
				saveAll(retirementDetail.getRetirementMarketfacts());
				
				AccountsComponentTO objAccountComponent = new AccountsComponentTO();		
				objAccountComponent.setIdBusinessProcess(BusinessProcessType.SIRTEX_EARLY_PAYMENT_REGISTER.getCode());
				objAccountComponent.setIdCustodyOperation(objRetirementOperation.getIdRetirementOperationPk());
				objAccountComponent.setIdOperationType(ParameterOperationType.SECURITIES_EARLY_PAYMENT.getCode());
				objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
				objAccountComponent.setLstSourceAccounts(new ArrayList<HolderAccountBalanceTO>());
				
				getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
				accountsComponentService.get().executeAccountsComponent(objAccountComponent);
			}
		}
		return strNroSolicitudes;
	}
	
	/**
	 * Execute sirtex withdrawal operation.
	 *
	 * @param lstWithdrawalSirtexOperation the lst withdrawal sirtex operation
	 * @param withdrawalState the withdrawal state
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String executeSirtexWithdrawalOperation(List<RetirementOperationTO> lstWithdrawalSirtexOperation, 
													Integer withdrawalState, LoggerUser loggerUser) throws ServiceException {
		String strNroSolicitudes= GeneralConstants.EMPTY_STRING;
		for (RetirementOperationTO objRetirementOperationTO: lstWithdrawalSirtexOperation) {
			RetirementOperation retirementOperation = find(RetirementOperation.class, objRetirementOperationTO.getIdRetirementOperationPk());
			if (!RetirementOperationStateType.REGISTERED.getCode().equals(retirementOperation.getState())) {
				Object[] param= new Object[1];
				param[0] = retirementOperation.getCustodyOperation().getOperationNumber();
				throw new ServiceException(ErrorServiceType.SIRTEX_WITHDRAWAL_STATE_ERROR, param);
			}
			
			retirementOperation.setState(withdrawalState);
			retirementOperation.getCustodyOperation().setState(retirementOperation.getState());
			
			//we execute the account component
			AccountsComponentTO objAccountComponent = new AccountsComponentTO();
			if (RetirementOperationStateType.CONFIRMED.getCode().equals(withdrawalState)) {
				objAccountComponent.setIdBusinessProcess(BusinessProcessType.SIRTEX_EARLY_PAYMENT_CONFIRM.getCode());
			} else if (RetirementOperationStateType.REJECTED.getCode().equals(withdrawalState)) {
				objAccountComponent.setIdBusinessProcess(BusinessProcessType.SIRTEX_EARLY_PAYMENT_REJECT.getCode());
			}
			objAccountComponent.setIdCustodyOperation(retirementOperation.getIdRetirementOperationPk());
			objAccountComponent.setIdOperationType(ParameterOperationType.SECURITIES_EARLY_PAYMENT.getCode());
			objAccountComponent.setIndMarketFact(idepositarySetup.getIndMarketFact());
			objAccountComponent.setLstSourceAccounts(new ArrayList<HolderAccountBalanceTO>());
			
			for(RetirementDetail retirementDetail:retirementOperation.getRetirementDetails()){
				getHolderAccountBalance(retirementDetail, objAccountComponent, ComponentConstant.SOURCE, ComponentConstant.ONE);
			}
			accountsComponentService.get().executeAccountsComponent(objAccountComponent);
			
			if (RetirementOperationStateType.CONFIRMED.getCode().equals(withdrawalState)) {
				retirementOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
				retirementOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
				
				SecuritiesComponentTO securitiesComponentTO = getSecuritiesComponentTO(retirementOperation,ComponentConstant.SOURCE, 
																						objAccountComponent.getIdBusinessProcess(), false);
				securitiesComponentService.get().executeSecuritiesComponent(securitiesComponentTO);
				//we update the payment chronogram
				updatePaymentChronogram(securitiesComponentTO.getIdSecurityCode(), loggerUser);
			} else {
				retirementOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
				retirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			}
			
			update(retirementOperation);
			
			if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strNroSolicitudes)) {
	        	strNroSolicitudes += retirementOperation.getCustodyOperation().getOperationNumber();
	        } else {
	        	strNroSolicitudes += GeneralConstants.STR_COMMA + retirementOperation.getCustodyOperation().getOperationNumber();
	        }
		}
		return strNroSolicitudes;
	}
	
	/**
	 * Update payment chronogram.
	 *
	 * @param idSecurityCode the id security code
	 * @param loggerUser the logger user
	 */
	private void updatePaymentChronogram(String idSecurityCode, LoggerUser loggerUser) {
		
		Object[] arrSecurityDetail = getSecurityDetails(idSecurityCode);
		Integer securityState = (Integer)arrSecurityDetail[0];
		Integer securityClass = (Integer)arrSecurityDetail[1];
		
		if(securityState.equals(SecurityStateType.REDEEMED.getCode())){
			//we must annul all the payment coupons and corporative events
			List<Long> lstIdProgramInterestCoupon= new ArrayList<Long>();
			List<Long> lstIdProgramAmortizationCoupon= new ArrayList<Long>();
			List<Long> lstIdCorporativeOperation= new ArrayList<Long>();
			List<CorporativeOperation> lstCorporativeOperation= getListCorporativeOperation(idSecurityCode);
			if (Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)) {
				for (CorporativeOperation objCorporativeOperation: lstCorporativeOperation) {
					lstIdCorporativeOperation.add(objCorporativeOperation.getIdCorporativeOperationPk());
					if (Validations.validateIsNotNull(objCorporativeOperation.getProgramAmortizationCoupon())) {
						lstIdProgramAmortizationCoupon.add(objCorporativeOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
					} else if (Validations.validateIsNotNull(objCorporativeOperation.getProgramInterestCoupon())) {
						lstIdProgramInterestCoupon.add(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk());
					}
				}
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdCorporativeOperation)) {
				updateCorporativeOperationState(lstIdCorporativeOperation, CorporateProcessStateType.ANNULLED.getCode(), loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramAmortizationCoupon)) {
				Integer state = null;
				Integer situation = null;
				if(securityClass.equals(SecurityClassType.DPF.getCode()) || securityClass.equals(SecurityClassType.DPA.getCode())){
					state = ProgramScheduleStateType.REDEEMED.getCode();
					situation = ProgramScheduleSituationType.ISSUER_PAID.getCode();
				}else{
					state = ProgramScheduleStateType.CANCELED.getCode();
				}
				
				updateProgramAmortizationState(lstIdProgramAmortizationCoupon, state, situation , loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				updateProgramInterestState(lstIdProgramInterestCoupon, ProgramScheduleStateType.CANCELED.getCode(), loggerUser);
			}
		}
	}
	
	/**
	 * Gets the security details.
	 *
	 * @param idSecurityCode the id security code
	 * @return the security details
	 */
	private Object[] getSecurityDetails(String idSecurityCode) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT stateSecurity, securityClass ");
		stringBuffer.append(" FROM Security ");
		stringBuffer.append(" WHERE idSecurityCodePk = :idSecurityCode ");
		
		Query typedQuery= em.createQuery(stringBuffer.toString());
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		Object[] resultSecurity = (Object[]) typedQuery.getSingleResult();
		return resultSecurity;
		
	}
	
	/**
	 * Gets the list corporative operation.
	 *
	 * @param idSecurityCode the id security code
	 * @return the list corporative operation
	 */
	private List<CorporativeOperation> getListCorporativeOperation(String idSecurityCode) {
		List<CorporativeOperation> lstCorporativeOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT CO ");
		stringBuffer.append(" FROM CorporativeOperation CO ");
		stringBuffer.append(" INNER JOIN FETCH CO.corporativeEventType ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programInterestCoupon ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programAmortizationCoupon ");
		stringBuffer.append(" WHERE CO.securities.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" AND CO.state in (:lstState) ");
		
		TypedQuery<CorporativeOperation> typedQuery= em.createQuery(stringBuffer.toString(),CorporativeOperation.class);
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		List<Integer> lstState= new ArrayList<Integer>();
		lstState.add(CorporateProcessStateType.REGISTERED.getCode());
		lstState.add(CorporateProcessStateType.MODIFIED.getCode());
		typedQuery.setParameter("lstState", lstState);
		
		lstCorporativeOperation= typedQuery.getResultList();
		return lstCorporativeOperation;
	}
	
	/**
	 * Update corporative operation state.
	 *
	 * @param lstIdCorporativeOperation the lst id corporative operation
	 * @param corporativeState the corporative state
	 * @param loggerUser the logger user
	 */
	private void updateCorporativeOperationState(List<Long> lstIdCorporativeOperation, Integer corporativeState, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE CorporativeOperation ");
		stringBuffer.append(" SET state = :corporativeState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idCorporativeOperationPk in (:lstIdCorporativeOperation) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdCorporativeOperation", lstIdCorporativeOperation);
		query.setParameter("corporativeState", corporativeState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update program interest state.
	 *
	 * @param lstIdProgramInterestCoupon the lst id program interest coupon
	 * @param couponState the coupon state
	 * @param loggerUser the logger user
	 */
	private void updateProgramInterestState(List<Long> lstIdProgramInterestCoupon, Integer couponState, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramInterestCoupon ");
		stringBuffer.append(" SET stateProgramInterest = :couponState ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramInterestPk in (:lstIdProgramInterestCoupon) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdProgramInterestCoupon", lstIdProgramInterestCoupon);
		query.setParameter("couponState", couponState);
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Update program amortization state.
	 *
	 * @param lstIdProgramAmortizationCoupon the lst id program amortization coupon
	 * @param couponState the coupon state
	 * @param couponSituation the coupon situation
	 * @param loggerUser the logger user
	 */
	private void updateProgramAmortizationState(List<Long> lstIdProgramAmortizationCoupon, Integer couponState, Integer couponSituation, LoggerUser loggerUser) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" UPDATE ProgramAmortizationCoupon ");
		stringBuffer.append(" SET stateProgramAmortization = :couponState ");
		if(couponSituation!=null){
			stringBuffer.append(" , situationProgramAmortization = :couponSituation ");
		}
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idProgramAmortizationPk in (:lstIdProgramAmortizationCoupon) ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("lstIdProgramAmortizationCoupon", lstIdProgramAmortizationCoupon);
		query.setParameter("couponState", couponState);
		if(couponSituation!=null){
			query.setParameter("couponSituation", couponSituation);
		}
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		
		query.executeUpdate();
	}
	
	/**
	 * Gets the list issuer by mnemonic.
	 *
	 * @param mnemonics the mnemonics
	 * @return the list issuer by mnemonic
	 */
	public List<Issuer> getListIssuerByMnemonic(List<String> mnemonics) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT ISS ");
		stringBuffer.append(" FROM Issuer ISS ");
		stringBuffer.append(" WHERE mnemonic in (:mnemonics) ");
		
		TypedQuery<Issuer> typedQuery= em.createQuery(stringBuffer.toString(), Issuer.class);
		typedQuery.setParameter("mnemonics", mnemonics);
		
		return typedQuery.getResultList();
	}
	
	public List<Issuer> getListIssuerEnabled() {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT ISS ");
		stringBuffer.append(" FROM Issuer ISS ");
		stringBuffer.append(" WHERE stateIssuer in (:stateIssuer) ");
		TypedQuery<Issuer> typedQuery= em.createQuery(stringBuffer.toString(), Issuer.class);
		typedQuery.setParameter("stateIssuer", 578);
		
		return typedQuery.getResultList();
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveReverseEarlyPaymentNoDpfTx(ReverseRegisterTO reverseRegisterTO, 
			LoggerUser loggerUser) throws ServiceException{
		
		Object objResult = validateAndSaveReverseEarlyPaymentNoDpf(reverseRegisterTO, loggerUser);
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * issue 1239: metodo que valida y registra una Reversion de Cancelacion anticipada. No aplica a DPFs por que ya tienen su propio metodo
	 * @param reverseRegisterTO
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public Object validateAndSaveReverseEarlyPaymentNoDpf(ReverseRegisterTO reverseRegisterTO, 
			LoggerUser loggerUser) throws ServiceException {
				
		SecurityObjectTO securityObjectTO = reverseRegisterTO.getSecurityObjectTO();		
		RetirementOperation objRetirementOperation = new RetirementOperation();	
		
		//obteniendo el mnemonico del la clase de valor
		String securityClassDesc = null;
		if(Validations.validateIsNotNull(reverseRegisterTO.getSecurityObjectTO().getSecurityClass())){
			try {
				securityClassDesc=SecurityClassType.get(reverseRegisterTO.getSecurityObjectTO().getSecurityClass()).getText1();
				reverseRegisterTO.getSecurityObjectTO().setSecurityClassDesc(securityClassDesc);
			} catch (Exception e) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS, null);
			}
		}
		if(Validations.validateIsNullOrEmpty(securityClassDesc)){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE_INCORRECT_INSTRUMENT_CLASS, null);
		}
		// validando el codifgo del valor
		String idSecurityCode = CommonsUtilities.getIdSecurityCode(securityClassDesc, 
				reverseRegisterTO.getSecurityObjectTO().getSecuritySerial());
		
		// validando el participante
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(reverseRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);		
		if(objParticipant == null){			
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			reverseRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		// verificando si el nro de operacion ya existe
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getOperationNumber(), reverseRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){			
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		// verificando que la fecha de envio se la fecha actual
		Date operationDate = CommonsUtilities.truncateDateTime(reverseRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SEND_DATE, null);
		}
		
		// validaddo si se trata de la operacion RPA (Revercion de Cancelacion Anticipada)
		if(!(ComponentConstant.INTERFACE_REVERSE_EARLY_PAYMENT_NO_DPF.equals(reverseRegisterTO.getOperationCode()))){			
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// validando al clase valor
		ParameterTableTO filter = new ParameterTableTO();
		filter.setText1(reverseRegisterTO.getSecurityObjectTO().getSecurityClassDesc());		
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);
			// si es igual a DPF o DPA entontonces NO es valido
			if(securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode())
					|| securityClassParam.getParameterTablePk().equals(SecurityClassType.DPA.getCode())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
			} else {
				securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
			}						
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CLASS, null);
		}

		// obteniendo el valor desde la BBDD para hacer las validaciones necesarias
		Security objSecurity = new Security();		 
		if(idSecurityCode != null){
			objSecurity = find(Security.class, idSecurityCode);
		}		
		if(objSecurity == null){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE, null);
		} else {
			/* REALIZANDO LAS VALIDACIONES NECESARIAS RESPECTO AL VALOR */
			reverseRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
			
			// validando el estado del valor, solo deberia permitir valores en estado redimido o registrado
			if(!SecurityStateType.REDEEMED.getCode().equals(objSecurity.getStateSecurity())
					&& !SecurityStateType.REGISTERED.getCode().equals(objSecurity.getStateSecurity())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
						new Object[]{ SecurityStateType.lookup.get(objSecurity.getStateSecurity()).getValue() });
			}			
			
			// validando q la fecha de emision sea igual a la fecha de emision del xml enviado
			if(Validations.validateIsNotNull(securityObjectTO.getIssuanceDate()) &&
					Validations.validateIsNotNull(objSecurity.getIssuanceDate())){
				if(CommonsUtilities.convertDatetoString(objSecurity.getIssuanceDate()).compareTo(
						CommonsUtilities.convertDatetoString(securityObjectTO.getIssuanceDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
			}
			
			// validando q la fecha de vencimiento sea igual a la fecha de vencimiento del xml enviado
			if(Validations.validateIsNotNull(securityObjectTO.getExpirationDate()) &&
					Validations.validateIsNotNull(objSecurity.getExpirationDate())) {
				if(objSecurity.getExpirationDate().compareTo(securityObjectTO.getExpirationDate()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
				} 
			}else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
			// validadno que la moneda sea igual a la moneda del xml enviado
			ParameterTableTO objParameterCurrency = new ParameterTableTO();
			objParameterCurrency.setParameterTablePk(securityObjectTO.getCurrencySource());		
			objParameterCurrency.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstCurrency = parameterServiceBean.getListParameterTableServiceBean(objParameterCurrency);			
			if(Validations.validateIsNotNullAndNotEmpty(lstCurrency)){
				ParameterTable objParameterTable = lstCurrency.get(0);
				if(Validations.validateIsNotNull(objSecurity.getCurrency())
						&& !objSecurity.getCurrency().equals(objParameterTable.getParameterTablePk())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
			
			// validadno q la tasa de interes sea igual a la tasa de interes del xml enviado
			if(Validations.validateIsNotNull(securityObjectTO.getInterestRate()) && 
					Validations.validateIsNotNull(objSecurity.getInterestRate())) {
				if(objSecurity.getInterestRate().compareTo(securityObjectTO.getInterestRate()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
			}				 
			
			// validadn que el nro de cupones sea igual al nro de cupoenes del xml enviado
			boolean blValidateCupons = false;
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
				blValidateCupons = true;
			} else {
				if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
					blValidateCupons = true;
				}
			}
			/* ESE MENSAJE ES PARA EL NRO DE CUPONES INCORRECTO
			 * return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
			 */
		}
		
		// obteniendo el HolderAccount a partir del nro de Operacio a Revertir
		Long idHolderAccountFind=0L;
		if(Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getReverseOperationNumber())){
			List<InterfaceTransaction> lstInterfaceTransactionReverse = parameterServiceBean.getListInterfaceTransaction(
					objParticipant.getIdParticipantPk(), reverseRegisterTO.getReverseOperationNumber(), ComponentConstant.INTERFACE_EARLY_PAYMENT_NO_DPF);
			// deberia ser si o si solo 1 registro
			if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionReverse) &&
					lstInterfaceTransactionReverse.size() == 1){
				InterfaceTransaction objInterfaceTransaction = lstInterfaceTransactionReverse.get(0);
				// retornando el RetirementOperation a revertir
				objRetirementOperation = getRetirmentOperationWithHolderAccount(objInterfaceTransaction.getCustodyOperation().getIdCustodyOperationPk());
				
				if(Validations.validateIsNotNull(objRetirementOperation)
						&& Validations.validateIsNotNull(objRetirementOperation.getAccountAnnotationOperation())
						&& Validations.validateIsNotNull(objRetirementOperation.getAccountAnnotationOperation().getHolderAccount())
						&& Validations.validateIsNotNull(objRetirementOperation.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk())){
					idHolderAccountFind = objRetirementOperation.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk();
				}else{
					//lanzamos error de nro de operacion a revertir incorrecto
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT_RPA, null);
				}
			}
		}
		
		// obteniendo la cartera del cleinte a ser restaurada
		HolderAccount objHolderAccount = new HolderAccount();
		if(objParticipant != null && objSecurity != null){		
			HolderAccountBalance objHolderAccountBalance = new HolderAccountBalance();
			List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
					objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(), Boolean.TRUE);				
			if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
			} else {
				// encontrando al HAB correcto
				for(HolderAccountBalance hab:lstHolderAccountBalances){
					if(hab.getHolderAccount().getIdHolderAccountPk() != null){
						if(hab.getHolderAccount().getIdHolderAccountPk().equals(idHolderAccountFind)){
							objHolderAccountBalance = hab;
							break;
						}
					}				
				}
			}	
			
			//VALIDATE EXITS AVAILABLE BALANCE 
			
			if(objHolderAccountBalance != null){				
				if(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk() != null){
					objHolderAccount = find(HolderAccount.class, objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
				}				
				if(objHolderAccount == null){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT, null);
				}
			}
		}
			
		if(objParticipant != null && objSecurity != null && objHolderAccount != null){						
			
			// validando el numero de operacion a revertir
			if(Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getReverseOperationNumber())){				
				List<InterfaceTransaction> lstInterfaceTransactionReverse = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getReverseOperationNumber(), ComponentConstant.INTERFACE_EARLY_PAYMENT_NO_DPF);
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionReverse) &&
						lstInterfaceTransactionReverse.size() == 1){
					
					InterfaceTransaction objInterfaceTransaction = lstInterfaceTransactionReverse.get(0);
					
					// retornando el RetirementOperation a revertir
					objRetirementOperation = getRetirmentOperationReverse(objInterfaceTransaction.getCustodyOperation().getIdCustodyOperationPk());
					
					if(objRetirementOperation == null) {				
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT_RPA,null);
					} else {
						// validando que no haya sido revertido con aterioridad
						if(RetirementOperationStateType.REVERSED.getCode().equals(objRetirementOperation.getState())){
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_REVERT_RPA, null);
						}
						
						// validando que la fecha de reversion sea igual a la fecha de RCA realizado
						if(Validations.validateIsNotNullAndNotEmpty(objRetirementOperation.getCustodyOperation().getOperationDate()) && 
								Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getOperationDate()) ){
							if(CommonsUtilities.convertDatetoString(objRetirementOperation.getCustodyOperation().getOperationDate()).compareTo(
									CommonsUtilities.convertDatetoString(reverseRegisterTO.getOperationDate())) != 0){
								return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_RETIREMENT_DATE, null);
							} 
						} else {
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_RETIREMENT_DATE, null);
						}							
						
						// validando el idSecurittyCodePk del XML sea igual al idCodeSecurityFk de la entidad RetirementOperation
						if(Validations.validateIsNotNull(idSecurityCode) && 
								Validations.validateIsNotNull(objRetirementOperation.getSecurity().getIdSecurityCodePk())){
							if(objRetirementOperation.getSecurity().getIdSecurityCodePk().compareTo(idSecurityCode) != 0){
								return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE, null);
							}					
						} else {
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CODE, null);
						}				
					}
				} else { 
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT_RPA, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT_RPA, null);
			}
			
			// validadndo la cartera a nivel de HM
			HolderMarketFactBalance holderMarketFactBalance = null;			
			try {
				holderMarketFactBalance = getLastHolderMarketfactBalanceEarlyNoDpf(objParticipant.getIdParticipantPk(), 
						objHolderAccount.getIdHolderAccountPk(), objSecurity.getIdSecurityCodePk());				
			} catch (NonUniqueResultException nuex) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_MULTIPLE_MARKETFACTS, null);
			}			
			if(holderMarketFactBalance == null){				
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NO_MKTFACT_BALANCES, null);
			}
			
			// ejecutando la reversion
			changeStateRetirementOperation(objRetirementOperation, loggerUser, RetirementOperationStateType.REVERSED.getCode());			
			reverseRegisterTO.setIdCustodyOperation(objRetirementOperation.getIdRetirementOperationPk());								
			reverseRegisterTO.setTransactionState(BooleanType.YES.getCode());
		}									
		return reverseRegisterTO;
	}
	
	/**
	 * Validate and save reverse early payment tx.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the record validation type
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW) 
	@Interceptors(ContextHolderInterceptor.class)
	public RecordValidationType validateAndSaveReverseEarlyPaymentTx(ReverseRegisterTO reverseRegisterTO, 
			LoggerUser loggerUser) throws ServiceException{
		
		Object objResult = validateAndSaveReverseEarlyPayment(reverseRegisterTO, loggerUser);
		if(objResult instanceof RecordValidationType){
			throw new ServiceException((RecordValidationType) objResult);
		}else{
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		}
	}
	
	/**
	 * Validate and save reverse early payment.
	 *
	 * @param reverseRegisterTO the reverse register to
	 * @param loggerUser the logger user
	 * @return the object
	 * @throws ServiceException the service exception
	 */
	public Object validateAndSaveReverseEarlyPayment(ReverseRegisterTO reverseRegisterTO, 
			LoggerUser loggerUser) throws ServiceException {
				
		SecurityObjectTO securityObjectTO = reverseRegisterTO.getSecurityObjectTO();		
		RetirementOperation objRetirementOperation = new RetirementOperation();	
		
		//VALIDATE ID SECURITY CODE
		
		String idSecurityCode = CommonsUtilities.getIdSecurityCode(reverseRegisterTO.getSecurityObjectTO().getSecurityClassDesc(), 
				reverseRegisterTO.getSecurityObjectTO().getSecuritySerial());
		
		//VALIDATE PARTICIPANT
		
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(reverseRegisterTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);		
		if(objParticipant == null){			
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
		} else {
			reverseRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//CHECK OPERATION NUMBER EXITS
		
		List<InterfaceTransaction> lstInterfaceTransaction = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getOperationNumber(), reverseRegisterTO.getOperationCode());
		if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransaction)){			
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_DUPLICATE_OPERATION_TAG_OPERATION, null);
		}
		
		//CHECK OPERATION DATE IS TODAY
		
		Date operationDate = CommonsUtilities.truncateDateTime(reverseRegisterTO.getOperationDate());
		Date date = CommonsUtilities.currentDate();
		if(operationDate.compareTo(date) != 0){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO,
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_DATE, null);
		}
		
		//CHECK OPERATION TYPE
		
		if(!(ComponentConstant.INTERFACE_REVERSE_EARLY_PAYMENT_DPF.equals(reverseRegisterTO.getOperationCode()))){			
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
		}
		
		// VALIDATE SECURITY CLASS
		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setText1(reverseRegisterTO.getSecurityObjectTO().getSecurityClassDesc());		
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		if(Validations.validateListIsNotNullAndNotEmpty(securityClasses)){
			ParameterTable securityClassParam = securityClasses.get(0);			
			if(!securityClassParam.getParameterTablePk().equals(SecurityClassType.DPF.getCode())
					&& !securityClassParam.getParameterTablePk().equals(SecurityClassType.DPA.getCode())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			} else {
				securityObjectTO.setSecurityClass(securityClassParam.getParameterTablePk());
				securityObjectTO.setSecurityClassDesc(securityClassParam.getText1());
			}						
		} else {
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
		}
		
		//VALIDATE SECURITY OBJECT
		
		Security objSecurity = new Security();		 
		if(idSecurityCode != null){
			objSecurity = find(Security.class, idSecurityCode);
		}		
		if(objSecurity == null){
			return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
					GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
		} else {
			reverseRegisterTO.setIdSecurityCode(objSecurity.getIdSecurityCodePk());
			
			//VALIDATE STATE SECURITY
			
			if(!SecurityStateType.REDEEMED.getCode().equals(objSecurity.getStateSecurity())){
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OR_BLOCKED_SECURITY_CODE, null,
						new Object[]{ SecurityStateType.lookup.get(objSecurity.getStateSecurity()).getValue() });
			}			
			
			//VALIDATE ISSUANCE DATE
			
			if(Validations.validateIsNotNull(securityObjectTO.getIssuanceDate()) &&
					Validations.validateIsNotNull(objSecurity.getIssuanceDate())){
				if(CommonsUtilities.convertDatetoString(objSecurity.getIssuanceDate()).compareTo(
						CommonsUtilities.convertDatetoString(securityObjectTO.getIssuanceDate())) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_ISSUANCE_DATE, null);
			}
			
			//VALIDATE EXPIRATION DATE
			
			if(Validations.validateIsNotNull(securityObjectTO.getExpirationDate()) &&
					Validations.validateIsNotNull(objSecurity.getExpirationDate())) {
				if(objSecurity.getExpirationDate().compareTo(securityObjectTO.getExpirationDate()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
				} 
			}else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_EXPIRATION_DATE, null);
			}
			
			// VALIDATE CURRENCY SECURITY
			
			ParameterTableTO objParameterCurrency = new ParameterTableTO();
			objParameterCurrency.setText1(securityObjectTO.getCurrencyMnemonic());		
			objParameterCurrency.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			List<ParameterTable> lstCurrency = parameterServiceBean.getListParameterTableServiceBean(objParameterCurrency);			
			if(Validations.validateIsNotNullAndNotEmpty(lstCurrency)){
				ParameterTable objParameterTable = lstCurrency.get(0);
				if(Validations.validateIsNotNull(objSecurity.getCurrency())
						&& !objSecurity.getCurrency().equals(objParameterTable.getParameterTablePk())){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_CURRENCY, null);
			}
			
			// VALIDATE NOMINAL VALUE
			
			if(Validations.validateIsNotNull(securityObjectTO.getNominalValue()) &&
					Validations.validateIsNotNull(objSecurity.getCurrentNominalValue())) {
				if(objSecurity.getCurrentNominalValue().compareTo(securityObjectTO.getNominalValue()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, null);
				} 
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NOMINAL_VALUE, null);
			}
			
			//VALIDATE INTEREST RATE
			
			if(Validations.validateIsNotNull(securityObjectTO.getInterestRate()) && 
					Validations.validateIsNotNull(objSecurity.getInterestRate())) {
				if(objSecurity.getInterestRate().compareTo(securityObjectTO.getInterestRate()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_INTEREST_RATE, null);
			}				 
			
			//VALIDATE NUMBER COUPONS
			
			boolean blValidateCupons = false;
			if(Validations.validateIsNullOrEmpty(securityObjectTO.getNumberCoupons()) ){
				blValidateCupons = true;
			} else {
				if(securityObjectTO.getNumberCoupons() != null && securityObjectTO.getNumberCoupons().equals(0) ){
					blValidateCupons = true;
				}
			}
			
			//DPA - issue 158
			if(!securityObjectTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
				if(blValidateCupons){
					if(!objSecurity.getNumberCoupons().equals(1)){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
					}
				} else {
					if(!objSecurity.getNumberCoupons().equals(securityObjectTO.getNumberCoupons())){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_COUPON_NUMBER, null);
					}
				}
			}
			
			// Issue 633
			// Validando que un DPF o DPA no sea parte de un fraccionamiento
			if(securityObjectTO.getSecurityClass().equals(SecurityClassType.DPA.getCode())
					|| securityObjectTO.getSecurityClass().equals(SecurityClassType.DPF.getCode())){
				
				// buscando serie a fraccionar
				SecurityTO securityTO = new SecurityTO();
				securityTO.setIdFractionSecurityCodePk(securityObjectTO.getSecurityClassDesc() + "-" + securityObjectTO.getSecuritySerial());
				List<Security> objFractionSecurityList = securitiesQueryService.findSecuritiesDpfForFatherFraction(securityTO);
				
				if(Validations.validateIsNotNull(objFractionSecurityList) && objFractionSecurityList.size() > 0 ){

					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_SECURITY_FATHER_FRACTION, null);
					
				}
				
			}
				 						
			//VALIDATE ALTERNATIVE CODE
			
			if(Validations.validateIsNotNull(securityObjectTO.getAlternativeCode()) && 
					Validations.validateIsNotNull(objSecurity.getAlternativeCode())){
				if(objSecurity.getAlternativeCode().trim().compareTo(securityObjectTO.getAlternativeCode().trim()) != 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_ALTERNATIVE_CODE, null);
			}				
		}
		
		//VALIDATE EXITS HOLDER ACCOUNT BALANCE
		
		HolderAccount objHolderAccount = new HolderAccount();
		BigDecimal bigAvailableBalance = BigDecimal.ZERO;
		if(objParticipant != null && objSecurity != null){		
			HolderAccountBalance objHolderAccountBalance = new HolderAccountBalance();
			List<HolderAccountBalance> lstHolderAccountBalances = participantServiceBean.getListHolderAccountBalance(
					objParticipant.getIdParticipantPk(), objSecurity.getIdSecurityCodePk(), Boolean.TRUE);				
			if (Validations.validateListIsNullOrEmpty(lstHolderAccountBalances)) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT_BALANCE, null);
			} else {
				objHolderAccountBalance = lstHolderAccountBalances.get(0);				
			}	
			
			//VALIDATE EXITS AVAILABLE BALANCE 
			
			if(objHolderAccountBalance != null){				
				if(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk() != null){
					objHolderAccount = find(HolderAccount.class, objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
				}				
				if(objHolderAccount == null){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_HOLDER_ACCOUNT, null);
				} else {
					if(objHolderAccountBalance.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_EXCEED_BALANCES, null);
					}					
				}
			}
		}
			
		if(objParticipant != null && objSecurity != null && objHolderAccount != null){						
			
			//VALIDATE REVERSE OPERATION NUMBER
			
			if(Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getReverseOperationNumber())){				
				List<InterfaceTransaction> lstInterfaceTransactionReverse = parameterServiceBean.getListInterfaceTransaction(
						objParticipant.getIdParticipantPk(), reverseRegisterTO.getReverseOperationNumber(), ComponentConstant.INTERFACE_EARLY_PAYMENT_DPF);
				
				if(Validations.validateListIsNotNullAndNotEmpty(lstInterfaceTransactionReverse) &&
						lstInterfaceTransactionReverse.size() == 1){
					
					InterfaceTransaction objInterfaceTransaction = lstInterfaceTransactionReverse.get(0);
					
					//RETURN RETIREMENT OPERATION REVERSE
					
					objRetirementOperation = getRetirmentOperationReverse(objInterfaceTransaction.getCustodyOperation().getIdCustodyOperationPk());
					
					if(objRetirementOperation == null) {				
						return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
								null, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT);
					} else {
						
						//VALIDATE OPERATION DATE CUSTODY
						
						if(Validations.validateIsNotNullAndNotEmpty(objRetirementOperation.getCustodyOperation().getOperationDate()) && 
								Validations.validateIsNotNullAndNotEmpty(reverseRegisterTO.getOperationDate()) ){
							if(CommonsUtilities.convertDatetoString(objRetirementOperation.getCustodyOperation().getOperationDate()).compareTo(
									CommonsUtilities.convertDatetoString(reverseRegisterTO.getOperationDate())) != 0){
								return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_RETIREMENT_DATE, null);
							} 
						} else {
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_REVERSE_OPERATION_RETIREMENT_DATE, null);
						}							
						
						//VALIDATE SECURITY CODE IN RETIREMENT OPERATION
						
						if(Validations.validateIsNotNull(idSecurityCode) && 
								Validations.validateIsNotNull(objRetirementOperation.getSecurity().getIdSecurityCodePk())){
							if(objRetirementOperation.getSecurity().getIdSecurityCodePk().compareTo(idSecurityCode) != 0){
								return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
										GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
							}					
						} else {
							return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
									GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_SERIAL, null);
						}				
					}
				} else { 
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT, null);
				}
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION_RETIREMENT_RECURRENT, null);
			}
			
			//VALIDATE HOLDER MARKET FACT BALANCE
			
			HolderMarketFactBalance holderMarketFactBalance = null;			
			try {
				holderMarketFactBalance = getLastHolderMarketfactBalanceEarlyRedemption(objParticipant.getIdParticipantPk(), 
						objHolderAccount.getIdHolderAccountPk(), objSecurity.getIdSecurityCodePk());				
			} catch (NonUniqueResultException nuex) {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_MULTIPLE_MARKETFACTS, null);
			}			
			if(holderMarketFactBalance != null){				
				if(holderMarketFactBalance.getAvailableBalance().compareTo(bigAvailableBalance) > 0){
					return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
							GenericPropertiesConstants.MSG_INTERFACE_INVALID_SECURITY_EXCEED_BALANCES, null);
				}								
			} else {
				return CommonsUtilities.populateRecordCodeValidation(reverseRegisterTO, 
						GenericPropertiesConstants.MSG_INTERFACE_INVALID_NO_MKTFACT_BALANCES, null);
			}
			
			//EXECUTE BUSSINES
			
			changeStateRetirementOperation(objRetirementOperation, loggerUser, RetirementOperationStateType.REVERSED.getCode());			
			reverseRegisterTO.setIdCustodyOperation(objRetirementOperation.getIdRetirementOperationPk());								
			reverseRegisterTO.setTransactionState(BooleanType.YES.getCode());
		}									
		return reverseRegisterTO;
	}
	
	/**
	 * Get Retirment Operation.
	 *
	 * @param idRetirementDetail the id retirement detail
	 * @return the retirement operation
	 * @throws ServiceException the service exception
	 */
	public RetirementMarketfact getRetirementMarketfactReverse(Long idRetirementDetail) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT rmd ");
			sbQuery.append("	FROM RetirementMarketfact rmd ");
			sbQuery.append("	WHERE rmd.retirementDetail.idRetirementDetailPk = :parRetirementDetail");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parRetirementDetail", idRetirementDetail);
			RetirementMarketfact result = (RetirementMarketfact) query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	/**
	 * Get Retirment Operation.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @return the retirement operation
	 * @throws ServiceException the service exception
	 */
	public RetirementOperation getRetirmentOperationReverse(Long idCustodyOperation) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro ");
			sbQuery.append("	FROM RetirementOperation ro ");
			sbQuery.append("	INNER JOIN FETCH ro.custodyOperation co ");
			sbQuery.append("	INNER JOIN FETCH ro.retirementDetails rd ");
			sbQuery.append("	WHERE co.idCustodyOperationPk = :parCustodyOperation");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parCustodyOperation", idCustodyOperation);
			RetirementOperation result = (RetirementOperation)query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	public RetirementOperation getRetirmentOperationWithHolderAccount(Long idCustodyOperation) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro ");
			sbQuery.append("	FROM RetirementOperation ro ");
			sbQuery.append("	INNER JOIN FETCH ro.custodyOperation co ");
			sbQuery.append("	INNER JOIN FETCH ro.accountAnnotationOperation aao ");
			sbQuery.append("	INNER JOIN FETCH aao.holderAccount ha ");
			sbQuery.append("	WHERE co.idCustodyOperationPk = :parCustodyOperation");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parCustodyOperation", idCustodyOperation);
			RetirementOperation result = (RetirementOperation)query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}		
	}
	
	/**
	 * Update reverse payment chronogram.
	 *
	 * @param idSecurityCode the id security code
	 * @param loggerUser the logger user
	 */
	private void updateReversePaymentChronogram(String idSecurityCode, LoggerUser loggerUser) {
		
		Object[] arrSecurityDetail = getSecurityDetails(idSecurityCode);
		Integer securityState = (Integer)arrSecurityDetail[0];
		Integer securityClass = (Integer)arrSecurityDetail[1];
		
		if(securityState.equals(SecurityStateType.REGISTERED.getCode())){
			//we must reverse annul all the payment coupons and corporative events
			List<Long> lstIdProgramInterestCoupon= new ArrayList<Long>();
			List<Long> lstIdProgramAmortizationCoupon= new ArrayList<Long>();
			List<Long> lstIdCorporativeOperation= new ArrayList<Long>();
			List<CorporativeOperation> lstCorporativeOperation= getListReverseCorporativeOperation(idSecurityCode);
			if (Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)) {
				for (CorporativeOperation objCorporativeOperation: lstCorporativeOperation) {
					lstIdCorporativeOperation.add(objCorporativeOperation.getIdCorporativeOperationPk());
					if (Validations.validateIsNotNull(objCorporativeOperation.getProgramAmortizationCoupon())) {
						lstIdProgramAmortizationCoupon.add(objCorporativeOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
					} else if (Validations.validateIsNotNull(objCorporativeOperation.getProgramInterestCoupon())) {
						lstIdProgramInterestCoupon.add(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk());
					}
				}
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdCorporativeOperation)) {
				updateCorporativeOperationState(lstIdCorporativeOperation, CorporateProcessStateType.REGISTERED.getCode(), loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramAmortizationCoupon)) {
				Integer state = null;
				Integer situation = null;
				if(securityClass.equals(SecurityClassType.DPF.getCode()) || securityClass.equals(SecurityClassType.DPA.getCode())){
					state = ProgramScheduleStateType.PENDING.getCode();
					situation = ProgramScheduleSituationType.PENDING.getCode();
				}else{
					state = ProgramScheduleStateType.PENDING.getCode();
				}				
				updateProgramAmortizationState(lstIdProgramAmortizationCoupon, state, situation , loggerUser);
			}
			if (Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)) {
				updateProgramInterestState(lstIdProgramInterestCoupon, ProgramScheduleStateType.PENDING.getCode(), loggerUser);
			}
		}
	}
	
	/**
	 * Gets the list reverse corporative operation.
	 *
	 * @param idSecurityCode the id security code
	 * @return the list reverse corporative operation
	 */
	private List<CorporativeOperation> getListReverseCorporativeOperation(String idSecurityCode) {
		List<CorporativeOperation> lstCorporativeOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT CO ");
		stringBuffer.append(" FROM CorporativeOperation CO ");
		stringBuffer.append(" INNER JOIN FETCH CO.corporativeEventType ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programInterestCoupon ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programAmortizationCoupon ");
		stringBuffer.append(" WHERE CO.securities.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" AND CO.state in (:lstState) ");
		
		TypedQuery<CorporativeOperation> typedQuery= em.createQuery(stringBuffer.toString(),CorporativeOperation.class);
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		List<Integer> lstState= new ArrayList<Integer>();
		lstState.add(CorporateProcessStateType.ANNULLED.getCode());		
		typedQuery.setParameter("lstState", lstState);
		
		lstCorporativeOperation= typedQuery.getResultList();
		return lstCorporativeOperation;
	}
	
	
	/**
	 * Validate early payment query information.
	 *
	 * @param stockEarlyPaymentQueryTO the stock early payment query to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> validateEarlyPaymentQueryInformation(StockEarlyPaymentQueryTO stockEarlyPaymentQueryTO) throws ServiceException {
		List<RecordValidationType> lstRecordValidationType= new ArrayList<RecordValidationType>();
		//OPERATION CODE
		if (!ComponentConstant.INTERFACE_EARLY_PAYMENT_QUERY.equalsIgnoreCase(stockEarlyPaymentQueryTO.getOperationCode())) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					stockEarlyPaymentQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_OPERATION, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		//PARTICIPANT
		Participant objParticipant = new Participant();
		objParticipant.setMnemonic(stockEarlyPaymentQueryTO.getEntityNemonic());
		objParticipant = participantServiceBean.findParticipantByFiltersServiceBean(objParticipant);
		if (Validations.validateIsNull(objParticipant)) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					stockEarlyPaymentQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_ENTITY_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		} else {
			stockEarlyPaymentQueryTO.setIdParticipant(objParticipant.getIdParticipantPk());
		}
		
		//SECURITY CLASS (DPF - DPA)
		boolean isDpfDpa= false;
		ParameterTableTO filter = new ParameterTableTO();
		filter.setIndicator1(GeneralConstants.ONE_VALUE_STRING);
		filter.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		List<ParameterTable> securityClasses = parameterServiceBean.getListParameterTableServiceBean(filter);
		for (ParameterTable parameterTable: securityClasses) {
			if (parameterTable.getText1().equalsIgnoreCase(stockEarlyPaymentQueryTO.getSecurityClassDesc())) {
				isDpfDpa= true;
				stockEarlyPaymentQueryTO.setSecurityClass(parameterTable.getParameterTablePk());
				break;
			}
		}
		if (!isDpfDpa) {
			RecordValidationType recordValidationType = CommonsUtilities.populateRecordCodeValidation(
					stockEarlyPaymentQueryTO, GenericPropertiesConstants.MSG_INTERFACE_INVALID_INSTRUMENT_TYPE_CODE, null);
			lstRecordValidationType.add(recordValidationType);
			return lstRecordValidationType;
		}
		
		return lstRecordValidationType;
	}
	
	
	/**
	 * Gets the stock early payment query to.
	 *
	 * @param stockEarlyPaymentQueryTO the stock early payment query to
	 * @return the stock early payment query to
	 */
	public StockEarlyPaymentQueryTO getStockEarlyPaymentQueryTO(StockEarlyPaymentQueryTO stockEarlyPaymentQueryTO) {
		List<SecurityObjectTO> lstSecurityObjectTO= new ArrayList<SecurityObjectTO>();
		List<Object[]> lstEarlyPaymentOperation= getListDpfEarlyPaymentOperation(stockEarlyPaymentQueryTO.getOperationDate(), 
																					stockEarlyPaymentQueryTO.getIdParticipant(),
																					stockEarlyPaymentQueryTO.getSecurityClass());
		if (Validations.validateListIsNotNullAndNotEmpty(lstEarlyPaymentOperation)) {
			for (Object[] objEarlyPayment: lstEarlyPaymentOperation) {
				Long operationNumber= new Long(objEarlyPayment[0].toString());
				RetirementOperation retirementOperation=  (RetirementOperation) objEarlyPayment[1];
				SecurityObjectTO securityObjectTO= holderBalanceMovementsServiceBean.populateSecurityObjectTO(retirementOperation.getSecurity());
				securityObjectTO.setOperationNumber(operationNumber);
				lstSecurityObjectTO.add(securityObjectTO);
			}
			stockEarlyPaymentQueryTO.setLstSecurityObjectTO(lstSecurityObjectTO);
		}
		return stockEarlyPaymentQueryTO;
	}
	
	
	/**
	 * Gets the list dpf early payment operation.
	 *
	 * @param operationDate the operation date
	 * @param idParticipant the id participant
	 * @param securityClass the security class
	 * @return the list dpf early payment operation
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getListDpfEarlyPaymentOperation(Date operationDate, Long idParticipant, Integer securityClass) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT.operationNumber, ");
		stringBuffer.append(" RO ");
		stringBuffer.append(" FROM InterfaceTransaction IT, RetirementOperation RO ");
		stringBuffer.append(" INNER JOIN FETCH RO.security SE ");
		stringBuffer.append(" WHERE RO.idRetirementOperationPk = IT.custodyOperation.idCustodyOperationPk ");
		stringBuffer.append(" and trunc(IT.interfaceProcess.processDate) = :operationDate ");
		stringBuffer.append(" and IT.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		stringBuffer.append(" and RO.security.securityClass = :securityClass ");
		
		Query query= em.createQuery(stringBuffer.toString());
		query.setParameter("operationDate", operationDate);
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("transactionState", BooleanType.YES.getCode());
		query.setParameter("securityClass", securityClass);
		
		return query.getResultList();
	}
	
	/**
	 * Gets the list hol acc balance.
	 *
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @param isReversal the is reversal
	 * @return the list hol acc balance
	 */
	public List<HolderAccountBalance> getListHolAccBalance(Long idParticipant, String idSecurityCode, boolean isReversal){
		return participantServiceBean.getListHolderAccountBalance(idParticipant, idSecurityCode, isReversal);
	}

	public AccountAnnotationOperation getAccountAnnotationOperation(Long idPhysicalCertificatePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aao ");
			sbQuery.append("	FROM AccountAnnotationOperation aao ");
			sbQuery.append("	JOIN FETCH aao.security s ");
			sbQuery.append("	JOIN FETCH aao.holderAccount ha ");
			sbQuery.append("	JOIN FETCH ha.participant p ");
			sbQuery.append("	WHERE aao.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
			AccountAnnotationOperation result = (AccountAnnotationOperation)query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public AccountAnnotationOperation getAccountAnnotationOperation(String idSecurityCodePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aao ");
			sbQuery.append("	FROM AccountAnnotationOperation aao ");
			sbQuery.append("	JOIN FETCH aao.security s ");
			sbQuery.append("	JOIN FETCH aao.holderAccount ha ");
			sbQuery.append("	JOIN FETCH ha.participant p ");
			sbQuery.append("	WHERE s.idSecurityCodePk = :idSecurityCodePk ");	
			sbQuery.append("	AND aao.state = :state ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			query.setParameter("state", 858);
			AccountAnnotationOperation result = (AccountAnnotationOperation)query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public AccountAnnotationOperation getAccountAnnotationOperation(String idSecurityCodePk, Long idPhysicalCertificatePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT aao ");
			sbQuery.append("	FROM AccountAnnotationOperation aao ");
			sbQuery.append("	JOIN FETCH aao.security s ");
			sbQuery.append("	JOIN FETCH aao.holderAccount ha ");
			sbQuery.append("	JOIN FETCH ha.participant p ");
			sbQuery.append("	WHERE s.idSecurityCodePk = :idSecurityCodePk ");
			sbQuery.append("	AND aao.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
			sbQuery.append("	AND aao.state = :state ");		
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
			query.setParameter("state", 858);
			AccountAnnotationOperation result = (AccountAnnotationOperation)query.getSingleResult();
			return result;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	
	
	public AccountAnnotationOperation getAccountAnnotationOperationWithRetirementOperation(Long idRetirementOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro ");
			sbQuery.append("	FROM RetirementOperation ro ");
			sbQuery.append("	JOIN FETCH ro.accountAnnotationOperation aao ");
			//sbQuery.append("	JOIN FETCH ro.physicalCertificate pc ");
			sbQuery.append("	WHERE ro.idRetirementOperationPk = :idRetirementOperationPk ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			RetirementOperation retirementOperation = (RetirementOperation)query.getSingleResult();
			AccountAnnotationOperation result = retirementOperation.getAccountAnnotationOperation();
			return result;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public RetirementOperation getRetirementOperationAndPhysicalCertificate(Long idRetirementOperationPk) throws ServiceException{
		try {
			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro ");
			sbQuery.append("	FROM RetirementOperation ro ");
			sbQuery.append("	JOIN FETCH ro.physicalCertificate pc ");
			sbQuery.append("	WHERE ro.idRetirementOperationPk = :idRetirementOperationPk ");			
			TypedQuery<RetirementOperation> query = em.createQuery(sbQuery.toString(), RetirementOperation.class);
			query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			return query.getSingleResult();
			
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	
	public List<RetirementDetail> getRetirementDetailWithRetirementOperation(Long idRetirementOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ro ");
			sbQuery.append("	FROM RetirementOperation ro ");
			sbQuery.append("	JOIN FETCH ro.retirementDetails rd ");
			sbQuery.append("	JOIN FETCH rd.physicalCertificate pc ");
			sbQuery.append("	WHERE ro.idRetirementOperationPk = :idRetirementOperationPk ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			RetirementOperation retirementOperation = (RetirementOperation)query.getSingleResult();

			return retirementOperation.getRetirementDetails();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public PayrollDetail getPayrollDetailValid(Long idRetirementOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT pd ");
			sbQuery.append("	FROM PayrollDetail pd ");
			sbQuery.append("	WHERE pd.retirementOperation.idRetirementOperationPk = :idRetirementOperationPk AND pd.payrollState not in (:lstNotIn) ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollDetailStateType.REJECT.getCode());
			lstNotIn.add(PayrollDetailStateType.ANNULATE.getCode());
			query.setParameter("lstNotIn", lstNotIn);
			
			PayrollDetail retirementOperation = (PayrollDetail)query.getSingleResult();
			return retirementOperation;
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public PayrollDetail getPayrollDetailOnliCapitalValid(Long idRetirementOperationPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT pd ");
			sbQuery.append("	FROM PayrollDetail pd ");
			sbQuery.append("	WHERE pd.retirementOperation.idRetirementOperationPk = :idRetirementOperationPk AND pd.indCupon = :indCupon AND pd.payrollState not in (:lstNotIn) ");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
			query.setParameter("indCupon", 0);
			List<Integer> lstNotIn = new ArrayList<Integer>();
			lstNotIn.add(PayrollDetailStateType.REJECT.getCode());
			lstNotIn.add(PayrollDetailStateType.ANNULATE.getCode());
			query.setParameter("lstNotIn", lstNotIn);
			
			PayrollDetail retirementOperation = (PayrollDetail)query.getSingleResult();
			return retirementOperation;
		} catch (NoResultException e) {
			return null;
		}	
	}

	public Security getSecurity(String idSecurityCodePk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT s ");
			sbQuery.append("	FROM Security s ");
			sbQuery.append("	WHERE s.idSecurityCodePk = : idSecurityCodePk ");			
			TypedQuery<Security> query = em.createQuery(sbQuery.toString(), Security.class);
			query.setParameter("idSecurityCodePk", idSecurityCodePk);
			
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}	
	}
	
	public void annulateRetirementOperation(RetirementOperation retirementOperation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update RetirementOperation ");
		sbQuery.append(" set state = :state ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" where idRetirementOperationPk = :idRetirementOperationPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("state", retirementOperation.getState());
		query.setParameter("lastModifyDate", retirementOperation.getLastModifyDate());
		query.setParameter("lastModifyUser", retirementOperation.getLastModifyUser());
		query.setParameter("idRetirementOperationPk", retirementOperation.getIdRetirementOperationPk());
		
		query.executeUpdate();	
	} 

	
	public void annulateCustodyOperation(CustodyOperation custodyOperation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update CustodyOperation ");
		sbQuery.append(" set state = :state ");
		sbQuery.append(" , rejectMotive = :rejectMotive ");
		sbQuery.append(" , rejectMotiveOther = :rejectMotiveOther ");
		sbQuery.append(" , annulationDate = :annulationDate ");
		sbQuery.append(" , annulationUser = :annulationUser ");
		sbQuery.append(" , lastModifyDate = :lastModifyDate ");
		sbQuery.append(" , lastModifyUser = :lastModifyUser ");
		sbQuery.append(" where idCustodyOperationPk = :idCustodyOperationPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("state", custodyOperation.getState());
		query.setParameter("rejectMotive", custodyOperation.getRejectMotive());
		query.setParameter("rejectMotiveOther", custodyOperation.getRejectMotiveOther());
		query.setParameter("annulationDate", custodyOperation.getAnnulationDate());
		query.setParameter("annulationUser", custodyOperation.getAnnulationUser());
		query.setParameter("lastModifyDate", custodyOperation.getLastModifyDate());
		query.setParameter("lastModifyUser", custodyOperation.getLastModifyUser());
		query.setParameter("idCustodyOperationPk", custodyOperation.getIdCustodyOperationPk());
		
		query.executeUpdate();	
	} 
	
	public void annulatePayrollDetail(Long idPayrollHeaderPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update PayrollDetail ");
		sbQuery.append(" set payrollState = :payrollState ");
		sbQuery.append(" where payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("payrollState", PayrollDetailStateType.ANNULATE.getCode());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.executeUpdate();	
	} 
	
	public void annulatePayrollHeader(Long idPayrollHeaderPk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update PayrollHeader ");
		sbQuery.append(" set payrollState = :payrollState ");
		sbQuery.append(" where idPayrollHeaderPk = :idPayrollHeaderPk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("payrollState", PayrollHeaderStateType.ANNULATE.getCode());
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.executeUpdate();	
	} 
	
}
