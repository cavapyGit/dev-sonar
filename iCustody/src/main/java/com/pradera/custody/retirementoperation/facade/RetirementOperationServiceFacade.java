package com.pradera.custody.retirementoperation.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.custody.retirementoperation.service.RetirementOperationServiceBean;
import com.pradera.custody.retirementoperation.to.RegisterWithdrawalSiterxTO;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.tranfersecurities.accounts.service.HolderAccountServiceBean;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationOperationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.to.StockEarlyPaymentRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementMarketfact;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationRejectMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementOperationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class RetirementOperationServiceFacade {
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The remove security service bean. */
	@Inject
	private RetirementOperationServiceBean removeSecurityServiceBean;
	
	/** The request affectation service facade. */
	@EJB
	RequestAffectationServiceFacade requestAffectationServiceFacade;
	
	/** The param service bean. */
	@EJB
	private ParameterServiceBean paramServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;

	/** The holder account component service bean. */
	@EJB
	private HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The holder account service. */
	@Inject
	private HolderAccountServiceBean holderAccountService;
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;	
	
	/** The affectations service bean. */
	@EJB
	AffectationsServiceBean affectationsServiceBean;

	/** The log. */
	@Inject
	PraderaLogger log;
	/**
	 * Find all participant.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> findAllParticipant() throws ServiceException{
		Participant participantFilter = new Participant();
		  participantFilter.setState(ParticipantStateType.REGISTERED
		    .getCode());
		return this.participantServiceBean.getLisParticipantServiceBean(participantFilter);
	}
	
	public List<Participant> findAllParticipantNoCavapy() throws ServiceException{
		Participant participantFilter = new Participant();
		  participantFilter.setState(ParticipantStateType.REGISTERED
		    .getCode());

		participantFilter.setIndDepositary(BooleanType.NO.getCode());
		return this.participantServiceBean.getLisParticipantServiceBean(participantFilter);
	}
	
	/**
	 * Find participant.
	 *
	 * @param idParticipant the id participant
	 * @return the participant
	 */
	public Participant findParticipant(Long idParticipant) {
		return participantServiceBean.getParticipantByPk(idParticipant);
	}
	
	/**
	 * Find holder.
	 *
	 * @param idHolder the id holder
	 * @return the holder
	 */
	public Holder findHolder(Long idHolder) {
		Holder holder= null;
		try {
			holder= participantServiceBean.find(idHolder, Holder.class);
		} catch (NoResultException e) {
			
		}
		return holder;
	}
	
	/**
	 * Find holder account.
	 *
	 * @param idHolderAccount the id holder account
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccount) {
		HolderAccount holderAccount= null;
		try {
			holderAccount= participantServiceBean.find(idHolderAccount, HolderAccount.class);
		} catch (NoResultException e) {
			
		}
		return holderAccount;
	}
	
	/**
	 * Gets the holder accounts.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder accounts
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccounts (HolderAccountTO holderAccountTO) throws ServiceException {
		return holderAccountComponentServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO );
	}
	

	
	
/*
 - las solicitudes de retiro pueden estar en estado registrado / aprobado
 - las aprobadas tienen si o si planilla
 - todos los que estan en registrado, deben estar eliminados
 - solo los que tienen planilla en estado diferente a aprobado se anulan, junto con todas sus solicitudes
 
 */
	
	private void annulateRetirementAndCustodyOperation(RetirementOperation retirementOperation) {

		CustodyOperation custodyOperation = retirementOperation.getCustodyOperation();
		custodyOperation.setState(RetirementOperationStateType.ANNULLED.getCode());
		custodyOperation.setRejectMotive(2328); 
		custodyOperation.setRejectMotiveOther("REGISTRO VENCIDO");
		custodyOperation.setAnnulationDate(CommonsUtilities.currentDate());
		custodyOperation.setAnnulationUser("BATCH");
		custodyOperation.setLastModifyDate(CommonsUtilities.currentDate());
		custodyOperation.setLastModifyUser("BATCH");
		log.info(getClass().getName() + " automaticRetirementCancelations => update custodyOperation: "+custodyOperation.toString());
		removeSecurityServiceBean.annulateCustodyOperation(custodyOperation);
		
		RetirementOperation ro = retirementOperation;
		ro.setState(RetirementOperationStateType.ANNULLED.getCode());
		ro.setLastModifyUser("BATCH");
		ro.setLastModifyDate(CommonsUtilities.currentDate());
		ro.setIdRetirementOperationPk(custodyOperation.getIdCustodyOperationPk());
		log.info(getClass().getName() + " automaticRetirementCancelations => update retirementOperation: "+retirementOperation.toString());
		removeSecurityServiceBean.annulateRetirementOperation(ro);
	}
	
	public void automaticRetirementCancelations(Date finalDate) throws ServiceException{		
		log.info(getClass().getName() + " automaticRetirementCancelations => finalDate:"+finalDate.toString());
		
		List<RetirementOperation> lstRetirement = removeSecurityServiceBean.searchRetirementOperationNoInExpirationCupon(finalDate);
		if(lstRetirement!=null) {
			log.info(getClass().getName() + " automaticRetirementCancelations => lstRetirement size: "+lstRetirement.toString());
		}
		
		List<Long> lstIdPayrollHeader = new ArrayList<Long>();
		
		if(lstRetirement != null && lstRetirement.size()>0 ) {
			for(RetirementOperation retirementOperation : lstRetirement) {
				if( retirementOperation.getState().equals(RetirementOperationStateType.REGISTERED.getCode()) ) {
					annulateRetirementAndCustodyOperation(retirementOperation);
				}else {
					PayrollHeader payrollHeader = removeSecurityServiceBean.getPayrollHeaderWithRetirementOperation(retirementOperation.getIdRetirementOperationPk());
					if( payrollHeader != null ) {
						if( lstIdPayrollHeader.size() == 0 || (lstIdPayrollHeader.size()>0 && !lstIdPayrollHeader.contains(payrollHeader.getIdPayrollHeaderPk())) ) {
							lstIdPayrollHeader.add(payrollHeader.getIdPayrollHeaderPk());	//planilas a anular
							log.info(getClass().getName() + " automaticRetirementCancelations => lstIdPayrollHeader add: "+payrollHeader.getIdPayrollHeaderPk());
						}
						annulateRetirementAndCustodyOperation(retirementOperation);
					}
				}
			}
			
			if( lstIdPayrollHeader.size() >0) {
				for(Long idPayrollHeaderPk : lstIdPayrollHeader) {
					log.info(getClass().getName() + " update PayrollHeader/Detail: "+idPayrollHeaderPk);
					removeSecurityServiceBean.annulatePayrollDetail(idPayrollHeaderPk);
					removeSecurityServiceBean.annulatePayrollHeader(idPayrollHeaderPk);
				}	
			}
		}
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_REGISTER)
	public void saveRetirementOperation(RetirementOperation retirementOperation, boolean totalRemoval) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

        CustodyOperation custodyOperation = new CustodyOperation();

        if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) || retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REVERSION.getCode());
		}else if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_OTC.getCode())){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REVERSION.getCode());
		}else if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) || retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) ){
			custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REVERSION.getCode());
		}

        if(retirementOperation.getPhysicalCertificate() != null) {
        	AccountAnnotationOperation accountAnnotationOperation = null;
        	if(retirementOperation.getPhysicalCertificate().getPaymentDate() != null) {
        		accountAnnotationOperation = getAccountAnnotationOperation(retirementOperation.getSecurity().getIdSecurityCodePk());
        		System.out.println("******** IF saveRetirementOperation:: "+accountAnnotationOperation.toString());
        	}else {
        		//para serializados
        		accountAnnotationOperation = getAccountAnnotationOperation(retirementOperation.getSecurity().getIdSecurityCodePk(), retirementOperation.getPhysicalCertificate().getIdPhysicalCertificatePk());
        		System.out.println("******** ELSE saveRetirementOperation:: "+accountAnnotationOperation.toString());
        	}
        	
        	retirementOperation.setAccountAnnotationOperation(accountAnnotationOperation);
        	
        	if(!retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) ){
		        if(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode().equals(retirementOperation.getAccountAnnotationOperation().getMotive())){
		        	custodyOperation.setOperationType(ParameterOperationType.SECURITIES_RETIREMENT_GUARDA_MANAGED.getCode());
		        	
		        }else if( retirementOperation.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) ) {
		        	custodyOperation.setOperationType(ParameterOperationType.SECURITIES_RETIREMENT_GUARDA_EXCLUSIVE.getCode());
		        	
		        }else if(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode().equals(retirementOperation.getAccountAnnotationOperation().getMotive())){
		        	custodyOperation.setOperationType(ParameterOperationType.SECURITIES_RETIREMENT_GUARDA_EXCLUSIVE.getCode());
		        }
        	}
        }
		
		custodyOperation.setAudit(loggerUser);
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
        custodyOperation = removeSecurityServiceBean.saveCustodyOperation(custodyOperation);
		System.out.println("******** saveRetirementOperation custodyOperation:: "+custodyOperation.toString());

		retirementOperation.setAudit(loggerUser);
        retirementOperation.setCustodyOperation(custodyOperation);
        retirementOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
		
        removeSecurityServiceBean.saveRetirementOperation(retirementOperation, loggerUser, totalRemoval);
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_REGISTER)
	public void saveRetirementOperationCorporative(RetirementOperation retirementOperation, LoggerUser loggerUser) throws ServiceException {
        
        
        CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setOperationType(ParameterOperationType.SECURITIES_REVERSION.getCode());
		custodyOperation.setAudit(loggerUser);
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
        custodyOperation = removeSecurityServiceBean.saveCustodyOperation(custodyOperation);
        
		retirementOperation.setAudit(loggerUser);
        retirementOperation.setCustodyOperation(custodyOperation);
        retirementOperation.setState(RetirementOperationStateType.REGISTERED.getCode());
		
        removeSecurityServiceBean.saveRetirementOperation(retirementOperation, loggerUser, true);
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_REGISTER)
	public void saveListRetirementOperation(List<RetirementOperation> lstretirementOperation, boolean totalRemoval) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		for (RetirementOperation retirementOperation : lstretirementOperation) {

			System.out.println(":::******* saveListRetirementOperation:: "+retirementOperation.toString());
			saveRetirementOperation(retirementOperation, totalRemoval);
		}
	}
	
	public void saveListRetirementOperation(List<RetirementOperation> lstretirementOperation,LoggerUser loggerUser) throws ServiceException {
		for (RetirementOperation retirementOperation : lstretirementOperation) {
			saveRetirementOperation(retirementOperation, true);
		}
	}
	
	/**
	 * Save massive retirement operation.
	 *
	 * @param processFileTO the process file to
	 */
	public void saveMassiveRetirementOperation(ProcessFileTO processFileTO){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        ValidationProcessTO validationTO = processFileTO.getValidationProcessTO();
        validationTO.setLstValidations(new ArrayList<RecordValidationType>());
        
        // #1 separate transaction: save file
        Long idInterfaceProcess = interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(processFileTO, 
        																ExternalInterfaceReceptionType.MANUAL.getCode(), loggerUser);
        
        // #2 separate transaction: save operations independently
		for(ValidationOperationType operation : validationTO.getLstOperations()){
			StockEarlyPaymentRegisterTO stockEarlyPaymentRegisterTO = (StockEarlyPaymentRegisterTO) operation;
			stockEarlyPaymentRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
			
			try {
				
				validationTO.getLstValidations().add(removeSecurityServiceBean.validateAndSaveEarlyPaymentTx(stockEarlyPaymentRegisterTO, loggerUser));
				
				interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.YES.getCode(), loggerUser);
				
			} catch (Exception ex) {
				validationTO.getLstValidations().addAll(paramServiceBean.generateErrorValidationProcess(stockEarlyPaymentRegisterTO, ex).getLstValidations());
				
				interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockEarlyPaymentRegisterTO, BooleanType.NO.getCode(), loggerUser);
				
			} 
			
		}
			
		//#3 separate transaction: update file independently
		Integer processState= ExternalInterfaceReceptionStateType.CORRECT.getCode();
		interfaceComponentServiceBean.get().saveExternalInterfaceResponseFileTx(
				processFileTO.getIdProcessFilePk(), validationTO.getLstValidations(),
				processFileTO.getStreamResponseFileDir(), 
				ComponentConstant.INTERFACE_TAG_EARLY_PAYMENT, 
				ComponentConstant.INTERFACE_RECORD_ISSUANCE_BCB, 
				processState, loggerUser);
	}
		
	/**
	 * Gets the holder accounts balances.
	 *
	 * @param idSecurityCode the id security code
	 * @param participantId the participant id
	 * @param idHolderAccount the id holder account
	 * @param totalRemoval the total removal
	 * @param validateTotal the validate total
	 * @param isSecuritiesRenew the is securities renew
	 * @return the holder accounts balances
	 * @throws ServiceException the service exception
	 */
	
	public List<TransferAccountBalanceTO> getHolderAccountsBalances(String idSecurityCode, Long participantId, Long idHolderAccount, 
			boolean totalRemoval, boolean validateTotal, boolean isSecuritiesRenew) throws ServiceException{
		List<TransferAccountBalanceTO> lstHolderAccBalTO=
				(List<TransferAccountBalanceTO>) removeSecurityServiceBean.getHolderAccountsBalances(idSecurityCode, participantId, idHolderAccount, validateTotal);
		if(totalRemoval) {
			
			for(TransferAccountBalanceTO transferAccountBalanceTO : lstHolderAccBalTO){
				BlockOperationTO blockOperationTO = new BlockOperationTO();
				blockOperationTO.setIdHolderAccountPk(transferAccountBalanceTO.getIdHolderAccountPk());
				blockOperationTO.setIdParticipantPk(transferAccountBalanceTO.getIdParticipantPk());
				blockOperationTO.setSecurityCodePk(transferAccountBalanceTO.getIdSecurityCodePk());
				blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
				if(!isSecuritiesRenew){
					blockOperationTO.setIndEarlyRedemption(ComponentConstant.ONE);
				}
				List<BlockOperation> blockOperations = affectationsServiceBean.findBlockOperationsByFilters(blockOperationTO);
				
				for (BlockOperation blockOperation : blockOperations) {
					if(blockOperation.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
						if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())){
							transferAccountBalanceTO.setBanBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getBanBalance()));
						}else if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())){
							transferAccountBalanceTO.setPawnBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getPawnBalance()));
						}else if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.OTHERS.getCode())){
							transferAccountBalanceTO.setOtherBlockBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getOtherBlockBalance()));
						}
					}
					
					blockOperation.setSelected(true);
					blockOperation.setBlockMarketFactOperations(affectationsServiceBean.lstBlockMarketFactOperation(null, blockOperation.getIdBlockOperationPk()));
				}
				
				transferAccountBalanceTO.setBlockOperations(blockOperations);
			}
		}
		return lstHolderAccBalTO;
	}
	
	public List<TransferAccountBalanceTO> getHolderAccountsBalancesCorporative(String issuerPk, String idSecurityCode, Integer currency, Date corporativePaymentDate, 
			boolean totalRemoval, boolean validateTotal, boolean isSecuritiesRenew) throws ServiceException{
		List<TransferAccountBalanceTO> lstHolderAccBalTO=
				removeSecurityServiceBean.getHolderAccountsBalancesCorporative(issuerPk, idSecurityCode, currency, corporativePaymentDate);
		return lstHolderAccBalTO;
	}
	
	public List<PhysicalCertificate> getPhysicalCertificateCorporative(String idIssuerPk, String idSecurityCodePk, Integer currency, Date corporativeDeliveryDate) throws CloneNotSupportedException{
		return removeSecurityServiceBean.getPhysicalCertificateCorporative(idIssuerPk, idSecurityCodePk, currency, corporativeDeliveryDate);
	}
	
	public List<RetirementDetail> generateRetirementDetailCorporative(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, Date corporativeDeliveryDate){
		List<RetirementDetail> lstReturn =null;
		List<RetirementDetail> lstReturnInterest = removeSecurityServiceBean.generateRetirementDetailCorporativeInterest(idIssuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk, corporativeDeliveryDate);
		List<RetirementDetail> lstReturnAmortization = removeSecurityServiceBean.generateRetirementDetailCorporativeAmoritzation(idIssuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk, corporativeDeliveryDate);
		
		if( (lstReturnInterest!=null && lstReturnInterest.size()>0) || 
			(lstReturnAmortization!=null && lstReturnAmortization.size()>0)	
		   ) {
			lstReturn = new ArrayList<RetirementDetail>();
			if(lstReturnInterest!=null && lstReturnInterest.size()>0) {
				lstReturn.addAll(lstReturnInterest);
			}
			if(lstReturnAmortization!=null && lstReturnAmortization.size()>0) {
				lstReturn.addAll(lstReturnAmortization);
			}
		}
		return 	lstReturn;
	}
	
	public List<RetirementDetail> generateRetirementExclusiveGuard(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, Date expirationPaymentDate){
		List<RetirementDetail> lstReturn =null;
		List<RetirementDetail> lstReturnInterest = removeSecurityServiceBean.generateRetirementGuardExclusiveInterest(idIssuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk, expirationPaymentDate);
		List<RetirementDetail> lstReturnAmortization = removeSecurityServiceBean.generateRetirementGuardExclusiveAmoritzation(idIssuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk, expirationPaymentDate);
		
		if( (lstReturnInterest!=null && lstReturnInterest.size()>0) || 
			(lstReturnAmortization!=null && lstReturnAmortization.size()>0)	
		   ) {
			lstReturn = new ArrayList<RetirementDetail>();
			if(lstReturnInterest!=null && lstReturnInterest.size()>0) {
				lstReturn.addAll(lstReturnInterest);
			}
			if(lstReturnAmortization!=null && lstReturnAmortization.size()>0) {
				lstReturn.addAll(lstReturnAmortization);
			}
		}
		return 	lstReturn;
	}
	
	public CorporativeOperation findCorporativeOperationById(Long idCorporativeOperationPk) {
		return removeSecurityServiceBean.findCorporativeOperationById(idCorporativeOperationPk);
	}
	
	public PhysicalCertificate  findPhysicalCertificateByIdProgramInterestPk(Long idProgramInterestPk) {
		return removeSecurityServiceBean.findPhysicalCertificateByIdProgramInterestPk(idProgramInterestPk);
	}
	
	public PhysicalCertificate  findPhysicalCertificateAmortization(String idSecurityCodePk) {
		return removeSecurityServiceBean.findPhysicalCertificateAmortization(idSecurityCodePk);
	}
	
	public RetirementOperation findRetirementOperationWithPhysicalCertificate(Long idPhysicalCertificatePk) {
		return removeSecurityServiceBean.findRetirementOperationWithPhysicalCertificate(idPhysicalCertificatePk);
	}

	public void confirmRetirementCorporative(RetirementOperation objRetirementOperation, LoggerUser loggerUser) throws ServiceException {
		removeSecurityServiceBean.confirmRetirementCorporative(objRetirementOperation, loggerUser);
	}
	
	public List<RetirementDetail> generateRetirementDetailCorporative(Long idCorporativeOperationPk){
		List<RetirementDetail> lstReturn =null;
		List<RetirementDetail> lstReturnInterest = removeSecurityServiceBean.generateRetirementDetailCorporativeInterest(idCorporativeOperationPk);
		List<RetirementDetail> lstReturnAmortization = removeSecurityServiceBean.generateRetirementDetailCorporativeAmoritzation(idCorporativeOperationPk);
		
		if( (lstReturnInterest!=null && lstReturnInterest.size()>0) || 
			(lstReturnAmortization!=null && lstReturnAmortization.size()>0)	
		   ) {
			lstReturn = new ArrayList<RetirementDetail>();
			if(lstReturnInterest!=null && lstReturnInterest.size()>0) {
				lstReturn.addAll(lstReturnInterest);
			}
			if(lstReturnAmortization!=null && lstReturnAmortization.size()>0) {
				lstReturn.addAll(lstReturnAmortization);
			}
		}
		return 	lstReturn;
	}
	
	public List<RetirementDetail> generateFixedIncomeRetirementDetailAvailable(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk){
		return removeSecurityServiceBean.generateFixedIncomeRetirementDetailAvailable(idIssuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk);	
	}
	
	public List<RetirementDetail> generateVariableIncomeRetirementDetailAvailable(String idIssuerPk, Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk){
		return removeSecurityServiceBean.generateVariableIncomeRetirementDetailAvailable(idIssuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk);	
	}
	
	
	public List<TransferAccountBalanceTO> getHolderAccountsBalancesWhitRetirement(String issuerPk,Long idParticipantPk, Long idHolderAccountPk, String idSecurityCode, Integer currency, Long idHolderPk, boolean totalRemoval, boolean isSecuritiesRenew) throws ServiceException{
		List<TransferAccountBalanceTO> lstHolderAccBalTO=
				(List<TransferAccountBalanceTO>) removeSecurityServiceBean.getHolderAccountsBalancesWithRetirement(issuerPk, idParticipantPk, idHolderAccountPk, idSecurityCode, currency, idHolderPk);
		/*if(totalRemoval) {
			
			for(TransferAccountBalanceTO transferAccountBalanceTO : lstHolderAccBalTO){
				BlockOperationTO blockOperationTO = new BlockOperationTO();
				blockOperationTO.setIdHolderAccountPk(transferAccountBalanceTO.getIdHolderAccountPk());
				blockOperationTO.setIdParticipantPk(transferAccountBalanceTO.getIdParticipantPk());
				blockOperationTO.setSecurityCodePk(transferAccountBalanceTO.getIdSecurityCodePk());
				blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
				if(!isSecuritiesRenew){
					blockOperationTO.setIndEarlyRedemption(ComponentConstant.ONE);
				}
				List<BlockOperation> blockOperations = affectationsServiceBean.findBlockOperationsByFilters(blockOperationTO);
				
				for (BlockOperation blockOperation : blockOperations) {
					if(blockOperation.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
						if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())){
							transferAccountBalanceTO.setBanBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getBanBalance()));
						}else if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())){
							transferAccountBalanceTO.setPawnBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getPawnBalance()));
						}else if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.OTHERS.getCode())){
							transferAccountBalanceTO.setOtherBlockBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getOtherBlockBalance()));
						}
					}
					
					blockOperation.setSelected(true);
					//blockOperation.setBlockMarketFactOperations(affectationsServiceBean.lstBlockMarketFactOperation(null, blockOperation.getIdBlockOperationPk()));
				}
				
				transferAccountBalanceTO.setBlockOperations(blockOperations);
			}
		}*/
		return lstHolderAccBalTO;
	}
	
	public List<TransferAccountBalanceTO> getHolderAccountsBalances(String issuerPk, String idSecurityCode, Integer currency, Long idHolderPk, boolean totalRemoval, boolean isSecuritiesRenew) throws ServiceException{
		List<TransferAccountBalanceTO> lstHolderAccBalTO=
				(List<TransferAccountBalanceTO>) removeSecurityServiceBean.getHolderAccountsBalances(issuerPk, idSecurityCode, currency, idHolderPk);
		if(totalRemoval) {
			
			for(TransferAccountBalanceTO transferAccountBalanceTO : lstHolderAccBalTO){
				BlockOperationTO blockOperationTO = new BlockOperationTO();
				blockOperationTO.setIdHolderAccountPk(transferAccountBalanceTO.getIdHolderAccountPk());
				blockOperationTO.setIdParticipantPk(transferAccountBalanceTO.getIdParticipantPk());
				blockOperationTO.setSecurityCodePk(transferAccountBalanceTO.getIdSecurityCodePk());
				blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
				if(!isSecuritiesRenew){
					blockOperationTO.setIndEarlyRedemption(ComponentConstant.ONE);
				}
				List<BlockOperation> blockOperations = affectationsServiceBean.findBlockOperationsByFilters(blockOperationTO);
				
				for (BlockOperation blockOperation : blockOperations) {
					if(blockOperation.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
						if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())){
							transferAccountBalanceTO.setBanBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getBanBalance()));
						}else if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())){
							transferAccountBalanceTO.setPawnBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getPawnBalance()));
						}else if(blockOperation.getBlockRequest().getBlockType().equals(AffectationType.OTHERS.getCode())){
							transferAccountBalanceTO.setOtherBlockBalance(blockOperation.getActualBlockBalance().add(transferAccountBalanceTO.getOtherBlockBalance()));
						}
					}
					
					blockOperation.setSelected(true);
					//blockOperation.setBlockMarketFactOperations(affectationsServiceBean.lstBlockMarketFactOperation(null, blockOperation.getIdBlockOperationPk()));
				}
				
				transferAccountBalanceTO.setBlockOperations(blockOperations);
			}
		}
		return lstHolderAccBalTO;
	}
	
	/**
	 * Search holders.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountDetail> searchHolders(Long idHolderAccountPk) throws ServiceException{
		return this.removeSecurityServiceBean.searchHolders(idHolderAccountPk);
	}
	
	/**
	 * Search retirement operations.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_QUERY)
	public List<RetirementOperationTO> searchRetirementOperations(RetirementOperationTO retirementOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());		
		return this.removeSecurityServiceBean.searchRetirementOperations(retirementOperationTO);
	}
		
	/**
	 * Find retirment operation.
	 *
	 * @param idRetirementOperationPk the id retirement operation pk
	 * @return the retirement operation
	 * @throws ServiceException the service exception
	 */
	public RetirementOperation findRetirmentOperation(Long idRetirementOperationPk) throws ServiceException{
		return removeSecurityServiceBean.findRetirmentOperation(idRetirementOperationPk);
	}
	
	/**
	 * metodo para obtener la descripcion del holder(cui-fullname) desde el id: HolderAccount(cuenta titular)
	 * @param idHolderAccountPk
	 * @return null si no se encontraron resultados
	 */
	public String getDescriptionHolderFromIdHolderAccount(Long idHolderAccountPk){
		return removeSecurityServiceBean.getDescriptionHolderFromIdHolderAccount(idHolderAccountPk);
	}
	
	/**
	 * Find retirement market fact by retirement.
	 *
	 * @param idRetirementDetail the id retirement detail
	 * @return the list
	 */
	public List<RetirementMarketfact> findRetirementMarketFactByRetirement(long idRetirementDetail){
		return removeSecurityServiceBean.findRetirementMarketFactByRetirement(idRetirementDetail);
	}
	
	/**
	 * Approve retirement operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_APPROVE)
	public void approveRetirementOperation(RetirementOperation retirementOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		
		removeSecurityServiceBean.changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.APPROVED.getCode());
	}
	
	
	/**
	 * Anullate retirement operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_CANCEL)
	public void anullateRetirementOperation(RetirementOperation retirementOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		
		removeSecurityServiceBean.changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.ANNULLED.getCode());
	}
	
	
	/**
	 * Confirm retirement operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_CONFIRM)
	public void confirmRetirementOperation(RetirementOperation retirementOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());

		removeSecurityServiceBean.changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.CONFIRMED.getCode());
		removeSecurityServiceBean.physicalBalanceZeroWithRetirement(retirementOperation);
	}
	
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_CONFIRM)
	public void physicalBalanceZeroWithRetirement(RetirementOperation retirementOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());

		removeSecurityServiceBean.physicalBalanceZeroWithRetirement(retirementOperation);
	}
	
	public void confirmRetirementOperation(RetirementOperation retirementOperation, LoggerUser loggerUser) throws ServiceException{
		removeSecurityServiceBean.confirmRetirementCorporative(retirementOperation, loggerUser);
	}
	/**
	 * Reject retirement operation.
	 *
	 * @param retirementOperation the retirement operation
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_RETIREMENT_REJECT)
	public void rejectRetirementOperation(RetirementOperation retirementOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		removeSecurityServiceBean.changeStateRetirementOperation(retirementOperation, loggerUser, RetirementOperationStateType.REJECTED.getCode());
	}
	
	/**
	 * Gets the exist balance for account.
	 *
	 * @param filter the filter
	 * @return the exist balance for account
	 * @throws ServiceException the service exception
	 */
	public Boolean getExistBalanceForAccount(HolderAccountBalance filter) throws ServiceException{
		return holderAccountService.getExistBalanceForAccount(filter)>0;
	}
	
	/**
	 * Automatic retirement operation cancel.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticRetirementOperationCancel(Date finalDate) throws ServiceException{								
		RetirementOperationTO retirementOperationTO = new RetirementOperationTO();
		retirementOperationTO.setEndDate(finalDate);		
		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(RetirementOperationStateType.REGISTERED.getCode());
		lstStates.add(RetirementOperationStateType.APPROVED.getCode());						
		List<RetirementOperation> lstRetirementOperation = removeSecurityServiceBean.searchRetirementOperationByRevert(retirementOperationTO, lstStates);				
		for (RetirementOperation objRetirementOperation : lstRetirementOperation){			
			objRetirementOperation.setMotive(RetirementOperationMotiveType.REVERSION.getCode());									
			if (objRetirementOperation.getState().equals(RetirementOperationStateType.REGISTERED.getCode())) {				
				anullateRetirementOperation(objRetirementOperation);
			}else if (objRetirementOperation.getState().equals(RetirementOperationStateType.APPROVED.getCode())) {				
				rejectRetirementOperation(objRetirementOperation);
			}					
		}		
	}
	
	/**
	 * Find market fact balance.
	 *
	 * @param marketFactBalanceTO the market fact balance to
	 * @return the holder market fact balance
	 * @throws ServiceException the service exception
	 */
	public HolderMarketFactBalance findMarketFactBalance(MarketFactBalanceTO marketFactBalanceTO) throws ServiceException{
		return marketServiceBean.findMarketFactBalance(marketFactBalanceTO);
	}
	
	/**
	 * Find participant by filters service.
	 *
	 * @param filter the filter
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipantByFiltersService(Participant filter) throws ServiceException{
		return participantServiceBean.findParticipantByFiltersServiceBean(filter);
	}

	/**
	 * Gets the holder market facts.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param idParticipantPk the id participant pk
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder market facts
	 */
	public List<MarketFactTO> getHolderMarketFacts(String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk) {
		return removeSecurityServiceBean.getHolderMarketFacts(idSecurityCodePk, idParticipantPk, idHolderAccountPk);
	}

	/**
	 * Gets the prepaid security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the prepaid security
	 */
	public Security getPrepaidSecurity(String idSecurityCodePk) {
		return removeSecurityServiceBean.getPrepaidSecurity(idSecurityCodePk);
	}
	
	/**
	 * Gets the issuer security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuer
	 */
	/*public Security getSecurityWithIssuer(String idSecurityCodePk) {
		return removeSecurityServiceBean.getSecurityWithIssuer(idSecurityCodePk);
	}
	*/
	/**
	 * Find block operations by filters.
	 *
	 * @param blockOperationTO the block operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperation> findBlockOperationsByFilters(BlockOperationTO blockOperationTO) throws ServiceException {
		List<BlockOperation> list = affectationsServiceBean.findBlockOperationsByFilters(blockOperationTO);
		return list;
	}

	/**
	 * Gets the block market fact operations.
	 *
	 * @param idBlockOperationPk the id block operation pk
	 * @return the block market fact operations
	 * @throws ServiceException the service exception
	 */
	public List<BlockMarketFactOperation> getBlockMarketFactOperations(Long idBlockOperationPk) throws ServiceException {
		return affectationsServiceBean.lstBlockMarketFactOperation(null, idBlockOperationPk);
	}

	/**
	 * Gets the retirement block details.
	 *
	 * @param idRetirementDetailPk the id retirement detail pk
	 * @return the retirement block details
	 */
	public List<RetirementBlockDetail> getRetirementBlockDetails(Long idRetirementDetailPk) {
		return removeSecurityServiceBean.getRetirementBlockDetails(idRetirementDetailPk);
	}

	/**
	 * Gets the issuer pk.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuer pk
	 */
	public String getIssuerPk(String idSecurityCodePk) {
		return removeSecurityServiceBean.getIssuerPk(idSecurityCodePk);
	}

	/**
	 * Gets the list holder account.
	 *
	 * @param idParticipant the id participant
	 * @param idHolder the id holder
	 * @return the list holder account
	 */
	public List<HolderAccount> getListHolderAccount(Long idParticipant, Long idHolder) {
		return removeSecurityServiceBean.getListHolderAccount(idParticipant, idHolder);
	}
	
	/**
	 * Gets the list holder account balance.
	 *
	 * @param objRegisterWithdrawalSiterxTO the obj register withdrawal siterx to
	 * @param lstIssuer the lst issuer
	 * @param mpParameter the mp parameter
	 * @return the list holder account balance
	 */
	public List<TransferAccountBalanceTO> getListHolderAccountBalance(RegisterWithdrawalSiterxTO objRegisterWithdrawalSiterxTO, 
																	List<Issuer> lstIssuer, Map<Integer, Object> mpParameter)
	{
		List<TransferAccountBalanceTO> lstHolderAccountBalanceTO= new ArrayList<TransferAccountBalanceTO>();
		List<HolderAccountBalance> lstHolderAccountBalance= removeSecurityServiceBean.getListHolderAccountBalance(
																					objRegisterWithdrawalSiterxTO.getHolderAccount().getIdHolderAccountPk(), 
																					lstIssuer, objRegisterWithdrawalSiterxTO.getSecurityClass());
		if (Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalance)) {
			for (HolderAccountBalance objHolderAccountBalance: lstHolderAccountBalance) {
				TransferAccountBalanceTO objHolderAccountBalanceTO= new TransferAccountBalanceTO();
				objHolderAccountBalanceTO.setIdHolderAccountPk(objRegisterWithdrawalSiterxTO.getHolderAccount().getIdHolderAccountPk());
				objHolderAccountBalanceTO.setIdParticipantPk(objRegisterWithdrawalSiterxTO.getParticipant().getIdParticipantPk());
				objHolderAccountBalanceTO.setIdSecurityCodePk(objHolderAccountBalance.getSecurity().getIdSecurityCodePk());
				objHolderAccountBalanceTO.setIdSecurityBCBcode(objHolderAccountBalance.getSecurity().getIdSecurityBcbCode());
				objHolderAccountBalanceTO.setSecurityClass(objHolderAccountBalance.getSecurity().getSecurityClass());
				objHolderAccountBalanceTO.setExpirationDate(objHolderAccountBalance.getSecurity().getExpirationDate());
				objHolderAccountBalanceTO.setHolderAccountBalance(objHolderAccountBalance);
				objHolderAccountBalanceTO.setMarketLock(objHolderAccountBalance.getHolderMarketfactBalance().size() != ComponentConstant.ONE);
				
				if(!objHolderAccountBalanceTO.getMarketLock()){
					objHolderAccountBalanceTO.setMarketFactDetails(new ArrayList<MarketFactTO>()); 
					for(HolderMarketFactBalance marketFactbalance: objHolderAccountBalance.getHolderMarketfactBalance()){
						MarketFactTO marketFactTO = new MarketFactTO();
						marketFactTO.setMarketDate(marketFactbalance.getMarketDate());
						marketFactTO.setMarketRate(marketFactbalance.getMarketRate());
						marketFactTO.setMarketPrice(marketFactbalance.getMarketPrice());
						marketFactTO.setIdMarketFactPk(marketFactbalance.getIdMarketfactBalancePk());
						objHolderAccountBalanceTO.getMarketFactDetails().add(marketFactTO);
					}
				}
				
				lstHolderAccountBalanceTO.add(objHolderAccountBalanceTO);
			}
		}
		return lstHolderAccountBalanceTO;
	}
	
	/**
	 * Save withdrawal sirtex operation.
	 *
	 * @param objRegisterWithdrawalSiterx the obj register withdrawal siterx
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_EARLY_PAYMENT_REGISTER)
	public String saveWithdrawalSirtexOperation(RegisterWithdrawalSiterxTO objRegisterWithdrawalSiterx) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		return removeSecurityServiceBean.saveWithdrawalSirtexOperation(objRegisterWithdrawalSiterx, loggerUser); 
	}
	
	/**
	 * Gets the list sirtex withdrawal operation.
	 *
	 * @param objFilterWithdrawalSirtex the obj filter withdrawal sirtex
	 * @return the list sirtex withdrawal operation
	 * @throws ServiceException the service exception
	 */
	public List<RetirementOperationTO> getListSirtexWithdrawalOperation(RetirementOperationTO objFilterWithdrawalSirtex) throws ServiceException {
		return removeSecurityServiceBean.searchRetirementOperations(objFilterWithdrawalSirtex);
	}
	
	/**
	 * Confirm sirtex withdrawal operation.
	 *
	 * @param lstWithdrawalSirtexOperation the lst withdrawal sirtex operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_EARLY_PAYMENT_CONFIRM)
	public String confirmSirtexWithdrawalOperation(List<RetirementOperationTO> lstWithdrawalSirtexOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
        
        return removeSecurityServiceBean.executeSirtexWithdrawalOperation(lstWithdrawalSirtexOperation, 
        													RetirementOperationStateType.CONFIRMED.getCode(), loggerUser);
	}
	
	/**
	 * Reject sirtex withdrawal operation.
	 *
	 * @param lstWithdrawalSirtexOperation the lst withdrawal sirtex operation
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.SIRTEX_EARLY_PAYMENT_REJECT)
	public String rejectSirtexWithdrawalOperation(List<RetirementOperationTO> lstWithdrawalSirtexOperation) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
        
        return removeSecurityServiceBean.executeSirtexWithdrawalOperation(lstWithdrawalSirtexOperation, 
        													RetirementOperationStateType.REJECTED.getCode(), loggerUser);
	}
	
	/**
	 * Find issuer by pk.
	 *
	 * @param idIssuer the id issuer
	 * @return the issuer
	 */
	public Issuer findIssuerByPk(String idIssuer) {
		return removeSecurityServiceBean.find(Issuer.class, idIssuer);
	}
	
	/**
	 * Gets the list issuer by mnemonic.
	 *
	 * @param mnemonics the mnemonics
	 * @return the list issuer by mnemonic
	 */
	public List<Issuer> getListIssuerByMnemonic(List<String> mnemonics) {
		return removeSecurityServiceBean.getListIssuerByMnemonic(mnemonics);
	}
	
	public List<Issuer> getListIssuerEnabled() {
		return removeSecurityServiceBean.getListIssuerEnabled();
	}
	
	/**
	 * Gets the list hol acc balance.
	 *
	 * @param idParticipant the id participant
	 * @param idSecurityCode the id security code
	 * @param isReversal the is reversal
	 * @return the list hol acc balance
	 */
	public List<HolderAccountBalance> getListHolAccBalance(Long idParticipant, String idSecurityCode, boolean isReversal){
		return removeSecurityServiceBean.getListHolAccBalance(idParticipant, idSecurityCode, isReversal);
	}
	
	public AccountAnnotationOperation getAccountAnnotationOperation(Long idPhysicalCertificatePk) throws ServiceException{
		//accountAnnotationOperation
		return removeSecurityServiceBean.getAccountAnnotationOperation(idPhysicalCertificatePk);
	}
	
	public AccountAnnotationOperation getAccountAnnotationOperation(String idSecurityCodePk) throws ServiceException{
		//accountAnnotationOperation
		return removeSecurityServiceBean.getAccountAnnotationOperation(idSecurityCodePk);
	}
	
	public AccountAnnotationOperation getAccountAnnotationOperation(String idSecurityCodePk, Long idPhysicalCertificatePk) throws ServiceException{
		//accountAnnotationOperation
		return removeSecurityServiceBean.getAccountAnnotationOperation(idSecurityCodePk, idPhysicalCertificatePk);
	}
	
	public PayrollDetail getPayrollDetailOnliCapitalValid(Long idRetirementOperationPk) throws ServiceException{
		//accountAnnotationOperation
		return removeSecurityServiceBean.getPayrollDetailOnliCapitalValid(idRetirementOperationPk);
	}
	
	public PayrollDetail getPayrollDetailValid(Long idRetirementOperationPk) throws ServiceException{
		//accountAnnotationOperation
		return removeSecurityServiceBean.getPayrollDetailValid(idRetirementOperationPk);
	}
	
}
