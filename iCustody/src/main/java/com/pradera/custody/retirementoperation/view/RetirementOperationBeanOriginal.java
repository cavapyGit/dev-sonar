package com.pradera.custody.retirementoperation.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.view.MarketFactBean;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.custody.affectation.to.BlockOperationTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.custody.retirementoperation.facade.RetirementOperationServiceFacade;
import com.pradera.custody.retirementoperation.to.RegisterRetirementOperationTO;
import com.pradera.custody.retirementoperation.to.RetirementOperationTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementMarketfact;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.RetirementOperationRedemptionType;
import com.pradera.model.custody.type.RetirementOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * The Class RetirementOperationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class RetirementOperationBeanOriginal extends GenericBaseBean{

	/** The Constant serialVersionUID. */
	/***/
	private static final long serialVersionUID = 1L;
	
	/** The Constant ERROR_ANULATE. */
	private static final Integer ERROR_ANULATE = 2328;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The retirement operation session. */
	private RegisterRetirementOperationTO registerRetirementOperation;
	
	/** The retirement operation filter. */
	private RetirementOperationTO retirementOperationFilter;
	
	/** The lst motives. */
	private List<ParameterTable> lstMotives = new ArrayList<ParameterTable>();
	
	/** The lst states. */
	private List<ParameterTable> lstStates = new ArrayList<ParameterTable>();
	
	/** The lst retirement type. */
	private List<ParameterTable> lstRetirementType = new ArrayList<ParameterTable>();
	
	/** The lst motives reject. */
	private List<ParameterTable> lstMotivesReject = new ArrayList<ParameterTable>();
	
	/** The lst cbo securitie class. */
	private List<ParameterTable> lstCboSecuritieClass = new ArrayList<ParameterTable>();
	
	/** The lst holder account balance. */
	private GenericDataModel<TransferAccountBalanceTO> lstHolderAccountBalance;
	
	/** The bol all check box. */
	private Boolean bolAllCheckBox;
	
	/** The lst retirement operation. */
	private GenericDataModel<RetirementOperationTO> lstRetirementOperation;
	
	/** The selected retirement operation. */
	private RetirementOperationTO selectedRetirementOperation;
	
	/** The remove security service facade. */
	@EJB
	RetirementOperationServiceFacade removeSecurityServiceFacade;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/** The helper component facade. */
	@Inject
	HelperComponentFacade helperComponentFacade;
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The retirement operation service facade. */
	@EJB
	RetirementOperationServiceFacade retirementOperationServiceFacade;
	
	/** The Constant REGISTER_VIEW_MAPPING. */
	private static final String REGISTER_VIEW_MAPPING = "removeSecuritiesMgmt";
	
	/** The massive remove securities type. */
	private String massiveRemoveSecuritiesType = ComponentConstant.INTERFACE_EARLY_PAYMENT;
	
	/** The Constant DETAIL_VIEW_MAPPING. */
	private static final String DETAIL_VIEW_MAPPING = "removeSecuritiesDetail";
	
	/** The multiple removal. */
	private boolean multipleRemoval;
	
	/** The total removal. */
	private boolean totalRemoval;
	
	/** The single removal. */
	private boolean singleRemoval;
	
	/** The holder parcial. */
	private boolean holderParcial;
	
	/** The show motive text. */
	private boolean showMotiveText;
	
	/** Validate security. */
	private Security securityTargetReg = null;
	
	/** The bol all check box. */
	private Boolean checkBoxReg;
	
	/** The bol all check box. */
	private Boolean existSecurityTarget;
	
	
	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The retirement market fact balance. */
	private MarketFactBalanceHelpTO retirementMarketFactBalance;
	
	/** The help market fact balance view. */
	private MarketFactBalanceHelpTO helpMarketFactBalanceView = new MarketFactBalanceHelpTO();
	
	/** The lst retirement market fact to. */
	private List<MarketFactTO> lstRetirementMarketFactTO=new ArrayList<MarketFactTO>();

	/** The id issuer bc. */
	@Inject @Configurable String idIssuerBC;
	
	/** The id issuer tgn. */
	@Inject @Configurable String idIssuerTGN;
	
	/** The market fact bean. */
	@Inject
	private MarketFactBean marketFactBean;
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){				
		try{
			super.setParametersTableMap(new HashMap<Integer,Object>());
			registerRetirementOperation = new RegisterRetirementOperationTO();
			retirementOperationFilter = new RetirementOperationTO();
			
			lstParticipant = removeSecurityServiceFacade.findAllParticipant();	
			
			loadParameters();
			showPrivilegeButtons();
			
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParameters () throws ServiceException{
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setLstMasterTableFk(new ArrayList<Integer>());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_MOTIVE_STATE.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_REDEMPTION_TYPE.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.ACCOUNTS_TYPE.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.INSTRUMENT_TYPE.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.AFFECTATION_TYPE.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.LEVEL_AFFECTATION.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.SECURITIES_CLASS.getCode());
		parameterFilter.getLstMasterTableFk().add(MasterTableType.RETIREMENT_REJECT_MOTIVE.getCode());
		
		List<ParameterTable> lstState = generalPametersFacade.getListParameterTableServiceBean(parameterFilter);
		for (ParameterTable parameterTable : lstState) {
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.RETIREMENT_MOTIVE_STATE.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState())){
					lstStates.add(parameterTable);
				}
			}
			
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.RETIREMENT_REDEMPTION_TYPE.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState())){
					lstRetirementType.add(parameterTable);
				}
			}
			
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState()) &&
						ComponentConstant.ONE.equals(parameterTable.getIndicator4())){
					lstMotives.add(parameterTable);
				}
			}
			
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.SECURITIES_CLASS.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState())){
					lstCboSecuritieClass.add(parameterTable);
				}
			}
			
			if(parameterTable.getMasterTable().getMasterTablePk().equals(MasterTableType.RETIREMENT_REJECT_MOTIVE.getCode())){
				if(ParameterTableStateType.REGISTERED.getCode().equals(parameterTable.getParameterState())){
					lstMotivesReject.add(parameterTable);
				}
			}
			
			getParametersTableMap().put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
	}
	
	/**
	 * Sets the all false.
	 */
	private void setAllFalse(){
		singleRemoval = false;
		multipleRemoval = false;
		totalRemoval = false;
		holderParcial = false;
	}
	
	/**
	 * After account helper.
	 */
	public void onChangeHolder(){
		try {
			registerRetirementOperation.setLstHolderAccounts(null);
			registerRetirementOperation.setSelectedHolderAccount(null);
			lstHolderAccountBalance=null;
			
			if(registerRetirementOperation.getHolder().getIdHolderPk() != null){
				
				if(!HolderStateType.REGISTERED.getCode().equals(registerRetirementOperation.getHolder().getStateHolder())) {
					registerRetirementOperation.setHolder(new Holder());
					registerRetirementOperation.setSelectedHolderAccount(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage("tdv.msg.holder.not.registered"));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}				
				
				HolderAccountTO holderAccountTO = new HolderAccountTO();
				holderAccountTO.setParticipantTO(registerRetirementOperation.getSelectedParticipant().getIdParticipantPk());
				holderAccountTO.setIdHolderPk(registerRetirementOperation.getHolder().getIdHolderPk());
				holderAccountTO.setNeedHolder(Boolean.TRUE);
				holderAccountTO.setNeedParticipant(Boolean.TRUE);
				holderAccountTO.setNeedBanks(Boolean.FALSE);
				List<HolderAccount> accountList = retirementOperationServiceFacade.getHolderAccounts(holderAccountTO);
				
				registerRetirementOperation.setLstHolderAccounts(accountList);
				
				if(Validations.validateListIsNotNullAndNotEmpty(accountList) && accountList.size() == ComponentConstant.ONE){
					registerRetirementOperation.setSelectedHolderAccount(accountList.get(0));
				}
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Validate holder account.
	 */
	public void validateHolderAccount(){
		try {
			lstHolderAccountBalance=null;
			HolderAccountBalance filter=new HolderAccountBalance();
			filter.setId( new HolderAccountBalancePK() );
			filter.getId().setIdHolderAccountPk( registerRetirementOperation.getSelectedHolderAccount().getIdHolderAccountPk());			
			if((!registerRetirementOperation.getSelectedHolderAccount().getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))){
				registerRetirementOperation.setHolder(new Holder());
				registerRetirementOperation.setSelectedHolderAccount(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}		
			
			for(HolderAccountDetail objHolderAccountDetail : registerRetirementOperation.getSelectedHolderAccount().getHolderAccountDetails()){
				if(!HolderStateType.REGISTERED.getCode().equals(objHolderAccountDetail.getHolder().getStateHolder())) {
					registerRetirementOperation.setHolder(new Holder());
					registerRetirementOperation.setSelectedHolderAccount(null);
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							PropertiesUtilities.getMessage("tdv.msg.holder.not.registered"));
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
			}
			
			if((!registerRetirementOperation.getSelectedHolderAccount().getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))){
				registerRetirementOperation.setSelectedHolderAccount(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			Boolean validate=removeSecurityServiceFacade.getExistBalanceForAccount(filter);
			if(!validate){
				registerRetirementOperation.setSelectedHolderAccount(null);
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(PropertiesConstants.MSGE_ACCREDITATION_CERTIFICATE_WRONG_BALANCE));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	
	/**
	 * Onchange motive.
	 */
	public void onchangeMotive(){
		JSFUtilities.resetViewRoot();
		JSFUtilities.hideGeneralDialogues();
		setAllFalse();
		lstHolderAccountBalance = null;
		securityTargetReg = null;
		setCheckBoxReg(false);
		existSecurityTarget = false;
		RetirementOperation retirementOperation = registerRetirementOperation.getRetirementOperation();
		retirementOperation.setSecurity(new Security());
		retirementOperation.setTargetSecurity(null);
		registerRetirementOperation = new RegisterRetirementOperationTO();
		registerRetirementOperation.setRetirementOperation(retirementOperation);
		registerRetirementOperation.getRetirementOperation().setRetirementType(null);
		if(RetirementOperationMotiveType.REVERSION.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive())){
			singleRemoval = true;
		}
		holderParcial = false;
	}
	
	/**
	 * On change retirement type.
	 */
	public void onChangeRetirementType(){
		JSFUtilities.resetViewRoot();
		JSFUtilities.hideGeneralDialogues();
		setAllFalse();
		lstHolderAccountBalance = null;
		securityTargetReg = null;
		setCheckBoxReg(false);
		existSecurityTarget = false;
		RetirementOperation retirementOperation = registerRetirementOperation.getRetirementOperation();
		retirementOperation.setSecurity(new Security());
		retirementOperation.setTargetSecurity(null);
		registerRetirementOperation = new RegisterRetirementOperationTO();
		registerRetirementOperation.setRetirementOperation(retirementOperation);
		if(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive())){
			if(RetirementOperationRedemptionType.PARTIAL.getCode().equals(registerRetirementOperation.getRetirementOperation().getRetirementType())){
				multipleRemoval = true;		
				holderParcial = true;
			}else if(RetirementOperationRedemptionType.TOTAL.getCode().equals(registerRetirementOperation.getRetirementOperation().getRetirementType())){
				totalRemoval = true;
			}
		}
	}

	/*
	 * Metodo que ejecuta el cuadro checkBox en clave de valor Destino
	 */
	public void actionChkBox(){
		
		 if(this.getCheckBoxReg().equals(true)){
			 registerRetirementOperation.getRetirementOperation().setTargetSecurity(securityTargetReg);
		 }else{
			 registerRetirementOperation.getRetirementOperation().setTargetSecurity(null);
		 }
		
		
	}
	
	/**
	 * Validate security.
	 */
	public void validateSecurity(){
		JSFUtilities.resetViewRoot();
		JSFUtilities.hideGeneralDialogues();
		lstHolderAccountBalance = null;
		securityTargetReg = null;
		setCheckBoxReg(false);
		existSecurityTarget = false;
		registerRetirementOperation.getRetirementOperation().setTargetSecurity(null);
		try {
			lstHolderAccountBalance = null;
			Security security = registerRetirementOperation.getRetirementOperation().getSecurity();
			
			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
				if(!SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("msg.retirement.security.invalid.state"));
					registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				/**
				 * Validation Not Retirement If Expiration Date is Now, Fixed Income
				 */
				if(security.isFixedInstrumentType()&&
						CommonsUtilities.isEqualDate(getCurrentSystemDate(), security.getExpirationDate())){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getMessage("msg.retirement.security.expiration.date.now"));
					registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
					JSFUtilities.showSimpleValidationDialog();
					return;
				}
				
				if(RetirementOperationMotiveType.REVERSION.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive())){
					if(!security.getIssuanceForm().equals(IssuanceType.MIXED.getCode())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("msg.retirement.security.invalid.issuance.form"));
						registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}

				if(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive()) || 
						RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive())){
					
					if(RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive()) &&
							!security.isVariableInstrumentType()){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getMessage("msg.retirement.security.invalid.variable.inst"));
						registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
					
					if(RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(registerRetirementOperation.getRetirementOperation().getMotive())){
						
						if(security.isDpfDpaSecurityClass()){
							List<HolderAccountBalance> lstBalance = retirementOperationServiceFacade.getListHolAccBalance(null, security.getIdSecurityCodePk(), false);
							if(Validations.validateListIsNotNullAndNotEmpty(lstBalance)){
								for(HolderAccountBalance objHolderAccountBalance : lstBalance){
									if(objHolderAccountBalance.getBanBalance().compareTo(BigDecimal.ZERO) > 0
											|| objHolderAccountBalance.getPawnBalance().compareTo(BigDecimal.ZERO) > 0
											|| objHolderAccountBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0){										
										showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
												PropertiesUtilities.getMessage("msg.retirement.security.invalid.security.blocked"));
										JSFUtilities.showSimpleValidationDialog();
										return;
									}
								}
							}
						}
						
						if(!security.isFixedInstrumentType()){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.retirement.security.invalid.fixed.inst"));
							registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
							JSFUtilities.showSimpleValidationDialog();
							return;
						}
						
//						String idIssuerPk =  retirementOperationServiceFacade.getIssuerPk(security.getIdSecurityCodePk());	
//						if(idIssuerPk.equals(idIssuerBC)){
//							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//									, PropertiesUtilities.getMessage("msg.retirement.security.invalid.issuer.class"));
//							registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
//							JSFUtilities.showSimpleValidationDialog();
//							return;
//						}
						
						//issue 949
						//if(security.isBtsSecurityClass() || security.isBbsSecurityClass() || security.isEnableYieldRate()){
						/*if(security.isBtsSecurityClass() || security.isBbsSecurityClass()){
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getMessage("msg.retirement.security.invalid.security.class"));
							registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
							JSFUtilities.showSimpleValidationDialog();
							return;							
						}
						*/
						//issue 116, 949
						if(	!security.isBbxSecurityClass() && 
							!security.isBtxSecurityClass() && 
							!security.isBtsSecurityClass() && 
							!security.isBbsSecurityClass() && 
							!(SecurityClassType.CUP.getCode().equals( security.getSecurityClass())) && 
							!(SecurityClassType.LTS.getCode().equals( security.getSecurityClass())) && 
							!(SecurityClassType.LBS.getCode().equals( security.getSecurityClass())) && 
							!(SecurityClassType.CDS.getCode().equals( security.getSecurityClass())) && 
							!(SecurityClassType.PGS.getCode().equals( security.getSecurityClass())) && 
							!security.isDpfDpaSecurityClass() 
							){
							
							if(		!(security.isBlpSecurityClass() || 
									security.isBbbSecurityClass() || 
									security.isBmsSecurityClass() || 
									security.isPgbSecurityClass() || 
									security.isBbcSecurityClass() ||
									security.isVtdSecurityClass())) {
								if(!security.isEarlyRedemption()){
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage("msg.retirement.security.invalid.security.redemption"));
									registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
									JSFUtilities.showSimpleValidationDialog();
									return;
								}
							}
							
							Security prepaidSecurity = retirementOperationServiceFacade.getPrepaidSecurity(security.getIdSecurityCodePk());		
							if(prepaidSecurity==null){
								if(!(security.isBlpSecurityClass() || 
										security.isBbbSecurityClass() || 
										security.isBmsSecurityClass() || 
										security.isPgbSecurityClass() || 
										security.isBbcSecurityClass() ||
										security.isVtdSecurityClass())) {
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_NO_PREPAID_SECURITY));
									registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
									JSFUtilities.showSimpleValidationDialog();
									return;
								}
							}else{
								if(!prepaidSecurity.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
									showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
											, PropertiesUtilities.getMessage("msg.retirement.security.invalid.state.q"));
									JSFUtilities.showSimpleValidationDialog();
								}else{
									registerRetirementOperation.getRetirementOperation().setTargetSecurity(prepaidSecurity);
									securityTargetReg = registerRetirementOperation.getRetirementOperation().getTargetSecurity();
									setCheckBoxReg(true);
									existSecurityTarget = true;
								}
							}
						}
						
						/*
						//issue 116 - CUI DEL EMISOR
						
						Long holdIssuer = null;
						Security issuerSecurity = retirementOperationServiceFacade.getSecurityWithIssuer(security.getIdSecurityCodePk());
						
						if (!issuerSecurity.equals(null)){
							holdIssuer = issuerSecurity.getIssuer().getHolder().getIdHolderPk();
						}
						*/
						
						if(totalRemoval){
							searchHolderAccountsBalances();
							if(lstHolderAccountBalance!=null && lstHolderAccountBalance.getDataList().size() > 0){
								for (TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance) {
									/*//REGLA INEXISTENTE si alguna cuenta no es juridica no se puede retirar de manera total
										if(!(!security.isBbbSecurityClass() && !security.isBlpSecurityClass() && 
											!security.isBmsSecurityClass() && !security.isPgbSecurityClass() && 
												!security.isBbcSecurityClass() && !security.isPgsSecurityClass() && 
													!security.isVtdSecurityClass())){
										if(!holderAccountBalanceTO.getHolderAccountBalance().getHolderAccount().getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
											showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
													, PropertiesUtilities.getMessage("msg.retirement.security.invalid.security.total"));
											registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
											JSFUtilities.showSimpleValidationDialog();
											lstHolderAccountBalance=null;
											return;
										}
										// CUI DEL EMISOR
										if(!holderAccountBalanceTO.getHolderAccountBalance().getHolderAccount().getHolderAccountDetails().get(0).getHolder().getIdHolderPk().equals(holdIssuer)){									
											showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
													, PropertiesUtilities.getMessage("MENSAJE: NO SE ENCUENTRA EN EL CUI DEL EMISOR"));
											registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
											JSFUtilities.showSimpleValidationDialog();
											lstHolderAccountBalance=null;
											return;
										}
									}*/
								}								
								for (TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance) {
										holderAccountBalanceTO.setBlCheck(true);
										//issue 186
										if(holderAccountBalanceTO.getHolderAccountBalance().getAvailableBalance().intValue() == 0){
											holderAccountBalanceTO.setQuantityRemoveBalance(null);
										}else{
											holderAccountBalanceTO.setQuantityRemoveBalance(holderAccountBalanceTO.getHolderAccountBalance().getAvailableBalance());
										}
								}
							}
						}
					}
					
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	/**
	 * Evaluate dates.
	 * Evaluate if firstDate is greater than secondDate
	 *
	 * @param firstDate the first date
	 * @param secondDate the second date
	 * @return the boolean
	 */
	public Boolean evaluateDates(Date firstDate, Date secondDate) {
		Boolean greaterDate = Boolean.FALSE;
		if (firstDate.after(secondDate)) {
			greaterDate = Boolean.TRUE;
		}
		return greaterDate;
	}// end evaluateDates
	
	/**
	 * Show isin detail.
	 *
	 * @param securityCode the security code
	 */
	public void showIsinDetail(String securityCode){
	  securityHelpBean.setSecurityCode(securityCode);
	  securityHelpBean.setName("securityHelp");
	  securityHelpBean.searchSecurityHandler();
	 }
	
	
	/**
	 * Register retirement.
	 *
	 * @return the string
	 */
	public String registerRetirement(){
		try {
			
			registerRetirementOperation =  new RegisterRetirementOperationTO();
			registerRetirementOperation.setRetirementOperation(new RetirementOperation());
			registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
			securityTargetReg = null;
			bolAllCheckBox = false;
			lstHolderAccountBalance = null;
			
			setViewOperationType(ViewOperationsType.REGISTER.getCode());
			retirementOperationFilter = new RetirementOperationTO();
			return REGISTER_VIEW_MAPPING;
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
			return null;
		}
	}
	
	/**
	 * Clean register.
	 */
	public void cleanRegister(){
		try {
			JSFUtilities.resetViewRoot();
			lstHolderAccountBalance = null;
			securityTargetReg = null;
			setCheckBoxReg(false);
			bolAllCheckBox= false;
			existSecurityTarget = false;
			setAllFalse();
			registerRetirementOperation =  new RegisterRetirementOperationTO();
			registerRetirementOperation.setRetirementOperation(new RetirementOperation());
			registerRetirementOperation.getRetirementOperation().setSecurity(new Security());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Clean.
	 */
	public void clean(){
		try {
			JSFUtilities.resetViewRoot();
			retirementOperationFilter = new RetirementOperationTO();
			
			selectedRetirementOperation = null;
			lstRetirementOperation = null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		
		for(TransferAccountBalanceTO hab : lstHolderAccountBalance){
			if(hab.getIdParticipantPk().equals( retirementMarketFactBalance.getParticipantPk() )&&
					hab.getIdSecurityCodePk().equals( retirementMarketFactBalance.getSecurityCodePk() ) &&
					hab.getIdHolderAccountPk().equals( retirementMarketFactBalance.getHolderAccountPk() )){
				
				hab.setMarketFactDetails(new ArrayList<MarketFactTO>());
				hab.setQuantityRemoveBalance(BigDecimal.ZERO);
			
				for( MarketFactDetailHelpTO detail : retirementMarketFactBalance.getMarketFacBalances() ){
					if(detail.getIsSelected() && detail.getEnteredBalance()!=null && detail.getEnteredBalance().compareTo(BigDecimal.ZERO) > 0){
						MarketFactTO marketFactDetail = new MarketFactTO();					
						marketFactDetail.setMarketDate(detail.getMarketDate());
						marketFactDetail.setMarketPrice(detail.getMarketPrice());
						marketFactDetail.setMarketRate(detail.getMarketRate());
						marketFactDetail.setQuantity(detail.getEnteredBalance());
						marketFactDetail.setIdMarketFactPk(detail.getMarketFactBalancePk());
						
						hab.setQuantityRemoveBalance(hab.getQuantityRemoveBalance().add(marketFactDetail.getQuantity()));
						hab.getMarketFactDetails().add(marketFactDetail);
					}
				}
			}
		}
		
		JSFUtilities.updateComponent("contentForm:dtbResult");
	}
	
	/**
	 * Input market fact balance.
	 *
	 * @param transferAccountBalanceTO the transfer account balance to
	 */
	public void inputMarketFactBalance(TransferAccountBalanceTO transferAccountBalanceTO) {	
		
		if(Validations.validateIsNullOrEmpty(transferAccountBalanceTO.getQuantityRemoveBalance()) ){
			transferAccountBalanceTO.setQuantityRemoveBalance(null);
			return ;
		}		
		if(BigDecimal.ZERO.equals(transferAccountBalanceTO.getQuantityRemoveBalance())){
			transferAccountBalanceTO.setQuantityRemoveBalance(null);
			return ;
		}
		
		if(transferAccountBalanceTO.getQuantityRemoveBalance().compareTo(transferAccountBalanceTO.getHolderAccountBalance().getAvailableBalance()) > 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("msg.retirement.security.retirement_balance"));
			transferAccountBalanceTO.setQuantityRemoveBalance(null);
			JSFUtilities.showSimpleValidationDialog();
			return ;
		}
		
		transferAccountBalanceTO.setQuantityRemoveBalance(transferAccountBalanceTO.getQuantityRemoveBalance());						
		transferAccountBalanceTO.setBlCheck(true);
		selectMarketFact(transferAccountBalanceTO);
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void showMarketFactUI(TransferAccountBalanceTO objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBalTO.getIdParticipantPk());
		marketFactBalance.setUiComponentName("balancemarket1");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
		
		MarketFactBalanceHelpTO marketFactBalanceHelpTO = marketFactBean.getMarketFat("balancemarket1");

		if(!totalRemoval){			
			if(Validations.validateListIsNotNullAndNotEmpty(objHolAccBalTO.getMarketFactDetails())){
				for (MarketFactTO marketFactDetail : objHolAccBalTO.getMarketFactDetails()) {
					for(MarketFactDetailHelpTO marketFactDetailHelpTO : marketFactBalanceHelpTO.getMarketFacBalances()){
						if(marketFactDetail.getIdMarketFactPk().equals(marketFactDetailHelpTO.getMarketFactBalancePk())){
							if(marketFactDetail.getQuantity()!=null && marketFactDetail.getQuantity().compareTo(BigDecimal.ZERO) > 0){
								marketFactDetailHelpTO.setEnteredBalance(marketFactDetail.getQuantity());
								marketFactDetailHelpTO.setIsSelected(true);
								marketFactBalanceHelpTO.setBalanceResult(marketFactBalanceHelpTO.getBalanceResult().add(marketFactDetailHelpTO.getEnteredBalance()));
							}
						}
					}
				}								
			}
		} else {
			marketFactBalanceHelpTO.setIndHandleDetails(ComponentConstant.ONE);
			for(MarketFactDetailHelpTO marketFactDetailHelpTO : marketFactBalanceHelpTO.getMarketFacBalances()){
				if(marketFactDetailHelpTO.getAvailableBalance()!=null && marketFactDetailHelpTO.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
					marketFactDetailHelpTO.setEnteredBalance(marketFactDetailHelpTO.getAvailableBalance());
					marketFactDetailHelpTO.setIsSelected(true);
					marketFactDetailHelpTO.setIsDisabled(true);
					marketFactBalanceHelpTO.setBalanceResult(marketFactBalanceHelpTO.getBalanceResult().add(marketFactDetailHelpTO.getEnteredBalance()));
				}
			}
		}
		
	}
	
	/**
	 * Show market fact ui detail.
	 *
	 * @param retirementDetail the retirement detail
	 */
	public void showMarketFactUIDetail(RetirementDetail retirementDetail){
		helpMarketFactBalanceView = new MarketFactBalanceHelpTO();

		helpMarketFactBalanceView.setSecurityCodePk(registerRetirementOperation.getRetirementOperation().getSecurity().getIdSecurityCodePk());
		helpMarketFactBalanceView.setHolderAccountPk(retirementDetail.getHolderAccount().getIdHolderAccountPk());
		helpMarketFactBalanceView.setParticipantPk(retirementDetail.getParticipant().getIdParticipantPk());
		helpMarketFactBalanceView.setSecurityDescription(registerRetirementOperation.getRetirementOperation().getSecurity().getDescription());
		helpMarketFactBalanceView.setInstrumentType(registerRetirementOperation.getRetirementOperation().getSecurity().getInstrumentType());
		helpMarketFactBalanceView.setInstrumentDescription((String) getParametersTableMap().get(helpMarketFactBalanceView.getInstrumentType()));
		helpMarketFactBalanceView.setBalanceResult(BigDecimal.ZERO);
		helpMarketFactBalanceView.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		helpMarketFactBalanceView.setIndHandleDetails(ComponentConstant.ONE);

		List<RetirementMarketfact> lstMarketFact = retirementOperationServiceFacade.findRetirementMarketFactByRetirement(retirementDetail.getIdRetirementDetailPk());
		if(Validations.validateListIsNotNullAndNotEmpty(lstMarketFact)){
			for (RetirementMarketfact marketFact : lstMarketFact) {
				MarketFactDetailHelpTO marketFactBalanceHelpTO = new MarketFactDetailHelpTO();
				marketFactBalanceHelpTO.setTotalBalance(marketFact.getOperationQuantity());
				marketFactBalanceHelpTO.setMarketDate(marketFact.getMarketDate());
				marketFactBalanceHelpTO.setMarketPrice(marketFact.getMarketPrice());
				marketFactBalanceHelpTO.setMarketRate(marketFact.getMarketRate());
				helpMarketFactBalanceView.getMarketFacBalances().add(marketFactBalanceHelpTO);
				
				helpMarketFactBalanceView.setBalanceResult(helpMarketFactBalanceView.getBalanceResult().add(marketFactBalanceHelpTO.getTotalBalance()));
			}
		}
		
	}
	
	/**
	 * Check retirement change.
	 *
	 * @param obj the obj
	 */
	public void onSelectHolderAccountBalance(TransferAccountBalanceTO obj){
		try {
			if(Validations.validateIsNull(obj)){
				for (TransferAccountBalanceTO lstHolderAccountBalanceAux : lstHolderAccountBalance) {
					if(!getBolAllCheckBox()){
						//lstHolderAccountBalanceAux.setQuantityRemoveBalance(BigDecimal.ZERO);
						lstHolderAccountBalanceAux.setBlockOperations(null);
						lstHolderAccountBalanceAux.setMarketFactDetails(new ArrayList<MarketFactTO>());
						lstHolderAccountBalanceAux.setBlCheck(false);
					} else {
						lstHolderAccountBalanceAux.setBlCheck(true);
					}
				}
			}else {
				if(!obj.isBlCheck()){
					obj.setQuantityRemoveBalance(null);
					obj.setMarketFactDetails(new ArrayList<MarketFactTO>());
					obj.setBlockOperations(null);
				}else{
					if(obj.getSecurityClass().equals(SecurityClassType.DPF.getCode()) || obj.getSecurityClass().equals(SecurityClassType.DPA.getCode())){
						selectMarketFact(obj);
					} else {
						//obj.setQuantityRemoveBalance(BigDecimal.ZERO);
					}										
				}
			}
			
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * On select block balance.
	 *
	 * @param obj the obj
	 * @param affectationType the affectation type
	 * @throws ServiceException the service exception
	 */
	public void onSelectBlockBalance(TransferAccountBalanceTO obj, Integer affectationType) throws ServiceException{
		obj.setCurrentAffectationType(affectationType);
		registerRetirementOperation.setSelectedAccountBalanceTO(obj);
		if(Validations.validateListIsNullOrEmpty(obj.getBlockOperations())){
			BlockOperationTO blockOperationTO = new BlockOperationTO();
			blockOperationTO.setIdHolderAccountPk(obj.getIdHolderAccountPk());
			blockOperationTO.setIdParticipantPk(obj.getIdParticipantPk());
			blockOperationTO.setSecurityCodePk(obj.getIdSecurityCodePk());
			blockOperationTO.setIdStateAffectation(AffectationStateType.BLOCKED.getCode());
			blockOperationTO.setIndEarlyRedemption(ComponentConstant.ONE);
			List<BlockOperation> boList = retirementOperationServiceFacade.findBlockOperationsByFilters(blockOperationTO);
			
			obj.setBlockOperations(boList);
		}
		obj.updateCurrentBlockOperations(affectationType);
	}
	
	/**
	 * On select block balance detail.
	 *
	 * @param retirementDetail the retirement detail
	 * @throws ServiceException the service exception
	 */
	public void onSelectBlockBalanceDetail(RetirementDetail retirementDetail) throws ServiceException{
		
		List<RetirementBlockDetail> list = retirementOperationServiceFacade.getRetirementBlockDetails(retirementDetail.getIdRetirementDetailPk());
		
		registerRetirementOperation.setRetirementBlockDetails(list);
	}
	
	/**
	 * On select block operation.
	 *
	 * @param blockOperation the block operation
	 * @throws ServiceException the service exception
	 */
	public void onSelectBlockOperation(BlockOperation blockOperation) throws ServiceException{
		BigDecimal blockBalance = registerRetirementOperation.getSelectedAccountBalanceTO().getCurrentBlockBalance();
		
		if(blockOperation.getSelected()){			
			
			if(!LevelAffectationType.BLOCK.getCode().equals(blockOperation.getBlockLevel())){
				if(!registerRetirementOperation.getSelectedAccountBalanceTO().isAllBlockLevelChecked()){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
							PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CHANGE_OWNERSHIP_BLOCK_TARGET_UNABLE_ADD_REBLOCK));
					JSFUtilities.showSimpleValidationDialog();
					blockOperation.setSelected(false);
					return;
				}
			}
			
			blockOperation.setBlockMarketFactOperations(removeSecurityServiceFacade.getBlockMarketFactOperations(blockOperation.getIdBlockOperationPk()));
			
			blockBalance = blockBalance.add(blockOperation.getActualBlockBalance());
		}else{
			
			if(LevelAffectationType.BLOCK.getCode().equals(blockOperation.getBlockLevel())){
				registerRetirementOperation.getSelectedAccountBalanceTO().setAllReblockLevelUnchecked();
			}
			
			blockBalance = blockBalance.subtract(blockOperation.getActualBlockBalance());
		}	
		
		registerRetirementOperation.getSelectedAccountBalanceTO().setCurrentBlockBalance(blockBalance);
	}

	/**
	 * Search retirement operations.
	 */
	@LoggerAuditWeb
	public void searchRetirementOperations(){
		showPrivilegeButtons();
		selectedRetirementOperation = null;
		List<RetirementOperationTO> retirementOperationTOsTemp = null;
		try {
			
			/* Setting on 1 when it isn't a sirtex's operation*/ 			
			retirementOperationFilter.setIndWithdrawalBCB(0);
			retirementOperationTOsTemp = removeSecurityServiceFacade.searchRetirementOperations(retirementOperationFilter);
			lstRetirementOperation = new GenericDataModel<RetirementOperationTO>(retirementOperationTOsTemp);
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	
	/**
	 * Metodo para verificar que el valor esta habilitado para el retiro
	 * issue 116 - retiro de valores Q
	 * @return
	 */
	/*public boolean verifySecurityClass (){
		
		Boolean totalRem = false;
		
		if(!totalRemoval){
			Security security = registerRetirementOperation.getRetirementOperation().getSecurity();
			
			if(!(!security.isBbbSecurityClass() && !security.isBlpSecurityClass() && 
					!security.isBmsSecurityClass() && !security.isPgbSecurityClass() && 
						!security.isBbcSecurityClass() && !security.isVtdSecurityClass())){
				totalRem=true;
			}else{
				totalRem=false;
			}
		}
		return totalRem;
	}
	*/
	
	/**
	 * Gets the holder accounts balances.
	 *
	 * @return the holder accounts balances
	 * @throws ServiceException the service exception
	 */
	public void searchHolderAccountsBalances() throws ServiceException{
		JSFUtilities.hideGeneralDialogues();
		//bolAllCheckBox = false;
		lstHolderAccountBalance = null;
		registerRetirementOperation.setSelectedAccountBalanceTO(null);
	
		Long idHolderAccountPk = null;
		
		if(singleRemoval){
			if(registerRetirementOperation.getSelectedHolderAccount()==null){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			idHolderAccountPk = registerRetirementOperation.getSelectedHolderAccount().getIdHolderAccountPk();
		}
		if(registerRetirementOperation.getSelectedHolderAccount()!=null){
			idHolderAccountPk = registerRetirementOperation.getSelectedHolderAccount().getIdHolderAccountPk();
		}
		String idSecurityCode = registerRetirementOperation.getRetirementOperation().getSecurity().getIdSecurityCodePk();
		Long participantId = registerRetirementOperation.getSelectedParticipant().getIdParticipantPk();
		
		List<TransferAccountBalanceTO> holderAccountsBalancesTOs = removeSecurityServiceFacade.getHolderAccountsBalances(idSecurityCode, 
				participantId, idHolderAccountPk, totalRemoval, true, false);											
		
		lstHolderAccountBalance = new GenericDataModel<TransferAccountBalanceTO>(holderAccountsBalancesTOs);
		/* CUI DEL EMISOR
		if(!totalRemoval){ //issue 116
			Security security = registerRetirementOperation.getRetirementOperation().getSecurity();
			
			if(!(!security.isBbbSecurityClass() && !security.isBlpSecurityClass() && 
					!security.isBmsSecurityClass() && !security.isPgbSecurityClass() && 
						!security.isBbcSecurityClass() && !security.isPgsSecurityClass() && 
							!security.isVtdSecurityClass())){
				Long holdIssuer = null;
				Security issuerSecurity = retirementOperationServiceFacade.getSecurityWithIssuer(security.getIdSecurityCodePk());
				
				if (!issuerSecurity.equals(null)){
					holdIssuer = issuerSecurity.getIssuer().getHolder().getIdHolderPk();
				}
				
				for (TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance) {
					if(holderAccountBalanceTO.getHolderAccountBalance().getHolderAccount().getAccountType().equals(HolderAccountType.JURIDIC.getCode())){
						if(holderAccountBalanceTO.getHolderAccountBalance().getHolderAccount().getHolderAccountDetails().get(0).getHolder().getIdHolderPk().equals(holdIssuer)){
							holderAccountBalanceTO.setBlCheckBlock(true);
							//holderAccountBalanceTO.setBlCheck(false);
						}else{
							holderAccountBalanceTO.setBlCheckBlock(false);
							//holderAccountBalanceTO.setBlCheck(false);
						}
					}else{
						holderAccountBalanceTO.setBlCheckBlock(false);
						//holderAccountBalanceTO.setBlCheck(false);
					}
				}
				return;
			}	
		}
		*/
		if(lstHolderAccountBalance!=null && lstHolderAccountBalance.getDataList().size() == 1){
			for (TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance) {
				holderAccountBalanceTO.setBlCheck(true);
			}
		}
		
		// issue 1023: obtencion de la de la descricpcion del Titular(cui-nombreTitular)
		String descriptionHolder;
		for (TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance)
			try {
				descriptionHolder = removeSecurityServiceFacade.getDescriptionHolderFromIdHolderAccount(holderAccountBalanceTO.getIdHolderAccountPk());
				if (descriptionHolder != null)
					holderAccountBalanceTO.setHolderName(descriptionHolder);
				else
					holderAccountBalanceTO.setHolderName("");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("::: error al obtener la descricion de los Titulares " + e.getMessage());
				holderAccountBalanceTO.setHolderName("");
			}
		
	}
	
	
	

	/**
	 * Before register retirement operation.
	 */
	public void beforeRegisterRetirementOperation(){

		if(lstHolderAccountBalance == null || lstHolderAccountBalance.getSize() == 0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_NOTHING_TO_RETIRE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		boolean atleast = false;
		List<String> incorrectAccounts = null;
		
		for(TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance){
			if(holderAccountBalanceTO.isBlCheck()) {
						
				if(totalRemoval){
					//issue 186
					if(!Validations.validateIsNotNull(holderAccountBalanceTO.getQuantityRemoveBalance())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_ZERO_TO_RETIRE, new Object[]{holderAccountBalanceTO.getAlternateCode()}));
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
					//issue 186
					if(holderAccountBalanceTO.getQuantityRemoveBalance().intValue() == 0){
						holderAccountBalanceTO.setQuantityRemoveBalance(null);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_ZERO_TO_RETIRE, new Object[]{holderAccountBalanceTO.getAlternateCode()}));
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
					incorrectAccounts = new ArrayList<String>();
					if(holderAccountBalanceTO.getHolderAccountBalance().getTransitBalance().compareTo(BigDecimal.ZERO) > 0 || 
							holderAccountBalanceTO.getHolderAccountBalance().getAccreditationBalance().compareTo(BigDecimal.ZERO) > 0 ||
							holderAccountBalanceTO.getHolderAccountBalance().getReportedBalance().compareTo(BigDecimal.ZERO) > 0 ||
							holderAccountBalanceTO.getHolderAccountBalance().getReportingBalance().compareTo(BigDecimal.ZERO) > 0 ){
						incorrectAccounts.add(holderAccountBalanceTO.getAlternateCode());
					}
				}else{
					if(!holderAccountBalanceTO.isSelected()){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_ZERO_TO_RETIRE, new Object[]{holderAccountBalanceTO.getAlternateCode()}));
						JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}
				
				atleast = true;
			}
		}
		
		if(!atleast){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_NOTHING_TO_RETIRE));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(incorrectAccounts)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_INCORRECT_BALANCES,
							new Object[]{StringUtils.join(incorrectAccounts,GeneralConstants.STR_COMMA_WITHOUT_SPACE)}));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_CONFIRM_SUCCESS));
		JSFUtilities.executeJavascriptFunction("PF('confirmSaveDialog').show()");
		
	}
	/**
	 * Register retirement operation.
	 */
	@LoggerAuditWeb
	public void saveRetirementOperation(){
		try{						
			
			registerRetirementOperation.getRetirementOperation().setRetirementDetails(new ArrayList<RetirementDetail>());
			BigDecimal totalBalance = BigDecimal.ZERO;
			
			for(TransferAccountBalanceTO holderAccountBalanceTO : lstHolderAccountBalance){
				
				if(holderAccountBalanceTO.isSelected()) {
					
					RetirementDetail retirementDetail = new RetirementDetail();
					if(Validations.validateIsNotNullAndNotEmpty(holderAccountBalanceTO.getQuantityRemoveBalance())){						
						retirementDetail.setAvailableBalance(holderAccountBalanceTO.getQuantityRemoveBalance());
					} else {
						retirementDetail.setAvailableBalance(BigDecimal.ZERO);
					}					
					retirementDetail.setHolderAccount(new HolderAccount(holderAccountBalanceTO.getIdHolderAccountPk()));
					retirementDetail.setParticipant(new Participant(holderAccountBalanceTO.getIdParticipantPk()));
					retirementDetail.setBanBalance(holderAccountBalanceTO.getBanBalance());
					retirementDetail.setPawnBalance(holderAccountBalanceTO.getPawnBalance());
					retirementDetail.setOtherBlockBalance(holderAccountBalanceTO.getOtherBlockBalance());
					retirementDetail.setRetirementOperation(registerRetirementOperation.getRetirementOperation());
					
					if(holderAccountBalanceTO.getQuantityRemoveBalance()!=null && holderAccountBalanceTO.getQuantityRemoveBalance().compareTo(BigDecimal.ZERO) > 0){
						if(ComponentConstant.ONE.equals(idepositarySetup.getIndMarketFact()) && !totalRemoval ){
							retirementDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());
							if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getMarketFactDetails())){
								for(MarketFactTO marketFactTO : holderAccountBalanceTO.getMarketFactDetails()){
									RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
									retirementMarketfact.setRetirementDetail(retirementDetail);
									retirementMarketfact.setMarketDate(marketFactTO.getMarketDate());
									retirementMarketfact.setMarketRate(marketFactTO.getMarketRate());
									retirementMarketfact.setMarketPrice(marketFactTO.getMarketPrice());
									retirementMarketfact.setOperationQuantity(marketFactTO.getQuantity());
									retirementDetail.getRetirementMarketfacts().add(retirementMarketfact);
								}
							}
						}
					}
					
					if(Validations.validateListIsNotNullAndNotEmpty(holderAccountBalanceTO.getBlockOperations())){						
						retirementDetail.setRetirementBlockDetails(new ArrayList<RetirementBlockDetail>());
						for (BlockOperation blockOperation : holderAccountBalanceTO.getBlockOperations()) {
							if(blockOperation.getSelected()){
								RetirementBlockDetail retirementBlockDetail = new RetirementBlockDetail();
								retirementBlockDetail.setBlockOperation(blockOperation);
								retirementBlockDetail.setRetirementDetail(retirementDetail);
								retirementBlockDetail.setBlockQuantity(blockOperation.getActualBlockBalance());
								retirementBlockDetail.setRetirementMarketfacts(new ArrayList<RetirementMarketfact>());
								
								for(BlockMarketFactOperation blockMarketFactOperation : blockOperation.getBlockMarketFactOperations()){
									RetirementMarketfact retirementMarketfact = new RetirementMarketfact();
									retirementMarketfact.setRetirementBlockDetail(retirementBlockDetail);
									retirementMarketfact.setMarketDate(blockMarketFactOperation.getMarketDate());
									retirementMarketfact.setMarketRate(blockMarketFactOperation.getMarketRate());
									retirementMarketfact.setMarketPrice(blockMarketFactOperation.getMarketPrice());
									retirementMarketfact.setOperationQuantity(blockMarketFactOperation.getActualBlockBalance());
									retirementBlockDetail.getRetirementMarketfacts().add(retirementMarketfact);
								}
								
								retirementDetail.getRetirementBlockDetails().add(retirementBlockDetail);
							}
						}
					}
					
					totalBalance = totalBalance.add(retirementDetail.getTotalBalance());
					registerRetirementOperation.getRetirementOperation().getRetirementDetails().add(retirementDetail);
				}
				
			}						
			
			registerRetirementOperation.getRetirementOperation().setTotalBalance(totalBalance);
			
			registerRetirementOperation.getRetirementOperation().setIndRetirementExpiration(BooleanType.NO.getCode());
			
				
			removeSecurityServiceFacade.saveRetirementOperation(registerRetirementOperation.getRetirementOperation(),totalRemoval);

			Object [] bodyData = {registerRetirementOperation.getRetirementOperation().getCustodyOperation().getOperationNumber().toString()};
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_REGISTER_SUCCESS,bodyData));
			JSFUtilities.executeJavascriptFunction("PF('confirmDialogMessage').show();");
			
		} catch (ServiceException e) {
			if(e.getMessage()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
		
	}
	
	/**
	 * Onchange participant.
	 */
	public void onchangeParticipant(){
		JSFUtilities.hideGeneralDialogues();
		lstHolderAccountBalance = null;
		registerRetirementOperation.setHolder(new Holder());
		registerRetirementOperation.setSelectedHolderAccount(null);
		registerRetirementOperation.setLstHolderAccounts(null);
	}

	/**
	 * Before reject.
	 *
	 * @return the string
	 */
	public String beforeReject(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			return validateBeforeAction(RetirementOperationStateType.REJECTED.getCode());
			
		}catch (ServiceException e) {
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Before confirm.
	 *
	 * @return the string
	 */
	public String beforeConfirm(){		
		JSFUtilities.hideGeneralDialogues();
		try{			
			return validateBeforeAction(RetirementOperationStateType.CONFIRMED.getCode());
			
		}catch (ServiceException e) {
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
		
	/**
	 * Validate before action.
	 *
	 * @param newState the new state
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String validateBeforeAction(Integer newState) throws ServiceException {
		if(Validations.validateIsNullOrEmpty(selectedRetirementOperation)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showValidationDialog();
			return null;
		}
		
		List<Integer> expectedStates = new ArrayList<Integer>();
		
		if(newState.equals(RetirementOperationStateType.ANNULLED.getCode())){
			expectedStates.add(RetirementOperationStateType.REGISTERED.getCode());
			setViewOperationType(ViewOperationsType.ANULATE.getCode());
		} else if(newState.equals(RetirementOperationStateType.APPROVED.getCode())){
			expectedStates.add(RetirementOperationStateType.REGISTERED.getCode());
			setViewOperationType(ViewOperationsType.APPROVE.getCode());
		} else if(newState.equals(RetirementOperationStateType.REJECTED.getCode())){
			expectedStates.add(RetirementOperationStateType.APPROVED.getCode());
			setViewOperationType(ViewOperationsType.REJECT.getCode());
		}  else if(newState.equals(RetirementOperationStateType.CONFIRMED.getCode())){
			expectedStates.add(RetirementOperationStateType.APPROVED.getCode());
			setViewOperationType(ViewOperationsType.CONFIRM.getCode());
		}
		
		if(!expectedStates.contains(selectedRetirementOperation.getState())){
			List<String> stringStates = new ArrayList<String>();
			for(Integer state : expectedStates){
				stringStates.add((String) getParametersTableMap().get(state));
			}
			
			Object[] bodyData = new Object[]{StringUtils.join(stringStates.toArray(),GeneralConstants.STR_COMMA_WITHOUT_SPACE)};
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)						
					, PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_INVALID_STATE_REQ, bodyData));
			JSFUtilities.showValidationDialog();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
			return null;
		}
		
		loadDetailView(selectedRetirementOperation);
		return DETAIL_VIEW_MAPPING;
	}

	/**
	 * Before approve.
	 *
	 * @return the string
	 */
	public String beforeApprove(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			return validateBeforeAction(RetirementOperationStateType.APPROVED.getCode());
			
		}catch (ServiceException e) {
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Before action.
	 */
	@LoggerAuditWeb
	public void beforeAction(){
		JSFUtilities.hideGeneralDialogues();
		String headerMessage = null;
		String bodyMessage = null;
		if(isViewOperationApprove()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_APPROVE;
			bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_CONFIRM_APPROVE;
		}else if(isViewOperationAnnul()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_ANNUL;
			bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_CONFIRM_ANNULATE;
		}else if(isViewOperationConfirm()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_CONFIRM_CONFIRM;
		}else if(isViewOperationReject()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REJECT;
			bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_CONFIRM_REJECT;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage)
				, PropertiesUtilities.getMessage(bodyMessage, new Object[]{
						registerRetirementOperation.getRetirementOperation().getCustodyOperation().getOperationNumber().toString()}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Before show reject.
	 */
	public void beforeShowReject(){
		JSFUtilities.resetComponent(":contentForm:dialogMotiveReject");
		showMotiveText = false;
		registerRetirementOperation.getRetirementOperation().setRejectMotive(null);
		registerRetirementOperation.getRetirementOperation().setRejectMotiveOther(null);
		JSFUtilities.showComponent(":contentForm:dialogMotiveReject");
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":contentForm:dialogMotiveReject");
		JSFUtilities.hideComponent(":contentForm:dialogMotiveReject");	
	}
	
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler() {
		if(Validations.validateIsNotNullAndNotEmpty(registerRetirementOperation.getRetirementOperation().getRejectMotive())){
			if(ERROR_ANULATE.equals(registerRetirementOperation.getRetirementOperation().getRejectMotive())){
				showMotiveText = true;
			} else {
				showMotiveText = false;
			}
		} else{
			showMotiveText = false;
		}
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		JSFUtilities.hideGeneralDialogues();
		try {
			
			String bodyMessage = null;
			if(isViewOperationApprove()){
				bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_APPROVED_SUCCESS;
				removeSecurityServiceFacade.approveRetirementOperation(registerRetirementOperation.getRetirementOperation());
			}else if(isViewOperationAnnul()){
				bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_ANULLED_SUCCESS;
				removeSecurityServiceFacade.anullateRetirementOperation(registerRetirementOperation.getRetirementOperation());
			}else if(isViewOperationConfirm()){
				bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_CONFIRMED_SUCCESS;
				removeSecurityServiceFacade.confirmRetirementOperation(registerRetirementOperation.getRetirementOperation());
			}else if(isViewOperationReject()){
				bodyMessage = PropertiesConstants.MSG_RETIREMENT_SECURITIES_REJECTED_SUCCESS;
				removeSecurityServiceFacade.rejectRetirementOperation(registerRetirementOperation.getRetirementOperation());
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, PropertiesUtilities.getMessage(bodyMessage, new Object[]{registerRetirementOperation.getRetirementOperation().getCustodyOperation().getOperationNumber()}));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show()");
		}
		searchRetirementOperations();
	}
	
	/**
	 * Before anullate.
	 *
	 * @return the string
	 */
	public String beforeAnullate(){
		JSFUtilities.hideGeneralDialogues();
		try{			
			return validateBeforeAction(RetirementOperationStateType.ANNULLED.getCode());
			
		}catch (ServiceException e) {
		    excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	/**
	 * Anullate retirement operation.
	 */

	
	@LoggerAuditWeb
	public void backToSearchPageHandler() {
		JSFUtilities.resetViewRoot();		
		searchRetirementOperations();
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
	}
	
	
	/**
	 * Go to detail.
	 *
	 * @param retirementOperationTO the retirement operation to
	 * @return the string
	 */
	public String goToDetail(RetirementOperationTO retirementOperationTO){	
		try {
			loadDetailView(retirementOperationTO);
			return DETAIL_VIEW_MAPPING;
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
		
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){
		PrivilegeComponent privilegeComp = new PrivilegeComponent();
		privilegeComp.setBtnApproveView(true);
		privilegeComp.setBtnAnnularView(true);
		privilegeComp.setBtnConfirmView(true);
		privilegeComp.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComp);
	}
	
	/**
	 * Load detail view.
	 *
	 * @param retirementOperationSelected the retirement operation selected
	 * @throws ServiceException the service exception
	 */
	private void loadDetailView(RetirementOperationTO retirementOperationSelected) throws ServiceException{
		registerRetirementOperation = new RegisterRetirementOperationTO();
		registerRetirementOperation.setRetirementOperation(removeSecurityServiceFacade.findRetirmentOperation(retirementOperationSelected.getIdRetirementOperationPk()));
		
		// issue 1023: obtencion de la de la descricpcion del Titular(cui-nombreTitular)
		String descriptionHolder;
		for (RetirementDetail retirementDetail : registerRetirementOperation.getRetirementOperation().getRetirementDetails())
			try {
				descriptionHolder = removeSecurityServiceFacade.getDescriptionHolderFromIdHolderAccount(retirementDetail.getHolderAccount().getIdHolderAccountPk());
				if (descriptionHolder != null)
					retirementDetail.setDescriptionHolder(descriptionHolder);
				else
					retirementDetail.setDescriptionHolder("");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("::: error al obtener la descricion de los Titulares " + e.getMessage());
				retirementDetail.setDescriptionHolder("");
			}
		
	}	
	
	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the register retirement operation.
	 *
	 * @return the register retirement operation
	 */
	public RegisterRetirementOperationTO getRegisterRetirementOperation() {
		return registerRetirementOperation;
	}

	/**
	 * Gets the lst holder account balance.
	 *
	 * @return the lst holder account balance
	 */
	public GenericDataModel<TransferAccountBalanceTO> getLstHolderAccountBalance() {
		return lstHolderAccountBalance;
	}
	
	/**
	 * Sets the lst holder account balance.
	 *
	 * @param lstHolderAccountBalance the new lst holder account balance
	 */
	public void setLstHolderAccountBalance(GenericDataModel<TransferAccountBalanceTO> lstHolderAccountBalance) {
		this.lstHolderAccountBalance = lstHolderAccountBalance;
	}
	
	/**
	 * Gets the lst motives.
	 *
	 * @return the lst motives
	 */
	public List<ParameterTable> getLstMotives() {
		return lstMotives;
	}

	/**
	 * Sets the lst motives.
	 *
	 * @param lstMotives the new lst motives
	 */
	public void setLstMotives(List<ParameterTable> lstMotives) {
		this.lstMotives = lstMotives;
	}

	/**
	 * Gets the lst states.
	 *
	 * @return the lst states
	 */
	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	/**
	 * Sets the lst states.
	 *
	 * @param lstStates the new lst states
	 */
	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	/**
	 * Gets the lst retirement type.
	 *
	 * @return the lst retirement type
	 */
	public List<ParameterTable> getLstRetirementType() {
		return lstRetirementType;
	}

	/**
	 * Sets the lst retirement type.
	 *
	 * @param lstRetirementType the new lst retirement type
	 */
	public void setLstRetirementType(List<ParameterTable> lstRetirementType) {
		this.lstRetirementType = lstRetirementType;
	}

	/**
	 * Sets the register retirement operation.
	 *
	 * @param retirementOperationSession the new register retirement operation
	 */
	public void setRegisterRetirementOperation(
			RegisterRetirementOperationTO retirementOperationSession) {
		this.registerRetirementOperation = retirementOperationSession;
	}

	/**
	 * Gets the retirement market fact balance.
	 *
	 * @return the retirement market fact balance
	 */
	public MarketFactBalanceHelpTO getRetirementMarketFactBalance() {
		return retirementMarketFactBalance;
	}

	/**
	 * Sets the retirement market fact balance.
	 *
	 * @param retirementMarketFactBalance the new retirement market fact balance
	 */
	public void setRetirementMarketFactBalance(
			MarketFactBalanceHelpTO retirementMarketFactBalance) {
		this.retirementMarketFactBalance = retirementMarketFactBalance;
	}

	/**
	 * Gets the lst retirement market fact to.
	 *
	 * @return the lst retirement market fact to
	 */
	public List<MarketFactTO> getLstRetirementMarketFactTO() {
		return lstRetirementMarketFactTO;
	}

	/**
	 * Sets the lst retirement market fact to.
	 *
	 * @param lstRetirementMarketFactTO the new lst retirement market fact to
	 */
	public void setLstRetirementMarketFactTO(
			List<MarketFactTO> lstRetirementMarketFactTO) {
		this.lstRetirementMarketFactTO = lstRetirementMarketFactTO;
	}

	/**
	 * Gets the lst cbo securitie class.
	 *
	 * @return the lst cbo securitie class
	 */
	public List<ParameterTable> getLstCboSecuritieClass() {
		return lstCboSecuritieClass;
	}

	/**
	 * Sets the lst cbo securitie class.
	 *
	 * @param lstCboSecuritieClass the new lst cbo securitie class
	 */
	public void setLstCboSecuritieClass(List<ParameterTable> lstCboSecuritieClass) {
		this.lstCboSecuritieClass = lstCboSecuritieClass;
	}

	/**
	 * Gets the bol all check box.
	 *
	 * @return the bol all check box
	 */
	public Boolean getBolAllCheckBox() {
		return bolAllCheckBox;
	}

	/**
	 * Sets the bol all check box.
	 *
	 * @param bolAllCheckBox the new bol all check box
	 */
	public void setBolAllCheckBox(Boolean bolAllCheckBox) {
		this.bolAllCheckBox = bolAllCheckBox;
	}

	/**
	 * Gets the selected retirement operation.
	 *
	 * @return the selected retirement operation
	 */
	public RetirementOperationTO getSelectedRetirementOperation() {
		return selectedRetirementOperation;
	}

	/**
	 * Sets the selected retirement operation.
	 *
	 * @param selectedRetirementOperation the new selected retirement operation
	 */
	public void setSelectedRetirementOperation(
			RetirementOperationTO selectedRetirementOperation) {
		this.selectedRetirementOperation = selectedRetirementOperation;
	}

	/**
	 * Sets the lst retirement operation.
	 *
	 * @param lstRetirementOperation the new lst retirement operation
	 */
	public void setLstRetirementOperation(
			GenericDataModel<RetirementOperationTO> lstRetirementOperation) {
		this.lstRetirementOperation = lstRetirementOperation;
	}

	/**
	 * Gets the lst retirement operation.
	 *
	 * @return the lst retirement operation
	 */
	public GenericDataModel<RetirementOperationTO> getLstRetirementOperation() {
		return lstRetirementOperation;
	}

	/**
	 * Checks if is multiple removal.
	 *
	 * @return true, if is multiple removal
	 */
	public boolean isMultipleRemoval() {
		return multipleRemoval;
	}

	/**
	 * Sets the multiple removal.
	 *
	 * @param multipleRemoval the new multiple removal
	 */
	public void setMultipleRemoval(boolean multipleRemoval) {
		this.multipleRemoval = multipleRemoval;
	}

	/**
	 * Checks if is total removal.
	 *
	 * @return true, if is total removal
	 */
	public boolean isTotalRemoval() {
		return totalRemoval;
	}

	/**
	 * Sets the total removal.
	 *
	 * @param totalRemoval the new total removal
	 */
	public void setTotalRemoval(boolean totalRemoval) {
		this.totalRemoval = totalRemoval;
	}

	/**
	 * Checks if is single removal.
	 *
	 * @return true, if is single removal
	 */
	public boolean isSingleRemoval() {
		return singleRemoval;
	}

	/**
	 * Sets the single removal.
	 *
	 * @param singleRemoval the new single removal
	 */
	public void setSingleRemoval(boolean singleRemoval) {
		this.singleRemoval = singleRemoval;
	}

	/**
	 * Gets the help market fact balance view.
	 *
	 * @return the help market fact balance view
	 */
	public MarketFactBalanceHelpTO getHelpMarketFactBalanceView() {
		return helpMarketFactBalanceView;
	}

	/**
	 * Sets the help market fact balance view.
	 *
	 * @param helpMarketFactBalanceView the new help market fact balance view
	 */
	public void setHelpMarketFactBalanceView(
			MarketFactBalanceHelpTO helpMarketFactBalanceView) {
		this.helpMarketFactBalanceView = helpMarketFactBalanceView;
	}

	/**
	 * Gets the retirement operation filter.
	 *
	 * @return the retirement operation filter
	 */
	public RetirementOperationTO getRetirementOperationFilter() {
		return retirementOperationFilter;
	}

	/**
	 * Sets the retirement operation filter.
	 *
	 * @param retirementOperationFilter the new retirement operation filter
	 */
	public void setRetirementOperationFilter(
			RetirementOperationTO retirementOperationFilter) {
		this.retirementOperationFilter = retirementOperationFilter;
	}

	/**
	 * Gets the massive remove securities type.
	 *
	 * @return the massive remove securities type
	 */
	public String getMassiveRemoveSecuritiesType() {
		return massiveRemoveSecuritiesType;
	}

	/**
	 * Sets the massive remove securities type.
	 *
	 * @param massiveRemoveSecuritiesType the new massive remove securities type
	 */
	public void setMassiveRemoveSecuritiesType(String massiveRemoveSecuritiesType) {
		this.massiveRemoveSecuritiesType = massiveRemoveSecuritiesType;
	}
	
	
	/**
	 * Select market fact.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void selectMarketFact(TransferAccountBalanceTO objHolAccBalTO){
		BigDecimal amountRemove = new BigDecimal(0);
		if(objHolAccBalTO.getHolderAccountBalance().getAvailableBalance().compareTo(BigDecimal.ONE) == 0){
			amountRemove = objHolAccBalTO.getHolderAccountBalance().getAvailableBalance();
		} else if(objHolAccBalTO.getQuantityRemoveBalance().compareTo(BigDecimal.ZERO) > 0){
			amountRemove = objHolAccBalTO.getQuantityRemoveBalance();
		}
		selectMarketFactAvailable(objHolAccBalTO, amountRemove);
	}
	
	/**
	 * Select market fact available.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 * @param amountRemove the amount remove
	 */
	public void selectMarketFactAvailable(TransferAccountBalanceTO objHolAccBalTO, BigDecimal amountRemove){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBalTO.getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBalTO.getIdParticipantPk());
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		marketFactBalance.setIndTotalBalance(BooleanType.YES.getCode());		
		marketFactBalance.setBalanceResult(amountRemove);
		
		try {
			retirementMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		MarketFactBalanceHelpTO marketFactBalanceHelpTO = this.retirementMarketFactBalance;			
		for(TransferAccountBalanceTO hab : lstHolderAccountBalance){
			if(hab.getIdParticipantPk().equals( objHolAccBalTO.getIdParticipantPk() )&&
					hab.getIdSecurityCodePk().equals( objHolAccBalTO.getIdSecurityCodePk() ) &&
					hab.getIdHolderAccountPk().equals( objHolAccBalTO.getIdHolderAccountPk() )){
				
				hab.setMarketFactDetails(new ArrayList<MarketFactTO>());
				hab.setQuantityRemoveBalance(BigDecimal.ZERO);
			
				for( MarketFactDetailHelpTO detail : marketFactBalanceHelpTO.getMarketFacBalances() ){
					if(detail.getIsSelected() && detail.getEnteredBalance()!=null && detail.getEnteredBalance().compareTo(BigDecimal.ZERO) > 0){
						MarketFactTO marketFactDetail = new MarketFactTO();					
						marketFactDetail.setMarketDate(detail.getMarketDate());
						marketFactDetail.setMarketPrice(detail.getMarketPrice());
						marketFactDetail.setMarketRate(detail.getMarketRate());
						marketFactDetail.setQuantity(detail.getEnteredBalance());
						marketFactDetail.setIdMarketFactPk(detail.getMarketFactBalancePk());							
						hab.setQuantityRemoveBalance(hab.getQuantityRemoveBalance().add(marketFactDetail.getQuantity()));
						hab.getMarketFactDetails().add(marketFactDetail);
					}
				}
			}
		}	
	}

	/**
	 * Checks if is holder parcial.
	 *
	 * @return the holderParcial
	 */
	public boolean isHolderParcial() {
		return holderParcial;
	}

	/**
	 * Sets the holder parcial.
	 *
	 * @param holderParcial the holderParcial to set
	 */
	public void setHolderParcial(boolean holderParcial) {
		this.holderParcial = holderParcial;
	}

	/**
	 * Gets the lst motives reject.
	 *
	 * @return the lstMotivesReject
	 */
	public List<ParameterTable> getLstMotivesReject() {
		return lstMotivesReject;
	}

	/**
	 * Sets the lst motives reject.
	 *
	 * @param lstMotivesReject the lstMotivesReject to set
	 */
	public void setLstMotivesReject(List<ParameterTable> lstMotivesReject) {
		this.lstMotivesReject = lstMotivesReject;
	}

	/**
	 * Checks if is show motive text.
	 *
	 * @return the showMotiveText
	 */
	public boolean isShowMotiveText() {
		return showMotiveText;
	}

	/**
	 * Sets the show motive text.
	 *
	 * @param showMotiveText the showMotiveText to set
	 */
	public void setShowMotiveText(boolean showMotiveText) {
		this.showMotiveText = showMotiveText;
	}
	
	public Boolean getCheckBoxReg() {
		return checkBoxReg;
	}

	public void setCheckBoxReg(Boolean checkBoxReg) {
		this.checkBoxReg = checkBoxReg;
	}
	
	public Boolean getExistSecurityTarget() {
		return existSecurityTarget;
	}

	public void setExistSecurityTarget(Boolean existSecurityTarget) {
		this.existSecurityTarget = existSecurityTarget;
	}
	
	public Security getSecurityTargetReg() {
		return securityTargetReg;
	}

	public void setSecurityTargetReg(Security securityTargetReg) {
		this.securityTargetReg = securityTargetReg;
	}
	
}
