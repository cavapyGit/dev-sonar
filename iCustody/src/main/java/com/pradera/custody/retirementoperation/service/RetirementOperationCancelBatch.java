//package com.pradera.custody.retirementoperation.service;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.enterprise.context.RequestScoped;
//import javax.enterprise.inject.Instance;
//import javax.inject.Inject;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import com.pradera.commons.type.BusinessProcessType;
//import com.pradera.commons.utils.CommonsUtilities;
//import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
//import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
//import com.pradera.core.framework.batchprocess.BatchProcess;
//import com.pradera.core.framework.batchprocess.service.JobExecution;
//import com.pradera.custody.retirementoperation.to.RegisterRetirementOperationTO;
//import com.pradera.integration.component.business.service.AccountsComponentService;
//import com.pradera.integration.component.business.to.AccountsComponentTO;
//import com.pradera.integration.exception.ServiceException;
//import com.pradera.model.component.type.ParameterOperationType;
//import com.pradera.model.corporatives.CorporativeOperation;
//import com.pradera.model.corporatives.type.CorporateProcessStateType;
//import com.pradera.model.custody.securitiesretirement.RetirementDetail;
//import com.pradera.model.custody.securitiesretirement.RetirementOperation;
//import com.pradera.model.custody.type.RetirementOperationMotiveType;
//import com.pradera.model.custody.type.RetirementOperationStateType;
//import com.pradera.model.generalparameter.type.MasterTableType;
//import com.pradera.model.process.ProcessLogger;
//// TODO: Auto-generated Javadoc
//
///**
// * <ul>
// * <li>
// * Project iDepositary.
// * Copyright PraderaTechnologies 2014.</li>
// * </ul>
// * 
// * The Class RetirementOperationCancelBatch.
// *
// * @author PraderaTechnologies.
// * @version 1.0 , 04-abr-2014
// */
//@BatchProcess(name="RetirementOperationCancelBatch")
//@RequestScoped
//public class RetirementOperationCancelBatch implements JobExecution {
//
//	/** The retirement operation service bean. */
//	@EJB
//	private RetirementOperationServiceBean retirementOperationServiceBean;
//	
//	/** The holiday query service bean. */
//	@EJB
//	private HolidayQueryServiceBean holidayQueryServiceBean;
//	/** The Constant logger. */
//	private static final Logger logger = LoggerFactory.getLogger(RetirementOperationCancelBatch.class);
//	
//	/** The parameters facade. */
//	@Inject
//	private GeneralParametersFacade parametersFacade;
//	
//	/** The master table fk. */
//	private final Integer masterTableFk = MasterTableType.MAX_DAYS_AVAILABLES_ACCREDITATION_CERTIFICATION.getCode();
//	
//	/* (non-Javadoc)
//	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
//	 */
//	@Override
//	public void startJob(ProcessLogger processLogger) {
//		
//		try {
//			this.cancelRetirementOperations();
//		} catch (ServiceException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			logger.error("cancelRetirementOperations");
//		}
//	}
//
//	/**
//	 * Cancel retirement operations.
//	 *
//	 * @throws ServiceException the service exception
//	 */
//	public void cancelRetirementOperations() throws ServiceException{
//		RegisterRetirementOperationTO retirementOperationTO = new RegisterRetirementOperationTO();		
//		Integer days = parametersFacade.getMaxDayForRequestsToCancell(masterTableFk); 
//		Date finalDate = this.holidayQueryServiceBean.getCalculateDate(CommonsUtilities.currentDate(),  days, 0,null);
//		retirementOperationTO.setEndDate(finalDate);
//		List<RetirementOperation> retirementOperationLst = retirementOperationServiceBean.searchRetirementOperationForBatch(retirementOperationTO);
//		for(RetirementOperation tmpRetirementOperation : retirementOperationLst){			
//			//Date cancelDate = this.holidayQueryServiceBean.getCalculateDate(tmpRetirementOperation.getCustodyOperation().getRegistryDate(),  3, 1);			
//			if((tmpRetirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) 
//				|| tmpRetirementOperation.getMotive().equals(RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode()))
//				){
//		        try {
//		        	Long businessType = BusinessProcessType.SECURITY_RETIREMENT_CANCEL.getCode();
//					/*** MOVING BALANCES ***/
//					this.moveBalance(retirementOperationTO, businessType);
//					tmpRetirementOperation.getCustodyOperation().setState(RetirementOperationStateType.REJECTED.getCode());
//					tmpRetirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
//					tmpRetirementOperation.setState(RetirementOperationStateType.REJECTED.getCode());
//					this.retirementOperationServiceBean.update(tmpRetirementOperation);
//				} catch (ServiceException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					logger.error("cancelRetirementOperations");
//				}
//			}
//			
//			if(RetirementOperationMotiveType.RESCUE.getCode().equals(tmpRetirementOperation.getMotive())
//					|| RetirementOperationMotiveType.REDEMPTION_FIXED_INCOME.getCode().equals(tmpRetirementOperation.getMotive())){
//				if(RetirementOperationStateType.APPROVED.getCode().equals(tmpRetirementOperation.getState())){
//					tmpRetirementOperation.getLstCorporativeOperation().size();
//					for(CorporativeOperation corporativeOperation:tmpRetirementOperation.getLstCorporativeOperation()){
//						corporativeOperation.setState(CorporateProcessStateType.ANNULLED.getCode());
//						retirementOperationServiceBean.update(corporativeOperation);
//					}
//				}
//				tmpRetirementOperation.getCustodyOperation().setState(RetirementOperationStateType.REJECTED.getCode());
//				tmpRetirementOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
//				tmpRetirementOperation.setState(RetirementOperationStateType.REJECTED.getCode());
//				retirementOperationServiceBean.update(tmpRetirementOperation);
//			}
//		}
//	}
//	
//	/**
//	 * Move balance.
//	 *
//	 * @param retirementOperation the retirement operation
//	 * @param businessProcessType the business process type
//	 * @throws ServiceException the service exception
//	 */
//	public void moveBalance(RegisterRetirementOperationTO retirementOperation, Long businessProcessType) throws ServiceException{		
//		//RetirementDetails list Object
//		List<RetirementDetail> retirementDetailList = retirementOperation.getRetirementDetails();
//		//HolderAccountBalanceTO list
//		List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<com.pradera.integration.component.business.to.HolderAccountBalanceTO>();
//		for(int i=0;i<retirementDetailList.size();++i){
//			com.pradera.integration.component.business.to.HolderAccountBalanceTO holderAccountBalanceTO = new com.pradera.integration.component.business.to.HolderAccountBalanceTO();
//			holderAccountBalanceTO.setIdHolderAccount(retirementDetailList.get(i).getHolderAccount().getIdHolderAccountPk());
//			holderAccountBalanceTO.setIdParticipant(retirementDetailList.get(i).getHolderAccount().getParticipant().getIdParticipantPk());
//			holderAccountBalanceTO.setIdSecurityCode(retirementOperation.getIsinCodePk());
//			holderAccountBalanceTO.setStockQuantity(retirementDetailList.get(i).getAccountBalance());
//			//Adding holderAccountBalanceTO to List
//			accountBalanceTOs.add(holderAccountBalanceTO);
//		}
//		Long operationType;
//		if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REDEMPTION_VARIABLE_INCOME.getCode())){
//			operationType = ParameterOperationType.SECURITIES_REDEMPTION.getCode();
//		}else if(retirementOperation.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode())){
//			operationType = ParameterOperationType.SECURITIES_REVERSION.getCode();
//		}else{
//			operationType = ParameterOperationType.SECURITIES_RESCUE.getCode();
//		}
//		accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(retirementOperation.getIdRetirementOperationPk(),
//																						  businessProcessType, 
//																				  		  accountBalanceTOs,operationType));	
//	}
//	
//	/**
//	 * Gets the accounts component to.
//	 *
//	 * @param idCustodyOperation the id custody operation
//	 * @param businessProcessType the business process type
//	 * @param accountBalanceTOs the account balance t os
//	 * @param operationType the operation type
//	 * @return the accounts component to
//	 */
//	public AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, List<com.pradera.integration.component.business.to.HolderAccountBalanceTO> accountBalanceTOs, Long operationType){
//		// ComponentTO Object
//		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
//		
//		objAccountComponent.setIdBusinessProcess(businessProcessType);
//		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
//		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
//		objAccountComponent.setIdOperationType(operationType);
//		return objAccountComponent;
//	}
//	
//	/** The executor component service bean. */
//    @Inject
//    Instance<AccountsComponentService> accountsComponentService;
//
//
//	/* (non-Javadoc)
//	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
//	 */
//	@Override
//	public Object[] getParametersNotification() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	/* (non-Javadoc)
//	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
//	 */
//	@Override
//	public List<Object> getDestinationInstitutions() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	/* (non-Javadoc)
//	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
//	 */
//	@Override
//	public boolean sendNotification() {
//		// TODO Auto-generated method stub
//		return true;
//	}
//}
