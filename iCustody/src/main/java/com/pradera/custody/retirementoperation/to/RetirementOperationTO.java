package com.pradera.custody.retirementoperation.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.component.OperationTypeCascade;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RetirementOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class RetirementOperationTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id retirement operation pk. */
	private Long idRetirementOperationPk;
	
	/** The operation number. */
	private Long operationNumber;
	
	/** The motive. */
	private Integer motive;
	
	/** The state. */
	private Integer state;
	
	/** The total balance. */
	private BigDecimal totalBalance;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The participant mnemonic. */
	private String participantMnemonic;
	
	/** The id holder pk. */
	private Long idHolderPk;
	
	/** The holder name. */
	private String holderName;
	
	/** The holder account number. */
	private Integer holderAccountNumber;
	
	/** The id holder account pk. */
	private List<Long> idHolderAccountPk;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The security. */
	private Security security;
	
	/** The isin code pk. */
	private String idSecurityCodePk;
	
	/** The id security bc bcode. */
	private String idSecurityBCBcode;
	
	/** The retirement balance. */
	private BigDecimal retirementBalance;
	
	/** The currency. */
	private Integer currency;
	
	/** The id holder account. */
	private Long idHolderAccount;
	
	/** The current nominal value. */
	private BigDecimal currentNominalValue;
	
	/** The currency type description. */
	private String currencyDescription;
	
	/** The motive type description. */
	private String motiveDescription;
	
	/** The state type description. */
	private String stateDescription;
	
	/** The ind withdrawal bcb. */
	private Integer indWithdrawalBCB;

	/** The init date. */
	private Date initDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The lst operation type. */
	private List<OperationTypeCascade> lstOperationType;
	
	private String coupon;
	
	private BigDecimal nominalValue;
	
	private String payroll;
	
	private String stateDesc;
	
	private String certificateFromTo;
	
	/**
	 * Instantiates a new retirement operation to.
	 */
	public RetirementOperationTO() {
		super();
		this.security = new Security();
		this.initDate = CommonsUtilities.currentDate();
		this.endDate = CommonsUtilities.currentDate();
	}

	/**
	 * Gets the id retirement operation pk.
	 *
	 * @return the id retirement operation pk
	 */
	public Long getIdRetirementOperationPk() {
		return idRetirementOperationPk;
	}

	/**
	 * Sets the id retirement operation pk.
	 *
	 * @param idRetirementOperationPk the new id retirement operation pk
	 */
	public void setIdRetirementOperationPk(Long idRetirementOperationPk) {
		this.idRetirementOperationPk = idRetirementOperationPk;
	}

	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}

	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Gets the participant mnemonic.
	 *
	 * @return the participant mnemonic
	 */
	public String getParticipantMnemonic() {
		return participantMnemonic;
	}

	/**
	 * Sets the participant mnemonic.
	 *
	 * @param participantMnemonic the new participant mnemonic
	 */
	public void setParticipantMnemonic(String participantMnemonic) {
		this.participantMnemonic = participantMnemonic;
	}

	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * Gets the holder name.
	 *
	 * @return the holder name
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * Sets the holder name.
	 *
	 * @param holderName the new holder name
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * Gets the holder account number.
	 *
	 * @return the holder account number
	 */
	public Integer getHolderAccountNumber() {
		return holderAccountNumber;
	}

	/**
	 * Sets the holder account number.
	 *
	 * @param holderAccountNumber the new holder account number
	 */
	public void setHolderAccountNumber(Integer holderAccountNumber) {
		this.holderAccountNumber = holderAccountNumber;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public List<Long> getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(List<Long> idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the retirement balance.
	 *
	 * @return the retirement balance
	 */
	public BigDecimal getRetirementBalance() {
		return retirementBalance;
	}

	/**
	 * Sets the retirement balance.
	 *
	 * @param retirementBalance the new retirement balance
	 */
	public void setRetirementBalance(BigDecimal retirementBalance) {
		this.retirementBalance = retirementBalance;
	}

	/**
	 * Gets the inits the date.
	 *
	 * @return the inits the date
	 */
	public Date getInitDate() {
		return initDate;
	}

	/**
	 * Sets the inits the date.
	 *
	 * @param initDate the new inits the date
	 */
	public void setInitDate(Date initDate) {
		this.initDate = initDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the id holder account.
	 *
	 * @return the id holder account
	 */
	public Long getIdHolderAccount() {
		return idHolderAccount;
	}

	/**
	 * Sets the id holder account.
	 *
	 * @param idHolderAccount the new id holder account
	 */
	public void setIdHolderAccount(Long idHolderAccount) {
		this.idHolderAccount = idHolderAccount;
	}

	/**
	 * Gets the current nominal value.
	 *
	 * @return the current nominal value
	 */
	public BigDecimal getCurrentNominalValue() {
		return currentNominalValue;
	}

	/**
	 * Sets the current nominal value.
	 *
	 * @param currentNominalValue the new current nominal value
	 */
	public void setCurrentNominalValue(BigDecimal currentNominalValue) {
		this.currentNominalValue = currentNominalValue;
	}


	/**
	 * Gets the currency description.
	 *
	 * @return the currency description
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * Sets the currency description.
	 *
	 * @param currencyDescription the new currency description
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	/**
	 * Gets the motive description.
	 *
	 * @return the motive description
	 */
	public String getMotiveDescription() {
		return motiveDescription;
	}

	/**
	 * Sets the motive description.
	 *
	 * @param motiveDescription the new motive description
	 */
	public void setMotiveDescription(String motiveDescription) {
		this.motiveDescription = motiveDescription;
	}

	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}

	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the lst operation type.
	 *
	 * @return the lst operation type
	 */
	public List<OperationTypeCascade> getLstOperationType() {
		return lstOperationType;
	}

	/**
	 * Sets the lst operation type.
	 *
	 * @param lstOperationType the new lst operation type
	 */
	public void setLstOperationType(List<OperationTypeCascade> lstOperationType) {
		this.lstOperationType = lstOperationType;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the ind withdrawal bcb.
	 *
	 * @return the ind withdrawal bcb
	 */
	public Integer getIndWithdrawalBCB() {
		return indWithdrawalBCB;
	}

	/**
	 * Sets the ind withdrawal bcb.
	 *
	 * @param indWithdrawalBCB the new ind withdrawal bcb
	 */
	public void setIndWithdrawalBCB(Integer indWithdrawalBCB) {
		this.indWithdrawalBCB = indWithdrawalBCB;
	}

	/**
	 * Gets the id security bc bcode.
	 *
	 * @return the id security bc bcode
	 */
	public String getIdSecurityBCBcode() {
		return idSecurityBCBcode;
	}

	/**
	 * Sets the id security bc bcode.
	 *
	 * @param idSecurityBCBcode the new id security bc bcode
	 */
	public void setIdSecurityBCBcode(String idSecurityBCBcode) {
		this.idSecurityBCBcode = idSecurityBCBcode;
	}
	
	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	public String getPayroll() {
		return payroll;
	}

	public void setPayroll(String payroll) {
		this.payroll = payroll;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public String getCertificateFromTo() {
		return certificateFromTo;
	}

	public void setCertificateFromTo(String certificateFromTo) {
		this.certificateFromTo = certificateFromTo;
	}

}
