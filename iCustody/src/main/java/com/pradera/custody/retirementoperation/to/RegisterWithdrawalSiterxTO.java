package com.pradera.custody.retirementoperation.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RegisterWithdrawalSiterxTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class RegisterWithdrawalSiterxTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The participant. */
	private Participant participant;
	
	/** The holder. */
	private Holder holder;
	
	/** The lst holder account. */
	private List<HolderAccount> lstHolderAccount;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The check all. */
	private boolean checkAll;
	
	/** The lst holder account balance. */
	private GenericDataModel<TransferAccountBalanceTO> lstHolderAccountBalance;
	
	/** The sirtex withdrawal operation. */
	private RetirementOperation sirtexWithdrawalOperation; //this will uses to view
	
	/**
	 * Instantiates a new register withdrawal siterx to.
	 */
	public RegisterWithdrawalSiterxTO() {
		super();
		this.issuer= new Issuer();
		this.holder= new Holder();
		this.participant= new Participant();
		this.holderAccount= new HolderAccount();
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the lst holder account balance.
	 *
	 * @return the lst holder account balance
	 */
	public GenericDataModel<TransferAccountBalanceTO> getLstHolderAccountBalance() {
		return lstHolderAccountBalance;
	}

	/**
	 * Sets the lst holder account balance.
	 *
	 * @param lstHolderAccountBalance the new lst holder account balance
	 */
	public void setLstHolderAccountBalance(
			GenericDataModel<TransferAccountBalanceTO> lstHolderAccountBalance) {
		this.lstHolderAccountBalance = lstHolderAccountBalance;
	}

	/**
	 * Gets the lst holder account.
	 *
	 * @return the lst holder account
	 */
	public List<HolderAccount> getLstHolderAccount() {
		return lstHolderAccount;
	}

	/**
	 * Sets the lst holder account.
	 *
	 * @param lstHolderAccount the new lst holder account
	 */
	public void setLstHolderAccount(List<HolderAccount> lstHolderAccount) {
		this.lstHolderAccount = lstHolderAccount;
	}

	/**
	 * Checks if is check all.
	 *
	 * @return true, if is check all
	 */
	public boolean isCheckAll() {
		return checkAll;
	}

	/**
	 * Sets the check all.
	 *
	 * @param checkAll the new check all
	 */
	public void setCheckAll(boolean checkAll) {
		this.checkAll = checkAll;
	}

	/**
	 * Gets the sirtex withdrawal operation.
	 *
	 * @return the sirtex withdrawal operation
	 */
	public RetirementOperation getSirtexWithdrawalOperation() {
		return sirtexWithdrawalOperation;
	}

	/**
	 * Sets the sirtex withdrawal operation.
	 *
	 * @param sirtexWithdrawalOperation the new sirtex withdrawal operation
	 */
	public void setSirtexWithdrawalOperation(
			RetirementOperation sirtexWithdrawalOperation) {
		this.sirtexWithdrawalOperation = sirtexWithdrawalOperation;
	}
	
}
