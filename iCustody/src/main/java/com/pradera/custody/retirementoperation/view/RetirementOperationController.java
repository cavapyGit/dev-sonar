package com.pradera.custody.retirementoperation.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.custody.retirementoperation.facade.RetirementOperationServiceFacade;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.RetirementOperationMotiveType;


@Singleton
@Performance
@Path("/CustodyProcessResource")
@Interceptors(ContextHolderInterceptor.class)
public class RetirementOperationController implements Serializable{

	@EJB
	RetirementOperationServiceFacade removeSecurityServiceFacade;
	
	private static final long serialVersionUID = 1L;
	
	@GET
	@Path("/retirementoperation/{idCorporativeOperationPk}")
	public String retirement(@PathParam("idCorporativeOperationPk")Long idCorporativeOperationPk) throws ServiceException {

		List<RetirementDetail> lstRegisterRetirementDetail = null;
		List<RetirementOperation> lstRetirementOperation = null;
		AccountAnnotationOperation accountAnnotationOperation = null;
		LoggerUser loggerUser = createDefaultLoggerUser();
		

		try {
			
			lstRegisterRetirementDetail = removeSecurityServiceFacade.generateRetirementDetailCorporative(idCorporativeOperationPk);
			
			if(lstRegisterRetirementDetail!=null && lstRegisterRetirementDetail.size()>0) {
				lstRetirementOperation = new ArrayList<RetirementOperation>();
				RetirementOperation retirementOperation = null;
				
				for(RetirementDetail retirementDetail : lstRegisterRetirementDetail) {
						accountAnnotationOperation = removeSecurityServiceFacade.getAccountAnnotationOperation(retirementDetail.getSecurity().getIdSecurityCodePk());
					
						retirementOperation =  new RetirementOperation();
						retirementOperation.setMotive(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode());
						retirementOperation.setSecurity(retirementDetail.getSecurity());
						retirementOperation.setIndRetirementExpiration(BooleanType.YES.getCode());
						retirementOperation.setTotalBalance(retirementDetail.getTotalBalance());
						retirementOperation.setPhysicalCertificate(retirementDetail.getPhysicalCertificate());
						retirementOperation.setHolderAccount(retirementDetail.getHolderAccount());
						retirementOperation.setParticipant(retirementDetail.getParticipant());
						retirementOperation.setAccountAnnotationOperation(accountAnnotationOperation);
						retirementOperation.setRetirementDetails(new ArrayList<RetirementDetail>());
						retirementOperation.getRetirementDetails().add(retirementDetail);
						lstRetirementOperation.add(retirementOperation);
				}
			}
		
			if( lstRetirementOperation!=null && lstRetirementOperation.size()>0 ) {
				removeSecurityServiceFacade.saveRetirementOperationCorporative(lstRetirementOperation.get(0),loggerUser);
				return "sucess";
			}
			
		} catch (ServiceException e) {
			return "error";
		}

		return "error";
	}
	

	@GET
	@Path("/retirementoperationConfirm/{idCorporativeOperationPk}")
	public String retirementoperationConfirm(@PathParam("idCorporativeOperationPk")Long idCorporativeOperationPk) throws ServiceException {

		CorporativeOperation corporativeOperation = removeSecurityServiceFacade.findCorporativeOperationById(idCorporativeOperationPk);
		PhysicalCertificate physicalCertificate = null;
		if(corporativeOperation.getProgramInterestCoupon()!=null) {
			physicalCertificate = removeSecurityServiceFacade.findPhysicalCertificateByIdProgramInterestPk(corporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk());
		}
		
		if(corporativeOperation.getProgramAmortizationCoupon()!=null) {
			physicalCertificate = removeSecurityServiceFacade.findPhysicalCertificateAmortization(corporativeOperation.getSecurities().getIdSecurityCodePk());
		}
		
		if(physicalCertificate != null && physicalCertificate.getIdPhysicalCertificatePk()!=null) {
			RetirementOperation objRetirementOperation = removeSecurityServiceFacade.findRetirementOperationWithPhysicalCertificate(physicalCertificate.getIdPhysicalCertificatePk());
			LoggerUser loggerUser = createDefaultLoggerUser();
			removeSecurityServiceFacade.confirmRetirementCorporative(objRetirementOperation, loggerUser);
		}
		
		return "error";
	}
	
	public LoggerUser createDefaultLoggerUser() {
		LoggerUser loggerUser = new LoggerUser();
		loggerUser.setIpAddress("127.0.0.1");
		loggerUser.setUserName("ADMIN");
		loggerUser.setAuditTime(CommonsUtilities.currentDate());
		loggerUser.setIdPrivilegeOfSystem(1);
		
		return loggerUser;
	}
	
}
