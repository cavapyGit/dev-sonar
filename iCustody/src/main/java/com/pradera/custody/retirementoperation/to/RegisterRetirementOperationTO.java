package com.pradera.custody.retirementoperation.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.custody.affectation.to.TransferAccountBalanceTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.securitiesretirement.RetirementBlockDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RetirementOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class RegisterRetirementOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The selected participant. */
	private Participant selectedParticipant;
	
	/** The holder. */
	private Holder holder;
	
	/** The security. */
	private Security security;
	
	/** The selected holder account. */
	private HolderAccount selectedHolderAccount;
	
	/** The lst holder accounts. */
	private List<HolderAccount> lstHolderAccounts;
	
	/** The retirement operation. */
	private RetirementOperation retirementOperation;
	
	/** The retirement block details. */
	private List<RetirementBlockDetail> retirementBlockDetails;
	
	/** The selected account balance to. */
	private TransferAccountBalanceTO selectedAccountBalanceTO;
	
	/**
	 * Instantiates a new register retirement operation to.
	 */
	public RegisterRetirementOperationTO() {
		super();
		holder = new Holder();
		security = new Security();
		selectedParticipant = new Participant();
		selectedHolderAccount = new HolderAccount();
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the selected participant.
	 *
	 * @return the selected participant
	 */
	public Participant getSelectedParticipant() {
		return selectedParticipant;
	}

	/**
	 * Sets the selected participant.
	 *
	 * @param selectedParticipant the new selected participant
	 */
	public void setSelectedParticipant(Participant selectedParticipant) {
		this.selectedParticipant = selectedParticipant;
	}

	/**
	 * Gets the selected holder account.
	 *
	 * @return the selected holder account
	 */
	public HolderAccount getSelectedHolderAccount() {
		return selectedHolderAccount;
	}

	/**
	 * Sets the selected holder account.
	 *
	 * @param selectedHolderAccount the new selected holder account
	 */
	public void setSelectedHolderAccount(HolderAccount selectedHolderAccount) {
		this.selectedHolderAccount = selectedHolderAccount;
	}

	/**
	 * Gets the retirement operation.
	 *
	 * @return the retirement operation
	 */
	public RetirementOperation getRetirementOperation() {
		return retirementOperation;
	}

	/**
	 * Sets the retirement operation.
	 *
	 * @param retirementOperation the new retirement operation
	 */
	public void setRetirementOperation(RetirementOperation retirementOperation) {
		this.retirementOperation = retirementOperation;
	}

	/**
	 * Gets the lst holder accounts.
	 *
	 * @return the lst holder accounts
	 */
	public List<HolderAccount> getLstHolderAccounts() {
		return lstHolderAccounts;
	}

	/**
	 * Sets the lst holder accounts.
	 *
	 * @param lstHolderAccounts the new lst holder accounts
	 */
	public void setLstHolderAccounts(List<HolderAccount> lstHolderAccounts) {
		this.lstHolderAccounts = lstHolderAccounts;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the selected account balance to.
	 *
	 * @return the selected account balance to
	 */
	public TransferAccountBalanceTO getSelectedAccountBalanceTO() {
		return selectedAccountBalanceTO;
	}

	/**
	 * Sets the selected account balance to.
	 *
	 * @param selectedAccountBalanceTO the new selected account balance to
	 */
	public void setSelectedAccountBalanceTO(
			TransferAccountBalanceTO selectedAccountBalanceTO) {
		this.selectedAccountBalanceTO = selectedAccountBalanceTO;
	}

	/**
	 * Gets the retirement block details.
	 *
	 * @return the retirement block details
	 */
	public List<RetirementBlockDetail> getRetirementBlockDetails() {
		return retirementBlockDetails;
	}

	/**
	 * Sets the retirement block details.
	 *
	 * @param retirementBlockDetails the new retirement block details
	 */
	public void setRetirementBlockDetails(
			List<RetirementBlockDetail> retirementBlockDetails) {
		this.retirementBlockDetails = retirementBlockDetails;
	}

}
