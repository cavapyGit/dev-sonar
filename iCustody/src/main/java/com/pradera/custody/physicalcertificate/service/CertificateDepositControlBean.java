package com.pradera.custody.physicalcertificate.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.Query;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.type.PhysicalCertificateOperationType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class CertificateDepositControlBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Sep 26, 2015
 */
@Singleton
public class CertificateDepositControlBean extends CrudDaoServiceBean{
	

	/**
	 * Exits certificate.
	 *
	 * @param physicalCertificateList the physical certificate list
	 * @return the string
	 */
	public String exitsCertificate(List<PhysicalCertificate> physicalCertificateList){
		
		StringBuilder numberPhysical=new StringBuilder();
		List<Object[]> dataMap=new ArrayList<>();
		List<String> listIdSecurityCode=new ArrayList<>();
		List<String> listCertificateNumber=new ArrayList<>();

		
		for(int i=0;i<physicalCertificateList.size();i++){
			listIdSecurityCode.add(physicalCertificateList.get(i).getSecurities().getIdSecurityCodePk());
			listCertificateNumber.add(physicalCertificateList.get(i).getCertificateNumber());
			
			if(listIdSecurityCode.size()==1000 || i==listIdSecurityCode.size()-1){

				dataMap.addAll(findCertificateNumber(listIdSecurityCode, listCertificateNumber));
				
				em.flush(); 
				em.clear();
			}
		}
		
		for (Object[] object : dataMap) {
			Integer quantity= Integer.parseInt(object[2].toString());
			if(quantity>0){
				numberPhysical.append(object[0].toString()).append(GeneralConstants.STR_COMMA);
			}
		}
		
		
		
		return numberPhysical.toString();
		
	}
	
	/**
	 * Find certificate number.
	 *
	 * @param listIdSecurityCode the list id security code
	 * @param listCertificateNumber the list certificate number
	 * @return the list
	 */
	public List<Object[]> findCertificateNumber(List<String> listIdSecurityCode, List<String> listCertificateNumber){
		

			StringBuilder stringSql = new StringBuilder();
			stringSql.append("SELECT	").
					  append("	phc.certificateNumber,  ").
					  append("	phc.securities.idSecurityCodePk,").
					  append("	COUNT(phc) as tam	").
					  append("FROM PhysicalCertificate phc  	").
					  append("	join phc.physicalOperationDetails  pod ").
					  append("WHERE  phc.securities.idSecurityCodePk IN :listSecurity ").
					  append("	And phc.certificateNumber IN :listCertificateNumber	").
					  append("	And pod.state in(598,601,602) ").
					  
					  append("	And pod.idPhysicalOperationDetail not in( ").
					  append("	Select pod from PhysicalCertificateOperation pcopx ").
					  append("	join pcopx.physicalOperationDetails podx ").
					  append("	join podx.physicalCertificate pcx ").
					  append("	join pcx.physicalBalanceDetails pbd ").
					  append("	where pcx.certificateNumber in :listCertificateNumber ").
					  append("	and pcx.securities.idSecurityCodePk in :listSecurity ").
					  append("	and podx.state in (1111,602,601) ").
					  append("	and pcopx.physicalOperationType = :physicalOperationType ").
					  append("	and pbd.state in (606) ) ").
					  
					  append("Group By phc.certificateNumber, phc.securities.idSecurityCodePk ");
			
			Query query = em.createQuery(stringSql.toString());
			query.setParameter("listSecurity", listIdSecurityCode);
			query.setParameter("listCertificateNumber", listCertificateNumber);
			query.setParameter("physicalOperationType", new Long(PhysicalCertificateOperationType.RETIREMENT.getCode()));
			List<Object[]> dataList = query.getResultList();



		return dataList;
	}
	
}
