package com.pradera.custody.physicalcertificate.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class RegisterPhysicalCertificateTO.
 *
 * @author jquino
 */
public class RegisterPhysicalCertificateTO implements Serializable, Cloneable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The Physical Certificate Operation */
	private PhysicalCertificateOperation physicalCertificateOperation;
	
	/** The lst Physical Certificates. */
	private List<PhysicalCertificate> lstPhysicalCertificate;
	
	//constructor
	public RegisterPhysicalCertificateTO(){}

	public PhysicalCertificateOperation getPhysicalCertificateOperation() {
		return physicalCertificateOperation;
	}

	public void setPhysicalCertificateOperation(PhysicalCertificateOperation physicalCertificateOperation) {
		this.physicalCertificateOperation = physicalCertificateOperation;
	}

	public List<PhysicalCertificate> getLstPhysicalCertificate() {
		return lstPhysicalCertificate;
	}

	public void setLstPhysicalCertificate(List<PhysicalCertificate> lstPhysicalCertificate) {
		this.lstPhysicalCertificate = lstPhysicalCertificate;
	}
	
}