package com.pradera.custody.physicalcertificate.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.physical.PhysicalCertFile;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.issuancesecuritie.Security;

/**
 * Issue 1278
 * Carga Masiva de Titulos Fisicos
 * 
 * @author jquino
 */
public class DataExcelRegCertificateDepositeTO implements Serializable {

	private static final long serialVersionUID = 1L;

	//PARA EL PADRE
	private List<DataExcelRegCertificateDepositeTO> lstDataExcelResult;
	private List<String> lstErrorReadExcel;
	private List<String> lstErrorReadAttachment;
	
	//PARA LOS HIJOS
	private String 	holder; //string porque puede ser mancomunada
	private Long 	idHolderPk; //string porque puede ser mancomunada
	private String 	idHolderPkCellReference;
	private Integer accountNumber;
	private String 	accountNumberCellReference;
	private String 	idSecurityCodePk;
	private String 	idSecurityCodePkCellReference;
	private Date 	marketfactDate; 	//fecha HM
	private String 	marketfactDateCellReference;
	private BigDecimal 	markefactRate;  //tasa HM
	private String 		markefactRateCellReference;
	private BigDecimal 	markefactPrice; //precio HM
	private String 		markefactPriceCellReference;
	private String numberCertificate;
	private String numberCertificateCellReference;
	private BigDecimal 	quantity;
	private String 		quantityCellReference;
	private Date 	issuanceDate;
	private String 	issuanceDateCellReference;
	
	private String securityCertificate;
	private HolderAccount holderAccount;
	private Security security;
	private List<String> lstSecurityCertificate; //RV: claseClaveValor_numeroCertificado - si es RF: claseClaveValor
	private List<PhysicalCertificate> lstPhysicalCertificates;
	private Map<String, PhysicalCertFile> mapPhysicalCertFiles;
	
	
	public DataExcelRegCertificateDepositeTO() {
		this.lstDataExcelResult = new ArrayList<>();
		this.lstErrorReadExcel = new ArrayList<>();
		this.lstErrorReadAttachment = new ArrayList<>();
		this.lstPhysicalCertificates = new ArrayList<>();
		this.lstSecurityCertificate = new ArrayList<>();
		this.mapPhysicalCertFiles = new HashMap<String,PhysicalCertFile>();
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public List<String> getLstErrorReadExcel() {
		return lstErrorReadExcel;
	}

	public void setLstErrorReadExcel(List<String> lstErrorReadExcel) {
		this.lstErrorReadExcel = lstErrorReadExcel;
	}

	public List<DataExcelRegCertificateDepositeTO> getLstDataExcelResult() {
		return lstDataExcelResult;
	}

	public void setLstDataExcelResult(List<DataExcelRegCertificateDepositeTO> lstDataExcelResult) {
		this.lstDataExcelResult = lstDataExcelResult;
	}

	public String getIdHolderPkCellReference() {
		return idHolderPkCellReference;
	}

	public void setIdHolderPkCellReference(String idHolderPkCellReference) {
		this.idHolderPkCellReference = idHolderPkCellReference;
	}

	public String getAccountNumberCellReference() {
		return accountNumberCellReference;
	}

	public void setAccountNumberCellReference(String accountNumberCellReference) {
		this.accountNumberCellReference = accountNumberCellReference;
	}

	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	public String getIdSecurityCodePkCellReference() {
		return idSecurityCodePkCellReference;
	}

	public void setIdSecurityCodePkCellReference(String idSecurityCodePkCellReference) {
		this.idSecurityCodePkCellReference = idSecurityCodePkCellReference;
	}

	public Date getMarketfactDate() {
		return marketfactDate;
	}

	public void setMarketfactDate(Date marketfactDate) {
		this.marketfactDate = marketfactDate;
	}

	public String getMarketfactDateCellReference() {
		return marketfactDateCellReference;
	}

	public void setMarketfactDateCellReference(String marketfactDateCellReference) {
		this.marketfactDateCellReference = marketfactDateCellReference;
	}

	public BigDecimal getMarkefactRate() {
		return markefactRate;
	}

	public void setMarkefactRate(BigDecimal markefactRate) {
		this.markefactRate = markefactRate;
	}

	public String getMarkefactRateCellReference() {
		return markefactRateCellReference;
	}

	public void setMarkefactRateCellReference(String markefactRateCellReference) {
		this.markefactRateCellReference = markefactRateCellReference;
	}

	public BigDecimal getMarkefactPrice() {
		return markefactPrice;
	}

	public void setMarkefactPrice(BigDecimal markefactPrice) {
		this.markefactPrice = markefactPrice;
	}

	public String getMarkefactPriceCellReference() {
		return markefactPriceCellReference;
	}

	public void setMarkefactPriceCellReference(String markefactPriceCellReference) {
		this.markefactPriceCellReference = markefactPriceCellReference;
	}

	public String getNumberCertificate() {
		return numberCertificate;
	}

	public void setNumberCertificate(String numberCertificate) {
		this.numberCertificate = numberCertificate;
	}

	public String getNumberCertificateCellReference() {
		return numberCertificateCellReference;
	}

	public void setNumberCertificateCellReference(String numberCertificateCellReference) {
		this.numberCertificateCellReference = numberCertificateCellReference;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getQuantityCellReference() {
		return quantityCellReference;
	}

	public void setQuantityCellReference(String quantityCellReference) {
		this.quantityCellReference = quantityCellReference;
	}

	public Date getIssuanceDate() {
		return issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public String getIssuanceDateCellReference() {
		return issuanceDateCellReference;
	}

	public void setIssuanceDateCellReference(String issuanceDateCellReference) {
		this.issuanceDateCellReference = issuanceDateCellReference;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	public Long getIdHolderPk() {
		return idHolderPk;
	}

	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public String getSecurityCertificate() {
		return securityCertificate;
	}

	public void setSecurityCertificate(String securityCertificate) {
		this.securityCertificate = securityCertificate;
	}

	public List<String> getLstErrorReadAttachment() {
		return lstErrorReadAttachment;
	}

	public void setLstErrorReadAttachment(List<String> lstErrorReadAttachment) {
		this.lstErrorReadAttachment = lstErrorReadAttachment;
	}

	public Map<String, PhysicalCertFile> getMapPhysicalCertFiles() {
		return mapPhysicalCertFiles;
	}

	public void setMapPhysicalCertFiles(Map<String, PhysicalCertFile> mapPhysicalCertFiles) {
		this.mapPhysicalCertFiles = mapPhysicalCertFiles;
	}

	public List<String> getLstSecurityCertificate() {
		return lstSecurityCertificate;
	}

	public void setLstSecurityCertificate(List<String> lstSecurityCertificate) {
		this.lstSecurityCertificate = lstSecurityCertificate;
	}

	public List<PhysicalCertificate> getLstPhysicalCertificates() {
		return lstPhysicalCertificates;
	}

	public void setLstPhysicalCertificates(List<PhysicalCertificate> lstPhysicalCertificates) {
		this.lstPhysicalCertificates = lstPhysicalCertificates;
	}
	
	public ArrayList<PhysicalCertFile> getLstPhysicalCertFile(){
		if(!this.mapPhysicalCertFiles.isEmpty()){
			return new ArrayList<PhysicalCertFile>(this.mapPhysicalCertFiles.values());
		}
		return new ArrayList<PhysicalCertFile>();
	}
	
}