package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CommonUpdateBeanInf.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class CommonUpdateBeanInf implements Serializable{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	

	/** The state. */
	private Integer state;
	
	/** The old state. */
	private Integer oldState;
	
	/** The id custody operation pk. */
	private Long idCustodyOperationPk;
	
	/** The current user name. */
	private String currentUserName;
	
	/** The current update date. */
	private Date currentUpdateDate;
	
	/** The operation. */
	private Integer operation;
	
	/** The int appilied. */
	private Integer intAppilied;
	
	/** The certificate pk. */
	private Long certificatePk;
	
	/** The physical balance detail pk. */
	private Long physicalBalanceDetailPk;
	
	/** The cancel desc. */
	private String cancelDesc;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject motive s. */
	private Integer rejectMotiveS;
	
	/** The reject motive other. */
	private String rejectMotiveOther;
	
	/** The last modify app. */
	private Integer lastModifyApp;

	/** The last modify ip. */
	private String lastModifyIp;
	
	/** The operation type. */
	private Long operationType;
	
	/** The currency. */
	private Integer currency;
	
	/** The nominal value. */
	private BigDecimal nominalValue;

	/** The situation. */
	private Integer situation;
	
	/** The request state. */
	private Integer requestState;
	
	/** The acc holder pk. */
	private Long accHolderPk;

	/** The stock quantity. */
	private BigDecimal stockQuantity;
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	
	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the rejectMotiveOther to set
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the certificate pk.
	 *
	 * @return the certificatePk
	 */
	public Long getCertificatePk() {
		return certificatePk;
	}

	/**
	 * Sets the certificate pk.
	 *
	 * @param certificatePk the certificatePk to set
	 */
	public void setCertificatePk(Long certificatePk) {
		this.certificatePk = certificatePk;
	}

	/**
	 * Gets the int appilied.
	 *
	 * @return the intAppilied
	 */
	public Integer getIntAppilied() {
		return intAppilied;
	}

	/**
	 * Sets the int appilied.
	 *
	 * @param intAppilied the intAppilied to set
	 */
	public void setIntAppilied(Integer intAppilied) {
		this.intAppilied = intAppilied;
	}

	/**
	 * Gets the current update date.
	 *
	 * @return the currentUpdateDate
	 */
	public Date getCurrentUpdateDate() {
		return currentUpdateDate;
	}

	/**
	 * Sets the current update date.
	 *
	 * @param currentUpdateDate the currentUpdateDate to set
	 */
	public void setCurrentUpdateDate(Date currentUpdateDate) {
		this.currentUpdateDate = currentUpdateDate;
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public Integer getOperation() {
		return operation;
	}

	/**
	 * Sets the operation.
	 *
	 * @param operation the operation to set
	 */
	public void setOperation(Integer operation) {
		this.operation = operation;
	}

	/**
	 * Gets the id custody operation pk.
	 *
	 * @return the idCustodyOperationPk
	 */
	public Long getIdCustodyOperationPk() {
		return idCustodyOperationPk;
	}

	/**
	 * Sets the id custody operation pk.
	 *
	 * @param idCustodyOperationPk the idCustodyOperationPk to set
	 */
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}

	/**
	 * Gets the current user name.
	 *
	 * @return the currentUserName
	 */
	public String getCurrentUserName() {
		return currentUserName;
	}

	/**
	 * Sets the current user name.
	 *
	 * @param currentUserName the currentUserName to set
	 */
	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}

	/**
	 * Gets the physical balance detail pk.
	 *
	 * @return the physicalBalanceDetailPk
	 */
	public Long getPhysicalBalanceDetailPk() {
		return physicalBalanceDetailPk;
	}

	/**
	 * Sets the physical balance detail pk.
	 *
	 * @param physicalBalanceDetailPk the physicalBalanceDetailPk to set
	 */
	public void setPhysicalBalanceDetailPk(Long physicalBalanceDetailPk) {
		this.physicalBalanceDetailPk = physicalBalanceDetailPk;
	}

	/**
	 * Gets the cancel desc.
	 *
	 * @return the cancelDesc
	 */
	public String getCancelDesc() {
		return cancelDesc;
	}

	/**
	 * Sets the cancel desc.
	 *
	 * @param cancelDesc the cancelDesc to set
	 */
	public void setCancelDesc(String cancelDesc) {
		this.cancelDesc = cancelDesc;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the rejectMotive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the rejectMotive to set
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}


	
	

	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	
	/**
	 * Gets the reject motive s.
	 *
	 * @return the rejectMotiveS
	 */
	public Integer getRejectMotiveS() {
		return rejectMotiveS;
	}

	/**
	 * Sets the reject motive s.
	 *
	 * @param rejectMotiveS the rejectMotiveS to set
	 */
	public void setRejectMotiveS(Integer rejectMotiveS) {
		this.rejectMotiveS = rejectMotiveS;
	}

	

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	/**
	 * Gets the acc holder pk.
	 *
	 * @return the accHolderPk
	 */
	public Long getAccHolderPk() {
		return accHolderPk;
	}

	/**
	 * Sets the acc holder pk.
	 *
	 * @param accHolderPk the accHolderPk to set
	 */
	public void setAccHolderPk(Long accHolderPk) {
		this.accHolderPk = accHolderPk;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the rejectMotiveOther
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the old state.
	 *
	 * @return the old state
	 */
	public Integer getOldState() {
		return oldState;
	}

	/**
	 * Sets the old state.
	 *
	 * @param oldState the new old state
	 */
	public void setOldState(Integer oldState) {
		this.oldState = oldState;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the request state.
	 *
	 * @return the request state
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the new request state
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	/**
	 * Gets the stock quantity.
	 *
	 * @return the stock quantity
	 */
	public BigDecimal getStockQuantity() {
		return stockQuantity;
	}

	/**
	 * Sets the stock quantity.
	 *
	 * @param stockQuantity the new stock quantity
	 */
	public void setStockQuantity(BigDecimal stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	
}
