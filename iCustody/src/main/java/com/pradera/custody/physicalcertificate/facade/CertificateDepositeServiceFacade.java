package com.pradera.custody.physicalcertificate.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.dematerializationcertificate.service.DematerializationCertificateServiceBean;
import com.pradera.custody.physicalcertificate.service.CertificateDepositControlBean;
import com.pradera.custody.physicalcertificate.service.CertificateDepositServiceBean;
import com.pradera.custody.physicalcertificate.service.ReteirementDepositeServiceBean;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateAndDetailsResultTO;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.custody.to.XmlRespuestaActivoFinanciero;
import com.pradera.integration.component.custody.to.XmlSolicitudActivoFinanciero;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertFile;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateFile;
import com.pradera.model.custody.physical.PhysicalCertificateMovement;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.ReasonRetirementType;
import com.pradera.model.custody.type.ReasonType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.operations.RequestCorrelativeType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class CertificateDepositeBeanFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Nov 17, 2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class CertificateDepositeServiceFacade{
	
	
	/** The certificate deposit service bean. */
	@EJB
	CertificateDepositServiceBean certificateDepositServiceBean;
	
	/** The certificate location facade bean. */
	@EJB
	CertificateLocationServiceFacade certificateLocationFacadeBean;
	
	/** The certificate deposit control bean. */
	@EJB
	CertificateDepositControlBean certificateDepositControlBean;
	
	/** The reteirement deposite service. */
	@EJB
	private ReteirementDepositeServiceBean reteirementDepositeService;
	
	/** The crud dato service bean. */
	@EJB
	CrudDaoServiceBean crudDatoServiceBean;

	/** Holds current logger user details. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	@Inject
	private ClientRestService clientRestService;
	
	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/**
	 * issue 895: metodo para actualizar los datos de un titulo fisico cuando el valor y existe
	 */
	public void updatePhysicalCertificate(PhysicalCertificate pc){
		certificateDepositServiceBean.updatePhysicalCertificate(pc);
	}
	
	/**
	 * issue 895: obtener un valor con su emisor
	 * @param idSecurityCodePk
	 * @return
	 */
	public Security getSecurityWithIssuer(String idSecurityCodePk){
		return certificateDepositServiceBean.getSecurityWithIssuer(idSecurityCodePk);
	}
	
	/**
	 * issue 895: metodo para obtener los PhysicalCertFile(archivos) de un PhysicalCertificate(Titulo Fisico)
	 * @param idPhysicalCertificatePk
	 * @return
	 */
	public List<PhysicalCertFile> getLstPhysicalCertFileFromOnePhysicalCert(Long idPhysicalCertificatePk){
		return certificateDepositServiceBean.getLstPhysicalCertFileFromOnePhysicalCert(idPhysicalCertificatePk);
	}
	
	/**
	 * Used to get the participant list.
	 *
	 * @param state Integer
	 * @param accountClass the account class
	 * @return the list particicpant
	 * @throws ServiceException the service exception
	 * List<Participant>
	 */
	public List<Participant> getListParticicpant(Integer state,Integer accountClass)throws ServiceException{
		List<Participant> lstParticipants = certificateDepositServiceBean.getParticipantList(state, accountClass);
		List<Participant> returnLst = new ArrayList<Participant>();
		for(Participant participant : lstParticipants){
			//participant.setCompleteDescription(participant.getDescription() + " - " + participant.getIdParticipantPk().toString());
			returnLst.add(participant);
		}
		return returnLst;
	}
	
	/**
	 * Gets the list particicpant.
	 *
	 * @param lstState the lst state
	 * @param accountClass the account class
	 * @return the list particicpant
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getListParticicpant(List<Integer> lstState,Integer accountClass)throws ServiceException{
		List<Participant> lstParticipants = certificateDepositServiceBean.getParticipantList(lstState, accountClass);
		List<Participant> returnLst = new ArrayList<Participant>();
		for(Participant participant : lstParticipants){
			//participant.setCompleteDescription(participant.getDescription() + " - " + participant.getIdParticipantPk().toString());
			returnLst.add(participant);
		}
		return returnLst;
	}
	
	/**
	 * Used to get the participant list.
	 *
	 * @param participantPk the participant pk
	 * @param accountClass the account class
	 * @return the participant description
	 * @throws ServiceException the service exception
	 * List<Participant>
	 */
	public Participant getParticipantDescription(Long participantPk,Integer accountClass)throws ServiceException{
		return certificateDepositServiceBean.getParticipant(participantPk, accountClass);
	}
	
	/**
	 * Find participant.
	 *
	 * @param idSecurityCodePk the participant pk
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Security findSecurity(String idSecurityCodePk)throws ServiceException{
		return certificateDepositServiceBean.findSecurity(idSecurityCodePk);
	}
	
	public Participant findParticipant(Long participantPk)throws ServiceException{
		return certificateDepositServiceBean.findParticipant(participantPk);
	}

	public Participant findParticipant(String nemonico)throws ServiceException{
		return certificateDepositServiceBean.findParticipant(nemonico);
	}
	
	/**
	 * Find Holder
	 * @param idHolderPk
	 * @return
	 * @throws ServiceException
	 */
	public Holder findHolder(Long idHolderPk)throws ServiceException{
		return certificateDepositServiceBean.findHolder(idHolderPk);
	}
	
	/**
	 * Gets the holder account details.
	 *
	 * @param id the id
	 * @return the holder account details
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountDetails(Long id)throws ServiceException{
		return (HolderAccount)certificateDepositServiceBean.getHolderAccount(id);
	}
	
	/**
	 * Gets the physical operation current.
	 *
	 * @param id the id
	 * @return the physical operation current
	 * @throws ServiceException the service exception
	 */
	public PhysicalOperationDetail getPhysicalOperationCurrent(Long id)throws ServiceException{
		return (PhysicalOperationDetail)certificateDepositServiceBean.getPhysicalOperationDetail(id);
	}
	
	/**
	 * Used to get Holder Account Associated to Participant.
	 *
	 * @param accountNumber the account number
	 * @param participantFk the participant fk
	 * @return the holdet account
	 * @throws ServiceException the service exception
	 * HolderAccount
	 */
	public HolderAccount getHoldetAccount(Integer accountNumber,Long participantFk)throws ServiceException{
		return (HolderAccount)certificateDepositServiceBean.getHolderAccountDetails(accountNumber, participantFk);
	}
	
	/**
	 * Used to get Search List.
	 *
	 * @param physicalCertificateFilter the physical certificate filter
	 * @return List<PhysicalCertificate>
	 * @throws ServiceException the service exception
	 */
	
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_QUERY)
	public List<PhysicalCertificateExt> getPhysicalCertificateSearchList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return certificateDepositServiceBean.getPhysicalCertificateExtList(physicalCertificateFilter);
	}
	
	/**
	 * issue 895: metodo para obtener la Clave-Clase de Valor que no existe y fue registrada en el DEPOSITO DE TITULOS FISICOS
	 * @param idPhysicalCertificatePk
	 * @return
	 */
	public String getIdSecurityCodeNoExists(Long idPhysicalCertificatePk){
		return certificateDepositServiceBean.getIdSecurityCodeNoExists(idPhysicalCertificatePk);
	}
	
	/**
	 * Lst physical cert marketfacts.
	 *
	 * @param idPhysicalCertificatePk the id physical certificate pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertMarketfact> lstPhysicalCertMarketfacts(Long idPhysicalCertificatePk)throws ServiceException{
		return certificateDepositServiceBean.lstPhysicalCertMarketfacts(idPhysicalCertificatePk);
	}
	
	/**
	 * Gets the physical certificate list.
	 *
	 * @param physicalCertificateFilter the physical certificate filter
	 * @return the physical certificate list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificateAndDetailsResultTO> getPhysicalCertificateList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		List<PhysicalCertificateAndDetailsResultTO> physicalCertificateList = new ArrayList<PhysicalCertificateAndDetailsResultTO>();
		PhysicalCertificateAndDetailsResultTO resultTO;
		List<Object[]> objectResults = certificateDepositServiceBean.getPhysicalCertificateList(physicalCertificateFilter);
		for(Object[] movementObjet: objectResults){
			resultTO=new PhysicalCertificateAndDetailsResultTO();
			resultTO.setCui(certificateDepositServiceBean.getHolders(((BigDecimal)(movementObjet[0]==null?BigDecimal.ZERO:movementObjet[0]))));//cui
			resultTO.setAccountNumber(((BigDecimal)(movementObjet[1]==null?BigDecimal.ZERO:movementObjet[1])));
			resultTO.setConfirmDate((Date)movementObjet[2]);//CONFIRM_DATE
			resultTO.setAdmissiondate((Date)movementObjet[3]);//DEPOSIT_DATE
			resultTO.setSecurity(certificateDepositServiceBean.getSecurity((String)movementObjet[4]));//SECURITY_PK
			resultTO.setRegisterDate((Date)(movementObjet[5]));//fecha de registro
			resultTO.setTitleNumber(((BigDecimal)(movementObjet[6]==null?null:movementObjet[6])));
			resultTO.setDescSituasion(""+movementObjet[7]);//DESC
			resultTO.setCetificateQuantity(((BigDecimal)(movementObjet[8]==null?BigDecimal.ZERO:movementObjet[8])));//cantidad de certificados
			resultTO.setMarketFactDate((Date)(movementObjet[9]));//fecha de mercado
			resultTO.setMarketFactRate(((BigDecimal)(movementObjet[10]==null?BigDecimal.ZERO:movementObjet[10])));
			resultTO.setMarketFactPrice(((BigDecimal)(movementObjet[11]==null?BigDecimal.ZERO:movementObjet[11])));
			resultTO.setValuedAmount(((BigDecimal)(movementObjet[12]==null?BigDecimal.ZERO:movementObjet[12])));
			resultTO.setIdPhysicalCertificatePK(((BigDecimal)(movementObjet[13]==null?BigDecimal.ZERO:movementObjet[13])));
			resultTO.setPartDesc(""+movementObjet[14]);
			resultTO.setCuiDesc(certificateDepositServiceBean.getHoldersDesc(((BigDecimal)(movementObjet[0]==null?BigDecimal.ZERO:movementObjet[0]))));
			BigDecimal bigDecimal= (BigDecimal) movementObjet[13];
			resultTO.setCertificate(getPhysicalCertificateObject(bigDecimal.longValue()));
			physicalCertificateList.add(resultTO);
		}
		return physicalCertificateList;
	}
	

	/**
	 * The method for getting the security locations.
	 *
	 * @param id Long
	 * @return List of ParameterTable objects
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getSecurityLocationFacade(Long id)throws ServiceException{ 
			return certificateDepositServiceBean.getSecurityLocation(id);
		}
	
	/**
	 * Used to search whether the certificate exists or not
	 * with certificate number by same issuer.
	 *
	 * @param certNums the cert nums
	 * @param idSecurityCode the id security code
	 * @return Integer
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalBalanceDetail> getPhysicalCertificateExists(List<String> certNums,String idSecurityCode)throws ServiceException{
		List<PhysicalBalanceDetail> physicalCertificates = certificateDepositServiceBean.getPhysicalCertificate(certNums, idSecurityCode);
		return physicalCertificates;
	}
	
	/**
	 * Used to Save the PhysicalCertficateList.
	 *
	 * @param physicalCertificates List<PhysicalCertificate>
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REGISTER)
	public List<PhysicalCertificate> saveList(List<PhysicalCertificate> physicalCertificates)throws ServiceException{
		if(physicalCertificates.size() > 0){
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			return certificateDepositServiceBean.saveAllCertificates(physicalCertificates);
		}else
			return null;
	}
	
	/**
	 * Save physical certificate operation.
	 *
	 * @param physicalCertificateOperation the physical certificate operation
	 * @param physicalCertificateList the physical certificate list
	 * @return the physical certificate operation
	 * @throws ServiceException the service exception
	 */
	/*public PhysicalCertificateOperation savePhysicalCertificateOperation(PhysicalCertificateOperation physicalCertificateOperation, List<PhysicalCertificate> physicalCertificateList)throws ServiceException{
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			
			String certificateNumber="";

			certificateNumber=certificateDepositControlBean.exitsCertificate(physicalCertificateList);
			if(Validations.validateIsNotNullAndNotEmpty(certificateNumber)){
				Object[] params={certificateNumber};
				throw new ServiceException(ErrorServiceType.CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY,params); 
			}
			
			String idSecurityCodeAux=null;
			for(PhysicalCertificate physicalCertificate : physicalCertificateList){
				
				if(Validations.validateIsNotNullAndNotEmpty(physicalCertificate.getPhysicalCertFileList())){
				
					//seteando pistas de auditoria
					for(PhysicalCertFile physicalCertFile : physicalCertificate.getPhysicalCertFileList()){
						
						physicalCertFile.setRegistryUser(loggerUser.getUserName());
						physicalCertFile.setRegistryDate(loggerUser.getAuditTime());
						
						physicalCertFile.setLastModifyUser(loggerUser.getUserName());
						physicalCertFile.setLastModifyDate(loggerUser.getAuditTime());
						physicalCertFile.setLastModifyIp(loggerUser.getIpAddress());
						physicalCertFile.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
						
						//setenado foranea (PHYSICAL_CERTIFICATE)
						physicalCertFile.setIdPhysicalCertificateFk(physicalCertificate);
					}
				}
				
				if(physicalCertificate.getIndExistsSecurity().equals(BooleanType.NO.getCode())){
					idSecurityCodeAux = physicalCertificate.getSecurities().getIdSecurityCodePk();
					physicalCertificate.setSecurities(null);
					physicalCertificate.setIssuer(null);
				}
				physicalCertificate=certificateDepositServiceBean.createWithFlush(physicalCertificate);
				
				// issue 895: si el valor NO existe, entonce asignarlo manualmante en la tabla PHYSICAL_CERTIFICATE
				if(physicalCertificate.getIndExistsSecurity().equals(BooleanType.NO.getCode())){
					certificateDepositServiceBean.updatePhysicalCertificateColumnSecurity(physicalCertificate.getIdPhysicalCertificatePk(), idSecurityCodeAux);
				}
			}
			
			Long operationNumber = crudDatoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_PHYSICAL_SECURITIES_DEPOSIT);
			physicalCertificateOperation.getCustodyOperation().setOperationNumber(operationNumber);
			physicalCertificateOperation.setAudit(loggerUser);
			certificateDepositServiceBean.create(physicalCertificateOperation);

			return physicalCertificateOperation;
	}*/
	
	public PhysicalCertificateOperation savePhysicalCertificateOperation(PhysicalCertificateOperation physicalCertificateOperation, List<PhysicalCertificate> physicalCertificateList)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		String certificateNumber="";
		/**
		 * certificateDepositControlBean
		 */
		certificateNumber=certificateDepositControlBean.exitsCertificate(physicalCertificateList);
		if(Validations.validateIsNotNullAndNotEmpty(certificateNumber)){
			Object[] params={certificateNumber};
			throw new ServiceException(ErrorServiceType.CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY,params); 
		}
		
		for(PhysicalCertificate physicalCertificate : physicalCertificateList){
			certificateDepositServiceBean.create(physicalCertificate);
		}
		
		Long operationNumber = crudDatoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_PHYSICAL_SECURITIES_DEPOSIT);
		physicalCertificateOperation.getCustodyOperation().setOperationNumber(operationNumber);
		physicalCertificateOperation.setAudit(loggerUser);
		certificateDepositServiceBean.create(physicalCertificateOperation);
		/*
		for(PhysicalOperationDetail physicalOperationDetail : physicalCertificateOperation.getPhysicalOperationDetails()){
			physicalOperationDetail.setAudit(loggerUser);
			certificateDepositServiceBean.create(physicalOperationDetail, PhysicalOperationDetail.class);
		}
		*/
		return physicalCertificateOperation;
	}
	
	/**
	 * Update certificates.
	 *
	 * @param commonUpdateBeanInf the common update bean inf
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateCertificates(CommonUpdateBeanInf commonUpdateBeanInf, LoggerUser loggerUser)throws ServiceException{
		certificateDepositServiceBean.updateCertificates(commonUpdateBeanInf,loggerUser);
	}
	/**
	 * Used to update the physical cerificate List.
	 *
	 * @param physicalOperationDetails the physical operation details
	 * @return the list
	 * Integer
	 */
	public List<PhysicalOperationDetail> savePhysicalOperationDetal(List<PhysicalOperationDetail> physicalOperationDetails){
		if(physicalOperationDetails.size() > 0){
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
			return (List<PhysicalOperationDetail>)certificateDepositServiceBean.saveAll(physicalOperationDetails);
		}else
			return null;
	}
	
	/**
	 * Used to Save the physicalBalanceDetails.
	 *
	 * @param physicalBalanceDetails List<PhysicalBalanceDetail>
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalBalanceDetail> savePhysicalBalanceList(List<PhysicalBalanceDetail> physicalBalanceDetails)throws ServiceException{
		if(physicalBalanceDetails.size() > 0){
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			for (PhysicalBalanceDetail physicalBalanceDetail : physicalBalanceDetails) {			
				//physicalBalanceDetailExists = certificateDepositServiceBean.getPhysicalBalanceDetails(physicalBalanceDetail);
				if (Validations.validateIsNull(physicalBalanceDetail.getIdPhysicalBalanceDetailPk())) {
					certificateDepositServiceBean.savePhysicalBalanceDetail(physicalBalanceDetail);
				} else {
					certificateDepositServiceBean.updatePhysicalBalanceDetail(physicalBalanceDetail);
				}
			}
			return physicalBalanceDetails;
		}else{
			return null;
		}
	}
	
	
	/**
	 * Used to Save the PhysicalCertificateMovement.
	 *
	 * @param certificateMovements List<PhysicalCertificateMovement>
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificateMovement> saveCertificateMovementList(List<PhysicalCertificateMovement> certificateMovements)throws ServiceException{
		if(certificateMovements.size() > 0){
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			return certificateDepositServiceBean.saveAllPhysicalCertificateMovements(certificateMovements);
		}
		else
			return null;
	}
	
	
	/**
	 * Used to Update the PhysicalCertficateList.
	 *
	 * @param state the state
	 * @return the physical certificate list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificate> getPhysicalCertificateList(Long state)throws ServiceException{
		return certificateDepositServiceBean.getPhysicalCertificates(state);
	}
	
	/**
	 * Update detail approve deposit.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	//Deposito de titulos 
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_APPROVE)
	public void updateDetailApproveDeposit(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		updateDetail(lstCommonUpdateBeanInf);
		
	}
	
	/**
	 * Update detail cancel deposit.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_CANCEL)
	public void updateDetailCancelDeposit(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
		updateDetail(lstCommonUpdateBeanInf);
		
	}

	/**
	 * Update detail reject deposit.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REJECT)
	public void updateDetailRejectDeposit(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		updateDetail(lstCommonUpdateBeanInf);
		
	}

	/**
	 * Certificate confirmation.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @param certificateMovements the certificate movements
	 * @param physicalBalanceDetails the physical balance details
	 * @param physicalCertificates the physical certificates
	 * @param operationType the operation type
	 * @throws ServiceException the service exception
	 */
	public void certificateConfirmation(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf,
										List<PhysicalCertificateMovement> certificateMovements,
										List<PhysicalBalanceDetail> physicalBalanceDetails,
										List<PhysicalCertificate> physicalCertificates,Integer operationType
										)throws ServiceException{

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		updateDetailConfirmDeposit(lstCommonUpdateBeanInf);
		saveCertificateMovementList(certificateMovements);
		savePhysicalBalanceList(physicalBalanceDetails);
		automaticLocationUpdate(physicalCertificates,operationType);
		updateSecurityPhysicalBalance(physicalCertificates);
	}
	
	/**
	 * Update security physical balance.
	 *
	 * @param physicalCertificates the physical certificates
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityPhysicalBalance(List<PhysicalCertificate> physicalCertificates)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		for(PhysicalCertificate physicalCertificate : physicalCertificates)
		{
			Security security=physicalCertificate.getSecurities();
			security = certificateDepositServiceBean.find(Security.class,security.getIdSecurityCodePk());
			BigDecimal bdPhysicalDepositAmount=security.getPhysicalDepositBalance();
			if(bdPhysicalDepositAmount == null){
				bdPhysicalDepositAmount = BigDecimal.ZERO;
			}
			security.setPhysicalDepositBalance(new BigDecimal(bdPhysicalDepositAmount.intValue()+
					physicalCertificate.getCertificateQuantity().intValue()));
			certificateDepositServiceBean.update(security,loggerUser);
		}
	}
	
	/**
	 * Update detail confirm deposit.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_CONFIRM)
	public void updateDetailConfirmDeposit(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		updateDetail(lstCommonUpdateBeanInf);
		
	}

	/**
	 * Update detail apply deposit.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_APPLY)
	public void updateDetailApplyDeposit(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		updateDetail(lstCommonUpdateBeanInf);
		
	}
	
	//Retiro de titulos
	
	/**
	 * Update detail approve retirement.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_APPROVE)
	public void updateDetailApproveRetirement(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		updateDetail(lstCommonUpdateBeanInf);
		
	}
	
	/**
	 * Update detail cancel retirement.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CANCEL)
	public void updateDetailCancelRetirement(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		updateDetail(lstCommonUpdateBeanInf);
		
	}

	/**
	 * Update detail confirm retirement.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CONFIRM)
	public void updateDetailConfirmRetirement(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		updateDetail(lstCommonUpdateBeanInf);
		
	}

	/**
	 * Update detail apply retirement.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_APPLY)
	public void updateDetailApplyRetirement(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		updateDetail(lstCommonUpdateBeanInf);
		
	}

	/**
	 * Update detail autorized retirement.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_AUTHORIZED)
	public void updateDetailAutorizedRetirement(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());
		updateDetail(lstCommonUpdateBeanInf);
		
	}
	
	
	/**
	 * Update detail.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @throws ServiceException the service exception
	 */
	public void updateDetail(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf)throws ServiceException{
		CommonUpdateBeanInf commonUpdateBeanInf = lstCommonUpdateBeanInf.get(0);
		
		if(commonUpdateBeanInf != null){
			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			if(loggerUser.getUserAction().isApprove() && commonUpdateBeanInf.getOperation().equals(GeneralConstants.APPROVE_OPERATION_CONSTANT)){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
			}else if(loggerUser.getUserAction().isAnnular() && (commonUpdateBeanInf.getOperation().equals(GeneralConstants.CANCEL_OPERATION_CONSTANT) ||
					commonUpdateBeanInf.getOperation().equals(GeneralConstants.CANCEL_DEPOSIT_OIPERATION))){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
			}else if(loggerUser.getUserAction().isConfirm() && commonUpdateBeanInf.getOperation().equals(GeneralConstants.CONFIRM_OPERATION_CONSTANT)){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
			}else if(loggerUser.getUserAction().isReject() && commonUpdateBeanInf.getOperation().equals(GeneralConstants.REJECT_OPERATION_CONSTANT)){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			}else if(loggerUser.getUserAction().isApply() && commonUpdateBeanInf.getOperation().equals(GeneralConstants.APPLY_OPERATION_CONSTANT)){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
			}else if(loggerUser.getUserAction().isAuthorize() && commonUpdateBeanInf.getOperation().equals(GeneralConstants.AUTHORIZE_OPERATION_CONSTANT)){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());
			}
			
			for(CommonUpdateBeanInf commonBeanInf: lstCommonUpdateBeanInf){
				certificateDepositServiceBean.updateCustodyOperation(commonBeanInf,loggerUser);
				certificateDepositServiceBean.updatePhysicalOperationDetail(commonBeanInf,loggerUser);
			}
			
			List<PhysicalOperationDetail> physicalOperationDetails=certificateDepositServiceBean.getPhysicalOperationDetailsForEachRequest(commonUpdateBeanInf.getIdCustodyOperationPk());
			//debio encontrar registros
			List<PhysicalOperationDetail> physicalOperationDetailCompare = new ArrayList<PhysicalOperationDetail>();
			
			for(PhysicalOperationDetail detail: physicalOperationDetails){
				for(CommonUpdateBeanInf commonBeanInf: lstCommonUpdateBeanInf){
					if( detail.getPhysicalCertificate().getIdPhysicalCertificatePk().equals(commonBeanInf.getCertificatePk()) ){
						detail.setState(commonBeanInf.getState());
					}
				}
				physicalOperationDetailCompare.add(detail);
			}
			
			
			Integer cantidadCertificados = physicalOperationDetailCompare.size();
			Integer nuevoEstadoCertificado = StateType.INPROCESSED_CERTIFICATE.getCode();//EN TRAMITE
			Integer cantidadProcesados = 0;
			
			if( commonUpdateBeanInf.getOperationType().equals(new Long(RequestType.DEPOSITEO.getCode())) ){
				
				//Actualizar certificado de titulos fisicos
				for(CommonUpdateBeanInf commonBeanInf: lstCommonUpdateBeanInf){
					
					if( commonBeanInf.getState().equals(StateType.CONFIRMED.getCode())  && commonBeanInf.getIntAppilied().equals(0) ){
						
						commonBeanInf.setSituation(SituationType.DEPOSITARY_OPERATIONS.getCode());
						updateCertificates(commonBeanInf,loggerUser);
						
					}else if( commonBeanInf.getState().equals(StateType.CONFIRMED.getCode()) && commonBeanInf.getIntAppilied().equals(1)  ){
						
						commonBeanInf.setSituation(SituationType.DEPOSITARY_VAULT.getCode());
						updateCertificates(commonBeanInf,loggerUser);	
					}
				}
				
				for(PhysicalOperationDetail detail: physicalOperationDetailCompare){
					if( 
					   (detail.getState().equals(StateType.CONFIRMED.getCode()) && detail.getIndApplied().equals(1) ) ||
						detail.getState().equals(StateType.ANNULLED.getCode()) ||
						detail.getState().equals(StateType.REJECTED.getCode())
					){
						cantidadProcesados++;
					}
				}
				
			}else{	//RETIRO
				
				//Actualizar certificado de titulos fisicos
				for(CommonUpdateBeanInf commonBeanInf: lstCommonUpdateBeanInf){
					/*
					if( commonBeanInf.getState().equals(StateType.CONFIRMED.getCode())  && commonBeanInf.getIntAppilied().equals(0) ){
						
						commonBeanInf.setSituation(SituationType.DEPOSITARY_VAULT.getCode());
						updateCertificates(commonBeanInf,loggerUser);
						
					}*/
					if( commonBeanInf.getState().equals(StateType.CONFIRMED.getCode())  && commonBeanInf.getIntAppilied().equals(1) ){
						
						commonBeanInf.setSituation(SituationType.DEPOSITARY_OPERATIONS.getCode());
						updateCertificates(commonBeanInf,loggerUser);
						
					}
					else if( commonBeanInf.getState().equals(StateType.AUTHORIZED.getCode()) ){
						
						commonBeanInf.setSituation(SituationType.PARTICIPANT.getCode());
						updateCertificates(commonBeanInf,loggerUser);
					}
					
				}
				
				for(PhysicalOperationDetail detail: physicalOperationDetailCompare){
					if( detail.getState().equals(StateType.AUTHORIZED.getCode()) ||
						detail.getState().equals(StateType.ANNULLED.getCode())
					){
						cantidadProcesados++;
					}
				}
					
			}
			
			if(cantidadProcesados.equals((cantidadCertificados))){
				nuevoEstadoCertificado = new Integer(StateType.CONFIRMED_CERTIFICATE.getCode()); //PROCESADO
			}
			cantidadProcesados = 0;
			cantidadCertificados = 0;
			commonUpdateBeanInf.setState(nuevoEstadoCertificado);
			certificateDepositServiceBean.updatePhysicalCertificationOperation(commonUpdateBeanInf,physicalOperationDetailCompare,loggerUser);
		}
	}
	
	/**
	 * Used to update Situation type.
	 *
	 * @param physicalCertificates List<PhysicalCertificate>
	 * @param operationType the operation type
	 * @throws ServiceException the service exception
	 */
	public void automaticLocationUpdate(List<PhysicalCertificate> physicalCertificates,Integer operationType)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		certificateLocationFacadeBean.automaticLocationUpdate(physicalCertificates,operationType,loggerUser);
	}
	
	

	/**
	 * Certificate action application.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @param physicalCertificates the physical certificates
	 * @param operationType the operation type
	 * @throws ServiceException the service exception
	 */
	public void certificateActionApplication(List<CommonUpdateBeanInf> lstCommonUpdateBeanInf,
											 List<PhysicalCertificate> physicalCertificates,Integer operationType)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		
		updateDetailApplyDeposit(lstCommonUpdateBeanInf);
		automaticLocationUpdate(physicalCertificates,operationType);
	}
	/**
	 * It gives security class details.
	 *
	 * @param instrumentType the Integer
	 * @param masterfk the Integer
	 * @return the  List<ParameterTable>
	 * @throws ServiceException the service exception
	 */	
	public List<ParameterTable> securityClassFacade(Integer instrumentType,Integer masterfk)throws ServiceException {	
		return certificateDepositServiceBean.securityClassService(instrumentType,masterfk);
	}
	
	/**
	 * Used to get Issuer name by state.
	 *
	 * @param state the state
	 * @return List<Issuer>
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getIssuerList(Integer state)throws ServiceException{
		return certificateDepositServiceBean.getIssuers(state);
	}
	
	/**
	 * Used to save PhysiscalCertificateOperation.
	 *
	 * @param physicalCertificateOperation the physical certificate operation
	 * @return the physical certificate operation
	 * @throws ServiceException the service exception
	 * PhysicalCertificateOperation
	 */
	public PhysicalCertificateOperation save(PhysicalCertificateOperation physicalCertificateOperation)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		Long operationNumber = crudDatoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_PHYSICAL_SECURITIES_DEPOSIT);
		physicalCertificateOperation.getCustodyOperation().setOperationNumber(operationNumber);
		return certificateDepositServiceBean.savePhysicalCertificateOperation(physicalCertificateOperation);
	}
	
	/**
	 * Exists certificate in bd.
	 *
	 * @param physicalCertificate the physical certificate
	 * @return true, if successful
	 */
	public boolean existsCertificateInBd(PhysicalCertificate physicalCertificate){
		AccountAnnotationOperation operation=certificateDepositServiceBean.getPhysicalOperation(physicalCertificate.getCertificateNumber(), physicalCertificate.getIndExistsSecurity().equals(BooleanType.YES.getCode()) ? physicalCertificate.getSecurities().getIdSecurityCodePk() : physicalCertificate.getIdSecurityCodePk() );
		if(Validations.validateIsNotNullAndNotEmpty(operation))
			return true;
		else 
			return false;
	}
	
	/**
	 * Used to save CustodyOperation.
	 *
	 * @param custodyOperation the custody operation
	 * @return the custody operation
	 * @throws ServiceException the service exception
	 * CustodyOperation
	 */
	public CustodyOperation saveCustody(CustodyOperation custodyOperation)throws ServiceException{
		return certificateDepositServiceBean.saveCustodyOperation(custodyOperation);
	}
	
	
	/**
	 * Used to get Physical certificate Operation.
	 *
	 * @param idCustodyOperationPk Long
	 * @return the physical certificate operation
	 * @throws ServiceException the service exception
	 * PhysicalCertificateOperation
	 */
	public PhysicalCertificateOperation findById(Long idCustodyOperationPk)throws ServiceException{
		return certificateDepositServiceBean.getPhysicalCertificationOperation(idCustodyOperationPk);
	}
	
	
	/**
	 * Used to get certificate which is in Retired State.
	 *
	 * @param certificateNumber Long
	 * @param issuerCode String
	 * @return the certificate
	 * @throws ServiceException the service exception
	 * PhysicalCertificate
	 */
	public PhysicalCertificate getCertificate(Long certificateNumber,String issuerCode)throws ServiceException{
		return certificateDepositServiceBean.getCertificateRetired(certificateNumber, issuerCode);
	}
	
	/**
	 * Used to get operation Detail which is in Approved or Registered State.
	 *
	 * @param certificateNumber Long
	 * @param idSecurityCode the id security code
	 * @return the operationdetail
	 * @throws ServiceException the service exception
	 * PhysicalOperationDetail
	 */
	public List<PhysicalOperationDetail> getOperationdetail(Long certificateNumber,String idSecurityCode)throws ServiceException{
		return certificateDepositServiceBean.getCertificateOperationDetail(certificateNumber, idSecurityCode);
	}
	
	/**
	 * Gets the physical balance detail.
	 *
	 * @param certificatePK the certificate pk
	 * @return the physical balance detail
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalBalanceDetail> getPhysicalBalanceDetail(Long certificatePK)throws ServiceException{
		return  certificateDepositServiceBean.getPhysicalBalanceDetail(certificatePK);
	}
	
	/**
	 * Used to get operation Detail retirement which is in Approved or Registered State.
	 *
	 * @param certificateNumber Long
	 * @param idSecurityCode the id security code
	 * @return the operationdetail
	 * @throws ServiceException the service exception
	 * PhysicalOperationDetail
	 */
	public List<PhysicalOperationDetail> getRetirementCertificate(Long certificateNumber, String idSecurityCode)throws ServiceException{
		return certificateDepositServiceBean.getRetirementCertificate(certificateNumber, idSecurityCode);
	}
	
	/**
	 * Used to get Holiday.
	 *
	 * @param currentDate Date
	 * @param state Integer
	 * @param geograpicalLoc Integer
	 * @return the holiday list by state
	 * @throws ServiceException the service exception
	 * Holiday
	 */
	public Holiday getHolidayListByState(Date currentDate,Integer state,Integer geograpicalLoc)throws ServiceException{
		return certificateDepositServiceBean.getHolidayList(currentDate, state, geograpicalLoc);
	}

	/**
	 * Gets the issuer object.
	 *
	 * @param issuerFK the issuer fk
	 * @return the issuer object
	 */
	public Issuer getIssuerObject(String issuerFK){
		return certificateDepositServiceBean.find(issuerFK, Issuer.class);
	}

	public AccountAnnotationOperation findAccountAnnotationOperation(AccountAnnotationOperation accountAnnotationOperation) {
		return certificateDepositServiceBean.findAccountAnnotationOperation(accountAnnotationOperation);
	}

	public AccountAnnotationOperation findAccountAnnotationOperationAcciones(AccountAnnotationOperation accountAnnotationOperation) {
		return certificateDepositServiceBean.findAccountAnnotationOperationAcciones(accountAnnotationOperation);
	}

	public List<AccountAnnotationOperation> lstAccountAnnotationOperationAccionesAllFromTo(AccountAnnotationOperation accountAnnotationOperation) {
		return certificateDepositServiceBean.lstAccountAnnotationOperationAccionesAllFromTo(accountAnnotationOperation);
	}
	
	/**
	 * Gets the physical certificate object.
	 *
	 * @param idPhysicalCertificatePk the id physical certificate pk
	 * @return the physical certificate object
	 */
	public PhysicalCertificate getPhysicalCertificateObject(Long idPhysicalCertificatePk){
		return certificateDepositServiceBean.find(idPhysicalCertificatePk, PhysicalCertificate.class);
	}
	
	/**
	 * Update security.
	 *
	 * @param objSecurity the obj security
	 * @throws ServiceException the service exception
	 */
	public void updateSecurity(Security objSecurity)throws ServiceException{
		certificateDepositServiceBean.update(objSecurity);
	}
	
	/**
	 * Gets the issuerby security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuerby security
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuerbySecurity(String idSecurityCodePk)throws ServiceException{
		return certificateDepositServiceBean.getIssuerbySecurity(idSecurityCodePk);
	}
	
	/**
	 * Gets the physical certificate by security.
	 *
	 * @param securityFk the security fk
	 * @return the physical certificate by security
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificate> getPhysicalCertificateBySecurity(String securityFk)throws ServiceException{
		List<PhysicalCertificate> physicalCertificates = certificateDepositServiceBean.getPhysicalCertificateBySecurity(securityFk);
		return physicalCertificates;
	}
	
	/**
	 * Gets the physical cert marketfact.
	 *
	 * @param idPhysicalCertificate the id physical certificate
	 * @return the physical cert marketfact
	 */
	public PhysicalCertMarketfact getPhysicalCertMarketfact(Long idPhysicalCertificate)	{
		return certificateDepositServiceBean.getPhysicalCertMarketfact(idPhysicalCertificate);
	}
	
	/**
	 * Gets the list account annotation operation by certificate.
	 *
	 * @param certificateNumber the certificate number
	 * @param idSecurityCode the id security code
	 * @return the list account annotation operation by certificate
	 */
	public List<AccountAnnotationOperation> getListAccountAnnotationOperationByCertificate(Long certificateNumber, String idSecurityCode)
	{
		return certificateDepositServiceBean.getListAccountAnnotationOperationByCertificate(certificateNumber, idSecurityCode);
	}
	
	/** issue 188 HM custodia fisica y desmaterializacion**/
	public HolderMarketFactBalance getHolderMarketFacts( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) throws ServiceException 
	{
		return certificateDepositServiceBean.getHolderMarketFact(idSecurityCodePk, idParticipantPk, idHolderAccountPk);
	}
	
	/**
	 * Cancela o Anula las solicitudes de deposito de titulos fisicos
	 * Dependiendo el estado en que se encuentre,
	 * Finalmente mueve los saldos.
	 *
	 * @param finalDate the final date
	 * @param lngOperationType the operation type
	 * @throws ServiceException the service exception
	 */
	
	public void automaticCertificateCancelations( Date finalDate, Long lngOperationType ) throws ServiceException {		
		PhysicalCertificateFilter physicalCertificateFilter = new PhysicalCertificateFilter();
		physicalCertificateFilter.setInitialDate(finalDate);
		List<Integer> statesPhysical = new ArrayList<Integer>();
		statesPhysical.add(PhysicalCertificateStateType.REGISTERED.getCode());
		statesPhysical.add(PhysicalCertificateStateType.APPROVED.getCode());
		List<PhysicalCertificateExt> lstPhysicalCertificateExt = certificateDepositServiceBean.getPhysicalCertificateReversion(physicalCertificateFilter, statesPhysical);
		if(lstPhysicalCertificateExt != null && lstPhysicalCertificateExt.size() > 0){
			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			for(PhysicalCertificateExt objPhysicalCertificateExt : lstPhysicalCertificateExt){
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setOperationType(lngOperationType);
				if (objPhysicalCertificateExt.getState().equals(PhysicalCertificateStateType.APPROVED.getCode())) {
					commonUpdateBeanInf.setState(PhysicalCertificateStateType.REJECTED.getCode());
					commonUpdateBeanInf.setOperation(GeneralConstants.REJECT_OPERATION_CONSTANT);
				} else if(objPhysicalCertificateExt.getState().equals(PhysicalCertificateStateType.REGISTERED.getCode())){
					commonUpdateBeanInf.setState(PhysicalCertificateStateType.ANNULED.getCode());
					commonUpdateBeanInf.setOperation(GeneralConstants.CANCEL_DEPOSIT_OIPERATION);														
				}
				commonUpdateBeanInf.setIdCustodyOperationPk(objPhysicalCertificateExt.getRequestNumber());
				commonUpdateBeanInf.setCertificatePk(objPhysicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setRejectMotiveS(ReasonType.OTRO.getCode());
				commonUpdateBeanInf.setRejectMotiveOther("REVERSION NOCTURNA");
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
			}
			updateDetail(lstCommonUpdateBeanInf);
		}		
	}
	
	/**
	 * Cancela o Anula las solicitudes de retiro de titulos fisicos
	 * Dependiendo el estado en que se encuentre,
	 * Finalmente mueve los saldos.
	 *
	 * @param finalDate the final date
	 * @param lngOperationType the operation type
	 * @throws ServiceException the service exception
	 */
	
	public void automaticCertificateRetirementCancelations( Date finalDate, Long lngOperationType ) throws ServiceException {	
		int operationCancel = 2;
		PhysicalCertificateFilter physicalCertificateFilter = new PhysicalCertificateFilter();
		physicalCertificateFilter.setInitialDate(finalDate);
		List<Integer> statesPhysical = new ArrayList<Integer>();
		statesPhysical.add(PhysicalCertificateStateType.REGISTERED.getCode());
		statesPhysical.add(PhysicalCertificateStateType.APPROVED.getCode());
	//	statesPhysical.add(PhysicalCertificateStateType.CONFIRMED.getCode());
		List<PhysicalCertificateExt> lstPhysicalCertificateExt = reteirementDepositeService.getPhysicalCertificateRetirementReversion(physicalCertificateFilter, statesPhysical);
		if(lstPhysicalCertificateExt != null && lstPhysicalCertificateExt.size() > 0){
			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			for(PhysicalCertificateExt objPhysicalCertificateExt : lstPhysicalCertificateExt){
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setOperationType(lngOperationType);
	//			if ((objPhysicalCertificateExt.getState().equals(PhysicalCertificateStateType.APPROVED.getCode()))|| 
	//					(objPhysicalCertificateExt.getState().equals(PhysicalCertificateStateType.CONFIRMED.getCode()))   ) {
				if (objPhysicalCertificateExt.getState().equals(PhysicalCertificateStateType.APPROVED.getCode())) {
					commonUpdateBeanInf.setLastModifyApp(new Integer(1));
					commonUpdateBeanInf.setState(StateType.REJECTED.getCode());
					commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
					commonUpdateBeanInf.setIdCustodyOperationPk(objPhysicalCertificateExt.getIdCustodyOperationPk());
					commonUpdateBeanInf.setCertificatePk(objPhysicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());					
					commonUpdateBeanInf.setCancelDesc("REVERSION NOCTURNA");					
					commonUpdateBeanInf.setRejectMotive(ReasonRetirementType.OTRO.getCode());
					commonUpdateBeanInf.setOperation(operationCancel);
					commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
					lstCommonUpdateBeanInf.add(commonUpdateBeanInf);					
				} else if(objPhysicalCertificateExt.getState().equals(PhysicalCertificateStateType.REGISTERED.getCode())){					
					commonUpdateBeanInf.setLastModifyApp(new Integer(1));
					commonUpdateBeanInf.setState(StateType.ANNULLED.getCode());
					commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
					commonUpdateBeanInf.setIdCustodyOperationPk(objPhysicalCertificateExt.getIdCustodyOperationPk());
					commonUpdateBeanInf.setCertificatePk(objPhysicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());					
					commonUpdateBeanInf.setCancelDesc("REVERSION NOCTURNA");					
					commonUpdateBeanInf.setRejectMotive(ReasonRetirementType.OTRO.getCode());
					commonUpdateBeanInf.setOperation(operationCancel);
					commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
					lstCommonUpdateBeanInf.add(commonUpdateBeanInf);					
				}				
			}
			updateDetail(lstCommonUpdateBeanInf);
		}		
	}
	
	/**
	 * Find holder accounts.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the list
	 */
	@LoggerAuditWeb
	public HolderAccount findHolderAccount(Long idHolderAccountPk){
		List<HolderAccount> holderAccountsSelected = new ArrayList<HolderAccount>();
		HolderAccount objHolderAccount = null;
		HolderTO holderTO = new HolderTO();
		try{
			if(idHolderAccountPk != null){
				holderTO.setHolderAccountId(idHolderAccountPk);				
			}			
			holderTO.setFlagAllHolders(true);			
			List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);			
			if(lstAccountTo != null && lstAccountTo.size() > 0){
				objHolderAccount = new HolderAccount();
				holderAccountsSelected = new ArrayList<HolderAccount>();
				HolderAccount ha;
				for(HolderAccountHelperResultTO hahrTO : lstAccountTo){
					ha = new HolderAccount();							
					ha.setAccountNumber(hahrTO.getAccountNumber());
					ha.setAlternateCode(hahrTO.getAlternateCode());
					ha.setAccountGroup(hahrTO.getAccountGroup());
					ha.setIdHolderAccountPk(hahrTO.getAccountPk());
					ha.setHolderDescriptionList(hahrTO.getHolderDescriptionList());
					ha.setStateAccountDescription(hahrTO.getAccountStatusDescription());
					holderAccountsSelected.add(ha);
				}
				objHolderAccount = holderAccountsSelected.get(0);
			}
			
		} catch(Exception e){
			return null;
		}
		return objHolderAccount;
	} 
	
	/**
	 * metodo para registrar en la BBDD un AccAccountAnnotationFile
	 * @param accAnnotationOperationFile
	 * @return si retorna null es porque hubo ocurrio un error
	 */
	public PhysicalCertificateFile registryPhysicalCertificateFile(PhysicalCertificateFile physicalCertificateFile) {
		LoggerUser loggerUser = (LoggerUser) transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		physicalCertificateFile.setRegistryUser(loggerUser.getUserName());
		physicalCertificateFile.setRegistryDate(CommonsUtilities.currentDateTime());
		physicalCertificateFile.setRegistryIp(loggerUser.getIpAddress());

		// validando si se ha realizado la carga de un archivo adjunto
		if (Validations.validateIsNotNull(physicalCertificateFile.getAttachedName())) {
			// pistas de auditoria para archivo adjunto
			physicalCertificateFile.setRegistryUserAtt(loggerUser.getUserName());
			physicalCertificateFile.setRegistryDateAtt(CommonsUtilities.currentDateTime());
			physicalCertificateFile.setRegistryIpAtt(loggerUser.getIpAddress());
		}

		PhysicalCertificateFile result = null;
		result = certificateDepositServiceBean.registryPhysicalCertificateFile(physicalCertificateFile);
		return result;
	}
	
	/**
	 * Certificate action application.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @param physicalCertificates the physical certificates
	 * @param operationType the operation type
	 * @throws ServiceException the service exception
	 */
	public XmlRespuestaActivoFinanciero sendDepositeRecord(PhysicalCertificateExt[] physicalCertificates)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		
		XmlSolicitudActivoFinanciero xmlSolicitudActivoFinanciero;
		XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero;
		ParameterTable parameterTable = reteirementDepositeService.find(ParameterTable.class, 2561);
		xmlSolicitudActivoFinanciero = reteirementDepositeService.getSolicitudDepositoActivoFinanciero(physicalCertificates, loggerUser);
		xmlRespuestaActivoFinanciero = clientRestService.envioSolicitud(xmlSolicitudActivoFinanciero,parameterTable.getDescription());
		
		return xmlRespuestaActivoFinanciero;
	}
	
	/**
	 * Certificate action application.
	 *
	 * @param lstCommonUpdateBeanInf the lst common update bean inf
	 * @param physicalCertificates the physical certificates
	 * @param operationType the operation type
	 * @throws ServiceException the service exception
	 */
	public XmlRespuestaActivoFinanciero sendReteirementRecord(PhysicalCertificateExt[] physicalCertificatesExt)throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApply());
		
		XmlSolicitudActivoFinanciero xmlSolicitudActivoFinanciero;
		XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero;
		ParameterTable parameterTable = reteirementDepositeService.find(ParameterTable.class, 2561);
		xmlSolicitudActivoFinanciero = reteirementDepositeService.getSolicitudRetiroActivoFinanciero(physicalCertificatesExt, loggerUser);
		xmlRespuestaActivoFinanciero = clientRestService.envioSolicitud(xmlSolicitudActivoFinanciero,parameterTable.getDescription());
		
		return xmlRespuestaActivoFinanciero;
	}

	public Issuer findIssuer(String issuerPk)throws ServiceException{
		return certificateDepositServiceBean.getIssuer(issuerPk);
	}

	public Issuer findIssuerWithNemonic(String issuerPk)throws ServiceException{
		return certificateDepositServiceBean.getIssuerWithNemonic(issuerPk);
	}
	
	public List<HolderAccountBank> findHolderAccountBank(Long holderAccount, Integer stateAccountBank) {
		return certificateDepositServiceBean.findHolderAccountBank(holderAccount, stateAccountBank);
	}
	
	public List<InstitutionBankAccount> findInstitutionBankAccount(Long idParticipantPk, Integer state){
		return certificateDepositServiceBean.findInstitutionBank(idParticipantPk, state);
	}
}