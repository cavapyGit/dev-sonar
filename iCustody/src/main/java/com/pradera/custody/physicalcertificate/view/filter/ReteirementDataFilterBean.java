package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ReteirementDataFilterBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class ReteirementDataFilterBean implements Serializable {

	/** added default serial version uid. */
	private static final long serialVersionUID = 1L;
	
	/** The desc date. */
	private Date descDate;	
	
	/** The has date. */
	private Date hasDate;
	
	/** The acc holder pk. */
	private Long accHolderPk;
	
	/** The id participanr pk. */
	private Long idParticipanrPk;
	
	/** The state. */
	private Integer state;
	
	/** The document number. */
	private Long documentNumber;
	
	/** The issue fk. */
	private String issueFk;

	/** The issue fk. */
	private String idSecurityCode;
	
	/** The acc number. */
	private Integer accNumber;
	
	/** The holder pk. */
	private Long idHolderPk;
	
	/** The deliver date. */
	private Date deliverDate;
	
	/** The instrumenttype. */
	private Integer instrumenttype;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The user name. */
	private String userName;
	
	/** The ip address. */
	private String ipAddress;
	
	/** The issuer pk search. */
	private String issuerPKSearch;
	
	/** The save data. */
	private List<ReteirementDepositeFilterBean> saveData;
	
	/**
	 * Gets the desc date.
	 *
	 * @return the descDate
	 */
	public Date getDescDate() {
		return descDate;
	}
	
	/**
	 * Gets the instrumenttype.
	 *
	 * @return the instrumenttype
	 */
	public Integer getInstrumenttype() {
		return instrumenttype;
	}
	
	/**
	 * Sets the instrumenttype.
	 *
	 * @param instrumenttype the instrumenttype to set
	 */
	public void setInstrumenttype(Integer instrumenttype) {
		this.instrumenttype = instrumenttype;
	}
	
	/**
	 * Gets the security class.
	 *
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}
	
	/**
	 * Sets the security class.
	 *
	 * @param securityClass the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
	/**
	 * Sets the desc date.
	 *
	 * @param descDate the descDate to set
	 */
	public void setDescDate(Date descDate) {
		this.descDate = descDate;
	}
	
	/**
	 * Gets the checks for date.
	 *
	 * @return the hasDate
	 */
	public Date getHasDate() {
		return hasDate;
	}
	
	/**
	 * Sets the checks for date.
	 *
	 * @param hasDate the hasDate to set
	 */
	public void setHasDate(Date hasDate) {
		this.hasDate = hasDate;
	}
	
	/**
	 * Gets the acc holder pk.
	 *
	 * @return the accHolderPk
	 */
	public Long getAccHolderPk() {
		return accHolderPk;
	}
	
	/**
	 * Sets the acc holder pk.
	 *
	 * @param accHolderPk the accHolderPk to set
	 */
	public void setAccHolderPk(Long accHolderPk) {
		this.accHolderPk = accHolderPk;
	}
	
	/**
	 * Gets the id participanr pk.
	 *
	 * @return the idParticipanrPk
	 */
	public Long getIdParticipanrPk() {
		return idParticipanrPk;
	}
	
	/**
	 * Sets the id participanr pk.
	 *
	 * @param idParticipanrPk the idParticipanrPk to set
	 */
	public void setIdParticipanrPk(Long idParticipanrPk) {
		this.idParticipanrPk = idParticipanrPk;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the document number.
	 *
	 * @return the documentNumber
	 */
	public Long getDocumentNumber() {
		return documentNumber;
	}
	
	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the documentNumber to set
	 */
	public void setDocumentNumber(Long documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	/**
	 * Gets the issue fk.
	 *
	 * @return the issueFk
	 */
	public String getIssueFk() {
		return issueFk;
	}
	
	/**
	 * Sets the issue fk.
	 *
	 * @param issueFk the issueFk to set
	 */
	public void setIssueFk(String issueFk) {
		this.issueFk = issueFk;
	}
	
	/**
	 * Gets the acc number.
	 *
	 * @return the accNumber
	 */
	public Integer getAccNumber() {
		return accNumber;
	}
	
	/**
	 * Sets the acc number.
	 *
	 * @param accNumber the accNumber to set
	 */
	public void setAccNumber(Integer accNumber) {
		this.accNumber = accNumber;
	}
	
	/**
	 * Gets the deliver date.
	 *
	 * @return the deliverDate
	 */
	public Date getDeliverDate() {
		return deliverDate;
	}
	
	/**
	 * Sets the deliver date.
	 *
	 * @param deliverDate the deliverDate to set
	 */
	public void setDeliverDate(Date deliverDate) {
		this.deliverDate = deliverDate;
	}
	
	/**
	 * Gets the request number.
	 *
	 * @return the requestNumber
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}
	
	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	/**
	 * Gets the user name.
	 *
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Sets the user name.
	 *
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * Gets the ip address.
	 *
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}
	
	/**
	 * Sets the ip address.
	 *
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * Gets the save data.
	 *
	 * @return the saveData
	 */
	public List<ReteirementDepositeFilterBean> getSaveData() {
		return saveData;
	}
	
	/**
	 * Sets the save data.
	 *
	 * @param saveData the saveData to set
	 */
	public void setSaveData(List<ReteirementDepositeFilterBean> saveData) {
		this.saveData = saveData;
	}
	
	/**
	 * Gets the issuer pk search.
	 *
	 * @return the issuer pk search
	 */
	public String getIssuerPKSearch() {
		return issuerPKSearch;
	}
	
	/**
	 * Sets the issuer pk search.
	 *
	 * @param issuerPKSearch the new issuer pk search
	 */
	public void setIssuerPKSearch(String issuerPKSearch) {
		this.issuerPKSearch = issuerPKSearch;
	}

	/**
	 * Gets the id security code.
	 *
	 * @return the id security code
	 */
	public String getIdSecurityCode() {
		return idSecurityCode;
	}

	/**
	 * Sets the id security code.
	 *
	 * @param idSecurityCode the new id security code
	 */
	public void setIdSecurityCode(String idSecurityCode) {
		this.idSecurityCode = idSecurityCode;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
}