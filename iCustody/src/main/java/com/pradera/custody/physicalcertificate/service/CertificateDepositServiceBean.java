package com.pradera.custody.physicalcertificate.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertFile;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateFile;
import com.pradera.model.custody.physical.PhysicalCertificateMovement;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.PhysicalCertificateOperationType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.ReasonType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.SecurityType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class CertificateDepositServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Sep 26, 2013
 */
@Stateless
public class CertificateDepositServiceBean extends CrudDaoServiceBean{
	
	/** The max results query. */
	@Inject
	@Configurable
	private Integer maxResultsQuery;
	
	/**
	 * issue 895: metodo para actualizar los datos de un titulo fisico cuando el valor y existe
	 */
	public void updatePhysicalCertificate(PhysicalCertificate physicalCertificate){
		em.merge(physicalCertificate);
		em.flush();
	}
	
	/**
	 * issue 895: obtener un valor con su emisor
	 * @param idSecurityCodePk
	 * @return
	 */
	public Security getSecurityWithIssuer(String idSecurityCodePk){
		List<Security> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" select s");
		sd.append(" from Security s");
		sd.append(" inner join fetch s.issuer i");
		sd.append(" where s.idSecurityCodePk = :idSecurityCodePk");
		try {
			Query q=em.createQuery(sd.toString());
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			lst= q.getResultList();
			if(lst.isEmpty())
				return null;
			else return lst.get(0); 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error en el metodo getSecurityWithIssuer.getSecurityWithIssuer "+e.getMessage());
			return null;
		}
	}
	
	/**
	 * issue 895: metodo para obtener los PhysicalCertFile(archivos) de un PhysicalCertificate(Titulo Fisico)
	 * @param idPhysicalCertificatePk
	 * @return
	 */
	public List<PhysicalCertFile> getLstPhysicalCertFileFromOnePhysicalCert(Long idPhysicalCertificatePk){
		List<PhysicalCertFile> lst=new ArrayList<>();
		StringBuilder sd=new StringBuilder();
		sd.append(" select fc");
		sd.append(" from PhysicalCertFile fc");
		sd.append(" where 0=0");
		sd.append(" and fc.idPhysicalCertificateFk.idPhysicalCertificatePk = :idPhysicalCertificatePk");
		sd.append(" order by fc.idCertFilePk asc");
		try {
			Query q=em.createQuery(sd.toString());
			q.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
			lst= q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error en el metodo CertificateDepositServiceBean.getLstPhysicalCertFileFromOnePhysicalCert "+e.getMessage());
		}
		return lst;
	}
	
	/**
	 * issue 895: metodo para actualizar manualmente el ID_SECURITY_CODE_FK de la tabla PHYSICAL_CERTIFICATE
	 * <br/>
	 * Este metodo se ejecuta unicamente cuando el valor NO existe.
	 * @param idPhysicalCertificatePk
	 * @param idSecurityCodePk
	 */
	public void updatePhysicalCertificateColumnSecurity(Long idPhysicalCertificatePk, String idSecurityCodePk){
		StringBuilder sd=new StringBuilder();
		sd.append(" update PHYSICAL_CERTIFICATE");
		sd.append(" set ID_SECURITY_CODE_FK = :idSecurityCodePk");
		sd.append(" where ID_PHYSICAL_CERTIFICATE_PK = :idPhysicalCertificatePk");
		try {
			Query q=em.createNativeQuery(sd.toString());
			q.setParameter("idSecurityCodePk", idSecurityCodePk);
			q.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
			q.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: Se ha producido un error ene el metodo: CertificateDepositServiceBean.updatePhysicalCertificateColumnSecurity "+e.getMessage());
		}
	}
	
	/**
	 * The method is used to get participant list.
	 *
	 * @param state Integer
	 * @param accountClass Integer
	 * @return List<Participant>
	 * List
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantList(Integer state,Integer accountClass)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("state", state);
		parameters.put("accountClass", accountClass);
		return (List<Participant>)findWithNamedQuery(Participant.PARTICIPANT,parameters);
	}
	
	/**
	 * Gets the participant list.
	 *
	 * @param lstState the lst state
	 * @param accountClass the account class
	 * @return the participant list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getParticipantList(List<Integer> lstState,Integer accountClass)throws ServiceException{
		String strQuery = "Select distinct p From Participant p WHERE p.state in :state and p.accountClass = :accountClass order by p.mnemonic asc";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("state", lstState);
		parameters.put("accountClass", accountClass);
		return (List<Participant>)findListByQueryString(strQuery,parameters);
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param participantPk the participant pk
	 * @param accountClass the account class
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipant(Long participantPk,Integer accountClass)throws ServiceException{
		String sql = "Select p from Participant p where p.idParticipantPk = :idParticipantPk and p.accountClass = :accountClass and p.state = :state";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idParticipantPk", participantPk);
		parameters.put("state", ParticipantStateType.REGISTERED.getCode());
		parameters.put("accountClass",accountClass);
		return (Participant)findObjectByQueryString(sql, parameters);
	}
	
	/**
	 * Find participant.
	 *
	 * @param participantPk the participant pk
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant findParticipant(Long participantPk)throws ServiceException{
		String sql = "Select p from Participant p where p.idParticipantPk = :idParticipantPk";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idParticipantPk", participantPk);
		return (Participant)findObjectByQueryString(sql, parameters);
	}
	

	public Participant findParticipant(String mnemonic)throws ServiceException{
		String sql = "Select p from Participant p where p.mnemonic = :mnemonic ";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("mnemonic", mnemonic);
		return (Participant)findObjectByQueryString(sql, parameters);
	}
	

	public Security findSecurity(String idSecurityCodePk)throws ServiceException{
		String sql = "SELECT s FROM Security s JOIN FETCH s.issuer i WHERE s.idSecurityCodePk = :idSecurityCodePk ";
		
		TypedQuery<Security> query = this.em.createQuery(sql, Security.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}	
	/**
	 * Find holder
	 * @param idHolderPk
	 * @return
	 * @throws ServiceException
	 */
	public Holder findHolder(Long idHolderPk)throws ServiceException{
		String sql = "Select h from Holder h where h.idHolderPk = :idHolderPk";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderPk", idHolderPk);
		return (Holder)findObjectByQueryString(sql, parameters);
	}
	
	/**
	 * The method is used to get HolderAccount Associated to the Particular Participant.
	 *
	 * @param id Long
	 * @return the holder account
	 * @throws ServiceException the service exception
	 * HolderAccount
	 */
	public HolderAccount getHolderAccount(Long id)throws ServiceException{
		return (HolderAccount)find(HolderAccount.class,id);
	}
	
	/**
	 * The method is used to get PhysicalOperationDetail Object based on primary key.
	 *
	 * @param id Long
	 * @return the physical operation detail
	 * @throws ServiceException the service exception
	 * PhysicalOperationDetail
	 */
	public PhysicalOperationDetail getPhysicalOperationDetail(Long id)throws ServiceException{
		return (PhysicalOperationDetail)find(PhysicalOperationDetail.class, id);
	}
	
	
	/**
	 * The method is used to get holder account details based on account number and participant.
	 *
	 * @param accountNumber Integer
	 * @param participantFk Long
	 * @return the holder account details
	 * @throws ServiceException the service exception
	 * HolderAccount
	 */
	public HolderAccount getHolderAccountDetails(Integer accountNumber,Long participantFk)throws ServiceException{
		String query = "select ha,h.fullName from Holder h,HolderAccount ha,HolderAccountDetail had where had.holderAccount.idHolderAccountPk = ha.idHolderAccountPk and had.holder.idHolderPk = h.idHolderPk and ha.accountNumber = :accountNumber and ha.participant.idParticipantPk = :participantPk";
		List<Object[]> objects = new ArrayList<Object[]>();
		HolderAccount holderAccount = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("accountNumber", accountNumber);
		parameters.put("participantPk", participantFk);
		objects = findListByQueryString(query, parameters);
		if(objects.size() > 0){
			holderAccount = new HolderAccount();
			for (Object[] object : objects) {
				holderAccount = (HolderAccount)object[0];
				holderAccount.setFullName((String)object[1]);
			}
			
		}
		return holderAccount;
	}
	
	/**
	 * Gets the holders.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holders
	 * @throws ServiceException the service exception
	 */
	public String getHolders(BigDecimal idHolderAccountPk)throws ServiceException {
		String query = "select had.holder.idHolderPk from HolderAccountDetail had where had.holderAccount.idHolderAccountPk = :idHolderAccountPk"; 
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk", idHolderAccountPk.longValue());
		try {
			List<Long> list=(List<Long>)findListByQueryString(query, parameters);
			String holders="";
			for (Long long1 : list) {
				holders=holders+"* "+long1.toString()+"<br/>";
			}
			return holders;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets the holders desc.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holders desc
	 * @throws ServiceException the service exception
	 */
	public String getHoldersDesc(BigDecimal idHolderAccountPk)throws ServiceException {
		String query = "select had.holder.fullName from HolderAccountDetail had where had.holderAccount.idHolderAccountPk = :idHolderAccountPk"; 
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountPk", idHolderAccountPk.longValue());
		try {
			List<String> list=(List<String>)findListByQueryString(query, parameters);
			String holders="";
			for (String string : list) {
				holders=holders+" "+string+" <br/> ";
			}
			return holders;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * issue 895: metodo para obtener la Clave-Clase de Valor que no existe y fue registrada en el DEPOSITO DE TITULOS FISICOS
	 * @param idPhysicalCertificatePk
	 * @return
	 */
	public String getIdSecurityCodeNoExists(Long idPhysicalCertificatePk){
		StringBuilder sd=new StringBuilder();
		sd.append(" select PC.ID_SECURITY_CODE_FK from PHYSICAL_CERTIFICATE PC");
		sd.append(" where 0=0");
		sd.append(" and PC.ID_PHYSICAL_CERTIFICATE_PK = :idPhysicalCertificatePk");
		try {
			Query q=em.createNativeQuery(sd.toString());
			q.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
			return (String) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return "---";
		}
	}
	/**
	 * The method is used to get PhysicalCertificateList.
	 *
	 * @param physicalCertificateFilter PhysicalCertificateFilter
	 * @return 	List<PhysicalCertificateExt>
	 * @throws ServiceException the service exception
	 */
	/*public List<PhysicalCertificateExt> getPhysicalCertificateExtList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		List<Object[]> list = new ArrayList<Object[]>();
		List<PhysicalCertificateExt> physicalCertificateExts = new ArrayList<PhysicalCertificateExt>();
		StringBuffer query = 
					new StringBuffer("Select pc," +
											"pco.participant," +
											"pco.custodyOperation.idCustodyOperationPk,"+
											"pod.state," +
											"pod.idPhysicalOperationDetail," +
											"pod," +
											"pco.state, " +
											"pco.custodyOperation.operationNumber, "+
											"pco.custodyOperation.registryDate, "+
											"pco "+
									 "from "+
										  " PhysicalCertificate pc," +
									 	  " PhysicalOperationDetail  pod," +
									 	  " PhysicalCertificateOperation pco " );
									 	 if( Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getCuiHolder()) ){
												query.append(", HolderAccount ho,HolderAccountDetail had,Holder h ");
											}
									 query.append( "where "+
									 "pco.physicalOperationType = :physicalOperationType and " +
									 "pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk and " +
									 "pc.idPhysicalCertificatePk = pod.physicalCertificate.idPhysicalCertificatePk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("physicalOperationType", new Long(RequestType.DEPOSITEO.getCode()));
		if(physicalCertificateFilter.getCertificateNumber() != null){
			query.append(" and pc.certificateNumber = :certificateNumber");
			parameters.put("certificateNumber", physicalCertificateFilter.getCertificateNumber().toString());
		}
		if(physicalCertificateFilter.getRequestNumber() != null){
			query.append(" and pco.custodyOperation.operationNumber = :requestNumber and pod.physicalCertificateOperation.custodyOperation.operationNumber = :requestNumber");
			parameters.put("requestNumber", physicalCertificateFilter.getRequestNumber());
		}else { 
			query.append(" and pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk");
		}
		query.append(" and TRUNC(pod.registryDate) >= TRUNC(:initialDate) and TRUNC(pod.registryDate) <= TRUNC(:finalDate) ");
		if(physicalCertificateFilter.getStatus() != null){
			query.append(" and pod.state = :state");
			parameters.put("state", physicalCertificateFilter.getStatus());
		}
		if(Validations.validateIsNotNull(physicalCertificateFilter.getSecurity())){
			if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getSecurity().getSecurityClass()))
			{
				query.append(" and pc.securities.securityClass = :securityClass");
				parameters.put("securityClass", physicalCertificateFilter.getSecurity().getSecurityClass());
			}
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getSecurity().getIdSecurityCodePk()))
			{
				query.append(" and pc.securities.idSecurityCodePk = :securityCode");
				parameters.put("securityCode", physicalCertificateFilter.getSecurity().getIdSecurityCodePk());
			}
		}
		if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getHolderAccountPk())){
			query.append(" and pco.holderAccount.idHolderAccountPk = :idHolderAccountFk");
			parameters.put("idHolderAccountFk", physicalCertificateFilter.getHolderAccountPk());
		}
		
		if( Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getCuiHolder()) ){
			query.append(" and pco.holderAccount.idHolderAccountPk=ho.idHolderAccountPk"
				  	+ " and ho.idHolderAccountPk=had.holderAccount.idHolderAccountPk"
				  	+ " and had.holder.idHolderPk=h.idHolderPk "
				  	+ " and h.idHolderPk=:idHolderFkParam");
			parameters.put("idHolderFkParam", physicalCertificateFilter.getCuiHolder());
		}
		
		if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getParticipantPk())){
			query.append(" and pco.participant.idParticipantPk = :idParticipantFk");
			parameters.put("idParticipantFk", physicalCertificateFilter.getParticipantPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getIssuerPk())){
			query.append(" and pc.issuer.idIssuerPk = :issuerPk");
			parameters.put("issuerPk", physicalCertificateFilter.getIssuerPk());
		}
		query.append(" ORDER BY pco.custodyOperation.operationNumber desc, pc.certificateNumber asc ");
		parameters.put("initialDate", physicalCertificateFilter.getInitialDate());
		parameters.put("finalDate", physicalCertificateFilter.getFinalDate());
		list = findListByQueryString(query.toString(), parameters);
		
		if(list != null){
			PhysicalCertificateExt physicalCertificateExt = null;
			for (Object[] object : list) {
				physicalCertificateExt = new PhysicalCertificateExt();
				physicalCertificateExt.setPhysicalCertificate((PhysicalCertificate)object[0]);
				physicalCertificateExt.setParticipantPk(((Participant)object[1]).getIdParticipantPk());
				physicalCertificateExt.setMnemonicParticipant(((Participant)object[1]).getMnemonic());
				physicalCertificateExt.setRequestNumber((Long)object[2]);
				physicalCertificateExt.setState((Integer)object[3]);
				physicalCertificateExt.setIdPhysicalOperationDetail((Long)object[4]);
				physicalCertificateExt.setPhysicalOperationDetail((PhysicalOperationDetail)object[5]);
				physicalCertificateExt.setRequestState((Integer)object[6]);
				physicalCertificateExt.setOperationNumber((Long)object[7]);
				physicalCertificateExt.setRegistryDate((Date)object[8]);
				physicalCertificateExt.setHolderAccount(((PhysicalCertificateOperation)object[9]).getHolderAccount());
				physicalCertificateExt.setCui(extractCUI(((PhysicalCertificateOperation)object[9]).getHolderAccount().getAlternateCode()));
				physicalCertificateExts.add(physicalCertificateExt);
			}
		}
		
		 return physicalCertificateExts;
	}*/
	public List<PhysicalCertificateExt> getPhysicalCertificateExtList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		List<Object[]> list = new ArrayList<Object[]>();
		List<PhysicalCertificateExt> physicalCertificateExts = new ArrayList<PhysicalCertificateExt>();
		StringBuffer query = 
					new StringBuffer("Select pc," +
											"pco.participant," +
											"pco.custodyOperation.idCustodyOperationPk,"+
											"pod.state," +
											"pod.idPhysicalOperationDetail," +
											"pod," +
											"pco.state, " +
											"pco.custodyOperation.operationNumber, "+
											"pco.custodyOperation.registryDate, "+
											"pco "+
									 "from "+
										  " PhysicalCertificate pc," +
									 	  " PhysicalOperationDetail  pod," +
									 	  " PhysicalCertificateOperation pco " +
									 "where "+
									 "pco.physicalOperationType = :physicalOperationType and " +
									 "pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk and " +
									 "pc.idPhysicalCertificatePk = pod.physicalCertificate.idPhysicalCertificatePk");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("physicalOperationType", new Long(RequestType.DEPOSITEO.getCode()));
		if(physicalCertificateFilter.getCertificateNumber() != null){
			query.append(" and pc.certificateNumber = :certificateNumber");
			parameters.put("certificateNumber", physicalCertificateFilter.getCertificateNumber());
		}
		if(physicalCertificateFilter.getRequestNumber() != null){
			query.append(" and pco.custodyOperation.operationNumber = :requestNumber and pod.physicalCertificateOperation.custodyOperation.operationNumber = :requestNumber");
			parameters.put("requestNumber", physicalCertificateFilter.getRequestNumber());
		}else 
			query.append(" and pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk");
		query.append(" and TRUNC(pod.registryDate) >= TRUNC(:initialDate) and TRUNC(pod.registryDate) <= TRUNC(:finalDate) ");
		if(physicalCertificateFilter.getStatus() != null){
			query.append(" and pod.state = :state");
			parameters.put("state", physicalCertificateFilter.getStatus());
		}
		if(Validations.validateIsNotNull(physicalCertificateFilter.getSecurity())){
			if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getSecurity().getSecurityClass()))
			{
				query.append(" and pc.securities.securityClass = :securityClass");
				parameters.put("securityClass", physicalCertificateFilter.getSecurity().getSecurityClass());
			}
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getSecurity().getIdSecurityCodePk()))
			{
				query.append(" and pc.securities.idSecurityCodePk = :securityCode");
				parameters.put("securityCode", physicalCertificateFilter.getSecurity().getIdSecurityCodePk());
			}
		}
		if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getHolderAccountPk())){
			query.append(" and pco.holderAccount.idHolderAccountPk = :idHolderAccountFk");
			parameters.put("idHolderAccountFk", physicalCertificateFilter.getHolderAccountPk());
		}
		if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getParticipantPk())){
			query.append(" and pco.participant.idParticipantPk = :idParticipantFk");
			parameters.put("idParticipantFk", physicalCertificateFilter.getParticipantPk());
		}
		query.append(" ORDER BY pco.custodyOperation.operationNumber desc, pc.certificateNumber asc ");
		parameters.put("initialDate", physicalCertificateFilter.getInitialDate());
		parameters.put("finalDate", physicalCertificateFilter.getFinalDate());
		list = findListByQueryString(query.toString(), parameters);
		if(list != null){
			PhysicalCertificateExt physicalCertificateExt = null;
			for (Object[] object : list) {
				physicalCertificateExt = new PhysicalCertificateExt();
				physicalCertificateExt.setPhysicalCertificate((PhysicalCertificate)object[0]);
				physicalCertificateExt.setParticipantPk(((Participant)object[1]).getIdParticipantPk());
				physicalCertificateExt.setRequestNumber((Long)object[2]);
				physicalCertificateExt.setState((Integer)object[3]);
				physicalCertificateExt.setIdPhysicalOperationDetail((Long)object[4]);
				physicalCertificateExt.setPhysicalOperationDetail((PhysicalOperationDetail)object[5]);
				physicalCertificateExt.setRequestState((Integer)object[6]);
				physicalCertificateExt.setOperationNumber((Long)object[7]);
				physicalCertificateExt.setRegistryDate((Date)object[8]);
				physicalCertificateExt.setHolderAccount(((PhysicalCertificateOperation)object[9]).getHolderAccount());
				physicalCertificateExts.add(physicalCertificateExt);
			}
		}
		
		 return physicalCertificateExts;
	}
	
	/** metodo que obtiene la lista de titulares a partir del codigo alterno */
	public String extractCUI(String alternativeCode) {
		if(alternativeCode==null)
			return null;
		int ini = alternativeCode.indexOf("(");
		int fin = alternativeCode.indexOf(")");
		String string = alternativeCode.substring(ini + 1, fin);
		String holders[] = string.split(",");
		List<Long> longs = new ArrayList<Long>();
		StringBuilder holdersResult=new StringBuilder();
		boolean firstLoop=true;
		for (int i = 0; i < holders.length; i++){
			if(!firstLoop)
				holdersResult.append(",");	
			holdersResult.append(holders[i]);
		}
					
		return holdersResult.toString();
	}
	
	/**
	 * Lst physical cert marketfacts.
	 *
	 * @param idPhysicalCertificatePk the id physical certificate pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertMarketfact> lstPhysicalCertMarketfacts(Long idPhysicalCertificatePk)throws ServiceException{
		
		String query = "Select pcm from PhysicalCertMarketfact pcm "
					 + " where pcm.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk " 
					 + " and pcm.indActive = :indActive ";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idPhysicalCertificatePk", idPhysicalCertificatePk);
		parameters.put("indActive", GeneralConstants.ONE_VALUE_INTEGER);
		
		List<PhysicalCertMarketfact> lstPysicalCertMarketfact = new ArrayList<PhysicalCertMarketfact>();
		PhysicalCertMarketfact certificate = new PhysicalCertMarketfact();
		List<Object> objects = new ArrayList<Object>();
		objects = findListByQueryString(query, parameters);
		if(objects.size() > 0){
			for (Object object : objects) {
				certificate = (PhysicalCertMarketfact)object;
				if(Validations.validateIsNull(certificate.getMarketRate()))
						certificate.setMarketRate(GeneralConstants.ZERO);
				lstPysicalCertMarketfact.add(certificate);
			}
		}
		
		return lstPysicalCertMarketfact;
	}
	
	/**
	 * Gets the physical certificate list.
	 *
	 * @param physicalCertificateFilter the physical certificate filter
	 * @return the physical certificate list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getPhysicalCertificateList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(""
				+ " SELECT HA.ID_HOLDER_ACCOUNT_PK CUI,"//0
				+ " HA.ACCOUNT_NUMBER as account_number,"//1
				+ " CO.CONFIRM_DATE CONFIRM_DATE,"//2
				+ " PBD.DEPOSIT_DATE DEPOSIT_DATE,"//3
				+ " PC.ID_SECURITY_CODE_FK AS SECURITY_PK,"//4
				+ " PC.REGISTER_DATE as REGISTER_DATE,"////5
				+ " PC.CERTIFICATE_NUMBER as CERTIFICATE_NUMBER ,"//6
				+ " (SELECT PT1.PARAMETER_NAME  FROM PARAMETER_TABLE PT1  WHERE PT1.parameter_table_pk=PC.SITUATION ) AS STUATION_DESC,"//7
				+ " PC.CERTIFICATE_QUANTITY CERTIFICATE_QUANTITY,"//8
				+ " PBD.REGISTER_DATE MARKETFACT_DATE,"//9
				+ " S.INTEREST_RATE MARKETFACT_RATE,"//10
				+ " PBD.NOMINAL_VALUE MARKETFACT_PRICE,"//11
				+ " (PC.CERTIFICATE_QUANTITY * S.CURRENT_NOMINAL_VALUE) VALUED_AMOUNT,"//12
				+ " PC.ID_PHYSICAL_CERTIFICATE_PK ID_PHYSICAL_CERTIFICATE_PK,"//13
				+ " (select PA.MNEMONIC||  ' - ' ||PA.ID_PARTICIPANT_PK from PARTICIPANT PA where PA.ID_PARTICIPANT_PK=PCO.ID_PARTICIPANT_FK)PART_DESC"//14
				+ " FROM PHYSICAL_CERTIFICATE PC"
				+ " JOIN PHYSICAL_OPERATION_DETAIL POD ON PC.ID_PHYSICAL_CERTIFICATE_PK=POD.ID_PHYSICAL_CERTIFICATE_FK"
				+ " JOIN PHYSICAL_CERTIFICATE_OPERATION PCO ON POD.ID_PHYSICAL_OPERATION_FK=PCO.ID_PHYSICAL_OPERATION_PK"
				+ " JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK=PCO.ID_HOLDER_ACCOUNT_FK"
				+ " JOIN ISSUER ISS ON ISS.ID_ISSUER_PK = PC.ID_ISSUER_FK"
				+ " JOIN CUSTODY_OPERATION CO ON CO.ID_CUSTODY_OPERATION_PK=PCO.ID_PHYSICAL_OPERATION_PK"
				+ " JOIN PHYSICAL_BALANCE_DETAIL PBD ON PBD.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK"
				+ " JOIN SECURITY SE ON SE.ID_SECURITY_CODE_PK=PC.ID_SECURITY_CODE_FK");
				//+ " JOIN PHYSICAL_CERT_MARKETFACT PCM ON PCM.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK");
		
				query.append(" WHERE ");
				query.append(" TRUNC(PC.REGISTER_DATE) >= :initialDate  ");
				query.append(" AND  TRUNC(PC.REGISTER_DATE) <= :finalDate ");
				query.append(" AND  POD.STATE in(598,601,602) ");//StateType
				query.append(" AND  PC.SITUATION in(608, 609) ");//Situation
				query.append(" AND  PCM.IND_ACTIVE = 1 ");//Activo
				parameters.put("initialDate", physicalCertificateFilter.getInitialDate());
				parameters.put("finalDate", physicalCertificateFilter.getFinalDate());
				//participante
				if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getParticipantPk())){
					query.append(" AND PCO.ID_PARTICIPANT_FK=:idParticipantPk");
					parameters.put("idParticipantPk", physicalCertificateFilter.getParticipantPk());
				}
				//numero de cuenta
				if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getHolderAccountPk())){
					query.append(" AND PCO.ID_HOLDER_ACCOUNT_FK = :idHolderAccountPk");
					parameters.put("idHolderAccountPk", physicalCertificateFilter.getHolderAccountPk());
				}
				//clase-clave de valor
				if(Validations.validateIsNotNull(physicalCertificateFilter.getSecurity()) && 
						Validations.validateIsNotNull(physicalCertificateFilter.getSecurity().getIdSecurityCodePk())){								
					query.append(" AND SE.ID_SECURITY_CODE_PK =:securityClassCode  ");
					parameters.put("securityClassCode",physicalCertificateFilter.getSecurity().getIdSecurityCodePk());
				}
				//emisor
				if(Validations.validateIsNotNull(physicalCertificateFilter.getIssuerPk())){								
					query.append(" AND PC.ID_ISSUER_FK=:idIssuerPk  ");
					parameters.put("idIssuerPk", physicalCertificateFilter.getIssuerPk());
				}	//numero de certifiacado
				if(Validations.validateIsNotNull(physicalCertificateFilter.getCertificateNumber())){
					query.append(" AND PC.CERTIFICATE_NUMBER = :certificateNumber");
					parameters.put("certificateNumber", physicalCertificateFilter.getCertificateNumber());
				}//clase de valor
				if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getSecurity()))
					if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getSecurity().getSecurityClass())){
						query.append(" AND PC.SECURITY_CLASS  = :idSecurtyClass");
						parameters.put("idSecurtyClass", physicalCertificateFilter.getSecurity().getSecurityClass());
					}//situacion de titulo fisico
				if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getCertificateLocation())){
					query.append(" AND PC.SITUATION  = :idSituation");
					parameters.put("idSituation", physicalCertificateFilter.getCertificateLocation());
				}//moneda
				if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getCurrency())){
					query.append(" AND PC.CURRENCY  = :idCurrency");
					parameters.put("idCurrency",physicalCertificateFilter.getCurrency());
				}
		query.append(" ORDER BY PC.CERTIFICATE_NUMBER ");
		
		return findByNativeQuery(query.toString(), parameters);
	}

	/**
	 * The method is used to get PhysicalCertificate.
	 *
	 * @param certNums the cert nums
	 * @param idSecurityCode the id security code
	 * @return 	PhysicalCertificate
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalBalanceDetail> getPhysicalCertificate(List<String> certNums,String idSecurityCode)throws ServiceException{
		
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT PBD FROM PhysicalBalanceDetail PBD ");
		stringBuffer.append(" WHERE PBD.physicalCertificate.certificateNumber in (:certificateNumberList) ");
		stringBuffer.append(" and PBD.physicalCertificate.securities.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" and PBD.state = :depositedState ");
		stringBuffer.append(" ORDER BY PBD.physicalCertificate.certificateNumber asc ");
		
		TypedQuery<PhysicalBalanceDetail> typedQuery= em.createQuery(stringBuffer.toString(),PhysicalBalanceDetail.class);
		typedQuery.setParameter("certificateNumberList", certNums);
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		typedQuery.setParameter("depositedState", StateType.DEPOSITED.getCode());
		
		return typedQuery.getResultList();
	}
	
	/**
	 * The emethod is used to get Certificate which is existed in PhysicalOperation Detail in Retire State.
	 *
	 * @param certificateNumber Long
	 * @param issuerCode the issuer code
	 * @return the certificate retired
	 * @throws ServiceException the service exception
	 * PhysicalCertificate
	 */
	public PhysicalCertificate getCertificateRetired(Long certificateNumber,String issuerCode)throws ServiceException{
		List<Object[]> list = new ArrayList<Object[]>();
		PhysicalCertificate physicalCertificate = null;
		String query = "Select physicalCertificate,physicalBalanceDetail.state from PhysicalCertificate physicalCertificate,PhysicalBalanceDetail physicalBalanceDetail where physicalCertificate.certificateNumber = :certificateNumber and physicalCertificate.issuer.idIssuerPk = :idIssuerPk "+
						"and physicalBalanceDetail.physicalCertificate.idPhysicalCertificatePk = physicalCertificate.idPhysicalCertificatePk and physicalBalanceDetail.state!=606";//ESTADO RETIRADO
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("certificateNumber", certificateNumber);
		parameters.put("idIssuerPk", issuerCode);
		list = findListByQueryString(query, parameters);
		if(list.size() > 0){
			for (Object[] objects : list) {
				physicalCertificate = (PhysicalCertificate)objects[0];
				physicalCertificate.setState((Integer)objects[1]);
			}
		}
		return physicalCertificate;
	}
	
	/**
	 * The method is used to get Operation detail in Approved or Registered.
	 *
	 * @param certificateNumber Long
	 * @param idSecurityCode the id security code
	 * @return the certificate operation detail
	 * @throws ServiceException the service exception
	 * PhysicalOperationDetail
	 */
	public List<PhysicalOperationDetail> getCertificateOperationDetail(Long certificateNumber,String idSecurityCode)throws ServiceException{		
		List<Integer> stateList = new ArrayList<Integer>();
		stateList.add(StateType.REGISTERED.getCode());
		stateList.add(StateType.APPROVED.getCode());
		stateList.add(StateType.CONFIRMED.getCode());
		String query = " Select pod from PhysicalOperationDetail pod " 
					 + " join fetch pod.physicalCertificate pc "
					 + " where pc.certificateNumber = :certificateNumber "
					 + " and pc.securities.idSecurityCodePk = :idSecurityCode "
					 + " and pod.state in (:stateList) ";
		TypedQuery<PhysicalOperationDetail> typedQueryList= em.createQuery(query.toString(), PhysicalOperationDetail.class);		
		typedQueryList.setParameter("certificateNumber", certificateNumber);
		typedQueryList.setParameter("idSecurityCode", idSecurityCode);
		typedQueryList.setParameter("stateList", stateList);
		return typedQueryList.getResultList();
	}
	
	/**
	 * Gets the physical balance detail.
	 *
	 * @param certificatePK the certificate pk
	 * @return the physical balance detail
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalBalanceDetail> getPhysicalBalanceDetail(Long certificatePK)throws ServiceException{
		String query = "Select pbd from PhysicalBalanceDetail pbd "
				 + " where pbd.physicalCertificate.idPhysicalCertificatePk = :certificatePK ";
		TypedQuery<PhysicalBalanceDetail> typedQueryList= em.createQuery(query.toString(), PhysicalBalanceDetail.class);		
		typedQueryList.setParameter("certificatePK", certificatePK);		
		return typedQueryList.getResultList();
	}
	
	/**
	 * The method is used to get Operation detail retirement in Approved or Registered.
	 *
	 * @param certificateNumber Long
	 * @param idSecurityCode String
	 * @return the certificate operation detail
	 * @throws ServiceException the service exception
	 * PhysicalOperationDetail
	 */
	public List<PhysicalOperationDetail> getRetirementCertificate(Long certificateNumber, String idSecurityCode)throws ServiceException{		
		List<Integer> stateList = new ArrayList<Integer>();
		stateList.add(StateType.CONFIRMED.getCode());
		stateList.add(StateType.AUTHORIZED.getCode());
		stateList.add(StateType.APPLY.getCode());
		String query = " Select pod from PhysicalCertificateOperation pcop " 
					 + " join pcop.physicalOperationDetails pod " 
					 + " join pod.physicalCertificate pc " 
					 + " join pc.physicalBalanceDetails pbd "
					 + " where pc.certificateNumber = :certificateNumber "
					 + " and pc.securities.idSecurityCodePk = :idSecurityCode "
					 + " and pod.state in (:stateList) " 
					 + " and pcop.physicalOperationType = :physicalOperationType " 
					 + " and pbd.state in (606)  ";
		TypedQuery<PhysicalOperationDetail> typedQueryList= em.createQuery(query.toString(), PhysicalOperationDetail.class);		
		typedQueryList.setParameter("certificateNumber", certificateNumber);
		typedQueryList.setParameter("idSecurityCode", idSecurityCode);
		typedQueryList.setParameter("stateList", stateList);
		typedQueryList.setParameter("physicalOperationType", new Long(PhysicalCertificateOperationType.RETIREMENT.getCode()));
		return typedQueryList.getResultList();
	}
	
	/**
	 * Gets the physical operation.
	 *
	 * @param certificateNumber the certificate number
	 * @param idSecurityCodePk the id security code pk
	 * @return the physical operation
	 */
	public AccountAnnotationOperation getPhysicalOperation(String certificateNumber,String idSecurityCodePk){
		String query = "Select aao from AccountAnnotationOperation aao "
				+ " where aao.certificateNumber= :certificateNumber and "
				+ " aao.security.idSecurityCodePk = :idSecurityCodePk and "
				+ " aao.state=858";//estado confirmado.
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("certificateNumber",certificateNumber);
		parameters.put("idSecurityCodePk", idSecurityCodePk);
		AccountAnnotationOperation operation;
		try {
			operation = (AccountAnnotationOperation)findObjectByQueryString(query, parameters);
			return operation;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	/**
	 * Gets the physical certificate by security.
	 *
	 * @param securityFk the security fk
	 * @return the physical certificate by security
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificate> getPhysicalCertificateBySecurity(String securityFk)throws ServiceException{
		List<Integer> stateList = new ArrayList<Integer>();
		stateList.add(PhysicalCertificateStateType.ANNULED.getCode());
		stateList.add(PhysicalCertificateStateType.REJECTED.getCode());
		StringBuilder query=new StringBuilder();
		query.append(" Select pc from PhysicalCertificate pc ").
	  	  	  append("  	join  pc.physicalOperationDetails  pod ").
			  append("where  pod.state not in (:stateList) ").
			  append(" 		and pc.securities.idSecurityCodePk =:securityFk ");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("securityFk", securityFk);
		parameters.put("stateList", stateList);
		List<PhysicalCertificate> physicalCertificates = (List<PhysicalCertificate>)findListByQueryString(query.toString(), parameters);

		return physicalCertificates;
	}
	
	/**
	 * The method is used to get Physical Balance Detail.
	 *
	 * @param physicalCertificates the physical certificates
	 * @return the list
	 * @throws ServiceException the service exception
	 * PhysicalBalanceDetail
	 *//*
	public PhysicalBalanceDetail getpPhysicalBalanceDetail(Long idPhysicalCertificatePk){
		String query = "Select physicalBalanceDetail from PhysicalBalanceDetail physicalBalanceDetail where physicalBalanceDetail.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk and physicalBalanceDetail.state = :state";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idPhysicalCertificatePk", idPhysicalCertificatePk);
		parameters.put("state",RequestType.RETIRO.getCode().longValue());
		return (PhysicalBalanceDetail)findObjectByQuery(query,parameters);
	}*/
	/**
	 * The method is used to save list of physical Certificates
	 * 
	 * @param physicalCertificates
	 * 								List
	 * @return	List<PhysicalCertificate>
	 */
	public List<PhysicalCertificate> saveAllCertificates(List<PhysicalCertificate> physicalCertificates)throws ServiceException{
		return saveAll(physicalCertificates);
	}
	
	/**
	 * Update certificates.
	 *
	 * @param commonUpdateBeanInf the common update bean inf
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateCertificates(CommonUpdateBeanInf commonUpdateBeanInf, LoggerUser loggerUser)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
					  sbQuery.append("Update physical_certificate set " +
					  						"SITUATION = :situation, "+
											"LAST_MODIFY_USER = :lastModifyUser, " +
											"LAST_MODIFY_DATE = :lastModifyDate, " +
											"LAST_MODIFY_IP   = :lastModifyIp, " +
											"LAST_MODIFY_APP  = :lastModifyApp ");
					  sbQuery.append(" where ID_PHYSICAL_CERTIFICATE_PK = :idphysicalcertificate");
		Query query = em.createNativeQuery(sbQuery.toString()); 
		query.setParameter("idphysicalcertificate",commonUpdateBeanInf.getCertificatePk());
		query.setParameter("situation", commonUpdateBeanInf.getSituation());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		
		query.executeUpdate();

	}
	
	/**
	 * It saves PhysicalBalanceDetail data.
	 *
	 * @param physicalBalanceDetail the PhysicalBalanceDetail
	 * @throws ServiceException the service exception
	 */
	public void savePhysicalBalanceDetail(PhysicalBalanceDetail physicalBalanceDetail)throws ServiceException{
		create(physicalBalanceDetail);
	}
	
	/**
	 * It updates PhysicalBalanceDetail data.
	 *
	 * @param physicalBalanceDetail the PhysicalBalanceDetail
	 * @throws ServiceException the service exception
	 */
	public void updatePhysicalBalanceDetail(PhysicalBalanceDetail physicalBalanceDetail)throws ServiceException{
		update(physicalBalanceDetail);
	}
	
	/**
	 * The method is used to fetch PhysicalBalnce Detail.
	 *
	 * @param physicalBalanceDetail the physical balance detail
	 * @return PhysicalBalanceDetail
	 * @throws ServiceException the service exception
	 */
	public PhysicalBalanceDetail getPhysicalBalanceDetails(PhysicalBalanceDetail physicalBalanceDetail)throws ServiceException{
		String query = "Select pbd from PhysicalBalanceDetail pbd "
				+ " where pbd.holderAccount.idHolderAccountPk = :idHolderAccountFk and "
				+ " pbd.participant.idParticipantPk = :idParticipantFk and "
				+ " pbd.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idHolderAccountFk", physicalBalanceDetail.getHolderAccount().getIdHolderAccountPk());
		parameters.put("idParticipantFk", physicalBalanceDetail.getParticipant().getIdParticipantPk());
		parameters.put("idPhysicalCertificatePk", physicalBalanceDetail.getPhysicalCertificate().getIdPhysicalCertificatePk());
		PhysicalBalanceDetail physicalBalanceDetailExists = (PhysicalBalanceDetail)findObjectByQueryString(query, parameters);
		
		return physicalBalanceDetailExists;
	}
	
	/**
	 * The method is used to save list of physicalCertificateMovements.
	 *
	 * @param physicalCertificateMovements List<PhysicalCertificateMovement>
	 * @return List<PhysicalCertificateMovement>
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificateMovement> saveAllPhysicalCertificateMovements(List<PhysicalCertificateMovement> physicalCertificateMovements)throws ServiceException{
		return saveAll(physicalCertificateMovements);
	}
	
	/**
	 * The method is used to get Physical certificate List.
	 *
	 * @param state Integer
	 * @return List<PhysicalCertificate>
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificate> getPhysicalCertificates(Long state)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("state", state);
		return (List<PhysicalCertificate>)findWithNamedQuery(PhysicalCertificate.PHYSICAL_CERTIFICATE_LIST,parameters);
	}
	
	/**
	 * The method is used to update PhysicalCertificateOperation.
	 *
	 * @param commonUpdateBeanInf CommonUpdateBeanInf
	 * @param physicalOperationDetails the physical operation details
	 * @param loggerUser the logger user
	 * @return Integer
	 * @throws ServiceException the service exception
	 */
	public Integer updatePhysicalCertificationOperation(CommonUpdateBeanInf commonUpdateBeanInf,List<PhysicalOperationDetail> physicalOperationDetails,LoggerUser loggerUser)throws ServiceException{
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		StringBuffer query = new StringBuffer("update PhysicalCertificateOperation physicalCertificateOperation " +
											  " set physicalCertificateOperation.state = :state," +
											  " physicalCertificateOperation.lastModifyUser = :lastModifyUser," +
											  " physicalCertificateOperation.lastModifyDate = :lastModifyDate," +
											  " physicalCertificateOperation.lastModifyIp = :lastModifyIp," +
											  " physicalCertificateOperation.lastModifyApp = :lastModifyApp");
		
		parameters.put("state", commonUpdateBeanInf.getState());
		
		//query.append(",physicalCertificateOperation.indApplied = :indApplied ");
		//parameters.put("indApplied", commonUpdateBeanInf.getIntAppilied());
			
		parameters.put("state", commonUpdateBeanInf.getState());
		parameters.put("lastModifyUser", loggerUser.getUserName());
		parameters.put("lastModifyDate", loggerUser.getAuditTime());
		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("lastModifyIp", loggerUser.getIpAddress());
		
		query.append(" where physicalCertificateOperation.custodyOperation.idCustodyOperationPk = :idCustodyOperationPk and " +
					 " physicalCertificateOperation.physicalOperationType = :physicalOperationType ");
		
		parameters.put("idCustodyOperationPk",commonUpdateBeanInf.getIdCustodyOperationPk());
		parameters.put("physicalOperationType", commonUpdateBeanInf.getOperationType());
		
		return (Integer)updateByQuery(query.toString(),parameters);
	}
	
	/*
	public Integer updatePhysicalCertificationOperation(CommonUpdateBeanInf commonUpdateBeanInf,List<PhysicalOperationDetail> physicalOperationDetails,LoggerUser loggerUser)throws ServiceException{
		int rejectedSize = 0;
		int cancelSize  =0;
		int actualSize = 0;
		boolean rejectedRequest = false;
		boolean cancelrequest  = false;
		List<Long> stateList = new ArrayList<Long>();
		if(commonUpdateBeanInf.getState().equals(StateType.APPROVED.getCode().longValue())){
			stateList.add(StateType.REGISTERED.getCode().longValue());
		}
		if(commonUpdateBeanInf.getOperation().equals(new Integer(6))){
			stateList.add(StateType.REGISTERED.getCode().longValue());
		}
		stateList.add(StateType.ANNULLED.getCode().longValue());
		stateList.add(StateType.REJECTED.getCode().longValue());
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer("update PhysicalCertificateOperation physicalCertificateOperation set physicalCertificateOperation.state = :state," +
				"physicalCertificateOperation.lastModifyUser = :lastModifyUser,physicalCertificateOperation.lastModifyDate = :lastModifyDate,physicalCertificateOperation.lastModifyIp = :lastModifyIp,physicalCertificateOperation.lastModifyApp = :lastModifyApp");
		parameters.put("state", commonUpdateBeanInf.getState());
		if(commonUpdateBeanInf.getOperation().equals(5)){	//APLICAR
			query.append(",physicalCertificateOperation.indApplied = :indApplied ");
			parameters.put("indApplied", commonUpdateBeanInf.getIntAppilied());
				if(commonUpdateBeanInf.getIntAppilied().equals(new Integer(1))){
					stateList.add(StateType.CONFIRMED.getCode().longValue());
				}
		}
		parameters.put("state", commonUpdateBeanInf.getState());
		parameters.put("lastModifyUser", loggerUser.getUserName());
		parameters.put("lastModifyDate", loggerUser.getAuditTime());
		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("lastModifyIp", loggerUser.getIpAddress());
		query.append(" where physicalCertificateOperation.custodyOperation.idCustodyOperationPk = :idCustodyOperationPk and physicalCertificateOperation.physicalOperationType = :physicalOperationType and physicalCertificateOperation.state IN (:stateList)");
		if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.CONFIRM_OPERATION_CONSTANT)){
			query.append(" and physicalCertificateOperation.indApplied = :indAppliedPrev");	
			parameters.put("indAppliedPrev", new Integer(0));
		}
		if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.APPLY_OPERATION_CONSTANT)){
			query.append(" and physicalCertificateOperation.indApplied = :indAppliedPrev");	
			parameters.put("indAppliedPrev", new Integer(1));
		}
		parameters.put("idCustodyOperationPk",commonUpdateBeanInf.getIdCustodyOperationPk());
		parameters.put("physicalOperationType", commonUpdateBeanInf.getOperationType());
		
		
		//when All Certificates Are rejected the state of Request is also Rejected
		if(physicalOperationDetails.size() > 0){
			actualSize = physicalOperationDetails.size();
			for (PhysicalOperationDetail requestState : physicalOperationDetails) {
				if(requestState.getState().equals(StateType.REJECTED.getCode().longValue()) || requestState.getState().equals(StateType.ANNULLED.getCode().longValue())){
					rejectedSize++;
				}
			}
		 }
		if(actualSize > 0 && actualSize == rejectedSize){
				rejectedRequest = true;
		}
		//when All Certificates Are Canceled the state of Request is also Canceled
		if(physicalOperationDetails.size() > 0){
			actualSize = physicalOperationDetails.size();
			for (PhysicalOperationDetail requestState : physicalOperationDetails) {
				if(requestState.getState().equals(StateType.ANNULLED.getCode().longValue()) || requestState.getState().equals(StateType.REGISTERED.getCode().longValue())){
					cancelSize++;
				}
			}
		 }
		if(actualSize > 0 && actualSize == cancelSize){
			cancelrequest = true;
		}
		
		if(rejectedRequest){
			if(commonUpdateBeanInf.getState().equals(StateType.REJECTED.getCode().longValue()))
				stateList.add(StateType.APPROVED.getCode().longValue());
		}else if(commonUpdateBeanInf.getState().equals(StateType.CONFIRMED.getCode().longValue())){
			stateList.add(StateType.APPROVED.getCode().longValue());
		}else if(commonUpdateBeanInf.getState().equals(StateType.AUTHORIZED.getCode().longValue())){
			stateList.add(StateType.CONFIRMED.getCode().longValue());
		}else if(cancelrequest){
			if(commonUpdateBeanInf.getState().equals(StateType.ANNULLED.getCode().longValue()))
				stateList.add(StateType.REGISTERED.getCode().longValue());
		}
		parameters.put("stateList", stateList);
		return (Integer)updateByQuery(query.toString(),parameters);
	}*/
	
	
	/**
	 * The method is used to get the OperationDetail for requset.
	 *
	 * @param requestNumber Long
	 * @return the physical operation details for each request
	 * @throws ServiceException the service exception
	 * List<PhysicalOperationDetail>
	 */
	public List<PhysicalOperationDetail> getPhysicalOperationDetailsForEachRequest(Long requestNumber)throws ServiceException{
		Map<String,Object> requestparam = new HashMap<String, Object>();
		requestparam.put("idPhysicalOperationPk",requestNumber);
		return (List<PhysicalOperationDetail>)findListByQueryString("select physicalOperationdetail from PhysicalOperationDetail physicalOperationdetail where physicalOperationdetail.physicalCertificateOperation.custodyOperation.idCustodyOperationPk = :idPhysicalOperationPk",requestparam);
	}
	
	/**
	 * The method is used to update PhysicalOperationdetail.
	 *
	 * @param commonUpdateBeanInf CommonUpdateBeanInf
	 * @param loggerUser the logger user
	 * @return Integer
	 * @throws ServiceException the service exception
	 */
	public Integer updatePhysicalOperationDetail(CommonUpdateBeanInf commonUpdateBeanInf,LoggerUser loggerUser)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer("update PhysicalOperationDetail physicalOperationDetail set physicalOperationDetail.state = :state,"+
				"physicalOperationDetail.lastModifyUser = :lastModifyUser,physicalOperationDetail.lastModifyDate = :lastModifyDate,physicalOperationDetail.lastModifyIp = :lastModifyIp,physicalOperationDetail.lastModifyApp = :lastModifyApp");
		parameters.put("state", commonUpdateBeanInf.getState());
		parameters.put("lastModifyUser", loggerUser.getUserName());
		parameters.put("lastModifyDate", loggerUser.getAuditTime());
		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("lastModifyIp", loggerUser.getIpAddress());
		//approval
		if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.APPROVE_OPERATION_CONSTANT)){
			query.append(",physicalOperationDetail.approvalDate = :approvalDate,physicalOperationDetail.approvalUser = :approvalUser");
			parameters.put("approvalDate", loggerUser.getAuditTime());
			parameters.put("approvalUser", loggerUser.getUserName());
		}
		//anular
		else if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.CANCEL_OPERATION_CONSTANT)){
			query.append(",physicalOperationDetail.rejectDate = :anulerDate,physicalOperationDetail.rejectUser = :anulerUser");
			query.append(",physicalOperationDetail.rejectMotive = :rejectmotive,physicalOperationDetail.rejectMotiveOther = :rejectReason");
			parameters.put("anulerDate", loggerUser.getAuditTime());
			parameters.put("anulerUser", loggerUser.getUserName());			
			parameters.put("rejectmotive", commonUpdateBeanInf.getRejectMotive());
			parameters.put("rejectReason", commonUpdateBeanInf.getCancelDesc());
		}
		//confirm
		else if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.CONFIRM_OPERATION_CONSTANT)){
			query.append(",physicalOperationDetail.confirmDate = :confirmDate,physicalOperationDetail.confirmUser = :confirmUser");
			parameters.put("confirmDate", loggerUser.getAuditTime());
			parameters.put("confirmUser", loggerUser.getUserName());
		}
		//reject
		else if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.REJECT_OPERATION_CONSTANT) ||
				commonUpdateBeanInf.getOperation().equals(GeneralConstants.CANCEL_DEPOSIT_OIPERATION)){
			query.append(",physicalOperationDetail.rejectDate = :rejectDate,physicalOperationDetail.rejectUser = :rejectUser,physicalOperationDetail.rejectMotive = :rejectMotive");
			parameters.put("rejectDate", loggerUser.getAuditTime());
			parameters.put("rejectUser", loggerUser.getUserName());
			parameters.put("rejectMotive", commonUpdateBeanInf.getRejectMotiveS());
			if(commonUpdateBeanInf.getRejectMotiveS().equals(ReasonType.OTRO.getCode())){
				query.append(",physicalOperationDetail.rejectMotiveOther = :rejectMotiveOther");
				parameters.put("rejectMotiveOther", commonUpdateBeanInf.getRejectMotiveOther());
			}
		}
		//apply
		else if(commonUpdateBeanInf.getOperation().equals(5)){
			query.append(",physicalOperationDetail.applicationDate = :applicationDate,physicalOperationDetail.applicationUser = :applicationUser,physicalOperationDetail.indApplied = :indApplied");
			parameters.put("applicationDate", loggerUser.getAuditTime());
			parameters.put("applicationUser", loggerUser.getUserName());
			parameters.put("indApplied", commonUpdateBeanInf.getIntAppilied());
		}
		//authorized
		if(commonUpdateBeanInf.getOperation().equals(6)){
			query.append(",physicalOperationDetail.authorizationDate = :authorizeDate,physicalOperationDetail.authorizationUser = :authorizeUser ");
			parameters.put("authorizeDate", loggerUser.getAuditTime());
			parameters.put("authorizeUser", loggerUser.getUserName());					
		}
		query.append(" where physicalOperationDetail.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePk and physicalOperationDetail.physicalCertificateOperation.idPhysicalOperationPk = :idCustodyOperationPk");
		if(commonUpdateBeanInf.getOperation().equals(5)){
			query.append(" and physicalOperationDetail.indApplied = :indAppliedPrev");
			parameters.put("indAppliedPrev", new Integer(0));
		}
			
		parameters.put("idPhysicalCertificatePk", commonUpdateBeanInf.getCertificatePk());
		parameters.put("idCustodyOperationPk", commonUpdateBeanInf.getIdCustodyOperationPk());
		return (Integer)updateByQuery(query.toString(),parameters);
	}
	
	/**
	 * The method is used to update PhysicalOperationdetail.
	 *
	 * @param commonUpdateBeanInf CommonUpdateBeanInf
	 * @param loggerUser the logger user
	 * @return 	Integer
	 * @throws ServiceException the service exception
	 */
	public Integer updateCustodyOperation(CommonUpdateBeanInf commonUpdateBeanInf,LoggerUser loggerUser)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer("Update CustodyOperation custodyOperation set custodyOperation.state = :state,"+
				"custodyOperation.lastModifyUser = :lastModifyUser,custodyOperation.lastModifyDate = :lastModifyDate,custodyOperation.lastModifyIp = :lastModifyIp,custodyOperation.lastModifyApp = :lastModifyApp");
		parameters.put("state", commonUpdateBeanInf.getState());
		parameters.put("lastModifyUser", loggerUser.getUserName());
		parameters.put("lastModifyDate", loggerUser.getAuditTime());
		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("lastModifyIp", loggerUser.getIpAddress());
		//approval
		if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.APPROVE_OPERATION_CONSTANT)){
			query.append(",custodyOperation.approvalDate = :approvalDate,custodyOperation.approvalUser = :approvalUser");
			parameters.put("approvalDate", loggerUser.getAuditTime());
			parameters.put("approvalUser", loggerUser.getUserName());
		}
		//anular
		else if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.CANCEL_OPERATION_CONSTANT)){
			query.append(",custodyOperation.rejectDate = :anulerDate,custodyOperation.rejectUser = :anulerUser");
			query.append(",custodyOperation.rejectMotive = :rejectmotive,custodyOperation.rejectMotiveOther = :rejectReason");
			parameters.put("anulerDate", loggerUser.getAuditTime());
			parameters.put("anulerUser", loggerUser.getUserName());			
			parameters.put("rejectmotive", commonUpdateBeanInf.getRejectMotive());
			parameters.put("rejectReason", commonUpdateBeanInf.getCancelDesc());
		}
		//confirm
		else if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.CONFIRM_OPERATION_CONSTANT)){
			query.append(",custodyOperation.confirmDate = :confirmDate,custodyOperation.confirmUser = :confirmUser");
			parameters.put("confirmDate", loggerUser.getAuditTime());
			parameters.put("confirmUser", loggerUser.getUserName());
		}
		//reject
		else if(commonUpdateBeanInf.getOperation().equals(GeneralConstants.REJECT_OPERATION_CONSTANT) ||
				commonUpdateBeanInf.getOperation().equals(GeneralConstants.CANCEL_DEPOSIT_OIPERATION)){
			query.append(",custodyOperation.rejectDate = :rejectDate,custodyOperation.rejectUser = :rejectUser,custodyOperation.rejectMotive = :rejectMotive");
			parameters.put("rejectUser", loggerUser.getUserName());
			parameters.put("rejectDate", loggerUser.getAuditTime());
			parameters.put("rejectMotive", commonUpdateBeanInf.getRejectMotiveS());
			if(commonUpdateBeanInf.getRejectMotiveS().equals(ReasonType.OTRO.getCode())){
				query.append(",custodyOperation.rejectMotiveOther = :rejectMotiveOther");
				parameters.put("rejectMotiveOther", commonUpdateBeanInf.getRejectMotiveOther());
			}
		}
		//apply
		else if(commonUpdateBeanInf.getOperation().equals(5)){
			query.append(",custodyOperation.applicationDate = :applicationDate,custodyOperation.applicationUser = :applicationUser");
			parameters.put("applicationUser", loggerUser.getUserName());
			parameters.put("applicationDate", loggerUser.getAuditTime());
		}
		//authorized
				if(commonUpdateBeanInf.getOperation().equals(6)){
					query.append(",custodyOperation.authorizationDate = :authorizeDate,custodyOperation.authorizationUser = :authorizeUser");
					parameters.put("authorizeUser", loggerUser.getUserName());
					parameters.put("authorizeDate", loggerUser.getAuditTime());
				}
		query.append(" where custodyOperation.idCustodyOperationPk = :idCustodyOperationPk and custodyOperation.operationType = :operationType");
		parameters.put("operationType", commonUpdateBeanInf.getOperationType());
		parameters.put("idCustodyOperationPk",commonUpdateBeanInf.getIdCustodyOperationPk());
		return (Integer)updateByQuery(query.toString(),parameters);
	}
	
	/**
	 * The method is used to get Issuers List.
	 *
	 * @param state Integer
	 * @return List<Issuer>
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> getIssuers(Integer state)throws ServiceException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("state", state);
		return (List<Issuer>)findWithNamedQuery(Issuer.ISSUER,parameters);
	}
	
	/**
	 * The method is used to save PhysicalCertificateOperation.
	 *
	 * @param physicalCertificateOperation the physical certificate operation
	 * @return the physical certificate operation
	 * @throws ServiceException the service exception
	 * PhysicalCertificateOperation
	 */
	public PhysicalCertificateOperation savePhysicalCertificateOperation(PhysicalCertificateOperation physicalCertificateOperation)throws ServiceException{
		return create(physicalCertificateOperation);
	}
	
	/**
	 * The method is used to save CustodyOperation.
	 *
	 * @param custodyOperation CustodyOperation
	 * @return 	CustodyOperation
	 * @throws ServiceException the service exception
	 */
	public CustodyOperation saveCustodyOperation(CustodyOperation custodyOperation)throws ServiceException{
		return create(custodyOperation);
	}
	
	/**
	 * The method is used to get PhysicalCertificateOperation.
	 *
	 * @param id Long
	 * @return PhysicalCertificateOperation
	 * @throws ServiceException the service exception
	 */
	public PhysicalCertificateOperation getPhysicalCertificationOperation(Long id)throws ServiceException{
		return find(PhysicalCertificateOperation.class, id);
	}
	
	/**
	 * It gives security class details.
	 *
	 * @param instrumentType the Integer
	 * @param masterfk the Integer
	 * @return the  List<ParameterTable>
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> securityClassService(Integer instrumentType,Integer masterfk)throws ServiceException {
		String query = "Select p from ParameterTable p where  p.masterTable.masterTablePk = :idmasterfk and p.shortInteger = :shortinteger"; 
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idmasterfk", masterfk);
		parameters.put("shortinteger", instrumentType);
		return (List<ParameterTable>)findListByQueryString(query, parameters);
	}
	
	/**
	 * The method is used to get security Location.
	 *
	 * @param id Long
	 * @return the security location
	 * @throws ServiceException the service exception
	 * List<ParameterTable>
	 */
	public List<ParameterTable> getSecurityLocation(Long id)throws ServiceException{
		String sql = "Select parameterTable from ParameterTable parameterTable where parameterTable.masterTable.masterTablePk = :id";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", id.intValue());
		return (List<ParameterTable>)findListByQueryString(sql, parameters);
	}
	
	/**
	 * The method is used to get holiday.
	 *
	 * @param currentDate Date
	 * @param state Integer
	 * @param geograpicalLoc Integer
	 * @return the holiday list
	 * @throws ServiceException the service exception
	 * Holiday
	 */
	public Holiday getHolidayList(Date currentDate,Integer state,Integer geograpicalLoc)throws ServiceException{
		Map<String,Object> parameters = new HashMap<String, Object>();
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append("select hol from Holiday hol ");
		sbQuery.append("where hol.holidayDate=:day");
		sbQuery.append(" and hol.state=:idstate ");
		sbQuery.append(" and hol.geographicLocation.idGeographicLocationPk=:idgeograp");
		parameters.put("day", currentDate);
		parameters.put("idstate", state);
		parameters.put("idgeograp", geograpicalLoc);		
		try{
			return (Holiday)findObjectByQueryString(sbQuery.toString(), parameters);
		}catch(NoResultException nre){
			return null;
		}
	}
	
	/**
	 * Gets the issuerby security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the issuerby security
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuerbySecurity(String idSecurityCodePk)throws ServiceException{
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT s");
			strQuery.append("	FROM Security s");
			strQuery.append("		INNER JOIN FETCH s.issuer");
			strQuery.append("	WHERE s.idSecurityCodePk=:security");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("security", idSecurityCodePk);
			Security security=(Security) query.getSingleResult();
			
			return 	security.getIssuer();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the security
	 */
	public Security getSecurity(String idSecurityCodePk){
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT se");
			strQuery.append("	FROM Security se");
			strQuery.append("	WHERE se.idSecurityCodePk=:security");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("security", idSecurityCodePk);
			Security security=(Security) query.getSingleResult();
			return 	security;
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @param idIssuerPk the id issuer pk
	 * @return the issuer
	 */
	public Issuer getIssuer(String idIssuerPk){
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iss");
			strQuery.append("	FROM Issuer iss");
			strQuery.append("	WHERE iss.idIssuerPk=:idIssuerPk");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("idIssuerPk", idIssuerPk);
			Issuer issuer=(Issuer) query.getSingleResult();
			return 	issuer;
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Issuer getIssuerWithNemonic(String mnemonic){
		try {
			StringBuilder strQuery = new StringBuilder();
			strQuery.append("	SELECT iss");
			strQuery.append("	FROM Issuer iss");
			strQuery.append("	WHERE iss.mnemonic=:mnemonic");
			Query query = em.createQuery(strQuery.toString());
			query.setParameter("mnemonic", mnemonic);
			Issuer issuer=(Issuer) query.getSingleResult();
			return 	issuer;
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Gets the physical cert marketfact.
	 *
	 * @param idPhysicalCertificate the id physical certificate
	 * @return the physical cert marketfact
	 */
	public PhysicalCertMarketfact getPhysicalCertMarketfact(Long idPhysicalCertificate)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT PCM FROM PhysicalCertMarketfact PCM ");
		stringBuffer.append(" WHERE PCM.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificate ");
		stringBuffer.append(" and PCM.indActive = :indicatorOne ");
		
		TypedQuery<PhysicalCertMarketfact> typedQuery= em.createQuery(stringBuffer.toString(), PhysicalCertMarketfact.class);
		typedQuery.setParameter("idPhysicalCertificate", idPhysicalCertificate);
		typedQuery.setParameter("indicatorOne", BooleanType.YES.getCode());
		
		return typedQuery.getSingleResult();
	}
	
	/**
	 * Search physical cert marketfact.
	 *
	 * @param idPhysicalCertificatePK the id physical certificate pk
	 * @return the physical cert marketfact
	 */
	public PhysicalCertMarketfact searchPhysicalCertMarketfact(Long idPhysicalCertificatePK)
	{ 
		if(Validations.validateIsNotNullAndPositive(idPhysicalCertificatePK)){
			try {
				StringBuilder strQuery = new StringBuilder();
				strQuery.append("	SELECT PCM");
				strQuery.append("	FROM PhysicalCertMarketfact PCM");
				strQuery.append("	WHERE PCM.physicalCertificate.idPhysicalCertificatePk = :idPhysicalCertificatePK ");
				Query query = em.createQuery(strQuery.toString());
				query.setParameter("idPhysicalCertificatePK", idPhysicalCertificatePK);
				PhysicalCertMarketfact certMarketfact=(PhysicalCertMarketfact) query.getSingleResult();
				return 	certMarketfact;
			
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Gets the list account annotation operation by certificate.
	 *
	 * @param certificateNumber the certificate number
	 * @param idSecurityCode the id security code
	 * @return the list account annotation operation by certificate
	 */
	public List<AccountAnnotationOperation> getListAccountAnnotationOperationByCertificate(Long certificateNumber, String idSecurityCode)
	{
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT AAO FROM AccountAnnotationOperation AAO ");
		stringBuffer.append(" WHERE AAO.certificateNumber = :certificateNumber ");
		stringBuffer.append(" and AAO.security.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" and AAO.state not in (:lstDematerializationState) ");
		
		TypedQuery<AccountAnnotationOperation> typedQuery= em.createQuery(stringBuffer.toString(), AccountAnnotationOperation.class);
		typedQuery.setParameter("certificateNumber", certificateNumber.toString());
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		List<Integer> lstDematerializationState= new ArrayList<Integer>();
		lstDematerializationState.add(DematerializationStateType.ANNULLED.getCode());
		lstDematerializationState.add(DematerializationStateType.REJECTED.getCode());
		lstDematerializationState.add(DematerializationStateType.CANCELLED.getCode());
		typedQuery.setParameter("lstDematerializationState", lstDematerializationState);
			
		return typedQuery.getResultList();
	}
//	public boolean checkNumberTitle(Security security, String certificateNumber){
//		try {
//			//SELECT * FROM ACCOUNT_ANNOTATION_OPERATION WHERE CERTIFICATE_NUMBER='123' AND ID_SECURITY_CODE_FK='BLP-SBC-6-N1U-12' AND STATE_ANNOTATION=858;
//			StringBuilder strQuery = new StringBuilder();
//			strQuery.append("	selec aao");
//			strQuery.append("	from AccountAnnotationOperation aao");
//			strQuery.append("	where 1=1 ");
//			
//			strQuery.append(" and aao.certificateNumber = :certificateNumber ");
//			strQuery.append(" and aao.security.idSecurityCodePk = :idSecurityCodePk ");
//			
//			Query query = em.createQuery(strQuery.toString());
//			
//			query.setParameter("certificateNumber", certificateNumber);
//			query.setParameter("idSecurityCodePk", security.getIdSecurityCodePk());
//			AccountAnnotationOperation operation=(AccountAnnotationOperation) query.getSingleResult();
//			if(Validations.validateIsNotNullAndNotEmpty(operation))
//				return true;
//			else 
//				return false;
//		} catch (NoResultException e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
	
	/**
 * The method is used to get PhysicalCertificateList to reversion.
 *
 * @param physicalCertificateFilter PhysicalCertificateFilter
 * @param statesPhysical the states physical
 * @return 	List<PhysicalCertificateExt>
 * @throws ServiceException the service exception
 */
	@SuppressWarnings("unchecked")
	public List<PhysicalCertificateExt> getPhysicalCertificateReversion(PhysicalCertificateFilter physicalCertificateFilter,
			List<Integer> statesPhysical ) throws ServiceException{
		List<Object[]> list = new ArrayList<Object[]>();
		List<PhysicalCertificateExt> physicalCertificateExts = new ArrayList<PhysicalCertificateExt>();
		StringBuffer query = 
								new StringBuffer("Select pc," +
										"pco.participant," +
										"pco.custodyOperation.idCustodyOperationPk,"+
										"pod.state," +
										"pod.idPhysicalOperationDetail," +
										"pod," +
										"pco.state, " +
										"pco.custodyOperation.operationNumber, "+
										"pco.custodyOperation.registryDate, "+
										"pco "+
									 " from "+
										  " PhysicalCertificate pc," +
									 	  " PhysicalOperationDetail  pod," +
									 	  " PhysicalCertificateOperation pco " +
									 "where "+
									 "pco.physicalOperationType = :physicalOperationType and " +
									 "pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk and " +
									 "pc.idPhysicalCertificatePk = pod.physicalCertificate.idPhysicalCertificatePk  " +
									 "and TRUNC(pod.registryDate) =  TRUNC(:dateRegister) " +
									 "and pod.state in :lstStates ");
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("physicalOperationType", new Long(RequestType.DEPOSITEO.getCode()));
		parameters.put("dateRegister", physicalCertificateFilter.getInitialDate());
		parameters.put("lstStates", statesPhysical);		
		list = findListByQueryString(query.toString(), parameters);
		if(list != null){
			PhysicalCertificateExt physicalCertificateExt = null;
			for (Object[] object : list) {
				physicalCertificateExt = new PhysicalCertificateExt();
				physicalCertificateExt.setPhysicalCertificate((PhysicalCertificate)object[0]);
				physicalCertificateExt.setParticipantPk(((Participant)object[1]).getIdParticipantPk());
				physicalCertificateExt.setRequestNumber((Long)object[2]);
				physicalCertificateExt.setState((Integer)object[3]);
				physicalCertificateExt.setIdPhysicalOperationDetail((Long)object[4]);
				physicalCertificateExt.setPhysicalOperationDetail((PhysicalOperationDetail)object[5]);
				physicalCertificateExt.setRequestState((Integer)object[6]);
				physicalCertificateExt.setOperationNumber((Long)object[7]);
				physicalCertificateExt.setRegistryDate((Date)object[8]);
				physicalCertificateExt.setHolderAccount(((PhysicalCertificateOperation)object[9]).getHolderAccount());
				physicalCertificateExts.add(physicalCertificateExt);
			}
		}		
		return physicalCertificateExts;
	}
	
	/** LISTA DE HECHOS DE MERCADO issue 188**/
    public List<HolderMarketFactBalance> getListHolderMarketFacts( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT a ");
		sbQuery.append("FROM HolderMarketFactBalance a ");
		sbQuery.append("WHERE 1=1 AND a.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append("AND a.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append("AND a.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append("AND a.indActive = :indActive ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("indActive", GeneralConstants.ONE_VALUE_INTEGER);
		return (List<HolderMarketFactBalance>)query.getResultList();		
	}    
    public HolderMarketFactBalance getHolderMarketFact( String idSecurityCodePk, Long idParticipantPk, Long idHolderAccountPk ) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT a ");
		sbQuery.append("FROM HolderMarketFactBalance a ");
		sbQuery.append("WHERE 1=1 ");
		sbQuery.append("AND a.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append("AND a.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append("AND a.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append("AND a.indActive = :indActive ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("indActive", GeneralConstants.ONE_VALUE_INTEGER);
		try {
			return (HolderMarketFactBalance)query.getSingleResult();
		} catch (Exception e) {
			return null;
		}		
	}
    public void changeStatusHolderMarketFactBalance(HolderMarketFactBalance regHolderMarketFact) {
    	Long idMarketfactBalancePk = regHolderMarketFact.getIdMarketfactBalancePk();
    	//changeStatusHolderMarketFactBalance(idMarketfactBalancePk);
    	System.out.println("++++SE CAMBIARA EL ESTADO DEL HN: "+idMarketfactBalancePk);
    }
    public void changeStatusHolderMarketFactBalance(Long idMarketfactBalancePk) {
    	StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" update HolderMarketFactBalance set indActive = 0");
		sbQuery.append(" where IdMarketfactBalancePk = :idMarketfactBalancePk");
		Query query=em.createQuery( sbQuery.toString() );
		query.setParameter("idMarketfactBalancePk", idMarketfactBalancePk);
		int deleted = query.executeUpdate();
    }
    /**FIN ISSUE 188**/
    
    /** metodo para registrar en la BBDD un AccAccountAnnotationFile
	 * 
	 * @param entity
	 * @return */
	public PhysicalCertificateFile registryPhysicalCertificateFile(PhysicalCertificateFile entity) {
		try {
			em.persist(entity);
			em.flush();
			return entity;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<AccountAnnotationOperation> lstAccountAnnotationOperationAccionesAllFromTo(AccountAnnotationOperation accountAnnotation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT a ");
		sbQuery.append(" FROM AccountAnnotationOperation a ");
		sbQuery.append(" WHERE a.state not in (:lststate) ");
		sbQuery.append(" AND a.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" AND a.indSerializable = :indSerializable ");
		
		TypedQuery<AccountAnnotationOperation> query = em.createQuery(sbQuery.toString(), AccountAnnotationOperation.class);

		List<Integer> lstNotInState = new ArrayList<Integer>();
		lstNotInState.add(DematerializationStateType.REVERSED.getCode());
		lstNotInState.add(DematerializationStateType.CANCELLED.getCode());
		lstNotInState.add(DematerializationStateType.ANNULLED.getCode());
		lstNotInState.add(DematerializationStateType.REJECTED.getCode());
		
		query.setParameter("idSecurityCodePk", accountAnnotation.getSecurity().getIdSecurityCodePk());
		query.setParameter("lststate", lstNotInState);
		query.setParameter("indSerializable", 1);
		
		return query.getResultList();
		
	}

	public AccountAnnotationOperation findAccountAnnotationOperationAcciones(AccountAnnotationOperation accountAnnotation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT a ");
		sbQuery.append(" FROM AccountAnnotationOperation a ");
		sbQuery.append(" WHERE a.state not in (:lststate) ");
		sbQuery.append(" AND a.certificateFrom = :certificateFrom ");
		sbQuery.append(" AND a.certificateTo = :certificateTo ");
		sbQuery.append(" AND a.security.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append(" AND a.indSerializable = :indSerializable ");
		 
		Query query = em.createQuery(sbQuery.toString());

		List<Integer> lstNotInState = new ArrayList<Integer>();
		lstNotInState.add(DematerializationStateType.REVERSED.getCode());
		lstNotInState.add(DematerializationStateType.CANCELLED.getCode());
		lstNotInState.add(DematerializationStateType.ANNULLED.getCode());
		lstNotInState.add(DematerializationStateType.REJECTED.getCode());
		
		
		query.setParameter("idSecurityCodePk", accountAnnotation.getSecurity().getIdSecurityCodePk());
		query.setParameter("certificateFrom", accountAnnotation.getCertificateFrom());
		query.setParameter("certificateTo", accountAnnotation.getCertificateTo());		
		query.setParameter("lststate", lstNotInState);
		query.setParameter("indSerializable", 1);
		
		List<AccountAnnotationOperation> lstAccountAnnotationOperation = query.getResultList();
		
		try {
			if(lstAccountAnnotationOperation!=null && lstAccountAnnotationOperation.size()> 0) {
				return lstAccountAnnotationOperation.get(0);
			}else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}	
	}
	
	public AccountAnnotationOperation findAccountAnnotationOperation(AccountAnnotationOperation accountAnnotation) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT a ");
		sbQuery.append(" FROM AccountAnnotationOperation a ");
		sbQuery.append(" WHERE a.state not in (:lststate) ");
		sbQuery.append(" AND a.issuer.idIssuerPk = :idIssuerPk ");
		sbQuery.append(" AND a.serialNumber = :serialNumber ");
		sbQuery.append(" AND a.certificateNumber = :certificateNumber ");
		sbQuery.append(" AND a.securityCurrency = :securityCurrency ");
		sbQuery.append(" AND a.indSerializable = :indSerializable ");
		if(accountAnnotation.getSecurityClass() != null) {
			sbQuery.append(" AND a.securityClass = :securityClass ");	
		}
		Query query = em.createQuery(sbQuery.toString());

		List<Integer> lstNotInState = new ArrayList<Integer>();
		lstNotInState.add(DematerializationStateType.REVERSED.getCode());
		lstNotInState.add(DematerializationStateType.CANCELLED.getCode());
		lstNotInState.add(DematerializationStateType.ANNULLED.getCode());
		lstNotInState.add(DematerializationStateType.REJECTED.getCode());
		
		query.setParameter("idIssuerPk", accountAnnotation.getIssuer().getIdIssuerPk());
		query.setParameter("serialNumber", accountAnnotation.getSerialNumber());
		query.setParameter("certificateNumber", accountAnnotation.getCertificateNumber());
		query.setParameter("securityCurrency", accountAnnotation.getSecurityCurrency());
		query.setParameter("lststate", lstNotInState);
		query.setParameter("indSerializable", 0);

		if(accountAnnotation.getSecurityClass() != null) {
			query.setParameter("securityClass", accountAnnotation.getSecurityClass());
		}
		
		
		List<AccountAnnotationOperation> lstAccountAnnotationOperation = query.getResultList();
		
		try {
			if(lstAccountAnnotationOperation!=null && lstAccountAnnotationOperation.size()> 0) {
				return lstAccountAnnotationOperation.get(0);
			}else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}	
	}
	
	public List<HolderAccountBank> findHolderAccountBank (Long holderAccount, Integer stateAccountBank) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT a ");
		sbQuery.append(" FROM HolderAccountBank a ");
		sbQuery.append(" JOIN FETCH a.holderAccount");
		sbQuery.append(" where a.holderAccount.idHolderAccountPk = :idHolderAccount ");
		sbQuery.append(" and a.stateAccountBank = :stateAccount ");
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idHolderAccount", holderAccount);
		query.setParameter("stateAccount", stateAccountBank);
		
		try {
			List<HolderAccountBank> lsHolderAccountBank = query.getResultList();
			if (lsHolderAccountBank != null && lsHolderAccountBank.size()>0) {
				return lsHolderAccountBank;
			}else {
				return null;
			}			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<InstitutionBankAccount> findInstitutionBank (Long idParticipantPk, Integer state){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT a ");
		sbQuery.append(" FROM InstitutionBankAccount a ");
		sbQuery.append(" where a.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append(" and a.state = :state");
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("idParticipantPk", idParticipantPk);
		query.setParameter("state", state);
		
		try {
			List<InstitutionBankAccount> lsInstitutionBankAccount = query.getResultList();
			if (lsInstitutionBankAccount != null && lsInstitutionBankAccount.size()>0) {
				return lsInstitutionBankAccount;
			}else {
				return null;
			}			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
