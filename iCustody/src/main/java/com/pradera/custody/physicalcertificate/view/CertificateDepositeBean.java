/*
 * 
 */
package com.pradera.custody.physicalcertificate.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.facade.ReteirementDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.service.UnzipUtility;
import com.pradera.custody.physicalcertificate.to.DataExcelRegCertificateDepositeTO;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.custody.to.XmlRespuestaActivoFinanciero;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertFile;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateFile;
import com.pradera.model.custody.physical.PhysicalCertificateMovement;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.PaymentType;
import com.pradera.model.custody.type.ReasonType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterFileType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CertificateDepositeBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class CertificateDepositeBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The em. */
	@Inject @DepositaryDataBase
	protected EntityManager em;
	
	/** The certificate deposite bean facade. */
	@EJB
	private CertificateDepositeServiceFacade certificateDepositeBeanFacade;
	
	/** The withdrawal depositfacade. */
	@EJB
	private ReteirementDepositeServiceFacade withdrawalDepositfacade;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
    /** The general parameters facade. */
    @EJB
    private GeneralParametersFacade generalParametersFacade;
    
    /** The accounts facade. */
    @EJB
    private AccountsFacade accountsFacade;
    
    /** The helper component facade. */
    @EJB
    private HelperComponentFacade helperComponentFacade;

	/** The transfer available facade. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailableFacade;
	
	/** The dematerialization certificate facade. */
	@Inject
	DematerializationCertificateFacade dematerializationCertificateFacade;

    
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
    
    /** The country residence. */
    @Inject @Configurable Integer countryResidence;

	/** The Constant DEPOSITED. */
    private static final Integer DEPOSITED = new Integer(605);
	
	/** The Constant RETIRE. */
	private static final Long 	 RETIRE = new Long(606);

	/** The participant list. */
	private List<Participant> 		participantList 	= new ArrayList<Participant>();
	
	/** The status deposit list. */
	private List<ParameterTable> 	statusDepositList 	= new ArrayList<ParameterTable>();

	//search - view
	/** The physical certificate. */
	private PhysicalCertificate physicalCertificate = new PhysicalCertificate();
	
	/** The physical certificate tmp. */
	private PhysicalCertificate physicalCertificateTmp = new PhysicalCertificate();
	
	//new
	/** The physical certificates. */
	private PhysicalCertificate[] 			physicalCertificates;
	
	/** The certificate data model. */
	private PhysicalCertificateDataModel 	certificateDataModel;
	
	/** The physical certificate list. */
	private List<PhysicalCertificate> 		physicalCertificateList;
	
	/** The from correlativo. */
	private Integer 						fromCorrelativo;
	
	/** The to correlativo. */
	private Integer 						toCorrelativo;
	
	/** The number of certificates. */
	private Integer 						numberOfCertificates;
	
	/** The certificate number. */
	private Long 							certificateNumber = null;
	
	/** The physical certificates existed temp. */
	private List<PhysicalCertificate> 		physicalCertificatesExistedTemp = new ArrayList<PhysicalCertificate>();
	
	/** The detelephysical certificates. */
	private PhysicalCertificate 			detelephysicalCertificates;
	
	/** The security. */
	private Security						security;
	
	/** The payment periodicity aux. */
	private Integer paymentPeriodicityAux;
	
	/** The initial issuer modify. */
	private String initialIssuerModify;
	
	/** The initial certificate number modify. */
	private String initialCertificateNumberModify;
	
	/** The initial instrument type modify. */
	private Integer initialInstrumentTypeModify;
	
	/** The initial payment periodicity modify. */
	private Integer initialPaymentPeriodicityModify;
	
	/** The initial security class modify. */
	private Integer initialSecurityClassModify;
	
	/** The initial interest rate modify. */
	private BigDecimal initialInterestRateModify;
	
	/** The initial issuance date modify. */
	private Date initialIssuanceDateModify;
	
	/** The initial expire date modify. */
	private Date initialExpireDateModify;
	
	/** The initial currency modify. */
	private Long initialCurrencyModify;
	
	/** The initial nominal value modify. */
	private BigDecimal initialNominalValueModify;
	
	
	//
	/** The participant. */
	private Participant 	participant = new Participant();
	
	/** The holder account. */
	private HolderAccount 	holderAccount = new HolderAccount();
	
	/** The holder account details. */
	private HolderAccount 	holderAccountDetails = new HolderAccount();
	
	/** The issuer list. */
	private List<HolderAccount>  holderAccountList;
	
	/** The issuer list. */
	private List<Issuer> 	issuerList;
	
	/** The participant selected. */
	private Long 			participantSelected;
	
	/** The security class selected. */
	private Integer 		securityClassSelected;
	
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The account number. */
	private Integer 		accountNumber;
	
	/** The status selected. */
	private Integer 		statusSelected = null;
	
	/** The certificate selected. */
	private Long 			certificateSelected = null;
	
	/** The request number. */
	private Long 			requestNumber;
	
	/** The request selected. */
	private Long 			requestSelected = null;
	
	/** The initial date. */
	private Date 			initialDate = CommonsUtilities.currentDate();
	
	/** The final date. */
	private Date 			finalDate = CommonsUtilities.currentDate();
	
	/** The max date. */
	private Date 			maxDate = CommonsUtilities.currentDate();
	
	/** The confirm alert action. */
	private String			confirmAlertAction = "";
	
	/** The reason selected. */
	/***/
	private Integer			reasonSelected = null;
	
	/** The reject other. */
	private String			rejectOther = null;
	
	/** The other reject motive id. */
	private Integer			otherRejectMotiveId = ReasonType.OTRO.getCode();
	
	
	/** The instrument type list. */
	private List<ParameterTable> instrumentTypeList = new ArrayList<ParameterTable>();
	
	/** The instrument type map. */
	private Map<Integer, String> instrumentTypeMap = new HashMap<Integer, String>();
	
	/** The payment type list. */
	private List<ParameterTable> paymentTypeList = new ArrayList<ParameterTable>();

	/** The depositRequest type list. */
	private List<ParameterTable> depositRequestStateList = new ArrayList<ParameterTable>();
	
	/** The depositState type list. */
	private List<ParameterTable> depositStateList = new ArrayList<ParameterTable>();

	/** The ReasonState type list. */
	private List<ParameterTable> ReasonStateList = new ArrayList<ParameterTable>();
	
	/** The payment type map. */
	private Map<Integer, String> paymentTypeMap = new HashMap<Integer, String>();
	
	/** The depositRequestState type map. */
	private Map<Integer, String> depositRequestStateTypeMap = new HashMap<Integer, String>();

	/** The depositState type map. */
	private Map<Integer, String> depositStateTypeMap = new HashMap<Integer, String>();

	/** The reasonState type map. */
	private Map<Integer, String> reasonStateTypeMap = new HashMap<Integer, String>();
	
	/** The money type list. */
	private List<ParameterTable> moneyTypeList = new ArrayList<ParameterTable>();
	
	/** The money type map. */
	private Map<Integer, String> moneyTypeMap = new HashMap<Integer, String>();
	
	/** The money type full map. */
	private Map<Integer, String> moneyTypeFullMap = new HashMap<Integer, String>();

	/** The security class list. */
	private List<ParameterTable> securityClassList = new ArrayList<ParameterTable>();
	
	/** The security class type map. */
	private Map<Integer, String> securityClassTypeMap = new HashMap<Integer, String>();
	
	/** The security search. */
	private Security	securitySearch;

	/** The lst security class. */
	private List<ParameterTable> lstSecurityClass;
	/*nw*/
	/** The quantity bs. */
	private int quantityBS;
	//private int quantityEUR;
	/** The quantity usd. */
	private int quantityUSD;
	
	/** The quantity eur. */
	private int quantityEUR;
	
	/** The quantity ufv. */
	private int quantityUFV;
	
	/** The quantity mvd. */
	private int quantityMVD;
	
	/** The sum usd. */
	private BigDecimal sumUSD = new BigDecimal(0);
	
	/** The sum bs. */
	private BigDecimal sumBS = new BigDecimal(0);

	/** The sum eur. */
	private BigDecimal sumEUR= BigDecimal.ZERO;
	
	/** The sum ufv. */
	private BigDecimal sumUFV= BigDecimal.ZERO;
	
	/** The sum mvd. */
	private BigDecimal sumMVD= BigDecimal.ZERO;
	
	
	/** The existed certificate. */
	private boolean	existedCertificate;
	
	/** The physical certificate modify. */
	private PhysicalCertificate 		physicalCertificateModify;

	/** The Certificate quantity. */
	private BigDecimal 					CertificateQuantity = null;
	
		/** The Certificate quantity. */
	private Date 					certificateDate = null; 
	
	public Date getCertificateDate() {
		return certificateDate;
	}

	public void setCertificateDate(Date certificateDate) {
		this.certificateDate = certificateDate;
	}

	/** The flag dpf. */
	private boolean						flagDPF;
	
	/** The issuer selectedmodify. */
	private String 						issuerSelectedmodify = null;
	
	/** The physical certificates existed. */
	private List<PhysicalCertificate> 	physicalCertificatesExisted;

	/** The instrument type selected. */
	private Integer instrumentTypeSelected;
	
	/** The Constant RENTA_FIJA. */
	private static final  Integer RENTA_FIJA = 124;
	
	/** The Constant RENTA_VARIABLE. */
	private static final  Integer RENTA_VARIABLE = 399;
	
	/** The rendered instrument type. */
	private boolean renderedInstrumentType = true;
	
	/** The select certificate count. */
	private Integer selectCertificateCount = new Integer(1);
	
	/*Listado de busqueda*/
	/** The search certificate data model. */
	private PhysicalCertificateExtDataModel searchCertificateDataModel = new PhysicalCertificateExtDataModel();
	
	/** The physical certificate search list. */
	private List<PhysicalCertificateExt> 	physicalCertificateSearchList;
	
	/** The selected physical certificates ext. */
	private PhysicalCertificateExt[] 		selectedPhysicalCertificatesExt;
	
	//Tmp para el view
	/** The certificates nums. */
	private List<Long> certificatesNums = new ArrayList<Long>(); 
	
	/** The physical certificate temp. */
	private PhysicalCertificateExt physicalCertificateTemp;
    
	/*Permisos*/
	/** The participant login. */
	private Long 	participantLogin = new Long(0);
	
	/** The is participant. */
	private boolean isParticipant = false;
	
	/** The is cevaldom. */
	private boolean isDepositary = false;
	
	/** The is bcr. */
	private boolean isBcr = false;
	
	/** The flag register. */
	private boolean flagRegister = false;
	
	/** The flag search. */
	private boolean flagSearch = false;
	
	/** The registers found. */
	private boolean registersFound = true;
	
	/** The reject confirm enabled. */
	private boolean rejectConfirmEnabled = false;
	
	/** The modify. */
	private boolean modify = false;
	
	/** The show. */
	private boolean show = false;
	
	/** The validator disabled. */
	private boolean validatorDisabled = false;
	
	/** The flag render field. */
	private boolean flagRenderField = false;
	
	/** The source holder. */
	//helpers
	private Holder sourceHolder = new Holder();
	
	/** The source account to. */
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	
	/** The lst source account to. */
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	/** The map state holder account. */
	Map<Integer, String> mapStateHolderAccount = new HashMap<Integer,String>();
	
	/** The lst holder account state. */
	private List<ParameterTable> lstHolderAccountState = new ArrayList<ParameterTable>();
	
	/** The reg physical cert marketfact. */
	private PhysicalCertMarketfact regPhysicalCertMarketfact = new PhysicalCertMarketfact();
	
	/** The arr reg physical cert marketfact. */
	private PhysicalCertMarketfact[] arrRegPhysicalCertMarketfact;
	
	/** The reg lst physical cert marketfacts. */
	private List<PhysicalCertMarketfact> regLstPhysicalCertMarketfacts = new ArrayList<PhysicalCertMarketfact>();
	
	/** The reg lst physical cert marketfacts. from data base */
	private List<HolderMarketFactBalance> listHolderMarketFactBalance = new ArrayList<HolderMarketFactBalance>();
	
	/** The reg lst physical cert marketfacts. from data base */
	private HolderMarketFactBalance holderMarketFactBalance = new HolderMarketFactBalance();
	/** flag para evaluar si el valor ingresado existe o no, se lo utliza para el REGISTRO de Depoito de Titulos Fisicos */
	private boolean existSecurity;
	
	/** Parametros de los archivos que se deben adjuntar ala momento de registrar un Deposito de titulos Fiscos */
	private List<ParameterTable> lstFileTypes = new ArrayList<>();
	
	/** lista en donde se suben los archivos de Titulos fisicos */
	private List<PhysicalCertFile> lstPhysicalCertFile = new ArrayList<>();
	
	private List<PhysicalCertFile> lstPhysicalCertFileView = new ArrayList<>(); 
	
	private String fUploadFileTypesToPhysicalCert="jpg|jpeg|pdf|doc|docx|png";
	private String fUploadFileTypesDocumentsDisplayToPhysicalCert="*.jpg, *.jpeg, *.png, *.doc, *.docx, *.pdf";	
	
	private boolean secIsFixedIncome;
	private boolean flagIsVariableIncome;
	
	/** The issuer help. */
	private Issuer issuerHelp = new Issuer();
	
	/** The issuer search pk. */
	private String issuerSearchPK;
	
	/** The verify */
	private boolean verifyToSend = true;
	
	/** Register Masive */ // Issue 1278
	
	private PhysicalCertificateFile fileExcelPersist;
	private PhysicalCertificateFile fileZip;
	
	private DataExcelRegCertificateDepositeTO resultExcel;
	
	private String fUploadFileSizeDisplayMassive="3000000";
	private String fUploadFileTypesDocumentsDisplayMassive=" *.zip ";	
	
	/**
	 * Post construc search.
	 */
	@PostConstruct
	public void postConstrucSearch(){
    	try {
			loadDepositeStatusList();
			securityEvents();
			loadParticipant();
			otherRejectMotiveId = ReasonType.OTRO.getCode();
			loadHolderAccountState();
			regPhysicalCertMarketfact = new PhysicalCertMarketfact();
			loadSecurityClass();
			securitySearch = new Security();
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
    }
	
	/**
	 * metodo ajax para caprura la clase de valor ingresada
	 */
	public void enteredSecurityClass(){
		security=new Security();
		security.setSecurityClass(securityClassSelected);
		certificateDate = null;
		flagIsVariableIncome = false;
		secIsFixedIncome = true;
		// determinando si es renta fija o variable
		if(securityClassSelected.equals(SecurityClassType.ACE.getCode())
				|| securityClassSelected.equals(SecurityClassType.ACC.getCode())
				|| securityClassSelected.equals(SecurityClassType.CFC.getCode())
				|| securityClassSelected.equals(SecurityClassType.AOP.getCode())
				|| securityClassSelected.equals(SecurityClassType.ACC_RF.getCode())
				|| securityClassSelected.equals(SecurityClassType.ANR.getCode())
				){
			flagIsVariableIncome = true;
			secIsFixedIncome = false;
		}
	}
	
	/**
	 * Load security.
	 */
	public void loadSecurity(){
		 
		try {
			
			if(Validations.validateIsNotNull(security.getIdSecurityCodePk())){
				
				// validando si la clase de valor corresponde a la clase valor ingresada
				if(Validations.validateIsNotNull(security.getSecurityClass())){
					// si la longitud del codigo de valor es menor a 3 lanzar mensaje de validacion  
					if(security.getIdSecurityCodePk().length()<3){
						//lanzar el mensaje de que el el valor no correspode a la clase-clave de valor ingresada
						alert(PropertiesUtilities.getMessage(PropertiesConstants.VALIDATE_SECURITYCLASS_AND_SECURITYCODE));
						security.setIdSecurityCodePk(null);
						return;
					}
					
					ParameterTable pt=em.find(ParameterTable.class, security.getSecurityClass());
					if(!pt.getText1().equals(security.getIdSecurityCodePk().substring(0, 3))){
						//lanzar el mensaje de que el el valor no correspode a la clase-clave de valor ingresada
						alert(PropertiesUtilities.getMessage(PropertiesConstants.VALIDATE_SECURITYCLASS_AND_SECURITYCODE));
						security.setIdSecurityCodePk(null);
						return;
					}
					
				}
				
				// verificando si el valor ingesado existe o no
				Security securityFind=null;
				try {
					securityFind = em.find(Security.class, security.getIdSecurityCodePk());
				} catch (Exception e) {
					existSecurity = false;
					e.printStackTrace();
				}
				
				// si el valor no existe, entonces salir inmendiatamente de las validaciones
				if(securityFind==null){
					existSecurity=false;
					//si el valor NO existe se habilitan todos los archivos para adjuntar
					loadParameterCertFileList();
					return;
				}else {
					existSecurity=true;
					
					//determinando el tipo derenta segun el valor obtenido
					if(Validations.validateIsNotNull(securityFind.getInstrumentType())){
						if(securityFind.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
							secIsFixedIncome = true;
						}else{
							secIsFixedIncome = false;
						}
					}
						
					
					//si el valor SI existe se habilita solo un archivo para adjuntar
					loadParameterCertFileList();
				}
				
				// si el valor existe se continua con las validaciones correspondientes
				security = transferAvailableFacade.getSecurity(security.getIdSecurityCodePk());
				regLstPhysicalCertMarketfacts = null;
				arrRegPhysicalCertMarketfact = null;
				regPhysicalCertMarketfact = new PhysicalCertMarketfact();
				
				if( Validations.validateIsNotNull(security) &&
						Validations.validateIsNotNull(security.getIdSecurityCodePk()) ){
						
						if(!security.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
							confirmAlertAction = "clearSecurity";
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID));
							/*alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_SECURITY_REGISTER,
									new Object[]{security.getIdSecurityCodePk()}));*/
							return;
						}
					
						// se valida si el valor ya fue depositado en la CAVAPY
						if(security.getPhysicalBalance().intValue()<=GeneralConstants.ZERO_VALUE_INT){
							Issuer issuer=certificateDepositeBeanFacade.getIssuerbySecurity(security.getIdSecurityCodePk());
							confirmAlertAction = "clearSecurity";
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_ALL_DEPOSITED,
													new Object[]{security.getIdSecurityCodePk(),issuer.getBusinessName()}));
							return;
						}
						
						// validando si el tipo de emision es correcto para esta operacion
						if(security.getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
							confirmAlertAction = "clearSecurity";
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ISSUER_TYPE_IN_SECURITY_INCORRECT,
													new Object[]{security.getIdSecurityCodePk()}));
							
							return;
						}
						
						// verificando si es DPF o no, aca se actualizan flag de: flagDPF
						if(security.getSecurityClass().equals(SecurityClassType.DPF.getCode())){
							List<PhysicalCertificate> lstPhysicalCert=certificateDepositeBeanFacade.getPhysicalCertificateBySecurity(security.getIdSecurityCodePk());
							if(Validations.validateIsNotNull(lstPhysicalCert) &&lstPhysicalCert.size()>0){
								confirmAlertAction = "clearSecurity";
								alert(PropertiesUtilities.getMessage(PropertiesConstants.PHYSICAL_CERTIFICATE_ERROR_EXIST_SECURITY));
								return;
							}
							flagDPF = true;
							CertificateQuantity=new BigDecimal(1);
							numberOfCertificates=1;
							certificateNumber = new Long(security.getIdSecurityCodeOnly().substring(4, 10));
						}else{
							flagDPF = false;
							CertificateQuantity=null;
						}
						
						if(security.getInstrumentType().equals(RENTA_FIJA)){
							flagRenderField = true;
						}else{
							flagRenderField = false;
						}
					}else{
						alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_ERROR_FINDING_ISINCODE));
					}	
				
					/** issue 188 - cargamos HM si solo tiene uno */
					holderMarketFactBalance = certificateDepositeBeanFacade.getHolderMarketFacts( security.getIdSecurityCodePk(), participantSelected, holderAccount.getIdHolderAccountPk() );    
					if (Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance)) {
						regPhysicalCertMarketfact.setMarketDate(holderMarketFactBalance.getMarketDate());
						regPhysicalCertMarketfact.setMarketPrice(holderMarketFactBalance.getMarketPrice());
						regPhysicalCertMarketfact.setMarketRate(holderMarketFactBalance.getMarketRate());
					} 
			}
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
	}    
    
	/**
	 * Instrument type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String instrumentTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return instrumentTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	/**
	 * Payment type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String paymentTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return paymentTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Money type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String moneyTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return moneyTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	/**
	 * Money type map full description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String moneyTypeMapFullDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return moneyTypeFullMap.get(code);
		}else{
			return "";
		}
	}
	
	

	/**
	 * Security class type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String securityClassTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return securityClassTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	/**
	 * Deposit request state type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String depositRequestStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return depositRequestStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Deposit state type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String depositStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return depositStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Reason state type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String reasonStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return reasonStateTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	
    /**
     * Search physical certificates.
     */
	@LoggerAuditWeb
	 public void searchPhysicalCertificates(){
    	try {

			loadListAndMap();
			
	    	PhysicalCertificateFilter physicalCertificateFilter = new PhysicalCertificateFilter();
	    	
	    	if(Validations.validateIsNotNullAndPositive(participantSelected))
	    		physicalCertificateFilter.setParticipantPk(participantSelected);
	    	
	    	if(Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk()))
	    		physicalCertificateFilter.setHolderAccountPk(holderAccount.getIdHolderAccountPk());
	    	
	    	if(Validations.validateIsNotNull(initialDate))
	    		physicalCertificateFilter.setInitialDate(initialDate);
	    	
	    	if(Validations.validateIsNotNull(finalDate))
	    		physicalCertificateFilter.setFinalDate(CommonsUtilities.addDate(finalDate,1));
			
	    	if(Validations.validateIsNotNullAndPositive(certificateSelected))
	    		physicalCertificateFilter.setCertificateNumber(certificateSelected);
	    	
	    	if(Validations.validateIsNotNullAndPositive(requestSelected))
	    		physicalCertificateFilter.setRequestNumber(requestSelected);
	    	
	    	if(Validations.validateIsNotNullAndPositive(statusSelected))
	    		physicalCertificateFilter.setStatus(statusSelected);
	    	
	    	if(Validations.validateIsNotNull(securitySearch))
	    		physicalCertificateFilter.setSecurity(securitySearch);
	    	
	    	if(Validations.validateIsNotNull(issuerSearchPK)){
	    		physicalCertificateFilter.setIssuerPk(issuerSearchPK);
	    	}
	    	
	    	if(Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()))
				physicalCertificateFilter.setCuiHolder(sourceHolder.getIdHolderPk());
	    	
	    	// issue 895: si en los depositos de titulos registrado ya existe el valor pues se 
	    	// setean los campos que dejamos en nulo (emisor,fechaEmision,ValorNominal,moneda,instrumentType)
	    	String idSecurityCodePkAux=null;
	    	Security securityAux=null;
	    	PhysicalCertificate physicalCertificateUpdated;
	    	List<PhysicalCertificateExt> physicalCertificateSearchListAux = certificateDepositeBeanFacade.getPhysicalCertificateSearchList(physicalCertificateFilter);
	    	for(PhysicalCertificateExt certificate : physicalCertificateSearchListAux){
	    		if(Validations.validateIsNotNull(certificate.getPhysicalCertificate().getIndExistsSecurity()) && 
	    				certificate.getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.NO.getCode())){
	    			idSecurityCodePkAux=certificateDepositeBeanFacade.getIdSecurityCodeNoExists(certificate.getPhysicalCertificate().getIdPhysicalCertificatePk());
	    			
	    			//obteniendo el Valor con su Emisor
	    			securityAux = certificateDepositeBeanFacade.getSecurityWithIssuer(idSecurityCodePkAux);
	    			if(Validations.validateIsNotNull(securityAux) && Validations.validateIsNotNull(securityAux.getIdSecurityCodePk())){
	    				
	    				//ISSUE 895: TODOS ESTOS CAMPOS SON LOS QUE DEBE TENER PHYSICAL_CERTIFICATE CUANDO EL VALOR YA EXISTE
	    				
	    				if(Validations.validateIsNotNull(securityAux.getIssuer()))
	    					certificate.getPhysicalCertificate().setIssuer(securityAux.getIssuer());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getIssuanceDate()))
	    					certificate.getPhysicalCertificate().setIssueDate(securityAux.getIssuanceDate());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getCurrentNominalValue()))
	    					certificate.getPhysicalCertificate().setNominalValue(securityAux.getCurrentNominalValue());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getCurrency()))
	    					certificate.getPhysicalCertificate().setCurrency(securityAux.getCurrency());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getInstrumentType()))
	    					certificate.getPhysicalCertificate().setInstrumentType(securityAux.getInstrumentType());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getSecurityClass()))
	    					certificate.getPhysicalCertificate().setSecurityClass(securityAux.getSecurityClass());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getExpirationDate()))
	    					certificate.getPhysicalCertificate().setExpirationDate(securityAux.getExpirationDate());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getPeriodicity()))
	    					certificate.getPhysicalCertificate().setPaymentPeriodicity(securityAux.getPeriodicity());
	    				
	    				if(Validations.validateIsNotNull(securityAux.getInterestRate()))
	    					certificate.getPhysicalCertificate().setInterestRate(securityAux.getInterestRate());
	    				
	    				// seteando indicador a 1 para indicar que ya existe el valor
	    				certificate.getPhysicalCertificate().setIndExistsSecurity(BooleanType.YES.getCode());
	    				
	    				physicalCertificateUpdated = certificate.getPhysicalCertificate();
	    				
	    				certificateDepositeBeanFacade.updatePhysicalCertificate(physicalCertificateUpdated);
//	    				em.merge(physicalCertificateUpdated);
//	    				em.flush();
	    			}
				}
	    	}
			
    		physicalCertificateSearchList = certificateDepositeBeanFacade.getPhysicalCertificateSearchList(physicalCertificateFilter);
    		
    		if(Validations.validateIsNotNull(physicalCertificateSearchList)){
    			if(physicalCertificateSearchList.size()>0){
    	
    				
    				for (PhysicalCertificateExt certificate : physicalCertificateSearchList) {
    					
    					// issue 895: si el valor no existe entonces se realiza una busqueda para setear su valor
    					idSecurityCodePkAux=certificateDepositeBeanFacade.getIdSecurityCodeNoExists(certificate.getPhysicalCertificate().getIdPhysicalCertificatePk());
    					certificate.getPhysicalCertificate().setIdSecurityCodePk(idSecurityCodePkAux);
    					if( Validations.validateIsNull(certificate.getPhysicalCertificate().getIndExistsSecurity()) || 
    							certificate.getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.YES.getCode())){
    						certificate.getPhysicalCertificate().setSecurities(em.find(Security.class, idSecurityCodePkAux));;
    					}
    					
    					certificate.setStateType(depositStateTypeMapDescription(certificate.getState()));
    					
    					if(certificate.getPhysicalCertificate().getSecurityClass() != null)
    						certificate.getPhysicalCertificate().setClassType(securityClassTypeMapDescription(certificate.getPhysicalCertificate().getSecurityClass()));
    					
    					if(certificate.getPhysicalOperationDetail().getState() != null)
    						certificate.getPhysicalCertificate().setStateType(depositStateTypeMapDescription(certificate.getPhysicalOperationDetail().getState()));
    					
    					if (certificate.getPhysicalOperationDetail().getRejectMotive() != null)
    						certificate.getPhysicalOperationDetail().setMotiveType(reasonStateTypeMapDescription(certificate.getPhysicalOperationDetail().getRejectMotive()));
    					
    					if (certificate.getPhysicalCertificate().getCurrency() != null)
    						certificate.getPhysicalCertificate().setCurrencyType(moneyTypeMap.get(certificate.getPhysicalCertificate().getCurrency()));
    					
//    					if (Validations.validateIsNotNullAndPositive(certificate.getParticipantPk())) {
//    						participantSelected = certificate.getParticipantPk();
//    						certificate.setMnemonic(findParticipantSelected().getMnemonic());
//    					}
    					
    					if(certificate.getRequestState() != null){
    						certificate.setRequestStateType(depositRequestStateTypeMapDescription(certificate.getRequestState()));
    					}
    					
    					if(certificate.getPhysicalOperationDetail().getIndApplied().equals(0)){
    						certificate.setIndApplied(false);
    					}else if(certificate.getPhysicalOperationDetail().getIndApplied().equals(1)){
    						certificate.setIndApplied(true);
    					}
    				}
    				
    				searchCertificateDataModel = new PhysicalCertificateExtDataModel(physicalCertificateSearchList);
    				registersFound = true;
    				
    				if( Validations.validateIsNotNullAndPositive(participant.getState()) &&
    					participant.getState().equals(ParticipantStateType.BLOCKED.getCode())
    				){
    					disabledButtons();
    				}else{
    					enabledButtons();	
    				}
    				
    			}else{
    				registersFound = false;
    				disabledButtons();
    			}
    		}else{
    			searchCertificateDataModel = new PhysicalCertificateExtDataModel();
				registersFound = false;
				disabledButtons();
    		}
			
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
    }
    
    /**
     * Question clear solicitud datos.
     */
    public void questionClearSolicitudDatos(){
    	if( existChangesToSearch() ||
			( physicalCertificateList != null && 
			physicalCertificateList.size() > 0 ) ||
			validateBackToScreen()
    	  ){
			String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_CLEAR_SCREEN);

    		question(headerMessage, bodyMessage);
    		confirmAlertAction="clearSolicitudDatos";
    	}
    }
    
    /**
     * Clear solicitud datos.
     */
    public void clearSolicitudDatos(){
    	clearSearchCriteria();
    	clearPopUpCertificate();
    	clearHeader();
    	physicalCertificateList = new ArrayList<PhysicalCertificate>();
    	certificateDataModel = new PhysicalCertificateDataModel();
    	securityEvents();
    	JSFUtilities.resetViewRoot();
    }
    
    /**
     * Clear search criteria.
     */
    public void clearSearchCriteria(){
    	certificateSelected = null;
		requestSelected = null;
		sourceHolder = new Holder();
		holderAccount = new HolderAccount();
		participantSelected = null;
		statusSelected = null;
		//issuerSelected = null;
		issuer = new Issuer();
		physicalCertificateSearchList = new ArrayList<PhysicalCertificateExt>();
		searchCertificateDataModel = new PhysicalCertificateExtDataModel();
		initialDate = CommonsUtilities.currentDate();
		finalDate = CommonsUtilities.currentDate();
		maxDate = CommonsUtilities.currentDate();
		registersFound = true;
		otherRejectMotiveId = ReasonType.OTRO.getCode();
		validatorDisabled = false;
		security = new Security();
		lstSourceAccountTo = null;
		sourceAccountTo = null;
		securitySearch=new Security();
		issuerHelp= new Issuer();
		issuerSearchPK = null;
		securityEvents();
    }
    
    /**
     * Enabled buttons.
     */
    public void enabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
    	
    	if(userPrivilege.getUserAcctions().isApprove())
			privilegeComponent.setBtnApproveView(true);
		
    	if(userPrivilege.getUserAcctions().isAnnular())
			privilegeComponent.setBtnAnnularView(true);
		
		if(userPrivilege.getUserAcctions().isApply())
			privilegeComponent.setBtnApply(true);
		
		if(userPrivilege.getUserAcctions().isConfirm())
			privilegeComponent.setBtnConfirmView(true);
		
		if(userPrivilege.getUserAcctions().isReject())
			privilegeComponent.setBtnRejectView(true);
		
		if(isDepositary)
			rejectConfirmEnabled = true;
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);	
    }
    
    /**
     * Enabled buttons with state.
     *
     * @param state the state
     */
    public void enabledButtonsWithState(Integer state){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
    	
    	if(state.equals(StateType.APPROVED.getCode())){
    		
        	if(userPrivilege.getUserAcctions().isApprove())
    			privilegeComponent.setBtnApproveView(true);
        	
    	}else if(state.equals(StateType.ANNULLED.getCode())){

        	if(userPrivilege.getUserAcctions().isAnnular())
    			privilegeComponent.setBtnAnnularView(true);
        	
    	}else if(state.equals(StateType.CONFIRMED.getCode())){

    		if(userPrivilege.getUserAcctions().isConfirm())
    			privilegeComponent.setBtnConfirmView(true);

    	}else if(state.equals(StateType.REJECTED.getCode())){

    		if(userPrivilege.getUserAcctions().isReject())
    			privilegeComponent.setBtnRejectView(true);
    		
    	}else if(state.equals(StateType.APPLY.getCode())) {

    		if(userPrivilege.getUserAcctions().isApply())
    			privilegeComponent.setBtnApply(true);
    		
    	}
		
    	userPrivilege.setPrivilegeComponent(privilegeComponent);	
    }
    
    /**
     * Disabled buttons.
     */
    public void disabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
	
		privilegeComponent.setBtnApproveView(false);
		privilegeComponent.setBtnAnnularView(false);
		privilegeComponent.setBtnApply(false);
		privilegeComponent.setBtnConfirmView(false);
		privilegeComponent.setBtnRejectView(false);
		rejectConfirmEnabled = false;
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);	
    }
    
    /**
     * Radio selection.
     */
    public void radioSelection(){
			fromCorrelativo = null;
			toCorrelativo = null;
			if(selectCertificateCount.intValue()==GeneralConstants.TWO_VALUE_INTEGER){
				if(flagDPF){
					String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
					String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_DPF_INTERVAL);
		    		alert(headerMessage,bodyMessage);
		    		selectCertificateCount=GeneralConstants.ONE_VALUE_INTEGER;
		    		return;
				}
			}
			numberOfCertificates = null;
			certificateNumber = null;
			CertificateQuantity = null;
			
				
			
	}
    
    /**
     * Gets the issuer on selection.
     *
     * @return the issuer on selection
     */
    public void getIssuerOnSelection(){
    	
    	//fillIssuer();
    	security = new Security();
    	physicalCertificate = new PhysicalCertificate();
		certificateNumber = null;
		CertificateQuantity = null;
		fromCorrelativo = null;
		toCorrelativo = null;
		numberOfCertificates = null;
		selectCertificateCount = new Integer(1);
		instrumentTypeSelected = -1;
		securityClassSelected = -1;
		securityClassList = new ArrayList<ParameterTable>();
		initialDate = CommonsUtilities.currentDate();
		finalDate = CommonsUtilities.currentDate();
		regLstPhysicalCertMarketfacts = null;
		regPhysicalCertMarketfact = new PhysicalCertMarketfact();
		arrRegPhysicalCertMarketfact = null;

    	JSFUtilities.resetComponent("frmCustody:outSourceSecurity");
    	
    	/*
		if(certificateNumber != null || fromCorrelativo != null || toCorrelativo != null ){
			CertificatesCountCal();
		}*/
    	
		removeRedAlerts();
	}
    
//    private void fillIssuer(){
//    	if(issuerSelected!=null){
////    		Issuer issuerFilter = new Issuer();
////    		issuerFilter.setIdIssuerPk(issuerSelected);
////    		
//    		issuer = helperComponentFacade.findIssuer(issuerSelected,null);
//    	} else {
//    		issuer = null;
//    	}  
//    }
    
    /**
     * Find issuer selected.
     *
     * @return the issuer
     */
    public Issuer findIssuerSelected( Security security) {
    	Issuer issuer=null;
    	try{
    		issuer=certificateDepositeBeanFacade.getIssuerbySecurity(security.getIdSecurityCodePk());
	    }catch (Exception e) {
	        excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return issuer;
	}
    
    public Issuer findIssuerSelected() {
    	Issuer issuer=null;
    	try{
    	issuer=certificateDepositeBeanFacade.getIssuerbySecurity(security.getIdSecurityCodePk());
	    }catch (Exception e) {
	        excepcion.fire(new ExceptionToCatchEvent(e));
		}
    	
		return issuer;
	}
    /**
     * Validate req field.
     *
     * @return true, if successful
     */
    public boolean validateReqField(){	
    	
		if( !Validations.validateIsNotNullAndPositive(securityClassSelected) ){
			return false;
		}else if( !Validations.validateIsNotNullAndPositive(instrumentTypeSelected) ){
			return false;
		}else if(instrumentTypeSelected.equals(RENTA_FIJA) &&  !Validations.validateIsNotNullAndPositive(physicalCertificate.getPaymentPeriodicity()) ){
			return false;
		}else if(instrumentTypeSelected.equals(RENTA_FIJA) && (physicalCertificate.getInterestRate() == null || physicalCertificate.getInterestRate().compareTo(new BigDecimal(0)) == 0) ){
			return false;
		}else if( !Validations.validateIsNotNullAndPositive(physicalCertificate.getCurrency()) ){
			return false;
		}else if(physicalCertificate.getNominalValue() == null || physicalCertificate.getNominalValue().compareTo(new BigDecimal(0)) == 0){
			return false;
		}else if(selectCertificateCount == 1){
			if(certificateNumber == null || certificateNumber.equals(new Long(0))){
				return false;
			}
		}else if(selectCertificateCount == 2){
			if(fromCorrelativo == null || fromCorrelativo.equals(new Integer(0))){
				return false;
			}if(toCorrelativo == null || toCorrelativo.equals(new Integer(0))){
				return false;
			}
		}
		
		return true;
	}
    
    /**
     * Back to screen.
     */
    public void backToScreen(){
    	//cancelToScreen();
    	backScreenMain();
        removeAlertNew();
    }
    
    /**
     * Cancel to screen.
     */
    
    public void cancelToScreen(){
		removeAlertPopUp();
		JSFUtilities.resetComponent(":frmCustody:panelRegistry");
		clearPopUpCertificate();
		clearHeader();
    }
    
    /**
     * Back screen main.
     */
    public void backScreenMain(){
    	removeAlertPopUp();
		JSFUtilities.resetComponent(":frmCustody:panelRegistry");
		clearPopUpCertificate();
		//clearHeader();
    }
    
    /**
     * Validate back to screen.
     *
     * @return true, if successful
     */
    public boolean validateBackToScreen(){
    	
    	if( Validations.validateIsNotNullAndPositive(securityClassSelected)  ||
    		Validations.validateIsNotNullAndPositive(instrumentTypeSelected) || 
    		Validations.validateIsNotNullAndPositive(physicalCertificate.getPaymentPeriodicity()) || 
    		Validations.validateIsNotNull(physicalCertificate.getInterestRate()) || 
    		Validations.validateIsNotNullAndPositive(physicalCertificate.getCurrency()) ||
    		Validations.validateIsNotNull(physicalCertificate.getNominalValue()) ||
    	//	Validations.validateIsNotNull(issuerSelected) ||
    		!finalDate.equals(CommonsUtilities.currentDate()) ||
    		!initialDate.equals(CommonsUtilities.currentDate())
    	){
    		return true;
		}
    	
    	return false;
    }
    
    /**
     * Removes the red alerts.
     */
    public void removeRedAlerts(){
		JSFUtilities.setValidViewComponentAndChildrens("frmCustody");
    }
    
    /**
     * Back to screen question.
     */
    public void backToScreenQuestion(){
    	removeAlertPopUp();
    	if(show){
    		backToScreen();
    	}else{
    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_CANCEL_SCREEN);
    		
    		if(modify){
    			
    			if(!validateReqField()){
    				question(headerMessage, bodyMessage);
    	    		confirmAlertAction="cancelToScreen";
    			}else if(verifyModification()){
    				question(headerMessage, bodyMessage);
    	    		confirmAlertAction="cancelToScreen";
    			}else{
    	    		cancelToScreen();
    	    		removeRedAlerts();
    			}
    		}else{//REGISTRO
    			boolean blModificated = verifyRegistry();
    			if( blModificated ||
        			!finalDate.equals(CommonsUtilities.currentDate()) ||
        			!initialDate.equals(CommonsUtilities.currentDate())
    			){
    				question(headerMessage, bodyMessage);
    	    		confirmAlertAction="cancelToScreen";
    			}else{
    				cancelToScreen();
    				removeRedAlerts();
    			}
    		}
    	}
    }
    
    /**
     * Display certificate register.
     *
     * @param physicalCertificateRegTemp the physical certificate reg temp
     */
    public void displayCertificateRegister(PhysicalCertificate physicalCertificateRegTemp){
		show = true;
		modify = true;
		physicalCertificate = physicalCertificateRegTemp;
		//issuerSelected = physicalCertificate.getIssuer().getIdIssuerPk();
		instrumentTypeSelected = physicalCertificate.getInstrumentType();
		certificateNumber = Long.valueOf(physicalCertificate.getCertificateNumber());
		securityClassSelected = physicalCertificate.getSecurityClass();
		initialDate = physicalCertificate.getIssueDate();
		
		if(instrumentTypeSelected.equals(RENTA_FIJA)){
			finalDate = physicalCertificate.getExpirationDate();
		}
		
		try {
			if(instrumentTypeSelected.equals(RENTA_VARIABLE))
				renderedInstrumentType=false;
			else
				renderedInstrumentType=true;
			for(PhysicalCertMarketfact objPhysicalCertMark : physicalCertificate.getPhysicalCertMarketfacts())
			{
				if(objPhysicalCertMark.getMarketPrice()==null)
					objPhysicalCertMark.setMarketPrice(new BigDecimal(GeneralConstants.ZERO_VALUE_INT));
				if(objPhysicalCertMark.getMarketRate()==null)
					objPhysicalCertMark.setMarketRate(new BigDecimal(GeneralConstants.ZERO_VALUE_INT));
			}
			
			securityClassList = certificateDepositeBeanFacade.securityClassFacade(instrumentTypeSelected, MasterTableType.SECURITIES_CLASS.getCode());
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		
		alerNewCertificate();
	}
    
    /**
     * Valid certificate quantity.
     */
    public void validCertificateQuantity(){
    	confirmAlertAction="removeAlertPopUp";
    	BigDecimal bdQuantityVal = null;
    	
    	// validacion de que se debe ingresar dato
    	if(CertificateQuantity==null){
    		CertificateQuantity = null;
    		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT));
    		return;
    	}

    	// validacion de que se debe ingresar dato
    	if( CertificateQuantity.compareTo(new BigDecimal(0)) == 0 ){
    		CertificateQuantity = null;
    		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT));
    		return;
    	}
    	
    	// si elvalor existe se ralizan las validaciones necesarias
    	if(existSecurity){
    		try {
        		
        		//validando si la cantida a depositar es vAlida
        		Security  securityBalance=new Security();
        		securityBalance = transferAvailableFacade.getSecurity(security.getIdSecurityCodePk());
//        		if( ((securityBalance.getPhysicalBalance().intValue()) - (securityBalance.getPhysicalDepositBalance().intValue())) >0){
        		if( CertificateQuantity.compareTo(securityBalance.getPhysicalBalance().subtract(securityBalance.getPhysicalDepositBalance()))>0){
            		CertificateQuantity = null;
            		
            		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_AMOUNT_DEPOSIT_EXCEED));
            		return;
            	}
    		} catch (ServiceException e) {
    			
    			e.printStackTrace();
    		}
        	
        	Integer tempAmount=GeneralConstants.ZERO_VALUE_INTEGER;
        	if(Validations.validateIsNotNull(physicalCertificateList) && physicalCertificateList.size()>0)
        	{
        		for(PhysicalCertificate  obj : physicalCertificateList)
        		{
        			if(obj.getSecurities().getIdSecurityCodePk().equals(security.getIdSecurityCodePk()))
        			{
        				tempAmount+=obj.getCertificateQuantity().intValue();
        			}
        		}
        	}
        	if(Validations.validateIsNotNull(security)
        			&& Validations.validateIsNotNull(security.getIdSecurityCodePk()))
        	{
        		if(toCorrelativo != null && fromCorrelativo != null){
        			List<Long> certificatesNumsList = generateLisCertificates(toCorrelativo, fromCorrelativo);
        			bdQuantityVal = new BigDecimal(CertificateQuantity.intValue() * certificatesNumsList.size());
        			if(bdQuantityVal.compareTo(security.getAvailablePhysicalBalance().subtract(new BigDecimal(tempAmount.toString()))) > 0)
        	    	{
        	    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
        	    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT);
        	    		showMessageOnDialog(headerMessage,bodyMessage); 
        	    		JSFUtilities.showSimpleValidationDialog();
        	    		CertificateQuantity=null;
        	    		return;
        	    	}
        		}
        	//	else if(((security.getAvailablePhysicalBalance().intValue()) - (tempAmount.intValue())) > 0) {
        		else if(CertificateQuantity.compareTo(security.getAvailablePhysicalBalance().subtract(new BigDecimal(tempAmount.toString()))) > 0) {
    	    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    	    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT);
    	    		showMessageOnDialog(headerMessage,bodyMessage); 
    	        	//JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
    	    		JSFUtilities.showSimpleValidationDialog();
//    	    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
//    						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT));
//    				JSFUtilities.showValidationDialog();
    	    		//CertificateQuantity=null;
    	    		return;
    	    	}
        	}
        	else
        	{
        		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
        		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MANAGE_NOT_SECURITY);
        		alert(headerMessage,bodyMessage);
        		CertificateQuantity=null;
        		return;
        	}
    	}
    	
    	confirmAlertAction="";
    }
    
    /**
     * Adds the certificate confirm.
     */
    //H
    @LoggerAuditWeb
    public void addCertificateConfirm(){
    	
    	//issue 895: validar que se suban todos los archivos requeridos
    	if(existSecurity){
    		if(lstFileTypes.size() != lstPhysicalCertFile.size()){
    			alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_ATTACHED));
    			return;
    		}
    	}else{
    		if(lstFileTypes.size() != lstPhysicalCertFile.size()){
    			// si el valor no existe se valida que se suban por lo menos 6 de los 7 archivos
        		if((lstFileTypes.size()-1) != lstPhysicalCertFile.size()){
        			alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_ATTACHED));
        			return;
        		}else{
        			// pero si se detecta que el archivo OTROS(opcional) esta presente en la lista de adjuntos significa que aun quedan archivos por completar
        			for(PhysicalCertFile fc: lstPhysicalCertFile)
        				if(fc.getDocumentType().equals(ParameterFileType.OTROS.getCode())){
        					alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_ATTACHED));
        	    			return;		
        				}
        		}
    		}
    	}
    	
//    	188 similar a desmaterializacion
    	// validando que se hayan ingresado los hechos de mercado
    	if(Validations.validateIsNotNullAndNotEmpty(regPhysicalCertMarketfact.getMarketDate()) 
    			&& (Validations.validateIsNotNull(security) &&   
        			 (Validations.validateIsNotNullAndNotEmpty(regPhysicalCertMarketfact.getMarketRate()) || Validations.validateIsNotNullAndNotEmpty(regPhysicalCertMarketfact.getMarketPrice()))
        			)
    			)
    	{    		
    		if(regLstPhysicalCertMarketfacts != null && regLstPhysicalCertMarketfacts.size()>=1){
        		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_MARKETFACT_EXIST));
    			return;
    		}
    		
    		if(CertificateQuantity!=null)
				regPhysicalCertMarketfact.setQuantity(CertificateQuantity);
    		else
    			regPhysicalCertMarketfact.setQuantity(new BigDecimal(0));
	        	regPhysicalCertMarketfact.setRegistryUser(userInfo.getUserAccountSession().getUserName());
	        	regPhysicalCertMarketfact.setRegistryDate(CommonsUtilities.currentDate());
        	
        	if(regLstPhysicalCertMarketfacts==null || regLstPhysicalCertMarketfacts.size()<=0){
        		regLstPhysicalCertMarketfacts = new ArrayList<PhysicalCertMarketfact>();
        	}
        	regLstPhysicalCertMarketfacts.add(regPhysicalCertMarketfact);
        	//regPhysicalCertMarketfact = new PhysicalCertMarketfact();
        	
    	} else {
      		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT));
      		return;
    	}
//    	-----------------------
    	
    	confirmAlertAction="removeAlertPopUp";
    	BigDecimal bdQuantityVal = null;
    	if( CertificateQuantity.compareTo(new BigDecimal(0)) == 0 ){
    		CertificateQuantity = null;
    	}
    	Integer tempAmount=GeneralConstants.ZERO_VALUE_INTEGER;
    	//calculando la cantidad de saldo utilizado
    	if(Validations.validateIsNotNull(physicalCertificateList) && physicalCertificateList.size()>0)
    	{
    		for(PhysicalCertificate  obj : physicalCertificateList)
    		{
    			if(obj.getSecurities().getIdSecurityCodePk().equals(security.getIdSecurityCodePk()))
    			{
    				tempAmount+=obj.getCertificateQuantity().intValue();
    			}
    		}
    	}
    	
    	//solo si el valor existe se realiza esta validacion
    	if(existSecurity)
	    	if(Validations.validateIsNotNull(security) && Validations.validateIsNotNull(security.getIdSecurityCodePk()))
	    	{
	    		if(toCorrelativo != null && fromCorrelativo != null){
	    			List<Long> certificatesNumsList = generateLisCertificates(toCorrelativo, fromCorrelativo);
	    			
	    			bdQuantityVal=new BigDecimal(CertificateQuantity.intValue()*certificatesNumsList.size());
	    			if(bdQuantityVal.intValue()>security.getAvailablePhysicalBalance().intValue()-tempAmount)
	    	    	{
	    	    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
	    	    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT);
	    	    		showMessageOnDialog(headerMessage,bodyMessage); 
	    	    		JSFUtilities.showSimpleValidationDialog();
	    	    		return;
	    	    	}
	    		} else // validando que la cantidad a depositar no exceda la cantidad de valores ya depositasdos
	    			if(Validations.validateIsNotNullAndNotEmpty(CertificateQuantity) && 
	    				(CertificateQuantity.intValue() > (security.getAvailablePhysicalBalance().intValue() - tempAmount)))
		    	{
		    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
		    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT);
		    		showMessageOnDialog(headerMessage,bodyMessage); 
		    		JSFUtilities.showSimpleValidationDialog();
		    		return;
		    	}
	    	}
	    	else
	    	{
	    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
	    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MANAGE_NOT_SECURITY);
	    		alert(headerMessage,bodyMessage);
	    		return;
	    	}
    	///
    	////
    	
    	String headerMessage="";
    	String bodyMessage="";
    	String strCerts = "";
    	
    	// validando que se haya ingresado los hechos de mercado
    	if(regLstPhysicalCertMarketfacts == null || regLstPhysicalCertMarketfacts.size()<=0){
    		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT));
    		return;
    	}
    	
    	// validando que se haya ingresado la cant de valores a depositar correcto
    	if(CertificateQuantity == null || CertificateQuantity.equals(BigDecimal.ZERO)){
    		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_COUNT_SECURITY_INVALID));
    		return;
    	}    	
    	if(regLstPhysicalCertMarketfacts.get(0).getQuantity()==null || regLstPhysicalCertMarketfacts.get(0).getQuantity().intValue()==GeneralConstants.ZERO_VALUE_INT){
    		regLstPhysicalCertMarketfacts.get(0).setQuantity(CertificateQuantity);
    	}
    	
    	strCerts = certificateNumber.toString();
//		if(selectCertificateCount == 1){
//			if(Validations.validateIsNotNull(certificateNumber)){
//				strCerts = certificateNumber.toString();
//			}			
//		}else if(selectCertificateCount == 2){
//			//List<Long> lstCerts = generateLisCertificates(toCorrelativo, fromCorrelativo);
//			//strCerts = certificateNumberMessage(lstCerts);
//			strCerts = fromCorrelativo + " al " + toCorrelativo;
//		}
		if(flagDPF)
		{
			if(Validations.validateIsNotNull(physicalCertificateList) && physicalCertificateList.size()>0)
				for(PhysicalCertificate objPysicalCertificate: physicalCertificateList){
					// validando que no se haya regitrado el mismo numero de certificado
					if( Long.valueOf(objPysicalCertificate.getCertificateNumber()).longValue()==certificateNumber.longValue()){
						headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_DPF_REGAPPROVE,
			    															new Object[]{certificateNumber.toString()});
			    		alert(headerMessage,bodyMessage);
			    		return;
					}
				}
		}
    		
		// si la validacion fue correcta se lanza el mensaje de confirmacion
		confirmAlertAction="addPhysicalCertificate";
		//headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_CONFIRM);LBL_HEADER_ALERT_WARNING
		headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_ADD,new Object[]{strCerts});
		question(headerMessage, bodyMessage);
    }
    
    /**
     * Valid certificate quantity confirm.
     */
    public void validCertificateQuantityConfirm(){
    	confirmAlertAction="removeAlertPopUp";
    	BigDecimal bdQuantityVal = null;
    	if( CertificateQuantity.compareTo(new BigDecimal(0)) == 0 ){
    		CertificateQuantity = null;
    	}
    	Integer tempAmount=GeneralConstants.ZERO_VALUE_INTEGER;
    	if(Validations.validateIsNotNull(physicalCertificateList) && physicalCertificateList.size()>0)
    	{
    		for(PhysicalCertificate  obj : physicalCertificateList)
    		{
    			if(obj.getSecurities().getIdSecurityCodePk().equals(security.getIdSecurityCodePk()))
    			{
    				tempAmount+=obj.getCertificateQuantity().intValue();
    			}
    		}
    	}
    	if(Validations.validateIsNotNull(security)
    			&& Validations.validateIsNotNull(security.getIdSecurityCodePk()))
    	{
    		if(toCorrelativo != null && fromCorrelativo != null){
    			List<Long> certificatesNumsList = generateLisCertificates(toCorrelativo, fromCorrelativo);
    			bdQuantityVal=new BigDecimal(CertificateQuantity.intValue()*certificatesNumsList.size());
    			if(bdQuantityVal.intValue()>security.getAvailablePhysicalBalance().intValue()-tempAmount)
    	    	{
    	    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    	    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT);
    	    		showMessageOnDialog(headerMessage,bodyMessage); 
    	    		JSFUtilities.showSimpleValidationDialog();
    	    		CertificateQuantity=null;
    	    		return;
    	    	}
    		}
    		else if(CertificateQuantity.intValue()>security.getAvailablePhysicalBalance().intValue()-tempAmount)
	    	{
	    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
	    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT);
	    		showMessageOnDialog(headerMessage,bodyMessage); 
	        	//JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	    		JSFUtilities.showSimpleValidationDialog();
//	    		JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
//						, PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT));
//				JSFUtilities.showValidationDialog();
	    		CertificateQuantity=null;
	    		return;
	    	}
    	}
    	else
    	{
    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MANAGE_NOT_SECURITY);
    		alert(headerMessage,bodyMessage);
    		CertificateQuantity=null;
    		return;
    	}
    	//JSFUtilities.updateComponent("frmCustody:idCertificateQuantity");
    	confirmAlertAction="";
    }
    
    /**
     * Load holder account state.
     *
     * @throws ServiceException the service exception
     */
    private void loadHolderAccountState() throws ServiceException{
		lstHolderAccountState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstHolderAccountState.add(param);
			mapStateHolderAccount.put(param.getParameterTablePk(), param.getDescription());
		}
	}
    
    
    /**
     * Gets the map holder account state.
     *
     * @param code the code
     * @return the map holder account state
     */
    public String getMapHolderAccountState(Integer code){
		return mapStateHolderAccount.get(code);
	}
    
	/**
	 * Valid source holder.
	 */
	public void validSourceHolder(){
	
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())){
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sourceHolder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(participantSelected);
			
			try {
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if(lstAccountTo.size()>0){
					if(lstAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
						sourceAccountTo=lstAccountTo.get(0);
						validSourceHolderAccount();
					}
					lstSourceAccountTo = lstAccountTo;
				}else{
					sourceHolder = new Holder();
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = new HolderAccountHelperResultTO();
					String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT);
					alert(mensaje);
				}
				
			} catch (ServiceException e) {
				
				e.printStackTrace();
			}
			
		}else{
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		}
	}
	
	/**
	 * Valid source holder account.
	 */
	public void validSourceHolderAccount(){
		
		if(Validations.validateIsNotNull(sourceAccountTo) && Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountNumber()) ){
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(sourceAccountTo.getAccountPk());
			this.holderAccount = holderAccount;
		
			if(!(Validations.validateIsNotNull(this.holderAccount) &&
				 Validations.validateIsNotNullAndPositive(this.holderAccount.getIdHolderAccountPk())) ){
				return;
			}
				
			holderAccount = getHolderAccountAndDetails(this.holderAccount);
			
			
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED));
				
				this.holderAccount = new HolderAccount();
				sourceAccountTo = new HolderAccountHelperResultTO();
			}
			
			
			this.holderAccount = holderAccount;
		
		}else{
			
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_NOT_SELECTED));
		}
	}
	
    /**
     * Show save confirmation.
     */
    @LoggerAuditWeb
    public void showSaveConfirmation() {
    	removeAlertPopUp();
    	String headerMessage="";
    	String bodyMessage="";
		
			if (physicalCertificateList != null) {
				if(Validations.validateIsNotNullAndPositive(participantSelected)){
					if(Validations.validateIsNotNullAndNotEmpty(holderAccount.getAccountNumber())){
							confirmAlertAction="saveAllPhysicalCertificates";
							headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
				    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_SAVE);
				    		question(headerMessage, bodyMessage);
					}else{
						headerMessage=PropertiesUtilities.getMessage(PropertiesConstants.MESSAGES_ALERT);
			    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_DATA_REQUIRED);
			    		alert(headerMessage, bodyMessage);
					}
				}else{	
					headerMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_PARETICIPANT_REQUIRE);
		    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERR_MSG_PARTICIPANT);
		    		alert(headerMessage, bodyMessage);
				}
			}
	}
    
    /**
     * Save all physical certificates.
     */
    @LoggerAuditWeb
	public void saveAllPhysicalCertificates() {

		try{
			if(validateCertificaSave()){
				return ;
			}
			
			//CUSTODIA
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
			custodyOperation.setOperationType(new Long(RequestType.DEPOSITEO.getCode()));
			custodyOperation.setState(StateType.REGISTERED.getCode());
			custodyOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
			//PHYSICAL_CERTIFICATE_OPERATION
			Participant participant = new Participant();
			participant.setIdParticipantPk(participantSelected);
			
			PhysicalCertificateOperation physicalCertificateOperation = new PhysicalCertificateOperation();
			physicalCertificateOperation.setHolderAccount(holderAccount);
			physicalCertificateOperation.setParticipant(participant);
			physicalCertificateOperation.setState(StateType.REGISTERED_CERTIFICATE.getCode()); 
			physicalCertificateOperation.setPhysicalOperationType(new Long(RequestType.DEPOSITEO.getCode()));
			physicalCertificateOperation.setCustodyOperation(custodyOperation);
			physicalCertificateOperation.setIndApplied(new Integer(0));
			

			List<PhysicalOperationDetail> physicalOperationDetails = new ArrayList<PhysicalOperationDetail>();
			List<Long> certificates = new ArrayList<Long>();
			
			for (PhysicalCertificate certificate : physicalCertificateList) {

				certificates.add( Long.valueOf(certificate.getCertificateNumber()) );
				//PHYSICAL_CERTIFICATE
				certificate.setState(StateType.REGISTERED.getCode());
				certificate.setRegisterDate(CommonsUtilities.currentDateTime());
				certificate.setSituation(SituationType.PARTICIPANT.getCode());
				certificate.setRegisterUser(userInfo.getUserAccountSession().getUserName());
				
				//PHYSICAL_OPERATION_DETAIL
				PhysicalOperationDetail physicalOperationDetail = new PhysicalOperationDetail();
				physicalOperationDetail.setPhysicalCertificateOperation(physicalCertificateOperation);
				physicalOperationDetail.setPhysicalCertificate(certificate);
				physicalOperationDetail.setIndApplied(0);
				physicalOperationDetail.setState(StateType.REGISTERED.getCode());
				physicalOperationDetail.setRegistryDate(CommonsUtilities.currentDateTime());
				physicalOperationDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				
				//DETALLES A GRABAR
				physicalOperationDetails.add(physicalOperationDetail);
			}
			
			physicalCertificateOperation.setPhysicalOperationDetails(physicalOperationDetails);
			//GRABO EN CASCADA LOS DATOS
			physicalCertificateOperation = certificateDepositeBeanFacade.savePhysicalCertificateOperation(physicalCertificateOperation,physicalCertificateList);
			
			requestNumber = physicalCertificateOperation.getCustodyOperation().getOperationNumber();
			
			String certs = certificateNumberMessage(certificates);
			String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_MESSAGE, new Object[] {certs,String.valueOf(requestNumber)});
			alert(headerMessage, bodyMessage);
			
			physicalCertificateList = null;
			
			

			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REGISTER.getCode());
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess,  
						   null, null);
			}else if(isBcr){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   null, null);
			}else if(isParticipant){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   participantSelected, null);
			}
			/*** end JH NOTIFICACIONES ***/

			
			
		}catch (Exception e) {
			if(e instanceof ServiceException){
				ServiceException serviceException = (ServiceException) e;
				if(  serviceException.getErrorService().equals(ErrorServiceType.CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY) 	){
					String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
					String bodyMessage = PropertiesUtilities.getMessage(ErrorServiceType.CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY.getMessage(), serviceException.getParams());
					alert(headerMessage, bodyMessage);

					return;
				}
			}
			
			excepcion.fire(new ExceptionToCatchEvent(e));  
		}
		
	}
    
    /**
     * Adds the reg market fact. no se usa desde el issue 188
     */
    public void addRegMarketFact(){
    	if(Validations.validateIsNotNullAndNotEmpty(regPhysicalCertMarketfact.getMarketDate()) && 
    		((Validations.validateIsNotNull(security) &&  Validations.validateIsNotNullAndPositive(security.getInstrumentType()) &&
    		    security.getInstrumentType() != 124 && Validations.validateIsNotNullAndNotEmpty(regPhysicalCertMarketfact.getMarketPrice())) ||
    		  (Validations.validateIsNotNull(security) &&  Validations.validateIsNotNullAndPositive(security.getInstrumentType()) && 
    			security.getInstrumentType() == 124 && Validations.validateIsNotNullAndNotEmpty(regPhysicalCertMarketfact.getMarketRate()))))
    	{    		
    		if(regLstPhysicalCertMarketfacts != null && regLstPhysicalCertMarketfacts.size()>=1){
        		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_MARKETFACT_EXIST));
    			return;
    		}
    		
    		if(CertificateQuantity!=null)
				regPhysicalCertMarketfact.setQuantity(CertificateQuantity);
    		else
    			regPhysicalCertMarketfact.setQuantity(new BigDecimal(0));
	        	regPhysicalCertMarketfact.setRegistryUser(userInfo.getUserAccountSession().getUserName());
	        	regPhysicalCertMarketfact.setRegistryDate(CommonsUtilities.currentDate());
        	
        	if(regLstPhysicalCertMarketfacts==null || regLstPhysicalCertMarketfacts.size()<=0){
        		regLstPhysicalCertMarketfacts = new ArrayList<PhysicalCertMarketfact>();
        	}
        	regLstPhysicalCertMarketfacts.add(regPhysicalCertMarketfact);
        	regPhysicalCertMarketfact = new PhysicalCertMarketfact();
        	
    	} else {
      		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT));
    	}
    }
    
    /**
     * Removes the marketfact.
     * no usado desde issue 188
     * @param regPhysicalCertMarketfact the reg physical cert marketfact 
     */
    public void removeMarketfact(PhysicalCertMarketfact regPhysicalCertMarketfact){
    	regLstPhysicalCertMarketfacts.remove(regPhysicalCertMarketfact);
    	if (Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance)) {
	    	holderMarketFactBalance.setMarketDate(null);
			holderMarketFactBalance.setMarketPrice(null);
			holderMarketFactBalance.setMarketRate(null);
		}
    }
    
    public void removeMarketfactDate(){
    	if (Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance)) {
			holderMarketFactBalance.setMarketDate(null);
		}
    }
    public void removeMarketfactPrice(){
    	if (Validations.validateIsNotNullAndNotEmpty(holderMarketFactBalance)) {
			holderMarketFactBalance.setMarketPrice(null);
		}
    }
    
        
    /**
    * Adds the physical certificate.
    */
   /*@LoggerAuditWeb
    public void addPhysicalCertificate() {
		
		quantityBS = 0;
		quantityUSD = 0;
		quantityEUR = 0;
		quantityUFV = 0;
		quantityMVD = 0;

		sumBS = new BigDecimal(0);
		sumUSD = new BigDecimal(0);
		sumEUR= BigDecimal.ZERO;
		sumUFV= BigDecimal.ZERO;
		sumMVD= BigDecimal.ZERO;
		physicalCertificates = null;
    	String strCerts = "";
    	Integer couponCount = 0;
    	certFileNameDisplay = null;
    	
		if(physicalCertificateList == null || physicalCertificateList.size()<=0){
			physicalCertificateList = new ArrayList<PhysicalCertificate>();
		}
		
		try {
			if(existSecurity)
				couponCount = transferAvailableFacade.getCouponCountWithSecurity(security.getIdSecurityCodePk());
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
		
		if (fromCorrelativo != null && toCorrelativo != null) {
			for (int i = fromCorrelativo; i <= toCorrelativo; i++) {
				physicalCertificate = new PhysicalCertificate();
				physicalCertificate.setCertificateNumber(new Long(i)+"");
				physicalCertificate.setSecurityClass(security.getSecurityClass());
				physicalCertificate.setClassType(securityClassTypeMapDescription(security.getSecurityClass()));
				physicalCertificate.setInstrumentType(security.getInstrumentType());
				physicalCertificate.setCurrencyType(moneyTypeMap.get(security.getCurrency()));
				physicalCertificate.setCurrency(security.getCurrency());
				physicalCertificate.setIssuer(findIssuerSelected(security));
				physicalCertificate.setSecurities(this.security);
				physicalCertificate.setCertificateQuantity(this.CertificateQuantity);
				physicalCertificate.setCouponCount(couponCount);
				physicalCertificate.setIssueDate(security.getIssuanceDate());
				physicalCertificate.setNominalValue(security.getCurrentNominalValue());
				physicalCertificate.setCertificateDate(certificateDate);
				//RENTA FIJA
				if(security.getInterestRate() != null){
					physicalCertificate.setInterestRate(security.getInterestRate());
				}
				if(security.getPeriodicity() != null){
					physicalCertificate.setPaymentPeriodicity(security.getPeriodicity());
				}
				if( Validations.validateIsNotNullAndPositive(security.getPeriodicity()) ){
					physicalCertificate.setPaymentType(paymentTypeMapDescription(physicalCertificate.getPaymentPeriodicity()));
				}
				if(security.getExpirationDate() != null){
					physicalCertificate.setExpirationDate(security.getExpirationDate());
				}
				
				List<PhysicalCertMarketfact> lstTmp = new ArrayList<PhysicalCertMarketfact>();
				for(PhysicalCertMarketfact obj: regLstPhysicalCertMarketfacts){
					PhysicalCertMarketfact physicalMarketfact = new PhysicalCertMarketfact();
					physicalMarketfact.setMarketDate(obj.getMarketDate());
					physicalMarketfact.setMarketPrice(obj.getMarketPrice());
					physicalMarketfact.setMarketRate(obj.getMarketRate());
					physicalMarketfact.setRegistryDate(obj.getRegistryDate());
					physicalMarketfact.setRegistryUser(obj.getRegistryUser());
					physicalMarketfact.setQuantity(obj.getQuantity());
					physicalMarketfact.setPhysicalCertificate(physicalCertificate);
					lstTmp.add(physicalMarketfact);
				}
				physicalCertificate.setPhysicalCertMarketfacts(lstTmp);
				physicalCertificateList.add(physicalCertificate);
			}

			regLstPhysicalCertMarketfacts = null;
			regPhysicalCertMarketfact = new PhysicalCertMarketfact();
			arrRegPhysicalCertMarketfact = null;
			
			//List<Long> lstCerts = generateLisCertificates(toCorrelativo, fromCorrelativo);
			//strCerts = certificateNumberMessage(lstCerts);
			strCerts = fromCorrelativo + " al " + toCorrelativo;
			
		} else {
			if(existSecurity){
				physicalCertificate = new PhysicalCertificate();
				
				physicalCertificate.setIndExistsSecurity(BooleanType.YES.getCode());
				physicalCertificate.setCurrencyType(moneyTypeMap.get(security.getCurrency()));
				physicalCertificate.setIssuer(findIssuerSelected(security));
				physicalCertificate.setSecurityClass(security.getSecurityClass());
				physicalCertificate.setCurrency(security.getCurrency());
				physicalCertificate.setInstrumentType(security.getInstrumentType());
				physicalCertificate.setClassType(securityClassTypeMapDescription(security.getSecurityClass()));
				physicalCertificate.setIssueDate(security.getIssuanceDate());
				physicalCertificate.setNominalValue(security.getCurrentNominalValue());
				physicalCertificate.setSecurities(this.security);
				physicalCertificate.setCertificateQuantity(this.CertificateQuantity);
				physicalCertificate.setCertificateNumber(certificateNumber+"");
				physicalCertificate.setCouponCount(couponCount);
				physicalCertificate.setCertificateDate(certificateDate);
				physicalCertificate.setPhysicalCertFileList(lstPhysicalCertFile);
				//RENTA FIJA
				if(security.getInterestRate() != null){
					physicalCertificate.setInterestRate(security.getInterestRate());
				}
				if(security.getPeriodicity() != null){
					physicalCertificate.setPaymentPeriodicity(security.getPeriodicity());
				}
				if( Validations.validateIsNotNullAndPositive(security.getPeriodicity()) ){
					physicalCertificate.setPaymentType(paymentTypeMapDescription(physicalCertificate.getPaymentPeriodicity()));
				}
				if(security.getExpirationDate() != null){
					physicalCertificate.setExpirationDate(security.getExpirationDate());
				}
			}else{
				// cuando el valor no existe
				physicalCertificate = new PhysicalCertificate();
				
				physicalCertificate.setIndExistsSecurity(BooleanType.NO.getCode());
				//physicalCertificate.setCurrencyType(moneyTypeMap.get(security.getCurrency()));
				//physicalCertificate.setIssuer(findIssuerSelected());
				physicalCertificate.setSecurityClass(security.getSecurityClass());
				//physicalCertificate.setCurrency(security.getCurrency());
				//physicalCertificate.setInstrumentType(security.getInstrumentType());
				physicalCertificate.setClassType(securityClassTypeMapDescription(security.getSecurityClass()));
				//physicalCertificate.setIssueDate(security.getIssuanceDate());
				//physicalCertificate.setNominalValue(security.getCurrentNominalValue());
				physicalCertificate.setSecurities(this.security);
				physicalCertificate.setCertificateQuantity(this.CertificateQuantity);
				physicalCertificate.setCertificateNumber(certificateNumber+"");
				physicalCertificate.setCouponCount(couponCount);
				physicalCertificate.setCertificateDate(certificateDate);
				physicalCertificate.setPhysicalCertFileList(lstPhysicalCertFile);
			}
			
			for(PhysicalCertMarketfact obj: regLstPhysicalCertMarketfacts){
				obj.setPhysicalCertificate(physicalCertificate);
			}
			
			physicalCertificate.setPhysicalCertMarketfacts(regLstPhysicalCertMarketfacts);
			regLstPhysicalCertMarketfacts = null;
			regPhysicalCertMarketfact = new PhysicalCertMarketfact();
			arrRegPhysicalCertMarketfact = null;
			physicalCertificateList.add(physicalCertificate);
		
			strCerts = certificateNumber.toString();
		}
		
		certificateDataModel = new PhysicalCertificateDataModel(physicalCertificateList);
		if(physicalCertificateList != null && physicalCertificateList.size()>0){
			for (PhysicalCertificate certificate : physicalCertificateList) {
				BigDecimal nominalValue = null;
				
				if(certificate.getNominalValue()!=null){
					nominalValue = certificate.getNominalValue().setScale(2,RoundingMode.HALF_UP);
				}else{
					nominalValue = new BigDecimal(0);
				}
				if(Validations.validateIsNotNull(certificate.getCurrency())){
					if(certificate.getCurrency().equals(CurrencyType.PYG.getCode())){
						quantityBS++;
						sumBS = sumBS.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);					
					}
					if(certificate.getCurrency().equals(CurrencyType.USD.getCode())){
						quantityUSD++;
						sumUSD = sumUSD.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);					
					}
					if(certificate.getCurrency().equals(CurrencyType.EU.getCode())){
						quantityEUR++;
						sumEUR = sumEUR.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);
					}
					if(certificate.getCurrency().equals(CurrencyType.UFV.getCode())){
						quantityUFV++;
						sumUFV = sumUFV.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);
					}
					if(certificate.getCurrency().equals(CurrencyType.DMV.getCode())){
						quantityMVD++;
						sumMVD = sumMVD.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);
					}
				}
			}
		}
		clearPopUpCertificate();
		validatorDisabled = false;
		
		String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_ADD_SUCCESS,new Object[]{strCerts});
		alert(headerMessage,bodyMessage);
    }*/
    @LoggerAuditWeb
    public void addPhysicalCertificate() {
		
		quantityBS = 0;
		quantityUSD = 0;
		quantityEUR = 0;
		quantityUFV = 0;
		quantityMVD = 0;

		sumBS = new BigDecimal(0);
		sumUSD = new BigDecimal(0);
		sumEUR= BigDecimal.ZERO;
		sumUFV= BigDecimal.ZERO;
		sumMVD= BigDecimal.ZERO;
		physicalCertificates = null;
    	String strCerts = "";
    	Integer couponCount = 0;
    	
		if(physicalCertificateList == null || physicalCertificateList.size()<=0){
			physicalCertificateList = new ArrayList<PhysicalCertificate>();
		}
		
		try {
			couponCount = transferAvailableFacade.getCouponCountWithSecurity(security.getIdSecurityCodePk());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (fromCorrelativo != null && toCorrelativo != null) {
			for (int i = fromCorrelativo; i <= toCorrelativo; i++) {
				physicalCertificate = new PhysicalCertificate();
				physicalCertificate.setCertificateNumber(new Integer(i).toString());
				physicalCertificate.setSecurityClass(security.getSecurityClass());
				physicalCertificate.setClassType(securityClassTypeMapDescription(security.getSecurityClass()));
				physicalCertificate.setInstrumentType(security.getInstrumentType());
				physicalCertificate.setCurrencyType(moneyTypeMap.get(security.getCurrency()));
				physicalCertificate.setCurrency(security.getCurrency());
				physicalCertificate.setIssuer(findIssuerSelected());
				physicalCertificate.setSecurities(this.security);
				physicalCertificate.setCertificateQuantity(this.CertificateQuantity);
				physicalCertificate.setCouponCount(couponCount);
				physicalCertificate.setIssueDate(security.getIssuanceDate());
				physicalCertificate.setNominalValue(security.getCurrentNominalValue());
				//RENTA FIJA
				if(security.getInterestRate() != null){
					physicalCertificate.setInterestRate(security.getInterestRate());
				}
				if(security.getPeriodicity() != null){
					physicalCertificate.setPaymentPeriodicity(security.getPeriodicity());
				}
				if( Validations.validateIsNotNullAndPositive(security.getPeriodicity()) ){
					physicalCertificate.setPaymentType(paymentTypeMapDescription(physicalCertificate.getPaymentPeriodicity()));
				}
				if(security.getExpirationDate() != null){
					physicalCertificate.setExpirationDate(security.getExpirationDate());
				}
				
				/*List<PhysicalCertMarketfact> lstTmp = new ArrayList<PhysicalCertMarketfact>();
				for(PhysicalCertMarketfact obj: regLstPhysicalCertMarketfacts){
					PhysicalCertMarketfact physicalMarketfact = new PhysicalCertMarketfact();
					physicalMarketfact.setMarketDate(obj.getMarketDate());
					physicalMarketfact.setMarketPrice(obj.getMarketPrice());
					physicalMarketfact.setMarketRate(obj.getMarketRate());
					physicalMarketfact.setRegistryDate(obj.getRegistryDate());
					physicalMarketfact.setRegistryUser(obj.getRegistryUser());
					physicalMarketfact.setQuantity(obj.getQuantity());
					physicalMarketfact.setPhysicalCertificate(physicalCertificate);
					lstTmp.add(physicalMarketfact);
				}
				physicalCertificate.setPhysicalCertMarketfacts(lstTmp);*/
				physicalCertificateList.add(physicalCertificate);
			}

			regLstPhysicalCertMarketfacts = null;
			regPhysicalCertMarketfact = new PhysicalCertMarketfact();
			arrRegPhysicalCertMarketfact = null;
			
			//List<Long> lstCerts = generateLisCertificates(toCorrelativo, fromCorrelativo);
			//strCerts = certificateNumberMessage(lstCerts);
			strCerts = fromCorrelativo + " al " + toCorrelativo;
			
		} else {
			physicalCertificate = new PhysicalCertificate();
			
			physicalCertificate.setCurrencyType(moneyTypeMap.get(security.getCurrency()));
			physicalCertificate.setIssuer(findIssuerSelected());
			physicalCertificate.setSecurityClass(security.getSecurityClass());
			physicalCertificate.setCurrency(security.getCurrency());
			physicalCertificate.setInstrumentType(security.getInstrumentType());
			physicalCertificate.setClassType(securityClassTypeMapDescription(security.getSecurityClass()));
			physicalCertificate.setIssueDate(security.getIssuanceDate());
			physicalCertificate.setNominalValue(security.getCurrentNominalValue());
			physicalCertificate.setSecurities(this.security);
			physicalCertificate.setCertificateQuantity(this.CertificateQuantity);
			physicalCertificate.setCertificateNumber(certificateNumber.toString());
			physicalCertificate.setCouponCount(couponCount);
			//RENTA FIJA
			if(security.getInterestRate() != null){
				physicalCertificate.setInterestRate(security.getInterestRate());
			}
			if(security.getPeriodicity() != null){
				physicalCertificate.setPaymentPeriodicity(security.getPeriodicity());
			}
			if( Validations.validateIsNotNullAndPositive(security.getPeriodicity()) ){
				physicalCertificate.setPaymentType(paymentTypeMapDescription(physicalCertificate.getPaymentPeriodicity()));
			}
			if(security.getExpirationDate() != null){
				physicalCertificate.setExpirationDate(security.getExpirationDate());
			}
			
			physicalCertificate.setPhysicalCertMarketfacts(regLstPhysicalCertMarketfacts);
			regLstPhysicalCertMarketfacts = null;
			regPhysicalCertMarketfact = new PhysicalCertMarketfact();
			arrRegPhysicalCertMarketfact = null;
			physicalCertificateList.add(physicalCertificate);
		
			strCerts = certificateNumber.toString();
		}
		
		certificateDataModel = new PhysicalCertificateDataModel(physicalCertificateList);
		if(physicalCertificateList != null && physicalCertificateList.size()>0){
			for (PhysicalCertificate certificate : physicalCertificateList) {
				BigDecimal nominalValue = null;
				
				if(certificate.getNominalValue()!=null){
					nominalValue = certificate.getNominalValue().setScale(2,RoundingMode.HALF_UP);
				}else{
					nominalValue = new BigDecimal(0);
				}
				if(certificate.getCurrency().equals(CurrencyType.PYG.getCode())){
					quantityBS++;
					sumBS = sumBS.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);					
				}
				if(certificate.getCurrency().equals(CurrencyType.USD.getCode())){
					quantityUSD++;
					sumUSD = sumUSD.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);					
				}
				if(certificate.getCurrency().equals(CurrencyType.EU.getCode())){
					quantityEUR++;
					sumEUR = sumEUR.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);
				}
				if(certificate.getCurrency().equals(CurrencyType.UFV.getCode())){
					quantityUFV++;
					sumUFV = sumUFV.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);
				}
				if(certificate.getCurrency().equals(CurrencyType.DMV.getCode())){
					quantityMVD++;
					sumMVD = sumMVD.add(nominalValue.multiply(certificate.getCertificateQuantity())).setScale(2,RoundingMode.HALF_UP);
				}
			}
		}
		clearPopUpCertificate();
		validatorDisabled = false;
		
		/*String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_ADD_SUCCESS,new Object[]{strCerts});
		alert(headerMessage,bodyMessage);*/
    }
    
   /**
     * Show delete confirmation.
     *
     * @param physicalCertificatesRow the physical certificates row
     */
    public void showDeleteConfirmation(PhysicalCertificate physicalCertificatesRow) {
    	
    	String headerMessage = "";
		String bodyMessage = "";

		List<Long> certNums = new ArrayList<Long>();
		certNums.add( Long.valueOf(physicalCertificatesRow.getCertificateNumber()) );
		String certs = certificateNumberMessage(certNums);
		
		if(modify){
			bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ELIMINAR_TITLE_ERROR, certs);
			alert(bodyMessage);
			return;
		}
		
		detelephysicalCertificates = physicalCertificatesRow;
		confirmAlertAction="deleteCertificates";
		headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_DELETE);
		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ELIMINAR_TITLE, certs);
		question(headerMessage, bodyMessage);
			
	}
    
    /**
     * Delete certificates.
     */
    public void deleteCertificates() {
		quantityBS = 0;
		quantityUSD = 0;
		sumBS = new BigDecimal(0);
		sumUSD = new BigDecimal(0);
		
		List<Long> certNums = new ArrayList<Long>();
		if(Validations.validateListIsNotNullAndNotEmpty(physicalCertificateList)){
			physicalCertificateList.remove(detelephysicalCertificates);
		}		
		certNums.add( Long.valueOf(detelephysicalCertificates.getCertificateNumber()) );
		
		certificateDataModel = new PhysicalCertificateDataModel(physicalCertificateList);
		if(physicalCertificateList != null){
			for (PhysicalCertificate certificate : physicalCertificateList) {
				if(CurrencyType.PYG.getCode().equals(certificate.getCurrency())){
					quantityBS++;
					BigDecimal nominalValue = certificate.getNominalValue().setScale(2,RoundingMode.HALF_UP);
					sumBS = sumBS.add(nominalValue).setScale(2,RoundingMode.HALF_UP);
				}
				if(CurrencyType.USD.getCode().equals(certificate.getCurrency())){
					BigDecimal nominalValue = certificate.getNominalValue().setScale(2,RoundingMode.HALF_UP);
					quantityUSD++;
					sumUSD = sumUSD.add(nominalValue).setScale(2,RoundingMode.HALF_UP);
				}
			}
		}

		String certs = certificateNumberMessage(certNums);
		
		String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ELIMINAR_SUCCESS, certs);
		alert(headerMessage, bodyMessage);
	}
    
    
    /**
     * Verify modification.
     *
     * @return true, if successful
     */
    public boolean verifyModification(){

		if(	//issuerSelected.equals(initialIssuerModify) && 
			certificateNumber.toString().equals(initialCertificateNumberModify) && 
			instrumentTypeSelected.equals(initialInstrumentTypeModify) && 
			physicalCertificate.getPaymentPeriodicity().equals(initialPaymentPeriodicityModify) && 
			securityClassSelected.equals(initialSecurityClassModify) && 
			physicalCertificate.getInterestRate().equals(initialInterestRateModify) && 
			initialCurrencyModify.equals(new Long(physicalCertificate.getCurrency())) && 
			physicalCertificate.getNominalValue().equals(initialNominalValueModify) && 
			initialDate.equals(physicalCertificate.getIssueDate()) && 
			finalDate.equals(physicalCertificate.getExpirationDate()) 
			){
			return false;
		}else{
			return true;
		}
	}
    
    /**
     * Verify registry.
     *
     * @return true, if successful
     */
    public boolean verifyRegistry(){

    		/*if( Validations.validateIsNotNull(issuerSelected) ){
				return true;
			}else */if( Validations.validateIsNotNullAndPositive(securityClassSelected) ){
    			return true;
    		}else if( Validations.validateIsNotNullAndPositive(instrumentTypeSelected) ){
    			return true;
    		}else if( Validations.validateIsNotNullAndPositive(physicalCertificate.getPaymentPeriodicity()) ){
    			return true;
    		}else if( Validations.validateIsNotNull(physicalCertificate.getInterestRate()) ){
    			return true;
    		}else if( Validations.validateIsNotNullAndPositive(physicalCertificate.getCurrency()) ){
    			return true;
    		}else if( Validations.validateIsNotNull(physicalCertificate.getNominalValue()) ){
    			return true;
    		}else if(selectCertificateCount == 1){
    			if( Validations.validateIsNotNullAndPositive(certificateNumber)){
    				return true;
    			}
    		}else if(selectCertificateCount == 2){
    			if( Validations.validateIsNotNullAndPositive(fromCorrelativo)){
    				return true;
    			}else if( Validations.validateIsNotNullAndPositive(toCorrelativo)){
    				return true;
    			}
    		}
    		
    		return false;
	}
    
    /**
     * Validate certificate number.
     */
    public void validateCertificateNumber(){
    	removeAlertPopUp();
    	
    	if(Validations.validateIsNotNull(certificateNumber)){
        	CertificatesCountCal();
    	}
    	
    	if(Validations.validateIsNullOrEmpty(CertificateQuantity)){
    		CertificateQuantity = new BigDecimal(1);	
    	}
		
    }
    
     /**
      * Checks if is exist certificate in bd.
      *
      * @return true, if is exist certificate in bd
      */
     public boolean isExistCertificateInBd(){
    	 PhysicalCertificate objPhysicalCertificate = new PhysicalCertificate();
    	 for(PhysicalCertificateExt obj : selectedPhysicalCertificatesExt )	{
    		 objPhysicalCertificate = new PhysicalCertificate();
    		 objPhysicalCertificate = obj.getPhysicalCertificate();
    		if(certificateDepositeBeanFacade.existsCertificateInBd(objPhysicalCertificate)){
    	 			String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    	     		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG,
    	     															new Object[]{objPhysicalCertificate.getCertificateNumber().toString(),
    	     															objPhysicalCertificate.getSecurities().getIdSecurityCodePk()});
    	     		alert(headerMessage,bodyMessage);
    	     		return true;
    	 	}
    	 }
    	 return false;
     }
    
     /**
     * Certificates count cal.
     */
    public void CertificatesCountCal(){
    	String headerMessage = "";
    	String bodyMessage = "";
    	confirmAlertAction="removeAlertPopUp";
    	numberOfCertificates = 1;
    	if((certificateNumber != null || ((fromCorrelativo != null) && (toCorrelativo != null))))
		{
			physicalCertificate.setIssuer(findIssuerSelected(security));
			if(certificateNumber != null){
				physicalCertificate.setCertificateNumber(certificateNumber+"");
				physicalCertificate.setSecurities(security);
				if( existCertificateInList(physicalCertificate) || existInSolicitud(physicalCertificate)){
						headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG,
			    															new Object[]{physicalCertificate.getCertificateNumber().toString(),
			    																		 physicalCertificate.getSecurities().getIdSecurityCodePk()});
			    		alert(headerMessage,bodyMessage);
			    		fromCorrelativo = null;
						toCorrelativo = null;
						numberOfCertificates = null;
						certificateNumber = null;
						CertificateQuantity = null;
			    		return ;
				}
			
				if( existCertificateInBd(physicalCertificate) ){
		    		headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
		    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_REG_EXISTS,
		    															new Object[]{physicalCertificate.getCertificateNumber().toString(),
		    																		 physicalCertificate.getSecurities().getIdSecurityCodePk()});
		    		alert(headerMessage,bodyMessage);
		    		fromCorrelativo = null;
					toCorrelativo = null;
					numberOfCertificates = null;
					certificateNumber = null;
					CertificateQuantity = null;
		    		return ;
				}
				
				if (isDematerializedCertificate(physicalCertificate)) {
					headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
		    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_DEMATERIALIZED,
		    															new Object[]{physicalCertificate.getCertificateNumber().toString(),
		    																		 physicalCertificate.getSecurities().getIdSecurityCodePk()});
		    		alert(headerMessage,bodyMessage);
		    		fromCorrelativo = null;
					toCorrelativo = null;
					numberOfCertificates = null;
					certificateNumber = null;
					CertificateQuantity = null;
		    		return ;
				}
				
			}else if(toCorrelativo != null && fromCorrelativo != null){
				List<Long> certificatesNumsList = generateLisCertificates(toCorrelativo, fromCorrelativo);
				
				if(toCorrelativo - fromCorrelativo == 1){
					numberOfCertificates = 2;
				}else{
					numberOfCertificates = toCorrelativo - fromCorrelativo + 1;
				}
				
				if(flagDPF){
					CertificateQuantity = new BigDecimal(1);
				}
				//validate quantity
				
				for (Long lngcertificateNumber: certificatesNumsList) {
					physicalCertificate.setCertificateNumber(lngcertificateNumber+"");
					physicalCertificate.setSecurities(security);
					if( existCertificateInList(physicalCertificate) || existInSolicitud(physicalCertificate)		
					){
						headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG,
			    															new Object[]{physicalCertificate.getCertificateNumber().toString(),
			    																		 physicalCertificate.getIssuer().getBusinessName()});
			    		alert(headerMessage,bodyMessage);
			    		fromCorrelativo = null;
						toCorrelativo = null;
						numberOfCertificates = null;
						certificateNumber = null;
						CertificateQuantity = null;
			    		return ;
					}
				
					if( existCertificateInBd(physicalCertificate) ){
			    		headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_REG_EXISTS,
			    															new Object[]{physicalCertificate.getCertificateNumber().toString(),
			    																		 physicalCertificate.getIssuer().getBusinessName()});
			    		alert(headerMessage,bodyMessage);
			    		fromCorrelativo = null;
						toCorrelativo = null;
						numberOfCertificates = null;
						certificateNumber = null;
						CertificateQuantity = null;
			    		return ;
					}
					
					if (isDematerializedCertificate(physicalCertificate)) {
						headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_DEMATERIALIZED,
			    															new Object[]{physicalCertificate.getCertificateNumber().toString(),
			    																		 physicalCertificate.getSecurities().getIdSecurityCodePk()});
			    		alert(headerMessage,bodyMessage);
			    		fromCorrelativo = null;
						toCorrelativo = null;
						numberOfCertificates = null;
						certificateNumber = null;
						CertificateQuantity = null;
			    		return ;
					}
				}
				
				if(numberOfCertificates>100){
					headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
		    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG_MAX);
		    		alert(headerMessage,bodyMessage);
		    		
		    		fromCorrelativo = null;
					toCorrelativo = null;
					numberOfCertificates = null;
					certificateNumber = null;
					CertificateQuantity = null;
					
		    		return ;
				}
				
				/* Valida */
				//certificateNumber	//nro de titulos
				//CertificateQuantity //cantidad de valores
				security.getCirculationBalance();
			}
				
		}
    	confirmAlertAction="";
    	
    }
    
    /**
     * Exist certificate in list.
     *
     * @param physicalCertificate the physical certificate
     * @return true, if successful
     */
    public boolean existCertificateInList(PhysicalCertificate physicalCertificate){
    	//EL NRO DEL TITULO ESTA EN LA LISTA DE TITULOS DEL REGISTRO
		if (physicalCertificate.getCertificateNumber() != null) {
			if (physicalCertificateList != null && physicalCertificateList.size() > 0) {
				for (PhysicalCertificate certificate : physicalCertificateList) {
					if (physicalCertificate.getCertificateNumber().equals(certificate.getCertificateNumber()) && 
						physicalCertificate.getSecurities().getIdSecurityCodePk().equals(certificate.getSecurities().getIdSecurityCodePk())) {
						return true;
					}
				}
			}
		}
		
		return false;
    }
    
    /**
     * Exist certificate in bd.
     *
     * @param physicalCertificate the physical certificate
     * @return true, if successful
     */
    public boolean existCertificateInBd(PhysicalCertificate physicalCertificate) {
		List<PhysicalBalanceDetail> 	physicalCertificatesExisted;
    	List<String> certificatesNumsList = new ArrayList<String>();
		try {
			if( Validations.validateIsNotNull(physicalCertificate.getCertificateNumber()) ){
				
				certificatesNumsList.add(physicalCertificate.getCertificateNumber());
				physicalCertificatesExisted = certificateDepositeBeanFacade.getPhysicalCertificateExists(certificatesNumsList,
																		physicalCertificate.getSecurities().getIdSecurityCodePk());
				if(Validations.validateListIsNotNullAndNotEmpty(physicalCertificatesExisted)){
					return true;
				}
			}
		} catch (ServiceException e) {
			
		}
    	return false;
    }
    
    
    /**
     * Checks if is dematerialized certificate.
     *
     * @param physicalCertificate the physical certificate
     * @return true, if is dematerialized certificate
     */
    public boolean isDematerializedCertificate(PhysicalCertificate physicalCertificate)
    {
    	List<AccountAnnotationOperation> lstAccountAnnotationOperation= certificateDepositeBeanFacade.
												    			getListAccountAnnotationOperationByCertificate( Long.valueOf(physicalCertificate.getCertificateNumber()), 
												    					physicalCertificate.getSecurities().getIdSecurityCodePk());
    	if (Validations.validateListIsNotNullAndNotEmpty(lstAccountAnnotationOperation)) {
    		return true;
    	}
    	return false;
    }
    
    /**
     * Generate lis certificates.
     *
     * @param toCorrelativo the to correlativo
     * @param fromCorrelativo the from correlativo
     * @return the list
     */
    public List<Long> generateLisCertificates(Integer toCorrelativo, Integer fromCorrelativo){
    	List<Long> certificatesNumsList = new ArrayList<Long>();
    	
    	if (toCorrelativo != null && fromCorrelativo != null && toCorrelativo > fromCorrelativo) {
			for (int i = fromCorrelativo; i <= toCorrelativo; i++) {
				certificatesNumsList.add(new Long(i));
			}
		}
    	return certificatesNumsList;
    }
    
    /**
     * Exist in solicitud.
     *
     * @param physicalCertificate the physical certificate
     * @return true, if successful
     */
    public boolean existInSolicitud(PhysicalCertificate physicalCertificate){
    	boolean existConfirmed = false;
    	boolean existDeposited = false;
		try {
			List<PhysicalOperationDetail> physicalOperationDetail = certificateDepositeBeanFacade.getOperationdetail( 
																	Long.valueOf(physicalCertificate.getCertificateNumber()), 
																	physicalCertificate.getSecurities().getIdSecurityCodePk());				
			if (Validations.validateListIsNotNullAndNotEmpty(physicalOperationDetail)) {	
				for(PhysicalOperationDetail objPhysicalOperationDetail : physicalOperationDetail){
					PhysicalCertificate objPhysicalCertificate = objPhysicalOperationDetail.getPhysicalCertificate();
					List<PhysicalBalanceDetail> lstPhysicalBalanceDetail = certificateDepositeBeanFacade.getPhysicalBalanceDetail(objPhysicalCertificate.getIdPhysicalCertificatePk());
					if(Validations.validateListIsNotNullAndNotEmpty(lstPhysicalBalanceDetail)){
						existConfirmed = true;
						PhysicalBalanceDetail objPhysicalBalanceDetail = lstPhysicalBalanceDetail.get(0);
						if(StateType.DEPOSITED.getCode().equals(objPhysicalBalanceDetail.getState())){
							existDeposited = true;
						}
					} else {
						existConfirmed = false;
					}
				}
				if(!existConfirmed){
					return true;
				}				
				if(existDeposited){
					return true;
				}			
			}										   
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
    	return false;
    }
    
    
    
    
    /**
     * Gets the holder account and details.
     *
     * @param holderAccount the holder account
     * @return the holder account and details
     */
    public HolderAccount getHolderAccountAndDetails(HolderAccount holderAccount){
		try {
			holderAccount = transferAvailableFacade.getHolderAccount(holderAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return holderAccount;
	}
    
    /**
     * Aprove certificates.
     *
     * @param single the single
     * @return the string
     */
    public String aproveCertificates(boolean single){
    	removeAlert();
    	if(isExistCertificateInBd())
			return "";
		confirmAlertAction = "certificateApproval";
		certificatesNums = new ArrayList<Long>();
		String strCertInvalid = validLisCertificateForState(StateType.REGISTERED.getCode());
		String certs = certificateNumberMessage(certificatesNums);
		if(!strCertInvalid.equals(GeneralConstants.EMPTY_STRING)){
			alert(errorStateMessageAlert(StateType.APPROVED.getCode(),strCertInvalid));
			confirmAlertAction = "";
			return "";
		}
		
		//ISSUE 895: se comenta esto porque en la etapa de aprobacion no es necesario validar debido a que el valor aun puede NO existir
//		String strCertValidated=validatePhysicalAmount();
//		if(!strCertValidated.equals(GeneralConstants.EMPTY_STRING))
//		{
//			if(selectedPhysicalCertificatesExt.length==GeneralConstants.ZERO_VALUE_INT)
//			{
//				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT));
//				confirmAlertAction = "";
//				return "";
//			}
//			else
//			{
//				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT_MANY,
//						new Object[]{strCertValidated}));
//				confirmAlertAction = "";
//				return "";
//			}
//		}
		if(single){
			
			String headerMessage = confirmHeaderMessage(StateType.APPROVED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.APPROVED.getCode(),certs);
			
			question(headerMessage,bodyMessage);
			return "";
		}else{
			return ejecutarAccion(StateType.REGISTERED.getCode(), StateType.APPROVED.getCode());
		}
    }
    
    /**
     * Validate physical amount.
     *
     * @return the string
     */
    public String validatePhysicalAmount()
    {
    	String strCertificateValidated=GeneralConstants.EMPTY_STRING;
    	if(selectedPhysicalCertificatesExt.length==1)//GeneralConstants.ZERO_VALUE_INT
    	{
    		//issue 895: si el valor no existe entonces no se evalua
    		if(selectedPhysicalCertificatesExt[0].getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.YES.getCode())){
    			BigDecimal  availablePhysicalBalance =selectedPhysicalCertificatesExt[0].getPhysicalCertificate().getSecurities().getAvailablePhysicalBalance();
        		if(availablePhysicalBalance.intValue()<selectedPhysicalCertificatesExt[0].getPhysicalCertificate().getCertificateQuantity().intValue())
        		{
        			strCertificateValidated=selectedPhysicalCertificatesExt[0].getPhysicalCertificate().getSecurities().getIdSecurityCodePk();
        		}	
    		}
    		return strCertificateValidated;
    	}
    	else
    	{
    		
    		//sorting by id_security_code_pk
    		PhysicalCertificateExt[] physicalCertExt =sortingListBySecurity(selectedPhysicalCertificatesExt);
    		
			int count=0;
    		Security objSecurityTmp=null;
    		Integer physicalAmountTemp=GeneralConstants.ZERO_VALUE_INTEGER;
    		for(PhysicalCertificateExt obj : physicalCertExt )
    		{	count++;
    			if(count==GeneralConstants.ONE_VALUE_INTEGER)
    			{
    				objSecurityTmp=obj.getPhysicalCertificate().getSecurities();
    				physicalAmountTemp=obj.getPhysicalCertificate().getCertificateQuantity().intValue();
    			}
    			else
    			{
    				if(objSecurityTmp.getIdSecurityCodePk().equals(obj.getPhysicalCertificate().getSecurities().getIdSecurityCodePk()))
    				{
    					physicalAmountTemp+=obj.getPhysicalCertificate().getCertificateQuantity().intValue();
    					if(count==physicalCertExt.length){
    						if(physicalAmountTemp>objSecurityTmp.getAvailablePhysicalBalance().intValue()){
        						strCertificateValidated+=objSecurityTmp.getIdSecurityCodePk()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
        					}
    					}
    				}
    				else
    				{
    					if(physicalAmountTemp>objSecurityTmp.getAvailablePhysicalBalance().intValue()){
    						strCertificateValidated+=objSecurityTmp.getIdSecurityCodePk()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
    						objSecurityTmp=null;
    					}
    					objSecurityTmp=obj.getPhysicalCertificate().getSecurities();
        				physicalAmountTemp=obj.getPhysicalCertificate().getCertificateQuantity().intValue();
        				if(physicalCertExt.length==count)
        				{
        					if(physicalAmountTemp>objSecurityTmp.getAvailablePhysicalBalance().intValue()){
        						strCertificateValidated+=objSecurityTmp.getIdSecurityCodePk()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
        						objSecurityTmp=null;
        					}
        				}
    				}
    			}
    		}
    		if(strCertificateValidated.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
    			strCertificateValidated=strCertificateValidated.substring(0,strCertificateValidated.length()-1);
    		return strCertificateValidated;
    	}
    }
    
    /**
     * Sorting list by security.
     *
     * @param selectedPhysicalCertificatesExt the selected physical certificates ext
     * @return the physical certificate ext[]
     */
    public PhysicalCertificateExt[] sortingListBySecurity(PhysicalCertificateExt[] selectedPhysicalCertificatesExt)
    {      
    	List<PhysicalCertificateExt> arrayPhysicalCertificatesExt=new ArrayList<PhysicalCertificateExt>( Arrays.asList( selectedPhysicalCertificatesExt ));
    	Collections.sort(arrayPhysicalCertificatesExt, new Comparator<PhysicalCertificateExt>() {
			@Override
			public int compare(PhysicalCertificateExt p1, PhysicalCertificateExt p2) {
				return p1.getPhysicalCertificate().getSecurities().getIdSecurityCodePk().compareTo(p2.getPhysicalCertificate().getSecurities().getIdSecurityCodePk());
			}
		});
    	
    	return arrayPhysicalCertificatesExt.toArray(new PhysicalCertificateExt[arrayPhysicalCertificatesExt.size()]);
    }
    
    /**
     * Cancel certificates.
     *
     * @param single the single
     * @return the string
     */
    public String  cancelCertificates(boolean single){
    	removeAlert();
    	if(isExistCertificateInBd())
			return "";
    	confirmAlertAction = "certificateRejection_anull";
		certificatesNums = new ArrayList<Long>();
		String strCertInvalid = validLisCertificateForState(StateType.REGISTERED.getCode());
		String certs = certificateNumberMessage(certificatesNums);
		if(isExistCertificateInBd())
			return "";
		if(!strCertInvalid.equals(GeneralConstants.EMPTY_STRING)){
			alert(errorStateMessageAlert(StateType.ANNULLED.getCode(),strCertInvalid));
			confirmAlertAction = "";
			return "";
		}
		
    	if(single){
			alertAnulledOrReject(StateType.ANNULLED.getCode());
			return "";
    	}else{
    		return ejecutarAccion(StateType.REGISTERED.getCode(), StateType.ANNULLED.getCode());
    	}
    	
    }
    
    /**
     * Apply certificates.
     *
     * @param single the single
     * @return the string
     */
    public String applyCertificates(boolean single){
    	removeAlert();
    	if(isExistCertificateInBd())
			return "";
		confirmAlertAction = "certificateApplication";
		certificatesNums = new ArrayList<Long>();
		
		String strCertInvalid = validLisCertificateForState(StateType.CONFIRMED.getCode());
		String certs = certificateNumberMessage(certificatesNums);
		
		if(!strCertInvalid.equals(GeneralConstants.EMPTY_STRING)){
			alert(errorStateMessageAlert(StateType.APPLY.getCode(),strCertInvalid));
			confirmAlertAction = "";
			return "";
		}
		
		if(single){
			certs = certificateNumberMessage(certificatesNums);
			
			String headerMessage = confirmHeaderMessage(StateType.APPLY.getCode());
			String bodyMessage = confirmbodyMessage(StateType.APPLY.getCode(),certs);
			
			question(headerMessage,bodyMessage);
			return "";
		}else{
			return ejecutarAccion(StateType.CONFIRMED.getCode(), StateType.APPLY.getCode());
		}
    }
    
    /**
     * Confirm certificates.
     *
     * @param single the single
     * @return the string
     */
    public String confirmCertificates(boolean single){
    	removeAlert();
    	if(isExistCertificateInBd())
			return "";
    	confirmAlertAction = "certificateConfirmation";
		certificatesNums = new ArrayList<Long>();
    	
		String strCertInvalid = validLisCertificateForState(StateType.APPROVED.getCode());
		String certs = certificateNumberMessage(certificatesNums);
		
		if(!strCertInvalid.equals(GeneralConstants.EMPTY_STRING)){
			alert(errorStateMessageAlert(StateType.CONFIRMED.getCode(),strCertInvalid));
			confirmAlertAction = "";
			return "";
		}
		
		// issue 895: antes de realizar las validaciones correspondientes se verificara la existencia de los valores seleccionados
		String idSecurityCodePkAuxx;
		StringBuilder securitiesNonExistent=new StringBuilder();
		boolean firstLoop=true;
		for(PhysicalCertificateExt pcExt : selectedPhysicalCertificatesExt){
			if(pcExt.getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.NO.getCode())){
				idSecurityCodePkAuxx = certificateDepositeBeanFacade.getIdSecurityCodeNoExists(pcExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
				if(firstLoop){
					firstLoop=false;
				}else{
					securitiesNonExistent.append(GeneralConstants.STR_COMMA);
				}
				securitiesNonExistent.append(idSecurityCodePkAuxx);
			}
		}
		
		if(securitiesNonExistent.toString().length()>1){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_SECURITIES_BEFORE_CONFIRM,
					new Object[]{securitiesNonExistent.toString()}));
			
			confirmAlertAction = "";
			return "";
		}
		
		String strCertValidated=validatePhysicalAmount();
		if(!strCertValidated.equals(GeneralConstants.EMPTY_STRING))
		{
			if(selectedPhysicalCertificatesExt.length==GeneralConstants.ZERO_VALUE_INT)
			{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT));
				confirmAlertAction = "";
				return "";
			}
			else
			{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_EXCEDDED_DEPOSIT_AMOUNT_MANY,
						new Object[]{strCertValidated}));
				confirmAlertAction = "";
				return "";
			}
		}
		
    	if(single){
    		
    		//ISSUE 895: si se va CONFIRMAR en singular pues es necesario verificar el titulos seleccionado existen su Clase-Clave de Valor en sistema
			String idSecurityCodePkAux;
			Security securityAux;
			for (PhysicalCertificateExt physicalCertificateExt : selectedPhysicalCertificatesExt){
				idSecurityCodePkAux=certificateDepositeBeanFacade.getIdSecurityCodeNoExists(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
    			
    			//obteniendo el Valor con su Emisor
    			securityAux = certificateDepositeBeanFacade.getSecurityWithIssuer(idSecurityCodePkAux);
    			if(Validations.validateIsNull(securityAux) || Validations.validateIsNull(securityAux.getIdSecurityCodePk())){
    				alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_SECURITY_BEFORE_CONFIRM,
    						new Object[]{idSecurityCodePkAux}));
    				confirmAlertAction = "";
    				return "";
    			}
    			
    			// issue 891: validando si el valor esta autorizado para entrar a cartera
				if (Validations.validateIsNotNull(securityAux.getIndAuthorized())) {
					if (securityAux.getIndAuthorized().equals(BooleanType.NO.getCode())) {
						alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_ALERT_VALIDATION_REQUIRED_AUTHORIZEDSECURITY));
	    				confirmAlertAction = "";
	    				return "";
					}
				}
			}
    		
			certs = certificateNumberMessage(certificatesNums);
			
			String headerMessage = confirmHeaderMessage(StateType.CONFIRMED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.CONFIRMED.getCode(),certs);
			
			question(headerMessage,bodyMessage);
			return "";
		}else{
			
			//issue 895: si se va CONFIRMAR en lote pues es necesario verificar si en todos titulos seleccionados existen su Clase-Clave de Valor en sistema
			String idSecurityCodePkAux=null;
			Security securityAux;
			StringBuilder secuNoExists=new StringBuilder();
			int loop=0;
			for (PhysicalCertificateExt physicalCertificateExt : selectedPhysicalCertificatesExt){
				loop++;
				idSecurityCodePkAux=certificateDepositeBeanFacade.getIdSecurityCodeNoExists(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
    			
    			//obteniendo el Valor con su Emisor
    			securityAux = certificateDepositeBeanFacade.getSecurityWithIssuer(idSecurityCodePkAux);
    			if(Validations.validateIsNull(securityAux) || Validations.validateIsNull(securityAux.getIdSecurityCodePk())){
    				secuNoExists.append(idSecurityCodePkAux);
    				secuNoExists.append(", ");
    			}else{
    				
    				// issue 891: validando si el valor esta autorizado para entrar a cartera
    				if (Validations.validateIsNotNull(securityAux.getIndAuthorized())) {
    					if (securityAux.getIndAuthorized().equals(BooleanType.NO.getCode())) {
    						alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_ALERT_VALIDATION_REQUIRED_AUTHORIZEDSECURITY_ESPECIFIED, new Object[]{idSecurityCodePkAux} ));
    	    				confirmAlertAction = "";
    	    				return "";
    					}
    				}
    			}
			}
			if(secuNoExists.length()>0){
				if(loop==1){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_SECURITY_BEFORE_CONFIRM,
    						new Object[]{idSecurityCodePkAux}));
				}else{
					alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_REQUIRED_SECURITIES_BEFORE_CONFIRM,
							new Object[]{secuNoExists.toString()}));
				}
				
				confirmAlertAction = "";
				return "";
			}
			
			return ejecutarAccion(StateType.APPROVED.getCode(), StateType.CONFIRMED.getCode());
		}
    }
    
    /**
     * Reject certificates.
     *
     * @param single the single
     * @return the string
     */
    public String rejectCertificates(boolean single){
    	removeAlert();
    	if(isExistCertificateInBd())
			return "";
    	confirmAlertAction = "certificateRejection_reject";
		certificatesNums = new ArrayList<Long>();

		String strCertInvalid = validLisCertificateForState(StateType.APPROVED.getCode());//CAMBIE EL APPLY
		String certs = certificateNumberMessage(certificatesNums);
		
		if(!strCertInvalid.equals(GeneralConstants.EMPTY_STRING)){
			alert(errorStateMessageAlert(StateType.REJECTED.getCode(),strCertInvalid));
			confirmAlertAction = "";
			return "";
		}
		
    	if(single){
			alertAnulledOrReject(StateType.REJECTED.getCode());
			return "";
    	}else{
    		return ejecutarAccion(StateType.APPROVED.getCode(), StateType.REJECTED.getCode());
    	}
    }
    
    /**
     * View certification detail.
     *
     * @param rowPhysicalCertificatesExt the row physical certificates ext
     * @return the string
     */
    public String viewCertificationDetail(PhysicalCertificateExt rowPhysicalCertificatesExt){
    	//issue 895: obteniendo los adjuntos para el view
    	lstPhysicalCertFileView = certificateDepositeBeanFacade.getLstPhysicalCertFileFromOnePhysicalCert(rowPhysicalCertificatesExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
    	setCertificatedetailView(rowPhysicalCertificatesExt);
    	disabledButtons();
    	return "certificateDepositeDetailView";
    }
    
    /**
     * New certification.
     *
     * @return the string
     */
    public String newCertification(){

    	try {
    		
    		loadListAndMap();
	    	disabledButtons();
	    	clearSearchCriteria();
	    	physicalCertificate = new PhysicalCertificate();
			physicalCertificateList = new ArrayList<PhysicalCertificate>();
			physicalCertificateSearchList = new ArrayList<PhysicalCertificateExt>();
			issuerList = certificateDepositeBeanFacade.getIssuerList(IssuerStateType.REGISTERED.getCode());
			certificateNumber=null;
			numberOfCertificates=null;
			CertificateQuantity=null;
			regPhysicalCertMarketfact = new PhysicalCertMarketfact();
			arrRegPhysicalCertMarketfact = null;
			regLstPhysicalCertMarketfacts = null;
			
			if(userInfo.getUserAccountSession().isIssuerInstitucion() &&
					Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) {
				//issuerSelected = userInfo.getUserAccountSession().getIssuerCode();
				getIssuerOnSelection();
			}
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
    	
    	return "CertificateDepositeMgmtView";
    }
    
    /**
     * Load list and map.
     */
    public void loadListAndMap(){
    	try {
    		
			if( ReasonStateList==null || ReasonStateList.size()<=0 ){
	    		
    			ParameterTableTO parameterTableTO = new ParameterTableTO();
	    		parameterTableTO.setMasterTableFk(MasterTableType.REASON_REJECT_ANULLED_REQUEST.getCode());
	    		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    		ReasonStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
	    			for (ParameterTable param : ReasonStateList) {
	    				reasonStateTypeMap.put(param.getParameterTablePk(), param.getParameterName());
	    		}
	    			
    		}
    		

    		if( depositStateList==null || depositStateList.size()<=0 ){
	    		
    			ParameterTableTO parameterTableTO = new ParameterTableTO();
	    		parameterTableTO.setMasterTableFk(MasterTableType.DEPOSIT_STATE.getCode());
	    		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    		depositStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
	    			for (ParameterTable param : depositStateList) {
	    				depositStateTypeMap.put(param.getParameterTablePk(), param.getParameterName());
	    		}
	    			
    		}

    		if( depositRequestStateList==null || depositRequestStateList.size()<=0 ){
    			
	    		ParameterTableTO parameterTableTO = new ParameterTableTO();
	    		parameterTableTO.setMasterTableFk(MasterTableType.DEPOSIT_STATE_REQUEST.getCode());
	    		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    		depositRequestStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
	    			for (ParameterTable param : depositRequestStateList) {
	    				depositRequestStateTypeMap.put(param.getParameterTablePk(), param.getParameterName());
	    		}
	    			
    		}
    			
    		if( instrumentTypeList==null || instrumentTypeList.size()<=0 ){
    			
    			ParameterTableTO parameterTableTO = new ParameterTableTO();
     			parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
     			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
     			instrumentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
     			for (ParameterTable param : instrumentTypeList) {
     				instrumentTypeMap.put(param.getParameterTablePk(), param.getParameterName());
    			}
     			
    		}

    		if( paymentTypeList==null || paymentTypeList.size()<=0 ){
    			
    			ParameterTableTO parameterTableTO = new ParameterTableTO();
    			parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PERIODICITY.getCode());
    			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
    			paymentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
     			for (ParameterTable param : paymentTypeList) {
     				paymentTypeMap.put(param.getParameterTablePk(), param.getParameterName());
    			}
     			
    		}

    		if( moneyTypeList==null || moneyTypeList.size()<=0 ){
    			
    			ParameterTableTO parameterTableTO = new ParameterTableTO();
    			parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
    			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
    			moneyTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
    			for (ParameterTable param : moneyTypeList) {
    				moneyTypeMap.put(param.getParameterTablePk(), param.getText1());
    				moneyTypeFullMap.put(param.getParameterTablePk(), param.getParameterName());
    			}
    			
    		}
    		
    		if( securityClassList==null || securityClassList.size()<=0 ){
    			
        		securityClassList = new ArrayList<ParameterTable>();
        		List<ParameterTable>  lstParam = certificateDepositeBeanFacade.
        											securityClassFacade(InstrumentType.MIXED_INCOME.getCode(), MasterTableType.SECURITIES_CLASS.getCode());
        		securityClassList.addAll(lstParam);
        		lstParam  = new ArrayList<ParameterTable>();
        		lstParam = certificateDepositeBeanFacade.securityClassFacade(InstrumentType.FIXED_INCOME.getCode(), MasterTableType.SECURITIES_CLASS.getCode());
        		securityClassList.addAll(lstParam);
        		lstParam  = new ArrayList<ParameterTable>();
        		lstParam = certificateDepositeBeanFacade.securityClassFacade(InstrumentType.VARIABLE_INCOME.getCode(), MasterTableType.SECURITIES_CLASS.getCode());
        		securityClassList.addAll(lstParam);
        		
        		for (ParameterTable par: securityClassList){
        				securityClassTypeMap.put(par.getParameterTablePk(), par.getParameterName());	
        		}
        		
    		}
    		
    		
		} catch (ServiceException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Exist changes to search.
     *
     * @return true, if successful
     */
    public boolean existChangesToSearch(){
    	if(Validations.validateIsNotNull(participantSelected) ||
    	   Validations.validateIsNotNull(holderAccount.getIdHolderAccountPk()) ){
    		return true;
    	}
    	return false;
    }


    /**
     * Execute back to search.
     *
     * @return the string
     */
    public String executeBackToSearch(){
    		removeAlertPopUp();
    		clearPopUpCertificate();
    		clearHeader();
        	removeAlertNew();
        	return "searchPhysicalCertificateView";
    }
    
    /**
     * Back to search.
     *
     * @param valid the valid
     * @return the string
     */
    public String backToSearch(boolean valid){
    	if(valid){
    		//comprobar si se modifico algo
    		if(existChangesToSearch()){
    			String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
        		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_BACK_VALIDATION);

        		question(headerMessage, bodyMessage);
        	    confirmAlertAction="executeBackToSearch";
        	    		
            	return "";
    		}else{
            	return "searchPhysicalCertificateView";
    		}
    	}else{
    		holderAccountDetails = new HolderAccount();
			physicalCertificateTemp = new PhysicalCertificateExt();
    		enabledButtons();
        	return "searchPhysicalCertificateView";	
    	}
    }
 
	/**
  * Cancel back to search.
  */
 public void cancelBackToSearch(){
	 	regLstPhysicalCertMarketfacts=null;
		removeAlert();
	}
    
    /**
     * Count certificates.
     */
    public void countCertificates(){
    	
    	confirmAlertAction="removeAlertPopUp";
    	
		if(fromCorrelativo != null && toCorrelativo != null){
			if(fromCorrelativo < toCorrelativo){
				numberOfCertificates = null;
				CertificatesCountCal();
				return;
			}else{
				String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CORRELATIVO_INVALID);
	    		alert(headerMessage,bodyMessage);
	    		
	    		certificateNumber = null;
				CertificateQuantity = null;
				fromCorrelativo = null;
				toCorrelativo = null;
				return;
			}
		}
		confirmAlertAction="";
	}
    
    /**
     * Validate state type annulled or rejected.
     *
     * @param stateType the state type
     * @return true, if successful
     */
    public boolean ValidateStateTypeAnnulledOrRejected(Integer stateType){
    	boolean viewDetailCertificate = Boolean.FALSE;
    	if(stateType.equals(StateType.ANNULLED.getCode()) || stateType.equals(StateType.REJECTED.getCode())){
    		viewDetailCertificate = Boolean.TRUE;
    	}
    	return viewDetailCertificate;
    }
    
    /**
     * Ejecutar accion.
     *
     * @param actualState the actual state
     * @param newState the new state
     * @return the string
     */
    public String ejecutarAccion(Integer actualState, Integer newState){
    	certificatesNums = new ArrayList<Long>();
    	String strCertInvalid=validLisCertificateForState(actualState);		
		
    	if (selectedPhysicalCertificatesExt != null && selectedPhysicalCertificatesExt.length > 0) {
    		
    		if(selectedPhysicalCertificatesExt.length == 1){
    			
    	    	//issue 895: obteniendo los adjuntos para el view
    	    	lstPhysicalCertFileView = certificateDepositeBeanFacade.getLstPhysicalCertFileFromOnePhysicalCert(selectedPhysicalCertificatesExt[0].getPhysicalCertificate().getIdPhysicalCertificatePk());
    			
    			if (selectedPhysicalCertificatesExt[0].getState().equals(StateType.CONFIRMED.getCode()) && 
    				Validations.validateIsNotNull(selectedPhysicalCertificatesExt[0].getPhysicalOperationDetail().getIndApplied())
    			) {
    				if(selectedPhysicalCertificatesExt[0].getPhysicalOperationDetail().getIndApplied().equals(Integer.valueOf(1))){
    					
    					String messageBody = PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPLIED_PREVIOUSLY, 
    							new Object[]{selectedPhysicalCertificatesExt[0].getPhysicalCertificate().getCertificateNumber().toString()} );
    					
	    				alert(messageBody);
	    				return "";
    				}
    			}
    			if(strCertInvalid.equals(GeneralConstants.EMPTY_STRING)){
    				 if( selectedPhysicalCertificatesExt[0].getState().equals(actualState) ){
    	    				//Redireccionar al detalle para ejecutar la accion
    	    				enabledButtonsWithState(newState);
    	    				setCertificatedetailView(selectedPhysicalCertificatesExt[0]);
    	    				return "certificateDepositeDetailView";
	    			}else{
	    				//manda alerta de que no se puede ejecutar la accion
	    				String messageBody = previouslyMessageAlert(newState);
	    				alert(messageBody);
	    			}
    			}else{
    				String messageBody = errorStateMessageAlert(newState,strCertInvalid);
    				alert(messageBody);
    			}
    			
    		}else{/*cuando se selecciona mas de uno*/
    			
    			//String strCertInvalid=validLisCertificateForState(actualState);
    			if(!strCertInvalid.equals(GeneralConstants.STR_COMMA_WITHOUT_SPACE)){
    				
    				if( newState.equals(StateType.ANNULLED.getCode()) || 
    					newState.equals(StateType.REJECTED.getCode()) ){
    					
    					alertAnulledOrReject(newState);
    					
    				}else{

        				//Mando el dialogo a ejecutar la accion multiple
        				String certs = certificateNumberMessage(certificatesNums);
        				
        				String headerMessage = confirmHeaderMessage(newState);
        				String bodyMessage = confirmbodyMessage(newState,certs);
        				
        				question(headerMessage,bodyMessage);
    				}
    				
    			}else{
    				//manda alerta de que no se puede ejecutar la accion
    				String messageBody = errorStateMessageAlert(newState,strCertInvalid);
    				alert(messageBody);
    			}
    		}
    		
    	}else{
    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MODIFY_ELIMINAR_CERTIFICATE_SELECT);
    		alert(headerMessage,bodyMessage);
    	}
    	return "";
    }
    
    /**
     * Certificate approval.
     */
    @LoggerAuditWeb
    public void certificateApproval() {
		try{

		if(certificatesNums != null)
			certificatesNums.clear();
		else
			certificatesNums = new ArrayList<Long>();
		
		List<PhysicalCertificateExt> existedCertificateList = new ArrayList<PhysicalCertificateExt>();
		PhysicalOperationDetail physicalOperationDetail = null;
			for (PhysicalCertificateExt certificate : selectedPhysicalCertificatesExt) {
				physicalOperationDetail = certificateDepositeBeanFacade.getPhysicalOperationCurrent(certificate.getIdPhysicalOperationDetail());
				if (physicalOperationDetail.getState().equals(StateType.APPROVED.getCode())) {
					existedCertificateList.add(certificate);
					certificatesNums.add( Long.valueOf(certificate.getPhysicalCertificate().getCertificateNumber()) );
				}
			}
			
			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			for (PhysicalCertificateExt physicalCertificateExt : selectedPhysicalCertificatesExt) {
				certificatesNums.add( Long.valueOf(physicalCertificateExt.getPhysicalCertificate().getCertificateNumber()) );
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.APPROVED.getCode());
				commonUpdateBeanInf.setOperation(GeneralConstants.APPROVE_OPERATION_CONSTANT);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.DEPOSITEO.getCode()));
				commonUpdateBeanInf.setIdCustodyOperationPk(physicalCertificateExt.getRequestNumber());
				commonUpdateBeanInf.setCertificatePk(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
			}
			certificateDepositeBeanFacade.updateDetailApproveDeposit(lstCommonUpdateBeanInf);
			
				String approveCerts = certificateNumberMessage(certificatesNums);
				String bodyMessage = "";
				
				if(certificatesNums.size()>1){
					bodyMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_APPROVAL_SUCCESS_MULTI,approveCerts);
				}else{
					bodyMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_APPROVAL_SUCCESS,approveCerts);
				}
				
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE);
				
				alert(headerMessage,bodyMessage);
				

				/*** JH NOTIFICACIONES ***/
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_APPROVE.getCode());
				
				if(isDepositary){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   null, null);
				}else if(isBcr){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   null, null);
				}else if(isParticipant){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   null, null);
				}
				/*** end JH NOTIFICACIONES ***/
				
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
    /**
     * Certificate question.
     */
    public void certificateQuestion(){
    	
    	String certs = certificateNumberMessage(certificatesNums);
    	String headerMessage = "";
		String bodyMessage = "";
		
    	if(confirmAlertAction.equals("certificateRejection_anull")){
    		headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		//headerMessage = confirmHeaderMessage(StateType.ANNULLED.getCode());
    		bodyMessage = confirmbodyMessage(StateType.ANNULLED.getCode(),certs);
    	}else{//certificateRejection_reject
    		headerMessage = confirmHeaderMessage(StateType.REJECTED.getCode());
    		bodyMessage = confirmbodyMessage(StateType.REJECTED.getCode(),certs);
    	}
    	removeAlert();
    	confirmAlertAction = "certificateRejection";
		question(headerMessage,bodyMessage);
    }
    
    /**
     * Certificate rejection.
     */
    @LoggerAuditWeb
    public void certificateRejection() {
		try{

		boolean flagApproved = false;	
			
		if(certificatesNums != null)
			certificatesNums.clear();
		else
			certificatesNums = new ArrayList<Long>();
		
		CommonUpdateBeanInf commonUpdateBeanInf = null;
		List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
		Long participantPk = null;
		Integer physicalCertificateState=null;
		for (PhysicalCertificateExt physicalCertificateExt : selectedPhysicalCertificatesExt) {
			certificatesNums.add( Long.valueOf(physicalCertificateExt.getPhysicalCertificate().getCertificateNumber()) );
			commonUpdateBeanInf = new CommonUpdateBeanInf();
			commonUpdateBeanInf.setOperationType(new Long(RequestType.DEPOSITEO.getCode()));
			
			if (physicalCertificateExt.getState().equals(StateType.APPROVED.getCode())) {
				flagApproved = true;
				this.setViewOperationType(ViewOperationsType.REJECT.getCode());
				commonUpdateBeanInf.setState(StateType.REJECTED.getCode());
				commonUpdateBeanInf.setOperation(GeneralConstants.REJECT_OPERATION_CONSTANT);
				physicalCertificateState = StateType.REJECTED.getCode();
			} else {
				flagApproved = false;
				this.setViewOperationType(ViewOperationsType.ANULATE.getCode());
				commonUpdateBeanInf.setOperation(GeneralConstants.CANCEL_DEPOSIT_OIPERATION);
				commonUpdateBeanInf.setState(StateType.ANNULLED.getCode());
				physicalCertificateState = StateType.ANNULLED.getCode();
			}
			commonUpdateBeanInf.setIdCustodyOperationPk(physicalCertificateExt.getRequestNumber());
			commonUpdateBeanInf.setCertificatePk(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
			commonUpdateBeanInf.setRejectMotiveS(reasonSelected);
			commonUpdateBeanInf.setRejectMotiveOther(rejectOther);
			lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
			participantPk = physicalCertificateExt.getParticipantPk();
		}
		
		if (physicalCertificateState.equals(StateType.REJECTED.getCode())) {
			certificateDepositeBeanFacade.updateDetailRejectDeposit(lstCommonUpdateBeanInf);
		}else{
			certificateDepositeBeanFacade.updateDetailCancelDeposit(lstCommonUpdateBeanInf);
		}
		
		String certs = certificateNumberMessage(certificatesNums);
		String bodyMessage = "";
		
		if (flagApproved) {
			
			if(certificatesNums.size()>1){
				bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECT_SUCCESS_MULTI,certs);
			}else{
				bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECT_SUCCESS,certs);
			}
			
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT);
			alert(headerMessage,bodyMessage);
			
		} else {
			
			if(certificatesNums.size()>1){
				bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_ANNULAR_SUCCESS,certs);
			}else{
				bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_ANNULAR_SUCCESS_MULTI,certs);
			}
			
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL);
			alert(headerMessage,bodyMessage);
		}
		reasonSelected = -1;
		rejectOther = "";
		Object[] parameters = new Object[1];
		parameters[0] =certs;

		/*** JH NOTIFICACIONES H**/
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REJECT.getCode());
		
		if(isDepositary){
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,participantPk, parameters);
		}else if(isBcr){
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,participantPk, parameters);
		}else if(isParticipant){
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,participantPk, parameters);
		}
		/*** end JH NOTIFICACIONES ***/
		
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
    /**
     * Certificate application.
     */
    @LoggerAuditWeb
    public void certificateApplication() {

		try{
			
			if(certificatesNums != null){
				certificatesNums.clear();
			}else{
				certificatesNums = new ArrayList<Long>();
			}
			
			List<PhysicalCertificateExt> existedCertificateList = new ArrayList<PhysicalCertificateExt>();
			PhysicalOperationDetail physicalOperationDetail = null;
			Long participantPk = null;
			for (PhysicalCertificateExt certificate : selectedPhysicalCertificatesExt) {
				physicalOperationDetail = certificateDepositeBeanFacade.getPhysicalOperationCurrent(certificate.getIdPhysicalOperationDetail());
				if (physicalOperationDetail.getIndApplied() == 1){
					existedCertificateList.add(certificate);
					certificatesNums.add( Long.valueOf(certificate.getPhysicalCertificate().getCertificateNumber()) );
				}
				participantPk = certificate.getParticipantPk();
			}

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			List<PhysicalCertificate> physicalCertificatesApllied = new ArrayList<PhysicalCertificate>(); 
			for (PhysicalCertificateExt physicalCertificateExt : selectedPhysicalCertificatesExt) {
				certificatesNums.add( Long.valueOf(physicalCertificateExt.getPhysicalCertificate().getCertificateNumber()) );
				physicalCertificatesApllied.add(physicalCertificateExt.getPhysicalCertificate());
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.CONFIRMED.getCode());
				commonUpdateBeanInf.setOperationType(new Long(RequestType.DEPOSITEO.getCode()));
				commonUpdateBeanInf.setOperation(GeneralConstants.APPLY_OPERATION_CONSTANT);
				commonUpdateBeanInf.setIntAppilied(Integer.valueOf(1));
				commonUpdateBeanInf.setIdCustodyOperationPk(physicalCertificateExt.getRequestNumber());
				commonUpdateBeanInf.setCertificatePk(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
			}
			
			String headerMessage = "";
			String bodyMessage = "";			
	
			certificateDepositeBeanFacade.certificateActionApplication( lstCommonUpdateBeanInf,
																		physicalCertificatesApllied,RequestType.DEPOSITEO.getCode());
			
			
			String certs = certificateNumberMessage(certificatesNums);
			
			if(certificatesNums.size()>1){
				bodyMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_APLICAR_SUCCESS_MULTI,certs);
			}else{
				bodyMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_APLICAR_SUCCESS,certs);
			}
			
			headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			alert(headerMessage,bodyMessage);
			
			//Enviando la solicitud de registro al AYNI 1210
			XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero = certificateDepositeBeanFacade.sendDepositeRecord(selectedPhysicalCertificatesExt);
			if(xmlRespuestaActivoFinanciero.getCodResp().equalsIgnoreCase("0000")){
				bodyMessage = bodyMessage + ". " + PropertiesUtilities.getMessage(PropertiesConstants.MSG_APLICAR_SUCCESS_AYNI_0000);
			}else{
				bodyMessage = bodyMessage + ". " + PropertiesUtilities.getMessage(PropertiesConstants.MSG_APLICAR_SUCCESS_AYNI_1000);
				bodyMessage = bodyMessage + " " + xmlRespuestaActivoFinanciero.getDescResp();
			}
				
			alert(headerMessage,bodyMessage);

			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_APPLY.getCode());
			
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
						   participantPk, null);
			}else if(isBcr){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   participantPk, null);
			}else if(isParticipant){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   null, null);
			}
			/*** end JH NOTIFICACIONES ***/
		}catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
    /**
     * Certificate confirmation.
     */
    @LoggerAuditWeb
    public void certificateConfirmation() {
		
		try{
			
		if(certificatesNums != null)
			certificatesNums.clear();
		else
			certificatesNums = new ArrayList<Long>();
		
		List<PhysicalCertificateExt> existedCertificateList = new ArrayList<PhysicalCertificateExt>();
		PhysicalOperationDetail physicalOperationDetail = null;
		
		if (selectedPhysicalCertificatesExt != null&& selectedPhysicalCertificatesExt.length > 0) {
			for (PhysicalCertificateExt certificate : selectedPhysicalCertificatesExt) {
				physicalOperationDetail = certificateDepositeBeanFacade.getPhysicalOperationCurrent(certificate.getIdPhysicalOperationDetail());
				if (physicalOperationDetail.getState().equals(StateType.CONFIRMED.getCode())){
					existedCertificateList.add(certificate);
					certificatesNums.add( Long.valueOf(certificate.getPhysicalCertificate().getCertificateNumber()) );
				}
			}
				List<PhysicalCertificate> physicalCertificateConfirm = new ArrayList<PhysicalCertificate>();
				CommonUpdateBeanInf commonUpdateBeanInf = null;
				List<PhysicalBalanceDetail> physicalBalanceDetails = new ArrayList<PhysicalBalanceDetail>();
				List<PhysicalCertificateMovement> physicalCertificateMovements = new ArrayList<PhysicalCertificateMovement>();
				List<PhysicalCertificate> physicalCertificates = new ArrayList<PhysicalCertificate>();
				PhysicalCertificateMovement physicalCertificateMovement = null;
				PhysicalCertificateOperation physicalCertificateOperation = null;
				
				List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
				Long participantPk = null;
				for (PhysicalCertificateExt physicalCertificateExt : selectedPhysicalCertificatesExt) {
					certificatesNums.add( Long.valueOf(physicalCertificateExt.getPhysicalCertificate().getCertificateNumber()) );
					physicalCertificateConfirm.add(physicalCertificateExt.getPhysicalCertificate());
					physicalCertificateMovement = new PhysicalCertificateMovement();
					commonUpdateBeanInf = new CommonUpdateBeanInf();
					commonUpdateBeanInf.setState(StateType.CONFIRMED.getCode());
					commonUpdateBeanInf.setOperationType(new Long(RequestType.DEPOSITEO.getCode()));
					commonUpdateBeanInf.setOperation(GeneralConstants.CONFIRM_OPERATION_CONSTANT);
					commonUpdateBeanInf.setIdCustodyOperationPk(physicalCertificateExt.getRequestNumber());
					commonUpdateBeanInf.setCertificatePk(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
					commonUpdateBeanInf.setAccHolderPk(physicalCertificateExt.getHolderAccount().getIdHolderAccountPk());
					commonUpdateBeanInf.setIntAppilied(GeneralConstants.ZERO_VALUE_INTEGER);
					lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
					physicalCertificateOperation = certificateDepositeBeanFacade.findById(physicalCertificateExt.getRequestNumber());
					
					PhysicalBalanceDetail physicalBalanceDetail = withdrawalDepositfacade.getPhysicalBalance(commonUpdateBeanInf);
					if (Validations.validateIsNull(physicalBalanceDetail)) {
						physicalBalanceDetail= new PhysicalBalanceDetail();
						physicalBalanceDetail.setPhysicalCertificate(physicalCertificateExt.getPhysicalCertificate());
						physicalBalanceDetail.setHolderAccount(physicalCertificateOperation.getHolderAccount());
						physicalBalanceDetail.setParticipant(physicalCertificateOperation.getParticipant());
						physicalBalanceDetail.setCertificateQuantity(physicalCertificateExt.getPhysicalCertificate().getCertificateQuantity());
						physicalBalanceDetail.setRegisterDate(CommonsUtilities.currentDateTime());
						physicalBalanceDetail.setCurrency(physicalCertificateExt.getPhysicalCertificate().getCurrency());
						physicalBalanceDetail.setNominalValue(physicalCertificateExt.getPhysicalCertificate().getNominalValue());
					}
					physicalBalanceDetail.setDepositeDate(CommonsUtilities.currentDateTime());
					physicalBalanceDetail.setState(DEPOSITED);
					
					physicalBalanceDetails.add(physicalBalanceDetail);
					
					physicalCertificateMovement.setHolderAccount(physicalCertificateOperation.getHolderAccount());
					physicalCertificateMovement.setMovementDate(CommonsUtilities.currentDateTime());
					physicalCertificateMovement.setPhysicalCertificate(physicalCertificateExt.getPhysicalCertificate());
					physicalCertificateMovement.setPhysicalCertificateOperation(physicalCertificateOperation);
					physicalCertificateMovement.setCertificateQuantity(physicalCertificateExt.getPhysicalCertificate().getCertificateQuantity());
					physicalCertificateMovement.setCurrency(physicalCertificateExt.getPhysicalCertificate().getCurrency());
					physicalCertificateMovement.setNominalValue(physicalCertificateExt.getPhysicalCertificate().getNominalValue());
					physicalCertificateMovement.setCurrency(physicalCertificateExt.getPhysicalCertificate().getCurrency());
					physicalCertificateMovement.setCertificateQuantity(physicalCertificateExt.getPhysicalCertificate().getCertificateQuantity());
					physicalCertificateMovements.add(physicalCertificateMovement);
					
					physicalCertificates.add(physicalCertificateExt.getPhysicalCertificate());
					participantPk = physicalCertificateExt.getParticipantPk();
					
				}

				certificateDepositeBeanFacade.certificateConfirmation(lstCommonUpdateBeanInf,
																	  physicalCertificateMovements,
																	  physicalBalanceDetails,
																	  physicalCertificateConfirm,RequestType.DEPOSITEO.getCode());
																	  
				String certs = certificateNumberMessage(certificatesNums);
				String bodyMessage = "";
				
				if(certificatesNums.size()>1){
					bodyMessage =PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_SUCCESS_MULTI,certs);
				}else{
					bodyMessage =PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_SUCCESS,certs);
				}
				
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
				
				alert(headerMessage,bodyMessage);
				

				/*** JH NOTIFICACIONES ***/
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_CONFIRM.getCode());
				
				if(isDepositary){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   participantPk, null);
				}else if(isBcr){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   null, null);
				}else if(isParticipant){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, 
							   null, null);
				}
				/*** end JH NOTIFICACIONES ***/
			} else {

				String headerMessage =PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR); 
				String bodyMessage =PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CONFIRM);
				alert(headerMessage,bodyMessage);
				
			}
		}catch (Exception e) {
	           excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
    
    /**
     * Valid lis certificate for state.
     *
     * @param actualState the actual state
     * @return true, if successful
     */
    public String validLisCertificateForState(Integer actualState){
    	String certValid=GeneralConstants.EMPTY_STRING;
    	int countApply=0;
		for (PhysicalCertificateExt certificate : selectedPhysicalCertificatesExt) {
			
			if (certificate.getState().equals(StateType.CONFIRMED.getCode()) && 
    			Validations.validateIsNotNull(certificate.getPhysicalOperationDetail().getIndApplied())
			){
				if(certificate.getPhysicalOperationDetail().getIndApplied().equals(Integer.valueOf(1))){
					certValid+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					countApply++;
				}
			}
			if(countApply==0){
				if( !certificate.getState().equals(actualState) ){
					certValid+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}else{
					certificatesNums.add( Long.valueOf(certificate.getPhysicalCertificate().getCertificateNumber()) );
				}
			}
			else{
				countApply=0;
			}
				
		}
		if(certValid.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
			certValid=certValid.substring(0,certValid.length()-1);
		return certValid;
	}
    
    /**
     * Previously message alert.
     *
     * @param actualState the actual state
     * @return the string
     */
    public String previouslyMessageAlert(Integer actualState){
    	if(actualState.equals(StateType.APPROVED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPROVED_PREVIOUSLY_MULTI);
			
		}else if(actualState.equals(StateType.ANNULLED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_CANCELLED_PREVIOUSLY_MULTI);
			
		}else if(actualState.equals(StateType.CONFIRMED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRMED_PREVIOUSLY_MULTI);
			
		}else if(actualState.equals(StateType.REJECTED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECTED_PREVIOUSLY_MULTI);
			
		}else if(actualState.equals(StateType.APPLY.getCode())){
			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPLIED_PREVIOUSLY_MULTI);
		}
    	
    	return "";
    }
    
    /**
     * Confirm header message.
     *
     * @param state the state
     * @return the string
     */
    public String confirmHeaderMessage(Integer state){
    	
    	if(state.equals(StateType.APPROVED.getCode())){

    		return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE);
			
		}else if(state.equals(StateType.ANNULLED.getCode())){

			return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL);
			
		}else if(state.equals(StateType.CONFIRMED.getCode())){

			return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
			
		}else if(state.equals(StateType.REJECTED.getCode())){

	    	return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT);
			
		}else if(state.equals(StateType.APPLY.getCode())){
	    	return PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPLY);
		}
    	
    	return "";
    }

    /**
     * Confirmbody message.
     *
     * @param state the state
     * @param certs the certs
     * @return the string
     */
    public String confirmbodyMessage(Integer state, String certs){
    	
    	if(state.equals(StateType.APPROVED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_APPROVAL_MULTI,certs);
			
		}else if(state.equals(StateType.ANNULLED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_ANNULAR_MULTI,certs);
			
		}else if(state.equals(StateType.CONFIRMED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_CERTIFIACTE_MULTI,certs);
			
		}else if(state.equals(StateType.REJECTED.getCode())){
			
			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECT_CERTIFICATE,certs);
			
		}else if(state.equals(StateType.APPLY.getCode())){
			return  PropertiesUtilities.getMessage(PropertiesConstants.MSG_APLICAR_CERTIFICATE_MULTI,certs);
		}
    	
    	return "";
    }
    
    /**
     * Error state message alert.
     *
     * @param actualState the actual state
     * @param strCert the str cert
     * @return the string
     */
    public String errorStateMessageAlert(Integer actualState,String strCert){
    	if(actualState.equals(StateType.APPROVED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.ERRO_APPROVAL,new Object[]{strCert});
			
		}else if(actualState.equals(StateType.ANNULLED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CANCEL,new Object[]{strCert});
			
		}else if(actualState.equals(StateType.CONFIRMED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CONFIRM,new Object[]{strCert});
			
		}else if(actualState.equals(StateType.REJECTED.getCode())){

			return  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_REJECT,new Object[]{strCert});
			
		}else if(actualState.equals(StateType.APPLY.getCode())){	//APPLY
			return  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_APPLY,new Object[]{strCert});
			
		}
    	
    	return "";
    }
    
    /**
     * Sets the certificatedetail view.
     *
     * @param physicalCertificate the new certificatedetail view
     */
    public void setCertificatedetailView(PhysicalCertificateExt physicalCertificate){
		try {
			
	    	physicalCertificateTemp = physicalCertificate;
	    	
			if (physicalCertificateTemp.getHolderAccount().getIdHolderAccountPk() != null){
					holderAccountDetails = certificateDepositeBeanFacade.findHolderAccount(physicalCertificateTemp.getHolderAccount().getIdHolderAccountPk());
			}else{
				holderAccountDetails = physicalCertificateTemp.getHolderAccount();
			}
			
			holderAccountList = new ArrayList<>();
						
			if(Validations.validateIsNotNullAndNotEmpty(holderAccountDetails)){
				holderAccountList.add(holderAccountDetails);
			}
			
			physicalCertificateTemp.getPhysicalCertificate().setClassType(securityClassTypeMapDescription(physicalCertificateTemp.getPhysicalCertificate().getSecurityClass()));
			// issue 895: si el valor no existe, entonces se setea el TIPO DE RENTA en null
			physicalCertificateTemp.getPhysicalCertificate().setInstrumentTypeName( physicalCertificateTemp.getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.YES.getCode()) ? getInstrumntType(physicalCertificateTemp.getPhysicalCertificate().getInstrumentType()) : null);
			//physicalCertificateTemp.getPhysicalCertificate().setCurrencyType(getCurrencyType(physicalCertificateTemp.getPhysicalCertificate().getCurrency()));
			// issue 895: si el valor no existe, entonces se setea la MONEDA en null
			physicalCertificateTemp.getPhysicalCertificate().setCurrencyFullDesc( physicalCertificateTemp.getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.YES.getCode()) ? getCurrencyType(physicalCertificateTemp.getPhysicalCertificate().getCurrency()) : null);
			physicalCertificateTemp.setStateType(depositStateTypeMapDescription(physicalCertificateTemp.getPhysicalOperationDetail().getState()));
			physicalCertificateTemp.setRequestStateType(depositRequestStateTypeMapDescription(physicalCertificateTemp.getRequestState()));
			
			if(physicalCertificateTemp.getPhysicalCertificate().getIndExistsSecurity().equals(BooleanType.YES.getCode())
					&& physicalCertificateTemp.getPhysicalCertificate().getInstrumentType().equals(RENTA_FIJA)){
				renderedInstrumentType=true;
				physicalCertificateTemp.getPhysicalCertificate().setPaymentType(paymentTypeMapDescription(physicalCertificateTemp.getPhysicalCertificate().getPaymentPeriodicity()));
			}else{
				renderedInstrumentType=false;
			}
			
			if(physicalCertificateTemp.getPhysicalOperationDetail().getIndApplied().equals(new Integer(1))){
				physicalCertificateTemp.setIndApplied(true);
			}else if(physicalCertificateTemp.getPhysicalOperationDetail().getIndApplied().equals(new Integer(0))){
				physicalCertificateTemp.setIndApplied(false);
			}
			
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateTemp.getPhysicalCertificate().getIdPhysicalCertificatePk())){
				List<PhysicalBalanceDetail> lstPhysicalBalanceDetail = certificateDepositeBeanFacade.getPhysicalBalanceDetail(
						physicalCertificateTemp.getPhysicalCertificate().getIdPhysicalCertificatePk());
				if(Validations.validateListIsNotNullAndNotEmpty(lstPhysicalBalanceDetail)){
					physicalCertificateTemp.setDepositeDate(lstPhysicalBalanceDetail.get(0).getDepositeDate());
				}				
			}
			
			List<PhysicalCertMarketfact> lstMarket =  certificateDepositeBeanFacade.lstPhysicalCertMarketfacts(physicalCertificateTemp.getPhysicalCertificate().getIdPhysicalCertificatePk());
			physicalCertificateTemp.getPhysicalCertificate().setPhysicalCertMarketfacts(lstMarket);
			
		} catch (ServiceException e) {
			
			e.printStackTrace();
		}
    }
    
	/**
     * Security events.
     */
    public void securityEvents(){

		try {
			
			if(userPrivilege.getUserAcctions().isSearch())
				flagSearch=true;

			if(userPrivilege.getUserAcctions().isRegister())
				flagRegister=true;
			
			participantLogin = userInfo.getUserAccountSession().getParticipantCode();
			
			if ( userInfo.getUserAccountSession().isInstitution(InstitutionType.BCR) ){
				isBcr = true;
			}else if(userInfo.getUserAccountSession().isParticipantInstitucion() ){
				participant = certificateDepositeBeanFacade.findParticipant(participantLogin);

				isParticipant = true;
				participantSelected = participantLogin;
				
				if(participant.getState().equals(ParticipantStateType.BLOCKED.getCode())){
					flagRegister = false;
				}
				
				RequestContext.getCurrentInstance().update("cboparticipant");
			}else{	
				isDepositary = true;
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
    
	//Si cambio de participante
    /**
	 * Change pariticipant search.
	 */
	public void changePariticipantSearch(){
		lstSourceAccountTo= new ArrayList<HolderAccountHelperResultTO>();
		sourceHolder=new Holder();
    	holderAccount = new HolderAccount();
    	resultExcel = new DataExcelRegCertificateDepositeTO();
    }
    
    /**
     * Valid status holder.
     */
//    public void validStatusHolder(){
//    	
//    	if( !(Validations.validateIsNotNull(holderAccount) && Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
//    		return;
//    	}
//    	
//    	HolderAccount varHolderAccount = getHolderAccountAndDetails(holderAccount);
//    	if(	Validations.validateIsNotNull(varHolderAccount) &&
//    		Validations.validateIsNotNullAndPositive(varHolderAccount.getIdHolderAccountPk()) ){
//    		holderAccount = varHolderAccount;
//    	}else{			
//    		alert(PropertiesUtilities.getMessage(PropertiesConstants.DEMATERIALIZATION_CERTIFICATE_WRONG_ACCCOUNT));
//    	}
//
//    	if(!Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber())){
//    		holderAccount = new HolderAccount();
//    		return ;
//    	}
//    	if(holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
//    		
//    		for (HolderAccountDetail holderAccountDetail : holderAccount.getHolderAccountDetails()) {
//				if(!HolderStateType.REGISTERED.getCode().equals(holderAccountDetail.getHolder().getStateHolder())){
//					removeAlert();
//		        	String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CERTIFICATE_HOLDER_NOT_ALLOWED);
//		        	String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
//					alert(headerMessage,bodyMessage);
//		    		holderAccount = new HolderAccount();
//		    		break;
//				}
//			}
//    		
//    		return ;
//    	}else{
//    		//AGREGAR si hay alguna excepcion con cevaldom
//        	if(holderAccount.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
//        		
//        	}
//        	removeAlert();
//        	String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED);
//        	String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
//			alert(headerMessage,bodyMessage);
//    		//se limpia
//    		holderAccount = new HolderAccount();
//    	}
//    }
    
    
    public void validAccount(){
    	
    	if( !(Validations.validateIsNotNull(holderAccount)) && !(Validations.validateIsNotNull(holderAccount.getIdHolderAccountPk()))){
    		holderAccount = new HolderAccount();
    	}
    	
    	
    }
    
    /**
     * Clear pop up certificate.
     */
    private void clearPopUpCertificate(){
    	physicalCertificate = new PhysicalCertificate();
    	security = new Security();
		certificateNumber = null;
		CertificateQuantity = null;
		//issuerSelected = null;
		issuer = new Issuer();
		fromCorrelativo = null;
		toCorrelativo = null;
		numberOfCertificates = null;
		selectCertificateCount = new Integer(1);
		instrumentTypeSelected = -1;
		securityClassSelected = -1;
		securityClassList = new ArrayList<ParameterTable>();
		initialDate = CommonsUtilities.currentDate();
		finalDate = CommonsUtilities.currentDate();
		modify=false;
		show = false;
		securityEvents();
		regLstPhysicalCertMarketfacts = null;
		regPhysicalCertMarketfact = new PhysicalCertMarketfact();
		arrRegPhysicalCertMarketfact = null;
		certificateDate = null;
		lstPhysicalCertFile = new ArrayList<>();
		existSecurity = false;
		certFileSelected = -1;
    }
    
    /**
     * Clear header.
     */
    public void clearHeader(){
    	if(!isParticipant){
			participantSelected = null;
		}
		sourceHolder = new Holder();
		lstSourceAccountTo = null;
		sourceAccountTo = null;
		holderAccount = new HolderAccount();
    }
    
    /**
     * List holidays.
     *
     * @throws ServiceException the service exception
     */
    private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
    
    /**
     * Load participant.
     *
     * @throws ServiceException the service exception
     */
    private void loadParticipant() throws ServiceException{
    	
    	if(Validations.validateIsNotNullAndPositive(participant.getState())){
	    	if(
    			participant.getState().equals(ParticipantStateType.BLOCKED.getCode()) || 
    			participant.getState().equals(ParticipantStateType.REGISTERED.getCode())
	    	){
	    		participant.setCompleteDescription(participant.getDescription() + " - " + participant.getIdParticipantPk().toString());
	    		participantList.add(participant);
	    		
			}else{
				Map<String, Object> filter = new HashMap<String, Object>();
				List<Integer> states = new ArrayList<Integer>();
				states.add(ParticipantStateType.REGISTERED.getCode());
				filter.put("states", states);
				participantList = helperComponentFacade.getComboParticipantsByMapFilter(filter);
				
	    	/*	participantList = certificateDepositeBeanFacade.getListParticicpant(ParticipantStateType.REGISTERED.getCode(),
	    																			ParticipantClassType.AFP.getCode());*/
			}
    	}else{

			Map<String, Object> filter = new HashMap<String, Object>();
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			//states.add(ParticipantStateType.BLOCKED.getCode());
			filter.put("states", states);
			participantList = helperComponentFacade.getComboParticipantsByMapFilter(filter);
			
    		/*List<Integer> lstStates = new ArrayList<Integer>();
    		lstStates.add(ParticipantStateType.REGISTERED.getCode());
    		lstStates.add(ParticipantStateType.BLOCKED.getCode());
    		participantList = certificateDepositeBeanFacade.getListParticicpant(lstStates, ParticipantClassType.AFP.getCode());*/
    	}
    	
	}
    
    /**
     * Load deposite status list.
     *
     * @throws ServiceException the service exception
     */
    public void loadDepositeStatusList() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.SECURITY_STATES.getCode());
			parameterTableTO.setParameterTablePk(StateType.AUTHORIZED.getCode());
		statusDepositList = generalParametersFacade.getListParameterTableExcludePkServiceFacade(parameterTableTO);
	}

    public void loadParameterCertFileList() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setMasterTableFk(MasterTableType.PHYSICAL_CERT_FILES.getCode());
			
			// si el valor existe entonces solo se pedira que adjunte el TITULO FISICO
			if(existSecurity)
				parameterTableTO.setParameterTablePk(ParameterFileType.TITULO_FISICO.getCode());
		lstFileTypes = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
    
    /**
     * Certificate number message.
     *
     * @param certificates the certificates
     * @return the string
     */
	private String certificateNumberMessage(List<Long> certificates){
		String certs = StringUtils.EMPTY;
		if(certificates != null && certificates.size() ==1)
			return certificates.get(0).toString();
		if(certificates != null && certificates.size() > 0){
			for(Long crt : certificates){
				if(crt != null){
					if(StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)){
						certs = crt.toString();
					}else{
						certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt.toString()));
					}
				}
			}
		}
		return certs;
	}
    
	/**
	 * Gets the state class type.
	 *
	 * @param stType the st type
	 * @return the state class type
	 */
	private String getStateClassType(Object stType) {
		String stateType = null;
		Set<Entry<Integer, StateType>> mapStateClass = StateType.lookup.entrySet();
		for (Entry<Integer, StateType> entry : mapStateClass) {
			if (((StateType) entry.getValue()).getCode() == Integer.parseInt(stType.toString())) {
				stateType = ((StateType) entry.getValue()).getValue();
			}
		}
		return stateType;
	}

	/**
	 * Gets the instrumnt type.
	 *
	 * @param instrumntType the instrumnt type
	 * @return the instrumnt type
	 */
	private String getInstrumntType(Object instrumntType) {
		String instrumentType = null;
		Set<Entry<Integer, InstrumentType>> mapInstrumntClass = InstrumentType.lookup.entrySet();
		for (Entry<Integer, InstrumentType> entry : mapInstrumntClass) {
			if (((InstrumentType) entry.getValue()).getCode() == Integer.parseInt(instrumntType.toString())) {
				instrumentType = ((InstrumentType) entry.getValue()).getValue();
			}
		}
		return instrumentType;
	}

	/**
	 * Gets the reason type.
	 *
	 * @param reasonType the reason type
	 * @return the reason type
	 */
	private String getReasonType(Object reasonType) {
		String reasnType = null;
		Set<Entry<Integer, ReasonType>> mapReasonType = ReasonType.lookup.entrySet();
		for (Entry<Integer, ReasonType> entry : mapReasonType) {
			if (((ReasonType) entry.getValue()).getCode() == Integer.parseInt(reasonType.toString())) {
				reasnType = ((ReasonType) entry.getValue()).getValue();
			}
		}
		return reasnType;
	}

	/**
	 * Gets the currency type.
	 *
	 * @param curType the cur type
	 * @return the currency type
	 */
	private String getCurrencyType(Object curType) {
		String currencyType = null;
		Set<Entry<Integer, CurrencyType>> mapCurrencyType = CurrencyType.lookup.entrySet();
		for (Entry<Integer, CurrencyType> entry : mapCurrencyType) {
			if (((CurrencyType) entry.getValue()).getCode() == Integer.parseInt(curType.toString())) {
				currencyType = ((CurrencyType) entry.getValue()).getValue();
			}
		}
		return currencyType;

	}

	/**
	 * Gets the payment type.
	 *
	 * @param payType the pay type
	 * @return the payment type
	 */
	private String getPaymentType(Object payType) {
		String paymentType = null;
		Set<Entry<Integer, PaymentType>> mapCurrencyType = PaymentType.lookup.entrySet();
		for (Entry<Integer, PaymentType> entry : mapCurrencyType) {
			if (((PaymentType) entry.getValue()).getCode() == Integer.parseInt(payType.toString())) {
				paymentType = ((PaymentType) entry.getValue()).getValue();
			}
		}
		return paymentType;
	}
	
	/**
	 * Find participant selected.
	 *
	 * @return the participant
	 */
	public Participant findParticipantSelected() {
		if (participantSelected != null) {
			for (Participant curParticipant : participantList) {
				if (participantSelected.equals(curParticipant.getIdParticipantPk())) {
					return curParticipant;
				}
			}
		}
		return null;
	}
	
    
    /* Alertas */
    
    /**
     * Removes the alert.
     */
    public void removeAlert(){
    	confirmAlertAction = "";
    }
    
    /**
     * Removes the alert pop up.
     */
    public void removeAlertPopUp(){
    	confirmAlertAction = "";
    }
    
    /**
     * Removes the alert new.
     */
    public void removeAlertNew(){
		JSFUtilities.executeJavascriptFunction("PF('idCertificateRegisterW').hide();");
    	confirmAlertAction = "";
    }
    
    
    /**
     * Aler new certificate.
     */
    public void alerNewCertificate(){
    	JSFUtilities.executeJavascriptFunction("PF('idCertificateRegisterW').show();");
    	validatorDisabled = false;
	}
    
    /**
     * Alert.
     *
     * @param bodyMessage the body message
     */
    public void alert(String bodyMessage){
		String headerMessage=PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 GeneralConstants.LBL_HEADER_ALERT);
		
		showMessageOnDialog(headerMessage,bodyMessage); 
    	JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
		//JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Alert.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */ 
	public void alert(String headerMessage,String bodyMessage){
		
		showMessageOnDialog(headerMessage,bodyMessage); 
		JSFUtilities.updateComponent(":formPopUp:alterValidation");
    	JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
		//JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Question.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */
	public void question(String headerMessage,String bodyMessage){
    	//RequestContext.getCurrentInstance().update(":formPopUp:cnfIdCAskDialog");	
    	showMessageOnDialog(headerMessage,bodyMessage); 
    	JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
    	//validatorDisabled = false;
    	//JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Alert anulled or reject.
	 *
	 * @param state the state
	 */
	public void alertAnulledOrReject(Integer state){
		String headerMessage="";

		if(state.equals(StateType.ANNULLED.getCode())){
			headerMessage=  PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_ANNULMENT);
		}else if(state.equals(StateType.REJECTED.getCode())){
			headerMessage=  PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_REJECTED);
		}

		reasonSelected = -1;
		rejectOther = "";
		JSFUtilities.updateComponent(":formPopUp:alterReject");
		JSFUtilities.resetViewRoot();
		showMessageOnDialog(headerMessage,""); 
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').show();");
		//JSFUtilities.showSimpleValidationDialog();
	}
	
	/**
	 * Confirm alert actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmAlertActions(){
		
		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(tmpAction.equals("certificateApproval")){
			confirmAlertAction = "searchPhysicalCertificateView";
			certificateApproval();
		}else if(tmpAction.equals("certificateConfirmation")){
			confirmAlertAction = "searchPhysicalCertificateView";
			certificateConfirmation();
		}else if(tmpAction.equals("certificateApplication")){
			confirmAlertAction = "searchPhysicalCertificateView";
			certificateApplication();
		}else if(tmpAction.equals("certificateRejection")){
			confirmAlertAction = "searchPhysicalCertificateView";
			certificateRejection();
		}else if(tmpAction.equals("addPhysicalCertificate")){
			addPhysicalCertificate();
		}else if(tmpAction.equals("deleteCertificates")){
			deleteCertificates();
		}else if(tmpAction.equals("saveAllPhysicalCertificates")){
			confirmAlertAction="sucessSaveCertificate";
			saveAllPhysicalCertificates();
		}else if(tmpAction.equals("backToScreen")){
			backToScreen();
		}else if(tmpAction.equals("cancelToScreen")){
			cancelToScreen();
		}else if(tmpAction.equals("executeBackToSearch")){
			return executeBackToSearch();
		}else if(tmpAction.equals("clearSolicitudDatos")){
			clearSolicitudDatos();
			removeAlert();
		}else if(tmpAction.equals("saveMassivePhysicalCertificates")){
			confirmAlertAction="sucessSaveCertificate";
			saveMassivePhysicalCertificates();
		}
		
		return "";
	}
	
	// $$$ todo lo que tiene q ver con adjuntos
	/** The participant file name display. */
	private String certFileNameDisplay=null;
	private Integer certFileSelected=null;
	public void changeSelectedCertFileType(){
		certFileNameDisplay = null;		
		if(Validations.validateIsNotNullAndPositive(certFileSelected)){
			validateFileAlreadyLoaded();
		}							
	}
	
	private boolean validateFileAlreadyLoaded(){
		if(Validations.validateListIsNullOrEmpty(lstPhysicalCertFile)){
			return false;
		} else {
			for (PhysicalCertFile objFile : lstPhysicalCertFile) {
				if(certFileSelected.equals(objFile.getDocumentType())){
					certFileSelected = null;
					certFileNameDisplay = null;
					JSFUtilities.showMessageOnDialog(
							PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_ALERT_DOCUMENT_ALREADY_LOADED));
					JSFUtilities.showSimpleValidationDialog();
					return true;
				}
			}
			return false;
		}
			
	}
	
	private String getDisplayName(UploadedFile fileUploaded){
		String fDisplayName = null;
		if(fileUploaded.getFileName().length()>30){
			StringBuilder sbFileName=new StringBuilder();
			sbFileName.append("...");
			sbFileName.append( fileUploaded.getFileName().substring(fileUploaded.getFileName().length()-27) );
			fDisplayName = sbFileName.toString();
		}else{
			fDisplayName =  fileUploaded.getFileName();
		}
		return fDisplayName;
	}
	
	/**
	 * issue 895: metodo para realizar la carga de los archivos de titulos fisicos
	 * @param event
	 */
	public void documentAttachCertificateFile(FileUploadEvent event){
		
		if(Validations.validateIsNotNullAndPositive(certFileSelected)){
			
			//hide dialog verification
			JSFUtilities.updateComponent("opUploadFile");
			JSFUtilities.executeJavascriptFunction("PF('dlgwFileUploadInvFile').hide();");

			String fDisplayName=getDisplayName(event.getFile());
			 
			 if(fDisplayName!=null){
				 certFileNameDisplay=fDisplayName;
			 }
			 for(int i=0;i<lstFileTypes.size();i++){
				if(lstFileTypes.get(i).getParameterTablePk().equals(certFileSelected)){	
					PhysicalCertFile physicalCertFile = new PhysicalCertFile();
					physicalCertFile.setDocumentType(lstFileTypes.get(i).getParameterTablePk());
					physicalCertFile.setDescription(lstFileTypes.get(i).getParameterName());
					
					physicalCertFile.setFilename(fUploadValidateFileNameLength(event.getFile().getFileName()));
					physicalCertFile.setDocumentFile(event.getFile().getContents());
					
					insertOrReplaceCertificateFileItem(physicalCertFile);
				}
			}						
		}
	}
	
	public void insertOrReplaceCertificateFileItem(PhysicalCertFile physicalCertFile){
		
		boolean flag = false;
		
		int listSize = lstPhysicalCertFile.size();
		if(listSize>0){
			for(int i=0;i<listSize;i++)
			{
			
				if(physicalCertFile.getDocumentType().equals(lstPhysicalCertFile.get(i).getDocumentType())){
					lstPhysicalCertFile.remove(i);
					lstPhysicalCertFile.add(i,physicalCertFile);
					certFileSelected = null;
					
					flag = true;
				}
			}
			if(!flag){
				lstPhysicalCertFile.add(physicalCertFile);
				certFileSelected = null;
			}
		}
		else{
			lstPhysicalCertFile.add(physicalCertFile);
			certFileSelected = null;
		}
		
	}
	
	/**
	 * issue 895: metodo para eliminar un registro de la tabla
	 * @param physicalCertFile
	 */
	public void removeCertificateFile(PhysicalCertFile physicalCertFile) {
		
		//lstPhysicalCertFile.remove(physicalCertFile);
		// removiendo manualmente, buscando el indice correcto
		int index = 0;
		for(PhysicalCertFile pcToRemove:lstPhysicalCertFile){
			if(pcToRemove.getDocumentType().equals(physicalCertFile.getDocumentType()))
				break;
			index++;
		}
		lstPhysicalCertFile.remove(index);
		
		certFileNameDisplay = null;
		certFileSelected = null;		
	}
	
	/**
	 * Acept alert actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String aceptAlertActions(){
		
		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(Validations.validateIsNotNull(tmpAction)){
		
			if(tmpAction.equals("searchPhysicalCertificateView")){
				searchPhysicalCertificates();
				return "searchPhysicalCertificateView";
			}else if(tmpAction.equals("removeAlertPopUp")){
				removeAlertPopUp();
				return "";
			}else if(tmpAction.equals("clearSecurity")){
				security = new Security();
				//searchPhysicalCertificates(); //Se aÃ±adio
				return "";
			}else if(tmpAction.equals("removeAlertNew")){
				removeAlertNew();
				return "";
			}else if(tmpAction.equals("sucessSaveCertificate")){
				//clearSearchCriteria();
				searchPhysicalCertificates();
				return "searchPhysicalCertificateView";
			}else{
				removeAlertPopUp();
				return "";
			}
		}else{
			removeAlertPopUp();
			return "";
		}
	}
	
	/**
	 * Cancel alert anull or reject.
	 */
	public void cancelAlertAnullOrReject(){
		removeAlert();
		reasonSelected = -1;
		rejectOther = "";
	}
	
	/**
	 * Exist certificate in list.
	 *
	 * @param certificate the certificate
	 * @return true, if successful
	 */
    public boolean existCertificateInRequestRegitered(PhysicalCertificate certificate){
    	boolean exitsCertificate = false;
    	if (certificate != null) {
    		exitsCertificate = existInSolicitud(certificate);	
			if(exitsCertificate){
				return exitsCertificate;
			} 
		}		
		return false;
    }
    
    /**
     * Certificates count cal.
     *
     * @return true, if successful
     */
    public boolean validateCertificaSave(){
    	String headerMessage = "";
    	String bodyMessage = "";   	
    	if (physicalCertificateList != null && physicalCertificateList.size() > 0) {    		
    		for (PhysicalCertificate certificate : physicalCertificateList) {    			
    			if(existCertificateInRequestRegitered(certificate)){
    				headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
            		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG,
            															new Object[]{certificate.getCertificateNumber().toString(),
            															certificate.getSecurities().getIdSecurityCodePk()});
            		alert(headerMessage,bodyMessage);            		
            		return true;
    			}    			    			
    		}    		
    	}
    	return false;
    }

	/**
	 * Validate search issuer.
	 */
	public void validateSearchIssuer() {

		if ( !(Validations.validateIsNotNull(issuerHelp) && Validations.validateIsNotNull(issuerHelp.getIdIssuerPk())) ) {
			return;
		}

		issuerSearchPK = issuerHelp.getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);

		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {

			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			issuerHelp = new Issuer();
		}
	}
	
	/* 
	 * REGISTER MASSIVE
	 */
	
	/**
	 * New massive request.
	 *
	 * @return the string
	 */
	
	
	
	public String newMassiveRequest(){
		try {
			fileExcelPersist = new PhysicalCertificateFile();
			
			try {
	    		
	    		loadListAndMap();
		    	disabledButtons();
		    	clearSearchCriteria();
		    	physicalCertificate = new PhysicalCertificate();
				physicalCertificateList = new ArrayList<PhysicalCertificate>();
				physicalCertificateSearchList = new ArrayList<PhysicalCertificateExt>();
				issuerList = certificateDepositeBeanFacade.getIssuerList(IssuerStateType.REGISTERED.getCode());
				certificateNumber = null;
				numberOfCertificates = null;
				CertificateQuantity = null;
				regPhysicalCertMarketfact = new PhysicalCertMarketfact();
				arrRegPhysicalCertMarketfact = null;
				regLstPhysicalCertMarketfacts = null;
				resultExcel = new DataExcelRegCertificateDepositeTO();
				
				if(userInfo.getUserAccountSession().isIssuerInstitucion() &&
						Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getIssuerCode())) {
					//issuerSelected = userInfo.getUserAccountSession().getIssuerCode();
					getIssuerOnSelection();
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
			return "registerCertificateDepositeExcel";
		} catch (Exception e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return null;
	}
	
	
	/** metodo para realizar lectura y validacion de los datos del archivo excel para el registro masivo de ACV
	 * (Desmaterializacion)
	 * 
	 * @param event */
	public void readFileExcel(FileUploadEvent event) {
		
		UploadedFile archivoExcel = event.getFile();
		fileExcelPersist = new PhysicalCertificateFile();
		fileExcelPersist.setMassiveRegister(true);

		if (participantSelected == null) {
			String bodyMessage = PropertiesUtilities.getMessage("msg.error.participant.required");
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR);
			alert(headerMessage, bodyMessage);
			return;
		}

		try {
			archivoExcel.getInputstream();
			
			fileExcelPersist.setFileName("Archivo Excel para Registro Masivo Titulos Fisicos");
			fileExcelPersist.setFileType(getFileExtension(archivoExcel.getFileName()));
			fileExcelPersist.setFileContent(archivoExcel.getContents());
			fileExcelPersist.setNameFileExcel(archivoExcel.getFileName());
			
			resultExcel = readCertificateDepositeDataMasive(archivoExcel.getInputstream());
			
			//validar que no tenga errores para seguir con las otras validaciones
			if(resultExcel.getLstErrorReadExcel().isEmpty()){
				validateCertificateDepositeDataMasive();
			}
			
			//si no tiene errores
			if(resultExcel.getLstErrorReadExcel().isEmpty()){
				// si todo fue exito se notifica al usuario, (tambien significa que todo esta listo para el registro masivo)
				String bodyMessage = "Se carg\u00F3 satisfactoriamente.";
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
				alert(headerMessage, bodyMessage);
			}else{
				fileExcelPersist = new PhysicalCertificateFile();
				fileExcelPersist.setMassiveRegister(true);
				String bodyMessage = PropertiesUtilities.getMessage("msg.error.read.file.excel");
				String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR);
				alert(headerMessage, bodyMessage);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			resultExcel = new DataExcelRegCertificateDepositeTO();
			fileExcelPersist = new PhysicalCertificateFile();
			fileExcelPersist.setMassiveRegister(true);
			String bodyMessage = PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage() != null ? e.getMessage().toString() : "?" });
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR);
			alert(headerMessage, bodyMessage);
		} catch (Exception e) {
			e.printStackTrace();
			resultExcel = new DataExcelRegCertificateDepositeTO();
			fileExcelPersist = new PhysicalCertificateFile();
			fileExcelPersist.setMassiveRegister(true);
			String bodyMessage = PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage() != null ? e.getMessage().toString() : "?" });
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR);
			alert(headerMessage, bodyMessage);
		}
	}
	
	/**
	 * metodo para extraer los datos del archivo excel
	 * @param entryFile
	 * @return
	 */
	public DataExcelRegCertificateDepositeTO readCertificateDepositeDataMasive(InputStream entryFile) {
		DataExcelRegCertificateDepositeTO totalResult = new DataExcelRegCertificateDepositeTO();
		DataExcelRegCertificateDepositeTO data;
		
		try {
			XSSFWorkbook book = new XSSFWorkbook(entryFile);
			XSSFSheet sheet = book.getSheetAt(0);
			XSSFRow row; XSSFCell cell;
			int rowFirst = 1, lastRow = sheet.getLastRowNum();
			
			int col0_Cui = 0;
			int col1_HolderAccountNumber = 1;
			int col2_IdSecurityCode = 2;
			int col3_DateMarketfact = 3;
			int col4_RateMarketfact = 4;
			int col5_PriceMarketfact = 5;
			int col6_NumCertificate = 6;
			int col7_Quantity = 7;
			int col8_DateIssuance = 8;

			BigDecimal oneHundred = new BigDecimal("100");
			Security securityAux;

			for (int filaExtrac = rowFirst; filaExtrac <= lastRow; filaExtrac++) {
				data = new DataExcelRegCertificateDepositeTO();
				
				row = sheet.getRow(filaExtrac);
				if (Validations.validateIsNotNull(row)) {
					
					/*-------------------------CUI-------------------------*/
					cell = row.getCell(col0_Cui);
					if (Validations.validateIsNotNull(cell)){
						data.setIdHolderPkCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) data.setHolder(String.valueOf(cell.getNumericCellValue()));
						else{ 
							if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) data.setHolder(cell.getStringCellValue());
							else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] {cell.getReference()}));
						}
					} else {
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cui.required"));
						entryFile.close();
						return totalResult;
					}
					
					/*-------------------------CUENTA TITULAR-------------------------*/
					cell = row.getCell(col1_HolderAccountNumber);
					if (Validations.validateIsNotNull(cell)){
						data.setAccountNumberCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC){
							data.setAccountNumber(new BigDecimal(String.valueOf(cell.getNumericCellValue())).intValue());
						}else{
							if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) data.setAccountNumber(new BigDecimal(cell.getStringCellValue()).intValue());
							else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
						}
					} else {
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.account.number.required"));
					}

					/*-------------------------CLASE-CLAVE DE VALOR-------------------------*/
					cell = row.getCell(col2_IdSecurityCode);
					if (Validations.validateIsNotNull(cell)){
						data.setIdSecurityCodePkCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
							data.setIdSecurityCodePk(cell.getStringCellValue().trim());
						else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.text.value", new Object[] { cell.getReference() }));
					} else {
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.security.required"));
					}
					
					/*-------------------------FECHA HM-------------------------*/
					cell = row.getCell(col3_DateMarketfact);
					if (Validations.validateIsNotNull(cell)){
						data.setMarketfactDateCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA) data.setMarketfactDate(cell.getDateCellValue());
						else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.date.value", new Object[] { cell.getReference() }));
					}else {
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.date.marketfact.required"));
					}
					
					/*-------------------------TASA HM-------------------------*/
					cell = row.getCell(col4_RateMarketfact);
					if (Validations.validateIsNotNull(cell)){
						data.setMarkefactRateCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA || cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
							if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC)
								data.setMarkefactRate(BigDecimal.valueOf(cell.getNumericCellValue()));
							else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK)
								data.setMarkefactRate(BigDecimal.ZERO);
							else if (cell.getCachedFormulaResultType() == XSSFCell.CELL_TYPE_NUMERIC)
								data.setMarkefactRate(BigDecimal.valueOf(cell.getNumericCellValue()));
						} else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
					}else data.setMarkefactRate(BigDecimal.ZERO);
					
					// validando que la tasa de mercado esta dentro del rango [0 - 100]
					if(data.getMarkefactRate().compareTo(BigDecimal.ZERO) < 0 || data.getMarkefactRate().compareTo(oneHundred) > 0){
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.rate.marketfact.out.of.range", new Object[] { cell.getReference() }));
					}
					
					/*-------------------------PRECIO HM-------------------------*/
					cell = row.getCell(col5_PriceMarketfact);
					if (Validations.validateIsNotNull(cell)){
						data.setMarkefactPriceCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) data.setMarkefactPrice(new BigDecimal(String.valueOf(cell.getNumericCellValue())));
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)data.setMarkefactPrice(new BigDecimal(cell.getStringCellValue()));
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) data.setMarkefactPrice(BigDecimal.ZERO);
						else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
					}else data.setMarkefactPrice(BigDecimal.ZERO);
					
					/*-------------------------NUMERO DE CERTIFICADO-------------------------*/
					cell = row.getCell(col6_NumCertificate);
					if (Validations.validateIsNotNull(cell)){
						data.setNumberCertificateCellReference(cell.getReference());
					
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
							BigDecimal bd = new BigDecimal(cell.getNumericCellValue());
							data.setNumberCertificate(bd.round(new MathContext(15)).toPlainString());
							if(data.getNumberCertificate().length()>15){
								totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.max.certificate", new Object[] {cell.getReference()}));
							}
						}else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] {cell.getReference()}));
					
					}else{
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference()}));
					}
					
					
					/*-------------------------CANTIDAD-------------------------*/
					cell = row.getCell(col7_Quantity);
					if (Validations.validateIsNotNull(cell)){
						data.setQuantityCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) data.setQuantity(new BigDecimal(String.valueOf(cell.getNumericCellValue())));
						else if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) data.setQuantity(new BigDecimal(cell.getStringCellValue()));
							 else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { cell.getReference() }));
					} else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.quantity.required"));
					// validacion de que la cantidad debe ser mayor a cero
					if (data.getQuantity().compareTo(BigDecimal.ZERO) <= 0) {
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.quantity.required.greater.zero", new Object[] { cell.getReference() }));
					}
					
					/*-------------------------FECHA EMISION-------------------------*/
					cell = row.getCell(col8_DateIssuance);
					if (Validations.validateIsNotNull(cell)){
						data.setIssuanceDateCellReference(cell.getReference());
						if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC || cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA) data.setIssuanceDate(cell.getDateCellValue());
						else totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.date.value", new Object[] { cell.getReference() }));
					}else {
						totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.date.marketfact.required"));
					}
					totalResult.getLstDataExcelResult().add(data);
				}
			}
			
			entryFile.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("::: se ha producido un error : " + e.getMessage());
			totalResult.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.exception", new Object[] { e.getMessage().toString() }));
		}

		return totalResult;
	}
	
	/**
	 * Metodo que valida que los datos sean coherentes
	 * @param dataExcelRegCertificateDepositeTO
	 * @return
	 * @throws ServiceException 
	 */
	public void validateCertificateDepositeDataMasive() throws ServiceException {
		
		List<PhysicalCertificate> lstPhysicalCert;
		PhysicalCertificate physicalCertificateAux;
		Security  securityBalance;
		Issuer issuerAux;
		String[] idHolders;
		
		for(DataExcelRegCertificateDepositeTO dataExcel: resultExcel.getLstDataExcelResult()){
			
			issuerAux = null;
			idHolders = null;
			lstPhysicalCert = null;
			securityBalance = null;
			
			/*-------------------------CUI Y CUENTA TITULAR-------------------------*/
			//verificando existencia de cuenta
			dataExcel.setHolderAccount(dematerializationCertificateFacade.getHolderAccountFromParticipantAndHoldAccountNumber(participantSelected,dataExcel.getAccountNumber()));
			
			if (Validations.validateIsNotNull(dataExcel.getHolderAccount())) {
				// validando que la cuenta titular tenga estado REGISTRADO
				if (!dataExcel.getHolderAccount().getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.account.number.state.invalid", new Object[] { holderAccount.getAccountNumber().toString(),dataExcel.getAccountNumberCellReference() }));
			} else {
				resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.account.number.invalid", new Object[] { dataExcel.getAccountNumber().toString(), dataExcel.getAccountNumberCellReference() }));
			}
			
			// validando si el CUI tiene relacion con la cuenta titular encontrada - extraer el CUI o CUI's (en caso de ser mancomunados)
			idHolders = dataExcel.getHolder().split(",");
			for (String idHolder : idHolders){
				
				dataExcel.setIdHolderPk((long) ((int) Float.parseFloat(idHolder.trim())));
				//validando existencia de cui
				if(Validations.validateIsNotNull(certificateDepositeBeanFacade.findHolder(dataExcel.getIdHolderPk()))){
					try {
						if (!dematerializationCertificateFacade.verifyRelationshipHolderAndHolderAccount(dataExcel.getHolderAccount().getIdHolderAccountPk(), dataExcel.getIdHolderPk()))
							resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.account.number.norelationship.cui", new Object[] { dataExcel.getIdHolderPk(),dataExcel.getAccountNumber().toString(), dataExcel.getIdHolderPkCellReference() }));
					} catch (Exception e) {
//						e.printStackTrace();
						resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cui.invalid.from.excel", new Object[] { dataExcel.getIdHolderPkCellReference() }));
					}
				}else{
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cui.invalid.from.excel", new Object[] {dataExcel.getIdHolderPkCellReference()}));
				}
			}
			
			
			/*-------------------------QUANTITY-------------------------*/
			dataExcel.setSecurity(em.find(Security.class, dataExcel.getIdSecurityCodePk().trim()));
			//VERIFICANDO SI ES RENTA FIJA O VARIABLE
			
			if(Validations.validateIsNotNull(dataExcel.getQuantity()) && (dataExcel.getQuantity().compareTo(BigDecimal.ZERO) != 0)){
				// validando que se haya ingresado la cant de valores a depositar correctamente
				if(Validations.validateIsNotNull(dataExcel.getSecurity())){
	        		//validando si la cantida a depositar es valida
	        		securityBalance = transferAvailableFacade.getSecurity(dataExcel.getSecurity().getIdSecurityCodePk());
	        		if(dataExcel.getQuantity().compareTo(securityBalance.getPhysicalBalance().subtract(securityBalance.getPhysicalDepositBalance()))>0){
	        			resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_AMOUNT_DEPOSIT_EXCEED)+ " la celda " + dataExcel.getQuantityCellReference());
	            	}
		    	}else{
		    		resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.validation.required.securities", new Object[] { dataExcel.getIdSecurityCodePk(), dataExcel.getIdSecurityCodePkCellReference() }));
		    	}
			}else{
				resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage("msg.error.cell.required.numeric.value", new Object[] { dataExcel.getQuantityCellReference() }));
				resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_COUNT_SECURITY_INVALID) + " en la celda " + dataExcel.getQuantityCellReference());
			}
				
			
			/*-------------------------SECURITY-------------------------*/
			if (Validations.validateIsNotNull(dataExcel.getSecurity())){
				dataExcel.setIdSecurityCodePk(dataExcel.getSecurity().getIdSecurityCodePk());
				//validando el estado
				if(!dataExcel.getSecurity().getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID) + " en la celda " + dataExcel.getIdSecurityCodePkCellReference());
				}
				
				// se valida si el valor ya fue depositado en la CAVAPY
				if(dataExcel.getSecurity().getPhysicalBalance().intValue() <= GeneralConstants.ZERO_VALUE_INT){
					issuerAux = certificateDepositeBeanFacade.getIssuerbySecurity(dataExcel.getSecurity().getIdSecurityCodePk());
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_ALL_DEPOSITED, new Object[]{dataExcel.getSecurity().getIdSecurityCodePk(),issuerAux.getBusinessName()})+ " en la celda " + dataExcel.getIdSecurityCodePkCellReference());
				}
					
				// validando si el tipo de emision es correcto para esta operacion
				if(dataExcel.getSecurity().getIssuanceForm().equals(IssuanceType.DEMATERIALIZED.getCode())){
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ISSUER_TYPE_IN_SECURITY_INCORRECT, new Object[]{dataExcel.getSecurity().getIdSecurityCodePk()})+ " en la celda " + dataExcel.getIdSecurityCodePkCellReference());
				}
					
				// verificando si es DPF o no, aca se actualizan flag de: flagDPF
				if(dataExcel.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					lstPhysicalCert = certificateDepositeBeanFacade.getPhysicalCertificateBySecurity(dataExcel.getSecurity().getIdSecurityCodePk());
					if(Validations.validateIsNotNull(lstPhysicalCert) && lstPhysicalCert.size()>0){
						resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.PHYSICAL_CERTIFICATE_ERROR_EXIST_SECURITY)+ " en la celda " + dataExcel.getIdSecurityCodePkCellReference());
					}
					// verificando que sea un solo valor
					if(Validations.validateIsNotNull(dataExcel.getQuantity()) && dataExcel.getQuantity().compareTo(new BigDecimal(1)) != 0){
						resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.INVALID_AMOUNT) + " en la celda " + dataExcel.getQuantityCellReference());
					}
				}
//				else{
//					flagDPF = false;
//					CertificateQuantity=null;
//				}
			}				
			
			/*-------------------------CERTIFICATE NUMBER-------------------------*/
//			physicalCertificate.setIssuer(findIssuerSelected(security));
			if(Validations.validateIsNotNull(dataExcel.getNumberCertificate()) && Validations.validateIsNotNull(dataExcel.getSecurity())){
				physicalCertificateAux = new PhysicalCertificate();
				physicalCertificateAux.setSecurities(dataExcel.getSecurity());
				physicalCertificateAux.setCertificateNumber(dataExcel.getNumberCertificate());
				
				if(dataExcel.getSecurity().isVariableInstrumentType()) dataExcel.setSecurityCertificate(dataExcel.getIdSecurityCodePk()+"_"+dataExcel.getNumberCertificate());
				else dataExcel.setSecurityCertificate(dataExcel.getIdSecurityCodePk());
				
				if(resultExcel.getLstSecurityCertificate().contains(dataExcel.getSecurityCertificate())){
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG,
							new Object[]{physicalCertificateAux.getCertificateNumber().toString(),dataExcel.getIdSecurityCodePk()}) + " en la celda " + dataExcel.getNumberCertificateCellReference());
				}else{
					resultExcel.getLstSecurityCertificate().add(dataExcel.getSecurityCertificate());
				}
				
				if( existInSolicitud(physicalCertificateAux)){
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_ERR_MSG,
							new Object[]{physicalCertificateAux.getCertificateNumber().toString(),dataExcel.getIdSecurityCodePk()}) + " en la celda " + dataExcel.getNumberCertificateCellReference());
				}
			
				if( existCertificateInBd(physicalCertificateAux) ){
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_REG_EXISTS,
		    				new Object[]{physicalCertificateAux.getCertificateNumber().toString(),physicalCertificateAux.getSecurities().getIdSecurityCodePk()})+ " en la celda " + dataExcel.getNumberCertificateCellReference());
				}
				
				if (isDematerializedCertificate(physicalCertificateAux)) {
					resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.CERTIFICATE_DEPOSIT_DEMATERIALIZED,
		    				new Object[]{physicalCertificateAux.getCertificateNumber().toString(),physicalCertificateAux.getSecurities().getIdSecurityCodePk()})+ " en la celda " + dataExcel.getNumberCertificateCellReference());
				}
			}
			
			
			/*-------------------------HECHOS DE MERCADO-------------------------*/
	    	
	    	// validando que se hayan ingresado los hechos de mercado
	    	if(Validations.validateIsNotNullAndNotEmpty(dataExcel.getMarketfactDate()) || Validations.validateIsNotNullAndNotEmpty(dataExcel.getMarkefactRate()) || Validations.validateIsNotNullAndNotEmpty(dataExcel.getMarkefactPrice())){
	    		
	    	} else {
	    		resultExcel.getLstErrorReadExcel().add(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_DEPOSIT_NOT_INSERT_MARKETFACT)+ " en la celda " + dataExcel.getMarketfactDateCellReference());
	    	}
		}
	}
	
	/**
	 * @param event
	 * @throws IOException
	 */
	public void readZipFileMassiveRegister(FileUploadEvent event) throws IOException {
		
		resultExcel.setMapPhysicalCertFiles(new HashMap<String,PhysicalCertFile>());
		resultExcel.setLstErrorReadAttachment(new ArrayList<String>());
		PhysicalCertFile physicalCertFile;
		String headerMessage="";
    	String bodyMessage="";
		ParameterTable parameter = em.find(ParameterTable.class, ParameterFileType.TITULO_FISICO.getCode());
		List<String> extensions = new ArrayList<String>();
		extensions.add("jpg");
		extensions.add("jpeg");
		extensions.add("pdf");
		extensions.add("doc");
		extensions.add("docx");
		extensions.add("png");
		String[] aux;
		
		byte[] content = event.getFile().getContents();
		File file;
		file = File.createTempFile("tempDesmafile", ".zip");
		FileUtils.writeByteArrayToFile(file, content);
		
		fileZip = new PhysicalCertificateFile();
		fileZip.setNameFileAttached(event.getFile().getFileName());
		
		UnzipUtility unzipUtility = new UnzipUtility();
		
		String tmpdir = System.getProperty("java.io.tmpdir");
		unzipUtility.unzip(file.getAbsolutePath(), tmpdir);
		
		File fileAux;
		
		
		// validar de cada archivo:
		//- extension
		//-si se encuentra en la lista de phisical certificate
		//-si no se encutnra en la lista
		//leyendo los archivos
		try (ZipFile zipFile = new ZipFile(file)) {
		    Enumeration zipEntries = zipFile.entries();
		    while (zipEntries.hasMoreElements()) {
		    	ZipEntry zipEntry = ((ZipEntry) zipEntries.nextElement());
		    	
		    	//validando la extension
		    	if(!extensions.contains(getFileExtension(zipEntry.getName()))){
		    		resultExcel.getLstErrorReadAttachment().add(PropertiesUtilities.getMessage("msg.massive.validation.attachment.extension", new Object[] {zipEntry.getName()}));
		    	}
		    	//validando que se encuentre en la lista de registros
		    	if(!resultExcel.getLstSecurityCertificate().contains(getFileName(zipEntry.getName()))){
		    		resultExcel.getLstErrorReadAttachment().add(PropertiesUtilities.getMessage("msg.massive.validation.attachment.security", new Object[] {zipEntry.getName()}));
		    	}
		    	
		    	//solo si no hay observaciones en los adjuntos se coloca en la lista de adjuntos
		    	if(resultExcel.getLstErrorReadAttachment().isEmpty()){
		    		fileAux = new File(tmpdir+ File.separator +zipEntry.getName());
		    		
		    		byte[] bytes = Files.readAllBytes(Paths.get(fileAux.getAbsolutePath()));
		    		
		    		physicalCertFile = new PhysicalCertFile();
			        physicalCertFile.setDocumentType(parameter.getParameterTablePk());
			        physicalCertFile.setDescription(parameter.getParameterName());
			        physicalCertFile.setFilename(zipEntry.getName());
			        physicalCertFile.setDocumentFile(bytes);
			        resultExcel.getMapPhysicalCertFiles().put(getFileName(zipEntry.getName()), physicalCertFile);
		    	}
		    }
		    
		    //validando que todos los registros tengan adjunto
		    if(resultExcel.getLstErrorReadAttachment().isEmpty()){
		    	for(String securityCertificate: resultExcel.getLstSecurityCertificate()){
		    		if(!resultExcel.getMapPhysicalCertFiles().containsKey(securityCertificate)){
		    			aux = securityCertificate.split("_");
			    		resultExcel.getLstErrorReadAttachment().add(PropertiesUtilities.getMessage("msg.massive.validation.attachment.attachment", new Object[] {aux[0]}));
		    		}
		    	}
		    }
		}
		
		if(!resultExcel.getLstErrorReadAttachment().isEmpty()){
    		headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_PHYSICAL_CERTIFICATE_VALIDATION_ATTACHMENT_ERROR);
    		alert(headerMessage, bodyMessage);
    	}
		
		if(resultExcel.getLstErrorReadExcel().isEmpty() && resultExcel.getLstErrorReadAttachment().isEmpty()){
    		getPhysicalCertificatesMassive();
    	}
		
	}  
	
	
	
	 /**
     * Show save confirmation.
     */
    @LoggerAuditWeb
    public void showSaveMassiveConfirmation() {
    	removeAlertPopUp();
    	String headerMessage="";
    	String bodyMessage="";
    	
    	if(resultExcel.getLstErrorReadExcel().isEmpty()){
    		
			if(!Validations.validateIsNotNullAndPositive(participantSelected)){
				headerMessage=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_PARETICIPANT_REQUIRE);
	    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERR_MSG_PARTICIPANT);
	    		alert(headerMessage, bodyMessage);
			}
		}
    	
    	if(!resultExcel.getLstErrorReadAttachment().isEmpty()){
    		headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
    		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_PHYSICAL_CERTIFICATE_VALIDATION_ATTACHMENT_ERROR);
    		alert(headerMessage, bodyMessage);
    	}
    	
    	if(resultExcel.getLstPhysicalCertificates().isEmpty()){
    		getPhysicalCertificatesMassive();
    	}
    	
    	confirmAlertAction="saveMassivePhysicalCertificates";
		headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_SAVE);
		question(headerMessage, bodyMessage);
	}
    
	public void getPhysicalCertificatesMassive(){
    	
    	Integer couponCount = 0;
    	PhysicalCertificate physicalCertificateAux;
    	List<PhysicalCertFile> physicalCertFiles;
		//HM
		List<PhysicalCertMarketfact> physicalCertMarketfacts;
		PhysicalCertMarketfact physicalCertMarketfact;
    	
    	//obteniendo la lista de titulos fisicos
    	for(DataExcelRegCertificateDepositeTO data: resultExcel.getLstDataExcelResult()){
        	
    		if(Validations.validateIsNotNull(data.getIdSecurityCodePk())){
    			try {
    				couponCount = transferAvailableFacade.getCouponCountWithSecurity(data.getIdSecurityCodePk());
	    		} catch (ServiceException e) {
	    			e.printStackTrace();
	    		}
    				
    			physicalCertificateAux = new PhysicalCertificate();
    				
    			physicalCertificateAux.setIndExistsSecurity(BooleanType.YES.getCode());
    			physicalCertificateAux.setCurrencyType(moneyTypeMap.get(data.getSecurity().getCurrency()));
    			physicalCertificateAux.setIssuer(findIssuerSelected(data.getSecurity()));
    			physicalCertificateAux.setSecurityClass(data.getSecurity().getSecurityClass());
    			physicalCertificateAux.setCurrency(data.getSecurity().getCurrency());
    			physicalCertificateAux.setInstrumentType(data.getSecurity().getInstrumentType());
    			physicalCertificateAux.setClassType(securityClassTypeMapDescription(data.getSecurity().getSecurityClass()));
    			physicalCertificateAux.setIssueDate(data.getSecurity().getIssuanceDate());
    			physicalCertificateAux.setNominalValue(data.getSecurity().getCurrentNominalValue());
    			physicalCertificateAux.setSecurities(data.getSecurity());
    			physicalCertificateAux.setCertificateQuantity(data.getQuantity());
    			physicalCertificateAux.setCertificateNumber(data.getNumberCertificate());
    			physicalCertificateAux.setCouponCount(couponCount);
    			physicalCertificateAux.setCertificateDate(data.getIssuanceDate());
    			physicalCertificateAux.setHolderAccount(data.getHolderAccount());
    			physicalCertificateAux.setIndExistsSecurity(1);
    			physicalCertFiles = new ArrayList<PhysicalCertFile>();
    			if(resultExcel.getMapPhysicalCertFiles().size()>0 && fileZip.getFileName()!= null){
	    			physicalCertFiles.add(resultExcel.getMapPhysicalCertFiles().get(data.getSecurityCertificate()));
	    		}
    			//physicalCertificateAux.setPhysicalCertFileList(physicalCertFiles);
				
				//RENTA FIJA
				if(data.getSecurity().getInterestRate() != null){
					physicalCertificateAux.setInterestRate(data.getSecurity().getInterestRate());
				}
				if(data.getSecurity().getPeriodicity() != null){
					physicalCertificateAux.setPaymentPeriodicity(data.getSecurity().getPeriodicity());
				}
				if( Validations.validateIsNotNullAndPositive(data.getSecurity().getPeriodicity()) ){
					physicalCertificateAux.setPaymentType(paymentTypeMapDescription(physicalCertificateAux.getPaymentPeriodicity()));
				}
				if(data.getSecurity().getExpirationDate() != null){
					physicalCertificateAux.setExpirationDate(data.getSecurity().getExpirationDate());
				}
			}else{
				// cuando el valor no existe
				physicalCertificateAux = new PhysicalCertificate();
				physicalCertificateAux.setIndExistsSecurity(BooleanType.NO.getCode());
				physicalCertificateAux.setSecurityClass(data.getSecurity().getSecurityClass());
				physicalCertificateAux.setClassType(securityClassTypeMapDescription(data.getSecurity().getSecurityClass()));
				physicalCertificateAux.setSecurities(data.getSecurity());
				physicalCertificateAux.setIndExistsSecurity(0);
				physicalCertificateAux.setCertificateQuantity(data.getQuantity());
				physicalCertificateAux.setCertificateNumber(data.getNumberCertificate());
				physicalCertificateAux.setCouponCount(couponCount);
				physicalCertificateAux.setCertificateDate(data.getIssuanceDate());
				physicalCertificateAux.setHolderAccount(data.getHolderAccount());
				physicalCertFiles = new ArrayList<PhysicalCertFile>();
    			if(resultExcel.getMapPhysicalCertFiles().size()>0 && fileZip.getFileName()!= null){
	    			physicalCertFiles.add(resultExcel.getMapPhysicalCertFiles().get(data.getSecurityCertificate()));
	    		}
    			//physicalCertificateAux.setPhysicalCertFileList(physicalCertFiles);
			}
    		
    		physicalCertMarketfacts = new ArrayList<>();
    		physicalCertMarketfact = new PhysicalCertMarketfact();
    		physicalCertMarketfact.setMarketDate(data.getMarketfactDate());
    		physicalCertMarketfact.setMarketPrice(data.getMarkefactPrice());
    		physicalCertMarketfact.setMarketRate(data.getMarkefactRate());
    		physicalCertMarketfact.setPhysicalCertificate(physicalCertificateAux);
    		physicalCertMarketfact.setQuantity(data.getQuantity());
    		physicalCertMarketfact.setRegistryDate(CommonsUtilities.currentDate());
    		
    		physicalCertMarketfacts.add(physicalCertMarketfact);
    		
    		physicalCertificateAux.setPhysicalCertMarketfacts(physicalCertMarketfacts);
			
    		resultExcel.getLstPhysicalCertificates().add(physicalCertificateAux);

    	}
    }
    
    /**
     * Save all physical certificates.
     */
    @LoggerAuditWeb
	public void saveMassivePhysicalCertificates() {
    	
    	CustodyOperation custodyOperation;
    	List<PhysicalOperationDetail> physicalOperationDetails;
    	List<PhysicalCertificate> certificates;
		PhysicalOperationDetail physicalOperationDetail;
		PhysicalCertificateOperation physicalCertificateOperation;
		Participant participant;

		try{
			//guardando el excel
			//si se realizo la carga de archivo adjunto primero se lo registra
			if(Validations.validateIsNotNull(fileExcelPersist.getNameFileExcel())){
				fileExcelPersist.setMassiveRegister(false);
				fileExcelPersist = certificateDepositeBeanFacade.registryPhysicalCertificateFile(fileExcelPersist);
			}
			
			//obteniendo la lista de titulos fisicos
			for(PhysicalCertificate physicalCertificate: resultExcel.getLstPhysicalCertificates()){
		        	
	    		//CUSTODIA
				custodyOperation = new CustodyOperation();
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
				custodyOperation.setOperationType(new Long(RequestType.DEPOSITEO.getCode()));
				custodyOperation.setState(StateType.REGISTERED.getCode());
				custodyOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				
				//PHYSICAL_CERTIFICATE_OPERATION
				participant = new Participant();
				participant.setIdParticipantPk(participantSelected);
				
				physicalCertificateOperation = new PhysicalCertificateOperation();
				physicalCertificateOperation.setHolderAccount(physicalCertificate.getHolderAccount());
				physicalCertificateOperation.setParticipant(participant);
				physicalCertificateOperation.setState(StateType.REGISTERED_CERTIFICATE.getCode()); 
				physicalCertificateOperation.setPhysicalOperationType(new Long(RequestType.DEPOSITEO.getCode()));
				physicalCertificateOperation.setCustodyOperation(custodyOperation);
				physicalCertificateOperation.setIndApplied(new Integer(0));
				
				physicalOperationDetails = new ArrayList<PhysicalOperationDetail>();
				
				physicalCertificate.setState(StateType.REGISTERED.getCode());
				physicalCertificate.setRegisterDate(CommonsUtilities.currentDateTime());
				physicalCertificate.setSituation(SituationType.PARTICIPANT.getCode());
				physicalCertificate.setRegisterUser(userInfo.getUserAccountSession().getUserName());
				certificates = new ArrayList<>();
				certificates.add(physicalCertificate);
				
				physicalOperationDetail = new PhysicalOperationDetail();
				physicalOperationDetail.setPhysicalCertificateOperation(physicalCertificateOperation);
				physicalOperationDetail.setPhysicalCertificate(physicalCertificate);
				physicalOperationDetail.setIndApplied(0);
				physicalOperationDetail.setState(StateType.REGISTERED.getCode());
				physicalOperationDetail.setRegistryDate(CommonsUtilities.currentDateTime());
				physicalOperationDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				
				physicalOperationDetails.add(physicalOperationDetail);
				
				physicalCertificateOperation.setPhysicalOperationDetails(physicalOperationDetails);
				
				//GRABO EN CASCADA LOS DATOS
				physicalCertificateOperation = certificateDepositeBeanFacade.savePhysicalCertificateOperation(physicalCertificateOperation,certificates);
				
	    	}
	    	String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = "Se registr\u00F3 satisfactoriamente la solicitud.";
			alert(headerMessage, bodyMessage);
			
			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REGISTER.getCode());
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   null, null);
			}else if(isBcr){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   null, null);
			}else if(isParticipant){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						   businessProcess, 
						   participantSelected, null);
			}
			/*** end JH NOTIFICACIONES ***/
			
			
		}catch (Exception e) {
			if(e instanceof ServiceException){
				ServiceException serviceException = (ServiceException) e;
				if(  serviceException.getErrorService().equals(ErrorServiceType.CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY) 	){
					String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
					String bodyMessage = PropertiesUtilities.getMessage(ErrorServiceType.CERTIFICATE_PHYSICAL_NOT_SAVE_CONCURRENCY.getMessage(), serviceException.getParams());
					alert(headerMessage, bodyMessage);

					return;
				}
			}
			
			excepcion.fire(new ExceptionToCatchEvent(e));  
		}
		
	}
	
	/** Metodo para extraer la extension de un archivo
	 * 
	 * @param fileName
	 * @return */
	private String getFileExtension(String fileName) {
		String[] partition = fileName.split("\\.");
		// verificando que se haya realizado la separacion del nombre del archivo-extension
		if (partition.length > 1) {
			return partition[partition.length - 1].toLowerCase();
		} else {
			// si no tiene extension se retorna una cadena vacia
			return "";
		}
	}
	
	/** Metodo para extraer el nombre de un archivo
	 * 
	 * @param fileName
	 * @return */
	private String getFileName(String fileName) {
		String[] partition = fileName.split("\\.");
		return partition[0];
	}
    
	/*GETTER's AND SETTER's */

	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the status deposit list.
	 *
	 * @return the status deposit list
	 */
	public List<ParameterTable> getStatusDepositList() {
		return statusDepositList;
	}

	/**
	 * Sets the status deposit list.
	 *
	 * @param statusDepositList the new status deposit list
	 */
	public void setStatusDepositList(List<ParameterTable> statusDepositList) {
		this.statusDepositList = statusDepositList;
	}

	/**
	 * Gets the physical certificate.
	 *
	 * @return the physical certificate
	 */
	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	/**
	 * Sets the physical certificate.
	 *
	 * @param physicalCertificate the new physical certificate
	 */
	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the status selected.
	 *
	 * @return the status selected
	 */
	public Integer getStatusSelected() {
		return statusSelected;
	}

	/**
	 * Sets the status selected.
	 *
	 * @param statusSelected the new status selected
	 */
	public void setStatusSelected(Integer statusSelected) {
		this.statusSelected = statusSelected;
	}

	/**
	 * Gets the certificate selected.
	 *
	 * @return the certificate selected
	 */
	public Long getCertificateSelected() {
		return certificateSelected;
	}

	/**
	 * Sets the certificate selected.
	 *
	 * @param certificateSelected the new certificate selected
	 */
	public void setCertificateSelected(Long certificateSelected) {
		this.certificateSelected = certificateSelected;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the request selected.
	 *
	 * @return the request selected
	 */
	public Long getRequestSelected() {
		return requestSelected;
	}

	/**
	 * Sets the request selected.
	 *
	 * @param requestSelected the new request selected
	 */
	public void setRequestSelected(Long requestSelected) {
		this.requestSelected = requestSelected;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the search certificate data model.
	 *
	 * @return the search certificate data model
	 */
	public PhysicalCertificateExtDataModel getSearchCertificateDataModel() {
		return searchCertificateDataModel;
	}

	/**
	 * Sets the search certificate data model.
	 *
	 * @param searchCertificateDataModel the new search certificate data model
	 */
	public void setSearchCertificateDataModel(
			PhysicalCertificateExtDataModel searchCertificateDataModel) {
		this.searchCertificateDataModel = searchCertificateDataModel;
	}

	/**
	 * Gets the physical certificate search list.
	 *
	 * @return the physical certificate search list
	 */
	public List<PhysicalCertificateExt> getPhysicalCertificateSearchList() {
		return physicalCertificateSearchList;
	}

	/**
	 * Sets the physical certificate search list.
	 *
	 * @param physicalCertificateSearchList the new physical certificate search list
	 */
	public void setPhysicalCertificateSearchList(
			List<PhysicalCertificateExt> physicalCertificateSearchList) {
		this.physicalCertificateSearchList = physicalCertificateSearchList;
	}

	/**
	 * Gets the selected physical certificates ext.
	 *
	 * @return the selected physical certificates ext
	 */
	public PhysicalCertificateExt[] getSelectedPhysicalCertificatesExt() {
		return selectedPhysicalCertificatesExt;
	}

	/**
	 * Sets the selected physical certificates ext.
	 *
	 * @param selectedPhysicalCertificatesExt the new selected physical certificates ext
	 */
	public void setSelectedPhysicalCertificatesExt(
			PhysicalCertificateExt[] selectedPhysicalCertificatesExt) {
		this.selectedPhysicalCertificatesExt = selectedPhysicalCertificatesExt;
	}

	/**
	 * Gets the participant login.
	 *
	 * @return the participant login
	 */
	public Long getParticipantLogin() {
		return participantLogin;
	}

	/**
	 * Sets the participant login.
	 *
	 * @param participantLogin the new participant login
	 */
	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	/**
	 * Gets the checks if is participant.
	 *
	 * @return the checks if is participant
	 */
	public boolean getIsParticipant() {
		return isParticipant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param isParticipant the new participant
	 */
	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Checks if is cevaldom.
	 *
	 * @return true, if is cevaldom
	 */
	public boolean isDepositary() {
		return isDepositary;
	}

	/**
	 * Sets the cevaldom.
	 *
	 * @param isDepositary the new depositary
	 */
	public void setDepositary(boolean isDepositary) {
		this.isDepositary = isDepositary;
	}

	/**
	 * Gets the checks if is bcr.
	 *
	 * @return the checks if is bcr
	 */
	public boolean getIsBcr() {
		return isBcr;
	}

	/**
	 * Sets the bcr.
	 *
	 * @param isBcr the new bcr
	 */
	public void setBcr(boolean isBcr) {
		this.isBcr = isBcr;
	}

	public String getfUploadFileTypesToPhysicalCert() {
		return fUploadFileTypesToPhysicalCert;
	}

	public void setfUploadFileTypesToPhysicalCert(String fUploadFileTypesToPhysicalCert) {
		this.fUploadFileTypesToPhysicalCert = fUploadFileTypesToPhysicalCert;
	}

	/**
	 * Gets the flag register.
	 *
	 * @return the flag register
	 */
	public boolean getFlagRegister() {
		return flagRegister;
	}

	/**
	 * Sets the flag register.
	 *
	 * @param flagRegister the new flag register
	 */
	public void setFlagRegister(boolean flagRegister) {
		this.flagRegister = flagRegister;
	}

	/**
	 * Gets the flag search.
	 *
	 * @return the flag search
	 */
	public boolean getFlagSearch() {
		return flagSearch;
	}

	/**
	 * Sets the flag search.
	 *
	 * @param flagSearch the new flag search
	 */
	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	/**
	 * Checks if is registers found.
	 *
	 * @return true, if is registers found
	 */
	public boolean isRegistersFound() {
		return registersFound;
	}

	/**
	 * Sets the registers found.
	 *
	 * @param registersFound the new registers found
	 */
	public void setRegistersFound(boolean registersFound) {
		this.registersFound = registersFound;
	}

	/**
	 * Checks if is reject confirm enabled.
	 *
	 * @return true, if is reject confirm enabled
	 */
	public boolean isRejectConfirmEnabled() {
		return rejectConfirmEnabled;
	}

	/**
	 * Sets the reject confirm enabled.
	 *
	 * @param rejectConfirmEnabled the new reject confirm enabled
	 */
	public void setRejectConfirmEnabled(boolean rejectConfirmEnabled) {
		this.rejectConfirmEnabled = rejectConfirmEnabled;
	}

	/**
	 * Gets the physical certificate temp.
	 *
	 * @return the physical certificate temp
	 */
	public PhysicalCertificateExt getPhysicalCertificateTemp() {
		return physicalCertificateTemp;
	}
	
	/**
	 * Sets the physical certificate temp.
	 *
	 * @param physicalCertificateTemp the new physical certificate temp
	 */
	public void setPhysicalCertificateTemp(
			PhysicalCertificateExt physicalCertificateTemp) {
		this.physicalCertificateTemp = physicalCertificateTemp;
	}

	/**
	 * Gets the holder account details.
	 *
	 * @return the holder account details
	 */
	public HolderAccount getHolderAccountDetails() {
		return holderAccountDetails;
	}

	/**
	 * Sets the holder account details.
	 *
	 * @param holderAccountDetails the new holder account details
	 */
	public void setHolderAccountDetails(HolderAccount holderAccountDetails) {
		this.holderAccountDetails = holderAccountDetails;
	}

	/**
	 * Gets the reason selected.
	 *
	 * @return the reason selected
	 */
	public Integer getReasonSelected() {
		return reasonSelected;
	}

	/**
	 * Sets the reason selected.
	 *
	 * @param reasonSelected the new reason selected
	 */
	public void setReasonSelected(Integer reasonSelected) {
		this.reasonSelected = reasonSelected;
	}

	/**
	 * Gets the reject other.
	 *
	 * @return the reject other
	 */
	public String getRejectOther() {
		return rejectOther;
	}

	/**
	 * Sets the reject other.
	 *
	 * @param rejectOther the new reject other
	 */
	public void setRejectOther(String rejectOther) {
		this.rejectOther = rejectOther;
	}

	/**
	 * Gets the other reject motive id.
	 *
	 * @return the other reject motive id
	 */
	public Integer getOtherRejectMotiveId() {
		return otherRejectMotiveId;
	}

	/**
	 * Sets the other reject motive id.
	 *
	 * @param otherRejectMotiveId the new other reject motive id
	 */
	public void setOtherRejectMotiveId(Integer otherRejectMotiveId) {
		this.otherRejectMotiveId = otherRejectMotiveId;
	}
	
	/**
	 * Gets the reason type list.
	 *
	 * @return the reason type list
	 */
	public List<ParameterTable> getReasonTypeList() {
		return ReasonStateList;
	}

	/**
	 * Gets the physical certificates.
	 *
	 * @return the physical certificates
	 */
	public PhysicalCertificate[] getPhysicalCertificates() {
		return physicalCertificates;
	}

	/**
	 * Sets the physical certificates.
	 *
	 * @param physicalCertificates the new physical certificates
	 */
	public void setPhysicalCertificates(PhysicalCertificate[] physicalCertificates) {
		this.physicalCertificates = physicalCertificates;
	}

	/**
	 * Gets the physical certificate list.
	 *
	 * @return the physical certificate list
	 */
	public List<PhysicalCertificate> getPhysicalCertificateList() {
		return physicalCertificateList;
	}

	/**
	 * Sets the physical certificate list.
	 *
	 * @param physicalCertificateList the new physical certificate list
	 */
	public void setPhysicalCertificateList(
			List<PhysicalCertificate> physicalCertificateList) {
		this.physicalCertificateList = physicalCertificateList;
	}

	public String getCertFileNameDisplay() {
		return certFileNameDisplay;
	}

	public void setCertFileNameDisplay(String certFileNameDisplay) {
		this.certFileNameDisplay = certFileNameDisplay;
	}

	public Integer getCertFileSelected() {
		return certFileSelected;
	}

	public List<PhysicalCertFile> getLstPhysicalCertFileView() {
		return lstPhysicalCertFileView;
	}

	public void setLstPhysicalCertFileView(List<PhysicalCertFile> lstPhysicalCertFileView) {
		this.lstPhysicalCertFileView = lstPhysicalCertFileView;
	}

	public void setCertFileSelected(Integer certFileSelected) {
		this.certFileSelected = certFileSelected;
	}

	/**
	 * Gets the quantity bs.
	 *
	 * @return the quantity bs
	 */
	public int getQuantityBS() {
		return quantityBS;
	}

	/**
	 * Sets the quantity bs.
	 *
	 * @param quantityBS the new quantity bs
	 */
	public void setQuantityBS(int quantityBS) {
		this.quantityBS = quantityBS;
	}


	/**
	 * Gets the quantity usd.
	 *
	 * @return the quantity usd
	 */
	public int getQuantityUSD() {
		return quantityUSD;
	}

	/**
	 * Sets the quantity usd.
	 *
	 * @param quantityUSD the new quantity usd
	 */
	public void setQuantityUSD(int quantityUSD) {
		this.quantityUSD = quantityUSD;
	}

	/**
	 * Gets the sum usd.
	 *
	 * @return the sum usd
	 */
	public BigDecimal getSumUSD() {
		return sumUSD;
	}

	public List<ParameterTable> getLstFileTypes() {
		return lstFileTypes;
	}

	public void setLstFileTypes(List<ParameterTable> lstFileTypes) {
		this.lstFileTypes = lstFileTypes;
	}

	/**
	 * Sets the sum usd.
	 *
	 * @param sumUSD the new sum usd
	 */
	public void setSumUSD(BigDecimal sumUSD) {
		this.sumUSD = sumUSD;
	}

	/**
	 * Gets the sum bs.
	 *
	 * @return the sum bs
	 */
	public BigDecimal getSumBS() {
		return sumBS;
	}

	public List<PhysicalCertFile> getLstPhysicalCertFile() {
		return lstPhysicalCertFile;
	}

	public void setLstPhysicalCertFile(List<PhysicalCertFile> lstPhysicalCertFile) {
		this.lstPhysicalCertFile = lstPhysicalCertFile;
	}

	/**
	 * Sets the sum bs.
	 *
	 * @param sumBS the new sum bs
	 */
	public void setSumBS(BigDecimal sumBS) {
		this.sumBS = sumBS;
	}

	/**
	 * Gets the certificate data model.
	 *
	 * @return the certificate data model
	 */
	public PhysicalCertificateDataModel getCertificateDataModel() {
		return certificateDataModel;
	}

	/**
	 * Sets the certificate data model.
	 *
	 * @param certificateDataModel the new certificate data model
	 */
	public void setCertificateDataModel(
			PhysicalCertificateDataModel certificateDataModel) {
		this.certificateDataModel = certificateDataModel;
	}

	/**
	 * Gets the select certificate count.
	 *
	 * @return the select certificate count
	 */
	public Integer getSelectCertificateCount() {
		return selectCertificateCount;
	}

	/**
	 * Sets the select certificate count.
	 *
	 * @param selectCertificateCount the new select certificate count
	 */
	public void setSelectCertificateCount(Integer selectCertificateCount) {
		this.selectCertificateCount = selectCertificateCount;
	}

	/**
	 * Gets the security class list.
	 *
	 * @return the security class list
	 */
	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}

	/**
	 * Sets the security class list.
	 *
	 * @param securityClassList the new security class list
	 */
	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}

	/**
	 * Gets the issuer list.
	 *
	 * @return the issuer list
	 */
	public List<Issuer> getIssuerList() {
		return issuerList;
	}

	public String getfUploadFileTypesDocumentsDisplayToPhysicalCert() {
		return fUploadFileTypesDocumentsDisplayToPhysicalCert;
	}

	public void setfUploadFileTypesDocumentsDisplayToPhysicalCert(String fUploadFileTypesDocumentsDisplayToPhysicalCert) {
		this.fUploadFileTypesDocumentsDisplayToPhysicalCert = fUploadFileTypesDocumentsDisplayToPhysicalCert;
	}

	/**
	 * Sets the issuer list.
	 *
	 * @param issuerList the new issuer list
	 */
	public void setIssuerList(List<Issuer> issuerList) {
		this.issuerList = issuerList;
	}

	/**
	 * Gets the instrument type selected.
	 *
	 * @return the instrument type selected
	 */
	public Integer getInstrumentTypeSelected() {
		return instrumentTypeSelected;
	}

	/**
	 * Sets the instrument type selected.
	 *
	 * @param instrumentTypeSelected the new instrument type selected
	 */
	public void setInstrumentTypeSelected(Integer instrumentTypeSelected) {
		this.instrumentTypeSelected = instrumentTypeSelected;
	}
	

	/**
	 * Gets the payment type list.
	 *
	 * @return the payment type list
	 */
	public List<ParameterTable> getPaymentTypeList() {
		return paymentTypeList;
	}

	/**
	 * Sets the payment type list.
	 *
	 * @param paymentTypeList the new payment type list
	 */
	public void setPaymentTypeList(List<ParameterTable> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}

	/**
	 * Gets the money type list.
	 *
	 * @return the money type list
	 */
	public List<ParameterTable> getMoneyTypeList() {
		return moneyTypeList;
	}

	/**
	 * Sets the money type list.
	 *
	 * @param moneyTypeList the new money type list
	 */
	public void setMoneyTypeList(List<ParameterTable> moneyTypeList) {
		this.moneyTypeList = moneyTypeList;
	}
	
	/**
	 * issue 895: habilitador para la subida de archivos
	 * @return
	 */
	public boolean isEnableSelectCertificateFile() {
		return Validations.validateIsNotNullAndPositive(certFileSelected);
	}

	/**
	 * Gets the from correlativo.
	 *
	 * @return the from correlativo
	 */
	public Integer getFromCorrelativo() {
		return fromCorrelativo;
	}

	/**
	 * Sets the from correlativo.
	 *
	 * @param fromCorrelativo the new from correlativo
	 */
	public void setFromCorrelativo(Integer fromCorrelativo) {
		this.fromCorrelativo = fromCorrelativo;
	}

	/**
	 * Gets the to correlativo.
	 *
	 * @return the to correlativo
	 */
	public Integer getToCorrelativo() {
		return toCorrelativo;
	}

	/**
	 * Sets the to correlativo.
	 *
	 * @param toCorrelativo the new to correlativo
	 */
	public void setToCorrelativo(Integer toCorrelativo) {
		this.toCorrelativo = toCorrelativo;
	}

	/**
	 * Gets the number of certificates.
	 *
	 * @return the number of certificates
	 */
	public Integer getNumberOfCertificates() {
		return numberOfCertificates;
	}

	public boolean isSecIsFixedIncome() {
		return secIsFixedIncome;
	}

	public void setSecIsFixedIncome(boolean secIsFixedIncome) {
		this.secIsFixedIncome = secIsFixedIncome;
	}

	/**
	 * Sets the number of certificates.
	 *
	 * @param numberOfCertificates the new number of certificates
	 */
	public void setNumberOfCertificates(Integer numberOfCertificates) {
		this.numberOfCertificates = numberOfCertificates;
	}

	/**
	 * Gets the certificate number.
	 *
	 * @return the certificate number
	 */
	public Long getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the new certificate number
	 */
	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * Gets the max date.
	 *
	 * @return the max date
	 */
	public Date getMaxDate() {
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate the new max date
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * Gets the certificates nums.
	 *
	 * @return the certificates nums
	 */
	public List<Long> getCertificatesNums() {
		return certificatesNums;
	}

	/**
	 * Sets the certificates nums.
	 *
	 * @param certificatesNums the new certificates nums
	 */
	public void setCertificatesNums(List<Long> certificatesNums) {
		this.certificatesNums = certificatesNums;
	}

	/**
	 * Gets the security class selected.
	 *
	 * @return the security class selected
	 */
	public Integer getSecurityClassSelected() {
		return securityClassSelected;
	}

	/**
	 * Sets the security class selected.
	 *
	 * @param securityClassSelected the new security class selected
	 */
	public void setSecurityClassSelected(Integer securityClassSelected) {
		this.securityClassSelected = securityClassSelected;
	}

	/**
	 * Checks if is modify.
	 *
	 * @return true, if is modify
	 */
	public boolean isModify() {
		return modify;
	}

	/**
	 * Sets the modify.
	 *
	 * @param modify the new modify
	 */
	public void setModify(boolean modify) {
		this.modify = modify;
	}

	/**
	 * Checks if is show.
	 *
	 * @return true, if is show
	 */
	public boolean isShow() {
		return show;
	}

	/**
	 * Sets the show.
	 *
	 * @param show the new show
	 */
	public void setShow(boolean show) {
		this.show = show;
	}

	/**
	 * Checks if is validator disabled.
	 *
	 * @return true, if is validator disabled
	 */
	public boolean isValidatorDisabled() {
		return validatorDisabled;
	}

	/**
	 * Sets the validator disabled.
	 *
	 * @param validatorDisabled the new validator disabled
	 */
	public void setValidatorDisabled(boolean validatorDisabled) {
		this.validatorDisabled = validatorDisabled;
	}

	/**
	 * Checks if is rendered instrument type.
	 *
	 * @return true, if is rendered instrument type
	 */
	public boolean isRenderedInstrumentType() {
		return renderedInstrumentType;
	}

	/**
	 * Sets the rendered instrument type.
	 *
	 * @param renderedInstrumentType the new rendered instrument type
	 */
	public void setRenderedInstrumentType(boolean renderedInstrumentType) {
		this.renderedInstrumentType = renderedInstrumentType;
	}

	/**
	 * Gets the instrument type list.
	 *
	 * @return the instrument type list
	 */
	public List<ParameterTable> getInstrumentTypeList() {
		return instrumentTypeList;
	}

	/**
	 * Sets the instrument type list.
	 *
	 * @param instrumentTypeList the new instrument type list
	 */
	public void setInstrumentTypeList(List<ParameterTable> instrumentTypeList) {
		this.instrumentTypeList = instrumentTypeList;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Checks if is flag render field.
	 *
	 * @return true, if is flag render field
	 */
	public boolean isFlagRenderField() {
		return flagRenderField;
	}

	/**
	 * Sets the flag render field.
	 *
	 * @param flagRenderField the new flag render field
	 */
	public void setFlagRenderField(boolean flagRenderField) {
		this.flagRenderField = flagRenderField;
	}

	/**
	 * Gets the certificate quantity.
	 *
	 * @return the certificate quantity
	 */
	public BigDecimal getCertificateQuantity() {
		return CertificateQuantity;
	}

	/**
	 * Sets the certificate quantity.
	 *
	 * @param CertificateQuantity the new certificate quantity
	 */
	public void setCertificateQuantity(BigDecimal CertificateQuantity) {
		this.CertificateQuantity = CertificateQuantity;
	}

	/**
	 * Checks if is flag dpf.
	 *
	 * @return true, if is flag dpf
	 */
	public boolean isFlagDPF() {
		return flagDPF;
	}

	/**
	 * Sets the flag dpf.
	 *
	 * @param flagDPF the new flag dpf
	 */
	public void setFlagDPF(boolean flagDPF) {
		this.flagDPF = flagDPF;
	}

	/**
	 * Gets the source holder.
	 *
	 * @return the source holder
	 */
	public Holder getSourceHolder() {
		return sourceHolder;
	}

	/**
	 * Sets the source holder.
	 *
	 * @param sourceHolder the new source holder
	 */
	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}

	/**
	 * Gets the source account to.
	 *
	 * @return the source account to
	 */
	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	/**
	 * Sets the source account to.
	 *
	 * @param sourceAccountTo the new source account to
	 */
	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}

	/**
	 * Gets the lst source account to.
	 *
	 * @return the lst source account to
	 */
	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	/**
	 * Sets the lst source account to.
	 *
	 * @param lstSourceAccountTo the new lst source account to
	 */
	public void setLstSourceAccountTo(
			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	/**
	 * Gets the reg physical cert marketfact.
	 *
	 * @return the reg physical cert marketfact
	 */
	public PhysicalCertMarketfact getRegPhysicalCertMarketfact() {
		return regPhysicalCertMarketfact;
	}

	/**
	 * Sets the reg physical cert marketfact.
	 *
	 * @param regPhysicalCertMarketfact the new reg physical cert marketfact
	 */
	public void setRegPhysicalCertMarketfact(
			PhysicalCertMarketfact regPhysicalCertMarketfact) {
		this.regPhysicalCertMarketfact = regPhysicalCertMarketfact;
	}

	/**
	 * Gets the reg lst physical cert marketfacts.
	 *
	 * @return the reg lst physical cert marketfacts
	 */
	public List<PhysicalCertMarketfact> getRegLstPhysicalCertMarketfacts() {
		return regLstPhysicalCertMarketfacts;
	}

	/**
	 * Sets the reg lst physical cert marketfacts.
	 *
	 * @param regLstPhysicalCertMarketfacts the new reg lst physical cert marketfacts
	 */
	public void setRegLstPhysicalCertMarketfacts(
			List<PhysicalCertMarketfact> regLstPhysicalCertMarketfacts) {
		this.regLstPhysicalCertMarketfacts = regLstPhysicalCertMarketfacts;
	}
	
	public List<HolderMarketFactBalance> getListHolderMarketFactBalance() {
		return listHolderMarketFactBalance;
	}
	
	public void setListHolderMarketFactBalance(
			List<HolderMarketFactBalance> listHolderMarketFactBalance) {
		this.listHolderMarketFactBalance = listHolderMarketFactBalance;
	}
	
	public HolderMarketFactBalance getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	public void setHolderMarketFactBalance(
			HolderMarketFactBalance holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}


	/**
	 * Gets the arr reg physical cert marketfact.
	 *
	 * @return the arr reg physical cert marketfact
	 */
	public PhysicalCertMarketfact[] getArrRegPhysicalCertMarketfact() {
		return arrRegPhysicalCertMarketfact;
	}

	/**
	 * Sets the arr reg physical cert marketfact.
	 *
	 * @param arrRegPhysicalCertMarketfact the new arr reg physical cert marketfact
	 */
	public void setArrRegPhysicalCertMarketfact(
			PhysicalCertMarketfact[] arrRegPhysicalCertMarketfact) {
		this.arrRegPhysicalCertMarketfact = arrRegPhysicalCertMarketfact;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the security search.
	 *
	 * @return the security search
	 */
	public Security getSecuritySearch() {
		return securitySearch;
	}

	/**
	 * Sets the security search.
	 *
	 * @param securitySearch the new security search
	 */
	public void setSecuritySearch(Security securitySearch) {
		this.securitySearch = securitySearch;
	}

	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}		
	
	/**
	 * Gets the quantity eur.
	 *
	 * @return the quantity eur
	 */
	public int getQuantityEUR() {
		return quantityEUR;
	}

	/**
	 * Sets the quantity eur.
	 *
	 * @param quantityEUR the new quantity eur
	 */
	public void setQuantityEUR(int quantityEUR) {
		this.quantityEUR = quantityEUR;
	}

	/**
	 * Gets the quantity ufv.
	 *
	 * @return the quantity ufv
	 */
	public int getQuantityUFV() {
		return quantityUFV;
	}

	/**
	 * Sets the quantity ufv.
	 *
	 * @param quantityUFV the new quantity ufv
	 */
	public void setQuantityUFV(int quantityUFV) {
		this.quantityUFV = quantityUFV;
	}

	/**
	 * Gets the quantity mvd.
	 *
	 * @return the quantity mvd
	 */
	public int getQuantityMVD() {
		return quantityMVD;
	}

	/**
	 * Sets the quantity mvd.
	 *
	 * @param quantityMVD the new quantity mvd
	 */
	public void setQuantityMVD(int quantityMVD) {
		this.quantityMVD = quantityMVD;
	}

	/**
	 * Gets the sum eur.
	 *
	 * @return the sum eur
	 */
	public BigDecimal getSumEUR() {
		return sumEUR;
	}

	/**
	 * Sets the sum eur.
	 *
	 * @param sumEUR the new sum eur
	 */
	public void setSumEUR(BigDecimal sumEUR) {
		this.sumEUR = sumEUR;
	}

	/**
	 * Gets the sum ufv.
	 *
	 * @return the sum ufv
	 */
	public BigDecimal getSumUFV() {
		return sumUFV;
	}

	/**
	 * Sets the sum ufv.
	 *
	 * @param sumUFV the new sum ufv
	 */
	public void setSumUFV(BigDecimal sumUFV) {
		this.sumUFV = sumUFV;
	}

	/**
	 * Gets the sum mvd.
	 *
	 * @return the sum mvd
	 */
	public BigDecimal getSumMVD() {
		return sumMVD;
	}

	/**
	 * Sets the sum mvd.
	 *
	 * @param sumMVD the new sum mvd
	 */
	public void setSumMVD(BigDecimal sumMVD) {
		this.sumMVD = sumMVD;
	}

	/**
	 * Load security class.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadSecurityClass() throws ServiceException {
	    ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		lstSecurityClass=generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
    }
	/**
	 * Gets the holder account list.
	 *
	 * @return the holder account list
	 */
	public List<HolderAccount> getHolderAccountList() {
		return holderAccountList;
	}

	/**
	 * Sets the holder account list.
	 *
	 * @param holderAccountList the new holder account list
	 */
	public void setHolderAccountList(List<HolderAccount> holderAccountList) {
		this.holderAccountList = holderAccountList;
	}

	/**
	 * Get flag is variable income
	 * 
	 * @return the flagIsVariableIncome
	 */
	public boolean isFlagIsVariableIncome() {
		return flagIsVariableIncome;
	}

	/**
	 * Set flag is variable income
	 * 
	 * @param flagIsVariableIncome the flagIsVariableIncome to set
	 */
	public void setFlagIsVariableIncome(boolean flagIsVariableIncome) {
		this.flagIsVariableIncome = flagIsVariableIncome;
	}	
		
	/**
	 * Gets the issuer help.
	 *
	 * @return the issuer help
	 */
	public Issuer getIssuerHelp() {
		return issuerHelp;
	}

	/**
	 * Sets the issuer help.
	 *
	 * @param issuerHelp the new issuer help
	 */
	public void setIssuerHelp(Issuer issuerHelp) {
		this.issuerHelp = issuerHelp;
	}
	
	/**
	 * Gets the issuer search pk.
	 *
	 * @return the issuer search pk
	 */
	public String getIssuerSearchPK() {
		return issuerSearchPK;
	}

	/**
	 * Sets the issuer search pk.
	 *
	 * @param issuerSearchPK the new issuer search pk
	 */
	public void setIssuerSearchPK(String issuerSearchPK) {
		this.issuerSearchPK = issuerSearchPK;
	}

	public PhysicalCertificateFile getFileExcelPersist() {
		return fileExcelPersist;
	}

	public void setFileExcelPersist(PhysicalCertificateFile fileExcelPersist) {
		this.fileExcelPersist = fileExcelPersist;
	}

	public DataExcelRegCertificateDepositeTO getResultExcel() {
		return resultExcel;
	}

	public void setResultExcel(DataExcelRegCertificateDepositeTO resultExcel) {
		this.resultExcel = resultExcel;
	}

	public PhysicalCertificateFile getFileZip() {
		return fileZip;
	}

	public void setFileZip(PhysicalCertificateFile fileZip) {
		this.fileZip = fileZip;
	}

	public String getfUploadFileSizeDisplayMassive() {
		return fUploadFileSizeDisplayMassive;
	}

	public String getfUploadFileTypesDocumentsDisplayMassive() {
		return fUploadFileTypesDocumentsDisplayMassive;
	}

}