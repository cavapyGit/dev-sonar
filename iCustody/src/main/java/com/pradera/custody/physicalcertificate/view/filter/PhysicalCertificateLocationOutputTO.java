package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateLocationOutputTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Oct 18, 2013
 */
public class PhysicalCertificateLocationOutputTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The request type. */
	private Integer 	 requestType;

	/** The request type desc. */
	private String		 requestTypeDesc;
	
	/** The request number. */
	private Integer 	 requestNumber;
	
	/** The deposit date. */
	private String 		 depositDate;
	
	/** The issuer code. */
	private String		 issuerCode;
	
	/** The issuer mnemonic. */
	private String		 issuerMnemonic;
	
	/** The issuer description. */
	private String		 issuerDescription;
	
	/** The certificate number. */
	private Long		 certificateNumber;
	
	/** The account number. */
	private Integer		 accountNumber;
	
	/** The holder list. */
	private List<String> holderList;
	
	/** The certificate status. */
	private	Integer		 certificateStatus;
	
	/** The certificate status desc. */
	private String 		 certificateStatusDesc;
	
	/** The situation. */
	private Integer 	 situation;
	
	/** The situation desc. */
	private String 		 situationDesc;
	
	/** The id physical certificate. */
	private Long 		 idPhysicalCertificate;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The str participant. */
	private String strParticipant;
	
	/** The str security code pk. */
	private String strSecurityCodePk;
	
	/** The str security class description. */
	private String strSecurityClassDescription;
	
	/** The id participantpk. */
	private Long idParticipantpk;
	
	/** The sequence. */
	private long sequence;
	
	//GETTERS AND SETTERS
	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}
	
	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}
	
	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Integer getRequestNumber() {
		return requestNumber;
	}
	
	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Integer requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	/**
	 * Gets the deposit date.
	 *
	 * @return the deposit date
	 */
	public String getDepositDate() {
		return depositDate;
	}
	
	/**
	 * Sets the deposit date.
	 *
	 * @param depositDate the new deposit date
	 */
	public void setDepositDate(String depositDate) {
		this.depositDate = depositDate;
	}
	
	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}
	
	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	
	/**
	 * Gets the issuer mnemonic.
	 *
	 * @return the issuer mnemonic
	 */
	public String getIssuerMnemonic() {
		return issuerMnemonic;
	}
	
	/**
	 * Sets the issuer mnemonic.
	 *
	 * @param issuerMnemonic the new issuer mnemonic
	 */
	public void setIssuerMnemonic(String issuerMnemonic) {
		this.issuerMnemonic = issuerMnemonic;
	}
	
	/**
	 * Gets the certificate number.
	 *
	 * @return the certificate number
	 */
	public Long getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the new certificate number
	 */
	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the holder list.
	 *
	 * @return the holder list
	 */
	public List<String> getHolderList() {
		return holderList;
	}
	
	/**
	 * Sets the holder list.
	 *
	 * @param holderList the new holder list
	 */
	public void setHolderList(List<String> holderList) {
		this.holderList = holderList;
	}
	
	/**
	 * Gets the certificate status.
	 *
	 * @return the certificate status
	 */
	public Integer getCertificateStatus() {
		return certificateStatus;
	}
	
	/**
	 * Sets the certificate status.
	 *
	 * @param certificateStatus the new certificate status
	 */
	public void setCertificateStatus(Integer certificateStatus) {
		this.certificateStatus = certificateStatus;
	}
	
	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}
	
	/**
	 * Sets the situation.
	 *
	 * @param situation the new situation
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}

	/**
	 * Gets the id physical certificate.
	 *
	 * @return the id physical certificate
	 */
	public Long getIdPhysicalCertificate() {
		return idPhysicalCertificate;
	}

	/**
	 * Sets the id physical certificate.
	 *
	 * @param idPhysicalCertificate the new id physical certificate
	 */
	public void setIdPhysicalCertificate(Long idPhysicalCertificate) {
		this.idPhysicalCertificate = idPhysicalCertificate;
	}

	/**
	 * Gets the issuer description.
	 *
	 * @return the issuer description
	 */
	public String getIssuerDescription() {
		return issuerDescription;
	}

	/**
	 * Sets the issuer description.
	 *
	 * @param issuerDescription the new issuer description
	 */
	public void setIssuerDescription(String issuerDescription) {
		this.issuerDescription = issuerDescription;
	}

	/**
	 * Gets the certificate status desc.
	 *
	 * @return the certificate status desc
	 */
	public String getCertificateStatusDesc() {
		return certificateStatusDesc;
	}

	/**
	 * Sets the certificate status desc.
	 *
	 * @param certificateStatusDesc the new certificate status desc
	 */
	public void setCertificateStatusDesc(String certificateStatusDesc) {
		this.certificateStatusDesc = certificateStatusDesc;
	}

	/**
	 * Gets the situation desc.
	 *
	 * @return the situation desc
	 */
	public String getSituationDesc() {
		return situationDesc;
	}

	/**
	 * Sets the situation desc.
	 *
	 * @param situationDesc the new situation desc
	 */
	public void setSituationDesc(String situationDesc) {
		this.situationDesc = situationDesc;
	}

	/**
	 * Gets the sequence.
	 *
	 * @return the sequence
	 */
	public long getSequence() {
		return sequence;
	}

	/**
	 * Sets the sequence.
	 *
	 * @param sequence the new sequence
	 */
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}

	/**
	 * Gets the request type desc.
	 *
	 * @return the request type desc
	 */
	public String getRequestTypeDesc() {
		return requestTypeDesc;
	}

	/**
	 * Sets the request type desc.
	 *
	 * @param requestTypeDesc the new request type desc
	 */
	public void setRequestTypeDesc(String requestTypeDesc) {
		this.requestTypeDesc = requestTypeDesc;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the str participant.
	 *
	 * @return the str participant
	 */
	public String getStrParticipant() {
		return strParticipant;
	}

	/**
	 * Sets the str participant.
	 *
	 * @param strParticipant the new str participant
	 */
	public void setStrParticipant(String strParticipant) {
		this.strParticipant = strParticipant;
	}

	/**
	 * Gets the str security code pk.
	 *
	 * @return the str security code pk
	 */
	public String getStrSecurityCodePk() {
		return strSecurityCodePk;
	}

	/**
	 * Sets the str security code pk.
	 *
	 * @param strSecurityCodePk the new str security code pk
	 */
	public void setStrSecurityCodePk(String strSecurityCodePk) {
		this.strSecurityCodePk = strSecurityCodePk;
	}

	/**
	 * Gets the str security class description.
	 *
	 * @return the str security class description
	 */
	public String getStrSecurityClassDescription() {
		return strSecurityClassDescription;
	}

	/**
	 * Sets the str security class description.
	 *
	 * @param strSecurityClassDescription the new str security class description
	 */
	public void setStrSecurityClassDescription(String strSecurityClassDescription) {
		this.strSecurityClassDescription = strSecurityClassDescription;
	}

	/**
	 * Gets the id participantpk.
	 *
	 * @return the id participantpk
	 */
	public Long getIdParticipantpk() {
		return idParticipantpk;
	}

	/**
	 * Sets the id participantpk.
	 *
	 * @param idParticipantpk the new id participantpk
	 */
	public void setIdParticipantpk(Long idParticipantpk) {
		this.idParticipantpk = idParticipantpk;
	}
	
	
}
