package com.pradera.custody.physicalcertificate.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class PhysicalCertificateDataModelFilter
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCustody
 * @Creation_Date :
 */
public class PhysicalCertificateDataModelFilter extends ListDataModel<PhysicalCertificateFilter> implements SelectableDataModel<PhysicalCertificateFilter>, Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/**
	 * Instantiates a new physical certificate data model filter.
	 */
	public PhysicalCertificateDataModelFilter() {
		
	}
	
	/**
	 * Instantiates a new physical certificate data model filter.
	 *
	 * @param lstPhysicalCertificate the lst physical certificate
	 */
	public PhysicalCertificateDataModelFilter(List<PhysicalCertificateFilter> lstPhysicalCertificate) {
		super(lstPhysicalCertificate);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public PhysicalCertificateFilter getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<PhysicalCertificateFilter> ListPhysicalCertificate = (List<PhysicalCertificateFilter>)getWrappedData();
		for(PhysicalCertificateFilter physicalCertificate : ListPhysicalCertificate){
			if(new Long(physicalCertificate.getSequence()).toString().equals(rowKey))
				return physicalCertificate;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(PhysicalCertificateFilter physicalCertificate) {//Return Object
		// TODO Auto-generated method stub
		return physicalCertificate.getSequence();
	}
}
