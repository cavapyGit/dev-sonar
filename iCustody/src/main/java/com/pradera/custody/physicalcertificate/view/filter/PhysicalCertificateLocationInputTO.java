package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.custody.type.PaymentType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateLocationInputTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Oct 17, 2013
 */
public class PhysicalCertificateLocationInputTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long 		idParticipantPk;
	
	/** The cui holder. */
	private Long 		cuiHolder;
	
	/** The account number. */
	private Integer 	accountNumber;
	
	/** The request type. */
	private Integer		requestType;
	
	/** The request number. */
	private Integer		requestNumber;
	
	/** The issuer code. */
	private String		issuerCode;
	
	/** The certificate number. */
	private Integer 	certificateNumber;
	
	/** The certificate status. */
	private Integer		certificateStatus;
	
	/** The certificate location. */
	private Integer		certificateLocation;
	
	/** The deposit initial date. */
	private Date		depositInitialDate;
	
	/** The deposit final date. */
	private	Date		depositFinalDate;
	
	/** The issue date. */
	private String issueDate;
	
	/** The expiration date. */
	private String expirationDate;
	
	/** The currency. */
	private Integer currency;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The payment periodicity. */
	private Integer paymentPeriodicity;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The class type. */
	private Integer classType;
	
	/** The str class type. */
	private String strClassType;

	/** The str participant. */
	private String strParticipant;
	
	/** The security help. */
	private Security securityHelp = new Security();
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	private Integer payRollType;
	
	private Integer motive;
	
	private Integer securityClass;
	
	private Integer securityCurrency;
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Gets the str participant.
	 *
	 * @return the str participant
	 */
	public String getStrParticipant() {
		return strParticipant;
	}

	/**
	 * Sets the str participant.
	 *
	 * @param strParticipant the new str participant
	 */
	public void setStrParticipant(String strParticipant) {
		this.strParticipant = strParticipant;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public Integer getRequestType() {
		return requestType;
	}
	
	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}
	
	/**
	 * Gets the request number.
	 *
	 * @return the request number
	 */
	public Integer getRequestNumber() {
		return requestNumber;
	}
	
	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the new request number
	 */
	public void setRequestNumber(Integer requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	/**
	 * Gets the issuer code.
	 *
	 * @return the issuer code
	 */
	public String getIssuerCode() {
		return issuerCode;
	}
	
	/**
	 * Sets the issuer code.
	 *
	 * @param issuerCode the new issuer code
	 */
	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}
	
	/**
	 * Gets the certificate number.
	 *
	 * @return the certificate number
	 */
	public Integer getCertificateNumber() {
		return certificateNumber;
	}
	
	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the new certificate number
	 */
	public void setCertificateNumber(Integer certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	
	/**
	 * Gets the certificate status.
	 *
	 * @return the certificate status
	 */
	public Integer getCertificateStatus() {
		return certificateStatus;
	}
	
	/**
	 * Sets the certificate status.
	 *
	 * @param certificateStatus the new certificate status
	 */
	public void setCertificateStatus(Integer certificateStatus) {
		this.certificateStatus = certificateStatus;
	}
	
	/**
	 * Gets the certificate location.
	 *
	 * @return the certificate location
	 */
	public Integer getCertificateLocation() {
		return certificateLocation;
	}
	
	/**
	 * Sets the certificate location.
	 *
	 * @param certificateLocation the new certificate location
	 */
	public void setCertificateLocation(Integer certificateLocation) {
		this.certificateLocation = certificateLocation;
	}
	
	/**
	 * Gets the deposit initial date.
	 *
	 * @return the deposit initial date
	 */
	public Date getDepositInitialDate() {
		return depositInitialDate;
	}
	
	/**
	 * Sets the deposit initial date.
	 *
	 * @param depositInitialDate the new deposit initial date
	 */
	public void setDepositInitialDate(Date depositInitialDate) {
		this.depositInitialDate = depositInitialDate;
	}
	
	/**
	 * Gets the deposit final date.
	 *
	 * @return the deposit final date
	 */
	public Date getDepositFinalDate() {
		return depositFinalDate;
	}
	
	/**
	 * Sets the deposit final date.
	 *
	 * @param depositFinalDate the new deposit final date
	 */
	public void setDepositFinalDate(Date depositFinalDate) {
		this.depositFinalDate = depositFinalDate;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the currency type.
	 *
	 * @return the currency type
	 */
	public String getCurrencyType() {
		String description = null;
		if (currency != null) {
			description = CurrencyType.get(currency).getValue();
			
		}
		return description;
	}
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public String getInstrumentsType() {
		String description = null;
		if (instrumentType != null) {
			description = InstrumentType.get(instrumentType).getValue();
			
		}
		return description;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the new instrument type
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the payment periodicity.
	 *
	 * @return the payment periodicity
	 */
	public Integer getPaymentPeriodicity() {
		return paymentPeriodicity;
	}

	/**
	 * Sets the payment periodicity.
	 *
	 * @return the payment periodicity type
	 */
	public String getPaymentPeriodicityType() {
		String description = null;
		if (paymentPeriodicity != null) {
			if(paymentPeriodicity.equals(PaymentType.MENSUAL.getCode())			||
			   paymentPeriodicity.equals(PaymentType.BIMESTRAL.getCode())		||
			   paymentPeriodicity.equals(PaymentType.TRIMESTRAL.getCode()) 		||
			   paymentPeriodicity.equals(PaymentType.CUATRIMESTRAL.getCode())	||
			   paymentPeriodicity.equals(PaymentType.SEMESTRAL.getCode()) 			||
			   paymentPeriodicity.equals(PaymentType.ANUAL.getCode())){
			description = PaymentType.get(paymentPeriodicity).getValue();
			}
			
		}
		return description;
	}

	/**
	 * Gets the issue date.
	 *
	 * @return the issue date
	 */
	public String getIssueDate() {
		return issueDate;
	}

	/**
	 * Sets the issue date.
	 *
	 * @param issueDate the new issue date
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the class type.
	 *
	 * @return the class type
	 */
	public Integer getClassType() {
		return classType;
	}

	/**
	 * Sets the class type.
	 *
	 * @param classType the new class type
	 */
	public void setClassType(Integer classType) {
		this.classType = classType;
	}

	/**
	 * Gets the str class type.
	 *
	 * @return the str class type
	 */
	public String getStrClassType() {
		return strClassType;
	}

	/**
	 * Sets the str class type.
	 *
	 * @param strClassType the new str class type
	 */
	public void setStrClassType(String strClassType) {
		this.strClassType = strClassType;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrument type
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the payment periodicity.
	 *
	 * @param paymentPeriodicity the new payment periodicity
	 */
	public void setPaymentPeriodicity(Integer paymentPeriodicity) {
		this.paymentPeriodicity = paymentPeriodicity;
	}

	/**
	 * Gets the cui holder.
	 *
	 * @return the cui holder
	 */
	public Long getCuiHolder() {
		return cuiHolder;
	}

	/**
	 * Sets the cui holder.
	 *
	 * @param cuiHolder the new cui holder
	 */
	public void setCuiHolder(Long cuiHolder) {
		this.cuiHolder = cuiHolder;
	}

	/**
	 * Gets the security help.
	 *
	 * @return the security help
	 */
	public Security getSecurityHelp() {
		return securityHelp;
	}

	/**
	 * Sets the security help.
	 *
	 * @param securityHelp the new security help
	 */
	public void setSecurityHelp(Security securityHelp) {
		this.securityHelp = securityHelp;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	public Integer getPayRollType() {
		return payRollType;
	}

	public void setPayRollType(Integer payRollType) {
		this.payRollType = payRollType;
	}
	
	public Integer getMotive() {
		return motive;
	}

	public void setMotive(Integer motive) {
		this.motive = motive;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public Integer getSecurityCurrency() {
		return securityCurrency;
	}

	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}
	
}
