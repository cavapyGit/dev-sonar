package com.pradera.custody.physicalcertificate.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.helperui.service.AccountQueryServiceBean;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.custody.physicalcertificate.service.CertificateLocationServiceBean;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateLocationInputTO;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateLocationOutputTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.custody.physical.CertificateSituationHistory;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CertificateLocationFacadeBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCustody
 * @Creation_Date :
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class CertificateLocationServiceFacade implements Serializable {
	
	/** added default serial uid. */
	private static final long serialVersionUID = 1L;
	
	/** The location service bean. */
	@EJB
	private CertificateLocationServiceBean locationServiceBean;
	
	/** The general parameters service. */
	//ADDED BY MARTIN ZARATE RAFAEL
	@EJB
	ParameterServiceBean generalParametersService;
	
	/** The account query service. */
	@EJB
	AccountQueryServiceBean accountQueryService;
	
	/** The physical operation type. */
	private static int PHYSICAL_OPERATION_TYPE 	  = 0;
	
	/** The operation number. */
	private static int OPERATION_NUMBER   = 1;
	
	/** The deposit date. */
	private static int DEPOSIT_DATE 			  = 2;
	
	/** The id issuer fk. */
	private static int ID_ISSUER_FK 			  = 3;
	
	/** The certificate number. */
	private static int CERTIFICATE_NUMBER     	  = 4;
	
	/** The account number. */
	private static int ACCOUNT_NUMBER		 	  = 5;
	
	/** The id holder account pk. */
	private static int ID_HOLDER_ACCOUNT_PK   	  = 6;
	
	/** The state. */
	private static int STATE 					  = 7;
	
	/** The situation. */
	private static int SITUATION 				  = 8;
	
	/** The id physical certificate pk. */
	private static int ID_PHYSICAL_CERTIFICATE_PK = 9;
	
	/** The last modify date. */
	private static int LAST_MODIFY_DATE 		  = 10;
	
	/** The security class. */
	private static int SECURITY_CLASS 		 	  = 11;
	
	/** The registry date. */
	private static int REGISTRY_DATE 			  = 12;
	
	/** The mnemonic. */
	private static int MNEMONIC 				  = 13;
	
	/** The business name. */
	private static int BUSINESS_NAME 			  = 14;
	
	/** The state desc. */
	private static int STATE_DESC 			  	  = 15;
	
	/** The situation desc. */
	private static int SITUATION_DESC 			  = 16;
	
	/** The security class desc. */
	private static int SECURITY_CLASS_DESC 		  = 17;
	
	/** The participant desc. */
	private static int PARTICIPANT_DESC 		  = 18;
	
	/** The security code pk. */
	private static int SECURITY_CODE_PK 		  = 19;
	
	/** The id participant pk. */
	private static int ID_PARTICIPANT_PK 		  = 20;
	
	/** Holds current logger user details. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;

	/**
	 * The method for getting the security locations.
	 *
	 * @param id Long
	 * @return List of ParameterTable objects
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getSecurityLocationFacade(Long id)throws ServiceException{ 
			return locationServiceBean.getSecurityLocationService(id);
	}
	
	/**
	 * Used to get Search List.
	 *
	 * @param physicalCertificateFilter PhysicalCertificateFilter
	 * @return the List<Object[]>
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> getPhysicalCertificateListObj(
			PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException {		
		return locationServiceBean.getPhysicalCertificateListObjService(physicalCertificateFilter);
	}

	/**
	 * Used to get Physical Certificate for the id .
	 *
	 * @param idPhysicalCertificatePk the id physical certificate pk
	 * @return  the PhysicalCertificate entity
	 * @throws ServiceException the service exception
	 */
	public PhysicalCertificate getPhysicalCertificateDetailsById(Long idPhysicalCertificatePk)throws ServiceException {
		return locationServiceBean.getLastModifiedDateDetailsService(idPhysicalCertificatePk);
	}


	/**
	 * Method for insert the record into the database when updating the location
	 * of the physical security.
	 *
	 * @param history            CertificateSituationHistory
	 * @return the object of CertificateSituationHistory entity
	 * @throws ServiceException the service exception
	 */
	public CertificateSituationHistory insertModifiedLocation(CertificateSituationHistory history)throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return locationServiceBean.insertModifiedLocationService(history);
	}
	
	/**
	 * Method for updating the location of the Physical certificate.
	 *
	 * @param certificate PhysicalCertificate
	 * @param confirmed the confirmed
	 * @return the object of PhysicalCertificate entity
	 * @throws ServiceException the service exception
	 */
	public Object updateModifiedLocation(PhysicalCertificate certificate,boolean confirmed )throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return locationServiceBean.updateModifiedLocationService(certificate,confirmed,loggerUser);
	}
	
	/**
	 * Method for updating the location automatically when confirming the deposit.
	 *
	 * @param physicalCertificates List<PhysicalCertificate>
	 * @param requestType the request type
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void automaticLocationUpdate(List<PhysicalCertificate> physicalCertificates,Integer requestType,LoggerUser loggerUser)throws ServiceException{
	    Integer tmpSituation=null;
	    boolean confirmed = false;
		for( PhysicalCertificate  certificate : physicalCertificates){
			CertificateSituationHistory situationHistory = new CertificateSituationHistory();
			Long state = locationServiceBean.getCertificateStateService(certificate);
			situationHistory.setPhysicalCertificate(certificate);
			situationHistory.setOldSituation(certificate.getSituation());
			
			if(requestType.equals(RequestType.DEPOSITEO.getCode()) && 
				certificate.getSituation().equals(SituationType.PARTICIPANT.getCode())){	//AL CONFIRMAR UN DEPOSITO
				situationHistory.setNewSituation(SituationType.DEPOSITARY_OPERATIONS.getCode());
				tmpSituation= SituationType.DEPOSITARY_OPERATIONS.getCode();						
				confirmed = true;
			}
			else if(requestType.equals(RequestType.DEPOSITEO.getCode()) &&
					certificate.getSituation().equals(SituationType.DEPOSITARY_OPERATIONS.getCode()) ){	//AL APLICAR CAVAPY
				situationHistory.setNewSituation(SituationType.DEPOSITARY_VAULT.getCode());
				tmpSituation= SituationType.DEPOSITARY_VAULT.getCode();	//BOVEDA CAVAPY
			}
			/*
			else if(requestType.equals(RequestType.DEPOSITEO.getCode()) && 
					certificate.getSituation().equals(SituationType.DEPOSITARY_MESSENGER.getCode()) ){	//AL APLICAR EL BANCO CUSTODIA (PARA LLEGAR ACA DEBI MODIFICAR LA SITUACION A MENSAJERO DESDE OPERACIONES)
				situationHistory.setNewSituation(SituationType.DEPOSITARY_VAULT.getCode());	//BOVEDA CUSTODIA
				tmpSituation= SituationType.CUSTODY_VAULT.getCode();	
			}*/
			//RETIROS
			/*
			else if(requestType.equals(RequestType.RETIRO.getCode()) && 
					 state.equals(StateType.CONFIRMED.getCode()) &&
					 certificate.getSituation().equals(new Long(SituationType.DEPOSITARY_VAULT.getCode()))){	//SI ESTOY EN BOVEDA DE CAVAPY - CONFIRMO
				 situationHistory.setNewSituation(SituationType.DEPOSITARY_OPERATIONS.getCode());
				 tmpSituation= SituationType.DEPOSITARY_OPERATIONS.getCode();
		    }*/
			/*
		    else if(requestType.equals(RequestType.RETIRO.getCode()) && 
					 state.equals(StateType.APPLY.getCode()) &&
					 certificate.getSituation().equals(new Long(SituationType.CUSTODY_VAULT.getCode()))){	//SI ESTOY EN BOVEDA DE CUSTODIA - APLICO (al confirmar no debo entrar aca)
		    	situationHistory.setNewSituation(SituationType.DEPOSITARY_MESSENGER.getCode());
				tmpSituation= SituationType.DEPOSITARY_MESSENGER.getCode();	
		    }*/
		    else if(requestType.equals(RequestType.RETIRO.getCode()) && 
		    		SituationType.DEPOSITARY_OPERATIONS.getCode().equals(certificate.getSituation()))
		    {	//SI ESTOY EN OPERACIONES
				 situationHistory.setNewSituation(SituationType.PARTICIPANT.getCode());
				 tmpSituation= SituationType.PARTICIPANT.getCode();				//AUTORIZO Y PASA A PARTICIPANTE
		    }

			situationHistory.setSituationDate(CommonsUtilities.currentDateTime());
			situationHistory.setRegisterUser(loggerUser.getUserName());
			locationServiceBean.insertModifiedLocationService(situationHistory);
			certificate.setSituation(tmpSituation);
			locationServiceBean.updateModifiedLocationService(certificate,confirmed,loggerUser);
			confirmed = false;
		}
		
	}
	
	/**
	 * Method for getting the last modified date .
	 *
	 * @param idPhysicalCertificate Long
	 * @return the PhysicalCertificate entity bean
	 * @throws ServiceException the service exception
	 */
	public PhysicalCertificate getLastModifiedDateDetails(long idPhysicalCertificate)throws ServiceException{
		return locationServiceBean.getLastModifiedDateDetailsService(idPhysicalCertificate);
	}

	/**
	 * Search physical certificate location.
	 *
	 * @param physicalCertificateLocationInputTO the physical certificate location input to
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws ParseException the parse exception
	 */
	public List<PhysicalCertificateLocationOutputTO> searchPhysicalCertificateLocation(PhysicalCertificateLocationInputTO physicalCertificateLocationInputTO) throws ServiceException, ParseException {
		List<PhysicalCertificateLocationOutputTO> physicalCertificateLocationOutputTOs = new ArrayList<PhysicalCertificateLocationOutputTO>();
		DateFormat format = new SimpleDateFormat(CommonsUtilities.DATE_PATTERN);
		List<Object[]> 	objtAccountList ;
		HolderTO holderTO = new HolderTO();
		
		//SEARCH ALL ACCOUNTS IN PARTICIPANT
		//ACTIVE ACCOUNT
		holderTO.setAccountState(HolderAccountStatusType.ACTIVE.getCode());
		if(Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getIdParticipantPk())){
			holderTO.setPartIdPk(physicalCertificateLocationInputTO.getIdParticipantPk());
		}

		if(Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getIdParticipantPk())){
			holderTO.setParticipantFk(physicalCertificateLocationInputTO.getIdParticipantPk());
		}
		
		objtAccountList = accountQueryService.searchAccountByFilter(holderTO);
		
		for(Object[] obj:locationServiceBean.searchPhysicalCertificateLocation(physicalCertificateLocationInputTO) ){
			PhysicalCertificateLocationOutputTO	physicalCertOutputTO = new PhysicalCertificateLocationOutputTO();
		
				physicalCertOutputTO.setRequestType(Integer.valueOf(obj[PHYSICAL_OPERATION_TYPE].toString()));
				physicalCertOutputTO.setRequestTypeDesc(RequestType.get(Integer.valueOf(obj[PHYSICAL_OPERATION_TYPE].toString())).getValue());
				physicalCertOutputTO.setRequestNumber(Integer.valueOf(obj[OPERATION_NUMBER].toString()));

				String dapositDate = format.format(obj[DEPOSIT_DATE]);
				physicalCertOutputTO.setDepositDate(dapositDate);
				
				physicalCertOutputTO.setIssuerCode(obj[ID_ISSUER_FK].toString());
				if(Validations.validateIsNotNullAndNotEmpty(obj[CERTIFICATE_NUMBER]) && 
						Validations.validateIsNumeric(obj[CERTIFICATE_NUMBER].toString())){
					physicalCertOutputTO.setCertificateNumber(Long.valueOf(obj[CERTIFICATE_NUMBER].toString()));
				}				
				physicalCertOutputTO.setAccountNumber(Integer.valueOf(obj[ACCOUNT_NUMBER].toString()));
				
				holderTO.setAccountState(HolderAccountStatusType.ACTIVE.getCode());
				
				if(! Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getIdParticipantPk())){
					holderTO.setPartIdPk(physicalCertificateLocationInputTO.getIdParticipantPk());
					holderTO.setParticipantFk(physicalCertificateLocationInputTO.getIdParticipantPk());
					holderTO.setHolderAccountId(Long.valueOf(obj[6].toString()));
					objtAccountList = accountQueryService.searchAccountByFilter(holderTO);
				}
				
				if (obj[ACCOUNT_NUMBER] != null) {
					List<String> holderStringList = new ArrayList<String>();
						for(Object[] objectAccount : objtAccountList){
							Integer accountNumber = Integer.valueOf(objectAccount[0].toString());
							if(accountNumber.equals(Integer.valueOf(obj[ACCOUNT_NUMBER].toString()))){
								String holderName = "";
								
								if(Validations.validateIsNotNullAndNotEmpty(objectAccount[9])){//account_type
										Integer TipoCuenta = Integer.valueOf(objectAccount[9].toString());
										if(TipoCuenta.equals(HolderAccountType.NATURAL.getCode())){
											holderName = objectAccount[10].toString()+" - "+(String)objectAccount[2]+" "+(String)objectAccount[3]+""+(String)objectAccount[4];
										}else{
											holderName = objectAccount[10].toString()+" - "+(String)objectAccount[5];
										}
								}
								
								holderStringList.add(holderName);
							}
						}
						physicalCertOutputTO.setHolderList(holderStringList);
				}
				SimpleDateFormat formatDate = new SimpleDateFormat(CommonsUtilities.DATE_PATTERN);
				String date = formatDate.format(obj[LAST_MODIFY_DATE]);
				
				physicalCertOutputTO.setLastModifyDate(formatDate.parse(date));
				physicalCertOutputTO.setCertificateStatus(Integer.valueOf(obj[STATE].toString()));
				physicalCertOutputTO.setSituation(Integer.valueOf(obj[SITUATION].toString()));
				physicalCertOutputTO.setSituationDesc(obj[SITUATION_DESC].toString());
				physicalCertOutputTO.setIdPhysicalCertificate(Long.valueOf(obj[ID_PHYSICAL_CERTIFICATE_PK].toString()));
				physicalCertOutputTO.setIssuerMnemonic(obj[MNEMONIC].toString());
				physicalCertOutputTO.setIssuerCode(obj[ID_ISSUER_FK].toString());
				physicalCertOutputTO.setIssuerDescription(obj[BUSINESS_NAME].toString());
				physicalCertOutputTO.setCertificateStatusDesc(obj[STATE_DESC].toString());
				physicalCertOutputTO.setStrParticipant(obj[PARTICIPANT_DESC].toString());
				physicalCertOutputTO.setStrSecurityCodePk(obj[SECURITY_CODE_PK].toString());
				physicalCertOutputTO.setStrSecurityClassDescription(obj[SECURITY_CLASS_DESC].toString());
				physicalCertOutputTO.setIdParticipantpk(Long.valueOf(obj[ID_PARTICIPANT_PK].toString()));
				
				
				physicalCertificateLocationOutputTOs.add(physicalCertOutputTO);

		}
		
		return physicalCertificateLocationOutputTOs;
	}
	
}
