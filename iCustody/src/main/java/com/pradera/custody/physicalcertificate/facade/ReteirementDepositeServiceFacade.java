package com.pradera.custody.physicalcertificate.facade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.physicalcertificate.service.CertificateDepositServiceBean;
import com.pradera.custody.physicalcertificate.service.ReteirementDepositeServiceBean;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDataFilterBean;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDepositeFilterBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateMovement;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuanceType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ReteirementDepositeFacadeBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCustody
 * @Creation_Date :
 */
/**
 * @author lakshmij
 *
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ReteirementDepositeServiceFacade implements Serializable {
	
	/** added default serial uid. */
	private static final long serialVersionUID = 1L;
	
	/** The reteirement deposite service. */
	@EJB
	private ReteirementDepositeServiceBean reteirementDepositeService;
	
	/** The apply value. */
	private final Integer APPLY_VALUE = 0;
	
	/** The transaction registry. */
	@Resource
    private TransactionSynchronizationRegistry transactionRegistry;
	
	/** The certificate deposit service bean. */
	@EJB
	CertificateDepositServiceBean certificateDepositServiceBean;
	/** The crud dato service bean. */
	
	@EJB
	CrudDaoServiceBean crudDatoServiceBean;
	
	/**
	 * It gives emisor data.
	 *
	 * @param issuer the Issuer
	 * @return the  Issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer issueDetailsFacade(Issuer issuer)throws ServiceException{
		return reteirementDepositeService.issueDetailsService(issuer);
	}
	
	/**
	 * It gives securities data	.
	 *
	 * @param searchdata the searchdata
	 * @return the List<ReteirementDepositeFilterBean>
	 * @throws ServiceException the service exception
	 */
	public List<ReteirementDepositeFilterBean> searchSecuritiesFacade(ReteirementDataFilterBean searchdata)throws ServiceException{
		List<Object[]> search=reteirementDepositeService.searchSecuritiesService(searchdata);
		 
		List<ReteirementDepositeFilterBean> searchResult = new ArrayList<ReteirementDepositeFilterBean>();
		ReteirementDepositeFilterBean filterBean = null;

		List<ReteirementDepositeFilterBean> saveDetailsData = searchdata.getSaveData();
		boolean flag = false;
		
		for (Object[] obj : search) {
			flag = false;
			if (saveDetailsData != null && saveDetailsData.size() > 0) {
				filterBean = new ReteirementDepositeFilterBean();
				
				for (ReteirementDepositeFilterBean saveData : saveDetailsData){
					if (obj[4] != null) {
						if (saveData.getIdphysicalbalancedetail().equals(Long.valueOf(obj[4].toString()))){
							flag = true;
						}
					}
				}
				
				if (!flag) {

					if(Validations.validateIsNotNull(obj[0])){
						filterBean.setStrDepositeDate((Date) obj[0]);
					}
					if(Validations.validateIsNotNull(obj[1])){
						filterBean.setPhysicalCertificate((PhysicalCertificate)obj[1]);
					}
					if(Validations.validateIsNotNull(obj[2])){
						filterBean.getPhysicalCertificate().setSecurities((Security)obj[2]);
					}
					if(Validations.validateIsNotNull(obj[3])){
						filterBean.getPhysicalCertificate().setIssuer((Issuer)obj[3]);
					}
					if(Validations.validateIsNotNull(obj[4])){
						filterBean.setIdphysicalbalancedetail((Long)obj[4]);
					}
					//we validate the issuance type
					if (IssuanceType.MIXED.getCode().equals(filterBean.getPhysicalCertificate().getSecurities().getIssuanceForm())) {
						filterBean.setMixedIssuance(true);
					} else {
						filterBean.setMixedIssuance(false);
					}
					searchResult.add(filterBean);
				}
			} else {
				filterBean = new ReteirementDepositeFilterBean();
				
				if(Validations.validateIsNotNull(obj[0])){
					filterBean.setStrDepositeDate((Date) obj[0]);
				}
				if(Validations.validateIsNotNull(obj[1])){
					filterBean.setPhysicalCertificate((PhysicalCertificate)obj[1]);
				}
				if(Validations.validateIsNotNull(obj[2])){
					filterBean.getPhysicalCertificate().setSecurities((Security)obj[2]);
				}
				if(Validations.validateIsNotNull(obj[3])){
					filterBean.getPhysicalCertificate().setIssuer((Issuer)obj[3]);
				}
				if(Validations.validateIsNotNull(obj[4])){
					filterBean.setIdphysicalbalancedetail((Long)obj[4]);
				}
				//we validate the issuance type
				if (IssuanceType.MIXED.getCode().equals(filterBean.getPhysicalCertificate().getSecurities().getIssuanceForm())) {
					filterBean.setMixedIssuance(true);
				} else {
					filterBean.setMixedIssuance(false);
				}
				searchResult.add(filterBean);
			}
		}
		return searchResult;
	}

	/**
	 * It saves retiro register data.
	 *
	 * @param idPhysicalCertificatePk the id physical certificate pk
	 * @return the List<PhysicalOperationDetail>
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_REGISTER)
	/*public List<PhysicalOperationDetail> saveDataFacade(List<ReteirementDepositeFilterBean> inserdata,ReteirementDataFilterBean retireData)throws ServiceException{
		//SETTING AUDIT PARAMETERS
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		CustodyOperation custOper = new CustodyOperation();
		custOper.setOperationType(Long.valueOf(RequestType.RETIRO.getCode()));
		custOper.setState(retireData.getState());
		custOper.setRegistryDate(CommonsUtilities.currentDateTime());
		custOper.setRegistryUser(retireData.getUserName());
		custOper.setOperationDate(CommonsUtilities.currentDateTime());
		CustodyOperation saveDataCustody=	reteirementDepositeService.saveCustodyOperation(custOper);

		PhysicalCertificateOperation phyCerfOper = new PhysicalCertificateOperation();
		phyCerfOper.setCustodyOperation(saveDataCustody);
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk(retireData.getAccHolderPk());
		phyCerfOper.setHolderAccount(holderAccount);
		Participant participant = new Participant();
		participant.setIdParticipantPk(retireData.getIdParticipanrPk());
		phyCerfOper.setParticipant(participant);
		phyCerfOper.setPhysicalOperationType(new Long(RequestType.RETIRO.getCode()));
		phyCerfOper.setRequestDeliveryDate(retireData.getDeliverDate());
		//phyCerfOper.setState(new Long(retireData.getState()));	//anterior
		phyCerfOper.setState(StateType.REGISTERED_CERTIFICATE.getCode());
		phyCerfOper.setIndApplied(new Integer(APPLY_VALUE));				
		PhysicalCertificateOperation operData=	reteirementDepositeService.savePhysicalCertificateOperation(phyCerfOper);
		
		PhysicalOperationDetail phyOperDetail = new PhysicalOperationDetail();
		PhysicalCertificate phyCert = new PhysicalCertificate();
		List<PhysicalOperationDetail> saveResult = new ArrayList<PhysicalOperationDetail>();
		for (ReteirementDepositeFilterBean fFilterBean : inserdata) {
			phyOperDetail = new PhysicalOperationDetail();
			phyCert = new PhysicalCertificate();
			phyCert.setIdPhysicalCertificatePk(fFilterBean.getIdPhysicalCertificatePk());
			phyCert.setCertificateNumber(fFilterBean.getCertificateNumber());
			phyOperDetail.setPhysicalCertificate(phyCert);
			phyOperDetail.setPhysicalCertificateOperation(operData);
			phyOperDetail.setState(retireData.getState());
			phyOperDetail.setRegistryUser(loggerUser.getUserName());
			phyOperDetail.setRegistryDate(CommonsUtilities.currentDateTime());
			phyOperDetail.setIndApplied(APPLY_VALUE);
			PhysicalOperationDetail savePhyOper = reteirementDepositeService.savePhysicalOperationDetail(phyOperDetail);					
			saveResult.add(savePhyOper);
		}
		return	saveResult;
	}*/
	public PhysicalCertMarketfact getPhysicalCertMarketfact(Long idPhysicalCertificatePk){
		return certificateDepositServiceBean.searchPhysicalCertMarketfact(idPhysicalCertificatePk);
	}
	
	/**
	 * Save physical certificate operation.
	 *
	 * @param physicalCertificateOperation the physical certificate operation
	 * @param custodyOperation the custody operation
	 * @param physicalOperationDetails the physical operation details
	 * @return the physical certificate operation
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_REGISTER)
	public PhysicalCertificateOperation savePhysicalCertificateOperation(PhysicalCertificateOperation physicalCertificateOperation, CustodyOperation custodyOperation, 
			List<PhysicalOperationDetail> physicalOperationDetails)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		Long operationNumber = crudDatoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_PHYSICAL_SECURITIES_WITHDRAWAL);
		custodyOperation.setOperationNumber(operationNumber);
		physicalCertificateOperation.setCustodyOperation(custodyOperation);

		physicalCertificateOperation = certificateDepositServiceBean.create(physicalCertificateOperation);
		
		return physicalCertificateOperation;
	}
	
	/**
	 * It gives holder details.
	 *
	 * @param accno Integer type
	 * @param participant Long type
	 * @return the List<Object[]>
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> accountHolderDetailsFacade(Integer accno, Long participant)throws ServiceException {
		return reteirementDepositeService.accountHolderDetailsService(accno, participant);
	}
	
	/**
	 * It gives security class details.
	 *
	 * @param instru the Integer
	 * @param masterfk the Integer
	 * @return the  List<ParameterTable>
	 * @throws ServiceException the service exception
	 */	
	public List<ParameterTable> securityClassFacade(Integer instru,Integer masterfk)throws ServiceException {	
		return reteirementDepositeService.securityClassService(instru,masterfk);
	}
	
	/**
	 * It gives parameter table details for the master fk.
	 *
	 * @param masterfk the masterfk
	 * @return the List<ParameterTable>
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterTableDetailsFacade(Integer masterfk)throws ServiceException{
		return reteirementDepositeService.getParameterTableDetailsService(masterfk);
		}
	
	
	/**
	 * Gets the physical certificate retirement search list.
	 *
	 * @param physicalCertificateFilter the physical certificate filter
	 * @return the physical certificate retirement search list
	 * @throws ServiceException the service exception
	 */
	public List<PhysicalCertificateExt> getPhysicalCertificateRetirementSearchList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		return reteirementDepositeService.getPhysicalCertificateRetirementExtList(physicalCertificateFilter);
	}
	
	/**
	 * Update certificates.
	 *
	 * @param commonUpdateBeanInf the common update bean inf
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateCertificates(CommonUpdateBeanInf commonUpdateBeanInf, LoggerUser loggerUser)throws ServiceException{
		certificateDepositServiceBean.updateCertificates(commonUpdateBeanInf,loggerUser);
	}
	
	/**
	 * It save the authorized data.
	 *
	 * @param commonInfo CommonUpdateBeanInf type
	 * @throws ServiceException the service exception
	 */
	public void authorizarFacade(CommonUpdateBeanInf commonInfo)throws ServiceException {
	//SETTING AUDIT PARAMETERS
	LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAuthorize());
				
	Integer	updateData=reteirementDepositeService.updatePhysicalBalance(commonInfo);
		if (updateData.intValue() > 0) {
				PhysicalBalanceDetail phybalDetail	= reteirementDepositeService.getPhysicalBalance(commonInfo);
				PhysicalCertificateMovement phyCertMov = new PhysicalCertificateMovement();
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(commonInfo.getAccHolderPk());
				phyCertMov.setHolderAccount(holderAccount);
				phyCertMov.setMovementDate(CommonsUtilities.currentDateTime());
				phyCertMov.setPhysicalCertificate(phybalDetail.getPhysicalCertificate());
				phyCertMov.setCurrency(commonInfo.getCurrency());
				phyCertMov.setNominalValue(commonInfo.getNominalValue());
				phyCertMov.setCertificateQuantity(commonInfo.getStockQuantity());
				PhysicalCertificateOperation phyCertOper = new PhysicalCertificateOperation();
				phyCertOper.setIdPhysicalOperationPk(commonInfo.getIdCustodyOperationPk());
				phyCertMov.setPhysicalCertificateOperation(phyCertOper);
				reteirementDepositeService.savePhysicalCertMovementService(phyCertMov);
		}
	}
	
	/**
	 * It gives PhysicalCertificate fetch data.
	 *
	 * @param idPhysicalCertificate PhysicalCertificate type
	 * @return the PhysicalCertificate
	 * @throws ServiceException the service exception
	 */
	public PhysicalCertificate getLastModifiedDateDetails(long idPhysicalCertificate)throws ServiceException{
		return reteirementDepositeService.getLastModifiedDateDetails(idPhysicalCertificate);
	}

	
	/**
	 * Update securities.
	 *
	 * @param security the security
	 * @param bdNewAmount the bd new amount
	 * @throws ServiceException the service exception
	 */
	public void updateSecurities(Security security,BigDecimal bdNewAmount)throws ServiceException{
		Security securityUpdated=reteirementDepositeService.find(Security.class, security.getIdSecurityCodePk());
		securityUpdated.setPhysicalDepositBalance(new BigDecimal(securityUpdated.getPhysicalDepositBalance().intValue()-bdNewAmount.intValue()));
		reteirementDepositeService.update(securityUpdated);
	}
	
	/**
	 * Gets the physical balance.
	 *
	 * @param commonInfo the common info
	 * @return the physical balance
	 * @throws ServiceException the service exception
	 */
	public PhysicalBalanceDetail getPhysicalBalance(CommonUpdateBeanInf commonInfo) throws ServiceException
	{
		return reteirementDepositeService.getPhysicalBalance(commonInfo);
	}
}