package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateMovement;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateExt.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class PhysicalCertificateExt implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The physical certificate. */
	private PhysicalCertificate physicalCertificate;
	
	/** The participant pk. */
	private Long participantPk;
	
	/** The request number. */
	private Long requestNumber;

	/** The operation number. */
	private Long operationNumber;
	
	/** The request state. */
	private Integer requestState;
	
	/** The request state type. */
	private String requestStateType;
	
	/** The state. */
	private Integer state;
	
	/** The state type. */
	private String stateType;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The id physical operation detail. */
	private Long idPhysicalOperationDetail;
	
	/** The id custody operation pk. */
	private Long idCustodyOperationPk;

	/** The physical certificate operation. */
	private PhysicalCertificateOperation physicalCertificateOperation;
	
	/** The physical operation detail. */
	private PhysicalOperationDetail physicalOperationDetail;
	
	/** The physical balance detail. */
	private PhysicalBalanceDetail physicalBalanceDetail;
	
	/** The ind applied. */
	private boolean indApplied;
	
	/** The holder account pk. */
	private Long holderAccountPk;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The ind dematerialization. */
	private boolean indDematerialization;
	
	/** The deposite date. */
	private Date depositeDate;
	
	
	/** The mnemonic. */
	private String mnemonic;
	
	private String mnemonicParticipant;
	private String cui;
	private Integer securityClass;
	
	/**
	 * Instantiates a new physical certificate ext.
	 */
	public PhysicalCertificateExt() {
		physicalCertificate=new PhysicalCertificate();
		physicalCertificate.setPhysicalCertificateMovements(new ArrayList<PhysicalCertificateMovement>(0));
	}
	
	/**
	 * Gets the request state.
	 *
	 * @return the requestState
	 */
	public Integer getRequestState() {
		return requestState;
	}

	/**
	 * Sets the request state.
	 *
	 * @param requestState the requestState to set
	 */
	public void setRequestState(Integer requestState) {
		this.requestState = requestState;
	}

	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public Integer getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	public String getMnemonicParticipant() {
		return mnemonicParticipant;
	}

	public void setMnemonicParticipant(String mnemonicParticipant) {
		this.mnemonicParticipant = mnemonicParticipant;
	}

	/**
	 * Gets the request state type.
	 *
	 * @return the requestStateType
	 */
	public String getRequestStateType() {
		return requestStateType;
	}

	/**
	 * Sets the request state type.
	 *
	 * @param requestStateType the requestStateType to set
	 */
	public void setRequestStateType(String requestStateType) {
		this.requestStateType = requestStateType;
	}

	/**
	 * Gets the state type.
	 *
	 * @return the stateType
	 */
	public String getStateType() {
		return stateType;
	}

	/**
	 * Sets the state type.
	 *
	 * @param stateType the stateType to set
	 */
	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	/**
	 * Gets the mnemonic.
	 *
	 * @return the mnemonic
	 */
	public String getMnemonic() {
		return mnemonic;
	}

	/**
	 * Sets the mnemonic.
	 *
	 * @param mnemonic the mnemonic to set
	 */
	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	/**
	 * Gets the participant pk.
	 *
	 * @return the participantPk
	 */
	public Long getParticipantPk() {
		return participantPk;
	}

	/**
	 * Sets the participant pk.
	 *
	 * @param participantPk the participantPk to set
	 */
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the holder account pk.
	 *
	 * @return the holderAccountPk
	 */
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPk the holderAccountPk to set
	 */
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	/**
	 * Checks if is ind applied.
	 *
	 * @return the indApplied
	 */
	public boolean isIndApplied() {
		return indApplied;
	}

	/**
	 * Gets the ind applied.
	 *
	 * @return the ind applied
	 */
	public boolean getIndApplied() {
		return indApplied;
	}
	
	/**
	 * Sets the ind applied.
	 *
	 * @param indApplied the indApplied to set
	 */
	public void setIndApplied(boolean indApplied) {
		this.indApplied = indApplied;
	}

	/**
	 * Gets the physical operation detail.
	 *
	 * @return the physicalOperationDetail
	 */
	public PhysicalOperationDetail getPhysicalOperationDetail() {
		return physicalOperationDetail;
	}

	/**
	 * Sets the physical operation detail.
	 *
	 * @param physicalOperationDetail the physicalOperationDetail to set
	 */
	public void setPhysicalOperationDetail(
			PhysicalOperationDetail physicalOperationDetail) {
		this.physicalOperationDetail = physicalOperationDetail;
	}

	/**
	 * Gets the id physical operation detail.
	 *
	 * @return the idPhysicalOperationDetail
	 */
	public Long getIdPhysicalOperationDetail() {
		return idPhysicalOperationDetail;
	}

	/**
	 * Sets the id physical operation detail.
	 *
	 * @param idPhysicalOperationDetail the idPhysicalOperationDetail to set
	 */
	public void setIdPhysicalOperationDetail(Long idPhysicalOperationDetail) {
		this.idPhysicalOperationDetail = idPhysicalOperationDetail;
	}

	/**
	 * Gets the physical certificate.
	 *
	 * @return the physicalCertificate
	 */
	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	/**
	 * Sets the physical certificate.
	 *
	 * @param physicalCertificate the physicalCertificate to set
	 */
	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}


	/**
	 * Gets the request number.
	 *
	 * @return the requestNumber
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the id custody operation pk.
	 *
	 * @return the id custody operation pk
	 */
	public Long getIdCustodyOperationPk() {
		return idCustodyOperationPk;
	}

	/**
	 * Sets the id custody operation pk.
	 *
	 * @param idCustodyOperationPk the new id custody operation pk
	 */
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}

	/**
	 * Gets the physical certificate operation.
	 *
	 * @return the physical certificate operation
	 */
	public PhysicalCertificateOperation getPhysicalCertificateOperation() {
		return physicalCertificateOperation;
	}

	/**
	 * Sets the physical certificate operation.
	 *
	 * @param physicalCertificateOperation the new physical certificate operation
	 */
	public void setPhysicalCertificateOperation(
			PhysicalCertificateOperation physicalCertificateOperation) {
		this.physicalCertificateOperation = physicalCertificateOperation;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Checks if is ind dematerialization.
	 *
	 * @return true, if is ind dematerialization
	 */
	public boolean isIndDematerialization() {
		return indDematerialization;
	}

	/**
	 * Sets the ind dematerialization.
	 *
	 * @param indDematerialization the new ind dematerialization
	 */
	public void setIndDematerialization(boolean indDematerialization) {
		this.indDematerialization = indDematerialization;
	}

	/**
	 * Gets the physical balance detail.
	 *
	 * @return the physical balance detail
	 */
	public PhysicalBalanceDetail getPhysicalBalanceDetail() {
		return physicalBalanceDetail;
	}

	/**
	 * Sets the physical balance detail.
	 *
	 * @param physicalBalanceDetail the new physical balance detail
	 */
	public void setPhysicalBalanceDetail(PhysicalBalanceDetail physicalBalanceDetail) {
		this.physicalBalanceDetail = physicalBalanceDetail;
	}
	
	/**
	 * Gets the deposite date.
	 *
	 * @return the depositeDate
	 */
	public Date getDepositeDate() {
		return depositeDate;
	}

	/**
	 * Sets the deposite date.
	 *
	 * @param depositeDate the depositeDate to set
	 */
	public void setDepositeDate(Date depositeDate) {
		this.depositeDate = depositeDate;
	}
	
}
