package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateAndDetailsResultTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Nov 17, 2013
 */
public class PhysicalCertificateAndDetailsResultTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6509733311059672538L;
	
	/** The id physical certificate pk. */
	private BigDecimal idPhysicalCertificatePK;
	
	/** The cui. */
	private String cui; 
	
	/** The cui desc. */
	private String cuiDesc;
	
	/** The account number. */
	private BigDecimal accountNumber;
	
	/** The confirm date. */
	private Date confirmDate;
	
	/** The admissiondate. */
	private Date admissiondate;
	
	/** The register date. */
	private Date registerDate;
	
	/** The security. */
	private Security security;
	
	/** The title number. */
	private BigDecimal titleNumber;
	
	/** The desc situasion. */
	private String descSituasion;
	
	/** The cetificate quantity. */
	private BigDecimal cetificateQuantity;
	
	/** The market fact rate. */
	private BigDecimal marketFactRate;
	
	/** The market fact date. */
	private Date marketFactDate;
	
	/** The market fact price. */
	private BigDecimal marketFactPrice;
	
	/** The valued amount. */
	private BigDecimal valuedAmount;
	
	/** The part desc. */
	private String partDesc;
	
	/** The issuer. */
	//para el dialgo del detalle
	private Issuer issuer;
	
	/** The certificate. */
	private PhysicalCertificate certificate;
	
	/**
	 * Instantiates a new physical certificate and details result to.
	 */
	public PhysicalCertificateAndDetailsResultTO() {
		security=new Security();
		certificate=new PhysicalCertificate();
	}

	/**
	 * Gets the cui.
	 *
	 * @return the cui
	 */
	public String getCui() {
		return cui;
	}

	/**
	 * Sets the cui.
	 *
	 * @param cui the new cui
	 */
	public void setCui(String cui) {
		this.cui = cui;
	}


	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public BigDecimal getAccountNumber() {
		return accountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(BigDecimal accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}
	
	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	
	/**
	 * Gets the admissiondate.
	 *
	 * @return the admissiondate
	 */
	public Date getAdmissiondate() {
		return admissiondate;
	}
	
	/**
	 * Sets the admissiondate.
	 *
	 * @param admissiondate the new admissiondate
	 */
	public void setAdmissiondate(Date admissiondate) {
		this.admissiondate = admissiondate;
	}
	
	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}
	
	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}
	

	/**
	 * Gets the title number.
	 *
	 * @return the title number
	 */
	public BigDecimal getTitleNumber() {
		return titleNumber;
	}

	/**
	 * Sets the title number.
	 *
	 * @param titleNumber the new title number
	 */
	public void setTitleNumber(BigDecimal titleNumber) {
		this.titleNumber = titleNumber;
	}

	/**
	 * Gets the desc situasion.
	 *
	 * @return the desc situasion
	 */
	public String getDescSituasion() {
		return descSituasion;
	}
	
	/**
	 * Sets the desc situasion.
	 *
	 * @param descSituasion the new desc situasion
	 */
	public void setDescSituasion(String descSituasion) {
		this.descSituasion = descSituasion;
	}
	
	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	
	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	/**
	 * Gets the cetificate quantity.
	 *
	 * @return the cetificate quantity
	 */
	public BigDecimal getCetificateQuantity() {
		return cetificateQuantity;
	}
	
	/**
	 * Sets the cetificate quantity.
	 *
	 * @param cetificateQuantity the new cetificate quantity
	 */
	public void setCetificateQuantity(BigDecimal cetificateQuantity) {
		this.cetificateQuantity = cetificateQuantity;
	}
	
	/**
	 * Gets the market fact rate.
	 *
	 * @return the market fact rate
	 */
	public BigDecimal getMarketFactRate() {
		return marketFactRate;
	}
	
	/**
	 * Sets the market fact rate.
	 *
	 * @param marketFactRate the new market fact rate
	 */
	public void setMarketFactRate(BigDecimal marketFactRate) {
		this.marketFactRate = marketFactRate;
	}
	
	/**
	 * Gets the market fact date.
	 *
	 * @return the market fact date
	 */
	public Date getMarketFactDate() {
		return marketFactDate;
	}
	
	/**
	 * Sets the market fact date.
	 *
	 * @param marketFactDate the new market fact date
	 */
	public void setMarketFactDate(Date marketFactDate) {
		this.marketFactDate = marketFactDate;
	}
	
	/**
	 * Gets the market fact price.
	 *
	 * @return the market fact price
	 */
	public BigDecimal getMarketFactPrice() {
		return marketFactPrice;
	}
	
	/**
	 * Sets the market fact price.
	 *
	 * @param marketFactPrice the new market fact price
	 */
	public void setMarketFactPrice(BigDecimal marketFactPrice) {
		this.marketFactPrice = marketFactPrice;
	}
	
	/**
	 * Gets the valued amount.
	 *
	 * @return the valued amount
	 */
	public BigDecimal getValuedAmount() {
		return valuedAmount;
	}
	
	/**
	 * Sets the valued amount.
	 *
	 * @param valuedAmount the new valued amount
	 */
	public void setValuedAmount(BigDecimal valuedAmount) {
		this.valuedAmount = valuedAmount;
	}
	
	/**
	 * Gets the id physical certificate pk.
	 *
	 * @return the id physical certificate pk
	 */
	public BigDecimal getIdPhysicalCertificatePK() {
		return idPhysicalCertificatePK;
	}
	
	/**
	 * Sets the id physical certificate pk.
	 *
	 * @param idPhysicalCertificatePK the new id physical certificate pk
	 */
	public void setIdPhysicalCertificatePK(BigDecimal idPhysicalCertificatePK) {
		this.idPhysicalCertificatePK = idPhysicalCertificatePK;
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}
	
	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	
	/**
	 * Gets the certificate.
	 *
	 * @return the certificate
	 */
	public PhysicalCertificate getCertificate() {
		return certificate;
	}
	
	/**
	 * Sets the certificate.
	 *
	 * @param certificate the new certificate
	 */
	public void setCertificate(PhysicalCertificate certificate) {
		this.certificate = certificate;
	}

	/**
	 * Gets the part desc.
	 *
	 * @return the part desc
	 */
	public String getPartDesc() {
		return partDesc;
	}

	/**
	 * Sets the part desc.
	 *
	 * @param partDesc the new part desc
	 */
	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}

	/**
	 * Gets the cui desc.
	 *
	 * @return the cui desc
	 */
	public String getCuiDesc() {
		return cuiDesc;
	}

	/**
	 * Sets the cui desc.
	 *
	 * @param cuiDesc the new cui desc
	 */
	public void setCuiDesc(String cuiDesc) {
		this.cuiDesc = cuiDesc;
	}
}
