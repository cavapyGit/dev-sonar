package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateFilter.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class PhysicalCertificateFilter implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The status. */
	private Integer status;
	
	/** The participant pk. */
	private Long participantPk;
	
	/** The cui holder. */
	private Long cuiHolder;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder account pk. */
	private Long holderAccountPk;
	
	/** The issuer pk. */
	private String issuerPk;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The certificate number. */
	private Long certificateNumber;
	
	/** The request type. */
	private Long requestType;
	
	/** The security location. */
	private Long securityLocation;
	
	/** The security number. */
	private Long securityNumber;
	
	/** The holder name. */
	private String holderName;
	
	/** The date deposit. */
	private String dateDeposit;
	
	/** The currency. */
	private Integer currency;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The id physical certificate pk. */
	private long idPhysicalCertificatePk;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The sequence. */
	private long sequence;
	
	/** The issue date. */
	private String issueDate;
	
	/** The expiration date. */
	private String expirationDate;
	
	/** The payment periodicity. */
	private Integer paymentPeriodicity;
	
	/** The class type. */
	private Integer classType;
	
	/** The holder list. */
	private List<String> holderList;
	
	/** The str class type. */
	private String strClassType;

	/** The issuer name. */
	private String issuerName;

	/** The issuer nemonic. */
	private String issuerNemonic;
	
	/** The key value. */
	private String keyValue;
	
	/** The security. */
	private Security security;

	//private List<ParameterTable> securityClassList = new ArrayList<ParameterTable>();
	
	/** The certificate location. */
	private Integer	certificateLocation;

	/**
	 * Instantiates a new physical certificate filter.
	 */
	public PhysicalCertificateFilter() {
		participantPk=null;
		security=null;
		holder=null;
	}
	/**
	 * Gets the class type.
	 *
	 * @return the classType
	 */
	public Integer getClassType() {
		return classType;
	}

	/**
	 * Sets the class type.
	 *
	 * @param classType the classType to set
	 */
	public void setClassType(Integer classType) {
		this.classType = classType;
	}

	/**
	 * Gets the str class type.
	 *
	 * @return the strClassType
	 */
	public String getStrClassType() {
		return strClassType;
	}

	/**
	 * Sets the str class type.
	 *
	 * @param strClassType the strClassType to set
	 */
	public void setStrClassType(String strClassType) {
		this.strClassType = strClassType;
	}

	/**
	 * Gets the payment periodicity.
	 *
	 * @return the paymentPeriodicity
	 */
	public Integer getPaymentPeriodicity() {
		return paymentPeriodicity;
	}

	/**
	 * Sets the payment periodicity.
	 *
	 * @param paymentPeriodicity the paymentPeriodicity to set
	 */
	public void setPaymentPeriodicity(Integer paymentPeriodicity) {
		this.paymentPeriodicity = paymentPeriodicity;
	}

	/**
	 * Gets the issue date.
	 *
	 * @return the issueDate
	 */
	public String getIssueDate() {
		return issueDate;
	}

	/**
	 * Sets the issue date.
	 *
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the sequence.
	 *
	 * @return the sequence
	 */
	public long getSequence() {
		return sequence;
	}

	/**
	 * Sets the sequence.
	 *
	 * @param sequence the sequence to set
	 */
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 * 
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the id physical certificate pk.
	 *
	 * @return the idPhysicalCertificatePk
	 */
	public long getIdPhysicalCertificatePk() {
		return idPhysicalCertificatePk;
	}

	/**
	 * Sets the id physical certificate pk.
	 *
	 * @param idPhysicalCertificatePk the idPhysicalCertificatePk to set
	 */
	public void setIdPhysicalCertificatePk(long idPhysicalCertificatePk) {
		this.idPhysicalCertificatePk = idPhysicalCertificatePk;
	}
	
	/**
	 * Gets the instrument type.
	 *
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the date deposit.
	 *
	 * @return the dateDeposit
	 */
	public String getDateDeposit() {
		return dateDeposit;
	}

	/**
	 * Sets the date deposit.
	 *
	 * @param dateDeposit the dateDeposit to set
	 */
	public void setDateDeposit(String dateDeposit) {
		this.dateDeposit = dateDeposit;
	}

	/**
	 * Gets the holder name.
	 *
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * Sets the holder name.
	 *
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the requestType
	 */
	public Long getRequestType() {
		return requestType;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the requestType to set
	 */
	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}

	/**
	 * Gets the security location.
	 *
	 * @return the securityLocation
	 */
	public Long getSecurityLocation() {
		return securityLocation;
	}

	/**
	 * Sets the security location.
	 *
	 * @param securityLocation the securityLocation to set
	 */
	public void setSecurityLocation(Long securityLocation) {
		this.securityLocation = securityLocation;
	}

	/**
	 * Gets the security number.
	 *
	 * @return the securityNumber
	 */
	public Long getSecurityNumber() {
		return securityNumber;
	}

	/**
	 * Sets the security number.
	 *
	 * @param securityNumber the securityNumber to set
	 */
	public void setSecurityNumber(Long securityNumber) {
		this.securityNumber = securityNumber;
	}

	/**
	 * Gets the certificate number.
	 *
	 * @return the certificateNumber
	 */
	public Long getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the certificateNumber to set
	 */
	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the participant pk.
	 *
	 * @return the participantPk
	 */
	public Long getParticipantPk() {
		return participantPk;
	}

	/**
	 * Sets the participant pk.
	 *
	 * @param participantPk the participantPk to set
	 */
	public void setParticipantPk(Long participantPk) {
		this.participantPk = participantPk;
	}

	/**
	 * Gets the holder account pk.
	 *
	 * @return the holderAccountPk
	 */
	public Long getHolderAccountPk() {
		return holderAccountPk;
	}

	/**
	 * Sets the holder account pk.
	 *
	 * @param holderAccountPk the holderAccountPk to set
	 */
	public void setHolderAccountPk(Long holderAccountPk) {
		this.holderAccountPk = holderAccountPk;
	}

	/**
	 * Gets the issuer pk.
	 *
	 * @return the issuerPk
	 */
	public String getIssuerPk() {
		return issuerPk;
	}

	/**
	 * Sets the issuer pk.
	 *
	 * @param issuerPk the issuerPk to set
	 */
	public void setIssuerPk(String issuerPk) {
		this.issuerPk = issuerPk;
	}

	/**
	 * Gets the request number.
	 *
	 * @return the requestNumber
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber the requestNumber to set
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}
	
	/**
	 * Gets the holder list.
	 *
	 * @return the holder list
	 */
	public List<String> getHolderList() {
		return holderList;
	}

	/**
	 * Sets the holder list.
	 *
	 * @param holderList the new holder list
	 */
	public void setHolderList(List<String> holderList) {
		this.holderList = holderList;
	}

	/**
	 * Gets the issuer name.
	 *
	 * @return the issuer name
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * Sets the issuer name.
	 *
	 * @param issuerName the new issuer name
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	/**
	 * Gets the issuer nemonic.
	 *
	 * @return the issuer nemonic
	 */
	public String getIssuerNemonic() {
		return issuerNemonic;
	}

	/**
	 * Sets the issuer nemonic.
	 *
	 * @param issuerNemonic the new issuer nemonic
	 */
	public void setIssuerNemonic(String issuerNemonic) {
		this.issuerNemonic = issuerNemonic;
	}

	/**
	 * Gets the cui holder.
	 *
	 * @return the cui holder
	 */
	public Long getCuiHolder() {
		return cuiHolder;
	}

	/**
	 * Sets the cui holder.
	 *
	 * @param cuiHolder the new cui holder
	 */
	public void setCuiHolder(Long cuiHolder) {
		this.cuiHolder = cuiHolder;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

//	public List<ParameterTable> getSecurityClassList() {
//		return securityClassList;
//	}
//
//	public void setSecurityClassList(List<ParameterTable> securityClassList) {
//		this.securityClassList = securityClassList;
/**
 * Gets the key value.
 *
 * @return the key value
 */
//	}
	public String getKeyValue() {
		return keyValue;
	}
	
	/**
	 * Sets the key value.
	 *
	 * @param keyValue the new key value
	 */
	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}
	
	/**
	 * Gets the certificate location.
	 *
	 * @return the certificate location
	 */
	public Integer getCertificateLocation() {
		return certificateLocation;
	}
	
	/**
	 * Sets the certificate location.
	 *
	 * @param certificateLocation the new certificate location
	 */
	public void setCertificateLocation(Integer certificateLocation) {
		this.certificateLocation = certificateLocation;
	}
	
	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}
	
	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	
}