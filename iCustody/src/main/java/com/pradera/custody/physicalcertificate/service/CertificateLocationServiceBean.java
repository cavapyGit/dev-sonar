package com.pradera.custody.physicalcertificate.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateLocationInputTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.physical.CertificateSituationHistory;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class CertificateLocationServiceBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCustody
 * @Creation_Date :
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CertificateLocationServiceBean {
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;
	
	/** The em. */
	@Inject @DepositaryDataBase
	protected EntityManager em;
	
	/**
	 * The method for getting the security locations.
	 *
	 * @param id Long
	 * @return List of ParameterTable objects
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getSecurityLocationService(Long id)throws ServiceException{ 
			String sql = "Select parameterTable from ParameterTable parameterTable "
						+ " where parameterTable.masterTable.masterTablePk = :id "
						+ " and parameterTable.parameterState = :indicatorOne ";
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("id", id.intValue());
			parameters.put("indicatorOne", BooleanType.YES.getCode());
			return (List<ParameterTable>)crudDaoServiceBean.findListByQueryString(sql, parameters);
		}
	
	/**
	 * Used to get Search List
	 *  
	 *
	 * @param physicalCertificateFilter PhysicalCertificateFilter
	 * @return List of Object[]
	 * @throws ServiceException the service exception
	 */
	
	// CREATED BY MARTIN ZARATE RAFAEL
	public List<Object[]> getPhysicalCertificateListObjService(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException {
		StringBuffer sb = new StringBuffer();
		
		sb.append(" SELECT 																 " +
				  "		DISTINCT														 " +
				  "			PCO.PHYSICAL_OPERATION_TYPE ,								 " +
				  "			POD.ID_PHYSICAL_OPERATION_FK ,								 " +
				  "			PBD.DEPOSIT_DATE ,											 " +
				  "			PC.ID_ISSUER_FK ,											 " +
				  "			PC.CERTIFICATE_NUMBER,										 " +
				  "			HA.ACCOUNT_NUMBER ,											 " +
				  "			HA.ID_HOLDER_ACCOUNT_PK	,									 " +
				  "			POD.STATE ,	 												 " +
				  "			PC.SITUATION ,												 " +
				  "			PC.ID_PHYSICAL_CERTIFICATE_PK ,								 " +
				  "			PC.LAST_MODIFY_DATE ,										 " +
				  "			PC.SECURITY_CLASS ,											 " +
				  "			POD.REGISTRY_DATE,											 " +
				  "			ISS.MNEMONIC,												 " +
				  "			ISS.BUSINESS_NAME											 " +
				  "	FROM																 " +
				  "			PHYSICAL_CERTIFICATE PC,									 " +
				  "			PHYSICAL_CERTIFICATE_OPERATION PCO,							 " +
				  "	     	PHYSICAL_OPERATION_DETAIL POD,								 " +
				  "	      	PHYSICAL_BALANCE_DETAIL PBD,		 					   	 " +
				  "	      	HOLDER_ACCOUNT HA,											 " +
				  "	      	HOLDER_ACCOUNT_DETAIL HAD,									 " +
				  "			ISSUER ISS													 " +
				  "	WHERE																 " +
				  "			PBD.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK " +
			      "	AND 	PCO.ID_PARTICIPANT_FK=PBD.ID_PARTICIPANT_FK 				 " +
			      "	AND 	HA.ID_HOLDER_ACCOUNT_PK=PCO.ID_HOLDER_ACCOUNT_FK 			 " +
			      "	AND 	PC.ID_PHYSICAL_CERTIFICATE_PK=POD.ID_PHYSICAL_CERTIFICATE_FK " +
			      "	AND 	PCO.ID_PHYSICAL_OPERATION_PK=POD.ID_PHYSICAL_OPERATION_FK 	 " +
			      "	AND 	HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK 			 " +
			      " AND 	ISS.id_issuer_pk = PC.ID_ISSUER_FK							 ");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		if(Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getParticipantPk())){
			sb.append(" AND PCO.ID_PARTICIPANT_FK = :idParticipantFk ");
			parameters.put("idParticipantFk",physicalCertificateFilter.getParticipantPk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getHolderAccountPk())&& (physicalCertificateFilter.getHolderAccountPk()!=0)) {
			sb.append(" AND HA.ACCOUNT_NUMBER = :idHolderAccountFk ");
			parameters.put("idHolderAccountFk", physicalCertificateFilter.getHolderAccountPk());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getIssuerPk())) {
			sb.append(" AND PC.ID_ISSUER_FK = :idIssuerFk ");
			parameters.put("idIssuerFk", physicalCertificateFilter.getIssuerPk().toUpperCase());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getCertificateNumber())&&(physicalCertificateFilter.getCertificateNumber()!=0)) {
			sb.append(" AND PC.CERTIFICATE_NUMBER = :certificateNumber ");
			parameters.put("certificateNumber", physicalCertificateFilter.getCertificateNumber());
		}

		if (Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getSecurityLocation())) {
			sb.append(" AND PC.SITUATION = :situation ");
			parameters.put("situation", physicalCertificateFilter.getSecurityLocation());
		}

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getRequestType())&& (physicalCertificateFilter.getRequestType()!=-1)) {
			sb.append(" and PCO.PHYSICAL_OPERATION_TYPE = :operationType ");
			parameters.put("operationType", physicalCertificateFilter.getRequestType());
		}

		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getRequestNumber())&&(physicalCertificateFilter.getRequestNumber()!=0)) {
			sb.append(" and PCO.ID_PHYSICAL_OPERATION_PK = :operationNumber ");
			parameters.put("operationNumber",physicalCertificateFilter.getRequestNumber());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getStatus())&&(physicalCertificateFilter.getStatus()!=-1)) {
			sb.append(" AND POD.STATE = :state ");
			parameters.put("state", physicalCertificateFilter.getStatus());
		}
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getInitialDate())){
			sb.append(" AND trunc(PBD.DEPOSIT_DATE) >= :startDepositeDate ");
			parameters.put("startDepositeDate", physicalCertificateFilter.getInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateFilter.getFinalDate())){
			sb.append(" AND trunc(PBD.DEPOSIT_DATE) <= :endDepositeDate ");
			parameters.put("endDepositeDate", physicalCertificateFilter.getFinalDate());
		}
		
		sb.append(" ORDER BY POD.REGISTRY_DATE desc");
		
		List<Object[]> lstPhyCertificates = crudDaoServiceBean.findByNativeQuery(sb.toString(), parameters);
		return lstPhyCertificates;
		
	}

	/**
	 * Search physical certificate location.
	 *
	 * @param physicalCertificateLocationInputTO the physical certificate location input to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> searchPhysicalCertificateLocation(PhysicalCertificateLocationInputTO physicalCertificateLocationInputTO)throws ServiceException {
		StringBuffer sb = new StringBuffer();
		
		sb.append(" SELECT 																 " +
				  "		DISTINCT														 " +
				  "			PCO.PHYSICAL_OPERATION_TYPE ,								 " +
				  "			CO.OPERATION_NUMBER ,								 		 " +
				  "			PBD.DEPOSIT_DATE ,											 " +
				  "			PC.ID_ISSUER_FK ,											 " +
				  "			PC.CERTIFICATE_NUMBER,										 " +
				  "			HA.ACCOUNT_NUMBER ,											 " +
				  "			HA.ID_HOLDER_ACCOUNT_PK	,									 " +
				  "			POD.STATE ,	 												 " +
				  "			PC.SITUATION ,												 " +
				  "			PC.ID_PHYSICAL_CERTIFICATE_PK ,								 " +
				  "			PC.LAST_MODIFY_DATE ,										 " +
				  "			PC.SECURITY_CLASS ,											 " +
				  "			POD.REGISTRY_DATE,											 " +
				  "			ISS.MNEMONIC,												 " +
				  "			ISS.BUSINESS_NAME,											 " +
				  "	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = POD.STATE), " +//DESC1
				  "	(SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = PC.SITUATION), " +//DESC2
				  "	(SELECT TEXT1 FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK = PC.SECURITY_CLASS), " +//DESC3
				  //"	(SELECT MNEMONIC||' - '||ID_PARTICIPANT_PK||' '||DESCRIPTION FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = PCO.ID_PARTICIPANT_FK), " +//PARTICIPANTE
				  "	(SELECT MNEMONIC||' - '||ID_PARTICIPANT_PK FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = PCO.ID_PARTICIPANT_FK), " +//PARTICIPANTE
				  "			PC.ID_SECURITY_CODE_FK,										 " +
				  "			PCO.ID_PARTICIPANT_FK									 " +
				  "	FROM																 " +
				  "			PHYSICAL_CERTIFICATE PC,									 " +
				  "			PHYSICAL_CERTIFICATE_OPERATION PCO,							 " +
				  "	     	PHYSICAL_OPERATION_DETAIL POD,								 " +
				  "	      	PHYSICAL_BALANCE_DETAIL PBD,		 					   	 " +
				  "	      	HOLDER_ACCOUNT HA,											 " +
				  "	      	HOLDER_ACCOUNT_DETAIL HAD,									 " +
				  "			ISSUER ISS,													 " +
				  "			CUSTODY_OPERATION CO										 " +
				  "	WHERE																 " +
				  "			PBD.ID_PHYSICAL_CERTIFICATE_FK=PC.ID_PHYSICAL_CERTIFICATE_PK " +
			      "	AND 	CO.ID_CUSTODY_OPERATION_PK=PCO.ID_PHYSICAL_OPERATION_PK 	 " +
			      "	AND 	PCO.ID_PARTICIPANT_FK=PBD.ID_PARTICIPANT_FK 				 " +
			      "	AND 	HA.ID_HOLDER_ACCOUNT_PK=PCO.ID_HOLDER_ACCOUNT_FK 			 " +
			      "	AND 	PC.ID_PHYSICAL_CERTIFICATE_PK=POD.ID_PHYSICAL_CERTIFICATE_FK " +
			      "	AND 	PCO.ID_PHYSICAL_OPERATION_PK=POD.ID_PHYSICAL_OPERATION_FK 	 " +
			      "	AND 	HAD.ID_HOLDER_ACCOUNT_FK=HA.ID_HOLDER_ACCOUNT_PK 			 " +
			      " AND 	ISS.id_issuer_pk = PC.ID_ISSUER_FK							 ");
				  //" AND 	POD.STATE NOT IN (602)						 "); //AUTORIZADA
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		if( Validations.validateIsNotNull(physicalCertificateLocationInputTO.getSecurityHelp()) &&
			Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getSecurityHelp().getSecurityClass())){
			sb.append(" AND PC.SECURITY_CLASS = :securityClass ");
			parameters.put("securityClass",physicalCertificateLocationInputTO.getSecurityHelp().getSecurityClass());
		}

		if( Validations.validateIsNotNull(physicalCertificateLocationInputTO.getSecurityHelp()) &&
			Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getSecurityHelp().getIdSecurityCodePk())){
			sb.append(" AND PC.ID_SECURITY_CODE_FK = :idSecurityCode ");
			parameters.put("idSecurityCode",physicalCertificateLocationInputTO.getSecurityHelp().getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getCuiHolder())){
			sb.append(" AND HAD.ID_HOLDER_FK = :cuiHolder ");
			parameters.put("cuiHolder",physicalCertificateLocationInputTO.getCuiHolder());
		}
		
		if(Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getIdParticipantPk())){
			sb.append(" AND PCO.ID_PARTICIPANT_FK = :idParticipantFk ");
			parameters.put("idParticipantFk",physicalCertificateLocationInputTO.getIdParticipantPk());
		}
		if (Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getIdHolderAccountPk())) {
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK = :idHolderAccountPk ");
			parameters.put("idHolderAccountPk", physicalCertificateLocationInputTO.getIdHolderAccountPk());
		}
		if (Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getAccountNumber())) {
			sb.append(" AND HA.ACCOUNT_NUMBER = :idAccountNumber ");
			parameters.put("idAccountNumber", physicalCertificateLocationInputTO.getAccountNumber());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getIssuerCode())) {
			sb.append(" AND PC.ID_ISSUER_FK = :idIssuerFk ");
			parameters.put("idIssuerFk", physicalCertificateLocationInputTO.getIssuerCode());
		}
		if (Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getCertificateNumber())) {
			sb.append(" AND PC.CERTIFICATE_NUMBER = :certificateNumber ");
			parameters.put("certificateNumber", physicalCertificateLocationInputTO.getCertificateNumber());
		}

		if (Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getCertificateLocation())) {
			sb.append(" AND PC.SITUATION = :situation ");
			parameters.put("situation", physicalCertificateLocationInputTO.getCertificateLocation());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getRequestType())) {
			sb.append(" and PCO.PHYSICAL_OPERATION_TYPE = :requestType ");
			parameters.put("requestType",physicalCertificateLocationInputTO.getRequestType());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getRequestNumber())) {
			sb.append(" and CO.OPERATION_NUMBER = :operationNumber ");
			parameters.put("operationNumber",physicalCertificateLocationInputTO.getRequestNumber());
		}
		if (Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getCertificateStatus())) {
			sb.append(" AND POD.STATE = :state ");
			parameters.put("state", physicalCertificateLocationInputTO.getCertificateStatus());
		}
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getDepositInitialDate())){
			sb.append(" AND trunc(PBD.DEPOSIT_DATE) >= :startDepositeDate ");
			parameters.put("startDepositeDate", physicalCertificateLocationInputTO.getDepositInitialDate());
		}
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateLocationInputTO.getDepositFinalDate())){
			sb.append(" AND trunc(PBD.DEPOSIT_DATE) <= :endDepositeDate ");
			parameters.put("endDepositeDate", physicalCertificateLocationInputTO.getDepositFinalDate());
		}
		
		sb.append(" ORDER BY POD.REGISTRY_DATE desc");
		
		List<Object[]> lstPhyCertificates = crudDaoServiceBean.findByNativeQuery(sb.toString(), parameters);
		return lstPhyCertificates;
		
	}
	
	/**
	 * Used to get Physical Certificate for the id .
	 *
	 * @param history the history
	 * @return  the PhysicalCertificate entity
	 * @throws ServiceException the service exception
	 */
//	public PhysicalCertificate getPhysicalCertificateDetailsByIdService(Long idPhysicalCertificatePk)throws ServiceException {
//		StringBuffer query = new StringBuffer(
//				"Select  pc from PhysicalCertificate pc where pc.idPhysicalCertificatePk=:idPhysicalCertificatePk");
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("idPhysicalCertificatePk", idPhysicalCertificatePk);z
//		PhysicalCertificate certificate=(PhysicalCertificate) crudDaoServiceBean.findObjectByQueryString(query.toString(), parameters);
//		return certificate;
//	}

	/**
	 * Method for insert the record into the database when updating the location
	 * of the physical security
	 * 
	 * @param history
	 *            CertificateSituationHistory
	 * @return the object of CertificateSituationHistory entity
	 */
	public CertificateSituationHistory insertModifiedLocationService(CertificateSituationHistory history)throws ServiceException {
		return crudDaoServiceBean.create(history);

	}
	
	/**
	 * Method for updating the location of the Physical certificate.
	 *
	 * @param certificate PhysicalCertificate
	 * @param confirmed the confirmed
	 * @param loggerUser the logger user
	 * @return the object of PhysicalCertificate entity
	 * @throws ServiceException the service exception
	 */
	public Object updateModifiedLocationService(PhysicalCertificate certificate,boolean confirmed,LoggerUser loggerUser)throws ServiceException{
		StringBuffer query = new StringBuffer(
				"update  PhysicalCertificate pc set pc.situation =:situation," +
				" pc.lastModifyUser = :lastModifyUser , pc.lastModifyDate = :lastModifyDate , pc.lastModifyIp = :lastModifyIp , pc.lastModifyApp = :lastModifyApp");
		Map<String, Object> parameters = new HashMap<String, Object>();
		if(confirmed){
			query.append(",pc.state = :state");
			parameters.put("state", StateType.CONFIRMED.getCode());
		}
		query.append(" where pc.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
		parameters.put("situation", certificate.getSituation());
		parameters.put("lastModifyUser", loggerUser.getUserName());
		parameters.put("lastModifyDate", loggerUser.getAuditTime());
		parameters.put("lastModifyIp", loggerUser.getIpAddress());
		parameters.put("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("idPhysicalCertificatePk", certificate.getIdPhysicalCertificatePk());
		return crudDaoServiceBean.updateByQuery(query.toString(), parameters);
	}
	
		/**
		 * The method for getting the state of the physical certificate .
		 *
		 * @param cert PhysicalCertificate
		 * @return  BigDecimal value
		 * @throws ServiceException the service exception
		 */
		public Long  getCertificateStateService(PhysicalCertificate cert)throws ServiceException{
			Map<String,Object> parameters = new HashMap<String, Object>();
			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append("select pod.state from PhysicalOperationDetail pod ");
			sbQuery.append(" where pod.physicalCertificate.idPhysicalCertificatePk=:idPhysicalCertificatePk");
			sbQuery.append(" and pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk=:idCustodyOperationPk ");
			parameters.put("idPhysicalCertificatePk", cert.getIdPhysicalCertificatePk());
			parameters.put("idCustodyOperationPk", cert.getRequestNumber());
	    	return (Long)crudDaoServiceBean.findObjectByQueryString(sbQuery.toString(), parameters);
		}
		
		/**
		 * Method for getting the Last Modified  date .
		 *
		 * @param idPhysicalCertificate Long
		 * @return the PhysicalCertificate entity object
		 * @throws ServiceException the service exception
		 */
		public PhysicalCertificate getLastModifiedDateDetailsService(Long idPhysicalCertificate)throws ServiceException{
			Map<String,Object> parameters = new HashMap<String, Object>();
			StringBuffer sbQuery = new StringBuffer(
					"Select pc from PhysicalCertificate pc join fetch pc.physicalCertMarketfacts where pc.idPhysicalCertificatePk=:idPhysicalCertificatePk");
			parameters.put("idPhysicalCertificatePk", idPhysicalCertificate.longValue());
			return (PhysicalCertificate)crudDaoServiceBean.findObjectByQueryString(sbQuery.toString(), parameters);
		}
}
