package com.pradera.custody.physicalcertificate.view;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.security.model.type.LogoutMotiveType;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.AccountQueryServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.facade.CertificateLocationServiceFacade;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateLocationInputTO;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateLocationOutputTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.custody.vaultcertificates.facade.CertificateVaultFacade;
import com.pradera.custody.vaultcertificates.view.SearchCertificateVaultTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.CertificateSituationHistory;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright ArkinSoftware 2014.</li>
 * </ul>
 * 
 * The Class CertificateLocationBean.
 *
 * @author ArkinSoftware.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class CertificateLocationBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The holder account result helper to. */
	private HolderAccountHelperResultTO	 holderAccountResultHelperTO;
	
	/** The issuer to. */
	private IssuerTO issuerTO;

	private Issuer issuerHelp = new Issuer();
	
	/** The physical certificate location input to. */
	private PhysicalCertificateLocationInputTO physicalCertificateLocationInputTO;
	
	/** The physical cert location output selected. */
	//private PhysicalCertificateLocationOutputTO physicalCertLocationOutputSelected;
	
	private PhysicalCertificateLocationOutputTO[] physicalCertListSel;
	
	/** The min date. */
	private Date minDate;
	
	/** The max date. */
	private Date maxDate;

	/** The row is selected. */
	private boolean rowIsSelected;
	
	/** The list empty. */
	private boolean listEmpty;
	
	/** The disabled. */
	private boolean disabled;
	
	/** The btn save. */
	private boolean btnSave;
	
	/** The list participants. */
	private List<SelectItem> listParticipants;
	
	/** The lst security states. */
	private List<ParameterTable> lstSecurityStates;
	
	/** The lst request type. */
	private List<ParameterTable> lstRequestType ;
	
	/** The lst security locations. */
	private List<ParameterTable> lstSecurityLocations;
	
	/** The certificate location output t os. */
	private List<PhysicalCertificateLocationOutputTO> certificateLocationOutputTOs;

	private String confirmAlertAction="";
	
	/** The lst new security locations. */
	private List<ParameterTable> lstNewSecurityLocations;
	private List<ParameterTable> lstSecurityClass;
	private List<ParameterTable> lstInstrumentType;
	private List<ParameterTable> lstPaymentType;
	private List<ParameterTable> lstMoneyType;
	private List<ParameterTable> lstPayrollTypesSearch;
	
	/*Mapa de situaciones*/
	private Map<Integer, ParameterTable> mapLocations = new HashMap<Integer, ParameterTable>();
	private Map<Integer, ParameterTable> mapSecurityClass = new HashMap<Integer, ParameterTable>();
	private Map<Integer, ParameterTable> mapInstrumentType = new HashMap<Integer, ParameterTable>();
	private Map<Integer, ParameterTable> paymentTypeMap = new HashMap<Integer,ParameterTable>();
	private Map<Integer,ParameterTable> moneyTypeMap= new HashMap<Integer,ParameterTable>();
	
	
	/** The physical certificate. */
	private PhysicalCertificate physicalCertificate;
	
	/** The str participant selected. */
	private String strParticipantSelected;
	
	/** The new location selected. */
	private Integer newLocationSelected;
	
	/** The physical certificate output data model. */
	private PhysicalCertificateOutputDataModel physicalCertificateOutputDataModel;

	/** The flag search. */
	private boolean flagSearch = false;
	
	/** The is bcr. */
	private boolean isBcr = false;
	
	/** The is participant. */
	private boolean isParticipant = false;
	
	/** The is cevaldom. */
	private boolean isCevaldom = false;
	
	/** The participant login. */
	private Long participantLogin = null;
	
	/** The participant. */
	private Participant participant = new Participant();
	
	private Holder sourceHolder = new Holder();
	
	private HolderAccount sourceHolderAccount = new HolderAccount();
	
	
	/** The certificate deposite bean facade. */
	@EJB
	private CertificateDepositeServiceFacade certificateDepositeBeanFacade;
	 
	/** The accounts facade. */
	@EJB
    private AccountsFacade  accountsFacade;
	
	/** The general parameters facade. */
	@EJB
    private GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	private HelperComponentFacade 		  helperComponentFacade;
	
	/** The location facade bean. */
	@EJB
	private CertificateLocationServiceFacade locationFacadeBean;
	
	/** The account query service. */
	@EJB
	private AccountQueryServiceBean accountQueryService;
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	private boolean listMassive;

	@Inject
	CertificateVaultFacade certificateVaultFacade;
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException{
		//SETTING PRIVILEGES TO SHOW MODIFY BUTTON
		enabledButtons();
		
		holderAccountResultHelperTO = new HolderAccountHelperResultTO();
		physicalCertificateLocationInputTO = new PhysicalCertificateLocationInputTO();
		//physicalCertLocationOutputSelected = new PhysicalCertificateLocationOutputTO();
		physicalCertListSel=null;
		listParticipants = new ArrayList<SelectItem>();
		lstSecurityStates = new ArrayList<ParameterTable>();
		lstRequestType = new ArrayList<ParameterTable>();
		lstSecurityLocations = new ArrayList<ParameterTable>();
		issuerTO = new IssuerTO();
		rowIsSelected = false;
		listEmpty = false;
		physicalCertificate = new PhysicalCertificate();
		physicalCertificateLocationInputTO.setDepositInitialDate(CommonsUtilities.currentDate());
		physicalCertificateLocationInputTO.setDepositFinalDate(CommonsUtilities.currentDate());
		listHolidays();
		fillCertificateStates();
		fillRequestTypes();
		fillSituations();
		fillParticipants();
		loadListAndMaps();
		fillPaymentTypeList();
		fillMoneyTypeList();
		fillPayrollTypeSearchList();
		securityEvents();
	}
	
	public void fillPaymentTypeList(){
		paymentTypeMap = new HashMap<Integer, ParameterTable>();
		lstPaymentType = new ArrayList<ParameterTable>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PERIODICITY.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			lstPaymentType = generalParametersFacade.getComboParameterTable(parameterTableTO);
			for (ParameterTable param : lstPaymentType) {
				paymentTypeMap.put(param.getParameterTablePk(), param);
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void fillMoneyTypeList (){
		moneyTypeMap = new HashMap<Integer, ParameterTable>();
		lstMoneyType = new ArrayList<ParameterTable>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			lstMoneyType = generalParametersFacade.getComboParameterTable(parameterTableTO);
			for (ParameterTable param : lstMoneyType) {
				moneyTypeMap.put(param.getParameterTablePk(), param);
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void fillPayrollTypeSearchList (){
		lstMoneyType = new ArrayList<ParameterTable>();
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		
		try {
			lstPayrollTypesSearch = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void loadListAndMaps(){

		try {
			if(lstSecurityClass == null || lstSecurityClass.size()<=0 ){
				mapSecurityClass = new HashMap<Integer, ParameterTable>();
				lstSecurityClass = new ArrayList<ParameterTable>();
				ParameterTableTO paramTable = new ParameterTableTO();
				paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
				paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
				paramTable.setOrderbyParameterName(1);
					for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
						lstSecurityClass.add(param);
						mapSecurityClass.put(param.getParameterTablePk(), param);
					}
			}
			
			if(lstInstrumentType == null || lstInstrumentType.size()<=0 ){
				mapInstrumentType = new HashMap<Integer, ParameterTable>();
				lstInstrumentType = new ArrayList<ParameterTable>();
				ParameterTableTO paramTable = new ParameterTableTO();
				paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
				paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
				paramTable.setOrderbyParameterName(1);
					for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
						lstInstrumentType.add(param);
						mapInstrumentType.put(param.getParameterTablePk(), param);
					}
			}
			
			if(lstNewSecurityLocations == null || lstNewSecurityLocations.size()<=0){
			
				lstNewSecurityLocations = locationFacadeBean.getSecurityLocationFacade(MasterTableType.SECURITY_LOCATION.getCode().longValue());
				for(ParameterTable param:lstNewSecurityLocations){
					mapLocations.put(param.getParameterTablePk(), param);
				}
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	/**
	 * Security events.
	 */
	
    public void securityEvents(){

		try {
			
			if(userInfo.getUserAcctions().isSearch())
				flagSearch=true;
			
			participantLogin = userInfo.getUserAccountSession().getParticipantCode();
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				participant = certificateDepositeBeanFacade.findParticipant(participantLogin);

				isParticipant = true;
				physicalCertificateLocationInputTO.setIdParticipantPk(participantLogin);
				
				RequestContext.getCurrentInstance().update("cboparticipant");
			}else{	//si no es niuno es cevaldom

				isCevaldom = true;
			}
					
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
	/**
	 * Row is selected.
	 */
	public void rowIsSelected(){
		rowIsSelected = true;
	}
	
	/**
	 * Load participant.
	 *
	 * @throws ServiceException the service exception
	 */
	
	private void loadParticipant() throws ServiceException{		
		List<Participant> listLocalParticipant=null;
		listParticipants = new ArrayList<SelectItem>();
		if(Validations.validateIsNotNull(participant) && Validations.validateIsNotNullAndPositive(participant.getState())){
	    	if(
    			participant.getState().equals(ParticipantStateType.BLOCKED.getCode()) || 
    			participant.getState().equals(ParticipantStateType.REGISTERED.getCode())
	    	){
	    		
	    		participant.setCompleteDescription(participant.getDescription() + " - " + participant.getIdParticipantPk().toString());
	    		listParticipants.add(new SelectItem(participant.getIdParticipantPk(), participant.getDisplayDescriptionCode()));
	    		
			}else{
				/*
				Map<String, Object> filter = new HashMap<String, Object>();
				List<Integer> states = new ArrayList<Integer>();
				states.add(ParticipantStateType.REGISTERED.getCode());
				filter.put("states", states);
				listLocalParticipant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
				*/
				Participant filterPar = new Participant();
				filterPar.setState(ParticipantStateType.REGISTERED.getCode());
				try {
					listLocalParticipant = accountsFacade.getLisParticipantServiceBean(filterPar);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				for(Participant participant: listLocalParticipant){
		    		listParticipants.add(new SelectItem(participant.getIdParticipantPk(), participant.getDisplayDescriptionCode()));
				}
			}
    	}else{
    		/*
    		Map<String, Object> filter = new HashMap<String, Object>();
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			states.add(ParticipantStateType.BLOCKED.getCode());
			filter.put("states", states);
			listLocalParticipant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
			*/
    		Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			try {
				listLocalParticipant = accountsFacade.getLisParticipantServiceBean(filterPar);
			} catch (Exception e) {
				e.printStackTrace();
			}
			

			for(Participant participant: listLocalParticipant){
			listParticipants.add(new SelectItem(participant.getIdParticipantPk(), participant.getDisplayDescriptionCode()));
			}

    	}
		
	}
	
	/**
	 * Fill participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillParticipants() throws ServiceException{
		loadParticipant();
	}
	
	/**
	 * Fill certificate states.
	 *
	 * @throws ServiceException the service exception
	 */
	
	private void fillCertificateStates() throws ServiceException{
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITY_STATES.getCode());
		List<Integer> lista = new ArrayList<>();
		lista.add(PhysicalCertificateStateType.AUTHORIZED.getCode());
		filter.setLstParameterTablePkNotIn(lista);
		//lstSecurityStates = locationFacadeBean.getSecurityLocationFacade(MasterTableType.SECURITY_STATES.getCode().longValue());
		lstSecurityStates = generalParametersFacade.getComboParameterTable(filter);	
	}
	
	/*private void fillCertificateStates() throws ServiceException{
		lstSecurityStates = locationFacadeBean.getSecurityLocationFacade(MasterTableType.SECURITY_STATES.getCode().longValue());
	}*/
	
	/**
	 * Fill request types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillRequestTypes() throws ServiceException{
		lstRequestType = locationFacadeBean.getSecurityLocationFacade(MasterTableType.REQUEST_TYPE.getCode().longValue());
	}
	
	/**
	 * Fill situations.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillSituations() throws ServiceException{
		lstSecurityLocations = locationFacadeBean.getSecurityLocationFacade(MasterTableType.SECURITY_LOCATION.getCode().longValue());
	}
	
	/**
	 * Search account by number.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchAccountByNumber() throws ServiceException{
		JSFUtilities.hideGeneralDialogues();
		//VALIDATING IF PARTICIPANT AND ACCOUNT NUMBER IS NOT NULL TO SEARCH HOLDERACCOUNT OBJECT
		if(Validations.validateIsNotNullAndPositive(physicalCertificateLocationInputTO.getIdParticipantPk())){
			if(Validations.validateIsNotNullAndPositive(holderAccountResultHelperTO.getAccountNumber())){
					 HolderAccountTO holderAccountTO = new HolderAccountTO();
					 HolderAccount 	 holderAccount	 = new HolderAccount(); 
					 
					 holderAccountTO.setParticipantTO(physicalCertificateLocationInputTO.getIdParticipantPk());
					 holderAccountTO.setHolderAccountNumber(holderAccountResultHelperTO.getAccountNumber());
					 
					 holderAccount = accountsFacade.getHolderAccountComponentServiceFacade(holderAccountTO);
					 
					 // IF HOLDER ACCOUNT EXIST
					 if(Validations.validateIsNotNullAndNotEmpty(holderAccount)){
						//SETTING RESULTS TO FILTER AND DISPLAY
						 physicalCertificateLocationInputTO.setAccountNumber(holderAccount.getAccountNumber());
						 holderAccountResultHelperTO.setAccountNumber(holderAccount.getAccountNumber());
						 holderAccountResultHelperTO.setHolderDescription(holderAccount.getHolderAccountDetailDescription());
					 }else{
						//JSFUtilities.showComponent("cnfEndTransactionSamePage");
						//JSFUtilities.showMessageOnDialog(PropertiesConstants.MSG_WARNING_TITLE,null,PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST,null);
						String header = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
						String body = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST);
						alert(header,body);
						holderAccountResultHelperTO = new HolderAccountHelperResultTO();
					 }
			}else{
				holderAccountResultHelperTO = new HolderAccountHelperResultTO();
				physicalCertificateLocationInputTO.setAccountNumber(null);
			}
		}else{
			/*JSFUtilities.showComponent("cnfEndTransactionSamePage");
			JSFUtilities.showMessageOnDialog(PropertiesConstants.MSG_WARNING_TITLE,null,PropertiesConstants.ERROR_PARETICIPANT_REQUIRE,null);
			JSFUtilities.addContextMessage("frmPhysicalCertificateSituation:idCboParticipant",FacesMessage.SEVERITY_ERROR, null, null);*/
			String header = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
			String body = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_PARETICIPANT_REQUIRE);
			alert(header,body);
		}	 
	}
	
	/**
	 * Call remote command holder account.
	 */
	public void callRemoteCommandHolderAccount(){
		holderAccountResultHelperTO = new HolderAccountHelperResultTO();
		physicalCertificateLocationInputTO.setAccountNumber(null);
	}
	
	/**
	 * Search issuer by code.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchIssuerByCode() throws ServiceException{
		if(Validations.validateIsNotNullAndNotEmpty(issuerTO.getIssuerCode())){
			IssuerSearcherTO issuerSearcher = new IssuerSearcherTO();
			issuerSearcher.setIssuerCode(issuerTO.getIssuerCode());
			issuerTO = helperComponentFacade.findIssuerByCode(issuerSearcher);
			if(!Validations.validateIsNotNullAndNotEmpty(issuerTO)){
				//JSFUtilities.showComponent("cnfEndTransactionSamePage");
				//JSFUtilities.showMessageOnDialog(PropertiesConstants.MSG_WARNING_TITLE,null,PropertiesConstants.NOT_FIND_DATA,null);
				String header = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
				String body = PropertiesUtilities.getMessage(PropertiesConstants.NOT_FIND_DATA);
				alert(header,body);
				issuerTO = new IssuerTO();
			}
		}else{
			issuerTO = new IssuerTO();
		}
	}
	
	/**
	 * Call remote command issuer.
	 */
	public void callRemoteCommandIssuer(){
		issuerTO = new IssuerTO();
	}
	
	/**
	 * Sets the issuer pk from helper.
	 */
	
	public void validIssuerHelp(){
		physicalCertificateLocationInputTO.setIssuerCode(issuerHelp.getIdIssuerPk());
	}
	
	public void setIssuerPkFromHelper(){
		//SETTING ISSUER CODE TO FILTER
		physicalCertificateLocationInputTO.setIssuerCode(issuerTO.getIssuerCode());
	}
	
	/**
	 * Sets the holder account pk from helper.
	 */
	
	public void validStatusHolder(){
    	
    	if(holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){    	
    		physicalCertificateLocationInputTO.setAccountNumber(holderAccount.getAccountNumber());
    	}else{
    		holderAccount = new HolderAccount();
    		physicalCertificateLocationInputTO.setAccountNumber(null);
    	}
    }
	
//	public void validStatusCui(){
//    	physicalCertificateLocationInputTO.setCuiHolder(sourceHolder.getIdHolderPk());
//    	
//    }
	 private GenericDataModel<HolderAccountHelperResultTO> genericDataModel;
	 private Holder selectedHolder;
	 private HolderAccountHelperResultTO selectedAccountTO;
	 private HolderAccount holderAccount=new HolderAccount();
	 public void cuiChangeEvent(){	
	    	genericDataModel = null;
	    	List<HolderAccountHelperResultTO> lista = null;
	    	try {
				if (Validations.validateIsNull(sourceHolder) || Validations.validateIsNull(sourceHolder.getIdHolderPk())) {
				} else {
					HolderTO holderTO = new HolderTO();
					holderTO.setHolderId(sourceHolder.getIdHolderPk());
					holderTO.setFlagAllHolders(true);
					holderTO.setParticipantFk(physicalCertificateLocationInputTO.getIdParticipantPk());
					lista = helperComponentFacade.searchAccountByFilter(holderTO);
					if (lista==null || lista.isEmpty()){
						genericDataModel = new GenericDataModel<>();
					}
					else{
						genericDataModel = new GenericDataModel<>(lista);
					}
					if(genericDataModel.getSize()<0){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getGenericMessage("Participante incorrecto"));//PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT));
						sourceHolder = new Holder();
						JSFUtilities.showSimpleValidationDialog();
					} else {
						fillDataParametersAccountTO();
					}
				}
				if(lista!=null && lista.size()==1){
					selectedAccountTO = lista.get(0);
					changeSelectAccount();
				}
				else 
					if(genericDataModel!=null){
					JSFUtilities.executeJavascriptFunction("resultHelperAccountVar.clearFilters(); resultHelperAccountVar.unselectAllRows();");
				}
			} catch (Exception e) {
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
	}
    public void fillDataParametersAccountTO() throws ServiceException{
		List<ParameterTable> lstHolderAccountState = generalParametersFacade.getListParameterTableServiceBean(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for (HolderAccountHelperResultTO objAccount : genericDataModel) 
			for (ParameterTable parameterTable : lstHolderAccountState) 
				objAccount.setAccountStatusDescription(parameterTable.getParameterName());
	}
    public void changeSelectAccount(){
		if(selectedAccountTO!=null){
			holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(selectedAccountTO.getAccountPk());
			holderAccount.setAccountNumber(selectedAccountTO.getAccountNumber());
		}
	}
	public void setHolderAccountPkFromHelper(){
		physicalCertificateLocationInputTO.setAccountNumber(holderAccountResultHelperTO.getAccountNumber());
	}
	
	/**
	 * Cbo participant ajax.
	 */
	public void cboParticipantAjax(){
		if(Validations.validateIsNullOrNotPositive(physicalCertificateLocationInputTO.getIdParticipantPk())){
			holderAccountResultHelperTO = new HolderAccountHelperResultTO();
		}
		sourceHolder=new Holder();
		genericDataModel=null;
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Search physical certificates.
	 *
	 * @throws ServiceException the service exception
	 * @throws ParseException the parse exception
	 */
	List<PayrollDetailTO> lstResult;
	private Map<Integer, String> mpVaultsSearch, mpSituationSearch, mpPayrollType, motiveDepositAnnotation;
	public void setParametersDescription() throws ServiceException {
		mpVaultsSearch = new HashMap<Integer, String>();
		mpSituationSearch = new HashMap<Integer, String>();
		mpPayrollType = new HashMap<Integer, String>();
		motiveDepositAnnotation = new HashMap<Integer, String>();
		 
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		
	    /*List<Integer> listIdSituation = new ArrayList<>();
	    listIdSituation.add(PayrollDetailStateType.VAULT.getCode());
	    listIdSituation.add(PayrollDetailStateType.TRANSIT.getCode());
	    listIdSituation.add(PayrollDetailStateType.DELIVERED.getCode());
	    paramTabTO.setLstParameterTablePk(listIdSituation);
	    */
	    paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_DETAIL_STATE.getCode());
	    List<ParameterTable> lstSituationSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
	    for(ParameterTable paramTab:lstSituationSearch){
	        mpSituationSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	    }

		paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    paramTabTO.setMasterTableFk(MasterTableType.VAULTS_FOR_BRANCH.getCode());
	    List<ParameterTable> lstVaultsSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
	    for(ParameterTable paramTab:lstVaultsSearch){
	        mpVaultsSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	    }
	    
		paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
	    List<ParameterTable> lstPayrollType = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
	    for(ParameterTable paramTab:lstPayrollType){
	    	mpPayrollType.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	    }

		paramTabTO = new ParameterTableTO();
		//paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    paramTabTO.setMasterTableFk(MasterTableType.DEMATERIALIZATION_MOTIVE.getCode());
	    List<ParameterTable> lstMotiveSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
	    for(ParameterTable paramTab:lstMotiveSearch){
	    	motiveDepositAnnotation.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	    }

		paramTabTO = new ParameterTableTO();
		//paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
	    paramTabTO.setMasterTableFk(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
	    List<ParameterTable> lstMotiveRetirementSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
	    for(ParameterTable paramTab:lstMotiveRetirementSearch){
	    	motiveDepositAnnotation.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	    }
	}
	
	public void searchPhysicalCertificates() throws ServiceException, ParseException {
		setParametersDescription();
	/*
		listEmpty = true;
		certificateLocationOutputTOs = new ArrayList<PhysicalCertificateLocationOutputTO>();
		physicalCertificateOutputDataModel = null;

		if(holderAccount!=null)
			physicalCertificateLocationInputTO.setIdHolderAccountPk(holderAccount.getIdHolderAccountPk());

			certificateLocationOutputTOs = locationFacadeBean.searchPhysicalCertificateLocation(physicalCertificateLocationInputTO);
			
			physicalCertificateOutputDataModel = new PhysicalCertificateOutputDataModel(certificateLocationOutputTOs);
			
			if(Validations.validateIsNotNullAndPositive(certificateLocationOutputTOs.size())){
				
				if( Validations.validateIsNotNullAndPositive(participant.getState()) &&
					participant.getState().equals(ParticipantStateType.BLOCKED.getCode())
				){
					disabledButtons();
				}else{
					enabledButtons();	
    			}
    				
				listEmpty = false;
			}else{
				disabledButtons();
				rowIsSelected = false;
			}
	*/
		SearchCertificateVaultTO filterCertificateVaultTO = new SearchCertificateVaultTO();
		filterCertificateVaultTO.setInitialDate(physicalCertificateLocationInputTO.getDepositInitialDate());
		filterCertificateVaultTO.setFinalDate(physicalCertificateLocationInputTO.getDepositFinalDate());

		filterCertificateVaultTO.setInitialDate(physicalCertificateLocationInputTO.getDepositInitialDate());
		filterCertificateVaultTO.setFinalDate(physicalCertificateLocationInputTO.getDepositFinalDate());
		filterCertificateVaultTO.setIdParticipantPk(physicalCertificateLocationInputTO.getIdParticipantPk());
		filterCertificateVaultTO.setIdHolderAccountPk(holderAccount.getIdHolderAccountPk());
		if(issuerHelp!=null) {
			filterCertificateVaultTO.setIdIssuerPk(issuerHelp.getIdIssuerPk());
		}
		
		filterCertificateVaultTO.setPayRollType(physicalCertificateLocationInputTO.getPayRollType());//PayrollType.REMOVE_SECURITIES_OPERATION.getCode()
		lstResult = certificateVaultFacade.searchPayrollDetailLocation(filterCertificateVaultTO);
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			filterCertificateVaultTO.setBlNoResult(false);
			filterCertificateVaultTO.setLstResult(new GenericDataModel<>(lstResult));
			for(PayrollDetailTO payrollDetailTO:filterCertificateVaultTO.getLstResult()){
				if(payrollDetailTO.getVaultLocation()!=null) {
					payrollDetailTO.setVaultLocationDesc(mpVaultsSearch.get(payrollDetailTO.getVaultLocation()));
				}
				if(payrollDetailTO.getSituation()!=null) {
					payrollDetailTO.setSituationDesc(mpSituationSearch.get(payrollDetailTO.getSituation()));
				}
				if(payrollDetailTO.getPayRollType()!=null) {
					payrollDetailTO.setPayRollTypeDesc(mpPayrollType.get(payrollDetailTO.getPayRollType()));
				}
				if(payrollDetailTO.getMotive()!=null) {
					payrollDetailTO.setAnnotationMotiveDescription(motiveDepositAnnotation.get(payrollDetailTO.getMotive()));
				}
				/*
				else {
					payrollDetailTO.setAnnotationMotiveDescription(motiveDepositAnnotation.get( DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode() ));
				}*/
				
			}
		}
		
	}
	
	@LoggerAuditWeb
	public String confirmAlertActions(){
		
		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(tmpAction.equals("closeQuestionBack")){
			closeQuestionBack();
		}else if(tmpAction.equals("onSaveClick")){
			onSaveClick();
		}
		
		return "";
	}
	
	public void question(String message) {
		String validationMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, 
								   FacesContext.getCurrentInstance().getViewRoot().getLocale(),GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	public void question(String header, String message) {
		showMessageOnDialog(header, message);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	
	/**
	 * Disabled buttons.
	 */
	public void disabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(false);
		userInfo.setPrivilegeComponent(privilegeComponent);	
    }
	
	/**
	 * Enabled buttons.
	 */
	public void enabledButtons(){
    	PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(true);
		userInfo.setPrivilegeComponent(privilegeComponent);	
    }
	
	
	public void alertDialog(String message){
		String validationMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, 
				   FacesContext.getCurrentInstance().getViewRoot().getLocale(),GeneralConstants.LBL_HEADER_ALERT_WARNING);
		
		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("PF('wdAlertDialog').show();");
	}
	
	public void alert(String message) {
		String validationMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, 
								   FacesContext.getCurrentInstance().getViewRoot().getLocale(),GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}
	
	public void alert(String header, String message) {
		showMessageOnDialog(header, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}
	
	
	/**
	 * On modify click.
	 *
	 * @return the string
	 */
	public void onModifyClick() {
		btnSave = false;
		disabled = false;
		newLocationSelected = null; 
		lstNewSecurityLocations = new ArrayList<ParameterTable>();
		String strInvalidState=null,strDifferentState=null;
		Integer iTemporalSituation=new Integer(GeneralConstants.ZERO_VALUE_INT);
		int iCount=0,iDifferent=0;
		try{
			
		if(Validations.validateIsNullOrEmpty(physicalCertListSel) || physicalCertListSel.length<=GeneralConstants.ZERO_VALUE_INT){
			String header = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_RECORD_SELECT_CNF);
			alert(header,message);
			return ;
		}
		
		strInvalidState=GeneralConstants.EMPTY_STRING;
		strDifferentState=GeneralConstants.EMPTY_STRING;
		for(PhysicalCertificateLocationOutputTO physicalCertLocationOutputSelected:   physicalCertListSel)
		{
			iCount++;
			if ( !physicalCertLocationOutputSelected.getCertificateStatus().equals(StateType.CONFIRMED.getCode()) ) { 
				strInvalidState+=physicalCertLocationOutputSelected.getRequestNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			}
			else{
				if(iCount==GeneralConstants.ONE_VALUE_INTEGER){
					iTemporalSituation=physicalCertLocationOutputSelected.getSituation();
					strDifferentState+=physicalCertLocationOutputSelected.getStrSecurityCodePk()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
				}
				else{
					if(iTemporalSituation.intValue()!=physicalCertLocationOutputSelected.getSituation().intValue()){
						iDifferent++;
						strDifferentState+=physicalCertLocationOutputSelected.getStrSecurityCodePk()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
					}
				}
			}
		}
		if(!strInvalidState.equals(GeneralConstants.EMPTY_STRING)){
			if(strInvalidState.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
					strInvalidState=strInvalidState.substring(0,strInvalidState.length()-1);
			if(physicalCertListSel.length==GeneralConstants.ONE_VALUE_INTEGER){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_OPERATION_NOT_MODIFY_SITUATION));
			}else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MSG_THIS_OPERATION_NOT_SITUATION_MASSIVE,strInvalidState));
			}
			return;
		}
		if(iDifferent>GeneralConstants.ZERO_VALUE_INT){
			if(!strDifferentState.equals(GeneralConstants.EMPTY_STRING)){
				if(strDifferentState.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
					strDifferentState=strDifferentState.substring(0,strDifferentState.length()-1);
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MSG_SITUATION_DIFFERENT_STATE,strDifferentState));
				return;
			}
		}
		if(physicalCertListSel.length>GeneralConstants.ONE_VALUE_INTEGER)
			listMassive=true;
		else
			listMassive=false;
		newLocationSelected = null;
		disabled = false;
		btnSave = false;
		if(physicalCertListSel.length==GeneralConstants.ONE_VALUE_INTEGER){
			Participant participant = findParticipant(physicalCertListSel[0].getIdParticipantpk());
			strParticipantSelected = participant.getDescription();
		}
		
		//the same many as one
		if ( physicalCertListSel[0].getRequestType().equals(RequestType.DEPOSITEO.getCode()) ){
			
			if(physicalCertListSel[0].getSituation().equals(SituationType.DEPOSITARY_OPERATIONS.getCode())){
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_VAULT.getCode()));
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_MESSENGER.getCode()));
				btnSave = true;
			}else if(physicalCertListSel[0].getSituation().equals(SituationType.DEPOSITARY_VAULT.getCode())){
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_OPERATIONS.getCode()));
				btnSave = true;

			}else if(physicalCertListSel[0].getSituation().equals(SituationType.DEPOSITARY_MESSENGER.getCode())){
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_OPERATIONS.getCode()));

				btnSave = true;
			}
			
		}else{ //RETIRO
			if(physicalCertListSel[0].getSituation().equals(SituationType.DEPOSITARY_OPERATIONS.getCode())){
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_VAULT.getCode()));
				btnSave = true;
			}else if(physicalCertListSel[0].getSituation().equals(SituationType.DEPOSITARY_MESSENGER.getCode())){
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_OPERATIONS.getCode()));

				btnSave = true;
			}else if(physicalCertListSel[0].getSituation().equals(SituationType.DEPOSITARY_VAULT.getCode())){
				lstNewSecurityLocations.add(mapLocations.get(SituationType.DEPOSITARY_OPERATIONS.getCode()));
				btnSave = true;
			}
		}
		
		if(physicalCertListSel[0].getSituation().equals(SituationType.CUSTODY_VAULT.getCode())){				
			alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_INVALID_OPERATION_PHYSICAL_CERTIFICATE_IN_BANK));
		}else{
			showLocation();	//MUESTRO EL DIALOG
		}
		
		
		}catch (Exception e) {
	           excepcion.fire(new ExceptionToCatchEvent(e));
			}
		return ;
	}
	
	public void showLocation(){
		JSFUtilities.executeJavascriptFunction("PF('locationsCertificateW').show();");
	}
	
	/**
	 * On save click.
	 */
	
	public void validateSave(){
		if(Validations.validateIsNotNullAndPositive(newLocationSelected)){
			onSaveClick();
		}else{
			alertDialog(PropertiesUtilities.getMessage("lbl.error.message.not.update.physicalsituation"));
			JSFUtilities.setInvalidViewComponent("frmDialogs:idNewLocation");
		}
	}
	
	public void onSaveClick() {
//		hideDialogs();
		String strCertificateInvalid=GeneralConstants.EMPTY_STRING,strCertificateValid=GeneralConstants.EMPTY_STRING;
		if(Validations.validateIsNullOrNotPositive(newLocationSelected)){
			return;
		}
		try{
			if (physicalCertListSel[0].getSituation() == newLocationSelected) {
				Object object[] = new Object[1];
				object[0] = physicalCertListSel[0].getCertificateNumber();
				alert(PropertiesUtilities.getMessage(PropertiesConstants.DUPLICATE_SITUATION));
				return;
			}
			for(PhysicalCertificateLocationOutputTO physiCertSel: physicalCertListSel)
			{
				if(physicalCertListSel.length==GeneralConstants.ONE_VALUE_INTEGER)
				{
					PhysicalCertificate certificateDate = locationFacadeBean.getLastModifiedDateDetails(physiCertSel.getIdPhysicalCertificate());
					if (certificateDate.getLastModifyDate() != null) {
						if (physiCertSel.getSituation().compareTo(certificateDate.getSituation().intValue()) < 0) {
							Object object[] = new Object[2];
							object[0] = physiCertSel.getRequestNumber();
							object[1] = strParticipantSelected;
							alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_MULTIPLE_USERS_UPDATION));
							return;
						}
					}
					List<String> objectList = new ArrayList<String>();
					Participant participant = findParticipant(physicalCertListSel[0].getIdParticipantpk());
					objectList.add(RequestType.get(physicalCertListSel[0].getRequestType().intValue()).getValue());
					objectList.add(participant.getMnemonic()+CommonsUtilities.STR_BLANK);

					JSFUtilities.executeJavascriptFunction("PF('idRechazDialog').show();");
					JSFUtilities.showMessageOnDialog(PropertiesConstants.MSG_HEADER_MODIFICATION,null,PropertiesConstants.MSG_LOCATION_MODIFICATION_CNFM,
							new Object[]{objectList.get(0),objectList.get(1),physicalCertListSel[0].getRequestNumber().toString()});
					break;
				}
				else
				{
					PhysicalCertificate certificateDate = locationFacadeBean.getLastModifiedDateDetails(physiCertSel.getIdPhysicalCertificate());
					if (certificateDate.getLastModifyDate() != null) {
						if (physiCertSel.getSituation().compareTo(certificateDate.getSituation().intValue()) < 0) {
							strCertificateInvalid+=physiCertSel.getRequestNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
						}
						else{
							strCertificateValid+=physiCertSel.getRequestNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
						}
					}
					
				}
			}
			if(!strCertificateInvalid.equals(GeneralConstants.EMPTY_STRING)){
				if(strCertificateInvalid.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
					strCertificateInvalid=strCertificateInvalid.substring(0,strCertificateInvalid.length()-1);
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MSG_LOCATION_MULTIPLE_USERS_MASSIVE));
				return;
			}
			if(!strCertificateValid.equals(GeneralConstants.EMPTY_STRING))
			{
				if(strCertificateValid.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
					strCertificateValid=strCertificateValid.substring(0,strCertificateValid.length()-1);
				JSFUtilities.executeJavascriptFunction("PF('idRechazDialog').show();");
				JSFUtilities.showMessageOnDialog(PropertiesConstants.MSG_HEADER_MODIFICATION,null,PropertiesConstants.LBL_ERROR_MSG_LOCATION_MSG_MASSIVE,
						new Object[]{strCertificateValid});
			}
			
		}catch (Exception e) {
           excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * On cancel click.
	 */
	public void onCancelClick(){
		/*if (physicalCertLocationOutputSelected.getRequestType() == RequestType.DEPOSITEO.getCode()
				&& physicalCertLocationOutputSelected.getSituation() == SituationType.DEPOSITARY_OPERATIONS.getCode()) {
			newLocationSelected=null;
		}*/
		
		if(Validations.validateIsNotNullAndPositive(newLocationSelected)){
			question(PropertiesUtilities.getMessage("message.back"));
			confirmAlertAction = "closeQuestionBack";
		}else{
			closeQuestionBack();
			JSFUtilities.updateComponent("opnlDialogs");
		}
		
	}
	
	public void closeQuestionBack(){
		newLocationSelected=null;
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').hide();");
	}
	
	/**
	 * Gets the certificate details.
	 *
	 * @param physicalCertSelected the physical cert selected
	 * @return the certificate details
	 */
	public void getCertificateDetails(PhysicalCertificateLocationOutputTO physicalCertSelected) {
		try {
			physicalCertificate = locationFacadeBean.getPhysicalCertificateDetailsById(physicalCertSelected.getIdPhysicalCertificate());
			physicalCertificate.setClassType(mapSecurityClass.get(physicalCertificate.getSecurityClass()).getParameterName());
			physicalCertificate.setInstrumentTypeName(mapInstrumentType.get(physicalCertificate.getInstrumentType()).getParameterName());
			physicalCertificateLocationInputTO.setCurrency(physicalCertificate.getCurrency().intValue());
			physicalCertificateLocationInputTO.setInstrumentType(physicalCertificate.getInstrumentType());
			physicalCertificateLocationInputTO.setPaymentPeriodicity(physicalCertificate.getPaymentPeriodicity());
			
		    DateFormat format = new SimpleDateFormat(CommonsUtilities.DATE_PATTERN);
		    String newDateString = format.format(physicalCertificate.getIssueDate());
		    physicalCertificateLocationInputTO.setIssueDate(newDateString);
			
			if(physicalCertificate.getExpirationDate()!=null){
				newDateString=format.format(physicalCertificate.getExpirationDate());
				physicalCertificateLocationInputTO.setExpirationDate(newDateString);
			}
			
			showDetailCertificate();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void showDetailCertificate() {
		JSFUtilities.executeJavascriptFunction("PF('idDetalleValorW').show();");
	}
	
	/**
	 * Update modify location.
	 */
	@LoggerAuditWeb
	private void updateModifyLocation() {
		try{
		physicalCertificate.setSituation(newLocationSelected);
		locationFacadeBean.updateModifiedLocation(physicalCertificate,false);
		}catch (Exception e) {
           excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Modify location.
	 *
	 * @throws ServiceException the service exception
	 */
	@LoggerAuditWeb
	public void modifyLocation() throws ServiceException {
		saveModifiedLocation();
		List<String> objectList = new ArrayList<String>();
		//Participant participant = findParticipant(physicalCertificateLocationInputTO.getIdParticipantPk());
		objectList.add(RequestType.get(physicalCertListSel[0].getRequestType().intValue()).getValue());
		//objectList.add(participant.getMnemonic()+CommonsUtilities.STR_BLANK);
		
		try {
			searchPhysicalCertificates();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String strRequestModified=GeneralConstants.EMPTY_STRING;
		for(PhysicalCertificateLocationOutputTO physicalCertLocationOutputSelected: physicalCertListSel){
			strRequestModified+=physicalCertLocationOutputSelected.getRequestNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
		}
		if(strRequestModified.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
			strRequestModified=strRequestModified.substring(0,strRequestModified.length()-1);
		String message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_MODIFICATION_SUCCESS,
						 new Object[]{objectList.get(0),strRequestModified});

		alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_CONFIRM_HEADER_MESSAGE),message);		
	}
	
	
	/**
	 * Save modified location.
	 */
	@LoggerAuditWeb
	private void saveModifiedLocation() {
		try{
			for(PhysicalCertificateLocationOutputTO physicalCertLocationOutputSelected: physicalCertListSel)
			{
				physicalCertificate.setIdPhysicalCertificatePk(physicalCertLocationOutputSelected.getIdPhysicalCertificate());
				CertificateSituationHistory situationHistory = new CertificateSituationHistory();
				situationHistory.setPhysicalCertificate(physicalCertificate);
				situationHistory.setOldSituation(physicalCertLocationOutputSelected.getSituation());
				situationHistory.setNewSituation(newLocationSelected);
				situationHistory.setSituationDate(CommonsUtilities.currentDateTime());
				if(userInfo!=null){
					situationHistory.setRegisterUser(userInfo.getUserAccountSession().getUserName());
				}
				updateModifyLocation();
				locationFacadeBean.insertModifiedLocation(situationHistory);
			}
		}catch (Exception e) {
           excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
 * Find participant.
 *
 * @param idParticipantPk the id participant pk
 * @return the participant
 * @throws ServiceException the service exception
 */
	private Participant findParticipant(Long idParticipantPk) throws ServiceException{
		Participant part = new Participant();
		part = helperComponentFacade.findParticipantsByCode(idParticipantPk);
		
		return part;
	}
	
	/**
	 * Clear.
	 */
	public void clear(){
		genericDataModel=null;
		selectedAccountTO=null;
		physicalCertificateLocationInputTO = new PhysicalCertificateLocationInputTO();
		physicalCertificateOutputDataModel = new PhysicalCertificateOutputDataModel();
//		physicalCertLocationOutputSelected = new PhysicalCertificateLocationOutputTO();
		physicalCertListSel=null;
		certificateLocationOutputTOs = new ArrayList<PhysicalCertificateLocationOutputTO>();
		issuerTO = new IssuerTO();
		issuerHelp = new Issuer();
		sourceHolder = new Holder();
		sourceHolderAccount = new HolderAccount();
		listEmpty = false;
		rowIsSelected = false;
		holderAccountResultHelperTO = new HolderAccountHelperResultTO();
		physicalCertificateLocationInputTO.setDepositInitialDate(CommonsUtilities.currentDate());
		physicalCertificateLocationInputTO.setDepositFinalDate(CommonsUtilities.currentDate());
		securityEvents();
	}
	
	/**
	 * Idle time logout user session.
	 */
	public void sessionLogout(){
		try{
			JSFUtilities.setHttpSessionAttribute(GeneralConstants.LOGOUT_MOTIVE, LogoutMotiveType.EXPIREDSESSION);
			JSFUtilities.killSession(); 
			JSFUtilities.showMessageOnDialog(null, null,PropertiesConstants.IDLE_TIME_EXPIRED, null);
			JSFUtilities.showComponent(":idCnfDialogKillSession");
		} catch (Exception ex) {
				excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	//GETTERS AND SETTERS
	/**
	 * Gets the holder account result helper to.
	 *
	 * @return the holder account result helper to
	 */
	public HolderAccountHelperResultTO getHolderAccountResultHelperTO() {
		return holderAccountResultHelperTO;
	}

	/**
	 * Sets the holder account result helper to.
	 *
	 * @param holderAccountResultHelperTO the new holder account result helper to
	 */
	public void setHolderAccountResultHelperTO(
			HolderAccountHelperResultTO holderAccountResultHelperTO) {
		this.holderAccountResultHelperTO = holderAccountResultHelperTO;
	}

	/**
	 * Gets the issuer to.
	 *
	 * @return the issuer to
	 */
	public IssuerTO getIssuerTO() {
		return issuerTO;
	}

	/**
	 * Sets the issuer to.
	 *
	 * @param issuerTO the new issuer to
	 */
	public void setIssuerTO(IssuerTO issuerTO) {
		this.issuerTO = issuerTO;
	}

	/**
	 * Gets the physical certificate location input to.
	 *
	 * @return the physical certificate location input to
	 */
	public PhysicalCertificateLocationInputTO getPhysicalCertificateLocationInputTO() {
		return physicalCertificateLocationInputTO;
	}

	/**
	 * Sets the physical certificate location input to.
	 *
	 * @param physicalCertificateLocationInputTO the new physical certificate location input to
	 */
	public void setPhysicalCertificateLocationInputTO(
			PhysicalCertificateLocationInputTO physicalCertificateLocationInputTO) {
		this.physicalCertificateLocationInputTO = physicalCertificateLocationInputTO;
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<SelectItem> getListParticipants() {
		return listParticipants;
	}

	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<SelectItem> listParticipants) {
		this.listParticipants = listParticipants;
	}

	/**
	 * Gets the lst security states.
	 *
	 * @return the lst security states
	 */
	public List<ParameterTable> getLstSecurityStates() {
		return lstSecurityStates;
	}

	/**
	 * Sets the lst security states.
	 *
	 * @param lstSecurityStates the new lst security states
	 */
	public void setLstSecurityStates(List<ParameterTable> lstSecurityStates) {
		this.lstSecurityStates = lstSecurityStates;
	}

	/**
	 * Gets the lst request type.
	 *
	 * @return the lst request type
	 */
	public List<ParameterTable> getLstRequestType() {
		return lstRequestType;
	}

	/**
	 * Sets the lst request type.
	 *
	 * @param lstRequestType the new lst request type
	 */
	public void setLstRequestType(List<ParameterTable> lstRequestType) {
		this.lstRequestType = lstRequestType;
	}

	/**
	 * Gets the lst security locations.
	 *
	 * @return the lst security locations
	 */
	public List<ParameterTable> getLstSecurityLocations() {
		return lstSecurityLocations;
	}

	/**
	 * Sets the lst security locations.
	 *
	 * @param lstSecurityLocations the new lst security locations
	 */
	public void setLstSecurityLocations(List<ParameterTable> lstSecurityLocations) {
		this.lstSecurityLocations = lstSecurityLocations;
	}
	
	/**
	 * Gets the min date.
	 *
	 * @return the min date
	 */
	public Date getMinDate() {
		if(minDate == null){
			Calendar instance = Calendar.getInstance();
			instance.add(Calendar.MONTH, -3);
			this.minDate = instance.getTime();
		}
		return minDate;
	}
	
	/**
	 * Sets the min date.
	 *
	 * @param minDate the new min date
	 */
	public void setMinDate(Date minDate) {
		this.minDate = minDate;
	}
	
	/**
	 * Gets the max date.
	 *
	 * @return the max date
	 */
	public Date getMaxDate() {
		maxDate = CommonsUtilities.currentDate();
		return maxDate;
	}

	/**
	 * Sets the max date.
	 *
	 * @param maxDate the new max date
	 */
	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}

	/**
	 * Gets the certificate location output t os.
	 *
	 * @return the certificate location output t os
	 */
	public List<PhysicalCertificateLocationOutputTO> getCertificateLocationOutputTOs() {
		return certificateLocationOutputTOs;
	}

	/**
	 * Sets the certificate location output t os.
	 *
	 * @param certificateLocationOutputTOs the new certificate location output t os
	 */
	public void setCertificateLocationOutputTOs(
			List<PhysicalCertificateLocationOutputTO> certificateLocationOutputTOs) {
		this.certificateLocationOutputTOs = certificateLocationOutputTOs;
	}

	/**
	 * Gets the physical certificate output data model.
	 *
	 * @return the physical certificate output data model
	 */
	public PhysicalCertificateOutputDataModel getPhysicalCertificateOutputDataModel() {
		return physicalCertificateOutputDataModel;
	}

	/**
	 * Sets the physical certificate output data model.
	 *
	 * @param physicalCertificateOutputDataModel the new physical certificate output data model
	 */
	public void setPhysicalCertificateOutputDataModel(
			PhysicalCertificateOutputDataModel physicalCertificateOutputDataModel) {
		this.physicalCertificateOutputDataModel = physicalCertificateOutputDataModel;
	}

	/**
	 * Checks if is row is selected.
	 *
	 * @return true, if is row is selected
	 */
	public boolean isRowIsSelected() {
		return rowIsSelected;
	}

	/**
	 * Sets the row is selected.
	 *
	 * @param rowIsSelected the new row is selected
	 */
	public void setRowIsSelected(boolean rowIsSelected) {
		this.rowIsSelected = rowIsSelected;
	}

	/**
	 * Checks if is list empty.
	 *
	 * @return true, if is list empty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * Gets the str participant selected.
	 *
	 * @return the str participant selected
	 */
	public String getStrParticipantSelected() {
		return strParticipantSelected;
	}

	/**
	 * Sets the str participant selected.
	 *
	 * @param strParticipantSelected the new str participant selected
	 */
	public void setStrParticipantSelected(String strParticipantSelected) {
		this.strParticipantSelected = strParticipantSelected;
	}

	/**
	 * Gets the lst new security locations.
	 *
	 * @return the lst new security locations
	 */
	public List<ParameterTable> getLstNewSecurityLocations() {
		return lstNewSecurityLocations;
	}

	/**
	 * Sets the lst new security locations.
	 *
	 * @param lstNewSecurityLocations the new lst new security locations
	 */
	public void setLstNewSecurityLocations(
			List<ParameterTable> lstNewSecurityLocations) {
		this.lstNewSecurityLocations = lstNewSecurityLocations;
	}

	/**
	 * Gets the new location selected.
	 *
	 * @return the new location selected
	 */
	public Integer getNewLocationSelected() {
		return newLocationSelected;
	}

	/**
	 * Sets the new location selected.
	 *
	 * @param newLocationSelected the new new location selected
	 */
	public void setNewLocationSelected(Integer newLocationSelected) {
		this.newLocationSelected = newLocationSelected;
	}

	/**
	 * Checks if is disabled.
	 *
	 * @return true, if is disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * Sets the disabled.
	 *
	 * @param disabled the new disabled
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * Checks if is btn save.
	 *
	 * @return true, if is btn save
	 */
	public boolean isBtnSave() {
		return btnSave;
	}

	/**
	 * Sets the btn save.
	 *
	 * @param btnSave the new btn save
	 */
	public void setBtnSave(boolean btnSave) {
		this.btnSave = btnSave;
	}

	/**
	 * Gets the physical certificate.
	 *
	 * @return the physical certificate
	 */
	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	/**
	 * Sets the physical certificate.
	 *
	 * @param physicalCertificate the new physical certificate
	 */
	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	/**
	 * Gets the flag search.
	 *
	 * @return the flag search
	 */
	public boolean getFlagSearch() {
		return flagSearch;
	}

	/**
	 * Sets the flag search.
	 *
	 * @param flagSearch the new flag search
	 */
	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	/**
	 * Gets the checks if is bcr.
	 *
	 * @return the checks if is bcr
	 */
	public boolean getIsBcr() {
		return isBcr;
	}

	/**
	 * Sets the checks if is bcr.
	 *
	 * @param isBcr the new checks if is bcr
	 */
	public void setIsBcr(boolean isBcr) {
		this.isBcr = isBcr;
	}

	/**
	 * Gets the checks if is participant.
	 *
	 * @return the checks if is participant
	 */
	public boolean getIsParticipant() {
		return isParticipant;
	}

	/**
	 * Sets the checks if is participant.
	 *
	 * @param isParticipant the new checks if is participant
	 */
	public void setIsParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Gets the checks if is cevaldom.
	 *
	 * @return the checks if is cevaldom
	 */
	public boolean getIsCevaldom() {
		return isCevaldom;
	}

	/**
	 * Sets the checks if is cevaldom.
	 *
	 * @param isCevaldom the new checks if is cevaldom
	 */
	public void setIsCevaldom(boolean isCevaldom) {
		this.isCevaldom = isCevaldom;
	}

	/**
	 * Gets the participant login.
	 *
	 * @return the participant login
	 */
	public Long getParticipantLogin() {
		return participantLogin;
	}

	/**
	 * Sets the participant login.
	 *
	 * @param participantLogin the new participant login
	 */
	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	public Holder getSourceHolder() {
		return sourceHolder;
	}

	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}

	public HolderAccount getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	public void setSourceHolderAccount(HolderAccount sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	public Issuer getIssuerHelp() {
		return issuerHelp;
	}

	public void setIssuerHelp(Issuer issuerHelp) {
		this.issuerHelp = issuerHelp;
	}

	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	public PhysicalCertificateLocationOutputTO[] getPhysicalCertListSel() {
		return physicalCertListSel;
	}

	public void setPhysicalCertListSel(
			PhysicalCertificateLocationOutputTO[] physicalCertListSel) {
		this.physicalCertListSel = physicalCertListSel;
	}

	public boolean isListMassive() {
		return listMassive;
	}

	public void setListMassive(boolean listMassive) {
		this.listMassive = listMassive;
	}

	public List<ParameterTable> getLstInstrumentType() {
		return lstInstrumentType;
	}

	public void setLstInstrumentType(List<ParameterTable> lstInstrumentType) {
		this.lstInstrumentType = lstInstrumentType;
	}

	public List<ParameterTable> getLstPaymentType() {
		return lstPaymentType;
	}

	public void setLstPaymentType(List<ParameterTable> lstPaymentType) {
		this.lstPaymentType = lstPaymentType;
	}

	public List<ParameterTable> getLstMoneyType() {
		return lstMoneyType;
	}

	public void setLstMoneyType(List<ParameterTable> lstMoneyType) {
		this.lstMoneyType = lstMoneyType;
	}
	
	public GenericDataModel<HolderAccountHelperResultTO> getGenericDataModel() {
		return genericDataModel;
	}

	public void setGenericDataModel(
			GenericDataModel<HolderAccountHelperResultTO> genericDataModel) {
		this.genericDataModel = genericDataModel;
	}

	public HolderAccountHelperResultTO getSelectedAccountTO() {
		return selectedAccountTO;
	}

	public void setSelectedAccountTO(HolderAccountHelperResultTO selectedAccountTO) {
		this.selectedAccountTO = selectedAccountTO;
	}

	public List<PayrollDetailTO> getLstResult() {
		return lstResult;
	}

	public void setLstResult(List<PayrollDetailTO> lstResult) {
		this.lstResult = lstResult;
	}

	public List<ParameterTable> getLstPayrollTypesSearch() {
		return lstPayrollTypesSearch;
	}

	public void setLstPayrollTypesSearch(List<ParameterTable> lstPayrollTypesSearch) {
		this.lstPayrollTypesSearch = lstPayrollTypesSearch;
	}
	
}