package com.pradera.custody.physicalcertificate.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.custody.physical.PhysicalCertificate;
// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class PhysicalCertificateDataModel
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCustody
 * @Creation_Date :
 */
public class PhysicalCertificateDataModel extends ListDataModel<PhysicalCertificate> implements SelectableDataModel<PhysicalCertificate>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new physical certificate data model.
	 */
	public PhysicalCertificateDataModel() {
		
	}
	
	/**
	 * Instantiates a new physical certificate data model.
	 *
	 * @param lstPhysicalCertificate the lst physical certificate
	 */
	public PhysicalCertificateDataModel(List<PhysicalCertificate> lstPhysicalCertificate) {
		super(lstPhysicalCertificate);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public PhysicalCertificate getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<PhysicalCertificate> ListPhysicalCertificate = (List<PhysicalCertificate>)getWrappedData();
		for(PhysicalCertificate physicalCertificate : ListPhysicalCertificate){
			if(physicalCertificate.getCertificateNumber().toString().equals(rowKey))
				return physicalCertificate;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(PhysicalCertificate physicalCertificate) {//Return Object
		// TODO Auto-generated method stub
		return physicalCertificate.getCertificateNumber();
	}
	

}
