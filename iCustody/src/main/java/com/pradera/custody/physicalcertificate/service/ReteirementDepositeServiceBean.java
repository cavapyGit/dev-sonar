package com.pradera.custody.physicalcertificate.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDataFilterBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.custody.to.XmlActivoFinanciero;
import com.pradera.integration.component.custody.to.XmlSolicitudActivoFinanciero;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.physical.PhysicalBalanceDetail;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateMovement;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ReteirementDepositeServiceBean
 * 
 * @Project : PraderaCustody
 * @author : PraderaTechnologies.
 * @Creation_Date :
 * @version 1.1
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ReteirementDepositeServiceBean extends CrudDaoServiceBean {

	/**
	 * It gives emisor data.
	 *
	 * @param issuer            the Issuer
	 * @return the List<Issuer>
	 * @throws ServiceException the service exception
	 */

	public Issuer issueDetailsService(Issuer issuer) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		if (Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())) {
			sbQuery.append("select issuer From Issuer issuer ");
			sbQuery.append(" where issuer.idIssuerPk=:idissuefk");
			parameters.put("idissuefk", issuer.getIdIssuerPk().toUpperCase());
		} else {
			return null;
		}
		return (Issuer) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * It gives securities data
	 * 
	 * @param searchdata
	 *            the ReteirementDataFilterBean
	 * @param accnumber
	 *            the Long
	 * @param issurfk
	 *            the String
	 * @param inciDate
	 *            the Date
	 * @param finDate
	 *            the Date
	 * @return the List<ReteirementDepositeFilterBean>
	 */

	public List<Object[]> searchSecuritiesService(ReteirementDataFilterBean searchdata) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		String issueFk = searchdata.getIssueFk();
		String idIssuer= searchdata.getIssuerPKSearch();
		String idSecurityCode = searchdata.getIdSecurityCode();
		Date inci = searchdata.getDescDate();
		Date fin = searchdata.getHasDate();
		Long docNo = searchdata.getDocumentNumber();
		Integer instrType = searchdata.getInstrumenttype();
		Integer secClass = searchdata.getSecurityClass();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select "+
						" DISTINCT(pc.registerDate), "+//0
						" pc, "+//1
						" pc.securities, "+//2
						" pc.issuer, "+//3
						" pbd.idPhysicalBalanceDetailPk "+ //4
						" from PhysicalBalanceDetail pbd, PhysicalCertificate pc, PhysicalOperationDetail phyOperDet ");
		sbQuery.append(" where pbd.physicalCertificate.idPhysicalCertificatePk = pc.idPhysicalCertificatePk " +
					   " and not exists(select 1 from PhysicalOperationDetail pod2 " +
						" where pod2.physicalCertificate.idPhysicalCertificatePk = pc.idPhysicalCertificatePk and " +
						" pod2.physicalCertificateOperation.physicalOperationType = :physicalOperationType and " +
						" pod2.state in (:lstCertificateState) ) ");

		sbQuery.append(" and pc.situation in (:situationValue1,:situationValue2) ");
		parameters.put("situationValue1", SituationType.CUSTODY_VAULT.getCode());
		parameters.put("situationValue2", SituationType.DEPOSITARY_VAULT.getCode());
		parameters.put("physicalOperationType", new Long(RequestType.RETIRO.getCode()));
		List<Integer> lstCertificateState= new ArrayList<Integer>();
		lstCertificateState.add(PhysicalCertificateStateType.REGISTERED.getCode());
		lstCertificateState.add(PhysicalCertificateStateType.APPROVED.getCode());
		lstCertificateState.add(PhysicalCertificateStateType.CONFIRMED.getCode());
		lstCertificateState.add(PhysicalCertificateStateType.AUTHORIZED.getCode());
		parameters.put("lstCertificateState", lstCertificateState);
		
		if (Validations.validateIsNotNull(issueFk)) {
			sbQuery.append(" and pc.securities.issuance.idIssuanceCodePk = :idissuefk ");
			parameters.put("idissuefk", issueFk.toUpperCase());
		}
		if (Validations.validateIsNotNull(idIssuer)) {
			sbQuery.append(" and pc.issuer.idIssuerPk = :idIssuer ");
			parameters.put("idIssuer", idIssuer);
		}
		if (Validations.validateIsNotNull(inci)) {
			sbQuery.append(" and pc.registerDate >= :incidate");
			parameters.put("incidate", inci);
		}
		if (Validations.validateIsNotNull(fin)) {
			sbQuery.append(" and pc.registerDate < :findate");
			parameters.put("findate", fin);
		}
		if (Validations.validateIsNotNullAndPositive(docNo)) {
			sbQuery.append(" and pc.certificateNumber = :certificateNo");
			parameters.put("certificateNo", docNo);
		}
		if (Validations.validateIsNotNullAndPositive(instrType)) {
			sbQuery.append("  and pc.instrumentType = :instrument");
			parameters.put("instrument", instrType);
		}
		if (Validations.validateIsNotNullAndPositive(secClass)) {
			sbQuery.append("  and pc.securityClass = :securityClass");
			parameters.put("securityClass", secClass);
		}
		if (Validations.validateIsNotNullAndNotEmpty(idSecurityCode)) {
			sbQuery.append("  and pc.securities.idSecurityCodePk = :idSecurityCode");
			parameters.put("idSecurityCode", idSecurityCode);
		}
		
		if (Validations.validateIsNotNullAndPositive(searchdata.getIdParticipanrPk())) {
			sbQuery.append("  and pbd.participant.idParticipantPk = :idParticipantFkPrm");
			parameters.put("idParticipantFkPrm", searchdata.getIdParticipanrPk());
		}
		if (Validations.validateIsNotNullAndPositive(searchdata.getAccHolderPk())) {
			sbQuery.append("  and pbd.holderAccount.idHolderAccountPk = :accHolderPk");
			parameters.put("accHolderPk", searchdata.getAccHolderPk());
		}

		List<Object[]> search = findListByQueryString(sbQuery.toString(),parameters);
		
		return search;

	}
	
	/**
	 * It save the Custody Operation data 
	 * @param custOper
	 * @return
	 * @throws ServiceException
	 */
	public CustodyOperation saveCustodyOperation(CustodyOperation custOper) throws ServiceException{
		// saving custody operation
				CustodyOperation saveDataCustody = create(custOper);
				return saveDataCustody;
	}
	
	/**
	 * It save the PhysicalCertificateOperation data 
	 * @param phyCerfOper PhysicalCertificateOperationtype
	 * @return the PhysicalCertificateOperation
	 * @throws ServiceException
	 */
	public PhysicalCertificateOperation savePhysicalCertificateOperation(PhysicalCertificateOperation phyCerfOper) throws ServiceException{
		PhysicalCertificateOperation operData = create(phyCerfOper);
		return operData;
	}
	
	
	 /**
		 * It saves the PhysicalOperationDetail
		 * 	 
		 * @param phyOperDetail
		 *            the PhysicalOperationDetail
		 * @return the PhysicalOperationDetail
		 * @throws ServiceException
	 */
	public PhysicalOperationDetail savePhysicalOperationDetail(PhysicalOperationDetail phyOperDetail ) throws ServiceException {
		
		PhysicalOperationDetail savephyOperDetail= create(phyOperDetail);

		return savephyOperDetail;
	}

	/**
	 * It gives holder details
	 * @param accno Integer type
	 * @param participant Long type
	 * @return the List<Object[]>
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> accountHolderDetailsService(Integer accno,Long participant) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select h.idHolderPk,h.fullName from Holder h, ");
		sbQuery.append("HolderAccount ha,HolderAccountDetail had ");
		sbQuery.append("where had.holderAccount=ha.idHolderAccountPk ");
		sbQuery.append("and had.holder=h.idHolderPk");
		if (Validations.validateIsNotNull(participant) && participant > 0l) {
			sbQuery.append(" and ha.participant.idParticipantPk=:idparticipant ");
			parameters.put("idparticipant", participant);
		}

		if (Validations.validateIsNotNull(accno) && accno > 0) {
			sbQuery.append(" and ha.accountNumber=:accnumber");
			parameters.put("accnumber", accno);
		}
		List<Object[]> accDetails = findListByQueryString(sbQuery.toString(),
				parameters);
		return accDetails;
	}
//	public PhysicalCertMarketfact getPhysicalCertMarketfact(Long idPhysicalCertificatePk){
//		StringBuffer sbQuery = new StringBuffer();
//		sbQuery.append(" select pcm from PhysicalCertMarketfact pcm");
//		sbQuery.append(" where pcm.physicalCertificate="+idPhysicalCertificatePk);
//		Object object = findObjectByQueryString(sbQuery.toString(), null);
//		return (PhysicalCertMarketfact) object;
//	}
	/**
	 * It gives security class details
	 * 
	 * @param instru
	 *            the Integer
	 * @param masterfk
	 *            the Integer
	 * @return the List<ParameterTable>
	 * @throws ServiceException
	 */
	
	@SuppressWarnings("unchecked")
	public List<ParameterTable> securityClassService(Integer instru,Integer masterfk) throws ServiceException {

		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("Select p from ParameterTable p ");
		sbQuery.append(" where  p.masterTable.masterTablePk=:idmasterfk ");
		parameters.put("idmasterfk", masterfk);

		if (Validations.validateIsNotNull(instru) && instru > 0) {
			sbQuery.append(" and p.shortInteger=:shortinteger");
			parameters.put("shortinteger", instru);
		}

		return (List<ParameterTable>) findListByQueryString(sbQuery.toString(),
				parameters);

	}

	/**
	 * It gives parameter table details for the master fk
	 * 
	 * @param masterFk
	 *            the Integer
	 * @return the List<ParameterTable>
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getParameterTableDetailsService(Integer masterFk)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p from ParameterTable p ");
		sbQuery.append(" where  p.masterTable.masterTablePk=:idmasterfk ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idmasterfk", masterFk);
		return (List<ParameterTable>) query.getResultList();
	}

	public List<PhysicalCertificateExt> getPhysicalCertificateRetirementExtList(PhysicalCertificateFilter physicalCertificateFilter)throws ServiceException{
		List<Object[]> list = new ArrayList<Object[]>();
		List<PhysicalCertificateExt> physicalCertificateExts = new ArrayList<PhysicalCertificateExt>();
		StringBuffer query = 
					new StringBuffer("Select pc," +
											"pco.participant," +
											"pco.custodyOperation.operationNumber,"+
											"pod.state," +
											"pod.idPhysicalOperationDetail," +
											"pod," +
											"pco.state, " +
											"pco.custodyOperation.idCustodyOperationPk, "+
											"pco "+
									 "from PhysicalCertificate pc," +
									 	  "PhysicalOperationDetail  pod," +
									 	  "PhysicalCertificateOperation pco ");
		if( Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getCuiHolder()) ){
			query.append(", HolderAccount ho,HolderAccountDetail had,Holder h");
		}
									 	  
		query.append(" where "+
									 "pco.physicalOperationType = :physicalOperationType and " +
									 "pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk and " +
									 "pc.idPhysicalCertificatePk = pod.physicalCertificate.idPhysicalCertificatePk");
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("physicalOperationType", new Long(RequestType.RETIRO.getCode()));
		
	
		if( Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getHolderAccountPk()) ){
			query.append(" and pco.holderAccount.idHolderAccountPk = :idHolderAccountFk ");
			parameters.put("idHolderAccountFk", physicalCertificateFilter.getHolderAccountPk());
		}
		
		if( Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getCuiHolder()) ){
			query.append(" and pco.holderAccount.idHolderAccountPk=ho.idHolderAccountPk"
				  	+ " and ho.idHolderAccountPk=had.holderAccount.idHolderAccountPk"
				  	+ " and had.holder.idHolderPk=h.idHolderPk "
				  	+ " and h.idHolderPk=:idHolderFkParam");
			parameters.put("idHolderFkParam", physicalCertificateFilter.getCuiHolder());
		}
		
		if(physicalCertificateFilter.getCertificateNumber() != null){
			query.append(" and pc.certificateNumber = :certificateNumber");
			parameters.put("certificateNumber", physicalCertificateFilter.getCertificateNumber());
		}
		if(physicalCertificateFilter.getRequestNumber() != null){
			query.append(" and pco.custodyOperation.operationNumber = :requestNumber and pod.physicalCertificateOperation.custodyOperation.operationNumber = :requestNumber");
			parameters.put("requestNumber", physicalCertificateFilter.getRequestNumber());
		}else 
			query.append(" and pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk");
		query.append(" and TRUNC(pod.registryDate) >= TRUNC(:initialDate) and TRUNC(pod.registryDate) <= TRUNC(:finalDate) ");
		if(physicalCertificateFilter.getStatus() != null){
			query.append(" and pod.state = :state");
			parameters.put("state", physicalCertificateFilter.getStatus());
		}
		if( Validations.validateIsNotNullAndPositive(physicalCertificateFilter.getParticipantPk())){
			query.append(" and pco.participant.idParticipantPk = :idParticipantFk");
			parameters.put("idParticipantFk", physicalCertificateFilter.getParticipantPk());
		}
		if( Validations.validateIsNotNull(physicalCertificateFilter.getIssuerPk())){
			query.append(" and pc.issuer.idIssuerPk = :idIssuerPk");
			parameters.put("idIssuerPk", physicalCertificateFilter.getIssuerPk());
		}
		query.append(" ORDER BY pod.registryDate desc ");
		parameters.put("initialDate", physicalCertificateFilter.getInitialDate());
		parameters.put("finalDate", physicalCertificateFilter.getFinalDate());
		list = findListByQueryString(query.toString(), parameters);
		if(list != null){
			PhysicalCertificateExt physicalCertificateExt = null;
			for (Object[] object : list) {
				physicalCertificateExt = new PhysicalCertificateExt();
				physicalCertificateExt.setPhysicalCertificate((PhysicalCertificate)object[0]);
				physicalCertificateExt.setParticipantPk(((Participant)object[1]).getIdParticipantPk());
				physicalCertificateExt.setRequestNumber((Long)object[2]);
				physicalCertificateExt.setState((Integer)object[3]);
				physicalCertificateExt.setIdPhysicalOperationDetail((Long)object[4]);
				physicalCertificateExt.setPhysicalOperationDetail((PhysicalOperationDetail)object[5]);
				physicalCertificateExt.setRequestState((Integer)object[6]);
				physicalCertificateExt.setIdCustodyOperationPk((Long)object[7]);
				physicalCertificateExt.setPhysicalCertificateOperation((PhysicalCertificateOperation)object[8]);
				physicalCertificateExts.add(physicalCertificateExt);
			}
		}
		
		 return physicalCertificateExts;
	}

	/**
	 * It updates retire date of the physicalbalancedetails 
	 * @param commonInfo CommonUpdateBeanInf type
	 * @return the Integer
	 * @throws ServiceException
	 */
	public Integer updatePhysicalBalance(CommonUpdateBeanInf commonInfo) throws ServiceException{
	
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" Update PhysicalBalanceDetail phyBalDetail");
		sbQuery.append(" set phyBalDetail.retiroDate = :retirodate,");
		sbQuery.append("  phyBalDetail.state = :state ,");
		sbQuery.append("  phyBalDetail.lastModifyApp = :lastModApp ,");
		sbQuery.append("  phyBalDetail.lastModifyDate = :lastModDate ,");
		sbQuery.append("  phyBalDetail.lastModifyIp = :lastModIp ,");
		sbQuery.append("  phyBalDetail.lastModifyUser = :lastModuser ");
		sbQuery.append(" where phyBalDetail.physicalCertificate.idPhysicalCertificatePk = :idphysicalcertificate ");
		sbQuery.append(" and phyBalDetail.holderAccount.idHolderAccountPk = :idHolderAccount ");

		parameters.put("retirodate", CommonsUtilities.currentDateTime());
		parameters.put("state",commonInfo.getRequestState());
		parameters.put("idphysicalcertificate", commonInfo.getCertificatePk());
		parameters.put("idHolderAccount",commonInfo.getAccHolderPk());
		parameters.put("lastModApp", commonInfo.getLastModifyApp());
		parameters.put("lastModDate", CommonsUtilities.currentDateTime());
		parameters.put("lastModIp", commonInfo.getLastModifyIp());
		parameters.put("lastModuser", commonInfo.getCurrentUserName());
		Integer updateData = (Integer) updateByQuery(sbQuery.toString(),parameters);
		
		return updateData;
	}
	
	/**
	 * It fetch the PhysicalBalanceDetail data
	 * @param commonInfo CommonUpdateBeanInf type
	 * @return the PhysicalBalanceDetail
	 * @throws ServiceException
	 */
	public PhysicalBalanceDetail getPhysicalBalance(CommonUpdateBeanInf commonInfo) throws ServiceException{
	
		
		StringBuffer strSql = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters = new HashMap<String, Object>();
		strSql.append("select phyBalDetail from PhysicalBalanceDetail phyBalDetail ");
		strSql.append(" where phyBalDetail.physicalCertificate.idPhysicalCertificatePk = :idphysicalcertificate ");
		strSql.append(" and phyBalDetail.holderAccount.idHolderAccountPk = :idHolderAccount ");
		
		parameters.put("idphysicalcertificate", commonInfo.getCertificatePk());
		parameters.put("idHolderAccount", commonInfo.getAccHolderPk());
		PhysicalBalanceDetail phybalDetail = (PhysicalBalanceDetail) findObjectByQueryString(strSql.toString(), parameters);
		
		return phybalDetail;
	}
	
	/**
	 * It save the PhysicalCertMovement data
	 * @param phyCertMov PhysicalCertificateMovement type
	 * @throws ServiceException
	 */
	public void savePhysicalCertMovementService(PhysicalCertificateMovement phyCertMov)
			throws ServiceException {		
			create(phyCertMov);
	}

	

	/**
	 * It get physicalCertificate data
	 * @param idPhysicalCertificate long type
	 * @return the PhysicalCertificate
	 * @throws ServiceException
	 */
	public PhysicalCertificate getLastModifiedDateDetails(long idPhysicalCertificate) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer(
				"Select pc from PhysicalCertificate pc where pc.idPhysicalCertificatePk=:idPhysicalCertificatePk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPhysicalCertificatePk", idPhysicalCertificate);
		return (PhysicalCertificate) query.getSingleResult();
	}

	/**
	 * It get physicalCertificateExt data
	 * @param physicalCertificateFilter PhysicalCertificateFilter type
	 * @param statesPhysical List<Integer> type
	 * @return the List PhysicalCertificateFilter
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	public List<PhysicalCertificateExt> getPhysicalCertificateRetirementReversion(PhysicalCertificateFilter physicalCertificateFilter,
			List<Integer> statesPhysical )throws ServiceException{
		List<Object[]> list = new ArrayList<Object[]>();
		List<PhysicalCertificateExt> physicalCertificateExts = new ArrayList<PhysicalCertificateExt>();
		StringBuffer query = 
					new StringBuffer("Select pc," +
											"pco.participant," +
											"pco.custodyOperation.operationNumber,"+
											"pod.state," +
											"pod.idPhysicalOperationDetail," +
											"pod," +
											"pco.state, " +
											"pco.custodyOperation.idCustodyOperationPk, "+
											"pco "+
									 "from PhysicalCertificate pc," +
									 	  "PhysicalOperationDetail  pod," +
									 	  "PhysicalCertificateOperation pco "+					 	  
									 	  "where "+
									 "pco.physicalOperationType = :physicalOperationType and " +
									 "pco.custodyOperation.idCustodyOperationPk = pod.physicalCertificateOperation.custodyOperation.idCustodyOperationPk and " +
									 "pc.idPhysicalCertificatePk = pod.physicalCertificate.idPhysicalCertificatePk " +
									 "and pod.state in :lstStatus " +
									 "and TRUNC(pod.registryDate) >= TRUNC(:initialDate)");		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("physicalOperationType", new Long(RequestType.RETIRO.getCode()));
		parameters.put("initialDate", physicalCertificateFilter.getInitialDate());
		parameters.put("lstStatus", statesPhysical);		
		list = findListByQueryString(query.toString(), parameters);
		if(list != null){
			PhysicalCertificateExt physicalCertificateExt = null;
			for (Object[] object : list) {
				physicalCertificateExt = new PhysicalCertificateExt();
				physicalCertificateExt.setPhysicalCertificate((PhysicalCertificate)object[0]);
				physicalCertificateExt.setParticipantPk(((Participant)object[1]).getIdParticipantPk());
				physicalCertificateExt.setRequestNumber((Long)object[2]);
				physicalCertificateExt.setState((Integer)object[3]);
				physicalCertificateExt.setIdPhysicalOperationDetail((Long)object[4]);
				physicalCertificateExt.setPhysicalOperationDetail((PhysicalOperationDetail)object[5]);
				physicalCertificateExt.setRequestState((Integer)object[6]);
				physicalCertificateExt.setIdCustodyOperationPk((Long)object[7]);
				physicalCertificateExt.setPhysicalCertificateOperation((PhysicalCertificateOperation)object[8]);
				physicalCertificateExts.add(physicalCertificateExt);
			}
		}		
		return physicalCertificateExts;
	}
	
	/**
	 * Gets xmlSolicitudActivoFinanciero
	 * @param listPhysicalCertificates
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public XmlSolicitudActivoFinanciero getSolicitudDepositoActivoFinanciero(PhysicalCertificateExt[] listPhysicalCertificates, LoggerUser loggerUser) throws ServiceException{
		XmlSolicitudActivoFinanciero solicitudActivoFinanciero = new XmlSolicitudActivoFinanciero();
		List<XmlActivoFinanciero> xmlActivoFinancieros = new ArrayList<>();
		XmlActivoFinanciero xmlActivoFinanciero;
		
		for(PhysicalCertificateExt physicalCertificate: listPhysicalCertificates){
			xmlActivoFinanciero = getPhysicalCertificate(physicalCertificate);
			xmlActivoFinanciero.setTipo("FISICO");
			xmlActivoFinancieros.add(xmlActivoFinanciero);
		}
		
		solicitudActivoFinanciero.setTipoSolicitud("DEP");
		solicitudActivoFinanciero.setMotivo("DEPOSITO TITULOS FISICOS");
		solicitudActivoFinanciero.setUsuarioSolicitante(loggerUser.getUserName());
		solicitudActivoFinanciero.setIpAlta(loggerUser.getIpAddress());
		solicitudActivoFinanciero.setListActivoFinanciero(xmlActivoFinancieros);
		
		return solicitudActivoFinanciero;
	}
	
	/**
	 * Gets xmlSolicitudActivoFinanciero
	 * @param listPhysicalCertificates
	 * @param loggerUser
	 * @return
	 * @throws ServiceException
	 */
	public XmlSolicitudActivoFinanciero getSolicitudRetiroActivoFinanciero(PhysicalCertificateExt[] listPhysicalCertificatesExt, LoggerUser loggerUser){
		XmlSolicitudActivoFinanciero solicitudActivoFinanciero = new XmlSolicitudActivoFinanciero();
		List<XmlActivoFinanciero> xmlActivoFinancieros = new ArrayList<>();
		XmlActivoFinanciero xmlActivoFinanciero;
		
		for(PhysicalCertificateExt physicalCertificateExt: listPhysicalCertificatesExt){
			xmlActivoFinanciero = new XmlActivoFinanciero();
			xmlActivoFinanciero.setClaseClaveValor(physicalCertificateExt.getPhysicalCertificate().getSecurities().getIdSecurityCodePk());
			xmlActivoFinanciero.setNroTitulo( Long.valueOf(physicalCertificateExt.getPhysicalCertificate().getCertificateNumber()) );
			xmlActivoFinanciero.setNroSolicitud(0);
			xmlActivoFinanciero.setTipo("FISICO");
			xmlActivoFinancieros.add(xmlActivoFinanciero);
		}
		
		solicitudActivoFinanciero.setTipoSolicitud("RET");
		solicitudActivoFinanciero.setMotivo("RETIRO TITULOS FISICOS");
		solicitudActivoFinanciero.setUsuarioSolicitante(loggerUser.getUserName());
		solicitudActivoFinanciero.setIpAlta(loggerUser.getIpAddress());
		solicitudActivoFinanciero.setListActivoFinanciero(xmlActivoFinancieros);
		
		return solicitudActivoFinanciero;
	}
	
	/**
	 * Gets data physical certificate by ids
	 */
	public XmlActivoFinanciero getPhysicalCertificate(PhysicalCertificateExt physicalCertificateExt) throws ServiceException {
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append(" SELECT                                                                                                        ");
		querySql.append(" distinct (co.OPERATION_NUMBER),                                                                               ");
		querySql.append(" pc.CERTIFICATE_NUMBER,                                                                                        ");
		querySql.append(" i.MNEMONIC,                                                                                                   ");
		querySql.append(" TO_CHAR(pod.REGISTRY_DATE, 'DD/MM/YYYY HH:MI') as REGISTRY_DATE,                                              ");
		querySql.append(" (select TEXT1 from PARAMETER_TABLE where PARAMETER_TABLE_PK = s.SECURITY_CLASS) as SECURITY_CLASS_DESC,       ");
		querySql.append(" pc.ID_SECURITY_CODE_FK,                                                                                       ");
		querySql.append(" pc.NOMINAL_VALUE,                                                                                             ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = pc.CURRENCY) as CURRENCY_DESC,            ");
		querySql.append(" p.MNEMONIC || ' - ' ||p.ID_PARTICIPANT_PK|| ' - ' ||p.DESCRIPTION as participant_DESC,                        ");
		querySql.append(" (SELECT LISTAGG(HAD.ID_HOLDER_FK , ',' ||CHR(13) ) WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)                   ");
		querySql.append("  FROM HOLDER_ACCOUNT_DETAIL HAD                                                                               ");
		querySql.append("  WHERE HAD.ID_HOLDER_ACCOUNT_FK = pco.ID_HOLDER_ACCOUNT_FK) HOLDERS,                                          ");
		querySql.append(" TO_CHAR(pc.ISSUE_DATE, 'DD/MM/YYYY') as ISSUE_DATE,                                                           ");
		querySql.append(" TO_CHAR(pc.EXPIRATION_DATE, 'DD/MM/YYYY') as EXPIRATION_DATE,                                                 ");
		querySql.append(" TO_CHAR(co.LAST_MODIFY_DATE, 'DD/MM/YYYY HH:MI') as CONFIRMATION_DATE,                                        ");
		querySql.append(" (select DESCRIPTION from PARAMETER_TABLE where PARAMETER_TABLE_PK = pc.STATE) as STATE_DESC,                  ");
		querySql.append(" pod.IND_APPLIED                                                                                               ");
		querySql.append(" FROM PHYSICAL_CERTIFICATE pc                                                                                  ");
		querySql.append(" INNER JOIN PHYSICAL_OPERATION_DETAIL pod on pod.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK    ");
		querySql.append(" INNER JOIN PHYSICAL_CERTIFICATE_OPERATION pco on pco.ID_PHYSICAL_OPERATION_PK = pod.ID_PHYSICAL_OPERATION_FK 	");
		querySql.append(" INNER JOIN CUSTODY_OPERATION co on co.ID_CUSTODY_OPERATION_PK = pco.ID_PHYSICAL_OPERATION_PK                  ");
		querySql.append(" INNER JOIN SECURITY s on s.ID_SECURITY_CODE_PK = pc.ID_SECURITY_CODE_FK                                       ");
		querySql.append(" INNER JOIN ISSUER i  ON i.ID_ISSUER_PK = s.ID_ISSUER_FK                                                       ");
		querySql.append(" INNER JOIN PARTICIPANT p on p.ID_PARTICIPANT_PK = pco.ID_PARTICIPANT_FK                                       ");
		querySql.append(" INNER JOIN HOLDER_ACCOUNT_DETAIL had on had.ID_HOLDER_ACCOUNT_FK = pco.ID_HOLDER_ACCOUNT_FK                   ");
		querySql.append(" where 1=1                                                                                                     ");
		querySql.append(" and co.ID_CUSTODY_OPERATION_PK like :idCustodyOperationPk                                               ");
		querySql.append(" and pc.state = 601                                                             								");
		//Adicionando mas validaciones cuando tienen el mismo CustodyOperation
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate())){
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate().getIdSecurityCodePk()))
				querySql.append(" and pc.id_security_code_Fk = :parIdSecurityCodePk                                               ");
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate().getCertificateNumber()))
				querySql.append(" and pc.CERTIFICATE_NUMBER = :parCertificateNumber                                               ");	
		}
		
		Query query = em.createNativeQuery(querySql.toString());
		query.setParameter("idCustodyOperationPk",physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificateOperation().getIdPhysicalOperationPk());
		//Adicionando mas validaciones cuando tienen el mismo CustodyOperation
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate())){
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate().getIdSecurityCodePk()))
			query.setParameter("parIdSecurityCodePk",physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate().getIdSecurityCodePk());
			if(Validations.validateIsNotNullAndNotEmpty(physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate().getCertificateNumber()))
			query.setParameter("parCertificateNumber",physicalCertificateExt.getPhysicalOperationDetail().getPhysicalCertificate().getCertificateNumber());	
		}
		
		Object[] result = (Object[]) query.getSingleResult();
		
		XmlActivoFinanciero xmlActivoFinanciero = new XmlActivoFinanciero();
		xmlActivoFinanciero.setNroSolicitud(Long.parseLong(result[0].toString()));
		if(Validations.validateIsNotNullAndNotEmpty(result[1]))
		xmlActivoFinanciero.setNroTitulo(Long.parseLong(result[1].toString()));
		xmlActivoFinanciero.setEmisor((String)result[2]);
		xmlActivoFinanciero.setFechaSolicitud((String)result[3]);
		xmlActivoFinanciero.setClaseValor((String)result[4]);
		xmlActivoFinanciero.setClaseClaveValor((String)result[5]);
		xmlActivoFinanciero.setValorNominal((BigDecimal)result[6]);
		xmlActivoFinanciero.setMoneda((String)result[7]);
		xmlActivoFinanciero.setParticipante((String)result[8]);
		xmlActivoFinanciero.setCui((String)result[9]);
		xmlActivoFinanciero.setFechaEmision((String)result[10]);
		xmlActivoFinanciero.setFechaVencimiento((String)result[11]);
		xmlActivoFinanciero.setFechaConfirmacion((String)result[12]);
		xmlActivoFinanciero.setEstado((String)result[13]);
		BigDecimal indApplied = (BigDecimal)result[14];
		xmlActivoFinanciero.setTituloAplicado(indApplied.intValue());
		
		return xmlActivoFinanciero;
			
	}
	
}
