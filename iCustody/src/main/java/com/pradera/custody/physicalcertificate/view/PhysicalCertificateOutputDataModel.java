package com.pradera.custody.physicalcertificate.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateLocationOutputTO;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateOutputDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class PhysicalCertificateOutputDataModel extends ListDataModel<PhysicalCertificateLocationOutputTO> implements SelectableDataModel<PhysicalCertificateLocationOutputTO>,Serializable{
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new physical certificate output data model.
	 */
	public PhysicalCertificateOutputDataModel() {
		
	}
	
	/**
	 * Instantiates a new physical certificate output data model.
	 *
	 * @param lstPhysicalCertificate the lst physical certificate
	 */
	public PhysicalCertificateOutputDataModel(List<PhysicalCertificateLocationOutputTO> lstPhysicalCertificate) {
		super(lstPhysicalCertificate);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public PhysicalCertificateLocationOutputTO getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<PhysicalCertificateLocationOutputTO> ListPhysicalCertificate = (List<PhysicalCertificateLocationOutputTO>)getWrappedData();
		for(PhysicalCertificateLocationOutputTO physicalCertificateTO : ListPhysicalCertificate){
			if(new Long(physicalCertificateTO.getSequence()).toString().equals(rowKey))
				return physicalCertificateTO;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(PhysicalCertificateLocationOutputTO physicalCertificateTO) {//Return Object
		// TODO Auto-generated method stub
		return physicalCertificateTO.getSequence();
	}
}
