package com.pradera.custody.physicalcertificate.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.dematerializationcertificate.to.RegisterAccountAnnotationTO;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.facade.ReteirementDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.service.CertificateDepositServiceBean;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDataFilterBean;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDepositeFilterBean;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

/**
 * <ul>
 * <li>Copyright 2012 pradera Software Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ReteirementDepositeBean
 * 
 * @Project : praderaCustody
 * @author : praderaSoftware.
 * @Creation_Date :
 * @version 1.1
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReteirementDepositeBean extends GenericBaseBean implements
		Serializable {

	/**
	 
	 */
	private static final long serialVersionUID = 1L;

	@Inject @Configurable 
	Integer countryResidence;
	
	@EJB
	private ReteirementDepositeServiceFacade withdrawalDepositeFacadeBean;

	@EJB
	private CertificateDepositeServiceFacade certificateDepositeBeanFacade;

	@Inject
	private IssuerHelperBean issuerHelper;

	@Inject
	private UserInfo userInfo;
	
	@Inject
	private UserPrivilege userPrivilege;

	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;

	@EJB
	NotificationServiceFacade notificationServiceFacade;

	/** The transfer available facade. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailableFacade;
	
	@EJB
    private HelperComponentFacade helperComponentFacade;
	
	@EJB
	private CertificateDepositServiceBean certificateDepositServiceBean;
	
	@EJB
	private DematerializationCertificateFacade dematerializationCertificateFacade;
	
	private List<Participant> lstParticpant = new ArrayList<Participant>();
	private Participant participant = new Participant();
	private Long participantSelected;
	private Long participantSelectedTmp;
	private HolderAccount holderAccount = new HolderAccount();
	private String issuerSearchPK;
	private Long docNumber;
	private Integer stateSelected;
	private List<ParameterTable> certificateDepositeEstatusTypeList;
	private List<ParameterTable> reasonTypeList;
	private Long selectedReqNumber;
	private Date initialDate=CommonsUtilities.currentDate();
	private Date finalDate=CommonsUtilities.currentDate();
	private Date maxDateAllow = CommonsUtilities.currentDate();
	private Date maxDateRetirement = CommonsUtilities.currentDate();
	private Date fechaMaxima = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), 1);
	private String confirmAlertAction = "";
	private Integer reasonSelected = null;
	private String rejectOther = null;
	private Integer otherRejectMotiveId = 726;
	private HolderAccount holderAccountDetail;
	private String textObservation;
	private List<String> certificatesNums = new ArrayList<String>();

	private GenericDataModel<PhysicalCertificateExt> certificateDepositeDataModel;
	private List<PhysicalCertificateExt> certificateDepositeList;
	private PhysicalCertificateExt[] certificateDepositeSelected;
	private PhysicalCertificateExt selectedReteirementDepositDetail;
	ReteirementDepositeFilterBean certificateDelete;

	private static final Integer CANCEL_TYPE_MASTERKEY = 287;
	private static final Integer RETIRESTATE = 606;
	private static final Integer DOMINICA_GEOGRAPHIC_CODE = 271;
	private static final Integer SECURITYCLASS_MASTERPK = 52;
	private static final Integer INSTRUMENT_TYPE_MASTERKEY = 50;
	private static final Integer OPEARTION_TYPE_MASTERKEY = 272;
	private static final Integer CERTIFICATE_STATUS_MASTERKEY = 253;
	/* Permisos */
	private Long participantLogin = new Long(0);
	private boolean isParticipant = false;
	private boolean isCevaldom = false;
	private boolean isBcr = false;
	private boolean flagRegister = false;
	private boolean flagSearch = false;
	private boolean registersFound = true;

	private static final int operationApprove = 1;
	private static final int operationCancel = 2;
	private static final int operationConfirm = 3;
	private static final int operationApply = 5;
	private static final int operationAuthorized = 6;

	/* nw */
	private Integer selectedCurrencyLocal;
	private Integer selectedCurrencyUSP;
	private Integer selectedCurrencyEUR;
	private Integer selectedCurrencyUFV;
	private Integer selectedCurrencyDMV;
	
	private BigDecimal selectedNominalUSP;
	private BigDecimal selectedNominalLocal;
	private BigDecimal selectedNominalEUR;
	private BigDecimal selectedNominalUFV;
	private BigDecimal selectedNominalDMV;


	private Integer selectedInstrument;
	private Integer selectedSecurityClass;
	private HolderAccount holderAccSearch;
	private String issuerPk;
	private Issuer issuerHelp = new Issuer();
	private Security securityHelp = new Security();
	private Date deliverReqDate;
	private ReteirementDepositeFilterBean showSelected;
	private Date maxNextWorkDay;
	private Date currentDate;

	private List<ReteirementDepositeFilterBean> physicalCertificateListAdded;
	private ReteirementDepositeFilterBean[] phyCertificateRemoveSelected;
	private GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateDataModel;
	private ReteirementDepositeFilterBean[] lstPhysicalCertificateSelected;
	private List<ReteirementDepositeFilterBean> physicalCertificateList;
	private GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateListAddedDataModel;
	private boolean renderedInstrumentType = true;
	

	/** The instrument type list. */
	private List<ParameterTable> instrumentTypeList = new ArrayList<ParameterTable>();

	/** The instrument type map. */
	private Map<Integer, String> instrumentTypeMap = new HashMap<Integer, String>();

	/** The payment type list. */
	private List<ParameterTable> paymentTypeList = new ArrayList<ParameterTable>();

	/** The depositRequest type list. */
	private List<ParameterTable> depositRequestStateList = new ArrayList<ParameterTable>();

	/** The depositState type list. */
	private List<ParameterTable> depositStateList = new ArrayList<ParameterTable>();

	/** The ReasonState type list. */
	private List<ParameterTable> ReasonStateList = new ArrayList<ParameterTable>();

	/** The payment type map. */
	private Map<Integer, String> paymentTypeMap = new HashMap<Integer, String>();

	/** The depositRequestState type map. */
	private Map<Integer, String> depositRequestStateTypeMap = new HashMap<Integer, String>();

	/** The depositState type map. */
	private Map<Integer, String> depositStateTypeMap = new HashMap<Integer, String>();

	/** The reasonState type map. */
	private Map<Integer, String> reasonStateTypeMap = new HashMap<Integer, String>();

	/** The money type list. */
	private List<ParameterTable> moneyTypeList = new ArrayList<ParameterTable>();

	/** The money type map. */
	private Map<Integer, String> moneyTypeMap = new HashMap<Integer, String>();

	/** The security class list. */
	private List<ParameterTable> securityClassList = new ArrayList<ParameterTable>();

	/** The security class type map. */
	private Map<Integer, String> securityClassTypeMap = new HashMap<Integer, String>();

	//helpers
	private Holder sourceHolder = new Holder();
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	Map<Integer, String> mapStateHolderAccount = new HashMap<Integer,String>();
	private List<ParameterTable> lstHolderAccountState = new ArrayList<ParameterTable>();

	private PhysicalCertificateExt dialogPhysicalCertificate = new PhysicalCertificateExt();
	
	private ParameterTableTO paramTabTO = new ParameterTableTO();
	
	private Map<Integer, String> mpStates, mpMnemonicClassSec;
	@PostConstruct
	public void init() {
		try {
			securityEvents();
			loadParticipant();
			loadParameterReason();
			initialDate =CommonsUtilities.currentDate();
			finalDate =CommonsUtilities.currentDate();
			otherRejectMotiveId = 726;
			certificateDepositeList = new ArrayList<PhysicalCertificateExt>();
			certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>();
			loadCertificateDepositeEstatus();
			loadHolderAccountState();
			sourceHolder = new Holder();
			sourceAccountTo = new HolderAccountHelperResultTO();
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			loadListAndMap();
			listHolidays();
			disabledButtons();
			loadIntrumentTypes();

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void backDialogDetail(){
		JSFUtilities.executeJavascriptFunction("PF('idCertificateRegisterW').hide();");
	}
	public PhysicalCertMarketfact getPhysicalCertMarketfact(Long idPhysicalCertificatePk){
		return withdrawalDepositeFacadeBean.getPhysicalCertMarketfact(idPhysicalCertificatePk);
	}
	public void loadIntrumentTypes(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			instrumentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		for (ParameterTable param : instrumentTypeList) {
			instrumentTypeMap.put(param.getParameterTablePk(), param.getParameterName());
	}
	}
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	private void loadHolderAccountState() throws ServiceException{
		lstHolderAccountState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstHolderAccountState.add(param);
			mapStateHolderAccount.put(param.getParameterTablePk(), param.getDescription());
		}
	}
    
    public String getMapHolderAccountState(Integer code){
		return mapStateHolderAccount.get(code);
	}
    
    public void validSourceHolder(){
		
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())){
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sourceHolder.getIdHolderPk());
			holderTO.setParticipantFk(participantSelected);
			holderTO.setAccountState(HolderAccountStatusType.ACTIVE.getCode());
			
			try {
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if(lstAccountTo.size()>0){
					if(lstAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
						sourceAccountTo=lstAccountTo.get(0);
						validSourceHolderAccount();
						
					}
					lstSourceAccountTo = lstAccountTo;
				} else{
					sourceHolder = new Holder();
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = new HolderAccountHelperResultTO();
					//holderAccount = new HolderAccount();
					//String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT);
					String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED);
					alert(mensaje);
				}
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		}
	}
    
	public void securityEvents() {

		try {

			if (userPrivilege.getUserAcctions().isRegister())
				flagRegister = true;

			if (userPrivilege.getUserAcctions().isSearch())
				flagSearch = true;

			participantLogin = userInfo.getUserAccountSession()
					.getParticipantCode();

			if (userInfo.getUserAccountSession().isInstitution(InstitutionType.BCR)) {

				isBcr = true;

			} else if (userInfo.getUserAccountSession().isParticipantInstitucion()) {
				participantSelected = participantLogin;
				participant = certificateDepositeBeanFacade.findParticipant(participantLogin);
				isParticipant = true;

				if (participant.getState().equals(
						ParticipantStateType.BLOCKED.getCode())) {
					flagRegister = false;
				}

			} else { // si no es niuno es cevaldom
				isCevaldom = true;
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clearSearchRetirementPhysicalCertificates() {
		try {
			holderAccount = new HolderAccount();
			participantSelected = null;
			stateSelected = null;
			issuerSearchPK = null;
			textObservation = null;
			initialDate =CommonsUtilities.currentDate();
			finalDate = CommonsUtilities.currentDate();
			docNumber = null;
			selectedReqNumber = null;
			certificateDepositeList = new ArrayList<PhysicalCertificateExt>();
			certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>();
			sourceHolder = new Holder();
			registersFound = true;
			issuerHelp= new Issuer();
			disabledButtons();
			securityEvents();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadListAndMap() {
		try {

			if (ReasonStateList == null || ReasonStateList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.REASON_REJECT_ANULLED_REQUEST.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				ReasonStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : ReasonStateList) {
					reasonStateTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (depositStateList == null || depositStateList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.DEPOSIT_STATE.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				depositStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : depositStateList) {
					depositStateTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (depositRequestStateList == null || depositRequestStateList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.DEPOSIT_STATE_REQUEST.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				depositRequestStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : depositRequestStateList) {
					depositRequestStateTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (instrumentTypeList == null || instrumentTypeList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				instrumentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : instrumentTypeList) {
					instrumentTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (paymentTypeList == null || paymentTypeList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PERIODICITY.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paymentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : paymentTypeList) {
					paymentTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (moneyTypeList == null || moneyTypeList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				moneyTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : moneyTypeList) {
					moneyTypeMap.put(param.getParameterTablePk(),param.getText1());
				}

			}
			fillSecurityClass();

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void fillSecurityClass() throws ServiceException{
		securityClassList = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		paramTable.setOrderByText1(1);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			securityClassList.add(param);
		}
		if(super.getParametersTableMap()==null){
			super.setParametersTableMap(new HashMap<Integer,Object>());
		}
		billParametersTableMap(securityClassList, true);
	}
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
//	public void fillSecurityClass() {
//		mpStates=new HashMap<>();
//		mpMnemonicClassSec=new HashMap<>();
//		paramTabTO = new ParameterTableTO();
//		paramTabTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
//		List<ParameterTable> lstSecClasses;
//		try {
//			lstSecClasses = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
//			for(ParameterTable paramTab:lstSecClasses){
//				if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()))
//					securityClassList.add(paramTab);
//				mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
//				mpMnemonicClassSec.put(paramTab.getParameterTablePk(), paramTab.getText1());
//			}
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		}
//	}
	@LoggerAuditWeb
	public void searchRetirementPhysicalCertificates(){
		boolean requiredFields = true;

		if(requiredFields){
			try {
				
				PhysicalCertificateFilter physicalCertificateFilter = new PhysicalCertificateFilter();
				

				if(Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()))
					physicalCertificateFilter.setCuiHolder(sourceHolder.getIdHolderPk());
				
				if(Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk()))
					physicalCertificateFilter.setHolderAccountPk(holderAccount.getIdHolderAccountPk());
	
				if(Validations.validateIsNotNullAndPositive(participantSelected))
					physicalCertificateFilter.setParticipantPk(participantSelected);
	
				if(Validations.validateIsNotNullAndPositive(stateSelected))
					physicalCertificateFilter.setStatus(stateSelected);
	
				if(Validations.validateIsNotNull(issuerSearchPK))
					physicalCertificateFilter.setIssuerPk(issuerSearchPK);
	
				if(Validations.validateIsNotNull(initialDate))
					physicalCertificateFilter.setInitialDate(initialDate);
	
				if(Validations.validateIsNotNull(finalDate))
					physicalCertificateFilter.setFinalDate(CommonsUtilities.addDate(finalDate,1));
	
				if(Validations.validateIsNotNullAndPositive(docNumber))
					physicalCertificateFilter.setCertificateNumber(docNumber);
	
				if(Validations.validateIsNotNullAndPositive(selectedReqNumber))
					physicalCertificateFilter.setRequestNumber(selectedReqNumber);
				
				List<PhysicalCertificateExt> searchData = withdrawalDepositeFacadeBean.getPhysicalCertificateRetirementSearchList(physicalCertificateFilter);
				loadListAndMap();
				
				certificateDepositeList = new ArrayList<PhysicalCertificateExt>();
				certificateDepositeList = searchData;
				ReteirementDepositeFilterBean modifyData = null;
				
				if(searchData != null && searchData.size()>0 ){

			    	
					for (PhysicalCertificateExt certificate : searchData) {
    					
    					certificate.setStateType(depositStateTypeMapDescription(certificate.getState()));
    					
    					if(certificate.getPhysicalCertificate().getSecurityClass() != null)
    						certificate.getPhysicalCertificate().setClassType(securityClassTypeMapDescription(certificate.getPhysicalCertificate().getSecurityClass()));
    					
    					if(certificate.getPhysicalOperationDetail().getState() != null)
    						certificate.getPhysicalCertificate().setStateType(depositStateTypeMapDescription(certificate.getPhysicalOperationDetail().getState()));
    					
    					if (certificate.getPhysicalOperationDetail().getRejectMotive() != null){
    						certificate.getPhysicalOperationDetail().setMotiveType(generalParametersFacade.getParameterTableById(certificate.getPhysicalOperationDetail().getRejectMotive()).getParameterName());
    					}
    					
    					if (certificate.getPhysicalCertificate().getCurrency() != null)
    						certificate.getPhysicalCertificate().setCurrencyType(moneyTypeMap.get(certificate.getPhysicalCertificate().getCurrency()));
    					
    					if (Validations.validateIsNotNullAndPositive(certificate.getParticipantPk())) {
    						certificate.setMnemonic(findParticipantSelected(certificate.getParticipantPk()).getMnemonic());
    					}
    					
    					if(certificate.getRequestState() != null){
    						certificate.setRequestStateType(depositRequestStateTypeMapDescription(certificate.getRequestState()));
    					}
    					
    					if(certificate.getPhysicalOperationDetail().getIndApplied().equals(0)){
    						certificate.setIndApplied(false);
    					}else if(certificate.getPhysicalOperationDetail().getIndApplied().equals(1)){
    						certificate.setIndApplied(true);
    					}
    					
    					if (BooleanType.YES.getCode().equals(certificate.getPhysicalOperationDetail().getIndDematerialized())) {
    						certificate.setIndDematerialization(true);
    					} else {
    						certificate.setIndDematerialization(false);
    					}
    				}
    				
    				certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>(searchData);
    				registersFound = true;
    				
    				if( Validations.validateIsNotNullAndPositive(participant.getState()) &&
    					participant.getState().equals(ParticipantStateType.BLOCKED.getCode())
    				){
    					disabledButtons();
    				}else{
    					enabledButtons();	
    				}
    				
				}else{
					disabledButtons();
					certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>();
					registersFound = false;
				}
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
	/**
	 * Instrument type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String instrumentTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return instrumentTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	/**
	 * Payment type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String paymentTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return paymentTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Money type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String moneyTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return moneyTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Security class type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String securityClassTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return securityClassTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	public String depositRequestStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return depositRequestStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	public String depositStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return depositStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	public String reasonStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return reasonStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	@LoggerAuditWeb
	public String aproveCertificates(boolean single) {
		confirmAlertAction = "retirementCertificateApprove";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.APPROVED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.APPROVED.getCode(), certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<String>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber());
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.REGISTERED.getCode(),StateType.APPROVED.getCode(), certs);
		}
	}
	
	@LoggerAuditWeb
	public String cancelCertificates(boolean single) {

		if (single) {
			alertAnulledOrReject(new Integer(StateType.ANNULLED.getCode()));
			return "";
		} else {
			certificatesNums = new ArrayList<String>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber());
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.REGISTERED.getCode(),StateType.ANNULLED.getCode(),certs); 
		}
	}
	
	@LoggerAuditWeb
	public String rejectCertificates(boolean single) {
		
		if (single) {
			alertAnulledOrReject(new Integer(StateType.REJECTED.getCode()));
			return "";
		} else {
			certificatesNums = new ArrayList<String>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber());
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.APPROVED.getCode(),StateType.REJECTED.getCode(),certs);	
		}
	}
	
	@LoggerAuditWeb
	public String applyCertificates(boolean single) {
		
		confirmAlertAction = "retirementCertificateApply";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.APPLY
					.getCode());
			String bodyMessage = confirmbodyMessage(StateType.APPLY.getCode(),
					certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<String>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber());
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.CONFIRMED.getCode(),StateType.APPLY.getCode(),certs);
		}
	}

	@LoggerAuditWeb
	public String confirmCertificates(boolean single) {

		confirmAlertAction = "retirementCertificateConfirm";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.CONFIRMED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.CONFIRMED.getCode(), certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<String>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber());
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.APPROVED.getCode(), StateType.CONFIRMED.getCode(),certs);
		}
	}

	@LoggerAuditWeb
	public String authorizeCertificates(boolean single) {
		confirmAlertAction = "retirementCertificateAuthorization";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.AUTHORIZED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.AUTHORIZED.getCode(), certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<String>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber());
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.CONFIRMED.getCode(),StateType.AUTHORIZED.getCode(),certs);
		}
	}

	public String ejecutarAccion(Integer actualState, Integer newState, String certs) {
		String certificateValidated=null;
		if (certificateDepositeSelected != null && certificateDepositeSelected.length > 0) {

			if (certificateDepositeSelected.length == 1) {

				if (certificateDepositeSelected[0].getState().equals(StateType.CONFIRMED.getCode())
					&& Validations.validateIsNotNull(certificateDepositeSelected[0].getIndApplied())
					&& !newState.equals(StateType.AUTHORIZED.getCode())) {
					
					if (certificateDepositeSelected[0].getIndApplied()) {

						String messageBody = PropertiesUtilities.getMessage(
								PropertiesConstants.MSG_APPLIED_PREVIOUSLY,
								new Object[] { certificateDepositeSelected[0].getPhysicalCertificate().getCertificateNumber().toString() });

						alert(messageBody);
						return "";
					}
				}

				if (certificateDepositeSelected[0].getState().equals(StateType.CONFIRMED.getCode())
					&& Validations.validateIsNotNull(certificateDepositeSelected[0].getIndApplied())
					&& newState.equals(StateType.AUTHORIZED.getCode())) {
					if (!certificateDepositeSelected[0].getIndApplied()) {

						String messageBody = PropertiesUtilities.getMessage(PropertiesConstants.ERRO_AUTHORIZE, 
								new Object[] { certificateDepositeSelected[0].getPhysicalCertificate().getCertificateNumber().toString() });
						alert(messageBody);
						return "";
					}
				}
				certificateValidated=validLisCertificateForState(actualState, newState);
				if (certificateValidated.equals(GeneralConstants.EMPTY_STRING)) {
					if (certificateDepositeSelected[0].getState().equals(actualState)) {
						// Redireccionar al detalle para ejecutar la accion
						enabledButtonsWithState(newState.intValue());
						return viewCertificationDetail(certificateDepositeSelected[0]);
					} else {
						// manda alerta de que no se puede ejecutar la accion
						String messageBody = previouslyMessageAlert(newState);
						alert(messageBody);
					}
				} else {
					String messageBody = errorStateMessageAlert(newState,certificateValidated);
					alert(messageBody);
				}

			} else {
				certificateValidated=validLisCertificateForState(actualState, newState);
				if(certificateValidated.equals(GeneralConstants.EMPTY_STRING)) {

					if (newState.equals(StateType.ANNULLED.getCode()) || newState.equals(StateType.REJECTED.getCode())) {

						alertAnulledOrReject(newState);

					} else {

						// Mando el dialogo a ejecutar la accion multiple
						certs = certificateNumberMessage(certificatesNums);
						String headerMessage = confirmHeaderMessage(newState);
						String bodyMessage = confirmbodyMessage(newState, certs);
						question(headerMessage, bodyMessage);
					}

				} else {
					// manda alerta de que no se puede ejecutar la accion
					String messageBody = errorStateMessageAlert(newState, certificateValidated);
					alert(messageBody);
				}
			}

		} else {
			String headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MODIFY_ELIMINAR_CERTIFICATE_SELECT);
			alert(headerMessage, bodyMessage);
		}

		return "";
	}

	public void changeParticipant() {
		holderAccount = new HolderAccount();
		holderAccount.setAccountNumber(null);
		docNumber = null;
	}

	public void changeParticipantAndDeleteInfo() {

		if (Validations.validateIsNullOrNotPositive(participantSelected)) {
			holderAccount = new HolderAccount();
			holderAccount.setAccountNumber(null);
			docNumber = null;
		} else {
			if (!participantSelected.equals(participantSelectedTmp)) {

				if (physicalCertificateListAdded.size() > 0 || physicalCertificateList.size() > 0) {

					String headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
					String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CHANGE_PARTICIPANT_QUESTION);

					question(headerMessage, bodyMessage);
					confirmAlertAction = "executeChangeParticipant";

				} else {
					participantSelectedTmp = participantSelected;
					sourceHolder = new Holder();
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = null;
					holderAccount = new HolderAccount();
					holderAccount.setAccountNumber(null);
					docNumber = null;
				}
			}
		}
	}

	public void executeChangeParticipant() {

		holderAccount = new HolderAccount();
		issuerPk = null;
		docNumber = null;
		selectedInstrument = null;
		selectedSecurityClass = null;
		physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
		physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();

		deliverReqDate = CommonsUtilities.currentDate();
		finalDate = null;//CommonsUtilities.currentDate();
		initialDate =null; //CommonsUtilities.currentDate();
		maxNextWorkDay = getNextWorkDay(CommonsUtilities.currentDate(), 1);
		registersFound = true;

		participantSelected = participantSelectedTmp;
	}

	/**
	 * Valid lis certificate for state.
	 *
	 * @param actualState the actual state
	 * @param newState the new state
	 * @return the string
	 */
	public String validLisCertificateForState(Integer actualState, Integer newState) {
		String strCertificateValidate=GeneralConstants.EMPTY_STRING;
		certificatesNums = new ArrayList<String>();
		for (PhysicalCertificateExt certificate : certificateDepositeSelected) {

			if (certificate.getState().equals(StateType.CONFIRMED.getCode()) && !newState.equals(StateType.AUTHORIZED.getCode())) {
				if (certificate.getIndApplied())
					strCertificateValidate+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			}

			if (certificate.getState().equals(StateType.CONFIRMED.getCode())
				&& newState.equals(StateType.AUTHORIZED.getCode())) {
				if (!certificate.getIndApplied())
					strCertificateValidate+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			}

			if (!certificate.getState().equals(actualState)) {
				strCertificateValidate+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			} else {
				certificatesNums.add(certificate.getPhysicalCertificate().getCertificateNumber());
			}
		}
		if(strCertificateValidate.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
			strCertificateValidate=strCertificateValidate.substring(0,strCertificateValidate.length()-1);
		return strCertificateValidate;
	}

	@LoggerAuditWeb
	public void retirementCertificateApprove() {
		try {

			if (certificatesNums != null) {
				certificatesNums.clear();
			} else {
				certificatesNums = new ArrayList<String>();
			}

			PhysicalOperationDetail physicalOperationDetail = null;

			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {

				physicalOperationDetail = retiroBean.getPhysicalOperationDetail();
				
				if (physicalOperationDetail.getState().equals(StateType.APPROVED.getCode())) {
					certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());
				}
			}

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<String>();
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());

				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.APPROVED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setOperation(operationApprove);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				certificateDepositeSelected = null;
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);

			}
			certificateDepositeBeanFacade.updateDetailApproveRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.APPROVE_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_APPROVE.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	@LoggerAuditWeb
	public void retirementCertificateAnuller() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<String>();
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setLastModifyApp(new Integer(1));
				commonUpdateBeanInf.setState(StateType.ANNULLED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				if (rejectOther != null) {
					commonUpdateBeanInf.setCancelDesc(rejectOther);
				}
				commonUpdateBeanInf.setRejectMotive(reasonSelected);
				commonUpdateBeanInf.setOperation(operationCancel);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}
			certificateDepositeBeanFacade.updateDetailCancelRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);
			certificateDepositeSelected = null;

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ANULER_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CANCEL.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/

		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}
	
	@LoggerAuditWeb
	public void retirementCertificateRejected() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<String>();
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setLastModifyApp(new Integer(1));
				commonUpdateBeanInf.setState(StateType.REJECTED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				if (rejectOther != null) {
					commonUpdateBeanInf.setCancelDesc(rejectOther);
				}
				commonUpdateBeanInf.setRejectMotive(reasonSelected);
				commonUpdateBeanInf.setOperation(operationCancel);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}
			certificateDepositeBeanFacade.updateDetailCancelRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);
			certificateDepositeSelected = null;

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REJECTED_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CANCEL.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/

		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}

	@LoggerAuditWeb
	public void retirementCertificateApply() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<String>();
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.CONFIRMED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setIntAppilied(1);
				commonUpdateBeanInf.setOperation(operationApply);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				//commonUpdateBeanInf.setSituation(SituationType.DEPOSITARY_MESSENGER.getCode());
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}

			certificateDepositeBeanFacade.updateDetailApplyRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);
			certificateDepositeSelected = null;

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.APLICAR_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_APPLY.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, participantPk, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	@LoggerAuditWeb
	public void retirementCertificateConfirm() {
		try {
			certificatesNums = new ArrayList<String>();
			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.CONFIRMED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setOperation(operationConfirm);
				commonUpdateBeanInf.setIntAppilied(0);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}
			certificateDepositeBeanFacade.updateDetailConfirmDeposit(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CONFIRM.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}

	@LoggerAuditWeb
	public void retirementCertificateAuthorization() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			List<PhysicalCertificate> lstSituation = new ArrayList<PhysicalCertificate>();
			PhysicalCertificate situationCert = null;
			certificatesNums = new ArrayList<String>();

			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				
				certificatesNums.add(retiroBean.getPhysicalCertificate().getCertificateNumber());
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				situationCert = new PhysicalCertificate();
				if (userInfo != null) {
					commonUpdateBeanInf.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
					commonUpdateBeanInf.setCurrentUserName(userInfo.getUserAccountSession().getUserName());
				}
				commonUpdateBeanInf.setLastModifyApp(new Integer(1));
				commonUpdateBeanInf.setState(StateType.AUTHORIZED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setStockQuantity(retiroBean.getPhysicalCertificate().getCertificateQuantity());
				//commonUpdateBeanInf.setPhysicalBalanceDetailPk(retiroBean.getIdPhysicalOperationDetail());
				commonUpdateBeanInf.setOperation(operationAuthorized);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				commonUpdateBeanInf.setCurrency(retiroBean.getPhysicalCertificate().getCurrency());
				commonUpdateBeanInf.setNominalValue(retiroBean.getPhysicalCertificate().getNominalValue());
				commonUpdateBeanInf.setRequestState(RETIRESTATE);
				commonUpdateBeanInf.setAccHolderPk(retiroBean.getPhysicalCertificateOperation().getHolderAccount().getIdHolderAccountPk());
				withdrawalDepositeFacadeBean.authorizarFacade(commonUpdateBeanInf);
				withdrawalDepositeFacadeBean.updateSecurities(retiroBean.getPhysicalCertificate().getSecurities(),retiroBean.getPhysicalCertificate().getCertificateQuantity());
				situationCert.setIdPhysicalCertificatePk(commonUpdateBeanInf.getCertificatePk());
				situationCert.setRequestNumber(commonUpdateBeanInf.getIdCustodyOperationPk()); 
				situationCert.setSituation(SituationType.PARTICIPANT.getCode());
				lstSituation.add(situationCert);
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				
				//we verify if the withdrawal of certificate because dematerialized way
				if (BooleanType.YES.getCode().equals(retiroBean.getPhysicalOperationDetail().getIndDematerialized())) {
					createAccountAnnotationOperation(retiroBean);
				}
			}

			certificateDepositeBeanFacade.updateDetailAutorizedRetirement(lstCommonUpdateBeanInf);
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.AUTHORIZAR_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);

		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}

	
	private void createAccountAnnotationOperation(PhysicalCertificateExt retiroBean) throws ServiceException
	{	
		AccountAnnotationOperation objAccountAnnotationOperation= new AccountAnnotationOperation();
		objAccountAnnotationOperation.setHolderAccount(retiroBean.getPhysicalCertificateOperation().getHolderAccount());
		objAccountAnnotationOperation.setSecurity(retiroBean.getPhysicalCertificate().getSecurities());
		objAccountAnnotationOperation.setTotalBalance(retiroBean.getPhysicalCertificate().getCertificateQuantity());
		objAccountAnnotationOperation.setCertificateNumber(retiroBean.getPhysicalCertificate().getCertificateNumber().toString());
		objAccountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
		objAccountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
		objAccountAnnotationOperation.setComments(PropertiesUtilities.getMessage(PropertiesConstants.MSG_COMMENT_WITHDRAWAL_BY_DEMATERIALIZATION));
		
		AccountAnnotationMarketFact objAccountAnnotationMarketFact= new AccountAnnotationMarketFact();
		PhysicalCertMarketfact objPhysicalCertMarketfact= certificateDepositeBeanFacade.getPhysicalCertMarketfact(
																		retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
		objAccountAnnotationMarketFact.setMarketDate(objPhysicalCertMarketfact.getMarketDate());
		objAccountAnnotationMarketFact.setMarketPrice(objPhysicalCertMarketfact.getMarketPrice());
		objAccountAnnotationMarketFact.setMarketRate(objPhysicalCertMarketfact.getMarketRate());
		objAccountAnnotationMarketFact.setOperationQuantity(objPhysicalCertMarketfact.getQuantity());
		
		RegisterAccountAnnotationTO objRegisterAccountAnnotationTO= new RegisterAccountAnnotationTO();
		objRegisterAccountAnnotationTO.setBlLien(false);
		objRegisterAccountAnnotationTO.setAccountAnnotationMarketFact(objAccountAnnotationMarketFact);
		objRegisterAccountAnnotationTO.setAccountAnnotationOperation(objAccountAnnotationOperation);
		
		dematerializationCertificateFacade.registerAccountAnnotationOperation(objRegisterAccountAnnotationTO);
	}
	
	public String previouslyMessageAlert(Integer actualState) {
		if (actualState.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPROVED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_CANCELLED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRMED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECTED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.APPLY.getCode())) { 

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPLIED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.AUTHORIZED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPLIED_PREVIOUSLY_AUTHORIZED);

		}

		return "";
	}

	public String errorStateMessageAlert(Integer actualState, String params) {
		if (actualState.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERRO_APPROVAL,params);

		} else if (actualState.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CANCEL,params);

		} else if (actualState.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CONFIRM,params);

		} else if (actualState.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_REJECT,params);

		} else if (actualState.equals(StateType.APPLY.getCode())) { // APPLY

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_APPLY,params);

		} else if (actualState.equals(StateType.AUTHORIZED.getCode())) { // APPLY

			return PropertiesUtilities.getMessage(PropertiesConstants.ERRO_AUTHORIZE,params);

		}

		return "";
	}

	public String confirmHeaderMessage(Integer state) {

		if (state.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE);

		} else if (state.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL);

		} else if (state.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);

		} else if (state.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT);

		} else if (state.equals(StateType.APPLY.getCode())) { // APPLY

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPLY);

		} else if (state.equals(StateType.AUTHORIZED.getCode())) { // APPLY

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_AUTHORIZE);
		}

		return "";
	}

	public String confirmbodyMessage(Integer state, String certs) {

		if (state.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_CONFIRM_APPROVAL_MULTI, certs);

		} else if (state.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_CONFIRM_ANNULAR_MULTI, certs);

		} else if (state.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_CONFIRM_CERTIFIACTE_MULTI, certs);

		} else if (state.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_REJECT_CERTIFICATE_MULTI, certs);

		} else if (state.equals(StateType.APPLY.getCode())) { 

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_APPLY_OPER_MULTI, certs);

		} else if (state.equals(StateType.AUTHORIZED.getCode())) { 

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_AUTHORIZED_OPER_MULTI, certs);
		}

		return "";
	}

	private String certificateNumberMessage(List<String> certificates) {
		String certs = StringUtils.EMPTY;
		if (certificates != null && certificates.size() > 0) {
			for (String crt : certificates) {
				if (StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)) {
					if(crt!=null)
						certs = crt.toString();
				} else {
					certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt
							.toString()));
				}
			}
		}
		return certs;
	}

	public String backToSearch(boolean flagValida) {
		if (flagValida) {
			if (existChangesInRegistry()) {
				String headerMessage = PropertiesUtilities
						.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
				String bodyMessage = PropertiesUtilities
						.getMessage(PropertiesConstants.MSG_BACK_VALIDATION);

				question(headerMessage, bodyMessage);
				confirmAlertAction = "executeBackToSearch";

				return "";
			} else {
				return "search";
			}
		} else {
			return executeBackToSearch();
		}
	}

	public void validCleanRegistry() {

		if (existChangesInRegistry()) {

			String headerMessage = PropertiesUtilities
					.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities
					.getMessage(PropertiesConstants.LBL_MSG_CLEAR_SCREEN);

			question(headerMessage, bodyMessage);
			confirmAlertAction = "cleanRegistry";
		}
	}

	public void cleanRegistry() {
		participantSelected = null;
		holderAccount = new HolderAccount();
		issuerPk = null;
		issuerHelp = new Issuer();
		securityHelp = new Security();
		docNumber = null;
		textObservation= null;
		selectedInstrument = null;
		selectedSecurityClass = null;
		physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
		physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();

		sourceHolder = new Holder();
		sourceAccountTo = new HolderAccountHelperResultTO();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		
		deliverReqDate = CommonsUtilities.currentDate();
		finalDate = null;//CommonsUtilities.currentDate();
		initialDate = null;//CommonsUtilities.currentDate();
		maxNextWorkDay = getNextWorkDay(CommonsUtilities.currentDate(), 1);
		registersFound = true;
		securityEvents();
    	JSFUtilities.resetViewRoot();
	}

	public String executeBackToSearch() {

		cleanSearchInputData();
		selectedReqNumber = null;
		docNumber = null;
		certificateDepositeSelected = null;
		if (certificateDepositeList.size() > 0)
			enabledButtons();

		return "search";
	}

	public boolean existChangesInRegistry() {
		Integer INT_NO_SELECTED = Integer.parseInt("-1");
		Long LNG_NO_SELECTED = Long.parseLong("-1");
		// VALIDATE IF WE HAVE MODIFIED ANY VALUE
		if ((participantSelected == null || LNG_NO_SELECTED.equals(participantSelected))
				&& holderAccount.getIdHolderAccountPk() == null
				&& (issuerPk == null || StringUtils.EMPTY.equals(issuerPk))
				&& (docNumber == null)
				&& (selectedInstrument == null || INT_NO_SELECTED.equals(selectedInstrument))
				&& (selectedSecurityClass == null || INT_NO_SELECTED.equals(selectedSecurityClass))) {
			return false;
		}
		return true;
	}

	public String viewCertificationDetail(
			PhysicalCertificateExt rowPhysicalCertificatesExt) {
		certificateDepositeSelected = new PhysicalCertificateExt[1];
		certificateDepositeSelected[0] = rowPhysicalCertificatesExt;
		
		if( certificateDepositeSelected[0].getPhysicalOperationDetail().getPhysicalCertificate().getSecurities().getInstrumentType().equals(new Integer(399)) ){
			renderedInstrumentType = false;
		}else{
			renderedInstrumentType = true;
		}
		selectedReteirementDepositDetail = rowPhysicalCertificatesExt;
		holderAccountDetail = certificateDepositeSelected[0].getPhysicalCertificateOperation().getHolderAccount();
				// holderAccount;

		return "singleRecord";
	}

	public String gotoNewRetirementCertificate() {

		try{
		
			clearSearchRetirementPhysicalCertificates();
			securityEvents();
			physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
			physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
			physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
			physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
	
			deliverReqDate = CommonsUtilities.currentDate();
			finalDate =null;//CommonsUtilities.currentDate();
			initialDate =null;// CommonsUtilities.currentDate();//aca
			issuerPk = null;
			maxNextWorkDay = getNextWorkDay(CommonsUtilities.currentDate(), 1);
			loadSecurityClass();
			loadListAndMap();
			registersFound = true;
			
			sourceHolder = new Holder();
			sourceAccountTo = new HolderAccountHelperResultTO();
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>(); 
			listHolidays();
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "registerPage";
	}

	public String goToviewCertificationDetail(
			PhysicalCertificateExt rowPhysicalCertificatesExt) {
		disabledButtons();
		return viewCertificationDetail(rowPhysicalCertificatesExt);
	}

	public void enabledButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (userPrivilege.getUserAcctions().isApprove())
			privilegeComponent.setBtnApproveView(true);

		if (userPrivilege.getUserAcctions().isAnnular())
			privilegeComponent.setBtnAnnularView(true);

		if (userPrivilege.getUserAcctions().isApply())
			privilegeComponent.setBtnApply(true);
		
		if (userPrivilege.getUserAcctions().isReject())
			privilegeComponent.setBtnRejectView(true);

		if (userPrivilege.getUserAcctions().isConfirm())
			privilegeComponent.setBtnConfirmView(true);

		if (userPrivilege.getUserAcctions().isAuthorize())
			privilegeComponent.setBtnAuthorize(true);

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	public void enabledButtonsWithState(Integer state) {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (state.equals(StateType.APPROVED.getCode())) {

			if (userPrivilege.getUserAcctions().isApprove())
				privilegeComponent.setBtnApproveView(true);

		} else if (state.equals(StateType.ANNULLED.getCode())) {

			if (userPrivilege.getUserAcctions().isAnnular())
				privilegeComponent.setBtnAnnularView(true);

		} else if (state.equals(StateType.REJECTED.getCode())) {

			if (userPrivilege.getUserAcctions().isReject())
				privilegeComponent.setBtnRejectView(true);

		} else if (state.equals(StateType.CONFIRMED.getCode())) {

			if (userPrivilege.getUserAcctions().isConfirm())
				privilegeComponent.setBtnConfirmView(true);

		} else if (state.equals(StateType.AUTHORIZED.getCode())) {

			if (userPrivilege.getUserAcctions().isAuthorize())
				privilegeComponent.setBtnAuthorize(true);

		} else if (state.equals(StateType.APPLY.getCode())) { // APLICAR

			if (userPrivilege.getUserAcctions().isApply())
				privilegeComponent.setBtnApply(true);

		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	public void disabledButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		privilegeComponent.setBtnApproveView(false);
		privilegeComponent.setBtnAnnularView(false);
		privilegeComponent.setBtnRejectView(false);
		privilegeComponent.setBtnApply(false);
		privilegeComponent.setBtnConfirmView(false);

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	public void validStatusHolder() {

		if (!Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber())) {
			holderAccount = new HolderAccount();
			return;
		}

		if (holderAccount.getStateAccount().equals(
				HolderAccountStatusType.ACTIVE.getCode())) {
			return;
		} else {
			// AGREGAR si hay alguna excepcion con edv
			if (holderAccount.getStateAccount().equals(
					HolderAccountStatusType.CLOSED.getCode())) {
			}
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED);
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			alert(headerMessage, bodyMessage);
			holderAccount = new HolderAccount();
		}
	}

	public void validateRegisterIssuer() {

		if ( !(Validations.validateIsNotNull(issuerHelp) && Validations.validateIsNotNull(issuerHelp.getIdIssuerPk())) ) {
			return;
		}
		
		issuerPk = issuerHelp.getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerPk);

		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {

			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerPk = StringUtils.EMPTY;
			issuerHelp = new Issuer();
		}
	}
	
	public void validateSearchIssuer() {

		if ( !(Validations.validateIsNotNull(issuerHelp) && Validations.validateIsNotNull(issuerHelp.getIdIssuerPk())) ) {
			return;
		}

		issuerSearchPK = issuerHelp.getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);

		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {

			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			issuerHelp = new Issuer();
		}
	}

	public void securityClassDetail() {
		try {
			if (selectedInstrument > -1) {
				securityClassList = withdrawalDepositeFacadeBean.securityClassFacade(selectedInstrument,SECURITYCLASS_MASTERPK);
			} else {
				securityClassList = null;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	public void loadSecurityClass() {
		try {
			instrumentTypeList = withdrawalDepositeFacadeBean.getParameterTableDetailsFacade(INSTRUMENT_TYPE_MASTERKEY);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public void loadSecurity(){
		
		try {
			if(Validations.validateIsNotNull(securityHelp.getIdSecurityCodePk())){
				securityHelp = transferAvailableFacade.getSecurity(securityHelp.getIdSecurityCodePk());
				
				if( Validations.validateIsNotNull(securityHelp) &&
						Validations.validateIsNotNull(securityHelp.getIdSecurityCodePk()) ){
					
						if(!securityHelp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
							confirmAlertAction = "clearSecurity";
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID));
							securityHelp=new Security();
						}
					}else{
						alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_ERROR_FINDING_ISINCODE));
						securityHelp=new Security();
					}
				
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// para agregar de una lista a otra
	public void addReteirementDepositeList() {

		certificatesNums = new ArrayList<String>();
		String cert = "";
		
		if (physicalCertificateListAdded == null) {
			physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
		}

		if (lstPhysicalCertificateSelected != null) {
			for (ReteirementDepositeFilterBean obj : lstPhysicalCertificateSelected) {
				physicalCertificateListAdded.add(obj);
				certificatesNums.add(obj.getPhysicalCertificate().getCertificateNumber());
			}
			cert = certificateNumberMessage(certificatesNums);
			physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(
					physicalCertificateListAdded);
		}
		for (ReteirementDepositeFilterBean loadData : lstPhysicalCertificateSelected) {
			physicalCertificateList.remove(loadData);
		}
		physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(
				physicalCertificateList);
			summeryDetail();
		if (lstPhysicalCertificateSelected != null) {
			lstPhysicalCertificateSelected = null;
		}
		
		/*Object[] data = new Object[]{cert};
		showMessageOnDialog(PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_ADD_CERTIFICATE_MULTI,data));
		 JSFUtilities.showSimpleValidationDialog();*/
	}

	private void summeryDetail() {

		int totalCurrencyLocal = 0;
		int totalCurrencyEUR = 0;
		int totalCurrencyUSD = 0;
	    int totalCurrencyUFV=0;
	    int totalCurrencyDMV=0;
	    
		BigDecimal totalNominaldUSD = new BigDecimal(0);
		BigDecimal totalNominalLocal = new BigDecimal(0);
		BigDecimal totalNominaldEUR = new BigDecimal(0);
		BigDecimal totalNominalUFV=new BigDecimal(0);
		BigDecimal totalNominalDMV= new BigDecimal(0);
		

		for (ReteirementDepositeFilterBean reteirBean : physicalCertificateListAdded) {
			// USD currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.USD.getCode())) {
				totalCurrencyUSD++;
				totalNominaldUSD = totalNominaldUSD.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			// DOP currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.PYG.getCode())) {
				totalCurrencyLocal++;
				totalNominalLocal = totalNominalLocal.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			// EUR currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.EU.getCode())) {
				totalCurrencyEUR++;
				totalNominaldEUR = totalNominaldEUR.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			//UFV Currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.UFV.getCode())) {
				totalCurrencyUFV++;
				totalNominalUFV = totalNominalUFV.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			//DMV Currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.DMV.getCode())) {
				totalCurrencyDMV++;
				totalNominalDMV = totalNominalDMV.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
		}

		setSelectedCurrencyLocal(new Integer(totalCurrencyLocal));
		setSelectedCurrencyUSP(new Integer(totalCurrencyUSD));
		setSelectedCurrencyEUR(new Integer(totalCurrencyEUR));
		setSelectedCurrencyUFV(new Integer(totalCurrencyUFV));
		setSelectedCurrencyDMV(new Integer(totalCurrencyDMV));

		setSelectedNominalLocal(totalNominalLocal);
		setSelectedNominalUSP(totalNominaldUSD);
		setSelectedNominalEUR(totalNominaldEUR);
		setSelectedNominalUFV(totalNominalUFV);
		setSelectedNominalDMV(totalNominalDMV);
	}

	private void cleanSearchInputData() {
		issuerPk = null;
		issuerPk = StringUtils.EMPTY;
	//	issuerHelper.setIssuerDescription(StringUtils.EMPTY);
	//	issuerHelper.setIssuerMnemonic(StringUtils.EMPTY);
		docNumber = null;
		selectedInstrument = null;
		selectedSecurityClass = null;
		certificatesNums = new ArrayList<String>();
		if (initialDate != null) {
			if (!initialDate.equals(CommonsUtilities.currentDate())) {
				initialDate = CommonsUtilities.currentDate();
			}
		}
		else
			initialDate = CommonsUtilities.currentDate();
		if (finalDate != null) {
			if (!finalDate.equals(CommonsUtilities.currentDate())) {
				finalDate = CommonsUtilities.currentDate();
			}
		}
		else
			finalDate = CommonsUtilities.currentDate();
		
		sourceAccountTo = new HolderAccountHelperResultTO();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		
		
		securityEvents();
	}

	public void popUpShowData(ReteirementDepositeFilterBean filterbean) {
		showSelected = filterbean;
		JSFUtilities.executeJavascriptFunction("PF('processData').show();");
	}

	public void showElimarData(ReteirementDepositeFilterBean certificate) {
		certificatesNums=new ArrayList<String>();
		certificatesNums.add(certificate.getPhysicalCertificate().getCertificateNumber());
		String certMessages = certificateNumberMessage(certificatesNums);
		certificateDelete = certificate;

		confirmAlertAction = "removeList";
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_DELETE, certMessages);
		String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_DELETE);
		question(headerMessage, bodyMessage);

	}

	public void removeList() {
		String cert = certificateNumberMessage(certificatesNums);
				
		Object[] data = new Object[]{cert};
		showMessageOnDialog(PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_DELETE,data));
		 JSFUtilities.showSimpleValidationDialog();
		
		physicalCertificateListAdded.remove(certificateDelete);
		certificateDelete = null;
		physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(
				physicalCertificateListAdded);
		summeryDetail();
	}

	@LoggerAuditWeb
	public void showSaveDialog() {
		if(physicalCertificateListAdded != null && physicalCertificateListAdded.size() > 0){
			confirmAlertAction = "saveData";
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_SAVE_RETIRO);
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER);
			question(headerMessage, bodyMessage);
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MESSAGES),
					"Debe adicionar almenos un titulo para retirar.");
			 JSFUtilities.showSimpleValidationDialog();
		}
	}

	@LoggerAuditWeb
	public void saveData() {
		Long requestNumber = null;
		try {
			ReteirementDataFilterBean retiroData = new ReteirementDataFilterBean();
			retiroData.setAccHolderPk(holderAccount.getIdHolderAccountPk());
			retiroData.setIdParticipanrPk(participantSelected);
			retiroData.setState(StateType.REGISTERED.getCode());
			retiroData.setDeliverDate(deliverReqDate);
			// CUSTODY
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
			custodyOperation.setOperationType(new Long(RequestType.RETIRO.getCode()));
			custodyOperation.setState(StateType.REGISTERED.getCode());
			custodyOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());

			// PHYSICAL CERTIFICATE OPERATION
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(this.holderAccount.getIdHolderAccountPk());
			Participant participant = new Participant();
			participant.setIdParticipantPk(this.participantSelected);

			PhysicalCertificateOperation physicalCertificateOperation = new PhysicalCertificateOperation();
			physicalCertificateOperation.setHolderAccount(holderAccount);
			physicalCertificateOperation.setParticipant(participant);
			physicalCertificateOperation.setComments(textObservation);
			physicalCertificateOperation.setState(StateType.REGISTERED_CERTIFICATE.getCode());
			physicalCertificateOperation.setPhysicalOperationType(new Long(RequestType.RETIRO.getCode()));
			physicalCertificateOperation.setIndApplied(new Integer(0));
			physicalCertificateOperation.setRequestDeliveryDate(retiroData.getDeliverDate());

			List<PhysicalOperationDetail> physicalOperationDetails = new ArrayList<PhysicalOperationDetail>();
			certificatesNums = new ArrayList<String>();

			for (ReteirementDepositeFilterBean ReteirementcertificateTo : physicalCertificateListAdded) {

				PhysicalCertificate certificate = ReteirementcertificateTo.getPhysicalCertificate();
				// PHYSICAL_CERTIFICATE
				certificatesNums.add(certificate.getCertificateNumber());

				// PHYSICAL_OPERATION_DETAIL
				PhysicalOperationDetail physicalOperationDetail = new PhysicalOperationDetail();
				physicalOperationDetail.setPhysicalCertificateOperation(physicalCertificateOperation);
				physicalOperationDetail.setPhysicalCertificate(certificate);
				physicalOperationDetail.setIndApplied(0);
				physicalOperationDetail.setState(retiroData.getState());
				physicalOperationDetail.setRegistryDate(CommonsUtilities.currentDateTime());
				physicalOperationDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				if (ReteirementcertificateTo.isIndDematerialized()) {
					physicalOperationDetail.setIndDematerialized(BooleanType.YES.getCode());
				} else {
					physicalOperationDetail.setIndDematerialized(BooleanType.NO.getCode());
				}
				// DETALLES A GRABAR
				physicalOperationDetails.add(physicalOperationDetail);
			}
			physicalCertificateOperation.setPhysicalOperationDetails(physicalOperationDetails);
			
			physicalCertificateOperation = withdrawalDepositeFacadeBean.savePhysicalCertificateOperation(physicalCertificateOperation, 
																										 custodyOperation,physicalOperationDetails);
			requestNumber = physicalCertificateOperation.getCustodyOperation().getOperationNumber();

			String certMessages = certificateNumberMessage(certificatesNums);
			certificatesNums = null;
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_LIST_SECURITY_NUMBER,new Object[] { certMessages, requestNumber.toString() });
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			  BusinessProcess businessProcess = new BusinessProcess();
			  businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_REGISTER.getCode());
			  if(isCevaldom){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null,null); 
			  }else if(isBcr){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, null); 
			  }else if(isParticipant){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, participantSelected, null); 
			  }
			 
			/*** end JH NOTIFICACIONES ***/

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	public void dialogExitHide() {
		showSelected = null;
	}

	public void addCertificateShowDialog() {
		if (lstPhysicalCertificateSelected == null || lstPhysicalCertificateSelected.length == 0) {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_RECORD_SELECT_CNF);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
		}else{
			confirmAlertAction = "addReteirementDepositeList";
			confirmAlertActions();
		}
		/*if (lstPhysicalCertificateSelected != null && lstPhysicalCertificateSelected.length > 0) {

			confirmAlertAction = "addReteirementDepositeList";
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOAD);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			question(headerMessage, bodyMessage);

		} else {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_RECORD_SELECT_CNF);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
			alert(headerMessage, bodyMessage);
		}*/

	}

	public void searchSecurities() {
		try {

			ReteirementDataFilterBean retiroData = new ReteirementDataFilterBean();
			
			if (Validations.validateIsNotNullAndPositive(participantSelected)){
				retiroData.setIdParticipanrPk(participantSelected);
			}
			else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_REQUIRED));
				return;
			}
			if (Validations.validateIsNotNull(sourceHolder)
					&& Validations.validateIsNotNullAndPositive(sourceHolder
							.getIdHolderPk())) {
				retiroData.setIdHolderPk(sourceHolder.getIdHolderPk());
			} else {
				alert(PropertiesUtilities
						.getMessage(PropertiesConstants.HOLDER_REQUIRED));
				return;
			}
			if (Validations.validateIsNotNull(holderAccount) && Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())){
				retiroData.setAccHolderPk(holderAccount.getIdHolderAccountPk());
			}
			else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_REQUIRED));
				return;
			}
			retiroData.setState(StateType.ANNULLED.getCode()); 
			retiroData.setDescDate(initialDate);
			if(Validations.validateIsNotNullAndNotEmpty(finalDate))
				retiroData.setHasDate(CommonsUtilities.addDate(finalDate, 1));
			else
				retiroData.setHasDate(finalDate);
			
			retiroData.setIssuerPKSearch(issuerPk);

			if (Validations.validateIsNotNull(securityHelp) && Validations.validateIsNotNullAndNotEmpty(securityHelp.getIdSecurityCodePk())){
				retiroData.setIdSecurityCode(securityHelp.getIdSecurityCodePk());
			}
			retiroData.setDocumentNumber(docNumber);
			retiroData.setDeliverDate(deliverReqDate);
			retiroData.setInstrumenttype(selectedInstrument);
			retiroData.setSecurityClass(selectedSecurityClass);
			retiroData.setSaveData(physicalCertificateListAdded);
			physicalCertificateList = withdrawalDepositeFacadeBean.searchSecuritiesFacade(retiroData);

			if (physicalCertificateList.size() > 0) {
				physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(physicalCertificateList);
				registersFound = true;
			} else {
				physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
				physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(physicalCertificateList);
				registersFound = false;
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	public HolderAccount getHolderAccountAndDetails(HolderAccount holderAccount){
		try {
			holderAccount = transferAvailableFacade.getHolderAccount(holderAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return holderAccount;
	}
	
	public void validSourceHolderAccount(){
		
		if(Validations.validateIsNotNull(sourceAccountTo) && Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountNumber()) ){
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(sourceAccountTo.getAccountPk());
			this.holderAccount = holderAccount;
			//holderAccount.setAccountNumber(HolderAccountStatusType.ACTIVE.getCode());
		
			if(!(Validations.validateIsNotNull(this.holderAccount) &&
				 Validations.validateIsNotNullAndPositive(this.holderAccount.getIdHolderAccountPk())) ){
				return;
			}
			holderAccount = getHolderAccountAndDetails(this.holderAccount);
			
			
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
				this.holderAccount = new HolderAccount();
			}
			
			this.holderAccount = holderAccount;
		
		}else{
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_NOT_SELECTED));
		}
	}
	//seleccionar participante - anterior
	public void validateRegisterHolderAccount() {

		if (!Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber())) {
			holderAccount = new HolderAccount();
			return;
		}

		if (holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {

			if (physicalCertificateListAdded.size() > 0 || physicalCertificateList.size() > 0) {
				HolderAccount tmpHolder = holderAccount;
				executeChangeParticipant();
				holderAccount = tmpHolder;
			}

			return;
		} else {

			if (physicalCertificateListAdded.size() > 0 || physicalCertificateList.size() > 0) {
				HolderAccount tmpHolder = holderAccount;
				executeChangeParticipant();
				holderAccount = tmpHolder;
			}

			// AGREGAR si hay alguna excepcion con cevaldom
			if (holderAccount.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())) {

			}
			
			holderAccount = new HolderAccount();
			String bodyMessage = PropertiesUtilities
					.getMessage(PropertiesConstants.ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED);
			String headerMessage = PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			alert(headerMessage, bodyMessage);
		}
	}

	public Date getNextWorkDay(Date actulaDate, int length) {

		Calendar dateInstance = Calendar.getInstance();
		try {
			dateInstance.setTime(actulaDate);

			dateInstance.set(Calendar.HOUR_OF_DAY, 0);
			dateInstance.set(Calendar.MINUTE, 0);
			dateInstance.set(Calendar.SECOND, 0);
			dateInstance.set(Calendar.MILLISECOND, 0);
			Holiday dayOFHoliday = null;

			while (length > 0) {
				dateInstance.add(Calendar.DATE, 1);
				if (dateInstance.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || dateInstance.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					continue;
				} else {

					dayOFHoliday = certificateDepositeBeanFacade.getHolidayListByState(dateInstance.getTime(),
									GeographicLocationStateType.REGISTERED.getCode(), DOMINICA_GEOGRAPHIC_CODE);
					if (dayOFHoliday != null) {
						continue;
					} else {
						length--;
					}
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return dateInstance.getTime();

	}

	public void loadParameterReason() {
		try {
			reasonTypeList = withdrawalDepositeFacadeBean
					.getParameterTableDetailsFacade(CANCEL_TYPE_MASTERKEY);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadParticipant() throws ServiceException {

		if (Validations.validateIsNotNullAndPositive(participant.getState())) {
			if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode()) || participant.getState().equals(ParticipantStateType.REGISTERED.getCode())) {

				/*participant.setCompleteDescription(participant.getDescription()
						+ " - " + participant.getIdParticipantPk().toString());*/
				lstParticpant.add(participant);

			} else {
				Map<String, Object> filter = new HashMap<String, Object>();
				List<Integer> states = new ArrayList<Integer>();
				states.add(ParticipantStateType.REGISTERED.getCode());
				filter.put("states", states);
				lstParticpant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
				
				//lstParticpant = certificateDepositeBeanFacade.getListParticicpant(ParticipantStateType.REGISTERED.getCode(),ParticipantClassType.AFP.getCode());
			}
		} else {

			Map<String, Object> filter = new HashMap<String, Object>();
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			//states.add(ParticipantStateType.BLOCKED.getCode());
			filter.put("states", states);
			lstParticpant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
			
			/*List<Integer> lstStates = new ArrayList<Integer>();
			lstStates.add(ParticipantStateType.REGISTERED.getCode());
			lstStates.add(ParticipantStateType.BLOCKED.getCode());
			lstParticpant = certificateDepositeBeanFacade.getListParticicpant(lstStates, ParticipantClassType.AFP.getCode());
			*/
		}
	}

	public void loadCertificateDepositeEstatus() throws ServiceException {
		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITY_STATES.getCode());
		List<Integer> lista = new ArrayList<>();
		lista.add(PhysicalCertificateStateType.AUTHORIZED.getCode());
		filter.setLstParameterTablePkNotIn(lista);
		certificateDepositeEstatusTypeList = generalParametersFacade.getComboParameterTable(filter);	
		
		//certificateDepositeEstatusTypeList = withdrawalDepositeFacadeBean.getParameterTableDetailsFacade(CERTIFICATE_STATUS_MASTERKEY);
	}

	public Participant findParticipantSelected(Long idParticipantPk) {
		
		if (Validations.validateIsNotNull(participantSelected)) {
			for (Participant curParticipant : lstParticpant) {
				if (participantSelected.equals(curParticipant.getIdParticipantPk())) {
					return curParticipant;
				}
			}
		}
		else if(Validations.validateIsNotNull(idParticipantPk)){
			for (Participant curParticipant : lstParticpant) {
				if (idParticipantPk.equals(curParticipant.getIdParticipantPk())) {
					return curParticipant;
				}
			}
		}
		return null;
	}
	
	public void actionCertificateDialog(ReteirementDepositeFilterBean physicalCertificateExt){
		
	//	PhysicalCertificate certificate = certificateDepositeBeanFacade.getPhysicalCertificateObject(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
		
		dialogPhysicalCertificate = new PhysicalCertificateExt();
		
	//	dialogPhysicalCertificate.setPhysicalCertificate(certificate);
		dialogPhysicalCertificate.setPhysicalCertificate(physicalCertificateExt.getPhysicalCertificate());
		if(dialogPhysicalCertificate.getPhysicalCertificate().getIssuer() == null || 
		Validations.validateIsNull(dialogPhysicalCertificate.getPhysicalCertificate().getIssuer().getIdIssuerPk()) ){
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk(physicalCertificateExt.getIssuerFk());
			issuer.setDescription(physicalCertificateExt.getIssuerDesc());
			issuer.setMnemonic(physicalCertificateExt.getIssuerNemonic());
			dialogPhysicalCertificate.getPhysicalCertificate().setIssuer(issuer);			
		}
		
		//Logica de cargar datos
		showDetailCertificate();
	}
	
	public void showDetailCertificate() {
		JSFUtilities.executeJavascriptFunction("PF('idDetalleValorW').show();");
	}

	public void certificateQuestion() {

		String certs = certificateNumberMessage(certificatesNums);
		String headerMessage = "";
		String bodyMessage = "";

		if (confirmAlertAction.equals("certificateRejection_anull")) {
			headerMessage = confirmHeaderMessage(StateType.ANNULLED.getCode());
			bodyMessage = confirmbodyMessage(StateType.ANNULLED.getCode(),certs);
			confirmAlertAction = "retirementCertificateAnuller";
		} else {// certificateRejection_reject
			headerMessage = confirmHeaderMessage(StateType.REJECTED.getCode());
			bodyMessage = confirmbodyMessage(StateType.REJECTED.getCode(),certs);
			confirmAlertAction = "retirementCertificateRejected";
		}

		question(headerMessage, bodyMessage);
	}

	/** ALERT's **/

	@LoggerAuditWeb
	public String confirmAlertActions() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (tmpAction.equals("retirementCertificateAuthorization")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateAuthorization();
		} else if (tmpAction.equals("retirementCertificateConfirm")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateConfirm();
		} else if (tmpAction.equals("retirementCertificateApply")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateApply();
		} else if (tmpAction.equals("retirementCertificateAnuller")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateAnuller();
		} else if (tmpAction.equals("retirementCertificateRejected")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateRejected();
		}else if (tmpAction.equals("retirementCertificateApprove")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateApprove();
		} else if (tmpAction.equals("addReteirementDepositeList")) {
			confirmAlertAction = "";
			addReteirementDepositeList();
		} else if (tmpAction.equals("removeList")) {
			confirmAlertAction = "";
			removeList();
		} else if (tmpAction.equals("saveData")) {
			confirmAlertAction = "postActionReturnToSearch";
			saveData();
			initialDate = CommonsUtilities.currentDate();
			finalDate = CommonsUtilities.currentDate();
		} else if (tmpAction.equals("cleanRegistry")) {
			confirmAlertAction = "";
			cleanRegistry();
		} else if (tmpAction.equals("executeBackToSearch")) {
			confirmAlertAction = "";
			return executeBackToSearch();
		} else if (tmpAction.equals("executeChangeParticipant")) {
			confirmAlertAction = "executeChangeParticipant";
			executeChangeParticipant();
		}
		return "";
	}

	@LoggerAuditWeb
	public String aceptAlertActions() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (Validations.validateIsNotNull(tmpAction)) {

			if (tmpAction.equals("postActionReturnToSearch")) {
				searchRetirementPhysicalCertificates();
				cleanSearchInputData();
				enabledButtons();
				return "search";
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	public String aceptAlertActionsNew() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (Validations.validateIsNotNull(tmpAction)) {

			if (tmpAction.equals("postActionReturnToSearch")) {
				//cleanRegistry();
				//initialDate = CommonsUtilities.currentDate();
				//finalDate = CommonsUtilities.currentDate();
				searchRetirementPhysicalCertificates();
				return "search";
			}

		} else {
			return "";
		}
		return "";
	}

	public void cancelAlertAnullOrReject() {
		reasonSelected = -1;
		rejectOther = "";
	}

	public String questionAlertNo() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (tmpAction.equals("executeChangeParticipant")) {
			confirmAlertAction = "";
			participantSelected = participantSelectedTmp;
		}

		return "";
	}

	public void alert(String bodyMessage) {
		String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext
						.getCurrentInstance().getViewRoot().getLocale(),GeneralConstants.LBL_HEADER_ALERT);

		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	public void alert(String headerMessage, String bodyMessage) {

		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	public void question(String headerMessage, String bodyMessage) {

		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}

	public boolean ValidateStateTypeAnnulledOrRejected(Integer stateType){
    	boolean viewDetailCertificate = Boolean.FALSE;
    	if(stateType.equals(StateType.ANNULLED.getCode()) || stateType.equals(StateType.REJECTED.getCode())){
    		viewDetailCertificate = Boolean.TRUE;
    	}
    	return viewDetailCertificate;
    }
	
	public void alertAnulledOrReject(Integer state) {
		String headerMessage = "";

		if (state.equals(StateType.ANNULLED.getCode())) {
			confirmAlertAction = "certificateRejection_anull";
			headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_ANNULMENT);
		} else if (state.equals(StateType.REJECTED.getCode())) {
			confirmAlertAction = "certificateRejection_reject";
			headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_REJECTED);
		}

		reasonSelected = -1;
		rejectOther = "";
		showMessageOnDialog(headerMessage, "");
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').show();");
	}


	/** GET AN SET **/

	public List<Participant> getLstParticpant() {
		return lstParticpant;
	}

	public void setLstParticpant(List<Participant> lstParticpant) {
		this.lstParticpant = lstParticpant;
	}

	public Long getParticipantSelected() {
		return participantSelected;
	}

	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	public String getIssuerSearchPK() {
		return issuerSearchPK;
	}

	public void setIssuerSearchPK(String issuerSearchPK) {
		this.issuerSearchPK = issuerSearchPK;
	}

	public Long getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(Long docNumber) {
		this.docNumber = docNumber;
	}

	public Integer getStateSelected() {
		return stateSelected;
	}

	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}

	public List<ParameterTable> getCertificateDepositeEstatusTypeList() {
		return certificateDepositeEstatusTypeList;
	}

	public void setCertificateDepositeEstatusTypeList(
			List<ParameterTable> certificateDepositeEstatusTypeList) {
		this.certificateDepositeEstatusTypeList = certificateDepositeEstatusTypeList;
	}

	public Long getSelectedReqNumber() {
		return selectedReqNumber;
	}

	public void setSelectedReqNumber(Long selectedReqNumber) {
		this.selectedReqNumber = selectedReqNumber;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public Date getMaxDateAllow() {
		return maxDateAllow;
	}

	public void setMaxDateAllow(Date maxDateAllow) {
		this.maxDateAllow = maxDateAllow;
	}

	public Long getParticipantLogin() {
		return participantLogin;
	}

	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	public boolean getIsParticipant() {
		return isParticipant;
	}

	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	public boolean getIsCevaldom() {
		return isCevaldom;
	}

	public void setCevaldom(boolean isCevaldom) {
		this.isCevaldom = isCevaldom;
	}

	public boolean getIsBcr() {
		return isBcr;
	}

	public void setBcr(boolean isBcr) {
		this.isBcr = isBcr;
	}

	public boolean isFlagRegister() {
		return flagRegister;
	}

	public void setFlagRegister(boolean flagRegister) {
		this.flagRegister = flagRegister;
	}

	public boolean isFlagSearch() {
		return flagSearch;
	}

	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	public boolean isRegistersFound() {
		return registersFound;
	}

	public void setRegistersFound(boolean registersFound) {
		this.registersFound = registersFound;
	}

	public String getConfirmAlertAction() {
		return confirmAlertAction;
	}

	public void setConfirmAlertAction(String confirmAlertAction) {
		this.confirmAlertAction = confirmAlertAction;
	}

	public Integer getReasonSelected() {
		return reasonSelected;
	}

	public void setReasonSelected(Integer reasonSelected) {
		this.reasonSelected = reasonSelected;
	}

	public String getRejectOther() {
		return rejectOther;
	}

	public void setRejectOther(String rejectOther) {
		this.rejectOther = rejectOther;
	}

	public Integer getOtherRejectMotiveId() {
		return otherRejectMotiveId;
	}

	public void setOtherRejectMotiveId(Integer otherRejectMotiveId) {
		this.otherRejectMotiveId = otherRejectMotiveId;
	}

	public GenericDataModel<PhysicalCertificateExt> getCertificateDepositeDataModel() {
		return certificateDepositeDataModel;
	}

	public void setCertificateDepositeDataModel(
			GenericDataModel<PhysicalCertificateExt> certificateDepositeDataModel) {
		this.certificateDepositeDataModel = certificateDepositeDataModel;
	}

	public List<PhysicalCertificateExt> getCertificateDepositeList() {
		return certificateDepositeList;
	}

	public void setCertificateDepositeList(
			List<PhysicalCertificateExt> certificateDepositeList) {
		this.certificateDepositeList = certificateDepositeList;
	}

	public PhysicalCertificateExt[] getCertificateDepositeSelected() {
		return certificateDepositeSelected;
	}

	public void setCertificateDepositeSelected(
			PhysicalCertificateExt[] certificateDepositeSelected) {
		this.certificateDepositeSelected = certificateDepositeSelected;
	}

	public List<ParameterTable> getReasonTypeList() {
		return reasonTypeList;
	}

	public void setReasonTypeList(List<ParameterTable> reasonTypeList) {
		this.reasonTypeList = reasonTypeList;
	}

	public PhysicalCertificateExt getSelectedReteirementDepositDetail() {
		return selectedReteirementDepositDetail;
	}

	public void setSelectedReteirementDepositDetail(
			PhysicalCertificateExt selectedReteirementDepositDetail) {
		this.selectedReteirementDepositDetail = selectedReteirementDepositDetail;
	}

	public HolderAccount getHolderAccountDetail() {
		return holderAccountDetail;
	}

	public void setHolderAccountDetail(HolderAccount holderAccountDetail) {
		this.holderAccountDetail = holderAccountDetail;
	}

	public IssuerHelperBean getIssuerHelper() {
		return issuerHelper;
	}

	public void setIssuerHelper(IssuerHelperBean issuerHelper) {
		this.issuerHelper = issuerHelper;
	}

	public List<String> getCertificatesNums() {
		return certificatesNums;
	}

	public void setCertificatesNums(List<String> certificatesNums) {
		this.certificatesNums = certificatesNums;
	}


	public Integer getSelectedCurrencyUSP() {
		return selectedCurrencyUSP;
	}

	public void setSelectedCurrencyUSP(Integer selectedCurrencyUSP) {
		this.selectedCurrencyUSP = selectedCurrencyUSP;
	}

	public Integer getSelectedCurrencyEUR() {
		return selectedCurrencyEUR;
	}

	public void setSelectedCurrencyEUR(Integer selectedCurrencyEUR) {
		this.selectedCurrencyEUR = selectedCurrencyEUR;
	}

	public BigDecimal getSelectedNominalUSP() {
		return selectedNominalUSP;
	}

	public void setSelectedNominalUSP(BigDecimal selectedNominalUSP) {
		this.selectedNominalUSP = selectedNominalUSP;
	}

	public BigDecimal getSelectedNominalEUR() {
		return selectedNominalEUR;
	}

	public void setSelectedNominalEUR(BigDecimal selectedNominalEUR) {
		this.selectedNominalEUR = selectedNominalEUR;
	}

	public Integer getSelectedInstrument() {
		return selectedInstrument;
	}

	public void setSelectedInstrument(Integer selectedInstrument) {
		this.selectedInstrument = selectedInstrument;
	}

	public Integer getSelectedSecurityClass() {
		return selectedSecurityClass;
	}

	public void setSelectedSecurityClass(Integer selectedSecurityClass) {
		this.selectedSecurityClass = selectedSecurityClass;
	}

	public HolderAccount getHolderAccSearch() {
		return holderAccSearch;
	}

	public void setHolderAccSearch(HolderAccount holderAccSearch) {
		this.holderAccSearch = holderAccSearch;
	}

	public String getIssuerPk() {
		return issuerPk;
	}

	public void setIssuerPk(String issuerPk) {
		this.issuerPk = issuerPk;
	}

	public Date getDeliverReqDate() {
		return deliverReqDate;
	}

	public void setDeliverReqDate(Date deliverReqDate) {
		this.deliverReqDate = deliverReqDate;
	}

	public List<ReteirementDepositeFilterBean> getPhysicalCertificateListAdded() {
		return physicalCertificateListAdded;
	}

	public void setPhysicalCertificateListAdded(
			List<ReteirementDepositeFilterBean> physicalCertificateListAdded) {
		this.physicalCertificateListAdded = physicalCertificateListAdded;
	}

	public ReteirementDepositeFilterBean[] getPhyCertificateRemoveSelected() {
		return phyCertificateRemoveSelected;
	}

	public void setPhyCertificateRemoveSelected(
			ReteirementDepositeFilterBean[] phyCertificateRemoveSelected) {
		this.phyCertificateRemoveSelected = phyCertificateRemoveSelected;
	}

	public GenericDataModel<ReteirementDepositeFilterBean> getPhysicalCertificateDataModel() {
		return physicalCertificateDataModel;
	}

	public void setPhysicalCertificateDataModel(
			GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateDataModel) {
		this.physicalCertificateDataModel = physicalCertificateDataModel;
	}

	public ReteirementDepositeFilterBean[] getLstPhysicalCertificateSelected() {
		return lstPhysicalCertificateSelected;
	}

	public void setLstPhysicalCertificateSelected(
			ReteirementDepositeFilterBean[] lstPhysicalCertificateSelected) {
		this.lstPhysicalCertificateSelected = lstPhysicalCertificateSelected;
	}

	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}

	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}

	public List<ParameterTable> getInstrumentTypeList() {
		return instrumentTypeList;
	}

	public void setInstrumentTypeList(List<ParameterTable> instrumentTypeList) {
		this.instrumentTypeList = instrumentTypeList;
	}

	public ReteirementDepositeFilterBean getShowSelected() {
		return showSelected;
	}

	public void setShowSelected(ReteirementDepositeFilterBean showSelected) {
		this.showSelected = showSelected;
	}

	public Date getMaxNextWorkDay() {
		return maxNextWorkDay;
	}

	public void setMaxNextWorkDay(Date maxNextWorkDay) {
		this.maxNextWorkDay = maxNextWorkDay;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public List<ReteirementDepositeFilterBean> getPhysicalCertificateList() {
		return physicalCertificateList;
	}

	public void setPhysicalCertificateList(
			List<ReteirementDepositeFilterBean> physicalCertificateList) {
		this.physicalCertificateList = physicalCertificateList;
	}

	public GenericDataModel<ReteirementDepositeFilterBean> getPhysicalCertificateListAddedDataModel() {
		return physicalCertificateListAddedDataModel;
	}

	public void setPhysicalCertificateListAddedDataModel(
			GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateListAddedDataModel) {
		this.physicalCertificateListAddedDataModel = physicalCertificateListAddedDataModel;
	}

	public Date getMaxDateRetirement() {
		return maxDateRetirement;
	}

	public void setMaxDateRetirement(Date maxDateRetirement) {
		this.maxDateRetirement = maxDateRetirement;
	}

	public boolean isRenderedInstrumentType() {
		return renderedInstrumentType;
	}

	public void setRenderedInstrumentType(boolean renderedInstrumentType) {
		this.renderedInstrumentType = renderedInstrumentType;
	}

	public Holder getSourceHolder() {
		return sourceHolder;
	}

	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}

	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}

	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	public void setLstSourceAccountTo(
			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	public Issuer getIssuerHelp() {
		return issuerHelp;
	}

	public void setIssuerHelp(Issuer issuerHelp) {
		this.issuerHelp = issuerHelp;
	}

	public PhysicalCertificateExt getDialogPhysicalCertificate() {
		return dialogPhysicalCertificate;
	}

	public void setDialogPhysicalCertificate(
			PhysicalCertificateExt dialogPhysicalCertificate) {
		this.dialogPhysicalCertificate = dialogPhysicalCertificate;
	}

	public String getTextObservation() {
		return textObservation;
	}

	public void setTextObservation(String textObservation) {
		this.textObservation = textObservation;
	}

	public Security getSecurityHelp() {
		return securityHelp;
	}

	public void setSecurityHelp(Security securityHelp) {
		this.securityHelp = securityHelp;
	}

	public Integer getSelectedCurrencyLocal() {
		return selectedCurrencyLocal;
	}

	public void setSelectedCurrencyLocal(Integer selectedCurrencyLocal) {
		this.selectedCurrencyLocal = selectedCurrencyLocal;
	}

	public BigDecimal getSelectedNominalLocal() {
		return selectedNominalLocal;
	}

	public void setSelectedNominalLocal(BigDecimal selectedNominalLocal) {
		this.selectedNominalLocal = selectedNominalLocal;
	}

	public Integer getSelectedCurrencyUFV() {
		return selectedCurrencyUFV;
	}

	public void setSelectedCurrencyUFV(Integer selectedCurrencyUFV) {
		this.selectedCurrencyUFV = selectedCurrencyUFV;
	}

	public Integer getSelectedCurrencyDMV() {
		return selectedCurrencyDMV;
	}

	public void setSelectedCurrencyDMV(Integer selectedCurrencyDMV) {
		this.selectedCurrencyDMV = selectedCurrencyDMV;
	}

	public BigDecimal getSelectedNominalUFV() {
		return selectedNominalUFV;
	}

	public void setSelectedNominalUFV(BigDecimal selectedNominalUFV) {
		this.selectedNominalUFV = selectedNominalUFV;
	}

	public BigDecimal getSelectedNominalDMV() {
		return selectedNominalDMV;
	}

	public void setSelectedNominalDMV(BigDecimal selectedNominalDMV) {
		this.selectedNominalDMV = selectedNominalDMV;
	}

	public List<ParameterTable> getMoneyTypeList() {
		return moneyTypeList;
	}
	public void setMoneyTypeList(List<ParameterTable> moneyTypeList) {
		this.moneyTypeList = moneyTypeList;
	}
	
	public Date getFechaMaxima() {
		return fechaMaxima;
	}
	
	public void setFechaMaxima(Date fechaMaxima) {
		this.fechaMaxima = fechaMaxima;
	}
}