package com.pradera.custody.physicalcertificate.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.RequestType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.view.IssuerHelperBean;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.dematerializationcertificate.to.RegisterAccountAnnotationTO;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.facade.ReteirementDepositeServiceFacade;
import com.pradera.custody.physicalcertificate.service.CertificateDepositServiceBean;
import com.pradera.custody.physicalcertificate.view.filter.CommonUpdateBeanInf;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateFilter;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDataFilterBean;
import com.pradera.custody.physicalcertificate.view.filter.ReteirementDepositeFilterBean;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.custody.to.XmlRespuestaActivoFinanciero;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.physical.PhysicalCertMarketfact;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.physical.PhysicalCertificateOperation;
import com.pradera.model.custody.physical.PhysicalOperationDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.StateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>Copyright 2012 Pradera Technologies Technologies, All rights reserved..</li>
 * </ul>
 * 
 * The Class ReteirementDepositeBean
 *
 * @author : PraderaTechnologies.
 * @version 1.1
 * @Project : PraderaCustody
 * @Creation_Date :
 */
@DepositaryWebBean
@LoggerCreateBean
public class ReteirementDepositeBeanOriginal extends GenericBaseBean implements
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The country residence. */
	@Inject @Configurable 
	Integer countryResidence;
	
	/** The withdrawal deposite facade bean. */
	@EJB
	private ReteirementDepositeServiceFacade withdrawalDepositeFacadeBean;
	
	/** The certificate deposite bean facade. */
	@EJB
	private CertificateDepositeServiceFacade certificateDepositeBeanFacade;

	/** The issuer helper. */
	@Inject
	private IssuerHelperBean issuerHelper;

	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;

	/** The general parameters facade. */
	@EJB
	private GeneralParametersFacade generalParametersFacade;

	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;

	/** The transfer available facade. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailableFacade;
	
	/** The helper component facade. */
	@EJB
    private HelperComponentFacade helperComponentFacade;
	
	/** The certificate deposit service bean. */
	@EJB
	private CertificateDepositServiceBean certificateDepositServiceBean;
	
	/** The dematerialization certificate facade. */
	@EJB
	private DematerializationCertificateFacade dematerializationCertificateFacade;
	
	/** The lst particpant. */
	private List<Participant> lstParticpant = new ArrayList<Participant>();
	
	/** The participant. */
	private Participant participant = new Participant();
	
	/** The participant selected. */
	private Long participantSelected;
	
	/** The participant selected tmp. */
	private Long participantSelectedTmp;
	
	/** The holder account. */
	private HolderAccount holderAccount = new HolderAccount();
	
	/** The issuer search pk. */
	private String issuerSearchPK;
	
	/** The doc number. */
	private Long docNumber;
	
	/** The state selected. */
	private Integer stateSelected;
	
	/** The certificate deposite estatus type list. */
	private List<ParameterTable> certificateDepositeEstatusTypeList;
	
	/** The reason type list. */
	private List<ParameterTable> reasonTypeList;
	
	/** The selected req number. */
	private Long selectedReqNumber;
	
	/** The initial date. */
	private Date initialDate=CommonsUtilities.currentDate();
	
	/** The final date. */
	private Date finalDate=CommonsUtilities.currentDate();
	
	/** The max date allow. */
	private Date maxDateAllow = CommonsUtilities.currentDate();
	
	/** The max date retirement. */
	private Date maxDateRetirement = CommonsUtilities.currentDate();
	
	/** The fecha maxima. */
	private Date fechaMaxima = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), 1);
	
	/** The confirm alert action. */
	private String confirmAlertAction = "";
	
	/** The reason selected. */
	private Integer reasonSelected = null;
	
	/** The reject other. */
	private String rejectOther = null;
	
	/** The other reject motive id. */
	private Integer otherRejectMotiveId = 726;
	
	/** The holder account detail. */
	private HolderAccount holderAccountDetail;
	
	/** The text observation. */
	private String textObservation;
	
	/** The certificates nums. */
	private List<Long> certificatesNums = new ArrayList<Long>();

	/** The certificate deposite data model. */
	private GenericDataModel<PhysicalCertificateExt> certificateDepositeDataModel;
	
	/** The certificate deposite list. */
	private List<PhysicalCertificateExt> certificateDepositeList;
	
	/** The certificate deposite selected. */
	private PhysicalCertificateExt[] certificateDepositeSelected;
	
	/** The selected reteirement deposit detail. */
	private PhysicalCertificateExt selectedReteirementDepositDetail;
	
	/** The certificate delete. */
	ReteirementDepositeFilterBean certificateDelete;

	/** The Constant CANCEL_TYPE_MASTERKEY. */
	private static final Integer CANCEL_TYPE_MASTERKEY = 287;
	
	/** The Constant RETIRESTATE. */
	private static final Integer RETIRESTATE = 606;
	
	/** The Constant DOMINICA_GEOGRAPHIC_CODE. */
	private static final Integer BOLIVIA_GEOGRAPHIC_CODE = 3513;
	
	/** The Constant SECURITYCLASS_MASTERPK. */
	private static final Integer SECURITYCLASS_MASTERPK = 52;
	
	/** The Constant INSTRUMENT_TYPE_MASTERKEY. */
	private static final Integer INSTRUMENT_TYPE_MASTERKEY = 50;
	
	/** The Constant OPEARTION_TYPE_MASTERKEY. */
	private static final Integer OPEARTION_TYPE_MASTERKEY = 272;
	
	/** The Constant CERTIFICATE_STATUS_MASTERKEY. */
	private static final Integer CERTIFICATE_STATUS_MASTERKEY = 253;
	
	/** The participant login. */
	/* Permisos */
	private Long participantLogin = new Long(0);
	
	/** The is participant. */
	private boolean isParticipant = false;
	
	/** The is cevaldom. */
	private boolean isCevaldom = false;
	
	/** The is bcr. */
	private boolean isBcr = false;
	
	/** The flag register. */
	private boolean flagRegister = false;
	
	/** The flag search. */
	private boolean flagSearch = false;
	
	/** The registers found. */
	private boolean registersFound = true;

	/** The Constant operationApprove. */
	private static final int operationApprove = 1;
	
	/** The Constant operationCancel. */
	private static final int operationCancel = 2;
	
	/** The Constant operationConfirm. */
	private static final int operationConfirm = 3;
	
	/** The Constant operationApply. */
	private static final int operationApply = 5;
	
	/** The Constant operationAuthorized. */
	private static final int operationAuthorized = 6;

	/** The selected currency local. */
	/* nw */
	private Integer selectedCurrencyLocal;
	
	/** The selected currency usp. */
	private Integer selectedCurrencyUSP;
	
	/** The selected currency eur. */
	private Integer selectedCurrencyEUR;
	
	/** The selected currency ufv. */
	private Integer selectedCurrencyUFV;
	
	/** The selected currency dmv. */
	private Integer selectedCurrencyDMV;
	
	/** The selected nominal usp. */
	private BigDecimal selectedNominalUSP;
	
	/** The selected nominal local. */
	private BigDecimal selectedNominalLocal;
	
	/** The selected nominal eur. */
	private BigDecimal selectedNominalEUR;
	
	/** The selected nominal ufv. */
	private BigDecimal selectedNominalUFV;
	
	/** The selected nominal dmv. */
	private BigDecimal selectedNominalDMV;


	/** The selected instrument. */
	private Integer selectedInstrument;
	
	/** The selected security class. */
	private Integer selectedSecurityClass;
	
	/** The holder acc search. */
	private HolderAccount holderAccSearch;
	
	/** The issuer pk. */
	private String issuerPk;
	
	/** The issuer help. */
	private Issuer issuerHelp = new Issuer();
	
	/** The security help. */
	private Security securityHelp = new Security();
	
	/** The deliver req date. */
	private Date deliverReqDate;
	
	/** The show selected. */
	private ReteirementDepositeFilterBean showSelected;
	
	/** The max next work day. */
	private Date maxNextWorkDay;
	
	/** The current date. */
	private Date currentDate;

	/** The physical certificate list added. */
	private List<ReteirementDepositeFilterBean> physicalCertificateListAdded;
	
	/** The phy certificate remove selected. */
	private ReteirementDepositeFilterBean[] phyCertificateRemoveSelected;
	
	/** The physical certificate data model. */
	private GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateDataModel;
	
	/** The lst physical certificate selected. */
	private ReteirementDepositeFilterBean[] lstPhysicalCertificateSelected;
	
	/** The physical certificate list. */
	private List<ReteirementDepositeFilterBean> physicalCertificateList;
	
	/** The physical certificate list added data model. */
	private GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateListAddedDataModel;
	
	/** The rendered instrument type. */
	private boolean renderedInstrumentType = true;
	

	/** The instrument type list. */
	private List<ParameterTable> instrumentTypeList = new ArrayList<ParameterTable>();

	/** The instrument type map. */
	private Map<Integer, String> instrumentTypeMap = new HashMap<Integer, String>();

	/** The payment type list. */
	private List<ParameterTable> paymentTypeList = new ArrayList<ParameterTable>();

	/** The depositRequest type list. */
	private List<ParameterTable> depositRequestStateList = new ArrayList<ParameterTable>();

	/** The depositState type list. */
	private List<ParameterTable> depositStateList = new ArrayList<ParameterTable>();

	/** The ReasonState type list. */
	private List<ParameterTable> ReasonStateList = new ArrayList<ParameterTable>();

	/** The payment type map. */
	private Map<Integer, String> paymentTypeMap = new HashMap<Integer, String>();

	/** The depositRequestState type map. */
	private Map<Integer, String> depositRequestStateTypeMap = new HashMap<Integer, String>();

	/** The depositState type map. */
	private Map<Integer, String> depositStateTypeMap = new HashMap<Integer, String>();

	/** The reasonState type map. */
	private Map<Integer, String> reasonStateTypeMap = new HashMap<Integer, String>();

	/** The money type list. */
	private List<ParameterTable> moneyTypeList = new ArrayList<ParameterTable>();

	/** The money type map. */
	private Map<Integer, String> moneyTypeMap = new HashMap<Integer, String>();

	/** The security class list. */
	private List<ParameterTable> securityClassList = new ArrayList<ParameterTable>();

	/** The security class type map. */
	private Map<Integer, String> securityClassTypeMap = new HashMap<Integer, String>();

	/** The source holder. */
	//helpers
	private Holder sourceHolder = new Holder();
	
	/** The source account to. */
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	
	/** The lst source account to. */
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	/** The map state holder account. */
	Map<Integer, String> mapStateHolderAccount = new HashMap<Integer,String>();
	
	/** The lst holder account state. */
	private List<ParameterTable> lstHolderAccountState = new ArrayList<ParameterTable>();

	/** The dialog physical certificate. */
	private PhysicalCertificateExt dialogPhysicalCertificate = new PhysicalCertificateExt();
	
	/** The param tab to. */
	private ParameterTableTO paramTabTO = new ParameterTableTO();
	
	/** The mp mnemonic class sec. */
	private Map<Integer, String> mpStates, mpMnemonicClassSec;
	
	private List<HolderAccountHelperResultTO> listHolderAccountForView;
	
	private String namePickupManager;
	private String ciPickupManager;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			securityEvents();
			loadParticipant();
			loadParameterReason();
			initialDate =CommonsUtilities.currentDate();
			finalDate =CommonsUtilities.currentDate();
			otherRejectMotiveId = 726;
			certificateDepositeList = new ArrayList<PhysicalCertificateExt>();
			certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>();
			loadCertificateDepositeEstatus();
			loadHolderAccountState();
			sourceHolder = new Holder();
			sourceAccountTo = new HolderAccountHelperResultTO();
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			loadListAndMap();
			listHolidays();
			disabledButtons();
			loadIntrumentTypes();

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Back dialog detail.
	 */
	public void backDialogDetail(){
		JSFUtilities.executeJavascriptFunction("PF('idCertificateRegisterW').hide();");
	}
	
	/**
	 * Gets the physical cert marketfact.
	 *
	 * @param idPhysicalCertificatePk the id physical certificate pk
	 * @return the physical cert marketfact
	 */
	public PhysicalCertMarketfact getPhysicalCertMarketfact(Long idPhysicalCertificatePk){
		return withdrawalDepositeFacadeBean.getPhysicalCertMarketfact(idPhysicalCertificatePk);
	}
	
	/**
	 * Load intrument types.
	 */
	public void loadIntrumentTypes(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		try {
			instrumentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		for (ParameterTable param : instrumentTypeList) {
			instrumentTypeMap.put(param.getParameterTablePk(), param.getParameterName());
	}
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		  Holiday holidayFilter = new Holiday();
		  holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		  GeographicLocation countryFilter = new GeographicLocation();
		  countryFilter.setIdGeographicLocationPk(countryResidence);
		  holidayFilter.setGeographicLocation(countryFilter);		    
		  allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}

	/**
	 * Load holder account state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadHolderAccountState() throws ServiceException{
		lstHolderAccountState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstHolderAccountState.add(param);
			mapStateHolderAccount.put(param.getParameterTablePk(), param.getDescription());
		}
	}
    
    /**
     * Gets the map holder account state.
     *
     * @param code the code
     * @return the map holder account state
     */
    public String getMapHolderAccountState(Integer code){
		return mapStateHolderAccount.get(code);
	}
    
    /**
     * Valid source holder.
     */
    public void validSourceHolder(){
		
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())){
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sourceHolder.getIdHolderPk());
			holderTO.setParticipantFk(participantSelected);
			holderTO.setAccountState(HolderAccountStatusType.ACTIVE.getCode());
			
			try {
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				
				if(lstAccountTo.size()>0){
					if(lstAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
						sourceAccountTo=lstAccountTo.get(0);
						validSourceHolderAccount();
						
					}
					lstSourceAccountTo = lstAccountTo;
				} else{
					sourceHolder = new Holder();
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = new HolderAccountHelperResultTO();
					//holderAccount = new HolderAccount();
					//String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT);
					String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED);
					alert(mensaje);
				}
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		}
	}
    
	/**
	 * Security events.
	 */
	public void securityEvents() {

		try {

			if (userPrivilege.getUserAcctions().isRegister())
				flagRegister = true;

			if (userPrivilege.getUserAcctions().isSearch())
				flagSearch = true;

			participantLogin = userInfo.getUserAccountSession()
					.getParticipantCode();

			if (userInfo.getUserAccountSession().isInstitution(InstitutionType.BCR)) {

				isBcr = true;

			} else if (userInfo.getUserAccountSession().isParticipantInstitucion()) {
				participantSelected = participantLogin;
				participant = certificateDepositeBeanFacade.findParticipant(participantLogin);
				isParticipant = true;

				if (participant.getState().equals(
						ParticipantStateType.BLOCKED.getCode())) {
					flagRegister = false;
				}

			} else { // si no es niuno es cevaldom
				isCevaldom = true;
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Clear search retirement physical certificates.
	 */
	public void clearSearchRetirementPhysicalCertificates() {
		try {
			holderAccount = new HolderAccount();
			participantSelected = null;
			stateSelected = null;
			issuerSearchPK = null;
			textObservation = null;
			initialDate =CommonsUtilities.currentDate();
			finalDate = CommonsUtilities.currentDate();
			docNumber = null;
			selectedReqNumber = null;
			certificateDepositeList = new ArrayList<PhysicalCertificateExt>();
			certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>();
			sourceHolder = new Holder();
			registersFound = true;
			issuerHelp= new Issuer();
			disabledButtons();
			securityEvents();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load list and map.
	 */
	public void loadListAndMap() {
		try {

			if (ReasonStateList == null || ReasonStateList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.REASON_REJECT_ANULLED_REQUEST.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				ReasonStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : ReasonStateList) {
					reasonStateTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (depositStateList == null || depositStateList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.DEPOSIT_STATE.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				depositStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : depositStateList) {
					depositStateTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (depositRequestStateList == null || depositRequestStateList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.DEPOSIT_STATE_REQUEST.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				depositRequestStateList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : depositRequestStateList) {
					depositRequestStateTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (instrumentTypeList == null || instrumentTypeList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				instrumentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : instrumentTypeList) {
					instrumentTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (paymentTypeList == null || paymentTypeList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.INTEREST_PERIODICITY.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				paymentTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : paymentTypeList) {
					paymentTypeMap.put(param.getParameterTablePk(),param.getParameterName());
				}

			}

			if (moneyTypeList == null || moneyTypeList.size() <= 0) {

				ParameterTableTO parameterTableTO = new ParameterTableTO();
				parameterTableTO.setMasterTableFk(MasterTableType.CURRENCY.getCode());
				parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
				moneyTypeList = generalParametersFacade.getComboParameterTable(parameterTableTO);
				for (ParameterTable param : moneyTypeList) {
					moneyTypeMap.put(param.getParameterTablePk(),param.getText1());
				}

			}
			fillSecurityClass();

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Fill security class.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillSecurityClass() throws ServiceException{
		securityClassList = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		paramTable.setOrderByText1(1);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			securityClassList.add(param);
			securityClassTypeMap.put(param.getParameterTablePk(), param.getText1());
		}
		
		if(super.getParametersTableMap()==null){
			super.setParametersTableMap(new HashMap<Integer,Object>());
		}
		billParametersTableMap(securityClassList, true);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
//	public void fillSecurityClass() {
//		mpStates=new HashMap<>();
//		mpMnemonicClassSec=new HashMap<>();
//		paramTabTO = new ParameterTableTO();
//		paramTabTO.setMasterTableFk( MasterTableType.SECURITIES_CLASS.getCode() );
//		List<ParameterTable> lstSecClasses;
//		try {
//			lstSecClasses = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
//			for(ParameterTable paramTab:lstSecClasses){
//				if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()))
//					securityClassList.add(paramTab);
//				mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
//				mpMnemonicClassSec.put(paramTab.getParameterTablePk(), paramTab.getText1());
//			}
//		} catch (ServiceException e) {
//			e.printStackTrace();
//		}
/**
 * Search retirement physical certificates.
 */
//	}
	@LoggerAuditWeb
	public void searchRetirementPhysicalCertificates(){
		boolean requiredFields = true;

		if(requiredFields){
			try {
				
				PhysicalCertificateFilter physicalCertificateFilter = new PhysicalCertificateFilter();
				

				if(Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()))
					physicalCertificateFilter.setCuiHolder(sourceHolder.getIdHolderPk());
				
				if(Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk()))
					physicalCertificateFilter.setHolderAccountPk(holderAccount.getIdHolderAccountPk());
	
				if(Validations.validateIsNotNullAndPositive(participantSelected))
					physicalCertificateFilter.setParticipantPk(participantSelected);
	
				if(Validations.validateIsNotNullAndPositive(stateSelected))
					physicalCertificateFilter.setStatus(stateSelected);
	
				if(Validations.validateIsNotNull(issuerSearchPK))
					physicalCertificateFilter.setIssuerPk(issuerSearchPK);
	
				if(Validations.validateIsNotNull(initialDate))
					physicalCertificateFilter.setInitialDate(initialDate);
	
				if(Validations.validateIsNotNull(finalDate))
					physicalCertificateFilter.setFinalDate(finalDate);
	
				if(Validations.validateIsNotNullAndPositive(docNumber))
					physicalCertificateFilter.setCertificateNumber(docNumber);
	
				if(Validations.validateIsNotNullAndPositive(selectedReqNumber))
					physicalCertificateFilter.setRequestNumber(selectedReqNumber);
				
				List<PhysicalCertificateExt> searchData = withdrawalDepositeFacadeBean.getPhysicalCertificateRetirementSearchList(physicalCertificateFilter);
				loadListAndMap();
				
				certificateDepositeList = new ArrayList<PhysicalCertificateExt>();
				certificateDepositeList = searchData;
				ReteirementDepositeFilterBean modifyData = null;
				
				if(searchData != null && searchData.size()>0 ){

					String idSecurityCodePkAux;
					Security securityAux;
			    	
					for (PhysicalCertificateExt certificate : searchData) {
    					
    					certificate.setStateType(depositStateTypeMapDescription(certificate.getState()));
    					
    					idSecurityCodePkAux=certificateDepositeBeanFacade.getIdSecurityCodeNoExists(certificate.getPhysicalCertificate().getIdPhysicalCertificatePk());
    	    			//obteniendo el Valor con su Emisor
    	    			securityAux = certificateDepositeBeanFacade.getSecurityWithIssuer(idSecurityCodePkAux);
    	    			certificate.getPhysicalCertificate().setSecurities(securityAux);
    					
    					if(certificate.getPhysicalCertificate().getSecurityClass() != null)
    						certificate.getPhysicalCertificate().setClassType(securityClassTypeMapDescription(certificate.getPhysicalCertificate().getSecurityClass()));
    					
    					if(certificate.getPhysicalOperationDetail().getState() != null)
    						certificate.getPhysicalCertificate().setStateType(depositStateTypeMapDescription(certificate.getPhysicalOperationDetail().getState()));
    					
    					if (certificate.getPhysicalOperationDetail().getRejectMotive() != null){
    						certificate.getPhysicalOperationDetail().setMotiveType(generalParametersFacade.getParameterTableById(certificate.getPhysicalOperationDetail().getRejectMotive()).getParameterName());
    					}
    					
    					if (certificate.getPhysicalCertificate().getCurrency() != null)
    						certificate.getPhysicalCertificate().setCurrencyType(moneyTypeMap.get(certificate.getPhysicalCertificate().getCurrency()));
    					
    					if (Validations.validateIsNotNullAndPositive(certificate.getParticipantPk())) {
    						certificate.setMnemonic(findParticipantSelected(certificate.getParticipantPk()).getMnemonic());
    					}
    					
    					if(certificate.getRequestState() != null){
    						certificate.setRequestStateType(depositRequestStateTypeMapDescription(certificate.getRequestState()));
    					}
    					
    					if(certificate.getPhysicalOperationDetail().getIndApplied().equals(0)){
    						certificate.setIndApplied(false);
    					}else if(certificate.getPhysicalOperationDetail().getIndApplied().equals(1)){
    						certificate.setIndApplied(true);
    					}
    					
    					if (BooleanType.YES.getCode().equals(certificate.getPhysicalOperationDetail().getIndDematerialized())) {
    						certificate.setIndDematerialization(true);
    					} else {
    						certificate.setIndDematerialization(false);
    					}
    				}
    				
    				certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>(searchData);
    				registersFound = true;
    				
    				if( Validations.validateIsNotNullAndPositive(participant.getState()) &&
    					participant.getState().equals(ParticipantStateType.BLOCKED.getCode())
    				){
    					disabledButtons();
    				}else{
    					enabledButtons();	
    				}
    				
				}else{
					disabledButtons();
					certificateDepositeDataModel = new GenericDataModel<PhysicalCertificateExt>();
					registersFound = false;
				}
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
	/**
	 * Instrument type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String instrumentTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return instrumentTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	/**
	 * Payment type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String paymentTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return paymentTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Money type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String moneyTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return moneyTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Security class type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String securityClassTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return securityClassTypeMap.get(code);
		}else{
			return "";
		}
	}
	
	/**
	 * Deposit request state type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String depositRequestStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return depositRequestStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Deposit state type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String depositStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return depositStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Reason state type map description.
	 *
	 * @param code the code
	 * @return the string
	 */
	public String reasonStateTypeMapDescription(Integer code){
		if(Validations.validateIsNotNullAndPositive(code)){
			return reasonStateTypeMap.get(code);
		}else{
			return "";
		}
	}

	/**
	 * Aprove certificates.
	 *
	 * @param single the single
	 * @return the string
	 */
	@LoggerAuditWeb
	public String aproveCertificates(boolean single) {
		confirmAlertAction = "retirementCertificateApprove";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.APPROVED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.APPROVED.getCode(), certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<Long>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add( Long.valueOf(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber()) );
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.REGISTERED.getCode(),StateType.APPROVED.getCode(), certs);
		}
	}
	
	/**
	 * Cancel certificates.
	 *
	 * @param single the single
	 * @return the string
	 */
	@LoggerAuditWeb
	public String cancelCertificates(boolean single) {

		if (single) {
			alertAnulledOrReject(new Integer(StateType.ANNULLED.getCode()));
			return "";
		} else {
			certificatesNums = new ArrayList<Long>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add( Long.valueOf(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber()) );
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.REGISTERED.getCode(),StateType.ANNULLED.getCode(),certs); 
		}
	}
	
	/**
	 * Reject certificates.
	 *
	 * @param single the single
	 * @return the string
	 */
	@LoggerAuditWeb
	public String rejectCertificates(boolean single) {
		
		if (single) {
			alertAnulledOrReject(new Integer(StateType.REJECTED.getCode()));
			return "";
		} else {
			certificatesNums = new ArrayList<Long>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add( Long.valueOf(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber()) );
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.APPROVED.getCode(),StateType.REJECTED.getCode(),certs);	
		}
	}
	
	/**
	 * Apply certificates.
	 *
	 * @param single the single
	 * @return the string
	 */
	@LoggerAuditWeb
	public String applyCertificates(boolean single) {
		
		confirmAlertAction = "retirementCertificateApply";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.APPLY
					.getCode());
			String bodyMessage = confirmbodyMessage(StateType.APPLY.getCode(),
					certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<Long>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(Long.valueOf(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber()));
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.CONFIRMED.getCode(),StateType.APPLY.getCode(),certs);
		}
	}

	/**
	 * Confirm certificates.
	 *
	 * @param single the single
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmCertificates(boolean single) {

		confirmAlertAction = "retirementCertificateConfirm";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.CONFIRMED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.CONFIRMED.getCode(), certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<Long>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add(Long.valueOf(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber()));
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.APPROVED.getCode(), StateType.CONFIRMED.getCode(),certs);
		}
	}

	/**
	 * Authorize certificates.
	 *
	 * @param single the single
	 * @return the string
	 */
	@LoggerAuditWeb
	public String authorizeCertificates(boolean single) {
		confirmAlertAction = "retirementCertificateAuthorization";

		if (single) {
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = confirmHeaderMessage(StateType.AUTHORIZED.getCode());
			String bodyMessage = confirmbodyMessage(StateType.AUTHORIZED.getCode(), certs);

			question(headerMessage, bodyMessage);
			return "";
		} else {
			certificatesNums = new ArrayList<Long>();
			for (int i = 0; i < certificateDepositeSelected.length; i++) {
				certificatesNums.add( Long.valueOf(certificateDepositeSelected[i].getPhysicalCertificate().getCertificateNumber()) );
			}
			String certs = certificateNumberMessage(certificatesNums);
			
			return ejecutarAccion(StateType.CONFIRMED.getCode(),StateType.AUTHORIZED.getCode(),certs);
		}
	}

	/**
	 * Ejecutar accion.
	 *
	 * @param actualState the actual state
	 * @param newState the new state
	 * @param certs the certs
	 * @return the string
	 */
	public String ejecutarAccion(Integer actualState, Integer newState, String certs) {
		String certificateValidated=null;
		if (certificateDepositeSelected != null && certificateDepositeSelected.length > 0) {

			if (certificateDepositeSelected.length == 1) {

				if (certificateDepositeSelected[0].getState().equals(StateType.CONFIRMED.getCode())
					&& Validations.validateIsNotNull(certificateDepositeSelected[0].getIndApplied())
					&& !newState.equals(StateType.AUTHORIZED.getCode())) {
					
					if (certificateDepositeSelected[0].getIndApplied()) {

						String messageBody = PropertiesUtilities.getMessage(
								PropertiesConstants.MSG_APPLIED_PREVIOUSLY,
								new Object[] { certificateDepositeSelected[0].getPhysicalCertificate().getCertificateNumber().toString() });

						alert(messageBody);
						return "";
					}
				}

				if (certificateDepositeSelected[0].getState().equals(StateType.CONFIRMED.getCode())
					&& Validations.validateIsNotNull(certificateDepositeSelected[0].getIndApplied())
					&& newState.equals(StateType.AUTHORIZED.getCode())) {
					if (!certificateDepositeSelected[0].getIndApplied()) {

						String messageBody = PropertiesUtilities.getMessage(PropertiesConstants.ERRO_AUTHORIZE, 
								new Object[] { certificateDepositeSelected[0].getPhysicalCertificate().getCertificateNumber().toString() });
						alert(messageBody);
						return "";
					}
				}
				certificateValidated=validLisCertificateForState(actualState, newState);
				if (certificateValidated.equals(GeneralConstants.EMPTY_STRING)) {
					if (certificateDepositeSelected[0].getState().equals(actualState)) {
						// Redireccionar al detalle para ejecutar la accion
						enabledButtonsWithState(newState.intValue());
						return viewCertificationDetail(certificateDepositeSelected[0]);
					} else {
						// manda alerta de que no se puede ejecutar la accion
						String messageBody = previouslyMessageAlert(newState);
						alert(messageBody);
					}
				} else {
					String messageBody = errorStateMessageAlert(newState,certificateValidated);
					alert(messageBody);
				}

			} else {
				certificateValidated=validLisCertificateForState(actualState, newState);
				if(certificateValidated.equals(GeneralConstants.EMPTY_STRING)) {

					if (newState.equals(StateType.ANNULLED.getCode()) || newState.equals(StateType.REJECTED.getCode())) {

						alertAnulledOrReject(newState);

					} else {

						// Mando el dialogo a ejecutar la accion multiple
						certs = certificateNumberMessage(certificatesNums);
						String headerMessage = confirmHeaderMessage(newState);
						String bodyMessage = confirmbodyMessage(newState, certs);
						question(headerMessage, bodyMessage);
					}

				} else {
					// manda alerta de que no se puede ejecutar la accion
					String messageBody = errorStateMessageAlert(newState, certificateValidated);
					alert(messageBody);
				}
			}

		} else {
			String headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MODIFY_ELIMINAR_CERTIFICATE_SELECT);
			alert(headerMessage, bodyMessage);
		}

		return "";
	}

	/**
	 * Change participant.
	 */
	public void changeParticipant() {
		holderAccount = new HolderAccount();
		holderAccount.setAccountNumber(null);
		docNumber = null;
	}

	/**
	 * Change participant and delete info.
	 */
	public void changeParticipantAndDeleteInfo() {

		if (Validations.validateIsNullOrNotPositive(participantSelected)) {
			holderAccount = new HolderAccount();
			holderAccount.setAccountNumber(null);
			docNumber = null;
		} else {
			if (!participantSelected.equals(participantSelectedTmp)) {

				if (physicalCertificateListAdded.size() > 0 || physicalCertificateList.size() > 0) {

					String headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
					String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CHANGE_PARTICIPANT_QUESTION);

					question(headerMessage, bodyMessage);
					confirmAlertAction = "executeChangeParticipant";

				} else {
					participantSelectedTmp = participantSelected;
					sourceHolder = new Holder();
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = null;
					holderAccount = new HolderAccount();
					holderAccount.setAccountNumber(null);
					docNumber = null;
				}
			}
		}
	}

	/**
	 * Execute change participant.
	 */
	public void executeChangeParticipant() {

		holderAccount = new HolderAccount();
		issuerPk = null;
		docNumber = null;
		selectedInstrument = null;
		selectedSecurityClass = null;
		physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
		physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();

		deliverReqDate = CommonsUtilities.currentDate();
		finalDate = null;//CommonsUtilities.currentDate();
		initialDate =null; //CommonsUtilities.currentDate();
		maxNextWorkDay = getNextWorkDay(CommonsUtilities.currentDate(), 1);
		registersFound = true;

		participantSelected = participantSelectedTmp;
	}

	/**
	 * Valid lis certificate for state.
	 *
	 * @param actualState the actual state
	 * @param newState the new state
	 * @return the string
	 */
	public String validLisCertificateForState(Integer actualState, Integer newState) {
		String strCertificateValidate=GeneralConstants.EMPTY_STRING;
		certificatesNums = new ArrayList<Long>();
		for (PhysicalCertificateExt certificate : certificateDepositeSelected) {

			if (certificate.getState().equals(StateType.CONFIRMED.getCode()) && !newState.equals(StateType.AUTHORIZED.getCode())) {
				if (certificate.getIndApplied())
					strCertificateValidate+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			}

			if (certificate.getState().equals(StateType.CONFIRMED.getCode())
				&& newState.equals(StateType.AUTHORIZED.getCode())) {
				if (!certificate.getIndApplied())
					strCertificateValidate+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			}

			if (!certificate.getState().equals(actualState)) {
				strCertificateValidate+=certificate.getPhysicalCertificate().getCertificateNumber()+GeneralConstants.STR_COMMA_WITHOUT_SPACE;
			} else {
				certificatesNums.add( Long.valueOf(certificate.getPhysicalCertificate().getCertificateNumber()) );
			}
		}
		if(strCertificateValidate.endsWith(GeneralConstants.STR_COMMA_WITHOUT_SPACE))
			strCertificateValidate=strCertificateValidate.substring(0,strCertificateValidate.length()-1);
		return strCertificateValidate;
	}

	/**
	 * Retirement certificate approve.
	 */
	@LoggerAuditWeb
	public void retirementCertificateApprove() {
		try {

			if (certificatesNums != null) {
				certificatesNums.clear();
			} else {
				certificatesNums = new ArrayList<Long>();
			}

			PhysicalOperationDetail physicalOperationDetail = null;

			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {

				physicalOperationDetail = retiroBean.getPhysicalOperationDetail();
				
				if (physicalOperationDetail.getState().equals(StateType.APPROVED.getCode())) {
					certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );
				}
			}

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<Long>();
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );

				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.APPROVED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setOperation(operationApprove);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				certificateDepositeSelected = null;
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);

			}
			certificateDepositeBeanFacade.updateDetailApproveRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.APPROVE_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_APPROVE.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/**
	 * Retirement certificate anuller.
	 */
	@LoggerAuditWeb
	public void retirementCertificateAnuller() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<Long>();
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setLastModifyApp(new Integer(1));
				commonUpdateBeanInf.setState(StateType.ANNULLED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				if (rejectOther != null) {
					commonUpdateBeanInf.setCancelDesc(rejectOther);
				}
				commonUpdateBeanInf.setRejectMotive(reasonSelected);
				commonUpdateBeanInf.setOperation(operationCancel);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}
			certificateDepositeBeanFacade.updateDetailCancelRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);
			certificateDepositeSelected = null;

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ANULER_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CANCEL.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/

		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}
	
	/**
	 * Retirement certificate rejected.
	 */
	@LoggerAuditWeb
	public void retirementCertificateRejected() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<Long>();
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setLastModifyApp(new Integer(1));
				commonUpdateBeanInf.setState(StateType.REJECTED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				if (rejectOther != null) {
					commonUpdateBeanInf.setCancelDesc(rejectOther);
				}
				commonUpdateBeanInf.setRejectMotive(reasonSelected);
				commonUpdateBeanInf.setOperation(operationCancel);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}
			certificateDepositeBeanFacade.updateDetailCancelRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);
			certificateDepositeSelected = null;

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.REJECTED_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CANCEL.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/

		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}

	/**
	 * Retirement certificate apply.
	 */
	@LoggerAuditWeb
	public void retirementCertificateApply() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			certificatesNums = new ArrayList<Long>();
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.CONFIRMED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setIntAppilied(1);
				commonUpdateBeanInf.setOperation(operationApply);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				//commonUpdateBeanInf.setSituation(SituationType.DEPOSITARY_MESSENGER.getCode());
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}

			certificateDepositeBeanFacade.updateDetailApplyRetirement(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);
			certificateDepositeSelected = null;

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.APLICAR_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_APPLY.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, participantPk, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}
	}

	/**
	 * Retirement certificate confirm.
	 */
	@LoggerAuditWeb
	public void retirementCertificateConfirm() {
		try {
			certificatesNums = new ArrayList<Long>();
			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			Long participantPk = null;
			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				commonUpdateBeanInf.setState(StateType.CONFIRMED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setOperation(operationConfirm);
				commonUpdateBeanInf.setIntAppilied(0);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				participantPk = retiroBean.getParticipantPk();
			}
			certificateDepositeBeanFacade.updateDetailConfirmDeposit(lstCommonUpdateBeanInf);

			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_SUCCESS_MESSAGE, certs);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_CONFIRM.getCode());

			if (isCevaldom) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, participantPk, null);
			} else if (isBcr) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			} else if (isParticipant) {
				notificationServiceFacade.sendNotification(userInfo
						.getUserAccountSession().getUserName(),
						businessProcess, null, null);
			}
			/*** end JH NOTIFICACIONES ***/
		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}

	/**
	 * Retirement certificate authorization.
	 */
	@LoggerAuditWeb
	public void retirementCertificateAuthorization() {
		try {

			List<CommonUpdateBeanInf> lstCommonUpdateBeanInf = new ArrayList<CommonUpdateBeanInf>();
			CommonUpdateBeanInf commonUpdateBeanInf = null;
			List<PhysicalCertificate> lstSituation = new ArrayList<PhysicalCertificate>();
			PhysicalCertificate situationCert = null;
			certificatesNums = new ArrayList<Long>();

			for (PhysicalCertificateExt retiroBean : certificateDepositeSelected) {
				
				certificatesNums.add( Long.valueOf(retiroBean.getPhysicalCertificate().getCertificateNumber()) );
				commonUpdateBeanInf = new CommonUpdateBeanInf();
				situationCert = new PhysicalCertificate();
				if (userInfo != null) {
					commonUpdateBeanInf.setLastModifyIp(userInfo.getUserAccountSession().getIpAddress());
					commonUpdateBeanInf.setCurrentUserName(userInfo.getUserAccountSession().getUserName());
				}
				commonUpdateBeanInf.setLastModifyApp(new Integer(1));
				commonUpdateBeanInf.setState(StateType.AUTHORIZED.getCode());
				commonUpdateBeanInf.setCurrentUpdateDate(CommonsUtilities.currentDateTime());
				commonUpdateBeanInf.setIdCustodyOperationPk(retiroBean.getIdCustodyOperationPk());
				commonUpdateBeanInf.setCertificatePk(retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
				commonUpdateBeanInf.setStockQuantity(retiroBean.getPhysicalCertificate().getCertificateQuantity());
				//commonUpdateBeanInf.setPhysicalBalanceDetailPk(retiroBean.getIdPhysicalOperationDetail());
				commonUpdateBeanInf.setOperation(operationAuthorized);
				commonUpdateBeanInf.setOperationType(new Long(RequestType.RETIRO.getCode()));
				commonUpdateBeanInf.setCurrency(retiroBean.getPhysicalCertificate().getCurrency());
				commonUpdateBeanInf.setNominalValue(retiroBean.getPhysicalCertificate().getNominalValue());
				commonUpdateBeanInf.setRequestState(RETIRESTATE);
				commonUpdateBeanInf.setAccHolderPk(retiroBean.getPhysicalCertificateOperation().getHolderAccount().getIdHolderAccountPk());
				withdrawalDepositeFacadeBean.authorizarFacade(commonUpdateBeanInf);
				withdrawalDepositeFacadeBean.updateSecurities(retiroBean.getPhysicalCertificate().getSecurities(),retiroBean.getPhysicalCertificate().getCertificateQuantity());
				situationCert.setIdPhysicalCertificatePk(commonUpdateBeanInf.getCertificatePk());
				situationCert.setRequestNumber(commonUpdateBeanInf.getIdCustodyOperationPk()); 
				situationCert.setSituation(SituationType.PARTICIPANT.getCode());
				lstSituation.add(situationCert);
				lstCommonUpdateBeanInf.add(commonUpdateBeanInf);
				
				//we verify if the withdrawal of certificate because dematerialized way
				if (BooleanType.YES.getCode().equals(retiroBean.getPhysicalOperationDetail().getIndDematerialized())) {
					createAccountAnnotationOperation(retiroBean);
				}
			}

			certificateDepositeBeanFacade.updateDetailAutorizedRetirement(lstCommonUpdateBeanInf);
			String certs = certificateNumberMessage(certificatesNums);

			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.AUTHORIZAR_SUCCESS_MESSAGE, certs);
			
			//Enviando la solicitud de retiro al AYNI 1210
			XmlRespuestaActivoFinanciero xmlRespuestaActivoFinanciero = certificateDepositeBeanFacade.sendReteirementRecord(certificateDepositeSelected);
			if(xmlRespuestaActivoFinanciero.getCodResp().equalsIgnoreCase("0000")){
				bodyMessage = bodyMessage + ". " + PropertiesUtilities.getMessage(PropertiesConstants.MSG_AUTORIZAR_SUCCESS_AYNI_0000);
			}else{
				bodyMessage = bodyMessage + ". " + PropertiesUtilities.getMessage(PropertiesConstants.MSG_AUTORIZAR_SUCCESS_AYNI_1000);
				bodyMessage = bodyMessage + " " + xmlRespuestaActivoFinanciero.getDescResp();
			}
			
			alert(headerMessage, bodyMessage);

		} catch (ServiceException e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
		}

	}

	
	/**
	 * Creates the account annotation operation.
	 *
	 * @param retiroBean the retiro bean
	 * @throws ServiceException the service exception
	 */
	private void createAccountAnnotationOperation(PhysicalCertificateExt retiroBean) throws ServiceException
	{	
		AccountAnnotationOperation objAccountAnnotationOperation= new AccountAnnotationOperation();
		objAccountAnnotationOperation.setHolderAccount(retiroBean.getPhysicalCertificateOperation().getHolderAccount());
		objAccountAnnotationOperation.setSecurity(retiroBean.getPhysicalCertificate().getSecurities());
		objAccountAnnotationOperation.setTotalBalance(retiroBean.getPhysicalCertificate().getCertificateQuantity());
		objAccountAnnotationOperation.setCertificateNumber(retiroBean.getPhysicalCertificate().getCertificateNumber().toString());
		objAccountAnnotationOperation.setState(DematerializationStateType.APPROVED.getCode());
		objAccountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
		objAccountAnnotationOperation.setComments(PropertiesUtilities.getMessage(PropertiesConstants.MSG_COMMENT_WITHDRAWAL_BY_DEMATERIALIZATION));
		
		AccountAnnotationMarketFact objAccountAnnotationMarketFact= new AccountAnnotationMarketFact();
		PhysicalCertMarketfact objPhysicalCertMarketfact= certificateDepositeBeanFacade.getPhysicalCertMarketfact(
																		retiroBean.getPhysicalCertificate().getIdPhysicalCertificatePk());
		objAccountAnnotationMarketFact.setMarketDate(objPhysicalCertMarketfact.getMarketDate());
		objAccountAnnotationMarketFact.setMarketPrice(objPhysicalCertMarketfact.getMarketPrice());
		objAccountAnnotationMarketFact.setMarketRate(objPhysicalCertMarketfact.getMarketRate());
		objAccountAnnotationMarketFact.setOperationQuantity(objPhysicalCertMarketfact.getQuantity());
		
		RegisterAccountAnnotationTO objRegisterAccountAnnotationTO= new RegisterAccountAnnotationTO();
		objRegisterAccountAnnotationTO.setBlLien(false);
		objRegisterAccountAnnotationTO.setAccountAnnotationMarketFact(objAccountAnnotationMarketFact);
		objRegisterAccountAnnotationTO.setAccountAnnotationOperation(objAccountAnnotationOperation);
		
		dematerializationCertificateFacade.registerAccountAnnotationOperationOriginal(objRegisterAccountAnnotationTO);
	}
	
	/**
	 * Previously message alert.
	 *
	 * @param actualState the actual state
	 * @return the string
	 */
	public String previouslyMessageAlert(Integer actualState) {
		if (actualState.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPROVED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_CANCELLED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRMED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_REJECTED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.APPLY.getCode())) { 

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPLIED_PREVIOUSLY_MULTI);

		} else if (actualState.equals(StateType.AUTHORIZED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPLIED_PREVIOUSLY_AUTHORIZED);

		}

		return "";
	}

	/**
	 * Error state message alert.
	 *
	 * @param actualState the actual state
	 * @param params the params
	 * @return the string
	 */
	public String errorStateMessageAlert(Integer actualState, String params) {
		if (actualState.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERRO_APPROVAL,params);

		} else if (actualState.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CANCEL,params);

		} else if (actualState.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CONFIRM,params);

		} else if (actualState.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_REJECT,params);

		} else if (actualState.equals(StateType.APPLY.getCode())) { // APPLY

			return PropertiesUtilities.getMessage(PropertiesConstants.ERROR_APPLY,params);

		} else if (actualState.equals(StateType.AUTHORIZED.getCode())) { // APPLY

			return PropertiesUtilities.getMessage(PropertiesConstants.ERRO_AUTHORIZE,params);

		}

		return "";
	}

	/**
	 * Confirm header message.
	 *
	 * @param state the state
	 * @return the string
	 */
	public String confirmHeaderMessage(Integer state) {

		if (state.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE);

		} else if (state.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ANNUL);

		} else if (state.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);

		} else if (state.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT);

		} else if (state.equals(StateType.APPLY.getCode())) { // APPLY

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPLY);

		} else if (state.equals(StateType.AUTHORIZED.getCode())) { // APPLY

			return PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_AUTHORIZE);
		}

		return "";
	}

	/**
	 * Confirmbody message.
	 *
	 * @param state the state
	 * @param certs the certs
	 * @return the string
	 */
	public String confirmbodyMessage(Integer state, String certs) {

		if (state.equals(StateType.APPROVED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_CONFIRM_APPROVAL_MULTI, certs);

		} else if (state.equals(StateType.ANNULLED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_CONFIRM_ANNULAR_MULTI, certs);

		} else if (state.equals(StateType.CONFIRMED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_CONFIRM_CERTIFIACTE_MULTI, certs);

		} else if (state.equals(StateType.REJECTED.getCode())) {

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_REJECT_CERTIFICATE_MULTI, certs);

		} else if (state.equals(StateType.APPLY.getCode())) { 

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_APPLY_OPER_MULTI, certs);

		} else if (state.equals(StateType.AUTHORIZED.getCode())) { 

			return PropertiesUtilities.getMessage(
					PropertiesConstants.MSG_AUTHORIZED_OPER_MULTI, certs);
		}

		return "";
	}

	/**
	 * Certificate number message.
	 *
	 * @param certificates the certificates
	 * @return the string
	 */
	private String certificateNumberMessage(List<Long> certificates) {
		String certs = StringUtils.EMPTY;
		if (certificates != null && certificates.size() > 0) {
			for (Long crt : certificates) {
				if (StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)) {
					if(crt!=null)
						certs = crt.toString();
				} else {
					certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt
							.toString()));
				}
			}
		}
		return certs;
	}

	/**
	 * Back to search.
	 *
	 * @param flagValida the flag valida
	 * @return the string
	 */
	public String backToSearch(boolean flagValida) {
		if (flagValida) {
			if (existChangesInRegistry()) {
				String headerMessage = PropertiesUtilities
						.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
				String bodyMessage = PropertiesUtilities
						.getMessage(PropertiesConstants.MSG_BACK_VALIDATION);

				question(headerMessage, bodyMessage);
				confirmAlertAction = "executeBackToSearch";

				return "";
			} else {
				return "search";
			}
		} else {
			return executeBackToSearch();
		}
	}

	/**
	 * Valid clean registry.
	 */
	public void validCleanRegistry() {

		if (existChangesInRegistry()) {

			String headerMessage = PropertiesUtilities
					.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities
					.getMessage(PropertiesConstants.LBL_MSG_CLEAR_SCREEN);

			question(headerMessage, bodyMessage);
			confirmAlertAction = "cleanRegistry";
		}
	}

	/**
	 * Clean registry.
	 */
	public void cleanRegistry() {
		participantSelected = null;
		holderAccount = new HolderAccount();
		issuerPk = null;
		issuerHelp = new Issuer();
		securityHelp = new Security();
		docNumber = null;
		textObservation= null;
		selectedInstrument = null;
		selectedSecurityClass = null;
		physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
		physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
		physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();

		sourceHolder = new Holder();
		sourceAccountTo = new HolderAccountHelperResultTO();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		
		deliverReqDate = CommonsUtilities.currentDate();
		finalDate = null;//CommonsUtilities.currentDate();
		initialDate = null;//CommonsUtilities.currentDate();
		maxNextWorkDay = getNextWorkDay(CommonsUtilities.currentDate(), 1);
		registersFound = true;
		securityEvents();
    	JSFUtilities.resetViewRoot();
	}

	/**
	 * Execute back to search.
	 *
	 * @return the string
	 */
	public String executeBackToSearch() {

//		cleanSearchInputData();
		clearSearchRetirementPhysicalCertificates();
		selectedReqNumber = null;
		docNumber = null;
		certificateDepositeSelected = null;
		if (certificateDepositeList.size() > 0)
			enabledButtons();

		return "search";
	}

	/**
	 * Exist changes in registry.
	 *
	 * @return true, if successful
	 */
	public boolean existChangesInRegistry() {
		Integer INT_NO_SELECTED = Integer.parseInt("-1");
		Long LNG_NO_SELECTED = Long.parseLong("-1");
		// VALIDATE IF WE HAVE MODIFIED ANY VALUE
		if ((participantSelected == null || LNG_NO_SELECTED.equals(participantSelected))
				&& holderAccount.getIdHolderAccountPk() == null
				&& (issuerPk == null || StringUtils.EMPTY.equals(issuerPk))
				&& (docNumber == null)
				&& (selectedInstrument == null || INT_NO_SELECTED.equals(selectedInstrument))
				&& (selectedSecurityClass == null || INT_NO_SELECTED.equals(selectedSecurityClass))) {
			return false;
		}
		return true;
	}

	/**
	 * View certification detail.
	 *
	 * @param rowPhysicalCertificatesExt the row physical certificates ext
	 * @return the string
	 */
	public String viewCertificationDetail(
			PhysicalCertificateExt rowPhysicalCertificatesExt) {
		certificateDepositeSelected = new PhysicalCertificateExt[1];
		certificateDepositeSelected[0] = rowPhysicalCertificatesExt;
		
		if( certificateDepositeSelected[0].getPhysicalOperationDetail().getPhysicalCertificate().getSecurities().getInstrumentType().equals(new Integer(399)) ){
			renderedInstrumentType = false;
		}else{
			renderedInstrumentType = true;
		}
		selectedReteirementDepositDetail = rowPhysicalCertificatesExt;
		holderAccountDetail = certificateDepositeSelected[0].getPhysicalCertificateOperation().getHolderAccount();
		
		listHolderAccountForView = new ArrayList<>();
		HolderTO holderTO = new HolderTO();
		holderTO.setLstHolderAccountId(new ArrayList<Long>());
		
		holderTO.getLstHolderAccountId().add(holderAccountDetail.getIdHolderAccountPk());
		
		try {
			listHolderAccountForView = helperComponentFacade.searchAccountByFilter(holderTO);
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return "singleRecord";
	}

	/**
	 * Goto new retirement certificate.
	 *
	 * @return the string
	 */
	public String gotoNewRetirementCertificate() {

		try{
			clearSearchRetirementPhysicalCertificates();
			securityEvents();
			physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
			physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
			physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
			physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>();
	
			deliverReqDate = CommonsUtilities.currentDate();
			finalDate =null;//CommonsUtilities.currentDate();
			initialDate =null;// CommonsUtilities.currentDate();//aca
			issuerPk = null;
			maxNextWorkDay = getNextWorkDay(CommonsUtilities.currentDate(), 1);
			loadSecurityClass();
			loadListAndMap();
			registersFound = true;
			
			sourceHolder = new Holder();
			sourceAccountTo = new HolderAccountHelperResultTO();
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>(); 
			listHolidays();
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "registerPage";
	}

	/**
	 * Go toview certification detail.
	 *
	 * @param rowPhysicalCertificatesExt the row physical certificates ext
	 * @return the string
	 */
	public String goToviewCertificationDetail(
			PhysicalCertificateExt rowPhysicalCertificatesExt) {
		disabledButtons();
		return viewCertificationDetail(rowPhysicalCertificatesExt);
	}

	/**
	 * Enabled buttons.
	 */
	public void enabledButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (userPrivilege.getUserAcctions().isApprove())
			privilegeComponent.setBtnApproveView(true);

		if (userPrivilege.getUserAcctions().isAnnular())
			privilegeComponent.setBtnAnnularView(true);

		if (userPrivilege.getUserAcctions().isApply())
			privilegeComponent.setBtnApply(true);
		
		if (userPrivilege.getUserAcctions().isReject())
			privilegeComponent.setBtnRejectView(true);

		if (userPrivilege.getUserAcctions().isConfirm())
			privilegeComponent.setBtnConfirmView(true);

		if (userPrivilege.getUserAcctions().isAuthorize())
			privilegeComponent.setBtnAuthorize(true);

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Enabled buttons with state.
	 *
	 * @param state the state
	 */
	public void enabledButtonsWithState(Integer state) {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (state.equals(StateType.APPROVED.getCode())) {

			if (userPrivilege.getUserAcctions().isApprove())
				privilegeComponent.setBtnApproveView(true);

		} else if (state.equals(StateType.ANNULLED.getCode())) {

			if (userPrivilege.getUserAcctions().isAnnular())
				privilegeComponent.setBtnAnnularView(true);

		} else if (state.equals(StateType.REJECTED.getCode())) {

			if (userPrivilege.getUserAcctions().isReject())
				privilegeComponent.setBtnRejectView(true);

		} else if (state.equals(StateType.CONFIRMED.getCode())) {

			if (userPrivilege.getUserAcctions().isConfirm())
				privilegeComponent.setBtnConfirmView(true);

		} else if (state.equals(StateType.AUTHORIZED.getCode())) {

			if (userPrivilege.getUserAcctions().isAuthorize())
				privilegeComponent.setBtnAuthorize(true);

		} else if (state.equals(StateType.APPLY.getCode())) { // APLICAR

			if (userPrivilege.getUserAcctions().isApply())
				privilegeComponent.setBtnApply(true);

		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Disabled buttons.
	 */
	public void disabledButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		privilegeComponent.setBtnApproveView(false);
		privilegeComponent.setBtnAnnularView(false);
		privilegeComponent.setBtnRejectView(false);
		privilegeComponent.setBtnApply(false);
		privilegeComponent.setBtnConfirmView(false);

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Valid status holder.
	 */
	public void validStatusHolder() {

		if (!Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber())) {
			holderAccount = new HolderAccount();
			return;
		}

		if (holderAccount.getStateAccount().equals(
				HolderAccountStatusType.ACTIVE.getCode())) {
			return;
		} else {
			// AGREGAR si hay alguna excepcion con edv
			if (holderAccount.getStateAccount().equals(
					HolderAccountStatusType.CLOSED.getCode())) {
			}
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED);
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			alert(headerMessage, bodyMessage);
			holderAccount = new HolderAccount();
		}
	}

	/**
	 * Validate register issuer.
	 */
	public void validateRegisterIssuer() {

		if ( !(Validations.validateIsNotNull(issuerHelp) && Validations.validateIsNotNull(issuerHelp.getIdIssuerPk())) ) {
			return;
		}
		
		issuerPk = issuerHelp.getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerPk);

		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {

			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerPk = StringUtils.EMPTY;
			issuerHelp = new Issuer();
		}
	}
	
	/**
	 * Validate search issuer.
	 */
	public void validateSearchIssuer() {

		if ( !(Validations.validateIsNotNull(issuerHelp) && Validations.validateIsNotNull(issuerHelp.getIdIssuerPk())) ) {
			return;
		}

		issuerSearchPK = issuerHelp.getIdIssuerPk();
		Issuer registerIssuer = certificateDepositeBeanFacade.getIssuerObject(issuerSearchPK);

		if (IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {

			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			issuerSearchPK = StringUtils.EMPTY;
			issuerHelp = new Issuer();
		}
	}

	/**
	 * Security class detail.
	 */
	public void securityClassDetail() {
		try {
			if (selectedInstrument > -1) {
				securityClassList = withdrawalDepositeFacadeBean.securityClassFacade(selectedInstrument,SECURITYCLASS_MASTERPK);
			} else {
				securityClassList = null;
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load security class.
	 */
	public void loadSecurityClass() {
		try {
			instrumentTypeList = withdrawalDepositeFacadeBean.getParameterTableDetailsFacade(INSTRUMENT_TYPE_MASTERKEY);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
/**
 * Load security.
 */
public void loadSecurity(){
		
		try {
			if(Validations.validateIsNotNull(securityHelp.getIdSecurityCodePk())){
				securityHelp = transferAvailableFacade.getSecurity(securityHelp.getIdSecurityCodePk());
				
				if( Validations.validateIsNotNull(securityHelp) &&
						Validations.validateIsNotNull(securityHelp.getIdSecurityCodePk()) ){
					
						if(!securityHelp.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
							confirmAlertAction = "clearSecurity";
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID));
							securityHelp=new Security();
						}
					}else{
						alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_ERROR_FINDING_ISINCODE));
						securityHelp=new Security();
					}
				
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Adds the reteirement deposite list.
	 */
	// para agregar de una lista a otra
	public void addReteirementDepositeList() {

		certificatesNums = new ArrayList<Long>();
		String cert = "";
		
		if (physicalCertificateListAdded == null) {
			physicalCertificateListAdded = new ArrayList<ReteirementDepositeFilterBean>();
		}

		if (lstPhysicalCertificateSelected != null) {
			for (ReteirementDepositeFilterBean obj : lstPhysicalCertificateSelected) {
				physicalCertificateListAdded.add(obj);
				certificatesNums.add( Long.valueOf(obj.getPhysicalCertificate().getCertificateNumber()) );
			}
			cert = certificateNumberMessage(certificatesNums);
			physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(
					physicalCertificateListAdded);
		}
		if (lstPhysicalCertificateSelected != null) {
			for (ReteirementDepositeFilterBean loadData : lstPhysicalCertificateSelected) {
				physicalCertificateList.remove(loadData);
			}
			physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(
					physicalCertificateList);
				summeryDetail();
		}
		
		if (lstPhysicalCertificateSelected != null) {
			lstPhysicalCertificateSelected = null;
		}
		
		//issue 895: limpiando los campos de la seccion Consulta Titulos Depositados
		issuerHelp = new Issuer();
		selectedSecurityClass = -1;
		securityHelp = new Security();
		docNumber = null;
		
		
		Object[] data = new Object[]{cert};
		showMessageOnDialog(PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_ADD_CERTIFICATE_MULTI,data));
		 JSFUtilities.showSimpleValidationDialog();
	}

	/**
	 * Summery detail.
	 */
	private void summeryDetail() {

		int totalCurrencyLocal = 0;
		int totalCurrencyEUR = 0;
		int totalCurrencyUSD = 0;
	    int totalCurrencyUFV=0;
	    int totalCurrencyDMV=0;
	    
		BigDecimal totalNominaldUSD = new BigDecimal(0);
		BigDecimal totalNominalLocal = new BigDecimal(0);
		BigDecimal totalNominaldEUR = new BigDecimal(0);
		BigDecimal totalNominalUFV=new BigDecimal(0);
		BigDecimal totalNominalDMV= new BigDecimal(0);
		

		for (ReteirementDepositeFilterBean reteirBean : physicalCertificateListAdded) {
			// USD currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.USD.getCode())) {
				totalCurrencyUSD++;
				totalNominaldUSD = totalNominaldUSD.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			// DOP currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.PYG.getCode())) {
				totalCurrencyLocal++;
				totalNominalLocal = totalNominalLocal.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			// EUR currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.EU.getCode())) {
				totalCurrencyEUR++;
				totalNominaldEUR = totalNominaldEUR.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			//UFV Currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.UFV.getCode())) {
				totalCurrencyUFV++;
				totalNominalUFV = totalNominalUFV.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
			//DMV Currency type
			if (reteirBean.getPhysicalCertificate().getCurrency().equals(CurrencyType.DMV.getCode())) {
				totalCurrencyDMV++;
				totalNominalDMV = totalNominalDMV.add(reteirBean.getPhysicalCertificate().getNominalValue());
			}
		}

		setSelectedCurrencyLocal(new Integer(totalCurrencyLocal));
		setSelectedCurrencyUSP(new Integer(totalCurrencyUSD));
		setSelectedCurrencyEUR(new Integer(totalCurrencyEUR));
		setSelectedCurrencyUFV(new Integer(totalCurrencyUFV));
		setSelectedCurrencyDMV(new Integer(totalCurrencyDMV));

		setSelectedNominalLocal(totalNominalLocal);
		setSelectedNominalUSP(totalNominaldUSD);
		setSelectedNominalEUR(totalNominaldEUR);
		setSelectedNominalUFV(totalNominalUFV);
		setSelectedNominalDMV(totalNominalDMV);
	}

	/**
	 * Clean search input data.
	 */
	private void cleanSearchInputData() {
		issuerPk = null;
		issuerPk = StringUtils.EMPTY;
	//	issuerHelper.setIssuerDescription(StringUtils.EMPTY);
	//	issuerHelper.setIssuerMnemonic(StringUtils.EMPTY);
		docNumber = null;
		selectedInstrument = null;
		selectedSecurityClass = null;
		certificatesNums = new ArrayList<Long>();
		if (initialDate != null) {
			if (!initialDate.equals(CommonsUtilities.currentDate())) {
				initialDate = CommonsUtilities.currentDate();
			}
		}
		else
			initialDate = CommonsUtilities.currentDate();
		if (finalDate != null) {
			if (!finalDate.equals(CommonsUtilities.currentDate())) {
				finalDate = CommonsUtilities.currentDate();
			}
		}
		else
			finalDate = CommonsUtilities.currentDate();
		
		sourceAccountTo = new HolderAccountHelperResultTO();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		
		
		securityEvents();
	}

	/**
	 * Pop up show data.
	 *
	 * @param filterbean the filterbean
	 */
	public void popUpShowData(ReteirementDepositeFilterBean filterbean) {
		showSelected = filterbean;
		JSFUtilities.executeJavascriptFunction("PF('processData').show();");
	}

	/**
	 * Show elimar data.
	 *
	 * @param certificate the certificate
	 */
	public void showElimarData(ReteirementDepositeFilterBean certificate) {
		certificatesNums=new ArrayList<Long>();
		certificatesNums.add( Long.valueOf(certificate.getPhysicalCertificate().getCertificateNumber()) );
		String certMessages = certificateNumberMessage(certificatesNums);
		certificateDelete = certificate;

		confirmAlertAction = "removeList";
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_DELETE, certMessages);
		String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_DELETE);
		question(headerMessage, bodyMessage);

	}

	/**
	 * Removes the list.
	 */
	public void removeList() {
		String cert = certificateNumberMessage(certificatesNums);
				
		Object[] data = new Object[]{cert};
		showMessageOnDialog(PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
				PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM_DELETE,data));
		 JSFUtilities.showSimpleValidationDialog();
		
		physicalCertificateListAdded.remove(certificateDelete);
		certificateDelete = null;
		physicalCertificateListAddedDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(
				physicalCertificateListAdded);
		summeryDetail();
	}

	/**
	 * Show save dialog.
	 */
	@LoggerAuditWeb
	public void showSaveDialog() {
		confirmAlertAction = "saveData";
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_SAVE_RETIRO);
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER);
		question(headerMessage, bodyMessage);
	}

	/**
	 * Save data.
	 */
	@LoggerAuditWeb
	public void saveData() {
		Long requestNumber = null;
		try {
			ReteirementDataFilterBean retiroData = new ReteirementDataFilterBean();
			retiroData.setAccHolderPk(holderAccount.getIdHolderAccountPk());
			retiroData.setIdParticipanrPk(participantSelected);
			retiroData.setState(StateType.REGISTERED.getCode());
			retiroData.setDeliverDate(deliverReqDate);
			// CUSTODY
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
			custodyOperation.setOperationType(new Long(RequestType.RETIRO.getCode()));
			custodyOperation.setState(StateType.REGISTERED.getCode());
			custodyOperation.setRegistryUser(userInfo.getUserAccountSession().getUserName());
			custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());

			// PHYSICAL CERTIFICATE OPERATION
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(this.holderAccount.getIdHolderAccountPk());
			Participant participant = new Participant();
			participant.setIdParticipantPk(this.participantSelected);

			PhysicalCertificateOperation physicalCertificateOperation = new PhysicalCertificateOperation();
			physicalCertificateOperation.setHolderAccount(holderAccount);
			physicalCertificateOperation.setParticipant(participant);
			//physicalCertificateOperation.setComments(textObservation);
			physicalCertificateOperation.setNamePickupManager(namePickupManager);
			physicalCertificateOperation.setCiPickupManager(ciPickupManager);
			physicalCertificateOperation.setState(StateType.REGISTERED_CERTIFICATE.getCode());
			physicalCertificateOperation.setPhysicalOperationType(new Long(RequestType.RETIRO.getCode()));
			physicalCertificateOperation.setIndApplied(new Integer(0));
			physicalCertificateOperation.setRequestDeliveryDate(retiroData.getDeliverDate());

			List<PhysicalOperationDetail> physicalOperationDetails = new ArrayList<PhysicalOperationDetail>();
			certificatesNums = new ArrayList<Long>();

			for (ReteirementDepositeFilterBean ReteirementcertificateTo : physicalCertificateListAdded) {

				PhysicalCertificate certificate = ReteirementcertificateTo.getPhysicalCertificate();
				// PHYSICAL_CERTIFICATE
				certificatesNums.add( Long.valueOf(certificate.getCertificateNumber()) );

				// PHYSICAL_OPERATION_DETAIL
				PhysicalOperationDetail physicalOperationDetail = new PhysicalOperationDetail();
				physicalOperationDetail.setPhysicalCertificateOperation(physicalCertificateOperation);
				physicalOperationDetail.setPhysicalCertificate(certificate);
				physicalOperationDetail.setIndApplied(0);
				physicalOperationDetail.setState(retiroData.getState());
				physicalOperationDetail.setRegistryDate(CommonsUtilities.currentDateTime());
				physicalOperationDetail.setRegistryUser(userInfo.getUserAccountSession().getUserName());
				if (ReteirementcertificateTo.isIndDematerialized()) {
					physicalOperationDetail.setIndDematerialized(BooleanType.YES.getCode());
				} else {
					physicalOperationDetail.setIndDematerialized(BooleanType.NO.getCode());
				}
				// DETALLES A GRABAR
				physicalOperationDetails.add(physicalOperationDetail);
			}
			physicalCertificateOperation.setPhysicalOperationDetails(physicalOperationDetails);
			
			physicalCertificateOperation = withdrawalDepositeFacadeBean.savePhysicalCertificateOperation(physicalCertificateOperation, 
																										 custodyOperation,physicalOperationDetails);
			requestNumber = physicalCertificateOperation.getCustodyOperation().getOperationNumber();

			String certMessages = certificateNumberMessage(certificatesNums);
			certificatesNums = null;
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.SUCCESS_LIST_SECURITY_NUMBER,new Object[] { certMessages, requestNumber.toString() });
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
			alert(headerMessage, bodyMessage);


			/*** JH NOTIFICACIONES ***/
			  BusinessProcess businessProcess = new BusinessProcess();
			  businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_REGISTER.getCode());
			  if(isCevaldom){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null,null); 
			  }else if(isBcr){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, null); 
			  }else if(isParticipant){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, participantSelected, null); 
			  }
			 
			/*** end JH NOTIFICACIONES ***/

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Dialog exit hide.
	 */
	public void dialogExitHide() {
		showSelected = null;
	}

	/**
	 * Adds the certificate show dialog.
	 */
	public void addCertificateShowDialog() {
		if (lstPhysicalCertificateSelected != null && lstPhysicalCertificateSelected.length > 0) {

			confirmAlertAction = "addReteirementDepositeList";
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_LOAD);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			question(headerMessage, bodyMessage);

		} else {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_RECORD_SELECT_CNF);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_WARNING_TITLE);
			alert(headerMessage, bodyMessage);
		}

	}

	/**
	 * Search securities.
	 */
	public void searchSecurities() {
		try {

			ReteirementDataFilterBean retiroData = new ReteirementDataFilterBean();
			
			if (Validations.validateIsNotNullAndPositive(participantSelected)){
				retiroData.setIdParticipanrPk(participantSelected);
			}
			else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_REQUIRED));
				return;
			}
			if (Validations.validateIsNotNull(sourceHolder)
					&& Validations.validateIsNotNullAndPositive(sourceHolder
							.getIdHolderPk())) {
				retiroData.setIdHolderPk(sourceHolder.getIdHolderPk());
			} else {
				alert(PropertiesUtilities
						.getMessage(PropertiesConstants.HOLDER_REQUIRED));
				return;
			}
			if (Validations.validateIsNotNull(holderAccount) && Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())){
				retiroData.setAccHolderPk(holderAccount.getIdHolderAccountPk());
			}
			else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_REQUIRED));
				return;
			}
			retiroData.setState(StateType.ANNULLED.getCode()); 
			retiroData.setDescDate(initialDate);
			if(Validations.validateIsNotNullAndNotEmpty(finalDate))
				retiroData.setHasDate(CommonsUtilities.addDate(finalDate, 1));
			else
				retiroData.setHasDate(finalDate);
			
			retiroData.setIssuerPKSearch(issuerPk);

			if (Validations.validateIsNotNull(securityHelp) && Validations.validateIsNotNullAndNotEmpty(securityHelp.getIdSecurityCodePk())){
				retiroData.setIdSecurityCode(securityHelp.getIdSecurityCodePk());
			}
			retiroData.setDocumentNumber(docNumber);
			retiroData.setDeliverDate(deliverReqDate);
			retiroData.setInstrumenttype(selectedInstrument);
			retiroData.setSecurityClass(selectedSecurityClass);
			retiroData.setSaveData(physicalCertificateListAdded);
			physicalCertificateList = withdrawalDepositeFacadeBean.searchSecuritiesFacade(retiroData);

			if (physicalCertificateList.size() > 0) {
				physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(physicalCertificateList);
				registersFound = true;
			} else {
				physicalCertificateList = new ArrayList<ReteirementDepositeFilterBean>();
				physicalCertificateDataModel = new GenericDataModel<ReteirementDepositeFilterBean>(physicalCertificateList);
				registersFound = false;
			}

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Gets the holder account and details.
	 *
	 * @param holderAccount the holder account
	 * @return the holder account and details
	 */
	public HolderAccount getHolderAccountAndDetails(HolderAccount holderAccount){
		try {
			holderAccount = transferAvailableFacade.getHolderAccount(holderAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return holderAccount;
	}
	
	/**
	 * Valid source holder account.
	 */
	public void validSourceHolderAccount(){
		
		if(Validations.validateIsNotNull(sourceAccountTo) && Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountNumber()) ){
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(sourceAccountTo.getAccountPk());
			this.holderAccount = holderAccount;
			//holderAccount.setAccountNumber(HolderAccountStatusType.ACTIVE.getCode());
		
			if(!(Validations.validateIsNotNull(this.holderAccount) &&
				 Validations.validateIsNotNullAndPositive(this.holderAccount.getIdHolderAccountPk())) ){
				return;
			}
			holderAccount = getHolderAccountAndDetails(this.holderAccount);
			
			
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
				this.holderAccount = new HolderAccount();
			}
			
			this.holderAccount = holderAccount;
		
		}else{
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_NOT_SELECTED));
		}
	}
	
	/**
	 * Validate register holder account.
	 */
	//seleccionar participante - anterior
	public void validateRegisterHolderAccount() {

		if (!Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber())) {
			holderAccount = new HolderAccount();
			return;
		}

		if (holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {

			if (physicalCertificateListAdded.size() > 0 || physicalCertificateList.size() > 0) {
				HolderAccount tmpHolder = holderAccount;
				executeChangeParticipant();
				holderAccount = tmpHolder;
			}

			return;
		} else {

			if (physicalCertificateListAdded.size() > 0 || physicalCertificateList.size() > 0) {
				HolderAccount tmpHolder = holderAccount;
				executeChangeParticipant();
				holderAccount = tmpHolder;
			}

			// AGREGAR si hay alguna excepcion con cevaldom
			if (holderAccount.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())) {

			}
			
			holderAccount = new HolderAccount();
			String bodyMessage = PropertiesUtilities
					.getMessage(PropertiesConstants.ERROR_CERTIFICATE_ACCOUNT_NOT_ALLOWED);
			String headerMessage = PropertiesUtilities
					.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			alert(headerMessage, bodyMessage);
		}
	}

	/**
	 * Gets the next work day.
	 *
	 * @param actulaDate the actula date
	 * @param length the length
	 * @return the next work day
	 */
	public Date getNextWorkDay(Date actulaDate, int length) {

		Calendar dateInstance = Calendar.getInstance();
		try {
			dateInstance.setTime(actulaDate);

			dateInstance.set(Calendar.HOUR_OF_DAY, 0);
			dateInstance.set(Calendar.MINUTE, 0);
			dateInstance.set(Calendar.SECOND, 0);
			dateInstance.set(Calendar.MILLISECOND, 0);
			Holiday dayOFHoliday = null;

			while (length > 0) {
				dateInstance.add(Calendar.DATE, 1);
				if (dateInstance.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || dateInstance.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					continue;
				} else {

					dayOFHoliday = certificateDepositeBeanFacade.getHolidayListByState(dateInstance.getTime(),
									GeographicLocationStateType.REGISTERED.getCode(), BOLIVIA_GEOGRAPHIC_CODE);
					if (dayOFHoliday != null) {
						continue;
					} else {
						length--;
					}
				}
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return dateInstance.getTime();

	}

	/**
	 * Load parameter reason.
	 */
	public void loadParameterReason() {
		try {
			reasonTypeList = withdrawalDepositeFacadeBean
					.getParameterTableDetailsFacade(CANCEL_TYPE_MASTERKEY);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Load participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipant() throws ServiceException {

		if (Validations.validateIsNotNullAndPositive(participant.getState())) {
			if (participant.getState().equals(ParticipantStateType.BLOCKED.getCode()) || participant.getState().equals(ParticipantStateType.REGISTERED.getCode())) {

				/*participant.setCompleteDescription(participant.getDescription()
						+ " - " + participant.getIdParticipantPk().toString());*/
				lstParticpant.add(participant);

			} else {
				Map<String, Object> filter = new HashMap<String, Object>();
				List<Integer> states = new ArrayList<Integer>();
				states.add(ParticipantStateType.REGISTERED.getCode());
				filter.put("states", states);
				lstParticpant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
				
				//lstParticpant = certificateDepositeBeanFacade.getListParticicpant(ParticipantStateType.REGISTERED.getCode(),ParticipantClassType.AFP.getCode());
			}
		} else {

			Map<String, Object> filter = new HashMap<String, Object>();
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			//states.add(ParticipantStateType.BLOCKED.getCode());
			filter.put("states", states);
			lstParticpant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
			
			/*List<Integer> lstStates = new ArrayList<Integer>();
			lstStates.add(ParticipantStateType.REGISTERED.getCode());
			lstStates.add(ParticipantStateType.BLOCKED.getCode());
			lstParticpant = certificateDepositeBeanFacade.getListParticicpant(lstStates, ParticipantClassType.AFP.getCode());
			*/
		}
	}

	/**
	 * Load certificate deposite estatus.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadCertificateDepositeEstatus() throws ServiceException {
		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setMasterTableFk(MasterTableType.SECURITY_STATES.getCode());
		List<Integer> lista = new ArrayList<>();
		lista.add(PhysicalCertificateStateType.AUTHORIZED.getCode());
		filter.setLstParameterTablePkNotIn(lista);
		certificateDepositeEstatusTypeList = generalParametersFacade.getComboParameterTable(filter);	
		
		//certificateDepositeEstatusTypeList = withdrawalDepositeFacadeBean.getParameterTableDetailsFacade(CERTIFICATE_STATUS_MASTERKEY);
	}

	/**
	 * Find participant selected.
	 *
	 * @param idParticipantPk the id participant pk
	 * @return the participant
	 */
	public Participant findParticipantSelected(Long idParticipantPk) {
		
		if (Validations.validateIsNotNull(participantSelected)) {
			for (Participant curParticipant : lstParticpant) {
				if (participantSelected.equals(curParticipant.getIdParticipantPk())) {
					return curParticipant;
				}
			}
		}
		else if(Validations.validateIsNotNull(idParticipantPk)){
			for (Participant curParticipant : lstParticpant) {
				if (idParticipantPk.equals(curParticipant.getIdParticipantPk())) {
					return curParticipant;
				}
			}
		}
		return null;
	}
	
	/**
	 * Action certificate dialog.
	 *
	 * @param physicalCertificateExt the physical certificate ext
	 */
	public void actionCertificateDialog(ReteirementDepositeFilterBean physicalCertificateExt){
		
	//	PhysicalCertificate certificate = certificateDepositeBeanFacade.getPhysicalCertificateObject(physicalCertificateExt.getPhysicalCertificate().getIdPhysicalCertificatePk());
		
		dialogPhysicalCertificate = new PhysicalCertificateExt();
		
	//	dialogPhysicalCertificate.setPhysicalCertificate(certificate);
		dialogPhysicalCertificate.setPhysicalCertificate(physicalCertificateExt.getPhysicalCertificate());
		if(dialogPhysicalCertificate.getPhysicalCertificate().getIssuer() == null || 
		Validations.validateIsNull(dialogPhysicalCertificate.getPhysicalCertificate().getIssuer().getIdIssuerPk()) ){
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk(physicalCertificateExt.getIssuerFk());
			issuer.setDescription(physicalCertificateExt.getIssuerDesc());
			issuer.setMnemonic(physicalCertificateExt.getIssuerNemonic());
			dialogPhysicalCertificate.getPhysicalCertificate().setIssuer(issuer);			
		}
		
		//Logica de cargar datos
		showDetailCertificate();
	}
	
	/**
	 * Show detail certificate.
	 */
	public void showDetailCertificate() {
		JSFUtilities.executeJavascriptFunction("PF('idDetalleValorW').show();");
	}

	/**
	 * Certificate question.
	 */
	public void certificateQuestion() {

		String certs = certificateNumberMessage(certificatesNums);
		String headerMessage = "";
		String bodyMessage = "";

		if (confirmAlertAction.equals("certificateRejection_anull")) {
			headerMessage = confirmHeaderMessage(StateType.ANNULLED.getCode());
			bodyMessage = confirmbodyMessage(StateType.ANNULLED.getCode(),certs);
			confirmAlertAction = "retirementCertificateAnuller";
		} else {// certificateRejection_reject
			headerMessage = confirmHeaderMessage(StateType.REJECTED.getCode());
			bodyMessage = confirmbodyMessage(StateType.REJECTED.getCode(),certs);
			confirmAlertAction = "retirementCertificateRejected";
		}

		question(headerMessage, bodyMessage);
	}

	/**
	 *  ALERT's *.
	 *
	 * @return the string
	 */

	@LoggerAuditWeb
	public String confirmAlertActions() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (tmpAction.equals("retirementCertificateAuthorization")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateAuthorization();
		} else if (tmpAction.equals("retirementCertificateConfirm")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateConfirm();
		} else if (tmpAction.equals("retirementCertificateApply")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateApply();
		} else if (tmpAction.equals("retirementCertificateAnuller")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateAnuller();
		} else if (tmpAction.equals("retirementCertificateRejected")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateRejected();
		}else if (tmpAction.equals("retirementCertificateApprove")) {
			confirmAlertAction = "postActionReturnToSearch";
			retirementCertificateApprove();
		} else if (tmpAction.equals("addReteirementDepositeList")) {
			confirmAlertAction = "";
			addReteirementDepositeList();
		} else if (tmpAction.equals("removeList")) {
			confirmAlertAction = "";
			removeList();
		} else if (tmpAction.equals("saveData")) {
			confirmAlertAction = "postActionReturnToSearch";
			saveData();
			initialDate = CommonsUtilities.currentDate();
			finalDate = CommonsUtilities.currentDate();
		} else if (tmpAction.equals("cleanRegistry")) {
			confirmAlertAction = "";
			cleanRegistry();
		} else if (tmpAction.equals("executeBackToSearch")) {
			confirmAlertAction = "";
			return executeBackToSearch();
		} else if (tmpAction.equals("executeChangeParticipant")) {
			confirmAlertAction = "executeChangeParticipant";
			executeChangeParticipant();
		}
		return "";
	}

	/**
	 * Acept alert actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String aceptAlertActions() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (Validations.validateIsNotNull(tmpAction)) {

			if (tmpAction.equals("postActionReturnToSearch")) {
				searchRetirementPhysicalCertificates();
				cleanSearchInputData();
				enabledButtons();
				return "search";
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	/**
	 * Acept alert actions new.
	 *
	 * @return the string
	 */
	public String aceptAlertActionsNew() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (Validations.validateIsNotNull(tmpAction)) {

			if (tmpAction.equals("postActionReturnToSearch")) {
				//cleanRegistry();
				//initialDate = CommonsUtilities.currentDate();
				//finalDate = CommonsUtilities.currentDate();
				searchRetirementPhysicalCertificates();
				return "search";
			}

		} else {
			return "";
		}
		return "";
	}

	/**
	 * Cancel alert anull or reject.
	 */
	public void cancelAlertAnullOrReject() {
		reasonSelected = -1;
		rejectOther = "";
	}

	/**
	 * Question alert no.
	 *
	 * @return the string
	 */
	public String questionAlertNo() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (tmpAction.equals("executeChangeParticipant")) {
			confirmAlertAction = "";
			participantSelected = participantSelectedTmp;
		}

		return "";
	}

	/**
	 * Alert.
	 *
	 * @param bodyMessage the body message
	 */
	public void alert(String bodyMessage) {
		String headerMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext
						.getCurrentInstance().getViewRoot().getLocale(),GeneralConstants.LBL_HEADER_ALERT);

		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	/**
	 * Alert.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */
	public void alert(String headerMessage, String bodyMessage) {

		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	/**
	 * Question.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */
	public void question(String headerMessage, String bodyMessage) {

		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}

	/**
	 * Validate state type annulled or rejected.
	 *
	 * @param stateType the state type
	 * @return true, if successful
	 */
	public boolean ValidateStateTypeAnnulledOrRejected(Integer stateType){
    	boolean viewDetailCertificate = Boolean.FALSE;
    	if(stateType.equals(StateType.ANNULLED.getCode()) || stateType.equals(StateType.REJECTED.getCode())){
    		viewDetailCertificate = Boolean.TRUE;
    	}
    	return viewDetailCertificate;
    }
	
	/**
	 * Alert anulled or reject.
	 *
	 * @param state the state
	 */
	public void alertAnulledOrReject(Integer state) {
		String headerMessage = "";

		if (state.equals(StateType.ANNULLED.getCode())) {
			confirmAlertAction = "certificateRejection_anull";
			headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_ANNULMENT);
		} else if (state.equals(StateType.REJECTED.getCode())) {
			confirmAlertAction = "certificateRejection_reject";
			headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_HEADER_ALERT_REJECTED);
		}

		reasonSelected = -1;
		rejectOther = "";
		showMessageOnDialog(headerMessage, "");
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').show();");
	}


	/**
	 *  GET AN SET *.
	 *
	 * @return the lst particpant
	 */

	public List<Participant> getLstParticpant() {
		return lstParticpant;
	}

	/**
	 * Sets the lst particpant.
	 *
	 * @param lstParticpant the new lst particpant
	 */
	public void setLstParticpant(List<Participant> lstParticpant) {
		this.lstParticpant = lstParticpant;
	}

	/**
	 * Gets the participant selected.
	 *
	 * @return the participant selected
	 */
	public Long getParticipantSelected() {
		return participantSelected;
	}

	/**
	 * Sets the participant selected.
	 *
	 * @param participantSelected the new participant selected
	 */
	public void setParticipantSelected(Long participantSelected) {
		this.participantSelected = participantSelected;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the issuer search pk.
	 *
	 * @return the issuer search pk
	 */
	public String getIssuerSearchPK() {
		return issuerSearchPK;
	}

	/**
	 * Sets the issuer search pk.
	 *
	 * @param issuerSearchPK the new issuer search pk
	 */
	public void setIssuerSearchPK(String issuerSearchPK) {
		this.issuerSearchPK = issuerSearchPK;
	}

	/**
	 * Gets the doc number.
	 *
	 * @return the doc number
	 */
	public Long getDocNumber() {
		return docNumber;
	}

	/**
	 * Sets the doc number.
	 *
	 * @param docNumber the new doc number
	 */
	public void setDocNumber(Long docNumber) {
		this.docNumber = docNumber;
	}

	/**
	 * Gets the state selected.
	 *
	 * @return the state selected
	 */
	public Integer getStateSelected() {
		return stateSelected;
	}

	/**
	 * Sets the state selected.
	 *
	 * @param stateSelected the new state selected
	 */
	public void setStateSelected(Integer stateSelected) {
		this.stateSelected = stateSelected;
	}

	/**
	 * Gets the certificate deposite estatus type list.
	 *
	 * @return the certificate deposite estatus type list
	 */
	public List<ParameterTable> getCertificateDepositeEstatusTypeList() {
		return certificateDepositeEstatusTypeList;
	}

	/**
	 * Sets the certificate deposite estatus type list.
	 *
	 * @param certificateDepositeEstatusTypeList the new certificate deposite estatus type list
	 */
	public void setCertificateDepositeEstatusTypeList(
			List<ParameterTable> certificateDepositeEstatusTypeList) {
		this.certificateDepositeEstatusTypeList = certificateDepositeEstatusTypeList;
	}

	/**
	 * Gets the selected req number.
	 *
	 * @return the selected req number
	 */
	public Long getSelectedReqNumber() {
		return selectedReqNumber;
	}

	/**
	 * Sets the selected req number.
	 *
	 * @param selectedReqNumber the new selected req number
	 */
	public void setSelectedReqNumber(Long selectedReqNumber) {
		this.selectedReqNumber = selectedReqNumber;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the max date allow.
	 *
	 * @return the max date allow
	 */
	public Date getMaxDateAllow() {
		return maxDateAllow;
	}

	/**
	 * Sets the max date allow.
	 *
	 * @param maxDateAllow the new max date allow
	 */
	public void setMaxDateAllow(Date maxDateAllow) {
		this.maxDateAllow = maxDateAllow;
	}

	/**
	 * Gets the participant login.
	 *
	 * @return the participant login
	 */
	public Long getParticipantLogin() {
		return participantLogin;
	}

	/**
	 * Sets the participant login.
	 *
	 * @param participantLogin the new participant login
	 */
	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	/**
	 * Gets the checks if is participant.
	 *
	 * @return the checks if is participant
	 */
	public boolean getIsParticipant() {
		return isParticipant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param isParticipant the new participant
	 */
	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Gets the checks if is cevaldom.
	 *
	 * @return the checks if is cevaldom
	 */
	public boolean getIsCevaldom() {
		return isCevaldom;
	}

	/**
	 * Sets the cevaldom.
	 *
	 * @param isCevaldom the new cevaldom
	 */
	public void setCevaldom(boolean isCevaldom) {
		this.isCevaldom = isCevaldom;
	}

	/**
	 * Gets the checks if is bcr.
	 *
	 * @return the checks if is bcr
	 */
	public boolean getIsBcr() {
		return isBcr;
	}

	/**
	 * Sets the bcr.
	 *
	 * @param isBcr the new bcr
	 */
	public void setBcr(boolean isBcr) {
		this.isBcr = isBcr;
	}

	/**
	 * Checks if is flag register.
	 *
	 * @return true, if is flag register
	 */
	public boolean isFlagRegister() {
		return flagRegister;
	}

	/**
	 * Sets the flag register.
	 *
	 * @param flagRegister the new flag register
	 */
	public void setFlagRegister(boolean flagRegister) {
		this.flagRegister = flagRegister;
	}

	/**
	 * Checks if is flag search.
	 *
	 * @return true, if is flag search
	 */
	public boolean isFlagSearch() {
		return flagSearch;
	}

	/**
	 * Sets the flag search.
	 *
	 * @param flagSearch the new flag search
	 */
	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	/**
	 * Checks if is registers found.
	 *
	 * @return true, if is registers found
	 */
	public boolean isRegistersFound() {
		return registersFound;
	}

	/**
	 * Sets the registers found.
	 *
	 * @param registersFound the new registers found
	 */
	public void setRegistersFound(boolean registersFound) {
		this.registersFound = registersFound;
	}

	/**
	 * Gets the confirm alert action.
	 *
	 * @return the confirm alert action
	 */
	public String getConfirmAlertAction() {
		return confirmAlertAction;
	}

	/**
	 * Sets the confirm alert action.
	 *
	 * @param confirmAlertAction the new confirm alert action
	 */
	public void setConfirmAlertAction(String confirmAlertAction) {
		this.confirmAlertAction = confirmAlertAction;
	}

	/**
	 * Gets the reason selected.
	 *
	 * @return the reason selected
	 */
	public Integer getReasonSelected() {
		return reasonSelected;
	}

	/**
	 * Sets the reason selected.
	 *
	 * @param reasonSelected the new reason selected
	 */
	public void setReasonSelected(Integer reasonSelected) {
		this.reasonSelected = reasonSelected;
	}

	/**
	 * Gets the reject other.
	 *
	 * @return the reject other
	 */
	public String getRejectOther() {
		return rejectOther;
	}

	/**
	 * Sets the reject other.
	 *
	 * @param rejectOther the new reject other
	 */
	public void setRejectOther(String rejectOther) {
		this.rejectOther = rejectOther;
	}

	/**
	 * Gets the other reject motive id.
	 *
	 * @return the other reject motive id
	 */
	public Integer getOtherRejectMotiveId() {
		return otherRejectMotiveId;
	}

	/**
	 * Sets the other reject motive id.
	 *
	 * @param otherRejectMotiveId the new other reject motive id
	 */
	public void setOtherRejectMotiveId(Integer otherRejectMotiveId) {
		this.otherRejectMotiveId = otherRejectMotiveId;
	}

	/**
	 * Gets the certificate deposite data model.
	 *
	 * @return the certificate deposite data model
	 */
	public GenericDataModel<PhysicalCertificateExt> getCertificateDepositeDataModel() {
		return certificateDepositeDataModel;
	}

	/**
	 * Sets the certificate deposite data model.
	 *
	 * @param certificateDepositeDataModel the new certificate deposite data model
	 */
	public void setCertificateDepositeDataModel(
			GenericDataModel<PhysicalCertificateExt> certificateDepositeDataModel) {
		this.certificateDepositeDataModel = certificateDepositeDataModel;
	}

	/**
	 * Gets the certificate deposite list.
	 *
	 * @return the certificate deposite list
	 */
	public List<PhysicalCertificateExt> getCertificateDepositeList() {
		return certificateDepositeList;
	}

	/**
	 * Sets the certificate deposite list.
	 *
	 * @param certificateDepositeList the new certificate deposite list
	 */
	public void setCertificateDepositeList(
			List<PhysicalCertificateExt> certificateDepositeList) {
		this.certificateDepositeList = certificateDepositeList;
	}

	/**
	 * Gets the certificate deposite selected.
	 *
	 * @return the certificate deposite selected
	 */
	public PhysicalCertificateExt[] getCertificateDepositeSelected() {
		return certificateDepositeSelected;
	}

	/**
	 * Sets the certificate deposite selected.
	 *
	 * @param certificateDepositeSelected the new certificate deposite selected
	 */
	public void setCertificateDepositeSelected(
			PhysicalCertificateExt[] certificateDepositeSelected) {
		this.certificateDepositeSelected = certificateDepositeSelected;
	}

	/**
	 * Gets the reason type list.
	 *
	 * @return the reason type list
	 */
	public List<ParameterTable> getReasonTypeList() {
		return reasonTypeList;
	}

	/**
	 * Sets the reason type list.
	 *
	 * @param reasonTypeList the new reason type list
	 */
	public void setReasonTypeList(List<ParameterTable> reasonTypeList) {
		this.reasonTypeList = reasonTypeList;
	}

	/**
	 * Gets the selected reteirement deposit detail.
	 *
	 * @return the selected reteirement deposit detail
	 */
	public PhysicalCertificateExt getSelectedReteirementDepositDetail() {
		return selectedReteirementDepositDetail;
	}

	/**
	 * Sets the selected reteirement deposit detail.
	 *
	 * @param selectedReteirementDepositDetail the new selected reteirement deposit detail
	 */
	public void setSelectedReteirementDepositDetail(
			PhysicalCertificateExt selectedReteirementDepositDetail) {
		this.selectedReteirementDepositDetail = selectedReteirementDepositDetail;
	}

	/**
	 * Gets the holder account detail.
	 *
	 * @return the holder account detail
	 */
	public HolderAccount getHolderAccountDetail() {
		return holderAccountDetail;
	}

	/**
	 * Sets the holder account detail.
	 *
	 * @param holderAccountDetail the new holder account detail
	 */
	public void setHolderAccountDetail(HolderAccount holderAccountDetail) {
		this.holderAccountDetail = holderAccountDetail;
	}

	/**
	 * Gets the issuer helper.
	 *
	 * @return the issuer helper
	 */
	public IssuerHelperBean getIssuerHelper() {
		return issuerHelper;
	}

	/**
	 * Sets the issuer helper.
	 *
	 * @param issuerHelper the new issuer helper
	 */
	public void setIssuerHelper(IssuerHelperBean issuerHelper) {
		this.issuerHelper = issuerHelper;
	}

	/**
	 * Gets the certificates nums.
	 *
	 * @return the certificates nums
	 */
	public List<Long> getCertificatesNums() {
		return certificatesNums;
	}

	/**
	 * Sets the certificates nums.
	 *
	 * @param certificatesNums the new certificates nums
	 */
	public void setCertificatesNums(List<Long> certificatesNums) {
		this.certificatesNums = certificatesNums;
	}


	/**
	 * Gets the selected currency usp.
	 *
	 * @return the selected currency usp
	 */
	public Integer getSelectedCurrencyUSP() {
		return selectedCurrencyUSP;
	}

	/**
	 * Sets the selected currency usp.
	 *
	 * @param selectedCurrencyUSP the new selected currency usp
	 */
	public void setSelectedCurrencyUSP(Integer selectedCurrencyUSP) {
		this.selectedCurrencyUSP = selectedCurrencyUSP;
	}

	/**
	 * Gets the selected currency eur.
	 *
	 * @return the selected currency eur
	 */
	public Integer getSelectedCurrencyEUR() {
		return selectedCurrencyEUR;
	}

	/**
	 * Sets the selected currency eur.
	 *
	 * @param selectedCurrencyEUR the new selected currency eur
	 */
	public void setSelectedCurrencyEUR(Integer selectedCurrencyEUR) {
		this.selectedCurrencyEUR = selectedCurrencyEUR;
	}

	/**
	 * Gets the selected nominal usp.
	 *
	 * @return the selected nominal usp
	 */
	public BigDecimal getSelectedNominalUSP() {
		return selectedNominalUSP;
	}

	/**
	 * Sets the selected nominal usp.
	 *
	 * @param selectedNominalUSP the new selected nominal usp
	 */
	public void setSelectedNominalUSP(BigDecimal selectedNominalUSP) {
		this.selectedNominalUSP = selectedNominalUSP;
	}

	/**
	 * Gets the selected nominal eur.
	 *
	 * @return the selected nominal eur
	 */
	public BigDecimal getSelectedNominalEUR() {
		return selectedNominalEUR;
	}

	/**
	 * Sets the selected nominal eur.
	 *
	 * @param selectedNominalEUR the new selected nominal eur
	 */
	public void setSelectedNominalEUR(BigDecimal selectedNominalEUR) {
		this.selectedNominalEUR = selectedNominalEUR;
	}

	/**
	 * Gets the selected instrument.
	 *
	 * @return the selected instrument
	 */
	public Integer getSelectedInstrument() {
		return selectedInstrument;
	}

	/**
	 * Sets the selected instrument.
	 *
	 * @param selectedInstrument the new selected instrument
	 */
	public void setSelectedInstrument(Integer selectedInstrument) {
		this.selectedInstrument = selectedInstrument;
	}

	/**
	 * Gets the selected security class.
	 *
	 * @return the selected security class
	 */
	public Integer getSelectedSecurityClass() {
		return selectedSecurityClass;
	}

	/**
	 * Sets the selected security class.
	 *
	 * @param selectedSecurityClass the new selected security class
	 */
	public void setSelectedSecurityClass(Integer selectedSecurityClass) {
		this.selectedSecurityClass = selectedSecurityClass;
	}

	/**
	 * Gets the holder acc search.
	 *
	 * @return the holder acc search
	 */
	public HolderAccount getHolderAccSearch() {
		return holderAccSearch;
	}

	/**
	 * Sets the holder acc search.
	 *
	 * @param holderAccSearch the new holder acc search
	 */
	public void setHolderAccSearch(HolderAccount holderAccSearch) {
		this.holderAccSearch = holderAccSearch;
	}

	/**
	 * Gets the issuer pk.
	 *
	 * @return the issuer pk
	 */
	public String getIssuerPk() {
		return issuerPk;
	}

	/**
	 * Sets the issuer pk.
	 *
	 * @param issuerPk the new issuer pk
	 */
	public void setIssuerPk(String issuerPk) {
		this.issuerPk = issuerPk;
	}

	/**
	 * Gets the deliver req date.
	 *
	 * @return the deliver req date
	 */
	public Date getDeliverReqDate() {
		return deliverReqDate;
	}

	/**
	 * Sets the deliver req date.
	 *
	 * @param deliverReqDate the new deliver req date
	 */
	public void setDeliverReqDate(Date deliverReqDate) {
		this.deliverReqDate = deliverReqDate;
	}

	/**
	 * Gets the physical certificate list added.
	 *
	 * @return the physical certificate list added
	 */
	public List<ReteirementDepositeFilterBean> getPhysicalCertificateListAdded() {
		return physicalCertificateListAdded;
	}

	/**
	 * Sets the physical certificate list added.
	 *
	 * @param physicalCertificateListAdded the new physical certificate list added
	 */
	public void setPhysicalCertificateListAdded(
			List<ReteirementDepositeFilterBean> physicalCertificateListAdded) {
		this.physicalCertificateListAdded = physicalCertificateListAdded;
	}

	/**
	 * Gets the phy certificate remove selected.
	 *
	 * @return the phy certificate remove selected
	 */
	public ReteirementDepositeFilterBean[] getPhyCertificateRemoveSelected() {
		return phyCertificateRemoveSelected;
	}

	/**
	 * Sets the phy certificate remove selected.
	 *
	 * @param phyCertificateRemoveSelected the new phy certificate remove selected
	 */
	public void setPhyCertificateRemoveSelected(
			ReteirementDepositeFilterBean[] phyCertificateRemoveSelected) {
		this.phyCertificateRemoveSelected = phyCertificateRemoveSelected;
	}

	public String getNamePickupManager() {
		return namePickupManager;
	}

	public void setNamePickupManager(String namePickupManager) {
		this.namePickupManager = namePickupManager;
	}

	public String getCiPickupManager() {
		return ciPickupManager;
	}

	public void setCiPickupManager(String ciPickupManager) {
		this.ciPickupManager = ciPickupManager;
	}

	/**
	 * Gets the physical certificate data model.
	 *
	 * @return the physical certificate data model
	 */
	public GenericDataModel<ReteirementDepositeFilterBean> getPhysicalCertificateDataModel() {
		return physicalCertificateDataModel;
	}

	/**
	 * Sets the physical certificate data model.
	 *
	 * @param physicalCertificateDataModel the new physical certificate data model
	 */
	public void setPhysicalCertificateDataModel(
			GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateDataModel) {
		this.physicalCertificateDataModel = physicalCertificateDataModel;
	}

	/**
	 * Gets the lst physical certificate selected.
	 *
	 * @return the lst physical certificate selected
	 */
	public ReteirementDepositeFilterBean[] getLstPhysicalCertificateSelected() {
		return lstPhysicalCertificateSelected;
	}

	/**
	 * Sets the lst physical certificate selected.
	 *
	 * @param lstPhysicalCertificateSelected the new lst physical certificate selected
	 */
	public void setLstPhysicalCertificateSelected(
			ReteirementDepositeFilterBean[] lstPhysicalCertificateSelected) {
		this.lstPhysicalCertificateSelected = lstPhysicalCertificateSelected;
	}

	/**
	 * Gets the security class list.
	 *
	 * @return the security class list
	 */
	public List<ParameterTable> getSecurityClassList() {
		return securityClassList;
	}

	/**
	 * Sets the security class list.
	 *
	 * @param securityClassList the new security class list
	 */
	public void setSecurityClassList(List<ParameterTable> securityClassList) {
		this.securityClassList = securityClassList;
	}

	/**
	 * Gets the instrument type list.
	 *
	 * @return the instrument type list
	 */
	public List<ParameterTable> getInstrumentTypeList() {
		return instrumentTypeList;
	}

	/**
	 * Sets the instrument type list.
	 *
	 * @param instrumentTypeList the new instrument type list
	 */
	public void setInstrumentTypeList(List<ParameterTable> instrumentTypeList) {
		this.instrumentTypeList = instrumentTypeList;
	}

	/**
	 * Gets the show selected.
	 *
	 * @return the show selected
	 */
	public ReteirementDepositeFilterBean getShowSelected() {
		return showSelected;
	}

	/**
	 * Sets the show selected.
	 *
	 * @param showSelected the new show selected
	 */
	public void setShowSelected(ReteirementDepositeFilterBean showSelected) {
		this.showSelected = showSelected;
	}

	/**
	 * Gets the max next work day.
	 *
	 * @return the max next work day
	 */
	public Date getMaxNextWorkDay() {
		return maxNextWorkDay;
	}

	/**
	 * Sets the max next work day.
	 *
	 * @param maxNextWorkDay the new max next work day
	 */
	public void setMaxNextWorkDay(Date maxNextWorkDay) {
		this.maxNextWorkDay = maxNextWorkDay;
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * Gets the physical certificate list.
	 *
	 * @return the physical certificate list
	 */
	public List<ReteirementDepositeFilterBean> getPhysicalCertificateList() {
		return physicalCertificateList;
	}

	/**
	 * Sets the physical certificate list.
	 *
	 * @param physicalCertificateList the new physical certificate list
	 */
	public void setPhysicalCertificateList(
			List<ReteirementDepositeFilterBean> physicalCertificateList) {
		this.physicalCertificateList = physicalCertificateList;
	}

	/**
	 * Gets the physical certificate list added data model.
	 *
	 * @return the physical certificate list added data model
	 */
	public GenericDataModel<ReteirementDepositeFilterBean> getPhysicalCertificateListAddedDataModel() {
		return physicalCertificateListAddedDataModel;
	}

	/**
	 * Sets the physical certificate list added data model.
	 *
	 * @param physicalCertificateListAddedDataModel the new physical certificate list added data model
	 */
	public void setPhysicalCertificateListAddedDataModel(
			GenericDataModel<ReteirementDepositeFilterBean> physicalCertificateListAddedDataModel) {
		this.physicalCertificateListAddedDataModel = physicalCertificateListAddedDataModel;
	}

	/**
	 * Gets the max date retirement.
	 *
	 * @return the max date retirement
	 */
	public Date getMaxDateRetirement() {
		return maxDateRetirement;
	}

	/**
	 * Sets the max date retirement.
	 *
	 * @param maxDateRetirement the new max date retirement
	 */
	public void setMaxDateRetirement(Date maxDateRetirement) {
		this.maxDateRetirement = maxDateRetirement;
	}

	/**
	 * Checks if is rendered instrument type.
	 *
	 * @return true, if is rendered instrument type
	 */
	public boolean isRenderedInstrumentType() {
		return renderedInstrumentType;
	}

	/**
	 * Sets the rendered instrument type.
	 *
	 * @param renderedInstrumentType the new rendered instrument type
	 */
	public void setRenderedInstrumentType(boolean renderedInstrumentType) {
		this.renderedInstrumentType = renderedInstrumentType;
	}

	/**
	 * Gets the source holder.
	 *
	 * @return the source holder
	 */
	public Holder getSourceHolder() {
		return sourceHolder;
	}

	/**
	 * Sets the source holder.
	 *
	 * @param sourceHolder the new source holder
	 */
	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}

	/**
	 * Gets the source account to.
	 *
	 * @return the source account to
	 */
	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	/**
	 * Sets the source account to.
	 *
	 * @param sourceAccountTo the new source account to
	 */
	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}

	/**
	 * Gets the lst source account to.
	 *
	 * @return the lst source account to
	 */
	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	/**
	 * Sets the lst source account to.
	 *
	 * @param lstSourceAccountTo the new lst source account to
	 */
	public void setLstSourceAccountTo(
			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	/**
	 * Gets the issuer help.
	 *
	 * @return the issuer help
	 */
	public Issuer getIssuerHelp() {
		return issuerHelp;
	}

	/**
	 * Sets the issuer help.
	 *
	 * @param issuerHelp the new issuer help
	 */
	public void setIssuerHelp(Issuer issuerHelp) {
		this.issuerHelp = issuerHelp;
	}

	/**
	 * Gets the dialog physical certificate.
	 *
	 * @return the dialog physical certificate
	 */
	public PhysicalCertificateExt getDialogPhysicalCertificate() {
		return dialogPhysicalCertificate;
	}

	/**
	 * Sets the dialog physical certificate.
	 *
	 * @param dialogPhysicalCertificate the new dialog physical certificate
	 */
	public void setDialogPhysicalCertificate(
			PhysicalCertificateExt dialogPhysicalCertificate) {
		this.dialogPhysicalCertificate = dialogPhysicalCertificate;
	}

	/**
	 * Gets the text observation.
	 *
	 * @return the text observation
	 */
	public String getTextObservation() {
		return textObservation;
	}

	/**
	 * Sets the text observation.
	 *
	 * @param textObservation the new text observation
	 */
	public void setTextObservation(String textObservation) {
		this.textObservation = textObservation;
	}

	/**
	 * Gets the security help.
	 *
	 * @return the security help
	 */
	public Security getSecurityHelp() {
		return securityHelp;
	}

	/**
	 * Sets the security help.
	 *
	 * @param securityHelp the new security help
	 */
	public void setSecurityHelp(Security securityHelp) {
		this.securityHelp = securityHelp;
	}

	/**
	 * Gets the selected currency local.
	 *
	 * @return the selected currency local
	 */
	public Integer getSelectedCurrencyLocal() {
		return selectedCurrencyLocal;
	}

	/**
	 * Sets the selected currency local.
	 *
	 * @param selectedCurrencyLocal the new selected currency local
	 */
	public void setSelectedCurrencyLocal(Integer selectedCurrencyLocal) {
		this.selectedCurrencyLocal = selectedCurrencyLocal;
	}

	/**
	 * Gets the selected nominal local.
	 *
	 * @return the selected nominal local
	 */
	public BigDecimal getSelectedNominalLocal() {
		return selectedNominalLocal;
	}

	/**
	 * Sets the selected nominal local.
	 *
	 * @param selectedNominalLocal the new selected nominal local
	 */
	public void setSelectedNominalLocal(BigDecimal selectedNominalLocal) {
		this.selectedNominalLocal = selectedNominalLocal;
	}

	/**
	 * Gets the selected currency ufv.
	 *
	 * @return the selected currency ufv
	 */
	public Integer getSelectedCurrencyUFV() {
		return selectedCurrencyUFV;
	}

	/**
	 * Sets the selected currency ufv.
	 *
	 * @param selectedCurrencyUFV the new selected currency ufv
	 */
	public void setSelectedCurrencyUFV(Integer selectedCurrencyUFV) {
		this.selectedCurrencyUFV = selectedCurrencyUFV;
	}

	/**
	 * Gets the selected currency dmv.
	 *
	 * @return the selected currency dmv
	 */
	public Integer getSelectedCurrencyDMV() {
		return selectedCurrencyDMV;
	}

	/**
	 * Sets the selected currency dmv.
	 *
	 * @param selectedCurrencyDMV the new selected currency dmv
	 */
	public void setSelectedCurrencyDMV(Integer selectedCurrencyDMV) {
		this.selectedCurrencyDMV = selectedCurrencyDMV;
	}

	/**
	 * Gets the selected nominal ufv.
	 *
	 * @return the selected nominal ufv
	 */
	public BigDecimal getSelectedNominalUFV() {
		return selectedNominalUFV;
	}

	/**
	 * Sets the selected nominal ufv.
	 *
	 * @param selectedNominalUFV the new selected nominal ufv
	 */
	public void setSelectedNominalUFV(BigDecimal selectedNominalUFV) {
		this.selectedNominalUFV = selectedNominalUFV;
	}

	/**
	 * Gets the selected nominal dmv.
	 *
	 * @return the selected nominal dmv
	 */
	public BigDecimal getSelectedNominalDMV() {
		return selectedNominalDMV;
	}

	/**
	 * Sets the selected nominal dmv.
	 *
	 * @param selectedNominalDMV the new selected nominal dmv
	 */
	public void setSelectedNominalDMV(BigDecimal selectedNominalDMV) {
		this.selectedNominalDMV = selectedNominalDMV;
	}

	/**
	 * Gets the money type list.
	 *
	 * @return the money type list
	 */
	public List<ParameterTable> getMoneyTypeList() {
		return moneyTypeList;
	}
	
	/**
	 * Sets the money type list.
	 *
	 * @param moneyTypeList the new money type list
	 */
	public void setMoneyTypeList(List<ParameterTable> moneyTypeList) {
		this.moneyTypeList = moneyTypeList;
	}
	
	/**
	 * Gets the fecha maxima.
	 *
	 * @return the fecha maxima
	 */
	public Date getFechaMaxima() {
		return fechaMaxima;
	}
	
	/**
	 * Sets the fecha maxima.
	 *
	 * @param fechaMaxima the new fecha maxima
	 */
	public void setFechaMaxima(Date fechaMaxima) {
		this.fechaMaxima = fechaMaxima;
	}

	/**
	 * @return the listHolderAccountForView
	 */
	public List<HolderAccountHelperResultTO> getListHolderAccountForView() {
		return listHolderAccountForView;
	}

	/**
	 * @param listHolderAccountForView the listHolderAccountForView to set
	 */
	public void setListHolderAccountForView(
			List<HolderAccountHelperResultTO> listHolderAccountForView) {
		this.listHolderAccountForView = listHolderAccountForView;
	}
	
}