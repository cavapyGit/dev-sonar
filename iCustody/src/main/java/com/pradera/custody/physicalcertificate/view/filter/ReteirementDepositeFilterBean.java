package com.pradera.custody.physicalcertificate.view.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.custody.physical.PhysicalCertificate;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ReteirementDepositeFilterBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class ReteirementDepositeFilterBean implements Serializable {

	/** added default serail version uid. */
	private static final long serialVersionUID = 1L;
	
	/** The physical certificate. */
	private PhysicalCertificate physicalCertificate;
	
	/** The deposite date. */
	private Date depositeDate;
	
	/** The issuer fk. */
	private String issuerFk;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The id physical certificate pk. */
	private Long idPhysicalCertificatePk;
	
	/** The currency. */
	private Integer currency;
	
	/** The nominal value. */
	private BigDecimal nominalValue;
	
	/** The issue date. */
	private Date issueDate;
	
	/** The expiration date. */
	private Date expirationDate;
	
	/** The request number. */
	private Long requestNumber;
	
	/** The state. */
	private Integer state;
	
	/** The id. */
	private Integer id;
	
	/** The idphysicaloperdetail. */
	private Long idphysicaloperdetail;
	
	/** The idphysicalbalancedetail. */
	private Long idphysicalbalancedetail;
	
	/** The str state. */
	private String strState;
	
	/** The instrument type. */
	private Integer instrumentType;
	
	/** The payment periodicity. */
	private Integer paymentPeriodicity;
	
	/** The interest rate. */
	private BigDecimal interestRate;
	
	/** The situation. */
	private Integer situation;
	
	/** The str situation. */
	private String strSituation;
	
	/** The request delivery date. */
	private Date requestDeliveryDate;
	
	/** The application date. */
	private Date applicationDate;
	
	/** The application user. */
	private String applicationUser;
	
	/** The approval date. */
	private Date approvalDate;
	
	/** The approval user. */
	private String approvalUser;
	
	/** The authorization date. */
	private Date authorizationDate;
	
	/** The authorization user. */
	private String authorizationUser;
	
	/** The confirm date. */
	private Date confirmDate;
	
	/** The confirm user. */
	private String confirmUser;
	
	/** The registry date. */
	private Date registryDate;
	
	/** The registry user. */
	private String registryUser;
	
	/** The reject date. */
	private Date rejectDate;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject motive desc. */
	private String rejectMotiveDesc;
	
	/** The reject motive other. */
	private String rejectMotiveOther;
	
	/** The reject user. */
	private String rejectUser;
	
	/** The phy cer oper state. */
	private Integer phyCerOperState;
	
	/** The str phy cer oper state. */
	private String strPhyCerOperState;
	
	/** The str currency. */
	private String strCurrency;
	
	/** The in apllied. */
	private Integer inApllied;
	
	/** The in apllied flag. */
	private boolean inAplliedFlag;
	
	/** The in apllied pco. */
	private Integer inAplliedPCO;
	
	/** The certificate number. */
	private Long certificateNumber;
	
	/** The idj. */
	private int idj;
	
	/** The str security class. */
	private String strSecurityClass;
	
	/** The st instrument type. */
	private String stInstrumentType;
	
	/** The str payment periodicity. */
	private String strPaymentPeriodicity;
	
	/** The participant description. */
	private String participantDescription;
	
	/** The participant pk. */
	private String participantPK;
	
	/** The str deposite date. */
	private Date strDepositeDate;
	
	/** The issuer desc. */
	private String  issuerDesc;
	
	/** The issuer nemonic. */
	private String issuerNemonic;
	
	/** The id holder acc fk. */
	private Long idHolderAccFk;
	
	/** The ind dematerialized. */
	private boolean indDematerialized;
	
	/** The mixed issuance. */
	private boolean mixedIssuance;
	
	
	/**
	 * Gets the deposite date.
	 *
	 * @return the depositeDate
	 */
	public Date getDepositeDate() {
		return depositeDate;
	}

	/**
	 * Sets the deposite date.
	 *
	 * @param depositeDate            the depositeDate to set
	 */
	public void setDepositeDate(Date depositeDate) {
		this.depositeDate = depositeDate;
	}

	/**
	 * Gets the issuer fk.
	 *
	 * @return the issuerFk
	 */
	public String getIssuerFk() {
		return issuerFk;
	}

	/**
	 * Sets the issuer fk.
	 *
	 * @param issuerFk            the issuerFk to set
	 */
	public void setIssuerFk(String issuerFk) {
		this.issuerFk = issuerFk;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the securityClass
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass            the securityClass to set
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the id physical certificate pk.
	 *
	 * @return the idPhysicalCertificatePk
	 */
	public Long getIdPhysicalCertificatePk() {
		return idPhysicalCertificatePk;
	}

	/**
	 * Sets the id physical certificate pk.
	 *
	 * @param idPhysicalCertificatePk            the idPhysicalCertificatePk to set
	 */
	public void setIdPhysicalCertificatePk(Long idPhysicalCertificatePk) {
		this.idPhysicalCertificatePk = idPhysicalCertificatePk;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency            the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * Gets the nominal value.
	 *
	 * @return the nominalValue
	 */
	public BigDecimal getNominalValue() {
		return nominalValue;
	}

	/**
	 * Sets the nominal value.
	 *
	 * @param nominalValue            the nominalValue to set
	 */
	public void setNominalValue(BigDecimal nominalValue) {
		this.nominalValue = nominalValue;
	}

	
	/**
	 * Gets the request number.
	 *
	 * @return the requestNumber
	 */
	public Long getRequestNumber() {
		return requestNumber;
	}

	/**
	 * Sets the request number.
	 *
	 * @param requestNumber            the requestNumber to set
	 */
	public void setRequestNumber(Long requestNumber) {
		this.requestNumber = requestNumber;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state            the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the idphysicaloperdetail.
	 *
	 * @return the idphysicaloperdetail
	 */
	public Long getIdphysicaloperdetail() {
		return idphysicaloperdetail;
	}

	/**
	 * Sets the idphysicaloperdetail.
	 *
	 * @param idphysicaloperdetail            the idphysicaloperdetail to set
	 */
	public void setIdphysicaloperdetail(Long idphysicaloperdetail) {
		this.idphysicaloperdetail = idphysicaloperdetail;
	}

	/**
	 * Gets the idphysicalbalancedetail.
	 *
	 * @return the idphysicalbalancedetail
	 */
	public Long getIdphysicalbalancedetail() {
		return idphysicalbalancedetail;
	}

	/**
	 * Sets the idphysicalbalancedetail.
	 *
	 * @param idphysicalbalancedetail            the idphysicalbalancedetail to set
	 */
	public void setIdphysicalbalancedetail(Long idphysicalbalancedetail) {
		this.idphysicalbalancedetail = idphysicalbalancedetail;
	}

	/**
	 * Gets the str state.
	 *
	 * @return the strState
	 */
	public String getStrState() {
		return strState;
	}

	/**
	 * Sets the str state.
	 *
	 * @param strState            the strState to set
	 */
	public void setStrState(String strState) {
		this.strState = strState;
	}

	/**
	 * Gets the instrument type.
	 *
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}

	/**
	 * Sets the instrument type.
	 *
	 * @param instrumentType            the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Gets the payment periodicity.
	 *
	 * @return the paymentPeriodicity
	 */
	public Integer getPaymentPeriodicity() {
		return paymentPeriodicity;
	}

	/**
	 * Sets the payment periodicity.
	 *
	 * @param paymentPeriodicity            the paymentPeriodicity to set
	 */
	public void setPaymentPeriodicity(Integer paymentPeriodicity) {
		this.paymentPeriodicity = paymentPeriodicity;
	}

	/**
	 * Gets the interest rate.
	 *
	 * @return the interestRate
	 */
	public BigDecimal getInterestRate() {
		return interestRate;
	}

	/**
	 * Sets the interest rate.
	 *
	 * @param interestRate            the interestRate to set
	 */
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}

	/**
	 * Gets the situation.
	 *
	 * @return the situation
	 */
	public Integer getSituation() {
		return situation;
	}

	/**
	 * Sets the situation.
	 *
	 * @param situation            the situation to set
	 */
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	

	/**
	 * Gets the str situation.
	 *
	 * @return the strSituation
	 */
	public String getStrSituation() {
		return strSituation;
	}

	/**
	 * Sets the str situation.
	 *
	 * @param strSituation the strSituation to set
	 */
	public void setStrSituation(String strSituation) {
		this.strSituation = strSituation;
	}

	


	/**
	 * Gets the application user.
	 *
	 * @return the applicationUser
	 */
	public String getApplicationUser() {
		return applicationUser;
	}

	/**
	 * Sets the application user.
	 *
	 * @param applicationUser the applicationUser to set
	 */
	public void setApplicationUser(String applicationUser) {
		this.applicationUser = applicationUser;
	}

	

	/**
	 * Gets the approval user.
	 *
	 * @return the approvalUser
	 */
	public String getApprovalUser() {
		return approvalUser;
	}

	/**
	 * Sets the approval user.
	 *
	 * @param approvalUser the approvalUser to set
	 */
	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	

	/**
	 * Gets the authorization user.
	 *
	 * @return the authorizationUser
	 */
	public String getAuthorizationUser() {
		return authorizationUser;
	}

	/**
	 * Sets the authorization user.
	 *
	 * @param authorizationUser the authorizationUser to set
	 */
	public void setAuthorizationUser(String authorizationUser) {
		this.authorizationUser = authorizationUser;
	}

	

	
	/**
	 * Gets the confirm user.
	 *
	 * @return the confirmUser
	 */
	public String getConfirmUser() {
		return confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the confirmUser to set
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	

	/**
	 * Gets the registry user.
	 *
	 * @return the registryUser
	 */
	public String getRegistryUser() {
		return registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the registryUser to set
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}


	/**
	 * Gets the reject motive.
	 *
	 * @return the rejectMotive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the rejectMotive to set
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}
	

	/**
	 * Gets the reject motive desc.
	 *
	 * @return the rejectMotiveDesc
	 */
	public String getRejectMotiveDesc() {
		return rejectMotiveDesc;
	}

	/**
	 * Sets the reject motive desc.
	 *
	 * @param rejectMotiveDesc the rejectMotiveDesc to set
	 */
	public void setRejectMotiveDesc(String rejectMotiveDesc) {
		this.rejectMotiveDesc = rejectMotiveDesc;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the rejectMotiveOther
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the rejectMotiveOther to set
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the rejectUser
	 */
	public String getRejectUser() {
		return rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the rejectUser to set
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the phy cer oper state.
	 *
	 * @return the phyCerOperState
	 */
	public Integer getPhyCerOperState() {
		return phyCerOperState;
	}

	/**
	 * Sets the phy cer oper state.
	 *
	 * @param phyCerOperState the phyCerOperState to set
	 */
	public void setPhyCerOperState(Integer phyCerOperState) {
		this.phyCerOperState = phyCerOperState;
	}

	/**
	 * Gets the str phy cer oper state.
	 *
	 * @return the strPhyCerOperState
	 */
	public String getStrPhyCerOperState() {
		return strPhyCerOperState;
	}

	/**
	 * Sets the str phy cer oper state.
	 *
	 * @param strPhyCerOperState the strPhyCerOperState to set
	 */
	public void setStrPhyCerOperState(String strPhyCerOperState) {
		this.strPhyCerOperState = strPhyCerOperState;
	}

	/**
	 * Gets the str currency.
	 *
	 * @return the strCurrency
	 */
	public String getStrCurrency() {
		return strCurrency;
	}

	/**
	 * Sets the str currency.
	 *
	 * @param strCurrency the strCurrency to set
	 */
	public void setStrCurrency(String strCurrency) {
		this.strCurrency = strCurrency;
	}

	/**
	 * Gets the in apllied.
	 *
	 * @return the inApllied
	 */
	public Integer getInApllied() {
		return inApllied;
	}

	/**
	 * Sets the in apllied.
	 *
	 * @param inApllied the inApllied to set
	 */
	public void setInApllied(Integer inApllied) {
		this.inApllied = inApllied;
	}

	/**
	 * Checks if is in apllied flag.
	 *
	 * @return the inAplliedFlag
	 */
	public boolean isInAplliedFlag() {
		return inAplliedFlag;
	}

	/**
	 * Sets the in apllied flag.
	 *
	 * @param inAplliedFlag the inAplliedFlag to set
	 */
	public void setInAplliedFlag(boolean inAplliedFlag) {
		this.inAplliedFlag = inAplliedFlag;
	}

	/**
	 * Gets the in apllied pco.
	 *
	 * @return the inAplliedPCO
	 */
	public Integer getInAplliedPCO() {
		return inAplliedPCO;
	}

	/**
	 * Sets the in apllied pco.
	 *
	 * @param inAplliedPCO the inAplliedPCO to set
	 */
	public void setInAplliedPCO(Integer inAplliedPCO) {
		this.inAplliedPCO = inAplliedPCO;
	}

	/**
	 * Gets the idj.
	 *
	 * @return the idj
	 */
	public int getIdj() {
		return idj;
	}

	/**
	 * Sets the idj.
	 *
	 * @param idj the idj to set
	 */
	public void setIdj(int idj) {
		this.idj = idj;
	}

	/**
	 * Gets the certificate number.
	 *
	 * @return the certificateNumber
	 */
	public Long getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the certificateNumber to set
	 */
	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * Gets the str security class.
	 *
	 * @return the strSecurityClass
	 */
	public String getStrSecurityClass() {
		return strSecurityClass;
	}

	/**
	 * Sets the str security class.
	 *
	 * @param strSecurityClass the strSecurityClass to set
	 */
	public void setStrSecurityClass(String strSecurityClass) {
		this.strSecurityClass = strSecurityClass;
	}

	/**
	 * Gets the st instrument type.
	 *
	 * @return the stInstrumentType
	 */
	public String getStInstrumentType() {
		return stInstrumentType;
	}

	/**
	 * Sets the st instrument type.
	 *
	 * @param stInstrumentType the stInstrumentType to set
	 */
	public void setStInstrumentType(String stInstrumentType) {
		this.stInstrumentType = stInstrumentType;
	}

	/**
	 * Gets the str payment periodicity.
	 *
	 * @return the strPaymentPeriodicity
	 */
	public String getStrPaymentPeriodicity() {
		return strPaymentPeriodicity;
	}

	/**
	 * Sets the str payment periodicity.
	 *
	 * @param strPaymentPeriodicity the strPaymentPeriodicity to set
	 */
	public void setStrPaymentPeriodicity(String strPaymentPeriodicity) {
		this.strPaymentPeriodicity = strPaymentPeriodicity;
	}
	
	/**
	 * Gets the issue date.
	 *
	 * @return the issueDate
	 */
	public Date getIssueDate() {
		return issueDate;
	}

	/**
	 * Sets the issue date.
	 *
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the request delivery date.
	 *
	 * @return the requestDeliveryDate
	 */
	public Date getRequestDeliveryDate() {
		return requestDeliveryDate;
	}

	/**
	 * Sets the request delivery date.
	 *
	 * @param requestDeliveryDate the requestDeliveryDate to set
	 */
	public void setRequestDeliveryDate(Date requestDeliveryDate) {
		this.requestDeliveryDate = requestDeliveryDate;
	}

	/**
	 * Gets the application date.
	 *
	 * @return the applicationDate
	 */
	public Date getApplicationDate() {
		return applicationDate;
	}

	/**
	 * Sets the application date.
	 *
	 * @param applicationDate the applicationDate to set
	 */
	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * Gets the approval date.
	 *
	 * @return the approvalDate
	 */
	public Date getApprovalDate() {
		return approvalDate;
	}

	/**
	 * Sets the approval date.
	 *
	 * @param approvalDate the approvalDate to set
	 */
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	/**
	 * Gets the authorization date.
	 *
	 * @return the authorizationDate
	 */
	public Date getAuthorizationDate() {
		return authorizationDate;
	}

	/**
	 * Sets the authorization date.
	 *
	 * @param authorizationDate the authorizationDate to set
	 */
	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirmDate
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the rejectDate
	 */
	public Date getRejectDate() {
		return rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the rejectDate to set
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the str deposite date.
	 *
	 * @return the strDepositeDate
	 */
	public Date getStrDepositeDate() {
		return strDepositeDate;
	}

	/**
	 * Sets the str deposite date.
	 *
	 * @param strDepositeDate the strDepositeDate to set
	 */
	public void setStrDepositeDate(Date strDepositeDate) {
		this.strDepositeDate = strDepositeDate;
	}

	/**
	 * Gets the issuer desc.
	 *
	 * @return the issuerDesc
	 */
	public String getIssuerDesc() {
		return issuerDesc;
	}

	/**
	 * Sets the issuer desc.
	 *
	 * @param issuerDesc the issuerDesc to set
	 */
	public void setIssuerDesc(String issuerDesc) {
		this.issuerDesc = issuerDesc;
	}

	/**
	 * Gets the id holder acc fk.
	 *
	 * @return the idHolderAccFk
	 */
	public Long getIdHolderAccFk() {
		return idHolderAccFk;
	}

	/**
	 * Sets the id holder acc fk.
	 *
	 * @param idHolderAccFk the idHolderAccFk to set
	 */
	public void setIdHolderAccFk(Long idHolderAccFk) {
		this.idHolderAccFk = idHolderAccFk;
	}

	/**
	 * Gets the issuer nemonic.
	 *
	 * @return the issuer nemonic
	 */
	public String getIssuerNemonic() {
		return issuerNemonic;
	}

	/**
	 * Sets the issuer nemonic.
	 *
	 * @param issuerNemonic the new issuer nemonic
	 */
	public void setIssuerNemonic(String issuerNemonic) {
		this.issuerNemonic = issuerNemonic;
	}

	/**
	 * Gets the participant description.
	 *
	 * @return the participant description
	 */
	public String getParticipantDescription() {
		return participantDescription;
	}

	/**
	 * Sets the participant description.
	 *
	 * @param participantDescription the new participant description
	 */
	public void setParticipantDescription(String participantDescription) {
		this.participantDescription = participantDescription;
	}

	/**
	 * Gets the participant pk.
	 *
	 * @return the participant pk
	 */
	public String getParticipantPK() {
		return participantPK;
	}

	/**
	 * Sets the participant pk.
	 *
	 * @param participantPK the new participant pk
	 */
	public void setParticipantPK(String participantPK) {
		this.participantPK = participantPK;
	}

	/**
	 * Gets the physical certificate.
	 *
	 * @return the physical certificate
	 */
	public PhysicalCertificate getPhysicalCertificate() {
		return physicalCertificate;
	}

	/**
	 * Sets the physical certificate.
	 *
	 * @param physicalCertificate the new physical certificate
	 */
	public void setPhysicalCertificate(PhysicalCertificate physicalCertificate) {
		this.physicalCertificate = physicalCertificate;
	}

	/**
	 * Checks if is ind dematerialized.
	 *
	 * @return true, if is ind dematerialized
	 */
	public boolean isIndDematerialized() {
		return indDematerialized;
	}

	/**
	 * Sets the ind dematerialized.
	 *
	 * @param indDematerialized the new ind dematerialized
	 */
	public void setIndDematerialized(boolean indDematerialized) {
		this.indDematerialized = indDematerialized;
	}

	/**
	 * Checks if is mixed issuance.
	 *
	 * @return true, if is mixed issuance
	 */
	public boolean isMixedIssuance() {
		return mixedIssuance;
	}

	/**
	 * Sets the mixed issuance.
	 *
	 * @param mixedIssuance the new mixed issuance
	 */
	public void setMixedIssuance(boolean mixedIssuance) {
		this.mixedIssuance = mixedIssuance;
	}

	
}