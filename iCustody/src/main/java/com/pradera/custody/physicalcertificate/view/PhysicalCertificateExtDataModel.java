package com.pradera.custody.physicalcertificate.view;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;
import com.pradera.custody.physicalcertificate.view.filter.PhysicalCertificateExt;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class PhysicalCertificateExtDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class PhysicalCertificateExtDataModel extends ListDataModel<PhysicalCertificateExt> implements SelectableDataModel<PhysicalCertificateExt>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5656602291295214159L;

	/**
	 * Instantiates a new physical certificate ext data model.
	 */
	public PhysicalCertificateExtDataModel() {
		
	}
	
	/**
	 * Instantiates a new physical certificate ext data model.
	 *
	 * @param lstPhysicalCertificate the lst physical certificate
	 */
	public PhysicalCertificateExtDataModel(List<PhysicalCertificateExt> lstPhysicalCertificate) {
		super(lstPhysicalCertificate);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public PhysicalCertificateExt getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<PhysicalCertificateExt> ListPhysicalCertificate = (List<PhysicalCertificateExt>)getWrappedData();
		for(PhysicalCertificateExt physicalCertificate : ListPhysicalCertificate){
			if(physicalCertificate.getPhysicalCertificate().getCertificateNumber().toString().equals(rowKey))
				return physicalCertificate;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(PhysicalCertificateExt physicalCertificate) {//Return Object
		// TODO Auto-generated method stub
		return physicalCertificate.getPhysicalCertificate().getCertificateNumber();
	}
}
