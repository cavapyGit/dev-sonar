package com.pradera.custody.core.service;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.CorrelativeServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.type.CatType;
import com.pradera.model.generalparameter.Correlative;

@Singleton
public class CustodyComponentSingleton  extends CrudDaoServiceBean {
	
	/** The Constant logger. */
//	private static final  Logger logger = LoggerFactory.getLogger(CustodyComponentSingleton.class);
	
	@Inject
	private PraderaLogger log;
	
	@EJB
	CorrelativeServiceBean correlativeServiceBean;

	/**
	 * Metodo que devuelve el correlativo de un determinado tipo de CAT
	 * @param correlativeFilter
	 * @param catType Tipo de CAT:  CAT o CATB
	 * @return
	 * @throws ServiceException
	 */
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Correlative correlativeServiceBean(Correlative correlativeFilter, CatType catType) throws ServiceException{
		Correlative correlative =correlativeServiceBean.getCorrelativeServiceBean(correlativeFilter);
		
		StringBuilder sql = null;
		
		if(catType.getCode().equals(CatType.CAT.getCode())){
			sql = new StringBuilder(" SELECT SQ_CORRELATIVECAT.NEXTVAL FROM DUAL ");
		}
		
		if(catType.getCode().equals(CatType.CATB.getCode())){
			sql = new StringBuilder(" SELECT SQ_CORRELATIVECATB.NEXTVAL FROM DUAL ");
		}
		
		Query query = em.createNativeQuery(sql.toString());
		BigDecimal correlativo = (BigDecimal) query.getSingleResult();
		em.flush();
		correlative.setCorrelativeNumber(Integer.valueOf(correlativo.intValue()));
		return correlative;
		
//		Correlative correlative =correlativeServiceBean.getCorrelativeServiceBean(correlativeFilter);
//		correlative.setCorrelativeNumber(correlative.getCorrelativeNumber()+1);
//		return correlative;
	}
	

}
