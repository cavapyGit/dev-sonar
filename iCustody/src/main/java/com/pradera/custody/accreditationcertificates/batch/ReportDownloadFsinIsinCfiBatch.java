package com.pradera.custody.accreditationcertificates.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.accreditationcertificates.facade.ReportDownloadFsinIsinCfiFacade;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.type.ProcessLoggerStateType;

@BatchProcess(name="ReportDownloadFsinIsinCfiBatch")
@RequestScoped
public class ReportDownloadFsinIsinCfiBatch implements Serializable,JobExecution {
	private static final long serialVersionUID = 1L;
	
	@EJB
	ReportDownloadFsinIsinCfiFacade reportDownloadFsinIsinCfiFacade;
	
	@Inject
	private PraderaLogger log;

	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info("************************************* BATCH ENVIO FsinIsinCfi A LA WEB  ******************************************************");
		try {
			reportDownloadFsinIsinCfiFacade.searchAndProcess();			
			System.out.println("************************************* FIN BATCH ENVIO FsinIsinCfi A LA WEB ******************************************************");
				
		} catch (Exception e) {
			processLogger.setErrorDetail("Error al ejecutar el script revisar la conexion dblink");
			processLogger.setLoggerState(ProcessLoggerStateType.ERROR.getCode());
		}
		
	}

	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
