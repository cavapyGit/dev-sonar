package com.pradera.custody.accreditationcertificates.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AccreditationOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/04/2014
 */
public class DateTypeTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The code. */
	private Long code;
	
	/** The description. */
	private String description;
	
	/**
	 * Instantiates a new date type to.
	 */
	public DateTypeTO(){}
	
	/**
	 * Instantiates a new date type to.
	 *
	 * @param code the code
	 * @param description the description
	 */
	public DateTypeTO(Long code, String description){
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Long getCode() {
		return code;
	}
	
	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Long code) {
		this.code = code;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
