package com.pradera.custody.accreditationcertificates.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.channel.opening.to.Constatnt;
import com.pradera.integration.exception.ServiceException;

@Stateless
public class ReportDownloadFsinIsinCfiServiceBean extends CrudDaoServiceBean {
	
	@Inject
	PraderaLogger log;

	@Resource(lookup = "java:jboss/datasources/iDepositaryDS")
	javax.sql.DataSource dataSourceBBV;
	
	@SuppressWarnings({ "resource" })
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendReportDownloadFsinIsinCfiToWeb() throws ServiceException, SQLException {
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		StringBuilder deleteReportDownloadFsinIsinCfi = new StringBuilder();
		deleteReportDownloadFsinIsinCfi.append(" DELETE from CAEDV.CFI_FSIN_WEB@"+searchConnection());

		StringBuilder insertReportDownloadFsinIsinCfi = new StringBuilder();
		insertReportDownloadFsinIsinCfi.append(" INSERT INTO CAEDV.CFI_FSIN_WEB@"+searchConnection()+" ");
		insertReportDownloadFsinIsinCfi.append(" select iss.mnemonic as issuer_Mnemonic,  ");
		insertReportDownloadFsinIsinCfi.append(" iss.description,  ");
		insertReportDownloadFsinIsinCfi.append(" se.instrument_type,  ");
		insertReportDownloadFsinIsinCfi.append(" (select text1 from parameter_table where parameter_table_pk =  se.SECURITY_CLASS) as INST_MNEMONIC,  ");
		insertReportDownloadFsinIsinCfi.append(" (select description from parameter_table where parameter_table_pk =  se.SECURITY_CLASS) as desc_inst,  ");
		insertReportDownloadFsinIsinCfi.append(" se.id_security_code_pk,  ");				
		insertReportDownloadFsinIsinCfi.append(" se.ID_ISIN_CODE,  ");
		insertReportDownloadFsinIsinCfi.append(" se.cfi_code,  ");
		insertReportDownloadFsinIsinCfi.append(" se.FISN_CODE,  ");
		insertReportDownloadFsinIsinCfi.append(" sysdate,  ");		
		insertReportDownloadFsinIsinCfi.append(" 123 ");
		insertReportDownloadFsinIsinCfi.append(" from idepositary.security se  ");
		insertReportDownloadFsinIsinCfi.append(" inner join issuer iss on se.id_issuer_fk = iss.id_issuer_pk  ");		
		insertReportDownloadFsinIsinCfi.append(" where 1 = 1 ");
		insertReportDownloadFsinIsinCfi.append(" and se.ID_ISIN_CODE is not null ");		
		insertReportDownloadFsinIsinCfi.append(" and se.CFI_CODE is not null  ");
		insertReportDownloadFsinIsinCfi.append(" and se.FISN_CODE is not null  ");
		insertReportDownloadFsinIsinCfi.append(" and state_security = 131 AND (se.IND_AUTHORIZED IS NULL OR se.IND_AUTHORIZED != 0) ");
		
		connection = dataSourceBBV.getConnection();
		preparedStatement = connection.prepareStatement(deleteReportDownloadFsinIsinCfi.toString());
		preparedStatement.execute();
		
		preparedStatement = connection.prepareStatement(insertReportDownloadFsinIsinCfi.toString());
		preparedStatement.execute();		

		if (preparedStatement != null) {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}
		
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				log.error(e.getMessage());
			}
		}		
				
	}
	
	/**
	 * Recupera la conexion dblink
	 * @return
	 * @throws ServiceException
	 */
	public String searchConnection() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append(" select pt.text1 from ParameterTable pt where pt.parameterTablePk="+Constatnt.TABLE_PARAMETER_DBLINK);		
		Query query = em.createQuery(sbQuery.toString());
		
		return (query.getSingleResult().toString()); 
	}

}
