package com.pradera.custody.accreditationcertificates.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.management.RuntimeErrorException;
import org.apache.commons.lang3.StringUtils;
import com.pradera.commons.httpevent.type.NotificationType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.ClientRestService;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserFilterTO;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.accreditationcertificates.facade.AccreditationCertificatesServiceFacade;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class AutomaticUnblockAccreditationBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/04/2014
 */
@BatchProcess(name="AutomaticUnblockAccreditationBatch")
@RequestScoped
public class AutomaticUnblockAccreditationBatch implements JobExecution , Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The logger. */
	@Inject
	private PraderaLogger logger;
	
	/** The accreditation certificates service bean. */
	@EJB
	private AccreditationCertificatesServiceBean accreditationCertificatesServiceBean;
	
	/** The accreditation facade bean. */
	@Inject
	private AccreditationCertificatesServiceFacade accreditationfacadeBean;	
	
	/** The client rest service. */
	@Inject 
	ClientRestService clientRestService;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;

	@Override	
	public void startJob(ProcessLogger processLogger) {
		
		//Inicia el prceso de CATS
		logger.info("Inicio ::::::: AutomaticUnblockAccreditationBatch ");
		try {
			
			//Ejecutamos el vencimiento de CATS, aqui puede mandar excepcion
			autoUnblockAccreditations();
			
		} catch (Exception e) {
			
			String messageNotification = null;
			
			//Si existe algun error enviamos el correo con el detalle del error
			logger.error("Error :::::::::::: AutomaticUnblockAccreditationBatch  ");
			
			//imprime en pantalla el detalle del error
			e.printStackTrace();			

			//Extraemos el mensaje de error
			ServiceException se = (ServiceException) e;
			messageNotification = PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage(), 
					se.getParams(), new Locale("es"));

			//Si es saldo negativo cambiamos el mensaje
			if(messageNotification.equals("Error saldo negativo")){
				messageNotification = "SALDO NEGATIVO: " + se.getConcatenatedParams();
			}

			//Enviamos el mensaje por MAIL
			UserFilterTO  userFilter = new UserFilterTO();
			userFilter.setInstitutionType(InstitutionType.DEPOSITARY.getCode());
			List<UserAccountSession> lstUserAccount= clientRestService.getUsersInformation(userFilter);						
			notificationServiceFacade.sendNotificationManual(processLogger.getIdUserAccount(), 
					processLogger.getBusinessProcess(), lstUserAccount,
					GeneralConstants.NOTIFICATION_DEFAULT_HEADER, messageNotification, NotificationType.EMAIL);

			//El batch se registra con error y con el siguiente mensaje
			
			if(Validations.validateIsNotNullAndNotEmpty(se.getConcatenatedParams())){
				throw new RuntimeErrorException(null, "SALDO NEGATIVO: "+ StringUtils.join(se.getConcatenatedParams(),",") +" Verifique.");
			}else{
				throw new RuntimeErrorException(null, "SALDO NEGATIVO: "+ StringUtils.join(se.getParams(),",") +" Verifique.");
			}
			
					}
		logger.debug("AutomaticUnblockAccreditationBatch: Finish the process Automatic Unblock Accreditation ...");
	}
	
	/**
	 * Auto unblock accreditations.
	 *
	 * @throws ServiceException the service exception
	 */
	public void autoUnblockAccreditations() throws ServiceException{
		Date currentDate = CommonsUtilities.currentDate();
		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
		accreditationOperationTO.setExpirationDate(currentDate);
		List<AccreditationOperation> accreditationOperationLst = this.accreditationCertificatesServiceBean.searchAccreditationOperationByUnblock(accreditationOperationTO);		
		for(AccreditationOperation tmpAccreditationOperation : accreditationOperationLst){
			accreditationfacadeBean.unBlockAccreditationProcess(tmpAccreditationOperation, Boolean.TRUE); 
		}
	}		

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
