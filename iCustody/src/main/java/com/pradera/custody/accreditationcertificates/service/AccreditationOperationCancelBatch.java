package com.pradera.custody.accreditationcertificates.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationRejectMotiveType;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * The Class AccreditationOperationCancelBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/04/2014
 */
@BatchProcess(name="AccreditationOperationCancelBatch")
@RequestScoped
public class AccreditationOperationCancelBatch implements JobExecution {
	
	/** The logger. */
	@Inject
	private static PraderaLogger logger;
	
	/** The accreditation certificates service bean. */
	@EJB
	private AccreditationCertificatesServiceBean accreditationCertificatesServiceBean;
	
	/** The accounts component service. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;

	
	/** The holiday query service bean. */
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The parameters facade. */
	@Inject
	private GeneralParametersFacade parametersFacade;	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		
		try {			
			Integer masterPk = MasterTableType.MAX_DAYS_AVAILABLES_ACCREDITATION_CERTIFICATION.getCode();
			Integer days = parametersFacade.getMaxDayForRequestsToCancell(masterPk); 

			if(Validations.validateIsNotNullAndPositive(days)){
				cancelAccreditations(days);
			}
			
		} catch (ServiceException e) {
			e.printStackTrace();
			logger.error("cancelAccreditations");
		}
		
	}
	
	/**
	 * Cancel accreditations.
	 *
	 * @param days the days
	 * @throws ServiceException the service exception
	 */
	public void cancelAccreditations(Integer days) throws ServiceException{
		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.REGISTRED.getCode());
		Date finalDate = this.holidayQueryServiceBean.getCalculateDate(CommonsUtilities.currentDate(), days.intValue(), 0,null);
		accreditationOperationTO.setRegisterDate(finalDate);
		List<AccreditationOperation> accreditationOperationLst = this.accreditationCertificatesServiceBean.searchAccreditationOperation(accreditationOperationTO);
		
		for(AccreditationOperation tmpAccreditationOperation : accreditationOperationLst){			
			
			/*** EXECUTING ACCOUNTS COMPONENT ***/
			this.executeAccountsComponent(tmpAccreditationOperation.getIdAccreditationOperationPk(), 
										  BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK.getCode(), 
										  ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode());
			
			/*** CHANGING TO NEW STATUS ***/
			AccreditationOperationTO accreditationOperationTOtmp = new AccreditationOperationTO();
			accreditationOperationTOtmp.setIdAccreditationOperationPk(tmpAccreditationOperation.getIdAccreditationOperationPk());
			accreditationOperationTOtmp.setAccreditationState(AccreditationOperationStateType.REJECTED.getCode());	
			accreditationOperationTOtmp.setIdRejectMotiveFk(AccreditationOperationRejectMotiveType.EXCEEDED_TIME_ALLOWED.getCode());
			
			CustodyOperation custodyOperation = this.accreditationCertificatesServiceBean.find(CustodyOperation.class, accreditationOperationTOtmp.getIdAccreditationOperationPk());
			custodyOperation.setRejectMotive(AccreditationOperationRejectMotiveType.EXCEEDED_TIME_ALLOWED.getCode());
			custodyOperation.setRejectDate(CommonsUtilities.currentDate());
			custodyOperation.setRejectUser("ADMIN");
			
			this.accreditationCertificatesServiceBean.update(custodyOperation);
			/*** UPDATING ACCREDITATIONS STATUS ***/
			this.accreditationCertificatesServiceBean.updateAccreditationOperation(accreditationOperationTOtmp);
		}
	}
	
	/**
	 * Execute accounts component.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @param businessProcessType the business process type
	 * @param parameterOperationType the parameter operation type
	 * @throws ServiceException the service exception
	 */
	public void executeAccountsComponent(Long accreditationOperationId, Long businessProcessType, Long parameterOperationType) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = this.getHolderAccountBalanceTOs(accreditationOperationId);
		
		accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
																							   businessProcessType, 
																							   	 accountBalanceTOs,
																							parameterOperationType));
	}
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(Long accreditationOperationId) throws ServiceException{
		//accreditationDetail list Object
		List<AccreditationDetail> accreditationDetailList = this.accreditationCertificatesServiceBean.searchAccreditationDetailByAccreditationOperation(accreditationOperationId);
		//HolderAccountBalanceTO list
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		for(int i=0;i<accreditationDetailList.size();++i){
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(accreditationDetailList.get(i).getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(accreditationDetailList.get(i).getHolderAccount().getParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(accreditationDetailList.get(i).getSecurity().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(accreditationDetailList.get(i).getTotalBalance());
			//Adding holderAccountBalanceTO to List
			accountBalanceTOs.add(holderAccountBalanceTO);
		}
		return accountBalanceTOs;
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	public AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType){
		// ComponentTO Object
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		return objAccountComponent;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}
}
