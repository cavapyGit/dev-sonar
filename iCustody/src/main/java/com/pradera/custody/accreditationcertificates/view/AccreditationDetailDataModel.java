package com.pradera.custody.accreditationcertificates.view;


import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccreditationOperationDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Feb 15, 2013
 */
public class AccreditationDetailDataModel extends ListDataModel<AccreditationDetail> implements SelectableDataModel<AccreditationDetail>{


	/** The size. */
	private int size;
	
	
	/**
	 * Instantiates a new accreditation operation data model.
	 */
	public AccreditationDetailDataModel(){
		super();
	}
	
	/**
	 * Instantiates a new accreditation operation data model.
	 *
	 * @param data the data
	 */
	public AccreditationDetailDataModel(List<AccreditationDetail> data){
		super(data);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public AccreditationDetail getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<AccreditationDetail> accreditationDetails = (List<AccreditationDetail>)getWrappedData();
        for(AccreditationDetail accreditation : accreditationDetails) {  
            if(String.valueOf(accreditation.getAccreditationOperation().getCustodyOperation().getIdCustodyOperationPk()).equals(rowKey))
                return accreditation;  
        }  
		return null;
	}

	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(AccreditationDetail accreditation) {
		// TODO Auto-generated method stub
		return accreditation.getAccreditationOperation().getCustodyOperation().getIdCustodyOperationPk();
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		List<System> systems=(List<System>)getWrappedData();
		if(systems==null)
			return 0;
		return systems.size();
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	
	
}
