package com.pradera.custody.accreditationcertificates.batch;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.accreditationcertificates.facade.ElectronicCatWebFacade;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.type.ProcessLoggerStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ElectronicCatWebRegisterBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 19-ago-2015
 */
@BatchProcess(name="ElectronicCatWebRegisterBatch")
@RequestScoped
public class ElectronicCatWebRegisterBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The security service facade. */
	
	@Inject
	private ParameterServiceBean parameterServiceBean;
	
	@EJB
	ElectronicCatWebFacade electronicCatWebFacade;
	
	@Inject
	private PraderaLogger log;
	
	

	/* (non-Javadoc)
	 * Batchero para registrar cat electronicos en la BD de la web
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		log.info("************************************* BATCH ENVIO CAT ELECTRONICO A LA WEB  ******************************************************");
		
		try {
			electronicCatWebFacade.serachAndProcess();
			
		System.out.println("************************************* FIN BATCH ENVIO CAT ELECTRONICO A LA WEB ******************************************************");
				
		} catch (Exception e) {
			processLogger.setErrorDetail("Error al ejecutar el script revisar la conexion dblink");
			processLogger.setLoggerState(1368);			
		}

	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener los parametros de notificaciones
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo implementado para obtener a los destinatarios
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * Metodo Implementado para enviar las notificaciones
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method st
		return false;
	}

}
