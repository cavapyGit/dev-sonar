package com.pradera.custody.accreditationcertificates.facade;

import java.sql.SQLException;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.accreditationcertificates.service.ReportDownloadFsinIsinCfiServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ReportDownloadFsinIsinCfiFacade {
	
	@Inject
    private PraderaLogger praderaLogger;
	
	@EJB
	private ReportDownloadFsinIsinCfiServiceBean reportDownloadFsinIsinCfiServiceBean;
	
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_ISSUER_MANAGEMENT)
	public void searchAndProcess() throws ServiceException, SQLException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

			reportDownloadFsinIsinCfiServiceBean.sendReportDownloadFsinIsinCfiToWeb();
			praderaLogger.info(":::: ENVIO EXITOSO DE FsinIsinCfi A LA WEB ::::"+new Date().toString());
			
	}

}
