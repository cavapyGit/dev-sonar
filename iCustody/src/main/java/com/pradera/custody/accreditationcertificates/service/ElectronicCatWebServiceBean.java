package com.pradera.custody.accreditationcertificates.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.channel.opening.to.Constatnt;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.SecuritySendBbv;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ElectronicCatWebServiceBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 05/02/2014
 */
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class ElectronicCatWebServiceBean extends CrudDaoServiceBean {

	@Inject
	PraderaLogger log;

	@Resource(lookup = "java:jboss/datasources/iDepositaryDS")
	javax.sql.DataSource dataSourceBBV;

	/**
	 * Registro de datos en la tabla de la web
	 * 
	 * @param idSecurityCode
	 * @throws ServiceException
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "resource" })
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void sendAvailableCatToWeb()	throws ServiceException, SQLException {
			
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		StringBuilder deleteAcretitationOperation = new StringBuilder();
		deleteAcretitationOperation.append(" DELETE from CAEDV.ACCREDITATION_OPERATION_WEB@"+searchConnection());

		StringBuilder insertAcretitationOperation = new StringBuilder();
		insertAcretitationOperation.append(" INSERT INTO CAEDV.ACCREDITATION_OPERATION_WEB@"+searchConnection()+" ");
		insertAcretitationOperation.append(" SELECT distinct(AO.ID_ACCREDITATION_OPERATION_PK) ID_ACCREDITATION_OPERATION_PK,  ");
		insertAcretitationOperation.append(" AO.REGISTER_DATE  REGISTER_DATE,  ");
		insertAcretitationOperation.append(" AO.validity_days validity_days,  ");
		insertAcretitationOperation.append(" AO.EXPIRATION_DATE EXPIRATION_DATE, ");
		insertAcretitationOperation.append(" AD.ID_PARTICIPANT_FK ID_PARTICIPANT_FK,  ");
		insertAcretitationOperation.append(" (SELECT DESCRIPTION FROM PARTICIPANT WHERE ID_PARTICIPANT_PK = AD.ID_PARTICIPANT_FK) AS PARTICIPANT_DESC,  ");
				
		insertAcretitationOperation.append(" (SELECT mnemonic FROM participant where id_participant_pk=AD.ID_PARTICIPANT_FK) NEMONICO,  ");
		insertAcretitationOperation.append(" AO.ACCREDITATION_STATE ACCREDITATION_STATE,  ");
		insertAcretitationOperation.append(" AO.CERTIFICATE_NUMBER CERTIFICATE_NUMBER,  ");
		insertAcretitationOperation.append(" AO.SECURITY_CODE_CAT SECURITY_CODE_CAT,  ");
		
		insertAcretitationOperation.append(" case when ao.cat_type = 2369 then null else (select description from parameter_table where parameter_table_pk = AO.motive) end as MOTIVE, ");
		insertAcretitationOperation.append(" AD.ID_HOLDER_ACCOUNT_FK ID_HOLDER_ACCOUNT_FK,  ");
		insertAcretitationOperation.append(" AD.ID_HOLDER_FK ID_HOLDER_FK,  ");		
		insertAcretitationOperation.append(" (SELECT LISTAGG(DOCUMENT_NUMBER, ', ') WITHIN GROUP ( ORDER BY DOCUMENT_NUMBER) FROM (SELECT DOCUMENT_NUMBER FROM HOLDER WHERE ID_HOLDER_PK IN (SELECT ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL WHERE ID_HOLDER_ACCOUNT_FK = AD.ID_HOLDER_ACCOUNT_FK ))) AS DOCUMENT_NUMBER, ");
		insertAcretitationOperation.append(" (SELECT LISTAGG(DOCUMENT_TYPE, ', ') WITHIN GROUP ( ORDER BY DOCUMENT_TYPE) FROM (SELECT DOCUMENT_TYPE FROM HOLDER WHERE ID_HOLDER_PK IN (SELECT ID_HOLDER_FK FROM HOLDER_ACCOUNT_DETAIL WHERE ID_HOLDER_ACCOUNT_FK = AD.ID_HOLDER_ACCOUNT_FK ))) AS DOCUMENT_TYPE,  ");
		insertAcretitationOperation.append(" (SELECT TEXT1 FROM parameter_table  WHERE  MASTER_TABLE_FK =504  and   PARAMETER_TABLE_PK = H.DOCUMENT_SOURCE) DOCUMENT_SOURCE, ");
		
		insertAcretitationOperation.append(" AD.ID_SECURITY_CODE_FK ID_SECURITY_CODE_FK,  ");
		insertAcretitationOperation.append(" AD.TOTAL_BALANCE TOTAL_BALANCE,  ");
		insertAcretitationOperation.append(" AD.AVAILABLE_BALANCE AVAILABLE_BALANCE, ");
		insertAcretitationOperation.append(" AD.BAN_BALANCE, ");
		insertAcretitationOperation.append(" AD.PAWN_BALANCE, ");
		insertAcretitationOperation.append(" AD.REPORTING_BALANCE ");
		
		insertAcretitationOperation.append(" FROM ACCREDITATION_OPERATION AO , ACCREDITATION_DETAIL AD, HOLDER_ACCOUNT_DETAIL HAD, HOLDER H ");
		insertAcretitationOperation.append(" WHERE 1 = 1 AND HAD.ID_HOLDER_ACCOUNT_FK = AD.ID_HOLDER_ACCOUNT_FK  ");
		insertAcretitationOperation.append(" AND AO.ID_ACCREDITATION_OPERATION_PK = AD.ID_ACCREDITATION_OPERATION_FK  ");
		insertAcretitationOperation.append(" AND AO.ACCREDITATION_STATE = 456 AND AO.SECURITY_CODE_CAT IS NOT NULL "); 
		insertAcretitationOperation.append(" AND AD.ID_HOLDER_FK = H.ID_HOLDER_PK  ");
		insertAcretitationOperation.append(" ORDER BY ID_ACCREDITATION_OPERATION_PK  ");
		
		
		StringBuilder deleteAcretitationDetail = new StringBuilder();
		deleteAcretitationDetail.append(" DELETE from CAEDV.ACCREDITATION_DETAIL_WEB@"+searchConnection());
		StringBuilder insertAcretitationDetail = new StringBuilder();
		insertAcretitationDetail.append(" INSERT INTO  CAEDV.ACCREDITATION_DETAIL_WEB@"+searchConnection()+" ");
		insertAcretitationDetail.append(" SELECT  ");
		insertAcretitationDetail.append(" ROWNUM ID_ACCREDITATION_DETAIL_WEB, ");
		insertAcretitationDetail.append(" AO.ID_ACCREDITATION_OPERATION_PK   ID_ACCREDITATION_OPERATION_PK,  ");
		insertAcretitationDetail.append(" AD.ID_HOLDER_ACCOUNT_FK ID_HOLDER_ACCOUNT_FK,  ");
		insertAcretitationDetail.append(" AD.ID_SECURITY_CODE_FK ID_SECURITY_CODE_FK,  ");
		insertAcretitationDetail.append(" (CASE when HA.ACCOUNT_TYPE = 106 then 'NATURAL' WHEN HA.ACCOUNT_TYPE = 107 then 'JURIDICO' WHEN HA.ACCOUNT_TYPE = 108 then 'MANCOMUNADO' END )ACCOUNT_TYPE,  ");
		insertAcretitationDetail.append(" H.ID_HOLDER_PK ID_HOLDER_PK,   ");
		insertAcretitationDetail.append(" H.DOCUMENT_NUMBER DOCUMENT_NUMBER, "); 
		insertAcretitationDetail.append(" (SELECT TEXT1 FROM parameter_table  WHERE  MASTER_TABLE_FK =504  and   PARAMETER_TABLE_PK = H.DOCUMENT_SOURCE) DOCUMENT_SOURCE, ");
		insertAcretitationDetail.append(" H.FULL_NAME   FULL_NAME  ");
		insertAcretitationDetail.append(" FROM ACCREDITATION_OPERATION AO , ACCREDITATION_DETAIL AD ,holder_account HA, holder_account_detail HAD, holder H  ");
		insertAcretitationDetail.append(" WHERE  AO.ID_ACCREDITATION_OPERATION_PK = AD.ID_ACCREDITATION_OPERATION_FK  ");
		insertAcretitationDetail.append(" AND  AO.ACCREDITATION_STATE =456  ");
		insertAcretitationDetail.append(" AND  AO.SECURITY_CODE_CAT IS NOT NULL "); 
		insertAcretitationDetail.append(" AND AD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  ");
		insertAcretitationDetail.append(" AND HA.ID_HOLDER_ACCOUNT_PK = HAD.id_holder_account_fk  ");
		insertAcretitationDetail.append(" AND HAD.ID_HOLDER_FK = H.ID_HOLDER_PK  ");
		insertAcretitationDetail.append(" ORDER BY  ID_ACCREDITATION_OPERATION_PK ");

		
		
			connection = dataSourceBBV.getConnection();
			preparedStatement = connection.prepareStatement(deleteAcretitationOperation.toString());
			preparedStatement.execute();
			
			preparedStatement = connection.prepareStatement(insertAcretitationOperation.toString());
			preparedStatement.execute();
			
			preparedStatement = connection.prepareStatement(deleteAcretitationDetail.toString());
			preparedStatement.execute();
			
			preparedStatement = connection.prepareStatement(insertAcretitationDetail.toString());
			preparedStatement.execute();
			
	

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
					
				} catch (SQLException e) {
					log.error(e.getMessage());
				}
			}
			
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.error(e.getMessage());
				}
			}
		

	}
	
	//*********
		
	/**
	 * Recupera la conexion dblink
	 * @return
	 * @throws ServiceException
	 */
	public String searchConnection() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append(" select pt.text1 from ParameterTable pt where pt.parameterTablePk="+Constatnt.TABLE_PARAMETER_DBLINK);		
		Query query = em.createQuery(sbQuery.toString());
		
		return (query.getSingleResult().toString()); 
	}
	
	

}
