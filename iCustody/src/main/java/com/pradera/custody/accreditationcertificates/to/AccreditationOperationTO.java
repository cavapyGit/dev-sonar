package com.pradera.custody.accreditationcertificates.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Version;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class AccreditationOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/04/2014
 */
public class AccreditationOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id accreditation operation pk. */
	private Long idAccreditationOperationPk;

	/** The custody operation. */
	private CustodyOperation custodyOperation;

	/** The accreditation certificate. */
	private byte[] accreditationCertificate;

	/** The certification file. */
	private byte[] certificationFile;

	/** The certification type. */
	private Integer certificationType;

	/** The expiration date. */
	private Date expirationDate;

	/** The holder. */
	private Holder holder;
	
	/** The holder description. */
	private String holderDescription;

	/** The participant petitioner. */
	private Participant participantPetitioner;

	/** The last modify app. */
	private Integer lastModifyApp;

	/** The last modify date. */
	private Date lastModifyDate;

	/** The last modify ip. */
	private String lastModifyIp;

	/** The last modify user. */
	private String lastModifyUser;

	/** The observations. */
	private String observations;

	/** The petitioner type. */
	private Integer petitionerType;

	/** The register date. */
	private Date registerDate;

	/** The way of payment. */
	private Integer wayOfPayment;

	/** The ind delivered. */
	private Integer indDelivered;
	
	/** The ind generated. */
	private Integer indGenerated;
	
	/** The destination certificate. */
	private Integer destinationCertificate;
	
	/** The destination certificate desc. */
	private String destinationCertificateDesc;
	
	/** The id signature certificate fk. */
	private Integer idSignatureCertificateFk;

	/** The id reject motive fk. */
	private Integer idRejectMotiveFk;

	/** The reject motive other. */
	private String rejectMotiveOther;

	/** The accreditation state. */
	private Integer accreditationState;
	
	/** The delivery date. */
	private Date deliveryDate;
	
	/** The generated date. */
	private Date generatedDate;
	
	/** The unblock motive. */
	private Integer unblockMotive;
	
	/** The unblock observation. */
	private String unblockObservation;
	
	/** The certificate number. */
	private Integer certificateNumber;
	
	/** The security code cat. */
	private String securityCodeCat;

	/** The list petitioner type. */
	private List<ParameterTable> listPetitionerType;
	
	/** The list security class. */
	private List<ParameterTable> listSecurityClass;
	
	/** The list participant petitioner. */
	private List<Participant> listParticipantPetitioner;
	
	/** The list date type. */
	private List<DateTypeTO> listDateType;
	
	/** The date type to. */
	private DateTypeTO dateTypeTO;
	
	/** The security. */
	private Security security;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The id block entity. */
	private Long idBlockEntity;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The ind operation type. */
	private Integer indOperationType;
	
	/** The real expiration date. */
	private Date realExpirationDate;
	
	/** The version. */
	@Version
    private Long version;
	
	/** The id unblock accreditation pk. */
	private Long idUnblockAccreditationPk;
	
	/** The user participant investor. */
	private boolean userParticipantInvestor;
	
	/** The user issuer dpf. */
	private boolean userIssuerDPF;
	
	/** The cat type. */
	private Integer catType;
	
	/**
	 * Instantiates a new accreditation operation to.
	 */
	public AccreditationOperationTO(){}
	
	/**
	 * Gets the unblock motive.
	 *
	 * @return the unblock motive
	 */
	public Integer getUnblockMotive() {
		return unblockMotive;
	}

	/**
	 * Sets the unblock motive.
	 *
	 * @param unblockMotive the new unblock motive
	 */
	public void setUnblockMotive(Integer unblockMotive) {
		this.unblockMotive = unblockMotive;
	}

	/**
	 * Gets the unblock observation.
	 *
	 * @return the unblock observation
	 */
	public String getUnblockObservation() {
		return unblockObservation;
	}

	/**
	 * Sets the unblock observation.
	 *
	 * @param unblockObservation the new unblock observation
	 */
	public void setUnblockObservation(String unblockObservation) {
		this.unblockObservation = unblockObservation;
	}

	/** The accreditation details. */
	private List<AccreditationDetail> accreditationDetails;

	/**
	 * Gets the accreditation certificate.
	 *
	 * @return the accreditation certificate
	 */
	public byte[] getAccreditationCertificate() {
		return this.accreditationCertificate;
	}

	/**
	 * Sets the accreditation certificate.
	 *
	 * @param accreditationCertificate the new accreditation certificate
	 */
	public void setAccreditationCertificate(byte[] accreditationCertificate) {
		this.accreditationCertificate = accreditationCertificate;
	}

	/**
	 * Gets the certification file.
	 *
	 * @return the certification file
	 */
	public byte[] getCertificationFile() {
		return this.certificationFile;
	}

	/**
	 * Sets the certification file.
	 *
	 * @param certificationFile the new certification file
	 */
	public void setCertificationFile(byte[] certificationFile) {
		this.certificationFile = certificationFile;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return this.expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return this.lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return this.lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return this.lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return this.lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return this.observations;
	}

	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}

	/**
	 * Gets the register date.
	 *
	 * @return the register date
	 */
	public Date getRegisterDate() {
		return this.registerDate;
	}

	/**
	 * Sets the register date.
	 *
	 * @param registerDate the new register date
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the participant petitioner.
	 *
	 * @return the participant petitioner
	 */
	public Participant getParticipantPetitioner() {
		return participantPetitioner;
	}

	/**
	 * Gets the ind generated.
	 *
	 * @return the ind generated
	 */
	public Integer getIndGenerated() {
		return indGenerated;
	}

	/**
	 * Sets the ind generated.
	 *
	 * @param indGenerated the new ind generated
	 */
	public void setIndGenerated(Integer indGenerated) {
		this.indGenerated = indGenerated;
	}

	/**
	 * Gets the generated date.
	 *
	 * @return the generated date
	 */
	public Date getGeneratedDate() {
		return generatedDate;
	}

	/**
	 * Sets the generated date.
	 *
	 * @param generatedDate the new generated date
	 */
	public void setGeneratedDate(Date generatedDate) {
		this.generatedDate = generatedDate;
	}

	/**
	 * Sets the participant petitioner.
	 *
	 * @param participantPetitioner the new participant petitioner
	 */
	public void setParticipantPetitioner(Participant participantPetitioner) {
		this.participantPetitioner = participantPetitioner;
	}

	/**
	 * Gets the accreditation details.
	 *
	 * @return the accreditation details
	 */
	public List<AccreditationDetail> getAccreditationDetails() {
		return accreditationDetails;
	}

	/**
	 * Sets the accreditation details.
	 *
	 * @param accreditationDetails the new accreditation details
	 */
	public void setAccreditationDetails(
			List<AccreditationDetail> accreditationDetails) {
		this.accreditationDetails = accreditationDetails;
	}

	/**
	 * Gets the certification type.
	 *
	 * @return the certification type
	 */
	public Integer getCertificationType() {
		return certificationType;
	}

	/**
	 * Sets the certification type.
	 *
	 * @param certificationType the new certification type
	 */
	public void setCertificationType(Integer certificationType) {
		this.certificationType = certificationType;
	}

	/**
	 * Gets the petitioner type.
	 *
	 * @return the petitioner type
	 */
	public Integer getPetitionerType() {
		return petitionerType;
	}

	/**
	 * Sets the petitioner type.
	 *
	 * @param petitionerType the new petitioner type
	 */
	public void setPetitionerType(Integer petitionerType) {
		this.petitionerType = petitionerType;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the id accreditation operation pk.
	 *
	 * @return the id accreditation operation pk
	 */
	public Long getIdAccreditationOperationPk() {
		return idAccreditationOperationPk;
	}

	/**
	 * Sets the id accreditation operation pk.
	 *
	 * @param idAccreditationOperationPk the new id accreditation operation pk
	 */
	public void setIdAccreditationOperationPk(Long idAccreditationOperationPk) {
		this.idAccreditationOperationPk = idAccreditationOperationPk;
	}

	/**
	 * Gets the way of payment.
	 *
	 * @return the way of payment
	 */
	public Integer getWayOfPayment() {
		return wayOfPayment;
	}

	/**
	 * Sets the way of payment.
	 *
	 * @param wayOfPayment the new way of payment
	 */
	public void setWayOfPayment(Integer wayOfPayment) {
		this.wayOfPayment = wayOfPayment;
	}

	/**
	 * Gets the ind delivered.
	 *
	 * @return the ind delivered
	 */
	public Integer getIndDelivered() {
		return indDelivered;
	}

	/**
	 * Sets the ind delivered.
	 *
	 * @param indDelivered the new ind delivered
	 */
	public void setIndDelivered(Integer indDelivered) {
		this.indDelivered = indDelivered;
	}

	/**
	 * Gets the destination certificate.
	 *
	 * @return the destination certificate
	 */
	public Integer getDestinationCertificate() {
		return destinationCertificate;
	}

	/**
	 * Sets the destination certificate.
	 *
	 * @param destinationCertificate the new destination certificate
	 */
	public void setDestinationCertificate(Integer destinationCertificate) {
		this.destinationCertificate = destinationCertificate;
	}

	/**
	 * Gets the accreditation state.
	 *
	 * @return the accreditation state
	 */
	public Integer getAccreditationState() {
		return accreditationState;
	}

	/**
	 * Sets the accreditation state.
	 *
	 * @param accreditationState the new accreditation state
	 */
	public void setAccreditationState(Integer accreditationState) {
		this.accreditationState = accreditationState;
	}

	/**
	 * Gets the id signature certificate fk.
	 *
	 * @return the id signature certificate fk
	 */
	public Integer getIdSignatureCertificateFk() {
		return idSignatureCertificateFk;
	}

	/**
	 * Sets the id signature certificate fk.
	 *
	 * @param idSignatureCertificateFk the new id signature certificate fk
	 */
	public void setIdSignatureCertificateFk(Integer idSignatureCertificateFk) {
		this.idSignatureCertificateFk = idSignatureCertificateFk;
	}

	/**
	 * Gets the id reject motive fk.
	 *
	 * @return the id reject motive fk
	 */
	public Integer getIdRejectMotiveFk() {
		return idRejectMotiveFk;
	}

	/**
	 * Sets the id reject motive fk.
	 *
	 * @param idRejectMotiveFk the new id reject motive fk
	 */
	public void setIdRejectMotiveFk(Integer idRejectMotiveFk) {
		this.idRejectMotiveFk = idRejectMotiveFk;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the delivery date.
	 *
	 * @return the delivery date
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * Sets the delivery date.
	 *
	 * @param deliveryDate the new delivery date
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * Gets the destination certificate desc.
	 *
	 * @return the destination certificate desc
	 */
	public String getDestinationCertificateDesc() {
		return destinationCertificateDesc;
	}

	/**
	 * Sets the destination certificate desc.
	 *
	 * @param destinationCertificateDesc the new destination certificate desc
	 */
	public void setDestinationCertificateDesc(String destinationCertificateDesc) {
		this.destinationCertificateDesc = destinationCertificateDesc;
	}

	/**
	 * Gets the certificate number.
	 *
	 * @return the certificate number
	 */
	public Integer getCertificateNumber() {
		return certificateNumber;
	}

	/**
	 * Sets the certificate number.
	 *
	 * @param certificateNumber the new certificate number
	 */
	public void setCertificateNumber(Integer certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	/**
	 * Gets the security code cat.
	 *
	 * @return the securityCodeCat
	 */
	public String getSecurityCodeCat() {
		return securityCodeCat;
	}

	/**
	 * Sets the security code cat.
	 *
	 * @param securityCodeCat the securityCodeCat to set
	 */
	public void setSecurityCodeCat(String securityCodeCat) {
		this.securityCodeCat = securityCodeCat;
	}
	
	/**
	 * Gets the list participant petitioner.
	 *
	 * @return the list participant petitioner
	 */
	public List<Participant> getListParticipantPetitioner() {
		return listParticipantPetitioner;
	}

	/**
	 * Sets the list participant petitioner.
	 *
	 * @param listParticipantPetitioner the new list participant petitioner
	 */
	public void setListParticipantPetitioner(
			List<Participant> listParticipantPetitioner) {
		this.listParticipantPetitioner = listParticipantPetitioner;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the new issuer
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the list petitioner type.
	 *
	 * @return the list petitioner type
	 */
	public List<ParameterTable> getListPetitionerType() {
		return listPetitionerType;
	}

	/**
	 * Sets the list petitioner type.
	 *
	 * @param listPetitionerType the new list petitioner type
	 */
	public void setListPetitionerType(List<ParameterTable> listPetitionerType) {
		this.listPetitionerType = listPetitionerType;
	}

	/**
	 * Gets the list security class.
	 *
	 * @return the list security class
	 */
	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}

	/**
	 * Sets the list security class.
	 *
	 * @param listSecurityClass the new list security class
	 */
	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the final date
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the new final date
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the list date type.
	 *
	 * @return the list date type
	 */
	public List<DateTypeTO> getListDateType() {
		return listDateType;
	}

	/**
	 * Sets the list date type.
	 *
	 * @param listDateType the new list date type
	 */
	public void setListDateType(List<DateTypeTO> listDateType) {
		this.listDateType = listDateType;
	}

	/**
	 * Gets the date type to.
	 *
	 * @return the date type to
	 */
	public DateTypeTO getDateTypeTO() {
		return dateTypeTO;
	}

	/**
	 * Sets the date type to.
	 *
	 * @param dateTypeTO the new date type to
	 */
	public void setDateTypeTO(DateTypeTO dateTypeTO) {
		this.dateTypeTO = dateTypeTO;
	}

	/**
	 * Gets the id block entity.
	 *
	 * @return the id block entity
	 */
	public Long getIdBlockEntity() {
		return idBlockEntity;
	}

	/**
	 * Sets the id block entity.
	 *
	 * @param idBlockEntity the new id block entity
	 */
	public void setIdBlockEntity(Long idBlockEntity) {
		this.idBlockEntity = idBlockEntity;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}

	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}

	/**
	 * Gets the ind operation type.
	 *
	 * @return the ind operation type
	 */
	public Integer getIndOperationType() {
		return indOperationType;
	}

	/**
	 * Sets the ind operation type.
	 *
	 * @param indOperationType the new ind operation type
	 */
	public void setIndOperationType(Integer indOperationType) {
		this.indOperationType = indOperationType;
	}

	/**
	 * Gets the real expiration date.
	 *
	 * @return the realExpirationDate
	 */
	public Date getRealExpirationDate() {
		return realExpirationDate;
	}

	/**
	 * Sets the real expiration date.
	 *
	 * @param realExpirationDate the realExpirationDate to set
	 */
	public void setRealExpirationDate(Date realExpirationDate) {
		this.realExpirationDate = realExpirationDate;
	}

	/**
	 * Gets the id unblock accreditation pk.
	 *
	 * @return the idUnblockAccreditationPk
	 */
	public Long getIdUnblockAccreditationPk() {
		return idUnblockAccreditationPk;
	}

	/**
	 * Sets the id unblock accreditation pk.
	 *
	 * @param idUnblockAccreditationPk the idUnblockAccreditationPk to set
	 */
	public void setIdUnblockAccreditationPk(Long idUnblockAccreditationPk) {
		this.idUnblockAccreditationPk = idUnblockAccreditationPk;
	}

	/**
	 * Gets if the user is participant investor.
	 *
	 * @return the userParticipantInvestor
	 */
	public boolean isUserParticipantInvestor() {
		return userParticipantInvestor;
	}

	/**
	 * Sets the user is participant investor.
	 *
	 * @param userParticipantInvestor the userParticipantInvestor to set
	 */
	public void setUserParticipantInvestor(boolean userParticipantInvestor) {
		this.userParticipantInvestor = userParticipantInvestor;
	}

	/**
	 * Gets if the user is issuer DPF.
	 *
	 * @return the userIssuerDPF
	 */
	public boolean isUserIssuerDPF() {
		return userIssuerDPF;
	}

	/**
	 * Sets the user is issuer DPF.
	 *
	 * @param userIssuerDPF the userIssuerDPF to set
	 */
	public void setUserIssuerDPF(boolean userIssuerDPF) {
		this.userIssuerDPF = userIssuerDPF;
	}

	public Integer getCatType() {
		return catType;
	}

	public void setCatType(Integer catType) {
		this.catType = catType;
	}

	
}
