package com.pradera.custody.accreditationcertificates.facade;

import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.accreditationcertificates.service.ElectronicCatWebServiceBean;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ElectronicCatWebFacade
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/06/2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class ElectronicCatWebFacade {
	
	@Inject
    private PraderaLogger praderaLogger;
	
	@EJB
	private ElectronicCatWebServiceBean electronicCatWebServiceBean;
	
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	@ProcessAuditLogger(process=BusinessProcessType.SECURITY_ISSUER_MANAGEMENT)
	public void serachAndProcess() throws ServiceException, SQLException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());

		
			electronicCatWebServiceBean.sendAvailableCatToWeb();
			praderaLogger.info(":::: ENVIO EXITOSO DE CATs A LA WEB ::::"+new Date().toString());
		

	}


}
