package com.pradera.custody.accreditationcertificates.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.services.remote.billing.BillingConexionService;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.CorrelativeServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.operation.to.MechanismOperationTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.accreditationcertificates.service.AccreditationCertificatesServiceBean;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.core.service.CustodyComponentSingleton;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDeliveryInformation;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationMarketFact;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.UnblockAccreditationOperation;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationRejectMotiveType;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.AccreditationOperationUnblockType;
import com.pradera.model.custody.type.AccreditationPaymentWayType;
import com.pradera.model.custody.type.CatType;
import com.pradera.model.custody.type.CertificationType;
import com.pradera.model.custody.type.UnblockAccreditationOperationStateType;
import com.pradera.model.generalparameter.Correlative;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CorrelativeType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccreditationCertificatesServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Feb 14, 2013
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class AccreditationCertificatesServiceFacade{

	/** The bean service. */
	@Inject
	private AccreditationCertificatesServiceBean beanService;
	

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The executor component service bean. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;

	/**  The calculation Component Billing*. */
	@Inject 
	Instance<BillingConexionService> billingConexion;
	
	
	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/** The correlative service. */
	@Inject
	private CorrelativeServiceBean correlativeService;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/** The certificate report id. */
	private final Long CERTIFICATE_REPORT_ID = new Long(6);
	
	/** The certificate report id. */
	private final Long CERTIFICATE_REPORT_PGS_ID = new Long(287);
	
	/** The certificate report id. */
	private final Long CERTIFICATE_REPORT_B_ID = new Long(288);
	
	/** The certificate report id. */
	private final Long CERTIFICATE_REPORT_B_PGS_ID = new Long(289);
	
	//private final Long   BLOCKED_REPORT_ID   = new Long(72);
	
	/** The custody service consumer. */
	@EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	@EJB
	private CustodyComponentSingleton custodyComponentSingleton;
	
	/**
	 * Register new Available Balance request.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @return String
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER)
	public AccreditationOperation save(AccreditationOperation accreditationOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());        
        //VALIDATING Holder And Participant
        this.validationsBeforeSave(accreditationOperation);
        //Calling operation Number
        Long operationNumber = beanService.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_ACCREDITATION_CERTIFICATE);        
        accreditationOperation.getCustodyOperation().setOperationNumber(operationNumber);
        accreditationOperation.getCustodyOperation().setIndRectification(ComponentConstant.ZERO);
        if(accreditationOperation.getExpirationDate() != null){
			Date realExpirationDateWithTime = CommonsUtilities.setTimeToDate(accreditationOperation.getExpirationDate(), CommonsUtilities.currentDateTime()); 
        	accreditationOperation.setRealExpirationDate(realExpirationDateWithTime);
        }
			
        AccreditationOperation accOperationSaved = beanService.create(accreditationOperation);
        this.moveToTransit(accOperationSaved);		
		return accOperationSaved;
	}
	
	/**
	 * Register new Blocked Balance request.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @return String
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER)
	public AccreditationOperation saveBlocked(AccreditationOperation accreditationOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        //VALIDATING Holder And Participant
        this.validationsBeforeSave(accreditationOperation);
        //Calling operation Number
        Long operationNumber = beanService.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_ACCREDITATION_CERTIFICATE);
        
        accreditationOperation.getCustodyOperation().setOperationNumber(operationNumber);
        if(accreditationOperation.getExpirationDate() != null){
			Date realExpirationDateWithTime = CommonsUtilities.setTimeToDate(accreditationOperation.getExpirationDate(), CommonsUtilities.currentDateTime()); 
        	accreditationOperation.setRealExpirationDate(realExpirationDateWithTime);
        }
        /*List<AccreditationDetail> accreditationDetailList = accreditationOperation.getAccreditationDetails();
		if(Validations.validateListIsNotNullAndNotEmpty(accreditationDetailList)){
			if (accreditationDetailList.get(0).getBlockOperationCertifications()!=null){
				for (int i = 0; i < accreditationDetailList.size(); i++) {
					accreditationDetailList.get(0).getBlockOperationCertifications().get(0).setBlockOperationCertPk(null);
	        	}
			}
		}*/
		//accreditationOperation.setAccreditationDetails(accreditationDetailList);
		
		AccreditationOperation accOperationSaved = beanService.create(accreditationOperation);

		return accOperationSaved;
	}
	
	/**
	 * Register new Reported Balance request.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @return String
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER)
	public AccreditationOperation insertReported(AccreditationOperation accreditationOperation) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        //VALIDATING Holder And Participant
        this.validationsBeforeSave(accreditationOperation);
        //Calling operation Number
        Long operationNumber = beanService.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_ACCREDITATION_CERTIFICATE);        
        accreditationOperation.getCustodyOperation().setOperationNumber(operationNumber);

        if(Validations.validateListIsNotNullAndNotEmpty(accreditationOperation.getListAccreditationDeliveryInformation())){
        	for(AccreditationDeliveryInformation objDeliveryInfo : accreditationOperation.getListAccreditationDeliveryInformation()){
        		objDeliveryInfo.setIdAccreditationDeliveryPk(null);
            }
        }

		List<AccreditationDetail> accreditationDetailList = accreditationOperation.getAccreditationDetails();
		if(Validations.validateListIsNotNullAndNotEmpty(accreditationDetailList)){
			if (accreditationDetailList.get(0).getAccreditationReported()!=null){
				for (int i = 0; i < accreditationDetailList.size(); i++) {
					accreditationDetailList.get(0).getAccreditationReported().get(0).setIdReportAccreditationPk(null);;
	        	}
			}
			/*if (accreditationDetailList.get(0).getBlockOperationCertifications()!=null){
				for (int i = 0; i < accreditationDetailList.size(); i++) {
					accreditationDetailList.get(0).getBlockOperationCertifications().get(0).setBlockOperationCertPk(null);
	        	}
			}*/
		}

		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();	
		for(AccreditationDetail objAccreditationDetail : accreditationDetailList){
			if(!objAccreditationDetail.getAvailableBalance().equals(BigDecimal.ZERO)){
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdHolderAccount(objAccreditationDetail.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(objAccreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdSecurityCode(objAccreditationDetail.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(objAccreditationDetail.getAvailableBalance());
				holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				if(Validations.validateIsNotNullAndNotEmpty(objAccreditationDetail.getAccreditationMarketFactList())) {
					for(AccreditationMarketFact objAccreditationMarketFact : objAccreditationDetail.getAccreditationMarketFactList()){
						MarketFactAccountTO mfaTO= new MarketFactAccountTO();
						mfaTO.setMarketPrice(objAccreditationMarketFact.getMarketPrice());
						mfaTO.setMarketDate(objAccreditationMarketFact.getMarketDate());
						mfaTO.setMarketRate(objAccreditationMarketFact.getMarketRate());
						mfaTO.setQuantity(objAccreditationMarketFact.getQuantityToAccredit());
						holderAccountBalanceTO.getLstMarketFactAccounts().add(mfaTO);
					}
				}
				accountBalanceTOs.add(holderAccountBalanceTO);
			}
		}
		
		if(accreditationOperation.getExpirationDate() != null){
			Date realExpirationDateWithTime = CommonsUtilities.setTimeToDate(accreditationOperation.getExpirationDate(), CommonsUtilities.currentDateTime()); 
        	accreditationOperation.setRealExpirationDate(realExpirationDateWithTime);
        }
		
		AccreditationOperation accOperationSaved = beanService.create(accreditationOperation);
		if(accountBalanceTOs.size() > 0){
			accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperation.getIdAccreditationOperationPk(),
					 BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER.getCode(), 
					 accountBalanceTOs,
					 ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));	
		}		
		return accOperationSaved;
	}
	

	/**
	 * Metodo para registrar la solicitud de CAT B
	 * @param accreditationOperation
	 * @return
	 * @throws ServiceException
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER)
	public AccreditationOperation insertRequestCatB(AccreditationOperation accreditationOperation) throws ServiceException{
		
		//Establecemos los privilegios para la operacion
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        //Validamos que el CUI y el Participante se encuentren activoss
        this.validationsBeforeSave(accreditationOperation);
        
        //Generamos el numero de operacionS
        Long operationNumber = beanService.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_ACCREDITATION_CERTIFICATE);        
        accreditationOperation.getCustodyOperation().setOperationNumber(operationNumber);
        
        //Llenamos datos irrelevantes para el CAT B
        accreditationOperation.setDestinationCertificate(GeneralConstants.ZERO_VALUE_INTEGER);
        accreditationOperation.setWayOfPayment(AccreditationPaymentWayType.FREE.getCode());
        accreditationOperation.setIndGenerated(GeneralConstants.ZERO_VALUE_INTEGER);
        accreditationOperation.setValidityDays(GeneralConstants.ZERO_VALUE_INTEGER);
        
        //Si tiene datos de entrega los registramos
        if(Validations.validateListIsNotNullAndNotEmpty(accreditationOperation.getListAccreditationDeliveryInformation())){
        	for(AccreditationDeliveryInformation objDeliveryInfo : accreditationOperation.getListAccreditationDeliveryInformation()){
        		objDeliveryInfo.setIdAccreditationDeliveryPk(null);
            }
        }
		//Registramos la solicitud
		AccreditationOperation accOperationSaved = beanService.create(accreditationOperation);
		
		//Retornamos el resultado
		return accOperationSaved;
	}
	
	
	
	/**
	 * moveToTransit.	 
	 *
	 * @param accreditationOperation the accreditation operation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public void moveToTransit(AccreditationOperation accreditationOperation) throws ServiceException{		
		List<AccreditationDetail> accreditationDetailList = accreditationOperation.getAccreditationDetails();
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();		
		for(AccreditationDetail objAccreditationDetail : accreditationDetailList){
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(objAccreditationDetail.getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(objAccreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(objAccreditationDetail.getSecurity().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(objAccreditationDetail.getTotalBalance());
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(AccreditationMarketFact objAccreditationMarketFact : objAccreditationDetail.getAccreditationMarketFactList()){
				MarketFactAccountTO mfaTO= new MarketFactAccountTO();
				mfaTO.setMarketPrice(objAccreditationMarketFact.getMarketPrice());
				mfaTO.setMarketDate(objAccreditationMarketFact.getMarketDate());
				mfaTO.setMarketRate(objAccreditationMarketFact.getMarketRate());
				mfaTO.setQuantity(objAccreditationMarketFact.getQuantityToAccredit());
				holderAccountBalanceTO.getLstMarketFactAccounts().add(mfaTO);
			}
			accountBalanceTOs.add(holderAccountBalanceTO);
		}		
		accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperation.getIdAccreditationOperationPk(),
															 BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER.getCode(), 
															 accountBalanceTOs,
															 ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));	
	}
	
	/**
	 * Gets the accreditation certification.
	 *
	 * @param certificationId the certification id
	 * @return the accreditation certification
	 * @throws ServiceException the service exception
	 */
	public byte[] getAccreditationCertification(Long certificationId) throws ServiceException{
		return this.beanService.getAccreditationCertification(certificationId);
	}
	
	/**
	 * Validations before save.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public void validationsBeforeSave(AccreditationOperation accreditationOperation) throws ServiceException{
		Map<String,Object> participantPkParam = new HashMap<String,Object>();
		Map<String,Object> holderPkParam = new HashMap<String,Object>();

		//Error service type
		ErrorServiceType errorServiceType = null ;
		
		//Validating Holder is REGISTERED
		if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
			holderPkParam.put("idHolderPkParam", accreditationOperation.getHolder().getIdHolderPk());	
			Holder holder = this.beanService.findObjectByNamedQuery(Holder.HOLDER_STATE, holderPkParam);
			if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				errorServiceType = ErrorServiceType.HOLDER_BLOCK;
			}
		}
		
		if(Validations.validateIsNotNull(accreditationOperation.getParticipantPetitioner()) 
		&& Validations.validateIsNotNull(accreditationOperation.getParticipantPetitioner().getIdParticipantPk())){
			//Validating Participant is REGISTERED
			participantPkParam.put("idParticipantPkParam", accreditationOperation.getParticipantPetitioner().getIdParticipantPk());	
			Participant participant = this.beanService.findObjectByNamedQuery(Participant.PARTICIPANT_STATE, participantPkParam);
			if(!participant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
				errorServiceType = ErrorServiceType.PARTICIPANT_BLOCK;
			}
		}
		
		if(errorServiceType!=null){
			throw new ServiceException(errorServiceType);
		}
	}
	
	/**
	 * Update.
	 *
	 * @param object the object
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */	
	public boolean update(Object object) throws ServiceException{
		beanService.update(object);
		return true;
	}

	/**
	 * Change accreditation status.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @throws ServiceException the service exception
	 */	
	public void updateAccreditationOperation(AccreditationOperationTO accreditationOperationTO) throws ServiceException{                
		beanService.updateAccreditationOperation(accreditationOperationTO);
	}
	
	/**
	 * Deliver accreditation.
	 *
	 * @param idAccreditationPk the id accreditation pk
	 * @param destination the destination
	 * @throws ServiceException the service exception
	 */
	public void deliverAccreditation(Long idAccreditationPk, Integer destination) throws ServiceException{
        
		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
		
		accreditationOperationTO.setIdAccreditationOperationPk(idAccreditationPk);
		accreditationOperationTO.setDestinationCertificate(destination);		
		accreditationOperationTO.setIndDelivered(1);
		accreditationOperationTO.setDeliveryDate(CommonsUtilities.currentDate());
        this.beanService.updateAccreditationOperation(accreditationOperationTO);
	}
	
	/**
	 * Metodo para generar el CAT
	 * @param accreditationOperationTO
	 * @throws ServiceException
	 */
	 
	public void generateConstance(AccreditationOperationTO accreditationOperationTO) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeGenerate());
        
        //Pk de la operacion
		Long accreditationId = accreditationOperationTO.getIdAccreditationOperationPk();
		
		//Extraemos la clase de valor de la solicitud
		Integer intClassSecurity = beanService.getSecurityClassRequest(accreditationId);
		
		// CAT NORMAL
		if(accreditationOperationTO.getCatType().equals(CatType.CAT.getCode())){
			//Si es una clase distinta a PGS enviamos REPORT_ID 6 sino 287
			if (SecurityClassType.PGS.getCode().equals(intClassSecurity)
					||SecurityClassType.PGB.getCode().equals(intClassSecurity)){
				this.reportService.get().setAccreditationCertificate(CERTIFICATE_REPORT_PGS_ID, accreditationId, loggerUser.getUserName(), loggerUser);
			}else{
				this.reportService.get().setAccreditationCertificate(CERTIFICATE_REPORT_ID, accreditationId, loggerUser.getUserName(), loggerUser);
			}
		}else{ //CAT B
			//Si es una clase distinta a PGS enviamos REPORT_ID 288 sino 289
			if (SecurityClassType.PGS.getCode().equals(intClassSecurity)
					||SecurityClassType.PGB.getCode().equals(intClassSecurity)){
				this.reportService.get().setAccreditationCertificate(CERTIFICATE_REPORT_B_PGS_ID, accreditationId, loggerUser.getUserName(), loggerUser);
			}else{
				this.reportService.get().setAccreditationCertificate(CERTIFICATE_REPORT_B_ID, accreditationId, loggerUser.getUserName(), loggerUser);
			}
		}
	}

	/**
	 * Generate constance massive.
	 *
	 * @param accreditationOperationTOs the accreditation operation t os
	 * @throws ServiceException the service exception
	 */
	public void generateConstanceMassive(List<AccreditationOperationTO> accreditationOperationTOs) throws ServiceException {
        for(AccreditationOperationTO accreditationOperationTO : accreditationOperationTOs){
        	generateConstance(accreditationOperationTO);
        }
	}
	
	/**
	 * Delete.
	 *
	 * @param object the object
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean delete(Object object, int id){

		beanService.delete(object.getClass(), id);
		return true;
	}
	// Get All participants
	/**
	 * Find all participant.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> findAllParticipant() throws ServiceException{

		return beanService.findAllParticipants();
	}
	
	
	/**
	 * Search holder account.
	 *
	 * @param holder the holder
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> searchHolderAccount(Holder holder) throws ServiceException{

		if(holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())||
				holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
			return beanService.searchHolderAccount(holder);	
		}else{
			return null;
		}			
	}
	
	/**
	 * Search holder account by participant.
	 *
	 * @param holder the holder
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> searchHolderAccountByParticipant(Holder holder, Participant participant) throws ServiceException{

		if(holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())||
				holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
			return beanService.searchHolderAccountByParticipant(holder, participant);
		}else{
			return null;
		}
		
	}
	// Get Any parameter List
	/**
	 * Gets the parameter list.
	 *
	 * @param type the type
	 * @return the parameter list
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterList(MasterTableType type) throws ServiceException {

		ParameterTableTO parameterFilter = new ParameterTableTO();

		parameterFilter.setMasterTableFk(type.getCode());
		parameterFilter.setState(1);

		List<ParameterTable> parameterList = new ArrayList<ParameterTable>();
	
		parameterList = beanService.getListParameterTable(parameterFilter);	
		return parameterList;

	}
	
	/**
	 * Search holder account balance.
	 *
	 * @param idHolderAccountPks the id holder account pks
	 * @param security the security
	 * @param userInfo the user info
	 * @param lstSecurities the lst securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance>searchHolderAccountBalance(List<Long> idHolderAccountPks, Security security, UserInfo userInfo,List<String> lstSecurities, Integer motive) throws ServiceException{

		List<HolderAccountBalance> list = beanService.searchHolderAccountBalance(idHolderAccountPks, security, userInfo , lstSecurities, motive);
		if(Validations.validateIsNotNullAndNotEmpty(list)){
			for(HolderAccountBalance hab : list){
				if(Validations.validateIsNotNull(hab.getHolderMarketfactBalance())){
					if(hab.getHolderMarketfactBalance().size()>1){
						hab.setDisabled(true);
					}				
				}
				if(Validations.validateIsNotNull(hab.getHolderAccount()!=null)){
					hab.getHolderAccount().getHolderAccountDetails().size();
					
					if(Validations.validateIsNotNullAndNotEmpty(hab.getHolderAccount().getHolderAccountDetails())){
						for(HolderAccountDetail had : hab.getHolderAccount().getHolderAccountDetails()){
							if(had.getHolder()!=null){
								had.getHolder().getIdHolderPk();
							}
						}
					}
				}
			}
		}
		
		return list;
	}
	
	/**
	 * Search reported balance.
	 *
	 * @param idHolderAccountPks the id holder account pks
	 * @param security the security
	 * @param expirationDate the expiration date
	 * @param userInfo the user info
	 * @param lstSecurities the lst securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<MechanismOperationTO> searchReportedBalance(List<Long> idHolderAccountPks,Security security, Date expirationDate,UserInfo userInfo,List<String> lstSecurities) throws ServiceException{

		return beanService.searchReportedOperation(idHolderAccountPks, security, expirationDate, userInfo, lstSecurities);
	}
	
	/**
	 * Metodo para buscar valores para CAT B
	 * @param idHolderAccountPks
	 * @param security
	 * @return
	 * @throws ServiceException
	 */
	public List<HolderAccountBalanceExp> searchHolderAccountBalanceB(List<Long> idHolderAccountPks, Long idParticipant,Security security) throws ServiceException{
		List<HolderAccountBalanceExp> list = beanService.searchHolderAccountBalanceB(idHolderAccountPks,idParticipant, security);
		return list;
	}
	
	
	/**
	 * Accreditation available consult.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @param initialDate the initial date
	 * @param endDate the end date
	 * @param currency the currency
	 * @param securityClass the security class
	 * @param issuer the issuer
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_QUERY)
	public List<AccreditationOperation> accreditationAvailableConsult(AccreditationOperation accreditationOperation, Date initialDate, Date endDate, Integer currency, Integer securityClass, String issuer)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
        List<AccreditationOperation> listAccreditationOperation = new ArrayList<AccreditationOperation>();
        listAccreditationOperation =  beanService.accreditationAvailableConsult(accreditationOperation, initialDate, endDate, currency, securityClass,issuer);
        for(int i=0;i<listAccreditationOperation.size();i++){
        	listAccreditationOperation.get(i).getCustodyOperation().getOperationNumber();
        		listAccreditationOperation.get(i).getAccreditationDetails().size();
            	if(listAccreditationOperation.get(i).getListAccreditationDeliveryInformation()!=null){
            		listAccreditationOperation.get(i).getListAccreditationDeliveryInformation().size();}
            	if(listAccreditationOperation.get(i).getParticipantPetitioner()!=null){
            	   listAccreditationOperation.get(i).getParticipantPetitioner().getIdParticipantPk();}
            	if(listAccreditationOperation.get(i).getHolder()!=null){
            		listAccreditationOperation.get(i).getHolder().getIdHolderPk();}
            	listAccreditationOperation.get(i).setFormatSecurities(listAccreditationOperation.get(i).getAccreditationDetails().get(0).getSecurity().getIdSecurityCodePk().toString());
        }
        return listAccreditationOperation;
	}
	
	/**
	 * Search block operation detail.
	 *
	 * @param holder the holder
	 * @param idHolderAccountPks the id holder account pks
	 * @param security the security
	 * @param blockType the block type
	 * @param idBlockEntity the id block entity
	 * @param userInfo the user info
	 * @param lstSecurities the lst securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperation> searchBlockOperationDetail(Holder holder, List<Long>idHolderAccountPks,Security security, Integer blockType,Long idBlockEntity, UserInfo userInfo, List<String> lstSecurities)
			throws ServiceException{
		List<BlockOperation> lstBlockOperation = beanService.searchBlockOperationDetail(holder,idHolderAccountPks,security ,blockType, idBlockEntity, userInfo, lstSecurities);
		return lstBlockOperation;
	}
	
	/**
	 * Gets the lst holder account for doc number and doc type.
	 *
	 * @param participant the participant
	 * @return the lst holder account for doc number and doc type
	 * @throws ServiceException the service exception
	 */
	public List<Long> getLstHolderAccountForDocNumberAndDocType(Participant participant) throws ServiceException{
		return beanService.getLstHolderAccountForDocNumberAndDocType(participant);
	}
	
	/**
	 * Gets the lst securities for part inv.
	 *
	 * @param participant the participant
	 * @param holderAccounts the holder accounts
	 * @return the lst securities for part inv
	 * @throws ServiceException the service exception
	 */
	public List<String> getLstSecuritiesForPartInv(Participant participant,List<Long> holderAccounts) throws ServiceException{
		return beanService.getLstSecuritiesForPartInv(participant,holderAccounts);
	}
	
	/**
	 * Find accreditation operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public AccreditationOperation findAccreditationOperation(Long accreditationOperationId)throws ServiceException{
		return beanService.findAccreditationOperation(accreditationOperationId);
	}
	
	/**
	 * Verify accreditation operation certificate.
	 *
	 * @param bo the bo
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationOperation> verifyAccreditationOperationCertificate(BlockOperation bo)throws ServiceException{
		return beanService.verifyAccreditationOperation(bo);
	}
	
	/**
	 * Verify accreditation operation reported.
	 *
	 * @param moTO the mo to
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public AccreditationOperation verifyAccreditationOperationReported(MechanismOperationTO moTO)throws ServiceException{
		return beanService.verifyAccreditationOperationReported(moTO);
	}
	
	/**
	 * Verify accreditation security amortization coupon.
	 *
	 * @param moTO the mo to
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public Integer getSecurityType(String idPkSecurity)throws ServiceException{
	 Integer SecurityType= beanService.securityType(idPkSecurity);
 	  return SecurityType;
	}
	
	/**
	 * Verify accreditation security amortization coupon.
	 *
	 * @param moTO the mo to
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public Date verifyAmortizationCoupon(String idPkSecurity)throws ServiceException{
		
	  Map<String, Object> parameters = new HashMap<String, Object>();
 	  parameters.put("idSecurityPrm", idPkSecurity);
 	  parameters.put("statePrm", PaymentScheduleStateType.REGTERED.getCode());
 	  AmortizationPaymentSchedule amort = new AmortizationPaymentSchedule();
 	  amort=beanService.findObjectByNamedQuery(AmortizationPaymentSchedule.FIND_BY_SECURITY_AND_STATE, parameters);
 	  Long idAmortization=amort.getIdAmoPaymentSchedulePk();
 	  
 	  return beanService.verifyAmortizationCoupon(idAmortization);
	}
	
	/**
	 * Find block operation certification.
	 *
	 * @param accreditationDetailId the accreditation detail id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperation> findBlockOperationCertification(Long accreditationDetailId) throws ServiceException{

		return beanService.findBlockOperationCertification(accreditationDetailId);
	}
	
	/**
	 * Search accreditation detail by accreditation operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationDetail> searchAccreditationDetailByAccreditationOperation(Long accreditationOperationId)throws ServiceException{
		return beanService.searchAccreditationDetailByAccreditationOperation(accreditationOperationId);
	}
	
	/**
	 * Confirm accreditation request.
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM)
	public void confirmAccreditationRequest(AccreditationOperation accreditationOperationToChange, Integer accreditationState) throws ServiceException{
		Long accreditationOperationId = accreditationOperationToChange.getCustodyOperation().getIdCustodyOperationPk();
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
//		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
//		accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperationId);
//		accreditationOperationTO.setAccreditationState(accreditationState);
		accreditationOperationToChange.setAccreditationState(accreditationState);
//		accreditationOperationTO.setIdSignatureCertificateFk(accreditationOperationToChange.getIdSignatureCertificateFk());
		Correlative objCorrelative=new Correlative();
		objCorrelative.setCorrelativeCd(CorrelativeType.CAT_CODE_COR.getValue());
		
		//llamada a singleton
		Correlative correlative=custodyComponentSingleton.correlativeServiceBean(objCorrelative, CatType.CAT);
//		Integer actualCorrelative=correlative.getCorrelativeNumber()+1;
		Integer actualCorrelative=correlative.getCorrelativeNumber();
		
		accreditationOperationToChange.setCertificateNumber(actualCorrelative);
		/*** SETTING AUDIT ***/
		//accreditationOperationTO = this.setAudit(accreditationOperationTO, loggerUser);
		//accreditationOperationTO.setCertificateNumber(actualCorrelative);				
		// Get security code cat
		String strSecurityCodeCat = getSecurityCodeCat(accreditationOperationToChange, actualCorrelative);	
		strSecurityCodeCat = strSecurityCodeCat +"-"+ actualCorrelative.toString();
		if(Validations.validateIsNotNullAndNotEmpty(strSecurityCodeCat)){
			accreditationOperationToChange.setSecurityCodeCat(strSecurityCodeCat);
			//accreditationOperationTO.setSecurityCodeCat(strSecurityCodeCat);
		}
		
		//this.updateAccreditationOperation(accreditationOperationTO);
		beanService.update(accreditationOperationToChange);
		correlative.setCorrelativeNumber(actualCorrelative);
		correlativeService.update(correlative);				
		
		List<HolderAccountBalanceTO> accountBalanceMixedTOs = this.getAccountBalanceTOsFromMixed(accreditationOperationId);
				
		if(accreditationOperationToChange.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())){
			List<HolderAccountBalanceTO> accountBalanceTOs = this.getHolderAccountBalanceTOs(accreditationOperationId);	
			accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
																						 BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode(), 
																						 accountBalanceTOs,
																						 ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));
			CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
			custodyOperation.setConfirmUser(loggerUser.getUserName());
			custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
			custodyOperation.setState(AccreditationOperationStateType.CONFIRMED.getCode());
			this.beanService.update(custodyOperation);
		
		}else if(accreditationOperationToChange.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())){
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationToChange.getIdAccreditationOperationPk());				
			for(BlockOperation objBlockOperation : blockOperations){
				objBlockOperation.setIndAccreditation(BooleanType.YES.getCode());
				this.beanService.updateBlockOperation(objBlockOperation);
			}
		}else{
			
			if(Validations.validateListIsNotNullAndNotEmpty(accountBalanceMixedTOs)){
				//Calling component			
				accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
																							 BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode(), 
																							 accountBalanceMixedTOs,
																							 ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));					
			}
			CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
			custodyOperation.setConfirmUser(loggerUser.getUserName());
			custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
			custodyOperation.setState(AccreditationOperationStateType.CONFIRMED.getCode());
			this.beanService.update(custodyOperation);
			
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationToChange.getIdAccreditationOperationPk());
			if(blockOperations != null){
				for(BlockOperation objBlockOperation : blockOperations){
					objBlockOperation.setIndAccreditation(BooleanType.YES.getCode());
					this.beanService.updateBlockOperation(objBlockOperation);
				}
			}
			
			Integer state=accreditationOperationToChange.getAccreditationState();
			List<HolderAccountOperation> lstHAO= searchHolderAccountOperationToUpdate(accreditationOperationToChange);
			if(lstHAO!=null){					
				beanService.updateReportedOperation(lstHAO, state);
			}
		}
		
		/*
		 * CALLING THE CLIENT WEB SERVICE
		 */
		
		if(!accreditationOperationToChange.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())
				&& Validations.validateListIsNotNullAndNotEmpty(accountBalanceMixedTOs)){
			
			for(AccreditationDetail detail : beanService.searchAccreditationDetailByAccreditationOperation(accreditationOperationToChange.getIdAccreditationOperationPk())){
				if(detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
						detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					try {
						custodyServiceConsumer.sendBlockAccreditationOperationWebClient(accreditationOperationToChange, loggerUser);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
			
	}
	
	/**
	 * Metodo para confirmar una solicitud de CAT B.
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM)
	public void confirmAccreditationRequestB(AccreditationOperation accreditationOperationToChange, Integer accreditationState) throws ServiceException{
		Long accreditationOperationId = accreditationOperationToChange.getCustodyOperation().getIdCustodyOperationPk();
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		accreditationOperationToChange.setAccreditationState(accreditationState);
		Correlative objCorrelative = new Correlative();
		objCorrelative.setCorrelativeCd(CorrelativeType.CAT_CODE_COR.getValue());
		
		//llamada a singleton
		Correlative correlative = custodyComponentSingleton.correlativeServiceBean(objCorrelative, CatType.CATB);
		Integer actualCorrelative=correlative.getCorrelativeNumber();
		
		accreditationOperationToChange.setCertificateNumber(actualCorrelative);
		/*** SETTING AUDIT ***/
		
			String strSecurityCodeCat = getSecurityCodeCat(accreditationOperationToChange, actualCorrelative);	
			strSecurityCodeCat = strSecurityCodeCat +"-"+ actualCorrelative.toString();
			if(Validations.validateIsNotNullAndNotEmpty(strSecurityCodeCat)){
				accreditationOperationToChange.setSecurityCodeCat(strSecurityCodeCat);
			}
		
		//this.updateAccreditationOperation(accreditationOperationTO);
		beanService.update(accreditationOperationToChange);
		correlative.setCorrelativeNumber(actualCorrelative);
		correlativeService.update(correlative);		
		
		CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
		custodyOperation.setConfirmUser(loggerUser.getUserName());
		custodyOperation.setConfirmDate(CommonsUtilities.currentDate());
		custodyOperation.setState(AccreditationOperationStateType.CONFIRMED.getCode());
		this.beanService.update(custodyOperation);
	}
	
	/**
	 * Metodo para Rechazar una solicitud de CAT B.
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT)
	public void rejectAccreditationRequestB(AccreditationOperation accreditationOperationToChange, Integer accreditationState) throws ServiceException{
		Long accreditationOperationId = accreditationOperationToChange.getCustodyOperation().getIdCustodyOperationPk();
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
		accreditationOperationToChange.setAccreditationState(accreditationState);
		beanService.update(accreditationOperationToChange);

		CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
		custodyOperation.setRejectUser(loggerUser.getUserName());
		custodyOperation.setRejectDate(CommonsUtilities.currentDate());
		custodyOperation.setRejectMotive(accreditationOperationToChange.getIdRejectMotive());
		custodyOperation.setState(AccreditationOperationStateType.REJECTED.getCode());
		this.beanService.update(custodyOperation);
	}
	
	
	/**
	 * Confirm accreditation status.
	 *
	 * @param accreditationOperations the accreditation operations
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM)
	public void confirmAccreditationRequestMassive(List<AccreditationOperation> accreditationOperations, Integer accreditationState) throws ServiceException{
		for(AccreditationOperation accreditationOperation : accreditationOperations){
			accreditationOperation.setAccreditationDetails(new ArrayList<AccreditationDetail>());
			
			if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
				confirmAccreditationRequest(accreditationOperation, accreditationState);
			}else{
				confirmAccreditationRequestB(accreditationOperation, accreditationState);
			}
		}
	}
	
	
	/**
	 * Cancel accreditation status.
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT)
	public void rejectAccreditationRequest(AccreditationOperation accreditationOperationToChange, Integer accreditationState) throws ServiceException{
		Long accreditationOperationId = accreditationOperationToChange.getCustodyOperation().getIdCustodyOperationPk();
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		
//		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
//		accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperationId);
//		accreditationOperationTO.setAccreditationState(accreditationState);
//		accreditationOperationTO.setIdRejectMotiveFk(accreditationOperationToChange.getIdRejectMotive());
		/*** SETTING AUDIT ***/
//		accreditationOperationTO = this.setAudit(accreditationOperationTO, loggerUser);
//		if(accreditationOperationToChange.getRejectMotiveOther() != null){
//			accreditationOperationTO.setRejectMotiveOther(accreditationOperationToChange.getRejectMotiveOther());
//		}
		accreditationOperationToChange.setAccreditationState(accreditationState);
//		this.updateAccreditationOperation(accreditationOperationTO);
		beanService.update(accreditationOperationToChange);
		if(accreditationOperationToChange.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())){	
			List<HolderAccountBalanceTO> accountBalanceTOs = this.getHolderAccountBalanceTOs(accreditationOperationId);
			//Calling component			
			accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
																						 BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT.getCode(), 
																						 accountBalanceTOs,
																						 ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));
		}else if(accreditationOperationToChange.getCertificationType().equals(CertificationType.REPORTED_BALANCE.getCode())){				
			List<HolderAccountBalanceTO> accountBalanceTOs = this.getAccountBalanceTOsFromMixed(accreditationOperationId);
			if(accountBalanceTOs.size() >0){
				//Calling component			
				accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
																							 BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT.getCode(), 
																							 accountBalanceTOs,
																							 ParameterOperationType.SECURITIES_ACCREDITATION.getCode()));
			}				
		}			
		CustodyOperation custodyOperation = this.beanService.find(CustodyOperation.class, accreditationOperationId);
		custodyOperation.setRejectUser(loggerUser.getUserName());
		custodyOperation.setRejectDate(CommonsUtilities.currentDate());
		custodyOperation.setRejectMotive(accreditationOperationToChange.getIdRejectMotive());
		custodyOperation.setState(AccreditationOperationStateType.REJECTED.getCode());
		this.beanService.update(custodyOperation);
	}
	
	/**
	 * Cancel accreditation status.
	 *
	 * @param accreditationOperations the accreditation operations
	 * @param accreditationState the accreditation state
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT)
	public void rejectAccreditationRequestMassive(List<AccreditationOperation> accreditationOperations, Integer accreditationState) throws ServiceException{
		for(AccreditationOperation accreditationOperation : accreditationOperations){
			accreditationOperation.setAccreditationDetails(new ArrayList<AccreditationDetail>());

			if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
				rejectAccreditationRequest(accreditationOperation, accreditationState);
			}else{
				rejectAccreditationRequestB(accreditationOperation, accreditationState);
			}
		}
	}
	
	
	/**
	 * Un block accreditation.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK)	
	public void unBlockAccreditation(AccreditationOperation accreditationOperationToUnblock) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock());
        
//		validateAccreditationOperationUnblocked(accreditationOperationToUnblock, AccreditationOperationStateType.CONFIRMED.getCode());
		
		Long accreditationOperationId = accreditationOperationToUnblock.getCustodyOperation().getIdCustodyOperationPk();
		List<HolderAccountBalanceTO> accountBalanceMixedTOs = this.getAccountBalanceTOsFromMixed(accreditationOperationId);
		
		if(accreditationOperationToUnblock.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())){
			this.executeAccountsComponent(accreditationOperationId, BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK.getCode(),
									  	  ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode());
			
		}else if(accreditationOperationToUnblock.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())){
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationId);
			for(BlockOperation objBlockOperation : blockOperations){
				objBlockOperation.setIndAccreditation(BooleanType.NO.getCode());
				this.beanService.updateBlockOperation(objBlockOperation);
			}
		}else{
			if(accountBalanceMixedTOs.size() >0){
				accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
						BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK.getCode(), accountBalanceMixedTOs,
						   	ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode()));
			}
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationId);
			if(blockOperations != null){
				for(BlockOperation objBlockOperation : blockOperations){
					objBlockOperation.setIndAccreditation(BooleanType.NO.getCode());
					this.beanService.updateBlockOperation(objBlockOperation);
				}
			}
			List<HolderAccountOperation> lstHAO= searchHolderAccountOperationToUpdate(accreditationOperationToUnblock);
			Integer state= accreditationOperationToUnblock.getAccreditationState();
			if(lstHAO != null){
				beanService.updateReportedOperation(lstHAO, state);
			}
		}
		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperationId);
		accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.UNBLOCKED.getCode());
//		accreditationOperationTO.setUnblockMotive(AccreditationOperationUnblockType.OTHERS.getCode());
		accreditationOperationTO.setUnblockMotive(accreditationOperationToUnblock.getUnblockMotive());
		accreditationOperationTO.setUnblockObservation(accreditationOperationToUnblock.getUnblockObservation());
		/*** SETTING AUDIT ***/
		accreditationOperationTO = this.setAudit(accreditationOperationTO, loggerUser);
		/*** UPDATING STATUS TO UNBLOCKED ***/
		this.beanService.updateAccreditationOperation(accreditationOperationTO);
		
		/*
		 * CALLING THE CLIENT WEB SERVICE
		 */
		
		if(!accreditationOperationToUnblock.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())
				&& Validations.validateListIsNotNullAndNotEmpty(accountBalanceMixedTOs)){
			
			for(AccreditationDetail detail : beanService.searchAccreditationDetailByAccreditationOperation(accreditationOperationToUnblock.getIdAccreditationOperationPk())){
				if(detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
						detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					try {
						custodyServiceConsumer.sendUnblockAccreditationOperationWebClient(accreditationOperationToUnblock, loggerUser);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	
	/**
	 * Un block accreditation.
	 *
	 * @param accreditationOperations the accreditation operations
	 * @throws ServiceException the service exception
	 */
	public void unBlockAccreditationMassive(List<AccreditationOperation> accreditationOperations) throws ServiceException{
		for(AccreditationOperation accreditationOperation : accreditationOperations){
			unBlockAccreditation(accreditationOperation);
		}
	}
	
	
	/**
	 * Un block accreditation process.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @throws ServiceException the service exception
	 */
	public void unBlockAccreditationProcess(AccreditationOperation accreditationOperationToUnblock, boolean blSendDpf) throws ServiceException{ 
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		Long accreditationOperationId = accreditationOperationToUnblock.getCustodyOperation().getIdCustodyOperationPk();
		if(accreditationOperationToUnblock.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())){
			this.executeAccountsComponent(accreditationOperationId, BusinessProcessType.ACCREDITATION_CERTIFICATE_AUTOMATIC_UNBLOCK.getCode(), 
									  	  ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode());
		}else if(accreditationOperationToUnblock.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())){			
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationId);			
			for(BlockOperation objBlockOperation : blockOperations){
				objBlockOperation.setIndAccreditation(BooleanType.NO.getCode());
				this.beanService.updateBlockOperation(objBlockOperation);
			}
		}else{			
			List<HolderAccountBalanceTO> accountBalanceTOs = this.getAccountBalanceTOsFromMixed(accreditationOperationId);
			if(accountBalanceTOs.size() >0){
				accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
						BusinessProcessType.ACCREDITATION_CERTIFICATE_AUTOMATIC_UNBLOCK.getCode(), accountBalanceTOs,
						   	ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode()));							
			}			
			List<BlockOperation> blockOperations = this.searchBlockOperations(accreditationOperationId);
			if(blockOperations != null){
				for(BlockOperation objBlockOperation : blockOperations){
					objBlockOperation.setIndAccreditation(BooleanType.NO.getCode());
					this.beanService.updateBlockOperation(objBlockOperation);
				}	
			}				
			List<HolderAccountOperation> lstHAO= searchHolderAccountOperationToUpdate(accreditationOperationToUnblock);
			Integer state= accreditationOperationToUnblock.getAccreditationState();
			if(lstHAO != null){
				beanService.updateReportedOperation(lstHAO, state);
			}
		}
		AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperationId);
		accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.UNBLOCKED.getCode());
		accreditationOperationTO.setUnblockMotive(AccreditationOperationUnblockType.OTHERS.getCode());
		accreditationOperationTO.setUnblockObservation(accreditationOperationToUnblock.getUnblockObservation());
		accreditationOperationTO.setRealExpirationDate(CommonsUtilities.currentDateTime());
		/*** SETTING AUDIT ***/
		accreditationOperationTO = this.setAudit(accreditationOperationTO, loggerUser);
		/*** UPDATING STATUS TO UNBLOCKED ***/
		this.beanService.updateAccreditationOperation(accreditationOperationTO);
		
		List<UnblockAccreditationOperation> lstUnblockAccreditationOperation = beanService.getUnblockAccreditationOperation(accreditationOperationTO);		
		if(Validations.validateListIsNotNullAndNotEmpty(lstUnblockAccreditationOperation)){
			for(UnblockAccreditationOperation objUnblockAccreditationOperation : lstUnblockAccreditationOperation){
				AccreditationOperationTO objAccreditationOperationTO = new AccreditationOperationTO();
				objAccreditationOperationTO.setIdUnblockAccreditationPk(objUnblockAccreditationOperation.getIdUnblockAccreditationPk());
				objAccreditationOperationTO.setAccreditationState(UnblockAccreditationOperationStateType.CONFIRMED.getCode());
				objAccreditationOperationTO = this.setAudit(objAccreditationOperationTO, loggerUser);
				this.beanService.updateUnblockAccreditationRequestOperation(objAccreditationOperationTO);
			}
		}				
		
		/*
		 * CALLING THE CLIENT WEB SERVICE
		 */
		if(!accreditationOperationToUnblock.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode()) && blSendDpf){
			
			for(AccreditationDetail detail : beanService.searchAccreditationDetailByAccreditationOperation(accreditationOperationToUnblock.getIdAccreditationOperationPk())){
				if(detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
						detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					try {
						custodyServiceConsumer.sendUnblockAccreditationOperationWebClient(accreditationOperationToUnblock, loggerUser);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
	
	
	/**
	 * Un block accreditation automatic.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_AUTOMATIC_UNBLOCK)	
	public void unBlockAccreditationAutomatic(AccreditationOperation accreditationOperationToUnblock) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		if(loggerUser.getUserAction().getIdPrivilegeUnblock()!=null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeUnblock()); 
		}
		unBlockAccreditationProcess(accreditationOperationToUnblock, Boolean.TRUE);
	}
	
	
	/**
	 * Execute accounts component.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @param businessProcessType the business process type
	 * @param parameterOperationType the parameter operation type
	 * @throws ServiceException the service exception
	 */
	public void executeAccountsComponent(Long accreditationOperationId, Long businessProcessType, Long parameterOperationType) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = this.getHolderAccountBalanceTOs(accreditationOperationId);
		
		accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(accreditationOperationId,
																							   businessProcessType, 
																							   	 accountBalanceTOs,
																							parameterOperationType));
	}
	
	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	public AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType){
		// ComponentTO Object
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
		
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(0);
		return objAccountComponent;
	}
	
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(Long accreditationOperationId) throws ServiceException{
		//accreditationDetail list Object
		List<AccreditationDetail> accreditationDetailList = this.beanService.searchAccreditationDetailByOperation(accreditationOperationId);	
		
		if(Validations.validateListIsNullOrEmpty(accreditationDetailList)){
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
		//HolderAccountBalanceTO list
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();		
		for(AccreditationDetail acreditationDetail : accreditationDetailList){
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(acreditationDetail.getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(acreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(acreditationDetail.getSecurity().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(acreditationDetail.getTotalBalance());		
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(AccreditationMarketFact accreditationMarketFact : acreditationDetail.getAccreditationMarketFactList()){
				if(accreditationMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
					MarketFactAccountTO mark = new MarketFactAccountTO();
					mark.setMarketDate(accreditationMarketFact.getMarketDate());
					mark.setMarketRate(accreditationMarketFact.getMarketRate());
					mark.setMarketPrice(accreditationMarketFact.getMarketPrice());
					mark.setQuantity(accreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mark);
				}
			}			
			accountBalanceTOs.add(holderAccountBalanceTO);
		}				
		return accountBalanceTOs;
	}
	
	
	/**
	 * Sets the audit.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @param loggerUser the logger user
	 * @return the accreditation operation to
	 */
	public AccreditationOperationTO setAudit(AccreditationOperationTO accreditationOperationTO, LoggerUser loggerUser){
		/*** SETTING AUDIT ***/
        accreditationOperationTO.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
        accreditationOperationTO.setLastModifyDate(loggerUser.getAuditTime());
        accreditationOperationTO.setLastModifyIp(loggerUser.getIpAddress()); 
        accreditationOperationTO.setLastModifyUser(loggerUser.getUserName());
        return accreditationOperationTO;
	}
	
	/**
	 * * BATCH METHOD **.
	 *
	 * @param idAccreditationPk the id accreditation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperation> searchBlockOperations(Long idAccreditationPk) throws ServiceException{
		return this.beanService.searchBlockOperations(idAccreditationPk);
	}
	
	/**
	 * Search holder account operation to update.
	 *
	 * @param objAO the obj ao
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountOperation> searchHolderAccountOperationToUpdate(AccreditationOperation objAO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return this.beanService.searchHolderAccountOperationToUpdate(objAO, loggerUser);
	}
	
	/**
	 * Search block entity.
	 *
	 * @param idAccreditationOperationPk the id accreditation operation pk
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	public BlockEntity searchBlockEntity(Long idAccreditationOperationPk) throws ServiceException{
		return this.beanService.searchBlockEntity(idAccreditationOperationPk);
	}

	/**
	 * Automatic accreditation cancelations.
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticAccreditationCancelations(Date finalDate) throws ServiceException{								
		String COMMENTS_REJECT = "REVERSION NOCTURNA";		
		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(AccreditationOperationStateType.REGISTRED.getCode());					
		List<AccreditationOperation> accreditationOperation = beanService.accreditationAvailableConsultByRevert(finalDate, lstStates);				
		for (AccreditationOperation objAccreditationOperation : accreditationOperation){	
			objAccreditationOperation.setIdRejectMotive(AccreditationOperationRejectMotiveType.OTHERS.getCode());
			objAccreditationOperation.setRejectMotiveOther(COMMENTS_REJECT);
			if (objAccreditationOperation.getAccreditationState().equals(AccreditationOperationStateType.REGISTRED.getCode())) {				
				rejectAccreditationRequest(objAccreditationOperation, AccreditationOperationStateType.REJECTED.getCode());
			}					
		}
		
	}
	
	
	/**
	 * Gets the holder account balance t os.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> getAccountBalanceTOsFromMixed(Long accreditationOperationId) throws ServiceException{
		List<AccreditationDetail> accreditationDetailList = this.beanService.searchAccreditationDetailByOperation(accreditationOperationId);
		log.info("size accreditationDetailList: " + accreditationDetailList.size());
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();		
		for(AccreditationDetail acreditationDetail : accreditationDetailList){
			log.info("acreditationDetail.getAvailableBalance(): acreditationDetail.getAvailableBalance()");
			if(acreditationDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdHolderAccount(acreditationDetail.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(acreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdSecurityCode(acreditationDetail.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(acreditationDetail.getAvailableBalance());		
				holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for(AccreditationMarketFact accreditationMarketFact : acreditationDetail.getAccreditationMarketFactList()){
					MarketFactAccountTO mark = new MarketFactAccountTO();
					mark.setMarketDate(accreditationMarketFact.getMarketDate());
					mark.setMarketRate(accreditationMarketFact.getMarketRate());
					mark.setMarketPrice(accreditationMarketFact.getMarketPrice());
					mark.setQuantity(accreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mark);
				}			
				accountBalanceTOs.add(holderAccountBalanceTO);
			}			
		}				
		return accountBalanceTOs;
	}
	
	/**
	 * Reject accreditation.
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT)
	public boolean rejectAccreditation(AccreditationOperation accreditationOperationToChange, Integer accreditationState) throws ServiceException{		
		return true;				
	}
	
	/**
	 * Find accreditation detail mixed.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationDetail> findAccreditationDetailMixed(Long accreditationOperationId)throws ServiceException{
		return beanService.searchhAccreditationDetailMixed(accreditationOperationId);
	}
	
	/**
	 * Find reported operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<MechanismOperationTO> findReportedOperation(Long accreditationOperationId)throws ServiceException{
		return beanService.findReportedOperation(accreditationOperationId);
	}
	
	/**
	 * Document file.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the byte[]
	 * @throws ServiceException the service exception
	 */
	public byte [] documentFile(Long accreditationOperationId)throws ServiceException{
		return beanService.documentFile(accreditationOperationId);
	}
	
	/**
	 * Issuer list.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Issuer> issuerList()throws ServiceException{
		return beanService.issuerList();
	}
	
	/**
	 * Identify block accreditation operations to send.
	 *
	 * @param sendDate the send date
	 * @throws ServiceException the service exception
	 */
	public void identifyBlockAccreditationOperationsToSend(Date sendDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		/** OBTENEMOS LA LISTA DE OP. DE BLOQUEO DE ACREDITACION A ENVIAR **/
		List<AccreditationOperation> lstBlockAccreditationOperation = beanService.getListBlockAccreditationOperationWebService(sendDate,true);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstBlockAccreditationOperation)){
			/** CALLING THE CLIENT WEB SERVICE **/
			for (AccreditationOperation objBlockAccreditationOperation : lstBlockAccreditationOperation){
				custodyServiceConsumer.sendBlockAccreditationOperationWebClient(objBlockAccreditationOperation, loggerUser);
			}
		}
	}
	
	/**
	 * Identify unblock accreditation operations to send.
	 *
	 * @param sendDate the send date
	 * @throws ServiceException the service exception
	 */
	public void identifyUnblockAccreditationOperationsToSend(Date sendDate) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		/** OBTENEMOS LA LISTA DE OP. DE DESBLOQUEO DE ACREDITACION A ENVIAR **/
		List<AccreditationOperation> lstUnblockAccreditationOperation = beanService.getListBlockAccreditationOperationWebService(sendDate,false);
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstUnblockAccreditationOperation)){
			/** CALLING THE CLIENT WEB SERVICE **/
			for (AccreditationOperation objUnblockAccreditationOperation : lstUnblockAccreditationOperation){
				custodyServiceConsumer.sendUnblockAccreditationOperationWebClient(objUnblockAccreditationOperation, loggerUser);
			}
		}
	}
	
	/**
	 * Gets the security code cat.
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param intCertificateNumber the certificate number
	 * @return the security code cat
	 */
	private String getSecurityCodeCat(AccreditationOperation accreditationOperationToChange, Integer intCertificateNumber) {
		String strSecurityCodeCat = GeneralConstants.EMPTY_STRING;
		AccreditationOperation objAccreditationOperation = beanService.find(AccreditationOperation.class, accreditationOperationToChange.getIdAccreditationOperationPk());
		// Generate security code cat (11)		
		String documentNumber = objAccreditationOperation.getAccreditationDetails().get(0).getHolder().getDocumentNumber();
		Integer accountNumber = objAccreditationOperation.getAccreditationDetails().get(0).getHolderAccount().getAccountNumber();
		String idSecurityPk = objAccreditationOperation.getAccreditationDetails().get(0).getSecurity().getIdSecurityCodePk();							
		try {			
			String certicateNumber11 = CommonsUtilities.obtenerModulo11(intCertificateNumber.toString());
			String documentNumber11 = CommonsUtilities.obtenerModulo11(documentNumber);
			String accountNumber11 = CommonsUtilities.obtenerModulo11(accountNumber.toString());
			String securityPk11 = CommonsUtilities.obtenerModulo11(idSecurityPk);
			String strIssuanceDate11 = CommonsUtilities.obtenerModulo11(objAccreditationOperation.getIssuanceDate());			
			strSecurityCodeCat = certicateNumber11 + documentNumber11 + accountNumber11 + securityPk11 + strIssuanceDate11;
			String globalCode = CommonsUtilities.obtenerModulo11(strSecurityCodeCat);
			strSecurityCodeCat = strSecurityCodeCat + globalCode;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strSecurityCodeCat;
	}
	
	/**
	 * Accreditation consult verification Cat.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationDetail> getAccreditationOperationForVerificationCat(AccreditationOperationTO accreditationOperationTO) throws ServiceException { 
		return beanService.getAccreditationOperationForVerificationCat(accreditationOperationTO);
	}
	
	/**
	 * Verify list digital signature.
	 *
	 * @param typeSignatureOne the type signature one
	 * @param typeSignatureTwo the type signature two
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean VerifyListDigitalSignature(Integer typeSignatureOne,Integer typeSignatureTwo) throws ServiceException { 
		return beanService.getListDigitalSignature(typeSignatureOne,typeSignatureTwo);
	}
	
	/**
	 * Expiration accreditation by DPF.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @throws ServiceException the service exception
	 */
	public void expirationAcreditationByDpf(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		List<AccreditationOperation> accreditationOperationLst = beanService.searchAccreditationOperationByUnblock(accreditationOperationTO);
		if(Validations.validateListIsNotNullAndNotEmpty(accreditationOperationLst)){
			for(AccreditationOperation tmpAccreditationOperation : accreditationOperationLst){
				unBlockAccreditationProcess(tmpAccreditationOperation, Boolean.FALSE);
			}
		}		
	}
	
	/**
	 * Verify accreditation operation certificate by registry.
	 *
	 * @param bo the bo
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationOperation> verifyAccreditationOperationByRegistry(HolderAccountBalanceExp holderAccountBalanceExp,Holder holder)throws ServiceException{
		return beanService.verifyAccreditationOperationByRegistry(holderAccountBalanceExp,holder);
	}
	
	/**
	 * search accreditation operation by unblock.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationOperation> searchAccreditationOperationByUnblock(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		List<AccreditationOperation> accreditationOperationLst = beanService.searchAccreditationOperationByUnblock(accreditationOperationTO);
		return accreditationOperationLst;		
	}
	
	/**
	 * Expiration Accreditation
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @throws ServiceException the service exception
	 */	
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_CONFIRM)
	public List<AccreditationDetail> expirationAcreditationFromAffectation(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		List<AccreditationDetail> lstAccreditationDetail = new ArrayList<>();
		List<AccreditationOperation> accreditationOperationLst = beanService.searchAccreditationOperationByUnblock(accreditationOperationTO);
		if(Validations.validateListIsNotNullAndNotEmpty(accreditationOperationLst)){
			for(AccreditationOperation tmpAccreditationOperation : accreditationOperationLst){
				unBlockAccreditationProcess(tmpAccreditationOperation, Boolean.FALSE);
				for(AccreditationDetail detail : beanService.searchAccreditationDetailByAccreditationOperation(tmpAccreditationOperation.getIdAccreditationOperationPk())){
					lstAccreditationDetail.add(detail);
					if(detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
							detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
						try {
							/* CALLING THE CLIENT WEB SERVICE */
							custodyServiceConsumer.sendUnblockAccreditationOperationWebClient(tmpAccreditationOperation, loggerUser);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return lstAccreditationDetail;
	}
}
