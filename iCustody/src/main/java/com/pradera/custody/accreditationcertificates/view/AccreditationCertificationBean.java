package com.pradera.custody.accreditationcertificates.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.InstitutionType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.HolidayQueryServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.component.helperui.to.SecurityFinancialDataHelpTO;
import com.pradera.core.component.helperui.view.SecurityHelpBean;
import com.pradera.core.component.operation.to.MarketFactBalanceTO;
import com.pradera.core.component.operation.to.MechanismOperationTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.accreditationcertificates.facade.AccreditationCertificatesServiceFacade;
import com.pradera.custody.accreditationcertificates.service.AccreditationCertificatesServiceBean;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.accreditationcertificates.to.FileAccreditationTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDeliveryInformation;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationMarketFact;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperationCertification;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.accreditationcertificates.ReportAccreditation;
import com.pradera.model.custody.affectation.type.AffectationType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationMotive;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.AccreditationOperationUnblockType;
import com.pradera.model.custody.type.AccreditationPaymentWayType;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.custody.type.CatType;
import com.pradera.model.custody.type.CertificationType;
import com.pradera.model.custody.type.DigitalSignatureType;
import com.pradera.model.custody.type.PetitionerType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccreditationCertificationBean.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , Feb 14, 2013
 */
@DepositaryWebBean
@LoggerCreateBean
public class AccreditationCertificationBean extends GenericBaseBean implements Serializable {
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3935627856379963817L;

	/** The accreditation facade bean. */	
	@Inject
	private AccreditationCertificatesServiceFacade accreditationfacadeBean;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The security help bean. */
	@Inject
	SecurityHelpBean securityHelpBean;
	
	/** The general parameters facade. */
	@EJB
    GeneralParametersFacade generalParametersFacade;
	
	/** The general parameters service. */
	@EJB
	private ParameterServiceBean generalParametersService;
	
	/** The holiday query service bean. */
	@EJB
	private HolidayQueryServiceBean holidayQueryServiceBean;
	
	/** The bean service. */
	@Inject
	private AccreditationCertificatesServiceBean beanService;
	
	/** The batch service facade. */
	@EJB
	 BatchProcessServiceFacade batchServiceFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** Instantiates a new accreditation certification bean.	 */
	@Inject
	private transient PraderaLogger log;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The market service bean. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
//	@EJB 
//	private IssuerfacadeService issuerfacadeService;
	
	/** The accreditation detail. */
	private AccreditationDetail accreditationDetail = new AccreditationDetail();

	/** The accreditation operation. */
	private AccreditationOperation accreditationOperation;

//	private AccreditationOperationTO searchAccreditationOperation;
	
	/** The block operation. */
	private BlockRequest blockRequest= new BlockRequest();

	/** The block operation certification. */
	private BlockOperationCertification blockOperationCertification = new BlockOperationCertification();

	/** The participant list. */
	private List<Participant> participantList;

	/** The holder accounts. */
	private List<HolderAccount> holderAccounts;
	
	/** The holder accounts issuer. */
	private List<HolderAccount> holderAccountsIssuer;
	
	/** The accreditation details. */
	private List<AccreditationDetail> accreditationDetails = new ArrayList<AccreditationDetail>();
	
	/** The block type. */
	private List<ParameterTable> blockType;

	/** The petitioner type. */
	private List<ParameterTable> petitionerType;
	
	/** The CAT type. */
	private List<ParameterTable> catType;

	/**  The issuer. */
	private List<Issuer> listIssuer;
	
	/** The state list. */
	private List<ParameterTable> stateList;

	/** The destination deliver list. */
	private List<ParameterTable> destinationDeliverList;
	
	/** The reject motive list. */
	private List<ParameterTable> rejectMotiveList;
	
	/** The unblock motive list. */
	private List<ParameterTable> unblockMotiveList;
	
	/** The signature certificate list. */
	private List<ParameterTable> signatureCertificateList;

	/** The temp account balance. */
	private List<HolderAccountBalance> tempAccountBalance;
	
	/** The block operation temp list. */
	private List<BlockOperation> blockOperationTempList; 
	
	/** The lst reported balance. */
	private List<MechanismOperationTO> lstReportedBalance;
	
	/** The balance type. */
	private List<ParameterTable> balanceType;
	
	/** The block operation detail list. */
	private List<BlockOperation> blockOperationList = new ArrayList<BlockOperation>();
	
	/** The reported operation to list. */
	private List<MechanismOperationTO> reportedOperationTOList = new ArrayList<MechanismOperationTO>();
	
	/** The now time. */
	private Date nowTime = new Date();

	/** The expiration date. */
	private Date expirationDate;

	/** The ha field. */
	private HolderAccount haField = new HolderAccount();

	/** The accredited balance. */
	private BigDecimal accreditateBalance;
	
	/** The issuance cert file name display. */
	private String issuanceCertFileNameDisplay;
	
	/** The issuer name. */
	private String issuerName;
	
	/** The message register. */
	private String messageRegister = null;
	
	/** The message balance. */
	private String messageBalance = null;
	
	/** The participant. */
	private Participant participant = new Participant();

	/** The holder. */
	private Holder holder = new Holder();

	/** The block balance. */
	private Integer blockBalance;

	/** The obj accreditation detail. */
	private AccreditationDetail objAccreditationDetail = null;

	/** The holder account balances. */
	private GenericDataModel<HolderAccountBalance> holderAccountBalances;
	
	/** The lst reported balance. */
	private List<HolderAccountBalanceExp> lstHolderAccountBalanceExp;
	
	/** The holder account balances. */
	private GenericDataModel<HolderAccountBalanceExp> holderAccountBalancesExp;
	
	/** The reported balance to. */
	private GenericDataModel<MechanismOperationTO> reportedBalanceTO;

	/** The block operation details. */
	private GenericDataModel<BlockOperation> blockOperations;
	
	/** The accreditation consult list. */
	private GenericDataModel<AccreditationOperation> accreditationOperationDataModel;

	/** The ao selection. */
	private AccreditationOperation accreditationOperationSelection;
	
	/** The ao selection. */
	private List<AccreditationOperation> listAccreditationOperationSelection;
	
	/** The file. */
	private UploadedFile file;

	/** The file download. */
	private transient StreamedContent fileDownload; 
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The registered type code. */
	private Integer registeredTypeCode=AccreditationOperationStateType.REGISTRED.getCode(); 
	
	/** The rejected type code. */
	private Integer rejectedTypeCode=AccreditationOperationStateType.REJECTED.getCode(); 
	
	/** The confirmeded type code. */
	private Integer confirmededTypeCode=AccreditationOperationStateType.CONFIRMED.getCode(); 
	
	/** The unblocked type code. */
	private Integer unblockedTypeCode=AccreditationOperationStateType.UNBLOCKED.getCode(); 
	
	/** The holder closed state type. */
	private Integer holderClosedStateType = HolderAccountStatusType.CLOSED.getCode();
	
	/** The disable participant selection. */
	private Boolean disableParticipantSelection;	
	
	/** The disable balance type. */
	private Boolean disableBalanceType			= Boolean.FALSE;
	
	/** The render blocked type. */
	private Boolean renderBlockedType			= Boolean.FALSE;
	
	/** The render available type. */
	private Boolean renderAvailableType			= Boolean.FALSE;
	
	/** The render request fail. */
	private Boolean renderRequestFail			= Boolean.FALSE;
	
	/** The render after selected motive. */
	private Boolean renderAfterSelectedMotive	= Boolean.FALSE;
	
	/** The render observations. */
	private Boolean renderObservations			= Boolean.FALSE;
	
	/** The confirm button selected. */
	private Boolean confirmButtonSelected 		= Boolean.FALSE; 
	
	/** The reject button selected. */
	private Boolean rejectButtonSelected 		= Boolean.FALSE; 
	
	/** The unblock button selected. */
	private Boolean unblockButtonSelected 		= Boolean.FALSE; 
	
	/** The participant logged is blocked. */
	private Boolean participantLoggedIsBlocked 	= Boolean.FALSE;
	
	/** The sustention disabled. */
	private Boolean sustentionDisabled		 	= Boolean.FALSE;
	
	/** The is issuer. */
	private Boolean isIssuer		 			= Boolean.FALSE;
	
	/** The flag exist participant dpf. */
	private Boolean flagExistParticipantDPF		= Boolean.FALSE;
	
	/** The flag day evaluate */
	private Boolean validityDaysBoo			= Boolean.FALSE;

	/**  Petitioner codes *. */
	private Integer participantPetitioner 	= PetitionerType.PARTICIPANT.getCode();
	
	/** The creditor petitioner. */
	private Integer creditorPetitioner 		= PetitionerType.CREDITOR.getCode();
	
	/** The holder petitioner. */
	private Integer holderPetitioner 		= PetitionerType.HOLDER.getCode();
	
	/** The inheritor petitioner. */
	private Integer inheritorPetitioner 	= PetitionerType.INHERITOR.getCode();
	
	/** The attorney petitioner. */
	private Integer attorneyPetitioner 		= PetitionerType.ATTORNEY.getCode();
	
	/** The issuer petitioner. */
	private Integer issuerPetitioner 		= PetitionerType.ISSUER.getCode();

	/** The other unblock motive. */
	private Integer otherUnblockMotive 		= AccreditationOperationUnblockType.OTHERS.getCode();
	
	/**  Balance type codes *. */
	private Integer blockedBalanceCode 	 = CertificationType.BLOCKED_BALANCE.getCode();
	
	/** The available balance code. */
	private Integer availableBalanceCode = CertificationType.AVAILABLE_BALANCE.getCode();
	
	/** The reported balance code. */
	private Integer reportedBalanceCode  = CertificationType.REPORTED_BALANCE.getCode();
	
	/**  Variables estaticas *. */
	private static final String CONFIRM 	 = "confirm";
	
	/** The Constant REJECT. */
	private static final String REJECT 	     = "reject";
	
	/** The Constant UNBLOCK. */
	private static final String UNBLOCK 	 = "unblock";
	
	/** The Constant GENERATE. */
	private static final String GENERATE 	 = "generate";
	
	/** The Constant DELIVER. */
	private static final String DELIVER 	 = "deliver";
	
	/** The block operation selected. */
	private BlockOperation blockOperationSelected;
	
	/** The uploaded file. */
	private UploadedFile uploadedFile;
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/** The accreditation delivery information. */
	private AccreditationDeliveryInformation accreditationDeliveryInformation;
	
	/** The sec. */
	private Security sec = new Security();
	
	/** The security. */
	private Security security;
	
	/** The option selected one radio. */
	private Integer optionSelectedOneRadio;
	
	/** The currency. */
	private Integer currency;
	
	/** The security class. */
	private Integer securityClass;
	
	/** The holder account. */
	private HolderAccount holderAccount;
	
	/** The lst accre mf. */
	List<AccreditationMarketFact> lstAccreMF = new ArrayList<AccreditationMarketFact>();
	
	/** The list motives. */
	private List<ParameterTable> listMotives;
	
	/** The list currency. */
	private List<ParameterTable> listCurrency;

	/** The list security class search. */
	private List<ParameterTable> listSecurityClassSearch;
	
	/** The list file accreditation to. */
	private List<FileAccreditationTO> listFileAccreditationTO;
	
	/** The lst security class. */
	private List<ParameterTable> lstSecurityClass;
	
	/** The lst accreditation delivery information. */
	private List<AccreditationDeliveryInformation> lstAccreditationDeliveryInformation;
	
	/** The lst accreditation operation to deliver. */
	private List<AccreditationOperationTO> lstAccreditationOperationTODeliver;
	
	/** The lst accreditation operation to delivers. */
	private List <AccreditationDeliveryInformation> lstAccreditationOperationTODelivers;
	
	/** The listid accreditation operation pk. */
	private String listidAccreditationOperationPk;
	
	/** The id reject motive operations selected. */
	private Integer idRejectMotiveOperationsSelected;
	
	/** The reject motive other operations selected. */
	private String rejectMotiveOtherOperationsSelected;
	
	private HolderAccountBalanceExp holderAccountBalanceExpTemp;
	
	/**
	 * Instantiates a new accreditation certification bean.
	 */
	public AccreditationCertificationBean() {}

	/**
	 * Inits .
	 */
	@PostConstruct	
	public void init() {	
		holderAccount= new HolderAccount();
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		optionSelectedOneRadio = Integer.valueOf(0);
		accreditationOperationSelection = new AccreditationOperation();
		//seleccion multiple
		listAccreditationOperationSelection = new ArrayList<>();
		//seleccion multiple
		
		lstAccreditationDeliveryInformation = new ArrayList<AccreditationDeliveryInformation>();
		// Setting participant list
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		participantList = new ArrayList<Participant>();
		security = new Security();
		accreditationDeliveryInformation = new AccreditationDeliveryInformation();
		lstSecurityClass = new ArrayList<ParameterTable>();
		listSecurityClassSearch = new ArrayList<ParameterTable>();
		listCurrency = new ArrayList<ParameterTable>();
		try {			
			participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
			// Setting Block type list from parameter List
			this.setBlockType(accreditationfacadeBean.getParameterList(MasterTableType.TYPE_AFFECTATION));
			// Setting States list from parameterlist
			this.setStateList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_CERTIFICATE_STATE));
			// Setting Petitioner Type from parameter list
			this.setPetitionerType(accreditationfacadeBean.getParameterList(MasterTableType.PETITIONER_TYPE));
			// Setting Petitioner Type from parameter list
			this.setCatType(accreditationfacadeBean.getParameterList(MasterTableType.CAT_TYPE));
			// Setting Balance type list
			this.setBalanceType(accreditationfacadeBean.getParameterList(MasterTableType.TYPES_BALANCE_CREDIT));
			//Setting signature list
			this.setSignatureCertificateList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_SIGNATURE));
			//Setting Reject Motive List
			this.setRejectMotiveList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_REJECT_MOTIVE));
			this.setUnblockMotiveList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_UNBLOCK_MOTIVE));
			this.setDestinationDeliverList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_DESTINATION_DELIVER));
			listHolidays();
			issuerList();
			this.setListMotives(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITACION_CERTIFICATION_MOTIVES));
//			this.setListCurrency(accreditationfacadeBean.getParameterList(MasterTableType.CURRENCY));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
		this.setAccreditationOperation(new AccreditationOperation());
		this.accreditationOperation.setRegisterDate(nowTime);
		accreditationOperation.setCustodyOperation(new CustodyOperation());
		this.accreditationOperation.setIdAccreditationOperationPk(null);
//		this.getAccreditationOperation().setExpirationDate(this.holidayQueryServiceBean.getCalculateDate(nowTime,  3, 1,null));
		this.getBlockRequest().setBlockEntity(new BlockEntity());
		this.setHolder(new Holder());
		initialDate = getCurrentSystemDate();
		endDate = getCurrentSystemDate();
		this.setDisableParticipantSelection(Boolean.FALSE);
//		getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
		getAccreditationOperation().setCertificationType(reportedBalanceCode);
		disableBalanceType = true;
		Integer typed = userInfo.getUserAccountSession().getInstitutionType();
		
		if(typed.equals(InstitutionType.ISSUER.getCode()) || typed.equals(InstitutionType.ISSUER_DPF.getCode())){
			loadSecurityClass();
			loadCurrencySearch();
			isIssuer=true;
		}else{
			loadSecurityClassSearch();
			loadCurrencySearch(); 
			isIssuer=false;
			this.getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
		}
		validateIsParticipantUser();		
		optionSelectedOneRadio = Integer.valueOf(0);
	}

	/**
	 * Evaluate option selected.
	 */
	public void evaluateOptionSelected(){
		JSFUtilities.resetViewRoot();
		if(optionSelectedOneRadio.equals(Integer.valueOf(0))){
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
			optionSelectedOneRadio = Integer.valueOf(0);
			accreditationOperationSelection = new AccreditationOperation();
			//seleccion multiple
			listAccreditationOperationSelection = new ArrayList<>();
			//seleccion multiple
			
			lstAccreditationDeliveryInformation = new ArrayList<AccreditationDeliveryInformation>();
			// Setting participant list
			Participant participantTO = new Participant();
			participantTO.setState(ParticipantStateType.REGISTERED.getCode());
			participantList = new ArrayList<Participant>();
			security = new Security();
			accreditationDeliveryInformation = new AccreditationDeliveryInformation();
			lstSecurityClass = new ArrayList<ParameterTable>();
			listSecurityClassSearch = new ArrayList<ParameterTable>();
			listCurrency = new ArrayList<ParameterTable>();
			try {			
				participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
				// Setting Block type list from parameter List
				this.setBlockType(accreditationfacadeBean.getParameterList(MasterTableType.TYPE_AFFECTATION));
				// Setting States list from parameterlist
				this.setStateList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_CERTIFICATE_STATE));
				// Setting Petitioner Type from parameter list
				this.setPetitionerType(accreditationfacadeBean.getParameterList(MasterTableType.PETITIONER_TYPE));
				// Setting Balance type list
				this.setBalanceType(accreditationfacadeBean.getParameterList(MasterTableType.TYPES_BALANCE_CREDIT));
				//Setting signature list
				this.setSignatureCertificateList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_SIGNATURE));
				//Setting Reject Motive List
				this.setRejectMotiveList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_REJECT_MOTIVE));
				this.setUnblockMotiveList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_UNBLOCK_MOTIVE));
				this.setDestinationDeliverList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_DESTINATION_DELIVER));
				listHolidays();
				issuerList();
				this.setListMotives(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITACION_CERTIFICATION_MOTIVES));
//				this.setListCurrency(accreditationfacadeBean.getParameterList(MasterTableType.CURRENCY));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				excepcion.fire(new ExceptionToCatchEvent(e));
			}		
			this.setAccreditationOperation(new AccreditationOperation());
			this.accreditationOperation.setRegisterDate(nowTime);
			this.accreditationOperation.setIdAccreditationOperationPk(null);
//			this.getAccreditationOperation().setExpirationDate(this.holidayQueryServiceBean.getCalculateDate(nowTime,  3, 1,null));
			
			this.getBlockRequest().setBlockEntity(new BlockEntity());
			this.setHolder(new Holder());
			initialDate = getCurrentSystemDate();
			endDate = getCurrentSystemDate();	
			accreditationOperation.setCustodyOperation(new CustodyOperation());
			this.setDisableParticipantSelection(Boolean.FALSE);
//			if (userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
//		 				this.setDisableParticipantSelection(Boolean.TRUE);
//		 				getAccreditationOperation().setPetitionerType(PetitionerType.ISSUER.getCode());
//		 				issuerName= userInfo.getUserAccountSession().getIssuerCode() + " - " + userInfo.getUserAccountSession().getUserInstitutionName();
//			}else{
//				getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
//			}
			getAccreditationOperation().setCertificationType(reportedBalanceCode);
			disableBalanceType = true;
			Integer typed = userInfo.getUserAccountSession().getInstitutionType();	  
			if(typed.equals(InstitutionType.ISSUER.getCode())){loadSecurityClass();loadCurrencySearch();
			}else{loadSecurityClassSearch();loadCurrencySearch(); this.getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());}
			validateIsParticipantUser();		
			optionSelectedOneRadio = Integer.valueOf(0);
		}
		else if(optionSelectedOneRadio.equals(Integer.valueOf(1))){
			this.setIssuanceCertFileNameDisplay(null);
			this.setAccreditationOperationSelection(null);
			//seleccion multiple
			this.setListAccreditationOperationSelection(null);
			//seleccion multiple
			
			this.setHolderAccountBalances(null);
			this.setHolderAccounts(null);
			this.setHolder(new Holder());
			this.setSecurity(new Security());
			this.setBlockOperations(null);
			this.setAccreditationOperationDataModel(null);
			this.setBlockRequest(new BlockRequest());
			this.getBlockRequest().setBlockEntity(new BlockEntity());
			this.setParticipant(new Participant());		
			this.setAccreditationDetails(new ArrayList<AccreditationDetail>());
			this.setAccreditationOperation(new AccreditationOperation());
			this.validateIsParticipantUser();
			this.getBlockRequest().setBlockEntity(new BlockEntity());
			lstAccreditationOperationTODeliver = null;
			this.setDisableBalanceType(Boolean.TRUE);
			reportedBalanceTO=null;
			reportedOperationTOList=new ArrayList<MechanismOperationTO>();
			getAccreditationDetail().setSecurity(new Security());
			getAccreditationOperation().setMotive(null);
			getAccreditationOperation().setIdAccreditationOperationPk(null);
			getAccreditationOperation().setPetitionerType(null);
			getAccreditationOperation().setCertificationType(null);
		    listIssuer=null;
			initialDate = null;
			endDate = null;		
			currency=null;
			securityClass=null;
			accreditationOperation.setCustodyOperation(new CustodyOperation());
			listFileAccreditationTO = new ArrayList<FileAccreditationTO>();
			accreditationDeliveryInformation = new AccreditationDeliveryInformation();
			lstAccreditationDeliveryInformation = new ArrayList<AccreditationDeliveryInformation>();
			optionSelectedOneRadio = Integer.valueOf(1);
			
		}
		else if(optionSelectedOneRadio.equals(Integer.valueOf(2))){
			this.setIssuanceCertFileNameDisplay(null);
			this.setAccreditationOperationSelection(null);
			//seleccion multiple
			this.setListAccreditationOperationSelection(null);
			//seleccion multiple
			
			this.setHolderAccountBalances(null);
			this.setHolderAccounts(null);
			this.setHolder(new Holder());
			this.setSecurity(new Security());
			this.setBlockOperations(null);
			this.setAccreditationOperationDataModel(null);
			this.setBlockRequest(new BlockRequest());
			this.getBlockRequest().setBlockEntity(new BlockEntity());
			this.setParticipant(new Participant());		
			this.setAccreditationDetails(new ArrayList<AccreditationDetail>());
			this.setAccreditationOperation(new AccreditationOperation());
			this.validateIsParticipantUser();
			this.getBlockRequest().setBlockEntity(new BlockEntity());
			lstAccreditationOperationTODeliver = null;
			this.setDisableBalanceType(Boolean.TRUE);
			reportedBalanceTO=null;
			reportedOperationTOList=new ArrayList<MechanismOperationTO>();
			getAccreditationDetail().setSecurity(new Security());
			getAccreditationOperation().setMotive(null);
			getAccreditationOperation().setIdAccreditationOperationPk(null);
			getAccreditationOperation().setPetitionerType(null);
			getAccreditationOperation().setCertificationType(null);
			listIssuer=null;
		    initialDate = null;
			endDate = null;		
			currency=null;
			securityClass=null;
			accreditationOperation.setCustodyOperation(new CustodyOperation());
			listFileAccreditationTO = new ArrayList<FileAccreditationTO>();
			accreditationDeliveryInformation = new AccreditationDeliveryInformation();
			lstAccreditationDeliveryInformation = new ArrayList<AccreditationDeliveryInformation>();
			optionSelectedOneRadio = Integer.valueOf(2);
		}
	}
	
	/**
	 * Adds the days habils to date.
	 *
	 * @param date the date
	 * @param days the days
	 * @return the date
	 */
	public Date addDaysHabilsToDate(Date date, Integer days){
		return CommonsUtilities.addDaysToDate(date, days);
	}
	
	/**
	 * Show isin detail.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityDetail(String securityCode){
	  securityHelpBean.setSecurityCode(securityCode);
	  securityHelpBean.setName("securityHelp");
	  securityHelpBean.searchSecurityHandler();
	 }
	 
	/**
	 * Apply quantity with market fact.
	 *
	 * @param hab the hab
	 */
	public void applyQuantityWithMarketFact(HolderAccountBalance hab){
		 		
			if(Validations.validateIsNotNullAndNotEmpty(hab.getTransferAmmount()) && hab.getTransferAmmount().compareTo(BigDecimal.ZERO) > 0){
				
				String balanceLabel = PropertiesUtilities.getMessage("lbl.accreditation.certificate.available_balance");
				
				if(hab.getTransferAmmount().compareTo(hab.getAvailableBalance())>0){
					hab.setTransferAmmount(null);
					Object [] object = {balanceLabel};
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getValidationMessage("error.greater.market.fact",object));
					JSFUtilities.showSimpleValidationDialog();
				}
				else{
					setMarketFactUI(hab);	
					return;
				}
			}
		}
	 
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Validate is participant user.
	 */
	public void validateIsParticipantUser(){	 
		if(Validations.validateIsNotNull(userInfo)){
	 		Integer type = userInfo.getUserAccountSession().getInstitutionType();
	 		if(Validations.validateIsNotNull(type)){
	 			if(type.equals(InstitutionType.ISSUER.getCode()) || type.equals(InstitutionType.ISSUER_DPF.getCode())){
	 				this.setDisableParticipantSelection(Boolean.TRUE);
	 				getAccreditationOperation().setPetitionerType(PetitionerType.ISSUER.getCode());
	 				issuerName=userInfo.getUserAccountSession().getIssuerCode();
	 				isIssuer=true;
	 				if(type.equals(InstitutionType.ISSUER_DPF.getCode())){
	 					//validar si tiene participante para settear el flag de participante
						Long participantDpf = getIssuerDpfInstitution(userInfo);
						if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
							flagExistParticipantDPF = true;
						}else{
							flagExistParticipantDPF = false;
						}
	 				}
	 			}else{
	 				issuerName=null;
	 				isIssuer=false;
		 			if((type.equals(InstitutionType.PARTICIPANT.getCode()))
						   || (type.equals(InstitutionType.AFP.getCode()))
						   || (type.equals(InstitutionType.PARTICIPANT_INVESTOR.getCode()))){
					    if(Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())){
						     for(Participant temp: this.getParticipantList()){
						    	 if(temp.getIdParticipantPk().equals(userInfo.getUserAccountSession().getParticipantCode())){
						    		 participant = temp;
						    		 this.setDisableParticipantSelection(Boolean.TRUE);
						    		 accreditationOperation.setParticipantPetitioner(temp);
						    		 this.getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
						    		 //VALIDATING IF PARTICIPANT LOGGED IS BLOCKED
						    		 if(temp.getState().equals(ParticipantStateType.BLOCKED.getCode())){
						    			 participantLoggedIsBlocked = Boolean.TRUE;
						    		 }
						    		 return;
						    	 }
						     }
					    }
				    }else{
				    	participant.setIdParticipantPk(null);
				    }
	 			}
	 		}
	 	}
	}

	/**
	 * Evaluate validity days.
	 */
	public void evaluateValidityDays(){
		clearGrill();
		JSFUtilities.resetViewRoot();
		if(accreditationOperation.getValidityDays()==null){
			accreditationOperation.setValidityDays(null);
			accreditationOperation.setExpirationDate(null);
			return;
		}else{
			if(accreditationOperation.getValidityDays()>90){
				accreditationOperation.setValidityDays(null);
				accreditationOperation.setExpirationDate(null);
			}else{
				Date expiration;
				expiration=CommonsUtilities.addDate(accreditationOperation.getIssuanceDate(), accreditationOperation.getValidityDays());		
//				while (holidayQueryServiceBean.isNonWorkingDayServiceBean(expiration,true)) {
//						expiration=CommonsUtilities.addDate(expiration,1);
//					}
				this.getAccreditationOperation().setExpirationDate(expiration);
				if (holidayQueryServiceBean.isNonWorkingDayServiceBean(expiration,true)){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
						    PropertiesUtilities.getMessage(PropertiesConstants.DAY_NOT_WORKING_ACCREDITATION));
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
	}

//	public void evaluateEmissionDate() {
//		JSFUtilities.hideGeneralDialogues();
//		if (Validations.validateIsNotNullAndNotEmpty(accreditationOperation.getIssuanceDate()) &&
//				accreditationOperation.getRegisterDate().after(accreditationOperation.getIssuanceDate())) {
//			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//						,PropertiesUtilities.getMessage(PropertiesConstants.EMISSION_DATE_LESS_THAN_REG_DATE));
//			JSFUtilities.showValidationDialog();
//			accreditationOperation.setIssuanceDate(null);
//		}else{
//				this.getAccreditationOperation().setValidityDays(null);
//				this.getAccreditationOperation().setExpirationDate(null);
//		}		
//	} 
	
	/**
	 * Evaluate date consult.
	 *
	 * @return true, if successful
	 */
	public boolean evaluateDateConsult() {		
		if (accreditationOperation.getRegisterDate().after(accreditationOperation.getEndDate())) {
			JSFUtilities.addContextMessage("frmSearch:idExpirationDate",FacesMessage.SEVERITY_ERROR,
										PropertiesUtilities.getMessage(PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE),
										PropertiesUtilities.getMessage(PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE));
			return true;
		}else{
			return false;
		}
	}// end evaluateDates

	/**
	 * Accreditation consult.
	 * This method is executed in Search.xml, it searchs all accreditation 
	 */
	@LoggerAuditWeb
	public void accreditationConsult() {
		
		accreditationOperationDataModel = null;
		accreditationOperation.setHolder(holder);
		accreditationOperation.setParticipantPetitioner(participant);
		accreditationOperation.setSecurity(security);
		try {
			List<AccreditationOperation> accOperationList = accreditationfacadeBean.accreditationAvailableConsult(accreditationOperation, initialDate, endDate, getCurrency(),getSecurityClass(),getIssuerName());
			//Validating if petitioner type is CREDITOR TO search entity Block Info			
			for(int i = 0; i < accOperationList.size(); ++i){
				AccreditationOperation accreditationOperationTmp = accOperationList.get(i);		
				if(accreditationOperationTmp.getPetitionerType().equals(this.getCreditorPetitioner())){
					BlockEntity blockEntity = accreditationfacadeBean.searchBlockEntity(accreditationOperationTmp.getIdAccreditationOperationPk());
					//VALIDATING IF ITS BLOCK ENTITY THAT IS FILTERED.
					if(Validations.validateIsNotNull(blockRequest.getBlockEntity().getIdBlockEntityPk()) &&
					  (!blockEntity.getIdBlockEntityPk().equals(blockRequest.getBlockEntity().getIdBlockEntityPk()))){
						accOperationList.remove(i);
						i=0;
						continue;
					}
					//SETTING BLOCK ENTITY NAME
					accreditationOperationTmp.setBlockEntityName(blockEntity.getFullName());
					accreditationOperationTmp.setIdBlockEntity(blockEntity.getIdBlockEntityPk());
				}
			}
			if(Validations.validateListIsNullOrEmpty(accOperationList)){
				accreditationOperationDataModel = new GenericDataModel<AccreditationOperation>();
			}else{
				for (int j = 0; j < accOperationList.size(); j++) {
					//setting values by generate and deliver columns
					if(accOperationList.get(j).getIndGenerated()!=null && 
							accOperationList.get(j).getIndGenerated().equals(new Integer(1))){
						accOperationList.get(j).setStrGenerated(BooleanType.YES.getValue());
					}else{
						accOperationList.get(j).setStrGenerated(BooleanType.NO.getValue());
					}
					if(!userInfo.getUserAccountSession().isDepositaryInstitution()
							&& Validations.validateIsNotNull(accOperationList.get(j).getAccreditationState())
							&& accOperationList.get(j).getAccreditationState().equals(AccreditationOperationStateType.UNBLOCKED.getCode()) ){
						accOperationList.get(j).setIndGenerated(BooleanType.NO.getCode());
					}
					
					if(accOperationList.get(j).getIndDelivered()!=null && 
							accOperationList.get(j).getIndDelivered().equals(new Integer(1))){
						accOperationList.get(j).setStrDelivered(BooleanType.YES.getValue());
					}else{
						accOperationList.get(j).setStrDelivered(BooleanType.NO.getValue());
					}
				}
				accreditationOperationDataModel = new GenericDataModel<AccreditationOperation>(accOperationList);
			}	
			showButtons();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Find holder.
	 * Method that find holder and holder accounts depending of holder
	 */
	public void findHolder() {				
		holderAccounts = null;		
		clearGrill();
		try {
			if(holder.getStateHolder()==null){
				holder = new Holder();
				holderAccounts = new ArrayList<HolderAccount>();	
				return;
			}
			if(holder.getStateHolder().equals(HolderStateType.BLOCKED.getCode())){
				holder = new Holder();
				holderAccounts = new ArrayList<HolderAccount>();				
					showMessageOnDialog(null, null,PropertiesConstants.ERROR_HOLDER_NOT_REGISTERED_STATE, null);
				JSFUtilities.updateComponent("contentForm:pnlHolderHelperPart");
				JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
				return;
			}else if( Validations.validateIsNull(holder.getStateHolder())){
				holder = new Holder();
				holderAccounts = new ArrayList<HolderAccount>();
				return;
			}
			if(Validations.validateIsNotNull(this.getAccreditationOperation().getPetitionerType())){
				if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.CREDITOR.getCode())){
					Participant objParticipant = null;
					List<Long> lstHolderAccount = null;
					List<String> lstSecurities = null;
					if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
						objParticipant = accountsFacade.getParticipantByPk(userInfo.getUserAccountSession().getParticipantCode());
						lstHolderAccount = accreditationfacadeBean.getLstHolderAccountForDocNumberAndDocType(objParticipant);
						lstSecurities = accreditationfacadeBean.getLstSecuritiesForPartInv(objParticipant,lstHolderAccount);
					}
					List<BlockOperation> blockOperationTempList = (accreditationfacadeBean.searchBlockOperationDetail(holder,null, null,null,
																	blockRequest.getBlockEntity().getIdBlockEntityPk(),userInfo,lstSecurities));
					if(Validations.validateListIsNullOrEmpty(blockOperationTempList)){
						//Calling Modal
						showMessageOnDialog(null, null,PropertiesConstants.ERROR_ON_HOLDER_AND_ENTITY_BLOCK, null);
						JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
						holder = new Holder();
						return;
					}
				}
			}
			//Validating if holder is not equal null and Holder State is registered
			if (Validations.validateIsNotNull(holder)) {
				// Validating if participant is not null
				if (participant.getIdParticipantPk() != null && participant.getIdParticipantPk() != 0) {					
					// Searching holder accounts by participant
					HolderTO holderTO = new HolderTO();
					if(holder.getIdHolderPk()!=null){
					holderTO.setHolderId(holder.getIdHolderPk());
					}
					holderTO.setFlagAllHolders(true);
					holderTO.setParticipantFk(participant.getIdParticipantPk());
					List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);					
					
					if(lstAccountTo!=null && lstAccountTo.size()>0){
						holderAccounts = new ArrayList<HolderAccount>();
						HolderAccount ha;
						for(HolderAccountHelperResultTO hahrTO : lstAccountTo){
							ha = new HolderAccount();
							ha.setAccountNumber(hahrTO.getAccountNumber());
							ha.setAlternateCode(hahrTO.getAlternateCode());
							ha.setAccountGroup(hahrTO.getAccountGroup());
							ha.setIdHolderAccountPk(hahrTO.getAccountPk());
							ha.setHolderDescriptionList(hahrTO.getHolderDescriptionList());
							ha.setStateAccountDescription(hahrTO.getAccountStatusDescription());
							ha.setStateAccount(hahrTO.getStateAccount());
							holderAccounts.add(ha);
						}
						
					}
					if(holderAccounts != null){
						// Validating if it was holderAccountDetails found
						if (holderAccounts.size() > 0) {
							accreditationOperation.setHolder(holder);
							if(holderAccounts.size()==1){
								if (holderAccounts.get(0).getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
									holderAccounts.get(0).setSelected(true);
								}else{
									holder = new Holder();
									holderAccounts = new ArrayList<HolderAccount>();				
										showMessageOnDialog(null, null,PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE, null);
									JSFUtilities.updateComponent("contentForm:pnlHolderHelperPart");
									JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
									return;
								}
							}
						} else {
							//Calling Modal
							showMessageOnDialog(null, null,PropertiesConstants.ERROR_ON_ACCOUNT_AND_PARTICIPANT, null);
							JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
							holder = new Holder();
						}
					}else{
						holder = new Holder();
						showMessageOnDialog(null, null,PropertiesConstants.ACCREDITATION_CERTIFICATE_HOLDER_WITH_NOT_ACCOUNT, null);
						JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
					}									
				} else {					
					// Searching holderAccountDetails by holder
					holderAccounts = accreditationfacadeBean.searchHolderAccount(holder);
					
					ParameterTableTO parameterTableTO = new ParameterTableTO();
					Map<Integer,String> mapAccountStateDescription = new HashMap<Integer,String>();
					parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
					for(ParameterTable param : generalParametersService.getListParameterTableServiceBean(parameterTableTO)){
						mapAccountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
					}

//					if(userInfo.getUserAccountSession().getInstitutionType().equals(InstitutionType.ISSUER.getCode())){
					if(getAccreditationOperation().getPetitionerType().equals(PetitionerType.ISSUER.getCode())){						
						holderAccountsIssuer = holderAccounts;
						holderAccounts=null;
						holderAccounts=new ArrayList<HolderAccount>();
//						String typer = userInfo.getUserAccountSession().getIssuerCode();	
						String typer = issuerName;	
						for (int i = 0; i < holderAccountsIssuer.size(); i++) {
							if (holderAccountsIssuer.get(i).getParticipant().getIssuer().getIdIssuerPk()!=null){
								if((holderAccountsIssuer.get(i).getParticipant().getIssuer().getIdIssuerPk()).equals(typer)){
									List<String> holderDescriptionListTwo = new ArrayList<String>();
									holderDescriptionListTwo.add(holderAccountsIssuer.get(i).getParticipant().getDescription());
									holderAccountsIssuer.get(i).setStateAccountDescription((mapAccountStateDescription.get(holderAccountsIssuer.get(i).getStateAccount())));
									holderAccountsIssuer.get(i).setHolderDescriptionList(holderDescriptionListTwo);
									holderAccounts.add(holderAccountsIssuer.get(i));
								}
							}
						}
						if((holderAccounts==null)
								|| (holderAccounts.size()==0)){
							holder = new Holder();
							holderAccounts = new ArrayList<HolderAccount>();	
							showMessageOnDialog(null, null,PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_HOLDER_ACCOUNT_ISSUER, null);
							JSFUtilities.updateComponent("contentForm:pnlHolderHelperPart");
							JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
							return;
						}
					}

					if(holderAccounts != null){
						// Validating if it was holderAccountDetails found
						if (holderAccounts.size() > 0) {
							// Setting the holder
							accreditationOperation.setHolder(holder);
							for (int i = 0; i < holderAccounts.size(); i++) {
										List<String> holderDescriptionListThree = new ArrayList<String>();
										holderDescriptionListThree.add(holderAccounts.get(i).getParticipant().getDescription());
										holderAccounts.get(i).setStateAccountDescription((mapAccountStateDescription.get(holderAccounts.get(i).getStateAccount())));
										holderAccounts.get(i).setHolderDescriptionList(holderDescriptionListThree);
									}
							if(holderAccounts.size()==1){
								if (holderAccounts.get(0).getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
									holderAccounts.get(0).setSelected(true);
								}else{
									holder = new Holder();
									holderAccounts = new ArrayList<HolderAccount>();				
										showMessageOnDialog(null, null,PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE, null);
									JSFUtilities.updateComponent("contentForm:pnlHolderHelperPart");
									JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
									return;
								}
							}
						} else {
							holder = new Holder();
							showMessageOnDialog(null, null,PropertiesConstants.ACCREDITATION_CERTIFICATE_HOLDER_WITH_NOT_ACCOUNT, null);
							JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
						}
					}else {
						holder = new Holder();
						showMessageOnDialog(null, null,PropertiesConstants.ACCREDITATION_CERTIFICATE_HOLDER_WITH_NOT_ACCOUNT, null);
						JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show()");
					}					
				}
			  //If holder is equal null
			} else if (Validations.validateIsNotNull(holder)) {
				holder = new Holder();			
			}
		}catch (Exception e) {			
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	} // end findHolder

	/**
	 * Metodo para bloquear los cambios en el campo Dias de Vigencia
	 */
	public void onChangeCatType() {
		
		//Validamos de que tipo es el cat que estams tratando de registrar
		if(this.accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
			validityDaysBoo = false;
		}else{
			validityDaysBoo = true;
		}
		//Cualquier cambio vacia estos campos
		accreditationOperation.setValidityDays(null);
		accreditationOperation.setExpirationDate(null);
		accreditationOperation.setMotive(null);
		accreditationOperation.setExpirationDate(null);
		holder = new Holder();
		holderAccounts = new ArrayList<HolderAccount>();
		this.getBlockRequest().setBlockType(null);
		JSFUtilities.resetComponent("contentForm:idHolderPart");
		JSFUtilities.resetComponent("contentForm:idValidityDays");
		clearGrill();
	}
	
	/**
	 * On change participant in comboBox register.
	 */
	public void onChangeIssuer() {
		holder = new Holder();
		holderAccounts = new ArrayList<HolderAccount>();
		this.getBlockRequest().setBlockType(null);
		JSFUtilities.resetComponent("contentForm:idHolderPart");
		clearGrill();
	}
	
	/**
	 * On change participant in comboBox register.
	 */
	public void onChangeParticipant() {
		if (participant.getIdParticipantPk() != null) {
			for (Participant p : participantList) {
				if (p.getIdParticipantPk().equals(participant.getIdParticipantPk())) {					
					accreditationOperation.setParticipantPetitioner(p);						
					break;
				}
			}
		}
		
		if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
			holder = new Holder();
			holderAccounts = new ArrayList<HolderAccount>();
			this.getBlockRequest().setBlockType(null);
			JSFUtilities.resetComponent("contentForm:idHolderPart");
		}
		
		clearGrill();
	}
	
	/**
	 * On change participant in comboBox search.
	 */
	public void onChangeParticipantSearch() {
		if (participant.getIdParticipantPk() != null) {
			for (Participant p : participantList) {
				if (p.getIdParticipantPk().equals(participant.getIdParticipantPk())) {					
					accreditationOperation.setParticipantPetitioner(p);						
					break;
				}
			}
		} 
		holder = new Holder();
		holderAccounts = new ArrayList<HolderAccount>();
		this.getBlockRequest().setBlockType(null);
		JSFUtilities.resetComponent("frmSearch:idHolderPart");
		clearGrill();
	}
	
	/**
	 * On change blocked type.
	 */
	public void onChangeBlockedType(){		
		if(getAccreditationOperation().getCertificationType() != null){
			if(getAccreditationOperation().getCertificationType().equals(availableBalanceCode)){
				renderBlockedType = Boolean.FALSE;
			}else if(getAccreditationOperation().getCertificationType().equals(blockedBalanceCode)){
				renderBlockedType = Boolean.TRUE;
			}else{
				renderBlockedType = Boolean.FALSE;
			}
		}else{
			renderBlockedType = Boolean.FALSE;
		}
		this.getBlockRequest().setBlockType(null);
		clearGrill();
	}
	
	/**
	 * On change petitioner in comboBox.
	 */
	public void onChangePetitioner(){
		JSFUtilities.hideGeneralDialogues();
		if(this.getDisableParticipantSelection()){
			return;
		}
		accreditationOperation.setParticipantPetitioner(null); //needles				
		//	Set petitioner
		this.setParticipant(new Participant());
		this.setHolder(new Holder());
		this.setIssuerName(null);
		this.setHolderAccounts(new ArrayList<HolderAccount>());
		this.getBlockRequest().setBlockEntity(new BlockEntity());
//		this.setDisableBalanceType(Boolean.FALSE);
		this.setDisableBalanceType(Boolean.TRUE);
		renderBlockedType = Boolean.FALSE;
		this.getAccreditationOperation().setCertificationType(reportedBalanceCode);	
		if(this.getAccreditationOperation().getPetitionerType() != null){			
			// 	Is Creditor or Participant or holder
			if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.CREDITOR.getCode())){						
//				this.setDisableBalanceType(Boolean.TRUE);
				renderBlockedType = Boolean.TRUE;
				//Balance type blocked (Bloqueado)
				this.getAccreditationOperation().setCertificationType(blockedBalanceCode);			
			}
//				else if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.PARTICIPANT.getCode())){			
//				this.setDisableBalanceType(Boolean.FALSE);			
				//Balance type reported (Mixto)
//				this.getAccreditationOperation().setCertificationType(reportedBalanceCode);			
//			}else if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.HOLDER.getCode())){
//				this.setDisableBalanceType(Boolean.FALSE);
//				this.getAccreditationOperation().setCertificationType(null);					
//			}else{	
//				this.setDisableBalanceType(Boolean.FALSE);									
//				this.getAccreditationOperation().setCertificationType(null);
//			}
//		}else{
//			this.setDisableBalanceType(Boolean.FALSE);
//			this.getAccreditationOperation().setCertificationType(null);
//			renderBlockedType = Boolean.FALSE;
		}
		accreditationOperation.setAccreditationCertificate(null);
		accreditationOperation.setSustentFileName(null);
		this.issuanceCertFileNameDisplay = null;
		resetCompRegister();
		onChangeBlockedType();		
	}
	
	/**
	 * Reset comp register.
	 */
	public void resetCompRegister(){
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Show buttons.
	 */
	public void showButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		//make sure the privileges are setting correct
		if(accreditationOperationDataModel.getSize()>0){
			if(userPrivilege.getUserAcctions().isConfirm()){
				privilegeComponent.setBtnConfirmView(true);
			}
			if(userPrivilege.getUserAcctions().isReject()){
				privilegeComponent.setBtnRejectView(true);
			}
//			if(userPrivilege.getUserAcctions().isUnblock()){
//				privilegeComponent.setBtnUnblockView(true);
//			}
			if(userPrivilege.getUserAcctions().isGenerate()){
				privilegeComponent.setBtnGenerateView(true);
			}
			/*if(userPrivilege.getUserAcctions().isDeliver()){
				privilegeComponent.setBtnDeliverView(true);
			}*/
		}
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Before confirm.
	 * Method that ask if you want to confirm or no
	 *
	 * @param fromView the from view
	 */
	public void beforeConfirm(String fromView){
		executeAction();		
		JSFUtilities.hideGeneralDialogues();
		if(fromView.equals("Search")){
			if(accreditationOperationSelection !=null &&
					accreditationOperationSelection.getCustodyOperation()!=null &&
							accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk() != null){
				JSFUtilities.executeJavascriptFunction("PF('acreditConfirm').show();");
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.executeJavascriptFunction("PF('acreditConfirm').show();");
				return;
			}
		}else if(fromView.equals("Detail")){
			JSFUtilities.executeJavascriptFunction("PF('acreditConfirm').show();");
		}
	}// end beforeConfirm
	
	/**
	 * Before reject.
	 *
	 * @param fromView the from view
	 */
	public void beforeReject(String fromView){
		executeAction();		
		JSFUtilities.hideGeneralDialogues();
		if(fromView.equals("Search")){
			if(	accreditationOperationSelection !=null &&  accreditationOperationSelection.getCustodyOperation()!=null &&
				accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk() != null){
				JSFUtilities.executeJavascriptFunction("PF('acreditReject').show();");
			}else{
				showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
				JSFUtilities.showValidationDialog();
				return;
			}
		}else if(fromView.equals("Detail")){
			//JSFUtilities.executeJavascriptFunction("PF('acreditReject').show();");
			//JSFUtilities.resetComponent("rejectForm");
			accreditationOperationSelection.setIdRejectMotive(null);
			accreditationOperationSelection.setRejectMotiveOther(null);
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
		}
	} // end beforeReject
	
	/**
	 * Before unblock.
	 *
	 * @param fromView the from view
	 */
	public void beforeUnblock(String fromView){
		executeAction();		
		JSFUtilities.hideGeneralDialogues();
		if(fromView.equals("Search")){
			if(Validations.validateIsNotNull(accreditationOperationSelection) &&
					Validations.validateListIsNullOrEmpty(this.getListAccreditationOperationSelection()) &&
			   Validations.validateIsNotNull(accreditationOperationSelection.getCustodyOperation()) && 
			   Validations.validateIsNotNull(accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk())){
				JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').show();");
			}else{
				showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
				JSFUtilities.showValidationDialog();
				return;
			}
		}else if(fromView.equals("Detail")){
						JSFUtilities.resetComponent(":frmMotiveUnblock:dlgUnblock");
						executeAction();
						JSFUtilities.hideGeneralDialogues();
						accreditationOperationSelection.setUnblockMotive(null);
						accreditationOperationSelection.setUnblockObservation(null);
//						JSFUtilities.showComponent(":frmMotiveUnblock:dlgUnblock");
						JSFUtilities.executeJavascriptFunction("PF('dlgUnblock').show();");
		}
	}// end beforeUnblock
	
	/**
	 * Before to detail.
	 *
	 * @param button the button
	 * @throws InterruptedException the interrupted exception
	 * @throws ServiceException the service exception
	 */
	public void beforeToDetail(String button) throws InterruptedException, ServiceException{
		/** Verifying if there is something SELECTED. **/
		
		StringBuilder result = new StringBuilder();
		listidAccreditationOperationPk = null;
		
		if(Validations.validateListIsNullOrEmpty(listAccreditationOperationSelection)){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		//CREANDO OBJETO TO PARA VALIDACION DE SUSCURSALES
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		OperationUserTO operationUserTO = new OperationUserTO();
		//concatenado de numeros de solicitud
	    for(AccreditationOperation accreditationOperation : this.getListAccreditationOperationSelection()) {
	        result.append(accreditationOperation.getCustodyOperation().getOperationNumber().toString());
	        result.append(", ");
	        //AGREGANDO USUARIOS Y PKS DE ACREDITACION A OBJETO TO
	        operationUserTO.setOperNumber(accreditationOperation.getCustodyOperation().getOperationNumber().toString());
			operationUserTO.setUserName(accreditationOperation.getCustodyOperation().getRegistryUser().toString());
			lstOperationUserParam.add(operationUserTO);
	    }
	    List<String> lstOperationUser = getIsSubsidiary(userInfo, lstOperationUserParam);
		if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
			StringBuilder strOperations = new StringBuilder();
			for(String operations : lstOperationUser) {
				strOperations.append(operations);
				strOperations.append(", ");
			}
			if(button.equals(CONFIRM)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM,
								new Object[] {strOperations.toString()}));
			}else if (button.equals(REJECT)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_REJECT,
								new Object[] {strOperations.toString()}));
			}else if (button.equals(UNBLOCK)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_UNBLOCK,
								new Object[] {strOperations.toString()}));
			}else if (button.equals(GENERATE)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_GENERATE,
								new Object[] {strOperations.toString()}));
			}else if (button.equals(DELIVER)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getGenericMessage(GeneralConstants.ERROR_MESSAGE_INVALID_USER_DELIVER,
								new Object[] {strOperations.toString()}));
			}
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
	    if(this.getListAccreditationOperationSelection() != null)
	    	listidAccreditationOperationPk = result.length() > 0 ? result.substring(0, result.length() -2): "";
		
		if(this.getListAccreditationOperationSelection() != null &&
			this.getListAccreditationOperationSelection().size() == 1 ){
			
			accreditationOperationSelection = this.getListAccreditationOperationSelection().get(0);
			
			if( accreditationOperationSelection !=null &&
					accreditationOperationSelection.getCustodyOperation()!=null &&
							accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk() != null){
				
				if(button.equals("confirm")){
					/** Verifying if CONFIRM action can be executed **/
					if(!accreditationOperationSelection.getAccreditationState().equals(this.getRegisteredTypeCode())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
																				  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACTION));
						JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show();");
						return;
					}	
					this.setConfirmButtonSelected(Boolean.TRUE);
					renderObservations = Boolean.FALSE;
					
				}else if(button.equals("reject")){
					if(!accreditationOperationSelection.getAccreditationState().equals(this.getRegisteredTypeCode())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
																				  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACTION));
						JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show();");
						return;
					}
					this.setRejectButtonSelected(Boolean.TRUE);
					renderObservations = Boolean.FALSE;
					
				}else if(button.equals("unblock")){
					if(!accreditationOperationSelection.getAccreditationState().equals(this.getConfirmededTypeCode())){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
																				  PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACTION));
						JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show();");
						return;
					}
					this.setUnblockButtonSelected(Boolean.TRUE);
					renderObservations = Boolean.TRUE;
					
				}else if(button.equals("generate")){
					
					//Validamos las firmas digitales, Redmine 765 se remueve la validacion de firmas digitales
//					if (accreditationfacadeBean.VerifyListDigitalSignature(DigitalSignatureType.FIRST_P12_CONTAINER_SIGNATURE.getCode(), DigitalSignatureType.SECOND_P12_CONTAINER_SIGNATURE.getCode())){
						if (!accreditationOperationSelection.getAccreditationState().equals(this.getConfirmededTypeCode())){
							JSFUtilities.executeJavascriptFunction("PF('stateNotConfirmed').show();");
						}else{
							
						//Solo se puede generar si el indicador es 0 o nulo
							if(accreditationOperationSelection.getIndGenerated()==null 
									|| accreditationOperationSelection.getIndGenerated()==0){
									JSFUtilities.executeJavascriptFunction("PF('generateDialogMessage').show();");
							}else{
								JSFUtilities.executeJavascriptFunction("PF('generatedAlreaydExists').show();");
							}	
						}
//					}else{
//						JSFUtilities.executeJavascriptFunction("PF('verifuSignatureVar').show();");
//					}
					
				}else if(button.equals("deliver")){
					//validate if the request was generated to continue the process "deliver"
					if(accreditationOperationSelection.getIndGenerated()!=null && accreditationOperationSelection.getIndGenerated()==1){
						if(accreditationOperationSelection.getIndDelivered()==null || accreditationOperationSelection.getIndDelivered()==0){
							JSFUtilities.executeJavascriptFunction("PF('deliverAccreditation').show();");
						}else{
							JSFUtilities.executeJavascriptFunction("PF('comittedAlreaydExists').show();");
						}
					}else{
						JSFUtilities.executeJavascriptFunction("PF('generationIsMandatory').show();");
					}
				}
				
				if(button.equals("reject") || button.equals("confirm") || button.equals("unblock")){
					try {
						this.accreditationDetail("");
						FacesContext.getCurrentInstance().getExternalContext().
									redirect("/iCustody/pages/accreditationCertificate/accreditationCertificateBlock/viewAccreditationCertificate.xhtml");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
				JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show();");
				return;
			}
			
		} else if( this.getListAccreditationOperationSelection() !=null &&
				this.getListAccreditationOperationSelection().size() > 1 ){
			
			Boolean equalStates = Boolean.TRUE;
			Boolean operationValidate = Boolean.TRUE;
			Boolean generated = Boolean.FALSE;
			String state = null;
			
			switch(button){
				case "confirm":
				case "reject":
					state = AccreditationOperationStateType.REGISTRED.getValue();
					break;
				case "unblock":
				case "generate":
					state = AccreditationOperationStateType.CONFIRMED.getValue();
					break;
			}
			
			for(AccreditationOperation accreditationOperation : this.getListAccreditationOperationSelection()){
				if(!accreditationOperation.getStateTypeDesc().equals(state)){
					equalStates = Boolean.FALSE;
					break;
				}
			}
			if(equalStates){
				for(AccreditationOperation operation : this.getListAccreditationOperationSelection()){
					if(!(operation != null && operation.getCustodyOperation() != null && operation.getCustodyOperation().getIdCustodyOperationPk() != null)){
						operationValidate = Boolean.FALSE;
						break;
					}
				}
				if(operationValidate){
					
					switch(button){
						case "confirm":
							JSFUtilities.executeJavascriptFunction("PF('acreditConfirm').show();");
							JSFUtilities.updateComponent("confirmForm:idAcreditConfirm");
							break;
						case "reject":
							this.setIdRejectMotiveOperationsSelected(null);
							this.setRejectMotiveOtherOperationsSelected(null);
							JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
							JSFUtilities.updateComponent("frmMotiveReject:dialogMotiveReject");
							break;
						case "unblock":
							JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').show();");
							JSFUtilities.updateComponent("unBlockForm:idUnBlockFormConfirm");
							break;
						case "generate":
							for(AccreditationOperation operation : this.getListAccreditationOperationSelection()){
								if(operation.getIndGenerated()!=null && operation.getIndGenerated()==GeneralConstants.ONE_VALUE_INTEGER){
									generated = Boolean.TRUE;
									break;
								}
							}
							
							if(!generated){
								JSFUtilities.executeJavascriptFunction("PF('generateDialogMessageSelection').show();");
							}else{
								JSFUtilities.executeJavascriptFunction("PF('generatedAlreaydExistsSelection').show();");
								return;
							}
							break;
					}
				}else{
					showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
					JSFUtilities.showValidationDialog();
					return;
				}
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.ERROR_STATE_NOT_EQUALS,new Object[]{state}));
				JSFUtilities.executeJavascriptFunction("PF('errorAccountList').show();");
				return;
			}
		}
	}
	
	/**
	 * After have selected motive.
	 */
	public void afterHaveSelectedMotive(){
		//executeAction();		
		//JSFUtilities.hideGeneralDialogues();
		renderAfterSelectedMotive = Boolean.TRUE;
		JSFUtilities.executeJavascriptFunction("PF('acreditRejectEnd').show();");
		JSFUtilities.updateComponent("rejectForm:acreditRejectEnd");
	}
	
	/**
	 * After have selected motive unblock.
	 */
	public void afterHaveSelectedMotiveUnblock(){
		//executeAction();		
		//JSFUtilities.hideGeneralDialogues();
		renderAfterSelectedMotive = Boolean.TRUE;
		JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').show();");
		JSFUtilities.updateComponent("unBlockForm:unBlockFormConfirm");
	}
	
	/**
	 * Close deliver dialog.
	 * Method that close deliver dialog
	 */
	public void closeDeliverDialog(){
		JSFUtilities.executeJavascriptFunction("PF('deliverAccreditation').hide();");
//		for(int i = 0; i < accreditationOperationDataModel.getDataList().size();++i){
//			if(accreditationOperationDataModel.getDataList().get(i).getIsSelected() != null && accreditationOperationDataModel.getDataList().get(i).getIsSelected()){
		accreditationOperationSelection.setIsSelected(false);
//				break;
//			}
//		}
	}// end closeDeliverDialog
	
	/**
	 * Close Generate dialog.
	 * Method that close Generate dialog
	 */
	public void closeGenerateDialog(){
		JSFUtilities.executeJavascriptFunction("PF('generateDialogMessage').hide();");
		accreditationOperationSelection.setIsGenerated(false);
	}
	
	
	/**
	 * Metodo para generar el CAT en PDF
	 * Generate accreditation constance.
	 */
	@LoggerAuditWeb
	public void generateAccreditationConstance(){
		try{
			//El indicador IND_GENERATED debe estar en 0 o nulo
			if(accreditationOperationSelection.getIndGenerated() == null 
					|| accreditationOperationSelection.getIndGenerated() == 0){
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk());
				accreditationOperationTO.setCertificationType(accreditationOperation.getCertificationType());
				accreditationOperationTO.setCatType(accreditationOperationSelection.getCatType());
				/** CHANGING TO GENERATED **/
				
				//Aqui generamos el CAT
				this.accreditationfacadeBean.generateConstance(accreditationOperationTO);
				
				//Enviamos mensaje de generacion exitosa
				JSFUtilities.executeJavascriptFunction("PF('generateDialogMessage').hide();");
				this.showDialogMessageWithParam(PropertiesConstants.ACCREDITATION_GENERATE_CONSTANCE_SUCCESS,
						"successDialog", new Object []{listidAccreditationOperationPk});
				
				//Limpiamos campos y botones y grilla
				accreditationOperationDataModel=null;
				JSFUtilities.updateComponent("frmSearch:outputPanelResult");
				this.setButtons();
				this.accreditationConsult();	
			}
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	
	/**
	 * Generate accreditation constance massive.
	 */
	@LoggerAuditWeb
	public void generateAccreditationConstanceMassive(){
		try{
			/** GENERATING CERTIFICATE**/
			// TODO Ver el tema de la generacion masiva
			List<AccreditationOperationTO> accreditationOperationTOs = new ArrayList<>();
			for(AccreditationOperation accreditationOperation : this.getListAccreditationOperationSelection()){
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperation.getCustodyOperation().getIdCustodyOperationPk());
				accreditationOperationTO.setCertificationType(accreditationOperation.getCertificationType());
				accreditationOperationTO.setCatType(accreditationOperation.getCatType());
				accreditationOperationTOs.add(accreditationOperationTO);
			}
			
			this.accreditationfacadeBean.generateConstanceMassive(accreditationOperationTOs);
			
			JSFUtilities.executeJavascriptFunction("PF('generateDialogMessage').hide();");
			this.showDialogMessageWithParam(PropertiesConstants.ACCREDITATION_GENERATE_CONSTANCES_SUCCESS,
					"successDialog", new Object []{listidAccreditationOperationPk});
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	
	
	/**
	 * Deliver accreditation.
	 * Method that set the destination if "MESSENGER" Or "HOLDER"
	 */
	@LoggerAuditWeb
	public void deliverAccreditation(){
		try{
			if(accreditationOperationSelection.getIndDelivered()==null || accreditationOperationSelection.getIndDelivered()==0){
				/** DELIVERING ACCREDITATION **/
				this.accreditationfacadeBean.deliverAccreditation(accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk(),
																  this.getAccreditationOperation().getDestinationCertificate());
				String message = PropertiesConstants.ACCREDITATION_DELIVER_SUCCESS;						
				this.showDialogMessage(message, "successDialog");
				//hide dialog
				JSFUtilities.executeJavascriptFunction("PF('deliverAccreditation').hide();");
				//Cleaning fields
				this.getBlockRequest().setBlockType(null);
				clearGrill();
				this.setButtons();
				accreditationOperation.setDestinationCertificate(null);
			}
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}// end deliverAccreditation
	
	/**
	 * Unblock accreditation.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 */
	@LoggerAuditWeb
	public void unblockAccreditation(AccreditationOperation accreditationOperationToUnblock){
		try {			
			this.accreditationfacadeBean.unBlockAccreditation(accreditationOperationToUnblock);
			JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').hide();");
			JSFUtilities.executeJavascriptFunction("PF('unblockSuccess').show();");
			
			this.accreditationConsult();
			this.clearAccreditationOperation();
			this.setButtons();
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').hide();");
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Unblock accreditation massive.
	 */
	@LoggerAuditWeb
	public void unblockAccreditationMassive(){
		try {
			this.accreditationfacadeBean.unBlockAccreditationMassive(this.getListAccreditationOperationSelection());
			JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').hide();");
			JSFUtilities.executeJavascriptFunction("PF('unblockSuccess').show();");
			
			this.accreditationConsult();
			this.clearAccreditationOperation();
			this.setButtons();
			//JSFUtilities.updateComponent("frmSearch:dtbResult");
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.executeJavascriptFunction("PF('unBlockFormConfirm').hide();");
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Change Accreditation Status.
	 * Change the accreditation Status
	 *
	 * @param accreditationOperationToChange the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws InterruptedException the interrupted exception
	 */
	@LoggerAuditWeb
	public void changeAccreditationStatus(AccreditationOperation accreditationOperationToChange, Integer accreditationState) throws InterruptedException {
		try {
			BusinessProcess businessProcess = new BusinessProcess();
			
			if(accreditationState.equals(AccreditationOperationStateType.CONFIRMED.getCode())){
				
				if(accreditationOperationToChange.getCatType().equals(CatType.CAT.getCode())){
					accreditationfacadeBean.confirmAccreditationRequest(accreditationOperationToChange, accreditationState);
				}else{
					accreditationfacadeBean.confirmAccreditationRequestB(accreditationOperationToChange, accreditationState);
				}
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode());
				
			}else if(accreditationState.equals(AccreditationOperationStateType.REJECTED.getCode())){
				
				if(accreditationOperationToChange.getCatType().equals(CatType.CAT.getCode())){
					accreditationfacadeBean.rejectAccreditationRequest(accreditationOperationToChange, accreditationState);
				}else{
					accreditationfacadeBean.rejectAccreditationRequestB(accreditationOperationToChange, accreditationState);
				}
				
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT.getCode());
			}
			
			if(accreditationState.equals(AccreditationOperationStateType.CONFIRMED.getCode())){
				
				JSFUtilities.executeJavascriptFunction("PF('acreditConfirm').hide();");
				JSFUtilities.executeJavascriptFunction("PF('acreditationSuccess').show();");
				
				/** GENERATING CERTIFICATE**/
				/*AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk());
				accreditationOperationTO.setCertificationType(accreditationOperation.getCertificationType());
				this.accreditationfacadeBean.generateConstance(accreditationOperationTO);*/
				
			}else if(accreditationState.equals(AccreditationOperationStateType.REJECTED.getCode())){
				JSFUtilities.executeJavascriptFunction("PF('acreditRejectEnd').hide();");
				JSFUtilities.executeJavascriptFunction("PF('acreditRejectSuccess').show();");
			}
			
			// INICIO ENVIO DE NOTIFICACIONES						
			Object[] parameters = {	String.valueOf(accreditationOperationToChange.getCustodyOperation().getOperationNumber()) };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES
			
			this.clearAccreditationOperation();
			this.accreditationConsult();
			this.setButtons();
			
		} catch (Exception e) {
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				if(se.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
										PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage()));
					JSFUtilities.executeJavascriptFunction("PF('acreditRejectEnd').hide();");
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		
	}// end changeAccreditationStatus
	
	/**
	 * Change accreditation status massive.
	 *
	 * @param accreditationOperations the accreditation operations
	 * @param accreditationState the accreditation state
	 * @throws InterruptedException the interrupted exception
	 */
	@LoggerAuditWeb
	public void changeAccreditationStatusMassive(List<AccreditationOperation> accreditationOperations, Integer accreditationState) throws InterruptedException {
		try {
			BusinessProcess businessProcess = new BusinessProcess();
			
			if(accreditationState.equals(this.getConfirmededTypeCode())){
				accreditationfacadeBean.confirmAccreditationRequestMassive(accreditationOperations, accreditationState);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_CONFIRM.getCode());
				/*// GENERATING CERTIFICATE
				List<AccreditationOperationTO> accreditationOperationTOs = new ArrayList<>();
				for(AccreditationOperation accreditationOperation : accreditationOperations){
					AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
					accreditationOperationTO.setIdAccreditationOperationPk(accreditationOperation.getCustodyOperation().getIdCustodyOperationPk());
					accreditationOperationTO.setCertificationType(accreditationOperation.getCertificationType());
					accreditationOperationTOs.add(accreditationOperationTO);						
				}
				this.accreditationfacadeBean.generateConstanceMassive(accreditationOperationTOs);*/
				
			}else if(accreditationState.equals(this.getRejectedTypeCode())){
				for(AccreditationOperation ao : accreditationOperations){
					ao.setIdRejectMotive(this.getIdRejectMotiveOperationsSelected());
					ao.setRejectMotiveOther(this.getRejectMotiveOtherOperationsSelected());
				}
				accreditationfacadeBean.rejectAccreditationRequestMassive(accreditationOperations, accreditationState);
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_REJECT.getCode());
			}
			
			if(accreditationState.equals(this.getConfirmededTypeCode())){
				JSFUtilities.executeJavascriptFunction("PF('acreditConfirm').hide();");
				JSFUtilities.executeJavascriptFunction("PF('acreditationSuccess').show();");
			}else if(accreditationState.equals(this.getRejectedTypeCode())){
				JSFUtilities.executeJavascriptFunction("PF('acreditRejectEnd').hide();");
				JSFUtilities.executeJavascriptFunction("PF('acreditRejectSuccess').show();");
			}
			
			// INICIO ENVIO DE NOTIFICACIONES	
			String strOperation= GeneralConstants.EMPTY_STRING;
			for(AccreditationOperation objAccreditationOperation : accreditationOperations){
				if (GeneralConstants.EMPTY_STRING.equalsIgnoreCase(strOperation)) {
					strOperation += objAccreditationOperation.getCustodyOperation().getOperationNumber();
				} else {
					strOperation += GeneralConstants.STR_COMMA + objAccreditationOperation.getCustodyOperation().getOperationNumber();
				}
			}										
			Object[] parameters = {	strOperation };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES						
			
		} catch (Exception e) {
			if (e instanceof ServiceException) {
				ServiceException se = (ServiceException) e;
				if(se.getErrorService()!=null){
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
										PropertiesUtilities.getExceptionMessage(se.getErrorService().getMessage()));
					JSFUtilities.executeJavascriptFunction("PF('acreditRejectEnd').hide();");
					JSFUtilities.showSimpleValidationDialog();
				}
			}
		}
		
	}// end changeAccreditationStatus
	
	//Setting a List of holderAccountIds from holderAccount Selected to search	
	/**
	 * Sets the holder accounts id selected.
	 *
	 * @param holderAccountList the holder account list
	 * @return the list
	 */
	public List<Long>setHolderAccountsIdSelected(List<HolderAccount>holderAccountList){
		// List of IDs
		List<Long> idHolderAccounts = new ArrayList<Long>();
		if(Validations.validateIsNotNullAndNotEmpty(holderAccount) 
			&& Validations.validateIsNotNullAndNotEmpty(holderAccount.getIdHolderAccountPk())) {
			idHolderAccounts.add(holderAccount.getIdHolderAccountPk());
		}
//		for (HolderAccount hAccount : holderAccountList) {
//			if (Validations.validateIsNotNull(hAccount.getSelected()) && hAccount.getSelected()) {
//				idHolderAccounts.add(hAccount.getIdHolderAccountPk());
//			}
//		}
		return idHolderAccounts;
	}//end setHolderAccountsIdSelected
	
	// Finding holding account balance
	/**
	 * Find holder account balance.
	 * Method that find hHolder account balance from accounts selected
	 */
	@LoggerAuditWeb
	public void findHolderAvailableAccountBalance() {
		JSFUtilities.hideGeneralDialogues();
		//Cleaning BlockOperationDetail object
		blockOperations = null;				
		// Validating there is not accounts
//		if (Validations.validateListIsNotNullAndNotEmpty(holderAccounts)) {	
			//Setting accounts id
			List<Long> holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
//			if(Validations.validateListIsNullOrEmpty(holderAccountIds)){
//				//Error, not account selected
//			     showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//			    		 ,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
//			     JSFUtilities.showSimpleValidationDialog();
//				 return;
//			}
			
			//Searching by account selected
			try{
				Security objSec= accreditationDetail.getSecurity();
				//clean list send
				
				List<String> lstSecurities = null;
				tempAccountBalance = new ArrayList<HolderAccountBalance>();
				tempAccountBalance= accreditationfacadeBean.searchHolderAccountBalance(holderAccountIds,objSec,userInfo,lstSecurities,accreditationOperation.getMotive());
				
				for (HolderAccountBalance balance : tempAccountBalance) {
					balance.setTransitBalance(new BigDecimal(0));
					balance.setBanBalance(new BigDecimal(0));
					balance.setOtherBlockBalance(new BigDecimal(0));
					balance.setSaleBalance(new BigDecimal(0));
					balance.setPurchaseBalance(new BigDecimal(0));
					balance.setReportedBalance(new BigDecimal(0));
					balance.setReportingBalance(new BigDecimal(0));
					balance.setMarginBalance(new BigDecimal(0));
					balance.setAccreditationBalance(new BigDecimal(0));
				}
				
				if(Validations.validateListIsNotNullAndNotEmpty(tempAccountBalance)){
					if(Validations.validateIsNotNullAndNotEmpty(objSec.getIdSecurityCodePk())){
						HolderAccountBalance hab = tempAccountBalance.get(0);
						if(hab.getAvailableBalance().intValue()>0){
							holderAccountBalances = new GenericDataModel<HolderAccountBalance>(tempAccountBalance);
						}else{
							showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
												PropertiesUtilities.getMessage(PropertiesConstants.MSGE_ACCREDITATION_CERTIFICATE_WRONG_BALANCE));
						     JSFUtilities.showSimpleValidationDialog();
						}
					}else{
						holderAccountBalances = new GenericDataModel<HolderAccountBalance>(tempAccountBalance);
					}
				}else{
					holderAccountBalances = new GenericDataModel<HolderAccountBalance>();
				}	
			}catch(Exception e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
			
//		}
		
	}
	
	/**
	 * Find holder blocked account balance.
	 * Method that find holder account balance from the holder accounts selected
	 */
	@LoggerAuditWeb
	public void findHolderBlockedAccountBalance() {
		JSFUtilities.hideGeneralDialogues();		 	
		// Validating there is not accounts
//		if (Validations.validateListIsNotNullAndNotEmpty(holderAccounts)) {
			List<Long> holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
//			if(Validations.validateListIsNullOrEmpty(holderAccountIds)){
//				//Error, not account selected
//			     showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//			    		 			,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
//			     JSFUtilities.showSimpleValidationDialog();
//				 return;
//			}
			//Searching by account selected
			try{
				Security objSec= accreditationDetail.getSecurity();
				// Searching..
				blockOperationTempList = new ArrayList<BlockOperation>();
				List<String> lstSecurities = null;
				blockOperationTempList = (accreditationfacadeBean.searchBlockOperationDetail(holder, holderAccountIds, objSec,
																												  blockRequest.getBlockType(), 
																												  blockRequest.getBlockEntity().getIdBlockEntityPk(),userInfo,lstSecurities));
			
				//Si tenemos valores con saldo bloqueado llenamos la lista
				if(Validations.validateListIsNotNullAndNotEmpty(blockOperationTempList)){					
					blockOperations = new GenericDataModel<BlockOperation>(blockOperationTempList);
				}
				
			}catch(Exception e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}			
//		}						
	}

	/**
	 * Metodo del boton buscar de la pantalla de registro de CAT
	 */
	@LoggerAuditWeb
	public void findHolderReportedAccountBalance() {
		
		JSFUtilities.hideGeneralDialogues();		
			try{
				Security objSec = accreditationDetail.getSecurity();
				//clean list send
				setAccreditationDetails(new ArrayList<AccreditationDetail>());
				setReportedOperationTOList(new ArrayList<MechanismOperationTO>());
				blockOperationList = new ArrayList<BlockOperation>();
				reportedBalanceTO=null;
				holderAccountBalancesExp=null;
				
				List<Long> holderAccountIds = null;
				// Validamos que tenga cuentas titulares seleccionadas
				if (Validations.validateListIsNotNullAndNotEmpty(holderAccounts)) {	
					
					//Establecemos las cuentas seleccionadas
					holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
					if(Validations.validateListIsNullOrEmpty(holderAccountIds)){

						//Enviamos mensaje de error en caso que no tenga cuentas seleccionadas
							  showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							  	 ,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
							   JSFUtilities.showSimpleValidationDialog();
						return;
					}
				}else{
					//Si no seleccionamos ninguna cuenta enviamos mensaje de alerta
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						  	 ,PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
						   JSFUtilities.showSimpleValidationDialog();
						   return;
				}
				
				//Si es CAT , mantenemos la busqueda 
				if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
					
					//Buscamos saldo Disponibles
					findHolderAvailableAccountBalance();
					
					//Buscamos saldo Bloqueado
					findHolderBlockedAccountBalance();
					
					Date expDate = CommonsUtilities.truncateDateTime(accreditationOperation.getExpirationDate());
					
					//Vaciamos lista de valores con saldo en reporto
					lstReportedBalance = new ArrayList<MechanismOperationTO>();
					//List<String> lstSecurities = null; //antes enviaba esto pero siempre inicia null
					lstReportedBalance = accreditationfacadeBean.searchReportedBalance(holderAccountIds, objSec, expDate, userInfo,null);
					
					//Si tenemos valores con saldo en reporto llenamos o no la grilla 
					if(Validations.validateListIsNotNullAndNotEmpty(lstReportedBalance)){
						reportedBalanceTO = new GenericDataModel<MechanismOperationTO>(lstReportedBalance);
					}else{
						reportedBalanceTO = new GenericDataModel<MechanismOperationTO>();
					}
					
				}else{
					//Buscamos saldo para CAT B
					Long idParticipantPk=null;
					if(Validations.validateIsNotNullAndNotEmpty(accreditationOperation.getParticipantPetitioner())) {
						idParticipantPk = accreditationOperation.getParticipantPetitioner().getIdParticipantPk();
					}
					lstHolderAccountBalanceExp = accreditationfacadeBean.searchHolderAccountBalanceB(holderAccountIds,
							idParticipantPk,
							objSec);
					//Si tenemos resultados para CAT B, lleganos el dataModel
					if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalanceExp)){
						holderAccountBalancesExp = new GenericDataModel<HolderAccountBalanceExp>(lstHolderAccountBalanceExp);
					}else{
						holderAccountBalancesExp = new GenericDataModel<HolderAccountBalanceExp>();
					}
				}
				
			}catch(Exception e){
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		
	}
	
	/**
	 * Metodo para realizar la busqueda de valores para registrar
	 */
	@LoggerAuditWeb
	public void searchSecurities(){
		JSFUtilities.hideGeneralDialogues();				
		
		//Si es un cat normal no puede tener la fecha de expiracion vacia 
		if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())
			&& accreditationOperation.getExpirationDate().equals(null)){
			return;
		}
		
		//Buscamos segun los filtros y llenamos la grilla
		findHolderReportedAccountBalance();
	}
	
	/**
	 * View document.
	 */
	public void viewDocument(){
		this.setBlockOperationSelected(blockOperations.getRowData());
		JSFUtilities.executeJavascriptFunction("PF('idDocumentDetail').show()");
	}
	
	/**
	 * View document blocked.
	 *
	 * @param blockOperationSelected the block operation selected
	 * @throws ServiceException the service exception
	 */
	public void viewDocumentBlocked(BlockOperation blockOperationSelected) throws ServiceException{
		
//			Map<Integer, Object> mapParametersDesc = new HashMap<>();
//			ParameterTableTO parameterTO = new ParameterTableTO();
//			parameterTO.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
//			for(ParameterTable pm: generalParametersFacade.getListParameterTableServiceBean(parameterTO))
//			mapParametersDesc.put(pm.getParameterTablePk(),pm.getDescription());
//			
		this.setBlockOperationSelected(blockOperationSelected);
		JSFUtilities.executeJavascriptFunction("PF('idDocumentDetail').show()");
	}
	
	/**
	 * Creates the accreditation.
	 */
	public void createAccreditation(){
		JSFUtilities.hideGeneralDialogues();
		hideDialogsFromNew();
//		if(validateFieldsSaveAccreditation().compareTo(GeneralConstants.EMPTY_STRING) == 0){
			if(availableBalanceCode.equals(accreditationOperation.getCertificationType())){
				confirmSaveAvailable();
			}else if(blockedBalanceCode.equals(accreditationOperation.getCertificationType())){
				confirmSaveBlocked();
			}else{
				confirmSaveReported();
			}
//		}else{
//			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), validateFieldsSaveAccreditation());
//			JSFUtilities.showSimpleValidationDialog();
//		}		
	}
	
	/**
	 * Confirm save.
	 * Method that show dialog when the accreditation is saved or not
	 */
	public void confirmSaveAvailable() {
		JSFUtilities.hideGeneralDialogues();				
//		if(Validations.validateIsNull(accreditationOperation.getAccreditationCertificate()) &&
//			!PetitionerType.PARTICIPANT.getCode().equals(accreditationOperation.getPetitionerType()) &&
//			!accreditationOperation.getPetitionerType().equals(PetitionerType.HOLDER.getCode())){
//			String message=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_SUSTENTION_SELECTED);
//			JSFUtilities.addContextMessage("contentForm:fuplCertificateIssuance",FacesMessage.SEVERITY_ERROR, message, message);
//			return;
//		}
//		if(!validateIncharge()){
//			return;
//		}
//		if(Validations.validateIsNull(accreditationOperation.getWayOfPayment())){
//			String message=PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_MESSAGE);
//			JSFUtilities.addContextMessage("contentForm:paymentWay",FacesMessage.SEVERITY_ERROR, message, message);
//			return;
//		}
		if(Validations.validateIsNull(holderAccountBalances)){
			String message=PropertiesUtilities.getMessage(PropertiesConstants.ACCREDITATION_SEARCH_BEFORE_SAVE);
	        showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
		    JSFUtilities.showSimpleValidationDialog();
			return;
		}
		else if(Validations.validateIsNotNull(holderAccountBalances)){
			boolean emptyQuantity=false;
			for(HolderAccountBalance hab : holderAccountBalances){
				if(hab.getSelected() && Validations.validateIsNullOrEmpty(hab.getTransferAmmount())){
					emptyQuantity = true;
				//	break;
				}
			}
			
			if(emptyQuantity){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("error.accreditation.emptyQuantity"));
			JSFUtilities.showSimpleValidationDialog();
			return;
			}
		}
		if (Validations.validateListIsNotNullAndNotEmpty(accreditationDetails)) {
			List<Long> holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
			holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
			if(Validations.validateListIsNullOrEmpty(holderAccountIds)){
		        showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
		        		, PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
			    JSFUtilities.showSimpleValidationDialog();
				return;
			}
//			if(this.validateSecurityExpiration()){
//				return;
//			}				
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_ACCREDITATION_SAVE));			
			JSFUtilities.showComponent("contentForm:cnfIdCAskDialog");
		} 
		else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
	        		, PropertiesUtilities.getMessage(PropertiesConstants.NOTHING_TO_ACCREDITATE));
		    JSFUtilities.showSimpleValidationDialog();					
		}
	}
	
	/**
	 * Confirm save blocked.
	 * Method that open dialog to confirm or cancel blocked accreditation
	 */
	public void confirmSaveBlocked() {
		JSFUtilities.hideGeneralDialogues();
//		if(Validations.validateIsNull(accreditationOperation.getAccreditationCertificate()) && 
//			!PetitionerType.PARTICIPANT.getCode().equals(accreditationOperation.getPetitionerType()) &&
//			!accreditationOperation.getPetitionerType().equals(PetitionerType.HOLDER.getCode())){
//			String message=PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_SUSTENTION_SELECTED);
//			JSFUtilities.addContextMessage("contentForm:fuplCertificateIssuance",FacesMessage.SEVERITY_ERROR, message, message);
//			return;
//		}
//		if(!validateIncharge()){
//			return;
//		}
//		if(Validations.validateIsNull(accreditationOperation.getWayOfPayment())){
//			String message=PropertiesUtilities.getMessage(PropertiesConstants.REQUIRED_MESSAGE);
//			JSFUtilities.addContextMessage("contentForm:paymentWay",FacesMessage.SEVERITY_ERROR, message, message);
//			return;
//		}
		if (Validations.validateListIsNotNullAndNotEmpty(blockOperationList)) {
//			if(this.validateSecurityExpirationBlocked()){
//				return;
//			} 
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER)
					, PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_ACCREDITATION_SAVE));			
			JSFUtilities.showComponent("contentForm:cnfIdCAskDialog");
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
	        		, PropertiesUtilities.getMessage(PropertiesConstants.NOTHING_TO_ACCREDITATE));
		    JSFUtilities.showSimpleValidationDialog();		
		}		
	}// end confirmSaveBlocked
		
	/**
	 * Confirm save reported.
	 */
	public void confirmSaveReported() {
		JSFUtilities.hideGeneralDialogues();	
		
		//Vaidamos que al menos una lista tenga datos
		if (Validations.validateListIsNotNullAndNotEmpty(accreditationDetails) 
				|| Validations.validateListIsNotNullAndNotEmpty(reportedOperationTOList) 
				|| Validations.validateListIsNotNullAndNotEmpty(blockOperationList)
				|| Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountBalanceExp)
				) {			
			
			//Validamos que tenga seleccionado cuentas titulares
			List<Long> holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
			holderAccountIds = setHolderAccountsIdSelected(holderAccounts);
			
			if(Validations.validateListIsNullOrEmpty(holderAccountIds)){
		        showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
		        					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
			    JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			//Si es CAT B verificamos que tenga seleccionados registros de la grilla
			if(!accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
				
				//Verificamos que algun registro haya sido seleccionado del CAT B
				Integer count = 0;
				for (HolderAccountBalanceExp flter :lstHolderAccountBalanceExp ){
					if(flter.getSelected().equals(true)){
						count = count + 1;
						break;
					}
				}
				//Sin registros seleccionados enviamos mensaje
				if(count < GeneralConstants.ONE_VALUE_INTEGER){
					 showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
	        					PropertiesUtilities.getMessage(PropertiesConstants.ERROR_NOT_ACCOUNT_SELECTED));
					 JSFUtilities.showSimpleValidationDialog();
					 return;
				} 		
			} 
			//Mostramos mensaje con Si y No
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
								PropertiesUtilities.getMessage(PropertiesConstants.CONFIRM_ACCREDITATION_SAVE));			
			JSFUtilities.showComponent("contentForm:cnfIdCAskDialog"); //yes: saveAccreditation()
			
		}else if(Validations.validateIsNull(holderAccountBalances)){			
			String message=PropertiesUtilities.getMessage(PropertiesConstants.ACCREDITATION_SEARCH_BEFORE_SAVE);
	        showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), message);
		    JSFUtilities.showSimpleValidationDialog();
			return;
		}	
		else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
	        		, PropertiesUtilities.getMessage(PropertiesConstants.NOTHING_TO_ACCREDITATE));
		    JSFUtilities.showSimpleValidationDialog();		
		}							
	}

	/**
 * Hide dialogs from new.
 */
public void hideDialogsFromNew(){
		JSFUtilities.hideComponent("contentForm:cnfIdCAskDialog");
		JSFUtilities.hideComponent("contentForm:cnfIdEndDialog");
	}

	/**
	 * Hide dialogs from search.
	 */
	public void hideDialogsFromSearch(){
		JSFUtilities.hideComponent("frmMessages:errorAccountList");
	}

	/**
	 * Accredited document.
	 * Method that accredited blocked's documents
	 *
	 * @param objBO the obj bo
	 * @throws ServiceException the service exception
	 */
	public void acreditateDocument(BlockOperation objBO) throws ServiceException {
		List<BlockOperation> lstBlockOperation= new ArrayList<BlockOperation>();
		Boolean selected = blockOperations.getRowData().getSelected();
		if (selected) {
			lstBlockOperation.add(blockOperations.getRowData());
			//validate if there is some request which will be generated again
			BlockOperation bo= (BlockOperation) lstBlockOperation.get(0);			
			List <AccreditationOperation> objAO= accreditationfacadeBean.verifyAccreditationOperationCertificate(bo);
			Long idOperation= bo.getBlockRequest().getIdBlockRequestPk();
			Date ExpDate = CommonsUtilities.truncateDateTime(accreditationOperation.getExpirationDate());
			
			//verify balance of security
			securityInOtherBalance(objBO.getSecurities().getIdSecurityCodePk());
			
			//verify have not a amortization into life time of cat
			if ((!objBO.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())) &&
				 (verifyAmortizationCoupon(objBO.getSecurities().getIdSecurityCodePk(),ExpDate))){
					renderRequestFail= Boolean.TRUE;
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
										PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_AMORTIZATION_COUPON_MESSAGE));
					JSFUtilities.showSimpleValidationDialog();
					//refreshing grill
					blockOperations.getRowData().setSelected(Boolean.FALSE);
					JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
				
			}else{
			if ((objBO.getSecurities().getExpirationDate()!=null) && 
					(!objBO.getSecurities().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
					&& (objBO.getSecurities().getExpirationDate().before(ExpDate) || objBO.getSecurities().getExpirationDate().equals(ExpDate))) {
					Object[] partBody = {null};												
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							,PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_EXPIRATION_DATE_BEFORE_ACCREDITATION_EXPIRATION_DATE, partBody));													
					JSFUtilities.showSimpleValidationDialog();
					blockOperations.getRowData().setSelected(Boolean.FALSE);
					JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");						
			}else{
				if(objAO.size()!=0){
//					Long idRequest= bo.getCustodyOperation().getOperationNumber();
					renderRequestFail= Boolean.TRUE;
					Object[] partBody = {idOperation.toString(), objAO.get(0).getCustodyOperation().getOperationNumber().toString()};
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
										PropertiesUtilities.getMessage(PropertiesConstants.INVALID_REQUEST_BLOCKED_ACCREDITATION_CERTIFICATE_MESSAGE, partBody));
					JSFUtilities.showSimpleValidationDialog();//show(cnfHasPendingRequest)
					blockOperations.getRowData().setSelected(Boolean.FALSE);
					JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
				}else{
					blockOperationList.add(blockOperations.getRowData());
				}
				}
			
			}
		}else{
			blockOperationList.remove(objBO);
		}
	} // end acreditateDocument
	
	/**
	 * Acreditate document reported.
	 *
	 * @param objMO the obj mo
	 * @throws ServiceException the service exception
	 */
	public void acreditateDocumentReported(MechanismOperationTO objMO) throws ServiceException {
		List<MechanismOperationTO> lstMechanism= new ArrayList<MechanismOperationTO>();
		Boolean selected = reportedBalanceTO.getRowData().getSelected();
		// Validating if the check box is true
		if (selected) {
			//sending the object to verify...
			lstMechanism.add(reportedBalanceTO.getRowData());
			MechanismOperationTO moTO= (MechanismOperationTO) lstMechanism.get(0);
			//validate if there is some request which will be generated again
			AccreditationOperation objAO = accreditationfacadeBean.verifyAccreditationOperationReported(moTO);
			Long idOperation= moTO.getOperationNumber();
			Date dateExpiration = moTO.getTermSettlementDate();
			Date ExpDate = CommonsUtilities.truncateDateTime(accreditationOperation.getExpirationDate());
			
			//verify balance of security
			securityInOtherBalance(moTO.getIdSecurityCodePk());
			//verify have not a amortization into life time of cat
			
			if (!(accreditationfacadeBean.getSecurityType(moTO.getIdSecurityCodePk())==(InstrumentType.VARIABLE_INCOME.getCode())) &&
			 (verifyAmortizationCoupon(moTO.getIdSecurityCodePk(),ExpDate))){
				renderRequestFail= Boolean.TRUE;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_AMORTIZATION_COUPON_MESSAGE));
				JSFUtilities.showSimpleValidationDialog();
				//refreshing grill
				reportedBalanceTO.getRowData().setSelected(Boolean.FALSE);
				JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
			}else{
			if (dateExpiration.before(ExpDate)){	
				renderRequestFail= Boolean.TRUE;
				Object[] partBody = {dateExpiration};
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.INVALID_REQUEST_REPORTED_ACCREDITATION_DATE_EXPIRATION_MESSAGE, partBody));
				JSFUtilities.showSimpleValidationDialog();
				//refreshing grill
				reportedBalanceTO.getRowData().setSelected(Boolean.FALSE);
				JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
			}else if(objAO!=null){//custodyServiceConsumer
				Long idRequest= objAO.getCustodyOperation().getOperationNumber();
				renderRequestFail= Boolean.TRUE;
				Object[] partBody = {idOperation.toString(), idRequest.toString()};
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getMessage(PropertiesConstants.INVALID_REQUEST_REPORTED_ACCREDITATION_CERTIFICATE_MESSAGE, partBody));
				JSFUtilities.showSimpleValidationDialog();
				//refreshing grill
				reportedBalanceTO.getRowData().setSelected(Boolean.FALSE);
				JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
			}else{
				//add each record to the main list of reported balance to save
					reportedOperationTOList.add(reportedBalanceTO.getRowData());
				}
			}
		}else{
			// if the check box is false then let's remove the reportedBalance from the list
			reportedOperationTOList.remove(objMO);
		}
	}	

	/**
	 * Save Balance.
	 * Method that saves accreditation
	 * @return true, if successful
	 */
	@LoggerAuditWeb
	public void saveAccreditation(){
		hideDialogsFromNew();
		JSFUtilities.hideGeneralDialogues();
		
		//Si el tipo de solicitud es CAT verificamos campos
		if(accreditationOperation.getCatType().equals(CatType.CAT.getCode())){
			// Validate days 		
			Date emisionDate = accreditationOperation.getIssuanceDate();
			Date expirationDate = accreditationOperation.getExpirationDate();
			
			if(expirationDate == null || emisionDate == null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getMessage("error.accreditation.certificate.expirationDate"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			if(expirationDate != null && emisionDate != null && accreditationOperation.getValidityDays() != null){
				int daysVigent = CommonsUtilities.getDaysBetween(emisionDate, expirationDate);
				if(!accreditationOperation.getValidityDays().equals(new Integer(daysVigent))){
					accreditationOperation.setValidityDays(daysVigent);
				}			
			}
			
			//Registramos el CAT
			if(availableBalanceCode.equals(accreditationOperation.getCertificationType())){
				saveAvailableBalance();
			}else if(blockedBalanceCode.equals(accreditationOperation.getCertificationType())){
				saveBlockedBalance();
			}else{
				saveReportedBalance();
			}
		}else{
			//Registro de Solicitud CAT B
			saveBalanceB();
		}
		
		// INICIO ENVIO DE NOTIFICACIONES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER.getCode());
		Object[] parameters = {	String.valueOf(messageRegister) };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
		// FIN ENVIO DE NOTIFICACIONES
	}
	
	/**
	 * Save available balance.
	 */
	public void saveAvailableBalance() {		
		if (saveBalance()) {
			Object[] partBody = {String.valueOf(messageRegister)};
			if(availableBalanceCode.equals(accreditationOperation.getCertificationType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
						,PropertiesUtilities.getMessage(PropertiesConstants.SAVING_ACCREDITATION_CERTIFICATE_SUCCESS_MESSAGE, partBody));
				JSFUtilities.showComponent("contentForm:cnfIdEndDialog");
			}
			
			// INICIO ENVIO DE NOTIFICACIONES
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.ACCREDITATION_CERTIFICATE_REGISTER.getCode());
			Object[] parameters = {	String.valueOf(messageRegister) };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
			// FIN ENVIO DE NOTIFICACIONES
			
		}else{
			// Showing error Dialog for saving accreditation certificate
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage("error.accreditation.certificate.save_message_error"));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Save balance.
	 *
	 * @return true, if successful
	 */
	public boolean saveBalance() {
		CustodyOperation custodyOperation = new CustodyOperation();
		boolean saved = false;
		if(Validations.validateIsNotEmpty(accreditationDetails)){
			for(AccreditationDetail objAccreditationDetail : accreditationDetails){
				List<AccreditationDetail>  lstAd=new ArrayList<AccreditationDetail>();
				lstAd.add(objAccreditationDetail);
				accreditationOperation.setAccreditationDetails(lstAd);
				accreditationOperation.setListAccreditationDeliveryInformation(lstAccreditationDeliveryInformation);
				accreditationOperation.setAccreditationState(AccreditationOperationStateType.REGISTRED.getCode());		
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_ACCREDITATION.getCode());		
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				custodyOperation.setRegistryUser(this.userInfo.getUserAccountSession().getUserName());	
				custodyOperation.setState(accreditationOperation.getAccreditationState());
				accreditationOperation.setCustodyOperation(custodyOperation);
				if ((lstAccreditationDeliveryInformation.isEmpty())
						|| (lstAccreditationDeliveryInformation.size()==0 )){
					accreditationOperation.setIndDelivered(BooleanType.NO.getCode());
				}else{
					accreditationOperation.setIndDelivered(BooleanType.YES.getCode());
				}
				accreditationOperation.setIndGenerated(BooleanType.NO.getCode());
				accreditationOperation.setDestinationCertificate(BooleanType.NO.getCode());
				try {
					accreditationOperation = (AccreditationOperation) (accreditationfacadeBean.save(accreditationOperation));			
			        if (messageRegister==null){ messageRegister = accreditationOperation.getCustodyOperation().getOperationNumber().toString();
			        }else{ messageRegister=messageRegister + ", " + accreditationOperation.getCustodyOperation().getOperationNumber().toString();}
				}  catch (ServiceException e) {
					if(e.getErrorService()!=null){
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
											PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
						JSFUtilities.showComponent("contentForm:cnfIdEndDialog");
					}
				}
			}
			saved = true;
		}
		return saved;
	}

	/**
	 * Save blocked balance.
	 * Method that save blocked Operations
	 */
	public void saveBlockedBalance() {
		fillAccreditationDetailsBlocked();
		if (registerBlockedBalance()) {
			Object[] partBody = {String.valueOf(messageRegister)};
			if(blockedBalanceCode.equals(accreditationOperation.getCertificationType())){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.SAVING_ACCREDITATION_CERTIFICATE_SUCCESS_MESSAGE,partBody));
				JSFUtilities.showComponent("contentForm:cnfIdEndDialog");
			}			
		} else {
			// Showing error Dialog for saving accreditation certificate
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage("error.accreditation.certificate.save_message_error"));
			JSFUtilities.showSimpleValidationDialog();
		}
	} // and saveBlockedBalance
	
	/**
	 * Register blocked balance.
	 *
	 * @return true, if successful
	 */
	public boolean registerBlockedBalance() {
		boolean saved = false;
		if(Validations.validateIsNotEmpty(accreditationDetails)){
			for(AccreditationDetail objAccreditationDetail : accreditationDetails){
				List<AccreditationDetail>  lstAd=new ArrayList<AccreditationDetail>();
				lstAd.add(objAccreditationDetail);
				// Setting the detail's list
				accreditationOperation.setAccreditationDetails(lstAd);
				accreditationOperation.setListAccreditationDeliveryInformation(lstAccreditationDeliveryInformation);
				// Setting the status
				accreditationOperation.setAccreditationState(AccreditationOperationStateType.REGISTRED.getCode());
				// Creating Custody Operation Object
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_ACCREDITATION.getCode());		
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				custodyOperation.setRegistryUser(this.userInfo.getUserAccountSession().getUserName());	
				custodyOperation.setState(accreditationOperation.getAccreditationState());
				accreditationOperation.setCustodyOperation(custodyOperation);
				if ((lstAccreditationDeliveryInformation.isEmpty())
						|| (lstAccreditationDeliveryInformation.size()==0 )){
					accreditationOperation.setIndDelivered(BooleanType.NO.getCode());
				}else{
					accreditationOperation.setIndDelivered(BooleanType.YES.getCode());
				}
				accreditationOperation.setIndGenerated(BooleanType.NO.getCode());
				accreditationOperation.setDestinationCertificate(BooleanType.NO.getCode());
				// custodyOperation.setCustodyOperation(custodyOperation);
				// Saving
				try {
					accreditationOperation = (AccreditationOperation) (accreditationfacadeBean.saveBlocked(accreditationOperation));			
					if (messageRegister==null){messageRegister= accreditationOperation.getCustodyOperation().getOperationNumber().toString();
			        }else{messageRegister=messageRegister + ", " + accreditationOperation.getCustodyOperation().getOperationNumber().toString();}
				}  catch (ServiceException e) {
					if(e.getErrorService()!=null){
						
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
											PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
						JSFUtilities.showComponent("contentForm:cnfIdEndDialog");	
					}
				}
			}
			saved = true;
		}
		return saved;
	} // end saveBalance

	
	/**
	 * Metodo para registrar una solicitud de CAT B
	 */
	public void saveBalanceB(){

		//Lista de numeros de operacion para el mensaeje de confirmacion
		List<String> operationNumber = new ArrayList<>();
		
		//Itermos la lista para registrar los valores seleccionados
		for (HolderAccountBalanceExp accountBalanceExp : lstHolderAccountBalanceExp){
			if(accountBalanceExp.getSelected().equals(true)){
				
				//Establecemos estado inicial de la operacion
				accreditationOperation.setAccreditationState(AccreditationOperationStateType.REGISTRED.getCode());
				
				//Registramos el custodyOperation
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_ACCREDITATION.getCode());		
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				custodyOperation.setRegistryUser(this.userInfo.getUserAccountSession().getUserName());	
				custodyOperation.setState(accreditationOperation.getAccreditationState());
				accreditationOperation.setCustodyOperation(custodyOperation);
				
		        //Agregamos el AccreditationDetail
		        List <AccreditationDetail> accreditationDetails = new ArrayList<>();
		        
		        AccreditationDetail accreditationDetail = new AccreditationDetail();
		        
		        //Agegamos entidades relacionadas
		        HolderAccount holderAccount = new HolderAccount();
		        holderAccount.setIdHolderAccountPk(accountBalanceExp.getHolderAccount());
		        
		        Participant participant = new Participant();
		        participant.setIdParticipantPk(accountBalanceExp.getParticipant());
		        
		        Security security = new Security();
		        security.setIdSecurityCodePk(accountBalanceExp.getSecurity());
		        
		        accreditationDetail.setHolderAccount(holderAccount);
		        accreditationDetail.setIdParticipantFk(participant);
		        accreditationDetail.setSecurity(security);
		        accreditationDetail.setAccreditationOperation(accreditationOperation);
		        accreditationDetail.setHolder(accreditationOperation.getHolder());
		        accreditationDetail.setTotalBalance(accountBalanceExp.getTotalBalance());
		        accreditationDetail.setAvailableBalance(accountBalanceExp.getAvailableBalance());
		        accreditationDetail.setPawnBalance(accountBalanceExp.getPawnBalance());
		        accreditationDetail.setBanBalance(accountBalanceExp.getBanBalance());
		        accreditationDetail.setOtherBalance(accountBalanceExp.getOtherBlockBalance());
		        accreditationDetail.setReportingBalance(new BigDecimal(0));
		        
		        accreditationDetails.add(accreditationDetail);
		        accreditationOperation.setAccreditationDetails(accreditationDetails);
				
				try {
					//Registramos la solicitud de CAT B
					accreditationOperation = (AccreditationOperation) (accreditationfacadeBean.insertRequestCatB(accreditationOperation));
					
					operationNumber.add(accreditationOperation.getCustodyOperation().getOperationNumber().toString());
					
				} catch (ServiceException e) {
					// TODO ME FALTA ESTABLECER EXCEPCIONES
				}
			}
		}
		
		//Enviamos mensaje de exito con el numero de operacion de cada CAT
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
			,PropertiesUtilities.getMessage(PropertiesConstants.SAVING_ACCREDITATION_CERTIFICATE_SUCCESS_MESSAGE, new Object[]{StringUtils.join(operationNumber,",")}));
		JSFUtilities.showComponent("contentForm:cnfIdEndDialog");
	}
	
	/**
	 * Save reported balance.
	 */
	public void saveReportedBalance() {			
		if (saveReported()) {			
			Object[] partBody = {String.valueOf(messageRegister)};
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
				,PropertiesUtilities.getMessage(PropertiesConstants.SAVING_ACCREDITATION_CERTIFICATE_SUCCESS_MESSAGE, partBody));
			JSFUtilities.showComponent("contentForm:cnfIdEndDialog");
		}else{
			// Showing error Dialog for saving accreditation certificate
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getMessage("error.accreditation.certificate.save_message_error"));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Save reported.
	 *
	 * @return true, if successful
	 */
	public boolean saveReported(){
		if (blockOperationList.size() != 0) {
			fillAccreditationDetailsBlocked();
		}
		if(reportedOperationTOList.size() != 0){
			fillAccreditationDetailsReported();
		}
		boolean saved = false;
		
		if(Validations.validateIsNotEmpty(accreditationDetails)){
			for(AccreditationDetail objAccreditationDetail : accreditationDetails){
				List<AccreditationDetail>  lstAd=new ArrayList<AccreditationDetail>();
				lstAd.add(objAccreditationDetail);
				accreditationOperation.setAccreditationDetails(lstAd);
				accreditationOperation.setListAccreditationDeliveryInformation(lstAccreditationDeliveryInformation);
				accreditationOperation.setAccreditationState(AccreditationOperationStateType.REGISTRED.getCode());
				if ((lstAccreditationDeliveryInformation.isEmpty())
						|| (lstAccreditationDeliveryInformation.size()==0 )){
					accreditationOperation.setIndDelivered(BooleanType.NO.getCode());
				}else{
					accreditationOperation.setIndDelivered(BooleanType.YES.getCode());
				}
				accreditationOperation.setIndGenerated(BooleanType.NO.getCode());
				accreditationOperation.setDestinationCertificate(BooleanType.NO.getCode());
				// Creating Custody Operation Object
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setOperationType(ParameterOperationType.SECURITIES_ACCREDITATION.getCode());		
				custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
				custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
				custodyOperation.setRegistryUser(this.userInfo.getUserAccountSession().getUserName());	
				custodyOperation.setState(accreditationOperation.getAccreditationState());
				accreditationOperation.setCustodyOperation(custodyOperation);
				
				// Saving
				try {
					accreditationOperation = (AccreditationOperation) (accreditationfacadeBean.insertReported(accreditationOperation));

					if (messageRegister==null){messageRegister= accreditationOperation.getCustodyOperation().getOperationNumber().toString();
			        }else{messageRegister=messageRegister + ", " + accreditationOperation.getCustodyOperation().getOperationNumber().toString();}
					
				} catch (ServiceException e) {
					if (e.getErrorService() != null) {
						showMessageOnDialog(
								PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
						JSFUtilities.showComponent("contentForm:cnfIdEndDialog");
					}
				}
			}
			saved = true;
		}
		return saved;
	}		
	
	/**
	 * Show dialog error.
	 *
	 * @param message the message
	 * @param dialog the dialog
	 */
	public void showDialogMessage(String message, String dialog){
		showMessageOnDialog(null, null, message, null);
		JSFUtilities.executeJavascriptFunction(dialog +".show();");	
	}

	/**
	 * Show dialog message with param.
	 *
	 * @param message the message
	 * @param dialog the dialog
	 * @param bodyData the body data
	 */
	public void showDialogMessageWithParam(String message, String dialog, Object[] bodyData){
		  showMessageOnDialog(null, null,message,bodyData);
		  JSFUtilities.executeJavascriptFunction(dialog +".show();");	
	}

	/**
	 * Certificate file handler.
	 * 
	 * @param event
	 *            the event
	 */
	public void certificateFileHandler(FileUploadEvent event) {
		try {
			 String fDisplayName = fUploadValidateFile(event.getFile(), null,null,getfUploadFileDocumentsTypes());
			 uploadedFile = event.getFile();
			 if(fDisplayName!=null){
				issuanceCertFileNameDisplay=fDisplayName;
				accreditationOperation.setSustentFileName(fDisplayName);
				accreditationOperation.setAccreditationCertificate(event.getFile().getContents());
				
				FileAccreditationTO fileAccreditationTO  = new FileAccreditationTO();
				fileAccreditationTO.setDocumentFile(event.getFile().getContents());
				fileAccreditationTO.setFileName(fDisplayName);
				String extensionFile = event.getFile().getFileName().toLowerCase().substring(event.getFile().getFileName().lastIndexOf(".")+1,event.getFile().getFileName().length());
				fileAccreditationTO.setFileExtension(extensionFile.toUpperCase());
				Long file= event.getFile().getSize();
				Double fileMb = (Math.round((((file.doubleValue())/1024)/1024)*100.0)/100.0);
				fileAccreditationTO.setFile(fileMb.toString());
				listFileAccreditationTO = new ArrayList<FileAccreditationTO>();
				listFileAccreditationTO.add(fileAccreditationTO);
			 }
			//Setting download for accreditationCertificate
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}

	} // end certificateFileHandler
	
	/**
	 * Sustention file.
	 */
	public void sustentionFile(){
		FileAccreditationTO fileAccreditationTO  = new FileAccreditationTO();
		fileAccreditationTO.setFileName(accreditationOperationSelection.getSustentFileName());
		listFileAccreditationTO = new ArrayList<FileAccreditationTO>();
		listFileAccreditationTO.add(fileAccreditationTO);
	}

	/**
	 * Gets the document.
	 *
	 * @return the document
	 * @throws ServiceException the service exception
	 */
	public StreamedContent getDocument() throws ServiceException{
		//Setting download for accreditationCertificate					
			InputStream stream = new ByteArrayInputStream(accreditationfacadeBean.documentFile(accreditationOperationSelection.getIdAccreditationOperationPk()));
				if(Validations.validateIsNotNull(accreditationOperationSelection.getSustentFileName())){
					String fileExt = accreditationOperationSelection.getSustentFileName().split("\\.")[1];
					fileDownload = new DefaultStreamedContent(stream, fileExt, accreditationOperationSelection.getSustentFileName());
				}else{				
					fileDownload = new DefaultStreamedContent(stream, "pdf", "sustento");
				}
				return fileDownload;
	 }
	 
	 /**
 	 * Gets the accreditation certification.
 	 *
 	 * @param certificationId the certification id
 	 * @return the accreditation certification
 	 */
 	public StreamedContent getAccreditationCertification(Long certificationId){
		 try {
			 byte[] certificationPdf = this.accreditationfacadeBean.getAccreditationCertification(certificationId);			
			 if(Validations.validateIsNotNull(certificationPdf)){						
				 InputStream stream = new ByteArrayInputStream(certificationPdf);
				 fileDownload = new DefaultStreamedContent(stream, "pdf", "CertificadoAcreditacion"+certificationId+".pdf"); 
			 }
		 } catch (ServiceException e) {
			 excepcion.fire(new ExceptionToCatchEvent(e));
		 }
		 return fileDownload;
	 }
	 
	 /**
 	 * Gets the stream filter.
 	 *
 	 * @return the stream filter
 	 */
 	public StreamedContent getStreamFilter(){
		 StreamedContent streamedContentFile = null;
			try {
				InputStream inputStream = null;
				inputStream = new ByteArrayInputStream(accreditationOperation.getAccreditationCertificate());
				streamedContentFile = new DefaultStreamedContent(inputStream, null,	accreditationOperation.getSustentFileName());
				inputStream.close();
			} catch (Exception e) {
			    excepcion.fire(new ExceptionToCatchEvent(e));
			}
			return streamedContentFile;
	}
	 
	/**
	 * Removes the uploaded file handler.
	 *
	 * @param event the event
	 */
	public void removeUploadedFileHandler(ActionEvent event){
		try {
			accreditationOperation.setAccreditationCertificate(null);
			accreditationOperation.setSustentFileName(null);
			issuanceCertFileNameDisplay=null;
			listFileAccreditationTO = null;
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}
	
	/**
	 * Fill accreditation details reported.
	 */
	public void fillAccreditationDetailsReported(){
		int count = 0; // count number to get the accreditation from list
		boolean found = false; // boolean variable | if exists an accreditation
		BigDecimal bdSumTotalB=null;
		BigDecimal bdDefault= new BigDecimal(0);
		List<ReportAccreditation> lstReportAccreditation = new ArrayList<ReportAccreditation>();
		int i=0;
		for(MechanismOperationTO objMechanism: reportedOperationTOList){
			ReportAccreditation objRepAccre= new ReportAccreditation();
			count = 0;
			found = false;
			//each object in report accreditation will be a new record
			if(objMechanism.getStockQuantity().intValue()>0){
				objRepAccre.setReportBalance(objMechanism.getStockQuantity());
			}else{
				objRepAccre.setReportBalance(bdDefault);
			}
			HolderAccountOperation objHAO= new HolderAccountOperation();
			objHAO.setIdHolderAccountOperationPk(objMechanism.getIdHolderAccountOperation());
			objRepAccre.setHolderAccountOperationFk(objHAO);
			objRepAccre.setAccreditationDetailFk(objAccreditationDetail);
			// Validating if the accreditation detail already exist for this Account Number its a different document
			if (accreditationDetails!=null) {
				for (AccreditationDetail acd : accreditationDetails) {
					if(objMechanism.getIdHolderAccount().equals(acd.getHolderAccount().getIdHolderAccountPk()) 
							&& objMechanism.getIdParticipant().equals(acd.getIdParticipantFk().getIdParticipantPk())
							&& objMechanism.getIdSecurityCodePk().equals(acd.getSecurity().getIdSecurityCodePk())
							){
						found = true;
						break;
					}										
					++count;
				}
				if (!found) {
					objAccreditationDetail = new AccreditationDetail();
					sec= new Security();
					sec.setIdSecurityCodePk(objMechanism.getIdSecurityCodePk());
					objAccreditationDetail.setSecurity(sec);
					Participant objPart= new Participant();
					objPart.setIdParticipantPk(objMechanism.getIdParticipant());
					objAccreditationDetail.setIdParticipantFk(objPart);
					objAccreditationDetail.setHolder(holder);
					objAccreditationDetail.setAccreditationOperation(accreditationOperation);
					HolderAccount objHA= new HolderAccount();
					objHA.setIdHolderAccountPk(objMechanism.getIdHolderAccount());
					objHA.setAccountNumber(objMechanism.getAccountHolderNumber());
					objAccreditationDetail.setHolderAccount(objHA);
					
					objAccreditationDetail.setRole(objMechanism.getOperationPart());
					/*own sets of balance type "reported"*/
					objAccreditationDetail.setReportingBalance(objRepAccre.getReportBalance());
					objAccreditationDetail.setTotalBalance(objMechanism.getStockQuantity());
					//rest fields of balances
					objAccreditationDetail.setBanBalance(bdDefault);
					objAccreditationDetail.setPawnBalance(bdDefault);
					objAccreditationDetail.setOtherBalance(bdDefault);
					objAccreditationDetail.setAvailableBalance(bdDefault);
					//total sum of market fact to accredited
					bdSumTotalB=objAccreditationDetail.getAvailableBalance().add(objAccreditationDetail.getBanBalance().add(
								objAccreditationDetail.getPawnBalance().add(objAccreditationDetail.getOtherBalance().add(objAccreditationDetail.getReportingBalance()))));
					objAccreditationDetail.setTotalBalance(bdSumTotalB);
					//setting list of Reported Balance to Details
					
					lstReportAccreditation.add(objRepAccre);
					List<ReportAccreditation> lstReportAccreditationTemp = new ArrayList<ReportAccreditation>();;
					lstReportAccreditationTemp.add(lstReportAccreditation.get(i));
//					objAccreditationDetail.setAccreditationReported(lstReportAccreditation);
					objAccreditationDetail.setAccreditationReported(lstReportAccreditationTemp);
					objRepAccre.setAccreditationDetailFk(objAccreditationDetail);
					
					//send all object to the main list 
					accreditationDetails.add(objAccreditationDetail);
					i++;
				}else{
					//adding rest of fields(objAccreditationDetail)
					if(accreditationDetails.get(count).getReportingBalance().intValue()>0){
						accreditationDetails.get(count).setReportingBalance(accreditationDetails.get(count).getReportingBalance().add(objMechanism.getStockQuantity()));
					}else{
						accreditationDetails.get(count).setReportingBalance(objMechanism.getStockQuantity());
					}
					//if a list of available balance was added to objAccreditationDetail, then sum to the TOTAL
					BigDecimal currentTotalBalance= accreditationDetails.get(count).getTotalBalance().add(objMechanism.getStockQuantity());
					accreditationDetails.get(count).setTotalBalance(currentTotalBalance);
					objRepAccre.setAccreditationDetailFk(accreditationDetails.get(count));
					//add an object to the list of Reported Balance
					if(accreditationDetails.get(count).getAccreditationReported()!=null){
						accreditationDetails.get(count).getAccreditationReported().add(objRepAccre);
					}else{
						accreditationDetails.get(count).setAccreditationReported(new ArrayList<ReportAccreditation>());
						accreditationDetails.get(count).getAccreditationReported().add(objRepAccre);
					}
				}
			}
		}
	}
	
	/**
	 * Fill accreditation details blocked.
	 */
	public void fillAccreditationDetailsBlocked(){
		JSFUtilities.hideGeneralDialogues();
		int count = 0;
		int pawn=0;
		boolean found = false;
		BigDecimal bdSumTotalB=null;
		BigDecimal bdDefault= new BigDecimal(0);
		if(!blockedBalanceCode.equals(accreditationOperation.getCertificationType()) && holderAccountBalances != null){
			for(HolderAccountBalance hab: holderAccountBalances){
				if (hab.getSelected()){
					if (accreditationDetails.get(pawn).getPawnBalance().doubleValue() > 0){ 
						accreditationDetails.get(pawn).setPawnBalance(new BigDecimal(0));
						pawn++;
					}
				}
			}
		}
		List<BlockOperationCertification> blockOperationCertifications = new ArrayList<BlockOperationCertification>();
		if (blockOperationList.size() != 0) {
			int i=0;
			for (BlockOperation bOperation : blockOperationList) {
				count = 0;
				found = false;
				BlockOperationCertification blockOpCer = new BlockOperationCertification();
				blockOpCer.setBlockBalanceCertification(bOperation.getActualBlockBalance());	
				bOperation.setIdBlockOperationPk(bOperation.getCustodyOperation().getIdCustodyOperationPk());
				blockOpCer.setBlockOperation(bOperation);
				blockOpCer.setBlockType(bOperation.getBlockRequest().getBlockType());
				blockOpCer.setBlockLevel(bOperation.getBlockLevel());
				if (accreditationDetails.size() != 0) {
					for (AccreditationDetail acd : accreditationDetails) {
						if(bOperation.getHolderAccount().getIdHolderAccountPk().equals(acd.getHolderAccount().getIdHolderAccountPk()) 
								&& bOperation.getParticipant().getIdParticipantPk().equals(acd.getIdParticipantFk().getIdParticipantPk())
								&& bOperation.getSecurities().getIdSecurityCodePk().equals(acd.getSecurity().getIdSecurityCodePk())
								){
							found = true;
							break;
						}
						++count;
					}
				}
				if (!found) {//new record to details list
					objAccreditationDetail= new AccreditationDetail();
					// Setting Accreditation Operation to accreditation Detail
					objAccreditationDetail.setAccreditationOperation(accreditationOperation);
					// Setting Balances to accreditation Detail
					if (bOperation.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())) {
						// setting PawnBalance to accreditation Detail
						objAccreditationDetail.setPawnBalance(bOperation.getActualBlockBalance());
						objAccreditationDetail.setBanBalance(bdDefault);
						objAccreditationDetail.setOtherBalance(bdDefault);
					} else if (bOperation.getBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())) {
						// Setting Ban Balance to accreditation Detail
						objAccreditationDetail.setBanBalance(bOperation.getActualBlockBalance());
						objAccreditationDetail.setPawnBalance(bdDefault);
						objAccreditationDetail.setOtherBalance(bdDefault);
					} else {
						// Setting Other Balances to accreditation Detail
						objAccreditationDetail.setOtherBalance(bOperation.getActualBlockBalance());
						objAccreditationDetail.setBanBalance(bdDefault);
						objAccreditationDetail.setPawnBalance(bdDefault);
					}
					//rest fields of balances
					objAccreditationDetail.setAvailableBalance(bdDefault);
					objAccreditationDetail.setReportingBalance(bdDefault);
					//total sum of balances
					bdSumTotalB=objAccreditationDetail.getAvailableBalance().add(objAccreditationDetail.getBanBalance().add(
								objAccreditationDetail.getPawnBalance().add(objAccreditationDetail.getOtherBalance().add(objAccreditationDetail.getReportingBalance()))));
					objAccreditationDetail.setTotalBalance(bdSumTotalB);
					// Setting Holder to accreditation Detail
					objAccreditationDetail.setHolder(holder);
					// Setting Holder Account to accreditation Detail
					objAccreditationDetail.setHolderAccount(bOperation.getHolderAccount());
					// Setting IsinCode to accreditation Detail
					sec=new Security();
					sec.setIdSecurityCodePk(bOperation.getSecurities().getIdSecurityCodePk());
					objAccreditationDetail.setSecurity(sec);
					// Setting Participant to accreditation Detail
					objAccreditationDetail.setIdParticipantFk(bOperation.getParticipant());
					// Setting Accreditation detail to Block Operation Certification;
					blockOpCer.setAccreditationDetail(objAccreditationDetail);
					// Setting Block Operation Certification to Accreditation detail;
					blockOperationCertifications.add(blockOpCer);
					
					List<BlockOperationCertification> lstBlockOperationCertificationsTemp = new ArrayList<BlockOperationCertification>();;
					lstBlockOperationCertificationsTemp.add(blockOperationCertifications.get(i));
					
					objAccreditationDetail.setBlockOperationCertifications(lstBlockOperationCertificationsTemp);
					accreditationDetails.add(objAccreditationDetail);
					i++;
				} else {
					if (bOperation.getBlockRequest().getBlockType().equals(AffectationType.PAWN.getCode())) {
						// setting PawnBalance to accreditation Detail
							
						BigDecimal pawnBalance = new BigDecimal(accreditationDetails.get(count).getPawnBalance().doubleValue()
																+ bOperation.getActualBlockBalance().doubleValue());
						accreditationDetails.get(count).setPawnBalance(pawnBalance);
					} else if (bOperation.getBlockRequest().getBlockType().equals(AffectationType.BAN.getCode())) {
						//Setting Ban Balance to variable
						BigDecimal banBalance = new BigDecimal(accreditationDetails.get(count).getBanBalance().doubleValue()
															   + bOperation.getActualBlockBalance().doubleValue());
						// Setting Ban Balance to accreditation Detail
						accreditationDetails.get(count).setBanBalance(banBalance);
					} else {
						BigDecimal otherBalance = new BigDecimal(accreditationDetails.get(count).getOtherBalance().doubleValue()
																 + bOperation.getActualBlockBalance().doubleValue());
						// Setting Other Balances to accreditation Detail
						accreditationDetails.get(count).setOtherBalance(otherBalance);						
					}
					//total sum of balances
					bdSumTotalB=accreditationDetails.get(count).getAvailableBalance().add(accreditationDetails.get(count).getBanBalance().add(
								accreditationDetails.get(count).getPawnBalance().add(accreditationDetails.get(count).getOtherBalance())));
					accreditationDetails.get(count).setTotalBalance(bdSumTotalB);
					// Setting Accreditation detail to Block Operation Certification
					blockOpCer.setAccreditationDetail(accreditationDetails.get(count));
					if(accreditationDetails.get(count).getBlockOperationCertifications()!=null){
						accreditationDetails.get(count).getBlockOperationCertifications().add(blockOpCer);
					}else{
						accreditationDetails.get(count).setBlockOperationCertifications(new ArrayList<BlockOperationCertification>());
						accreditationDetails.get(count).getBlockOperationCertifications().add(blockOpCer);
					}
				}
			}
		} else {
			// Showing error dialog, when the user has hadn't selected a document to certificate
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					PropertiesUtilities.getMessage("error.accreditation.certificate.save_message_error"));
			JSFUtilities.showValidationDialog();
		}		
	}

	/**
	 * Accreditation detail.
	 * method that goes to the accreditation detail
	 *
	 * @param from the from
	 * @return the view String
	 */
	@LoggerAuditWeb
	public String accreditationDetail(String from) {
//		if (accreditationOperation.getCertificationType()!=null && accreditationOperation.getCertificationType().equals(availableBalanceCode)) {
//			loadAvailableAccreditation(from);
//		} else if(accreditationOperation.getCertificationType()!=null && accreditationOperation.getCertificationType().equals(blockedBalanceCode)) {			
//			loadBlockAccreditation(from);
//		}else{
			loadMixtoAccreditation(from);
//		}
		
		return "accreditattionCertificateDetail";
	}// end accreditationDetail

	/**
	 * Load available accreditation.
	 *
	 * @param from the from
	 */
	public void loadAvailableAccreditation(String from){
		holderAccounts = new ArrayList<HolderAccount>();
		lstAccreditationOperationTODelivers = null;
		Long accreditationOperationId = null;
		if(from.equals("click")){				
			accreditationOperationId = this.accreditationOperationDataModel.getRowData().getCustodyOperation().getIdCustodyOperationPk();				
		}else{
			accreditationOperationId = accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk();
		}
		try{
			AccreditationOperation accreditationOperationSelection = findAccreditationOperation(accreditationOperationId);
			listAccreditationOperationSelection.add(accreditationOperationSelection);

			if(Validations.validateIsNotNull(accreditationOperationSelection.getIndDelivered()) 
					&& accreditationOperationSelection.getIndDelivered().equals(1)){
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				
				accreditationOperationTO.setDeliveryDate(accreditationOperationSelection.getDeliveryDate());				
				lstAccreditationOperationTODelivers = accreditationOperationSelection.getListAccreditationDeliveryInformation();
			}
			//Find HolderAccounts
			if(accreditationOperationSelection.getPetitionerType().equals(PetitionerType.ISSUER.getCode())){
				holderAccounts = accreditationfacadeBean.searchHolderAccount(accreditationOperationSelection.getHolder());
				loadHolderAccountsToIssuerView();
			}else{
			holderAccounts = findHolderAccounts(accreditationOperationSelection);}	
			//Searching accreditation details
			accreditationDetails = accreditationfacadeBean.searchAccreditationDetailByAccreditationOperation(accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk());
			if(Validations.validateListIsNotNullAndNotEmpty(holderAccounts)){
				//Validating accounts selected				
				for(int i=0; i < holderAccounts.size();++i){
					for(int j=0;j<accreditationDetails.size();++j){
						if(holderAccounts.get(i).getAccountNumber().equals(accreditationDetails.get(j).getHolderAccount().getAccountNumber())){
							holderAccounts.get(i).setSelected(true);
						}
					}
				}
			}
		}catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load block accreditation.
	 *
	 * @param from the from
	 */
	public void loadBlockAccreditation(String from){
		holderAccounts = new ArrayList<HolderAccount>();
		lstAccreditationOperationTODelivers = null;
		Long accreditationOperationId = null;
		if(from.equals("click")){
			AccreditationOperation accreditationOperationtmp = this.accreditationOperationDataModel.getRowData();
			if(accreditationOperationtmp.getPetitionerType().equals(this.getCreditorPetitioner())){
				BlockEntity blockEntityTmp = new BlockEntity();
				blockEntityTmp.setIdBlockEntityPk(accreditationOperationtmp.getIdBlockEntity());
				blockEntityTmp.setFullName(accreditationOperationtmp.getBlockEntityName());
				this.getBlockRequest().setBlockEntity(blockEntityTmp);
			}
			accreditationOperationId = this.accreditationOperationDataModel.getRowData().getCustodyOperation().getIdCustodyOperationPk();
			
		}else{
			accreditationOperationId = accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk();
		}
		try{
			// Searching the accreditationOperation Data
			AccreditationOperation accreditationOperationSelection = findAccreditationOperation(accreditationOperationId);
			listAccreditationOperationSelection.add(accreditationOperationSelection);
			
			if(Validations.validateIsNotNull(accreditationOperationSelection.getIndDelivered()) && accreditationOperationSelection.getIndDelivered().equals(1)){
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();					
				accreditationOperationTO.setDeliveryDate(accreditationOperationSelection.getDeliveryDate());				
				lstAccreditationOperationTODelivers = accreditationOperationSelection.getListAccreditationDeliveryInformation();
			}
			if(accreditationOperationSelection.getPetitionerType().equals(PetitionerType.ISSUER.getCode())){
				holderAccounts = accreditationfacadeBean.searchHolderAccount(accreditationOperationSelection.getHolder());
				loadHolderAccountsToIssuerView();
			}else{
			holderAccounts = findHolderAccounts(accreditationOperationSelection);}				
			List<BlockOperation> blockOperationDetailList = accreditationfacadeBean.findBlockOperationCertification(accreditationOperationId);
			//Validating Accounts Selected to show accounts selected in grid view
			if(holderAccounts!=null){
				for(int i=0; i < holderAccounts.size();++i){
					for(int j=0;j<blockOperationDetailList.size();++j){
						if(holderAccounts.get(i).getAccountNumber().equals(blockOperationDetailList.get(j).getHolderAccount().getAccountNumber())){
							holderAccounts.get(i).setSelected(true);
						}
					}
				}
			}
			blockOperations = new GenericDataModel<BlockOperation>(blockOperationDetailList);
		}catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}	
	}

	/**
	 * Load mixto accreditation.
	 *
	 * @param from the from
	 */
	public void loadMixtoAccreditation(String from){
		holderAccounts = new ArrayList<HolderAccount>();
		lstAccreditationOperationTODelivers = null;
		Long accreditationOperationId = null;
		if(from.equals("click")){
			accreditationOperationId = this.accreditationOperationDataModel.getRowData().getCustodyOperation().getIdCustodyOperationPk();
		}else{
			accreditationOperationId = accreditationOperationSelection.getCustodyOperation().getIdCustodyOperationPk();
		}
		try{
			listAccreditationOperationSelection = new ArrayList<>();
			accreditationOperationSelection = findAccreditationOperation(accreditationOperationId);
			//listAccreditationOperationSelection.add(accreditationOperationSelection);
			
			//En caso de CAT B, este campo es nulo
			if(accreditationOperationSelection.getCatType().equals(CatType.CAT.getCode())){
				if(accreditationOperationSelection.getValidityDays().equals(GeneralConstants.ZERO_VALUE_INTEGER)){
					accreditationOperationSelection.setValidityDays(GeneralConstants.ONE_VALUE_INTEGER);
				}
			}
			
			if (Validations.validateIsNotNull(this.getAccreditationOperationSelection().getSustentFileName())){
				sustentionDisabled = Boolean.TRUE;
				sustentionFile();
			}else{
				sustentionDisabled = Boolean.FALSE;
			}
			if(Validations.validateIsNotNull(accreditationOperationSelection.getIndDelivered())
					&& accreditationOperationSelection.getIndDelivered().equals(1)){
				AccreditationOperationTO accreditationOperationTO = new AccreditationOperationTO();
				accreditationOperationTO.setDeliveryDate(accreditationOperationSelection.getDeliveryDate());
				
				lstAccreditationOperationTODelivers = new ArrayList<AccreditationDeliveryInformation>();
				lstAccreditationOperationTODelivers = (accreditationOperationSelection.getListAccreditationDeliveryInformation());
			}
			//Find HolderAccounts
			if(accreditationOperationSelection.getPetitionerType().equals(PetitionerType.ISSUER.getCode())){
				issuerName=userInfo.getUserAccountSession().getIssuerCode();
				holderAccounts = accreditationfacadeBean.searchHolderAccount(accreditationOperationSelection.getHolder());
				loadHolderAccountsToIssuerView();
			}else{
				holderAccounts = findHolderAccounts(accreditationOperationSelection);
			}	
			List<HolderAccount> lstHolderAccountAux = null;
			//Searching accreditation details searchhAccreditationDetailMixed			
			accreditationDetails = accreditationfacadeBean.findAccreditationDetailMixed(accreditationOperationId);
			if(Validations.validateListIsNotNullAndNotEmpty(holderAccounts)){
				lstHolderAccountAux = new ArrayList<HolderAccount>();
				//Validating accounts selected				
				for(int i=0; i < holderAccounts.size();++i){
					for(int j=0;j<accreditationDetails.size();++j){
						if(holderAccounts.get(i).getAccountNumber().equals(accreditationDetails.get(j).getHolderAccount().getAccountNumber())){
							holderAccounts.get(i).setSelected(true);
							lstHolderAccountAux.add(holderAccounts.get(i));
						}
					}
				}
				holderAccounts = lstHolderAccountAux;
			}	
			
		}catch(ServiceException e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Load holder accounts to issuer view.
	 *
	 * @return the list
	 */
	public List<HolderAccount> loadHolderAccountsToIssuerView(){
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		Map<Integer,String> mapAccountStateDescription = new HashMap<Integer,String>();
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersService.getListParameterTableServiceBean(parameterTableTO)){
			mapAccountStateDescription.put(param.getParameterTablePk(), param.getParameterName());
		}			
			holderAccountsIssuer = holderAccounts;
			holderAccounts=null;
			holderAccounts=new ArrayList<HolderAccount>();
			String typer = userInfo.getUserAccountSession().getIssuerCode();
			Integer depositary = userInfo.getUserAccountSession().getInstitutionType();	
			for (int i = 0; i < holderAccountsIssuer.size(); i++) {
				if (holderAccountsIssuer.get(i).getParticipant().getIssuer().getIdIssuerPk()!=null){
					if(holderAccountsIssuer.get(i).getParticipant().getIssuer().getIdIssuerPk().equals(typer)){
						List<String> holderDescriptionListTwo = new ArrayList<String>();
						holderDescriptionListTwo.add(holderAccountsIssuer.get(i).getParticipant().getDescription());
						holderAccountsIssuer.get(i).setStateAccountDescription((mapAccountStateDescription.get(holderAccountsIssuer.get(i).getStateAccount())));
						holderAccountsIssuer.get(i).setHolderDescriptionList(holderDescriptionListTwo);
						holderAccounts.add(holderAccountsIssuer.get(i));
					}else{
						if(depositary.equals(InstitutionType.DEPOSITARY.getCode())){
							List<String> holderDescriptionListTwo = new ArrayList<String>();
							holderDescriptionListTwo.add(holderAccountsIssuer.get(i).getParticipant().getDescription());
							holderAccountsIssuer.get(i).setStateAccountDescription((mapAccountStateDescription.get(holderAccountsIssuer.get(i).getStateAccount())));
							holderAccountsIssuer.get(i).setHolderDescriptionList(holderDescriptionListTwo);
							holderAccounts.add(holderAccountsIssuer.get(i));
						}
					}
				}
		}
	return holderAccounts;
	}

	/**
	 * Go to.
	 * 
	 * @param viewToGo
	 *            the view to go
	 * @return the string
	 */
	public String goTo(String viewToGo) {
		this.clear();
		return viewToGo;
	} // end goTo
	
	/**
	 * Clear.
	 * Method that clean every field
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadRegistrationPage() throws ServiceException{
		this.setIssuanceCertFileNameDisplay(null);
		this.setAccreditationOperationSelection(null);				
		this.setHolderAccountBalances(null);
		this.setHolderAccounts(null);
		this.setHolder(new Holder());
		this.setSecurity(new Security());
		this.setBlockOperations(null);
		this.setAccreditationOperationDataModel(null);
		this.setBlockRequest(new BlockRequest());
		this.getBlockRequest().setBlockEntity(new BlockEntity());
		this.setParticipant(new Participant());		
		this.setAccreditationDetails(new ArrayList<AccreditationDetail>());
		this.getAccreditationOperation().setRegisterDate(getCurrentSystemDate());
		this.setAccreditationOperation(new AccreditationOperation());
		this.accreditationOperation.setRegisterDate(nowTime);
		accreditationOperation.setCatType(CatType.CAT.getCode());
		this.getAccreditationOperation().setIssuanceDate(nowTime);
		this.validateIsParticipantUser();
		this.getBlockRequest().setBlockEntity(new BlockEntity());
		this.setDisableBalanceType(Boolean.FALSE);
		this.setValidityDaysBoo(false);
		lstAccreditationOperationTODeliver = null;
		initialDate = getCurrentSystemDate();
		endDate = getCurrentSystemDate();	
		reportedBalanceTO=null;
		holderAccountBalancesExp=null;
		// messageRegister=null;
		setMessageRegister(null);
		issuerList();
		reportedOperationTOList=new ArrayList<MechanismOperationTO>();
		getAccreditationDetail().setSecurity(new Security());
		getAccreditationOperation().setMotive(null);
		this.getAccreditationOperation().setCertificationType(reportedBalanceCode);	
		if (userInfo.getUserAccountSession().getIssuerCode()!=null){
			getAccreditationOperation().setPetitionerType(PetitionerType.ISSUER.getCode());
			issuerName=userInfo.getUserAccountSession().getIssuerCode();
			isIssuer=true;
		}else{
			getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
			isIssuer=false;
			onChangePetitioner();
		}
		listFileAccreditationTO = new ArrayList<FileAccreditationTO>();
		loadSecurityClass();
		accreditationDeliveryInformation = new AccreditationDeliveryInformation();
		lstAccreditationDeliveryInformation = new ArrayList<AccreditationDeliveryInformation>();
	}
		
	/**
	 * Clear.
	 */
	public void clear(){	
		JSFUtilities.resetViewRoot();
		clearGrill();
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		this.listAccreditationOperationSelection = new ArrayList<>();
		lstAccreditationDeliveryInformation = new ArrayList<AccreditationDeliveryInformation>();
		Participant participantTO = new Participant();
		participantTO.setState(ParticipantStateType.REGISTERED.getCode());
		participantList = new ArrayList<Participant>();
		security = new Security();
		accreditationDeliveryInformation = new AccreditationDeliveryInformation();
		lstSecurityClass = new ArrayList<ParameterTable>();
		listSecurityClassSearch = new ArrayList<ParameterTable>();
		try {			
			participantList = accountsFacade.getLisParticipantServiceBean(participantTO);
			this.setBlockType(accreditationfacadeBean.getParameterList(MasterTableType.TYPE_AFFECTATION));
			this.setStateList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_CERTIFICATE_STATE));
			this.setPetitionerType(accreditationfacadeBean.getParameterList(MasterTableType.PETITIONER_TYPE));
			this.setBalanceType(accreditationfacadeBean.getParameterList(MasterTableType.TYPES_BALANCE_CREDIT));
			this.setSignatureCertificateList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_SIGNATURE));
			this.setRejectMotiveList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_REJECT_MOTIVE));
			this.setUnblockMotiveList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_UNBLOCK_MOTIVE));
			this.setDestinationDeliverList(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITATION_DESTINATION_DELIVER));
			listHolidays();
			this.setListMotives(accreditationfacadeBean.getParameterList(MasterTableType.ACCREDITACION_CERTIFICATION_MOTIVES));
			this.setListCurrency(accreditationfacadeBean.getParameterList(MasterTableType.CURRENCY));
			loadSecurityClassSearch();
			loadCurrencySearch();
//			this.setListSecurityClassSearch(accreditationfacadeBean.getParameterList(MasterTableType.SECURITIES_CLASS));
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
		this.setAccreditationOperation(new AccreditationOperation());
		this.accreditationOperation.setRegisterDate(nowTime);
		this.accreditationOperation.setIdAccreditationOperationPk(null);
//		this.getAccreditationOperation().setExpirationDate(this.holidayQueryServiceBean.getCalculateDate(nowTime,  3, 1,null));
		
		this.getBlockRequest().setBlockEntity(new BlockEntity());
		this.setHolder(new Holder());
		initialDate = getCurrentSystemDate();
		endDate = getCurrentSystemDate();	
		currency = null;
		securityClass=null;
		accreditationOperation.setCustodyOperation(new CustodyOperation());
		this.setDisableParticipantSelection(Boolean.FALSE);
		Integer typed = userInfo.getUserAccountSession().getInstitutionType();	  
		if(typed.equals(InstitutionType.ISSUER.getCode()) || typed.equals(InstitutionType.ISSUER_DPF.getCode())){
			loadSecurityClass();
		}else{
			loadSecurityClassSearch();
			loadCurrencySearch();
			getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
		}
		getAccreditationOperation().setCertificationType(reportedBalanceCode);
//		disableBalanceType = true;
		validateIsParticipantUser();		
		optionSelectedOneRadio = Integer.valueOf(0);
	}
	
	/**
	 * Load security class.
	 */
	private void loadSecurityClass(){
		try{
			lstSecurityClass = new ArrayList<ParameterTable>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			paramTable.setOrderByText1(1);
			for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
				Integer type = userInfo.getUserAccountSession().getInstitutionType();	  
				if(type.equals(InstitutionType.ISSUER.getCode()) || type.equals(InstitutionType.ISSUER_DPF.getCode())){
					if ((param.getParameterTablePk().equals(SecurityClassType.DPF.getCode()))
							|| (param.getParameterTablePk().equals(SecurityClassType.DPA.getCode())))
						lstSecurityClass.add(param);
						this.setListSecurityClassSearch(lstSecurityClass);
				}else{
					lstSecurityClass.add(param);
				}
			}
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Load security class search.
	 */
	private void loadSecurityClassSearch(){
		try{
			listSecurityClassSearch = new ArrayList<ParameterTable>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			paramTable.setOrderByText1(1);
			for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
				Integer type = userInfo.getUserAccountSession().getInstitutionType();	  
				if(type.equals(InstitutionType.ISSUER.getCode()) || type.equals(InstitutionType.ISSUER_DPF.getCode())){
					if ((param.getParameterTablePk().equals(SecurityClassType.DPF.getCode()))
							|| (param.getParameterTablePk().equals(SecurityClassType.DPA.getCode())))
						listSecurityClassSearch.add(param);
						this.setListSecurityClassSearch(listSecurityClassSearch);
				}else{
					listSecurityClassSearch.add(param);
				}
			}
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/**
	 * Load currency search.
	 */
	private void loadCurrencySearch(){
		try{
			listCurrency = new ArrayList<ParameterTable>();
			ParameterTableTO paramTable = new ParameterTableTO();
			paramTable.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
			paramTable.setOrderByText1(1);
			for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
				listCurrency.add(param);
			}
		}
		catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	/**
	 * Clear accreditation operation.
	 */
	public void clearAccreditationOperation(){
		Integer typed = userInfo.getUserAccountSession().getInstitutionType();	  
		if(!typed.equals(InstitutionType.ISSUER.getCode()) || !typed.equals(InstitutionType.ISSUER_DPF.getCode())){
			this.getAccreditationOperation().setPetitionerType(PetitionerType.PARTICIPANT.getCode());
		}
		validateIsParticipantUser();
		this.getAccreditationOperation().setIdSignatureCertificateFk(null);		
	}
	
	/**
	 * Sets the buttons.
	 */
	public void setButtons(){
		this.setConfirmButtonSelected(Boolean.FALSE);
		this.setRejectButtonSelected(Boolean.FALSE);
		this.setUnblockButtonSelected(Boolean.FALSE);
	}
	
	/**
	 * Clear search.
	 */
	public void clearSearch(){
		JSFUtilities.resetComponent("frmSearch");
		clear();
	}
	
	/**
	 * Clear register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void clearRegister() throws ServiceException{
		JSFUtilities.resetComponent("contentForm");
		loadRegistrationPage();
	}

	/**
	 * Clear grill.
	 */
	public void clearGrill() {
		// search screen
		accreditationOperationDataModel = null;
		this.listAccreditationOperationSelection = new ArrayList<>();
		// register screen
		holderAccountBalances = null;
		holderAccountBalancesExp = null;
		blockOperations = null;
		reportedBalanceTO = null;
		blockOperationList = new ArrayList<>();
		lstHolderAccountBalanceExp = new ArrayList<>();
		this.setAccreditationDetails(new ArrayList<AccreditationDetail>());	
	}
	
	/**
	 * Verify holder account.
	 */
	public void verifyHolderAccount(){
		clearGrill();		
		for (int i = 0; i < holderAccounts.size(); i++) {
			if ((!(holderAccounts.get(i).getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))) && (holderAccounts.get(i).getSelected())){
				holderAccounts.get(i).setSelected(false);
				showMessageOnDialog(null, null,PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE, null);
				JSFUtilities.updateComponent("contentForm:pnlHolderHelperPart");
				JSFUtilities.updateComponent("contentForm:errorAccountList");
				JSFUtilities.executeJavascriptFunction("errorAccountList.show()");
				return;
			}
		}
		if ((!(holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()))) ){
			holderAccount.setSelected(false);
			showMessageOnDialog(null, null,PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_REGISTERED_STATE, null);
			JSFUtilities.updateComponent("contentForm:pnlHolderHelperPart");
			JSFUtilities.updateComponent("contentForm:errorAccountList");
			JSFUtilities.executeJavascriptFunction("errorAccountList.show()");
			return;
		}
	}

	/**
	 * Go back.
	 *
	 * @param viewToGo the view to go
	 * @param aOperation the a operation
	 * @return the string
	 */
	public String goBack(String viewToGo, AccreditationOperation aOperation){
		String searchView = "";
		if(aOperation.getObservations() != null || aOperation.getHolder() != null ||
		   aOperation.getParticipantPetitioner() != null  || aOperation.getPetitionerType() != null ||
		   aOperation.getCertificationType() != null 
			){
			JSFUtilities.executeJavascriptFunction("goBackDialog.show()");			
		}else{
			searchView = viewToGo;
		}
		return searchView;
	}
	
	/**
	 * Back to search page handler.
	 */
	public void backToSearchPageHandler() {
		try {
			this.clear();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Find accreditation operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation
	 */
	@LoggerAuditWeb
	public AccreditationOperation findAccreditationOperation(Long accreditationOperationId){
		AccreditationOperation accreditationOperationFound = null;
		try {
			accreditationOperationFound = accreditationfacadeBean.findAccreditationOperation(accreditationOperationId);
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return accreditationOperationFound;
	}
	
	/**
	 * Find holder accounts.
	 *
	 * @param accrOperation the accr operation
	 * @return the list
	 */
	@LoggerAuditWeb
	public List<HolderAccount> findHolderAccounts(AccreditationOperation accrOperation){
		List<HolderAccount> holderAccountsSelected = new ArrayList<HolderAccount>();
		HolderTO holderTO = new HolderTO();
		try{
		if(accrOperation.getHolder()!=null && accrOperation.getHolder().getIdHolderPk()!=null){
			holderTO.setHolderId(accrOperation.getHolder().getIdHolderPk());
		}
		if(accrOperation.getParticipantPetitioner()!=null && accrOperation.getParticipantPetitioner().getIdParticipantPk()!=null){
			holderTO.setParticipantFk(accrOperation.getParticipantPetitioner().getIdParticipantPk());
		}
		holderTO.setFlagAllHolders(true);
		
		List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
		
		if(lstAccountTo!=null && lstAccountTo.size()>0){
			holderAccountsSelected = new ArrayList<HolderAccount>();
			HolderAccount ha;
			for(HolderAccountHelperResultTO hahrTO : lstAccountTo){
				ha = new HolderAccount();							
				ha.setAccountNumber(hahrTO.getAccountNumber());
				ha.setAlternateCode(hahrTO.getAlternateCode());
				ha.setAccountGroup(hahrTO.getAccountGroup());
				ha.setIdHolderAccountPk(hahrTO.getAccountPk());
				ha.setHolderDescriptionList(hahrTO.getHolderDescriptionList());
				ha.setStateAccountDescription(hahrTO.getAccountStatusDescription());
				holderAccountsSelected.add(ha);
			}
		}
			
		} catch(Exception e){
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return holderAccountsSelected;
	} // end findHolderAccounts
	
	/**
	 * Select market fact balance.
	 */
	public void selectMarketFactBalance(){
		JSFUtilities.hideGeneralDialogues();
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;		
		for(HolderAccountBalance hab: holderAccountBalances){			
			/*if(hab.getHolderAccount().getParticipant().getIdParticipantPk().equals(marketFactBalance.getParticipantPk()) &&
			   hab.getHolderAccount().getIdHolderAccountPk().equals(marketFactBalance.getHolderAccountPk()) &&
			   hab.getSecurity().getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
				//verify if the textBox was modified, it means there is almost a record selected
				if(!marketFactBalance.getBalanceResult().equals(BigDecimal.ZERO)){
					hab.setTransferAmmount(marketFactBalance.getBalanceResult());						
					accreditBalance(hab, marketFactBalance);
				}else{
								cleanRowForAccreditation(hab);
								removeAccreditationDetail(hab);
				}
				JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
				return;
			}*/
			
			accreditBalance(hab, marketFactBalance);
		}
		JSFUtilities.updateComponent("contentForm:tipoSaldoPanel");
		
	}
	
	/**
	 * Sets the market fact ui.
	 *
	 * @param objHolAccBal the new market fact ui
	 */
	public void setMarketFactUI(HolderAccountBalance objHolAccBal){		
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBal.getSecurity().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBal.getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBal.getHolderAccount().getParticipant().getIdParticipantPk());
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		if(objHolAccBal.getTransferAmmount()!=null && !objHolAccBal.getTransferAmmount().equals(BigDecimal.ZERO)){
			marketFactBalance.setBalanceResult(objHolAccBal.getTransferAmmount());
		}
		
		if(accreditationDetails!=null && accreditationDetails.size()>0){
		
			for(AccreditationDetail list : accreditationDetails){
			
				if(list.getHolderAccount().getIdHolderAccountPk().equals(objHolAccBal.getHolderAccount().getIdHolderAccountPk())
					&& list.getIdParticipantFk().getIdParticipantPk().equals(objHolAccBal.getHolderAccount().getParticipant().getIdParticipantPk())
					&& list.getSecurity().getIdSecurityCodePk().equals(objHolAccBal.getSecurity().getIdSecurityCodePk())){
				
				for (AccreditationMarketFact amf: list.getAccreditationMarketFactList()) {
				    MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
					marketDetail.setMarketFactBalancePk(amf.getIdAccreditationMF());
					if(objHolAccBal.getTransferAmmount()!=null){
						marketDetail.setEnteredBalance(objHolAccBal.getTransferAmmount());
					}else{
					marketDetail.setEnteredBalance(amf.getQuantityToAccredit());
					}
					marketDetail.setMarketDate(amf.getMarketDate());
					marketDetail.setMarketPrice(amf.getMarketPrice());
					marketDetail.setMarketRate(amf.getMarketRate());
					marketFactBalance.getMarketFacBalances().add(marketDetail);
				}
		      }
			}
		}
		
		try {
			holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
			
		selectMarketFactBalance();
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param objHolAccBal the obj hol acc bal
	 */
	public void showMarketFactUI(HolderAccountBalance objHolAccBal){		
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBal.getSecurity().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(objHolAccBal.getHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(objHolAccBal.getHolderAccount().getParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName("accreditBalanceMarketFact");
		marketFactBalance.setMarketFacBalances(new ArrayList<MarketFactDetailHelpTO>());
		
		
		if(accreditationDetails!=null && accreditationDetails.size()>0){
		
			for(AccreditationDetail list : accreditationDetails){
			
				if(list.getHolderAccount().getIdHolderAccountPk().equals(objHolAccBal.getHolderAccount().getIdHolderAccountPk())
					&& list.getIdParticipantFk().getIdParticipantPk().equals(objHolAccBal.getHolderAccount().getParticipant().getIdParticipantPk())
					&& list.getSecurity().getIdSecurityCodePk().equals(objHolAccBal.getSecurity().getIdSecurityCodePk())){
				
				for (AccreditationMarketFact amf: list.getAccreditationMarketFactList()) {
				    MarketFactDetailHelpTO marketDetail = new MarketFactDetailHelpTO();
					marketDetail.setMarketFactBalancePk(amf.getIdAccreditationMF());
					marketDetail.setEnteredBalance(amf.getQuantityToAccredit());
					marketDetail.setMarketDate(amf.getMarketDate());
					marketDetail.setMarketPrice(amf.getMarketPrice());
					marketDetail.setMarketRate(amf.getMarketRate());
					marketFactBalance.getMarketFacBalances().add(marketDetail);
				}
		      }
			}
		}
		else{
			if(objHolAccBal.getTransferAmmount()!=null && !objHolAccBal.getTransferAmmount().equals(BigDecimal.ZERO)){
				marketFactBalance.setBalanceResult(objHolAccBal.getTransferAmmount());
			}
		}
		
		showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
		JSFUtilities.updateComponent("contentForm:accreditBalanceMarketFact:dialogaccreditBalanceMarketFact");
	}
	
	/**
	 * Reset securities.
	 */
	public void resetSecurities(){		
		clearGrill();
		JSFUtilities.resetViewRoot();
		Integer securityClass = accreditationDetail.getSecurity().getSecurityClass();
		accreditationDetail.setSecurity(new Security());
		accreditationDetail.getSecurity().setSecurityClass(securityClass);
	}
	
	/**
	 * Show security financial data search.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialDataSearch(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);		
		JSFUtilities.updateComponent("frmSearch:detailSecurityHelp:securityDetailsecurityHelp");
	}
	
	/**
	 * Show security financial data.
	 *
	 * @param securityCode the security code
	 */
	public void showSecurityFinancialData(String securityCode){
		SecurityFinancialDataHelpTO securityData = new SecurityFinancialDataHelpTO();
		securityData.setSecurityCodePk(securityCode);
		securityData.setUiComponentName("securityHelp");
		showUIComponent(UICompositeType.SECURITY_FINANCIAL_DATA.getCode(),securityData);		
		JSFUtilities.updateComponent("contentForm:detailSecurityHelp:securityDetailsecurityHelp");
	}
	
	/**
	 * Clean row for accreditation.
	 *
	 * @param objHolAccBal the obj hol acc bal
	 */
	public void cleanRowForAccreditation(HolderAccountBalance objHolAccBal){
			if(objHolAccBal.getSelected()){
				securityInOtherBalance(objHolAccBal.getSecurity().getIdSecurityCodePk());
				Date ExpDate=null;
				if(accreditationOperation.getExpirationDate()!=null){
					ExpDate = CommonsUtilities.truncateDateTime(accreditationOperation.getExpirationDate());
				}
				
				//verify have not a amortization into life time of cat
				
				if ((!objHolAccBal.getSecurity().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode())) &&
					 (verifyAmortizationCoupon(objHolAccBal.getSecurity().getIdSecurityCodePk(),ExpDate))){
						Object[] partBody = {null};
						objHolAccBal.setTransferAmmount(null);
						removeAccreditationDetail(objHolAccBal);
						objHolAccBal.setSelected(false);
						
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
											PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_AMORTIZATION_COUPON_MESSAGE));
						JSFUtilities.showSimpleValidationDialog();
					
				}else{
				
				if (objHolAccBal.getSecurity().getExpirationDate()!=null &&
					(!objHolAccBal.getSecurity().getInstrumentType().equals(InstrumentType.VARIABLE_INCOME.getCode()))
					&&(ExpDate!= null &&
					(objHolAccBal.getSecurity().getExpirationDate().before(ExpDate) || objHolAccBal.getSecurity().getExpirationDate().equals(ExpDate)))){
						Object[] partBody = {null};
						objHolAccBal.setTransferAmmount(null);
						removeAccreditationDetail(objHolAccBal);
						objHolAccBal.setSelected(false);
						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								,PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_EXPIRATION_DATE_BEFORE_ACCREDITATION_EXPIRATION_DATE, partBody));
						JSFUtilities.showSimpleValidationDialog();
				
				}else{
					MarketFactBalanceTO marketFactBalanceTO = new MarketFactBalanceTO();
					marketFactBalanceTO.setSecurityCodePk(objHolAccBal.getSecurity().getIdSecurityCodePk());
					marketFactBalanceTO.setParticipantPk(objHolAccBal.getParticipant().getIdParticipantPk());
					marketFactBalanceTO.setHolderAccountPk(objHolAccBal.getHolderAccount().getIdHolderAccountPk());
					List<HolderMarketFactBalance> marketFactBalances = null;
					try {
						marketFactBalances = marketServiceBean.findMarketFactBalanceByAccreditationAccount(marketFactBalanceTO);
					} catch (ServiceException e) {
						e.printStackTrace();
					}
					if(objHolAccBal.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())
							|| objHolAccBal.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())
							|| (marketFactBalances!=null && marketFactBalances.size() == 1) ){
						objHolAccBal.setDisabled(false);
						objHolAccBal.setSelected(true);
						if(objHolAccBal.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())
								|| objHolAccBal.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()))
							objHolAccBal.setTransferAmmount(new BigDecimal(1));
						applyQuantityWithMarketFact(objHolAccBal);
						}
				}
				}
			}else{
				objHolAccBal.setTransferAmmount(null);
				removeAccreditationDetail(objHolAccBal);
			}
		}
	
	
	/**
	 * Recibe el pk del valor y verifica que no tenga una amortizacion dentro del plazo de vigencia
	 * @param idPkSecurity
	 * @param expDate
	 * @return
	 */
	public Boolean verifyAmortizationCoupon(String idPkSecurity,Date expDate) {
		//Oscar
		Date amortizationDate = new Date();
			try {
				amortizationDate=CommonsUtilities.truncateDateTime(accreditationfacadeBean.verifyAmortizationCoupon(idPkSecurity));
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		if(expDate.after(amortizationDate)){
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Recibe el pk del valor y verifica si se encuentra en otro tipo de saldo.
	 *
	 * @param idPk the id pk
	 */
	public void securityInOtherBalance(String idPk){
		int i =0;
		if(Validations.validateListIsNotNullAndNotEmpty(tempAccountBalance)){
			for (HolderAccountBalance lstAvailable : tempAccountBalance) {
				if(idPk.equals(lstAvailable.getSecurity().getIdSecurityCodePk().toString()) && 
						lstAvailable.getAvailableBalance().intValue()>0){
						i=i+1;		
						messageBalance=PropertiesUtilities.getMessage(PropertiesConstants.BALANCE_AVAILABLE_ACCREDITATION_CERTIFICATE_MESSAGE);	
					break;
				}
			}
		}
		if(Validations.validateListIsNotNullAndNotEmpty(blockOperationTempList)){
			for (BlockOperation lstBlocked : blockOperationTempList) {
				if(idPk.equals(lstBlocked.getSecurities().getIdSecurityCodePk().toString())){
					if(messageBalance==null){	
						i=i+1;
						messageBalance=PropertiesUtilities.getMessage(PropertiesConstants.BLOCK_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE);
					}else{
						i=i+1;
						messageBalance=messageBalance + ", "+ PropertiesUtilities.getMessage(PropertiesConstants.BLOCK_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE);
					}
					break;
				}
			}
		}
		if(Validations.validateListIsNotNullAndNotEmpty(lstReportedBalance)){
			for (MechanismOperationTO lstReported : lstReportedBalance) {
				if(idPk.equals(lstReported.getIdSecurityCodePk().toString())){
					if(messageBalance==null){
						i=i+1;
						messageBalance=PropertiesUtilities.getMessage(PropertiesConstants.REPORTER_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE);
					}else{
						i=i+1;
						messageBalance=messageBalance + ", "+ PropertiesUtilities.getMessage(PropertiesConstants.REPORTER_BALANCE_ACCREDITATION_CERTIFICATE_MESSAGE);
					}
					break;
				}
			}
		}
		if(i>1){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING)
					,PropertiesUtilities.getMessage(PropertiesConstants.REPEAT_SECURITY_ACCREDITATION_CERTIFICATE_MESSAGE, messageBalance));
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	/**
	 * Removes the accreditation detail.
	 *
	 * @param objHolAccBal the obj hol acc bal
	 */
	public void removeAccreditationDetail(HolderAccountBalance objHolAccBal){		
		for(Iterator<AccreditationDetail> iter = this.accreditationDetails.iterator(); iter.hasNext();){
			final AccreditationDetail accDetail = iter.next();
			if(objHolAccBal.getHolderAccount().getIdHolderAccountPk().equals(accDetail.getHolderAccount().getIdHolderAccountPk()) 
					&& objHolAccBal.getSecurity().getIdSecurityCodePk().compareTo(accDetail.getSecurity().getIdSecurityCodePk()) == 0 ){
				iter.remove();
			}
		}		
		
	}
	
	/**
	 * Valida transfer amount.
	 *
	 * @param objHolAccBal the obj hol acc bal
	 */
	public void validaTransferAmount(HolderAccountBalance objHolAccBal){
		if(Validations.validateIsNotNullAndNotEmpty(objHolAccBal.getTransferAmmount())){
			
		}
	}
	
	/**
	 * Onchange block type.
	 */
	public void onchangeBlockType(){
		clearGrill();
	}
	
	/**
	 * Accredit Available Balance.
	 * Method that accredited available balance
	 *
	 * @param holderAccountBalance the holder account balance
	 * @param marketList the market list
	 */
	public void accreditBalance(HolderAccountBalance holderAccountBalance, MarketFactBalanceHelpTO marketList) {						
		JSFUtilities.hideGeneralDialogues();	
		BigDecimal bdSumTotalB=null;
		if(holderAccountBalance.getTransferAmmount() == null){
			return;
		}
		if ((holderAccountBalance.getTransferAmmount().doubleValue() <= holderAccountBalance.getAvailableBalance().doubleValue())) {						
			if (holderAccountBalance.getTransferAmmount().doubleValue() >= 0) {	
				
				objAccreditationDetail = new AccreditationDetail();
				sec= new Security();
				sec.setIdSecurityCodePk(holderAccountBalance.getSecurity().getIdSecurityCodePk());
				objAccreditationDetail.setSecurity(sec);
				objAccreditationDetail.setAvailableBalance(holderAccountBalance.getTransferAmmount());
				objAccreditationDetail.setIdParticipantFk(holderAccountBalance.getHolderAccount().getParticipant());
				objAccreditationDetail.setHolder(holder);
				objAccreditationDetail.setAccreditationOperation(accreditationOperation);
				objAccreditationDetail.setHolderAccount(holderAccountBalance.getHolderAccount());
				
				objAccreditationDetail.setBanBalance(holderAccountBalance.getBanBalance());
				objAccreditationDetail.setPawnBalance(holderAccountBalance.getPawnBalance());
				objAccreditationDetail.setOtherBalance(holderAccountBalance.getOtherBlockBalance());
				objAccreditationDetail.setReportingBalance(holderAccountBalance.getReportingBalance());				
				bdSumTotalB=objAccreditationDetail.getAvailableBalance().add(objAccreditationDetail.getBanBalance().add(objAccreditationDetail.getPawnBalance().add(
							objAccreditationDetail.getOtherBalance().add(objAccreditationDetail.getReportingBalance()))));
				objAccreditationDetail.setTotalBalance(bdSumTotalB);							
			}						
			if(marketList != null){
				List<AccreditationMarketFact> lstAMF = new ArrayList<AccreditationMarketFact>();	
				objAccreditationDetail.setAccreditationMarketFactList(new ArrayList<AccreditationMarketFact>());
				for(MarketFactDetailHelpTO mkfDetTO : marketList.getMarketFacBalances()){
					AccreditationMarketFact marketFact = new AccreditationMarketFact();
					marketFact.setIdAccreditationMF(mkfDetTO.getMarketFactBalancePk());
					marketFact.setQuantityToAccredit(mkfDetTO.getEnteredBalance());
					marketFact.setMarketDate(mkfDetTO.getMarketDate());
					marketFact.setMarketPrice(mkfDetTO.getMarketPrice());
					marketFact.setMarketRate(mkfDetTO.getMarketRate());			
					marketFact.setAccreditationDetailFk(objAccreditationDetail);
					lstAMF.add(marketFact);					
				}				
				objAccreditationDetail.setAccreditationMarketFactList(lstAMF);
				Integer position = existAccreditationDetail(objAccreditationDetail, accreditationDetails);
				if(position!=null){
					accreditationDetails.set(position, objAccreditationDetail);
				}
				else{
					accreditationDetails.add(objAccreditationDetail);
				}
				
			} else {
				Integer position = existAccreditationDetail(objAccreditationDetail, accreditationDetails);
				if(position!=null){
					accreditationDetails.set(position, objAccreditationDetail);
				}
				else{
					accreditationDetails.add(objAccreditationDetail);
				}
			}
			
		} else {
			holderAccountBalances.getDataList().get(holderAccountBalances.getRowIndex()).setTransferAmmount(null);
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getMessage("error.accreditation.certificate.accreditation_balance"));
			JSFUtilities.showValidationDialog();
		}
	}
	
	/**
	 * Exist accreditation detail.
	 *
	 * @param acreditationDetail the acreditation detail
	 * @param listAcreditationDetail the list acreditation detail
	 * @return the integer
	 */
	public Integer existAccreditationDetail(AccreditationDetail acreditationDetail,List<AccreditationDetail> listAcreditationDetail){
		Integer position=null;
		for(int i=0;i<listAcreditationDetail.size();i++){
			if(listAcreditationDetail.get(i).getHolderAccount().getIdHolderAccountPk().equals(acreditationDetail.getHolderAccount().getIdHolderAccountPk())
					&& listAcreditationDetail.get(i).getIdParticipantFk().getIdParticipantPk().equals(acreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk())
					&& listAcreditationDetail.get(i).getSecurity().getIdSecurityCodePk().equals(acreditationDetail.getSecurity().getIdSecurityCodePk())){
				position = i;
			}
		}
		return position;
	}
	
	/**
	 * Change motive wayof payment.
	 */
	public void changeMotiveWayofPayment(){
		if(this.accreditationOperation.getMotive()==null){
			this.getAccreditationOperation().setWayOfPayment(null);
		}else{
			if(this.accreditationOperation.getMotive().equals(AccreditationOperationMotive.ECONOMIC.getCode())){
				this.getAccreditationOperation().setWayOfPayment(AccreditationPaymentWayType.FREE.getCode());
			}else{
				this.getAccreditationOperation().setWayOfPayment(AccreditationPaymentWayType.INVOICED_PARTICIPANT.getCode());
			}
		}
	}
	
	/**
	 * Issuer list.
	 *
	 * @throws ServiceException the service exception
	 */
	public void issuerList() throws ServiceException {
			this.setListIssuer(accreditationfacadeBean.issuerList());
	}
	
	/**
	 * On change issuer for search.
	 */
	public void onChangeIssuerForSearch(){
		JSFUtilities.hideGeneralDialogues();
//		if(this.getDisableParticipantSelection()){
//			return;
//		}						
//		this.setHolder(new Holder());
//		this.setHolderAccounts(new ArrayList<HolderAccount>());
//		this.getBlockRequest().setBlockEntity(new BlockEntity());
//		this.setDisableBalanceType(Boolean.TRUE);
//		resetCompSearch();
//		onChangeBlockedType();
//		this.getBlockRequest().setBlockType(null);
		clearGrill();
	}
	
	/**
	 * On change petitioner for search.
	 */
	public void onChangePetitionerForSearch(){
		JSFUtilities.hideGeneralDialogues();
		if(this.getDisableParticipantSelection()){
			return;
		}		
		accreditationOperation.setParticipantPetitioner(null); //needles				
		//	Set petitioner
		this.setParticipant(new Participant());
		this.setIssuerName(null);
		this.setHolder(new Holder());
		this.setHolderAccounts(new ArrayList<HolderAccount>());
		this.getBlockRequest().setBlockEntity(new BlockEntity());
		//	Disable field (Forma de cobro - Tipo de Saldo)
//		this.setDisableBalanceType(Boolean.FALSE);
//		this.setDisableBalanceType(Boolean.TRUE);
		// 	Is Creditor or Participant or holder
		this.getAccreditationOperation().setCertificationType(reportedBalanceCode);	
		
		if(this.getAccreditationOperation().getPetitionerType() != null){
			if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.CREDITOR.getCode())){
//				this.setDisableBalanceType(Boolean.TRUE);
				renderBlockedType = Boolean.TRUE;
				//Balance type blocked (Bloqueado)
				this.getAccreditationOperation().setCertificationType(blockedBalanceCode);
			}
//			else if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.PARTICIPANT.getCode())){			
//				this.setDisableBalanceType(Boolean.TRUE);			
				//Balance type reported (Mixto)
//				this.getAccreditationOperation().setCertificationType(reportedBalanceCode);				
//			}
//			else if(this.getAccreditationOperation().getPetitionerType().equals(PetitionerType.HOLDER.getCode())){
//				getAccreditationOperation().setCertificationType(reportedBalanceCode);
//			}else{								
//				getAccreditationOperation().setCertificationType(reportedBalanceCode);
//			}
			resetCompSearch();
			onChangeBlockedType();
			this.getBlockRequest().setBlockType(null);
			clearGrill();
		}		
	}
	
	/**
	 * Adds the in charge.
	 */
	public void addInCharge(){
		if(accreditationDeliveryInformation.getDocumentNumber()!=null && 
				accreditationDeliveryInformation.getFullName()!=null){
			JSFUtilities.resetViewRoot();
			accreditationDeliveryInformation.setAccreditationOperation(accreditationOperation);
			lstAccreditationDeliveryInformation.add(accreditationDeliveryInformation);
			accreditationDeliveryInformation = new AccreditationDeliveryInformation();
		}
	}
	
	/**
	 * Removes the in charge.
	 *
	 * @param accreditationDI the accreditation di
	 */
	public void removeInCharge(AccreditationDeliveryInformation accreditationDI){
		lstAccreditationDeliveryInformation.remove(accreditationDI);
		accreditationDeliveryInformation = new AccreditationDeliveryInformation();	
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":frmMotiveReject:dialogMotiveReject");
		JSFUtilities.hideComponent(":frmMotiveReject:dialogMotiveReject");	
	}
	
	/**
	 * Close dialog unblock motive.
	 */
	public void closeDialogUnblockMotive(){
//		JSFUtilities.resetComponent(":frmMotiveUnblock:dialogMotiveUnblock");
//		JSFUtilities.hideComponent(":frmMotiveUnblock:dialogMotiveUnblock");
//		
		JSFUtilities.resetComponent(":frmMotiveUnblock:dialogMotiveUnblock");
		JSFUtilities.hideGeneralDialogues();
	}
	
//	public void closeDialogRejectMotive(){
//		JSFUtilities.resetComponent(":frmMotive:dialogMotive");
//		hideLocalComponents();
//	}
	
	
	/**
 * Reset comp search.
 */
public void resetCompSearch(){
		JSFUtilities.resetComponent("frmSearch:balanceType");		
		JSFUtilities.resetComponent("frmSearch:blockEntityOutputPanel2");
		JSFUtilities.resetComponent("frmSearch:participant2");
		JSFUtilities.resetComponent("frmSearch:pnlHolderHelper");
	}
	
	/**
	 * View incharge detail.
	 *
	 * @param event the event
	 */
	public void viewInchargeDetail(ActionEvent event){
		AccreditationOperation accreditationOperation = (AccreditationOperation) (event.getComponent().getAttributes().get("attributeIncharge"));
		lstAccreditationDeliveryInformation = accreditationOperation.getListAccreditationDeliveryInformation();
		JSFUtilities.executeJavascriptFunction("widgViewInchargeDialog.show()");
	}
	
	/**
	 * Search list block entity for Accreditation.
	 */
	public void searchListBlockEntityAccreditation(){
		if(Validations.validateIsNotNullAndNotEmpty(blockRequest.getBlockEntity().getIdBlockEntityPk())){			
			if(BlockEntityStateType.OFF.getCode().equals(blockRequest.getBlockEntity().getStateBlockEntity())){				
				Object[] bodyData = new Object[1];
				bodyData[0] = blockRequest.getBlockEntity().getIdBlockEntityPk().toString();								
				this.getBlockRequest().setBlockEntity(new BlockEntity());
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getMessage("alt.affectation.block.entity.invalidState", bodyData));
				JSFUtilities.showSimpleValidationDialog();
			}
		}else{
			this.getBlockRequest().setBlockEntity(new BlockEntity());
		}
	}
	
	/**
	 * Select Accreditation CatB
	 *
	 * @param objAccountBalanceExp
	 */
	public void selectAccreditationCatB(HolderAccountBalanceExp objAccountBalanceExp){
		holderAccountBalanceExpTemp = null;
		if (objAccountBalanceExp.getSelected()) {
			// verifying if there is cat generated with the same characteristics
			AccreditationOperation accreditationOperationAux = null;
			try {
				List<AccreditationOperation> lst = accreditationfacadeBean.verifyAccreditationOperationByRegistry(objAccountBalanceExp,holder);
				if (Validations.validateListIsNotNullAndNotEmpty(lst)) {
					accreditationOperationAux = lst.get(0);
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			if (Validations.validateIsNotNullAndNotEmpty(accreditationOperationAux)) {
				holderAccountBalanceExpTemp = objAccountBalanceExp;
				Object[] partBody = {CommonsUtilities.convertDateToString(accreditationOperationAux.getCustodyOperation().getOperationDate(),CommonsUtilities.DATE_PATTERN),CommonsUtilities.convertDateToString(accreditationOperationAux.getCustodyOperation().getOperationDate(),CommonsUtilities.HOUR_PATTERN) };
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_VALIDATION_RECORDS_FOUND_CERTIFICATE_ACCREDITATION,partBody));
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogAccreditationCatB').show()");	
			}
		}	
	}
	
	public void hideDialogsCleanCheck() {
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountBalanceExpTemp)) {
			holderAccountBalanceExpTemp.setSelected(false);
		}
	}
	
	/**
	 * Gets the accreditation detail.
	 * 
	 * @return the accreditation detail
	 */
	public AccreditationDetail getAccreditationDetail() {
		return accreditationDetail;
	}

	/**
	 * Sets the accreditation detail.
	 * 
	 * @param accreditationDetail
	 *            the new accreditation detail
	 */
	public void setAccreditationDetail(AccreditationDetail accreditationDetail) {
		this.accreditationDetail = accreditationDetail;
	}

	/**
	 * Gets the accreditation operation.
	 * 
	 * @return the accreditation operation
	 */
	public AccreditationOperation getAccreditationOperation() {
		return accreditationOperation;
	}

	/**
	 * Sets the accreditation operation.
	 * 
	 * @param accreditationOperation
	 *            the new accreditation operation
	 */
	public void setAccreditationOperation(
			AccreditationOperation accreditationOperation) {
		this.accreditationOperation = accreditationOperation;
	}

	/**
	 * Gets the block operation.
	 * 
	 * @return the block operation
	 */
	public BlockRequest getBlockRequest() {
		return blockRequest;
	}

	/**
	 * Sets the block operation.
	 *
	 * @param blockRequest the new block request
	 */
	public void setBlockRequest(BlockRequest blockRequest) {
		this.blockRequest = blockRequest;
	}

	/**
	 * Gets the block operation certification.
	 * 
	 * @return the block operation certification
	 */
	public BlockOperationCertification getBlockOperationCertification() {
		return blockOperationCertification;
	}

	/**
	 * Sets the block operation certification.
	 * 
	 * @param blockOperationCertification
	 *            the new block operation certification
	 */
	public void setBlockOperationCertification(
			BlockOperationCertification blockOperationCertification) {
		this.blockOperationCertification = blockOperationCertification;
	}

	/**
	 * Gets the participant list.
	 * 
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 * 
	 * @param participantList
	 *            the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}

	/**
	 * Gets the participant.
	 * 
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 * 
	 * @param participant
	 *            the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the now time.
	 * 
	 * @return the now time
	 */
	public Date getNowTime() {
		return nowTime;
	}

	/**
	 * Sets the now time.
	 * 
	 * @param nowTime
	 *            the new now time
	 */
	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}

	/**
	 * Gets the holder.
	 * 
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 * 
	 * @param holder
	 *            the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the block type.
	 * 
	 * @return the block type
	 */
	public List<ParameterTable> getBlockType() {
		return blockType;
	}

	/**
	 * Sets the block type.
	 * 
	 * @param getBlockType
	 *            the new block type
	 */
	public void setBlockType(List<ParameterTable> getBlockType) {
		this.blockType = getBlockType;
	}

	/**
	 * Gets the balance type.
	 * 
	 * @return the balance type
	 */
	public List<ParameterTable> getBalanceType() {
		return balanceType;
	}

	/**
	 * Sets the balance type.
	 * 
	 * @param list
	 *            the new balance type
	 */
	public void setBalanceType(List<ParameterTable> list) {
		this.balanceType = list;
	}

	/**
	 * Gets the petitioner type.
	 * 
	 * @return the petitioner type
	 */
	public List<ParameterTable> getPetitionerType() {
		return petitionerType;
	}

	/**
	 * Sets the petitioner type.
	 * 
	 * @param petitionerType
	 *            the new petitioner type
	 */
	public void setPetitionerType(List<ParameterTable> petitionerType) {
		this.petitionerType = petitionerType;
	}

	/**
	 * Gets the holder account balances.
	 * 
	 * @return the holder account balances
	 */
	public GenericDataModel<HolderAccountBalance> getHolderAccountBalances() {
		return holderAccountBalances;
	}

	/**
	 * Sets the holder account balances.
	 * 
	 * @param holderAccountBalances
	 *            the new holder account balances
	 */
	public void setHolderAccountBalances(GenericDataModel<HolderAccountBalance> holderAccountBalances) {
		this.holderAccountBalances = holderAccountBalances;
	}

	/**
	 * Gets the ha field.
	 * 
	 * @return the ha field
	 */
	public HolderAccount getHaField() {
		return haField;
	}

	/**
	 * Sets the ha field.
	 * 
	 * @param haField
	 *            the new ha field
	 */
	public void setHaField(HolderAccount haField) {
		this.haField = haField;
	}

	/**
	 * Gets the accreditation details.
	 * 
	 * @return the accreditation details
	 */
	public List<AccreditationDetail> getAccreditationDetails() {
		return accreditationDetails;
	}

	/**
	 * Sets the accreditation details.
	 * 
	 * @param accreditationDetails
	 *            the new accreditation details
	 */
	public void setAccreditationDetails(
			List<AccreditationDetail> accreditationDetails) {
		this.accreditationDetails = accreditationDetails;
	}

	/**
	 * Gets the accreditate balance.
	 * 
	 * @return the accreditate balance
	 */
	public BigDecimal getAccreditateBalance() {
		return accreditateBalance;
	}

	/**
	 * Sets the accreditate balance.
	 * 
	 * @param accreditateBalance
	 *            the new accreditate balance
	 */
	public void setAccreditateBalance(BigDecimal accreditateBalance) {
		this.accreditateBalance = accreditateBalance;
	}

	/**
	 * Gets the state list.
	 * 
	 * @return the state list
	 */
	public List<ParameterTable> getStateList() {
		return stateList;
	}

	/**
	 * Sets the state list.
	 * 
	 * @param stateList
	 *            the new state list
	 */
	public void setStateList(List<ParameterTable> stateList) {
		this.stateList = stateList;
	}

	/**
	 * Gets the expiration date.
	 * 
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 * 
	 * @param expirationDate
	 *            the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the block operation details.
	 * 
	 * @return the block operation details
	 */
	public GenericDataModel<BlockOperation> getBlockOperations() {
		return blockOperations;
	}

	/**
	 * Sets the block operation details.
	 *
	 * @param blockOperations the new block operations
	 */
	public void setBlockOperations(GenericDataModel<BlockOperation> blockOperations) {
		this.blockOperations = blockOperations;
	}

	/**
	 * Gets the block balance.
	 * 
	 * @return the block balance
	 */
	public Integer getBlockBalance() {
		return blockBalance;
	}

	/**
	 * Sets the block balance.
	 * 
	 * @param blockBalance
	 *            the new block balance
	 */
	public void setBlockBalance(Integer blockBalance) {
		this.blockBalance = blockBalance;
	}

	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * Gets the holder accounts.
	 *
	 * @return the holder accounts
	 */
	public List<HolderAccount> getHolderAccounts() {
		return holderAccounts;
	}

	/**
	 * Sets the holder accounts.
	 *
	 * @param holderAccounts the new holder accounts
	 */
	public void setHolderAccounts(List<HolderAccount> holderAccounts) {
		this.holderAccounts = holderAccounts;
	}

	/**
	 * Gets the file download.
	 *
	 * @return the file download
	 */
	public StreamedContent getFileDownload() {
		return fileDownload;
	}

	/**
	 * Sets the file download.
	 *
	 * @param fileDownload the new file download
	 */
	public void setFileDownload(StreamedContent fileDownload) {
		this.fileDownload = fileDownload;
	}

	/**
	 * Gets the accreditation operation selection.
	 *
	 * @return the accreditation operation selection
	 */
	public AccreditationOperation getAccreditationOperationSelection() {
		return accreditationOperationSelection;
	}

	/**
	 * Sets the accreditation operation selection.
	 *
	 * @param accreditationOperationSelection the new accreditation operation selection
	 */
	public void setAccreditationOperationSelection(AccreditationOperation accreditationOperationSelection) {
		this.accreditationOperationSelection = accreditationOperationSelection;
	}
	
	/**
	 * Gets the accreditation operation data model.
	 *
	 * @return the accreditation operation data model
	 */
	public GenericDataModel<AccreditationOperation> getAccreditationOperationDataModel() {
		return accreditationOperationDataModel;
	}

	/**
	 * Sets the accreditation operation data model.
	 *
	 * @param accreditationOperationDataModel the new accreditation operation data model
	 */
	public void setAccreditationOperationDataModel(
			GenericDataModel<AccreditationOperation> accreditationOperationDataModel) {
		this.accreditationOperationDataModel = accreditationOperationDataModel;
	}

	/**
	 * Gets the destination deliver list.
	 *
	 * @return the destination deliver list
	 */
	public List<ParameterTable> getDestinationDeliverList() {
		return destinationDeliverList;
	}

	/**
	 * Sets the destination deliver list.
	 *
	 * @param destinationDeliverList the new destination deliver list
	 */
	public void setDestinationDeliverList(List<ParameterTable> destinationDeliverList) {
		this.destinationDeliverList = destinationDeliverList;
	}

	/**
	 * Gets the reject motive list.
	 *
	 * @return the reject motive list
	 */
	public List<ParameterTable> getRejectMotiveList() {
		return rejectMotiveList;
	}

	/**
	 * Sets the reject motive list.
	 *
	 * @param rejectMotiveList the new reject motive list
	 */
	public void setRejectMotiveList(List<ParameterTable> rejectMotiveList) {
		this.rejectMotiveList = rejectMotiveList;
	}

	/**
	 * Gets the signature certificate list.
	 *
	 * @return the signature certificate list
	 */
	public List<ParameterTable> getSignatureCertificateList() {
		return signatureCertificateList;
	}

	/**
	 * Sets the signature certificate list.
	 *
	 * @param signatureCertificateList the new signature certificate list
	 */
	public void setSignatureCertificateList(List<ParameterTable> signatureCertificateList) {
		this.signatureCertificateList = signatureCertificateList;
	}

	/**
	 * Gets the disable participant selection.
	 *
	 * @return the disable participant selection
	 */
	public Boolean getDisableParticipantSelection() {
		return disableParticipantSelection;
	}

	/**
	 * Sets the disable participant selection.
	 *
	 * @param disableParticipantSelection the new disable participant selection
	 */
	public void setDisableParticipantSelection(
			Boolean disableParticipantSelection) {
		this.disableParticipantSelection = disableParticipantSelection;
	}

	/**
	 * Gets the issuance cert file name display.
	 *
	 * @return the issuance cert file name display
	 */
	public String getIssuanceCertFileNameDisplay() {
		return issuanceCertFileNameDisplay;
	}

	/**
	 * Sets the issuance cert file name display.
	 *
	 * @param issuanceCertFileNameDisplay the new issuance cert file name display
	 */
	public void setIssuanceCertFileNameDisplay(
			String issuanceCertFileNameDisplay) {
		this.issuanceCertFileNameDisplay = issuanceCertFileNameDisplay;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the registered type code.
	 *
	 * @return the registered type code
	 */
	public Integer getRegisteredTypeCode() {
		return registeredTypeCode;
	}

	/**
	 * Sets the registered type code.
	 *
	 * @param registeredTypeCode the new registered type code
	 */
	public void setRegisteredTypeCode(Integer registeredTypeCode) {
		this.registeredTypeCode = registeredTypeCode;
	}

	/**
	 * Gets the rejected type code.
	 *
	 * @return the rejected type code
	 */
	public Integer getRejectedTypeCode() {
		return rejectedTypeCode;
	}

	/**
	 * Sets the rejected type code.
	 *
	 * @param rejectedTypeCode the new rejected type code
	 */
	public void setRejectedTypeCode(Integer rejectedTypeCode) {
		this.rejectedTypeCode = rejectedTypeCode;
	}

	/**
	 * Gets the confirmeded type code.
	 *
	 * @return the confirmeded type code
	 */
	public Integer getConfirmededTypeCode() {
		return confirmededTypeCode;
	}

	/**
	 * Sets the confirmeded type code.
	 *
	 * @param confirmededTypeCode the new confirmeded type code
	 */
	public void setConfirmededTypeCode(Integer confirmededTypeCode) {
		this.confirmededTypeCode = confirmededTypeCode;
	}
	
	/**
	 * Gets the unblocked type code.
	 *
	 * @return the unblocked type code
	 */
	public Integer getUnblockedTypeCode() {
		return unblockedTypeCode;
	}

	/**
	 * Sets the unblocked type code.
	 *
	 * @param unblockedTypeCode the new confirmeded type code
	 */
	public void setUnblockedTypeCode(Integer unblockedTypeCode) {
		this.unblockedTypeCode = unblockedTypeCode;
	}

	/**
	 * Gets the holder closed state type.
	 *
	 * @return the holder closed state type
	 */
	public Integer getHolderClosedStateType() {
		return holderClosedStateType;
	}

	/**
	 * Sets the holder closed state type.
	 *
	 * @param holderClosedStateType the new holder closed state type
	 */
	public void setHolderClosedStateType(Integer holderClosedStateType) {
		this.holderClosedStateType = holderClosedStateType;
	}

	/**
	 * Gets the disable balance type.
	 *
	 * @return the disable balance type
	 */
	public Boolean getDisableBalanceType() {
		return disableBalanceType;
	}

	/**
	 * Sets the disable balance type.
	 *
	 * @param disableBalanceType the new disable balance type
	 */
	public void setDisableBalanceType(Boolean disableBalanceType) {
		this.disableBalanceType = disableBalanceType;
	}

	/**
	 * Gets the confirm button selected.
	 *
	 * @return the confirm button selected
	 */
	public Boolean getConfirmButtonSelected() {
		return confirmButtonSelected;
	}

	/**
	 * Sets the confirm button selected.
	 *
	 * @param confirmButtonSelected the new confirm button selected
	 */
	public void setConfirmButtonSelected(Boolean confirmButtonSelected) {
		this.confirmButtonSelected = confirmButtonSelected;
	}

	/**
	 * Gets the reject button selected.
	 *
	 * @return the reject button selected
	 */
	public Boolean getRejectButtonSelected() {
		return rejectButtonSelected;
	}

	/**
	 * Sets the reject button selected.
	 *
	 * @param rejectButtonSelected the new reject button selected
	 */
	public void setRejectButtonSelected(Boolean rejectButtonSelected) {
		this.rejectButtonSelected = rejectButtonSelected;
	}

	/**
	 * Gets the unblock button selected.
	 *
	 * @return the unblock button selected
	 */
	public Boolean getUnblockButtonSelected() {
		return unblockButtonSelected;
	}

	/**
	 * Sets the unblock button selected.
	 *
	 * @param unblockButtonSelected the new unblock button selected
	 */
	public void setUnblockButtonSelected(Boolean unblockButtonSelected) {
		this.unblockButtonSelected = unblockButtonSelected;
	}

	/**
	 * Gets the participant petitioner.
	 *
	 * @return the participant petitioner
	 */
	public Integer getParticipantPetitioner() {
		return participantPetitioner;
	}

	/**
	 * Sets the participant petitioner.
	 *
	 * @param participantPetitioner the new participant petitioner
	 */
	public void setParticipantPetitioner(Integer participantPetitioner) {
		this.participantPetitioner = participantPetitioner;
	}

	/**
	 * Gets the creditor petitioner.
	 *
	 * @return the creditor petitioner
	 */
	public Integer getCreditorPetitioner() {
		return creditorPetitioner;
	}

	/**
	 * Sets the creditor petitioner.
	 *
	 * @param creditorPetitioner the new creditor petitioner
	 */
	public void setCreditorPetitioner(Integer creditorPetitioner) {
		this.creditorPetitioner = creditorPetitioner;
	}

	/**
	 * Gets the holder petitioner.
	 *
	 * @return the holder petitioner
	 */
	public Integer getHolderPetitioner() {
		return holderPetitioner;
	}

	/**
	 * Sets the holder petitioner.
	 *
	 * @param holderPetitioner the new holder petitioner
	 */
	public void setHolderPetitioner(Integer holderPetitioner) {
		this.holderPetitioner = holderPetitioner;
	}

	/**
	 * Gets the inheritor petitioner.
	 *
	 * @return the inheritor petitioner
	 */
	public Integer getInheritorPetitioner() {
		return inheritorPetitioner;
	}

	/**
	 * Sets the inheritor petitioner.
	 *
	 * @param inheritorPetitioner the new inheritor petitioner
	 */
	public void setInheritorPetitioner(Integer inheritorPetitioner) {
		this.inheritorPetitioner = inheritorPetitioner;
	}

	/**
	 * Gets the attorney petitioner.
	 *
	 * @return the attorney petitioner
	 */
	public Integer getAttorneyPetitioner() {
		return attorneyPetitioner;
	}

	/**
	 * Sets the attorney petitioner.
	 *
	 * @param attorneyPetitioner the new attorney petitioner
	 */
	public void setAttorneyPetitioner(Integer attorneyPetitioner) {
		this.attorneyPetitioner = attorneyPetitioner;
	}

	/**
	 * Gets the block operation selected.
	 *
	 * @return the block operation selected
	 */
	public BlockOperation getBlockOperationSelected() {
		return blockOperationSelected;
	}

	/**
	 * Sets the block operation selected.
	 *
	 * @param blockOperationSelected the new block operation selected
	 */
	public void setBlockOperationSelected(BlockOperation blockOperationSelected) {
		this.blockOperationSelected = blockOperationSelected;
	}

	/**
	 * Gets the blocked balance code.
	 *
	 * @return the blocked balance code
	 */
	public Integer getBlockedBalanceCode() {
		return blockedBalanceCode;
	}

	/**
	 * Sets the blocked balance code.
	 *
	 * @param blockedBalanceCode the new blocked balance code
	 */
	public void setBlockedBalanceCode(Integer blockedBalanceCode) {
		this.blockedBalanceCode = blockedBalanceCode;
	}

	/**
	 * Gets the available balance code.
	 *
	 * @return the available balance code
	 */
	public Integer getAvailableBalanceCode() {
		return availableBalanceCode;
	}

	/**
	 * Sets the available balance code.
	 *
	 * @param availableBalanceCode the new available balance code
	 */
	public void setAvailableBalanceCode(Integer availableBalanceCode) {
		this.availableBalanceCode = availableBalanceCode;
	}

	/**
	 * Gets the lst accreditation operation to deliver.
	 *
	 * @return the lst accreditation operation to deliver
	 */
	public List<AccreditationOperationTO> getLstAccreditationOperationTODeliver() {
		return lstAccreditationOperationTODeliver;
	}

	/**
	 * Sets the lst accreditation operation to deliver.
	 *
	 * @param lstAccreditationOperationTODeliver the new lst accreditation operation to deliver
	 */
	public void setLstAccreditationOperationTODeliver(
			List<AccreditationOperationTO> lstAccreditationOperationTODeliver) {
		this.lstAccreditationOperationTODeliver = lstAccreditationOperationTODeliver;
	}

	/**
	 * Gets the participant logged is blocked.
	 *
	 * @return the participant logged is blocked
	 */
	public Boolean getParticipantLoggedIsBlocked() {
		return participantLoggedIsBlocked;
	}

	/**
	 * Sets the participant logged is blocked.
	 *
	 * @param participantLoggedIsBlocked the new participant logged is blocked
	 */
	public void setParticipantLoggedIsBlocked(Boolean participantLoggedIsBlocked) {
		this.participantLoggedIsBlocked = participantLoggedIsBlocked;
	}

	/**
	 * Gets the unblock motive list.
	 *
	 * @return the unblock motive list
	 */
	public List<ParameterTable> getUnblockMotiveList() {
		return unblockMotiveList;
	}

	/**
	 * Sets the unblock motive list.
	 *
	 * @param unblockMotiveList the new unblock motive list
	 */
	public void setUnblockMotiveList(List<ParameterTable> unblockMotiveList) {
		this.unblockMotiveList = unblockMotiveList;
	}

	/**
	 * Gets the other unblock motive.
	 *
	 * @return the other unblock motive
	 */
	public Integer getOtherUnblockMotive() {
		return otherUnblockMotive;
	}

	/**
	 * Sets the other unblock motive.
	 *
	 * @param otherUnblockMotive the new other unblock motive
	 */
	public void setOtherUnblockMotive(Integer otherUnblockMotive) {
		this.otherUnblockMotive = otherUnblockMotive;
	}

	/**
	 * Gets the reported balance code.
	 *
	 * @return the reported balance code
	 */
	public Integer getReportedBalanceCode() {
		return reportedBalanceCode;
	}

	/**
	 * Sets the reported balance code.
	 *
	 * @param reportedBalanceCode the new reported balance code
	 */
	public void setReportedBalanceCode(Integer reportedBalanceCode) {
		this.reportedBalanceCode = reportedBalanceCode;
	}

	/**
	 * Gets the render blocked type.
	 *
	 * @return the render blocked type
	 */
	public Boolean getRenderBlockedType() {
		return renderBlockedType;
	}

	/**
	 * Sets the render blocked type.
	 *
	 * @param renderBlockedType the new render blocked type
	 */
	public void setRenderBlockedType(Boolean renderBlockedType) {
		this.renderBlockedType = renderBlockedType;
	}

	/**
	 * Gets the render available type.
	 *
	 * @return the render available type
	 */
	public Boolean getRenderAvailableType() {
		return renderAvailableType;
	}

	/**
	 * Sets the render available type.
	 *
	 * @param renderAvailableType the new render available type
	 */
	public void setRenderAvailableType(Boolean renderAvailableType) {
		this.renderAvailableType = renderAvailableType;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(
			MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Gets the render request fail.
	 *
	 * @return the render request fail
	 */
	public Boolean getRenderRequestFail() {
		return renderRequestFail;
	}

	/**
	 * Sets the render request fail.
	 *
	 * @param renderRequestFail the new render request fail
	 */
	public void setRenderRequestFail(Boolean renderRequestFail) {
		this.renderRequestFail = renderRequestFail;
	}

	/**
	 * Gets the reported balance to.
	 *
	 * @return the reported balance to
	 */
	public GenericDataModel<MechanismOperationTO> getReportedBalanceTO() {
		return reportedBalanceTO;
	}

	public GenericDataModel<HolderAccountBalanceExp> getHolderAccountBalancesExp() {
		return holderAccountBalancesExp;
	}

	public void setHolderAccountBalancesExp(
			GenericDataModel<HolderAccountBalanceExp> holderAccountBalancesExp) {
		this.holderAccountBalancesExp = holderAccountBalancesExp;
	}

	/**
	 * Sets the reported balance to.
	 *
	 * @param reportedBalanceTO the new reported balance to
	 */
	public void setReportedBalanceTO(GenericDataModel<MechanismOperationTO> reportedBalanceTO) {
		this.reportedBalanceTO = reportedBalanceTO;
	}

	/**
	 * Gets the reported operation to list.
	 *
	 * @return the reported operation to list
	 */
	public List<MechanismOperationTO> getReportedOperationTOList() {
		return reportedOperationTOList;
	}

	/**
	 * Sets the reported operation to list.
	 *
	 * @param reportedOperationTOList the new reported operation to list
	 */
	public void setReportedOperationTOList(
			List<MechanismOperationTO> reportedOperationTOList) {
		this.reportedOperationTOList = reportedOperationTOList;
	}

	/**
	 * Gets the render after selected motive.
	 *
	 * @return the render after selected motive
	 */
	public Boolean getRenderAfterSelectedMotive() {
		return renderAfterSelectedMotive;
	}

	/**
	 * Sets the render after selected motive.
	 *
	 * @param renderAfterSelectedMotive the new render after selected motive
	 */
	public void setRenderAfterSelectedMotive(Boolean renderAfterSelectedMotive) {
		this.renderAfterSelectedMotive = renderAfterSelectedMotive;
	}

	/**
	 * Gets the render observations.
	 *
	 * @return the render observations
	 */
	public Boolean getRenderObservations() {
		return renderObservations;
	}

	/**
	 * Sets the render observations.
	 *
	 * @param renderObservations the new render observations
	 */
	public void setRenderObservations(Boolean renderObservations) {
		this.renderObservations = renderObservations;
	}

	/**
	 * Gets the list motives.
	 *
	 * @return the list motives
	 */
	public List<ParameterTable> getListMotives() {
		return listMotives;
	}

	/**
	 * Sets the list motives.
	 *
	 * @param listMotives the new list motives
	 */
	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the list file accreditation to.
	 *
	 * @return the list file accreditation to
	 */
	public List<FileAccreditationTO> getListFileAccreditationTO() {
		return listFileAccreditationTO;
	}

	/**
	 * Sets the list file accreditation to.
	 *
	 * @param listFileAccreditationTO the new list file accreditation to
	 */
	public void setListFileAccreditationTO(
			List<FileAccreditationTO> listFileAccreditationTO) {
		this.listFileAccreditationTO = listFileAccreditationTO;
	}

	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	/**
	 * Gets the accreditation delivery information.
	 *
	 * @return the accreditation delivery information
	 */
	public AccreditationDeliveryInformation getAccreditationDeliveryInformation() {
		return accreditationDeliveryInformation;
	}

	/**
	 * Sets the accreditation delivery information.
	 *
	 * @param accreditationDeliveryInformation the new accreditation delivery information
	 */
	public void setAccreditationDeliveryInformation(
			AccreditationDeliveryInformation accreditationDeliveryInformation) {
		this.accreditationDeliveryInformation = accreditationDeliveryInformation;
	}

	/**
	 * Gets the lst accreditation delivery information.
	 *
	 * @return the lst accreditation delivery information
	 */
	public List<AccreditationDeliveryInformation> getLstAccreditationDeliveryInformation() {
		return lstAccreditationDeliveryInformation;
	}

	/**
	 * Sets the lst accreditation delivery information.
	 *
	 * @param lstAccreditationDeliveryInformation the new lst accreditation delivery information
	 */
	public void setLstAccreditationDeliveryInformation(
			List<AccreditationDeliveryInformation> lstAccreditationDeliveryInformation) {
		this.lstAccreditationDeliveryInformation = lstAccreditationDeliveryInformation;
	}
	
	/**
	 * Gets the option selected one radio.
	 *
	 * @return the option selected one radio
	 */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}

	/**
	 * Sets the option selected one radio.
	 *
	 * @param optionSelectedOneRadio the new option selected one radio
	 */
	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}
	
	/**
	 * Gets the user privilege.
	 *
	 * @return the user privilege
	 */
	public UserPrivilege getUserPrivilege() {
		return userPrivilege;
	}

	/**
	 * Sets the user privilege.
	 *
	 * @param userPrivilege the new user privilege
	 */
	public void setUserPrivilege(UserPrivilege userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the currency.
	 *
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * Sets the currency.
	 *
	 * @param currency the new currency
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	
	/**
	 * Gets the list security class search.
	 *
	 * @return the list security class search
	 */
	public List<ParameterTable> getListSecurityClassSearch() {
		return listSecurityClassSearch;
	}

	/**
	 * Sets the list security class search.
	 *
	 * @param listSecurityClassSearch the new list security class search
	 */
	public void setListSecurityClassSearch(
			List<ParameterTable> listSecurityClassSearch) {
		this.listSecurityClassSearch = listSecurityClassSearch;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the list currency.
	 *
	 * @return the list currency
	 */
	public List<ParameterTable> getListCurrency() {
		return listCurrency;
	}

	/**
	 * Sets the list currency.
	 *
	 * @param listCurrency the new list currency
	 */
	public void setListCurrency(List<ParameterTable> listCurrency) {
		this.listCurrency = listCurrency;
	}
	
	/**
	 * Gets the lst accreditation operation to delivers.
	 *
	 * @return the lst accreditation operation to delivers
	 */
	public List<AccreditationDeliveryInformation> getLstAccreditationOperationTODelivers() {
		return lstAccreditationOperationTODelivers;
	}

	/**
	 * Sets the lst accreditation operation to delivers.
	 *
	 * @param lstAccreditationOperationTODelivers the new lst accreditation operation to delivers
	 */
	public void setLstAccreditationOperationTODelivers(
			List<AccreditationDeliveryInformation> lstAccreditationOperationTODelivers) {
		this.lstAccreditationOperationTODelivers = lstAccreditationOperationTODelivers;
	}

	/**
	 * Gets the sustention disabled.
	 *
	 * @return the sustention disabled
	 */
	public Boolean getSustentionDisabled() {
		return sustentionDisabled;
	}

	/**
	 * Sets the sustention disabled.
	 *
	 * @param sustentionDisabled the new sustention disabled
	 */
	public void setSustentionDisabled(Boolean sustentionDisabled) {
		this.sustentionDisabled = sustentionDisabled;
	}
	
	/**
	 * Gets the issuer petitioner.
	 *
	 * @return the issuer petitioner
	 */
	public Integer getIssuerPetitioner() {
		return issuerPetitioner;
	}

	/**
	 * Sets the issuer petitioner.
	 *
	 * @param issuerPetitioner the new issuer petitioner
	 */
	public void setIssuerPetitioner(Integer issuerPetitioner) {
		this.issuerPetitioner = issuerPetitioner;
	}
	
	/**
	 * Gets the issuer name.
	 *
	 * @return the issuer name
	 */
	public String getIssuerName() {
		return issuerName;
	}

	/**
	 * Sets the issuer name.
	 *
	 * @param issuerName the new issuer name
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	/**
	 * Gets the message register.
	 *
	 * @return the message register
	 */
	public String getMessageRegister() {
		return messageRegister;
	}

	/**
	 * Sets the message register.
	 *
	 * @param messageRegister the new message register
	 */
	public void setMessageRegister(String messageRegister) {
		this.messageRegister = messageRegister;
	}

	/**
	 * Gets the holder accounts issuer.
	 *
	 * @return the holder accounts issuer
	 */
	public List<HolderAccount> getHolderAccountsIssuer() {
		return holderAccountsIssuer;
	}

	/**
	 * Sets the holder accounts issuer.
	 *
	 * @param holderAccountsIssuer the new holder accounts issuer
	 */
	public void setHolderAccountsIssuer(List<HolderAccount> holderAccountsIssuer) {
		this.holderAccountsIssuer = holderAccountsIssuer;
	}

	/**
	 * Gets the list issuer.
	 *
	 * @return the list issuer
	 */
	public List<Issuer> getListIssuer() {
		return listIssuer;
	}

	/**
	 * Sets the list issuer.
	 *
	 * @param listIssuer the new list issuer
	 */
	public void setListIssuer(List<Issuer> listIssuer) {
		this.listIssuer = listIssuer;
	}
	
	/**
	 * Gets the list accreditation operation selection.
	 *
	 * @return the listAccreditationOperationSelection
	 */
	public List<AccreditationOperation> getListAccreditationOperationSelection() {
		return listAccreditationOperationSelection;
	}

	/**
	 * Sets the list accreditation operation selection.
	 *
	 * @param listAccreditationOperationSelection the listAccreditationOperationSelection to set
	 */
	public void setListAccreditationOperationSelection(
			List<AccreditationOperation> listAccreditationOperationSelection) {
		this.listAccreditationOperationSelection = listAccreditationOperationSelection;
	}
	
	/**
	 * Gets the listid accreditation operation pk.
	 *
	 * @return the listidAccreditationOperationPk
	 */
	public String getListidAccreditationOperationPk() {
		return listidAccreditationOperationPk;
	}

	/**
	 * Sets the listid accreditation operation pk.
	 *
	 * @param listidAccreditationOperationPk the listidAccreditationOperationPk to set
	 */
	public void setListidAccreditationOperationPk(
			String listidAccreditationOperationPk) {
		this.listidAccreditationOperationPk = listidAccreditationOperationPk;
	}

	
	/**
	 * Gets the id reject motive operations selected.
	 *
	 * @return the idRejectMotiveOperationsSelected
	 */
	public Integer getIdRejectMotiveOperationsSelected() {
		return idRejectMotiveOperationsSelected;
	}

	
	/**
	 * Sets the id reject motive operations selected.
	 *
	 * @param idRejectMotiveOperationsSelected the idRejectMotiveOperationsSelected to set
	 */
	public void setIdRejectMotiveOperationsSelected(
			Integer idRejectMotiveOperationsSelected) {
		this.idRejectMotiveOperationsSelected = idRejectMotiveOperationsSelected;
	}

	/**
	 * Gets the reject motive other operations selected.
	 *
	 * @return the rejectMotiveOtherOperationsSelected
	 */
	public String getRejectMotiveOtherOperationsSelected() {
		return rejectMotiveOtherOperationsSelected;
	}

	/**
	 * Sets the reject motive other operations selected.
	 *
	 * @param rejectMotiveOtherOperationsSelected the rejectMotiveOtherOperationsSelected to set
	 */
	public void setRejectMotiveOtherOperationsSelected(
			String rejectMotiveOtherOperationsSelected) {
		this.rejectMotiveOtherOperationsSelected = rejectMotiveOtherOperationsSelected;
	}

	/**
	 * Gets the message balance.
	 *
	 * @return the message balance
	 */
	public String getMessageBalance() {
		return messageBalance;
	}

	/**
	 * Sets the message balance.
	 *
	 * @param messageBalance the new message balance
	 */
	public void setMessageBalance(String messageBalance) {
		this.messageBalance = messageBalance;
	}

	/**
	 * Gets the checks if is issuer.
	 *
	 * @return the checks if is issuer
	 */
	public Boolean getIsIssuer() {
		return isIssuer;
	}

	/**
	 * Sets the checks if is issuer.
	 *
	 * @param isIssuer the new checks if is issuer
	 */
	public void setIsIssuer(Boolean isIssuer) {
		this.isIssuer = isIssuer;
	}

	/**
	 * Gets the flag exist participant dpf.
	 *
	 * @return the flagExistParticipantDPF
	 */
	public Boolean getFlagExistParticipantDPF() {
		return flagExistParticipantDPF;
	}

	/**
	 * Sets the flag exist participant dpf.
	 *
	 * @param flagExistParticipantDPF the flagExistParticipantDPF to set
	 */
	public void setFlagExistParticipantDPF(Boolean flagExistParticipantDPF) {
		this.flagExistParticipantDPF = flagExistParticipantDPF;
	}

	public List<ParameterTable> getCatType() {
		return catType;
	}

	public void setCatType(List<ParameterTable> catType) {
		this.catType = catType;
	}

	public Boolean getValidityDaysBoo() {
		return validityDaysBoo;
	}

	public void setValidityDaysBoo(Boolean validityDaysBoo) {
		this.validityDaysBoo = validityDaysBoo;
	}
	
	
}
