package com.pradera.custody.accreditationcertificates.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.core.component.accounts.service.HolderBalanceMovementsServiceBean;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.operation.to.MechanismOperationTO;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.to.AffectationObjectTO;
import com.pradera.integration.component.custody.to.AffectationRegisterTO;
import com.pradera.integration.component.custody.to.LegalInformationObjetTO;
import com.pradera.integration.component.custody.to.StockUnblockObjectTO;
import com.pradera.integration.component.custody.to.StockUnblockRegisterTO;
import com.pradera.integration.component.securities.to.SecurityObjectTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalanceExp;
import com.pradera.model.custody.accreditationcertificates.AccreditationDeliveryInformation;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.accreditationcertificates.DigitalSignature;
import com.pradera.model.custody.accreditationcertificates.ReportAccreditation;
import com.pradera.model.custody.accreditationcertificates.UnblockAccreditationOperation;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.CertificationType;
import com.pradera.model.custody.type.UnblockAccreditationOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.negotiation.type.HolderAccountOperationStateType;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.negotiation.type.ParticipantRoleType;

// TODO: Auto-generated Javadoc
/**
 * The Class AccreditationCertificatesServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/04/2014
 */
@Stateless
//@Performance
public class AccreditationCertificatesServiceBean extends CrudDaoServiceBean {
	
	/** The general parameter facade. */
	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	/** The holder balance movements service bean. */
	@Inject 
	private HolderBalanceMovementsServiceBean holderBalanceMovementsServiceBean;

	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	/**
	 * Find by query.
	 *
	 * 
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Participant> findAllParticipants() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p.idParticipantPk,p.description,p.state from Participant p ");		
		sbQuery.append(" order by p.description");
		Query query = em.createQuery(sbQuery.toString());		
		List<Object> oList = query.getResultList();
		List<Participant> participantList = new ArrayList<Participant>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating Participant
			Participant participant = new Participant();
			participant.setDescription((String) objects[1]);
			participant.setIdParticipantPk((Long) objects[0]);
			participant.setState((Integer) objects[2]);
			participantList.add(participant);
		}
		return participantList;
	}
	
	/**
	 * Gets the accreditation certification.
	 *
	 * @param certificationId the certification id
	 * @return the accreditation certification
	 * @throws ServiceException the service exception
	 */
	public byte[] getAccreditationCertification(Long certificationId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();				
		sbQuery.append("SELECT AO.certificationFile");
		sbQuery.append("  FROM AccreditationOperation AO");
		sbQuery.append(" WHERE AO.idAccreditationOperationPk =:certificationId");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("certificationId", certificationId);
		return (byte[]) query.getSingleResult();
	}
	
	/**
	 * Find holder.
	 *
	 * @param holderTO the holder to
	 * @return the holder
	 */
	public Holder findHolder(HolderTO holderTO ){
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("SELECT H.idHolderPk,");
		sbQuery.append("       H.name,");
		sbQuery.append("       H.fullName,");
		sbQuery.append("       H.stateHolder");
		sbQuery.append("  FROM Holder H");
		sbQuery.append(" WHERE H.idHolderPk =:idHolderPk");
		mapParam.put("idHolderPk", holderTO.getHolderId());
		
		if(Validations.validateIsNotNull(holderTO.getName())){
			sbQuery.append(" AND H.name =:holderName");
			mapParam.put("holderName", holderTO.getName());
		}
		
		if(Validations.validateIsNotNull(holderTO.getFullName())){
			sbQuery.append(" AND H.fullName =:holderFullName");
			mapParam.put("holderFullName", holderTO.getFullName());
		}
		
		if(Validations.validateIsNotNull(holderTO.getFullName())){
			sbQuery.append(" AND H.fullName =:holderFullName");
			mapParam.put("holderFullName", holderTO.getFullName());
		}
		
		if(Validations.validateIsNotNull(holderTO.getFirstLastName())){
			sbQuery.append(" AND H.firstLastName =:holderFirstLastName");
			mapParam.put("holderFirstLastName", holderTO.getFirstLastName());
		}
		
		if(Validations.validateIsNotNull(holderTO.getSecondLastName())){
			sbQuery.append(" AND H.secondLastName =:holderSecondLastName");
			mapParam.put("holderSecondLastName", holderTO.getSecondLastName());
		}
		
		if(Validations.validateIsNotNull(holderTO.getDocumentNumber())){
			sbQuery.append(" AND H.documentNumber =:holderDocumentNumber");
			mapParam.put("holderDocumentNumber", holderTO.getDocumentNumber());
		}
		
		if(Validations.validateIsNotNull(holderTO.getDocumentType())){
			sbQuery.append(" AND H.documentType =:holderDocumentType");
			mapParam.put("holderDocumentType", holderTO.getDocumentType());
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
			
		//Creating Holder
        Holder holder = new Holder();
		Object [] objectList = (Object[]) query.getSingleResult(); 
		
		holder.setIdHolderPk((Long)objectList[0]);
		holder.setName((String)objectList[1]);
		holder.setFullName((String)objectList[2]);
		holder.setStateHolder((Integer)objectList[3]);
		
		return holder;
	}
	
	/**
	 * Change accreditation status.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @throws ServiceException the service exception
	 */
	public void updateAccreditationOperation(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("UPDATE AccreditationOperation accreditationOp SET accreditationOp.custodyOperation.idCustodyOperationPk=:idAccreditationPk ");
		if(accreditationOperationTO.getAccreditationState()!=null){
			sbQuery.append(",accreditationOp.accreditationState =:accreditationState");
			mapParam.put("accreditationState", accreditationOperationTO.getAccreditationState());
		}
		
		if(accreditationOperationTO.getDestinationCertificate() != null){
			sbQuery.append(",accreditationOp.destinationCertificate =:destinationCertificate");
			mapParam.put("destinationCertificate", accreditationOperationTO.getDestinationCertificate());
		}
		if(accreditationOperationTO.getGeneratedDate() != null){
			sbQuery.append(",accreditationOp.generatedDate =:generatedDate");
			mapParam.put("generatedDate", accreditationOperationTO.getGeneratedDate());
		}
		if(accreditationOperationTO.getDeliveryDate() != null){
			sbQuery.append(",accreditationOp.deliveryDate =:deliveryDate");
			mapParam.put("deliveryDate", accreditationOperationTO.getDeliveryDate());
		}
		if(accreditationOperationTO.getIndDelivered()!=null){
			sbQuery.append(",accreditationOp.indDelivered =:indDelivered");
			mapParam.put("indDelivered", accreditationOperationTO.getIndDelivered());
		}
		if(accreditationOperationTO.getIndGenerated()!=null){
			sbQuery.append(",accreditationOp.indGenerated =:indGenerated");
			mapParam.put("indGenerated", accreditationOperationTO.getIndGenerated());
		}
		if(accreditationOperationTO.getRejectMotiveOther()!=null){
			sbQuery.append(",accreditationOp.rejectMotiveOther=:rejectMotiveOther");
			mapParam.put("rejectMotiveOther", accreditationOperationTO.getRejectMotiveOther());
		}
		if(accreditationOperationTO.getIdRejectMotiveFk()!=null){
			sbQuery.append(",accreditationOp.idRejectMotive=:idRejectMotiveFk");
			mapParam.put("idRejectMotiveFk", accreditationOperationTO.getIdRejectMotiveFk());
		}
		
		if(accreditationOperationTO.getUnblockMotive()!=null){
			sbQuery.append(",accreditationOp.unblockMotive=:unblockMotive");
			mapParam.put("unblockMotive", accreditationOperationTO.getUnblockMotive());
		}
		if(accreditationOperationTO.getUnblockObservation()!=null){
			sbQuery.append(",accreditationOp.unblockObservation=:unblockObservation");
			mapParam.put("unblockObservation", accreditationOperationTO.getUnblockObservation());
		}
		
		if(accreditationOperationTO.getIdSignatureCertificateFk()!=null){
			sbQuery.append(",accreditationOp.idSignatureCertificateFk=:idSignatureCertificateFk");
			mapParam.put("idSignatureCertificateFk", accreditationOperationTO.getIdSignatureCertificateFk());
		}
		
		if(accreditationOperationTO.getExpirationDate()!=null){
			sbQuery.append(",accreditationOp.expirationDate=:expirationDate");
			mapParam.put("expirationDate", accreditationOperationTO.getExpirationDate());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getRealExpirationDate())){
			sbQuery.append(", accreditationOp.realExpirationDate=:realExpirationDate ");
			mapParam.put("realExpirationDate", accreditationOperationTO.getRealExpirationDate());
		}
		
		if(accreditationOperationTO.getLastModifyApp()!=null){
			sbQuery.append(",accreditationOp.lastModifyApp=:lastModifyApp");
			mapParam.put("lastModifyApp", accreditationOperationTO.getLastModifyApp());
		}
		
		if(accreditationOperationTO.getLastModifyDate()!=null){
			sbQuery.append(",accreditationOp.lastModifyDate=:lastModifyDate");
			mapParam.put("lastModifyDate", accreditationOperationTO.getLastModifyDate());
		}
		if(accreditationOperationTO.getLastModifyIp()!=null){
			sbQuery.append(",accreditationOp.lastModifyIp=:lastModifyIp");
			mapParam.put("lastModifyIp", accreditationOperationTO.getLastModifyIp());
		}
		if(accreditationOperationTO.getLastModifyUser()!=null){
			sbQuery.append(",accreditationOp.lastModifyUser=:lastModifyUser");
			mapParam.put("lastModifyUser", accreditationOperationTO.getLastModifyUser());
		}
		if(accreditationOperationTO.getWayOfPayment()!=null){
			sbQuery.append(",accreditationOp.wayOfPayment=:wayOfPayment");
			mapParam.put("wayOfPayment", accreditationOperationTO.getWayOfPayment());
		}
		if(accreditationOperationTO.getCertificationFile()!=null){
			sbQuery.append(",accreditationOp.certificationFile=:certificationFile");
			mapParam.put("certificationFile", accreditationOperationTO.getCertificationFile());
		}
		if(accreditationOperationTO.getCertificateNumber() != null){
			sbQuery.append(",accreditationOp.certificateNumber =:certificateNumber");
			mapParam.put("certificateNumber", accreditationOperationTO.getCertificateNumber());
		}
		
		if(accreditationOperationTO.getSecurityCodeCat() != null){
			sbQuery.append(", accreditationOp.securityCodeCat =:securityCodeCat ");
			mapParam.put("securityCodeCat", accreditationOperationTO.getSecurityCodeCat());
		}
		
		sbQuery.append(" WHERE accreditationOp.custodyOperation.idCustodyOperationPk=:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		query.setParameter("idAccreditationPk", accreditationOperationTO.getIdAccreditationOperationPk()).executeUpdate();
		
	}
	
	/**
	 * Change accreditation status.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @throws ServiceException the service exception
	 */
	public void updateUnblockAccreditationRequestOperation(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("UPDATE CustodyOperation co SET co.state=:accreditationState ");
		mapParam.put("accreditationState", accreditationOperationTO.getAccreditationState());		
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getLastModifyUser())){
			sbQuery.append(", co.confirmUser=:confirmUser");
			mapParam.put("confirmUser", accreditationOperationTO.getLastModifyUser());
		}	
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getLastModifyDate())){
			sbQuery.append(", co.confirmDate=:confirmDate");
			mapParam.put("confirmDate", accreditationOperationTO.getLastModifyDate());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getLastModifyApp())){
			sbQuery.append(", co.lastModifyApp=:lastModifyApp");
			mapParam.put("lastModifyApp", accreditationOperationTO.getLastModifyApp());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getLastModifyDate())){
			sbQuery.append(", co.lastModifyDate=:lastModifyDate");
			mapParam.put("lastModifyDate", accreditationOperationTO.getLastModifyDate());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getLastModifyIp())){
			sbQuery.append(", co.lastModifyIp=:lastModifyIp");
			mapParam.put("lastModifyIp", accreditationOperationTO.getLastModifyIp());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getLastModifyUser())){
			sbQuery.append(", co.lastModifyUser=:lastModifyUser");
			mapParam.put("lastModifyUser", accreditationOperationTO.getLastModifyUser());
		}
		
		sbQuery.append(" WHERE co.idCustodyOperationPk=:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		query.setParameter("idAccreditationPk", accreditationOperationTO.getIdUnblockAccreditationPk()).executeUpdate();
	}
	
	
	/**
	 * Gets the unblock accreditation operation.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @return the unblock accreditation operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UnblockAccreditationOperation> getUnblockAccreditationOperation(AccreditationOperationTO accreditationOperationTO) throws ServiceException {	
		StringBuilder sbQuery = new StringBuilder();
		List<Integer> lstState = new ArrayList<Integer>();
		lstState.add(UnblockAccreditationOperationStateType.REGISTERED.getCode());
		lstState.add(UnblockAccreditationOperationStateType.APPROVED.getCode());
		sbQuery.append(" Select uao " );
		sbQuery.append(" from UnblockAccreditationOperation uao ");		
		sbQuery.append(" WHERE 1=1 ");		
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getIdAccreditationOperationPk())){
			sbQuery.append(" AND uao.accreditationOperation.custodyOperation.idCustodyOperationPk = :idAccreditationOper");
		}	
		sbQuery.append(" AND uao.custodyOperation.state IN (:lstState) ");		
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getIdAccreditationOperationPk())){
			query.setParameter("idAccreditationOper", accreditationOperationTO.getIdAccreditationOperationPk());
		}
		query.setParameter("lstState", lstState);
		return (List<UnblockAccreditationOperation>)query.getResultList();
	}
	
	/**
	 * Update accreditation operation.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public void updateAccreditationOperation(AccreditationOperation accreditationOperation) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();		
			Map<String,Object> mapParam = new HashMap<String,Object>();
			sbQuery.append("UPDATE AccreditationOperation accreditationOp SET accreditationOp.custodyOperation.idCustodyOperationPk=:idAccreditationPk");
			if(accreditationOperation.getAccreditationState()!=null){
				sbQuery.append(",accreditationOp.accreditationState =:accreditationState");
				mapParam.put("accreditationState", accreditationOperation.getAccreditationState());
			}
			
			if(accreditationOperation.getDestinationCertificate() != null){
				sbQuery.append(",accreditationOp.destinationCertificate =:destinationCertificate");
				mapParam.put("destinationCertificate", accreditationOperation.getDestinationCertificate());
			}
			if(accreditationOperation.getGeneratedDate() != null){
				sbQuery.append(",accreditationOp.generatedDate =:generatedDate");
				mapParam.put("generatedDate", accreditationOperation.getGeneratedDate());
			}
			if(accreditationOperation.getDeliveryDate() != null){
				sbQuery.append(",accreditationOp.deliveryDate =:deliveryDate");
				mapParam.put("deliveryDate", accreditationOperation.getDeliveryDate());
			}
			if(accreditationOperation.getIndDelivered()!=null){
				sbQuery.append(",accreditationOp.indDelivered =:indDelivered");
				mapParam.put("indDelivered", accreditationOperation.getIndDelivered());
			}
			if(accreditationOperation.getIndGenerated()!=null){
				sbQuery.append(",accreditationOp.indGenerated =:indGenerated");
				mapParam.put("indGenerated", accreditationOperation.getIndGenerated());
			}
			if(accreditationOperation.getRejectMotiveOther()!=null){
				sbQuery.append(",accreditationOp.rejectMotiveOther=:rejectMotiveOther");
				mapParam.put("rejectMotiveOther", accreditationOperation.getRejectMotiveOther());
			}
			if(accreditationOperation.getIdRejectMotive()!=null){
				sbQuery.append(",accreditationOp.idRejectMotive=:idRejectMotiveFk");
				mapParam.put("idRejectMotiveFk", accreditationOperation.getIdRejectMotive());
			}
			
			if(accreditationOperation.getUnblockMotive()!=null){
				sbQuery.append(",accreditationOp.unblockMotive=:unblockMotive");
				mapParam.put("unblockMotive", accreditationOperation.getUnblockMotive());
			}
			if(accreditationOperation.getUnblockObservation()!=null){
				sbQuery.append(",accreditationOp.unblockObservation=:unblockObservation");
				mapParam.put("unblockObservation", accreditationOperation.getUnblockObservation());
			}
			
			if(accreditationOperation.getIdSignatureCertificateFk()!=null){
				sbQuery.append(",accreditationOp.idSignatureCertificateFk=:idSignatureCertificateFk");
				mapParam.put("idSignatureCertificateFk", accreditationOperation.getIdSignatureCertificateFk());
			}
			if(accreditationOperation.getExpirationDate()!=null){
				sbQuery.append(",accreditationOp.expirationDate=:expirationDate");
				mapParam.put("expirationDate", accreditationOperation.getExpirationDate());
			}
			if(accreditationOperation.getLastModifyApp()!=null){
				sbQuery.append(",accreditationOp.lastModifyApp=:lastModifyApp");
				mapParam.put("lastModifyApp", accreditationOperation.getLastModifyApp());
			}
			
			if(accreditationOperation.getLastModifyDate()!=null){
				sbQuery.append(",accreditationOp.lastModifyDate=:lastModifyDate");
				mapParam.put("lastModifyDate", accreditationOperation.getLastModifyDate());
			}
			if(accreditationOperation.getLastModifyIp()!=null){
				sbQuery.append(",accreditationOp.lastModifyIp=:lastModifyIp");
				mapParam.put("lastModifyIp", accreditationOperation.getLastModifyIp());
			}
			if(accreditationOperation.getLastModifyUser()!=null){
				sbQuery.append(",accreditationOp.lastModifyUser=:lastModifyUser");
				mapParam.put("lastModifyUser", accreditationOperation.getLastModifyUser());
			}
			if(accreditationOperation.getWayOfPayment()!=null){
				sbQuery.append(",accreditationOp.wayOfPayment=:wayOfPayment");
				mapParam.put("wayOfPayment", accreditationOperation.getWayOfPayment());
			}
			if(accreditationOperation.getCertificationFile()!=null){
				sbQuery.append(",accreditationOp.certificationFile=:certificationFile");
				mapParam.put("certificationFile", accreditationOperation.getCertificationFile());
			}
			if(accreditationOperation.getCertificateNumber() != null){
				sbQuery.append(",accreditationOp.certificateNumber =:certificateNumber");
				mapParam.put("certificateNumber", accreditationOperation.getCertificateNumber());
			}
			sbQuery.append(" WHERE accreditationOp.custodyOperation.idCustodyOperationPk=:idAccreditationPk");
			Query query = em.createQuery(sbQuery.toString());
			//Building Itarator 
			@SuppressWarnings("rawtypes")
			Iterator it= mapParam.entrySet().iterator();
			//Iterating
	        while(it.hasNext()){
	        	@SuppressWarnings("unchecked")
	        	//Building the entry
				Entry<String,Object> entry = (Entry<String,Object>)it.next();
	        	//Setting Param
	        	query.setParameter(entry.getKey(), entry.getValue());
	        }
			query.setParameter("idAccreditationPk", accreditationOperation.getIdAccreditationOperationPk()).executeUpdate();
		} catch (Exception e) {
			e.getStackTrace();
		}
	}
	
	/**
	 * Gets the report logger state.
	 *
	 * @param idReportLoggerPk the id report logger pk
	 * @return the report logger state
	 */
	public Integer getReportLoggerState(Long idReportLoggerPk){
		StringBuilder sbQuery = new StringBuilder();				
		sbQuery.append("SELECT RL.reportState");		
		sbQuery.append("  FROM ReportLogger RL");
		sbQuery.append(" WHERE RL.idReportLoggerPk =:idReportLoggerPk");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idReportLoggerPk", idReportLoggerPk);
		
		return (Integer) query.getSingleResult();
	}
	/**
	 * Deliver accreditation.
	 *
	 * @param idAccreditationPk the id accreditation pk
	 * @param destination the destination
	 * @throws ServiceException the service exception
	 */
	public void deliverAccreditation(Long idAccreditationPk,Integer destination) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("UPDATE AccreditationOperation accreditationOp SET accreditationOp.indDelivered=1, accreditationOp.destinationCertificate=:destination");
		sbQuery.append(" WHERE accreditationOp.custodyOperation.idCustodyOperationPk=:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("destination", destination);
		query.setParameter("idAccreditationPk", idAccreditationPk).executeUpdate();
	}
	// get all Holder's accounts
	/**
	 * Search holder account.
	 *
	 * @param holder the holder
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> searchHolderAccount(Holder holder) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Building the query
		sbQuery.append("Select ");
		sbQuery.append("DISTINCT ha.idHolderAccountPk, ");
		sbQuery.append("ha.accountNumber, ");
		sbQuery.append("ha.participant.mnemonic, ");
		sbQuery.append("ha.participant.idParticipantPk, ");
		sbQuery.append("ha.stateAccount, ");	
		sbQuery.append("ha.participant.description, ");
		sbQuery.append("ha.alternateCode, ");
		sbQuery.append("had.holder.fullName, ");
		sbQuery.append("ha.participant.issuer.idIssuerPk, ");
		sbQuery.append("ha.accountType ");
		sbQuery.append("from HolderAccount ha ");
		sbQuery.append(" join ha.holderAccountDetails had ");
		sbQuery.append(" where HAD.holder.idHolderPk=:idHolderPkPrm");
		sbQuery.append(" and  ha.stateAccount=:stateAccount");
		//Setting the query
		Query query = em.createQuery(sbQuery.toString());
		//Setting parameters
		query.setParameter("idHolderPkPrm", holder.getIdHolderPk());
		query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
		//Getting the result
		List<Object> oList = query.getResultList();
		List<HolderAccount> ha = new ArrayList<HolderAccount>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating HolderAccount
			HolderAccount haObject = new HolderAccount();
			haObject.setIdHolderAccountPk((Long)objects[0]);
			haObject.setAccountNumber((Integer) objects[1]);
					
			Issuer issuer = new Issuer();
			issuer.setIdIssuerPk((String) objects[8]);
			
			//Creating Participant
			Participant participant = new Participant();
			participant.setIssuer(issuer);
			participant.setMnemonic((String) objects[2]);
			participant.setIdParticipantPk((Long) objects[3]);	
			participant.setDescription((String) objects[5]);
			haObject.setStateAccount((Integer) objects[4]);			
			haObject.setParticipant(participant);
			haObject.setAlternateCode((String)objects[6]);
			haObject.setFullName((String)objects[7]);
			haObject.setAccountType((Integer) objects[9]);
			ha.add(haObject);
		}
		return ha;
	}

	// Get holder's accounts by participant
	/**
	 * Search holder account by participant.
	 *
	 * @param holder the holder
	 * @param participant the participant
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccount> searchHolderAccountByParticipant(Holder holder, Participant participant) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select ");
		sbQuery.append("DISTINCT ha.idHolderAccountPk, ");
		sbQuery.append("ha.accountNumber, ");
		sbQuery.append("ha.participant.mnemonic, ");
		sbQuery.append("ha.participant.idParticipantPk, ");
		sbQuery.append("ha.stateAccount, ");
		sbQuery.append("ha.participant.description, ");
		sbQuery.append("ha.alternateCode,");
		sbQuery.append("had.holder.fullName ");
		sbQuery.append("from HolderAccount ha ");
		sbQuery.append(" join ha.holderAccountDetails had ");
		sbQuery.append(" where  had.holder.idHolderPk=:idHolderPkPrm");
		sbQuery.append(" and  ha.participant.idParticipantPk=:idParticipantPkPrm ");	
		sbQuery.append(" and  ha.stateAccount=:stateAccount");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderPkPrm", holder.getIdHolderPk());		
		query.setParameter("idParticipantPkPrm",participant.getIdParticipantPk());	
		query.setParameter("stateAccount", HolderAccountStatusType.ACTIVE.getCode());
		//Getting the result
		List<Object> oList = query.getResultList();
		List<HolderAccount> holderAccountList = new ArrayList<HolderAccount>();
		//Parsing the result
		for(int i = 0; i < oList.size();++i){
			Object[] objects =  (Object[]) oList.get(i);
			//Creating HolderAccount
			HolderAccount haObject = new HolderAccount();
			haObject.setIdHolderAccountPk((Long)objects[0]);
			haObject.setAccountNumber((Integer) objects[1]);
            //Creating Participant
			Participant p = new Participant();
			p.setMnemonic((String) objects[2]);
			p.setIdParticipantPk((Long) objects[3]);	
			p.setDescription((String) objects[5]);
			haObject.setStateAccount((Integer) objects[4]);			
			haObject.setParticipant(p);
			haObject.setAlternateCode((String)objects[6]);
			haObject.setFullName((String)objects[7]);
			
			holderAccountList.add(haObject);
		}
		return holderAccountList;
	}
	
	/**
	 * Search holder account balance.
	 *
	 * @param idAccreditationPk the id accreditation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<BlockOperation> searchBlockOperations(Long idAccreditationPk) throws ServiceException{
		//Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select BOC.blockOperation.idBlockOperationPk ");
		sbQuery.append("from BlockOperationCertification BOC ");
		sbQuery.append("where  BOC.accreditationDetail.accreditationOperation.idAccreditationOperationPk =:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idAccreditationPk", idAccreditationPk);
		
		List<BlockOperation> blockOperations = new ArrayList<BlockOperation>();
		
		List<Object> objectList = query.getResultList(); 
		for(int i=0;i<objectList.size();++i){
			//Setting new Holder Account balance
			BlockOperation objBlockOperation = new BlockOperation();
			//Getting Results from data base			
			objBlockOperation.setIdBlockOperationPk((Long)objectList.get(i));
			blockOperations.add(objBlockOperation);
		}
		return blockOperations;
	}
	
	/**
	 * Update block operation.
	 *
	 * @param blockOperation the block operation
	 * @throws ServiceException the service exception
	 */
	public void updateBlockOperation(BlockOperation blockOperation) throws ServiceException{
		//Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE BlockOperation BO");
		sbQuery.append("   SET BO.indAccreditation = :indAccreditation");
		sbQuery.append(" WHERE  BO.idBlockOperationPk=:blockOperationPk");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("blockOperationPk", blockOperation.getIdBlockOperationPk());
		query.setParameter("indAccreditation", blockOperation.getIndAccreditation());
		query.executeUpdate();
	}
	
	/**
	 * Update reported operation.
	 *
	 * @param lstHAO the lst hao
	 * @param state the state
	 * @throws ServiceException the service exception
	 */
	public void updateReportedOperation(List<HolderAccountOperation> lstHAO, Integer state) throws ServiceException{
		//Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("UPDATE HolderAccountOperation hao");
		sbQuery.append(" SET hao.indAccreditation = :indAccreditation, ");
		sbQuery.append(" hao.lastModifyApp = :lastModifyApp, ");
		sbQuery.append(" hao.lastModifyDate = :lastModifyDate, ");
		sbQuery.append(" hao.lastModifyIp = :lastModifyIp, ");
		sbQuery.append(" hao.lastModifyUser = :lastModifyUser ");
		sbQuery.append(" WHERE  hao.idHolderAccountOperationPk in :lstPks");
		Query query = em.createQuery(sbQuery.toString());
		List<Long> lstIds= new ArrayList<Long>();
		for (int i = 0; i < lstHAO.size(); i++) {
			Long id= lstHAO.get(i).getIdHolderAccountOperationPk();
			lstIds.add(id);
		}
		query.setParameter("lstPks", lstIds);
		if(AccreditationOperationStateType.REGISTRED.getCode().equals(state)){
			query.setParameter("indAccreditation", BooleanType.YES.getCode());
		}else if(AccreditationOperationStateType.CONFIRMED.getCode().equals(state)){
			query.setParameter("indAccreditation", BooleanType.NO.getCode());
		}
		query.setParameter("lastModifyApp", lstHAO.get(0).getLastModifyApp());
		query.setParameter("lastModifyDate", lstHAO.get(0).getLastModifyDate());
		query.setParameter("lastModifyIp", lstHAO.get(0).getLastModifyIp());
		query.setParameter("lastModifyUser", lstHAO.get(0).getLastModifyUser());
		query.executeUpdate();
	}
	
	/**
	 * Search holder account balance.
	 *
	 * @param idHolderAccountPks the id holder account pks
	 * @param security the security
	 * @param userInfo the user info
	 * @param lstSecurities the lst securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalance> searchHolderAccountBalance(List<Long> idHolderAccountPks, Security security, UserInfo userInfo, List<String> lstSecurities, Integer motive) throws ServiceException{
		//Creating String builder
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select hab ");
		sbQuery.append(" from HolderAccountBalance hab ");
		sbQuery.append(" left join fetch hab.security sec ");
		sbQuery.append(" left join fetch hab.holderAccount ha ");
		sbQuery.append(" left join fetch ha.participant pa ");
		sbQuery.append(" WHERE  hab.holderAccount.idHolderAccountPk in (:idHolderAccountPks) ");
		
		if(Validations.validateIsNotNull(security.getIdSecurityCodePk())){
			sbQuery.append(" and hab.security.idSecurityCodePk =:securityPk");
		}
		if(Validations.validateIsNotNull(security.getSecurityClass())){
				sbQuery.append(" and hab.security.securityClass =:securityClass");
		}			
//		else 
//			if(Validations.validateIsNull(security.getIdSecurityCodePk()) && Validations.validateIsNull(security.getSecurityClass())){
//			sbQuery.append(" and hab.availableBalance > 0");
//		}
		
		sbQuery.append(" and hab.availableBalance > 0");
		if (Validations.validateIsNotNull(motive)) {
			if(motive!=2816) {
				sbQuery.append(" and hab.security.stateSecurity <> " + SecurityStateType.SUSPENDED.getCode());//SUSPENDIDO issue 1194
			}
		}
		
		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			sbQuery.append( " and sec.issuer.idIssuerPk = :idIssuer " );
			sbQuery.append( " and sec.securityClass in (:lstSecurityClass) ");
		}
		
		if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion() &&
				Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
			sbQuery.append(" and sec.idSecurityCodePk in (:lstSecurities) " );
		}
		
		sbQuery.append(" order by hab.security.idSecurityCodePk asc ");	
		
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountPks", idHolderAccountPks);
		
		if(Validations.validateIsNotNull(security.getIdSecurityCodePk())){
				query.setParameter("securityPk", security.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(security.getSecurityClass())){
					query.setParameter("securityClass", security.getSecurityClass());
		}

		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			   query.setParameter("idIssuer", userInfo.getUserAccountSession().getIssuerCode());
			   List<Integer> lstSecurityClass= new ArrayList<Integer>();
			   lstSecurityClass.add(SecurityClassType.DPF.getCode());
			   lstSecurityClass.add(SecurityClassType.DPA.getCode());
			   query.setParameter("lstSecurityClass", lstSecurityClass);
			  }
		
		if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion() &&
				Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
			query.setParameter("lstSecurities", lstSecurities);
		}
		
		return (List<HolderAccountBalance>)query.getResultList();
	}

	/**
	 * Metodo para buscar valores para CAT B
	 * @param idHolderAccountPks
	 * @param security
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountBalanceExp> searchHolderAccountBalanceB (List<Long> idHolderAccountPks, Long idParticipant,Security security){

		List<HolderAccountBalanceExp> lstAccountBalanceExps = new ArrayList<>();
		
		StringBuilder querySql = new StringBuilder();
		
		querySql.append("  	SELECT                                                                                     ");
		querySql.append("  	HABE.ID_HOLDER_EXP_PK,                                                                     ");
		querySql.append("  	HABE.ID_PARTICIPANT_PK,                                                                    ");
		querySql.append("  	HABE.ID_HOLDER_ACCOUNT_PK,                                                                 ");
		querySql.append("  	HABE.ID_SECURITY_CODE_PK,                                                                  ");
		querySql.append("  	HABE.TOTAL_BALANCE,                                                                        ");
		querySql.append("  	HABE.AVAILABLE_BALANCE,                                                                    ");
		querySql.append("  	HABE.PAWN_BALANCE,                                                                         ");
		querySql.append("  	HABE.BAN_BALANCE,                                                                          ");
		querySql.append("  	HABE.OTHER_BLOCK_BALANCE,                                                                  ");
		querySql.append("  	HABE.ACCREDITATION_BALANCE,                                                                ");
		querySql.append("  	TO_CHAR(P.MNEMONIC || ' - ' || P.ID_PARTICIPANT_PK) PART,                                  ");
		querySql.append("  	HA.ACCOUNT_NUMBER,                                                                         ");
		querySql.append("  	(SELECT                                                                                    ");
		querySql.append("  			LISTAGG(HAD.ID_HOLDER_FK                                                           ");
		querySql.append("  			||' - '                                                                            ");
		querySql.append("  			||(select h.full_name from holder h                                                ");
		querySql.append("  			   where h.ID_HOLDER_PK=HAD.ID_HOLDER_FK)                                          ");
		querySql.append("  			||chr(13)                                                                          ");
		querySql.append("  			)                                                                                  ");
		querySql.append("  	WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK)  												   ");
		querySql.append("  	FROM HOLDER_ACCOUNT_DETAIL HAD  														   ");
		querySql.append("  	WHERE HAD.ID_HOLDER_ACCOUNT_FK = HA.ID_HOLDER_ACCOUNT_PK  								   ");
		querySql.append("  	) HOLDERS                                                                                  ");
		querySql.append("  	FROM HOLDER_ACCOUNT_BALANCE_EXP HABE                                                       ");
		querySql.append("  	INNER JOIN SECURITY SEC ON HABE.ID_SECURITY_CODE_PK = SEC.ID_SECURITY_CODE_PK              ");
		querySql.append("  	INNER JOIN PARTICIPANT P ON P.ID_PARTICIPANT_PK = HABE.ID_PARTICIPANT_PK                   ");
		querySql.append("  	INNER JOIN HOLDER_ACCOUNT HA ON HA.ID_HOLDER_ACCOUNT_PK = HABE.ID_HOLDER_ACCOUNT_PK        ");
		querySql.append("  	WHERE 1 = 1                                                                                ");
		querySql.append("  	and SEC.STATE_SECURITY = " + SecurityStateType.EXPIRED.getCode()                            );
		//Issue 1377
		querySql.append("  	and SEC.SECURITY_CLASS != " + SecurityClassType.DPF.getCode()                            );
		querySql.append("  	AND HA.ID_HOLDER_ACCOUNT_PK IN (:idHolderAccountPks)                                       ");
		
		if(Validations.validateIsNotNull(idParticipant)){
			querySql.append("  	AND P.ID_PARTICIPANT_PK = :idParticipant                   				               ");
		}	
		if(Validations.validateIsNotNull(security.getIdSecurityCodePk())){
			querySql.append("  	AND SEC.ID_SECURITY_CODE_PK = :securityPk                                              ");
		}
		if(Validations.validateIsNotNull(security.getSecurityClass())){
			querySql.append("  	AND SEC.SECURITY_CLASS = :securityClass                                                ");
		}		
		querySql.append("  	UNION ALL                                                                             ");
		querySql.append("  	select                                                                                ");
		querySql.append("  	    (select (select h.ID_HOLDER_PK from HOLDER h where h.ID_HOLDER_PK = had.ID_HOLDER_FK)  ");   
		querySql.append("  	 	 from HOLDER_ACCOUNT_DETAIL had                                                   ");
		querySql.append("    	 where had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK and rownum = 1) as ID_HOLDER_EXP_PK    ");
		querySql.append("  		,hab.ID_PARTICIPANT_PK                                                            ");
		querySql.append("  		,hab.ID_HOLDER_ACCOUNT_PK                                                         ");
		querySql.append("  		,hab.ID_SECURITY_CODE_PK                                                          ");
		querySql.append("  		,hab.TOTAL_BALANCE                                                                ");
		querySql.append("  		,hab.AVAILABLE_BALANCE                                                            ");
		querySql.append("  		,hab.PAWN_BALANCE                                                                 ");
		querySql.append("  		,hab.BAN_BALANCE                                                                  ");
		querySql.append("  		,hab.OTHER_BLOCK_BALANCE                                                          ");
		querySql.append("  		,hab.ACCREDITATION_BALANCE                                                        ");
		querySql.append("  		,to_char(p.MNEMONIC || ' - ' || p.ID_PARTICIPANT_PK) PART                         ");
		querySql.append("  		,ha.ACCOUNT_NUMBER                                                                ");
		querySql.append("  		,(select LISTAGG(had.ID_HOLDER_FK || ' - ' || (                                   ");
		querySql.append("  				         select h.FULL_NAME                                               ");
		querySql.append("  				         from HOLDER h                                                    ");
		querySql.append("  			    	     where h.ID_HOLDER_PK = had.ID_HOLDER_FK) ||chr(13)               ");
		querySql.append("  			        	 ) WITHIN GROUP (order by had.ID_HOLDER_FK)                       ");
		querySql.append("     	  from HOLDER_ACCOUNT_DETAIL had                                                  ");
		querySql.append("  	      where had.ID_HOLDER_ACCOUNT_FK = ha.ID_HOLDER_ACCOUNT_PK) HOLDERS               ");
		querySql.append("  	from HOLDER_ACCOUNT_BALANCE hab                                                       ");
		querySql.append("  		left outer join SECURITY s on s.ID_SECURITY_CODE_PK = hab.ID_SECURITY_CODE_PK                ");
		querySql.append("  		left outer join PARTICIPANT p on p.ID_PARTICIPANT_PK = hab.ID_PARTICIPANT_PK                 ");
		querySql.append("  		left outer join HOLDER_ACCOUNT ha on ha.ID_HOLDER_ACCOUNT_PK = hab.ID_HOLDER_ACCOUNT_PK      ");
		querySql.append("  	where 1 = 1                                                                           ");
		querySql.append("  		and s.STATE_SECURITY = " + SecurityStateType.SUSPENDED.getCode()                   );
		//Issue 1377
		querySql.append("  		and s.SECURITY_CLASS != " + SecurityClassType.DPF.getCode()                        );
		querySql.append("  		and hab.ID_HOLDER_ACCOUNT_PK in (:idHolderAccountPks)                             ");
		if(Validations.validateIsNotNull(idParticipant)){
			querySql.append("  	and p.ID_PARTICIPANT_PK = :idParticipant           				                  ");
		}	
		if(Validations.validateIsNotNull(security.getIdSecurityCodePk())){
			querySql.append("  	and s.ID_SECURITY_CODE_PK = :securityPk                                           ");
		}
		if(Validations.validateIsNotNull(security.getSecurityClass())){
			querySql.append("  	and s.SECURITY_CLASS = :securityClass                                             ");
		}
		querySql.append(" order by 4 asc ");	
		
		Query query = em.createNativeQuery(querySql.toString());
		
		query.setParameter("idHolderAccountPks", idHolderAccountPks);
		
		if(Validations.validateIsNotNull(security.getIdSecurityCodePk())){
			query.setParameter("securityPk", security.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(security.getSecurityClass())){
			query.setParameter("securityClass", security.getSecurityClass());
		}
		if(Validations.validateIsNotNull(idParticipant)){
			query.setParameter("idParticipant", idParticipant);
		}
		
		List<Object[]> lstRestul = query.getResultList();
		
		if (lstRestul.size()>0){
			for (Object[] lstFiltr : lstRestul){
				HolderAccountBalanceExp accountBalanceExp = new HolderAccountBalanceExp();
				
				accountBalanceExp.setIdHolderExpPk(Long.valueOf(lstFiltr[0].toString()));
				accountBalanceExp.setParticipant(Long.valueOf(lstFiltr[1].toString()));
				accountBalanceExp.setHolderAccount(Long.valueOf(lstFiltr[2].toString()));
				accountBalanceExp.setSecurity(lstFiltr[3].toString());
				accountBalanceExp.setTotalBalance((BigDecimal)lstFiltr[4]);
				accountBalanceExp.setAvailableBalance((BigDecimal)lstFiltr[5]);
				accountBalanceExp.setPawnBalance((BigDecimal)lstFiltr[6]);
				accountBalanceExp.setBanBalance((BigDecimal)lstFiltr[7]);
				accountBalanceExp.setOtherBlockBalance((BigDecimal)lstFiltr[8]);
				accountBalanceExp.setAccreditationBalance((BigDecimal)lstFiltr[9]);
				accountBalanceExp.setParticipantDesc((String)lstFiltr[10].toString());
				accountBalanceExp.setAccountNumberDesc(lstFiltr[11].toString());
				accountBalanceExp.setHolderDesc(lstFiltr[12].toString());
				lstAccountBalanceExps.add(accountBalanceExp);
			}
		}	
		return lstAccountBalanceExps;
	}
	
	
	/**
	 * Search reported operation.
	 *
	 * @param idHolderAccountPks the id holder account pks
	 * @param security the security
	 * @param expirationDate the expiration date
	 * @param userInfo the user info
	 * @param lstSecurities the lst securities
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> searchReportedOperation(List<Long> idHolderAccountPks, Security security, Date expirationDate, UserInfo userInfo,List<String> lstSecurities) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();
		Query query = null;
		//Creating query string
		sbQuery.append("SELECT distinct hao.holderAccount.participant.mnemonic, ");//0
		sbQuery.append(" hao.holderAccount.participant.idParticipantPk, ");//1
		sbQuery.append(" hao.holderAccount.accountNumber, ");//2
		sbQuery.append(" had.holder.name, ");//3
		sbQuery.append(" had.holder.firstLastName, ");//4
		sbQuery.append(" had.holder.secondLastName, ");//5
		sbQuery.append(" had.holder.fullName, ");//6
		sbQuery.append(" mo.operationState, ");//7
		sbQuery.append(" mo.operationNumber, ");//8
		sbQuery.append(" mo.securities.idSecurityCodePk, ");//9
		//sbQuery.append(" mo.stockQuantity, ");//10 //issue 1236
		sbQuery.append(" so.stockQuantity, ");//10
		sbQuery.append(" mo.operationDate, ");//11
		sbQuery.append(" mo.termSettlementDate, ");//12
		sbQuery.append(" (mo.termSettlementDate - mo.cashSettlementDate) AS plazo, ");//13
		sbQuery.append(" hao.holderAccount.idHolderAccountPk, ");//14
		sbQuery.append(" mo.idMechanismOperationPk, ");//15
		sbQuery.append(" had.holder.idHolderPk, ");//16 
		sbQuery.append(" hao.idHolderAccountOperationPk, ");//17 
		sbQuery.append(" hao.operationPart ");//18
		
		sbQuery.append(" from SettlementOperation so "); //issue 1236
		sbQuery.append(" inner join so.mechanismOperation mo ");
		sbQuery.append(" inner join mo.holderAccountOperations hao ");
		sbQuery.append(" Inner join hao.holderAccount.holderAccountDetails had ");
		sbQuery.append(" where mo.operationState=:stateOperation ");
		sbQuery.append(" and hao.role=:roleType ");
		sbQuery.append(" and hao.holderAccountState=:holderAccState "); //issue 230
		sbQuery.append(" and mo.mechanisnModality.negotiationModality.indReportingBalance=:indReported ");
		
		//issue 1236
		sbQuery.append(" and so.operationPart=:operationPart ");
		sbQuery.append(" and so.indPrepaid=:indPrepaid ");
		mapParam.put("operationPart", GeneralConstants.TWO_VALUE_INTEGER);
		mapParam.put("indPrepaid", GeneralConstants.ZERO_VALUE_INTEGER);
		
		mapParam.put("stateOperation", MechanismOperationStateType.CASH_SETTLED.getCode());
		mapParam.put("indReported", GeneralConstants.ONE_VALUE_INTEGER);
		mapParam.put("roleType", ParticipantRoleType.BUY.getCode());
		mapParam.put("holderAccState", HolderAccountOperationStateType.CONFIRMED.getCode()); //issue 230

		sbQuery.append("and mo.termSettlementDate >=:expirationDateCat ");
		mapParam.put("expirationDateCat", expirationDate);
		
		if(Validations.validateListIsNotNullAndNotEmpty(idHolderAccountPks)){
			sbQuery.append(" and hao.holderAccount.idHolderAccountPk in (:idHolderAccountPks) ");
			mapParam.put("idHolderAccountPks", idHolderAccountPks);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk())){
			sbQuery.append(" and mo.securities.idSecurityCodePk in (:securityPk) ");
			mapParam.put("securityPk", security.getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(security.getSecurityClass())){
			sbQuery.append(" and mo.securities.securityClass =:securityClass ");
			mapParam.put("securityClass", security.getSecurityClass());
		}
		
		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			sbQuery.append( " and mo.securities.issuer.idIssuerPk = :idIssuer ");
			sbQuery.append(" and mo.securities.securityClass in (:lstSecurityClass) " );
			  }
		
		if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion() &&
				Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
			sbQuery.append(" and mo.securities.idSecurityCodePk in (:lstSecurities) " );
		}

		if (userInfo.getUserAccountSession().isParticipantInstitucion() &&
				Validations.validateIsNotNullAndNotEmpty(userInfo.getUserAccountSession().getParticipantCode())) {
			sbQuery.append(" and mo.buyerParticipant.idParticipantPk=:parIdParticipant ");
		}
		
			sbQuery.append( " order by mo.securities.idSecurityCodePk ASC ");
		
		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
				mapParam.put("idIssuer", userInfo.getUserAccountSession().getIssuerCode());
			   List<Integer> lstSecurityClass= new ArrayList<Integer>();
			   lstSecurityClass.add(SecurityClassType.DPF.getCode());
			   lstSecurityClass.add(SecurityClassType.DPA.getCode());
			   mapParam.put("lstSecurityClass", lstSecurityClass);
			  }
		if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion() &&
				Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
			mapParam.put("lstSecurities", lstSecurities);
		}
		if (userInfo.getUserAccountSession().isParticipantInstitucion() &&
				Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode())) {
			mapParam.put("parIdParticipant", userInfo.getUserAccountSession().getParticipantCode());
		}
		
		query = em.createQuery(sbQuery.toString());
		
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		//getting result list
		List<Object> objectList = query.getResultList(); 
		
		//issue 230
		if(objectList.size()>0){
			HashMap<Long, Object> hmObjectList = new HashMap<>();
			
			for(int i=0;i<objectList.size();++i){
				Object[] objResults = (Object[])objectList.get(i);
				hmObjectList.put((Long)objResults[15], objResults);
			}	
			
			if (hmObjectList.size()>0){
				List<Object> tempObjectList = new ArrayList<Object>();	
				for (Entry keyHash: hmObjectList.entrySet()) {
					tempObjectList.add(keyHash.getValue());
					}
				objectList = tempObjectList;
			}
		}
			
		List<MechanismOperationTO> mechanismOperationTOList = null;
		if(objectList.size()>0){
			mechanismOperationTOList = new ArrayList<MechanismOperationTO>();
			for(int i=0;i<objectList.size();++i){
				String empty="---";
				BigDecimal zero=new BigDecimal(0);
				MechanismOperationTO objMO= new MechanismOperationTO();
				//Getting Results from data base
				Object[] objResults = (Object[])objectList.get(i);

				if(objResults[2]!=null){
					objMO.setAccountHolderNumber((Integer)objResults[2]);
				}
				if(objResults[3]!=null){
					objMO.setNameHolder((String)objResults[3]);
				}
				if(objResults[4]!=null){
					objMO.setFirstLastNameHolder((String)objResults[4]);
				}
				if(objResults[5]!=null){
					objMO.setSecondLastNameHolder((String)objResults[5]);
				}
				if(objResults[6]!=null){
					objMO.setFullNameHolder((String)objResults[6]);
				}else{
					objMO.setFullNameHolder(empty);
				}

				if(objResults[0]!=null){
					objMO.setMnemonicParticipant((String)objResults[0]);
				}else{
					objMO.setMnemonicParticipant(empty);
				}
				if(objResults[1]!=null){
					objMO.setIdParticipant((Long)objResults[1]);
					if(objMO.getMnemonicParticipant()!=null){
						objMO.setParticipant(objMO.getMnemonicParticipant()+" - "+objMO.getIdParticipant().toString());
					}
				}else{
					objMO.setIdParticipant(zero.longValue());
				}

				if(objResults[9]!=null){
					objMO.setIdSecurityCodePk((String)objResults[9]);
				}else{
					objMO.setIdSecurityCodePk(empty);
				}

				if(objResults[7]!=null){
					objMO.setOperationState((Integer)objResults[7]);
					//return description of modality
					ParameterTableTO filterParameterTable = new ParameterTableTO();
					filterParameterTable.setParameterTablePk(MechanismOperationStateType.CASH_SETTLED.getCode());
					filterParameterTable.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
					List<ParameterTable> lstModalityPT= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
					if(lstModalityPT.size()>0){
						ParameterTable objPT= lstModalityPT.get(0);
						objMO.setModalityOperation(objPT.getDescription());
					}
				}else{
					objMO.setOperationState(zero.intValue());
				}
				if(objResults[8]!=null){
					objMO.setOperationNumber((Long)objResults[8]);
				}else{
					objMO.setOperationNumber(zero.longValue());
				}
				if(objResults[10]!=null){
					objMO.setStockQuantity((BigDecimal)objResults[10]);
				}else{
					objMO.setStockQuantity(zero);
				}
				if(objResults[11]!=null){
					objMO.setOperationDate((Date)objResults[11]);
				}
				if(objResults[12]!=null){
					objMO.setTermSettlementDate((Date)objResults[12]);
				}
				if(objResults[13]!=null){
					objMO.setTermReportedOperation((Double)objResults[13]);
					//convert to int setTermReportedOperation
					objMO.setTermInDays(objMO.getTermReportedOperation().intValue());
				}else{
					objMO.setTermReportedOperation(zero.doubleValue());
				}

				if(objResults[14]!=null){
					objMO.setIdHolderAccount((Long)objResults[14]);
				}
				if(objResults[15]!=null){
					objMO.setIdMechanismOperationPk((Long)objResults[15]);
				}
				if(objResults[16]!=null){
					objMO.setIdHolder((Long)objResults[16]);
				}
				if(objResults[17]!=null){
					objMO.setIdHolderAccountOperation((Long)objResults[17]);
				}
				if(objResults[18]!=null){
					objMO.setOperationPart((Integer)objResults[18]);
				}
				
				//add each record into the list
				mechanismOperationTOList.add(objMO);
			}
		}
		
		return mechanismOperationTOList;
	}
	
	/**
	 * Search block operation detail.
	 *
	 * @param holder the holder
	 * @param idHolderAccountPks the id holder account pks
	 * @param tempSecurity the temp security
	 * @param blockType the block type
	 * @param idBlockEntity the id block entity
	 * @param userInfo the user info
	 * @param lstSecurities the lst securities
	 * @return the list< block operation detail>
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<BlockOperation> searchBlockOperationDetail (Holder holder,List<Long> idHolderAccountPks,Security tempSecurity, Integer blockType, Long idBlockEntity, UserInfo userInfo,List<String> lstSecurities)
			throws ServiceException {
		//Creating String Builder
		StringBuilder sbQuery = new StringBuilder();
		//Creating Query Variable
		Map<String,Object> mapParam = new HashMap<String,Object>();
		Query query = null;
		//Creating query string
		sbQuery.append("SELECT  ");		
		sbQuery.append("co.idCustodyOperationPk, ");
		sbQuery.append("BR.idBlockRequestPk, ");
		sbQuery.append("BR.blockNumberDate, ");
		sbQuery.append("BR.blockNumber, ");
		sbQuery.append("BR.blockType, ");
		sbQuery.append("ha.idHolderAccountPk, ");
		sbQuery.append("ha.accountNumber, ");
		sbQuery.append("pa.idParticipantPk, ");
		sbQuery.append("pa.description, ");
		sbQuery.append("sec.idSecurityCodePk, ");
		sbQuery.append("BO.actualBlockBalance, ");
		sbQuery.append("co.registryDate, ");		
		sbQuery.append("BO.originalBlockBalance, ");
		sbQuery.append("sec.currentNominalValue, ");
		sbQuery.append("BO.documentNumber, "); //14
		sbQuery.append("sec.description, ");//15
		sbQuery.append("sec.instrumentType, ");//16
		sbQuery.append("iss.mnemonic, ");//17
		sbQuery.append("iss.idIssuerPk, ");//18
		sbQuery.append("sec.securityClass, ");//19
		sbQuery.append("BO.indAmortization, ");//20
		sbQuery.append("BO.indCashDividend, ");//21
		sbQuery.append("BO.indInterest, ");//22
		sbQuery.append("BO.indReturnContributions, ");//23
		sbQuery.append("BO.indStockDividend, ");//24
		sbQuery.append("BO.indAccreditation, ");//25
		sbQuery.append("BO.indSuscription, ");//26
		sbQuery.append("BO.blockLevel, ");//27
		sbQuery.append("BO.blockState, ");//28
		sbQuery.append("be.idBlockEntityPk, ");//29
		sbQuery.append("be.fullName, ");//30
		sbQuery.append("pa.mnemonic, ");//31
		sbQuery.append("iss.description, ");//32
		sbQuery.append(" sec.expirationDate, ");//33
		sbQuery.append(" co.operationNumber ");//34
		
		sbQuery.append(" FROM BlockRequest BR " );
		sbQuery.append(" join BR.blockOperation BO ");
		sbQuery.append(" left join BO.custodyOperation co " );
		sbQuery.append(" left join BO.holderAccount ha " );
		sbQuery.append(" left join BO.participant pa ");
		sbQuery.append(" left join BR.blockEntity be " );
		sbQuery.append(" left join BO.securities sec " );
		sbQuery.append(" left join sec.issuer iss ");
		sbQuery.append(" WHERE BR.holder.idHolderPk=:idHolderPk ");
		sbQuery.append(" AND BO.blockState=:blockState ");
		
		mapParam.put("blockState", AffectationStateType.BLOCKED.getCode());
		sbQuery.append("AND BO.blockLevel=:blockLevel ");
		mapParam.put("blockLevel", LevelAffectationType.BLOCK.getCode());
		
		mapParam.put("idHolderPk", holder.getIdHolderPk());
		
		if(Validations.validateListIsNotNullAndNotEmpty(idHolderAccountPks)){
			sbQuery.append("   AND BO.holderAccount.idHolderAccountPk in (:idHolderAccountPks)");
			mapParam.put("idHolderAccountPks", idHolderAccountPks);
		}
		
		if(Validations.validateIsNotNull(tempSecurity) && Validations.validateIsNotNullAndNotEmpty(tempSecurity.getIdSecurityCodePk())){
			sbQuery.append(" and sec.idSecurityCodePk =:securityPk ");
			mapParam.put("securityPk", tempSecurity.getIdSecurityCodePk());
		}
		
		if(Validations.validateIsNotNull(tempSecurity) && Validations.validateIsNotNullAndNotEmpty(tempSecurity.getSecurityClass())){
			sbQuery.append(" and sec.securityClass =:securityClass ");
			mapParam.put("securityClass", tempSecurity.getSecurityClass());
		}
		
		//Validating if blockType is not equal 0
		if (Validations.validateIsNotNull(blockType)) {			
			//Searching by blockType
			sbQuery.append("   AND BR.blockType=:blockType");
			mapParam.put("blockType", blockType);	
		}
		if(Validations.validateIsNotNull(idBlockEntity)){
			sbQuery.append("   AND BR.blockEntity.idBlockEntityPk=:blockEntity");
			mapParam.put("blockEntity", idBlockEntity);
		}
		
		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			sbQuery.append( " and sec.issuer.idIssuerPk = :idIssuer ");
			sbQuery.append(" and sec.securityClass in (:lstSecurityClass) " );
		}
		
		if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion() &&
				Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
			sbQuery.append(" and sec.idSecurityCodePk in (:lstSecurities) " );
		}
		
		sbQuery.append(" and sec.stateSecurity <> " + SecurityStateType.SUSPENDED.getCode());//SUSPENDIDO issue 1194
		
		sbQuery.append(" order by sec.idSecurityCodePk asc ");

		if (Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode())) {
			mapParam.put("idIssuer", userInfo.getUserAccountSession().getIssuerCode());
			   List<Integer> lstSecurityClass= new ArrayList<Integer>();
			   lstSecurityClass.add(SecurityClassType.DPF.getCode());
			   lstSecurityClass.add(SecurityClassType.DPA.getCode());
			   mapParam.put("lstSecurityClass", lstSecurityClass);
		}
		
		if (userInfo.getUserAccountSession().isParticipantInvestorInstitucion() &&
				Validations.validateListIsNotNullAndNotEmpty(lstSecurities)) {
			mapParam.put("lstSecurities", lstSecurities);
		}
		
		//Setting the query
		query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		//getting result list
		List<Object> objectList = query.getResultList(); 
		List<BlockOperation> blockOperationList = null;
		if(objectList.size()>0){
			blockOperationList = new ArrayList<BlockOperation>();
			for(int i=0;i<objectList.size();++i){
				BlockOperation blockOperation = new BlockOperation();
				//Getting Results from data base
				Object[] sResults = (Object[])objectList.get(i);
				BlockRequest blockRequest = new BlockRequest();
				CustodyOperation custodyOperation = new CustodyOperation();
				custodyOperation.setIdCustodyOperationPk((Long)sResults[0]);
				custodyOperation.setOperationNumber((Long)sResults[34]);
				custodyOperation.setRegistryDate((Date)sResults[11]);
				blockOperation.setCustodyOperation(custodyOperation);
				blockOperation.setIdBlockOperationPk((Long)sResults[0]);
				blockRequest.setIdBlockRequestPk((Long)sResults[1]);
				blockRequest.setBlockNumberDate((Date)sResults[2]);
				blockRequest.setBlockNumber((String) sResults[3]);
				blockRequest.setBlockType((Integer) sResults[4]);
				if(blockRequest.getBlockType()!=null){
					//return description of modality
					ParameterTableTO filterParameterTable = new ParameterTableTO();
					filterParameterTable.setParameterTablePk(blockRequest.getBlockType());
					filterParameterTable.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
					List<ParameterTable> lstModalityPT= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
					if(lstModalityPT.size()>0){
						ParameterTable objPT= lstModalityPT.get(0);
						blockRequest.setBlockTypeDescription(objPT.getDescription());
					}
					
				}
				
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk((Long)sResults[5]);
				holderAccount.setAccountNumber((Integer) sResults[6]);
				
				Participant participant = new Participant();
				participant.setIdParticipantPk((Long)sResults[7]);
				participant.setDescription((String) sResults[8]);
				participant.setMnemonic((String)sResults[31]);
				
				Security security = new Security();
				security.setIdSecurityCodePk((String) sResults[9]);
				security.setCurrentNominalValue((BigDecimal)sResults[13]);
				security.setDescription((String)sResults[15]);
				security.setInstrumentType((Integer)sResults[16]);
				security.setSecurityClass((Integer)sResults[19]);
				security.setExpirationDate((Date)sResults[33]);
				
				Issuer issuer = new Issuer();
				issuer.setIdIssuerPk((String)sResults[18]);
				issuer.setMnemonic((String)sResults[17]);
				issuer.setDescription((String)sResults[32]);
				security.setIssuer(issuer);
				
				BlockEntity blockEntity = new BlockEntity();
				blockEntity.setIdBlockEntityPk((Long)sResults[29]);
				blockEntity.setFullName((String)sResults[30]);
				blockRequest.setBlockEntity(blockEntity);
				
				blockOperation.setHolderAccount(holderAccount);
				blockOperation.setParticipant(participant);
				blockOperation.setSecurities(security);
				blockOperation.setBlockRequest(blockRequest);
				blockOperation.setHolderAccount(holderAccount);
				blockOperation.setActualBlockBalance((BigDecimal) sResults[10]);
				blockOperation.setOriginalBlockBalance((BigDecimal)sResults[12]);
				blockOperation.setNominalValue((BigDecimal)sResults[13]);
				blockOperation.setDocumentNumber((Long)sResults[14]);
				blockOperation.setIndAmortization((Integer)sResults[20]);
				blockOperation.setIndCashDividend((Integer)sResults[21]);
				blockOperation.setIndInterest((Integer)sResults[22]);
				blockOperation.setIndReturnContributions((Integer)sResults[23]);
				blockOperation.setIndStockDividend((Integer)sResults[24]);
				blockOperation.setIndAccreditation((Integer)sResults[25]);
				blockOperation.setIndSuscription((Integer)sResults[26]);
				blockOperation.setBlockLevel((Integer)sResults[27]);
				blockOperation.setBlockState((Integer)sResults[28]);
				if(blockOperation.getBlockLevel()!=null){
					//return description of modality
					ParameterTableTO filterParameterTable = new ParameterTableTO();
					filterParameterTable.setParameterTablePk(blockOperation.getBlockLevel());
					filterParameterTable.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
					List<ParameterTable> lstBlock= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
					if(lstBlock.size()>0){
						ParameterTable objPT= lstBlock.get(0);
						blockOperation.setBlockLevelDescription(objPT.getDescription());
					}
					
				}
				//Adding to List
				blockOperationList.add(blockOperation);
			}
		}
		
		return blockOperationList; 
	}
	
	/**
	 * Gets the lst holder account for doc number and doc type.
	 *
	 * @param participant the participant
	 * @return the lst holder account for doc number and doc type
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getLstHolderAccountForDocNumberAndDocType(Participant participant) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		List<Long> lstHolderAccounts = new ArrayList<Long>();
		
		stringBuilderQry.append(" 	SELECT DISTINCT                                        ");
		stringBuilderQry.append(" 	    HA.ID_HOLDER_ACCOUNT_PK                            ");
		stringBuilderQry.append(" 	FROM                                                   ");
		stringBuilderQry.append(" 	    HOLDER H,                                          ");
		stringBuilderQry.append(" 	    HOLDER_ACCOUNT HA,                                 ");
		stringBuilderQry.append(" 	    HOLDER_ACCOUNT_DETAIL HAD                          ");
		stringBuilderQry.append(" 	WHERE                                                  ");
		stringBuilderQry.append(" 	    H.STATE_HOLDER = :stateHolder AND		           ");
		stringBuilderQry.append(" 	    H.DOCUMENT_TYPE = :documentType AND 	           ");
		stringBuilderQry.append(" 	    H.DOCUMENT_NUMBER = :documentNumber AND            ");
		stringBuilderQry.append(" 	    H.ID_HOLDER_PK = HAD.ID_HOLDER_FK AND              ");
		stringBuilderQry.append(" 	    HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
		
		Query query = em.createNativeQuery(stringBuilderQry.toString());
		
		query.setParameter("stateHolder", HolderStateType.REGISTERED.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(participant.getDocumentType())){
			query.setParameter("documentType", participant.getDocumentType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(participant.getDocumentNumber())){
			query.setParameter("documentNumber", participant.getDocumentNumber());
		}

		List<BigDecimal> lstResult = query.getResultList();
		
		for(BigDecimal obj : lstResult){
			Long dato = new Long(obj.toString());
			lstHolderAccounts.add(dato);
		}
		return lstHolderAccounts;
	}
	
	/**
	 * Gets the lst securities for part inv.
	 *
	 * @param participant the participant
	 * @param holderAccounts the holder accounts
	 * @return the lst securities for part inv
	 * @throws ServiceException the service exception
	 */
	public List<String> getLstSecuritiesForPartInv(Participant participant,List<Long> holderAccounts) throws ServiceException{
		StringBuilder stringBuilderQry = new StringBuilder();
		List<String> lstSecurities = new ArrayList<String>();
		
		stringBuilderQry.append(" 	SELECT DISTINCT                                         ");
		stringBuilderQry.append(" 	    S.ID_SECURITY_CODE_PK                              	");
		stringBuilderQry.append(" 	FROM                                                   	");
		stringBuilderQry.append(" 	    SECURITY S,                                        	");
		stringBuilderQry.append(" 	    HOLDER_ACCOUNT_BALANCE HAB                         	");
		stringBuilderQry.append(" 	WHERE                                                  	");
		stringBuilderQry.append(" 	    S.ID_SECURITY_CODE_PK = HAB.ID_SECURITY_CODE_PK AND	");
		stringBuilderQry.append(" 	    HAB.AVAILABLE_BALANCE > 0  AND	");
		stringBuilderQry.append(" 	    S.STATE_SECURITY = :state AND 	           					");
		stringBuilderQry.append(" 	    S.INSTRUMENT_TYPE = :instrument AND            		");
		stringBuilderQry.append(" 	    HAB.ID_PARTICIPANT_PK = :participant AND            ");
		stringBuilderQry.append(" 	    HAB.ID_HOLDER_ACCOUNT_PK IN (:lstHolderAccounts) 	");//lstHolderAccounts
		
		Query query = em.createNativeQuery(stringBuilderQry.toString());
		
		query.setParameter("state", SecurityStateType.REGISTERED.getCode());
		query.setParameter("instrument", InstrumentType.FIXED_INCOME.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(participant.getIdParticipantPk())){
			query.setParameter("participant", participant.getIdParticipantPk());
		}
		if(Validations.validateListIsNotNullAndNotEmpty(holderAccounts)){
			query.setParameter("lstHolderAccounts", holderAccounts);
		}

		List<String>  lstResult = query.getResultList();
		
		for(String obj : lstResult){
			lstSecurities.add(obj);
		}
		return lstSecurities;
	}
	
	/**
	 * Search block entity.
	 *
	 * @param idAccreditationOperationPk the id accreditation operation pk
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public BlockEntity searchBlockEntity(Long idAccreditationOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("Select BOC.blockOperation.blockRequest.blockEntity.idBlockEntityPk, ");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockEntity.fullName,");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockNumber,");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockNumberDate,");
		sbQuery.append("       BOC.blockOperation.blockRequest.blockType");
		sbQuery.append("  FROM BlockOperationCertification BOC ");
		sbQuery.append(" where BOC.accreditationDetail.accreditationOperation.idAccreditationOperationPk =:idAccreditationPk");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("idAccreditationPk", idAccreditationOperationPk);
		//getting result list
		List<Object> objectList = query.getResultList(); 
		BlockEntity blockEntityTmp = new BlockEntity();
		for(int i=0;i<objectList.size();++i){
			Object[] sResults = (Object[])objectList.get(i);
			blockEntityTmp.setIdBlockEntityPk((Long)sResults[0]);
			blockEntityTmp.setFullName((String)sResults[1]);
		}
		return blockEntityTmp;
	}
	
	/**
	 * Search holder account operation to update.
	 *
	 * @param objAO the obj ao
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountOperation> searchHolderAccountOperationToUpdate(AccreditationOperation objAO, LoggerUser loggerUser) 
																			throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<HolderAccountOperation> lstHao=null;
		Map<String,Object> mapParam = new HashMap<String,Object>();
		sbQuery.append("Select mixto.idReportAccreditationPk, ");
	    sbQuery.append(" hao.idHolderAccountOperationPk ");
	    sbQuery.append(" from AccreditationDetail ad ");
		sbQuery.append(" join ad.accreditationReported mixto join mixto.holderAccountOperationFk hao ");
		sbQuery.append(" WHERE ad.accreditationOperation.idAccreditationOperationPk =:idOperation");
		mapParam.put("idOperation", objAO.getCustodyOperation().getIdCustodyOperationPk());
		
		Query query = em.createQuery(sbQuery.toString());
		
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }

		List<Object> objectList = query.getResultList();
		if(objectList.size()>0){
			lstHao= new ArrayList<HolderAccountOperation>();
			for(int i=0;i<objectList.size();++i){
				HolderAccountOperation objHao= new HolderAccountOperation();
				//Getting Results from data base
				Object[] sResults = (Object[])objectList.get(i);
				if(sResults[1]!=null){
					objHao.setIdHolderAccountOperationPk((Long)sResults[1]);	
				}
				if(loggerUser.getIdPrivilegeOfSystem()!=null){
					objHao.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
				}
				if(loggerUser.getAuditTime()!=null){
					objHao.setLastModifyDate(loggerUser.getAuditTime());	
				}
				if(loggerUser.getIpAddress()!=null){
					objHao.setLastModifyIp(loggerUser.getIpAddress());
				}
				if(loggerUser.getUserName()!=null){
					objHao.setLastModifyUser(loggerUser.getUserName());
				}
				lstHao.add(objHao);
			}
		}
		return lstHao;
	}
	
	/**
	 * Accreditation consult.
	 *
	 * @param aOperation the a operation
	 * @param initialDate the initial date
	 * @param endDate the end date
	 * @param currency the currency
	 * @param securityClass the security class
	 * @param issuer the issuer
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationOperation> accreditationAvailableConsult
	(AccreditationOperation aOperation, Date initialDate, Date endDate, Integer currency, Integer securityClass, String issuer)throws ServiceException {
	
		StringBuilder sbQuery = new StringBuilder();
		List<Integer> lstCertificationTypeAll = new ArrayList<Integer>();		
		lstCertificationTypeAll.add(CertificationType.AVAILABLE_BALANCE.getCode());
		lstCertificationTypeAll.add(CertificationType.BLOCKED_BALANCE.getCode());
		lstCertificationTypeAll.add(CertificationType.REPORTED_BALANCE.getCode());

		List<Integer> lstCertificationTypeAR = new ArrayList<Integer>();		
		lstCertificationTypeAR.add(CertificationType.AVAILABLE_BALANCE.getCode());
		lstCertificationTypeAR.add(CertificationType.REPORTED_BALANCE.getCode());
		
		List<Integer> lstCertificationTypeBR = new ArrayList<Integer>();		
		lstCertificationTypeBR.add(CertificationType.BLOCKED_BALANCE.getCode());
		lstCertificationTypeBR.add(CertificationType.REPORTED_BALANCE.getCode());
		
		//Building the Query
		sbQuery.append("Select" );
		sbQuery.append(" ao ");
		sbQuery.append(" from AccreditationDetail ad ");
		sbQuery.append(" left join ad.accreditationOperation ao ");
		sbQuery.append(" left join fetch ao.custodyOperation co ");
		sbQuery.append(" left join ao.participantPetitioner participantPetitioner ");

		sbQuery.append(" WHERE 1=1");
		if(aOperation.getCertificationType()!=null){
			sbQuery.append(" AND ao.certificationType in :certificationType");
		}
		
		if(aOperation.getAccreditationState()!=null){
			sbQuery.append(" AND ao.accreditationState =:statePrm");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(aOperation.getCatType())){
			sbQuery.append(" AND ao.catType =:catType");
		}
		
		Query query = null;

		if (aOperation.getPetitionerType() != null && aOperation.getPetitionerType() != 0) {
			sbQuery.append(" AND ao.petitionerType =:petitionerPrm");
		}
		
		if (aOperation.getHolder().getIdHolderPk() != null) {
			sbQuery.append(" AND ao.holder.idHolderPk =:holderIdPrm");
		}

		if (aOperation.getParticipantPetitioner() != null && Validations.validateIsNotNull(aOperation.getParticipantPetitioner().getIdParticipantPk()) && aOperation.getParticipantPetitioner().getIdParticipantPk()!=0) {
			sbQuery.append(" AND ao.participantPetitioner.idParticipantPk =:participantPetitionerPrm");
		}
		
		if(aOperation.getMotive()!=null){
			sbQuery.append(" AND ao.motive =:motivePrm ");
		}
		if(aOperation.getCertificateNumber()!=null){
			sbQuery.append(" AND ao.certificateNumber =:certificateNumber ");
		}
		
		if(aOperation.getSecurity().getIdSecurityCodePk()!=null){
			sbQuery.append(" AND ad.security.idSecurityCodePk =:securityCodepk ");
		}
	
		if(currency!=null){
			sbQuery.append(" AND ad.security.currency =:currency ");
		}
		
		if(securityClass!=null){
			sbQuery.append(" AND ad.security.securityClass =:securityClass ");
		}
		if(issuer!=null){
			sbQuery.append(" AND ad.security.issuer.idIssuerPk =:issuer ");
		}
		
		
		if (aOperation.getCustodyOperation() != null) {
			if(aOperation.getCustodyOperation().getOperationNumber() != null && aOperation.getCustodyOperation().getOperationNumber() != 0 ){
				sbQuery.append(" AND ao.custodyOperation.operationNumber =:requestNumber");
			}
			else if(Validations.validateIsNullOrNotPositive(aOperation.getCertificateNumber())){
				sbQuery.append(" AND Trunc(ao.custodyOperation.registryDate) between :registerDatePrm and :endDatePrm ");
				sbQuery.append(" ORDER BY ao.custodyOperation.operationNumber DESC ");
			}
		}
				
		query = em.createQuery(sbQuery.toString());
		
		if(aOperation.getAccreditationState()!=null){
			query.setParameter("statePrm", aOperation.getAccreditationState());
		}
		
		if(aOperation.getCertificateNumber()!=null){
			query.setParameter("certificateNumber", aOperation.getCertificateNumber());
		}
		
		if(currency!=null){
			query.setParameter("currency", currency);
		}
		
		if(securityClass!=null){
			query.setParameter("securityClass", securityClass);
		}
		
		if(issuer!=null){
			query.setParameter("issuer", issuer);
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(aOperation.getCatType())){
			query.setParameter("catType", aOperation.getCatType());
		}

		if(aOperation.getCertificationType()!=null){
			if(aOperation.getCertificationType().equals(CertificationType.REPORTED_BALANCE.getCode())){
				query.setParameter("certificationType",lstCertificationTypeAll);
			}
			else{
				if(aOperation.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())){
					query.setParameter("certificationType",lstCertificationTypeAR);
				}
				if(aOperation.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())){
					query.setParameter("certificationType",lstCertificationTypeBR);
				}
		
			}
		}

		
		if (aOperation.getCustodyOperation() != null) {
			if(aOperation.getCustodyOperation().getOperationNumber() != null && aOperation.getCustodyOperation().getOperationNumber() != 0){
				query.setParameter("requestNumber", aOperation.getCustodyOperation().getOperationNumber());			
			}
			else if(Validations.validateIsNullOrNotPositive(aOperation.getCertificateNumber())){
				query.setParameter("registerDatePrm", initialDate);
				query.setParameter("endDatePrm", endDate);
			}
		}
		
		if (aOperation.getPetitionerType() != null && aOperation.getPetitionerType() != 0) {
			query.setParameter("petitionerPrm", aOperation.getPetitionerType());			
		}
		
		if (aOperation.getHolder().getIdHolderPk() != null) {
			query.setParameter("holderIdPrm", aOperation.getHolder().getIdHolderPk());	
		}
		
		if (aOperation.getParticipantPetitioner() != null && Validations.validateIsNotNull(aOperation.getParticipantPetitioner().getIdParticipantPk()) && aOperation.getParticipantPetitioner().getIdParticipantPk()!=0) {
			query.setParameter("participantPetitionerPrm", aOperation.getParticipantPetitioner().getIdParticipantPk());
		}
		
		if(aOperation.getMotive()!=null){
			query.setParameter("motivePrm", aOperation.getMotive());
		}
		
		if(aOperation.getSecurity().getIdSecurityCodePk()!=null){
			query.setParameter("securityCodepk", aOperation.getSecurity().getIdSecurityCodePk());
		}

		return (List<AccreditationOperation>)query.getResultList();
	}
	
	
	/**
	 * 
	 * @param idSecurityCode
	 * @return
	 */

	public Integer securityType(String idSecurityCode){
		
		StringBuilder stringBuilderQry = new StringBuilder();
		Object test = new Object();
		stringBuilderQry.append("    Select se.instrumentType from Security  se ");
		stringBuilderQry.append("    where 1=1 and se.idSecurityCodePk = :idSecurityCode  ");
		
		Query query = em.createQuery(stringBuilderQry.toString());
		
		query.setParameter("idSecurityCode", idSecurityCode);
		
		test= query.getSingleResult();
		Integer instrument = (Integer) test;
		return instrument;
	}
	
	/**
	 * 
	 * @param idProgramAmoCoupon
	 * @return
	 */

	public Date verifyAmortizationCoupon(Long idProgramAmoCoupon){
		
		StringBuilder stringBuilderQry = new StringBuilder();
		Object test = new Object();
		stringBuilderQry.append("    select EXPRITATION_DATE from PROGRAM_AMORTIZATION_COUPON ");
		stringBuilderQry.append("    where 1=1   ");
		stringBuilderQry.append("    and ID_AMO_PAYMENT_SCHEDULE_FK= :idProgramAmoCoupon  ");
		stringBuilderQry.append("    and STATE_PROGRAM_AMORTIZATON= :state  ");
	//	stringBuilderQry.append("    and trunc(PAYMENT_DATE) > sysdate  ");
		stringBuilderQry.append("    and rownum=1  ");
		stringBuilderQry.append("    order by COUPON_NUMBER asc  ");
		
		Query query = em.createNativeQuery(stringBuilderQry.toString());
		
		query.setParameter("state", ProgramScheduleStateType.PENDING.getCode());
		query.setParameter("idProgramAmoCoupon", idProgramAmoCoupon);
		
		test= query.getSingleResult();
		Date createdDate = (Date) test;
		return createdDate;
	}
	
	/**
	 * Verify accreditation operation reported.
	 *
	 * @param objMechanism the obj mechanism
	 * @return the accreditation operation
	 */
	@SuppressWarnings("unchecked")
	public AccreditationOperation verifyAccreditationOperationReported(MechanismOperationTO objMechanism){
		StringBuilder sbQuery= new StringBuilder();
		AccreditationOperation objAO= null;
		CustodyOperation custodyOperation = new CustodyOperation();
		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(AccreditationOperationStateType.REGISTRED.getCode());
		lstStates.add(AccreditationOperationStateType.CONFIRMED.getCode());
		//Creating query
		sbQuery.append("select ra.idReportAccreditationPk, ao.idAccreditationOperationPk, 		");//0,1
		sbQuery.append("	co.idCustodyOperationPk, co.operationNumber 						");//2,3
		
		sbQuery.append("from ReportAccreditation ra												");
		sbQuery.append("	join ra.holderAccountOperationFk hao 								");
		sbQuery.append("	join ra.accreditationDetailFk ad 									");
		sbQuery.append("	join ad.accreditationOperation ao 									");
		sbQuery.append("	join ao.custodyOperation co 										");
		
		sbQuery.append("where 1=1 " );

		if(objMechanism.getIdHolderAccountOperation()!=null){
			sbQuery.append("	and hao.idHolderAccountOperationPk=:idHAO	");
			sbQuery.append("	and ao.accreditationState in :lstStateOperation	");
		}		
		Query query = em.createQuery(sbQuery.toString());
		if(objMechanism.getIdHolderAccountOperation()!=null){
			query.setParameter("idHAO", objMechanism.getIdHolderAccountOperation());
			query.setParameter("lstStateOperation", lstStates);
		}		
		List<Object> objectList = query.getResultList();
		if(objectList.size()>0){
			objAO=new AccreditationOperation();
			Object[] sResults = (Object[])objectList.get(0);
			objAO.setIdAccreditationOperationPk((Long)sResults[1]);
			custodyOperation.setIdCustodyOperationPk((Long)sResults[2]);
			custodyOperation.setOperationNumber((Long)sResults[3]);
			objAO.setCustodyOperation(custodyOperation);
		}
		return objAO;
	}
	
	/**
	 * Verify accreditation operation.
	 *
	 * @param objBlockOp the obj block op
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List <AccreditationOperation> verifyAccreditationOperation(BlockOperation objBlockOp) throws ServiceException{
		StringBuilder sbQuery= new StringBuilder();

		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(AccreditationOperationStateType.REGISTRED.getCode());
		lstStates.add(AccreditationOperationStateType.CONFIRMED.getCode());
		//Creating query
//		sbQuery.append(" select boc.blockOperationCertPk, ao.idAccreditationOperationPk, co.operationNumber " );
		sbQuery.append(" select ao " );
		sbQuery.append(" from BlockOperationCertification boc " );
		sbQuery.append(" join boc.blockOperation bo  ");
		sbQuery.append(" join boc.accreditationDetail ad " );
		sbQuery.append(" join ad.accreditationOperation ao " );
		sbQuery.append(" left join fetch ao.custodyOperation co " );	
		sbQuery.append(" where bo.idBlockOperationPk=boc.blockOperation.idBlockOperationPk  ");

		if(objBlockOp.getIdBlockOperationPk()!=null){
			sbQuery.append("	and boc.blockOperation.idBlockOperationPk=:idOperation ");
			sbQuery.append("	and ao.accreditationState in :lstStateOperation ");
		}
		
		Query query = em.createQuery(sbQuery.toString());

		if(objBlockOp.getIdBlockOperationPk()!=null){
			query.setParameter("idOperation", objBlockOp.getIdBlockOperationPk());
			query.setParameter("lstStateOperation", lstStates);
		}
		
		List <AccreditationOperation> objAO=null;
		objAO = query.getResultList();
//		if(objectList.size()>0){
//			objAO=new AccreditationOperation();
//			Object[] sResults = (Object[])objectList.get(0);
//			objAO.setIdAccreditationOperationPk((Long)sResults[1]);
//			objAO.getCustodyOperation().setOperationNumber((Long)sResults[2]);
//		}
		
		return objAO;
	}
	
	/**
	 * Metodo que devuelve el archivo adjunto.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the byte[]
	 * @throws ServiceException the service exception
	 */
	public byte [] documentFile(Long accreditationOperationId)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select ao.accreditationCertificate " );
		sbQuery.append(" FROM AccreditationOperation ao " );
		sbQuery.append(" where ao.idAccreditationOperationPk = :accreditationOperationId ");
		Map<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("accreditationOperationId", accreditationOperationId);
		return (byte[]) findObjectByQueryString(sbQuery.toString(), parameters);
	}
	
	/**
	 * Metodo que devuelve la lista de encargados de recojo.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationDeliveryInformation> accreditationDeliver(Long accreditationOperationId) throws ServiceException{		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" Select acc " );
		sbQuery.append(" FROM AccreditationDeliveryInformation acc " );
		sbQuery.append(" inner join fetch AccreditationOperation ao " );
		sbQuery.append(" where ao.idAccreditationOperationPk = :accreditationOperationId ");
		Map<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("accreditationOperationId", accreditationOperationId);
		Query query = em.createQuery(sbQuery.toString());		
		List<AccreditationDeliveryInformation> accreditationDeliver1 = query.getResultList(); 
		return   accreditationDeliver1;
	}

	/**
	 * Find accreditation operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public AccreditationOperation findAccreditationOperation(Long accreditationOperationId)throws ServiceException {
		//String builder
		StringBuilder sbQuery = new StringBuilder();	
//		AccreditationOperation accreditationOperation = new AccreditationOperation();
		//Creating query
		sbQuery.append("Select ao " );
		sbQuery.append("FROM AccreditationOperation AO " );
		sbQuery.append(" left JOIN fetch AO.holder H " );
		sbQuery.append(" left JOIN fetch AO.participantPetitioner PP " );
		sbQuery.append(" inner JOIN fetch AO.custodyOperation co " );
		sbQuery.append(" left JOIN fetch AO.listAccreditationDeliveryInformation lad " );
		sbQuery.append("where AO.idAccreditationOperationPk = :accreditationOperationId ");
		Map<String,Object> parameters = new HashMap<String, Object>();
		// Setting parameter
		parameters.put("accreditationOperationId", accreditationOperationId);
		//Query Result
		return (AccreditationOperation) findObjectByQueryString(sbQuery.toString(), parameters);		
	}
	
	/**
	 * Search accreditation detail by accreditation operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationDetail> searchAccreditationDetailByAccreditationOperation(Long accreditationOperationId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("	Select AD.totalBalance,");
		sbQuery.append(		  "AD.availableBalance, " );
		sbQuery.append(		  "AD.security.idSecurityCodePk, ");
		sbQuery.append(	      "AD.holderAccount.participant.mnemonic,");
		sbQuery.append(		  "AD.holderAccount.accountNumber, ");
		sbQuery.append(	      "AD.holderAccount.idHolderAccountPk, ");
		sbQuery.append(	      "AD.holderAccount.participant.idParticipantPk, ");
		sbQuery.append(		  "AD.banBalance, " ); //7
		sbQuery.append(		  "AD.otherBalance, " ); //8
		sbQuery.append(		  "AD.pawnBalance, " );//9
		sbQuery.append(		  "AD.reportingBalance, " );//10
		sbQuery.append(		  "AD.idAccreditationDetailPk, " );//11
		sbQuery.append(		  "AD.security.securityClass, ");//12
		sbQuery.append(		  "AO.idAccreditationOperationPk, ");//13
		sbQuery.append(		  "AO.validityDays, ");//14
		sbQuery.append(		  "AO.holder.idHolderPk, ");//15
		sbQuery.append(		  "AO.holder.fullName ");//16
		sbQuery.append("	FROM AccreditationDetail AD	");
		sbQuery.append("	JOIN AD.accreditationOperation AO	");
		sbQuery.append("	where AO.custodyOperation.idCustodyOperationPk=:accreditationOperationId	");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("accreditationOperationId", accreditationOperationId);
			
		List<AccreditationDetail> accreditationDetailList = new ArrayList<AccreditationDetail>();
		//Result List	
		List<Object> objectList = query.getResultList(); 
		for(int i=0;i<objectList.size();++i){
			//Setting new Holder Account balance
			AccreditationDetail accreditationDetail = new AccreditationDetail();
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			//Creating new HolderAccount
			HolderAccount holderAccount = new HolderAccount();
			//Creating new Participant
			Participant participant = new Participant();
		
			//Setting mNemonic to participant
			participant.setMnemonic((String) sResults[3]);
			participant.setIdParticipantPk((Long)sResults[6]);
			//Setting Account Number to holder Account
			holderAccount.setAccountNumber((Integer) sResults[4]);
			//Setting Participant to holder Account
			holderAccount.setParticipant(participant);
			//Setting Id Holder Account pk to holder Account
			holderAccount.setIdHolderAccountPk((Long)sResults[5]);
			//Setting Total Balance from result
			accreditationDetail.setTotalBalance((BigDecimal)sResults[0]);
			accreditationDetail.setBanBalance((BigDecimal)sResults[7]);
			accreditationDetail.setOtherBalance((BigDecimal)sResults[8]);
			accreditationDetail.setPawnBalance((BigDecimal)sResults[9]);
			accreditationDetail.setReportingBalance((BigDecimal)sResults[10]);
			accreditationDetail.setIdAccreditationDetailPk((Long)sResults[11]);
			//Setting Available Balance from result
			accreditationDetail.setAvailableBalance((BigDecimal) sResults[1]);
			//Setting Security object
			Security sec = new Security();
			sec.setIdSecurityCodePk((String) sResults[2]);
			sec.setSecurityClass((Integer) sResults[12]);
			accreditationDetail.setSecurity(sec);
			//Setting Holder Account object
			accreditationDetail.setHolderAccount(holderAccount);
			
			AccreditationOperation objAccreditationOperation = new AccreditationOperation();
			objAccreditationOperation.setIdAccreditationOperationPk((Long) sResults[13]);
			objAccreditationOperation.setValidityDays((Integer) sResults[14]);			
			Holder objHolder = new Holder();
			if(Validations.validateIsNotNullAndNotEmpty(sResults[15]) && 
					Validations.validateIsNotNullAndNotEmpty(sResults[16])){
				objHolder.setIdHolderPk((Long) sResults[15]);
				objHolder.setFullName(sResults[16].toString());
			}			
			objAccreditationOperation.setHolder(objHolder);
			accreditationDetail.setAccreditationOperation(objAccreditationOperation);
			//Setting Object Account Balance Object to List.
			accreditationDetailList.add(accreditationDetail);
		}
		return accreditationDetailList;		
	}
	
	/**
	 * Find block operation certification.
	 *
	 * @param idAccreditationOperation the id accreditation operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperation> findBlockOperationCertification(Long idAccreditationOperation)throws ServiceException {
		//String builder
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("Select ");		
		sbQuery.append("BO.blockNumberDate, ");
		sbQuery.append("BO.blockNumber, ");
		sbQuery.append("BO.blockType, ");
		sbQuery.append("BOD.holderAccount.idHolderAccountPk, ");
		sbQuery.append("BOD.holderAccount.accountNumber, ");
		sbQuery.append("BOD.participant.idParticipantPk, ");
		sbQuery.append("BOD.participant.mnemonic, ");
		sbQuery.append("BOD.securities.idSecurityCodePk, ");
		sbQuery.append("BOD.actualBlockBalance, ");
		sbQuery.append("BOD.custodyOperation.registryDate, ");		
		sbQuery.append("BOD.originalBlockBalance, ");
		sbQuery.append("BOD.nominalValue, ");
		sbQuery.append("BOD.custodyOperation.idCustodyOperationPk, ");
		sbQuery.append("BOD.documentNumber, ");
		sbQuery.append("(Select p.parameterName From ParameterTable p Where p.parameterTablePk = BO.blockType) ");
		sbQuery.append("FROM AccreditationDetail accreditationDetail left join accreditationDetail.holderAccount ha");
		sbQuery.append("left join accreditationDetail.idParticipantFk part ");
		sbQuery.append("left join accreditationDetail.blockOperationCertifications bOperationCert ");
		sbQuery.append("left join bOperationCert.blockOperation BOD left join BOD.custodyOperation co ");
		sbQuery.append("left join BOD.blockRequest BO ");
		sbQuery.append("where accreditationDetail.accreditationOperation.custodyOperation.idCustodyOperationPk=:idAccreditationOperation");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("idAccreditationOperation", idAccreditationOperation);
		//Result List
		@SuppressWarnings("unchecked")
		List<Object> objectList = query.getResultList(); 
		List<BlockOperation> blockOperationDetailList = new ArrayList<BlockOperation>();
		for(int i=0;i<objectList.size();++i){
			BlockOperation blockOperationDetail = new BlockOperation();
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			BlockRequest blockOperation = new BlockRequest();				
						
			blockOperation.setBlockNumberDate((Date)sResults[0]);
			blockOperation.setBlockNumber((String) sResults[1]);
			blockOperation.setBlockType((Integer) sResults[2]);
			blockOperation.setBlockTypeDescription((String) sResults[14]);
			
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk((Long)sResults[3]);
			holderAccount.setAccountNumber((Integer) sResults[4]);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long)sResults[5]);
			participant.setMnemonic((String) sResults[6]);
			
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setIdCustodyOperationPk((Long) sResults[12]);
			custodyOperation.setRegistryDate((Date)sResults[9]);
			
			Security security = new Security();
			security.setIdSecurityCodePk(((String) sResults[7]));
			blockOperationDetail.setCustodyOperation(custodyOperation);
			blockOperationDetail.setActualBlockBalance((BigDecimal) sResults[10]);
			blockOperationDetail.setNominalValue((BigDecimal) sResults[11]);
			blockOperationDetail.setDocumentNumber((Long) sResults[13]);
			blockOperationDetail.setHolderAccount(holderAccount);
			blockOperationDetail.setParticipant(participant);
			blockOperationDetail.setSecurities(security);
			blockOperationDetail.setBlockRequest(blockOperation);
			blockOperationDetail.setHolderAccount(holderAccount);
			blockOperationDetail.setActualBlockBalance((BigDecimal) sResults[8]);
			//Adding to List
			blockOperationDetailList.add(blockOperationDetail);			
		}
		return blockOperationDetailList;
	}
	
	/**
	 * Gets the list parameter table service bean.
	 *
	 * @param filter the filter
	 * @return the list parameter table service bean
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParameterTable> getListParameterTable(ParameterTableTO filter) throws ServiceException	{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select P.parameterTablePk,P.parameterName From ParameterTable P ");
		sbQuery.append(" Where 1 = 1 ");

		if(Validations.validateIsNotNull(filter.getState())){
			sbQuery.append(" And P.parameterState = :statePrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
			sbQuery.append(" And P.parameterTablePk = :parameterTablePkPrm");
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
			sbQuery.append(" And P.masterTable.masterTablePk = :masterTableFkPrm");
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
			sbQuery.append(" And P.parameterTableCd = :parameterTableCdPrm");
		}
				
		sbQuery.append(" order by P.parameterName");
    	Query query = em.createQuery(sbQuery.toString());  

    	if(Validations.validateIsNotNull(filter.getState())){
        	query.setParameter("statePrm", filter.getState());
		}

		if(Validations.validateIsNotNull(filter.getParameterTablePk())){
	    	query.setParameter("parameterTablePkPrm", filter.getParameterTablePk());
		}

		if(Validations.validateIsNotNull(filter.getMasterTableFk())){
	    	query.setParameter("masterTableFkPrm", filter.getMasterTableFk());
		}

		if(Validations.validateIsNotNull(filter.getParameterTableCd())){
	    	query.setParameter("parameterTableCdPrm", filter.getParameterTableCd());
		}
		List<Object> objectList = query.getResultList();
		List<ParameterTable> parameterTableList = new ArrayList<ParameterTable>();
		for(int i=0;i<objectList.size();++i){			
			//Getting Results from data base
			Object[] sResults = (Object[])objectList.get(i);
			ParameterTable parameterTable = new ParameterTable();
			parameterTable.setParameterTablePk((Integer) sResults[0]);
			parameterTable.setParameterName((String)sResults[1]);
			parameterTableList.add(parameterTable);
		}
		return parameterTableList;
	}
	
	/**
	 * Find accreditation operation by pk.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @return the accreditation operation
	 */
	public AccreditationOperation findAccreditationOperationByPk(AccreditationOperationTO accreditationOperationTO){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT AO " );
		sbQuery.append("  FROM AccreditationOperation AO ");
		sbQuery.append("  Inner Join Fetch AO.custodyOperation co ");
		sbQuery.append(" WHERE 1=1");
		
		if(Validations.validateIsNotNullAndPositive(accreditationOperationTO.getIdAccreditationOperationPk())){
			sbQuery.append(" AND   AO.idAccreditationOperationPk=:idAccreditationOperationPk ");
		}
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndPositive(accreditationOperationTO.getIdAccreditationOperationPk())){
			query.setParameter("idAccreditationOperationPk", accreditationOperationTO.getIdAccreditationOperationPk());
		}
		AccreditationOperation accreditationOperation=(AccreditationOperation) query.getSingleResult();
		return accreditationOperation;
	}
	
	/**
	 * Search accreditation operation.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationOperation> searchAccreditationOperation(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String,Object>();
		//String builder
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("SELECT AO " );
		sbQuery.append("  FROM AccreditationOperation AO ");
		sbQuery.append("  Inner Join Fetch AO.custodyOperation co ");
		sbQuery.append(" WHERE 1=1");
		
		if(Validations.validateIsNotNull(accreditationOperationTO.getRegisterDate())){
			sbQuery.append(" AND AO.registerDate <=:endDate ");
			mapParam.put("endDate", accreditationOperationTO.getRegisterDate());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getAccreditationState())){
			sbQuery.append(" AND AO.accreditationState =:accreditationState");
			mapParam.put("accreditationState", accreditationOperationTO.getAccreditationState());
		}
		
		if(Validations.validateIsNotNull(accreditationOperationTO.getDestinationCertificate())){
			sbQuery.append("AND AO.destinationCertificate =:destinationCertificate");
			mapParam.put("destinationCertificate", accreditationOperationTO.getDestinationCertificate());
		}
		
		if(Validations.validateIsNotNull(accreditationOperationTO.getIndDelivered())){
			sbQuery.append("AND AO.indDelivered =:indDelivered");
			mapParam.put("indDelivered", accreditationOperationTO.getIndDelivered());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getRejectMotiveOther())){
			sbQuery.append(" AND AO.rejectMotiveOther=:rejectMotiveOther");
			mapParam.put("rejectMotiveOther", accreditationOperationTO.getRejectMotiveOther());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getIdRejectMotiveFk())){
			sbQuery.append(" AND AO.idRejectMotive=:idRejectMotiveFk");
			mapParam.put("idRejectMotiveFk", accreditationOperationTO.getIdRejectMotiveFk());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getIdSignatureCertificateFk())){
			sbQuery.append(" AND AO.idSignatureCertificateFk=:idSignatureCertificateFk");
			mapParam.put("idSignatureCertificateFk", accreditationOperationTO.getIdSignatureCertificateFk());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getExpirationDate())){
			sbQuery.append(" AND AO.expirationDate=:expirationDate");
			mapParam.put("expirationDate", accreditationOperationTO.getExpirationDate());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getLastModifyApp())){
			sbQuery.append(" AND AO.lastModifyApp=:lastModifyApp");
			mapParam.put("lastModifyApp", accreditationOperationTO.getLastModifyApp());
		}
		
		if(Validations.validateIsNotNull(accreditationOperationTO.getLastModifyDate())){
			sbQuery.append(" AND AO.lastModifyDate=:lastModifyDate");
			mapParam.put("lastModifyDate", accreditationOperationTO.getLastModifyDate());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getLastModifyIp())){
			sbQuery.append(" AND AO.lastModifyIp=:lastModifyIp");
			mapParam.put("lastModifyIp", accreditationOperationTO.getLastModifyIp());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getLastModifyUser())){
			sbQuery.append(" AND AO.lastModifyUser=:lastModifyUser");
			mapParam.put("lastModifyUser", accreditationOperationTO.getLastModifyUser());
		}
		if(Validations.validateIsNotNull(accreditationOperationTO.getWayOfPayment())){
			sbQuery.append(" AND AO.wayOfPayment=:wayOfPayment");
			mapParam.put("wayOfPayment", accreditationOperationTO.getWayOfPayment());
		}
		
		Query query = em.createQuery(sbQuery.toString());
		//Building Itarator 
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }               
		return (List<AccreditationOperation>) query.getResultList();
	}
	
	/**
	 * Accreditation available consult by revert.
	 *
	 * @param endDate the end date
	 * @param lstStates the lst states
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationOperation> accreditationAvailableConsultByRevert(Date endDate , List<Integer> lstStates)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("	Select accreditationOperation	" );		
        sbQuery.append(" from AccreditationOperation accreditationOperation  ");
        sbQuery.append(" Inner Join Fetch accreditationOperation.custodyOperation co  ");
		sbQuery.append(" WHERE trunc(co.registryDate) = :endDate ");		
		sbQuery.append(" AND accreditationOperation.accreditationState IN :lstStatePrm");				
		Query query = em.createQuery(sbQuery.toString());				
		query.setParameter("endDate", endDate);
		query.setParameter("lstStatePrm", lstStates);
		return (List<AccreditationOperation>) query.getResultList();		
	}
	
	/**
	 * Search accreditation detail by operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationDetail> searchAccreditationDetailByOperation(Long accreditationOperationId) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("	Select DISTINCT AD	");		
		sbQuery.append("	FROM AccreditationDetail AD	");
		sbQuery.append("	JOIN AD.accreditationOperation AO	");
		sbQuery.append("	Inner Join Fetch AD.accreditationMarketFactList	AML ");
		sbQuery.append("	where AO.custodyOperation.idCustodyOperationPk=:accreditationOperationId	");
		sbQuery.append("	and AML.indActive=:parOne	");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("accreditationOperationId", accreditationOperationId);
		query.setParameter("parOne", GeneralConstants.ONE_VALUE_INTEGER);
		
		return (List<AccreditationDetail>) query.getResultList();		
	}
	
	/**
	 * Find reported operation.
	 *
	 * @param idAccreditationOperation the id accreditation operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<MechanismOperationTO> findReportedOperation(Long idAccreditationOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> mapParam = new HashMap<String,Object>();
		Query query = null;
		//Creating query string
		sbQuery.append("SELECT HAO.holderAccount.participant.mnemonic, ");//0
		sbQuery.append("HAO.holderAccount.participant.idParticipantPk, ");//1
		sbQuery.append("HAO.holderAccount.accountNumber, ");//2
		sbQuery.append("HAD.holder.name, ");//3
		sbQuery.append("HAD.holder.firstLastName, ");//4
		sbQuery.append("HAD.holder.secondLastName, ");//5
		sbQuery.append("HAD.holder.fullName, ");//6
		sbQuery.append("MO.operationState, ");//7
		sbQuery.append("MO.operationNumber, ");//8
		sbQuery.append("MO.securities.idSecurityCodePk, ");//9
		sbQuery.append("MO.stockQuantity, ");//10
		sbQuery.append("MO.operationDate, ");//11
		sbQuery.append("MO.termSettlementDate, ");//12
		sbQuery.append("(MO.termSettlementDate - MO.cashSettlementDate) AS plazo, ");//13
		sbQuery.append("HAO.holderAccount.idHolderAccountPk, ");//14
		sbQuery.append("MO.idMechanismOperationPk, ");//15
		sbQuery.append("HAD.holder.idHolderPk, ");//16
		sbQuery.append("HAO.idHolderAccountOperationPk ");//17
		sbQuery.append(" 	FROM AccreditationDetail AC    ");
		sbQuery.append("	JOIN AC.accreditationReported AR	");
		sbQuery.append("	JOIN AR.holderAccountOperationFk HAO 	");
		sbQuery.append("	JOIN HAO.mechanismOperation MO 	");
		sbQuery.append("	JOIN HAO.holderAccount.holderAccountDetails HAD 	");
		sbQuery.append("	WHERE AC.accreditationOperation.custodyOperation.idCustodyOperationPk=:idAccreditationOperation");			
		mapParam.put("idAccreditationOperation", idAccreditationOperation);						
		query = em.createQuery(sbQuery.toString());		
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
		//Iterating
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
        	//Building the entry
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	//Setting Param
        	query.setParameter(entry.getKey(), entry.getValue());
        }
		//getting result list
		List<Object> objectList = query.getResultList(); 
		List<MechanismOperationTO> mechanismOperationTOList = null;
		if(objectList.size()>0){
			mechanismOperationTOList = new ArrayList<MechanismOperationTO>();
			for(int i=0;i<objectList.size();++i){
				String empty="---";
				BigDecimal zero=new BigDecimal(0);
				MechanismOperationTO objMO= new MechanismOperationTO();
				//Getting Results from data base
				Object[] objResults = (Object[])objectList.get(i);

				if(objResults[2]!=null){
					objMO.setAccountHolderNumber((Integer)objResults[2]);
				}
				if(objResults[3]!=null){
					objMO.setNameHolder((String)objResults[3]);
				}
				if(objResults[4]!=null){
					objMO.setFirstLastNameHolder((String)objResults[4]);
				}
				if(objResults[5]!=null){
					objMO.setSecondLastNameHolder((String)objResults[5]);
				}
				if(objResults[6]!=null){
					objMO.setFullNameHolder((String)objResults[6]);
				}else{
					objMO.setFullNameHolder(empty);
				}

				if(objResults[0]!=null){
					objMO.setMnemonicParticipant((String)objResults[0]);
				}else{
					objMO.setMnemonicParticipant(empty);
				}
				if(objResults[1]!=null){
					objMO.setIdParticipant((Long)objResults[1]);
					if(objMO.getMnemonicParticipant()!=null){
						objMO.setParticipant(objMO.getMnemonicParticipant()+" - "+objMO.getIdParticipant().toString());
					}
				}else{
					objMO.setIdParticipant(zero.longValue());
				}

				if(objResults[9]!=null){
					objMO.setIdSecurityCodePk((String)objResults[9]);
				}else{
					objMO.setIdSecurityCodePk(empty);
				}

				if(objResults[7]!=null){
					objMO.setOperationState((Integer)objResults[7]);
					//return description of modality
					ParameterTableTO filterParameterTable = new ParameterTableTO();
					filterParameterTable.setParameterTablePk(MechanismOperationStateType.CASH_SETTLED.getCode());
					filterParameterTable.setMasterTableFk(MasterTableType.ESTADOS_OPERACION_MCN.getCode());
					List<ParameterTable> lstModalityPT= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
					if(lstModalityPT.size()>0){
						ParameterTable objPT= lstModalityPT.get(0);
						objMO.setModalityOperation(objPT.getDescription());
					}
				}else{
					objMO.setOperationState(zero.intValue());
				}
				if(objResults[8]!=null){
					objMO.setOperationNumber((Long)objResults[8]);
				}else{
					objMO.setOperationNumber(zero.longValue());
				}
				if(objResults[10]!=null){
					objMO.setStockQuantity((BigDecimal)objResults[10]);
				}else{
					objMO.setStockQuantity(zero);
				}
				if(objResults[11]!=null){
					objMO.setOperationDate((Date)objResults[11]);
				}
				if(objResults[12]!=null){
					objMO.setTermSettlementDate((Date)objResults[12]);
				}
				if(objResults[13]!=null){
					objMO.setTermReportedOperation((Double)objResults[13]);
					//convert to int setTermReportedOperation
					objMO.setTermInDays(objMO.getTermReportedOperation().intValue());
				}else{
					objMO.setTermReportedOperation(zero.doubleValue());
				}

				if(objResults[14]!=null){
					objMO.setIdHolderAccount((Long)objResults[14]);
				}
				if(objResults[15]!=null){
					objMO.setIdMechanismOperationPk((Long)objResults[15]);
				}
				if(objResults[16]!=null){
					objMO.setIdHolder((Long)objResults[16]);
				}
				if(objResults[17]!=null){
					objMO.setIdHolderAccountOperation((Long)objResults[17]);
				}
				//add each record into the list
				mechanismOperationTOList.add(objMO);
			}
		}		
		return mechanismOperationTOList;
	}
	
	/**
	 * Searchh accreditation detail mixed.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationDetail> searchhAccreditationDetailMixed(Long accreditationOperationId) throws ServiceException{
		ParameterTableTO filterParameterTable = new ParameterTableTO();		
		ParameterTableTO filterParameterTable1 = new ParameterTableTO();		
		filterParameterTable.setMasterTableFk(MasterTableType.TYPE_AFFECTATION.getCode());
		List<ParameterTable> lstTypeAffectacion= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable);
		
		filterParameterTable1.setMasterTableFk(MasterTableType.LEVEL_AFFECTATION.getCode());
		List<ParameterTable> lstLevelAffectation= generalParameterFacade.getListParameterTableServiceBean(filterParameterTable1);
		
		List<AccreditationDetail> accreditationDetail = new ArrayList<AccreditationDetail>();	
		
		BlockOperation bo = new BlockOperation();
		
		String strQuery= "	SELECT DISTINCT AD FROM AccreditationDetail AD "  +
						"	INNER JOIN FETCH  AD.holder	" +
						"	INNER JOIN FETCH  AD.holderAccount	" +						
						"	INNER JOIN FETCH  AD.security	" +
						"	INNER JOIN FETCH  AD.idParticipantFk	" +
						"	LEFT OUTER JOIN FETCH  AD.blockOperationCertifications BOC	" +
						"	WHERE AD.accreditationOperation.idAccreditationOperationPk=:accreditationOperationId	";
		Query query = em.createQuery(strQuery);
		query.setParameter("accreditationOperationId", accreditationOperationId);
		accreditationDetail = query.getResultList();		
		for(AccreditationDetail ad : accreditationDetail){
			ad.getAccreditationReported().size();	
			for(com.pradera.model.custody.accreditationcertificates.BlockOperationCertification boc : ad.getBlockOperationCertifications()){
				bo = boc.getBlockOperation();
				bo.getBlockLevel();
				bo.getBlockRequest().getBlockEntity().getIdBlockEntityPk();
				bo.getCustodyOperation().getIdCustodyOperationPk();
				bo.getSecurities().getIssuer().getIdIssuerPk();
				bo.getHolderAccount().getIdHolderAccountPk();
				bo.getParticipant().getIdParticipantPk();
				for(ParameterTable parameter: lstTypeAffectacion){
					if(parameter.getParameterTablePk().equals(boc.getBlockType())){
						boc.getBlockOperation().getBlockRequest().setBlockTypeDescription(parameter.getDescription());
					}
				}	
				for(ParameterTable parameter: lstLevelAffectation){
					if(parameter.getParameterTablePk().equals(bo.getBlockLevel())){
						boc.getBlockOperation().setBlockLevelDescription(parameter.getDescription());
					}
				}	
			}
			for(ReportAccreditation ra : ad.getAccreditationReported()){
				ra.getHolderAccountOperationFk().getMechanismOperation().getIdMechanismOperationPk();				
			}			
		}
		return accreditationDetail;
	}
	
	/**
	 * Issuer list.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Issuer> issuerList() throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		//Creating query
		sbQuery.append("	Select su	");		
		sbQuery.append("	FROM Issuer su	");
		sbQuery.append("	where su.stateIssuer=:stateIs	");
		sbQuery.append("	order by su.mnemonic	");
		Query query = em.createQuery(sbQuery.toString());
		// Setting parameter
		query.setParameter("stateIs", IssuerStateType.REGISTERED.getCode());
		return (List<Issuer>) query.getResultList();

	}
	
	/**
	 * Generate accreditation operation register to.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateAccreditationOperationRegisterTO(AccreditationOperation accreditationOperation, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		AffectationRegisterTO affectationRegisterTO= getAccreditationOperationRegisterTO(accreditationOperation);
		affectationRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(affectationRegisterTO, 
				GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransactionTx(affectationRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}

	/**
	 * Gets the accreditation operation register to.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @return the accreditation operation register to
	 */
	public AffectationRegisterTO getAccreditationOperationRegisterTO(AccreditationOperation accreditationOperation) {
		AffectationRegisterTO affectationRegisterTO = new AffectationRegisterTO();
		
		affectationRegisterTO.setOperationNumber(accreditationOperation.getCustodyOperation().getOperationNumber());
		affectationRegisterTO.setIdCustodyOperation(accreditationOperation.getCustodyOperation().getIdCustodyOperationPk());
		affectationRegisterTO.setOperationCode(ComponentConstant.INTERFACE_STOCK_BLOCKING_DPF);
		List<AccreditationDetail> details = null;
		try {
			details = searchAccreditationDetailByAccreditationOperation(accreditationOperation.getIdAccreditationOperationPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(details)){
			Security security = this.find(Security.class, details.get(0).getSecurity().getIdSecurityCodePk());		
			Participant objParticipant = participantServiceBean.getParticipantIsIssuer(security.getIssuer().getIdIssuerPk());
			if(objParticipant != null){
				affectationRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
				affectationRegisterTO.setEntityNemonic(objParticipant.getMnemonic());
			}						
			
			affectationRegisterTO.setOperationDate(CommonsUtilities.currentDate());
			
			HolderAccount objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountForCustody(details.get(0).getHolderAccount());
			if(objHolderAccount != null){
				affectationRegisterTO.setAccountNumber(objHolderAccount.getAccountNumber());
				affectationRegisterTO.setDocumentNumber(objHolderAccount.getHolderAccountDetails().get(0).getHolder().getDocumentNumber().trim());
			}			
			
			SecurityObjectTO securityObjectTO = holderBalanceMovementsServiceBean.populateSecurityObjectTO(security);
			securityObjectTO.setUnblockRequirement(GeneralConstants.STR_UNBLOCK_TAG_FALSE);
			affectationRegisterTO.setSecurityObjectTO(securityObjectTO);
			
			AffectationObjectTO affectationObjetTO = new AffectationObjectTO();
			LegalInformationObjetTO legalInformationObjetTO = new LegalInformationObjetTO();
			
			affectationObjetTO.setAffectationCode(ComponentConstant.AFFECTATION_CODE_DPF_ECA);
			affectationObjetTO.setLockingDays(details.get(0).getAccreditationOperation().getValidityDays());
			affectationObjetTO.setReferenceDocument(accreditationOperation.getCustodyOperation().getOperationNumber().toString());
			affectationObjetTO.setDocumentType(ComponentConstant.ONE);
			affectationObjetTO.setReceiptDate(CommonsUtilities.currentDate());			
			if(details.get(0).getAccreditationOperation().getHolder() != null){
				affectationObjetTO.setIdHolder(details.get(0).getAccreditationOperation().getHolder().getIdHolderPk());			
				affectationObjetTO.setDescriptionHolder(details.get(0).getAccreditationOperation().getHolder().getFullName());
			} else {
				affectationObjetTO.setIdHolder(null);			
				affectationObjetTO.setDescriptionHolder(GeneralConstants.STR_UNBLOCK_TAG);
			}			
			legalInformationObjetTO.setCourtCode(null);
			legalInformationObjetTO.setJobNumber(null);
			legalInformationObjetTO.setRecord(null);
			legalInformationObjetTO.setApplicant(null);
			legalInformationObjetTO.setMotive(null);
			legalInformationObjetTO.setObservations(ComponentConstant.NOT_ACTIVE);
			
			affectationObjetTO.setLegalInformationObjetTO(legalInformationObjetTO);
			affectationRegisterTO.setAffectationObjectTO(affectationObjetTO);
		}				
		return affectationRegisterTO;
	}
	
	/**
	 * Generate unblock accreditation operation register to.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @param idInterfaceProcess the id interface process
	 * @param loggerUser the logger user
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<RecordValidationType> generateUnblockAccreditationOperationRegisterTO(AccreditationOperation accreditationOperationToUnblock, Long idInterfaceProcess, 
			LoggerUser loggerUser) throws ServiceException{
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		
		StockUnblockRegisterTO stockUnblockRegisterTO= getUnblockAccreditationOperationRegisterTO(accreditationOperationToUnblock);
		stockUnblockRegisterTO.setIdInterfaceProcess(idInterfaceProcess);
		RecordValidationType objRecordValidationType = CommonsUtilities.populateRecordCodeValidation(stockUnblockRegisterTO, 
				GenericPropertiesConstants.MSG_INTERFACE_TRANSACTION_SUCCESS, null);
		lstRecordValidationTypes.add(objRecordValidationType);
		interfaceComponentServiceBean.get().saveInterfaceTransactionTx(stockUnblockRegisterTO, BooleanType.YES.getCode(), loggerUser);
		return lstRecordValidationTypes;
	}

	/**
	 * Gets the unblock accreditation operation register to.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @return the unblock accreditation operation register to
	 */
	public StockUnblockRegisterTO getUnblockAccreditationOperationRegisterTO(AccreditationOperation accreditationOperationToUnblock) {
		StockUnblockRegisterTO stockUnblockRegisterTO = new StockUnblockRegisterTO();
		
		stockUnblockRegisterTO.setOperationNumber(accreditationOperationToUnblock.getCustodyOperation().getOperationNumber());
		stockUnblockRegisterTO.setIdCustodyOperation(accreditationOperationToUnblock.getCustodyOperation().getIdCustodyOperationPk());
		stockUnblockRegisterTO.setOperationCode(ComponentConstant.INTERFACE_STOCK_UNBLOCKING_DPF);
		List<AccreditationDetail> details = null;
		try {
			details = searchAccreditationDetailByAccreditationOperation(accreditationOperationToUnblock.getIdAccreditationOperationPk());
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(details)){
			Security security = this.find(Security.class, details.get(0).getSecurity().getIdSecurityCodePk());
			Participant objParticipant = participantServiceBean.getParticipantIsIssuer(security.getIssuer().getIdIssuerPk());
			if(objParticipant != null){
				stockUnblockRegisterTO.setIdParticipant(objParticipant.getIdParticipantPk());
				stockUnblockRegisterTO.setEntityNemonic(objParticipant.getMnemonic());
			}		
			
			stockUnblockRegisterTO.setOperationDate(CommonsUtilities.currentDate());
			
			HolderAccount objHolderAccount = holderBalanceMovementsServiceBean.getHolderAccountForCustody(details.get(0).getHolderAccount());
			if(objHolderAccount != null){
				stockUnblockRegisterTO.setAccountNumber(objHolderAccount.getAccountNumber());
				stockUnblockRegisterTO.setDocumentNumber(objHolderAccount.getHolderAccountDetails().get(0).getHolder().getDocumentNumber().trim());
			}			
			
			SecurityObjectTO securityObjectTO = holderBalanceMovementsServiceBean.populateSecurityObjectTO(security);
			stockUnblockRegisterTO.setSecurityObjectTO(securityObjectTO);
			
			StockUnblockObjectTO stockUnblockObjectTO = new StockUnblockObjectTO();
			LegalInformationObjetTO legalInformationObjetTO = new LegalInformationObjetTO();
			
			stockUnblockObjectTO.setAffectationCode(ComponentConstant.AFFECTATION_CODE_DPF_ECA);
			stockUnblockObjectTO.setReferenceDocument(accreditationOperationToUnblock.getCustodyOperation().getOperationNumber().toString());			
			stockUnblockObjectTO.setObservations(ComponentConstant.NOT_ACTIVE);
			
			legalInformationObjetTO.setCourtCode(null);
			legalInformationObjetTO.setJobNumber(null);
			legalInformationObjetTO.setRecord(null);
			legalInformationObjetTO.setApplicant(null);
			legalInformationObjetTO.setMotive(null);
			
			stockUnblockObjectTO.setLegalInformationObjetTO(legalInformationObjetTO);
			stockUnblockRegisterTO.setStockUnblockObjectTO(stockUnblockObjectTO);
		}		
		return stockUnblockRegisterTO;
	}
	
	/**
	 * Search accreditation operation by unblock.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<AccreditationOperation> searchAccreditationOperationByUnblock(AccreditationOperationTO accreditationOperationTO) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT AO " );
		sbQuery.append("  FROM AccreditationOperation AO ");
		sbQuery.append("  Inner Join Fetch AO.custodyOperation co ");
		sbQuery.append("  Inner Join AO.accreditationDetails adet ");
		sbQuery.append("  Inner Join adet.security sec ");
		sbQuery.append(" WHERE 1=1 ");						
		
		if(Validations.validateIsNotNull(accreditationOperationTO.getAccreditationState())){
			sbQuery.append(" AND AO.accreditationState =:accreditationState ");
			mapParam.put("accreditationState", accreditationOperationTO.getAccreditationState());
		}
		
		if(Validations.validateIsNotNull(accreditationOperationTO.getExpirationDate())){
			sbQuery.append(" AND ( trunc(AO.expirationDate) <= trunc(:expirationDate) ");
			mapParam.put("expirationDate", accreditationOperationTO.getExpirationDate());			
			sbQuery.append(" or trunc(sec.expirationDate) = trunc(:expirationSec) ) ");
			mapParam.put("expirationSec", accreditationOperationTO.getExpirationDate());
		}
		if(accreditationOperationTO.getSecurity()!=null &&
				Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getSecurity().getIdSecurityCodePk())){
			sbQuery.append(" AND adet.security.idSecurityCodePk =:idSecurityCode ");
			mapParam.put("idSecurityCode", accreditationOperationTO.getSecurity().getIdSecurityCodePk());
		}
		
		Query query = em.createQuery(sbQuery.toString());
		Iterator it= mapParam.entrySet().iterator();
        while(it.hasNext()){
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }               
		return (List<AccreditationOperation>) query.getResultList();
	}
	
	/**
	 * Gets the list block accreditation operation web service.
	 *
	 * @param sendDate the send date
	 * @param flagBlock the flag block
	 * @return the list block accreditation operation web service
	 * @throws ServiceException the service exception
	 */
	public List<AccreditationOperation> getListBlockAccreditationOperationWebService(Date sendDate,boolean flagBlock) throws ServiceException{
		List<AccreditationOperation> lstBlockAccreditationOperation = null;
		
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT AO FROM AccreditationOperation AO ");
		stringBuffer.append(" 	inner join fetch AO.custodyOperation co ");
		stringBuffer.append(" 	inner join fetch AO.accreditationDetails ad ");
		stringBuffer.append(" 	inner join fetch ad.security sec ");
		stringBuffer.append(" WHERE trunc(co.confirmDate) = trunc(:date) ");
		stringBuffer.append(" 	and AO.accreditationState = :opState ");
		stringBuffer.append(" 	and co.indWebservice = :indWebService ");
		stringBuffer.append(" 	and sec.securityClass in (:classDPF,:classDPA) ");
		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("date", sendDate);
		if(flagBlock){
			query.setParameter("opState", AccreditationOperationStateType.CONFIRMED.getCode());
		}else{
			query.setParameter("opState", AccreditationOperationStateType.UNBLOCKED.getCode());
		}
		query.setParameter("indWebService", ComponentConstant.ZERO);
		query.setParameter("classDPF", SecurityClassType.DPF.getCode());
		query.setParameter("classDPA", SecurityClassType.DPA.getCode());
		lstBlockAccreditationOperation  = (List<AccreditationOperation>) query.getResultList();
		
		return lstBlockAccreditationOperation;
	}
	
	
	/**
	 * Accreditation consult verification Cat.
	 *
	 * @param accreditationOperationTO the accreditation operation to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationDetail> getAccreditationOperationForVerificationCat(AccreditationOperationTO accreditationOperationTO) throws ServiceException {	
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select ad " );
		sbQuery.append(" from AccreditationDetail ad ");
		sbQuery.append(" left join fetch ad.accreditationOperation ao ");		
		sbQuery.append(" left join fetch ad.holder ho ");
		sbQuery.append(" left join fetch ad.holderAccount ha ");
		sbQuery.append(" left join fetch ad.security se ");
		sbQuery.append(" WHERE 1=1 ");	
		sbQuery.append(" and ao.accreditationState=:state ");
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getSecurityCodeCat())){
			sbQuery.append(" AND ao.securityCodeCat = :securityCodeCat");
		}						
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(accreditationOperationTO.getSecurityCodeCat())){
			query.setParameter("securityCodeCat", accreditationOperationTO.getSecurityCodeCat());
		}
		query.setParameter("state", AccreditationOperationStateType.CONFIRMED.getCode());
		return (List<AccreditationDetail>)query.getResultList();
	}
	
	/**
	 * Extraemos la clase de valor de la solicitud de CAT
	 * @param certificationId
	 * @return
	 * @throws ServiceException
	 */
	public Integer getSecurityClassRequest(Long certificationId) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();	
		
		sbQuery.append(" 	SELECT DISTINCT SEC.SECURITY_CLASS                                                 	");
		sbQuery.append(" 	FROM ACCREDITATION_DETAIL AD                                                       	");
		sbQuery.append(" 	INNER JOIN SECURITY SEC ON AD.ID_SECURITY_CODE_FK = SEC.ID_SECURITY_CODE_PK        	");
		sbQuery.append(" 	WHERE AD.ID_ACCREDITATION_OPERATION_FK = :certificationId                          	");
		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		query.setParameter("certificationId", certificationId);
		
		return Integer.valueOf(query.getSingleResult().toString());
	}
	
	
	
	/**
	 * Gets the list digital signature.
	 *
	 * @param typeSignatureOne the type signature one
	 * @param typeSignatureTwo the type signature two
	 * @return the list digital signature
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public boolean getListDigitalSignature(Integer typeSignatureOne,Integer typeSignatureTwo) throws ServiceException {
		boolean verifySignature=false;
		List<DigitalSignature> lstDigitalSignature = new ArrayList<DigitalSignature>();
		List<Integer> names = Arrays.asList(typeSignatureOne, typeSignatureTwo);
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Select ds.idDigitalSignaturePk from DigitalSignature ds ");
			sbQuery.append(" where ds.typeSignature IN :names ");
			sbQuery.append(" and ds.activeSignature = 1");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("names", names);
			lstDigitalSignature = (List<DigitalSignature>) query.getResultList();
			if (lstDigitalSignature.size()==2)
				verifySignature=true;
			
		} catch (NoResultException e) {
			return false;
		}
		return verifySignature;
	}
	
	/**
	 * Verify accreditation operation by registry.
	 *
	 * @param holderAccountBalanceExp
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List <AccreditationOperation> verifyAccreditationOperationByRegistry(HolderAccountBalanceExp holderAccountBalanceExp,Holder holder) throws ServiceException{
				
		List<Integer> lstStates = new ArrayList<Integer>();		
		lstStates.add(AccreditationOperationStateType.REGISTRED.getCode());
		lstStates.add(AccreditationOperationStateType.CONFIRMED.getCode());

		StringBuilder sbQuery= new StringBuilder();
		sbQuery.append(" SELECT ao " );
		sbQuery.append(" FROM AccreditationDetail ad " );
		sbQuery.append(" JOIN ad.accreditationOperation ao " );
		sbQuery.append(" LEFT JOIN FETCH ao.custodyOperation co " );	
		sbQuery.append(" WHERE 1=1 ");
		sbQuery.append(" AND ao.accreditationState in :lstStateOperation ");
		sbQuery.append(" AND ad.holderAccount.idHolderAccountPk = :parIdHolderAccount ");
		sbQuery.append(" AND ad.security.idSecurityCodePk = :parIdSecurityCodePk ");
		sbQuery.append(" AND ad.idParticipantFk.idParticipantPk = :parIdParticipantPk ");
		sbQuery.append(" AND ad.holder.idHolderPk = :parIdHolderPk ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("lstStateOperation", lstStates);
		query.setParameter("parIdHolderAccount", holderAccountBalanceExp.getHolderAccount());
		query.setParameter("parIdSecurityCodePk", holderAccountBalanceExp.getSecurity());
		query.setParameter("parIdParticipantPk", holderAccountBalanceExp.getParticipant());
		query.setParameter("parIdHolderPk", holder.getIdHolderPk());
		
		List <AccreditationOperation> objAO=null;
		objAO = query.getResultList();		
		
		return objAO;
	}
	
}
