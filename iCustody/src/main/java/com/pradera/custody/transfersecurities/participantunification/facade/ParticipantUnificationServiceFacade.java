package com.pradera.custody.transfersecurities.participantunification.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.services.remote.reports.ReportGenerationService;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.accounts.to.PuHolderAccountBalanceTO;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.ownershipexchange.service.ChangeOwnershipServiceBean;
import com.pradera.custody.reversals.facade.RequestReversalsServiceFacade;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.tranfersecurities.to.CustodyOperationTO;
import com.pradera.custody.transfersecurities.participantunification.service.ParticipantUnificationServiceBean;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionBalanceTO;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionOperationTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.participantunion.BlockOperationUnion;
import com.pradera.model.custody.participantunion.ParticipantUnionBalance;
import com.pradera.model.custody.participantunion.ParticipantUnionFile;
import com.pradera.model.custody.participantunion.ParticipantUnionOperation;
import com.pradera.model.custody.reversals.type.MotiveReverlsasType;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.ParticipantUnionOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.operations.RequestCorrelativeType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnificationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class) 
public class ParticipantUnificationServiceFacade {

	/** The affectations service bean. */
	@EJB
	AffectationsServiceBean affectationsServiceBean;
	/** The param service bean. */
	@EJB
	private ParameterServiceBean paramServiceBean;
	
	/** The participant unification service bean. */
	@EJB
	private ParticipantUnificationServiceBean participantUnificationServiceBean;
	
	/** The accounts facade. */
	@EJB
	private HolderAccountComponentServiceBean accountServiceBean;
	
	/** The executor component service bean. */
    @Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The request affectation service bean. */
	@EJB
	RequestAffectationServiceBean requestAffectationServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The change ownership service bean. */
	@EJB
	ChangeOwnershipServiceBean changeOwnershipServiceBean;

	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The report service. */
	@Inject
	Instance<ReportGenerationService> reportService;
	
	/** The request reversals service bean. */
	@EJB
	private RequestReversalsServiceFacade requestReversalsServiceFacade;
	
	/** The id report unification. */
	private final Long ID_REPORT_UNIFICATION = new Long(5);
	
	/** The limit. */
	private final int limit = 400;
	
	/** The em. */
	@Inject @DepositaryDataBase
	private EntityManager 	em;
	
	/**
	 * Find all participant.
	 *
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> findAllParticipant() throws ServiceException{
		
		return participantUnificationServiceBean.findAllParticipants();
	}
	
	/**
	 * Gets the parameter list.
	 *
	 * @param type the type
	 * @return the parameter list
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getParameterList(MasterTableType type) throws ServiceException {

		ParameterTableTO parameterFilter = new ParameterTableTO();

		parameterFilter.setMasterTableFk(type.getCode());
		parameterFilter.setState(1);

		List<ParameterTable> parameterList = new ArrayList<ParameterTable>();
	
		parameterList = paramServiceBean.getListParameterTableServiceBean(parameterFilter);	
		return parameterList;

	}
	
	/**
	 * Gets the participant union operations registered.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param indSourceTarget the ind source target
	 * @return the participant union operations registered
	 * @throws ServiceException the service exception
	 */
	public boolean validateExistsParticipantUnifications(Long idParticipantPk,Long indSourceTarget) throws ServiceException{
		return this.participantUnificationServiceBean.validateExistsParticipantUnifications(idParticipantPk, indSourceTarget);
	}
	
	/**
	 * Gets the participant union operations.
	 *
	 * @param unionOperationTO the union operation to
	 * @param mapParameters the map parameters
	 * @param isParticipant the is participant
	 * @return the participant union operations
	 * @throws ServiceException the service exception
	 */	
	public List<ParticipantUnionOperation> getParticipantUnionOperations(ParticipantUnionOperationTO unionOperationTO, HashMap<Integer, ParameterTable> mapParameters, boolean isParticipant) throws ServiceException{		
		return this.participantUnificationServiceBean.getParticipantUnionOperations(unionOperationTO,mapParameters,isParticipant);
	}
	
	/**
	 * Reg hold account req component service bean.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest regHoldAccountReqComponentServiceBean(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        return this.participantUnificationServiceBean.regHoldAccountReqComponentServiceBean(holderAccountRequestHy);
	}
	/**
	 * Gets the participant union files.
	 *
	 * @param participantUnionOperationPk the participant union operation pk
	 * @return the participant union files
	 * @throws Exception the exception
	 */
	public List<ParticipantUnionFile> getParticipantUnionFiles(Long participantUnionOperationPk) throws Exception{
		return this.participantUnificationServiceBean.getParticipantUnionFiles(participantUnionOperationPk);
	}
	
	/**
	 * Update participant union operation.
	 *
	 * @param idParticipantOperationPk the id participant operation pk
	 * @throws ServiceException the service exception
	 */
	
	public void registryBatchDefinitive(Long idParticipantOperationPk) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode());

		Map<String, Object> param = new HashMap<String, Object>();
	    param.put("idParticipantUnion",idParticipantOperationPk);				    		    
	    param.put("userNameLogged", loggerUser.getUserName());

	    this.batchProcessServiceFacade.registerBatch(loggerUser.getUserName(), businessProcess, param);
	}
	
	/**
	 * Update participant union operation.
	 *
	 * @param participantUnionOperationTO the participant union operation to
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.PARTICIPANT_UNIFICATION_CANCEL)
	public void updateParticipantUnionOperation(ParticipantUnionOperationTO participantUnionOperationTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        
		if(participantUnionOperationTO.getState().equals(ParticipantUnionOperationStateType.ANULLED.getCode())){
			
			//PARTICIPANTE ORIGEN COMO REGISTRADO
			Participant participant = participantUnionOperationTO.getSourceParticipant();
			participant.setState(ParticipantStateType.REGISTERED.getCode());
			participant.setRegistryDate(CommonsUtilities.currentDate());
			participantUnificationServiceBean.updateParticipant(participant);
			
			//PARTICIPANTE DESTINO COMO REGISTRADO
			Participant participant2 = participantUnionOperationTO.getTargetParticipant();
			participant2.setState(ParticipantStateType.REGISTERED.getCode());
			participant2.setRegistryDate(CommonsUtilities.currentDate());
			participantUnificationServiceBean.updateParticipant(participant2);
			
			//ACTUALIZO LA OPERACION DE CUSTODIA
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setIdCustodyOperationPk(participantUnionOperationTO.getIdPartUnionOperationPk());
			custodyOperation.setRejectMotive(participantUnionOperationTO.getRejectMotive());
			if(Validations.validateIsNotNullAndNotEmpty(participantUnionOperationTO.getRejectMotiveOther())){
				custodyOperation.setRejectMotiveOther(participantUnionOperationTO.getRejectMotiveOther());
			}
			this.participantUnificationServiceBean.updateCustodyOperation(custodyOperation);
			participantUnionOperationTO.setState(ParticipantUnionOperationStateType.ANULLED.getCode());
			
		}else if(participantUnionOperationTO.getState().equals(ParticipantUnionOperationStateType.PRELIMINARY.getCode())){
			
			participantUnionOperationTO.setState(ParticipantUnionOperationStateType.IN_PROCESS.getCode());
			
		}else if(participantUnionOperationTO.getState().equals(ParticipantUnionOperationStateType.DEFINITIVE.getCode())){

			participantUnionOperationTO.setState(ParticipantUnionOperationStateType.IN_PROCESS.getCode());
			
		}		
		
		//ACTUALIZO EL ESTADO DE MI SOLICITUD
		this.participantUnificationServiceBean.updateParticipantUnionOperation(participantUnionOperationTO);
	}
	
	/**
	 * Update state only participant union.
	 * using in preliminar and definitive
	 * @param participantUnionOperationTO the participant union operation to
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateStateOnlyParticipantUnion(ParticipantUnionOperationTO participantUnionOperationTO) throws ServiceException{
		participantUnificationServiceBean.updateParticipantUnionOperation(participantUnionOperationTO);
	}
	

	/**
	 * Gets the holder account with balance list component service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account with balance list component service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getHolderAccountWithBalanceList(HolderAccountTO holderAccountTO) throws ServiceException{
		return participantUnificationServiceBean.getHolderAccountBalanceLst(holderAccountTO);
	}
	
	/**
	 * Register participant union.
	 *
	 * @param participantUnionOperation the participant union operation
	 * @return the participant union operation
	 * @throws ServiceException the service exception
	 */
	//@ProcessAuditLogger(process=BusinessProcessType.PARTICIPANT_UNIFICATION_REGISTER)
	public ParticipantUnionOperation registerParticipantUnion(ParticipantUnionOperation participantUnionOperation) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		
        //VALIDAMOS LAS CUENTAS MATRICES DEL PARTICIPANTE ORIGEN
        if(validateHolderAccountBalance(participantUnionOperation.getSourceParticipant().getIdParticipantPk())){
        	throw new ServiceException(ErrorServiceType.PARTICIPANT_BALANCE_NOT_VALID);
        }
        
        //DATOS DE CUSTODIA
		CustodyOperation custodyOperation = new CustodyOperation();
		custodyOperation.setOperationType(ParameterOperationType.PARTICIPANT_UNION.getCode());		
		custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());		
		custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		custodyOperation.setRegistryUser(loggerUser.getUserName());
		custodyOperation.setState(ParticipantUnionOperationStateType.REGISTERED.getCode());
		participantUnionOperation.setCustodyOperation(custodyOperation);	
		 //Calling operation Number
		
		//PASO A REGISTRAR
		participantUnionOperation.setState(ParticipantUnionOperationStateType.REGISTERED.getCode());
		participantUnificationServiceBean.registerParticipantUnion(participantUnionOperation);
		
		return participantUnionOperation;
	}
	
	/**
	 * Validate Holder Account Balance.
	 *
	 * @param idParticipant Id Participant
	 * @return Validate Holder Account Balance
	 * @throws ServiceException the service exception
	 */
	public boolean validateHolderAccountBalance(Long idParticipant)throws ServiceException{
		if(participantUnificationServiceBean.validateBalanceForParticipant(idParticipant)){
			return true;
		}
		return false;
	}
	/**
	 * Gets the pending sell balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the pending sell balance
	 */
	public BigDecimal getPendingSellBalance(HolderAccountBalance holderAccountBalance) {
		return this.changeOwnershipServiceBean.getPendingSellBalance(holderAccountBalance);
	}
	
	/**
	 * Sets the audit.
	 *
	 * @param participantUnionOperationTO the participant union operation to
	 * @param loggerUser the logger user
	 * @return the participant union operation to
	 */
	public ParticipantUnionOperationTO setAudit(ParticipantUnionOperationTO participantUnionOperationTO, LoggerUser loggerUser){
		/*** SETTING AUDIT ***/
		participantUnionOperationTO.setLastModifyApp(loggerUser.getIdPrivilegeOfSystem());
		participantUnionOperationTO.setLastModifyDate(loggerUser.getAuditTime());
		participantUnionOperationTO.setLastModifyIp(loggerUser.getIpAddress()); 
		participantUnionOperationTO.setLastModifyUser(loggerUser.getUserName());
        return participantUnionOperationTO;
	}
	
	/**
	 * Gets the reject motive other custody operation.
	 *
	 * @param idCustodyOperationPk the id custody operation pk
	 * @return the reject motive other custody operation
	 * @throws ServiceException the service exception
	 */
	public CustodyOperationTO getRejectMotiveOtherCustodyOperation(Long idCustodyOperationPk) throws ServiceException{
		return participantUnificationServiceBean.getRejectMotiveOtherCustodyOperation(idCustodyOperationPk);
	}
	
	/**
	 * Gets the next correlative operations.
	 *
	 * @param requestOperation the request operation
	 * @return the next correlative operations
	 */
	public Long getNextCorrelativeOperations(RequestCorrelativeType requestOperation){
		return this.participantUnificationServiceBean.getNextCorrelativeOperations(requestOperation);
	}
	
	/**
	 * Gets the last holder account request.
	 *
	 * @param requestHy the request hy
	 * @return the last holder account request
	 * @throws ServiceException the service exception
	 */
	public Long getLastHolderAccountRequest(HolderAccountRequest requestHy) throws ServiceException{
		return this.participantUnificationServiceBean.getLastHolderAccountRequest(requestHy);
	}
	
	/**
	 * Send report.
	 *
	 * @param participantUnification the participant unification
	 * @throws ServiceException the service exception
	 */
	public void sendReport(ParticipantUnionOperation participantUnification) throws ServiceException {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());

		Long sourceParticipantPk = participantUnification.getSourceParticipant().getIdParticipantPk();	
		
		this.reportService.get().sendReportParticipantUnification(ID_REPORT_UNIFICATION, sourceParticipantPk, loggerUser);
		
	}
	
	/**
	 * Execute definitive.
	 *
	 * @param idParticipantUnionOperation the id participant union operation
	 * @param userNameLogged the user name logged
	 * @throws ServiceException the service exception
	 * @throws Exception the exception
	 */
	@TransactionTimeout(unit=TimeUnit.MINUTES,value=60)
	@Performance
	public void executeDefinitive(Long idParticipantUnionOperation, String userNameLogged)throws ServiceException, Exception{
		//privileges in services
		LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
		if(loggerUser!= null){
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDefinitive());
			loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
		}
		if(Validations.validateIsNotNull(idParticipantUnionOperation)){
			List<BlockOperationUnion> blockOperationUnions = this.participantUnificationServiceBean.searchBlockOperationUnion(idParticipantUnionOperation);		
			Long idParticipantPk = null;
			Long idParticipantTargetPk = null;

			//lista de afectaciones por cada documento de bloqueo
			Map<Long, List<AffectationTransferTO>> mapAffectaciones = new HashMap<Long, List<AffectationTransferTO>>();
			List<AffectationTransferTO> lstComponentAffectation = null;
			AffectationTransferTO affectationTransferTO = null;

		
		if(blockOperationUnions !=null && blockOperationUnions.size()>0){
			//GETTING BLOCK OPERATIONS
			for(BlockOperationUnion blockOperationUnion : blockOperationUnions){
				lstComponentAffectation = new ArrayList<AffectationTransferTO>();
				
				BlockOperation tmpBlockOperation = blockOperationUnion.getBlockOperation();
				BigDecimal totalUnblock = new BigDecimal(0);
				ParameterTable motive = new ParameterTable();
		        motive.setParameterTablePk(MotiveReverlsasType.EJECUTION.getCode());
		        
		        BlockRequest blockRequest = new BlockRequest();
				blockRequest = getBlockRequest(tmpBlockOperation.getBlockRequest());
		        
				RegisterReversalTO regReversalTO = new RegisterReversalTO();
		    	regReversalTO.setRegistryUser(userNameLogged); 
		    	regReversalTO.setDocumentNumber(blockRequest.getBlockNumber());
		    	regReversalTO.setParamTableMotiveUnblock(motive);
		    	regReversalTO.setIdState(RequestReversalsStateType.APPROVED.getCode());
		    	regReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
		    	
		    	
				//hechos del mercado por operacion de bloqueo
	    		List<BlockMarketFactOperation> lstBlockMarketFactOperation = lstBlockMarketFactOperation(null, tmpBlockOperation.getIdBlockOperationPk());
	    		//componente de reversion
	    		List<ReversalsMarkFactTO> reversalsMarkFactList = new ArrayList<ReversalsMarkFactTO>();
				//componente de afectacion
		    	ArrayList<MarketFactTO> lstMarketFact = new ArrayList<MarketFactTO>();
				
		    	
	    		//recorro los hechos del mercado por 
	    		for(BlockMarketFactOperation obj: lstBlockMarketFactOperation){
	    			
	    			//DESAFECTACION
	    			ReversalsMarkFactTO reversalsMarkFact = new ReversalsMarkFactTO();	
	    			reversalsMarkFact.setBlockAmount(obj.getActualBlockBalance());
	    			reversalsMarkFact.setMarketDate(obj.getMarketDate());
	    			reversalsMarkFact.setMarketPrice(obj.getMarketPrice());
	    			reversalsMarkFact.setMarketRate(obj.getMarketRate());
	    			reversalsMarkFactList.add(reversalsMarkFact);
	    			
	    			//AFECTACION
					MarketFactTO objMarketFactAff = new MarketFactTO();
					objMarketFactAff.setMarketDate(obj.getMarketDate());
					objMarketFactAff.setMarketPrice(obj.getMarketPrice());
					objMarketFactAff.setMarketRate(obj.getMarketRate());
					objMarketFactAff.setQuantity(obj.getActualBlockBalance());
					lstMarketFact.add(objMarketFactAff);
					
	    		}
	    		
	    		//************ DATOS PARA LA REVERSION ***********
	    		totalUnblock = totalUnblock.add(tmpBlockOperation.getActualBlockBalance());
	    		
	    		BlockOpeForReversalsTO blockOpeForReversal = new BlockOpeForReversalsTO();
	    		blockOpeForReversal.setIdBlockOperationPk(tmpBlockOperation.getIdBlockOperationPk());
	    		blockOpeForReversal.setAmountUnblock(tmpBlockOperation.getActualBlockBalance());
	    		blockOpeForReversal.setReversalsMarkFactList(reversalsMarkFactList);
	    		
	    		regReversalTO.setIdHolderPk(tmpBlockOperation.getBlockRequest().getHolder().getIdHolderPk());
	    		regReversalTO.getLstBlockOpeForReversals().add(blockOpeForReversal);

	    		//listado para mi afectacion obtengo el participante y cuenta destino
	    		affectationTransferTO = getAffectationTransferTO(tmpBlockOperation, tmpBlockOperation.getHolderAccount().getIdHolderAccountPk(), idParticipantUnionOperation);
	    		affectationTransferTO.setIdHolderPk(tmpBlockOperation.getBlockRequest().getHolder().getIdHolderPk());
	    		affectationTransferTO.setIdSecurityCodePk(tmpBlockOperation.getSecurities().getIdSecurityCodePk());
	    		affectationTransferTO.setQuantity(totalUnblock);
	    		affectationTransferTO.setIdBlockOperationPk(tmpBlockOperation.getIdBlockOperationPk());
	    		affectationTransferTO.setMarketFactAffectations(lstMarketFact);
	    			
		    	/*** EJECUTO LAS DESAFECTACIONES ****/
		    	
		    	regReversalTO.setTotalUnblock(totalUnblock);
		    	regReversalTO.setConfirmationUser(userNameLogged);
		        Long unblockRequestPk = requestReversalsServiceFacade.saveReversalRequest(regReversalTO);
		    	
		        RequestReversalsTO requestReversalsTO = new RequestReversalsTO();
		        requestReversalsTO.setIdUnblockRequestPk(unblockRequestPk);
		        requestReversalsTO.setBusinessProcessTypeId(BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode());
		        
		        /********EJECUTO LA CONFIRMACION PARA LA DESAFECTACION****************/
		        /***********************************************************************/
		        requestReversalsServiceFacade.confirmReversalRequest(requestReversalsTO,loggerUser);

		        //agrego a mi lista para afectarlo
				lstComponentAffectation.add(affectationTransferTO);
				mapAffectaciones.put(blockOperationUnion.getIdBlockOperationUnionPk(), lstComponentAffectation);
			}
			
		}
			//OBTENGO MIS PARTICIPANT_UNION_BALANCE
			List<ParticipantUnionBalance> participantUnionBalances = this.participantUnificationServiceBean.getParticipantUnionBalances(idParticipantUnionOperation);
			
			List<HolderAccountBalanceTO> lstOriginHolderAccount = new ArrayList<HolderAccountBalanceTO>();
			List<HolderAccountBalanceTO> lstTargetHolderAccount = new ArrayList<HolderAccountBalanceTO>();
			//Getting Accounts origin and target
			for(ParticipantUnionBalance participantUnionBalance : participantUnionBalances){
				//LISTAS ORIGEN Y DESTINO DE LOS balancesTo
				HolderAccountBalanceTO originHolderAccountBalanceTO = new HolderAccountBalanceTO();
				HolderAccountBalanceTO targetHolderAccountBalanceTO = new HolderAccountBalanceTO();
				

				//SETEO EL PARTICIPANTE ORIGEN
				Participant originParticipant = new Participant();
				originParticipant.setIdParticipantPk(participantUnionBalance.getOriginParticipant().getIdParticipantPk());
				
				//Creating origin holder account
				HolderAccount originHolderAccount = new HolderAccount();
				originHolderAccount.setIdHolderAccountPk(participantUnionBalance.getOriginHolderAccount().getIdHolderAccountPk());	
				originHolderAccount.setAccountNumber(participantUnionBalance.getOriginHolderAccount().getAccountNumber());
				originHolderAccount.setAccountGroup(participantUnionBalance.getTargetHolderAccount().getAccountGroup());
				originHolderAccount.setAccountType(participantUnionBalance.getOriginHolderAccount().getAccountType());
				originHolderAccount.setAlternateCode(participantUnionBalance.getOriginHolderAccount().getAlternateCode());
				originHolderAccount.setClosingDate(participantUnionBalance.getOriginHolderAccount().getClosingDate());
				originHolderAccount.setOpeningDate(participantUnionBalance.getOriginHolderAccount().getOpeningDate());
				originHolderAccount.setStateAccount(participantUnionBalance.getOriginHolderAccount().getStateAccount());
				originHolderAccount.setParticipant(participantUnionBalance.getOriginParticipant());
				originHolderAccount.setRegistryDate(CommonsUtilities.currentDate());
				originHolderAccount.setRegistryUser(userNameLogged);
				originHolderAccount.setHolderAccountDetails(new ArrayList<HolderAccountDetail>());
				originHolderAccount.setHolderAccountFiles(new ArrayList<HolderAccountFile>());
				originHolderAccount.setHolderAccountBanks(new ArrayList<HolderAccountBank>());
				originHolderAccount.setHolderAccountAdjuments(new ArrayList<HolderAccountAdjusment>());
				originHolderAccount.setHolderAccountOperations(new ArrayList<HolderAccountOperation>());
				originHolderAccount.setHolderAccountBalances(new ArrayList<HolderAccountBalance>());
				originHolderAccount.setHolderAccountRequest(new ArrayList<HolderAccountRequest>());
				//GUARDO EL PK DEL PARTICPIANTE ORIGEN PARA LUEGO CAMBIARLE EL ESTADO
				idParticipantPk = participantUnionBalance.getOriginParticipant().getIdParticipantPk();

				//SETEO EL PARTICIPANTE DESTINO
				Participant targetParticipant = new Participant();
				targetParticipant.setIdParticipantPk(participantUnionBalance.getTargetParticipant().getIdParticipantPk());
				idParticipantTargetPk = targetParticipant.getIdParticipantPk();
				
				//ASIGNO DATOS DE LA CUENTA DESTINO
				HolderAccount targetHolderAccount = new HolderAccount();
				targetHolderAccount.setIdHolderAccountPk(participantUnionBalance.getTargetHolderAccount().getIdHolderAccountPk());								
				targetHolderAccount.setAccountGroup(participantUnionBalance.getTargetHolderAccount().getAccountGroup());
				targetHolderAccount.setAccountNumber(participantUnionBalance.getTargetHolderAccount().getAccountNumber());
				targetHolderAccount.setAccountType(participantUnionBalance.getTargetHolderAccount().getAccountType());
				targetHolderAccount.setAlternateCode(participantUnionBalance.getTargetHolderAccount().getAlternateCode());
				targetHolderAccount.setClosingDate(participantUnionBalance.getTargetHolderAccount().getClosingDate());
				targetHolderAccount.setOpeningDate(participantUnionBalance.getTargetHolderAccount().getOpeningDate());
				targetHolderAccount.setStateAccount(participantUnionBalance.getTargetHolderAccount().getStateAccount());
				targetHolderAccount.setParticipant(participantUnionBalance.getTargetParticipant());
				targetHolderAccount.setRegistryDate(CommonsUtilities.currentDate());
				targetHolderAccount.setRegistryUser(userNameLogged);

				//ASIGNO DATOS DEL VALOR
				Security security = new Security();
				security.setIdSecurityCodePk(participantUnionBalance.getSecurities().getIdSecurityCodePk());
				
				
				//SETEO DATOS a mis accountBalancesTo's
				originHolderAccountBalanceTO.setIdHolderAccount(originHolderAccount.getIdHolderAccountPk());
				originHolderAccountBalanceTO.setIdParticipant(originParticipant.getIdParticipantPk());
				originHolderAccountBalanceTO.setIdSecurityCode(security.getIdSecurityCodePk());
				originHolderAccountBalanceTO.setStockQuantity(participantUnionBalance.getTotalBalance());
				
				targetHolderAccountBalanceTO.setIdHolderAccount(targetHolderAccount.getIdHolderAccountPk());
				targetHolderAccountBalanceTO.setIdParticipant(targetParticipant.getIdParticipantPk());
				targetHolderAccountBalanceTO.setIdSecurityCode(security.getIdSecurityCodePk());
				targetHolderAccountBalanceTO.setStockQuantity(participantUnionBalance.getTotalBalance());
				
				
//				/** VALIDO QUE LAS CUENTAS ORIGEN ESTEN BLOQUEADAS **/
				//TODO in preliminary not change state account origin
//				if(originHolderAccount.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode())){
					
					/** SI LA CUENTA ORIGEN ESTA BLOQUEADA, 
					 * ACTUALIZO LOS ESTADOS DE LOS HOLDER_ACCOUNT Y ASIGNARLE UN NUMERO DE CUENTA **/
					activeHolderAccountServiceBean(targetHolderAccount);
//				}
				
				/*** CIERRO LAS CUENTAS DEL ORIGEN ****/
				this.closeHolderAccountServiceBean(originHolderAccount);
				
				/**LISTADO DE HECHOS DEL MERCADO**/
				List<MarketFactAccountTO> lstMarketFactAccountTO = affectationsServiceBean.lstHolderMarketFactBalance(originHolderAccountBalanceTO.getIdHolderAccount(), 
																   originHolderAccountBalanceTO.getIdParticipant(), 
																   originHolderAccountBalanceTO.getIdSecurityCode());
				
				originHolderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);
				
				targetHolderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				for (MarketFactAccountTO marketFactAccountTO: lstMarketFactAccountTO) {
					MarketFactAccountTO targetMarketFactAccountTO= null;
					try {
						targetMarketFactAccountTO= marketFactAccountTO.clone();
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
					targetHolderAccountBalanceTO.getLstMarketFactAccounts().add(targetMarketFactAccountTO);
				}
//				targetHolderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);
				//Adding origins accounts
				lstOriginHolderAccount.add(originHolderAccountBalanceTO);
				//Adding target accounts
				lstTargetHolderAccount.add(targetHolderAccountBalanceTO);
			}			
		
			if(lstOriginHolderAccount!=null && lstOriginHolderAccount.size() <= limit){
				//SETEO EL OBJETO QUE NECESITA EL COMPONENTE
				AccountsComponentTO objAccountComponent = this.getAccountsComponentTO(lstOriginHolderAccount, lstTargetHolderAccount, 
						   idParticipantUnionOperation, BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode(),
						   ParameterOperationType.PARTICIPANT_UNION.getCode());
				
				/***********EJECUTO LA LLAMADAS PARA LA UNIFICACION*********************/
		        /***********************************************************************/
				accountsComponentService.get().executeAccountsComponent(objAccountComponent);	
			}else{
				List<HolderAccountBalanceTO> lstOriginHolderAccountAux = new ArrayList<HolderAccountBalanceTO>();
				List<HolderAccountBalanceTO> lstTargetHolderAccountAux = new ArrayList<HolderAccountBalanceTO>();
				int endSubList = 0;
				while(endSubList < lstOriginHolderAccount.size()){
					if((endSubList + limit)>lstOriginHolderAccount.size()){
						lstOriginHolderAccountAux = lstOriginHolderAccount.subList(endSubList, lstOriginHolderAccount.size());
						lstTargetHolderAccountAux = lstTargetHolderAccount.subList(endSubList, lstTargetHolderAccount.size());
					}else{
						lstOriginHolderAccountAux = lstOriginHolderAccount.subList(endSubList, (endSubList + limit));
						lstTargetHolderAccountAux = lstTargetHolderAccount.subList(endSubList, (endSubList + limit));
					}
					
					//SETEO EL OBJETO QUE NECESITA EL COMPONENTE
					AccountsComponentTO objAccountComponent = this.getAccountsComponentTO(lstOriginHolderAccountAux, lstTargetHolderAccountAux, 
							   idParticipantUnionOperation, BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode(),
							   ParameterOperationType.PARTICIPANT_UNION.getCode());
					
					/***********EJECUTO LA LLAMADAS PARA LA UNIFICACION*********************/
			        /***********************************************************************/
					accountsComponentService.get().executeAccountsComponent(objAccountComponent);
					em.flush();
					em.clear();

					endSubList = endSubList + limit;
				}
			}
			
			
			//GENERO NUEVAMENTE LAS AFECTACIONES
			if(blockOperationUnions !=null && blockOperationUnions.size()>0){ 

				for(BlockOperationUnion blockOperationUnion : blockOperationUnions){
					lstComponentAffectation = mapAffectaciones.get(blockOperationUnion.getIdBlockOperationUnionPk());
					
					if(lstComponentAffectation!= null && lstComponentAffectation.size()>0){
						
						/***********EJECUTO LA AFECTACION EN LA CONTRAPARTE *********************/
				        /***********************************************************************/
						affectationsServiceBean.transferBlockOperations(lstComponentAffectation, 
								BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode(), userNameLogged,ComponentConstant.ZERO);
						
					}
					
				}
			}

			/** CAMBIO EL ESTADO A UNIFICADO AL PARTICIPANTE ORIGEN **/
			updateParticipantUnified(idParticipantPk);
			/** SETEAR EL PARTICIPANTE ORIGEN AL PARTICIPANTE DESTINO **/
			updateParticipantTarget(idParticipantPk,idParticipantTargetPk);
			//TODO improvement code
			//change state
			ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();		
			partUnionOpeTO.setIdPartUnionOperationPk(idParticipantUnionOperation);
			partUnionOpeTO.setState(ParticipantUnionOperationStateType.DEFINITIVE.getCode());	
			participantUnificationServiceBean.updateParticipantUnionOperation(partUnionOpeTO);
			CustodyOperation custody = participantUnificationServiceBean.find(CustodyOperation.class, idParticipantUnionOperation);
			custody.setState(ParticipantUnionOperationStateType.DEFINITIVE.getCode());
		}
	}
	
	/**
	 * Gets the block request.
	 *
	 * @param filters the filters
	 * @return the block request
	 * @throws ServiceException the service exception
	 */
	public BlockRequest getBlockRequest(BlockRequest filters) throws ServiceException{
		return affectationsServiceBean.getBlockRequest(filters);
	}
	
	/**
	 * Lst block market fact operation.
	 *
	 * @param idBlockMarketfactPk the id block marketfact pk
	 * @param idBlockOperationPk the id block operation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 * @throws Exception the exception
	 */
	//PARA OBTENER LOS HECHOS DEL MERCADO APARTIR DEL PK DE LA OPERACION DE BLOQUEO
		public List<BlockMarketFactOperation> lstBlockMarketFactOperation(Long idBlockMarketfactPk, Long idBlockOperationPk) throws ServiceException, Exception{
			List<BlockMarketFactOperation> lstBlockMarketFactOperation = affectationsServiceBean.lstBlockMarketFactOperation(idBlockMarketfactPk, idBlockOperationPk);
			return lstBlockMarketFactOperation;
		}
		
		/**
		 * Sets the holder account and participant target.
		 *
		 * @param tmpBlockOperation the tmp block operation
		 * @param idOriginHolderAccountPk the id origin holder account pk
		 * @param idParticipantUnionOperation the id participant union operation
		 * @return the block operation
		 * @throws ServiceException the service exception
		 */
		public AffectationTransferTO getAffectationTransferTO(BlockOperation tmpBlockOperation, Long idOriginHolderAccountPk, Long idParticipantUnionOperation) throws ServiceException{
			
			/** SEARCHING HOLDER ACCOUNT AND PARTICIPANT TARGET **/ 
			ParticipantUnionBalance participantUnionBalanceTmp = this.participantUnificationServiceBean.searchTargetHolderAccountAndParticipant(idOriginHolderAccountPk, idParticipantUnionOperation);
			
			AffectationTransferTO at = new AffectationTransferTO();
			at.setIdParticipantPk(participantUnionBalanceTmp.getTargetParticipant().getIdParticipantPk());
			at.setIdHolderAccountPk(participantUnionBalanceTmp.getTargetHolderAccount().getIdHolderAccountPk());
			return at;
		}	
		
		/**
		 * Active holder account service bean.
		 *
		 * @param holderAccountRequestHy the holder account request hy
		 * @return the holder account
		 * @throws ServiceException the service exception
		 */
		public HolderAccount activeHolderAccountServiceBean(HolderAccount holderAccountRequestHy) throws ServiceException{

			this.participantUnificationServiceBean.updateHolderAccountNumber(holderAccountRequestHy);	//CORRELATIVO
					
			holderAccountRequestHy.setStateAccount(HolderAccountStatusType.ACTIVE.getCode());			//ESTADO
			this.participantUnificationServiceBean.updateHolderAccountState(holderAccountRequestHy);
			
			return holderAccountRequestHy;
		}
		/**
		 * Close holder account service bean.
		 *
		 * @param holderAccountRequestHy the holder account request hy
		 * @return the holder account request
		 * @throws ServiceException the service exception
		 */
		public HolderAccount closeHolderAccountServiceBean(HolderAccount holderAccountRequestHy) throws ServiceException{

			//holderAccountRequestHy.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());
			//this.participantUnificationServiceBean.update(holderAccountRequestHy);
			//obtenemos la cuenta de la solicitud
			//HolderAccount holderAccount = holderAccountRequestHy.getHolderAccount();
			holderAccountRequestHy.setStateAccount(HolderAccountStatusType.CLOSED.getCode());//Bloqueamos la cuenta
			this.participantUnificationServiceBean.updateHolderAccountState(holderAccountRequestHy);
			
			return holderAccountRequestHy;
		}
		
		/**
		 * Gets the accounts component to.
		 *
		 * @param sourceHolderAccount the source holder account
		 * @param targetHolderAccount the target holder account
		 * @param idRefCustodyOperation the id ref custody operation
		 * @param businessProcessType the business process type
		 * @param operationType the operation type
		 * @return the accounts component to
		 */
		public AccountsComponentTO getAccountsComponentTO(List<HolderAccountBalanceTO> sourceHolderAccount,List<HolderAccountBalanceTO> targetHolderAccount,Long idRefCustodyOperation, Long businessProcessType, Long operationType){
			// ComponentTO Object
			AccountsComponentTO objAccountComponent = new AccountsComponentTO();
			
			objAccountComponent.setIdBusinessProcess(businessProcessType);
			objAccountComponent.setIdRefCustodyOperation(idRefCustodyOperation);
			objAccountComponent.setIdCustodyOperation(idRefCustodyOperation);
			objAccountComponent.setIndMarketFact(1);
			objAccountComponent.setLstSourceAccounts(sourceHolderAccount);
			objAccountComponent.setLstTargetAccounts(targetHolderAccount);
			objAccountComponent.setIdOperationType(operationType);
			return objAccountComponent;
		}
		
		/**
		 * Update participant unified.
		 *
		 * @param idParticipantPk the id participant pk
		 * @throws ServiceException the service exception
		 */
		public void updateParticipantUnified(Long idParticipantPk) throws ServiceException{
			Participant participant = new Participant();
			participant.setIdParticipantPk(idParticipantPk);
			participant.setState(ParticipantStateType.UNIFIED.getCode());
			participant.setRegistryDate(CommonsUtilities.currentDate());
			//CAMBIO EL ESTADO DEL PARTICIPANTE
			participantUnificationServiceBean.updateParticipant(participant);
		}
		
		/**
		 * Update participant target.
		 *
		 * @param idParticipantSource the id participant source
		 * @param idParticipantTarget the id participant target
		 * @throws ServiceException the service exception
		 */
		public void updateParticipantTarget(Long idParticipantSource,Long idParticipantTarget) throws ServiceException{
			Participant participant = new Participant();
			participant.setIdParticipantPk(idParticipantTarget);
			Participant auxParticipant = new Participant();
			auxParticipant.setIdParticipantPk(idParticipantSource);
			participant.setParticipantOrigin(auxParticipant);
			
			//SETEAR EL PARTICIPANTE ORIGEN AL PARTICIPANTE DESTINO
			participantUnificationServiceBean.updateParticipant(participant);
		}
		/**
		 * Block holder account service bean.
		 *
		 * @param holderAccountRequestHy the holder account request hy
		 * @return the holder account request
		 * @throws ServiceException the service exception
		 */
		public HolderAccountRequest blockHolderAccountServiceBean(HolderAccountRequest holderAccountRequestHy) throws ServiceException{

			holderAccountRequestHy.setRequestState(HolderAccountRequestHysStatusType.CONFIRM.getCode());
			this.participantUnificationServiceBean.update(holderAccountRequestHy);
			//obtenemos la cuenta de la solicitud
			HolderAccount holderAccount = holderAccountRequestHy.getHolderAccount();
			holderAccount.setStateAccount(HolderAccountStatusType.BLOCK.getCode());//Bloqueamos la cuenta
			this.participantUnificationServiceBean.updateHolderAccountState(holderAccount);
			
			return holderAccountRequestHy;
		}
		
		/**
		 * Execute preliminary.
		 *
		 * @param objPartUnion the obj part union
		 * @param usernameLogged the username logged
		 * @throws ServiceException the service exception
		 */
		public void executePreliminary(ParticipantUnionOperationTO objPartUnion, String usernameLogged) throws ServiceException{
				//privileges in services
				LoggerUser loggerUser = (LoggerUser)ThreadLocalContextHolder.get(RegistryContextHolderType.LOGGER_USER.name());
				if(loggerUser!= null){
					loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegePreliminary());
					loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
				}				
				
				HolderAccountTO preHolderAccountTO = new HolderAccountTO();	
				preHolderAccountTO.setParticipantTO(objPartUnion.getSourceParticipant().getIdParticipantPk());
				
				//OBTIENE UNA LISTA DE LOS REGISTROS YA IGRESADOS Y OBTENGO el pk del unionBalance, las cuenta y participantes, origen y destino
				List<ParticipantUnionBalanceTO> lstHolderExistInUnification = this.participantUnificationServiceBean.LstDataWithParticipanteUnionBalance(objPartUnion.getIdPartUnionOperationPk());

				//LISTA DE CUENTAS QUE YA EXISTEN
				List<Long> lstHolderAccountsPk = null;
				
				if(lstHolderExistInUnification != null && lstHolderExistInUnification.size()>0){
					// OBTENGO EL PK DE TODAS LAS CUENTAS A ORIGEN
					lstHolderAccountsPk = new ArrayList<Long>();
					for(ParticipantUnionBalanceTO hab: lstHolderExistInUnification){
						lstHolderAccountsPk.add(hab.getIdSourceHolderAccountPk());
					}
				}
				
				//editar query, hacer el left join, por ahora pasarle los registros que ya existen
				//BUSCO LOS REGISTROS QUE NO SE HALLAN REGISTRADO ANTES
				List<HolderAccount> registryHolderAccountList = getHolderAccountListWithoutAccounts(preHolderAccountTO,lstHolderAccountsPk);


				/** EJECUCION DE PROCESOS **/
				
				//SI YA HAY REGISTROS , LOS EDITO LOS DATOS DE SALDOS Y BLOQUEOS EN LA UNIFICACION
				if(Validations.validateListIsNotNullAndNotEmpty(lstHolderExistInUnification)){
					editExistData(lstHolderExistInUnification,objPartUnion,lstHolderAccountsPk);
				}
				
				//SI HAY NUEVAS CUENTAS, LAS REGISTRO
				if(Validations.validateListIsNotNullAndNotEmpty(registryHolderAccountList)){
					addNewData(registryHolderAccountList,objPartUnion,usernameLogged);
				}
				
				//change state
				ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();		
				partUnionOpeTO.setIdPartUnionOperationPk(objPartUnion.getIdPartUnionOperationPk());
				partUnionOpeTO.setState(ParticipantUnionOperationStateType.PRELIMINARY.getCode());	
				participantUnificationServiceBean.updateParticipantUnionOperation(partUnionOpeTO);
				CustodyOperation custody = participantUnificationServiceBean.find(CustodyOperation.class, objPartUnion.getIdPartUnionOperationPk());
				custody.setState(ParticipantUnionOperationStateType.PRELIMINARY.getCode());
				
				//LOS REGISTROS QUE YA NO ESTAN, BORRARLOS
				/**
				 	removeData(Listado);
				 */
				
		}
		
		/**
		 * Gets the holder account list without accounts.
		 *
		 * @param holderAccountTO the holder account to
		 * @param lstHoldersAccountsPk the lst holders accounts pk
		 * @return the holder account list without accounts
		 * @throws ServiceException the service exception
		 */
		public List<HolderAccount> getHolderAccountListWithoutAccounts(HolderAccountTO holderAccountTO, List<Long> lstHoldersAccountsPk) throws ServiceException {
			List<HolderAccount> holderAccountList = participantUnificationServiceBean.getHolderAccountListWithoutAccounts(holderAccountTO,lstHoldersAccountsPk);
			return holderAccountList;
		}
		
		/**
		 * Edits the exist data.
		 *
		 * @param lstHolderExistInUnification the lst holder exist in unification
		 * @param objPartUnion the obj part union
		 * @param lstHolderAccountsPk the lst holder accounts pk
		 * @throws ServiceException the service exception
		 */
		public void editExistData(List<ParticipantUnionBalanceTO> lstHolderExistInUnification, ParticipantUnionOperationTO objPartUnion,List<Long> lstHolderAccountsPk) throws ServiceException{
			
			ParticipantUnionOperation participantUnionOperation = new ParticipantUnionOperation();
			participantUnionOperation.setIdPartUnionOperationPk(objPartUnion.getIdPartUnionOperationPk());
			// ELIMINO LOS BLOCK_OPERATION_UNIONS EXISTENTES
			this.participantUnificationServiceBean.deleteBlockOperationUnion(objPartUnion.getIdPartUnionOperationPk());		
			
			
			// LISTA DE LOS SALDOS ACTUALES A PARTIR DE LAS CUENTAS EXISTENTES
			List<PuHolderAccountBalanceTO> lstPuHolderAccountBalanceTO = this.accountServiceBean.getPuHolderAccountBalanceTo(objPartUnion.getSourceParticipant().getIdParticipantPk(),null,lstHolderAccountsPk);
			
			// RECORRO CUENTAS QUE YA FUERON REGISTRADAS
			for(ParticipantUnionBalanceTO editAccounts: lstHolderExistInUnification){

				HolderAccount originHolderAccount = new HolderAccount();
				
				// RECORRO CUENTAS CON LOS SALDOS ACTUALES
				for(PuHolderAccountBalanceTO hab: lstPuHolderAccountBalanceTO){
					
					//solo compare cuenta y valor, xq se trajeron los datos del mismo participante 
					if(hab.getIdHolderAccountPk().equals(editAccounts.getIdSourceHolderAccountPk()) &&
					   hab.getIdSecurityCodePk().equals(editAccounts.getIdSecurityCodePk())
					 ){
				
						ParticipantUnionBalance pub = new ParticipantUnionBalance();
						originHolderAccount.setIdHolderAccountPk(editAccounts.getIdSourceHolderAccountPk());
						pub.setIdParticipantUnionBalance(editAccounts.getIdPartUnionBalancePk());
						pub.setTotalBalance(hab.getTotalBalance());
						pub.setAvailableBalance(hab.getAvailableBalance());
						pub.setBanBalance(hab.getBanBalance());
						pub.setPawnBalance(hab.getPawnBalance());
						pub.setOtherBlockBalance(hab.getOtherBlockBalance());
						pub.setReserveBalance(hab.getReserveBalance());
						pub.setOpositionBalance(hab.getOppositionBalance());
						pub.setParticipantUnionOperation(participantUnionOperation);
						
						//ACTUALIZO LOS SALDOS EN EL PARTICIPANT_UNION_BALANCE
						this.participantUnificationServiceBean.updateParticipantUnionBalance(pub);
						
						//VEO SI TENGO SALDOS BLOQUEADOS
						if((pub.getBanBalance().compareTo(BigDecimal.ZERO) > 0) ||
						   (pub.getPawnBalance().compareTo(BigDecimal.ZERO) > 0) ||
						   (pub.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0) ||
						   (pub.getReserveBalance().compareTo(BigDecimal.ZERO) > 0) ||
						   (pub.getOpositionBalance().compareTo(BigDecimal.ZERO) > 0)){
							
							//BUSCO LAS OPERACIONES DE BLOQUEO DE LA CUENTA ORIGEN
							List<BlockOperation> blockOperations = this.participantUnificationServiceBean.getBlockOperation(hab.getIdParticipantPk(),
																															editAccounts.getIdSourceHolderAccountPk(),
																															editAccounts.getIdSecurityCodePk());
							//OBTENGO LOS BLOQUEOS DE LA CUENTA ORIGEN Y CREO EL BLOCK_OPERATION_UNION
							for(BlockOperation blockOperation : blockOperations){
								BlockOperationUnion blockOperationUnion = new BlockOperationUnion();
								blockOperationUnion.setParticipantUnionOperation(participantUnionOperation);
								blockOperationUnion.setBlockOperation(blockOperation);
								blockOperationUnion.setOriginalBlockBalance(blockOperation.getOriginalBlockBalance());
								participantUnificationServiceBean.create(blockOperationUnion);
							}
						}
						
					}		
				}	
			}
				
		
		}
		
		/**
		 * Adds the new data.
		 *
		 * @param registryHolderAccountList the registry holder account list
		 * @param objPartUnion the obj part union
		 * @param usernameLogged the username logged
		 * @throws ServiceException the service exception
		 */
		public void addNewData(List<HolderAccount> registryHolderAccountList, ParticipantUnionOperationTO objPartUnion, String usernameLogged) throws ServiceException{
			
			for(HolderAccount holderAccount : registryHolderAccountList){
			//SOLO SE REGISTRAN LOS SALDOS DISTINTOS A REGISTRADO	
			if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.CLOSED.getCode())){
				Participant participant = new Participant();
				participant.setIdParticipantPk(objPartUnion.getTargetParticipant().getIdParticipantPk());					
				
				/************ DATOS PARA MI PARTICIPANT_UNION_OPERATION *********/
				ParticipantUnionOperation participantUnionOperation = new ParticipantUnionOperation();
				participantUnionOperation.setIdPartUnionOperationPk(objPartUnion.getIdPartUnionOperationPk());
			
				List<ParticipantUnionBalance> participantUnionBalanceList = new ArrayList<ParticipantUnionBalance>();
				if(Validations.validateListIsNullOrEmpty(holderAccount.getHolderAccountBalances())){
					
					//BLOQUEO LA CUENTA EN EL ORIGEN
					blockHolderAccountServiceBean(holderAccount);
					continue;
				}
				
				//RECORRO LOS SALDOS
				for(HolderAccountBalance holderAccountBalance: holderAccount.getHolderAccountBalances()){
						if(holderAccountBalance.getTotalBalance().compareTo(BigDecimal.ZERO)==1){
							ParticipantUnionBalance participantUnionBalance = new ParticipantUnionBalance();
							participantUnionBalance.setParticipantUnionOperation(participantUnionOperation);
							participantUnionBalance.setOriginParticipant(objPartUnion.getSourceParticipant());	//cuenta del origen, ya que son los saldos existentes
							participantUnionBalance.setOriginHolderAccount(holderAccountBalance.getHolderAccount());
							participantUnionBalance.setTargetParticipant(objPartUnion.getTargetParticipant());
							//* participantUnionBalance.setTargetHolderAccount() <-  la cuenta destino se va a crear
							
							//SALDOS
							participantUnionBalance.setTotalBalance(holderAccountBalance.getTotalBalance());
							participantUnionBalance.setAvailableBalance(holderAccountBalance.getAvailableBalance());
							participantUnionBalance.setPawnBalance(holderAccountBalance.getPawnBalance());
							participantUnionBalance.setBanBalance(holderAccountBalance.getBanBalance());
							participantUnionBalance.setOtherBlockBalance(holderAccountBalance.getOtherBlockBalance());
							participantUnionBalance.setReserveBalance(holderAccountBalance.getReserveBalance());
							participantUnionBalance.setOpositionBalance(holderAccountBalance.getOppositionBalance());
							
							participantUnionBalance.setSecurities(holderAccountBalance.getSecurity());
							participantUnionBalanceList.add(participantUnionBalance);	
							
							//COMPRUEBO SI TENGO ALGUN SALDO BLOQUEADO
							if((participantUnionBalance.getBanBalance().compareTo(BigDecimal.ZERO) > 0) ||
							   (participantUnionBalance.getPawnBalance().compareTo(BigDecimal.ZERO) > 0) ||
							   (participantUnionBalance.getOtherBlockBalance().compareTo(BigDecimal.ZERO) > 0) ||
							   (participantUnionBalance.getReserveBalance().compareTo(BigDecimal.ZERO) > 0) ||
							   (participantUnionBalance.getOpositionBalance().compareTo(BigDecimal.ZERO) > 0)){
								
									//OBTENGO LAS OPERACIONES DE BLOQUEO
									List<BlockOperation> blockOperations = this.participantUnificationServiceBean.getBlockOperation(objPartUnion.getSourceParticipant().getIdParticipantPk(),
																																	holderAccount.getIdHolderAccountPk(),
																																	holderAccountBalance.getSecurity().getIdSecurityCodePk());
									for(BlockOperation blockOperation : blockOperations){
										BlockOperationUnion blockOperationUnion = new BlockOperationUnion();
										blockOperationUnion.setParticipantUnionOperation(participantUnionOperation);
										blockOperationUnion.setBlockOperation(blockOperation);
										blockOperationUnion.setOriginalBlockBalance(blockOperation.getOriginalBlockBalance());
										participantUnificationServiceBean.create(blockOperationUnion);
									}
								}
						}								
					}
					
					//CREO LA CUENTA COMO CERRADA Y CON NRO DE CUENTA 0, NO SE TOMA EN CUENTA LAS SOLICITUDES
					//TODO improvement code
					//create account number equal to negative holder account pk origin becasue is not allow register 0 duplicates in account number
					HolderAccount targetHolderAccount = this.accountServiceBean.regHoldAccountComponentReqServiceBean(holderAccount,objPartUnion.getTargetParticipant());
					
					//CREO LOS PARTICIPANT_UNION_BALANCE APARTIR DE LOS BLOQUEOS ENCONTRADOS
					for(ParticipantUnionBalance participantUnionBalance: participantUnionBalanceList){
						participantUnionBalance.setTargetHolderAccount(targetHolderAccount);
						participantUnionBalance.setIndProcessed(BooleanType.YES.getCode());
						participantUnificationServiceBean.create(participantUnionBalance);
					}					
					
				}
			}
		
		}
			
		/**
		 * Block holder account service bean.
		 *
		 * @param objHolderAccount the obj holder account
		 * @return the holder account
		 * @throws ServiceException the service exception
		 */
		public HolderAccount blockHolderAccountServiceBean(HolderAccount objHolderAccount) throws ServiceException{
			
			objHolderAccount.setStateAccount(HolderAccountStatusType.BLOCK.getCode());
			this.participantUnificationServiceBean.updateHolderAccountState(objHolderAccount);
			return objHolderAccount;
		}
		
		/**
		 * Delete participant union balance.
		 *
		 * @param idParticipantUnion the id participant union
		 * @throws ServiceException the service exception
		 */
		public void deleteParticipantUnionBalance(Long idParticipantUnion) throws ServiceException{
			participantUnificationServiceBean.deleteParticipantUnionBalance(idParticipantUnion);
		}
			
				
}
