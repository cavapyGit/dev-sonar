package com.pradera.custody.transfersecurities.participantunification.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.threadlocal.ThreadLocalContextHolder;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.reversals.facade.RequestReversalsServiceFacade;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.transfersecurities.participantunification.facade.ParticipantUnificationServiceFacade;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.HolderAccountFile;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountRequestHysStatusType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.corporatives.HolderAccountAdjusment;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.participantunion.BlockOperationUnion;
import com.pradera.model.custody.participantunion.ParticipantUnionBalance;
import com.pradera.model.custody.reversals.type.MotiveReverlsasType;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.ParticipantUnionOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ParticipantUnionOperationDefinitiveBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@BatchProcess(name="ParticipantUnionOperationDefinitiveBatch")
public class ParticipantUnionOperationDefinitiveBatch implements JobExecution{
	
	/** The participant unification service facade. */
	@EJB
	private ParticipantUnificationServiceFacade participantUnificationServiceFacade;
	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The sucess. */
	private boolean sucess=false;
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {	
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Long idParticipantUnionOperationPk = null;
		String userNameLogged = "";
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			if(processLoggerDetail.getParameterName().equals("idParticipantUnion")){
				idParticipantUnionOperationPk = Long.valueOf((processLoggerDetail.getParameterValue()));
			}else if(processLoggerDetail.getParameterName().equals("userNameLogged")){
				userNameLogged = processLoggerDetail.getParameterValue();
			}
		}
		
		try{
			
			if(Validations.validateIsNotNull(idParticipantUnionOperationPk) && Validations.validateIsNotNull(userNameLogged)){
				participantUnificationServiceFacade.executeDefinitive(idParticipantUnionOperationPk, userNameLogged);
				sucess=true;
			}
			
		}catch (ServiceException e) {
			e.printStackTrace();
			log.info(e.getMessage());
			
			catchActionBatch(idParticipantUnionOperationPk);

		}catch(Exception ex){
			ex.printStackTrace();
			log.info(ex.getMessage());
			
			catchActionBatch(idParticipantUnionOperationPk);
		}
	}
	
	/**
	 * Catch action batch.
	 *
	 * @param idParticipantUnionOperationPk the id participant union operation pk
	 */
	@Transactional
	public void catchActionBatch(Long idParticipantUnionOperationPk){

		try {
			
			//CAMBIAR A ESTADO FALLIDO EN DEFINITIVO
			ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();		
			partUnionOpeTO.setIdPartUnionOperationPk(idParticipantUnionOperationPk);
			partUnionOpeTO.setState(ParticipantUnionOperationStateType.DEFINITIVE_ERROR.getCode());	//pedir pk para error en definitivo
			participantUnificationServiceFacade.updateStateOnlyParticipantUnion(partUnionOpeTO);
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return sucess;
	}
}
