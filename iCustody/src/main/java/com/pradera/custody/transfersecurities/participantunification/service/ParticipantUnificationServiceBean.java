package com.pradera.custody.transfersecurities.participantunification.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.tranfersecurities.to.CustodyOperationTO;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionBalanceTO;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountRequest;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.blockoperation.UnblockRequest;
import com.pradera.model.custody.participantunion.BlockOperationUnion;
import com.pradera.model.custody.participantunion.ParticipantUnionBalance;
import com.pradera.model.custody.participantunion.ParticipantUnionFile;
import com.pradera.model.custody.participantunion.ParticipantUnionOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.ParticipantUnionOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.MechanismOperationStateType;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnificationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class ParticipantUnificationServiceBean extends CrudDaoServiceBean {

	/**
	 * Find all participants.
	 * 
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	public List<Participant> findAllParticipants() throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select p.idParticipantPk,p.description, p.state from Participant p order by p.description");
		Query query = em.createQuery(sbQuery.toString());
		List<Object> oList = query.getResultList();
		List<Participant> participantList = new ArrayList<Participant>();
		// Parsing the result
		for (int i = 0; i < oList.size(); ++i) {
			Object[] objects = (Object[]) oList.get(i);
			// Creating Participant
			Participant participant = new Participant();
			participant.setDescription((String) objects[1]);
			participant.setIdParticipantPk((Long) objects[0]);
			participant.setState((Integer) objects[2]);
			participantList.add(participant);
		}
		return participantList;
	}
	
	/**
	 * Delete block operation union.
	 *
	 * @param idParticipantUnion the id participant union
	 * @throws ServiceException the service exception
	 */
	public void deleteBlockOperationUnion(Long idParticipantUnion) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" DELETE");
		sbQuery.append("   FROM BlockOperationUnion BOU ");
		sbQuery.append("  WHERE BOU.participantUnionOperation.idPartUnionOperationPk=:idParticipantUnion ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantUnion",idParticipantUnion);
		query.executeUpdate();		
	}
	
	/**
	 * Delete participant union balance.
	 *
	 * @param idParticipantUnion the id participant union
	 * @throws ServiceException the service exception
	 */
	public void deleteParticipantUnionBalance(Long idParticipantUnion) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" DELETE");
		sbQuery.append("   FROM ParticipantUnionBalance PUB ");
		sbQuery.append("  WHERE PUB.participantUnionOperation.idPartUnionOperationPk=:idParticipantUnion ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantUnion",idParticipantUnion);
		query.executeUpdate();		
		em.flush();
		em.clear();
	}
	
	/**
	 * Lst holder accounts with participante union balance.
	 *
	 * @param idParticipantUnion the id participant union
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Long> LstHolderAccountsWithParticipanteUnionBalance(Long idParticipantUnion) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select PUB.targetHolderAccount.idHolderAccountPk From ParticipantUnionBalance PUB ");
		sbQuery.append(" Where ");
		sbQuery.append(" PUB.participantUnionOperation.idPartUnionOperationPk = :idParticipantUnionPrm ");
		TypedQuery<Long> query = em.createQuery(sbQuery.toString(),Long.class); 
		query.setParameter("idParticipantUnionPrm", idParticipantUnion);
		
		return query.getResultList();	
	}
	
	/**
	 * Lst data with participante union balance.
	 *
	 * @param idParticipantUnion the id participant union
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ParticipantUnionBalanceTO> LstDataWithParticipanteUnionBalance(Long idParticipantUnion) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select PUB.idParticipantUnionBalance, "
						    + " PUB.originHolderAccount.idHolderAccountPk, "
						    + " PUB.targetHolderAccount.idHolderAccountPk, "
						    + " PUB.securities.idSecurityCodePk "
				     + " From ParticipantUnionBalance PUB ");
		sbQuery.append(" Where ");
		sbQuery.append(" PUB.participantUnionOperation.idPartUnionOperationPk = :idParticipantUnionPrm ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantUnionPrm", idParticipantUnion);
		
		ParticipantUnionBalanceTO participantButo = null;
		
		List<Object> oList = query.getResultList();
		List<ParticipantUnionBalanceTO> participantUnionBalanceTOList = new ArrayList<ParticipantUnionBalanceTO>();
		// Parsing the result
		for (int i = 0; i < oList.size(); ++i) {
			Object[] objects = (Object[]) oList.get(i);
			participantButo = new ParticipantUnionBalanceTO();
			participantButo.setIdPartUnionBalancePk((Long) objects[0]);
			participantButo.setIdSourceHolderAccountPk((Long) objects[1]);
			participantButo.setIdTargetHolderAccountPk((Long) objects[2]);
			participantButo.setIdSecurityCodePk((String) objects[3]);
			participantUnionBalanceTOList.add(participantButo);
		}
		
		return participantUnionBalanceTOList;	
	}
	
	
	
	/**
	 * Delete accounts and dependences.
	 *
	 * @param lstAccountsPks the lst accounts pks
	 * @throws ServiceException the service exception
	 */
	public void deleteAccountsAndDependences(List<Long> lstAccountsPks) throws ServiceException{
		for(Long accountPk: lstAccountsPks){
			deleteHolderAccountRequestWithAccountPk(accountPk);
			delete(HolderAccount.class, accountPk);
		}	
	}
	
	/**
	 * Delete holder account request with account pk.
	 *
	 * @param accountPk the account pk
	 * @throws ServiceException the service exception
	 */
	public void deleteHolderAccountRequestWithAccountPk(Long accountPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" DELETE");
		sbQuery.append("   FROM HolderAccountRequest HAR ");
		sbQuery.append("  WHERE HAR.idHolderAccountReqPk = :idHolderAccountReqPkPrm ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountReqPkPrm",accountPk);
		query.executeUpdate();		
	}
	
	/**
	 * Holder acccount with participante union balance.
	 *
	 * @param idParticipantUnionOperation the id participant union operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> holderAcccountWithParticipanteUnionBalance(Long idParticipantUnionOperation) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT ");
		sbQuery.append(" 	   PUB.targetHolderAccount ");
		sbQuery.append("  FROM ParticipantUnionBalance PUB");
		sbQuery.append(" WHERE PUB.originHolderAccount.idHolderAccountPk=:idOriginHolderAccountPk");
		sbQuery.append("   AND PUB.participantUnionOperation.idPartUnionOperationPk=:idParticipantUnionOperation");
		
		TypedQuery<HolderAccount> query = em.createQuery(sbQuery.toString(),HolderAccount.class); 
		
		query.setParameter("idParticipantUnionOperation", idParticipantUnionOperation);
		
		
		return query.getResultList();	
	}
	
	/**
	 * Update holder account state.
	 *
	 * @param holderAccount the holder account
	 * @throws ServiceException the service exception
	 */
	public void updateHolderAccountState(HolderAccount holderAccount) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();		
		sbQuery.append("UPDATE HolderAccount HA SET HA.stateAccount=:stateAccount");
		sbQuery.append(" WHERE HA.idHolderAccountPk=:idHolderAccountPk");
			
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("stateAccount",holderAccount.getStateAccount());
		query.setParameter("idHolderAccountPk",holderAccount.getIdHolderAccountPk()).executeUpdate();
	}
	
	/**
	 * Update holder account number.
	 *
	 * @param holderAccount the holder account
	 * @throws ServiceException the service exception
	 */
	public void updateHolderAccountNumber(HolderAccount holderAccount) throws ServiceException{
		//get code befor ) not included i means we should concate with )- and new account number
		String firtsAlternativeCode=StringUtils.substringBefore(holderAccount.getAlternateCode(),")");
		StringBuilder sbQuery = new StringBuilder();
		Query query = em.createQuery("SELECT MAX(ha.accountNumber) + 1 FROM HolderAccount ha WHERE ha.participant.idParticipantPk = :idParticipantPkParameter");
		query.setParameter("idParticipantPkParameter", holderAccount.getParticipant().getIdParticipantPk());
		Integer correlative = 0;
		correlative = (Integer) query.getSingleResult();
		sbQuery.append(" UPDATE HolderAccount HA SET  HA.accountNumber = :accountNumber, HA.alternateCode=:alternate ");
		sbQuery.append(" WHERE HA.idHolderAccountPk=:idHolderAccountPk");
		query = em.createQuery(sbQuery.toString());
		query.setParameter("accountNumber", correlative);
		query.setParameter("alternate", firtsAlternativeCode+")-"+correlative);
		query.setParameter("idHolderAccountPk",holderAccount.getIdHolderAccountPk());
		query.executeUpdate();
	}
	
	/**
	 * Update participant union balance.
	 *
	 * @param participantUnionBalance the participant union balance
	 * @throws ServiceException the service exception
	 */
	public void updateParticipantUnionBalance(ParticipantUnionBalance participantUnionBalance) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("UPDATE ParticipantUnionBalance PUB SET PUB.idParticipantUnionBalance=:idParticipantUnionBalance");
		
		if (Validations.validateIsNotNull(participantUnionBalance.getAvailableBalance())) {
			sbQuery.append(",PUB.availableBalance=:availableBalance");
			mapParam.put("availableBalance", participantUnionBalance.getAvailableBalance());
		}
		if (Validations.validateIsNotNull(participantUnionBalance.getParticipantUnionOperation())) {
			sbQuery.append(",PUB.participantUnionOperation.idPartUnionOperationPk=:idPartUnionOperationPk");
			mapParam.put("idPartUnionOperationPk", participantUnionBalance.getParticipantUnionOperation().getIdPartUnionOperationPk());
		}
		if (Validations.validateIsNotNull(participantUnionBalance.getBanBalance())) {
			sbQuery.append(",PUB.banBalance=:banBalance");
			mapParam.put("banBalance", participantUnionBalance.getBanBalance());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getPawnBalance())) {
			sbQuery.append(",PUB.pawnBalance=:pawnBalance");
			mapParam.put("pawnBalance", participantUnionBalance.getPawnBalance());
		}			
		if (Validations.validateIsNotNull(participantUnionBalance.getOtherBlockBalance())) {
			sbQuery.append(",PUB.otherBlockBalance=:otherBlockBalance");
			mapParam.put("otherBlockBalance", participantUnionBalance.getOtherBlockBalance());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getReserveBalance())) {
			sbQuery.append(",PUB.reserveBalance=:reserveBalance");
			mapParam.put("reserveBalance", participantUnionBalance.getReserveBalance());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getTotalBalance())) {
			sbQuery.append(",PUB.totalBalance=:totalBalance");
			mapParam.put("totalBalance", participantUnionBalance.getTotalBalance());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getLastModifyApp())) {
			sbQuery.append(",PUB.lastModifyApp=:lastModifyApp");
			mapParam.put("lastModifyApp", participantUnionBalance.getLastModifyApp());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getLastModifyDate())) {
			sbQuery.append(",PUB.lastModifyDate=:lastModifyDate");
			mapParam.put("lastModifyDate", participantUnionBalance.getLastModifyDate());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getLastModifyIp())) {
			sbQuery.append(",PUB.lastModifyIp=:lastModifyIp");
			mapParam.put("lastModifyIp", participantUnionBalance.getLastModifyIp());
		}
		
		if (Validations.validateIsNotNull(participantUnionBalance.getLastModifyUser())) {
			sbQuery.append(",PUB.lastModifyUser=:lastModifyUser");
			mapParam.put("lastModifyUser", participantUnionBalance.getLastModifyUser());
		}
		
		sbQuery.append(" WHERE PUB.idParticipantUnionBalance=:idParticipantUnionBalance");
		Query query = em.createQuery(sbQuery.toString());
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setParameter("idParticipantUnionBalance",participantUnionBalance.getIdParticipantUnionBalance()).executeUpdate();	
	}
	
	/**
	 * Gets the participant union operations registered.
	 *
	 * @param idParticipantPk the id participant pk
	 * @param indSourceTarget the ind source target
	 * @return the participant union operations registered
	 * @throws ServiceException             the service exception
	 */
	public boolean validateExistsParticipantUnifications(Long idParticipantPk, Long indSourceTarget) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT ");
		sbQuery.append("  nvl (count ( PUO.custodyOperation.idCustodyOperationPk ) , 0 ) ");
		sbQuery.append("  FROM ParticipantUnionOperation PUO");
		sbQuery.append(" WHERE PUO.state in :partUnionOpeRegisState ");
		if(indSourceTarget.equals(ComponentConstant.SOURCE)){
			sbQuery.append(" AND PUO.sourceParticipant.idParticipantPk =:idParticipantPk ");
		}else{
			sbQuery.append(" AND PUO.targetParticipant.idParticipantPk =:idParticipantPk ");
		}
		List<Integer> lstState=new ArrayList<Integer>();
		lstState.add(ParticipantUnionOperationStateType.REGISTERED.getCode());
		lstState.add(ParticipantUnionOperationStateType.PRELIMINARY.getCode());
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("partUnionOpeRegisState",lstState);
		query.setParameter("idParticipantPk",idParticipantPk);		
		
		Long count = (Long) query.getSingleResult();
		if(count > 0L){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * Gets the participant union files.
	 * 
	 * @param participantUnionOperationPk
	 *            the participant union operation pk
	 * @return the participant union files
	 * @throws Exception
	 *             the exception
	 */
	public List<ParticipantUnionFile> getParticipantUnionFiles(
			Long participantUnionOperationPk) throws Exception {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT ");
		sbQuery.append("       PUF.idParticipantUnionFilePk, ");
		sbQuery.append("       PUF.file, ");
		sbQuery.append("       PUF.filename,");
		sbQuery.append("       PUF.documentType");
		sbQuery.append("  FROM ParticipantUnionFile PUF");
		sbQuery.append(" WHERE PUF.participantUnionOperation.custodyOperation.idCustodyOperationPk=:participantUnionOperationPk ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("participantUnionOperationPk",participantUnionOperationPk);
		List<Object> objectsList = (List<Object>) query.getResultList();
		List<ParticipantUnionFile> participantUnionFileList = new ArrayList<ParticipantUnionFile>();
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			ParticipantUnionFile participantUnionFile = new ParticipantUnionFile();

			participantUnionFile.setIdParticipantUnionFilePk((Long) objects[0]);
			participantUnionFile.setFile((byte[]) objects[1]);
			participantUnionFile.setFilename((String) objects[2]);
			participantUnionFile.setDocumentType((Integer)objects[3]);
			InputStream inputStream = new ByteArrayInputStream(
					participantUnionFile.getFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(
					inputStream, null, participantUnionFile.getFilename());
		//	participantUnionFile.setStreamedContentFile(streamedContentFile);
			inputStream.close();

			participantUnionFileList.add(participantUnionFile);
		}
		return participantUnionFileList;
	}

	/**
	 * Gets the participant union operations.
	 *
	 * @param unionOperationTO            the union operation to
	 * @param mapParameters the map parameters
	 * @param isParticipant the is participant
	 * @return the participant union operations
	 * @throws ServiceException             the service exception
	 */
	public List<ParticipantUnionOperation> getParticipantUnionOperations(ParticipantUnionOperationTO unionOperationTO, 
			HashMap<Integer, ParameterTable> mapParameters, boolean isParticipant)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		// Param's map
		Map<String, Object> mapParam = new HashMap<String, Object>();
		// Building the Query
		sbQuery.append("SELECT");
		sbQuery.append("       PUO.custodyOperation.idCustodyOperationPk, ");//0
		sbQuery.append("       PUO.sourceParticipant.idParticipantPk, ");//1
		sbQuery.append("       PUO.targetParticipant.idParticipantPk,");//2
		sbQuery.append("       PUO.state,");//3
		sbQuery.append("       PUO.unionDate, ");//4
		sbQuery.append("       PUO.sourceParticipant.mnemonic, ");//5
		sbQuery.append("       PUO.sourceParticipant.state, ");//6
		sbQuery.append("       PUO.targetParticipant.mnemonic,");//7
		sbQuery.append("       PUO.targetParticipant.state,");//8
		sbQuery.append("       PUO.motive,");//9
		sbQuery.append("       PUO.observations,");//10
		sbQuery.append("       PUO.custodyOperation.registryDate,");//11
		sbQuery.append("       PUO.councilResolutionDate,");//12
		sbQuery.append("       PUO.superResolutionDate,");//13
		sbQuery.append("       PUO.custodyOperation.operationNumber,");//14
		sbQuery.append("       PUO.sourceParticipant.description, ");//15
		sbQuery.append("       PUO.targetParticipant.description ");//16
		sbQuery.append("  FROM ParticipantUnionOperation PUO");
		sbQuery.append(" WHERE 1=1");
		Query query = null;
		if (Validations.validateIsNotNullAndPositive(unionOperationTO.getIdPartUnionOperationPk())) {
			sbQuery.append(" AND PUO.custodyOperation.operationNumber =:operationNumber");
			mapParam.put("operationNumber",unionOperationTO.getIdPartUnionOperationPk());
		}
		
		if(isParticipant){
			if (Validations.validateIsNotNull(unionOperationTO.getSourceParticipant()) && Validations.validateIsNotNullAndPositive(unionOperationTO.getSourceParticipant().getIdParticipantPk())) {
				sbQuery.append(" AND ( PUO.sourceParticipant.idParticipantPk =:sourceParticipant ");
				mapParam.put("sourceParticipant", unionOperationTO.getSourceParticipant().getIdParticipantPk());
			}

			//si no ingreso un participante destino
			if (Validations.validateIsNotNull(unionOperationTO.getTargetParticipant()) && 
				Validations.validateIsNotNullAndPositive(unionOperationTO.getTargetParticipant().getIdParticipantPk())) {
				sbQuery.append(" ) AND PUO.targetParticipant.idParticipantPk =:targetParticipant ");
				mapParam.put("targetParticipant", unionOperationTO.getTargetParticipant().getIdParticipantPk());
			}else{
				//el participante puede estar en el origen o destino.
				sbQuery.append(" OR PUO.targetParticipant.idParticipantPk =:targetParticipant ) ");
				mapParam.put("targetParticipant", unionOperationTO.getSourceParticipant().getIdParticipantPk());
			}
			
		}else{
			if (Validations.validateIsNotNull(unionOperationTO.getSourceParticipant()) && Validations.validateIsNotNullAndPositive(unionOperationTO.getSourceParticipant().getIdParticipantPk())) {
				sbQuery.append(" AND PUO.sourceParticipant.idParticipantPk =:sourceParticipant");
				mapParam.put("sourceParticipant", unionOperationTO.getSourceParticipant().getIdParticipantPk());
			}

			if (Validations.validateIsNotNull(unionOperationTO.getTargetParticipant()) && Validations.validateIsNotNullAndPositive(unionOperationTO.getTargetParticipant().getIdParticipantPk())) {
				sbQuery.append(" AND PUO.targetParticipant.idParticipantPk =:targetParticipant");
				mapParam.put("targetParticipant", unionOperationTO.getTargetParticipant().getIdParticipantPk());
			}
		}
		

		if (Validations.validateIsNotNullAndPositive(unionOperationTO.getMotive())) {
			sbQuery.append(" AND PUO.motive=:motive");
			mapParam.put("motive", unionOperationTO.getMotive());
		}

		if (Validations.validateIsNotNullAndPositive(unionOperationTO.getState())) {
			sbQuery.append(" AND PUO.state=:state");
			mapParam.put("state", unionOperationTO.getState());
		}
		// Validating if register date and End Date are not null
		if (Validations.validateIsNotNull(unionOperationTO.getInitialDate()) && Validations.validateIsNotNull(unionOperationTO.getEndDate())) { //
			sbQuery.append(" AND trunc(PUO.custodyOperation.registryDate) >=:initialDatePrm and trunc(PUO.custodyOperation.registryDate) <=:endDatePrm");
			mapParam.put("initialDatePrm", unionOperationTO.getInitialDate());
			mapParam.put("endDatePrm", unionOperationTO.getEndDate());
		} else if (Validations.validateIsNotNull(unionOperationTO.getInitialDate())&& Validations.validateIsNull(unionOperationTO.getEndDate())) {
			sbQuery.append(" AND trunc(PUO.custodyOperation.registryDate) >=:initialDatePrm");
			mapParam.put("initialDatePrm", unionOperationTO.getInitialDate());
		} else if (Validations.validateIsNull(unionOperationTO.getInitialDate())&& Validations.validateIsNotNull(unionOperationTO.getEndDate())) {
			sbQuery.append(" AND trunc(PUO.custodyOperation.registryDate) <=:endDatePrm");
			mapParam.put("endDatePrm", unionOperationTO.getEndDate());
		}
		sbQuery.append("  ORDER BY PUO.custodyOperation.idCustodyOperationPk desc ");

		query = em.createQuery(sbQuery.toString());

		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}

		List<ParticipantUnionOperation> participantUnionOperationList = new ArrayList<ParticipantUnionOperation>();
		List<Object> objectsList = query.getResultList();

		// Parsing the result
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			// Creating Participant source
			Participant sourceParticipant = new Participant();
			sourceParticipant.setIdParticipantPk((Long) objects[1]);
			sourceParticipant.setMnemonic((String) objects[5]);
			sourceParticipant.setState((Integer) objects[6]);
			sourceParticipant.setDescription((String) objects[15]);
			ParameterTable sourceDescription = mapParameters.get(sourceParticipant.getState()) ;
			sourceParticipant.setStateDescription(sourceDescription.getParameterName()== null?  null : sourceDescription.getParameterName() );
			// Creating target Participant
			Participant targetParticipant = new Participant();
			targetParticipant.setIdParticipantPk((Long) objects[2]);
			targetParticipant.setMnemonic((String) objects[7]);
			targetParticipant.setState((Integer) objects[8]);
			targetParticipant.setDescription((String) objects[16]);
			ParameterTable targetDescription = mapParameters.get(targetParticipant.getState()) ;
			targetParticipant.setStateDescription(targetDescription.getParameterName()== null?  null : targetDescription.getParameterName() );
			
			// Creating CustodyOperation
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setIdCustodyOperationPk((Long) objects[0]);
			custodyOperation.setOperationNumber((Long) objects[14]);
			custodyOperation.setRegistryDate((Date)objects[11]);
			
			ParticipantUnionOperation participantUnionOperation = new ParticipantUnionOperation();
			participantUnionOperation.setIdPartUnionOperationPk((Long) objects[0]);
			participantUnionOperation.setSourceParticipant(sourceParticipant);
			participantUnionOperation.setTargetParticipant(targetParticipant);
			participantUnionOperation.setCustodyOperation(custodyOperation);
			participantUnionOperation.setState(new Integer(objects[3].toString()));
			ParameterTable stateDescription = mapParameters.get(participantUnionOperation.getState()) ;
			participantUnionOperation.setStateDescription((stateDescription == null || stateDescription.getParameterName()== null)?  null : stateDescription.getParameterName() );
			participantUnionOperation.setUnionDate((Date) objects[4]);
			participantUnionOperation.setMotive(Integer.parseInt(objects[9].toString()));
			ParameterTable motiveDescription = mapParameters.get(participantUnionOperation.getMotive()) ;
			participantUnionOperation.setMotiveDescription(motiveDescription.getParameterName()== null?  null : motiveDescription.getParameterName() );
			participantUnionOperation.setObservations((String) objects[10]);
			participantUnionOperation.setCouncilResolutionDate((Date)objects[12]);
			participantUnionOperation.setSuperResolutionDate((Date)objects[13]);
			
			participantUnionOperationList.add(participantUnionOperation);
		}

		return participantUnionOperationList;
	}
	
	/**
	 * Update participant.
	 *
	 * @param participant the participant
	 * @throws ServiceException the service exception
	 */
	public void updateParticipant(Participant participant) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("UPDATE Participant P SET P.idParticipantPk=:idParticipantPk");
		
		if (Validations.validateIsNotNull(participant.getLastModifyApp())) {
			sbQuery.append(",P.lastModifyApp =:lastModifyApp");
			mapParam.put("lastModifyApp", participant.getLastModifyApp());
		}
		if (Validations.validateIsNotNull(participant.getLastModifyDate())) {
			sbQuery.append(",P.lastModifyDate =:lastModifyDate");
			mapParam.put("lastModifyDate", participant.getLastModifyDate());
		}		
		if (Validations.validateIsNotNull(participant.getLastModifyIp())) {
			sbQuery.append(",P.lastModifyIp =:lastModifyIp");
			mapParam.put("lastModifyIp", participant.getLastModifyIp());
		}
		if (Validations.validateIsNotNull(participant.getLastModifyUser())) {
			sbQuery.append(",P.lastModifyUser =:lastModifyUser");
			mapParam.put("lastModifyUser", participant.getLastModifyUser());
		}
		if (Validations.validateIsNotNull(participant.getState())) {
			sbQuery.append(",P.state =:state");
			mapParam.put("state", participant.getState());
		}
		if(Validations.validateIsNotNull(participant.getParticipantOrigin())
				&& Validations.validateIsNotNull(participant.getParticipantOrigin().getIdParticipantPk())){
			sbQuery.append(",P.participantOrigin.idParticipantPk = :participantSource");
			mapParam.put("participantSource", participant.getParticipantOrigin().getIdParticipantPk());
		}
		
		sbQuery.append(" WHERE P.idParticipantPk=:idParticipantPk");
		Query query = em.createQuery(sbQuery.toString());
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setParameter("idParticipantPk",participant.getIdParticipantPk()).executeUpdate();
	}
	
	/**
	 * Update custody operation.
	 *
	 * @param custodyOperation the custody operation
	 * @throws ServiceException the service exception
	 */
	public void updateCustodyOperation(CustodyOperation custodyOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("UPDATE CustodyOperation CO SET CO.idCustodyOperationPk=:idCustodyOperationPk");
		if (Validations.validateIsNotNull(custodyOperation.getRejectMotive())) {
			sbQuery.append(",CO.rejectMotive =:rejectMotive");
			mapParam.put("rejectMotive", custodyOperation.getRejectMotive());
		}
		if (Validations.validateIsNotNull(custodyOperation.getRejectMotiveOther())) {
			sbQuery.append(",CO.rejectMotiveOther =:rejectMotiveOther");
			mapParam.put("rejectMotiveOther", custodyOperation.getRejectMotiveOther());
		}
		sbQuery.append(" WHERE CO.idCustodyOperationPk=:idCustodyOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		// Building Itarator
		@SuppressWarnings("rawtypes")
		Iterator it = mapParam.entrySet().iterator();
		// Iterating
		while (it.hasNext()) {
			@SuppressWarnings("unchecked")
			// Building the entry
			Entry<String, Object> entry = (Entry<String, Object>) it.next();
			// Setting Param
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setParameter("idCustodyOperationPk",custodyOperation.getIdCustodyOperationPk()).executeUpdate();
		
	}
	
	 /**
 	 * Gets the reject motive other custody operation.
 	 *
 	 * @param idCustodyOperationPk the id custody operation pk
 	 * @return the reject motive other custody operation
 	 * @throws ServiceException the service exception
 	 */
 	/* Select other motive reject of custody operation.
	 *
	 * @param idCustodyOperationPk the id custody operation
	 */
	public CustodyOperationTO getRejectMotiveOtherCustodyOperation(Long idCustodyOperationPk) throws ServiceException{
		StringBuilder selectSQL = new StringBuilder();
		CustodyOperation objCustodyOperation = new CustodyOperation();
		selectSQL.append("Select CO ");
		selectSQL.append("from CustodyOperation CO												");
		selectSQL.append(" where CO.idCustodyOperationPk=:idCustodyOperationPk					");
		
		Query query = this.em.createQuery(selectSQL.toString());
		query.setParameter("idCustodyOperationPk",idCustodyOperationPk);
		CustodyOperationTO otherMotive = null;
		try {
			objCustodyOperation = (CustodyOperation) query.getSingleResult();			
			otherMotive = new CustodyOperationTO();
			otherMotive.setRejectMotive(objCustodyOperation.getRejectMotive());
			otherMotive.setRejectMotiveOther(objCustodyOperation.getRejectMotiveOther());			
		} catch (NoResultException ex) {
			otherMotive=null;
		} catch (NonUniqueResultException nu) {
			otherMotive=null;
		}
		return otherMotive;
	}
	
	/**
	 * Update participant union operation.
	 * 
	 * @param participantUnionOperationTO
	 *            the participant union operation to
	 * @throws ServiceException
	 *             the service exception
	 */
	public void updateParticipantUnionOperation(ParticipantUnionOperationTO participantUnionOperationTO) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		Map<String, Object> mapParam = new HashMap<String, Object>();
		sbQuery.append("UPDATE ParticipantUnionOperation PUO SET ");
		sbQuery.append(" PUO.lastModifyDate=:lastModifyDate ");
		
		if(Validations.validateIsNull(participantUnionOperationTO.getLastModifyDate())){
			participantUnionOperationTO.setLastModifyDate(CommonsUtilities.currentDate());
		}
		mapParam.put("lastModifyDate", participantUnionOperationTO.getLastModifyDate());
		

		if (Validations.validateIsNotNull(participantUnionOperationTO.getSourceParticipant())&& Validations.validateIsNotNull(participantUnionOperationTO.getSourceParticipant().getIdParticipantPk())) {
			sbQuery.append(",PUO.sourceParticipant.idParticipantPk =:sourceParticipant");
			mapParam.put("sourceParticipant", participantUnionOperationTO.getSourceParticipant().getIdParticipantPk());
		}

		if (Validations.validateIsNotNull(participantUnionOperationTO.getTargetParticipant())&& Validations.validateIsNotNull(participantUnionOperationTO.getTargetParticipant().getIdParticipantPk())) {
			sbQuery.append(",PUO.targetParticipant.idParticipantPk =:targetParticipant");
			mapParam.put("targetParticipant", participantUnionOperationTO.getTargetParticipant().getIdParticipantPk());
		}

		if (Validations.validateIsNotNull(participantUnionOperationTO.getMotive())) {
			sbQuery.append(",PUO.motive=:motive");
			mapParam.put("motive", participantUnionOperationTO.getMotive());
		}

		if (Validations.validateIsNotNull(participantUnionOperationTO.getState())) {
			sbQuery.append(",PUO.state=:state");
			mapParam.put("state", participantUnionOperationTO.getState());
		}
		if (Validations.validateIsNotNull(participantUnionOperationTO.getEndDate())) {
			sbQuery.append(",PUO.unionDate=:unionDate");
			mapParam.put("unionDate",participantUnionOperationTO.getEndDate());
		}
		if(Validations.validateIsNotNull(participantUnionOperationTO.getLastModifyApp())){
			sbQuery.append(",PUO.lastModifyApp=:lastModifyApp");
			mapParam.put("lastModifyApp", participantUnionOperationTO.getLastModifyApp());
		}
		
		if(Validations.validateIsNotNull(participantUnionOperationTO.getLastModifyIp())){
			sbQuery.append(",PUO.lastModifyIp=:lastModifyIp");
			mapParam.put("lastModifyIp", participantUnionOperationTO.getLastModifyIp());
		}
		if(Validations.validateIsNotNull(participantUnionOperationTO.getLastModifyUser())){
			sbQuery.append(",PUO.lastModifyUser=:lastModifyUser");
			mapParam.put("lastModifyUser", participantUnionOperationTO.getLastModifyUser());
		}
		sbQuery.append(" WHERE PUO.custodyOperation.idCustodyOperationPk=:idParticipantUnionOperation");
		
			Query query = em.createQuery(sbQuery.toString());
			// Building Itarator
			@SuppressWarnings("rawtypes")
			Iterator it = mapParam.entrySet().iterator();
			// Iterating
			while (it.hasNext()) {
				@SuppressWarnings("unchecked")
				// Building the entry
				Entry<String, Object> entry = (Entry<String, Object>) it.next();
				// Setting Param
				query.setParameter(entry.getKey(), entry.getValue());
			}
			query.setParameter("idParticipantUnionOperation",participantUnionOperationTO.getIdPartUnionOperationPk()).executeUpdate();
	}

	/**
	 * Gets the holder account balance by participant.
	 *
	 * @param originParticipantId the origin participant id
	 * @return the holder account balance by participant
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getHolderAccountBalanceByParticipant(Long originParticipantId) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();	
		// Building the Query
		sbQuery.append("SELECT");
		sbQuery.append(" 		HAB.totalBalance,");
		sbQuery.append(" 		HAB.availableBalance,");
		sbQuery.append(" 		HAB.pawnBalance,");
		sbQuery.append(" 		HAB.banBalance,");
		sbQuery.append(" 		HAB.transitBalance,");
		sbQuery.append(" 		HAB.otherBlockBalance,");
		sbQuery.append(" 		HAB.holderAccount.idHolderAccountId,");
		sbQuery.append(" 		HAB.security.idSecurityCodePk,");
		sbQuery.append(" 		HAB.participant.idParticipantPk,");
		sbQuery.append(" 		HAB.transitBalance,");
		sbQuery.append(" 		HAB.borrowerBalance,");
		sbQuery.append(" 		HAB.buyBalance,");
		sbQuery.append(" 		HAB.lenderBalance,");
		sbQuery.append(" 		HAB.reportedBalance,");
		sbQuery.append(" 		HAB.sellBalance,");
		sbQuery.append(" 		HAB.loanableBalance,");
		sbQuery.append(" 		HAB.marginBalance,");
		sbQuery.append(" 		HAB.oppositionBalance");
		sbQuery.append(" 		HAB.reportingBalance,");
		sbQuery.append(" 		HAB.reserveBalance");
		sbQuery.append("  FROM HolderAccountBalance HAB");
		sbQuery.append(" WHERE HAB.participant.idParticipantPk=:originParticipantId");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("originParticipantId", originParticipantId);

		List<HolderAccountBalance> holderAccountBalanceList = new ArrayList<HolderAccountBalance>();
		List<Object> objectsList = query.getResultList();

		return holderAccountBalanceList;

	}
	
	/**
	 * Reg hold account req component service bean.
	 *
	 * @param holderAccountRequestHy the holder account request hy
	 * @return the holder account request hy
	 * @throws ServiceException the service exception
	 */
	public HolderAccountRequest regHoldAccountReqComponentServiceBean(HolderAccountRequest holderAccountRequestHy) throws ServiceException{
		holderAccountRequestHy = create(holderAccountRequestHy);
		Query query = em.createQuery(new StringBuilder("UPDATE HolderAccountRequest r SET r.requestNumber = r.idHolderAccountReqPk ").append(
									 				   " WHERE r.idHolderAccountReqPk = :idHolderAccountRequestParameter").toString());
		query.setParameter("idHolderAccountRequestParameter", holderAccountRequestHy.getIdHolderAccountReqPk());
		query.executeUpdate();
		return holderAccountRequestHy;
	}
	
	/**
	 * Gets the block operation.
	 *
	 * @param idParticipant the id participant
	 * @param idHolderAccount the id holder account
	 * @param idSecurityCodePk the id security code pk
	 * @return the block operation
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperation> getBlockOperation(Long idParticipant, Long idHolderAccount, String idSecurityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 		BO.custodyOperation.idCustodyOperationPk,");
		sbQuery.append(" 		BO.originalBlockBalance");
		sbQuery.append("  FROM BlockOperation BO");
		sbQuery.append(" WHERE BO.securities.idSecurityCodePk=:idSecurityCodePk");
		sbQuery.append("   AND BO.holderAccount.idHolderAccountPk=:idHolderAccount");
		sbQuery.append("   AND BO.participant.idParticipantPk=:idParticipant");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("idHolderAccount", idHolderAccount);
		query.setParameter("idParticipant", idParticipant);
		
		List<BlockOperation> blockOperations = new ArrayList<BlockOperation>();
		
		List<Object> objectsList = ((List<Object>)query.getResultList());
		for (int i = 0; i < objectsList.size(); ++i) {			
			Object[] objects = (Object[]) objectsList.get(i);
			CustodyOperation custodyOperation = new CustodyOperation();
			custodyOperation.setIdCustodyOperationPk((Long)objects[0]);
			
			BlockOperation blockOperation = new BlockOperation();
			blockOperation.setIdBlockOperationPk((Long)objects[0]);
			blockOperation.setCustodyOperation(custodyOperation);
			
			blockOperation.setOriginalBlockBalance((BigDecimal) objects[1]);
			blockOperations.add(blockOperation);
		}
		return blockOperations;
	}
	
	/**
	 * Search target holder account and participant.
	 *
	 * @param idOriginHolderAccountPk the id origin holder account pk
	 * @param idParticipantUnionOperation the id participant union operation
	 * @return the participant union balance
	 * @throws ServiceException the service exception
	 */
	public ParticipantUnionBalance searchTargetHolderAccountAndParticipant(Long idOriginHolderAccountPk, Long idParticipantUnionOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 		PUB.targetHolderAccount.idHolderAccountPk,");
		sbQuery.append(" 		PUB.targetParticipant.idParticipantPk");
		sbQuery.append("  FROM ParticipantUnionBalance PUB");
		sbQuery.append(" WHERE PUB.originHolderAccount.idHolderAccountPk=:idOriginHolderAccountPk");
		sbQuery.append("   AND PUB.participantUnionOperation.idPartUnionOperationPk=:idParticipantUnionOperation");
		
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idOriginHolderAccountPk", idOriginHolderAccountPk);
		query.setParameter("idParticipantUnionOperation", idParticipantUnionOperation);
		
		
		Object [] objectList = (Object[]) query.getResultList().get(0); 
		HolderAccount targetHolderAccount = new HolderAccount();
		targetHolderAccount.setIdHolderAccountPk((Long)objectList[0]);
		
		Participant targetParticipant = new Participant();
		targetParticipant.setIdParticipantPk((Long)objectList[1]);
		
		ParticipantUnionBalance participantUnionBalance = new ParticipantUnionBalance();
		participantUnionBalance.setTargetHolderAccount(targetHolderAccount);
		participantUnionBalance.setTargetParticipant(targetParticipant);
		return participantUnionBalance;
	}
	
	/**
	 * Search block operation union.
	 *
	 * @param idParticipantUnionOperation the id participant union operation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockOperationUnion> searchBlockOperationUnion(Long idParticipantUnionOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		// Building the Query
		sbQuery.append("SELECT");
		sbQuery.append(" 		BOU.idBlockOperationUnionPk,");//0
		sbQuery.append(" 		BOU.blockOperation.idBlockOperationPk,");//1
		sbQuery.append(" 		BOU.blockOperation.actualBlockBalance,");//2
		sbQuery.append(" 		BOU.blockOperation.originalBlockBalance,");	//3	
		sbQuery.append(" 		BOU.originalBlockBalance,");//4
		sbQuery.append(" 		BOU.participantUnionOperation.idPartUnionOperationPk, ");//5
		sbQuery.append(" 		BOU.blockOperation.blockRequest.blockNumber,");//6
		sbQuery.append(" 		BOU.blockOperation.blockRequest.blockType,");//7
		sbQuery.append(" 		BOU.blockOperation.participant.idParticipantPk,");//8
		sbQuery.append(" 		BOU.blockOperation.blockRequest.holder.idHolderPk,");//9
		sbQuery.append(" 		BOU.blockOperation.blockRequest.valorizationType,");//10
		sbQuery.append(" 		BOU.blockOperation.blockRequest.currency,");//11
		sbQuery.append(" 		BOU.blockOperation.holderAccount.idHolderAccountPk,");//12
		sbQuery.append(" 		BOU.blockOperation.documentNumber,");//13
		sbQuery.append(" 		BOU.blockOperation.securities.idSecurityCodePk,");//14
		sbQuery.append(" 		BOU.blockOperation.blockLevel,");//15
		sbQuery.append(" 		BOU.blockOperation.blockRequest.blockEntity.idBlockEntityPk, ");//16
		sbQuery.append(" 		BOU.blockOperation.blockRequest.idBlockRequestPk");//17		
		sbQuery.append("  FROM BlockOperationUnion BOU");
		sbQuery.append(" WHERE BOU.participantUnionOperation.idPartUnionOperationPk=:idParticipantUnionOperation");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantUnionOperation", idParticipantUnionOperation);

		List<BlockOperationUnion> blockOperationUnions = new ArrayList<BlockOperationUnion>();
		List<Object> objectsList = ((List<Object>)query.getResultList());
		
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			
			BlockOperationUnion blockOperationUnion = new BlockOperationUnion();
			
			BlockRequest blockRequest = new BlockRequest();
			blockRequest.setBlockNumber((String)objects[6]);
			blockRequest.setBlockType((Integer)objects[7]);		
			
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long)objects[8]);
			
			Holder holder = new Holder();
			holder.setIdHolderPk((Long)objects[9]);
			
			BlockEntity blockEntity = new BlockEntity();
			blockEntity.setIdBlockEntityPk((Long)objects[16]);
			
			blockRequest.setHolder(holder);
			blockRequest.setValorizationType((BigDecimal)objects[10]);
			blockRequest.setCurrency((Integer)objects[11]);
			blockRequest.setIdBlockRequestPk((Long)objects[17]);
			blockRequest.setBlockEntity(blockEntity);
			
			Security security = new Security();
			security.setIdSecurityCodePk((String)objects[14]);
			//New Holder account
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk((Long)objects[12]);
			
			blockOperationUnion.setIdBlockOperationUnionPk( (Long)objects[0]);
			
			BlockOperation blockOperation = new BlockOperation();
			
			blockOperation.setBlockRequest(blockRequest);
			blockOperation.setIdBlockOperationPk((Long)objects[1]);
			blockOperation.setActualBlockBalance((BigDecimal)objects[2]);
			blockOperation.setOriginalBlockBalance((BigDecimal)objects[3]);
			blockOperation.setHolderAccount(holderAccount);
			blockOperation.setDocumentNumber((Long)objects[13]);
			blockOperation.setParticipant(participant);
			blockOperation.setSecurities(security);
			blockOperation.setBlockLevel((Integer)objects[15]);
			
			blockOperationUnion.setBlockOperation(blockOperation);
			blockOperationUnion.setOriginalBlockBalance((BigDecimal)objects[4]);
			
			ParticipantUnionOperation participantUnionOperation = new ParticipantUnionOperation();

			blockOperationUnion.setParticipantUnionOperation(participantUnionOperation);
			
			blockOperationUnions.add(blockOperationUnion);
		}
		return blockOperationUnions;
	}
	
	/**
	 * Gets the unblock operation from block operation.
	 *
	 * @param idBlockOperation the id block operation
	 * @return the unblock operation from block operation
	 * @throws ServiceException the service exception
	 */
	public UnblockOperation getUnblockOperationFromBlockOperation(Long idBlockOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 		UO.unblockRequest.documentNumber,");
		sbQuery.append(" 		UO.unblockRequest.motive");
		sbQuery.append(" 		UO.blockOperation.currentBlockBalance,");		
		sbQuery.append(" 		UO.unblockRequest.holder.idHolderPk,");		
		sbQuery.append(" 		UO.blockOperation.idBlockOperationPk,");
		sbQuery.append(" 		UO.blockOperation.currentBlockBalance,");	
		sbQuery.append(" 		UO.blockOperation.actualBlockBalance,");		
		sbQuery.append(" 		UO.blockOperation.holderAccount.idHolderAccountPk,");
		sbQuery.append(" 		UO.blockOperation.participant.idParticipantPK,");
		sbQuery.append(" 		UO.blockOperation.securities.idSecurityCodePk");
		sbQuery.append("  FROM UnblockOperation UO");
		sbQuery.append(" WHERE UR.idBlockOperationPk:=idBlockOperation");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idBlockOperation", idBlockOperation);

		
		Object [] objectList = (Object[]) query.getSingleResult(); 
		
		Holder holder = new Holder();
		holder.setIdHolderPk((Long)objectList[3]);
		
		UnblockRequest unblockRequest = new UnblockRequest();
		unblockRequest.setDocumentNumber((String)objectList[0]);
		unblockRequest.setMotive((Integer)objectList[1]);
		unblockRequest.setRequestQuantity((BigDecimal)objectList[2]);
		unblockRequest.setHolder(holder);
		
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk((Long)objectList[7]);
		
		Participant participant = new Participant();
		participant.setIdParticipantPk((Long)objectList[8]);
		
		BlockOperation blockOperation = new BlockOperation();		
		blockOperation.setOriginalBlockBalance((BigDecimal)objectList[5]);
		blockOperation.setActualBlockBalance((BigDecimal)objectList[6]);
		blockOperation.setIdBlockOperationPk((Long)objectList[4]);
		blockOperation.setHolderAccount(holderAccount);
		blockOperation.setParticipant(participant);
		
		UnblockOperation unblockOperation = new UnblockOperation();
		unblockOperation.setBlockOperation(blockOperation);
		unblockOperation.setUnblockQuantity((BigDecimal)objectList[2]);
		unblockOperation.setUnblockRequest(unblockRequest);
		
		return unblockOperation;
	}
	
	/**
	 * Gets the participant union balances.
	 *
	 * @param idParticipantUnion the id participant union
	 * @return the participant union balances
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<ParticipantUnionBalance> getParticipantUnionBalances(Long idParticipantUnion)throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 	   PUB.idParticipantUnionBalance,");//0
		sbQuery.append(" 	   PUB.originHolderAccount.idHolderAccountPk,");//1
		sbQuery.append(" 	   PUB.originParticipant.idParticipantPk,");//2
		sbQuery.append(" 	   PUB.securities.idSecurityCodePk,");//3
		sbQuery.append(" 	   PUB.availableBalance,");//4
		sbQuery.append(" 	   PUB.targetHolderAccount.idHolderAccountPk,");//5
		sbQuery.append(" 	   PUB.targetParticipant.idParticipantPk,");//6
		sbQuery.append(" 	   PUB.originHolderAccount.accountNumber,");//7
		sbQuery.append(" 	   PUB.originHolderAccount.accountType,");//8
		sbQuery.append(" 	   PUB.originHolderAccount.alternateCode,");//9
		sbQuery.append(" 	   PUB.originHolderAccount.closingDate,");//10
		sbQuery.append(" 	   PUB.originHolderAccount.openingDate,");//11
		sbQuery.append(" 	   PUB.originHolderAccount.stateAccount,");//12
		sbQuery.append(" 	   PUB.totalBalance,");//13
		sbQuery.append(" 	   PUB.targetHolderAccount.accountNumber,");//14
		sbQuery.append(" 	   PUB.targetHolderAccount.accountType,");//15
		sbQuery.append(" 	   PUB.targetHolderAccount.alternateCode,");//16
		sbQuery.append(" 	   PUB.targetHolderAccount.closingDate,");//17
		sbQuery.append(" 	   PUB.targetHolderAccount.openingDate,");//18
		sbQuery.append(" 	   PUB.targetHolderAccount.stateAccount,");//19
		sbQuery.append(" 	   PUB.originHolderAccount.accountGroup,");//20
		sbQuery.append(" 	   PUB.targetHolderAccount.accountGroup");//21
		sbQuery.append("  FROM ParticipantUnionBalance PUB");
		sbQuery.append(" WHERE PUB.participantUnionOperation.idPartUnionOperationPk=:idParticipantUnion");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idParticipantUnion", idParticipantUnion);		
		List<ParticipantUnionBalance> participantUnionBalances = new ArrayList<ParticipantUnionBalance>();
		List<Object> objectsList = query.getResultList();					
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);						
			ParticipantUnionBalance participantUnionBalance = new ParticipantUnionBalance();
			participantUnionBalance.setIdParticipantUnionBalance( (Long)objects[0]);
			participantUnionBalance.setTotalBalance((BigDecimal)objects[13]);
			HolderAccount originHolderAccount = new HolderAccount();
			originHolderAccount.setIdHolderAccountPk((Long)objects[1]);
			originHolderAccount.setAccountNumber((Integer)objects[7]);
			originHolderAccount.setAccountType((Integer)objects[8]);
			originHolderAccount.setAlternateCode((String)objects[9]);
			originHolderAccount.setClosingDate((Date)objects[10]);
			originHolderAccount.setOpeningDate((Date)objects[11]);
			originHolderAccount.setStateAccount((Integer)objects[12]);
			originHolderAccount.setAccountGroup((Integer)objects[20]);			
			HolderAccount targetHolderAccount = new HolderAccount();
			targetHolderAccount.setIdHolderAccountPk((Long)objects[5]);			
			targetHolderAccount.setAccountNumber((Integer)objects[14]);
			targetHolderAccount.setAccountType((Integer)objects[15]);
			targetHolderAccount.setAlternateCode((String)objects[16]);
			targetHolderAccount.setClosingDate((Date)objects[17]);
			targetHolderAccount.setOpeningDate((Date)objects[18]);
			targetHolderAccount.setStateAccount((Integer)objects[19]);
			targetHolderAccount.setAccountGroup((Integer)objects[21]);			
			Participant originParticipant = new Participant();
			originParticipant.setIdParticipantPk((Long)objects[2]);			
			Participant targetParticipant = new Participant();
			targetParticipant.setIdParticipantPk((Long)objects[6]);						
			Security security = new Security();
			security.setIdSecurityCodePk((String)objects[3]);			
			participantUnionBalance.setOriginHolderAccount(originHolderAccount);
			participantUnionBalance.setTargetHolderAccount(targetHolderAccount);
			participantUnionBalance.setOriginParticipant(originParticipant);
			participantUnionBalance.setTargetParticipant(targetParticipant);
			participantUnionBalance.setSecurities(security);
			participantUnionBalance.setAvailableBalance((BigDecimal)objects[4]);
			participantUnionBalances.add(participantUnionBalance);
		}
		return participantUnionBalances;
	}
	
	
	
	/**
	 * Search holder by holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder
	 * @throws ServiceException the service exception
	 */
	public Holder searchHolderByHolderAccount(Long idHolderAccountPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 	   HAD.holder.idHolderPk ");
		sbQuery.append("  FROM HolderAccountDetail HAD");
		sbQuery.append(" WHERE HAD.holderAccount.idHolderAccountPk=:idHolderAccountPk ");
		sbQuery.append("   AND HAD.indRepresentative=:indRepresentative");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("idHolderAccountPk", idHolderAccountPk);
		query.setParameter("indRepresentative", GeneralConstants.ONE_VALUE_INTEGER.intValue());
				
		Long idHolderPk = (Long) query.getSingleResult();
		Holder holder = new Holder();
		holder.setIdHolderPk(idHolderPk);
		return holder;
	}
	
	/**
	 * Gets the last holder account request.
	 *
	 * @param requestHy the request hy
	 * @return the last holder account request
	 * @throws ServiceException the service exception
	 */
	public Long getLastHolderAccountRequest(HolderAccountRequest requestHy) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT");
		sbQuery.append(" 	   max() ");
		sbQuery.append("  FROM HolderAccountRequest HARH");
		sbQuery.append(" WHERE HARH.requestType=:requestType ");
		sbQuery.append("   AND HARH.holderAccount.idHolderAccountPk =:idHolderAccount");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("requestType", requestHy.getRequestType());
		query.setParameter("idHolderAccount", requestHy.getHolderAccount().getIdHolderAccountPk());
				
		return (Long) query.getSingleResult();
	}
	
	/**
	 * Register participant union.
	 *
	 * @param participantUnionOperation the participant union operation
	 * @return the participant union operation
	 * @throws ServiceException the service exception
	 */
	public ParticipantUnionOperation registerParticipantUnion(ParticipantUnionOperation participantUnionOperation) throws ServiceException {
		//CREO MI OPERACION DE UNIFICACION
		Long operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_PARTICIPANT_UNIFICATION);
		participantUnionOperation.getCustodyOperation().setOperationNumber(operationNumber);
		//participantUnionOperation.setState(ParticipantUnionOperationStateType.REGISTERED.getCode());
		create(participantUnionOperation);

		//BLOQUEO AL PARTICIPANTE ORIGEN
		Participant participant = participantUnionOperation.getSourceParticipant();		
		participant.setState(ParticipantStateType.BLOCKED.getCode());
		updateParticipant(participant);
		
		return participantUnionOperation;
	}
	
	/**
	 * Validate Balance For Participant.
	 *
	 * @param idParticipant id participant
	 * @return Validate Balance For Participant
	 * @throws ServiceException the Service Exception
	 */
	public boolean validateBalanceForParticipant(Long idParticipant)throws ServiceException{
    	boolean validateAux = false;
    	StringBuilder sbQuery = new StringBuilder();
    	Map<String, Object> parameters = new HashMap<String, Object>();
    	sbQuery.append("select hab from HolderAccountBalance hab ");
    	sbQuery.append(" where hab.participant.idParticipantPk = :idParticipantPk ");
    	sbQuery.append(" and (hab.transitBalance > 0 ");  	
    	sbQuery.append(" or hab.oppositionBalance > 0 ");
    	sbQuery.append(" or hab.accreditationBalance > 0 ");
    	sbQuery.append(" or hab.purchaseBalance > 0 ");
    	sbQuery.append(" or hab.saleBalance > 0 ");
    	sbQuery.append(" or hab.reportingBalance > 0 ");
    	sbQuery.append(" or hab.reportedBalance > 0 ");
    	sbQuery.append(" or hab.marginBalance > 0 ");
    	sbQuery.append(" or hab.borrowerBalance > 0 ");
    	sbQuery.append(" or hab.lenderBalance > 0 ");
    	sbQuery.append(" or hab.loanableBalance > 0 ");    	
    	sbQuery.append(" or hab.reserveBalance > 0) ");
       	parameters.put("idParticipantPk", idParticipant);
		List<HolderAccountBalance> errorProcess = findListByQueryString(sbQuery.toString(), parameters);
		if(errorProcess!= null && !errorProcess.isEmpty()){
			validateAux=true;
		}
    	return validateAux;
    }
	
	/**
	 * Gets the holder account lst.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account lst
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountLst(HolderAccountTO holderAccountTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" SELECT ha.idHolderAccountPk,"
					+ " 	    ha.accountNumber"
					+ "	   FROM HolderAccount ha ");
		sbQuery.append("  WHERE ha.participant.idParticipantPk =:participant ");
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("participant", holderAccountTO.getParticipantTO());
		
		List<HolderAccount> holderAccounts = new ArrayList<HolderAccount>();
		List<Object> objectsList = query.getResultList();			
		
		for (int i = 0; i < objectsList.size(); ++i) {
			HolderAccount holderAccount = new HolderAccount();
			Object[] objects = (Object[]) objectsList.get(i);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk(holderAccountTO.getParticipantTO());
			
			holderAccount.setParticipant(participant);
			holderAccount.setIdHolderAccountPk((Long)objects[0]);
			holderAccount.setAccountNumber((Integer)objects[1]);
			holderAccounts.add(holderAccount);
		}
		return holderAccounts;
	}
	
	/**
	 * Gets the holder account balance lst.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account balance lst
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getHolderAccountBalanceLst(HolderAccountTO holderAccountTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" SELECT hab.transitBalance,"); //00
		sbQuery.append("		hab.accreditationBalance, ");//01
		sbQuery.append(" 	    hab.purchaseBalance,");//02
		sbQuery.append("	    hab.reportedBalance,");//03
		sbQuery.append("	    hab.reportingBalance,");//04
		sbQuery.append("	    hab.reserveBalance,");//05
		sbQuery.append("	    hab.saleBalance,");//06
		sbQuery.append("	    hab.security.idSecurityCodePk,");//07
		sbQuery.append("	    hab.holderAccount.idHolderAccountPk,");//08
		sbQuery.append("	    hab.holderAccount.accountNumber,");//09
		sbQuery.append("	    hab.participant.idParticipantPk,");//10
		sbQuery.append("	    hab.participant.description, " ); //11
		sbQuery.append("  (select nvl(sum(hao.stockQuantity),0) from ");	//12
		sbQuery.append("  	HolderAccountOperation hao inner join hao.mechanismOperation mo ");
		sbQuery.append("  	where hao.role = :idRole ");
		sbQuery.append("  	and hao.operationPart = :idOperationPart ");
		sbQuery.append("  	and hao.stockReference is null ");				
		sbQuery.append("  	and mo.operationState in :operationStates ");
		sbQuery.append("  	and mo.securities.idSecurityCodePk =  hab.security.idSecurityCodePk ");
		sbQuery.append("  	and mo.buyerParticipant.idParticipantPk =  hab.participant.idParticipantPk ");
		sbQuery.append("  	and hao.holderAccount.idHolderAccountPk = hab.holderAccount.idHolderAccountPk ),  "); // 12
		sbQuery.append("	    hab.security.securityClass ");//13
		sbQuery.append("  FROM HolderAccountBalance hab ");
		sbQuery.append("  WHERE hab.participant.idParticipantPk =:participant ");
		sbQuery.append("    AND hab.holderAccount.participant.idParticipantPk =:participant ");
		sbQuery.append("    AND hab.totalBalance > 0 ");
		
		Query query = em.createQuery(sbQuery.toString());		
		query.setParameter("participant", holderAccountTO.getParticipantTO());
		query.setParameter("idRole", GeneralConstants.TWO_VALUE_INTEGER.intValue());
		query.setParameter("idOperationPart", GeneralConstants.ONE_VALUE_INTEGER.intValue());
		List<Integer> states = new ArrayList<Integer>();
		states.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		states.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("operationStates", states );
		
		List<HolderAccountBalance> holderAccountBalances = new ArrayList<HolderAccountBalance>();
		List<Object> objectsList = query.getResultList();			
		
		for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			
			HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
			
			holderAccountBalance.setTransitBalance((BigDecimal)objects[0]);
			holderAccountBalance.setAccreditationBalance((BigDecimal)objects[1]);
			holderAccountBalance.setPurchaseBalance((BigDecimal)objects[2]);
			holderAccountBalance.setReportedBalance((BigDecimal)objects[3]);
			holderAccountBalance.setReportingBalance((BigDecimal)objects[4]);
			holderAccountBalance.setReserveBalance((BigDecimal)objects[5]);
			holderAccountBalance.setSaleBalance((BigDecimal)objects[6]);
			
			Security security = new Security();
			security.setIdSecurityCodePk((String)objects[7]);
			security.setSecurityClass((Integer)objects[13]);
			holderAccountBalance.setSecurity(security);

			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk((Long)objects[8]);
			holderAccount.setAccountNumber((Integer)objects[9]);
			holderAccountBalance.setHolderAccount(holderAccount);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk((Long)objects[10]);
			participant.setDescription((String)objects[11]);
			
			holderAccountBalance.setParticipant(participant);
			holderAccountBalance.setPendingSellBalance((BigDecimal)objects[12]);
			holderAccountBalances.add(holderAccountBalance);
		}
		return holderAccountBalances;
	}

	/**
	 * Gets the holder account list.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountList(HolderAccountTO holderAccountTO) throws ServiceException{
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append("select ha from HolderAccount ha inner join fetch ");
		sbQuery.append(" ha.holderAccountBalances hab ");
		sbQuery.append(" where ha.participant.idParticipantPk = :idParticipantPk ");
		parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());
		return (List<HolderAccount>)findListByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Gets the holder account list without accounts.
	 *
	 * @param holderAccountTO the holder account to
	 * @param lstHoldersAccountsPk the lst holders accounts pk
	 * @return the holder account list without accounts
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountListWithoutAccounts(HolderAccountTO holderAccountTO, List<Long> lstHoldersAccountsPk) throws ServiceException{
			StringBuffer sbQuery = new StringBuffer();
			Object obj = null;
			Map<String, Object> parameters = new HashMap<String, Object>();
			sbQuery.append("select distinct ha from HolderAccount ha inner join fetch ");
			sbQuery.append(" ha.holderAccountBalances hab ");
			sbQuery.append(" where ha.participant.idParticipantPk = :idParticipantPk ");
		if(lstHoldersAccountsPk != null && lstHoldersAccountsPk.size()>0){		
			sbQuery.append(" and   ha.idHolderAccountPk not in (:lstHoldersAccountsPk) ");
		}
			parameters.put("idParticipantPk", holderAccountTO.getParticipantTO());
		if(lstHoldersAccountsPk != null && lstHoldersAccountsPk.size()>0){
			parameters.put("lstHoldersAccountsPk", lstHoldersAccountsPk);
		}
		obj = findListByQueryString(sbQuery.toString(), parameters);
		if(obj!=null)
			return (List<HolderAccount>)findListByQueryString(sbQuery.toString(), parameters);
		return null;
	}
	
	/**
	 * Gets the participant union info.
	 *
	 * @param idPartUnionOperationPk the id part union operation pk
	 * @return the participant union info
	 * @throws ServiceException the service exception
	 */
	public ParticipantUnionOperationTO getParticipantUnionInfo(
			Long idPartUnionOperationPk) throws ServiceException {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		Object[] objPartUnion = null;
		sbQuery.append("SELECT");
		sbQuery.append(" PUO.custodyOperation.idCustodyOperationPk, ");//0
		sbQuery.append(" PUO.sourceParticipant.idParticipantPk, ");//1
		sbQuery.append(" PUO.targetParticipant.idParticipantPk,");//2
		sbQuery.append(" PUO.state,");//3
		sbQuery.append(" PUO.custodyOperation.registryDate ");//4
		sbQuery.append(" FROM ParticipantUnionOperation PUO");
		sbQuery.append(" where PUO.idPartUnionOperationPk = :idPartUnionOperationPk");
		parameters.put("idPartUnionOperationPk", idPartUnionOperationPk);
		try {
			objPartUnion  =  (Object[])findObjectByQueryString(sbQuery.toString(), parameters);
		} catch (NoResultException e) {
			objPartUnion = null;
		}
		ParticipantUnionOperationTO participantUnionOperationTO = null;
		if(objPartUnion!=null){
			participantUnionOperationTO = new ParticipantUnionOperationTO();
			participantUnionOperationTO.setIdPartUnionOperationPk((Long)objPartUnion[0]);
			participantUnionOperationTO.setSourceParticipant(new Participant((Long)objPartUnion[1]));
			participantUnionOperationTO.setTargetParticipant(new Participant((Long)objPartUnion[2]));
			participantUnionOperationTO.setState((Integer)objPartUnion[3]);
		}
		return participantUnionOperationTO;
	}
}
