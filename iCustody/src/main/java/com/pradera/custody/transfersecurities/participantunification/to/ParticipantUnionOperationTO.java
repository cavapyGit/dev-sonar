package com.pradera.custody.transfersecurities.participantunification.to;

import java.io.Serializable;
import java.util.Date;

import com.pradera.model.accounts.Participant;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnionOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class ParticipantUnionOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id part union operation pk. */
	private Long idPartUnionOperationPk;
	
	/** The source participant. */
	private Participant sourceParticipant;
	
	/** The target participant. */
	private Participant targetParticipant;
	
	/** The motive. */
	private Integer motive;
	
	/** The state. */
	private Integer state;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The observations. */
	private String observations;
	
	/** The last modify app. */
	private Integer lastModifyApp;
	
	/** The last modify date. */
	private Date lastModifyDate;
	
	/** The last modify ip. */
	private String lastModifyIp;
	
	/** The last modify user. */
	private String lastModifyUser;
	
	/** The reject motive. */
	private Integer rejectMotive;
	
	/** The reject motive other. */
	private String rejectMotiveOther;
	
	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}
	
	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	
	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Gets the id part union operation pk.
	 *
	 * @return the id part union operation pk
	 */
	public Long getIdPartUnionOperationPk() {
		return idPartUnionOperationPk;
	}
	
	/**
	 * Sets the id part union operation pk.
	 *
	 * @param idPartUnionOperationPk the new id part union operation pk
	 */
	public void setIdPartUnionOperationPk(Long idPartUnionOperationPk) {
		this.idPartUnionOperationPk = idPartUnionOperationPk;
	}
	
	/**
	 * Gets the source participant.
	 *
	 * @return the source participant
	 */
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}
	
	/**
	 * Sets the source participant.
	 *
	 * @param sourceParticipant the new source participant
	 */
	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}
	
	/**
	 * Gets the target participant.
	 *
	 * @return the target participant
	 */
	public Participant getTargetParticipant() {
		return targetParticipant;
	}
	
	/**
	 * Sets the target participant.
	 *
	 * @param targetParticipant the new target participant
	 */
	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}
	
	/**
	 * Gets the motive.
	 *
	 * @return the motive
	 */
	public Integer getMotive() {
		return motive;
	}
	
	/**
	 * Sets the motive.
	 *
	 * @param motive the new motive
	 */
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the observations.
	 *
	 * @return the observations
	 */
	public String getObservations() {
		return observations;
	}
	
	/**
	 * Sets the observations.
	 *
	 * @param observations the new observations
	 */
	public void setObservations(String observations) {
		this.observations = observations;
	}
	
	/**
	 * Gets the last modify app.
	 *
	 * @return the last modify app
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}
	
	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the new last modify app
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}
	
	/**
	 * Gets the last modify date.
	 *
	 * @return the last modify date
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}
	
	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the new last modify date
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}
	
	/**
	 * Gets the last modify ip.
	 *
	 * @return the last modify ip
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}
	
	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the new last modify ip
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}
	
	/**
	 * Gets the last modify user.
	 *
	 * @return the last modify user
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}
	
	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the new last modify user
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}
	
	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return rejectMotiveOther;
	}
	
	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}
	
	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return rejectMotive;
	}
	
	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

}
