package com.pradera.custody.transfersecurities.participantunification.to;

import java.io.Serializable;
import java.math.BigDecimal;
// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnionOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class PuHolderAccountBalanceTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	/** The total balance. */
	private BigDecimal totalBalance = new BigDecimal(0);
	
	/** The available balance. */
	private BigDecimal availableBalance = new BigDecimal(0);
	
	/** The ban balance. */
	private BigDecimal banBalance = new BigDecimal(0);
	
	/** The pawn balance. */
	private BigDecimal pawnBalance = new BigDecimal(0);
	
	/** The other block balance. */
	private BigDecimal otherBlockBalance = new BigDecimal(0);
	
	/** The reserve balance. */
	private BigDecimal reserveBalance = new BigDecimal(0);
	
	/** The opposition balance. */
	private BigDecimal oppositionBalance = new BigDecimal(0);
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
	/**
	 * Gets the total balance.
	 *
	 * @return the total balance
	 */
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}
	
	/**
	 * Sets the total balance.
	 *
	 * @param totalBalance the new total balance
	 */
	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	
	/**
	 * Gets the available balance.
	 *
	 * @return the available balance
	 */
	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}
	
	/**
	 * Sets the available balance.
	 *
	 * @param availableBalance the new available balance
	 */
	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	/**
	 * Gets the ban balance.
	 *
	 * @return the ban balance
	 */
	public BigDecimal getBanBalance() {
		return banBalance;
	}
	
	/**
	 * Sets the ban balance.
	 *
	 * @param banBalance the new ban balance
	 */
	public void setBanBalance(BigDecimal banBalance) {
		this.banBalance = banBalance;
	}
	
	/**
	 * Gets the pawn balance.
	 *
	 * @return the pawn balance
	 */
	public BigDecimal getPawnBalance() {
		return pawnBalance;
	}
	
	/**
	 * Sets the pawn balance.
	 *
	 * @param pawnBalance the new pawn balance
	 */
	public void setPawnBalance(BigDecimal pawnBalance) {
		this.pawnBalance = pawnBalance;
	}
	
	/**
	 * Gets the other block balance.
	 *
	 * @return the other block balance
	 */
	public BigDecimal getOtherBlockBalance() {
		return otherBlockBalance;
	}
	
	/**
	 * Sets the other block balance.
	 *
	 * @param otherBlockBalance the new other block balance
	 */
	public void setOtherBlockBalance(BigDecimal otherBlockBalance) {
		this.otherBlockBalance = otherBlockBalance;
	}
	
	/**
	 * Gets the reserve balance.
	 *
	 * @return the reserve balance
	 */
	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}
	
	/**
	 * Sets the reserve balance.
	 *
	 * @param reserveBalance the new reserve balance
	 */
	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}
	
	/**
	 * Gets the opposition balance.
	 *
	 * @return the opposition balance
	 */
	public BigDecimal getOppositionBalance() {
		return oppositionBalance;
	}
	
	/**
	 * Sets the opposition balance.
	 *
	 * @param oppositionBalance the new opposition balance
	 */
	public void setOppositionBalance(BigDecimal oppositionBalance) {
		this.oppositionBalance = oppositionBalance;
	}

}
