package com.pradera.custody.transfersecurities.participantunification.service;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.custody.transfersecurities.participantunification.facade.ParticipantUnificationServiceFacade;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionOperationTO;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.custody.type.ParticipantUnionOperationStateType;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnionOperationPreliminaryBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@BatchProcess(name="ParticipantUnionOperationPreliminaryBatch")
@RequestScoped
public class ParticipantUnionOperationPreliminaryBatch implements JobExecution {

	/** The participant unification service bean. */
	@EJB
	private ParticipantUnificationServiceBean participantUnificationServiceBean;
	/** The participant unification service facade. */
	@EJB
	private ParticipantUnificationServiceFacade participantUnificationServiceFacade;
	/** The accounts facade. */
	@EJB
	private HolderAccountComponentServiceBean accountServiceBean;

	
	/** The log. */
	@Inject
	PraderaLogger log;
	
	private boolean sucess=false;
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Transactional
	@Override
	public void startJob(ProcessLogger processLogger) {
		List<ProcessLoggerDetail> processLoggerDetails = processLogger.getProcessLoggerDetails();
		Long idPartUnionOperationPk = null;
		String userNameLogged = "";
		/*** GETTING PARAMS ***/
		for(ProcessLoggerDetail processLoggerDetail: processLoggerDetails){
			/*** GETTING PARAMETERS ***/
			if(processLoggerDetail.getParameterName().equals("idParticipantUnion")){
				idPartUnionOperationPk = Long.valueOf(processLoggerDetail.getParameterValue());
			}else if(processLoggerDetail.getParameterName().equals("userNameLogged")){
				userNameLogged = processLoggerDetail.getParameterValue();
			}
		}
		
		try{
			
			ParticipantUnionOperationTO objPartUnion = getParticipantUnionInfo(idPartUnionOperationPk);
			participantUnificationServiceFacade.deleteParticipantUnionBalance(idPartUnionOperationPk);
			participantUnificationServiceFacade.executePreliminary(objPartUnion,userNameLogged);
			//finish ok
			sucess=true;
		}catch (ServiceException e) {
			e.printStackTrace();
			log.info(e.getMessage());
			
			catchPreeliminaryException(idPartUnionOperationPk);

		}catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage());
			
			catchPreeliminaryException(idPartUnionOperationPk);

		}
	}
	

	private void catchPreeliminaryException(Long idPartUnionOperationPk){
		try {
			
			//CAMBIAR ESTADO A FALLIDO EN PREELIMINAR
			ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();		
			partUnionOpeTO.setIdPartUnionOperationPk(idPartUnionOperationPk);
			partUnionOpeTO.setState(ParticipantUnionOperationStateType.PRELIMINARY_ERROR.getCode());	//pedir pk para error en preeliminar
			participantUnificationServiceFacade.updateStateOnlyParticipantUnion(partUnionOpeTO);
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private ParticipantUnionOperationTO getParticipantUnionInfo(Long idPartUnionOperationPk) throws ServiceException {
		return participantUnificationServiceBean.getParticipantUnionInfo(idPartUnionOperationPk);
	}
	

	
	public void removeData(List<HolderAccount> deleteHolderAccountList){
		
	}

	

	
	/**
	 * Gets the holder account list component service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account list component service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountList(HolderAccountTO holderAccountTO) throws ServiceException{
		List<HolderAccount> holderAccountList = participantUnificationServiceBean.getHolderAccountList(holderAccountTO);
		return holderAccountList;
	}
	
	
	
	/**
	 * Gets the holder account with balance list component service facade.
	 *
	 * @param holderAccountTO the holder account to
	 * @return the holder account with balance list component service facade
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getHolderAccountWithBalanceListComponentServiceFacade(HolderAccountTO holderAccountTO) throws ServiceException{
		List<HolderAccount> holderAccountList = accountServiceBean.getHolderAccountListComponentServiceBean(holderAccountTO);
		for(int i = 0; i< holderAccountList.size();++i){
			holderAccountList.get(i).getHolderAccountBalances().size();	
			
		}
		return holderAccountList;
	}
	
	/**
	 * Close holder account service bean.
	 *
	 * @param objHolderAccount the holder account request hy
	 * @return the holder account request
	 * @throws ServiceException the service exception
	 */
	public HolderAccount closeHolderAccountServiceBean(HolderAccount objHolderAccount) throws ServiceException{
		
		objHolderAccount.setStateAccount(HolderAccountStatusType.CLOSED.getCode());
		this.participantUnificationServiceBean.updateHolderAccountState(objHolderAccount);
		return objHolderAccount;
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return sucess;
	}
}
