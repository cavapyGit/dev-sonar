package com.pradera.custody.transfersecurities.participantunification.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.FileUploadEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.tranfersecurities.to.CustodyOperationTO;
import com.pradera.custody.transfersecurities.participantunification.facade.ParticipantUnificationServiceFacade;
import com.pradera.custody.transfersecurities.participantunification.to.ParticipantUnionOperationTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantFileStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.RequestParticipantFileType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.custody.participantunion.ParticipantUnionFile;
import com.pradera.model.custody.participantunion.ParticipantUnionOperation;
import com.pradera.model.custody.type.ParticipantUnionOperationRejectMotiveType;
import com.pradera.model.custody.type.ParticipantUnionOperationStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class ParticipantUnificationBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class ParticipantUnificationBean extends GenericBaseBean{
	
	/** The country residence. */
	@Inject @Configurable Integer countryResidence;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The participant list. */
	private List<Participant> participantList;

	/** The unification motives list. */
	private List<ParameterTable> unificationMotivesList;
	
	/** The file type list. */
	private List<ParameterTable> fileTypeList;
	
	/** The unification prm state. */
	private List<ParameterTable> unificationPrmState;
	
	/** The participant union operation session. */
	private ParticipantUnionOperation participantUnionOperationSession;
	
	/** The participant union operation to. */
	private ParticipantUnionOperationTO participantUnionOperationTO;
	/** The source participant. */
	private Participant sourceParticipant;
	
	/** The target participant. */
	private Participant targetParticipant;
	/** The reject motive list. */
	private List<ParameterTable> rejectMotiveList;
	/** The lst participant union files. */
	private List<ParticipantUnionFile> lstParticipantUnionFiles;
	/** The now time. */
	private Date nowTime = new Date();
	//Creating holder account balance object if exists transit balance
	/** The lst holder account balance. */
	private List<HolderAccountBalance> lstHolderAccountBalance;

	/** The lst participant union operation. */
	private GenericDataModel<ParticipantUnionOperation> lstParticipantUnionOperation;

	/** The lst security class. */
	private List<ParameterTable> lstSecurityClass = new ArrayList<ParameterTable>();
	
	/** The map security class. */
	Map<Integer, String> mapSecurityClass = new HashMap<Integer,String>();
	
	/** The current date. */
	private Date currentDate;
	
	/** The other reject motive id. */
	private Integer otherRejectMotiveId;
	
	/** The state type id. */
	private Integer stateTypeId;
	
	/** The index file. */
	private Integer indexFile = 1;
	
	/** The file type param. */
	private Integer fileTypeParam;
	
	/** The pre button selected. */
	private Boolean preButtonSelected = Boolean.FALSE; 
	
	/** The reject button selected. */
	private Boolean rejectButtonSelected = Boolean.FALSE; 
	
	/** The def button selected. */
	private Boolean defButtonSelected = Boolean.FALSE; 
	
	/** The xls_extension. */
	private String xls_extension  = ".xls";
	
	/** The xls_extension_upercase. */
	private String xls_extension_upercase  = ".XLS";
	
	/** The xlsx_extension. */
	private String xlsx_extension = ".xlsx";
	
	/** The xlsx_extension_upercase. */
	private String xlsx_extension_upercase = ".XLSX";
	
	/** The doc_extension. */
	private String doc_extension  = ".doc";
	
	/** The doc_extension_upercase. */
	private String doc_extension_upercase  = ".DOC";
	
	/** The docx_extension. */
	private String docx_extension = ".docx";
	
	/** The docx_extension_upercase. */
	private String docx_extension_upercase = ".DOCX";
	
	
	/** The pdf_extension. */
	private String pdf_extension  = ".pdf";
	
	/** The pdf_extension_upercase. */
	private String pdf_extension_upercase  = ".PDF";

	/** The is participant. */
	private boolean isParticipant;
	
	/** The is depositary. */
	private boolean isDepositary;
	
	/** The default participant. */
	private Participant defaultParticipant;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	private UserPrivilege userPrivilege;
	
	/** The unification service facade. */
	@EJB
	private ParticipantUnificationServiceFacade unificationServiceFacade;

	/** The batch process service facade. */
	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;

	/** The transfer available facade. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailableFacade;

	/** The general parameters facade. */
	@EJB
    private GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The map parameters. */
	private HashMap<Integer, ParameterTable> mapParameters = new HashMap<Integer,ParameterTable>();
	
	/** The custody operation to. */
	private CustodyOperationTO custodyOperationTO;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
		this.setfUploadFileSizeDisplay("10000");
		try {

			securityEvents();
			
			
			currentDate = CommonsUtilities.currentDate();
			sourceParticipant = new Participant();
			targetParticipant = new Participant();
			lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
			
			participantUnionOperationTO = new ParticipantUnionOperationTO();
			participantUnionOperationTO.setSourceParticipant(sourceParticipant);
			participantUnionOperationTO.setTargetParticipant(targetParticipant);
			participantUnionOperationTO.setEndDate(CommonsUtilities.currentDateTime());
			participantUnionOperationTO.setInitialDate(CommonsUtilities.currentDateTime());

			otherRejectMotiveId = ParticipantUnionOperationRejectMotiveType.OTHER.getCode();
			stateTypeId = ParticipantUnionOperationStateType.ANULLED.getCode();

			Map<String, Object> filter = new HashMap<String, Object>();
			List<Integer> states = new ArrayList<Integer>();
			states.add(ParticipantStateType.REGISTERED.getCode());
			states.add(ParticipantStateType.BLOCKED.getCode());
			filter.put("states", states);
			participantList = helperComponentFacade.getComboParticipantsByMapFilter(filter);
			if(isParticipant){
				Participant tmpParticipant = getParticipantFromList(defaultParticipant.getIdParticipantPk(), participantList);
				if( !(Validations.validateIsNotNull(tmpParticipant) && 
					  Validations.validateIsNotNullAndPositive(tmpParticipant.getIdParticipantPk())) ){
					participantList.add(defaultParticipant);
				}
				participantUnionOperationTO.setSourceParticipant(defaultParticipant);
				sourceParticipant = defaultParticipant;
			}
			
			fileTypeList = unificationServiceFacade.getParameterList(MasterTableType.UNIFICATION_FILE_TYPE);
			unificationMotivesList = unificationServiceFacade.getParameterList(MasterTableType.PARTICIPANT_UNIFICATION_MOTIVES);
			unificationPrmState = unificationServiceFacade.getParameterList(MasterTableType.PARTICIPANT_UNION_STATE);
			rejectMotiveList = unificationServiceFacade.getParameterList(MasterTableType.MASTER_TABLE_REJECT_MOTIVE_PARTICIPANT_UNION);
			
			listHolidays();
			loadParameters();
			loadSecurityClass();
			enabledButtons();
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}	
	
	/**
	 * Security events.
	 */
	public void securityEvents(){
		try {
			if(userInfo.getUserAccountSession().isParticipantInstitucion()){
				isParticipant = true;
					defaultParticipant = helperComponentFacade.findParticipantsByCode(userInfo.getUserAccountSession().getParticipantCode());
				
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				isDepositary = true;
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParameters() throws ServiceException {
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		//parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		List<Integer> masterPks = new ArrayList<Integer>(); 
			masterPks.add(MasterTableType.PARTICIPANT_STATE.getCode());
			masterPks.add(MasterTableType.PARTICIPANT_UNION_STATE.getCode());
			masterPks.add(MasterTableType.PARTICIPANT_UNIFICATION_MOTIVES.getCode());
			masterPks.add(MasterTableType.UNIFICATION_FILE_TYPE.getCode());
		parameterTableTO.setLstMasterTableFk(masterPks);
		
		List<ParameterTable> lst = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		for(ParameterTable pt : lst ){
			mapParameters.put(pt.getParameterTablePk(), pt);
		}
		
	}
	
	/**
	 * Go to.
	 *
	 * @param screenToGo the screen to go
	 * @return the string
	 */
	public String goTo(String screenToGo){
		if(screenToGo.equals("participantUnificationNew")){
			this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
			participantUnionOperationSession = new ParticipantUnionOperation();
		}else if(screenToGo.equals("participantUnificationSearch")){
			this.setViewOperationType(ViewOperationsType.CONSULT.getCode());
			preButtonSelected = Boolean.FALSE; 
			rejectButtonSelected = Boolean.FALSE; 
			defButtonSelected = Boolean.FALSE; 
		}
		return screenToGo;
	}
	/**
	 * Clean.
	 */	
	public void clean(){		
		JSFUtilities.setValidViewComponentAndChildrens("participantUnificationForm");
		if(isParticipant){
			this.setSourceParticipant(defaultParticipant);
		}else{
			this.setSourceParticipant(new Participant());
		}
		this.setTargetParticipant(new Participant());
		this.setLstParticipantUnionFiles(null);
		this.setLstParticipantUnionOperation(null);
		this.setFileTypeParam(null);
		this.setParticipantUnionOperationTO(new ParticipantUnionOperationTO());
		if(isParticipant){
			this.getParticipantUnionOperationTO().setSourceParticipant(defaultParticipant);	
		}else{
			this.getParticipantUnionOperationTO().setSourceParticipant(new Participant());
		}
		this.getParticipantUnionOperationTO().setTargetParticipant(new Participant());
		this.getParticipantUnionOperationTO().setEndDate(CommonsUtilities.currentDateTime());
		this.getParticipantUnionOperationTO().setInitialDate(CommonsUtilities.currentDateTime());	
		this.setLstHolderAccountBalance(new ArrayList<HolderAccountBalance>());
		this.indexFile = 1;
	}
	
	/**
	 * Clean register.
	 */
	public void cleanRegister(){
		clean();
		participantUnionOperationSession = new ParticipantUnionOperation();
		this.setViewOperationType(ViewOperationsType.REGISTER.getCode());
	}
	
	/**
	 * Go to detail.
	 *
	 * @param participantUnionOperationPrm the participant union operation prm
	 * @return the string
	 */
	public String goToDetail(ParticipantUnionOperation participantUnionOperationPrm){
		this.setViewOperationType(ViewOperationsType.DETAIL.getCode());
		participantUnionOperationSession = participantUnionOperationPrm;
		custodyOperationTO = new CustodyOperationTO();
		
		try {
			if(participantUnionOperationSession.getIdPartUnionOperationPk() != null)
				custodyOperationTO = unificationServiceFacade.getRejectMotiveOtherCustodyOperation(participantUnionOperationSession.getIdPartUnionOperationPk());
			
			lstParticipantUnionFiles = unificationServiceFacade.getParticipantUnionFiles(participantUnionOperationSession.getIdPartUnionOperationPk());
			
			for(ParticipantUnionFile participantUnionFile : lstParticipantUnionFiles){
				ParameterTable docDes = mapParameters.get(participantUnionFile.getDocumentType());
				participantUnionFile.setFileTypeDescription(docDes == null ? null : docDes.getParameterName());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		return "participantUnificationDetail";
	}
	
	/**
	 * Evaluate union date.
	 *
	 * @return the boolean
	 */
	public Boolean evaluateUnionDate() {
		if (CommonsUtilities.currentDate().after(this.getParticipantUnionOperationSession().getUnionDate())) {
			this.showIconError(PropertiesUtilities.getMessage(PropertiesConstants.UNION_DATE_LESS_THAN_CURRENT_DATE), "participantUnificationForm:unionDate");
			this.getParticipantUnionOperationSession().setUnionDate(new Date());
			return Boolean.TRUE;			
		}else{
			return Boolean.FALSE;
		}
		
	}// end evaluateUnionDate
	
	/**
	 * Onchange initial date.
	 */
	public void onchangeInitialDate(){
		Date initialDate = this.getParticipantUnionOperationTO().getInitialDate();
		Date endDate = this.getParticipantUnionOperationTO().getEndDate();
		String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
		if(evaluateDates(initialDate,endDate)){
			showIconError(PropertiesUtilities.getMessage(propertieConstant),"frmParticipantUnion:calendarInic");
		}
	}
	
	/**
	 * Onchange end date.
	 */
	public void onchangeEndDate(){
		Date initialDate = this.getParticipantUnionOperationTO().getInitialDate();
		Date endDate = this.getParticipantUnionOperationTO().getEndDate();
		if(evaluateDates(initialDate,endDate)){
			String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
			showIconError(PropertiesUtilities.getMessage(propertieConstant),"frmParticipantUnion:endcalendar");
			return;
		}
		if(evaluateDates(endDate,CommonsUtilities.currentDateTime())){
			String propertieConstant = PropertiesConstants.END_DATE_GREATER_THAN_SYSTEM_DATE;
			showIconError(PropertiesUtilities.getMessage(propertieConstant),"frmParticipantUnion:endcalendar");
			return;
		}
	}
	
	/**
	 * Evaluate dates.
	 * Evaluate if firstDate is greater than secondDate
	 *
	 * @param firstDate the first date
	 * @param secondDate the second date
	 * @return the boolean
	 */
	public Boolean evaluateDates(Date firstDate, Date secondDate) {
		Boolean greaterDate = Boolean.FALSE;
		if (firstDate.after(secondDate)) {
			greaterDate = Boolean.TRUE;
		}
		return greaterDate;
	}
	
	/**
	 * List holidays.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listHolidays() throws ServiceException{
		Holiday holidayFilter = new Holiday();
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		GeographicLocation countryFilter = new GeographicLocation();
		countryFilter.setIdGeographicLocationPk(countryResidence);
		holidayFilter.setGeographicLocation(countryFilter);
				
		allHolidays = CommonsUtilities.convertListDateToString(generalParametersFacade.getListHolidayDateServiceFacade(holidayFilter));
	}
	
	/**
	 * Before cancel.
	 */
	
	public void enabledButtons(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(userPrivilege.getUserAcctions().isAnnular()){
			privilegeComponent.setBtnAnnularView(true);
		}
		
		if(userPrivilege.getUserAcctions().isPreliminary()){
			privilegeComponent.setBtnPreliminary(true);
		}
		 
		if(userPrivilege.getUserAcctions().isDefinitive()){
			privilegeComponent.setBtnDefinitive(true);
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Before cancel.
	 *
	 * @param fromView the from view
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeCancel(String fromView){
		if(Validations.validateIsNotNull(participantUnionOperationSession)){
			//validating if the state is REGISTERED
			if(participantUnionOperationSession.getState().equals(ParticipantUnionOperationStateType.REGISTERED.getCode())){
				//Validating the view that is called the method
				if(fromView.equals("Search")){
					this.setRejectButtonSelected(Boolean.TRUE);
					return goToDetail(participantUnionOperationSession);
				}else{
					this.showBeforeDialog("idConfirmDialogAnul");
				}
			}else if(participantUnionOperationSession.getState().equals(ParticipantUnionOperationStateType.IN_PROCESS.getCode())){
				
				//SHOWING NOT REGISTERED MESSAGE ERROR
				showMessageOnDialog(null, null, PropertiesConstants.LBL_MSG_ERROR_UNIFICATION_STATE_INVALID, null);
				JSFUtilities.executeJavascriptFunction("PF('notSelectedDialog').show();");		
				
			}else{
				//SHOWING NOT REGISTERED MESSAGE ERROR
				showMessageOnDialog(null, null, PropertiesConstants.LBL_MSG_ERROR_UNIFICATION_STATE_INVALID, null);
				JSFUtilities.executeJavascriptFunction("PF('notSelectedDialog').show();");			
			}
		}else{
			showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.executeJavascriptFunction("PF('notSelectedDialog').show();");
		}
		return null;	
	}// end beforeCancel
	
	/**
	 * Cancel union operation.
	 * THIS METHOD CANCEL THE UNION OPERATION REQUEST
	 */
	@LoggerAuditWeb
	public void cancelUnionOperation(){
		ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();
		partUnionOpeTO.setLastModifyDate(CommonsUtilities.currentDate());
		partUnionOpeTO.setIdPartUnionOperationPk(this.getParticipantUnionOperationSession().getIdPartUnionOperationPk());
		partUnionOpeTO.setState(ParticipantUnionOperationStateType.ANULLED.getCode());
		partUnionOpeTO.setRejectMotive(this.getParticipantUnionOperationTO().getRejectMotive());
		partUnionOpeTO.setSourceParticipant(this.getParticipantUnionOperationSession().getSourceParticipant());
		partUnionOpeTO.setTargetParticipant(this.getParticipantUnionOperationSession().getTargetParticipant());
		
		//Validating the reject motive
		if(Validations.validateIsNotNullAndNotEmpty(this.getParticipantUnionOperationTO().getRejectMotiveOther())){
			partUnionOpeTO.setRejectMotiveOther(this.getParticipantUnionOperationTO().getRejectMotiveOther());
		}
		try{
			this.unificationServiceFacade.updateParticipantUnionOperation(partUnionOpeTO);
			//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.PARTICIPANT_UNIFICATION_CANCEL.getCode());
			Object[] parameters = {
					participantUnionOperationSession.getCustodyOperation().getOperationNumber(),
					partUnionOpeTO.getSourceParticipant().getMnemonic(),
					partUnionOpeTO.getTargetParticipant().getMnemonic() };
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,null, parameters, userInfo.getUserAccountSession());
			//FIN ENVIO DE MENSAJES
			this.hideDialog("idConfirmDialogAnul");
			String propertieConstant = PropertiesConstants.MSG_PARTICIPANT_UNIFICATION_ANULLED_SUCCESS;
			Object [] partUnionId = {participantUnionOperationSession.getCustodyOperation().getOperationNumber()};
			this.showDialogMessageWithParam(propertieConstant, "idSuccessDialog", partUnionId);			
			clearActions();
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Accept reject motive.
	 */
	public void acceptRejectMotive(){
		JSFUtilities.setValidViewComponentAndChildrens("rejectForm");
		JSFUtilities.executeJavascriptFunction("PF('idConfirmAnullDialog').show();");
	}
	
	/**
	 * Before register preliminary.
	 *
	 * @param fromView the from view
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeRegisterPreliminary(String fromView){
		if(Validations.validateIsNotNull(participantUnionOperationSession)){
			//Validating if the status is REGISTERED OR PRELIMINARY.
			if(Validations.validateIsNotNull(participantUnionOperationSession.getState())){
				participantUnionOperationSession.setStateDescription(mapParameters.get(participantUnionOperationSession.getState()).getParameterName());
			}
			if(participantUnionOperationSession.getState().equals(ParticipantUnionOperationStateType.REGISTERED.getCode()) ||
					participantUnionOperationSession.getState().equals(ParticipantUnionOperationStateType.PRELIMINARY.getCode()) ||
					participantUnionOperationSession.getState().equals(ParticipantUnionOperationStateType.PRELIMINARY_ERROR.getCode())){	
				//Validating the view that is called the method
				if(fromView.equals("Search")){
					this.goToDetail(participantUnionOperationSession);
					this.setPreButtonSelected(Boolean.TRUE);
					return "participantUnificationDetail";
					
				}else{
					this.showBeforeDialog("idConfirmDialogPre");
					return "";
				}	
			}else{
				//SHOWING NOT REGISTERED OR PRELIMINARY STATE MESSAGE ERROR.
				String propertieConstant = PropertiesConstants.PARTICIPANT_UNION_CONSULT_BUTTONS_TWO_STATE;	
				Object [] body = {PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_UNION_REGISTERED_LBL).toLowerCase(),
								  PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_UNION_EXECUTE_PRELIMINARY_LBL).toLowerCase()};			
				this.showDialogMessageWithParam(propertieConstant, "errorDialog", body);
				return "";
			}
		}else{
			showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.executeJavascriptFunction("PF('notSelectedDialog').show();");
			return "";
		}
	}
	
	/**
	 * Before register definitive.
	 *
	 * @param fromView the from view
	 * @return the string
	 */
	@LoggerAuditWeb
	public String beforeRegisterDefinitive(String fromView){
		//Validating if there is a Participant union Operation Selected
		if(Validations.validateIsNotNull(this.getParticipantUnionOperationSession())){
			//Validating if the state is preliminary
			if(this.getParticipantUnionOperationSession().getState().equals(ParticipantUnionOperationStateType.PRELIMINARY.getCode()) ||
			   this.getParticipantUnionOperationSession().getState().equals(ParticipantUnionOperationStateType.DEFINITIVE_ERROR.getCode()) ){
				//Validating the view that is called the method
				if(fromView.equals("Search")){
					this.goToDetail(this.getParticipantUnionOperationSession());
					this.setDefButtonSelected(Boolean.TRUE);
					return "participantUnificationDetail";
				}else{
					this.showBeforeDialog("idConfirmDialogDef");
					return "";
				}				
			}else{
				String propertieConstant = PropertiesConstants.PARTICIPANT_UNION_CONSULT_BUTTONS;	
				Object [] body = {PropertiesUtilities.getMessage(PropertiesConstants.PARTICIPANT_UNION_EXECUTE_PRELIMINARY_LBL).toLowerCase()};
				this.showDialogMessageWithParam(propertieConstant, "errorDialog", body);
				return "";
			}
		}else{
			showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.executeJavascriptFunction("PF('notSelectedDialog').show();");
			return "";
		}
	}
	
	/**
	 * Execute definitive.
	 */
	@LoggerAuditWeb
	public void executeDefinitive(){
		try{
			Boolean existTransitBalance = Boolean.FALSE;
			//COMPRUEBO SI TENGO SALDOS INCORRECTOS PARA LA UNIFICACION	
			if(this.unificationServiceFacade.validateHolderAccountBalance(participantUnionOperationSession.getSourceParticipant().getIdParticipantPk())){
				existTransitBalance = Boolean.TRUE;		
			}		
			if(existTransitBalance){
				//send report indicating wrong balances
				unificationServiceFacade.sendReport(participantUnionOperationSession);
				//MUESTRO EL DIALOGO CON LOS SALDOS QUE NO DEJAN EJECUTAR EL PREELIMINAR
				this.hideDialog("idConfirmDialogPre");
				String propertieConstant = PropertiesConstants.MSG_ACCOUNTS_WITH_BALANCES_OPERATIONS_INVALID;												
				this.showDialogMessage(propertieConstant,"existTransitBalanceDlg");

			}else{
				//REGISTRO MI BACHERO
			    this.unificationServiceFacade.registryBatchDefinitive(this.getParticipantUnionOperationSession().getIdPartUnionOperationPk());
			    
			    //ACTUALIZO MI SOLICITUD
			    ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();
				partUnionOpeTO.setIdPartUnionOperationPk(this.getParticipantUnionOperationSession().getIdPartUnionOperationPk());
				partUnionOpeTO.setState(ParticipantUnionOperationStateType.DEFINITIVE.getCode());
				partUnionOpeTO.setEndDate(this.getNowTime());
				this.unificationServiceFacade.updateParticipantUnionOperation(partUnionOpeTO);
				
//				//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
//				BusinessProcess businessProcess = new BusinessProcess();
//				businessProcess.setIdBusinessProcessPk(BusinessProcessType.PARTICIPANT_UNIFICATION_DEFINITIVE_EXECUTION.getCode());
//				Object[] parameters = {
//						participantUnionOperationSession.getCustodyOperation().getOperationNumber(),
//						partUnionOpeTO.getSourceParticipant().getMnemonic(),
//						partUnionOpeTO.getTargetParticipant().getMnemonic() };
//				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,null, parameters, userInfo.getUserAccountSession());
//				//FIN ENVIO DE MENSAJES
				
				//ENVIO EL MENSAJE DE EXITO
			    this.hideDialog("idConfirmDialogDef");
				String propertieConstant = PropertiesConstants.MSG_PARTICIPANT_UNIFICATION_DEFINITIVE_SUCCESS;
				clearActions();
				this.showDialogMessage(propertieConstant,"idSuccessDialog");
				
			}			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Load security class.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityClass() throws ServiceException{
		lstSecurityClass = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstSecurityClass.add(param);
			mapSecurityClass.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Security class description.
	 *
	 * @param parameterTablePk the parameter table pk
	 * @return the string
	 */
	public String securityClassDescription(Integer parameterTablePk){
		return mapSecurityClass.get(parameterTablePk);
	}
	
	/**
	 * Register preliminary.
	 */
	@LoggerAuditWeb
	public void registerPreliminary(){		
		
		this.setLstHolderAccountBalance(new ArrayList<HolderAccountBalance>());
		Boolean existTransitBalance = Boolean.FALSE;
		
		try{
			
			//COMPRUEBO SI TENGO SALDOS INCORRECTOS PARA LA UNIFICACION	
			if(this.unificationServiceFacade.validateHolderAccountBalance(participantUnionOperationSession.getSourceParticipant().getIdParticipantPk())){
				existTransitBalance = Boolean.TRUE;		
			}		
			if(existTransitBalance){
				//send report indicating wrong balances
				unificationServiceFacade.sendReport(participantUnionOperationSession);
				//MUESTRO EL DIALOGO CON LOS SALDOS QUE NO DEJAN EJECUTAR EL PREELIMINAR
				this.hideDialog("idConfirmDialogPre");
				String propertieConstant = PropertiesConstants.MSG_ACCOUNTS_WITH_BALANCES_OPERATIONS_INVALID;												
				this.showDialogMessage(propertieConstant,"existTransitBalanceDlg");

			}else{//SI LOS SALDOS SON CORRECTOS PARA LA UNIFICACION
				
				//PARAMETROS PARA EL PREELIMINAR
			    Map<String, Object> param = new HashMap<String, Object>();
			    param.put("userNameLogged", userInfo.getUserAccountSession().getUserName());
			    param.put("participantUnionState", participantUnionOperationSession.getState());
			    param.put("idParticipantUnion",    participantUnionOperationSession.getIdPartUnionOperationPk());			    						    
			    param.put("idTargetParticipantPk", participantUnionOperationSession.getTargetParticipant().getIdParticipantPk());
			    param.put("idSourceParticipantPk", participantUnionOperationSession.getSourceParticipant().getIdParticipantPk());
			    			
			    // LANZO EL BACHERO DE PREELIMINAR
			    BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.PARTICIPANT_UNIFICATION_PRELIMINAR_EXECUTION.getCode());
			    batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(), businessProcess, param);
			    
			    //ACTUALIZO MIS ESTADOS DE LA SOLICITUD
			    ParticipantUnionOperationTO partUnionOpeTO = new ParticipantUnionOperationTO();		
				partUnionOpeTO.setIdPartUnionOperationPk(participantUnionOperationSession.getIdPartUnionOperationPk());
				partUnionOpeTO.setTargetParticipant(participantUnionOperationSession.getTargetParticipant());
				partUnionOpeTO.setSourceParticipant(participantUnionOperationSession.getSourceParticipant());
				partUnionOpeTO.setState(participantUnionOperationSession.getState());
			    partUnionOpeTO.setState(ParticipantUnionOperationStateType.PRELIMINARY.getCode());
				this.unificationServiceFacade.updateParticipantUnionOperation(partUnionOpeTO);
				
				//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
				Object[] parameters = {
						participantUnionOperationSession.getCustodyOperation().getOperationNumber(),
						partUnionOpeTO.getSourceParticipant().getMnemonic(),
						partUnionOpeTO.getTargetParticipant().getMnemonic() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, parameters, userInfo.getUserAccountSession());
				//FIN ENVIO DE MENSAJES
				//MENSAJE DE EXITO
				hideDialog("idConfirmDialogPre");
				String propertieConstant = PropertiesConstants.MSG_PARTICIPANT_UNIFICATION_PRELIMINARY_SUCCESS;
				clearActions();
				showDialogMessage(propertieConstant,"idSuccessDialog");
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}		
	}
	/**
	 * Show before dialog.
	 * Validates if the participant union operation session selected is not null
	 * @param dialogToShow the dialog to show
	 */
	public void showBeforeDialog(String dialogToShow){
		if(Validations.validateIsNotNull(this.getParticipantUnionOperationSession())){
			JSFUtilities.executeJavascriptFunction("PF('"+dialogToShow+"').show();");
		}else{
			showMessageOnDialog(null, null, PropertiesConstants.ERROR_RECORD_NOT_SELECTED, null);
			JSFUtilities.executeJavascriptFunction("PF('notSelectedDialog').show();");
			return;
		}
	}// end showBeforeDialog
	/**
	 * Onchange participant origin.
	 */
	public void onChangeSourceParticipant(){		
		JSFUtilities.setValidViewComponentAndChildrens("participantUnificationForm");
		try{
			//Validating if sourceParticipant is not null
			if(Validations.validateIsNotNullAndPositive(sourceParticipant.getIdParticipantPk())){
				//Validating if target participant is not null to validate
				if(Validations.validateIsNotNullAndPositive(targetParticipant.getIdParticipantPk())){			
					/* Validating if target participant and origin participant are the same.  */
					if(sourceParticipant.getIdParticipantPk().equals(targetParticipant.getIdParticipantPk())){
						String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_EQUALS_ORIGIN;
						this.showDialogMessage(propertieConstant,"errorDialog");
						sourceParticipant = new Participant();
						return;	
					}
				}
				
				sourceParticipant = getParticipantFromList(sourceParticipant.getIdParticipantPk(), participantList);
								
				//Validating if participant is registered or blocked
				if(!(sourceParticipant.getState().equals(ParticipantStateType.REGISTERED.getCode()) || 
					 sourceParticipant.getState().equals(ParticipantStateType.BLOCKED.getCode()))		){
					String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_ORIGIN_NOT_REGITERED_BLOCKED;
					this.showDialogMessage(propertieConstant,"errorDialog");
					sourceParticipant = new Participant();
					return;
				}
				
				//Calling Participant Union Registered
				boolean exists = unificationServiceFacade.validateExistsParticipantUnifications(sourceParticipant.getIdParticipantPk(),ComponentConstant.SOURCE);
				if(exists){
					String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_REGISTERED_IN_OTHER;
					this.showDialogMessage(propertieConstant,"errorDialog");
					sourceParticipant = new Participant();
					return;
				}
			}			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	} // end onchangeParticipantOrigin
	
	/**
	 * Onchange participant target.21
	 */
	public void onChangeTargetParticipant(){
		JSFUtilities.setValidViewComponentAndChildrens("participantUnificationForm");
		try{
			if(Validations.validateIsNotNullAndPositive(targetParticipant.getIdParticipantPk())){			
				
				if(targetParticipant.getIdParticipantPk().equals(sourceParticipant.getIdParticipantPk())){
					String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_EQUALS_ORIGIN;
					this.showDialogMessage(propertieConstant,"errorDialog");
					targetParticipant = new Participant();
					return;	
				}
				
				targetParticipant = getParticipantFromList(targetParticipant.getIdParticipantPk(), participantList);
				
				if(!targetParticipant.getState().equals(ParticipantStateType.REGISTERED.getCode())){
					String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_TARGET_NOT_REGISTERED;
					targetParticipant = new Participant();
					this.showDialogMessage(propertieConstant,"errorDialog");
					return;			
				}
				
				//Calling Participant Union Registered
				boolean exists = unificationServiceFacade.validateExistsParticipantUnifications(targetParticipant.getIdParticipantPk(), ComponentConstant.TARGET);
				if(exists){
					String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_REGISTERED_IN_OTHER;
					this.showDialogMessage(propertieConstant,"errorDialog");
					targetParticipant = new Participant();
					return;
				}

			}
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
	}
	/**
	 * Back to search page handler.
	 */
	public void backToSearchPageHandler() {
		try {
			this.clean();
			setViewOperationType(ViewOperationsType.CONSULT.getCode());
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Before save.
	 */
	public void beforeSave(){
		JSFUtilities.hideGeneralDialogues();
		if(Validations.validateIsNull(this.getLstParticipantUnionFiles())){
			String propertieConstant = PropertiesConstants.PARTICIPANT_UNIFICATION_PARTICIPANT_SUSTENTS;
			this.showDialogMessage(propertieConstant,"errorDialog");
		}else{
			this.showBeforeDialog("idConfirmDialog");
		}
	}
	
	
	/**
	 * Validate participant state.
	 *
	 * @param participantToValidate the participant to validate
	 * @param state the state
	 * @return the boolean
	 */
	public Boolean validateParticipantState(Participant participantToValidate, Integer state){
		if(participantToValidate.getState().equals(state))
			return false;
		else
			return true;
	}
	
	/**
	 * Gets the participant from list.
	 *
	 * @param idParticipant the id participant
	 * @param participantList the participant list
	 * @return the participant from list
	 */
	public Participant getParticipantFromList(Long idParticipant, List<Participant> participantList){
		Participant participant = new Participant();
		for(Participant p : participantList){
			if(p.getIdParticipantPk().equals(idParticipant)){
				participant.setIdParticipantPk(idParticipant);
				participant.setState(p.getState());
				participant.setMnemonic(p.getMnemonic());
				participant.setDescription(p.getDescription());
				break;
			}
		}
		return participant;
	}
	/**
	 * Show dialog message with param.
	 *
	 * @param message the message
	 * @param dialog the dialog
	 * @param bodyData the body data
	 */
	public void showDialogMessageWithParam(String message, String dialog, Object[] bodyData){
		  showMessageOnDialog(null, null,message,bodyData);
		  JSFUtilities.executeJavascriptFunction("PF('"+dialog+"').show();");	
	}
	/**
	 * Show dialog error.
	 *
	 * @param message the message
	 * @param dialog the dialog
	 */
	public void showDialogMessage(String message, String dialog){
		showMessageOnDialog(null, null, message, null);
		JSFUtilities.executeJavascriptFunction("PF('"+dialog+"').show();");	
	}
	
	/**
	 * Hide dialog.
	 *
	 * @param dialog the dialog
	 */
	public void hideDialog(String dialog){
		JSFUtilities.executeJavascriptFunction("PF('"+dialog +"').hide();");	
	}
	
	/**
	 * Show dialog.
	 *
	 * @param dialog the dialog
	 */
	public void showDialog(String dialog){	
		JSFUtilities.executeJavascriptFunction("PF('"+dialog +"').show();");
	}
	/**
	 * Show icon error.
	 *
	 * @param message the message
	 * @param component the component
	 */
	public void showIconError(String message, String component){		
		JSFUtilities.addContextMessage(component,FacesMessage.SEVERITY_ERROR,message, message);
	}
	

	/**
	 * Upload sustent files.
	 *
	 * @param eventFile the event file
	 */
	public void uploadSustentFiles(FileUploadEvent eventFile) {
		
		if(!Validations.validateIsNotNullAndPositive(fileTypeParam)){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_SUSTENT_SELECTED));
			return ;
		}
		
		JSFUtilities.setValidViewComponentAndChildrens("participantUnificationForm");
		try{
			
			if(Validations.validateIsNull(this.getLstParticipantUnionFiles())){
				this.setLstParticipantUnionFiles(new ArrayList<ParticipantUnionFile>());
			}
			long fileSize = eventFile.getFile().getSize();
			String extension = eventFile.getFile().getFileName().substring(eventFile.getFile().getFileName().lastIndexOf(".", eventFile.getFile().getFileName().length()-4), eventFile.getFile().getFileName().length());
			String tipoArchivo ="";

			if(Validations.validateIsNotNullAndNotEmpty(extension)){
				if(extension.trim().equalsIgnoreCase(xls_extension) || extension.equalsIgnoreCase(xlsx_extension) ||
					extension.trim().equalsIgnoreCase(xls_extension_upercase) || extension.equalsIgnoreCase(xlsx_extension_upercase)
				){
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_EXCEL_FILE);
				}else if(extension.trim().equalsIgnoreCase(doc_extension) || extension.equalsIgnoreCase(docx_extension) ||
						extension.trim().equalsIgnoreCase(doc_extension_upercase) || extension.equalsIgnoreCase(docx_extension_upercase)
				){
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_WORD_FILE);
				}else if(extension.trim().equalsIgnoreCase(pdf_extension) || 
						 extension.trim().equalsIgnoreCase(pdf_extension_upercase)){
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_PDF_FILE);
				}else{
					tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_UNKNOW_FILE);
				}
			}else{
				tipoArchivo = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MSG_UNKNOW_FILE);
			}
			
			ParticipantUnionFile fileSustent = new ParticipantUnionFile();			
			fileSustent.setRequestFileType(RequestParticipantFileType.REGISTER.getCode());
			fileSustent.setFilename(eventFile.getFile().getFileName());
			fileSustent.setFile(eventFile.getFile().getContents());
			fileSustent.setFileSize(fileSize/((long)1024));
			fileSustent.setIndex(this.indexFile);
			fileSustent.setDocumentType(fileTypeParam);
			fileSustent.setStateFile(ParticipantFileStateType.REGISTERED.getCode());
			fileSustent.setFileDocumentType(tipoArchivo);
			ParameterTable paramDescription = mapParameters.get(fileTypeParam);
			fileSustent.setFileTypeDescription(paramDescription == null ? null : paramDescription.getParameterName());
			++indexFile;
			
			lstParticipantUnionFiles.add(fileSustent);			

		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
		
    }
	
	/**
	 * Alert.
	 *
	 * @param message the message
	 */
	public void alert(String message) {
		String validationMessage = PropertiesUtilities.getMessage(
				GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext
						.getCurrentInstance().getViewRoot().getLocale(),
				GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}
	
	/**
	 * Alert.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void alert(String header, String message) {
		showMessageOnDialog(header, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	/**
	 * Removes the file sustent.
	 *
	 * @param participantUnionFileRemove the participant union file remove
	 */
	public void removeFileSustent(ParticipantUnionFile participantUnionFileRemove){
		--indexFile;
		this.getLstParticipantUnionFiles().remove(participantUnionFileRemove);
	}
	
	
	/**
	 * Register participation union.
	 * Method that register a ParticipationUnionOperation
	 */
	@LoggerAuditWeb
	public void registerParticipationUnion(){		
		try{
			if(Validations.validateIsNotNull(this.getParticipantUnionOperationSession())){
				
				participantUnionOperationSession.setSourceParticipant(sourceParticipant);
				participantUnionOperationSession.setTargetParticipant(targetParticipant);
				participantUnionOperationSession.setParticipantUnionFile(lstParticipantUnionFiles);
				
				for(ParticipantUnionFile participantUnionFile : participantUnionOperationSession.getParticipantUnionFile()){
					participantUnionFile.setParticipantUnionOperation(participantUnionOperationSession);
					participantUnionFile.setRegistryUser(userInfo.getUserAccountSession().getUserName());
					participantUnionFile.setRegistryDate(CommonsUtilities.currentDateTime());
				}
				
				//REGISTRO LA SOLICITUD DE UNIFICACION
				unificationServiceFacade.registerParticipantUnion(participantUnionOperationSession);
				//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.PARTICIPANT_UNIFICATION_REGISTER.getCode());
				Object[] parameters = {
						participantUnionOperationSession.getCustodyOperation().getOperationNumber(),
						participantUnionOperationSession.getSourceParticipant().getMnemonic(),
						participantUnionOperationSession.getTargetParticipant().getMnemonic() };
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess,null, parameters, userInfo.getUserAccountSession());
				//FIN ENVIO DE MENSAJES
				hideDialog("idConfirmDialog");
				showDialog("idDialogMensaje");
			}
		}catch (ServiceException e) {
			JSFUtilities.putRequestMap("executeAction", Boolean.TRUE);
			showExceptionMessage(GeneralConstants.HEADER_MESSAGE_EXCEPTION, null, e.getErrorService().getMessage(), e.getParams());
			JSFUtilities.showSimpleValidationDialog();		
		}	
	}
	
	/**
	 * Clear actions.
	 */
	public void clearActions(){
		init();
		consultParticipantUnionOperation();
	}
	
	/**
	 * Consult participant union operation.
	 */
	public void consultParticipantUnionOperation(){
		try{	
			List<ParticipantUnionOperation> partUnOpList = unificationServiceFacade.getParticipantUnionOperations(participantUnionOperationTO,mapParameters,isParticipant);

			participantUnionOperationTO.setRejectMotive(null);
			participantUnionOperationTO.setRejectMotiveOther(null);
			
			this.setLstParticipantUnionOperation(new GenericDataModel<ParticipantUnionOperation>(partUnOpList));
			
		} catch (ServiceException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	
	
	/**
	 * Gets the participant list.
	 *
	 * @return the participant list
	 */
	public List<Participant> getParticipantList() {
		return participantList;
	}

	/**
	 * Sets the participant list.
	 *
	 * @param participantList the new participant list
	 */
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}



	/**
	 * Gets the unification motives list.
	 *
	 * @return the unification motives list
	 */
	public List<ParameterTable> getUnificationMotivesList() {
		return unificationMotivesList;
	}	


	/**
	 * Sets the unification motives list.
	 *
	 * @param unificationMotivesList the new unification motives list
	 */
	public void setUnificationMotivesList(List<ParameterTable> unificationMotivesList) {
		this.unificationMotivesList = unificationMotivesList;
	}



	/**
	 * Gets the participant union operation session.
	 *
	 * @return the participant union operation session
	 */
	public ParticipantUnionOperation getParticipantUnionOperationSession() {
		return participantUnionOperationSession;
	}



	/**
	 * Sets the participant union operation session.
	 *
	 * @param participantUnionOperationSession the new participant union operation session
	 */
	public void setParticipantUnionOperationSession(ParticipantUnionOperation participantUnionOperationSession) {
		this.participantUnionOperationSession = participantUnionOperationSession;
	}

	/**
	 * Gets the source participant.
	 *
	 * @return the source participant
	 */
	public Participant getSourceParticipant() {
		return sourceParticipant;
	}

	/**
	 * Sets the source participant.
	 *
	 * @param sourceParticipant the new source participant
	 */
	public void setSourceParticipant(Participant sourceParticipant) {
		this.sourceParticipant = sourceParticipant;
	}

	/**
	 * Gets the target participant.
	 *
	 * @return the target participant
	 */
	public Participant getTargetParticipant() {
		return targetParticipant;
	}

	/**
	 * Sets the target participant.
	 *
	 * @param targetParticipant the new target participant
	 */
	public void setTargetParticipant(Participant targetParticipant) {
		this.targetParticipant = targetParticipant;
	}

	/**
	 * Gets the lst participant union files.
	 *
	 * @return the lst participant union files
	 */
	public List<ParticipantUnionFile> getLstParticipantUnionFiles() {
		return lstParticipantUnionFiles;
	}

	/**
	 * Sets the lst participant union files.
	 *
	 * @param lstParticipantUnionFiles the new lst participant union files
	 */
	public void setLstParticipantUnionFiles(List<ParticipantUnionFile> lstParticipantUnionFiles) {
		this.lstParticipantUnionFiles = lstParticipantUnionFiles;
	}

	/**
	 * Gets the participant union operation to.
	 *
	 * @return the participant union operation to
	 */
	public ParticipantUnionOperationTO getParticipantUnionOperationTO() {
		return participantUnionOperationTO;
	}

	/**
	 * Sets the participant union operation to.
	 *
	 * @param participantUnionOperationTO the new participant union operation to
	 */
	public void setParticipantUnionOperationTO(
			ParticipantUnionOperationTO participantUnionOperationTO) {
		this.participantUnionOperationTO = participantUnionOperationTO;
	}

	/**
	 * Gets the unification prm state.
	 *
	 * @return the unification prm state
	 */
	public List<ParameterTable> getUnificationPrmState() {
		return unificationPrmState;
	}

	/**
	 * Sets the unification prm state.
	 *
	 * @param unificationPrmState the new unification prm state
	 */
	public void setUnificationPrmState(List<ParameterTable> unificationPrmState) {
		this.unificationPrmState = unificationPrmState;
	}
	
	/**
	 * Gets the lst participant union operation.
	 *
	 * @return the lst participant union operation
	 */
	public GenericDataModel<ParticipantUnionOperation> getLstParticipantUnionOperation() {
		return lstParticipantUnionOperation;
	}
	
	/**
	 * Sets the lst participant union operation.
	 *
	 * @param lstParticipantUnionOperation the new lst participant union operation
	 */
	public void setLstParticipantUnionOperation(
			GenericDataModel<ParticipantUnionOperation> lstParticipantUnionOperation) {
		this.lstParticipantUnionOperation = lstParticipantUnionOperation;
	}

	/**
	 * Gets the lst holder account balance.
	 *
	 * @return the lst holder account balance
	 */
	public List<HolderAccountBalance> getLstHolderAccountBalance() {
		return lstHolderAccountBalance;
	}

	/**
	 * Sets the lst holder account balance.
	 *
	 * @param lstHolderAccountBalance the new lst holder account balance
	 */
	public void setLstHolderAccountBalance(List<HolderAccountBalance> lstHolderAccountBalance) {
		this.lstHolderAccountBalance = lstHolderAccountBalance;
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * Sets the current date.
	 *
	 * @param currentDate the new current date
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	/**
	 * Gets the now time.
	 *
	 * @return the now time
	 */
	public Date getNowTime() {
		return nowTime;
	}
	
	/**
	 * Sets the now time.
	 *
	 * @param nowTime the new now time
	 */
	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}
	
	/**
	 * Gets the reject motive list.
	 *
	 * @return the reject motive list
	 */
	public List<ParameterTable> getRejectMotiveList() {
		return rejectMotiveList;
	}
	
	/**
	 * Sets the reject motive list.
	 *
	 * @param rejectMotiveList the new reject motive list
	 */
	public void setRejectMotiveList(List<ParameterTable> rejectMotiveList) {
		this.rejectMotiveList = rejectMotiveList;
	}
	
	/**
	 * Gets the other reject motive id.
	 *
	 * @return the other reject motive id
	 */
	public Integer getOtherRejectMotiveId() {
		return otherRejectMotiveId;
	}
	
	/**
	 * Sets the other reject motive id.
	 *
	 * @param otherRejectMotiveId the new other reject motive id
	 */
	public void setOtherRejectMotiveId(Integer otherRejectMotiveId) {
		this.otherRejectMotiveId = otherRejectMotiveId;
	}
	
	/**
	 * Gets the file type list.
	 *
	 * @return the file type list
	 */
	public List<ParameterTable> getFileTypeList() {
		return fileTypeList;
	}
	
	/**
	 * Sets the file type list.
	 *
	 * @param fileTypeList the new file type list
	 */
	public void setFileTypeList(List<ParameterTable> fileTypeList) {
		this.fileTypeList = fileTypeList;
	}
	
	/**
	 * Gets the file type param.
	 *
	 * @return the file type param
	 */
	public Integer getFileTypeParam() {
		return fileTypeParam;
	}
	
	/**
	 * Sets the file type param.
	 *
	 * @param fileTypeParam the new file type param
	 */
	public void setFileTypeParam(Integer fileTypeParam) {
		this.fileTypeParam = fileTypeParam;
	}
	
	/**
	 * Gets the pre button selected.
	 *
	 * @return the pre button selected
	 */
	public Boolean getPreButtonSelected() {
		return preButtonSelected;
	}
	
	/**
	 * Sets the pre button selected.
	 *
	 * @param preButtonSelected the new pre button selected
	 */
	public void setPreButtonSelected(Boolean preButtonSelected) {
		this.preButtonSelected = preButtonSelected;
		this.rejectButtonSelected = false;
		this.defButtonSelected = false;
	}
	
	/**
	 * Gets the reject button selected.
	 *
	 * @return the reject button selected
	 */
	public Boolean getRejectButtonSelected() {
		return rejectButtonSelected;
	}
	
	/**
	 * Sets the reject button selected.
	 *
	 * @param rejectButtonSelected the new reject button selected
	 */
	public void setRejectButtonSelected(Boolean rejectButtonSelected) {
		this.rejectButtonSelected = rejectButtonSelected;
		this.preButtonSelected = false;
		this.defButtonSelected = false;
	}
	
	/**
	 * Gets the def button selected.
	 *
	 * @return the def button selected
	 */
	public Boolean getDefButtonSelected() {
		return defButtonSelected;
	}
	
	/**
	 * Sets the def button selected.
	 *
	 * @param defButtonSelected the new def button selected
	 */
	public void setDefButtonSelected(Boolean defButtonSelected) {
		this.defButtonSelected = defButtonSelected;		
		this.preButtonSelected = false;
		this.rejectButtonSelected = false;
	}


	/**
	 * Gets the map parameters.
	 *
	 * @return the map parameters
	 */
	public HashMap<Integer, ParameterTable> getMapParameters() {
		return mapParameters;
	}


	/**
	 * Sets the map parameters.
	 *
	 * @param mapParameters the map parameters
	 */
	public void setMapParameters(HashMap<Integer, ParameterTable> mapParameters) {
		this.mapParameters = mapParameters;
	}

	/**
	 * Checks if is participant.
	 *
	 * @return true, if is participant
	 */
	public boolean isParticipant() {
		return isParticipant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param isParticipant the new participant
	 */
	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Checks if is depositary.
	 *
	 * @return true, if is depositary
	 */
	public boolean isDepositary() {
		return isDepositary;
	}

	/**
	 * Sets the depositary.
	 *
	 * @param isDepositary the new depositary
	 */
	public void setDepositary(boolean isDepositary) {
		this.isDepositary = isDepositary;
	}

	/**
	 * Gets the default participant.
	 *
	 * @return the default participant
	 */
	public Participant getDefaultParticipant() {
		return defaultParticipant;
	}

	/**
	 * Sets the default participant.
	 *
	 * @param defaultParticipant the new default participant
	 */
	public void setDefaultParticipant(Participant defaultParticipant) {
		this.defaultParticipant = defaultParticipant;
	}

	/**
	 * Gets the custody operation to.
	 *
	 * @return the custody operation to
	 */
	public CustodyOperationTO getCustodyOperationTO() {
		return custodyOperationTO;
	}

	/**
	 * Sets the custody operation to.
	 *
	 * @param custodyOperationTO the new custody operation to
	 */
	public void setCustodyOperationTO(CustodyOperationTO custodyOperationTO) {
		this.custodyOperationTO = custodyOperationTO;
	}

	/**
	 * Gets the state type id.
	 *
	 * @return the state type id
	 */
	public Integer getStateTypeId() {
		return stateTypeId;
	}

	/**
	 * Sets the state type id.
	 *
	 * @param stateTypeId the new state type id
	 */
	public void setStateTypeId(Integer stateTypeId) {
		this.stateTypeId = stateTypeId;
	}
}
