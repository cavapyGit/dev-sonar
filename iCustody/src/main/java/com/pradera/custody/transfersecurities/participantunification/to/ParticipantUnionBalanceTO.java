package com.pradera.custody.transfersecurities.participantunification.to;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import com.pradera.model.accounts.Participant;
import com.pradera.model.generalparameter.ParameterTable;

// TODO: Auto-generated Javadoc
/**
 * The Class ParticipantUnionOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class ParticipantUnionBalanceTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id part union operation pk. */
	private Long idPartUnionBalancePk;
	
	/** The id source participant pk. */
	private Long idSourceParticipantPk;
	
	/** The id target participant pk. */
	private Long idTargetParticipantPk;
	
	/** The id source holder account pk. */
	private Long idSourceHolderAccountPk;
	
	/** The id target holder account pk. */
	private Long idTargetHolderAccountPk;
	
	/** The id security code pk. */
	private String idSecurityCodePk;
	
	
	
	/**
	 * Gets the id part union balance pk.
	 *
	 * @return the id part union balance pk
	 */
	public Long getIdPartUnionBalancePk() {
		return idPartUnionBalancePk;
	}
	
	/**
	 * Sets the id part union balance pk.
	 *
	 * @param idPartUnionBalancePk the new id part union balance pk
	 */
	public void setIdPartUnionBalancePk(Long idPartUnionBalancePk) {
		this.idPartUnionBalancePk = idPartUnionBalancePk;
	}
	
	/**
	 * Gets the id source participant pk.
	 *
	 * @return the id source participant pk
	 */
	public Long getIdSourceParticipantPk() {
		return idSourceParticipantPk;
	}
	
	/**
	 * Sets the id source participant pk.
	 *
	 * @param idSourceParticipantPk the new id source participant pk
	 */
	public void setIdSourceParticipantPk(Long idSourceParticipantPk) {
		this.idSourceParticipantPk = idSourceParticipantPk;
	}
	
	/**
	 * Gets the id target participant pk.
	 *
	 * @return the id target participant pk
	 */
	public Long getIdTargetParticipantPk() {
		return idTargetParticipantPk;
	}
	
	/**
	 * Sets the id target participant pk.
	 *
	 * @param idTargetParticipantPk the new id target participant pk
	 */
	public void setIdTargetParticipantPk(Long idTargetParticipantPk) {
		this.idTargetParticipantPk = idTargetParticipantPk;
	}
	
	/**
	 * Gets the id source holder account pk.
	 *
	 * @return the id source holder account pk
	 */
	public Long getIdSourceHolderAccountPk() {
		return idSourceHolderAccountPk;
	}
	
	/**
	 * Sets the id source holder account pk.
	 *
	 * @param idSourceHolderAccountPk the new id source holder account pk
	 */
	public void setIdSourceHolderAccountPk(Long idSourceHolderAccountPk) {
		this.idSourceHolderAccountPk = idSourceHolderAccountPk;
	}
	
	/**
	 * Gets the id target holder account pk.
	 *
	 * @return the id target holder account pk
	 */
	public Long getIdTargetHolderAccountPk() {
		return idTargetHolderAccountPk;
	}
	
	/**
	 * Sets the id target holder account pk.
	 *
	 * @param idTargetHolderAccountPk the new id target holder account pk
	 */
	public void setIdTargetHolderAccountPk(Long idTargetHolderAccountPk) {
		this.idTargetHolderAccountPk = idTargetHolderAccountPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	
}
