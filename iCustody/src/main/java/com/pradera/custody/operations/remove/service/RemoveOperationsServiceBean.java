package com.pradera.custody.operations.remove.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.IssuanceOperation;
import com.pradera.model.component.SecuritiesOperation;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class RemoveOperationsServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
public class RemoveOperationsServiceBean extends CrudDaoServiceBean{
	
	/**
	 * The method is used to get list HolderAccountMovement.
	 *
	 * @param idParticipant Long
	 * @param idHolderAccount Long
	 * @param idSecurityCode String
	 * @param lstIdCustodyOperation the lst id custody operation
	 * @return the validate holder account movement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovement> getValidateHolderAccountMovement(Long idParticipant, Long idHolderAccount, 
			String idSecurityCode, List<Long> lstIdCustodyOperation) throws ServiceException{
		
		List<HolderAccountMovement> lstHolderAccountMovement = new ArrayList<HolderAccountMovement>();		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT ham FROM HolderAccountMovement ham												");
		jpqlQuery.append("	WHERE ham.holderAccountBalance.participant.idParticipantPk = :parIdParticipant			");
		jpqlQuery.append(" 	AND ham.holderAccountBalance.holderAccount.idHolderAccountPk = :parIdHolderAccount		");
		jpqlQuery.append(" 	AND ham.holderAccountBalance.security.idSecurityCodePk = :parIdSecurityCode 			");
		jpqlQuery.append(" 	AND (ham.custodyOperation.idCustodyOperationPk not in (:parlstIdCustodyOperation)		");
		jpqlQuery.append(" 	OR ham.custodyOperation.idCustodyOperationPk is null )									");
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("parIdParticipant", idParticipant);
		queryJpql.setParameter("parIdHolderAccount", idHolderAccount);
		queryJpql.setParameter("parIdSecurityCode", idSecurityCode);
		queryJpql.setParameter("parlstIdCustodyOperation", lstIdCustodyOperation);						
		try{
			lstHolderAccountMovement = queryJpql.getResultList();			
		}catch(NoResultException ex){
			return null;
		}		
		return lstHolderAccountMovement;
	}
	
	/**
	 * The method is used to delete SecuritiesMovement and SecurityOperation.
	 *
	 * @param objCustodyOperation CustodyOperation
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritiesMovementAndSecurityOperationRenewal(CustodyOperation objCustodyOperation, Security objSecurity) throws ServiceException{		
		List<SecuritiesOperation> lstSecuritiesOperation = getListSecuritiesOperation(objSecurity, objCustodyOperation, null);
		if(Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesOperation)){
			for(SecuritiesOperation objSecuritiesOperation : lstSecuritiesOperation){					
				deleteIssuanceMovementAndIssuanceOperationRenewal(objSecuritiesOperation, objSecurity);
				deleteSecuritiesMovementRenewal(objSecuritiesOperation);
				deleteSecuritiesOperationRenewal(objSecuritiesOperation);
			}
		}		
	}		
	
	/**
	 * Gets the list securities operation.
	 *
	 * @param objSecurity the obj security
	 * @param objCustodyOperation the obj custody operation
	 * @param objCorporativeOperation the obj corporative operation
	 * @return the list securities operation
	 */
	@SuppressWarnings("unchecked")
	public List<SecuritiesOperation> getListSecuritiesOperation(Security objSecurity, 
			CustodyOperation objCustodyOperation, CorporativeOperation objCorporativeOperation){
		List<SecuritiesOperation> lstSecuritiesOperation = new ArrayList<SecuritiesOperation>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT SO FROM SecuritiesOperation SO ");
			stringBuilderSql.append(" inner join fetch SO.securities SEC ");			
			stringBuilderSql.append(" where 1=1  ");
			if(objCorporativeOperation != null){
				if(Validations.validateIsNotNullAndNotEmpty(objCorporativeOperation.getIdCorporativeOperationPk())){
					stringBuilderSql.append(" and SO.corporativeOperation.idCorporativeOperationPk = :parIdCorporateOperation ");
					stringBuilderSql.append(" and TRUNC(SO.operationDate) = TRUNC(:parOperationDate) ");
				}
			}
			if(objCustodyOperation != null){
				if(Validations.validateIsNotNullAndNotEmpty(objCustodyOperation.getIdCustodyOperationPk())){
					stringBuilderSql.append(" and SO.custodyOperation.idCustodyOperationPk = :parIdCustodyOperation ");
					stringBuilderSql.append(" and TRUNC(SO.operationDate) = TRUNC(:parOperationDate) ");
				}
			}						
			stringBuilderSql.append(" and SEC.idSecurityCodePk = :parIdSecurityCode ");
			
			Query query = em.createQuery(stringBuilderSql.toString());		
			if(objCorporativeOperation != null){
				if(Validations.validateIsNotNullAndNotEmpty(objCorporativeOperation.getIdCorporativeOperationPk())){
					query.setParameter("parIdCorporateOperation", objCorporativeOperation.getIdCorporativeOperationPk());
					query.setParameter("parOperationDate", CommonsUtilities.currentDate());
				}
			}
			if(objCustodyOperation != null){
				if(Validations.validateIsNotNullAndNotEmpty(objCustodyOperation.getIdCustodyOperationPk())){
					query.setParameter("parIdCustodyOperation", objCustodyOperation.getIdCustodyOperationPk());
					query.setParameter("parOperationDate", objCustodyOperation.getOperationDate());
				}
			}			
			query.setParameter("parIdSecurityCode", objSecurity.getIdSecurityCodePk());			
			lstSecuritiesOperation = (List<SecuritiesOperation>) query.getResultList();			
		} catch (NoResultException e) {
			return null;
		}		
		return lstSecuritiesOperation;
	}
	
	/**
	 * The method is used to delete IssuanceMovement and IssuanceOperation.
	 *
	 * @param objSecuritiesOperation SecuritiesOperation
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteIssuanceMovementAndIssuanceOperationRenewal(SecuritiesOperation objSecuritiesOperation, Security objSecurity) throws ServiceException{		
		List<IssuanceOperation> lstIssuanceOperation = getListIssuanceOperation(objSecuritiesOperation, objSecurity);
		if(Validations.validateListIsNotNullAndNotEmpty(lstIssuanceOperation)){
			for(IssuanceOperation objIssuanceOperation : lstIssuanceOperation){
				deleteIssuanceMovementRenewal(objIssuanceOperation);
				deleteIssuanceOperationRenewal(objIssuanceOperation);
			}
		}		
	}
	
	/**
	 * The method is used to delete SecuritiesMovement.
	 *
	 * @param objSecuritiesOperation SecuritiesOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritiesMovementRenewal(SecuritiesOperation objSecuritiesOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecuritiesOperation.getIdSecuritiesOperationPk()) 
				&& Validations.validateIsNotNullAndNotEmpty(objSecuritiesOperation.getSecurities().getIdSecurityCodePk())
				&& Validations.validateIsNotNullAndNotEmpty(objSecuritiesOperation.getOperationDate())){
			sbQuery.append(" Delete SecuritiesMovement secm  ");
			sbQuery.append(" where secm.securitiesOperation.idSecuritiesOperationPk = :parIdSecuritiesOperation");
			sbQuery.append(" and secm.securities.idSecurityCodePk = :parIdSecurityCode");
			sbQuery.append(" and trunc(secm.movementDate) = trunc(:parOperationDate)");
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecuritiesOperation", objSecuritiesOperation.getIdSecuritiesOperationPk());
			query.setParameter("parIdSecurityCode", objSecuritiesOperation.getSecurities().getIdSecurityCodePk());
			query.setParameter("parOperationDate", objSecuritiesOperation.getOperationDate());
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete SecuritiesOperation.
	 *
	 * @param objSecuritiesOperation SecuritiesOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritiesOperationRenewal(SecuritiesOperation objSecuritiesOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecuritiesOperation.getIdSecuritiesOperationPk())){
			sbQuery.append(" Delete SecuritiesOperation secop  ");
			sbQuery.append(" where secop.idSecuritiesOperationPk = :parIdSecuritiesOperation");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecuritiesOperation", objSecuritiesOperation.getIdSecuritiesOperationPk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * Gets the list issuance operation.
	 *
	 * @param objSecuritiesOperation the obj securities operation
	 * @param objSecurity the obj security
	 * @return the list issuance operation
	 */
	@SuppressWarnings("unchecked")
	public List<IssuanceOperation> getListIssuanceOperation(SecuritiesOperation objSecuritiesOperation, Security objSecurity){
		List<IssuanceOperation> lstIssuanceOperation = new ArrayList<IssuanceOperation>();
		try {
			StringBuilder stringBuilderSql = new StringBuilder();
			stringBuilderSql.append(" SELECT IO FROM IssuanceOperation IO ");
			stringBuilderSql.append(" inner join fetch IO.issuance ISSU ");
			stringBuilderSql.append(" where IO.securitiesOperation.idSecuritiesOperationPk = :parIdSecuritiesOperation ");
			stringBuilderSql.append(" and IO.issuance.idIssuanceCodePk = :parIdIssuanceCode ");
			stringBuilderSql.append(" and TRUNC(IO.operationDate) = TRUNC(:parOperationDate) ");
			Query query = em.createQuery(stringBuilderSql.toString());			
			query.setParameter("parIdSecuritiesOperation", objSecuritiesOperation.getIdSecuritiesOperationPk());
			query.setParameter("parIdIssuanceCode", objSecurity.getIssuance().getIdIssuanceCodePk());
			query.setParameter("parOperationDate", objSecuritiesOperation.getOperationDate());
			lstIssuanceOperation = (List<IssuanceOperation>) query.getResultList();			
		} catch (NoResultException e) {
			return null;
		}		
		return lstIssuanceOperation;
	}
	
	/**
	 * The method is used to delete IssuanceMovement.
	 *
	 * @param objIssuanceOperation IssuanceOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteIssuanceMovementRenewal(IssuanceOperation objIssuanceOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objIssuanceOperation.getIdIssuanceOperationPk()) 
				&& Validations.validateIsNotNullAndNotEmpty(objIssuanceOperation.getIssuance().getIdIssuanceCodePk())
				&& Validations.validateIsNotNullAndNotEmpty(objIssuanceOperation.getOperationDate())){
			sbQuery.append(" Delete IssuanceMovement im  ");
			sbQuery.append(" where im.issuanceOperation.idIssuanceOperationPk = :parIdIssuanceOperation ");
			sbQuery.append(" and im.issuance.idIssuanceCodePk = :parIdIssuanceCode");
			sbQuery.append(" and trunc(im.movementDate) = trunc(:parOperationDate)");
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdIssuanceOperation", objIssuanceOperation.getIdIssuanceOperationPk());
			query.setParameter("parIdIssuanceCode", objIssuanceOperation.getIssuance().getIdIssuanceCodePk());
			query.setParameter("parOperationDate", objIssuanceOperation.getOperationDate());
			query.executeUpdate();
		}								
	}
	
	/**
	 * The method is used to delete SecuritiesOperation.
	 *
	 * @param objIssuanceOperation the obj issuance operation
	 * @throws ServiceException the service exception
	 */
	public void deleteIssuanceOperationRenewal(IssuanceOperation objIssuanceOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objIssuanceOperation.getIdIssuanceOperationPk())){
			sbQuery.append(" Delete IssuanceOperation issop  ");
			sbQuery.append(" where issop.idIssuanceOperationPk = :parIdIssuanceOperation");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdIssuanceOperation", objIssuanceOperation.getIdIssuanceOperationPk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete HolderAccountMovement.
	 *
	 * @param objCustodyOperation CustodyOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteHolderAccountMovementRenewal(CustodyOperation objCustodyOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		if(Validations.validateIsNotNullAndNotEmpty(objCustodyOperation.getIdCustodyOperationPk())
				&& Validations.validateIsNotNullAndNotEmpty(objCustodyOperation.getOperationNumber())
				&& Validations.validateIsNotNullAndNotEmpty(objCustodyOperation.getOperationDate())){
			
			List<Long> lstHolderAccountPk = geHolderAccountMovementForMarkeFact(objCustodyOperation);				
			deleteHolderMarketFactMovementRenewal(lstHolderAccountPk);
			
			sbQuery.append(" Delete HolderAccountMovement ham  ");
			sbQuery.append(" where ham.custodyOperation.idCustodyOperationPk = :parIdsecuritiesRenewal");
			sbQuery.append(" and ham.operationNumber = :parOperationNumber");
			sbQuery.append(" and trunc(ham.operationDate) = trunc(:parOperationDate)");
			sbQuery.append(" and trunc(ham.movementDate) = trunc(:parMovementDate)");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdsecuritiesRenewal", objCustodyOperation.getIdCustodyOperationPk());
			query.setParameter("parOperationNumber", objCustodyOperation.getOperationNumber());
			query.setParameter("parOperationDate", objCustodyOperation.getOperationDate());
			query.setParameter("parMovementDate", objCustodyOperation.getOperationDate());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete CustodyOperation.
	 *
	 * @param objCustodyOperation CustodyOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteCustodyOperationRenewal(CustodyOperation objCustodyOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objCustodyOperation.getIdCustodyOperationPk())){
			sbQuery.append(" Delete CustodyOperation cu  ");
			sbQuery.append(" where cu.idCustodyOperationPk = :parIdCustodyOperation ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdCustodyOperation", objCustodyOperation.getIdCustodyOperationPk());
			query.executeUpdate();
		}		
	}
	
	/**
	 * The method is used to delete Holder Account Balance.
	 *
	 * @param objHolderAccountBalance HolderAccountBalance
	 * @throws ServiceException the service exception
	 */
	public void deleteHolderAccountBalanceTarget(HolderAccountBalance objHolderAccountBalance) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk())
				&& Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getSecurity().getIdSecurityCodePk())
				&& Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getParticipant().getIdParticipantPk())){
			sbQuery.append(" Delete HolderAccountBalance habal  ");
			sbQuery.append(" where habal.holderAccount.idHolderAccountPk = :parIdHolderAccount ");
			sbQuery.append(" and habal.participant.idParticipantPk = :parIdParticipant   ");	
			sbQuery.append(" and habal.security.idSecurityCodePk = :parIdSecurityCode  ");	
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdHolderAccount", objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
			query.setParameter("parIdParticipant", objHolderAccountBalance.getParticipant().getIdParticipantPk());
			query.setParameter("parIdSecurityCode", objHolderAccountBalance.getSecurity().getIdSecurityCodePk());
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete Holder Account MarketFact Balance.
	 *
	 * @param objHolderAccountBalance HolderAccountBalance
	 * @throws ServiceException the service exception
	 */
	public void deleteHolderMarketFactBalanceTarget(HolderAccountBalance objHolderAccountBalance) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk())
				&& Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getSecurity().getIdSecurityCodePk())
				&& Validations.validateIsNotNullAndNotEmpty(objHolderAccountBalance.getParticipant().getIdParticipantPk())){
			sbQuery.append(" Delete HolderMarketFactBalance habal  ");
			sbQuery.append(" where habal.holderAccount.idHolderAccountPk = :parIdHolderAccount ");
			sbQuery.append(" and habal.participant.idParticipantPk = :parIdParticipant   ");	
			sbQuery.append(" and habal.security.idSecurityCodePk = :parIdSecurityCode  ");	
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdHolderAccount", objHolderAccountBalance.getHolderAccount().getIdHolderAccountPk());
			query.setParameter("parIdParticipant", objHolderAccountBalance.getParticipant().getIdParticipantPk());
			query.setParameter("parIdSecurityCode", objHolderAccountBalance.getSecurity().getIdSecurityCodePk());
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete HolderMarketFactMovement.
	 *
	 * @param lstHolderAccountPk the lst holder account pk
	 * @throws ServiceException the service exception
	 */
	public void deleteHolderMarketFactMovementRenewal(List<Long> lstHolderAccountPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		if(Validations.validateListIsNotNullAndNotEmpty(lstHolderAccountPk)){
			sbQuery.append(" Delete HolderMarketFactMovement hmfm  ");
			sbQuery.append(" where hmfm.holderAccountMovement.idHolderAccountMovementPk in (:lstHolderAccountPk) ");	
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("lstHolderAccountPk", lstHolderAccountPk);			
			query.executeUpdate();
		}					
	}
	
	/**
	 * The method is used to delete list deletePaymentChronogram.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteSecurityAndPaymentChronogramTarget(Security objSecurity) throws ServiceException {		
		Object[] arrSecurityDetail = getSecurityDetails(objSecurity.getIdSecurityCodePk());
		Integer securityState = (Integer)arrSecurityDetail[0];		
		if(securityState.equals(SecurityStateType.REGISTERED.getCode())){			
			List<Long> lstIdProgramInterestCoupon= new ArrayList<Long>();
			List<Long> lstIdProgramAmortizationCoupon= new ArrayList<Long>();
			List<Long> lstIdCorporativeOperation= new ArrayList<Long>();			
			List<CorporativeOperation> lstCorporativeOperation= getListCorporativeOperation(objSecurity.getIdSecurityCodePk());			
			if (Validations.validateListIsNotNullAndNotEmpty(lstCorporativeOperation)) {
				for (CorporativeOperation objCorporativeOperation: lstCorporativeOperation) {
					lstIdCorporativeOperation.add(objCorporativeOperation.getIdCorporativeOperationPk());
					if (Validations.validateIsNotNull(objCorporativeOperation.getProgramAmortizationCoupon())) {
						lstIdProgramAmortizationCoupon.add(objCorporativeOperation.getProgramAmortizationCoupon().getIdProgramAmortizationPk());
					} else if (Validations.validateIsNotNull(objCorporativeOperation.getProgramInterestCoupon())) {
						lstIdProgramInterestCoupon.add(objCorporativeOperation.getProgramInterestCoupon().getIdProgramInterestPk());
					}
				}
			}		
			deleteCorporativeOperationSecurityTarget(lstIdCorporativeOperation);
			deleteProgramInterestCouponSecurityTarget(lstIdProgramInterestCoupon);
			deleteInterestPaymentSchedule(objSecurity);
			deleteProgramAmortizationCouponSecurityTarget(lstIdProgramAmortizationCoupon);		
			deleteAmortizationPaymentSchedule(objSecurity);
			deleteIssuanceSecurityTarget(objSecurity);
			deleteSecurityNegotiationMechanismTarget(objSecurity);
			deleteSecurityInvestor(objSecurity);
			deleteSecuritySecurityTarget(objSecurity);
		}
	}
	
	/**
	 * The method is used to delete list ProgramAmortizationCoupon.
	 *
	 * @param lstIdCorporativeOperation the lst id corporative operation
	 * @throws ServiceException the service exception
	 */
	public void deleteCorporativeOperationSecurityTarget(List<Long> lstIdCorporativeOperation) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdCorporativeOperation)){
			sbQuery.append(" Delete CorporativeOperation co  ");
			sbQuery.append(" where co.idCorporativeOperationPk in (:lstIdCorporativeOperation)");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("lstIdCorporativeOperation", lstIdCorporativeOperation);			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete security investor.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteSecurityInvestor(Security objSecurity) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecurity.getIdSecurityCodePk())){
			sbQuery.append(" Delete SecurityInvestor sinv  ");
			sbQuery.append(" where sinv.security.idSecurityCodePk = :parIdSecurityCode");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecurityCode", objSecurity.getIdSecurityCodePk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete list ProgramInterestCoupon.
	 *
	 * @param lstIdProgramInterestCoupon List<Long>
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramInterestCouponSecurityTarget(List<Long> lstIdProgramInterestCoupon) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdProgramInterestCoupon)){
			sbQuery.append(" Delete ProgramInterestCoupon pic  ");
			sbQuery.append(" where pic.idProgramInterestPk in (:lstProgramInterestCoupon)");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("lstProgramInterestCoupon", lstIdProgramInterestCoupon);			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete list ProgramAmortizationCoupon.
	 *
	 * @param lstIdProgramAmortizationCoupon List<Long>
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramAmortizationCouponSecurityTarget(List<Long> lstIdProgramAmortizationCoupon) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateListIsNotNullAndNotEmpty(lstIdProgramAmortizationCoupon)){
			sbQuery.append(" Delete ProgramAmortizationCoupon pac  ");
			sbQuery.append(" where pac.idProgramAmortizationPk in (:lstProgramAmortizationCoupon)");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("lstProgramAmortizationCoupon", lstIdProgramAmortizationCoupon);			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete Issuance.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteIssuanceSecurityTarget(Security objSecurity) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecurity.getIssuance().getIdIssuanceCodePk())){
			sbQuery.append(" Delete Issuance issu  ");
			sbQuery.append(" where issu.idIssuanceCodePk = :parIdIssuanceCode ");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdIssuanceCode", objSecurity.getIssuance().getIdIssuanceCodePk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete SecurityNegotiationMechanism.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteSecurityNegotiationMechanismTarget(Security objSecurity) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecurity.getIdSecurityCodePk())){
			sbQuery.append(" Delete SecurityNegotiationMechanism snm  ");
			sbQuery.append(" where snm.security.idSecurityCodePk = :parIdSecurityCode");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecurityCode", objSecurity.getIdSecurityCodePk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete Security.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritySecurityTarget(Security objSecurity) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecurity.getIdSecurityCodePk())){
			sbQuery.append(" Delete Security sec  ");
			sbQuery.append(" where sec.idSecurityCodePk = :parIdSecurityCode");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecurityCode", objSecurity.getIdSecurityCodePk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * The method is used to delete Interest Payment Schedule.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteInterestPaymentSchedule(Security objSecurity) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecurity.getIdSecurityCodePk())){
			sbQuery.append(" Delete InterestPaymentSchedule ips  ");
			sbQuery.append(" where ips.security.idSecurityCodePk = :parIdSecurityCode");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecurityCode", objSecurity.getIdSecurityCodePk());			
			query.executeUpdate();
		}
	}
		
	/**
	 * The method is used to delete Amortization Payment Schedule.
	 *
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteAmortizationPaymentSchedule(Security objSecurity) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();		
		if(Validations.validateIsNotNullAndNotEmpty(objSecurity.getIdSecurityCodePk())){
			sbQuery.append(" Delete AmortizationPaymentSchedule aps  ");
			sbQuery.append(" where aps.security.idSecurityCodePk = :parIdSecurityCode");			
			Query query = em.createQuery(sbQuery.toString());	
			query.setParameter("parIdSecurityCode", objSecurity.getIdSecurityCodePk());			
			query.executeUpdate();
		}
	}
	
	/**
	 * Gets object security details.
	 *
	 * @param idSecurityCode the security code
	 * @return object security details
	 */
	public Object[] getSecurityDetails(String idSecurityCode) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT sec.stateSecurity, sec.securityClass, 		");
		stringBuffer.append(" isu.idIssuanceCodePk 								");
		stringBuffer.append(" FROM Security sec 								");
		stringBuffer.append(" inner join sec.issuance isu						");
		stringBuffer.append(" WHERE sec.idSecurityCodePk = :idSecurityCode 		");		
		Query typedQuery= em.createQuery(stringBuffer.toString());
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		Object[] resultSecurity = (Object[]) typedQuery.getSingleResult();
		return resultSecurity;		
	}
	
	/**
	 * Gets list corporative operation.
	 *
	 * @param idSecurityCode the security code
	 * @return list corporative operation
	 */
	public List<CorporativeOperation> getListCorporativeOperation(String idSecurityCode) {
		List<CorporativeOperation> lstCorporativeOperation= null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT CO ");
		stringBuffer.append(" FROM CorporativeOperation CO ");
		stringBuffer.append(" INNER JOIN FETCH CO.corporativeEventType ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programInterestCoupon ");
		stringBuffer.append(" LEFT JOIN FETCH CO.programAmortizationCoupon ");
		stringBuffer.append(" WHERE CO.securities.idSecurityCodePk = :idSecurityCode ");
		stringBuffer.append(" AND CO.state in (:lstState) ");		
		TypedQuery<CorporativeOperation> typedQuery= em.createQuery(stringBuffer.toString(),CorporativeOperation.class);
		typedQuery.setParameter("idSecurityCode", idSecurityCode);
		List<Integer> lstState= new ArrayList<Integer>();
		lstState.add(CorporateProcessStateType.REGISTERED.getCode());
		lstState.add(CorporateProcessStateType.MODIFIED.getCode());
		typedQuery.setParameter("lstState", lstState);		
		lstCorporativeOperation= typedQuery.getResultList();
		return lstCorporativeOperation;
	}
	
	/**
	 * The method is used to update issuance source.
	 *
	 * @param objCustodyOperation the obj custody operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void updateInterfaceTransaction(CustodyOperation objCustodyOperation, LoggerUser loggerUser) throws ServiceException {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuffer query = new StringBuffer(" Update InterfaceTransaction interface set  "+				
				" interface.custodyOperation.idCustodyOperationPk = :parCustody , " +
				" interface.security.idSecurityCodePk = :parCustody , " +				
				" interface.lastModifyUser = :parLastModifyUser , interface.lastModifyDate = :parLastModifyDate , " +
				" interface.lastModifyIp = :parLastModifyIp , interface.lastModifyApp = :parLastModifyApp " +
				" where interface.idInterfaceTransactionPk in (" +
				" select int.idInterfaceTransactionPk from InterfaceTransaction int where int.custodyOperation.idCustodyOperationPk = :parIdCustodyOperation) ");
		parameters.put("parIdCustodyOperation", objCustodyOperation.getIdCustodyOperationPk());		
		parameters.put("parCustody", null);				
		parameters.put("parLastModifyUser", loggerUser.getUserName());
		parameters.put("parLastModifyDate", loggerUser.getAuditTime());
		parameters.put("parLastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		parameters.put("parLastModifyIp", loggerUser.getIpAddress());
		updateByQuery(query.toString(), parameters);
	}
	
	/**
	 * The method is used to delete SecuritiesMovement and SecurityOperation.
	 *
	 * @param objCorporativeOperation the obj corporative operation
	 * @param objSecurity Security
	 * @throws ServiceException the service exception
	 */
	public void deleteSecuritiesMovementAndRenewalCorporative(CorporativeOperation objCorporativeOperation, Security objSecurity) throws ServiceException{		
		List<SecuritiesOperation> lstSecuritiesOperation = getListSecuritiesOperation(objSecurity, null, objCorporativeOperation);
		if(Validations.validateListIsNotNullAndNotEmpty(lstSecuritiesOperation)){
			for(SecuritiesOperation objSecuritiesOperation : lstSecuritiesOperation){					
				deleteIssuanceMovementAndIssuanceOperationRenewal(objSecuritiesOperation, objSecurity);
				deleteSecuritiesMovementRenewal(objSecuritiesOperation);
				deleteSecuritiesOperationRenewal(objSecuritiesOperation);
			}
		}		
	}
	
	/**
	 * The method is used to delete HolderAccountMovement.
	 *
	 * @param objCorporativeOperation CorporativeOperation
	 * @throws ServiceException the service exception
	 */
	public void deleteHolderAccountMovementRenewal(CorporativeOperation objCorporativeOperation) throws ServiceException {
		StringBuilder sbQuery=new StringBuilder();
		if(objCorporativeOperation != null && 
				Validations.validateIsNotNullAndNotEmpty(objCorporativeOperation.getIdCorporativeOperationPk()) ){
			
			List<Long> lstHolderAccountPk = geHolderAccountMovementForMarkeFact(objCorporativeOperation);				
			deleteHolderMarketFactMovementRenewal(lstHolderAccountPk);
			
			sbQuery.append(" delete HolderAccountMovement ham  ");
			sbQuery.append(" where ham.corporativeOperation.idCorporativeOperationPk = :parIdsecuritiesRenewal ");			
			sbQuery.append(" and trunc(ham.movementDate) = trunc(:parMovementDate) ");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("parIdsecuritiesRenewal", objCorporativeOperation.getIdCorporativeOperationPk());			
			query.setParameter("parMovementDate", CommonsUtilities.currentDate());
			query.executeUpdate();
		}		
	}
	
	
	/**
	 * The method is used to get list HolderAccountMovement.
	 *
	 * @param objCustodyOperation CustodyOperation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> geHolderAccountMovementForMarkeFact(CustodyOperation objCustodyOperation) throws ServiceException{		
		List<Long> lstHolderAccountPks = null;		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" SELECT ham.idHolderAccountMovementPk FROM HolderAccountMovement ham						");				
		jpqlQuery.append(" where ham.custodyOperation.idCustodyOperationPk = :parIdsecuritiesRenewal				");
		jpqlQuery.append(" and ham.operationNumber = :parOperationNumber											");
		jpqlQuery.append(" and trunc(ham.operationDate) = trunc(:parOperationDate)									");
		jpqlQuery.append(" and trunc(ham.movementDate) = trunc(:parMovementDate)									");
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("parIdsecuritiesRenewal", objCustodyOperation.getIdCustodyOperationPk());
		query.setParameter("parOperationNumber", objCustodyOperation.getOperationNumber());
		query.setParameter("parOperationDate", objCustodyOperation.getOperationDate());
		query.setParameter("parMovementDate", objCustodyOperation.getOperationDate());						
		try {			
			List<Object> resultList = query.getResultList();	
			lstHolderAccountPks = new ArrayList<Long>();
			int i = 0;
			while (i < resultList.size()) {
				Object object = resultList.get(i);
				lstHolderAccountPks.add((Long) object);
				i++;
			}
		} catch(NoResultException ex) {
			return null;
		}		
		return lstHolderAccountPks;
	}
	
	/**
	 * The method is used to get list HolderAccountMovement.
	 *
	 * @param objCorporativeOperation CorporativeOperation
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<Long> geHolderAccountMovementForMarkeFact(CorporativeOperation objCorporativeOperation) throws ServiceException{		
		List<Long> lstHolderAccountPks = null;		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" SELECT ham.idHolderAccountMovementPk FROM HolderAccountMovement ham						");				
		jpqlQuery.append(" where ham.corporativeOperation.idCorporativeOperationPk = :parIdsecuritiesRenewal ");			
		jpqlQuery.append(" and trunc(ham.movementDate) = trunc(:parMovementDate) ");
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("parIdsecuritiesRenewal", objCorporativeOperation.getIdCorporativeOperationPk());			
		query.setParameter("parMovementDate", CommonsUtilities.currentDate());								
		try {			
			List<Object> resultList = query.getResultList();	
			lstHolderAccountPks = new ArrayList<Long>();
			int i = 0;
			while (i < resultList.size()) {
				Object object = resultList.get(i);
				lstHolderAccountPks.add((Long) object);
				i++;
			}
		} catch(NoResultException ex) {
			return null;
		}		
		return lstHolderAccountPks;
	}
	
}
