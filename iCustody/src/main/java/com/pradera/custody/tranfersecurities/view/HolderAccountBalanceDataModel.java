package com.pradera.custody.tranfersecurities.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.component.HolderAccountBalance;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountBalanceDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class HolderAccountBalanceDataModel extends ListDataModel<HolderAccountBalance> implements SelectableDataModel<HolderAccountBalance>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new holder account balance data model.
	 */
	public HolderAccountBalanceDataModel() {
			
	}
	
	/**
	 * Instantiates a new holder account balance data model.
	 *
	 * @param lstHolderAccountBalance the lst holder account balance
	 */
	public HolderAccountBalanceDataModel(List<HolderAccountBalance> lstHolderAccountBalance) {
		super(lstHolderAccountBalance);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public HolderAccountBalance getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<HolderAccountBalance> ListHolderAccountBalance = (List<HolderAccountBalance>)getWrappedData();
		for(HolderAccountBalance holderAccountBalance : ListHolderAccountBalance){
			if(holderAccountBalance.getId().toString().equals(rowKey))
				return holderAccountBalance;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(HolderAccountBalance holderAccountBalance) {//Return Object
		// TODO Auto-generated method stub
		return holderAccountBalance.getId();
	}

}
