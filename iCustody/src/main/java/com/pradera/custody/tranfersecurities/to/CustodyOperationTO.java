package com.pradera.custody.tranfersecurities.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CustodyOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class CustodyOperationTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id custody operation pk. */
	private Long idCustodyOperationPk;
	
    /** The approval date. */
	private Date approvalDate;

	/** The approval user. */
	private String approvalUser;
	
	/** The application date. */
	private Date applicationDate;
	
	/** The application user. */
	private String applicationUser;
	
	/** The authorization date. */
	private Date authorizationDate;
	
	/** The authorization user. */
	private String authorizationUser;

    /** The confirm date. */
	private Date confirmDate;

	/** The confirm user. */
	private String confirmUser;

    /** The operation date. */
	private Date operationDate;
    
    /** The operation date. */
	private Date operationInit;
    
    /** The operation date. */
	private Date operationEnd;

	/** The operation number. */
	private Long operationNumber;

	/** The operation type. */
	private Long operationType;

    /** The registry date. */
	private Date registryDate;

	/** The registry user. */
	private String registryUser;

    /** The reject date. */
	private Date rejectDate;

    /** The reject motive. */
	private Integer rejectMotive;

	/** The reject motive other. */
	private String rejectMotiveOther;

	/** The reject user. */
	private String rejectUser;

    /** The review date. */
	private Date reviewDate;

	/** The review user. */
	private String reviewUser;

	/** The state. */
	private Integer state;
	
	/** The ind rectification. */
	private Long indRectification; 
	
    /**
     * Instantiates a new custody operation.
     */
    public CustodyOperationTO() {}
    
    
    
    
    /**
     * Instantiates a new custody operation.
     * @param idCustodyOperationPk the new id custody operation pk
     */
    public CustodyOperationTO(Long idCustodyOperationPk){
    	super();
    	this.idCustodyOperationPk=idCustodyOperationPk;
    }

	/**
	 * Instantiates a new custody operation to.
	 *
	 * @param rejectMotive the reject motive
	 * @param rejectMotiveOther the reject motive other
	 */
	public CustodyOperationTO(Integer rejectMotive, String rejectMotiveOther) {
		super();
		this.rejectMotive = rejectMotive;
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the id custody operation pk.
	 *
	 * @return the id custody operation pk
	 */
	public Long getIdCustodyOperationPk() {
		return this.idCustodyOperationPk;
	}

	/**
	 * Sets the id custody operation pk.
	 *
	 * @param idCustodyOperationPk the new id custody operation pk
	 */
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}

	/**
	 * Gets the approval date.
	 *
	 * @return the approval date
	 */
	public Date getApprovalDate() {
		return this.approvalDate;
	}

	/**
	 * Sets the approval date.
	 *
	 * @param approvalDate the new approval date
	 */
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	/**
	 * Gets the approval user.
	 *
	 * @return the approval user
	 */
	public String getApprovalUser() {
		return this.approvalUser;
	}

	/**
	 * Sets the approval user.
	 *
	 * @param approvalUser the new approval user
	 */
	public void setApprovalUser(String approvalUser) {
		this.approvalUser = approvalUser;
	}

	/**
	 * Gets the confirm date.
	 *
	 * @return the confirm date
	 */
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * Sets the confirm date.
	 *
	 * @param confirmDate the new confirm date
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * Gets the confirm user.
	 *
	 * @return the confirm user
	 */
	public String getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * Sets the confirm user.
	 *
	 * @param confirmUser the new confirm user
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return this.operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return this.operationNumber;
	}

	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}

	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return this.operationType;
	}

	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}

	/**
	 * Gets the registry date.
	 *
	 * @return the registry date
	 */
	public Date getRegistryDate() {
		return this.registryDate;
	}

	/**
	 * Sets the registry date.
	 *
	 * @param registryDate the new registry date
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}

	/**
	 * Gets the registry user.
	 *
	 * @return the registry user
	 */
	public String getRegistryUser() {
		return this.registryUser;
	}

	/**
	 * Sets the registry user.
	 *
	 * @param registryUser the new registry user
	 */
	public void setRegistryUser(String registryUser) {
		this.registryUser = registryUser;
	}

	/**
	 * Gets the reject date.
	 *
	 * @return the reject date
	 */
	public Date getRejectDate() {
		return this.rejectDate;
	}

	/**
	 * Sets the reject date.
	 *
	 * @param rejectDate the new reject date
	 */
	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	/**
	 * Gets the reject motive.
	 *
	 * @return the reject motive
	 */
	public Integer getRejectMotive() {
		return this.rejectMotive;
	}

	/**
	 * Sets the reject motive.
	 *
	 * @param rejectMotive the new reject motive
	 */
	public void setRejectMotive(Integer rejectMotive) {
		this.rejectMotive = rejectMotive;
	}

	/**
	 * Gets the reject motive other.
	 *
	 * @return the reject motive other
	 */
	public String getRejectMotiveOther() {
		return this.rejectMotiveOther;
	}

	/**
	 * Sets the reject motive other.
	 *
	 * @param rejectMotiveOther the new reject motive other
	 */
	public void setRejectMotiveOther(String rejectMotiveOther) {
		this.rejectMotiveOther = rejectMotiveOther;
	}

	/**
	 * Gets the reject user.
	 *
	 * @return the reject user
	 */
	public String getRejectUser() {
		return this.rejectUser;
	}

	/**
	 * Sets the reject user.
	 *
	 * @param rejectUser the new reject user
	 */
	public void setRejectUser(String rejectUser) {
		this.rejectUser = rejectUser;
	}

	/**
	 * Gets the review date.
	 *
	 * @return the review date
	 */
	public Date getReviewDate() {
		return this.reviewDate;
	}

	/**
	 * Sets the review date.
	 *
	 * @param reviewDate the new review date
	 */
	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	/**
	 * Gets the review user.
	 *
	 * @return the review user
	 */
	public String getReviewUser() {
		return this.reviewUser;
	}

	/**
	 * Sets the review user.
	 *
	 * @param reviewUser the new review user
	 */
	public void setReviewUser(String reviewUser) {
		this.reviewUser = reviewUser;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return this.state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	
	/**
	 * Gets the application date.
	 *
	 * @return the application date
	 */
	public Date getApplicationDate() {
		return applicationDate;
	}

	/**
	 * Sets the application date.
	 *
	 * @param applicationDate the new application date
	 */
	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * Gets the application user.
	 *
	 * @return the application user
	 */
	public String getApplicationUser() {
		return applicationUser;
	}

	/**
	 * Sets the application user.
	 *
	 * @param applicationUser the new application user
	 */
	public void setApplicationUser(String applicationUser) {
		this.applicationUser = applicationUser;
	}

	/**
	 * Gets the authorization date.
	 *
	 * @return the authorization date
	 */
	public Date getAuthorizationDate() {
		return authorizationDate;
	}

	/**
	 * Sets the authorization date.
	 *
	 * @param authorizationDate the new authorization date
	 */
	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	/**
	 * Gets the authorization user.
	 *
	 * @return the authorization user
	 */
	public String getAuthorizationUser() {
		return authorizationUser;
	}

	/**
	 * Sets the authorization user.
	 *
	 * @param authorizationUser the new authorization user
	 */
	public void setAuthorizationUser(String authorizationUser) {
		this.authorizationUser = authorizationUser;
	}

	/**
	 * Gets the ind rectification.
	 *
	 * @return the ind rectification
	 */
	public Long getIndRectification() {
		return indRectification;
	}

	/**
	 * Sets the ind rectification.
	 *
	 * @param indRectification the new ind rectification
	 */
	public void setIndRectification(Long indRectification) {
		this.indRectification = indRectification;
	}

	/**
	 * Gets the operation init.
	 *
	 * @return the operation init
	 */
	public Date getOperationInit() {
		return operationInit;
	}

	/**
	 * Sets the operation init.
	 *
	 * @param operationInit the new operation init
	 */
	public void setOperationInit(Date operationInit) {
		this.operationInit = operationInit;
	}

	/**
	 * Gets the operation end.
	 *
	 * @return the operation end
	 */
	public Date getOperationEnd() {
		return operationEnd;
	}

	/**
	 * Sets the operation end.
	 *
	 * @param operationEnd the new operation end
	 */
	public void setOperationEnd(Date operationEnd) {
		this.operationEnd = operationEnd;
	}
	
}
