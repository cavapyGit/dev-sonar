package com.pradera.custody.tranfersecurities.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;
import com.pradera.custody.ownershipexchange.facade.ChangeOwnershipServiceFacade;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipOperationTO;
import com.pradera.custody.ownershipexchange.to.ChangeOwnershipTransferTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.changeownership.ChangeOwnershipMarketFact;
import com.pradera.model.custody.changeownership.ChangeOwnershipOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class AffectationCancelBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@BatchProcess(name = "ChangeOwnerShipOperationBatch")
@RequestScoped
public class ChangeOwnerShipOperationBatch implements JobExecution, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The request affectation service facade. */
	//@EJB
	//private RequestAffectationServiceFacade requestAffectationServiceFacade;
	
	/** The change ownership service facade. */
	@EJB
	ChangeOwnershipServiceFacade changeOwnershipServiceFacade;
	
	/** The holder account balances data model. */
	private GenericDataModel<HolderAccountBalance> holderAccountBalancesDataModel;
	
	/** The selected holder account balance. */
	private HolderAccountBalance selectedHolderAccountBalance; // for availables
	
	/** The current detail list. */
	private List<ChangeOwnershipTransferTO> currentDetailList;
	
	/** The change ownership operation to. */
	private ChangeOwnershipOperationTO changeOwnershipOperationTO;	

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info(getClass().getName() + " being executed.");
		try {
			
			Long idHolderAccountPk=0L;
			//obteniendo los parametros para obter las lista de valores que se quiere transferiri a otro titular
			Map<String, Object> parametersToQuery = new HashMap<String, Object>();
			for(ProcessLoggerDetail processLoggerDetail: processLogger.getProcessLoggerDetails()){
				/*** GETTING PARAMETERS ***/
				if(processLoggerDetail.getParameterName().equals("idParticipantPk"))
					parametersToQuery.put("idParticipantPk", processLoggerDetail.getParameterValue());
				else if(processLoggerDetail.getParameterName().equals("idHolderAccountPk")){
					parametersToQuery.put("idHolderAccountPk", processLoggerDetail.getParameterValue());
				}else if(processLoggerDetail.getParameterName().equals("idIsinCodePk")){
					parametersToQuery.put("idIsinCodePk", processLoggerDetail.getParameterValue());
				}else if(processLoggerDetail.getParameterName().equals("securityClass")){
					parametersToQuery.put("securityClass", processLoggerDetail.getParameterValue());
				}else if(processLoggerDetail.getParameterName().equals("targetAccount")){
					idHolderAccountPk=Long.parseLong(processLoggerDetail.getParameterValue());
				}
			}
			
			
			ChangeOwnershipOperation changeOwnershipOperation;
			Integer indMarketFact;//idepositarySetup.getIndMarketFact()
			
			//changeOwnershipServiceFacade.saveAvailChangeOwnerShipServiceFacade(changeOwnershipOperation,idepositarySetup.getIndMarketFact());
			
			//changeOwnershipServiceFacade.saveBlockChangeOwnerShipServiceFacade(changeOwnershipOperation);
			
			//requestAffectationServiceFacade.automaticAffectationCancelations(currentDate);
			
			throw new ServiceException();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Transfer all.
	 */
	public void transferAll(){
		
		selectedHolderAccountBalance = null;
		changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
		
		for(HolderAccountBalance hab: holderAccountBalancesDataModel.getDataList()){
			selectedHolderAccountBalance = hab;
			currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();
			selectedHolderAccountBalance.setCopiedAvailableBalance(selectedHolderAccountBalance.getAvailableBalance());
			
			// - si es sucesion, y tiene accreditation o transito
//			if(!isValidAvailSingle(hab)){
//				selectedHolderAccountBalance = null;
//				changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
//				currentDetailList = new ArrayList<ChangeOwnershipTransferTO>();
//				return;
//			}
			
			if(selectedHolderAccountBalance.getCopiedAvailableBalance().compareTo(BigDecimal.ZERO) > 0 ){
				
				// - solo va a ser a un solo titular destino.
				for(HolderAccount targetHolderAccount : changeOwnershipOperationTO.getTargetHolderAccounts()){
					
					ChangeOwnershipTransferTO detail = new ChangeOwnershipTransferTO();
					detail.setHolderAccount(targetHolderAccount);
					detail.setParticipant(targetHolderAccount.getParticipant());
					detail.setSecurity(selectedHolderAccountBalance.getSecurity());
					detail.setQuantityBalance(selectedHolderAccountBalance.getCopiedAvailableBalance());
					detail.setSave(true);
					
					List<HolderMarketFactBalance> mktBalances = changeOwnershipServiceFacade.getMarketFactBalances(
							selectedHolderAccountBalance.getParticipant().getIdParticipantPk(),
							selectedHolderAccountBalance.getHolderAccount().getIdHolderAccountPk(),
							selectedHolderAccountBalance.getSecurity().getIdSecurityCodePk());
					
					BigDecimal totalMktBalance = BigDecimal.ZERO;
					for(HolderMarketFactBalance holderMarketFactBalance : mktBalances ){
						ChangeOwnershipMarketFact marketFactDetail = new ChangeOwnershipMarketFact();
						marketFactDetail.setMarketDate(holderMarketFactBalance.getMarketDate());
						marketFactDetail.setMarketPrice(holderMarketFactBalance.getMarketPrice());
						marketFactDetail.setMarketRate(holderMarketFactBalance.getMarketRate());
						marketFactDetail.setMarketBalance(holderMarketFactBalance.getAvailableBalance());
						totalMktBalance = totalMktBalance.add(marketFactDetail.getMarketBalance());
						detail.getChangeOwnershipMarketFact().add(marketFactDetail);				
					}
					
					if(totalMktBalance.compareTo(detail.getQuantityBalance()) != 0){
//						showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
//								"Inconsistencia entre saldos y hechos de mercado. Valor: "+selectedHolderAccountBalance.getSecurity().getIdSecurityCodePk());
//						JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID);
//						changeOwnershipOperationTO.setChangeOwnershipDetails(new ArrayList<ChangeOwnershipDetail>(0));
						return;
					}
					
					currentDetailList.add(detail);
				}
				
				//saveCurrentTransfer(true);
			}
		}
		
//		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
//				PropertiesUtilities.getMessage(PropertiesConstants.MESSAGE_CHANGE_OWNERSHIP_ALL_TRANSFERED_OK));
//		JSFUtilities.showComponent(GENERIC_WARNING_DIALOG_ID); 
//		allTransfered = true;
		
	}
	
	//TODO (esto es una copia fiel de class:ChangeOwnershipBean->listHolderAccountBalances) issue980
	/**
	 * List holder account balances. 
	 * @param filters the filters
	 * @param mapParameters the map parameters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> listHolderAccountBalances(Map<String, Object> filters
			//,HashMap<Integer, ParameterTable> mapParameters
			) throws ServiceException {
		
		List<HolderAccountBalance> balances = new ArrayList<HolderAccountBalance>();
		List<Object[]> result = changeOwnershipServiceFacade.listHolderAccountBalancesServiceFacade(filters);
		
		for (Object[] balObject : result) {
			
			HolderAccountBalance holderAccountBalance =  (HolderAccountBalance) balObject[0];
			Security security = new Security((String) balObject[1]);
			security.setDescription((String) balObject[2]);
			security.setSecurityClass((Integer) balObject[3]);
			security.setCfiCode((String) balObject[4]);
			security.setIdIsinCode((String) balObject[5]);
			
//			ParameterTable securityClass = mapParameters.get(security.getSecurityClass());
//			if(securityClass!=null){
//				security.setSecurityClassDesc(securityClass.getText1());
//			}
			
			holderAccountBalance.setSecurity(security);
			holderAccountBalance.setPendingSellBalance((BigDecimal)balObject[6]);
			
			BigDecimal availableToTransfer = holderAccountBalance.getAvailableBalance().subtract(holderAccountBalance.getPendingSellBalance());
			
			if(availableToTransfer.compareTo(BigDecimal.ZERO) > 0){
				holderAccountBalance.setCopiedAvailableBalance(availableToTransfer);
			}else{
				holderAccountBalance.setCopiedAvailableBalance(BigDecimal.ZERO);
			}
			
			balances.add(holderAccountBalance);
		}
		return balances;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
