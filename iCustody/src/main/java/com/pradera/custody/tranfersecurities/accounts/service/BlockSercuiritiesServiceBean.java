package com.pradera.custody.tranfersecurities.accounts.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.tranfersecurities.to.SecurityBlockTransferTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.custody.affectation.type.AffectationStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class BlockSercuiritiesServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
public class BlockSercuiritiesServiceBean extends CrudDaoServiceBean{

	/** The logger. */
	@Inject  
	PraderaLogger logger;
	
	/**
	 * Search document numbers with operation.
	 *
	 * @param filters the filters
	 * @param onlyread the onlyread
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityBlockTransferTO> searchDocumentNumbersWithOperation(SecurityBlockTransferTO filters, boolean onlyread) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		 sbQuery.append(" select "+
						"	bo.documentNumber, "+
						"	bo.blockState, " +
						"	bo.actualBlockBalance "+
						" from"+
						"       BlockOperationTransfer bot "+
						"   join "+
						//"           on bot.securityTransferOperation.custodyOperation.idCustodyOperationPk=sto.custodyOperation.idCustodyOperationPk"+
						"       bot.securityTransferOperation sto "+ 
						"   join "+
						"       sto.custodyOperation co "+ 
						//"           on sto.custodyOperation.idCustodyOperationPk=co.idCustodyOperationPk"+
						//"   join"+
						//"       BlockOperationTransfer bot"+
						//"           on bot.securityTransferOperation.custodyOperation.idCustodyOperationPk=sto.custodyOperation.idCustodyOperationPk"+
						"   join "+
						"       bot.blockOperation bo "); 
						//"           on bot.bo.idBlockOperationPk=bo.idBlockOperationPk");
		
		sbQuery.append(" Where 1 = 1 ");
		
		if(onlyread==false){
			sbQuery.append(" AND bo.blockState IN ("+AffectationStateType.BLOCKED.getCode()+") ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filters.getOperationNumber()))
			sbQuery.append(" AND co.operationNumber = :operationNumberPrm ");

		Query query = em.createQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndPositive(filters.getOperationNumber()))
			query.setParameter("operationNumberPrm", filters.getOperationNumber());
		
		List<SecurityBlockTransferTO> lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		SecurityBlockTransferTO securityBlockTo = null;

		List<Object> objectList = query.getResultList();
		for (int i = 0; i < objectList.size(); i++) {

			Object obj[] = (Object[])objectList.get(i);
			securityBlockTo = new SecurityBlockTransferTO();
			
			if(Validations.validateIsNotNull(obj[0]))
				securityBlockTo.setDocumentNumber((Long)obj[0]);//

			if(Validations.validateIsNotNull(obj[1]))
				securityBlockTo.setState((Integer)obj[1]);//
			
			if(Validations.validateIsNotNull(obj[2]))
				securityBlockTo.setActualBlockBalance((BigDecimal)obj[2]);//
			
			lstSecurityBlockTransferTO.add(securityBlockTo);
		}
		if (lstSecurityBlockTransferTO.size()>0){
			return lstSecurityBlockTransferTO;
		}else{
			lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
			return lstSecurityBlockTransferTO;
		}
	}

	
	/**
	 * Search solicitud security block transfer to.
	 *
	 * @param filters the filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityBlockTransferTO> searchSolicitudSecurityBlockTransferTo(SecurityBlockTransferTO filters) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();	
		 sbQuery.append("select "+
		" co.OPERATION_NUMBER,"+//0
		" co.OPERATION_TYPE,"+//1
		" co.OPERATION_DATE,"+//2
		" br.ID_BLOCK_REQUEST_PK,"+//3
		" br.BLOCK_TYPE,"+//4
		" (select pt.description from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK=br.BLOCK_TYPE) as block_description,"+//5
		"  sto.ID_SECURITY_CODE_FK,"+//6
		"  sto.QUANTITY_OPERATION,"+//7
		"  sto.STATE,"+//8
		"  sto.ID_SOURCE_PARTICIPANT_FK,"+//9
		" (select pa.DESCRIPTION from Participant pa where pa.ID_PARTICIPANT_PK=sto.ID_SOURCE_PARTICIPANT_FK) as part_source_description,"+//10
		"  sto.ID_TARGET_PARTICIPANT_FK,"+//11
		" (select pa.DESCRIPTION from Participant pa where pa.ID_PARTICIPANT_PK=sto.ID_TARGET_PARTICIPANT_FK) as part_target_description,"+//12
		" (select pt.description from PARAMETER_TABLE pt where pt.PARAMETER_TABLE_PK=sto.STATE) as estado_desc,"+//13
		" (select ha.account_number from holder_account ha where ha.id_holder_account_pk=sto.ID_SOURCE_HOLDER_ACCOUNT_FK) as source_account_number,"+//14
		" (select ha.account_number from holder_account ha where ha.id_holder_account_pk=sto.ID_TARGET_HOLDER_ACCOUNT_FK) as target_account_number,"+//15
		"  co.ID_CUSTODY_OPERATION_PK,"+//16
		"  sto.ID_SOURCE_HOLDER_ACCOUNT_FK,"+//17
		"  sto.ID_TARGET_HOLDER_ACCOUNT_FK,"+//18
		"  bo.DOCUMENT_NUMBER,"+//19
		"  bo.ID_BLOCK_OPERATION_PK,"+//20
		"  br.ID_BLOCK_ENTITY_FK," + //21
		"  sto.transfer_type, "+//22
		" (select pa.MNEMONIC from Participant pa where pa.ID_PARTICIPANT_PK=sto.ID_SOURCE_PARTICIPANT_FK) as part_source_mnemonic, "+//23
		" (select pa.MNEMONIC from Participant pa where pa.ID_PARTICIPANT_PK=sto.ID_TARGET_PARTICIPANT_FK) as part_target_mnemonic, "+//24
		" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=co.REJECT_MOTIVE), "+//25
		" co.REJECT_MOTIVE_OTHER "+//26
		" from "+
		"       SECURITY_TRANSFER_OPERATION sto"+ 
		"   inner join"+
		"       CUSTODY_OPERATION co"+ 
		"           on sto.ID_TRANSFER_OPERATION_PK=co.ID_CUSTODY_OPERATION_PK"+
		"   inner join"+
		"       BLOCK_OPERATION_TRANSFER bot"+
		"           on bot.ID_TRANSFER_OPERATION_FK=sto.ID_TRANSFER_OPERATION_PK"+
		"   inner join"+
		"       BLOCK_OPERATION bo"+ 
		"           on bot.ID_BLOCK_OPERATION_FK=bo.ID_BLOCK_OPERATION_PK"+
		"   inner join"+
		"       BLOCK_REQUEST br"+ 
		"           on bo.ID_BLOCK_REQUEST_FK=br.ID_BLOCK_REQUEST_PK ");
		 
		 if(Validations.validateIsNotNull(filters.getHolder()) && 
			Validations.validateIsNotNullAndPositive(filters.getHolder().getIdHolderPk()) &&
		   !Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk())){
				
			sbQuery.append(" left join holder_account_detail hado " +
						   " on hado.ID_HOLDER_ACCOUNT_FK = sto.ID_SOURCE_HOLDER_ACCOUNT_FK ");
			/*sbQuery.append(" left join holder_account_detail hadt " +
					   " on hadt.ID_HOLDER_ACCOUNT_FK = sto.ID_TARGET_HOLDER_ACCOUNT_FK ");*/
		}
		 
		if(Validations.validateIsNotNullAndPositive(filters.getSecurityClass())){
		 sbQuery.append("   inner join"+
						"       SECURITY sec"+ 
						"           on sec.ID_SECURITY_CODE_PK=sto.ID_SECURITY_CODE_FK");
		}
		
		sbQuery.append(" Where 1 = 1  and sto.IND_BLOCK_TRANSFER = 1 ");

		if(Validations.validateIsNotNullAndPositive(filters.getTransferType()))
			sbQuery.append(" and sto.transfer_Type = :transferTypePrm ");

		if(Validations.validateIsNotNullAndPositive(filters.getBlockType()))
			sbQuery.append(" and br.block_Type = :blockTypePrm ");

		if(Validations.validateIsNotNullAndPositive(filters.getOperationNumber()))
			sbQuery.append(" and co.operation_Number = :operationNumberPrm ");
		
		if( Validations.validateIsNotNullAndPositive(filters.getSourceIdParticipantPk()) ){
			//if(Validations.validateIsNotNullAndPositive(filters.getTransferType())){
				//if(filters.getTransferType().equals(TransferType.ORIGEN_DESTINO.getCode())){
				sbQuery.append(" and ( sto.ID_SOURCE_PARTICIPANT_FK = :sourceParticipantPrm ");
				sbQuery.append("  or sto.ID_TARGET_PARTICIPANT_FK = :sourceParticipantPrm ) ");
					
				if(Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk()))
					sbQuery.append(" and sto.ID_SOURCE_HOLDER_ACCOUNT_FK = :idHolderAccountPkPrm ");
				//}
			//}
		}
		
		if(Validations.validateIsNotNull(filters.getHolder()) && 
		   Validations.validateIsNotNullAndPositive(filters.getHolder().getIdHolderPk()) &&
		  !Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk()) ){
			sbQuery.append(" and hado.ID_HOLDER_FK = :idHolderPkPrm ");
		}
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk())){
			sbQuery.append(" and sto.id_security_code_fk = :idSecurityCodePkPrm ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filters.getSecurityClass())){
			 sbQuery.append(" and sec.SECURITY_CLASS = :securityClasPrm ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filters.getState()))
			sbQuery.append(" and sto.state = :statePrm ");

		if(Validations.validateIsNotNullAndNotEmpty(filters.getOperationInitDate()))
			sbQuery.append(" and :operationInitDatePrm <= co.operation_Date ");
	
		if(Validations.validateIsNotNullAndNotEmpty(filters.getOperationEndDate()))
			sbQuery.append(" and co.operation_Date < :operationEndDatePrm ");

		sbQuery.append(" Order by  co.operation_Number desc,co.operation_Date ");
		
		Query query = em.createNativeQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndPositive(filters.getTransferType()))
			query.setParameter("transferTypePrm", filters.getTransferType());

		if(Validations.validateIsNotNullAndPositive(filters.getBlockType()))
			query.setParameter("blockTypePrm", filters.getBlockType());

		if(Validations.validateIsNotNullAndPositive(filters.getOperationNumber()))
			query.setParameter("operationNumberPrm", filters.getOperationNumber());
		
		if(Validations.validateIsNotNullAndPositive(filters.getSourceIdParticipantPk()))
			query.setParameter("sourceParticipantPrm", filters.getSourceIdParticipantPk());

		if(Validations.validateIsNotNull(filters.getHolder()) && 
		   Validations.validateIsNotNullAndPositive(filters.getHolder().getIdHolderPk()) &&
		  !Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk()) ){
			query.setParameter("idHolderPkPrm", filters.getHolder().getIdHolderPk());
		}
		
		/*if(Validations.validateIsNotNullAndPositive(filters.getTargetIdParticipantPk()))
			query.setParameter("targetParticipantPrm", filters.getTargetIdParticipantPk());*/

		//if(Validations.validateIsNotNullAndPositive(filters.getSourceAccountNumber()))
		//	query.setParameter("accountNumberPrm", filters.getSourceAccountNumber());

		if(Validations.validateIsNotNullAndPositive(filters.getSourceIdHolderAccountPk()))
			query.setParameter("idHolderAccountPkPrm", filters.getSourceIdHolderAccountPk());

		if(Validations.validateIsNotNullAndNotEmpty(filters.getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePkPrm", filters.getIdSecurityCodePk());

		if(Validations.validateIsNotNullAndPositive(filters.getSecurityClass()))
			query.setParameter("securityClasPrm",filters.getSecurityClass());
		
		if(Validations.validateIsNotNullAndPositive(filters.getState()))
			query.setParameter("statePrm", filters.getState());

		if(Validations.validateIsNotNullAndNotEmpty(filters.getOperationInitDate()))
			query.setParameter("operationInitDatePrm", filters.getOperationInitDate(),TemporalType.DATE);
		
		if(Validations.validateIsNotNullAndNotEmpty(filters.getOperationEndDate()))
			query.setParameter("operationEndDatePrm", CommonsUtilities.addDate(filters.getOperationEndDate(), 1),TemporalType.DATE);
		
		List<SecurityBlockTransferTO> lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		SecurityBlockTransferTO securityBlockTo = null;

		List<Object> objectList = query.getResultList();
		for (int i = 0; i < objectList.size(); i++) {
			
			boolean isNewOperation=true;
			Object obj[] = (Object[])objectList.get(i);
			securityBlockTo = new SecurityBlockTransferTO();

			for(int j = 0; j < lstSecurityBlockTransferTO.size(); j++){
				if( lstSecurityBlockTransferTO.get(j).getOperationNumber().equals(new Long(((BigDecimal)obj[0]).toString())) ){
					isNewOperation=false;
				}
			}
			
			if(isNewOperation){
				securityBlockTo.setOperationNumber(new Long(((BigDecimal)obj[0]).toString()));//
				securityBlockTo.setOperationType(new Long(((BigDecimal)obj[1]).toString()));//
				securityBlockTo.setOperationDate(new Date(((Timestamp)obj[2]).getTime()));//
				securityBlockTo.setIdBlockRequest(new Long(((BigDecimal)obj[3]).toString()));//
				securityBlockTo.setBlockType(new Integer(((BigDecimal)obj[4]).toString()));//
				securityBlockTo.setBlockTypeDescription((String)obj[5]);//
				securityBlockTo.setIdSecurityCodePk((String)obj[6]);//
				securityBlockTo.setActualBlockBalance((BigDecimal)obj[7]);///*Cantidad de operacion*/
				securityBlockTo.setState(new Integer(((BigDecimal)obj[8]).toString()));//
				if(Validations.validateIsNotNull(obj[9]))
					securityBlockTo.setSourceIdParticipantPk(new Long(((BigDecimal)obj[9]).toString()));//
				if(Validations.validateIsNotNull(obj[10]))
					securityBlockTo.setSourceParticipantDescription((String)obj[10]);//
				if(Validations.validateIsNotNull(obj[11]))
					securityBlockTo.setTargetIdParticipantPk(new Long(((BigDecimal)obj[11]).toString()));//
				if(Validations.validateIsNotNull(obj[12]))
				securityBlockTo.setTargetParticipantDescription((String)obj[12]);//
				if(Validations.validateIsNotNull(obj[13]))
					securityBlockTo.setStateDescription((String)obj[13]);//
				if(Validations.validateIsNotNull(obj[14]))
					securityBlockTo.setSourceAccountNumber(new Integer(((BigDecimal)obj[14]).toString()));//
				if(Validations.validateIsNotNull(obj[15]))
					securityBlockTo.setTargetAccountNumber(new Integer(((BigDecimal)obj[15]).toString()));//				
				securityBlockTo.setIdCustodyOperationPk(new Long(((BigDecimal)obj[16]).toString()));//
				if(Validations.validateIsNotNull(obj[16]))
				securityBlockTo.setIdTransferOperationPk(new Long(((BigDecimal)obj[16]).toString()));//Es el mismo pk que el custodyOperation
				if(Validations.validateIsNotNull(obj[17])){
					securityBlockTo.setSourceIdHolderAccountPk(new Long(((BigDecimal)obj[17]).toString()));//
					HolderAccount sourceHolderAccount =  new HolderAccount();
					sourceHolderAccount.setIdHolderAccountPk(new Long(((BigDecimal)obj[17]).toString()));//
					sourceHolderAccount = getHolderAccount(sourceHolderAccount);
					securityBlockTo.setSourceHolderAccount(sourceHolderAccount);
				}
				if(Validations.validateIsNotNull(obj[18]))
					securityBlockTo.setTargetIdHolderAccountPk(new Long(((BigDecimal)obj[18]).toString()));
				if(Validations.validateIsNotNull(obj[19]))
					securityBlockTo.setDocumentNumber(new Long(((BigDecimal)obj[19]).toString()));//
				if(Validations.validateIsNotNull(obj[20]))
					securityBlockTo.setIdBlockOperationPk(new Long(((BigDecimal)obj[20]).toString()));//
				if(Validations.validateIsNotNull(obj[21]))
					securityBlockTo.setIdblockEntityPk(new Long(((BigDecimal)obj[21]).toString()));
				if(Validations.validateIsNotNull(obj[22]))
					securityBlockTo.setTransferType(new Integer(((BigDecimal)obj[22]).toString()));
				
				if(Validations.validateIsNotNull(obj[23]))
					securityBlockTo.setSourceParticipantMnemonic((String)obj[23]);//
				if(Validations.validateIsNotNull(obj[24]))
					securityBlockTo.setTargetParticipantMnemonic((String)obj[24]);//
				if(Validations.validateIsNotNull(obj[25]))
					securityBlockTo.setRejectMotiveDesc((String)obj[25]);
				if(Validations.validateIsNotNull(obj[26]))
					securityBlockTo.setRejectOther((String)obj[26]);
				
				lstSecurityBlockTransferTO.add(securityBlockTo);
				isNewOperation=true;
			}
			
		}
		if (lstSecurityBlockTransferTO.size()>0){
			return lstSecurityBlockTransferTO;
		}else{
			lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
			return lstSecurityBlockTransferTO;
		}
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param holderAcc the holder acc
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccount holderAcc) throws ServiceException{
		HolderAccount holderAccount = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			
		sbQuery.append(" select ha from HolderAccount ha join fetch ha.holderAccountDetails had join fetch had.holder ho ");
		sbQuery.append(" where 1 = 1 ");
		
		if(Validations.validateIsNotNullAndPositive(holderAcc.getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :idHolderAccountPkPrm " );
		}else{
			
			if(Validations.validateIsNotNullAndPositive(holderAcc.getAccountNumber())){
				sbQuery.append(" and ha.accountNumber = :accountNumberPrm " );
			}
			
			if(Validations.validateIsNotNull(holderAcc.getParticipant()) &&
			   Validations.validateIsNotNullAndPositive(holderAcc.getParticipant().getIdParticipantPk()) ){
				sbQuery.append(" and ha.participant.idParticipantPk = :idParticipantPkPrm " );
			}
			
		}
		
		TypedQuery<HolderAccount> typedQuery = em.createQuery(sbQuery.toString(),HolderAccount.class); 

		if(Validations.validateIsNotNullAndPositive(holderAcc.getIdHolderAccountPk())){
			typedQuery.setParameter("idHolderAccountPkPrm", holderAcc.getIdHolderAccountPk());
		}else{

			if(Validations.validateIsNotNullAndPositive(holderAcc.getAccountNumber())){
				typedQuery.setParameter("accountNumberPrm",holderAcc.getAccountNumber());
			}
			
			if(Validations.validateIsNotNull(holderAcc.getParticipant()) &&
			   Validations.validateIsNotNullAndPositive(holderAcc.getParticipant().getIdParticipantPk()) ){
				typedQuery.setParameter("idParticipantPkPrm",holderAcc.getParticipant().getIdParticipantPk());
			}
			
			
		}
		
		holderAccount = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			logger.info("No result found for named query: " + sbQuery.toString());
		} catch (Exception e) {
			logger.info("Error while running query: " + e.getMessage());
			e.printStackTrace();
		}
		
		return holderAccount;
	}
	
}
