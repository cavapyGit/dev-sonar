package com.pradera.custody.tranfersecurities.accounts.service;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.custody.tranfersecurities.to.SecurityTransferOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.SecuritiesTransferType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class CustodyOperationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
public class CustodyOperationServiceBean extends CrudDaoServiceBean{

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The Constant logger. */
	@Inject  
	PraderaLogger logger;

	/**
	 * Gets the complement holder account.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @return the complement holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getComplementHolderAccount(SecurityTransferOperation securityTransferOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select ");
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				sbQuery.append(" sto.sourceHolderAccount.idHolderAccountPk ");
			}else{
				sbQuery.append(" sto.targetHolderAccount.idHolderAccountPk ");
			}
		sbQuery.append(" from SecurityTransferOperation sto ");
		sbQuery.append(" where sto.custodyOperation.idCustodyOperationPk = :idCustodyOperationPkPrm " +
							" and sto.transferType = :transferTypePrm " +
							" and sto.state = :statePrm " );
		
		Query query = em.createQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndNotEmpty(securityTransferOperation.getIdTransferOperationPk()))
			query.setParameter("idCustodyOperationPkPrm", securityTransferOperation.getIdTransferOperationPk());

		if(Validations.validateIsNotNullAndNotEmpty(securityTransferOperation.getTransferType()))
			query.setParameter("transferTypePrm", securityTransferOperation.getTransferType());

		if(Validations.validateIsNotNullAndNotEmpty(securityTransferOperation.getState()))
			query.setParameter("statePrm", securityTransferOperation.getState());
		
		HolderAccount holderAccount = new HolderAccount();
		List<Object> lstObj = query.getResultList();
		if(lstObj.size()>0){
			holderAccount.setIdHolderAccountPk((Long)lstObj.get(0));
			return getHolderAccount(holderAccount);
		}else{
			return holderAccount;
		}
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param holderAcc the holder acc
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccount holderAcc) throws ServiceException{
		HolderAccount holderAccount = null;
		StringBuilder sbQuery = new StringBuilder();
		
		try {
			
		sbQuery.append(" select ha from HolderAccount ha join fetch ha.holderAccountDetails had join fetch had.holder ho ");
		sbQuery.append(" where 1 = 1  and ha.accountGroup= :investor");
		
		if(Validations.validateIsNotNullAndPositive(holderAcc.getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :idHolderAccountPkPrm " );
		}else{
			
			if(Validations.validateIsNotNullAndPositive(holderAcc.getAccountNumber())){
				sbQuery.append(" and ha.accountNumber = :accountNumberPrm " );
			}
			
			if(Validations.validateIsNotNull(holderAcc.getParticipant()) &&
			   Validations.validateIsNotNullAndPositive(holderAcc.getParticipant().getIdParticipantPk()) ){
				sbQuery.append(" and ha.participant.idParticipantPk = :idParticipantPkPrm " );
			}
			
		}
		
		
		TypedQuery<HolderAccount> typedQuery = em.createQuery(sbQuery.toString(),HolderAccount.class); 

		if(Validations.validateIsNotNullAndPositive(holderAcc.getIdHolderAccountPk())){
			typedQuery.setParameter("idHolderAccountPkPrm", holderAcc.getIdHolderAccountPk());
		}else{

			if(Validations.validateIsNotNullAndPositive(holderAcc.getAccountNumber())){
				typedQuery.setParameter("accountNumberPrm",holderAcc.getAccountNumber());
			}
			
			if(Validations.validateIsNotNull(holderAcc.getParticipant()) &&
			   Validations.validateIsNotNullAndPositive(holderAcc.getParticipant().getIdParticipantPk()) ){
				typedQuery.setParameter("idParticipantPkPrm",holderAcc.getParticipant().getIdParticipantPk());
			}
			
			
		}
		typedQuery.setParameter("investor",HolderAccountGroupType.INVERSTOR.getCode());
		
		holderAccount = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			logger.info("No result found for named query: " + sbQuery.toString());
		} catch (Exception e) {
			logger.info("Error while running query: " + e.getMessage());
			e.printStackTrace();
		}
		
		return holderAccount;
	}
	
	/**
	 * Exist security transfer block.
	 *
	 * @param blockType the block type
	 * @param securityCode the security code
	 * @param operationNumber the operation number
	 * @return the int
	 */
	public int existSecurityTransferBlock(Integer blockType, String securityCode,Long operationNumber){
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select sto.id_security_code_fk,br.block_type "+
						" from "+
						" custody_operation co "+
						" inner join block_operation bo "+
						"  on bo.id_block_operation_pk = co.id_custody_operation_pk "+
						" inner join block_operation_transfer bot "+
						"  on bot.id_block_operation_fk = bo.id_block_operation_pk "+
						" inner join security_transfer_operation sto "+
						"  on sto.id_transfer_operation_pk = bot.id_transfer_operation_fk "+
						" inner join block_request br "+
						"  on br.id_block_request_pk=bo.id_block_request_fk ");
		
		sbQuery.append(" where sto.ind_block_transfer = 1 " +
					   " and sto.state in ("+TransferSecuritiesStateType.REGISTRADO.getCode()+","+TransferSecuritiesStateType.APROBADO.getCode()+","+TransferSecuritiesStateType.REVISADO.getCode()+") " +
					   " and br.block_type = :blockTyperm " +
					   " and co.operation_number = :operationNumberm " +
					   " and sto.id_security_code_fk = :securityCodePrm "); 
		
		Query query = em.createNativeQuery(sbQuery.toString()); 

		query.setParameter("blockTyperm", blockType);
		query.setParameter("securityCodePrm", securityCode);
		query.setParameter("operationNumberm", operationNumber);
		
		int cantidadLista = query.getResultList().size();
		
		return cantidadLista;
	}

	/**
	 * Gets the dates custody operation for pk.
	 *
	 * @param custodyOperation the custody operation
	 * @return the dates custody operation for pk
	 * @throws ServiceException the service exception
	 */
	public CustodyOperation getDatesCustodyOperationForPk(CustodyOperation custodyOperation) throws ServiceException{

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select co.idCustodyOperationPk, " +
				" TO_CHAR(co.operationDate,'dd/MM/yyyy'), " +
				" TO_CHAR(co.confirmDate,'dd/MM/yyyy'), " +
				" TO_CHAR(co.registryDate,'dd/MM/yyyy'), " +
				" TO_CHAR(co.rejectDate,'dd/MM/yyyy'), " +
				" TO_CHAR(co.reviewDate,'dd/MM/yyyy') " +
				" From CustodyOperation co ");
		sbQuery.append(" Where 1=1 ");
		if(Validations.validateIsNotNullAndPositive(custodyOperation.getIdCustodyOperationPk()))
		sbQuery.append(" and co.idCustodyOperationPk= :idCustodyOperationPkPrm ");
		
		Query query = em.createQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndPositive(custodyOperation.getIdCustodyOperationPk()))
			query.setParameter("idCustodyOperationPkPrm", custodyOperation.getIdCustodyOperationPk());

		for (int i = 0; i < query.getResultList().size(); i++) {
			Object obj[] = (Object[])query.getResultList().get(i);
			custodyOperation.setIdCustodyOperationPk((Long)obj[0]);
			if(Validations.validateIsNotNull(obj[1]))
			custodyOperation.setOperationDate(CommonsUtilities.convertStringtoDate(obj[1].toString()));
			if(Validations.validateIsNotNull(obj[2]))
			custodyOperation.setConfirmDate(CommonsUtilities.convertStringtoDate(obj[2].toString()));
			if(Validations.validateIsNotNull(obj[3]))
			custodyOperation.setRegistryDate(CommonsUtilities.convertStringtoDate(obj[3].toString()));
			if(Validations.validateIsNotNull(obj[4]))
			custodyOperation.setRejectDate(CommonsUtilities.convertStringtoDate(obj[4].toString()));
			if(Validations.validateIsNotNull(obj[5]))
			custodyOperation.setReviewDate(CommonsUtilities.convertStringtoDate(obj[5].toString()));
		}
		
		return custodyOperation;
	}
	
	
	
	/**
	 * Gets the list holder account for user.
	 *
	 * @param objUserAccountSession the obj user account session
	 * @return the list holder account for user
	 * @throws ServiceException the service exception
	 */
	public List<Long> getListHolderAccountForUser(UserAccountSession objUserAccountSession)throws ServiceException{
		List<Long> lstHolderAccountPk = new ArrayList<Long>();
		Participant objParticipant = null;
		
		objParticipant = accountsFacade.getParticipantByPk(objUserAccountSession.getParticipantCode());		
				
		if(objParticipant!=null){
			HolderAccountTO holderAccountTO = new HolderAccountTO();
			holderAccountTO.setParticipantTO(objParticipant.getIdParticipantPk());
			
			List<Long> lstHolderPk = new ArrayList<Long>();
			HolderSearchTO holderSearchTO = new HolderSearchTO();
			holderSearchTO.setDocumentType(objParticipant.getDocumentType());
			holderSearchTO.setDocumentNumber(objParticipant.getDocumentNumber());
			holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
			List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
			if(lstHolder!=null && lstHolder.size()>0){
				for(Holder objHolder : lstHolder){
					lstHolderAccountPk.add(objHolder.getIdHolderPk());
				}
			}			
			
//			holderAccountTO.setLstHolderPk(lstHolderPk);
//			List<HolderAccount> lstHolderAccount = null;
//			if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
//				 lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
//			}						
//			
//			if(lstHolderAccount!=null && lstHolderAccount.size()>0){
//				for(HolderAccount objHolderAccount : lstHolderAccount){
//					lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
//				}
//			}
		}		

		return lstHolderAccountPk;
	}
	
	/**
	 * Search transfer security available nativo.
	 *
	 * @param searchSecurityTransferOperation the search security transfer operation
	 * @param objUserAccountSession the obj user account session
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityTransferOperation> searchTransferSecurityAvailableNativo(SecurityTransferOperation searchSecurityTransferOperation,UserAccountSession objUserAccountSession) throws ServiceException{
		
		List<Long> lstHolderAccountPk = new ArrayList<Long>();
		if(objUserAccountSession.isParticipantInvestorInstitucion()){
			lstHolderAccountPk = getListHolderAccountForUser(objUserAccountSession);			
		}
		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" select " +
			" sto.id_transfer_operation_pk, " +//0
			" sto.transfer_type, " +//1
			" sto.id_source_participant_fk, "+//2
			" (select (mnemonic||'-'||id_participant_pk) from participant where id_participant_pk = sto.id_source_participant_fk) source_participant_desc, "+//3
			" sto.id_target_participant_fk, "+//4
			" (select (mnemonic||'-'||id_participant_pk) from participant where id_participant_pk = sto.id_target_participant_fk) target_participant_desc, "+//5
			" sto.id_source_holder_account_fk, "+//6
			" sto.id_target_holder_account_fk, "+//7
			" sto.quantity_operation, " +//8
			" (select id_isin_code from security where id_security_code_pk = sto.ID_SECURITY_CODE_FK), " +//9
			" sto.state, " +//10
			" co.operation_type, " +//11
			" co.operation_number, "+//12
			" co.operation_date, "+//13
			" (select description from security where id_security_code_pk = sto.id_security_code_fk) security_description ," +//14
			" sto.ID_SECURITY_CODE_FK, "+//15
			" (select description from participant where id_participant_pk = sto.id_source_participant_fk) source_participant_mnemonic, "+//16
			" (select description from participant where id_participant_pk = sto.id_target_participant_fk) target_participant_mnemonic, "+//17
			" (SELECT PARAMETER_NAME FROM PARAMETER_TABLE WHERE PARAMETER_TABLE_PK=co.REJECT_MOTIVE), "+//18
			" co.REJECT_MOTIVE_OTHER,co.REGISTRY_USER,co.REVIEW_USER,  "+//19,20,21
			//" (select had.id_holder_pk )had.id_holder_fk "+//22
			" (SELECT LISTAGG(H.ID_HOLDER_PK,'<br>') WITHIN GROUP (ORDER BY HAD.ID_HOLDER_FK) "+
	        " FROM HOLDER_ACCOUNT_DETAIL HAD INNER JOIN HOLDER H ON H.ID_HOLDER_PK = HAD.ID_HOLDER_FK "+
	        " WHERE HAD.ID_HOLDER_ACCOUNT_FK = sto.id_source_holder_account_fk) AS id_holder_fk, (select SECURITY_CLASS from security where id_security_code_pk = sto.ID_SECURITY_CODE_FK) security_class,  "
	        + " (select not_Traded from security where id_security_code_pk = sto.ID_SECURITY_CODE_FK) notTraded "+
			" from security_transfer_operation sto " +
				" inner join custody_operation co " +
				" on sto.ID_TRANSFER_OPERATION_PK = co.ID_CUSTODY_OPERATION_PK " +
				" left join holder_account ha " +
				" on ha.id_holder_account_pk = sto.id_source_holder_account_fk " );
//		if( Validations.validateIsNotNull(searchSecurityTransferOperation.getHolder()) && 
//			Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolder().getIdHolderPk()) && 
//		  !(Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
//			Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())) ){
//			
//			sbQuery.append(" left join holder_account_detail hado " +
//						   " on hado.ID_HOLDER_ACCOUNT_FK = sto.ID_SOURCE_HOLDER_ACCOUNT_FK ");
//			sbQuery.append(" left join holder_account_detail hadt " +
//					   " on hadt.ID_HOLDER_ACCOUNT_FK = sto.ID_TARGET_HOLDER_ACCOUNT_FK ");
//		}
		
		sbQuery.append(" where 1=1 and sto.IND_BLOCK_TRANSFER = 0 ");
		
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getCustodyOperation().getOperationNumber()))
			sbQuery.append(" and co.operation_number = :operationNumber ");
		
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getTransferType())){
			sbQuery.append(" and sto.transfer_type = :transfer_typePrm ");
			
			if(searchSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				
				if(	Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) &&  
					Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())){

					sbQuery.append(" and (sto.id_target_participant_fk = :id_participant_fkPrm " +
									" and sto.id_target_holder_account_fk = :id_holder_account_fkPrm) ");
				}else if(	Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
							Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) ){

					sbQuery.append(" and (sto.id_target_participant_fk = :id_participant_fkPrm) ");
				}
				
				//si solo selecciono el cui y no la cuenta
				if( Validations.validateIsNotNull(searchSecurityTransferOperation.getHolder()) && 
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolder().getIdHolderPk()) && 
				  !(Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())) ){
					
					//sbQuery.append(" and (hadt.ID_HOLDER_FK = :id_holder_fkPrm) ");
					sbQuery.append(" AND ((SELECT COUNT(*) FROM holder_account_detail hado WHERE hado.ID_HOLDER_ACCOUNT_FK = sto.ID_SOURCE_HOLDER_ACCOUNT_FK");
					sbQuery.append(" AND hado.ID_HOLDER_FK = :id_holder_fkPrm)>0  OR (SELECT COUNT(*) FROM holder_account_detail hadt"); 
					sbQuery.append(" WHERE hadt.ID_HOLDER_ACCOUNT_FK = sto.ID_TARGET_HOLDER_ACCOUNT_FK AND hadt.ID_HOLDER_FK = :id_holder_fkPrm)>0)");
				}
				
			}else{	//ORIGEN_DESTINO || MISMO_PARTICIPANTE
				
				if( Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) 
						&& Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount())
						&& Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())){

						sbQuery.append(" and ( (sto.id_target_participant_fk = :id_participant_fkPrm " +
								" and sto.id_target_holder_account_fk = :id_holder_account_fkPrm) ");
						sbQuery.append(" or (sto.id_source_participant_fk = :id_participant_fkPrm " +
								" and sto.id_source_holder_account_fk = :id_holder_account_fkPrm) ) ");
						
//						sbQuery.append(" and (sto.id_source_participant_fk = :id_participant_fkPrm " +
//								" and sto.id_source_holder_account_fk = :id_holder_account_fkPrm) ");
						}else if( Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
								  Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) ){
						sbQuery.append(" and ( sto.id_target_participant_fk = :id_participant_fkPrm ");
						sbQuery.append(" or sto.id_source_participant_fk = :id_participant_fkPrm ) ");
//						sbQuery.append(" and (sto.id_source_participant_fk = :id_participant_fkPrm) ");
					}
				

				//si solo selecciono el cui y no la cuenta
				if( Validations.validateIsNotNull(searchSecurityTransferOperation.getHolder()) && 
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolder().getIdHolderPk()) && 
				  !(Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
					Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())) ){
					
					//sbQuery.append(" and (hado.ID_HOLDER_FK = :id_holder_fkPrm) ");
					sbQuery.append(" AND ((SELECT COUNT(*) FROM holder_account_detail hado WHERE hado.ID_HOLDER_ACCOUNT_FK = sto.ID_SOURCE_HOLDER_ACCOUNT_FK");
					sbQuery.append(" AND hado.ID_HOLDER_FK = :id_holder_fkPrm)>0  OR (SELECT COUNT(*) FROM holder_account_detail hadt"); 
					sbQuery.append(" WHERE hadt.ID_HOLDER_ACCOUNT_FK = sto.ID_TARGET_HOLDER_ACCOUNT_FK AND hadt.ID_HOLDER_FK = :id_holder_fkPrm)>0)");
				}
			}
			
		}else{
			
			if(	Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
				Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) 
				&& Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount())
				&& Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())){
			
				sbQuery.append(" and ( (sto.id_target_participant_fk = :id_participant_fkPrm " +
								" and sto.id_target_holder_account_fk = :id_holder_account_fkPrm) ");
				sbQuery.append(" or (sto.id_source_participant_fk = :id_participant_fkPrm " +
								" and sto.id_source_holder_account_fk = :id_holder_account_fkPrm) ) ");
			
			}else if( Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
					  Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk())){
				
				sbQuery.append(" and ( sto.id_target_participant_fk = :id_participant_fkPrm ");
				sbQuery.append(" or sto.id_source_participant_fk = :id_participant_fkPrm ) ");
				
			}
			
			//si solo selecciono el cui y no la cuenta
			if( Validations.validateIsNotNull(searchSecurityTransferOperation.getHolder()) && 
				Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolder().getIdHolderPk()) && 
			  !(Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
				Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())) ){
				
				//sbQuery.append(" and (hado.ID_HOLDER_FK = :id_holder_fkPrm or hadt.ID_HOLDER_FK = :id_holder_fkPrm) ");
				sbQuery.append(" AND ((SELECT COUNT(*) FROM holder_account_detail hado WHERE hado.ID_HOLDER_ACCOUNT_FK = sto.ID_SOURCE_HOLDER_ACCOUNT_FK");
				sbQuery.append(" AND hado.ID_HOLDER_FK = :id_holder_fkPrm)>0  OR (SELECT COUNT(*) FROM holder_account_detail hadt"); 
				sbQuery.append(" WHERE hadt.ID_HOLDER_ACCOUNT_FK = sto.ID_TARGET_HOLDER_ACCOUNT_FK AND hadt.ID_HOLDER_FK = :id_holder_fkPrm)>0)");
			}
			
		}
		
		if( Validations.validateIsNotNull(searchSecurityTransferOperation.getSecurities()) ){
		
			if(Validations.validateIsNotNullAndNotEmpty(searchSecurityTransferOperation.getSecurities().getIdSecurityCodePk())){
				sbQuery.append(" and sto.ID_SECURITY_CODE_FK = :id_security_code_fkPrm ");
			}
		}
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getSecurityClass())){
			sbQuery.append(" and sto.ID_SECURITY_CODE_FK in ( select se.id_security_code_pk from security se where se.security_class = :id_security_classPrm and se.id_security_code_pk = sto.ID_SECURITY_CODE_FK ) ");
		}else if(objUserAccountSession.isIssuerDpfInstitucion()){
			sbQuery.append(" and sto.ID_SECURITY_CODE_FK in ( select se.id_security_code_pk from security se where se.security_class in (:lst_id_security_classPrm) and se.id_security_code_pk = sto.ID_SECURITY_CODE_FK ) ");
		}
		
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getState())){
			sbQuery.append(" and sto.state = :statePrm ");
		}
		
		if(Validations.validateIsNotNull(searchSecurityTransferOperation.getCustodyOperation().getOperationInit()) 
		&& Validations.validateIsNotNull(searchSecurityTransferOperation.getCustodyOperation().getOperationEnd())){
			sbQuery.append(" and :init_operation_datePrm <= co.operation_date ");
			sbQuery.append(" and co.operation_date < :end_operation_datePrm ");
		}
		
		sbQuery.append(" Order By co.operation_number desc");

		Query query = em.createNativeQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getCustodyOperation().getOperationNumber()))
			query.setParameter("operationNumber", searchSecurityTransferOperation.getCustodyOperation().getOperationNumber());
		
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getTransferType()))
			query.setParameter("transfer_typePrm", searchSecurityTransferOperation.getTransferType());
		
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getState()))
			query.setParameter("statePrm", searchSecurityTransferOperation.getState());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchSecurityTransferOperation.getCustodyOperation().getOperationInit()))
			query.setParameter("init_operation_datePrm", searchSecurityTransferOperation.getCustodyOperation().getOperationInit(),TemporalType.DATE);
		
		if(Validations.validateIsNotNullAndNotEmpty(searchSecurityTransferOperation.getCustodyOperation().getOperationEnd()))
			query.setParameter("end_operation_datePrm", CommonsUtilities.addDate(searchSecurityTransferOperation.getCustodyOperation().getOperationEnd(), 1),TemporalType.DATE);
	
		if( Validations.validateIsNotNull(searchSecurityTransferOperation.getSecurities())){
			if(Validations.validateIsNotNullAndNotEmpty(searchSecurityTransferOperation.getSecurities().getIdSecurityCodePk())){
			query.setParameter("id_security_code_fkPrm", searchSecurityTransferOperation.getSecurities().getIdSecurityCodePk());
			}
		}
		if(Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getSecurityClass())){
			query.setParameter("id_security_classPrm", searchSecurityTransferOperation.getSecurityClass());
		}else if(objUserAccountSession.isIssuerDpfInstitucion()){
			List<Integer> lstClass = new ArrayList<Integer>();
			lstClass.add(SecurityClassType.DPA.getCode());
			lstClass.add(SecurityClassType.DPF.getCode());
			query.setParameter("lst_id_security_classPrm",lstClass);
		}
		
		if( Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
			Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) &&
			Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) &&
		    Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk()) )
			query.setParameter("id_holder_account_fkPrm", searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk());
	
		if( Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) &&
			Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()))
			query.setParameter("id_participant_fkPrm", searchSecurityTransferOperation.getParticipant().getIdParticipantPk());
		
		//si solo selecciono el cui y no la cuenta
		if( Validations.validateIsNotNull(searchSecurityTransferOperation.getHolder()) && 
			Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolder().getIdHolderPk()) && 
		  !(Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
			Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())) ){

			query.setParameter("id_holder_fkPrm", searchSecurityTransferOperation.getHolder().getIdHolderPk());
		}
		
		/* settear los parametros */

		List<SecurityTransferOperation> lstSecurityTransferOperation = new ArrayList<SecurityTransferOperation>();
		
		SecurityTransferOperation securityTransferOperation = null;
		
		List<Object[]> resultList =  query.getResultList();
		
		for (int i = 0; i < resultList.size(); i++) {
			Object obj[] = (Object[])resultList.get(i);				
			if(Integer.valueOf(obj[1].toString()).equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) 
					&& lstHolderAccountPk.size()>0 && objUserAccountSession.isParticipantInvestorInstitucion()){
				if(objUserAccountSession.isParticipantInvestorInstitucion() && lstHolderAccountPk.contains(Long.valueOf(obj[22].toString()))){
					securityTransferOperation = getSecurityTransferOperation(obj, searchSecurityTransferOperation);
					lstSecurityTransferOperation.add(securityTransferOperation);
				}
			}
			else
			{
				securityTransferOperation = getSecurityTransferOperation(obj, searchSecurityTransferOperation);
				lstSecurityTransferOperation.add(securityTransferOperation);
			}						
		}
		return lstSecurityTransferOperation;
	}
	
	/**
	 * Gets the security transfer operation.
	 *
	 * @param obj the obj
	 * @param searchSecurityTransferOperation the search security transfer operation
	 * @return the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public SecurityTransferOperation getSecurityTransferOperation(Object obj[],SecurityTransferOperation searchSecurityTransferOperation)throws ServiceException{
		SecurityTransferOperation securityTransferOperation = new SecurityTransferOperation();
		CustodyOperation custodyOperation = null;
		Participant participant = null;
		Security security = null;
		
		if(Validations.validateIsNotNull(obj[0])){
			securityTransferOperation.setIdTransferOperationPk(new Long(((BigDecimal)obj[0]).toString()));
		}
		
		if(Validations.validateIsNotNull(obj[1])){
			securityTransferOperation.setTransferType(Integer.parseInt(((BigDecimal)obj[1]).toString()));
		}
		
		participant = new Participant();	//source Participant
		
		if(Validations.validateIsNotNull(obj[2])){
			participant.setIdParticipantPk(new Long(((BigDecimal)obj[2]).toString()));
		}

		if(Validations.validateIsNotNull(obj[3])){
			participant.setDisplayCodeMnemonic((String)obj[3]);
		}
		if(Validations.validateIsNotNull(obj[16])){
			participant.setDescription((String)obj[16]);
		}
		
		securityTransferOperation.setSourceParticipant(participant);
		participant = new Participant();
		
		if(Validations.validateIsNotNull(obj[4])){
			participant.setIdParticipantPk(new Long(((BigDecimal)obj[4]).toString()));
		}
		
		if(Validations.validateIsNotNull(obj[5])){
			participant.setDisplayCodeMnemonic((String)obj[5]);
		}

		if(Validations.validateIsNotNull(obj[17])){
			participant.setDescription((String)obj[17]);
		}
		
		securityTransferOperation.setTargetParticipant(participant);
		participant = new Participant();

		if(Validations.validateIsNotNull(obj[8])){
			securityTransferOperation.setQuantityOperation((BigDecimal)obj[8]);
		}
		
		security = new Security();
		
		if(Validations.validateIsNotNull(obj[15])){
			security.setIdIsinCode((String)obj[9]);
			security.setDescription((String)obj[14]);
			security.setIdSecurityCodePk((String)obj[15]);
			security.setSecurityClass(Integer.parseInt(((BigDecimal)obj[23]).toString()));
			security.setNotTraded(obj[24] != null ? Integer.parseInt(((BigDecimal)obj[24]).toString()) : GeneralConstants.ZERO_VALUE_INTEGER );
		}
		
		securityTransferOperation.setSecurities(security);
		//security = new Security();
		
		if(Validations.validateIsNotNull(obj[10])){
			securityTransferOperation.setState(Integer.parseInt(((BigDecimal)obj[10]).toString()));
			
			Map<Integer, String> mapState = searchSecurityTransferOperation.getMapSecurityTransferState();
			if(mapState != null && mapState.size()>0){
			securityTransferOperation.setStateDescription(mapState.get(securityTransferOperation.getState()));
			}
		}
		
		custodyOperation = new CustodyOperation();
		
		custodyOperation.setIdCustodyOperationPk(securityTransferOperation.getIdTransferOperationPk());
		
		if(Validations.validateIsNotNull(obj[11])){
			custodyOperation.setOperationType(new Long(((BigDecimal)obj[11]).toString()));
		}
		
		if(Validations.validateIsNotNull(obj[12])){
			custodyOperation.setOperationNumber(new Long(((BigDecimal)obj[12]).toString()));
		}
		
		if(Validations.validateIsNotNull(obj[13])){
			custodyOperation.setOperationDate(new Date(((Timestamp)obj[13]).getTime()));
		}
		
		if(Validations.validateIsNotNull(obj[18])){
			custodyOperation.setRejectMotiveDesc(obj[18].toString());
		}
		
		if(Validations.validateIsNotNull(obj[19])){
			custodyOperation.setRejectMotiveOther(obj[19].toString());
		}
		if(Validations.validateIsNotNull(obj[20])){
			custodyOperation.setRegistryUser(obj[20].toString());
		}
		if(Validations.validateIsNotNull(obj[21])){
			custodyOperation.setReviewUser(obj[21].toString());
		}
		securityTransferOperation.setCustodyOperation(custodyOperation);
		
		
		if(Validations.validateIsNotNull(obj[6])){
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(new Long(((BigDecimal)obj[6]).toString()));
			holderAccount = getHolderAccount(holderAccount);
			securityTransferOperation.setSourceHolderAccount(holderAccount);
		}else{
			HolderAccount holderAccount = new HolderAccount();
			securityTransferOperation.setSourceHolderAccount(holderAccount);
		}
		
		if(Validations.validateIsNotNull(obj[7])){
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(new Long(((BigDecimal)obj[7]).toString()));
			holderAccount = getHolderAccount(holderAccount);
			securityTransferOperation.setTargetHolderAccount(holderAccount);
		}else{
			HolderAccount holderAccount = new HolderAccount();
			securityTransferOperation.setTargetHolderAccount(holderAccount);
		}
		
		securityTransferOperation.setSelected(false);
		
		return securityTransferOperation;
	}
	/**
	 * Gets the security transfer operation.
	 *
	 * @param idSecurityTransferOperation the id security transfer operation
	 * @return the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public SecurityTransferOperation getSecurityTransferOperation(Long idSecurityTransferOperation) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		SecurityTransferOperation objSecurityTransferOperation;
		sbQuery.append(" Select sto From SecurityTransferOperation sto ");
		sbQuery.append(" Inner Join Fetch sto.custodyOperation co ");
		sbQuery.append(" Where sto.idTransferOperationPk= :idTransferOperationPk ");
		Query query = em.createQuery(sbQuery.toString()); 
		query.setParameter("idTransferOperationPk", idSecurityTransferOperation);
		return (SecurityTransferOperation)query.getSingleResult();
	}
	
	public Security getExpirationDateInfo(String securityCode) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT s");
		sbQuery.append("	FROM Security s left join fetch s.interestPaymentSchedule ips join fetch s.issuer ");
		sbQuery.append("	WHERE s.idSecurityCodePk = :securityCode");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("securityCode", securityCode);
		
		Security sec = null;
		try{
			sec = (Security) query.getSingleResult();
			sec.getInterestPaymentSchedule();
		} catch(NoResultException nrex) {
			sec=null;
		} catch (NonUniqueResultException nuex) {
			sec=null;
		}

		return sec;
	}
}
