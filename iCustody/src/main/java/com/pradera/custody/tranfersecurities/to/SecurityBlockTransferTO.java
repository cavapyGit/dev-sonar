package com.pradera.custody.tranfersecurities.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.holderaccounts.HolderAccount;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SecurityBlockTransferTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class SecurityBlockTransferTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id block operation pk. */
	private Long idBlockOperationPk;		//BlockOperation
	
	/** The document number. */
	private Long documentNumber;			//BlockOperation
	
	/** The actual block balance. */
	private BigDecimal actualBlockBalance;	//BlockBalance
	
	/** The actual block amount. */
	private BigDecimal actualBlockAmount;	//BlockBalance
	
	/** The id isin code pk. */
	private String idIsinCodePk;			//SecurityTransferOperation (reference ISIN en security)

	/** The security class. */
	private Integer securityClass;			//SecurityTransferOperation (reference ISIN en security)
	/** The id security code pk. */
	private String idSecurityCodePk;		//SecurityTransferOperation (reference Security)
	
	/** The id holder pk. */
	private Long idHolderPk;				//holder
	
	/** The source account number. */
	private Integer sourceAccountNumber;	//HolderAccount
	
	/** The source holder account. */
	private HolderAccount sourceHolderAccount;	//HolderAccount
	
	/** The target account number. */
	private Integer targetAccountNumber;	//HolderAccount

	/** The source id participant pk. */
	private Long sourceIdParticipantPk;		//BlockOperation	(reference Participant)
	
	/** The target id participant pk. */
	private Long targetIdParticipantPk;		//BlockOperation	(reference Participant)

	/** The source participant description. */
	private String sourceParticipantDescription;		//BlockOperation	(reference Participant)
	/** The source participant description. */
	private String sourceParticipantMnemonic;			//BlockOperation	(reference Participant)
	
	/** The target participant description. */
	private String targetParticipantDescription;		//BlockOperation	(reference Participant)
	
	/** The target participant mnemonic. */
	private String targetParticipantMnemonic;			//BlockOperation	(reference Participant)
	
	/** The transfer type. */
	private Integer transferType;			//--

	/** The block type. */
	private Integer blockType;				//RequestOperation
	
	/** The block type description. */
	private String blockTypeDescription;	//RequestOperation

	/** The operation date. */
	private Date operationDate;				//CustodyOperation
	
	/** The operation init date. */
	private Date operationInitDate;			//CustodyOperation
	
	/** The operation end date. */
	private Date operationEndDate;			//CustodyOperation
	
	/** The operation number. */
	private Long operationNumber;			//CustodyOperation
	
	/** The operation type. */
	private Long operationType;				//CustodyOperation

	/** The idblock entity pk. */
	public Long   idblockEntityPk;
	
	/** The block entity description. */
	public String blockEntityDescription;
	
	/** The state. */
	private Integer state;					//---
	
	/** The state description. */
	private String stateDescription;

	//opcionales
	/** The id custody operation pk. */
	private Long idCustodyOperationPk;		//CustodyOperation	= pk_SecurityTransferOperation 
	
	/** The id transfer operation pk. */
	private Long idTransferOperationPk;
	
	/** The source id holder account pk. */
	private long sourceIdHolderAccountPk;	//BlockOperation	(reference HolderAccount)
	
	/** The target id holder account pk. */
	private long targetIdHolderAccountPk;	//BlockOperation	(reference HolderAccount)
	
	/** The id block request. */
	private Long idBlockRequest;			//RequestOperation
	
	/** The isin description. */
	private String isinDescription;			//SecurityTransferOperation (reference Security)
	
	/** The block leve desc. */
	private String blockLeveDesc;
	
	/** The block level. */
	private Integer blockLevel;
	
	/** The state old. */
	private Integer stateOld;
	
	/** The state new. */
	private Integer stateNew;
	
	/** The bussines process. */
	private Long bussinesProcess;
	
	/** The holder. */
	private Holder holder;
	
	/** The reject motive desc. */
	private String rejectMotiveDesc;
	
	/** The reject other. */
	private String rejectOther;
	
	/**
	 * Gets the id block operation pk.
	 *
	 * @return the id block operation pk
	 */
	public Long getIdBlockOperationPk() {
		return idBlockOperationPk;
	}
	
	/**
	 * Sets the id block operation pk.
	 *
	 * @param idBlockOperationPk the new id block operation pk
	 */
	public void setIdBlockOperationPk(Long idBlockOperationPk) {
		this.idBlockOperationPk = idBlockOperationPk;
	}
	
	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public Long getDocumentNumber() {
		return documentNumber;
	}
	
	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(Long documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the source account number.
	 *
	 * @return the source account number
	 */
	public Integer getSourceAccountNumber() {
		return sourceAccountNumber;
	}
	
	/**
	 * Sets the source account number.
	 *
	 * @param sourceAccountNumber the new source account number
	 */
	public void setSourceAccountNumber(Integer sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}
	
	/**
	 * Gets the target account number.
	 *
	 * @return the target account number
	 */
	public Integer getTargetAccountNumber() {
		return targetAccountNumber;
	}
	
	/**
	 * Sets the target account number.
	 *
	 * @param targetAccountNumber the new target account number
	 */
	public void setTargetAccountNumber(Integer targetAccountNumber) {
		this.targetAccountNumber = targetAccountNumber;
	}
	
	/**
	 * Gets the source id participant pk.
	 *
	 * @return the source id participant pk
	 */
	public Long getSourceIdParticipantPk() {
		return sourceIdParticipantPk;
	}
	
	/**
	 * Sets the source id participant pk.
	 *
	 * @param sourceIdParticipantPk the new source id participant pk
	 */
	public void setSourceIdParticipantPk(Long sourceIdParticipantPk) {
		this.sourceIdParticipantPk = sourceIdParticipantPk;
	}
	
	/**
	 * Gets the target id participant pk.
	 *
	 * @return the target id participant pk
	 */
	public Long getTargetIdParticipantPk() {
		return targetIdParticipantPk;
	}
	
	/**
	 * Sets the target id participant pk.
	 *
	 * @param targetIdParticipantPk the new target id participant pk
	 */
	public void setTargetIdParticipantPk(Long targetIdParticipantPk) {
		this.targetIdParticipantPk = targetIdParticipantPk;
	}
	
	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public Integer getTransferType() {
		return transferType;
	}
	
	/**
	 * Sets the transfer type.
	 *
	 * @param transferType the new transfer type
	 */
	public void setTransferType(Integer transferType) {
		this.transferType = transferType;
	}
	
	/**
	 * Gets the block type.
	 *
	 * @return the block type
	 */
	public Integer getBlockType() {
		return blockType;
	}
	
	/**
	 * Sets the block type.
	 *
	 * @param blockType the new block type
	 */
	public void setBlockType(Integer blockType) {
		this.blockType = blockType;
	}
	
	/**
	 * Gets the block type description.
	 *
	 * @return the block type description
	 */
	public String getBlockTypeDescription() {
		return blockTypeDescription;
	}
	
	/**
	 * Sets the block type description.
	 *
	 * @param blockTypeDescription the new block type description
	 */
	public void setBlockTypeDescription(String blockTypeDescription) {
		this.blockTypeDescription = blockTypeDescription;
	}
	
	/**
	 * Gets the operation init date.
	 *
	 * @return the operation init date
	 */
	public Date getOperationInitDate() {
		return operationInitDate;
	}
	
	/**
	 * Sets the operation init date.
	 *
	 * @param operationInitDate the new operation init date
	 */
	public void setOperationInitDate(Date operationInitDate) {
		this.operationInitDate = operationInitDate;
	}
	
	/**
	 * Gets the operation end date.
	 *
	 * @return the operation end date
	 */
	public Date getOperationEndDate() {
		return operationEndDate;
	}
	
	/**
	 * Sets the operation end date.
	 *
	 * @param operationEndDate the new operation end date
	 */
	public void setOperationEndDate(Date operationEndDate) {
		this.operationEndDate = operationEndDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return operationType;
	}
	
	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the id custody operation pk.
	 *
	 * @return the id custody operation pk
	 */
	public Long getIdCustodyOperationPk() {
		return idCustodyOperationPk;
	}
	
	/**
	 * Sets the id custody operation pk.
	 *
	 * @param idCustodyOperationPk the new id custody operation pk
	 */
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}
	
	/**
	 * Gets the source id holder account pk.
	 *
	 * @return the source id holder account pk
	 */
	public long getSourceIdHolderAccountPk() {
		return sourceIdHolderAccountPk;
	}
	
	/**
	 * Sets the source id holder account pk.
	 *
	 * @param sourceIdHolderAccountPk the new source id holder account pk
	 */
	public void setSourceIdHolderAccountPk(long sourceIdHolderAccountPk) {
		this.sourceIdHolderAccountPk = sourceIdHolderAccountPk;
	}
	
	/**
	 * Gets the target id holder account pk.
	 *
	 * @return the target id holder account pk
	 */
	public long getTargetIdHolderAccountPk() {
		return targetIdHolderAccountPk;
	}
	
	/**
	 * Sets the target id holder account pk.
	 *
	 * @param targetIdHolderAccountPk the new target id holder account pk
	 */
	public void setTargetIdHolderAccountPk(long targetIdHolderAccountPk) {
		this.targetIdHolderAccountPk = targetIdHolderAccountPk;
	}
	
	/**
	 * Gets the id block request.
	 *
	 * @return the id block request
	 */
	public Long getIdBlockRequest() {
		return idBlockRequest;
	}
	
	/**
	 * Sets the id block request.
	 *
	 * @param idBlockRequest the new id block request
	 */
	public void setIdBlockRequest(Long idBlockRequest) {
		this.idBlockRequest = idBlockRequest;
	}
	
	/**
	 * Gets the isin description.
	 *
	 * @return the isin description
	 */
	public String getIsinDescription() {
		return isinDescription;
	}
	
	/**
	 * Sets the isin description.
	 *
	 * @param isinDescription the new isin description
	 */
	public void setIsinDescription(String isinDescription) {
		this.isinDescription = isinDescription;
	}
	
	/**
	 * Gets the actual block amount.
	 *
	 * @return the actual block amount
	 */
	public BigDecimal getActualBlockAmount() {
		return actualBlockAmount;
	}
	
	/**
	 * Sets the actual block amount.
	 *
	 * @param actualBlockAmount the new actual block amount
	 */
	public void setActualBlockAmount(BigDecimal actualBlockAmount) {
		this.actualBlockAmount = actualBlockAmount;
	}
	
	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}
	
	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}
	
	/**
	 * Gets the block entity description.
	 *
	 * @return the block entity description
	 */
	public String getBlockEntityDescription() {
		return blockEntityDescription;
	}
	
	/**
	 * Sets the block entity description.
	 *
	 * @param blockEntityDescription the new block entity description
	 */
	public void setBlockEntityDescription(String blockEntityDescription) {
		this.blockEntityDescription = blockEntityDescription;
	}
	
	/**
	 * Gets the block leve desc.
	 *
	 * @return the block leve desc
	 */
	public String getBlockLeveDesc() {
		return blockLeveDesc;
	}
	
	/**
	 * Sets the block leve desc.
	 *
	 * @param blockLeveDesc the new block leve desc
	 */
	public void setBlockLeveDesc(String blockLeveDesc) {
		this.blockLeveDesc = blockLeveDesc;
	}
	
	/**
	 * Gets the actual block balance.
	 *
	 * @return the actual block balance
	 */
	public BigDecimal getActualBlockBalance() {
		return actualBlockBalance;
	}
	
	/**
	 * Sets the actual block balance.
	 *
	 * @param actualBlockBalance the new actual block balance
	 */
	public void setActualBlockBalance(BigDecimal actualBlockBalance) {
		this.actualBlockBalance = actualBlockBalance;
	}
	
	/**
	 * Gets the source participant description.
	 *
	 * @return the source participant description
	 */
	public String getSourceParticipantDescription() {
		return sourceParticipantDescription;
	}
	
	/**
	 * Sets the source participant description.
	 *
	 * @param sourceParticipantDescription the new source participant description
	 */
	public void setSourceParticipantDescription(String sourceParticipantDescription) {
		this.sourceParticipantDescription = sourceParticipantDescription;
	}
	
	/**
	 * Gets the target participant description.
	 *
	 * @return the target participant description
	 */
	public String getTargetParticipantDescription() {
		return targetParticipantDescription;
	}
	
	/**
	 * Sets the target participant description.
	 *
	 * @param targetParticipantDescription the new target participant description
	 */
	public void setTargetParticipantDescription(String targetParticipantDescription) {
		this.targetParticipantDescription = targetParticipantDescription;
	}
	
	/**
	 * Gets the id holder pk.
	 *
	 * @return the id holder pk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}
	
	/**
	 * Sets the id holder pk.
	 *
	 * @param idHolderPk the new id holder pk
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}
	
	/**
	 * Gets the state description.
	 *
	 * @return the state description
	 */
	public String getStateDescription() {
		return stateDescription;
	}
	
	/**
	 * Sets the state description.
	 *
	 * @param stateDescription the new state description
	 */
	public void setStateDescription(String stateDescription) {
		this.stateDescription = stateDescription;
	}
	
	/**
	 * Gets the state old.
	 *
	 * @return the state old
	 */
	public Integer getStateOld() {
		return stateOld;
	}
	
	/**
	 * Sets the state old.
	 *
	 * @param stateOld the new state old
	 */
	public void setStateOld(Integer stateOld) {
		this.stateOld = stateOld;
	}
	
	/**
	 * Gets the state new.
	 *
	 * @return the state new
	 */
	public Integer getStateNew() {
		return stateNew;
	}
	
	/**
	 * Sets the state new.
	 *
	 * @param stateNew the new state new
	 */
	public void setStateNew(Integer stateNew) {
		this.stateNew = stateNew;
	}
	
	/**
	 * Gets the bussines process.
	 *
	 * @return the bussines process
	 */
	public Long getBussinesProcess() {
		return bussinesProcess;
	}
	
	/**
	 * Sets the bussines process.
	 *
	 * @param bussinesProcess the new bussines process
	 */
	public void setBussinesProcess(Long bussinesProcess) {
		this.bussinesProcess = bussinesProcess;
	}
	
	/**
	 * Gets the id transfer operation pk.
	 *
	 * @return the id transfer operation pk
	 */
	public Long getIdTransferOperationPk() {
		return idTransferOperationPk;
	}
	
	/**
	 * Sets the id transfer operation pk.
	 *
	 * @param idTransferOperationPk the new id transfer operation pk
	 */
	public void setIdTransferOperationPk(Long idTransferOperationPk) {
		this.idTransferOperationPk = idTransferOperationPk;
	}
	
	/**
	 * Gets the idblock entity pk.
	 *
	 * @return the idblock entity pk
	 */
	public Long getIdblockEntityPk() {
		return idblockEntityPk;
	}
	
	/**
	 * Sets the idblock entity pk.
	 *
	 * @param idblockEntityPk the new idblock entity pk
	 */
	public void setIdblockEntityPk(Long idblockEntityPk) {
		this.idblockEntityPk = idblockEntityPk;
	}
	
	/**
	 * Gets the block level.
	 *
	 * @return the block level
	 */
	public Integer getBlockLevel() {
		return blockLevel;
	}
	
	/**
	 * Sets the block level.
	 *
	 * @param blockLevel the new block level
	 */
	public void setBlockLevel(Integer blockLevel) {
		this.blockLevel = blockLevel;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the source holder account.
	 *
	 * @return the source holder account
	 */
	public HolderAccount getSourceHolderAccount() {
		return sourceHolderAccount;
	}

	/**
	 * Sets the source holder account.
	 *
	 * @param sourceHolderAccount the new source holder account
	 */
	public void setSourceHolderAccount(HolderAccount sourceHolderAccount) {
		this.sourceHolderAccount = sourceHolderAccount;
	}

	/**
	 * Gets the source participant mnemonic.
	 *
	 * @return the source participant mnemonic
	 */
	public String getSourceParticipantMnemonic() {
		return sourceParticipantMnemonic;
	}

	/**
	 * Sets the source participant mnemonic.
	 *
	 * @param sourceParticipantMnemonic the new source participant mnemonic
	 */
	public void setSourceParticipantMnemonic(String sourceParticipantMnemonic) {
		this.sourceParticipantMnemonic = sourceParticipantMnemonic;
	}

	/**
	 * Gets the target participant mnemonic.
	 *
	 * @return the target participant mnemonic
	 */
	public String getTargetParticipantMnemonic() {
		return targetParticipantMnemonic;
	}

	/**
	 * Sets the target participant mnemonic.
	 *
	 * @param targetParticipantMnemonic the new target participant mnemonic
	 */
	public void setTargetParticipantMnemonic(String targetParticipantMnemonic) {
		this.targetParticipantMnemonic = targetParticipantMnemonic;
	}

	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the reject motive desc.
	 *
	 * @return the reject motive desc
	 */
	public String getRejectMotiveDesc() {
		return rejectMotiveDesc;
	}

	/**
	 * Sets the reject motive desc.
	 *
	 * @param rejectMotiveDesc the new reject motive desc
	 */
	public void setRejectMotiveDesc(String rejectMotiveDesc) {
		this.rejectMotiveDesc = rejectMotiveDesc;
	}

	/**
	 * Gets the reject other.
	 *
	 * @return the reject other
	 */
	public String getRejectOther() {
		return rejectOther;
	}

	/**
	 * Sets the reject other.
	 *
	 * @param rejectOther the new reject other
	 */
	public void setRejectOther(String rejectOther) {
		this.rejectOther = rejectOther;
	}
	
}
