package com.pradera.custody.tranfersecurities.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.SecuritiesTransferType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;



// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransferSecuritiesMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class TransferSecuritiesMgmtBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The max days of custody request. */
	@Inject 
	@Configurable Integer maxDaysOfCustodyRequest;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	
	/** The transfer available facade. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailableFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/**  JH *. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/**  JH *. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The flag search participant. */
	private boolean flagSearchParticipant = false;
	
	/** The flag register origin participant. */
	private boolean flagRegisterOriginParticipant = false;
	
	/** The flag register target participant. */
	private boolean flagRegisterTargetParticipant = false;
	
	/** The flag account origin visible. */
	private boolean flagAccountOriginVisible = false;
	
	/** The flag account target visible. */
	private boolean flagAccountTargetVisible = false;

	/** The str document number. */
	private String strDocumentNumber;
	
	/** The str sate description. */
	private String strSateDescription;
	
	/** The confirm alert action. */
	private String confirmAlertAction="";
	
	/** The flag participante investor. */
	private boolean flagParticipanteInvestor = false;
	
	/** The custody operation. */
	private CustodyOperation custodyOperation = new CustodyOperation();

	/** The lst transfer type. */
	private List<ParameterTable> lstTransferType = new ArrayList<ParameterTable>();
	
	/** The lst instrument type. */
	private List<ParameterTable> lstInstrumentType = new ArrayList<ParameterTable>();

	/** The lst instrument type. */
	private List<ParameterTable> lstSecurityClass = new ArrayList<ParameterTable>();

	/** The lst instrument type. */
	private List<ParameterTable> lstSecurityType = new ArrayList<ParameterTable>();
	
	/** The lst transfer state. */
	private List<ParameterTable> lstTransferState = new ArrayList<ParameterTable>();

	/** The lst transfer state. */
	private List<ParameterTable> lstHolderAccountState = new ArrayList<ParameterTable>();
	
	/** The lst transfer actions. */
	private List<ParameterTable> lstTransferActions = new ArrayList<ParameterTable>();
	
	/** The lst participant. */
	private List<Participant> lstParticipant = new ArrayList<Participant>();
	
	/** The lst target participant. */
	private List<Participant> lstTargetParticipant;
	
	/** The reject motive list. */
	private List<ParameterTable> rejectMotiveList;
	
	/** The other reject motive id. */
	private Integer 	otherRejectMotiveId;
	
	/** The int reject motive. */
	private Integer 	intRejectMotive;
	

	/** The str reject motive. */
	private String  	strRejectMotive;
	
	/** The participant login. */
	private Long 		participantLogin = new Long(0);
	
	/** The Defaultparticipant. */
	private Participant Defaultparticipant = new Participant();

	/** The now time. */
	private Date nowTime = new Date();			/**  chekear donde se usa ***. */
	private Date minTime = new Date();
	
	/** The is participant. */
	private boolean isParticipant = false;
	
	/** The is depositary. */
	private boolean isDepositary = false;
	
	/** The check onli available. */
	private boolean checkOnliAvailable = false;
	
	/** The radio selected. */
	private Integer radioSelected=1;
	
	/** The registers found. */
	private boolean registersFound = true;
	
	/** The flag same participant. */
	private boolean flagSameParticipant = false;
	
	/** The flag target source. */
	private boolean flagTargetSource = false;
	
	/** The flag source target. */
	private boolean flagSourceTarget = true;
	
	/** The flag review target. */
	private boolean flagReviewTarget = true;
	
	/** The flag review source. */
	private boolean flagReviewSource = true;
	
	/** The flag target origin. */
	private boolean flagTargetOrigin = false;
	
	/** The validation message. */
	private String validationMessage = "";
	
	/** The Constant STR_APPROVE. */
	private static final String STR_APPROVE = "approve";
	
	/** The Constant STR_REJECT. */
	private static final String STR_REJECT = "reject";
	
	/** The Constant STR_REVIEW. */
	private static final String STR_REVIEW = "review";
	
	/** The Constant STR_ANNULAR. */
	private static final String STR_ANNULAR = "annular";
	
	/** The Constant STR_CONFIRM. */
	private static final String STR_CONFIRM = "confirm";
	
	/** The Constant INT_OTHER_MOTIVE_REJECT. */
	private static final Integer INT_OTHER_MOTIVE_REJECT = new Integer(1718);
	
	/*Botones de privilegios*/
	/** The flag revised sto. */
	boolean flagRevisedSto = false;
	
	/** The flag reject sto. */
	boolean flagRejectSto = false;
	
	/** The flag approved sto. */
	boolean flagApprovedSto = false;
	
	/** The flasg annuled sto. */
	boolean flasgAnnuledSto = false;
	
	/** The flag confirm sto. */
	boolean flagConfirmSto = false;
	
	/** The flag register. */
	boolean flagRegister = false;
	
	/** The flag search. */
	boolean flagSearch = false;
	
	/** The flag view market fact. */
	private boolean flagViewMarketFact = true;
	
	/*busqueda*/
	/** The lst transfer security available. */
	List<SecurityTransferOperation> lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
	
	/** The data model transfer security available. */
	private  GenericDataModel<SecurityTransferOperation> dataModelTransferSecurityAvailable;
	
	/** The arr security transfer operation. */
	private SecurityTransferOperation[] arrSecurityTransferOperation;
	
	/** The search security transfer operation. */
	private SecurityTransferOperation 	searchSecurityTransferOperation = new SecurityTransferOperation();
	
	/** The last serch security transfer operation. */
	private SecurityTransferOperation 	lastSerchSecurityTransferOperation = new SecurityTransferOperation();
	/*new*/
	/** The security transfer operation. */
	private SecurityTransferOperation securityTransferOperation = new SecurityTransferOperation();
	
	/** The lst holder account balance. */
	private List<HolderAccountBalance> lstHolderAccountBalance= new ArrayList<HolderAccountBalance>();
	
	/** The data model holder account balance. */
	private GenericDataModel<HolderAccountBalance> dataModelHolderAccountBalance;
	
	/** The map transfer marketfact. */
	private Map<String, List<HolderMarketFactBalance>> mapTransferMarketfact = new HashMap<String, List<HolderMarketFactBalance>>();

	/** The security transfer operation tmp. */
	private SecurityTransferOperation securityTransferOperationTmp = new SecurityTransferOperation();

	/** The arr holder account balance. */
	private HolderAccountBalance[] arrHolderAccountBalance;
	
	/** The holder account balance. */
	private HolderAccountBalance holderAccountBalance = new HolderAccountBalance();

	/** The lst action transfer security available. */
	List<SecurityTransferOperation> lstActionTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
	
	/* Mapas de estado */
	/** The map state security transfer operation. */
	Map<Integer, String> mapStateSecurityTransferOperation = new HashMap<Integer,String>();
	
	/** The map state holder account. */
	Map<Integer, String> mapStateHolderAccount = new HashMap<Integer,String>();
	
	/** The map security instrument type. */
	Map<Integer, String> mapSecurityInstrumentType = new HashMap<Integer,String>();
	
	/** The map security type. */
	Map<Integer, String> mapSecurityType = new HashMap<Integer,String>();
	
	/** The source holder. */
	/* Para los helpers */
	private Holder sourceHolder = new Holder();
	
	/** The target holder. */
	private Holder targetHolder = new Holder();
	
	/** The operation number market face. */
	private Long   operationNumberMarketFace = new Long(0);

	/** The source account to. */
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	
	/** The target account to. */
	private HolderAccountHelperResultTO targetAccountTo = new HolderAccountHelperResultTO();
	
	/** The lst source account to. */
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	/** The lst target account to. */
	private List<HolderAccountHelperResultTO> lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();

	/** The map lst source account to. */
	private Map<Long, List<HolderAccountHelperResultTO>> mapLstSourceAccountTo = new HashMap<Long, List<HolderAccountHelperResultTO>>();
	
	/** The map lst target account to. */
	private Map<Long, List<HolderAccountHelperResultTO>> mapLstTargetAccountTo = new HashMap<Long, List<HolderAccountHelperResultTO>>();
	
	
	/** The flag ind market fact. */
	boolean flagIndMarketFact = false;
	/** The str document number. */
	private String viewErrorMessage="";
	
	/** The style btn active. */
	private String styleBtnActive = "buttonGreen";
	
	/** The style btn pending. */
	private String styleBtnPending = "buttonRed";
	
	/** The str ui component market fact. */
	private String strUiComponentMarketFact = "balancemarket1";
	
	/** The holder market fact balance. */
	private MarketFactBalanceHelpTO holderMarketFactBalance;
	
	/** The int hidden motive. */
	private Integer intHiddenMotive = 0;
	
	/** The list operation number. */
	private String listOperationNumber;

	/** The review all. */
	private boolean reviewAll;
	
	Boolean useMarketFact = Boolean.FALSE; // RCH - Flag para usar marketfact en esta validacion. Para PGY no se va a usar
	
	/** The help market fact balance view. */
	MarketFactBalanceHelpTO helpMarketFactBalanceView = new MarketFactBalanceHelpTO();
	/**
	 * Post constructor new.
	 *
	 * @return the string
	 */
	//@LoggerAuditWeb
	public String postConstructorNew(){
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
				&& Validations.validateIsNullOrNotPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk())){
			validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
			return "";
		}
		JSFUtilities.setValidViewComponentAndChildrens("frmCustody");
		JSFUtilities.setValidViewComponent("frmCustody:idCuiTargetHolder:idCuiTargetHolder");
		JSFUtilities.setValidViewComponent("frmCustody:idCuiSourceHolder:idCuiSourceHolder");
		initScreenNew();
		return "newTransferAvailableView";
	}
	
	/**
	 * Clean registry screen.
	 */
	public void cleanRegistryScreen(){
		JSFUtilities.setValidViewComponentAndChildrens("frmCustody");
		JSFUtilities.setValidViewComponent("frmCustody:idCuiTargetHolder:idCuiTargetHolder");
		JSFUtilities.setValidViewComponent("frmCustody:idCuiSourceHolder:idCuiSourceHolder");
		initScreenNew();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Inits the screen new.
	 */
	public void initScreenNew(){
		lstHolderAccountBalance = null;
		dataModelHolderAccountBalance = null;
		flagTargetSource = false;
		flagSameParticipant = false;
		flagSourceTarget = true;
		registersFound=true;
		loadCombos();
		securityEvents();
		securityTransferOperation = inicializeSecurityTransferOperation();
		//securityTransferOperation.getSecurities().setSecurityClass(SecurityClassType.DPF.getCode());
		securityTransferOperation.setTransferType(SecuritiesTransferType.ORIGEN_DESTINO.getCode());
		//se agrego por los helpers
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		sourceAccountTo = new HolderAccountHelperResultTO();
		targetAccountTo = new HolderAccountHelperResultTO();
		sourceHolder = new Holder();
		targetHolder = new Holder();
		checkOnliAvailable = true;
//		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion() || 
//				userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
//			checkOnliAvailable = true;
//		}else {
//			checkOnliAvailable = false;
//		}
		
	}
	
	/**
	 * Inicialize security transfer operation.
	 *
	 * @return the security transfer operation
	 */
	public SecurityTransferOperation inicializeSecurityTransferOperation(){
		securityTransferOperation = new SecurityTransferOperation();
		CustodyOperation custodyOperation = new CustodyOperation();
		Participant participant = new Participant();
		Participant sourceParticipant = new Participant();
		Participant targetParticipant = new Participant();
		Security security = new Security();
		HolderAccount holderAccount = new HolderAccount();
		HolderAccount sourceHolderAccount = new HolderAccount();
		HolderAccount targetHolderAccount = new HolderAccount();
		
		securityTransferOperation.setCustodyOperation(custodyOperation);
		securityTransferOperation.setHolderAccount(holderAccount);
		securityTransferOperation.setSourceHolderAccount(sourceHolderAccount);
		securityTransferOperation.setTargetHolderAccount(targetHolderAccount);
		securityTransferOperation.setSecurities(security);
		securityTransferOperation.setSecurityClass(null);
		
		if(flagSearchParticipant && isParticipant){
			flagRegisterOriginParticipant = true;
			flagRegisterTargetParticipant = false;
			securityTransferOperation.setParticipant(Defaultparticipant);
		}else{
			securityTransferOperation.setParticipant(participant);
		}
		
		if(flagRegister && isParticipant){
			flagRegisterOriginParticipant = true;
			flagRegisterTargetParticipant = false;
			securityTransferOperation.setSourceParticipant(Defaultparticipant);
		}else{
			securityTransferOperation.setSourceParticipant(sourceParticipant);
		}
		
		securityTransferOperation.setTargetParticipant(targetParticipant);
		securityTransferOperation.getCustodyOperation().setOperationInit(CommonsUtilities.currentDate());
		securityTransferOperation.getCustodyOperation().setOperationEnd(CommonsUtilities.currentDate());
		securityTransferOperation.setTransferType(SecuritiesTransferType.ORIGEN_DESTINO.getCode());
		return securityTransferOperation;
	}
	
	/**
	 * Sets the ting last search.
	 *
	 * @param securityTransferOperation the new ting last search
	 */
	public void settingLastSearch(SecurityTransferOperation securityTransferOperation){
		lastSerchSecurityTransferOperation.setCustodyOperation(securityTransferOperation.getCustodyOperation());
		lastSerchSecurityTransferOperation.setHolderAccount(securityTransferOperation.getHolderAccount());
		lastSerchSecurityTransferOperation.setSourceHolderAccount(securityTransferOperation.getSourceHolderAccount());
		lastSerchSecurityTransferOperation.setTargetHolderAccount(securityTransferOperation.getTargetHolderAccount());
		lastSerchSecurityTransferOperation.setParticipant(securityTransferOperation.getParticipant());
		lastSerchSecurityTransferOperation.setSourceParticipant(securityTransferOperation.getSourceParticipant());
		lastSerchSecurityTransferOperation.setTargetParticipant(securityTransferOperation.getTargetParticipant());
	}
	
	/**
	 * Load securities with account.
	 */
	public void loadSecuritiesWithAccount(){
		
		if( Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()) ){
			HolderAccountBalance hab = new HolderAccountBalance();
			HolderAccountBalancePK id = new HolderAccountBalancePK();
			id.setIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
			id.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			id.setIdSecurityCodePk(lstHolderAccountBalance.get(0).getSecurity().getIdSecurityCodePk());
			hab.setId(id);
			BigDecimal bigTransferAmmount = lstHolderAccountBalance.get(0).getTransferAmmount();
			hab.setTransferAmmount(bigTransferAmmount);
			List<HolderAccountBalance> lstTmpData = new ArrayList<HolderAccountBalance>();
			try {
				lstTmpData = transferAvailableFacade.getListHolderAccountBalance(hab, false, new Security(),userInfo);
			} catch (ServiceException e) {
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			}
			
			if(lstTmpData.size()>0){
				lstHolderAccountBalance = lstTmpData;
				dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
			}else{
				String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_SECURITY_NO_RELATION, new Object[]{securityTransferOperation.getSourceHolderAccount().getAccountNumber()});
				alert(message);
				securityTransferOperation.setSourceHolderAccount(new HolderAccount());
				lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
			}
		}
		
	}

	
	/**
	 * Valid securitie origin.
	 */
	public void validSecuritieOrigin(){
		try {
			
			if(Validations.validateIsNullOrNotPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_SOURCE));
				securityTransferOperation.setSecurities(new Security());
				return;
			}
			
			if(Validations.validateIsNotNull(securityTransferOperation.getSecurities()) && 
			   Validations.validateIsNotNull(securityTransferOperation.getSecurities().getIdSecurityCodePk())){
				
				if(!(securityTransferOperation.getSecurities().isRegisteredState()
						|| securityTransferOperation.getSecurities().isGuardaExclusive())){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID_GUARDA_EXCLUSIVE));
					securityTransferOperation.setSecurities(new Security());
					return;
				}
				
				//verifico si el valor le pertenece al participante y cuenta	
				HolderAccountBalance filter = new HolderAccountBalance();
				HolderAccountBalancePK id = new HolderAccountBalancePK();
				id.setIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
				id.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
				id.setIdSecurityCodePk(securityTransferOperation.getSecurities().getIdSecurityCodePk());
				filter.setId(id);
				
				if(transferAvailableFacade.getExistSecurityForHolder(filter) <= 0){
					securityTransferOperation.setSecurities(new Security());
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_NO_RELATION_PARTICIPANT_AND_ACCOUNT));
					return;
				}
			
				//validate if it is correct instrument type
				Integer secCount = transferAvailableFacade.validSecurityWithSecurityClass(
						securityTransferOperation.getSecurities().getIdSecurityCodePk(),
						securityTransferOperation.getSecurities().getSecurityClass());
				if(secCount<=0){
					securityTransferOperation.setSecurities(new Security());
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_SECURITY_INVALID_INSTRUMENT_TYPE));
				}
			}
			dataTableRegistry();
		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}
	}
	
	/**
	 * Reset securities.
	 */
	public void resetSecurities(){

		Integer securityClass = securityTransferOperation.getSecurities().getSecurityClass();
		securityTransferOperation.setSecurities(new Security());
		securityTransferOperation.getSecurities().setSecurityClass(securityClass);
	}
	
	
	/**
	 * Selected radio.
	 *
	 * @param e the e
	 */
	public void selectedRadio(SelectEvent e){
		SecurityTransferOperation securityTransferOperation = (SecurityTransferOperation)e.getObject();
		this.securityTransferOperation.setState(securityTransferOperation.getState());
	}
	
	/**
	 * Gets the complement holder account.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @return the complement holder account
	 */
	public HolderAccount getComplementHolderAccount(SecurityTransferOperation securityTransferOperation){
		HolderAccount holderAccount = new HolderAccount();
		try {
			holderAccount = transferAvailableFacade.getComplementHolderAccount(securityTransferOperation);
		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}
		return holderAccount;
	}
	

	/**
	 * Adds the blank holder origin balances.
	 */
	public void AddBlankHolderOriginBalances(){
		if(flagTargetSource && !(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) && 
		   Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));					
			/*securityTransferOperation.setTargetHolderAccount(new HolderAccount());
			targetAccountTo = new HolderAccountHelperResultTO();*/
			return;
		}
		
		if( !(Validations.validateIsNotNull(securityTransferOperation.getSecurities()) &&
			  Validations.validateIsNotNull(securityTransferOperation.getSecurities().getIdSecurityCodePk()))
			){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_INSERT_SECURITY));
			return;
		}
		
		if(Validations.validateIsNull(lstHolderAccountBalance)){	/**Validation If not null the List: lstHolderOriginAccountBalance*/
			lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
		}else{
			for (int i = 0; i < lstHolderAccountBalance.size(); i++) {
				if( lstHolderAccountBalance.get(i).getId().getIdSecurityCodePk().equals(securityTransferOperation.getSecurities().getIdSecurityCodePk()) ){
					
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_EXIST_IN_REGISTRY,new Object[]{securityTransferOperation.getSecurities().getIdSecurityCodePk()}));
					securityTransferOperation.setSecurities(new Security());
					return;
				}
			}
		}
		
		for (int i = 0; i < lstHolderAccountBalance.size(); i++) {
			if(lstHolderAccountBalance.get(i).getTransferAmmount()==null){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AMOUNT_NOT_INSERT_IN_REGISTRY,new Object[]{lstHolderAccountBalance.get(i).getSecurity().getIdSecurityCodePk()}));
				return;
			}
		}
		
			
		HolderAccountBalance holderAccountOriginBalance = new HolderAccountBalance();
		holderAccountOriginBalance.setSecurity(securityTransferOperation.getSecurities());
		
		holderAccountOriginBalance.setHolderAccount(securityTransferOperation.getSourceHolderAccount());
		holderAccountOriginBalance.setParticipant(securityTransferOperation.getSourceParticipant());
		holderAccountOriginBalance.setTotalBalance(null);
		holderAccountOriginBalance.setAvailableBalance(null);
		holderAccountOriginBalance.setTransferAmmount(null);
		holderAccountOriginBalance.setMarketCount(GeneralConstants.ZERO_VALUE_LONG);
		
			HolderAccountBalancePK holderAccountBalancePK = new HolderAccountBalancePK();
			holderAccountBalancePK.setIdHolderAccountPk(Long.valueOf(lstHolderAccountBalance.size()+1));
			holderAccountBalancePK.setIdSecurityCodePk(securityTransferOperation.getSecurities().getIdSecurityCodePk());
			/*if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				holderAccountBalancePK.setIdParticipantPk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
			}else{*/
				holderAccountBalancePK.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			//}				
		holderAccountOriginBalance.setId(holderAccountBalancePK);
		lstHolderAccountBalance.add(holderAccountOriginBalance);
		dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
		
		//reset
		securityTransferOperation.setSecurities(new Security());
	}
	
	/**
	 * Valid the transfer Ammount <= Available Balance.
	 *
	 * @param holderAccountBalance the holder account balance
	 */
	@LoggerAuditWeb
	public void validTransferAmmount(HolderAccountBalance holderAccountBalance){
		if(Validations.validateIsNotNull(holderAccountBalance.getTransferAmmount())){
			validationMessage = transferAvailableFacade.isValidTheTransferAmmount(holderAccountBalance);
			if( !validationMessage.equals("") ){
				alert(validationMessage);
				holderAccountBalance.setTransferAmmount(null);
			}
		}else{
			holderAccountBalance.setTransferAmmount(null);
		}
	}
	
	/**
	 * Show market fact ui.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void showMarketFactUI(HolderAccountBalance objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getSecurity().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName(strUiComponentMarketFact);
		
		MarketFactBalanceHelpTO marketFactoResult = null;
		
		try {
			marketFactoResult = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
			marketFactoResult.setIndHandleDetails(BooleanType.YES.getCode());
			marketFactoResult.setUiComponentName(strUiComponentMarketFact);
			
			List<HolderMarketFactBalance> mapList = mapTransferMarketfact.get(objHolAccBalTO.getSecurity().getIdSecurityCodePk());
			if( mapList!= null && mapList.size()>0 ){
				BigDecimal EnteredBalanceTotal = new BigDecimal(0);
				for(MarketFactDetailHelpTO obj: marketFactoResult.getMarketFacBalances() ){
					for(HolderMarketFactBalance mapa: mapList){
						if(obj.getMarketDate().equals(mapa.getMarketDate()) ){
							if(Validations.validateIsNotNull(obj.getMarketRate()) && Validations.validateIsNotNull(mapa.getMarketRate())){
								if(obj.getMarketRate().equals(mapa.getMarketRate())){
									obj.setEnteredBalance(mapa.getTransferAmmount());
									obj.setIsSelected(true);
									EnteredBalanceTotal = EnteredBalanceTotal.add(mapa.getTransferAmmount());
								}
							}else if(Validations.validateIsNotNull(obj.getMarketPrice()) && Validations.validateIsNotNull(mapa.getMarketPrice())){
								if(obj.getMarketPrice().equals(mapa.getMarketPrice())){
									obj.setEnteredBalance(mapa.getTransferAmmount());
									obj.setIsSelected(true);
									EnteredBalanceTotal = EnteredBalanceTotal.add(mapa.getTransferAmmount());
								}
							}
						}
					}
					
				}
				marketFactoResult.setBalanceResult(EnteredBalanceTotal);
			}else{
				if(Validations.validateIsNotNull(objHolAccBalTO.getTransferAmmount())){
					marketFactoResult.setBalanceResult(objHolAccBalTO.getTransferAmmount());
				}
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			switch(e.getErrorService()){
			case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						  PropertiesUtilities.getExceptionMessage(e.getMessage()));
					
			default:
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
			}
			return;
		}
		
		if( marketFactoResult.getMarketFacBalances().size() == 1 ){
			holderMarketFactBalance = marketFactoResult;
			objHolAccBalTO.setDisabled(false);
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactoResult); 
		}else{
			//objHolAccBalTO.setDisabled(true);
			objHolAccBalTO.setDisabled(false);
			holderMarketFactBalance = null;
			//marketFactBalance
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactoResult); 
		}
		
	}
	
	/**
	 * Holder market fact input.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void holderMarketFactInput(HolderAccountBalance objHolAccBalTO){
		
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getId().getIdSecurityCodePk());
		if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
			marketFactBalance.setHolderAccountPk(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk());
			marketFactBalance.setParticipantPk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
		}
		else{
			marketFactBalance.setHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
			marketFactBalance.setParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
		}
		marketFactBalance.setUiComponentName(strUiComponentMarketFact);
		
		try {
			holderMarketFactBalance = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			switch(e.getErrorService()){
			case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						  PropertiesUtilities.getExceptionMessage(e.getMessage()));
					
			default:
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
			}

			holderMarketFactBalance = new MarketFactBalanceHelpTO();
			return;
		}
		
	}
	
	/**
	 * Input market fact balance.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void inputMarketFactBalance(HolderAccountBalance objHolAccBalTO){
		
		if( !(Validations.validateIsNotNull(objHolAccBalTO.getTransferAmmount()) &&
			  Validations.validateIsNotNullAndPositive(objHolAccBalTO.getTransferAmmount().longValue())) ){
			
			objHolAccBalTO.setStyleClassButton(styleBtnPending);
			objHolAccBalTO.setTransferAmmount(null);
			if(!securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()))
			{
				mapTransferMarketfact.remove(objHolAccBalTO.getId().getIdSecurityCodePk());
				holderMarketFactInput(objHolAccBalTO);	//setteo los datos (holderMarketFactBalance)
				MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
				if(Validations.validateIsNotNull(marketFactBalance)){
					if(marketFactBalance.getMarketFacBalances() != null && marketFactBalance.getMarketFacBalances().size()==1){
						objHolAccBalTO.setStyleClassButton(styleBtnActive);
						objHolAccBalTO.setDisabled(false);
					}
				}
			}
			else{
				objHolAccBalTO.setStyleClassButton(styleBtnActive);
				objHolAccBalTO.setDisabled(false);
			}
			return;
		}
			
		if(Validations.validateIsNotNull(objHolAccBalTO.getAvailableBalance())){
			if( objHolAccBalTO.getAvailableBalance().compareTo(objHolAccBalTO.getTransferAmmount()) == 1 || 
				objHolAccBalTO.getAvailableBalance().compareTo(objHolAccBalTO.getTransferAmmount()) == 0){

			mapTransferMarketfact.remove(objHolAccBalTO.getId().getIdSecurityCodePk());
			holderMarketFactInput(objHolAccBalTO);	//setteo los datos (holderMarketFactBalance)
			MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
			
			if(marketFactBalance.getMarketFacBalances() != null && marketFactBalance.getMarketFacBalances().size()==1){

				List<HolderMarketFactBalance> lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
				HolderMarketFactBalance holderMarketfactBalance;
				
				MarketFactDetailHelpTO marketFact= marketFactBalance.getMarketFacBalances().get(0);
					
				holderMarketfactBalance = new HolderMarketFactBalance();
				holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
				
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
				holderMarketfactBalance.setHolderAccount(holderAccount);
				
				Security security = new Security();
				security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
				holderMarketfactBalance.setSecurity(security);
				
				Participant participant = new Participant();
				participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
				holderMarketfactBalance.setParticipant(participant);
				
				holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
				holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
				holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
				holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
				holderMarketfactBalance.setTransferAmmount(objHolAccBalTO.getTransferAmmount());
				
				lstHolderAccountMF.add(holderMarketfactBalance);
				mapTransferMarketfact.put(marketFactBalance.getSecurityCodePk(), lstHolderAccountMF);
				objHolAccBalTO.setStyleClassButton(styleBtnActive);
				objHolAccBalTO.setDisabled(false);
			}else{
				objHolAccBalTO.setStyleClassButton(styleBtnPending);
				objHolAccBalTO.setDisabled(true);
			}
			
			objHolAccBalTO.setDisabled(false);
			for (HolderAccountBalance hab: lstHolderAccountBalance) {
				if(hab.getSecurity().getIdSecurityCodePk().equals(objHolAccBalTO.getId().getIdSecurityCodePk())){
					hab.setTransferAmmount(objHolAccBalTO.getTransferAmmount());
					break;
				}
			} 
			dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
			
			JSFUtilities.updateComponent("frmCustody:pnlRegisterFound");

		}else{
			
			for (HolderAccountBalance hab: lstHolderAccountBalance) {
				//el participante y la cuenta siempre van a ser iguales
				if(hab.getSecurity().getIdSecurityCodePk().equals(objHolAccBalTO.getSecurity().getIdSecurityCodePk())){
					hab.setTransferAmmount(null);
					hab.setDisabled(true);
					hab.setStyleClassButton(styleBtnPending);
					alert(PropertiesUtilities.getMessage(PropertiesConstants.AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT));
					break;
				}
			}

			dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
			mapTransferMarketfact.remove(objHolAccBalTO.getSecurity().getIdSecurityCodePk());
			JSFUtilities.updateComponent("frmCustody:pnlRegisterFound");
		}
		}
	}
	
	
	/**
	 * Show market fact ui review.
	 *
	 * @param objHolAccBalTO the obj hol acc bal to
	 */
	public void showMarketFactUIReview(HolderAccountBalance objHolAccBalTO){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		
		marketFactBalance.setSecurityCodePk(objHolAccBalTO.getId().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName(strUiComponentMarketFact);
		
		MarketFactBalanceHelpTO marketFactoResult = null;
		try {
			marketFactoResult = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
			marketFactoResult.setIndHandleDetails(BooleanType.YES.getCode());
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			switch(e1.getErrorService()){
			case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						  PropertiesUtilities.getExceptionMessage(e1.getMessage()));
					
			default:
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
			}
			return;
		}
		
		if( marketFactoResult.getMarketFacBalances() != null && marketFactoResult.getMarketFacBalances().size() >0 ){
			marketFactoResult.setUiComponentName(strUiComponentMarketFact);
			List<HolderMarketFactBalance> mapList = mapTransferMarketfact.get(objHolAccBalTO.getSecurity().getIdSecurityCodePk());

			if( mapList!= null && mapList.size()>0 ){
				BigDecimal EnteredBalanceTotal = new BigDecimal(0);
				for(MarketFactDetailHelpTO obj: marketFactoResult.getMarketFacBalances() ){
					for(HolderMarketFactBalance mapa: mapList){
						if(obj.getMarketDate().equals(mapa.getMarketDate()) ){
							if(Validations.validateIsNotNull(obj.getMarketRate()) && Validations.validateIsNotNull(mapa.getMarketRate())){
								if(obj.getMarketRate().equals(mapa.getMarketRate())){
									obj.setEnteredBalance(mapa.getTransferAmmount());
									obj.setIsSelected(true);
									EnteredBalanceTotal = EnteredBalanceTotal.add(mapa.getTransferAmmount());
								}
							}
							if(Validations.validateIsNotNull(obj.getMarketPrice()) && Validations.validateIsNotNull(mapa.getMarketPrice())){
								if(obj.getMarketPrice().equals(mapa.getMarketPrice())){
									obj.setEnteredBalance(mapa.getTransferAmmount());
									obj.setIsSelected(true);
									EnteredBalanceTotal = EnteredBalanceTotal.add(mapa.getTransferAmmount());
								}
							}
						}
					}
				}
				marketFactoResult.setBalanceResult(objHolAccBalTO.getTransferAmmount());
			}
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactoResult); 
		}else{
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
		}
	}
	
	/**
	 * Sol number valid.
	 */
	public void solNumberValid(){
		if(Validations.validateIsNotNull(searchSecurityTransferOperation) && 
		   Validations.validateIsNotNull(searchSecurityTransferOperation.getCustodyOperation()) && 
		   Validations.validateIsNotNull(searchSecurityTransferOperation.getCustodyOperation().getOperationNumber())){
			if(searchSecurityTransferOperation.getCustodyOperation().getOperationNumber().equals(0L)){
				searchSecurityTransferOperation.getCustodyOperation().setOperationNumber(null);
			}
		}	 
	}
	
	/**
	 * Show market fact ui search.
	 *
	 * @param securityTransferOperation the security transfer operation
	 */
	public void showMarketFactUISearch(SecurityTransferOperation securityTransferOperation){
		MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
		operationNumberMarketFace = securityTransferOperation.getCustodyOperation().getOperationNumber();
		marketFactBalance.setSecurityCodePk(securityTransferOperation.getSecurities().getIdSecurityCodePk());
		marketFactBalance.setHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
		marketFactBalance.setParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
		marketFactBalance.setUiComponentName(strUiComponentMarketFact);
		
		MarketFactBalanceHelpTO marketFactoResult = null;
		try {
			marketFactoResult = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
			marketFactoResult.setIndHandleDetails(BooleanType.YES.getCode());
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			switch(e1.getErrorService()){
			case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
						  PropertiesUtilities.getExceptionMessage(e1.getMessage()));
					
			default:
				alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
			}
			return;
		}
		
		if( marketFactoResult.getMarketFacBalances() != null && marketFactoResult.getMarketFacBalances().size() >0 ){
			marketFactoResult.setUiComponentName(strUiComponentMarketFact);
			List<HolderMarketFactBalance> mapList = securityTransferOperation.getLstHolderMarketfactBalance();

			if( mapList!= null && mapList.size()>0 ){
				BigDecimal EnteredBalanceTotal = new BigDecimal(0);
				for(MarketFactDetailHelpTO obj: marketFactoResult.getMarketFacBalances() ){
					for(HolderMarketFactBalance mapa: mapList){
						if(obj.getMarketDate().equals(mapa.getMarketDate()) ){
							if(Validations.validateIsNotNull(obj.getMarketRate()) && Validations.validateIsNotNull(mapa.getMarketRate())){
								if(obj.getMarketRate().equals(mapa.getMarketRate())){
									obj.setEnteredBalance(mapa.getTransferAmmount());
									obj.setIsSelected(true);
									EnteredBalanceTotal = EnteredBalanceTotal.add(mapa.getTransferAmmount());
								}
							}
							if(Validations.validateIsNotNull(obj.getMarketPrice()) && Validations.validateIsNotNull(mapa.getMarketPrice())){
								if(obj.getMarketPrice().equals(mapa.getMarketPrice())){
									obj.setEnteredBalance(mapa.getTransferAmmount());
									obj.setIsSelected(true);
									EnteredBalanceTotal = EnteredBalanceTotal.add(mapa.getTransferAmmount());
								}
							}
						}
					}
					
				}
				marketFactoResult.setBalanceResult(securityTransferOperation.getQuantityOperation());
			}
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactoResult); 
		}else{
			showUIComponent(UICompositeType.MARKETFACT_BALANCE.getCode(),marketFactBalance); 
		}
	}
	
	/**
	 * Select market fact balance review.
	 */
	//PARA CUANDO SE VA A REVISAR EL DESTINO - ORIGEN
	public void selectMarketFactBalanceReview(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		BigDecimal transferAmmount = new BigDecimal(0);
		List<HolderMarketFactBalance> lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
		HolderMarketFactBalance holderMarketfactBalance;
		for (MarketFactDetailHelpTO marketFact: marketFactBalance.getMarketFacBalances()) {
			
			holderMarketfactBalance = new HolderMarketFactBalance();
			holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
			
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
			holderMarketfactBalance.setHolderAccount(holderAccount);
			
			Security security = new Security();
			security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
			holderMarketfactBalance.setSecurity(security);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
			holderMarketfactBalance.setParticipant(participant);
			
			holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
			holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
			holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
			holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
			holderMarketfactBalance.setTransferAmmount(marketFact.getEnteredBalance());
			
			//sumatoria de los montos
			transferAmmount = transferAmmount.add(marketFact.getEnteredBalance());
			
			lstHolderAccountMF.add(holderMarketfactBalance);
		} 
		
		

		//SETTEO EL MONTO A TRANSFERIR
		for (HolderAccountBalance hab: lstHolderAccountBalance) {
			//el participante y la cuenta siempre van a ser iguales
			if(hab.getSecurity().getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
				
				if(!transferAmmount.equals(hab.getTransferAmmount())){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AMOUNT_NOT_EQUALS_IN_REGISTRY));
					hab.setStyleClassButton(styleBtnPending);
					mapTransferMarketfact = new HashMap<String, List<HolderMarketFactBalance>>(); 
					return;
				}
			}
		} 

		dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
		JSFUtilities.updateComponent("frmCustody:pnlRegisterFound");
		mapTransferMarketfact.put(marketFactBalance.getSecurityCodePk(), lstHolderAccountMF);
		for(HolderAccountBalance obj: lstHolderAccountBalance){
			if(obj.getId().getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
				obj.setStyleClassButton(styleBtnActive);
			}
		}
		dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
		
	}

	/**
	 * Select market fact balance search.
	 */
	//hechos del mercado en la busqueda
	public void selectMarketFactBalanceSearch(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		BigDecimal transferAmmount = new BigDecimal(0);
		List<HolderMarketFactBalance> lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
		HolderMarketFactBalance holderMarketfactBalance;
		for (MarketFactDetailHelpTO marketFact: marketFactBalance.getMarketFacBalances()) {
			
			holderMarketfactBalance = new HolderMarketFactBalance();
			holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
			
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
			holderMarketfactBalance.setHolderAccount(holderAccount);
			
			Security security = new Security();
			security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
			holderMarketfactBalance.setSecurity(security);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
			holderMarketfactBalance.setParticipant(participant);
			
			holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
			holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
			holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
			holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
			holderMarketfactBalance.setTransferAmmount(marketFact.getEnteredBalance());
			
			//sumatoria de los montos
			transferAmmount = transferAmmount.add(marketFact.getEnteredBalance());
			
			lstHolderAccountMF.add(holderMarketfactBalance);
		} 

		//SETTEO EL MONTO A TRANSFERIR
		for (SecurityTransferOperation sto: lstTransferSecurityAvailable) {
			//el participante y la cuenta siempre van a ser iguales
			if(sto.getCustodyOperation().getOperationNumber().equals(operationNumberMarketFace)){
				if(!transferAmmount.equals(sto.getQuantityOperation())){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_AMOUNT_NOT_EQUALS_IN_REGISTRY));
					JSFUtilities.updateComponent("opnlDialogs");
					sto.setLstHolderMarketfactBalance(null);
					sto.setStyleClassButton(styleBtnPending);
					JSFUtilities.updateComponent("frmCustody:dtbResult");
					return;
				}else{
					operationNumberMarketFace = new Long(0);
					sto.setLstHolderMarketfactBalance(lstHolderAccountMF);
					sto.setStyleClassButton(styleBtnActive);
					JSFUtilities.updateComponent("frmCustody:dtbResult");
					return;
				}
			}
		} 

		JSFUtilities.updateComponent("frmCustody:pnlRegisterFound");
	}

	
	/**
	 * Select market fact balance.
	 */
	//PARA EL REGISTRO ORIGEN DESTINO
	public void selectMarketFactBalance(){
		MarketFactBalanceHelpTO marketFactBalance = this.holderMarketFactBalance;
		BigDecimal transferAmmount = new BigDecimal(0);
		List<HolderMarketFactBalance> lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
		HolderMarketFactBalance holderMarketfactBalance;
		for (MarketFactDetailHelpTO marketFact: marketFactBalance.getMarketFacBalances()) {
			
			holderMarketfactBalance = new HolderMarketFactBalance();
			holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
			
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
			holderMarketfactBalance.setHolderAccount(holderAccount);
			
			Security security = new Security();
			security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
			holderMarketfactBalance.setSecurity(security);
			
			Participant participant = new Participant();
			participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
			holderMarketfactBalance.setParticipant(participant);
			
			holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
			holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
			holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
			holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
			holderMarketfactBalance.setTransferAmmount(marketFact.getEnteredBalance());
			
			//sumatoria de los montos
			transferAmmount = transferAmmount.add(marketFact.getEnteredBalance());
			
			lstHolderAccountMF.add(holderMarketfactBalance);
		} 
		

		//SETTEO EL MONTO A TRANSFERIR
		for (HolderAccountBalance hab: lstHolderAccountBalance) {
			//el participante y la cuenta siempre van a ser iguales
			if(hab.getSecurity().getIdSecurityCodePk().equals(marketFactBalance.getSecurityCodePk())){
				if(transferAmmount==null || transferAmmount.equals(new BigDecimal(0))){
					hab.setTransferAmmount(transferAmmount);
					hab.setStyleClassButton(styleBtnPending);
					mapTransferMarketfact.remove(marketFactBalance.getSecurityCodePk());
				}else{
					hab.setTransferAmmount(transferAmmount);
					hab.setStyleClassButton(styleBtnActive);
					mapTransferMarketfact.put(marketFactBalance.getSecurityCodePk(), lstHolderAccountMF);
				}
			}
		} 
		marketFactBalance.getTotalBalance();
		dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
		JSFUtilities.updateComponent("frmCustody:pnlRegisterFound");
		
	}

	/**
	 * Valid transfer ammount target.
	 *
	 * @param holderAccountBalance the holder account balance
	 */
	public void validTransferAmmountTarget(HolderAccountBalance holderAccountBalance){
		if(Validations.validateIsNotNull(holderAccountBalance.getTransferAmmount())){
			if(!Validations.validateIsNotNullAndPositive(holderAccountBalance.getTransferAmmount().intValue())){
				/*validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT);*/
					//alert(validationMessage);
					holderAccountBalance.setTransferAmmount(null);
			}
		}else{
			holderAccountBalance.setTransferAmmount(null);
		}
	}
	
	/**
	 * Load name and edit attribute's with isinCode and return new HolderAccountBalanceDataModel(List<HolderAccountBalance>).
	 *
	 * @param holderAccountBalance the holder account balance
	 */
	@LoggerAuditWeb
	public void loadSecurityDescription(HolderAccountBalance holderAccountBalance){
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountBalance.getId().getIdSecurityCodePk())){	//** If holderAccountBalance has data
			try {
				holderAccountBalance.getSecurity().setIdSecurityCodePk(holderAccountBalance.getId().getIdSecurityCodePk());
				List<Security> lstSecurity = listSecurity(holderAccountBalance.getSecurity());
				if(Validations.validateIsNotNull(lstSecurity) && lstSecurity.size()>0){	// If the List has data 
					holderAccountBalance.setSecurity(lstSecurity.get(0));
					lstHolderAccountBalance.set(lstHolderAccountBalance.indexOf(holderAccountBalance), holderAccountBalance);
					dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
				}else{	//**If the list is not valid 
					holderAccountBalance.getId().setIdSecurityCodePk("");
					validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
							 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
							 PropertiesConstants.SECURITY_DONT_EXIST);
					alert(validationMessage);
				}
			} catch (ServiceException e) {
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams())	);
			}
		}
	}
	
	/**
	 * Register remove data.
	 *
	 * @param holderAccountBalance the holder account balance
	 */
	@LoggerAuditWeb
	public void RegisterRemoveData(HolderAccountBalance holderAccountBalance){
		if(Validations.validateIsNotNullAndNotEmpty(holderAccountBalance)){
			lstHolderAccountBalance.remove(holderAccountBalance);
			dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
		}
	}


	/**
	 * used to get the selected value in the data table on row Check ***************method checkActionTransfer.
	 *
	 * @param e the e
	 */
	public void checkActionTransfer(SelectEvent e){
		SecurityTransferOperation securityTransferOperation = (SecurityTransferOperation)e.getObject();
		lstActionTransferSecurityAvailable.add(securityTransferOperation);
		this.securityTransferOperation = securityTransferOperation;
		custodyOperation = securityTransferOperation.getCustodyOperation();
	}
	
	/**
	 * used to get the selected value in the data table on row Uncheck ***************method checkActionTransfer.
	 *
	 * @param e the e
	 */
	public void uncheckActionTransfer(UnselectEvent e){
		SecurityTransferOperation securityTransferOperation = (SecurityTransferOperation)e.getObject();
		lstActionTransferSecurityAvailable.remove(securityTransferOperation);
	}
	
	/**
	 * Execute operations.
	 *
	 * @param action the action
	 */
	@LoggerAuditWeb
	public void executeOperations(String action){
		
		confirmAlertAction=action;
		String message="";
		String header= "";
		if(confirmAlertAction.equals("executeMultiApprove")){
			message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPROVE);
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE);
		}else if(confirmAlertAction.equals("executeMultiRevise")){
			message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_REVIEW);
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW);
		}else if(confirmAlertAction.equals("executeMultiConfirm")){
			message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM);
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
		}
		List <String> notNegotiable = new ArrayList<String>();
		List<Long> listOperationNumber = new ArrayList<Long>();
		for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
			listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());
			if((arrSecurityTransferOperation[i].getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode()) && 
					arrSecurityTransferOperation[i].getSecurities().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER))
					||(arrSecurityTransferOperation[i].getSecurities().getSecurityClass().equals(SecurityClassType.DPA.getCode()) &&
							arrSecurityTransferOperation[i].getSecurities().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER)
							)){
				notNegotiable.add(arrSecurityTransferOperation[i].getSecurities().getIdSecurityCodePk());
			}
		}
		String strOperationNumber = operationNumberMessage(listOperationNumber);
		
		if(notNegotiable.size()>0){
			String strNotNeg="";
			for (String notNegStr : notNegotiable){
				strNotNeg=strNotNeg+notNegStr+" |";
			}
			
			Object[] parametros = {message.toLowerCase(),strOperationNumber+" con los valores NO negociables: "+strNotNeg};
			validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.TRANSFER_QUESTION_MULTIPLE_SAVE_VD,parametros);
		}else{
			Object[] parametros = {message.toLowerCase(),strOperationNumber};
			validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.TRANSFER_QUESTION_MULTIPLE_SAVE_VD,parametros);
		}
		
		
		
		question(validationMessage,header);
	}

	/**
	 * Execute operation.
	 *
	 * @param action the action
	 */
	@LoggerAuditWeb
	public void executeOperation(String action){
		
		confirmAlertAction=action;
		String message="";
		String header="";
		if(confirmAlertAction.equals("executeApprove")){
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
			
			//issue 817
			if(Validations.validateIsNotNull(securityTransferOperation.getSecurities().getNotTraded()) &&
					securityTransferOperation.getSecurities().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				validationMessage = PropertiesUtilities.getMessage( 
						 PropertiesConstants.TRANSFER_NOT_NEGOTIABLE_SECURITY,securityTransferOperation.getSecurities().getIdSecurityCodePk());
			}else{
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPROVE);
				
				Object[] parametros = {message.toLowerCase(),securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString()};
				
				validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VD,parametros);
				
			}
			
		}else if(confirmAlertAction.equals("executeRevise")){
			if( securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
				if( !(Validations.validateIsNotNull(targetAccountTo) && 
					Validations.validateIsNotNullAndPositive(targetAccountTo.getAccountPk())) ){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));
					return;
				}
			}else if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				if( !(Validations.validateIsNotNull(sourceAccountTo) && 
						Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountPk())) ){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_SOURCE));
						return;
					}
			}
			
			if( useMarketFact &&  securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
				!(Validations.validateIsNotNull(mapTransferMarketfact) && mapTransferMarketfact.size()>0) ){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MARKETFACT_NOT_SELECTED));
				return;
			}
			
			//ISSUE 817
			
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
			
			if(Validations.validateIsNotNull(securityTransferOperation.getSecurities().getNotTraded()) 
					&& securityTransferOperation.getSecurities().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER)){
				validationMessage = PropertiesUtilities.getMessage( 
						 PropertiesConstants.TRANSFER_NOT_NEGOTIABLE_SECURITY,securityTransferOperation.getSecurities().getIdSecurityCodePk());
			}else{
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_REVIEW);
				
				Object[] parametros = {message.toLowerCase(),securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString()};
				
				validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VD,parametros);
			}
			
		}else if(confirmAlertAction.equals("executeConfirm")){
			
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
			
			if((securityTransferOperation.getSecurities().getSecurityClass().equals(SecurityClassType.DPF.getCode()) && 
					securityTransferOperation.getSecurities().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER))
					||(securityTransferOperation.getSecurities().getSecurityClass().equals(SecurityClassType.DPA.getCode()) &&
							securityTransferOperation.getSecurities().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER))
					){
				/*Object[] parametros = {message.toLowerCase(),securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString()+" con el valor NO negociable: "+securityTransferOperationTmp.getSecurities().getIdSecurityCodePk()};
				
				validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VD,parametros);*/
				
				validationMessage = PropertiesUtilities.getMessage( 
						 PropertiesConstants.TRANSFER_NOT_NEGOTIABLE_SECURITY,securityTransferOperation.getSecurities().getIdSecurityCodePk());
				
			}else{
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM);
				
				Object[] parametros = {message.toLowerCase(),securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString()};
				
				validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VD,parametros);
			}
		}else{
			Object[] parametros = {message.toLowerCase(),securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString()};
			
			validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VD,parametros);
		}
		question(validationMessage,header);
	}


	/**
	 * Operation number message.
	 *
	 * @param operationNumber the operation number
	 * @return the string
	 */
	private String operationNumberMessage(List<Long> operationNumber){
		String certs = StringUtils.EMPTY;
		if(operationNumber != null && operationNumber.size() > 0){
			for(Long crt : operationNumber){
				if(StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)){
					certs = crt.toString();
				}else{
					certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt.toString()));
				}
			}
		}
		return certs;
	}
	
	/**
	 * Operation reblock number message.
	 *
	 * @param operationNumber the operation number
	 * @return the string
	 */
	private String operationReblockNumberMessage(List<Long> operationNumber){
		String certs = StringUtils.EMPTY;
		if(operationNumber != null && operationNumber.size() > 0){
			for(Long crt : operationNumber){
				if(StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)){
					certs = crt.toString();
				}else{
					certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt.toString()));
				}
			}
		}
		return certs;
	}
	
	/**
	 * Execute multi approve.
	 */
	@LoggerAuditWeb
	public void executeMultiApprove(){
		try {			
			List<Long> listOperationNumber = new ArrayList<Long>();
			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
				listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());
				
				if(Validations.validateIsNotNull(arrSecurityTransferOperation[i].getTargetHolderAccount())  &&
					Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getTargetHolderAccount().getIdHolderAccountPk()) ){
					arrSecurityTransferOperation[i].setTargetHolderAccount(null);
				}
				if( Validations.validateIsNotNull(arrSecurityTransferOperation[i].getSourceHolderAccount())  &&
					Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getSourceHolderAccount().getIdHolderAccountPk()) ){
					arrSecurityTransferOperation[i].setSourceHolderAccount(null);
				}
			}
			
			transferAvailableFacade.executeChangeStateAllAproveAvailable(TransferSecuritiesStateType.REGISTRADO.getCode(), 
													   TransferSecuritiesStateType.APROBADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE.getCode(), 
													   arrSecurityTransferOperation,flagIndMarketFact);
			
			String strOperationNumber = operationNumberMessage(listOperationNumber);
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_APPROVE_VD, new Object[] {strOperationNumber.trim()});
			confirmAlertAction="searchTransferAvailableViewMultiple";
			alert(header,message);
			
			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE.getCode());						
			Object[] parameters = {message, strOperationNumber};			
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
													businessProcess, null, parameters);
			} else {
				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
					SecurityTransferOperation objSecurityTransferOperation = (SecurityTransferOperation) arrSecurityTransferOperation[i];
					Object [] parametersNotification ={objSecurityTransferOperation.getCustodyOperation().getOperationNumber()};
					if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNotification);
					} else {
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								businessProcess, objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNotification);						
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								businessProcess, objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNotification);
					}
				}
			}
			/*** FIN NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}
	}
	
	/**
	 * Execute multi revise.
	 */
	@LoggerAuditWeb
	public void executeMultiRevise(){
		
		try {
			
			List<Long> listOperationNumber = new ArrayList<Long>();
			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
				listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());

				if(Validations.validateIsNotNull(arrSecurityTransferOperation[i].getTargetHolderAccount())  &&
						Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getTargetHolderAccount().getIdHolderAccountPk()) ){
						arrSecurityTransferOperation[i].setTargetHolderAccount(null);
				}
				if( Validations.validateIsNotNull(arrSecurityTransferOperation[i].getSourceHolderAccount())  &&
					Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getSourceHolderAccount().getIdHolderAccountPk()) ){
					arrSecurityTransferOperation[i].setSourceHolderAccount(null);
				}
			}
			
			transferAvailableFacade.executeChangeStateAllReviewAvailable(TransferSecuritiesStateType.APROBADO.getCode(), 
													   TransferSecuritiesStateType.REVISADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode(), 
													   arrSecurityTransferOperation,flagIndMarketFact);

			String strOperationNumber = operationNumberMessage(listOperationNumber);
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_REVIEW_VD, new Object[] {strOperationNumber});
			confirmAlertAction="searchTransferAvailableViewMultiple";
			alert(header,message);
			
			/*** INICIO NOTIFICACIONES ***/			
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode());
			Object[] parameters = {message,strOperationNumber};
			
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
														   businessProcess, null, parameters);
			} else {					
				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
					SecurityTransferOperation objSecurityTransferOperation =  (SecurityTransferOperation) arrSecurityTransferOperation[i];
					Object [] parametersNotification = {objSecurityTransferOperation.getCustodyOperation().getOperationNumber()};
					if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNotification);
					} else {
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNotification);
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNotification);
					}
				}				
			}								
			/*** FIN NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}	
	}
	
	/**
	 * Execute multi confirm.
	 */
	@LoggerAuditWeb
	public void executeMultiConfirm(){

		try {

			List<Long> listOperationNumber = new ArrayList<Long>();
			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
				listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());

				if(Validations.validateIsNotNull(arrSecurityTransferOperation[i].getTargetHolderAccount())  &&
						Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getTargetHolderAccount().getIdHolderAccountPk()) ){
						arrSecurityTransferOperation[i].setTargetHolderAccount(null);
				}
				if( Validations.validateIsNotNull(arrSecurityTransferOperation[i].getSourceHolderAccount())  &&
					Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getSourceHolderAccount().getIdHolderAccountPk()) ){
					arrSecurityTransferOperation[i].setSourceHolderAccount(null);
				}
			}
			
			transferAvailableFacade.executeChangeStateAllConfirmAvailable(TransferSecuritiesStateType.REVISADO.getCode(), 
													   TransferSecuritiesStateType.CONFIRMADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM.getCode(), 
													   arrSecurityTransferOperation,flagIndMarketFact);

			String strOperationNumber = operationNumberMessage(listOperationNumber);
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_CONFIRM_D_VD, new Object[] {strOperationNumber});
			confirmAlertAction="searchTransferAvailableViewMultiple";
			alert(header,message);			
			
			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM.getCode());
			Object[] parameters = {message,strOperationNumber};
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcess, null, parameters);
			} else {
				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
					SecurityTransferOperation objSecurityTransferOperation = (SecurityTransferOperation) arrSecurityTransferOperation[i];
					String accountNumber = GeneralConstants.EMPTY_STRING;
					String descParticipant = GeneralConstants.EMPTY_STRING;
					if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
						accountNumber = objSecurityTransferOperation.getTargetHolderAccount().getAccountNumber().toString();
						descParticipant = objSecurityTransferOperation.getTargetParticipant().getDisplayCodeMnemonic()+ GeneralConstants.HYPHEN +
								objSecurityTransferOperation.getTargetParticipant().getDescription();
					} else {
						accountNumber = objSecurityTransferOperation.getSourceHolderAccount().getAccountNumber().toString();
						descParticipant = objSecurityTransferOperation.getSourceParticipant().getDisplayCodeMnemonic()+ GeneralConstants.HYPHEN +
								objSecurityTransferOperation.getSourceParticipant().getDescription();
					}														
					Object [] parametersNotification = {descParticipant, accountNumber, objSecurityTransferOperation.getSecurities().getIdSecurityCodePk()};						
					if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								businessProcess, objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNotification);
					} else {
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								businessProcess, objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNotification);}
				}				
			}
			/*** FIN NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}
	}
	
	/**
	 * Execute approve.
	 */
	@LoggerAuditWeb
	public void executeApprove(){

		try {
			
			String messages = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_EXECUTE_ACTION);
			if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
				alert(messages+securityTransferOperationTmp.getCustodyOperation().getOperationNumber()+"");
				return;
			}
			
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setTargetHolderAccount(null);
			}
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setSourceHolderAccount(null);
			}
			
			securityTransferOperation.setCustodyOperation(custodyOperation);

			transferAvailableFacade.executeChangeStateAproveAvailable(TransferSecuritiesStateType.REGISTRADO.getCode(), 
													   TransferSecuritiesStateType.APROBADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE.getCode(), 
													   securityTransferOperation,flagIndMarketFact);

			confirmAlertAction="searchTransferAvailableView";

			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_APPROVE_VD, new Object[] {strDocumentNumber.toString()});
			alert(header,message);

			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE.getCode());
			Object[] parameters = new Object[1];
			parameters[0] = strDocumentNumber.toString();
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),
						businessProcess, null, parameters);
			} else {
				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);
				} else {
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);
					
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);
				}
			}
			/*** END 	NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}
	}
	
	
	/**
	 * Execute anulled or reject.
	 */
	@LoggerAuditWeb
	public void executeAnulledOrReject(){
		strRejectMotive="";
		intRejectMotive=null;
		alertAnulledOrReject();	
	}
	
	/**
	 * Execute all anulled or reject.
	 */
	@LoggerAuditWeb
	public void executeAllAnulledOrReject(){
		strRejectMotive="";
		intRejectMotive=null;
		alertMultiAnulledOrReject();
		
	}

	/**
	 * Execute anulled.
	 */
	@LoggerAuditWeb
	public void executeAnulled(){
		
		try {

			String messages = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_EXECUTE_ACTION);
			if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
				alert(messages+securityTransferOperationTmp.getCustodyOperation().getOperationNumber()+"");
				return;
			}

			
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setTargetHolderAccount(null);
			}
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setSourceHolderAccount(null);
			}
			
			securityTransferOperation.setCustodyOperation(custodyOperation);
		
			transferAvailableFacade.executeChangeStateCancelAvailable(TransferSecuritiesStateType.REGISTRADO.getCode(), 
													   TransferSecuritiesStateType.ANULADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL.getCode(), 
													   securityTransferOperation,flagIndMarketFact);
			
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_ANNULER_VD, new Object[] {strDocumentNumber.toString()});
			confirmAlertAction="searchTransferAvailableView";
			alert(header,message);
			

			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL.getCode());
			Object[] parameters = new Object[1];
			parameters[0] = strDocumentNumber.toString();
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
			} else {
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, participantLogin, parameters);
			}
			/*** INICIO NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams())	);
		}	
	}
	
	/**
	 * Execute multi anulled.
	 */
	@LoggerAuditWeb
	public void executeMultiAnulled(){
		
		try {
		
			List<Long> listOperationNumber = new ArrayList<Long>();
			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
				listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());

				if(Validations.validateIsNotNull(arrSecurityTransferOperation[i].getTargetHolderAccount())  &&
						Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getTargetHolderAccount().getIdHolderAccountPk()) ){
						arrSecurityTransferOperation[i].setTargetHolderAccount(null);
					}
				if( Validations.validateIsNotNull(arrSecurityTransferOperation[i].getSourceHolderAccount())  &&
					Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getSourceHolderAccount().getIdHolderAccountPk()) ){
					arrSecurityTransferOperation[i].setSourceHolderAccount(null);
				}
			}

			transferAvailableFacade.executeChangeStateAllCancelAvailable(TransferSecuritiesStateType.REGISTRADO.getCode(), 
													   TransferSecuritiesStateType.ANULADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL.getCode(), 
													   arrSecurityTransferOperation,flagIndMarketFact);

			String strOperationNumber = operationNumberMessage(listOperationNumber);
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_ANNULER_VD, new Object[] {strOperationNumber});
			confirmAlertAction="searchTransferAvailableViewMultiple";
			alert(header,message);
			
			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL.getCode());
				Object[] parameters = {message,strOperationNumber};
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
			} else {
				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
					SecurityTransferOperation objSecurityTransferOperation =  (SecurityTransferOperation)arrSecurityTransferOperation[i];
					Object [] parametersNotification = {objSecurityTransferOperation.getCustodyOperation().getOperationNumber()};
					if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								businessProcess, objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNotification);
					}else{
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
								businessProcess, objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNotification);}
				}
			}
			/*** FIN NOTIFICACIONES ***/
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}	
	}
	
	/**
	 * Execute multi reject.
	 */
	@LoggerAuditWeb
	public void executeMultiReject(){
		try {

			List<Long> listOperationNumber = new ArrayList<Long>();
			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
				listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());

				if(Validations.validateIsNotNull(arrSecurityTransferOperation[i].getTargetHolderAccount())  &&
						Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getTargetHolderAccount().getIdHolderAccountPk()) ){
						arrSecurityTransferOperation[i].setTargetHolderAccount(null);
				}
				if( Validations.validateIsNotNull(arrSecurityTransferOperation[i].getSourceHolderAccount())  &&
					Validations.validateIsNullOrNotPositive(arrSecurityTransferOperation[i].getSourceHolderAccount().getIdHolderAccountPk()) ){
					arrSecurityTransferOperation[i].setSourceHolderAccount(null);
				}
			}
			
			Integer stateOld = ( arrSecurityTransferOperation[0].getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) )?
								TransferSecuritiesStateType.APROBADO.getCode():
								TransferSecuritiesStateType.REVISADO.getCode();
		
			transferAvailableFacade.executeChangeStateRejecAllAvailable(stateOld, 
													   TransferSecuritiesStateType.RECHAZADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode(), 
													   arrSecurityTransferOperation,flagIndMarketFact);

			String strOperationNumber = operationNumberMessage(listOperationNumber);
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_REJECT_VD, new Object[] {strOperationNumber});
			confirmAlertAction="searchTransferAvailableViewMultiple";
			alert(header,message);
			
			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode());
				Object[] parameters = {message,strOperationNumber};
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
			}else if(isParticipant){
				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
					SecurityTransferOperation objSecurityTransferOperation =  (SecurityTransferOperation)arrSecurityTransferOperation[i];					
					Object [] parametersNotification = {objSecurityTransferOperation.getCustodyOperation().getOperationNumber()};					
					if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
								objSecurityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNotification);
					}else{
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, 
								objSecurityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNotification);}
				}
			}
			/*** FIN NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}	
	}
	
	
	/**
	 * Call anulled or reject.
	 */
	@LoggerAuditWeb
	public void callAnulledOrReject(){
		alertAnulledOrReject();
	}
	
	/**
	 * Sets the reject or anulled properties.
	 */
	@LoggerAuditWeb
	public void setRejectOrAnulledProperties(){
		custodyOperation.setRejectDate(CommonsUtilities.currentDate());
		custodyOperation.setRejectMotive(intRejectMotive);
		if(intRejectMotive.equals(otherRejectMotiveId)){
			custodyOperation.setRejectMotiveOther(strRejectMotive);
		}
		custodyOperation.setRejectUser(userInfo.getUserAccountSession().getUserName());
		
		String header = "";
		String strMessage = "";
		if(securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			strMessage=PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ANULLED);
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_ANNUL);
			confirmAlertAction="executeAnulled";
		}else{
			strMessage=PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECTED);
			header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_MOTIVE_REJECT);
			confirmAlertAction="executeReject";
		}

		Object[] parametros = {strMessage.toLowerCase(),securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString()};
		
		validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VD,parametros);
		
		question(validationMessage,header);
	}
	

	/**
	 * Sets the reject or anulled all properties.
	 */
	@LoggerAuditWeb
	public void setRejectOrAnulledAllProperties(){
		
		List<Long> listOperationNumber = new ArrayList<Long>();
		for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
			arrSecurityTransferOperation[i].getCustodyOperation().setRejectDate(CommonsUtilities.currentDate());
			arrSecurityTransferOperation[i].getCustodyOperation().setRejectMotive(intRejectMotive);
			arrSecurityTransferOperation[i].getCustodyOperation().setRejectUser(userInfo.getUserAccountSession().getUserName());
			if(intRejectMotive.equals(otherRejectMotiveId)){
				arrSecurityTransferOperation[i].getCustodyOperation().setRejectMotiveOther(strRejectMotive);
			}
			listOperationNumber.add(arrSecurityTransferOperation[i].getCustodyOperation().getOperationNumber());
		}
		
		String strOperationNumber = operationNumberMessage(listOperationNumber);
		String strMessage = "";
		if(arrSecurityTransferOperation[0].getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			strMessage=PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ANULLED);;
			confirmAlertAction="executeMultiAnulled";
		}else{
			strMessage=PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECTED);
			confirmAlertAction="executeMultiReject";
		}

		Object[] parametros = {strMessage,strOperationNumber};
		
		validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 PropertiesConstants.TRANSFER_QUESTION_MULTIPLE_SAVE_VD,parametros);
		
		question(validationMessage);
	}
	
	/**
	 * Close reject or anulled properties.
	 */
	public void closeRejectOrAnulledProperties(){
		intRejectMotive=null;
		strRejectMotive=null;
	}
	
	/**
	 * Execute reject.
	 */
	@LoggerAuditWeb
	public void executeReject(){
		try {
		
			String messages = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_EXECUTE_ACTION);
			if(!( securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
					|| securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REVISADO.getCode()) )
			){
				alert(messages+securityTransferOperationTmp.getCustodyOperation().getOperationNumber()+"");
				return;
			}

			
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setTargetHolderAccount(null);
			}
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setSourceHolderAccount(null);
			}
			
			securityTransferOperation.setCustodyOperation(custodyOperation);
			
			Integer stateOld = ( securityTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) )?
								TransferSecuritiesStateType.APROBADO.getCode():
								TransferSecuritiesStateType.REVISADO.getCode();
		
			transferAvailableFacade.executeChangeStateRejectAvailable(stateOld, 
													   TransferSecuritiesStateType.RECHAZADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode(), 
													   securityTransferOperation,flagIndMarketFact);

			
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_REJECT_VD, new Object[] {strDocumentNumber.toString()});
			confirmAlertAction="searchTransferAvailableView";
			alert(header,message);

			/*** JH NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode());
				Object[] parameters = new Object[1];
				parameters[0] = strDocumentNumber.toString();
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
														   businessProcess, 
														   null, parameters);
			}else if(isParticipant){

				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){

					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															   businessProcess, 
															   securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);
				}else{

					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
															   businessProcess, 
															   securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);
				}
			}
			/*** end JH NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}	
	}
	
	/**
	 * Return search.
	 *
	 * @return the string
	 */
	public String returnSearch(){
		securityTransferOperationTmp = new SecurityTransferOperation();
		custodyOperation = new CustodyOperation();
		securityTransferOperation = new SecurityTransferOperation();
		enabledButtons();
		registersFound = true;
		return "searchTransferAvailableView";
	}
	
	//@LoggerAuditWeb
	/**
	 * Execute revise.
	 */
	public void executeRevise(){
		try {
			
			String messages = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_EXECUTE_ACTION);
			if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())){
				alert(messages+securityTransferOperationTmp.getCustodyOperation().getOperationNumber()+"");
				return;
			}

			if( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setTargetHolderAccount(null);
			}
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setSourceHolderAccount(null);
			}
			
			if( !transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation) ){
				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
					securityTransferOperation.setSourceHolderAccount(new HolderAccount());
				}else{
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				}
				String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ORGIN_RNT_ACCOUNT_NO_RELATION);
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),message);	//agregar a propertie ***
				return;
			}
			
			securityTransferOperation.setCustodyOperation(custodyOperation);
			securityTransferOperation.setSecurities(lstHolderAccountBalance.get(0).getSecurity());
			securityTransferOperation.setQuantityOperation(lstHolderAccountBalance.get(0).getTransferAmmount());
			
			//SOLO SI SE USA EL HECHO DEL MERCADO
			if(flagIndMarketFact && flagTargetSource){
				List<HolderMarketFactBalance> lstHolderMarketfactBalance  = mapTransferMarketfact.get(securityTransferOperation.getSecurities().getIdSecurityCodePk());
				securityTransferOperation.setLstHolderMarketfactBalance(lstHolderMarketfactBalance);
			}
			
			transferAvailableFacade.executeChangeStateReviewAvailable(TransferSecuritiesStateType.APROBADO.getCode(), 
													   TransferSecuritiesStateType.REVISADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode(), 
													   securityTransferOperation,flagIndMarketFact);
			
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_REVIEW_VD, new Object[] {strDocumentNumber.toString()});
			confirmAlertAction="searchTransferAvailableView";
			alert(header,message);
			
			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode());
			Object[] parameters = new Object[1];
			parameters[0] = strDocumentNumber.toString();
			if(isDepositary){
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
														   businessProcess, null, parameters);
			} else {				
				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);
				} else {
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							   businessProcess, securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);
				}
				
			}
			/*** END NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}	
	}
	
	/**
	 * Execute confirm.
	 */
	@LoggerAuditWeb
	public void executeConfirm(){

		try {
			
			String messages = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_EXECUTE_ACTION);
			if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())){
				alert(messages+securityTransferOperationTmp.getCustodyOperation().getOperationNumber()+"");
				return;
			}
			
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setTargetHolderAccount(null);
			}
			if( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) && 
				!Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ){
				securityTransferOperationTmp.setSourceHolderAccount(null);
			}

			securityTransferOperation.setSecurities(lstHolderAccountBalance.get(0).getSecurity());
			securityTransferOperation.setQuantityOperation(lstHolderAccountBalance.get(0).getTransferAmmount());
		
			transferAvailableFacade.executeChangeStateConfirmAvailable(TransferSecuritiesStateType.REVISADO.getCode(), 
													   TransferSecuritiesStateType.CONFIRMADO.getCode(), 
													   BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM.getCode(), 
													   securityTransferOperation,flagIndMarketFact);
			
			
			String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
			String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_CONFIRM_D_VD, new Object[] {strDocumentNumber.toString()});
			confirmAlertAction="searchTransferAvailableView";
			alert(header,message);
			
			/*** INICIO NOTIFICACIONES ***/
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM.getCode());
			Object[] parameters = new Object[3];
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				parameters[1] = securityTransferOperation.getTargetHolderAccount().getAccountNumber().toString();
			}else{
				parameters[1] = securityTransferOperation.getSourceHolderAccount().getAccountNumber().toString();
			}
			
			parameters[2] = securityTransferOperation.getSecurities().getIdSecurityCodePk();
			if(isDepositary){				
				parameters[0] = securityTransferOperation.getTargetParticipant().getDisplayCodeMnemonic()+GeneralConstants.HYPHEN + 
						securityTransferOperation.getTargetParticipant().getDescription();
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						businessProcess, null, parameters);
			} else {
				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){				
					parameters[0] = securityTransferOperation.getTargetParticipant().getDisplayCodeMnemonic()+GeneralConstants.HYPHEN +
							securityTransferOperation.getTargetParticipant().getDescription();					
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							businessProcess, securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);						
				} else {
					parameters[0] = securityTransferOperation.getSourceParticipant().getDisplayCodeMnemonic()+GeneralConstants.HYPHEN + 
								securityTransferOperation.getSourceParticipant().getDescription();
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							businessProcess, securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);
				}
			}
			/*** END FIN NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			//clearFields();
			confirmAlertAction="searchTransferAvailableView";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
		}
	}
	
	/**
	 * Save confirm.
	 */
	@LoggerAuditWeb
	public void saveConfirm() throws ServiceException {	
		
		if(flagTargetSource && !(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) && 
		   Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));					
			/*securityTransferOperation.setTargetHolderAccount(new HolderAccount());
			targetAccountTo = new HolderAccountHelperResultTO();*/
		}else  if(flagSameParticipant && !(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) && 
		   Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));					
			/*securityTransferOperation.setTargetHolderAccount(new HolderAccount());
			targetAccountTo = new HolderAccountHelperResultTO();*/
		}else{
			
			//todo ok
			
			if(arrHolderAccountBalance.length>0){
				
				//Expiration date validation
				Calendar today = Calendar.getInstance();
				for(int i=0; i<arrHolderAccountBalance.length;i++){
					Date expirationDate = transferAvailableFacade.getExpirationDateInfo(arrHolderAccountBalance[i].getId().getIdSecurityCodePk());
					if(Validations.validateIsNotNullAndNotEmpty(expirationDate)) {
						if(today.getTime().after(expirationDate)) {
							validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
									 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
									 PropertiesConstants.EXPIRATION_DATE_SECURITY);
							alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), validationMessage);
							return;
						}
					}
				}
								
				
				String notNegotiableStr = "";
				
				ArrayList<String> notNegotiableList = new ArrayList<String>();
				
				ArrayList<String> lstPks = new ArrayList<String>();
				for(int i=0; i<arrHolderAccountBalance.length;i++){
					
					BigDecimal transferAmmount = (Validations.validateIsNotNull(arrHolderAccountBalance[i].getTransferAmmount())) 
											? arrHolderAccountBalance[i].getTransferAmmount() 
											: new BigDecimal(0);
					
					if( Validations.validateIsNotNullAndPositive(transferAmmount.intValue()) ){
						String pk = (transferAmmount.toString());
						if(!pk.equals(""))
							lstPks.add(pk);
							if(Validations.validateIsNull(arrHolderAccountBalance[i].getSecurity().getNotTraded())) {
								arrHolderAccountBalance[i].getSecurity().setNotTraded(GeneralConstants.ZERO_VALUE_INTEGER);
							}
							if((arrHolderAccountBalance[i].getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())
									&& arrHolderAccountBalance[i].getSecurity().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER))
									|| (arrHolderAccountBalance[i].getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode())
											&& arrHolderAccountBalance[i].getSecurity().getNotTraded().equals(GeneralConstants.ONE_VALUE_INTEGER)											
											)){
								notNegotiableList.add(arrHolderAccountBalance[i].getSecurity().getIdSecurityCodePk());
							}
						
					}
				}
				
				if(lstPks.size()>0){

					confirmAlertAction="save";

					if(notNegotiableList.size()>0){
						
						for (String notneg : notNegotiableList){
							notNegotiableStr=notNegotiableStr+notneg+" | ";
						}
						String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
						
						validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
								 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
								 PropertiesConstants.TRANSFER_NOT_NEGOTIABLE_SECURITY,notNegotiableStr,
								 PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
						
						question(validationMessage,header);
					}else{
						String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
						validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
								 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
								 PropertiesConstants.TRANSFER_CONFIRM_REGISTRY,
								 PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
						
						question(validationMessage,header);
					}
				}else{
					validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
							 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
							 PropertiesConstants.TRANSFER_NOT_SELECT_AMOUNT_REGISTER);
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
				}
				
			}else{
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.TRANSFER_NOT_SELECT_SECURITY);
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
			}
			
		}
		
					
	}
	
	
	/**
	 * Save the transfer of security availables.
	 */
	@LoggerAuditWeb
	public void save(){

		try {
			Long operationType = null;
			Long businessProcess = BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REGISTER.getCode();
			List<SecurityTransferOperation> lstsecurityTransferOperation = new ArrayList<SecurityTransferOperation>();
			
			if(lstHolderAccountBalance.size()>1){
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE_PLURAL);
			}else{
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.REGISTER_COMPLETE_TRANSFER_SECURITY_AVAILABLE);
			}
			
			SecurityTransferOperation securituTransferOp = null;
			
			for(int i=0; i<lstHolderAccountBalance.size();i++){
				
				if( Validations.validateIsNotNull(lstHolderAccountBalance.get(i).getTransferAmmount())  && 
					lstHolderAccountBalance.get(i).getTransferAmmount().intValue()>0){
					
					securituTransferOp = new SecurityTransferOperation();
					securituTransferOp.setCustodyOperation(securityTransferOperation.getCustodyOperation());
					securituTransferOp.setSourceParticipant(securityTransferOperation.getSourceParticipant());
					securituTransferOp.setSourceHolderAccount(securityTransferOperation.getSourceHolderAccount());
					securituTransferOp.setTargetParticipant(securityTransferOperation.getTargetParticipant());
					securituTransferOp.setTargetHolderAccount(securityTransferOperation.getTargetHolderAccount());
					securituTransferOp.setTransferType(securityTransferOperation.getTransferType());
					
					securituTransferOp.setQuantityOperation(lstHolderAccountBalance.get(i).getTransferAmmount());
					Security securitie = new Security();
					securitie.setIdSecurityCodePk(lstHolderAccountBalance.get(i).getId().getIdSecurityCodePk());
					securituTransferOp.setSecurities(securitie);
					securituTransferOp.setIndBlockTransfer(ComponentConstant.ZERO);// 0 para traspaso de disponibles
						
					if( securituTransferOp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
						operationType = ParameterOperationType.SECURITIES_TRANSFER_AVAILABLE_SOURCE_TARGET.getCode();
						securituTransferOp.setTargetHolderAccount(null);
					}else if(securituTransferOp.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
						operationType = ParameterOperationType.SECURITIES_TRANSFER_AVAILABLE_SOURCE_TARGET.getCode();
					}else{
						operationType = ParameterOperationType.SECURITIES_TRANSFER_AVAILABLE_TARGET_SOURCE.getCode();
						securituTransferOp.setSourceHolderAccount(null);
					}
					
					//SOLO SI SE USA EL HECHO DEL MERCADO
					if(flagIndMarketFact && (flagSourceTarget || flagSameParticipant)){
						List<HolderMarketFactBalance> lstHolderMarketfactBalance  =  mapTransferMarketfact.get(securituTransferOp.getSecurities().getIdSecurityCodePk());
						securituTransferOp.setLstHolderMarketfactBalance(lstHolderMarketfactBalance); 
					}

					lstsecurityTransferOperation.add(securituTransferOp);
				}
			}
			
			String pk = transferAvailableFacade.registryListSecurityTransferServiceBeanAvailable(lstsecurityTransferOperation,
																								 businessProcess, operationType, null,
																								 flagIndMarketFact);
			
			String strHeader = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 GeneralConstants.LBL_HEADER_ALERT_SUCCESS);

			validationMessage += pk;

			confirmAlertAction = "saveSucess";
			alert(strHeader,validationMessage);
			
			/*** INICIO NOTIFICACIONES ***/
			/** se envian las notificaciones a los usuarios del participante origen**/
			BusinessProcess objBusinessProcess = new BusinessProcess();
			objBusinessProcess.setIdBusinessProcessPk(businessProcess);
			Object[] parameters = new Object[1];
			parameters[0] = pk;		
			
			if(isDepositary){
				// Enviar notificicacion a participante destino
				notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
						objBusinessProcess, null, parameters);	
			} else {
				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
					// Enviar notificicacion a participante destino
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							objBusinessProcess, securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);	
				} else {
					// Enviar notificicacion a participante origen			
					notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							objBusinessProcess, securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);	
				}
			}			
								
			/*** FIN 	NOTIFICACIONES ***/
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
					, PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
			JSFUtilities.showSimpleValidationDialog();
			e.printStackTrace();
		}	
	}
	
	/**
	 * Valid check list.
	 *
	 * @return the integer
	 */
	public Integer validCheckList(){
		HolderAccountBalance[] arrTmpHolderAccountBalance=arrHolderAccountBalance;
		arrHolderAccountBalance = new HolderAccountBalance[arrTmpHolderAccountBalance.length+1];
		int j=0;
		
		for (int i = 0; i < arrTmpHolderAccountBalance.length; i++) {
			if(arrTmpHolderAccountBalance[i].getSelected()){
				if(Validations.validateIsNotNull(arrTmpHolderAccountBalance[i].getTransferAmmount())){
					arrHolderAccountBalance[j] = arrTmpHolderAccountBalance[i];		
					j++;
				}else{
					arrTmpHolderAccountBalance[i].setSelected(false);
					arrTmpHolderAccountBalance[i].setDisabled(true);
				}
			}
		}
		return j;
	}


	/**
	 * Return the currency Name whit parameterTableCd.
	 *
	 * @param parameterTableCd the parameter table cd
	 * @return the string
	 */
	public String currencyName(String parameterTableCd){
		try {
			ParameterTableTO filter = new ParameterTableTO();
			filter.setMasterTableFk(MasterTableType.CURRENCY.getCode());
			filter.setParameterTableCd(parameterTableCd);
			return generalParametersFacade.getComboParameterTable(filter).get(0).getParameterName();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Return the State name whit parameterTableCd.
	 *
	 * @param parameterTablePk the parameter table pk
	 * @return the string
	 */
	public String AccountstateName(Integer parameterTablePk){
		try {
			ParameterTableTO filter = new ParameterTableTO();
			filter.setParameterTablePk(parameterTablePk);
			return generalParametersFacade.getComboParameterTable(filter).get(0).getParameterName();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	//APROBADO
	/**
	 * Load mapstate name.
	 */
	public void loadMapstateName(){
		try {
			ParameterTableTO filter = new ParameterTableTO();
			filter.setMasterTableFk(MasterTableType.SECURITY_TRANSFER_STATE.getCode());
			List<ParameterTable> lstParameter = generalParametersFacade.getComboParameterTable(filter);
			for (ParameterTable parameterTable : lstParameter) {
				mapStateSecurityTransferOperation.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Valid the Transfer type.
	 */
	
	
	public void validateBalance(){
		try {
			if(Validations.validateIsNotNull(securityTransferOperation.getSecurities().getIdSecurityCodePk())){
				securityTransferOperation.setSecurities(transferAvailableFacade.getSecurity(securityTransferOperation.getSecurities().getIdSecurityCodePk()));
				
				if( Validations.validateIsNotNull(securityTransferOperation.getSecurities()) &&
					Validations.validateIsNotNull(securityTransferOperation.getSecurities().getIdSecurityCodePk()) ){
					if(!securityTransferOperation.getSecurities().getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())){
						confirmAlertAction = "clearTargetSecurity";
						String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID);
						alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),message);
					}
				}else{
					String message = PropertiesUtilities.getMessage(PropertiesConstants.SECURITY_DONT_EXIST);
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),message);
				}
				
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//APROBADO
	/**
	 * Valid transfer type.
	 */
	public void validTransferType(){
		if( Validations.validateIsNotNullAndPositive(securityTransferOperation.getTransferType())){
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
				flagTargetSource = false;
				flagSameParticipant = false;
				flagSourceTarget = true;
				clearTargetParticipant();
				flagSearch=true;
				if(isParticipant){
					flagRegisterOriginParticipant=true;
					flagRegisterTargetParticipant=false;
					securityTransferOperation.setSourceParticipant(Defaultparticipant);
				}
			}else if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				flagSameParticipant = false;
				flagTargetSource = true;
				flagSourceTarget = false;
				flagSearch=false;
				clearSourceParticipant();
				if(isParticipant){
					flagRegisterOriginParticipant=false;
					flagRegisterTargetParticipant=true;
					securityTransferOperation.setTargetParticipant(Defaultparticipant);
				}
			}else if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				flagSameParticipant = true;
				flagTargetSource = false;
				flagSourceTarget = true;
				flagSearch=true;
				clearTargetParticipant();

				if(isParticipant){
					securityTransferOperation.setSourceParticipant(Defaultparticipant);
					flagRegisterOriginParticipant=true;
					flagRegisterTargetParticipant=false;
				}
				
				if(Validations.validateIsNotNull(securityTransferOperation.getSourceParticipant()) && 
				   Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk()) ){
					securityTransferOperation.setTargetParticipant(securityTransferOperation.getSourceParticipant());
					
					if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()) ){
						targetHolder = new Holder();
						targetHolder.setIdHolderPk(sourceHolder.getIdHolderPk());
						targetHolder.setDescriptionHolder(sourceHolder.getDescriptionHolder());
						targetHolder.setFirstLastName(sourceHolder.getFirstLastName());
						targetHolder.setName(sourceHolder.getName());
						targetHolder.setFullName(sourceHolder.getFullName());
						targetHolder.setSecondLastName(sourceHolder.getSecondLastName());
						targetHolder.setHolderType(sourceHolder.getHolderType());
						
						HolderTO holderTO = new HolderTO();
						holderTO.setHolderId(sourceHolder.getIdHolderPk());
						holderTO.setFlagAllHolders(true);
						holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
						List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
						try {
							lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						lstTargetAccountTo = lstAccountTo;
						
						if(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) && 
							Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())	){
							
							for(HolderAccountHelperResultTO obj:lstTargetAccountTo){
								if(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().equals(obj.getAccountPk())){
									obj.setDisabled(true);
								}else{
									obj.setDisabled(false);
								}
							}
						}
					}
					
				}
			}
			dataTableRegistry();
		}
		
	}
	
	/**
	 * Data table registry.
	 */
	public void dataTableRegistry(){
		lstHolderAccountBalance = null;
		dataModelHolderAccountBalance = null;
		arrHolderAccountBalance = null;
	}
	
	//APROBADO
	
	/**
	 * Valid target participant.
	 */
	
	public void validTargetParticipant(){
		
		Participant tmpParticipant = securityTransferOperation.getTargetParticipant();
		Security objSecurities=securityTransferOperation.getSecurities();
		clearTargetParticipant();
		securityTransferOperation.setTargetParticipant(tmpParticipant);
		
		
		
		if( 	Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk())
			&& 	Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
			&& !securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())
			&&	securityTransferOperation.getSourceParticipant().getIdParticipantPk().
			 equals(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
		){
			String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.EQUALS_PARTICIPANT);
			
			if(flagTargetSource){
				confirmAlertAction="clearSourceParticipant";
			}else if(flagSourceTarget){
				confirmAlertAction="clearTargetParticipant";
			}
			
			alert(headerMessage,bodyMessage);
		}
		dataTableRegistry();
		securityTransferOperation.setSecurities(objSecurities);
	}
	
	/**
	 * Valid source participant.
	 */
	public void validSourceParticipant(){

		Participant tmpParticipant = securityTransferOperation.getSourceParticipant();
		clearSourceParticipant();
		securityTransferOperation.setSourceParticipant(tmpParticipant);
		if(flagSourceTarget || flagSameParticipant){

			clearTargetParticipant();
			
			if(flagSameParticipant){
				clearTargetParticipant();
				securityTransferOperation.setTargetParticipant(tmpParticipant);
			}
		}
		
		if( 	Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk())
				&& 	Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
				&& !securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())
				&&	securityTransferOperation.getSourceParticipant().getIdParticipantPk().
				 equals(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
			){
			
				if(flagTargetSource){
					confirmAlertAction="clearSourceParticipant";
				}else if(flagSourceTarget){
					confirmAlertAction="clearTargetParticipant";
				}
				String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
				String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.EQUALS_PARTICIPANT);
				alert(headerMessage,bodyMessage);
		}
		dataTableRegistry();
	}
	
	//APROBADO
	/**
	 * Valid source holder account.
	 */
	
	public void validSourceHolder(){
		securityTransferOperation.setSourceHolderAccount(new HolderAccount());
		securityTransferOperation.setSecurities(new Security());
		
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())){
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			sourceAccountTo = new HolderAccountHelperResultTO();
			lstHolderAccountBalance = null;
			dataModelHolderAccountBalance = null;
			arrHolderAccountBalance = null;
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				targetHolder = new Holder();
				lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				targetAccountTo = new HolderAccountHelperResultTO();
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
			}
			
			
			if( !sourceHolder.getStateHolder().equals(HolderStateType.REGISTERED.getCode()) ){
				
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CUI_NOT_REGISTER));
				sourceHolder = new Holder();
				lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				sourceAccountTo = new HolderAccountHelperResultTO();
				if(!securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
					lstHolderAccountBalance = null;
					arrHolderAccountBalance= null;
					dataModelHolderAccountBalance= null;
				}
				
				if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
					targetHolder = new Holder();
					lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					targetAccountTo = new HolderAccountHelperResultTO();
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				}
				
				return;
			}
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sourceHolder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			
			try {
				
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				if(lstAccountTo.size()>0){
					lstSourceAccountTo = lstAccountTo;
					
					if(flagSameParticipant){
						targetHolder = new Holder();
						targetHolder.setIdHolderPk(sourceHolder.getIdHolderPk());
						targetHolder.setDescriptionHolder(sourceHolder.getDescriptionHolder());
						targetHolder.setFirstLastName(sourceHolder.getFirstLastName());
						targetHolder.setName(sourceHolder.getName());
						targetHolder.setFullName(sourceHolder.getFullName());
						targetHolder.setSecondLastName(sourceHolder.getSecondLastName());
						targetHolder.setHolderType(sourceHolder.getHolderType());
						
						holderTO = new HolderTO();
						holderTO.setHolderId(sourceHolder.getIdHolderPk());
						holderTO.setFlagAllHolders(true);
						holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
						lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
						lstTargetAccountTo = lstAccountTo;
					}
					if(lstAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
						sourceAccountTo=lstAccountTo.get(0);
						validSourceHolderAccount();
					}
				}else{
					sourceHolder = new Holder();
					lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					sourceAccountTo = new HolderAccountHelperResultTO();
					String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT);
					alert(mensaje);
				}
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			targetAccountTo = new HolderAccountHelperResultTO();
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			securityTransferOperation.setSourceHolderAccount(new HolderAccount());
			
			if(flagSourceTarget || flagSameParticipant){
				Integer securityClass = securityTransferOperation.getSecurities().getSecurityClass();
				securityTransferOperation.setSecurities(new Security());
				securityTransferOperation.getSecurities().setSecurityClass(securityClass);
			}
			if(!securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				lstHolderAccountBalance = null;
				arrHolderAccountBalance= null;
				dataModelHolderAccountBalance= null;
			}
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				targetHolder = new Holder();
				lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				targetAccountTo = new HolderAccountHelperResultTO();
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				
			}
			
		}
	}

	/**
	 * Valid holder search.
	 */
	public void validHolderSearch(){
		sourceHolder.getIdHolderPk();
		searchSecurityTransferOperation.getParticipant();
	}
	
	
	/**
	 * Valid target holder.
	 */
	public void validTargetHolder(){
		
		if(Validations.validateIsNotNull(targetHolder) && Validations.validateIsNotNullAndPositive(targetHolder.getIdHolderPk())){
			if( !targetHolder.getStateHolder().equals(HolderStateType.REGISTERED.getCode()) ){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_CUI_NOT_REGISTER));
				
				targetHolder = new Holder();
				lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				targetAccountTo = new HolderAccountHelperResultTO();
				
				return;
			}
			
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(targetHolder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
			holderTO.setPersonType(securityTransferOperation.getSourceHolderAccount().getAccountType());
			
			try {
				
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				for(HolderAccountHelperResultTO holderAccTo: lstAccountTo){
					if(holderAccTo.getAccountPk().equals(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())){
						lstAccountTo.remove(holderAccTo);
					}
				}
				lstTargetAccountTo = lstAccountTo;
				
				if(lstTargetAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
					targetAccountTo=lstTargetAccountTo.get(0);
					validTargetHolderAccount();
				}
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			targetHolder = new Holder();
			lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			targetAccountTo = new HolderAccountHelperResultTO();
		}
	}
	
	
	/**
	 * Valid source holder account.
	 */
	public void validSourceHolderAccount(){

			securityTransferOperation.setSecurities(new Security());
			if(Validations.validateIsNotNull(sourceAccountTo) && Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountNumber()) ){
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(sourceAccountTo.getAccountPk());
				securityTransferOperation.setSourceHolderAccount(holderAccount);
				
				arrHolderAccountBalance = null;
				lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
				dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
				mapTransferMarketfact = new HashMap<String, List<HolderMarketFactBalance>>(); 
				try {
							
					if(!(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) &&
						 Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())) ){
						return;
					}
						
					holderAccount = getHolderAccountAndDetails(securityTransferOperation.getSourceHolderAccount());
					
					if(flagSourceTarget || flagSameParticipant){
						Integer securityClass = securityTransferOperation.getSecurities().getSecurityClass();
						securityTransferOperation.setSecurities(new Security());
						securityTransferOperation.getSecurities().setSecurityClass(securityClass);
					}
					
					if(flagSameParticipant){
						targetAccountTo = null;
					}
					
					if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_ACCOUNT_NOT_REGISTERED));
						securityTransferOperation.setSourceHolderAccount(new HolderAccount());
						sourceAccountTo = new HolderAccountHelperResultTO();
						if(flagSameParticipant){
							for(HolderAccountHelperResultTO obj:lstTargetAccountTo){
								obj.setDisabled(false);
							}
						}
						return;
					}
					
					if(flagSameParticipant){
						boolean disableAnyRegistry=false;
						for(HolderAccountHelperResultTO obj:lstTargetAccountTo){
							if(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().equals(obj.getAccountPk())){
								obj.setDisabled(true);
								disableAnyRegistry=true;
							}else{
								obj.setDisabled(false);
							}
						}
						if(disableAnyRegistry){
							return;
						}
						
						if( 
							Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()) && 
							Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())
						){	
							
							if(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().equals(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())){
								
									alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_IS_EQUAL));
									securityTransferOperation.setTargetHolderAccount(new HolderAccount());
									targetAccountTo = new HolderAccountHelperResultTO();
									dataTableRegistry();
									return;
								}
							
							if( !transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation) ){
								securityTransferOperation.setTargetHolderAccount(new HolderAccount());
								targetAccountTo = new HolderAccountHelperResultTO();
								alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
								return;
							}
						}
					}
					securityTransferOperation.setSourceHolderAccount(holderAccount);
					dataTableRegistry();
					
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_NOT_SELECTED));
				if(flagSameParticipant){
					targetAccountTo = null;
				}
			}
	}
	
	/**
	 * Valid target holder account.
	 */
	public void validTargetHolderAccount(){
		

		if(flagSameParticipant && !(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) && 
		   Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())) ){
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_SOURCE));					
			securityTransferOperation.setTargetHolderAccount(new HolderAccount());
			targetAccountTo = new HolderAccountHelperResultTO();
			return;
		}
		
		
		if(Validations.validateIsNotNull(targetAccountTo) && Validations.validateIsNotNullAndPositive(targetAccountTo.getAccountNumber()) ){
			HolderAccount holderAccount = new HolderAccount();
			holderAccount.setIdHolderAccountPk(targetAccountTo.getAccountPk());
			securityTransferOperation.setTargetHolderAccount(holderAccount);
		
			try {
								
				if(!(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) &&
					 Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
					targetAccountTo = new HolderAccountHelperResultTO();
					return;
				}
					
				holderAccount = getHolderAccountAndDetails(securityTransferOperation.getTargetHolderAccount());
				
				if(!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_ACCOUNT_NOT_REGISTERED));
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
					targetAccountTo = new HolderAccountHelperResultTO();
					return;
				}
				
				if(flagSameParticipant){
					
					if( 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()) && 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())
					){
						if(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().
							equals(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())){
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_IS_EQUAL));
							securityTransferOperation.setTargetHolderAccount(new HolderAccount());
							targetAccountTo = new HolderAccountHelperResultTO();
							dataTableRegistry();
							return;
						}
						if( !transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation) ){
							securityTransferOperation.setTargetHolderAccount(new HolderAccount());
							targetAccountTo = new HolderAccountHelperResultTO();
							alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
							dataTableRegistry();
							return;
						}
					}
				}
				securityTransferOperation.setTargetHolderAccount(holderAccount);
				dataTableRegistry();
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_NOT_SELECTED));
		}
	}
	
	/**
	 * Unvalid source holder account.
	 */
	public void unvalidSourceHolderAccount(){
		securityTransferOperation.setSourceHolderAccount(null);
		dataTableRegistry();
	}
	
	//APROBADO
	/**
	 * Gets the holder account and details.
	 *
	 * @param holderAccount the holder account
	 * @return the holder account and details
	 */
	public HolderAccount getHolderAccountAndDetails(HolderAccount holderAccount){
		try {
			holderAccount = transferAvailableFacade.getHolderAccount(holderAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return holderAccount;
	}


	
	/**
	 * Valid target holder account.
	 */
		
	/**
	 * add alert for validations.
	 */
	@LoggerAuditWeb
	public void alertAnulledOrReject(){
		String messages = "";
		if(securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			messages =PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ANULL);
		}else{
			messages =PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT);
		}

		validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_MOTIVE_ANULLED_REJECT,new Object[]{messages.toLowerCase()});
		alterRejectCancel(validationMessage);
	}
	
	/**
	 * Alert multi anulled or reject.
	 */
	@LoggerAuditWeb
	public void alertMultiAnulledOrReject(){

		String messages = "";
		if(arrSecurityTransferOperation[0].getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			messages =PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ANULL);
		}else{
			messages =PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT);
		}
		
		validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_MOTIVE_ANULLED_REJECT,new Object[]{messages});
		alterRejectCancel(validationMessage);
	}
	
	/**
	 * clear all fields.
	 */
	public void clearFields(){
		securityTransferOperation = new SecurityTransferOperation();
		securityTransferOperation.setTransferType(null);
		flagTargetSource = false;
		flagSameParticipant = false;
		flagSourceTarget = true;
	}
	
	/**
	 *  
	 * Load Security table.
	 *
	 * @param security the security
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	//@LoggerAuditWeb <- lo llaman
	public List<Security> listSecurity(Security security) throws ServiceException{
		return transferAvailableFacade.getLisHolderAccountServiceBean(security);
	}
	
	//APROBADO
	/**
	 * Load combos.
	 */
	public void loadCombos(){
		try {
			
			if(lstTransferType.size()<=0){
				loadTransferType();
			}

			if (lstParticipant.size()<=0) {
				if(isParticipant){
					
					loadParticipant();
					if(Validations.validateIsNotNull(Defaultparticipant) && 
					   Validations.validateIsNotNullAndPositive(Defaultparticipant.getIdParticipantPk())){
						boolean flagInList = false;
						for(Participant obj: lstParticipant){
							if(obj.getIdParticipantPk().equals(Defaultparticipant.getIdParticipantPk())){
								flagInList = true;
								break;
							}
						}
						if(!flagInList){
							lstParticipant.add(Defaultparticipant);
						}
					}
					
				}else{
					loadParticipant();
				}
			}
			
			if(lstTransferState.size()<=0){
				loadTransferState();
				loadMapstateName();
			}
			
			if(lstHolderAccountState.size()<=0){
				loadHolderAccountState();
			}
			
			if(lstTransferActions.size()<=0){
				loadTransferActions();
			}
			
			if(lstInstrumentType.size()<=0){
				loadInstrumentType();
			}
			
			if(lstSecurityClass.size()<=0){
				loadSecurityClass();
			}
			
			if(lstSecurityType.size()<=0){
				loadSecurityType();
			}
				
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//APORBADO
	/**
	 * Load rejector cancel combos.
	 */
	public void loadRejectorCancelCombos(){
		
		try {
			
			if(rejectMotiveList == null || rejectMotiveList.size()<=0){
				rejectMotiveList = new ArrayList<ParameterTable>();
				ParameterTableTO paramTable = new ParameterTableTO();
				paramTable.setMasterTableFk(MasterTableType.MASTER_TABLE_REJECT_MOTIVE_TRANSFER_SECURITIES_AVAILABLE.getCode());
				paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
				for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
					rejectMotiveList.add(param);
				}
			}
			
			otherRejectMotiveId = new Integer(INT_OTHER_MOTIVE_REJECT);
			intRejectMotive=null;
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//APROBADO
	/**
	 * Post constructor search.
	 */
	@PostConstruct
	public void init(){
		/*POST CONSTRUCTOR QUE CARGA POR DEFECTO*/
		minTime = CommonsUtilities.addDate(CommonsUtilities.currentDate(), -maxDaysOfCustodyRequest);
		searchSecurityTransferOperation = inicializeSecurityTransferOperation();
		securityEvents();
		loadCombos();
		flagIndMarketFact = Boolean.FALSE;
	}
	
	/**
	 * Clean after actions.
	 */
	public void cleanAfterActions()
	{
		securityEvents();
		sourceHolder=new Holder();
		lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
		searchSolicitud();
	}
	/**
	 * Security events.
	 */
	public void securityEvents(){
		try {
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || 
					userInfo.getUserAccountSession().isParticipantInvestorInstitucion() || 
					userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){					
					Long participantDpf = getIssuerDpfInstitution(userInfo);					
					if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
						participantLogin = participantDpf;
					}else{
						//para la busqueda
						flagSearchParticipant = true;
					}
				}else if( Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode()) ){
					participantLogin = userInfo.getUserAccountSession().getParticipantCode();					
				}
				if(participantLogin !=null &&  participantLogin>0){
					isParticipant = true;
					if( !Validations.validateIsNotNullAndPositive(Defaultparticipant.getIdParticipantPk()) ){
						Defaultparticipant = helperComponentFacade.findParticipantsByCode(participantLogin);
						//para la busqueda
						flagSearchParticipant = true;
						//para el registro
						flagRegisterOriginParticipant=true;
						flagRegisterTargetParticipant=false;
					}
					searchSecurityTransferOperation.setParticipant(Defaultparticipant);
					JSFUtilities.updateComponent("frmCustody:idParticipantOrigin");		
				}			
					
			}else if(userInfo.getUserAccountSession().isDepositaryInstitution()){
				isDepositary = true;
			}
			
			if(userPrivilege.getUserAcctions().isRegister()){
				flagRegister=true;
			}
			
			if(userPrivilege.getUserAcctions().isSearch()){
				flagSearch=true;
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Load transfer state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTransferState() throws ServiceException{
		lstTransferState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITY_TRANSFER_STATE.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstTransferState.add(param);
		}
	}
	
	/**
	 * Load transfer state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadHolderAccountState() throws ServiceException{
		lstHolderAccountState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstHolderAccountState.add(param);
			mapStateHolderAccount.put(param.getParameterTablePk(), param.getDescription());
		}
	}
	
	/**
	 * Gets the map holder account state.
	 *
	 * @param code the code
	 * @return the map holder account state
	 */
	public String getMapHolderAccountState(Integer code){
		return mapStateHolderAccount.get(code);
	}
	
	/**
	 * Load transfer actions.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTransferActions() throws ServiceException{
		lstTransferActions = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(294);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstTransferActions.add(param);
		}
	}
	
	/**
	 * Load transfer type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTransferType() throws ServiceException{
		lstTransferType = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.TRANSFER_TYPE.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstTransferType.add(param);
		}
	}
	
	/**
	 * Load instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadInstrumentType() throws ServiceException{
		lstInstrumentType = new ArrayList<ParameterTable>();
		mapSecurityInstrumentType = new HashMap<Integer, String>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstInstrumentType.add(param);
			mapSecurityInstrumentType.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Load security type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityType() throws ServiceException{
		lstSecurityType = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_TYPE.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstSecurityType.add(param);
			mapSecurityType.put(param.getParameterTablePk(), param.getParameterName());
		}
	}
	
	/**
	 * Load security class.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityClass() throws ServiceException{
		lstSecurityClass = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		paramTable.setOrderByText1(1);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstSecurityClass.add(param);
		}
		if(super.getParametersTableMap()==null){
			super.setParametersTableMap(new HashMap<Integer,Object>());
		}
		billParametersTableMap(lstSecurityClass, true);
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && lstSecurityClass!=null 
				&& lstSecurityClass.size()>0){
			/** The lst instrument type. */
			List<ParameterTable> lstSecurityClassTemp = new ArrayList<ParameterTable>();
			for(ParameterTable objParameterTable :  lstSecurityClass){
				if(objParameterTable.getParameterTablePk().equals(SecurityClassType.DPA.getCode()) || 
						objParameterTable.getParameterTablePk().equals(SecurityClassType.DPF.getCode())){
					lstSecurityClassTemp.add(objParameterTable);
				}
			}
			lstSecurityClass = lstSecurityClassTemp;
		}
	}
	
	//PROBADO
	/**
	 * Load participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipant()throws ServiceException{
		lstParticipant = new ArrayList<Participant>();
		Map<String, Object> filter = new HashMap<String, Object>();
		List<Integer> states = new ArrayList<Integer>();
		List<Integer> indDepositary = new ArrayList<Integer>();
		states.add(ParticipantStateType.REGISTERED.getCode());
		indDepositary.add(1);
		filter.put("states", states);
		filter.put("indDepositary", indDepositary);
		lstTargetParticipant=helperComponentFacade.getComboParticipantsByMapFilter(filter);
		filter.remove("indDepositary");
		filter.remove("states");
		if(userInfo.getUserAccountSession().isDepositaryInstitution()){
			states.add(ParticipantStateType.BLOCKED.getCode());
			states.add(ParticipantStateType.UNIFIED.getCode());
		}
		filter.put("states", states);
		lstParticipant = helperComponentFacade.getComboParticipantsByMapFilter(filter);
	}
	
	//APROBADO
	/**
	 * Radio select event.
	 */
	public void radioSelectEvent(){
		clearSearchTransfer();
	}
	
	/**
	 * Check security transfer available.
	 *
	 * @param e the e
	 */
	public void checkSecurityTransferAvailable(SelectEvent e){
		SecurityTransferOperation securityTransferOperation = (SecurityTransferOperation)e.getObject();
		for(SecurityTransferOperation arrTransferOperation:arrSecurityTransferOperation){
			if( arrTransferOperation.getIdTransferOperationPk().equals(securityTransferOperation.getIdTransferOperationPk()) ){
				arrTransferOperation.setSelected(true);
			}
		}
	}
	
	
	/**
	 * Un check security transfer available.
	 *
	 * @param e the e
	 */
	public void unCheckSecurityTransferAvailable(UnselectEvent e){
		SecurityTransferOperation securityTransferOperation = (SecurityTransferOperation)e.getObject();
		securityTransferOperation.setSelected(false);
		for(SecurityTransferOperation arrTransferOperation:lstTransferSecurityAvailable){
			
			if( arrTransferOperation.getIdTransferOperationPk().equals(securityTransferOperation.getIdTransferOperationPk()) ){
				arrTransferOperation.setSelected(false);
				
				if(arrTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
					arrTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
				){
					arrTransferOperation.setSourceHolderAccount(new HolderAccount());
				}
				if(arrTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) &&
					arrTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
				){
					arrTransferOperation.setTargetHolderAccount(new HolderAccount());
				}
			}
		}
		dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
	}
	
	
	
	/**
	 * Setting source view account.
	 */
	public void settingSourceViewAccount(){
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk(sourceAccountTo.getAccountPk());
		holderAccount.setAccountNumber(sourceAccountTo.getAccountNumber());
		securityTransferOperation.setSourceHolderAccount(holderAccount);
		settingUniqueContrapartAccount(false);
		
		//verifico si el valor le pertenece al participante y cuenta	
		HolderAccountBalance filter = new HolderAccountBalance();
		HolderAccountBalancePK id = new HolderAccountBalancePK();
		id.setIdHolderAccountPk(holderAccount.getIdHolderAccountPk());
		id.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
		id.setIdSecurityCodePk(lstHolderAccountBalance.get(0).getId().getIdSecurityCodePk());
		
		filter.setId(id);
		
		try {
			
			if(transferAvailableFacade.getExistSecurityForHolder(filter).intValue() <= 0){	//NO EXISTE
				sourceAccountTo = null;
				securityTransferOperation.setSourceHolderAccount(new HolderAccount());
				String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_PARTICIPANT_ACCOUNT_NO_RELATION);
				alert(message);
				return;
				
			}else{	//SI EXISTE

				//si pasa todas las validaciones

				List<HolderAccountBalance> lstTmp = new ArrayList<HolderAccountBalance>();
				lstTmp.addAll(lstHolderAccountBalance);
				BigDecimal bigDTmp = lstHolderAccountBalance.get(0).getTransferAmmount();
				
				lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
				lstHolderAccountBalance = transferAvailableFacade.getListHolderAccountBalance(filter, false, new Security(), userInfo);
				
				if(lstHolderAccountBalance.size()>0){
					lstHolderAccountBalance.get(0).setTransferAmmount(bigDTmp);
				}else{
					lstHolderAccountBalance = lstTmp;
				}
				dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Setting target view account.
	 */
	public void settingTargetViewAccount(){
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk(targetAccountTo.getAccountPk());
		holderAccount.setAccountNumber(targetAccountTo.getAccountNumber());
		securityTransferOperation.setTargetHolderAccount(holderAccount);
		settingUniqueContrapartAccount(false);
	}
	
	/**
	 * Setting unique contrapart account.
	 *
	 * @param cleanData the new ting unique contrapart account
	 */
	public void settingUniqueContrapartAccount(boolean cleanData){

		try {
					
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
				
				HolderAccount holderAccount = new HolderAccount();
				if(!Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getAccountNumber())){
					if(cleanData){
						securityTransferOperation.setTargetHolderAccount(holderAccount);
					}
					return;
				}
				holderAccount.setParticipant(securityTransferOperation.getTargetParticipant());
				holderAccount.setAccountNumber(securityTransferOperation.getTargetHolderAccount().getAccountNumber());
				holderAccount = getHolderAccountAndDetails(holderAccount);
				
				if( !(Validations.validateIsNotNull(holderAccount) &&
					  Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
					if(cleanData){
						securityTransferOperation.setTargetHolderAccount(new HolderAccount());
						targetHolder = new Holder();
						lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					}
					targetAccountTo = new HolderAccountHelperResultTO();
					return;
				}else{
						securityTransferOperation.setTargetHolderAccount(holderAccount);
				}
				
				if(!transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation)){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
					if(cleanData){
						securityTransferOperation.setTargetHolderAccount(new HolderAccount());
						targetHolder = new Holder();
						lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					}
					targetAccountTo = new HolderAccountHelperResultTO();
					return;
				}
				
			}else if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
	
				HolderAccount holderAccount = new HolderAccount();
				/*if(	Validations.validateIsNull(securityTransferOperation.getSourceHolderAccount()) &&
						Validations.validateIsNull(securityTransferOperation.getSourceHolderAccount().getAccountNumber())){
					if(cleanData){
						securityTransferOperation.setSourceHolderAccount(holderAccount);
					}
					return;
				}*/
				
				holderAccount.setParticipant(securityTransferOperation.getTargetParticipant());
				holderAccount.setAccountNumber(securityTransferOperation.getTargetHolderAccount().getAccountNumber());
				holderAccount = getHolderAccountAndDetails(holderAccount);
				
				if( !(Validations.validateIsNotNull(holderAccount) &&
					  Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
					if(cleanData){
						securityTransferOperation.setTargetHolderAccount(new HolderAccount());
						targetHolder = new Holder();
						lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					}
					sourceAccountTo = new HolderAccountHelperResultTO();
					return;
				}else{
					securityTransferOperation.setTargetHolderAccount(holderAccount);
				}
				
				if(!transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation)){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
					if(cleanData){
						securityTransferOperation.setTargetHolderAccount(new HolderAccount());
						targetHolder = new Holder();
						lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
					}
					targetAccountTo = new HolderAccountHelperResultTO();
					return;
				}
				if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())){
					boolean todate = false;
					if(securityTransferOperation.getCustodyOperation().getRegistryDate().equals(CommonsUtilities.currentDate())){
						todate = true;
					}
					List<MarketFactDetailHelpTO> lstMarketFact = transferAvailableFacade.getListMarketFactBalanceInTransferOperation(securityTransferOperation.getCustodyOperation().getIdCustodyOperationPk(), todate);
					if(lstMarketFact !=null && lstMarketFact.size()>0){
					helpMarketFactBalanceView.setMarketFacBalances(lstMarketFact);
					helpMarketFactBalanceView.setTodate(todate);
					}
				}
				
			}
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	/**
	 * Sets the ting contrapart account.
	 *
	 * @param transferOperation the new ting contrapart account
	 */
	
	public void settingSourceAccountContrapart(SecurityTransferOperation transferOperation){
	
		try {
			transferOperation.setLstHolderMarketfactBalance(null); 
			if( Validations.validateIsNotNull(transferOperation.getSourceHolderAccount()) && 
				Validations.validateIsNotNullAndPositive(transferOperation.getSourceHolderAccount().getAccountNumber()) ){
	
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setParticipant(transferOperation.getSourceParticipant());
				holderAccount.setAccountNumber(transferOperation.getSourceHolderAccount().getAccountNumber());
				holderAccount = getHolderAccountAndDetails(holderAccount);
				
				if(!(Validations.validateIsNotNull(holderAccount) && 
				     Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
					
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
					transferOperation.getSourceHolderAccount().setAccountNumber(null);
					return;
					
				}else{
					if( !holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()) ){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
						transferOperation.getSourceHolderAccount().setAccountNumber(null);
						return;
					}else{
						//validar que tienen el mismo rnt
						transferOperation.setSourceHolderAccount(holderAccount);
							if(!transferAvailableFacade.equalsRntWithHolderAccount(transferOperation)){
								transferOperation.setSourceHolderAccount(new HolderAccount());
								alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
								return;
							}else{
								//SI TIENEN EL MISMO RNT
								//validar que el valor le pertenesca
								HolderAccountBalancePK id = new HolderAccountBalancePK();
								id.setIdHolderAccountPk(transferOperation.getSourceHolderAccount().getIdHolderAccountPk());
								id.setIdParticipantPk(transferOperation.getSourceParticipant().getIdParticipantPk());
								id.setIdSecurityCodePk(transferOperation.getSecurities().getIdSecurityCodePk());
								HolderAccountBalance filter = new HolderAccountBalance();
								filter.setId(id);
								
								if(transferAvailableFacade.getExistSecurityForHolder(filter) <= 0){	//NO LE PERTENECE
									for(SecurityTransferOperation arrTransferOperation:lstTransferSecurityAvailable){
										if( arrTransferOperation.getIdTransferOperationPk().equals(transferOperation.getIdTransferOperationPk()) ){
											arrTransferOperation.setEnabledForReview(true); //no lo dejo revisar
											arrTransferOperation.setSourceHolderAccount(new HolderAccount());
											dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
										}
									}
									String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_PARTICIPANT_ACCOUNT_NO_RELATION);
									alert(message);
									return;
								}else{	//SI TIENE RELACION
									
									//VALIDACIONES CONSIDERANDO ES UNA REVISION DESTINO-ORIGEN
									
									//BUSCO LOS HECHOS DEL MERCADO
									MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
									marketFactBalance.setSecurityCodePk(transferOperation.getSecurities().getIdSecurityCodePk());
									marketFactBalance.setHolderAccountPk(transferOperation.getSourceHolderAccount().getIdHolderAccountPk());
									marketFactBalance.setParticipantPk(transferOperation.getSourceParticipant().getIdParticipantPk());
									MarketFactBalanceHelpTO marketFactoResult = null;
									try {
										marketFactoResult = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
										marketFactoResult.setIndHandleDetails(BooleanType.YES.getCode());
									} catch (ServiceException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
										switch(e1.getErrorService()){
										case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
												alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
													  PropertiesUtilities.getExceptionMessage(e1.getMessage()));
												
										default:
											alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
										}
										transferOperation.getSourceHolderAccount().setAccountNumber(null);
										return;
									}

									if( marketFactoResult.getMarketFacBalances() != null && marketFactoResult.getMarketFacBalances().size() == 1 ){
										BigDecimal AvailableBalance =  marketFactoResult.getMarketFacBalances().get(0).getAvailableBalance();
										//List<HolderMarketFactBalance> lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
										//SI EL MONTO DISPONIBLE DEL UNICO HECHO DEL MERCADO CUBRE EL TRASPASO
										if(AvailableBalance.compareTo(transferOperation.getQuantityOperation()) == 1 || AvailableBalance.compareTo(transferOperation.getQuantityOperation()) == 0){
											
											//LLENO MI LISTA DE HECHO DEL MERCADO COMO UN UNICO REGISTRO
											List<HolderMarketFactBalance> lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
											MarketFactDetailHelpTO marketFact = marketFactoResult.getMarketFacBalances().get(0);
											
											HolderMarketFactBalance holderMarketfactBalance = new HolderMarketFactBalance();
											holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
											HolderAccount holderAccounts = new HolderAccount();
											holderAccounts.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
											holderMarketfactBalance.setHolderAccount(holderAccounts);
											Security security = new Security();
											security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
											holderMarketfactBalance.setSecurity(security);
											Participant participant = new Participant();
											participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
											holderMarketfactBalance.setParticipant(participant);
											holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
											holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
											holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
											holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
											holderMarketfactBalance.setTransferAmmount(transferOperation.getQuantityOperation());
											lstHolderAccountMF.add(holderMarketfactBalance);
											
											//SETEO EL MAPA DE LISTAS DE HECHO DEL MERCADO
											transferOperation.setLstHolderMarketfactBalance(lstHolderAccountMF); 
											transferOperation.setStyleClassButton(styleBtnActive);
											
										}else{//SI NO CUBRE EL MONTO

											alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MARKETFACT_AMOUNT_INVALID));
											transferOperation.setSourceHolderAccount(new HolderAccount());
											return;
										}
										
									}else{	//SI TIENE MAS DE UN HECHO DEL MERCADO
										transferOperation.setStyleClassButton(styleBtnPending);
									}
									
									
								}
								
								for(SecurityTransferOperation arrTransferOperation:lstTransferSecurityAvailable){
									if( arrTransferOperation.getIdTransferOperationPk().equals(transferOperation.getIdTransferOperationPk()) ){
										arrTransferOperation.setSourceHolderAccount(holderAccount);
										arrTransferOperation.setStyleClassButton(transferOperation.getStyleClassButton());
										dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
									}
								}
								
							}
					}
				}
			}else{
				transferOperation.setStyleClassButton(styleBtnPending);
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	/**
	 * Sets the ting target account contrapart.
	 *
	 * @param transferOperation the new ting target account contrapart
	 */
	public void settingTargetAccountContrapart(SecurityTransferOperation transferOperation){
		
		try {
	
			if( Validations.validateIsNotNull(transferOperation.getTargetHolderAccount()) && 
				Validations.validateIsNotNullAndPositive(transferOperation.getTargetHolderAccount().getAccountNumber()) ){
	
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setParticipant(transferOperation.getTargetParticipant());
				holderAccount.setAccountNumber(transferOperation.getTargetHolderAccount().getAccountNumber());
				holderAccount = getHolderAccountAndDetails(holderAccount);
				
				if(!(Validations.validateIsNotNull(holderAccount) && 
				   Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
					
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
					transferOperation.getTargetHolderAccount().setAccountNumber(null);
					return;
					
				}else{
					if( !holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode()) ){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
					}else{
						//validar que tienen el mismo rnt
						transferOperation.setTargetHolderAccount(holderAccount);
							if(!transferAvailableFacade.equalsRntWithHolderAccount(transferOperation)){
								transferOperation.setTargetHolderAccount(new HolderAccount());
								alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
								return;
							}
					}
				}
			}
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	/**
	 * Sets the ting contrapart account.
	 *
	 * @param transferOperation the new ting contrapart account
	 */
	public void settingContrapartAccount(SecurityTransferOperation transferOperation){

		try {
			
			for(SecurityTransferOperation arrTransferOperation:arrSecurityTransferOperation){
				if( arrTransferOperation.getIdTransferOperationPk().equals(transferOperation.getIdTransferOperationPk()) ){
					
					if(arrTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
						
						HolderAccount holderAccount = new HolderAccount();
						if(!Validations.validateIsNotNullAndPositive(transferOperation.getTargetHolderAccount().getAccountNumber())){
							arrTransferOperation.setTargetHolderAccount(holderAccount);
							return;
						}
						holderAccount.setParticipant(transferOperation.getTargetParticipant());
						holderAccount.setAccountNumber(transferOperation.getTargetHolderAccount().getAccountNumber());
						holderAccount = getHolderAccountAndDetails(holderAccount);
						
						if( !(Validations.validateIsNotNull(holderAccount) &&
							  Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
							arrTransferOperation.setTargetHolderAccount(new HolderAccount());
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
							return;
						}else{
							arrTransferOperation.setTargetHolderAccount(holderAccount);
						}
						
						if(!transferAvailableFacade.equalsRntWithHolderAccount(arrTransferOperation)){
							arrTransferOperation.setTargetHolderAccount(new HolderAccount());
							alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
							return;
						}
						
					}else if(arrTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){

						HolderAccount holderAccount = new HolderAccount();
						if(!Validations.validateIsNotNullAndPositive(transferOperation.getSourceHolderAccount().getAccountNumber())){
							arrTransferOperation.setSourceHolderAccount(holderAccount);
							return;
						}
						
						holderAccount.setParticipant(transferOperation.getSourceParticipant());
						holderAccount.setAccountNumber(transferOperation.getSourceHolderAccount().getAccountNumber());
						holderAccount = getHolderAccountAndDetails(holderAccount);
						
						if( !(Validations.validateIsNotNull(holderAccount) &&
							  Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
							arrTransferOperation.setSourceHolderAccount(new HolderAccount());
							alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
							return;
						}else{
							arrTransferOperation.setSourceHolderAccount(holderAccount);
						}
						
						if(!transferAvailableFacade.equalsRntWithHolderAccount(arrTransferOperation)){
							arrTransferOperation.setSourceHolderAccount(new HolderAccount());
							alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
							return;
						}
						
					}
					return ;
				}
			}
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Check holder account balance.
	 *
	 * @param e the e
	 */
	public void checkHolderAccountBalance(SelectEvent e){
		int j = validCheckList();	
		
		HolderAccountBalance holderAccB = (HolderAccountBalance)e.getObject();
		holderAccB.setTransferAmmount(null);			
		holderAccB.setSelected(true);
		holderAccB.setStyleClassButton(styleBtnPending);
		if(holderAccB.getSecurity().isDpfSecurityClass() || holderAccB.getSecurity().isDpaSecurityClass()){
			holderAccB.setTransferAmmount(GeneralConstants.ONE_VALUE_BIGDECIMAL);
			//inputMarketFactBalance(holderAccB);
		}else if(holderAccB.getMarketCount().equals(GeneralConstants.ONE_VALUE_LONG)) { 
			holderAccB.setTransferAmmount(holderAccB.getAvailableBalance());
			//inputMarketFactBalance(holderAccB);
		}else {
			mapTransferMarketfact.remove(holderAccB.getSecurity().getIdSecurityCodePk());
			//inputMarketFactBalance(holderAccB);
		}
		//
		arrHolderAccountBalance[j] = holderAccB;
			
	}
	
	/**
	 * Massive Check holder account balance.
	 *
	 * @param e the e
	 */
	public void massiveCheckHolderAccountBalance(ToggleSelectEvent e){
		for (int i = 0; i < lstHolderAccountBalance.size(); i++) {
			lstHolderAccountBalance.get(i).setTransferAmmount(null);
			lstHolderAccountBalance.get(i).setSelected(false);
			lstHolderAccountBalance.get(i).setDisabled(true);
			lstHolderAccountBalance.get(i).setStyleClassButton("");
		}
		dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
		
		HolderAccountBalance[] arrTmpHolderAccountBalance = arrHolderAccountBalance;
		arrHolderAccountBalance = new HolderAccountBalance[arrTmpHolderAccountBalance.length+1];
		int j=0;
		for (int i = 0; i < arrTmpHolderAccountBalance.length; i++) {
			HolderAccountBalance holderAccB = (HolderAccountBalance) arrTmpHolderAccountBalance[i];
			holderAccB.setTransferAmmount(null);			
			holderAccB.setSelected(true);
			holderAccB.setStyleClassButton(styleBtnPending);
			if( holderAccB.getSecurity().isDpfSecurityClass() || holderAccB.getSecurity().isDpaSecurityClass()){
				holderAccB.setTransferAmmount(GeneralConstants.ONE_VALUE_BIGDECIMAL);
				if (idepositarySetup.getIndMarketFact().equals(GeneralConstants.ONE_VALUE_INTEGER))
					inputMarketFactBalance(holderAccB);
			}else if(holderAccB.getMarketCount().equals(GeneralConstants.ONE_VALUE_LONG)) {
				holderAccB.setTransferAmmount(holderAccB.getAvailableBalance());
				if (idepositarySetup.getIndMarketFact().equals(GeneralConstants.ONE_VALUE_INTEGER))
					inputMarketFactBalance(holderAccB);
			}else {
				holderAccB.setTransferAmmount(holderAccB.getAvailableBalance());
			}
			if(Validations.validateIsNotNull(arrTmpHolderAccountBalance[i].getTransferAmmount())){
				arrHolderAccountBalance[j] = arrTmpHolderAccountBalance[i];
				j++;
			}else{
				arrTmpHolderAccountBalance[i].setSelected(false);
				arrTmpHolderAccountBalance[i].setDisabled(true);
			}
		}
	}
	
	/**
	 * Check holder account balance view.
	 *
	 * @param e the e
	 */
	public void checkHolderAccountBalanceView(SelectEvent e){		 
		
		HolderAccountBalance holderAccB = (HolderAccountBalance)e.getObject();
		
		//COMPARO SI EL MONTO DISPONIBLE CUBRE EL MONTO A TRANSFERIR INGRESADI
		if(holderAccB.getAvailableBalance().compareTo(holderAccB.getTransferAmmount()) == 1 || holderAccB.getAvailableBalance().compareTo(holderAccB.getTransferAmmount()) == 0 ){
			
			holderAccB.setSelected(true);
			arrHolderAccountBalance[0] = holderAccB;
			
			if( useMarketFact ) {
				//BUSCO LOS HECHOS DEL MERCADO
				MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
				marketFactBalance.setSecurityCodePk(holderAccB.getSecurity().getIdSecurityCodePk());
				marketFactBalance.setHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
				marketFactBalance.setParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
				MarketFactBalanceHelpTO marketFactoResult = null;
				try {
					marketFactoResult = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
					marketFactoResult.setIndHandleDetails(BooleanType.YES.getCode());
				} catch (ServiceException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					switch(e1.getErrorService()){
					case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
							alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								  PropertiesUtilities.getExceptionMessage(e1.getMessage()));
							
					default:
						alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
					}
					
					holderAccB.setStyleClassButton("");
					arrHolderAccountBalance = null;
					holderAccB.setSelected(false);
					
					return;
				}
				
				//SI TIENE SOLO UN HECHO DEL MERCADO
				List<HolderMarketFactBalance> lstHolderAccountMF=null;
				if( marketFactoResult.getMarketFacBalances() != null && marketFactoResult.getMarketFacBalances().size() == 1 ){
					BigDecimal AvailableBalance =  marketFactoResult.getMarketFacBalances().get(0).getAvailableBalance();
					
					//SI EL MONTO DISPONIBLE DEL UNICO HECHO DEL MERCADO CUBRE EL TRASPASO
					if(AvailableBalance.compareTo(holderAccB.getTransferAmmount()) == 1 || AvailableBalance.compareTo(holderAccB.getTransferAmmount()) == 0 ){
						holderAccB.setSelected(true);
						arrHolderAccountBalance[0] = holderAccB;
						
						//LLENO MI LISTA DE HECHO DEL MERCADO COMO UN UNICO REGISTRO
						lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
						MarketFactDetailHelpTO marketFact = marketFactoResult.getMarketFacBalances().get(0);
						
						HolderMarketFactBalance holderMarketfactBalance = new HolderMarketFactBalance();
						holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
						HolderAccount holderAccount = new HolderAccount();
						holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
						holderMarketfactBalance.setHolderAccount(holderAccount);
						Security security = new Security();
						security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
						holderMarketfactBalance.setSecurity(security);
						Participant participant = new Participant();
						participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
						holderMarketfactBalance.setParticipant(participant);
						holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
						holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
						holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
						holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
						holderMarketfactBalance.setTransferAmmount(holderAccB.getTransferAmmount());
						lstHolderAccountMF.add(holderMarketfactBalance);
						
						//SETEO EL MAPA DE LISTAS DE HECHO DEL MERCADO
						mapTransferMarketfact.put(holderAccB.getId().getIdSecurityCodePk(), lstHolderAccountMF);
						helpMarketFactBalanceView.setMarketFacBalances(marketFactoResult.getMarketFacBalances());
						holderAccB.setStyleClassButton(styleBtnActive);
					}else{//SI NO CUBRE EL MONTO
						alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MARKETFACT_AMOUNT_INVALID));
						arrHolderAccountBalance = null;
						holderAccB.setSelected(false);
						holderAccB.setStyleClassButton("");
					}
					
				}else{	//SI TIENE MAS DE UN HECHO DEL MERCADO
					holderAccB.setSelected(true);
					/*lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
					for( MarketFactDetailHelpTO marketFact : marketFactoResult.getMarketFacBalances()){
						HolderMarketFactBalance holderMarketfactBalance = new HolderMarketFactBalance();
						holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
						HolderAccount holderAccount = new HolderAccount();
						holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
						holderMarketfactBalance.setHolderAccount(holderAccount);
						Security security = new Security();
						security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
						holderMarketfactBalance.setSecurity(security);
						Participant participant = new Participant();
						participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
						holderMarketfactBalance.setParticipant(participant);
						holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
						holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
						holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
						holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
						holderMarketfactBalance.setTransferAmmount(holderAccB.getTransferAmmount());
						lstHolderAccountMF.add(holderMarketfactBalance);
					}
					mapTransferMarketfact.put(holderAccB.getId().getIdSecurityCodePk(), lstHolderAccountMF);
					helpMarketFactBalanceView.setMarketFacBalances(marketFactoResult.getMarketFacBalances());*/
					holderAccB.setStyleClassButton(styleBtnPending);
				}
			}
			
		}else{	//EL MONTO DISPONIBLE NO CUBRE EL MONTO INGRESADO
			alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_AMOUNT_INVALID));
			holderAccB.setSelected(false);
			arrHolderAccountBalance = null;
			holderAccB.setStyleClassButton("");
			return;
		}
		
	}
	
	/**
	 * Check holder account balance destiny origin.
	 *
	 * @param holderAccB the holder acc b
	 */
	public void checkHolderAccountBalanceDestinyOrigin(HolderAccountBalance holderAccB){	
		//COMPARO SI EL MONTO DISPONIBLE CUBRE EL MONTO A TRANSFERIR INGRESADI
		if(holderAccB.getAvailableBalance().compareTo(holderAccB.getTransferAmmount()) == 1 || holderAccB.getAvailableBalance().compareTo(holderAccB.getTransferAmmount()) == 0 ){

			//BUSCO LOS HECHOS DEL MERCADO
			MarketFactBalanceHelpTO marketFactBalance = new MarketFactBalanceHelpTO();
			marketFactBalance.setSecurityCodePk(holderAccB.getId().getIdSecurityCodePk());
			marketFactBalance.setHolderAccountPk(holderAccB.getId().getIdHolderAccountPk());
			marketFactBalance.setParticipantPk(holderAccB.getId().getIdParticipantPk());
			MarketFactBalanceHelpTO marketFactoResult = null;
			try {
				marketFactoResult = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
				marketFactoResult.setIndHandleDetails(BooleanType.YES.getCode());
			} catch (ServiceException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				switch(e1.getErrorService()){
				case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT:
						alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
							  PropertiesUtilities.getExceptionMessage(e1.getMessage()));
						
				default:
					alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_MARKETFACT_UNKNOW));
				}
				
				holderAccB.setStyleClassButton("");
				arrHolderAccountBalance = null;
				holderAccB.setSelected(false);
				
				return;
			}
			
			//SI TIENE SOLO UN HECHO DEL MERCADO
			List<HolderMarketFactBalance> lstHolderAccountMF=null;
			if( marketFactoResult.getMarketFacBalances() != null && marketFactoResult.getMarketFacBalances().size() == 1 ){
				BigDecimal AvailableBalance =  marketFactoResult.getMarketFacBalances().get(0).getAvailableBalance();
				
				//SI EL MONTO DISPONIBLE DEL UNICO HECHO DEL MERCADO CUBRE EL TRASPASO
				if(AvailableBalance.compareTo(holderAccB.getTransferAmmount()) == 1 || AvailableBalance.compareTo(holderAccB.getTransferAmmount()) == 0 ){
					holderAccB.setSelected(true);
					arrHolderAccountBalance[0] = holderAccB;
					
					//LLENO MI LISTA DE HECHO DEL MERCADO COMO UN UNICO REGISTRO
					lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
					MarketFactDetailHelpTO marketFact = marketFactoResult.getMarketFacBalances().get(0);
					
					HolderMarketFactBalance holderMarketfactBalance = new HolderMarketFactBalance();
					holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
					holderMarketfactBalance.setHolderAccount(holderAccount);
					Security security = new Security();
					security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
					holderMarketfactBalance.setSecurity(security);
					Participant participant = new Participant();
					participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
					holderMarketfactBalance.setParticipant(participant);
					holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
					holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
					holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
					holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
					holderMarketfactBalance.setTransferAmmount(holderAccB.getTransferAmmount());
					lstHolderAccountMF.add(holderMarketfactBalance);
					
					//SETEO EL MAPA DE LISTAS DE HECHO DEL MERCADO
					mapTransferMarketfact.put(holderAccB.getId().getIdSecurityCodePk(), lstHolderAccountMF);
					helpMarketFactBalanceView.setMarketFacBalances(marketFactoResult.getMarketFacBalances());
					holderAccB.setStyleClassButton(styleBtnActive);
				}else{//SI NO CUBRE EL MONTO
					alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MARKETFACT_AMOUNT_INVALID));
					arrHolderAccountBalance = null;
					holderAccB.setSelected(false);
					holderAccB.setStyleClassButton("");
				}
				
			}else{	//SI TIENE MAS DE UN HECHO DEL MERCADO
				holderAccB.setSelected(true);
				/*lstHolderAccountMF = new ArrayList<HolderMarketFactBalance>();
				for( MarketFactDetailHelpTO marketFact : marketFactoResult.getMarketFacBalances()){
					HolderMarketFactBalance holderMarketfactBalance = new HolderMarketFactBalance();
					holderMarketfactBalance.setIdMarketfactBalancePk(marketFact.getMarketFactBalancePk());
					HolderAccount holderAccount = new HolderAccount();
					holderAccount.setIdHolderAccountPk(marketFactBalance.getHolderAccountPk());
					holderMarketfactBalance.setHolderAccount(holderAccount);
					Security security = new Security();
					security.setIdSecurityCodePk(marketFactBalance.getSecurityCodePk());
					holderMarketfactBalance.setSecurity(security);
					Participant participant = new Participant();
					participant.setIdParticipantPk(marketFactBalance.getParticipantPk());
					holderMarketfactBalance.setParticipant(participant);
					holderMarketfactBalance.setAvailableBalance(marketFact.getAvailableBalance());
					holderMarketfactBalance.setMarketDate(marketFact.getMarketDate());
					holderMarketfactBalance.setMarketPrice(marketFact.getMarketPrice());
					holderMarketfactBalance.setMarketRate(marketFact.getMarketRate());
					holderMarketfactBalance.setTransferAmmount(holderAccB.getTransferAmmount());
					lstHolderAccountMF.add(holderMarketfactBalance);
				}
				mapTransferMarketfact.put(holderAccB.getId().getIdSecurityCodePk(), lstHolderAccountMF);
				helpMarketFactBalanceView.setMarketFacBalances(marketFactoResult.getMarketFacBalances());*/
				holderAccB.setStyleClassButton(styleBtnPending);
			}
			
		}else{	//EL MONTO DISPONIBLE NO CUBRE EL MONTO INGRESADO
			alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_AMOUNT_INVALID));
			holderAccB.setSelected(false);
			arrHolderAccountBalance = null;
			holderAccB.setStyleClassButton("");
			return;
		}
		
	}
	
	/**
	 * Uncheck holder account balance.
	 *
	 * @param e the e
	 */
	public void uncheckHolderAccountBalance(UnselectEvent e){
		HolderAccountBalance holderAccB = (HolderAccountBalance)e.getObject();
		mapTransferMarketfact.remove(holderAccB.getId().getIdSecurityCodePk());
			holderAccB.setTransferAmmount(null);
			holderAccB.setSelected(false);
			holderAccB.setDisabled(true);
			holderAccB.setStyleClassButton("");
	}
	
	/**
	 * Uncheck holder account balance view.
	 *
	 * @param e the e
	 */
	public void uncheckHolderAccountBalanceView(UnselectEvent e){
		HolderAccountBalance holderAccB = (HolderAccountBalance)e.getObject();
		mapTransferMarketfact.remove(holderAccB.getId().getIdSecurityCodePk());
		holderAccB.setSelected(false);
	}
	
	/**
	 * Post constructor view.
	 *
	 * @param securityTransfope the security transfope
	 * @param action the action
	 * @return the string
	 */
	//@LoggerAuditWeb
	public String postConstructorView(SecurityTransferOperation securityTransfope, String action){
		try {
			intHiddenMotive = 0;
			disabledButtons();
			
			if(securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
				flagSameParticipant = false;
				flagTargetSource = false;				
				flagSourceTarget = true;
				flagTargetOrigin=false;
			}else if(securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				flagSameParticipant = false;
				flagTargetSource = true;				
				flagSourceTarget = false;
				flagTargetOrigin=true;
			}else if(securityTransfope.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				flagSameParticipant = true;
				flagTargetSource = false;				
				flagSourceTarget = true;
				flagTargetOrigin=false;
			}
			
			if( action.equals(STR_REVIEW.toString()) && 
				securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
				flagReviewSource = false;
				flagReviewTarget = true;
			}else if( action.equals(STR_REVIEW.toString()) && 
					securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
				flagReviewSource = true;
				flagReviewTarget = false;
			}else{
				flagReviewSource = false;
				flagReviewTarget = false;
			}
			
			lstSourceAccountTo = null;
			lstTargetAccountTo = null;
			
			CustodyOperation custodyOp = securityTransfope.getCustodyOperation();
			securityTransfope.setIdTransferOperationPk(securityTransfope.getCustodyOperation().getIdCustodyOperationPk());
		
			lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
			HolderAccountBalance hab = new HolderAccountBalance();
			HolderAccountBalancePK id = new HolderAccountBalancePK();
			custodyOp = transferAvailableFacade.getDatesCustodyOperationForPk(securityTransfope.getCustodyOperation());
			securityTransfope.setCustodyOperation(custodyOp);
			
			if( Validations.validateIsNotNull(securityTransfope.getTargetHolderAccount()) &&
				Validations.validateIsNotNullAndPositive(securityTransfope.getTargetHolderAccount().getIdHolderAccountPk()) ){
				
				HolderAccount holderAccount = getHolderAccountAndDetails(securityTransfope.getTargetHolderAccount());
				securityTransfope.setTargetHolderAccount(holderAccount);
				HolderTO holderTO = new HolderTO();
				holderTO.setParticipantFk(securityTransfope.getTargetParticipant().getIdParticipantPk());
				holderTO.setHolderAccountId(securityTransfope.getTargetHolderAccount().getIdHolderAccountPk());
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				lstTargetAccountTo = lstAccountTo;
			}
		

			if( Validations.validateIsNotNull(securityTransfope.getSourceHolderAccount()) &&
				Validations.validateIsNotNullAndPositive(securityTransfope.getSourceHolderAccount().getIdHolderAccountPk())  ){
				
				HolderAccount holderAccount = getHolderAccountAndDetails(securityTransfope.getSourceHolderAccount());
				securityTransfope.setSourceHolderAccount(holderAccount);
				
				HolderTO holderTO = new HolderTO();
				holderTO.setParticipantFk(securityTransfope.getSourceParticipant().getIdParticipantPk());
				holderTO.setHolderAccountId(securityTransfope.getSourceHolderAccount().getIdHolderAccountPk());
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				lstSourceAccountTo = lstAccountTo;
			}
			Security security= transferAvailableFacade.getSecurity(securityTransfope.getSecurities().getIdSecurityCodePk());
			security.setSecurityTypeDescription(mapSecurityType.get(security.getSecurityType()));
			securityTransfope.setSecurities(security);
			
			/**** LISTADO DE LOS BALANCES *****/
			//SI ES EL ADMINISTRADOR O EL PARTICIPANTE QUE CREO LA SOLICITUD, ESTE PUEDE VER TODOS LOS DATOS COMPLETOS
			if(
				(!securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
				 (isDepositary  || isParticipant && participantLogin.equals(securityTransfope.getSourceParticipant().getIdParticipantPk())) ) || 
				 
				( securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
				 (isDepositary  || isParticipant && participantLogin.equals(securityTransfope.getTargetParticipant().getIdParticipantPk())) &&
				 (securityTransfope.getState().equals(TransferSecuritiesStateType.RECHAZADO.getCode()) ||
				  securityTransfope.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())  || 
				  securityTransfope.getState().equals(TransferSecuritiesStateType.CONFIRMADO.getCode())) )
			){
				
				if( securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
					securityTransfope.getState().equals(TransferSecuritiesStateType.RECHAZADO.getCode())){
					
					id.setIdHolderAccountPk(securityTransfope.getTargetHolderAccount().getIdHolderAccountPk());
					id.setIdParticipantPk(securityTransfope.getTargetParticipant().getIdParticipantPk());
					id.setIdSecurityCodePk(securityTransfope.getSecurities().getIdSecurityCodePk());
					hab.setId(id);
					hab.setParticipant(securityTransfope.getTargetParticipant());
					hab.setHolderAccount(securityTransfope.getTargetHolderAccount());
					hab.setTransferAmmount(securityTransfope.getQuantityOperation());
					hab.setSecurity(securityTransfope.getSecurities());
					lstHolderAccountBalance.add(hab);
					dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
					
				}else{
					
					if(!securityTransfope.getState().equals(TransferSecuritiesStateType.CONFIRMADO.getCode())){
						id.setIdHolderAccountPk(securityTransfope.getSourceHolderAccount().getIdHolderAccountPk());
						id.setIdParticipantPk(securityTransfope.getSourceParticipant().getIdParticipantPk());
						id.setIdSecurityCodePk(securityTransfope.getSecurities().getIdSecurityCodePk());
						hab.setId(id);
					}
					else{
						id.setIdHolderAccountPk(securityTransfope.getTargetHolderAccount().getIdHolderAccountPk());
						id.setIdParticipantPk(securityTransfope.getTargetParticipant().getIdParticipantPk());
						id.setIdSecurityCodePk(securityTransfope.getSecurities().getIdSecurityCodePk());
						hab.setId(id);
					}
					hab.setParticipant(securityTransfope.getTargetParticipant());
					hab.setHolderAccount(securityTransfope.getTargetHolderAccount());
					hab.setTransferAmmount(securityTransfope.getQuantityOperation());
					hab.setSecurity(securityTransfope.getSecurities());
					lstHolderAccountBalance.add(hab);
					dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
					
				}
				flagViewMarketFact=true;
				
			}else{				
				if(securityTransfope.getState().equals(TransferSecuritiesStateType.CONFIRMADO.getCode()) || 
						securityTransfope.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())){
					id.setIdHolderAccountPk(securityTransfope.getSourceHolderAccount().getIdHolderAccountPk());
					id.setIdParticipantPk(securityTransfope.getSourceParticipant().getIdParticipantPk());
					id.setIdSecurityCodePk(securityTransfope.getSecurities().getIdSecurityCodePk());
					hab.setId(id);
				}
				else if(securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) &&
						securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
					id.setIdHolderAccountPk(securityTransfope.getSourceHolderAccount().getIdHolderAccountPk());
					id.setIdParticipantPk(securityTransfope.getSourceParticipant().getIdParticipantPk());
					id.setIdSecurityCodePk(securityTransfope.getSecurities().getIdSecurityCodePk());
					hab.setId(id);
				}
				else
				{
					id.setIdHolderAccountPk(securityTransfope.getTargetHolderAccount().getIdHolderAccountPk());
					id.setIdParticipantPk(securityTransfope.getTargetParticipant().getIdParticipantPk());
					id.setIdSecurityCodePk(securityTransfope.getSecurities().getIdSecurityCodePk());
					hab.setId(id);
				}						
				hab.setParticipant(securityTransfope.getTargetParticipant());
				hab.setHolderAccount(securityTransfope.getTargetHolderAccount());
				hab.setTransferAmmount(securityTransfope.getQuantityOperation());
				hab.setSecurity(securityTransfope.getSecurities());
				hab.setAvailableBalance(null);
				hab.setTotalBalance(null);
				lstHolderAccountBalance.add(hab);
				dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);
				
				Long idUserParticipant = new Long(0);
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					Long participantDpf = getIssuerDpfInstitution(userInfo);					
					if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
						idUserParticipant = participantDpf;
					}					
				}else{
					idUserParticipant = userInfo.getUserAccountSession().getParticipantCode();
				}
				
				if((userInfo.getUserAccountSession().isParticipantInstitucion() || 
						userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
						userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
						Validations.validateIsNotNull(securityTransfope.getSourceParticipant()) &&
						Validations.validateIsNotNullAndPositive(securityTransfope.getSourceParticipant().getIdParticipantPk()) &&
						securityTransfope.getSourceParticipant().getIdParticipantPk().equals(idUserParticipant) &&
						securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
						&& securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
					flagViewMarketFact=true;
				}else if((userInfo.getUserAccountSession().isParticipantInstitucion() || 
						userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
						userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
						Validations.validateIsNotNull(securityTransfope.getTargetParticipant()) &&
						Validations.validateIsNotNullAndPositive(securityTransfope.getTargetParticipant().getIdParticipantPk()) &&
						securityTransfope.getTargetParticipant().getIdParticipantPk().equals(idUserParticipant) &&
						(securityTransfope.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()) 
								|| securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()))
						&& securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
					flagViewMarketFact=false;
				}else if((userInfo.getUserAccountSession().isParticipantInstitucion()|| 
						userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
						userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
						Validations.validateIsNotNull(securityTransfope.getTargetParticipant()) &&
						Validations.validateIsNotNullAndPositive(securityTransfope.getTargetParticipant().getIdParticipantPk()) &&
						securityTransfope.getTargetParticipant().getIdParticipantPk().equals(idUserParticipant) &&
						securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) &&
						securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
					flagViewMarketFact=false;
				}else{
					flagViewMarketFact=true;
				}
			}
			
			securityTransferOperation = securityTransfope;
			securityTransferOperation.setIdTransferOperationPk(securityTransferOperation.getCustodyOperation().getIdCustodyOperationPk());
			/*Para el helper de hechos del mercado*/
			helpMarketFactBalanceView = new MarketFactBalanceHelpTO();
			helpMarketFactBalanceView.setSecurityCodePk(security.getIdSecurityCodePk());
			helpMarketFactBalanceView.setSecurityDescription(security.getDescription());
			helpMarketFactBalanceView.setInstrumentType(securityTransfope.getSecurities().getInstrumentType());
			helpMarketFactBalanceView.setInstrumentDescription(mapSecurityInstrumentType.get(security.getInstrumentType()));
			helpMarketFactBalanceView.setBalanceResult(securityTransfope.getQuantityOperation());
			boolean todate = false;//marketServiceBean
			if(securityTransferOperation.getCustodyOperation().getRegistryDate().equals(CommonsUtilities.currentDate())){
				todate = true;
			}
			
			Long idUserParticipant = new Long(0);
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				Long participantDpf = getIssuerDpfInstitution(userInfo);				
				if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
					idUserParticipant = participantDpf;
				}					
			}else{
				idUserParticipant = userInfo.getUserAccountSession().getParticipantCode();
			}
			
			if( securityTransfope.getState().equals(TransferSecuritiesStateType.REVISADO.getCode()) || 
					((userInfo.getUserAccountSession().isParticipantInstitucion()|| 
							userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
							userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
							Validations.validateIsNotNull(securityTransfope.getSourceParticipant()) &&
							Validations.validateIsNotNullAndPositive(securityTransfope.getSourceParticipant().getIdParticipantPk()) &&
							securityTransfope.getSourceParticipant().getIdParticipantPk().equals(idUserParticipant) &&
							securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
							&& securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()))){
				List<MarketFactDetailHelpTO> lstMarketFact = transferAvailableFacade.getListMarketFactBalanceInTransferOperation(hab);
				if(lstMarketFact !=null && lstMarketFact.size()>0){
				helpMarketFactBalanceView.setMarketFacBalances(lstMarketFact);
				helpMarketFactBalanceView.setTodate(todate);
				}
			}else{				
				List<MarketFactDetailHelpTO> lstMarketFact = transferAvailableFacade.getListMarketFactBalanceInTransferOperation(securityTransferOperation.getCustodyOperation().getIdCustodyOperationPk(), todate);
				if(lstMarketFact !=null && lstMarketFact.size()>0){
				helpMarketFactBalanceView.setMarketFacBalances(lstMarketFact);
				helpMarketFactBalanceView.setTodate(todate);
				}
			}			
			/**/
			strDocumentNumber = securityTransfope.getCustodyOperation().getOperationNumber().toString();
			strSateDescription = mapStateSecurityTransferOperation.get(securityTransfope.getState());
			
			viewErrorMessage = "";
			
			/*** MUESTRO LAS CUENTAS***/
			
			if( (action.equals(STR_REVIEW.toString()) && 
				securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())) || 
				(securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())) 
			&&  securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
			&& (userInfo.getUserAccountSession().isParticipantInstitucion() || 
					userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
					userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
			Validations.validateIsNotNull(securityTransfope.getTargetParticipant()) &&
			Validations.validateIsNotNullAndPositive(securityTransfope.getTargetParticipant().getIdParticipantPk()) &&
			securityTransfope.getTargetParticipant().getIdParticipantPk().equals(idUserParticipant)){

				List<Long> lstAccounts = transferAvailableFacade.getAccountsWithCuisAndParticipant(securityTransfope.getSourceHolderAccount(), 
																				securityTransfope.getTargetParticipant().getIdParticipantPk());
				
				List<HolderAccountHelperResultTO> lstAccountTo = null;
				if(lstAccounts != null && lstAccounts.size()>0){
					HolderTO holderTO = new HolderTO();
					holderTO.setParticipantFk(securityTransfope.getTargetParticipant().getIdParticipantPk());
					holderTO.setLstHolderAccountId(lstAccounts);
					lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				}
				lstTargetAccountTo = lstAccountTo;
				targetAccountTo = null;
				if(lstTargetAccountTo==null || lstTargetAccountTo.size()<=0){
					
					viewErrorMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONTRAPART_NOT_EXIST_ACCOUNT);
				}
					
			}else if( (action.equals(STR_REVIEW.toString()) 
					&& securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())) || 
						(securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())) 
					&&  securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
					&& (userInfo.getUserAccountSession().isParticipantInstitucion()|| 
							userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
							userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
					Validations.validateIsNotNull(securityTransfope.getSourceParticipant()) &&
					Validations.validateIsNotNullAndPositive(securityTransfope.getSourceParticipant().getIdParticipantPk()) &&
					securityTransfope.getSourceParticipant().getIdParticipantPk().equals(idUserParticipant)){
				
				List<Long> lstAccounts = transferAvailableFacade.getAccountsWithCuisAndParticipant(securityTransfope.getTargetHolderAccount(), 
																				securityTransfope.getSourceParticipant().getIdParticipantPk());

				List<HolderAccountHelperResultTO> lstAccountTo = null;
				if(lstAccounts != null && lstAccounts.size()>0){
					HolderTO holderTO = new HolderTO();
					holderTO.setParticipantFk(securityTransfope.getSourceParticipant().getIdParticipantPk());
					holderTO.setLstHolderAccountId(lstAccounts);
					lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				}
				lstSourceAccountTo = lstAccountTo;
				sourceAccountTo = null;
				if(lstSourceAccountTo ==null || lstSourceAccountTo.size()<=GeneralConstants.ZERO_VALUE_INT){
					viewErrorMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONTRAPART_NOT_EXIST_ACCOUNT);
				}
				if(Validations.validateIsNotNull(lstSourceAccountTo)
						&& lstSourceAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
					sourceAccountTo=lstSourceAccountTo.get(GeneralConstants.ZERO_VALUE_INT);
					settingSourceViewAccount();
				}
				if(lstHolderAccountBalance.size()==GeneralConstants.ONE_VALUE_INTEGER && !((userInfo.getUserAccountSession().isParticipantInstitucion() || 
						userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
						userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) &&
						Validations.validateIsNotNull(securityTransfope.getSourceParticipant()) &&
						Validations.validateIsNotNullAndPositive(securityTransfope.getSourceParticipant().getIdParticipantPk()) &&
						securityTransfope.getSourceParticipant().getIdParticipantPk().equals(idUserParticipant) &&
						securityTransfope.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
						&& securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()))){
					List<HolderAccountBalance> lstHolderAccB = new ArrayList<HolderAccountBalance>();
					try{
						lstHolderAccB = transferAvailableFacade.getListHolderAccountBalance(hab, false, new Security(), userInfo);
					}
					catch (ServiceException e) {
						alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
								PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()) );
						e.printStackTrace();
					}	
					if(lstHolderAccB.size()==GeneralConstants.ONE_VALUE_INTEGER){
						arrHolderAccountBalance=new HolderAccountBalance[lstHolderAccountBalance.size()];
						checkHolderAccountBalanceDestinyOrigin(lstHolderAccB.get(0));	
					}					
				}
				
			}
			if(Validations.validateIsNotNull(lstTargetAccountTo)&&lstTargetAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
				targetAccountTo=lstTargetAccountTo.get(0);
				settingTargetViewAccount();
			}
			
			/*visibilidad de cuentas*/
			if(isDepositary){
				flagAccountOriginVisible = true;
				flagAccountTargetVisible = true;
			}else if(isParticipant){
				if(participantLogin.equals(securityTransfope.getSourceParticipant().getIdParticipantPk())){	//participante origen
					if(securityTransfope.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
						flagAccountOriginVisible = true;
						flagAccountTargetVisible = false;
					}else if(securityTransfope.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
						flagAccountOriginVisible = true;
						flagAccountTargetVisible = true;
					}else{	//DESTINO ORIGEN
						flagAccountOriginVisible = true;
						flagAccountTargetVisible = false;
					}
				}else{	//participante destino
					if(securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){	//ori-des / mismo part
						flagAccountTargetVisible = true;
						flagAccountOriginVisible = false;
					}else{
						flagAccountTargetVisible = true;
						flagAccountOriginVisible = false;
					}
				}
			}
			
			
		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()));
			e.printStackTrace();
		}
		
		return "viewTransferAvailableView";
	}
	
	/**
	 * Listener modify motives.
	 */
	public void listenerModifyMotives(){
		if(Validations.validateIsNotNullAndPositive(intRejectMotive)){
			intHiddenMotive++;
		}
		if(Validations.validateIsNotNullAndNotEmpty(strRejectMotive)){
			intHiddenMotive++;	
		}
	}
	
	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":formPopUp:alterReject");
	}
	
	
	/**
	 * Review all transfer operation.
	 *
	 * @throws Exception the exception
	 */
	public void reviewAllTransferOperation() throws Exception{
		Integer transferType = searchSecurityTransferOperation.getTransferType();
		Integer searchState  = searchSecurityTransferOperation.getState();
		GenericDataModel<SecurityTransferOperation> transferOperationForReview = dataModelTransferSecurityAvailable;
		
		if(!(transferType==null || searchState==null)){
			if(reviewAll){
				if(transferType.equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) && searchState.equals(TransferSecuritiesStateType.APROBADO.getCode())){
					for(SecurityTransferOperation transferOperation: transferOperationForReview){
						transferOperation.setFlagTargetSource(false);
						List<HolderAccountHelperResultTO> lstTargetAccountTo = mapLstTargetAccountTo.get(transferOperation.getCustodyOperation().getOperationNumber());
						if(lstTargetAccountTo==null || lstTargetAccountTo.isEmpty()){
							List<Long> lstAccounts = transferAvailableFacade.getAccountsWithCuisAndParticipant(transferOperation.getSourceHolderAccount(), 
									transferOperation.getTargetParticipant().getIdParticipantPk());
							transferOperation.setCheckForReview(BooleanType.YES.getBooleanValue());

							List<HolderAccountHelperResultTO> lstAccountTo = null;
							if(lstAccounts!= null && lstAccounts.size()>0){
								HolderTO holderTO = new HolderTO();
								holderTO.setLstHolderAccountId(lstAccounts);
								holderTO.setParticipantFk(transferOperation.getTargetParticipant().getIdParticipantPk());
								lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
								mapLstTargetAccountTo.put(transferOperation.getCustodyOperation().getOperationNumber(), lstAccountTo);
							}
							
							if(lstAccountTo!=null && lstAccountTo.size()==ComponentConstant.ONE){
								transferOperation.getTargetHolderAccount().setAccountNumber(lstAccountTo.get(0).getAccountNumber());
								settingTargetAccountContrapart(transferOperation);
							}
						}else{
							transferOperation.setCheckForReview(BooleanType.YES.getBooleanValue());
							List<HolderAccountHelperResultTO> lstAccountTo = mapLstTargetAccountTo.get(transferOperation.getCustodyOperation().getOperationNumber());
							if(lstAccountTo!= null && lstAccountTo.size()==ComponentConstant.ONE){
								transferOperation.getTargetHolderAccount().setAccountNumber(lstAccountTo.get(0).getAccountNumber());
								settingTargetAccountContrapart(transferOperation);
							}
						}
					}
				}
			}else{
				for(SecurityTransferOperation transferOperation: transferOperationForReview){
					transferOperation.setCheckForReview(BooleanType.NO.getBooleanValue());
				}
			}
		}
	}
	
	/**
	 * Check for review.
	 *
	 * @param securityTransfope the security transfope
	 */
	public void checkForReview(SecurityTransferOperation securityTransfope){

		try {
		//remuevo del mapa de hechos del mercado	
		securityTransfope.setLstHolderMarketfactBalance(null);
		securityTransfope.setStyleClassButton("");
			
		if(securityTransfope.isCheckForReview()){
			if(securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				securityTransfope.setFlagTargetSource(true);
				securityTransfope.setStyleClassButton(styleBtnPending);
				
				List<HolderAccountHelperResultTO> lstSourceAccountTo = mapLstSourceAccountTo.get(securityTransfope.getCustodyOperation().getOperationNumber());
				if(!(Validations.validateIsNotNull(lstSourceAccountTo) && lstSourceAccountTo.size() > 0)){
					List<Long> lstAccounts = transferAvailableFacade.getAccountsWithCuisAndParticipant(securityTransfope.getTargetHolderAccount(), 
																									   securityTransfope.getSourceParticipant().getIdParticipantPk());

					List<HolderAccountHelperResultTO> lstAccountTo = null;
					if(lstAccounts!= null && lstAccounts.size()>0){
						HolderTO holderTO = new HolderTO();
						holderTO.setParticipantFk(securityTransfope.getSourceParticipant().getIdParticipantPk());
						holderTO.setLstHolderAccountId(lstAccounts);
						lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
					}
					mapLstSourceAccountTo.put(securityTransfope.getCustodyOperation().getOperationNumber(), lstAccountTo);
					
					if(lstAccountTo!= null && lstAccountTo.size()==1){
						securityTransfope.getSourceHolderAccount().setAccountNumber(lstAccountTo.get(0).getAccountNumber());
						settingSourceAccountContrapart(securityTransfope);
					}
					
				}else{
					List<HolderAccountHelperResultTO> lstAccountTo = mapLstSourceAccountTo.get(securityTransfope.getCustodyOperation().getOperationNumber());
					
					if(lstAccountTo!= null && lstAccountTo.size()==1){
						securityTransfope.getSourceHolderAccount().setAccountNumber(lstAccountTo.get(0).getAccountNumber());
						settingSourceAccountContrapart(securityTransfope);
					}
				}
				
			}else{//SOLO ORIGEN_DESTINO 
				securityTransfope.setFlagTargetSource(false);
				List<HolderAccountHelperResultTO> lstTargetAccountTo = mapLstTargetAccountTo.get(securityTransfope.getCustodyOperation().getOperationNumber());
				if( !(Validations.validateIsNotNull(lstTargetAccountTo) && 
						lstTargetAccountTo.size() > 0) ){

					List<Long> lstAccounts = transferAvailableFacade.getAccountsWithCuisAndParticipant(securityTransfope.getSourceHolderAccount(), 
																									   securityTransfope.getTargetParticipant().getIdParticipantPk());

					List<HolderAccountHelperResultTO> lstAccountTo = null;
					if(lstAccounts!= null && lstAccounts.size()>0){
						HolderTO holderTO = new HolderTO();
						holderTO.setParticipantFk(securityTransfope.getTargetParticipant().getIdParticipantPk());
						holderTO.setLstHolderAccountId(lstAccounts);
						lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
					}
					mapLstTargetAccountTo.put(securityTransfope.getCustodyOperation().getOperationNumber(), lstAccountTo);
					
					if(lstAccountTo!= null && lstAccountTo.size()==1){
						securityTransfope.getTargetHolderAccount().setAccountNumber(lstAccountTo.get(0).getAccountNumber());
						settingTargetAccountContrapart(securityTransfope);
					}
					
				}else{
					List<HolderAccountHelperResultTO> lstAccountTo = mapLstTargetAccountTo.get(securityTransfope.getCustodyOperation().getOperationNumber());
					
					if(lstAccountTo!= null && lstAccountTo.size()==1){
						securityTransfope.getTargetHolderAccount().setAccountNumber(lstAccountTo.get(0).getAccountNumber());
						settingTargetAccountContrapart(securityTransfope);
					}
				}
				
			}
		}else{
			if(securityTransfope.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				securityTransfope.setSourceHolderAccount(new HolderAccount());
				securityTransfope.setLstHolderMarketfactBalance(null);
				securityTransfope.setFlagTargetSource(true);
				securityTransfope.setStyleClassButton(styleBtnPending);
			}else{//SOLO ORIGEN_DESTINO 
				securityTransfope.setTargetHolderAccount(new HolderAccount());
				securityTransfope.setFlagTargetSource(false);
				securityTransfope.setStyleClassButton(styleBtnActive);
			}
		}
		for (SecurityTransferOperation sto: lstTransferSecurityAvailable) {
			if(sto.getCustodyOperation().getOperationNumber().equals(securityTransfope.getCustodyOperation().getOperationNumber() )){
				sto = securityTransfope;
				break;
			}
		}
		dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//APROBADO
	/**
	 * Confirm alert actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmAlertActions(){
		
		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(tmpAction.equals("save")){
			save();
			cleanAfterActions();
		}else if(tmpAction.equals("executeApprove")){
			executeApprove();
			cleanAfterActions();
		}else if(tmpAction.equals("executeMultiApprove")){
			executeMultiApprove();
			cleanAfterActions();
		}else if(tmpAction.equals("executeReject")){
			executeReject();
			cleanAfterActions();
		}else if(tmpAction.equals("executeMultiReject")){
			executeMultiReject();
			cleanAfterActions();
		}else if(tmpAction.equals("executeAnulled")){
			executeAnulled();
			cleanAfterActions();
		}else if(tmpAction.equals("executeMultiAnulled")){
			executeMultiAnulled();
			cleanAfterActions();
		}else if(tmpAction.equals("executeRevise")){
			executeRevise();
			cleanAfterActions();
		}else if(tmpAction.equals("executeMultiRevise")){
			executeMultiRevise();
			cleanAfterActions();
		}else if(tmpAction.equals("executeConfirm")){
			executeConfirm();
			cleanAfterActions();
		}else if(tmpAction.equals("executeMultiConfirm")){
			executeMultiConfirm();
			cleanAfterActions();
		}else if(tmpAction.equals("returnToSearch")){
			clearRegistryTransfer();
			return "searchTransferAvailableView";
		}
		
		return "";
	}
	
	/**
	 * Confirm actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmActions(){

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";
		
		if(tmpAction.equals("saveSucess")){
			//clearRegistryTransfer();
			return "searchTransferAvailableView";
		}else if(tmpAction.equals("searchTransferAvailableView")){
			securityTransferOperation = inicializeSecurityTransferOperation();
			//clearDataModelInSearch();
			return "searchTransferAvailableView";
		}else if(tmpAction.equals("clearTargetSecurity")){
			securityTransferOperation.setSecurities(new Security());
		}else if(tmpAction.equals("returnToSearch")){
			clearSearchTransfer();
			clearRegistryTransfer();
			return "searchTransferAvailableView";
		}else if(tmpAction.equals("clearTargetParticipant")){
			clearTargetParticipant();
		}else if(tmpAction.equals("clearSourceParticipant")){
			clearSourceParticipant();
		}else if(tmpAction.equals("searchTransferAvailableViewMultiple")){
			searchSecurityTransferOperation = lastSerchSecurityTransferOperation;
			settingLastSearchSucess();
			arrSecurityTransferOperation = null;
			searchSolicitud();
			return null;
		}
		
		
		return null;
	}

	/**
	 * Clear target participant.
	 */
	public void clearTargetParticipant(){
		securityTransferOperation.setTargetParticipant(new Participant());
		securityTransferOperation.setTargetHolderAccount(new HolderAccount());

		Integer securityClass = securityTransferOperation.getSecurities().getSecurityClass();
		securityTransferOperation.setSecurities(new Security());
		securityTransferOperation.getSecurities().setSecurityClass(securityClass);
		
		targetHolder = new Holder();
		lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		targetAccountTo = new HolderAccountHelperResultTO();
	}
	
	/**
	 * Clear source participant.
	 */
	public void clearSourceParticipant(){
		securityTransferOperation.setSourceParticipant(new Participant());
		securityTransferOperation.setSourceHolderAccount(new HolderAccount());

		Integer securityClass = securityTransferOperation.getSecurities().getSecurityClass();
		securityTransferOperation.setSecurities(new Security());
		securityTransferOperation.getSecurities().setSecurityClass(securityClass);
		
		sourceHolder = new Holder();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		sourceAccountTo = new HolderAccountHelperResultTO();
	}
	/**
	 * Setting last search sucess.
	 */
	public void settingLastSearchSucess(){
		searchSecurityTransferOperation = new  SecurityTransferOperation();
		
		if(Validations.validateIsNotNullAndPositive(lastSerchSecurityTransferOperation.getTransferType())){
			searchSecurityTransferOperation.setTransferType(lastSerchSecurityTransferOperation.getTransferType());
		}

		if(Validations.validateIsNotNull(lastSerchSecurityTransferOperation.getParticipant()) &&
		   Validations.validateIsNotNullAndPositive(lastSerchSecurityTransferOperation.getParticipant().getIdParticipantPk())
			){
			searchSecurityTransferOperation.setParticipant(lastSerchSecurityTransferOperation.getParticipant());
		}else{
			searchSecurityTransferOperation.setParticipant(new Participant());
		}
		
		if(Validations.validateIsNotNull(lastSerchSecurityTransferOperation.getHolderAccount()) &&
		   Validations.validateIsNotNullAndPositive(lastSerchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk())
			){
			searchSecurityTransferOperation.setHolderAccount(lastSerchSecurityTransferOperation.getHolderAccount());
		}else{
			searchSecurityTransferOperation.setHolderAccount(new HolderAccount());
		}
		
		if(Validations.validateIsNotNull(lastSerchSecurityTransferOperation.getSecurities()) &&
		   Validations.validateIsNotNull(lastSerchSecurityTransferOperation.getSecurities().getIdSecurityCodePk())
			){
			searchSecurityTransferOperation.setSecurities(lastSerchSecurityTransferOperation.getSecurities());
		}else{
			searchSecurityTransferOperation.setSecurities(new Security());
		}

		if(Validations.validateIsNotNullAndPositive(lastSerchSecurityTransferOperation.getState())){
			searchSecurityTransferOperation.setState(lastSerchSecurityTransferOperation.getState());
		}
		
		CustodyOperation custodyOperation = new CustodyOperation();
		if(Validations.validateIsNotNull(lastSerchSecurityTransferOperation.getCustodyOperation())){
			
			if(Validations.validateIsNotNullAndPositive(lastSerchSecurityTransferOperation.getCustodyOperation().getOperationNumber())){
				searchSecurityTransferOperation.setCustodyOperation(lastSerchSecurityTransferOperation.getCustodyOperation());
			}
			
			if(Validations.validateIsNull(lastSerchSecurityTransferOperation.getCustodyOperation().getOperationInit()) ||
			   Validations.validateIsNull(lastSerchSecurityTransferOperation.getCustodyOperation().getOperationInit()) ){
				custodyOperation.setOperationInit(CommonsUtilities.currentDate());
				custodyOperation.setOperationEnd(CommonsUtilities.currentDate());
				searchSecurityTransferOperation.setCustodyOperation(custodyOperation);
			}else{
				searchSecurityTransferOperation.setCustodyOperation(lastSerchSecurityTransferOperation.getCustodyOperation());
			}
		}else{
			custodyOperation.setOperationInit(CommonsUtilities.currentDate());
			custodyOperation.setOperationEnd(CommonsUtilities.currentDate());
			searchSecurityTransferOperation.setCustodyOperation(custodyOperation);
		}
	}
	
	//APROBADO
	/**
	 * Clear registry transfer.
	 */
	public void clearRegistryTransfer(){
		securityTransferOperation = inicializeSecurityTransferOperation();
		lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
		dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>();
		arrHolderAccountBalance = null;
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		sourceAccountTo = new HolderAccountHelperResultTO();
		targetAccountTo = new HolderAccountHelperResultTO();
		sourceHolder = new Holder();
		targetHolder = new Holder();
		clearSearchTransfer();
	}
	
	//APROBADO
	/**
	 * Apply standar owa.
	 */
	public void applyStandarOwa(){
		try {
			
			if(lstTransferSecurityAvailable != null && lstTransferSecurityAvailable.size()>0){
				clearDataModelInSearch();
			}
			if(Validations.validateIsNull(searchSecurityTransferOperation.getSecurities().getSecurityClass())) 
				searchSecurityTransferOperation.getSecurities().setSecurityClass(0);
			
			
			if(Validations.validateIsNotNull(searchSecurityTransferOperation.getSecurities())
					&&Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getSecurities().getSecurityClass())){
				searchSecurityTransferOperation.setSecurityClass(searchSecurityTransferOperation.getSecurities().getSecurityClass());
			}
			
			if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNull(sourceHolder.getIdHolderPk())
					&& Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant())
					&& Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant().getIdParticipantPk())){
				if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
					flagParticipanteInvestor = true;
				}else{
					flagParticipanteInvestor = false;
				}
				if(!helperComponentFacade.findRelationHolderParticipant(sourceHolder.getIdHolderPk(), 
						searchSecurityTransferOperation.getParticipant().getIdParticipantPk(),flagParticipanteInvestor)){
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
							, PropertiesUtilities.getGenericMessage("holder.helper.alert.not.relation.participant"));
					JSFUtilities.showSimpleValidationDialog();
					sourceHolder=new Holder();
					return;
				}
			}
			
			
		} catch (NullPointerException e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	/**
	 * Valid holder account.
	 */
	public void validHolderAccount(){

		if(Validations.validateIsNotNull(searchSecurityTransferOperation.getParticipant()) && 
		   Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk()) &&
		   Validations.validateIsNotNull(sourceHolder) && 
		   Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()) &&
		   Validations.validateIsNotNull(searchSecurityTransferOperation.getHolderAccount()) && 
		   Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getHolderAccount().getIdHolderAccountPk()) 
		 ){
		
			HolderAccount holderAccount = getHolderAccountAndDetails(searchSecurityTransferOperation.getHolderAccount());
			
				
			if(Validations.validateIsNotNull(holderAccount) && Validations.validateIsNotNull(holderAccount.getIdHolderAccountPk())){
				if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
					searchSecurityTransferOperation.setHolderAccount(new HolderAccount());
					return;
				}
			}
			else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_EXISTED));
				searchSecurityTransferOperation.setHolderAccount(new HolderAccount());
				return;
			}
			
			searchSecurityTransferOperation.setHolderAccount(holderAccount);
		}
			
	}
	
	/**
	 * Valid participant.
	 */
	public void validParticipant(){
		searchSecurityTransferOperation.setSecurities(new Security());
		searchSecurityTransferOperation.setHolderAccount(new HolderAccount());
		
	}
	
	/**
	 * Clear search transfer.
	 */
	public void clearSearchTransfer(){
		searchSecurityTransferOperation = inicializeSecurityTransferOperation();
		sourceHolder = new Holder();
		clearDataModelInSearch();
		securityEvents();
		JSFUtilities.resetViewRoot();
	}
	
	/**
	 * Clear data model in search.
	 */
	public void clearDataModelInSearch(){
		dataModelTransferSecurityAvailable = null;
		lstTransferSecurityAvailable = null;
		registersFound = true;
	}
	

	//APROBADO
	/**
	 * Execute search action.
	 *
	 * @param action the action
	 * @return the string
	 */
	
	public String propertiesConstantsAllActions(String action){
		if(action.equals("approve")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_APROVE;
		}else if(action.equals("annular")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_ANULL;
		}else if(action.equals("review")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_REVIEW;
		}else if(action.equals("reject")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_REJECT;
		}else if(action.equals("confirm")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_CONFIRM;
		}
		
		return PropertiesConstants.NO_ACTION_VALID;
	}
	
	/**
	 * Validate is valid user part one.
	 *
	 * @param arrSecurityTransferOperation the arr security transfer operation
	 * @return the list
	 */
	private List<String> validateIsValidUserPartOne(SecurityTransferOperation[] arrSecurityTransferOperation) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;
		for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
			SecurityTransferOperation securityTransferOperationTmp = arrSecurityTransferOperation[i];
			 OperationUserTO operationUserTO = new OperationUserTO();
			  operationUserTO.setOperNumber(securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString());
			  operationUserTO.setUserName(securityTransferOperationTmp.getCustodyOperation().getRegistryUser());
			  lstOperationUserParam.add(operationUserTO);
		}
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Validate is valid user part two.
	 *
	 * @param arrSecurityTransferOperation the arr security transfer operation
	 * @return the list
	 */
	private List<String> validateIsValidUserPartTwo(SecurityTransferOperation[] arrSecurityTransferOperation) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;
		for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
			SecurityTransferOperation securityTransferOperationTmp = arrSecurityTransferOperation[i];
			 OperationUserTO operationUserTO = new OperationUserTO();
			  operationUserTO.setOperNumber(securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString());
			  operationUserTO.setUserName(securityTransferOperationTmp.getCustodyOperation().getReviewUser());
			  lstOperationUserParam.add(operationUserTO);
		}
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Execute search action.
	 *
	 * @param action the action
	 * @return the string
	 */
	@LoggerAuditWeb
	public String executeSearchAction(String action){
		
		if(isParticipant){

			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
			
				if(arrSecurityTransferOperation[i].getSourceParticipant().getIdParticipantPk().equals(participantLogin) 
						&& (arrSecurityTransferOperation[i].getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()) 
								|| arrSecurityTransferOperation[i].getState().equals(TransferSecuritiesStateType.ANULADO.getCode()))){
						if(arrSecurityTransferOperation[i].getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){														
							String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
				    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MESSAGE_REBLOCK_TRANSFER_AUTORIZE);
				    		alert(headerMessage,bodyMessage);				    		
							return "";
						}
				}
				
				if(arrSecurityTransferOperation[i].getTargetParticipant().getIdParticipantPk().equals(participantLogin)
						&& (arrSecurityTransferOperation[i].getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()) 
								|| arrSecurityTransferOperation[i].getState().equals(TransferSecuritiesStateType.ANULADO.getCode()))){
					if(arrSecurityTransferOperation[i].getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
						String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MESSAGE_REBLOCK_TRANSFER_AUTORIZE);
			    		alert(headerMessage,bodyMessage);
			    		return "";
					}						
				}
			}			
		}
		
		if (arrSecurityTransferOperation != null && arrSecurityTransferOperation.length > 0){
			if(action.equals(STR_APPROVE.toString()) || action.equals(STR_ANNULAR.toString()) ){
				List<String> lstOperationUser = validateIsValidUserPartOne(arrSecurityTransferOperation);
				  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
					  String act = GeneralConstants.EMPTY_STRING;
					  if(action.equals(STR_ANNULAR.toString())){
						  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL;
					  }else  if(action.equals(STR_APPROVE.toString())){
						  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE;
					  }
					  String bodyMessage = PropertiesUtilities.getMessage(act,
								new Object[]{StringUtils.join(lstOperationUser,",")});
						alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),bodyMessage);
						return "";
				  }
			}else if(action.equals(STR_CONFIRM.toString()) || action.equals(STR_REJECT.toString()) ){
				List<String> lstOperationUser = validateIsValidUserPartTwo(arrSecurityTransferOperation);
				  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
					  String act = GeneralConstants.EMPTY_STRING;
					  if(action.equals(STR_CONFIRM.toString())){
						  act = PropertiesConstants.ERROR_MESSAGE_INVALID;//act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM;//Aqui corregir mensaje
					  }else  if(action.equals(STR_REJECT.toString())){
						  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT;
					  }
					  
					  String bodyMessage = PropertiesUtilities.getMessage(act,
								new Object[]{StringUtils.join(lstOperationUser,",")});
						alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),bodyMessage);
						return "";
				  }
			}
		}		
		  
		StringBuilder result = new StringBuilder();
		listOperationNumber = null;
		if (arrSecurityTransferOperation != null && arrSecurityTransferOperation.length > 0){
			//concatenado de numeros de solicitud
			for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
				SecurityTransferOperation securityTransferOperation = arrSecurityTransferOperation[i];
				result.append(securityTransferOperation.getCustodyOperation().getOperationNumber().toString());
		        result.append(", ");
			}
		}		
		
	    if(arrSecurityTransferOperation != null)
	    	listOperationNumber = result.length() > 0 ? result.substring(0, result.length() -2): "";
		
    	if (arrSecurityTransferOperation != null && arrSecurityTransferOperation.length > 0) {
    		
    		if(arrSecurityTransferOperation.length == 1){		//SI SOLO SELECCIONE UN REGISTRO
    			securityTransferOperationTmp = arrSecurityTransferOperation[0];
    			
    			try {
					if(action.equals(STR_CONFIRM.toString())){
						List<Long> lstReblockOperationNumber = transferAvailableFacade.reblockInTransferAvalaible(securityTransferOperationTmp.getCustodyOperation().getIdCustodyOperationPk());
						
						if( lstReblockOperationNumber != null && lstReblockOperationNumber.size()>0 ){
							
							String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
				    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MESSAGE_REBLOCK_IN_TRANSFER,
				    															new Object[]{ operationReblockNumberMessage(lstReblockOperationNumber),
				    																			listOperationNumber});
				    		alert(headerMessage,bodyMessage);
				    		
							return "";
						} 
					}
					
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			
    			return executeSearchAction(action,securityTransferOperationTmp);
    			
    		}else if(arrSecurityTransferOperation.length>1){	//SI SELECCIONE MAS DE UN REGISTRO
    			
    			
    			if(action.equals(STR_APPROVE.toString())){
    				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
    					
    					if(!validApprove(arrSecurityTransferOperation[i])){
    						return null;
    					}
					}
    				
    				executeOperations("executeMultiApprove");
    			}else if(action.equals(STR_REVIEW.toString())){
    				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
    					
    					if(!validReview(arrSecurityTransferOperation[i])){
    						return null;
    					}
					}
    				
    				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
    					
    					if( !(
    					   Validations.validateIsNotNull(arrSecurityTransferOperation[i].getSourceHolderAccount()) &&
    					   Validations.validateIsNotNull(arrSecurityTransferOperation[i].getTargetHolderAccount()) &&
    					   Validations.validateIsNotNullAndPositive(arrSecurityTransferOperation[i].getSourceHolderAccount().getIdHolderAccountPk()) &&
    					   Validations.validateIsNotNullAndPositive(arrSecurityTransferOperation[i].getTargetHolderAccount().getIdHolderAccountPk())
    					     )
    					){
    						
    						alert(PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_TITULAR_CONTRAPART));
    						return null;
    					}else{
							if(arrSecurityTransferOperation[i].getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) &&
	   						    ! (Validations.validateIsNotNull(arrSecurityTransferOperation[i].getLstHolderMarketfactBalance()) &&
	   						     arrSecurityTransferOperation[i].getLstHolderMarketfactBalance().size()>0) ){
								
	    						alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_MARKETFACT_NOT_SELECTED_IN_TARGET_SOURCE));
	   							return null;
	   						}
    					}
					}
    				
    				executeOperations("executeMultiRevise");
    			}else if(action.equals(STR_ANNULAR.toString())){
    				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
    					
    					if(!validAnnular(arrSecurityTransferOperation[i])){
    						return null;
    					}
					}
    				alertMultiAnulledOrReject();
    			}else if(action.equals(STR_REJECT.toString())){
    				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
    					
    					if(!validReject(arrSecurityTransferOperation[i])){
    						return null;
    					}
					}
    				alertMultiAnulledOrReject();
    			}else if(action.equals(STR_CONFIRM.toString())){
    				for (int i = 0; i < arrSecurityTransferOperation.length; i++) {
    					
    					/***/
    					try {
    						
    						List<Long> lstReblockOperationNumber = transferAvailableFacade.reblockInTransferAvalaible(arrSecurityTransferOperation[i].getCustodyOperation().getIdCustodyOperationPk());
							
    						if( lstReblockOperationNumber != null && lstReblockOperationNumber.size()>0 ){
    							
    							String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    				    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_MESSAGE_REBLOCK_IN_TRANSFER,
    				    															new Object[]{ operationReblockNumberMessage(lstReblockOperationNumber),
    				    																			listOperationNumber});
    				    		alert(headerMessage,bodyMessage);
    				    		return "";
							} 
    						
    					} catch (ServiceException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    					/***/
    					if(!validConfirm(arrSecurityTransferOperation[i])){
    						return null;
    					}
					}
    				executeOperations("executeMultiConfirm");
    			}
    			
    		}
    	}else{
    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_NOSELECTDATA);
    		alert(headerMessage,bodyMessage);
    	}
		
    	return null;
	}
	
	/**
	 * Valid approve.
	 *
	 * @param securityTransferOperationTmp the security transfer operation tmp
	 * @return true, if successful
	 */
	public boolean validApprove(SecurityTransferOperation securityTransferOperationTmp){
		
		String messages = "";
		
		if( !(isParticipant || isDepositary) ){
			messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
			alert(messages);
		}
		
		if(isParticipant){
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getSourceParticipant()) && 
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getTargetParticipant()) &&
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
		}
		
		if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			messages = PropertiesUtilities.getMessage(propertiesConstantsAllActions("approve"),new Object[]{listOperationNumber});
			alert(messages);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Valid reject.
	 *
	 * @param securityTransferOperationTmp the security transfer operation tmp
	 * @return true, if successful
	 */
	public boolean validReject(SecurityTransferOperation securityTransferOperationTmp){

		String messages = "";
		
		if(isParticipant){
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getSourceParticipant()) &&
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
					
				}
			}
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getTargetParticipant()) &&
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
		}

		if(!( securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())
				|| securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REVISADO.getCode()) )
		){
			messages = PropertiesUtilities.getMessage(propertiesConstantsAllActions("reject"),new Object[]{listOperationNumber});
			alert(messages);
			return false;
		}
	
		return true;
	}
	
	/**
	 * Valid review.
	 *
	 * @param securityTransferOperationTmp the security transfer operation tmp
	 * @return true, if successful
	 */
	public boolean validReview(SecurityTransferOperation securityTransferOperationTmp){

		String messages = "";
		
		if(isParticipant){
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getSourceParticipant()) &&
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getTargetParticipant()) &&
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
			
		}
		
		if(isParticipant){
			
			if(securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				if(!securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin)){
					messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
					alert(messages);
					return false;
				}
			}else if(securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
				if(!securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)){
					messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
					alert(messages);
					return false;
				}
			}else if(securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				if(!(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)
					&& securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin) )){
					messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
					alert(messages);
					return false;
				}
			}
			
		}
						
		if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())){
			messages = PropertiesUtilities.getMessage(propertiesConstantsAllActions("review"),new Object[]{listOperationNumber});
			alert(messages);
			return false;
		}
		
		return true;
	}
	

	/**
	 * Valid annular.
	 *
	 * @param securityTransferOperationTmp the security transfer operation tmp
	 * @return true, if successful
	 */
	public boolean validAnnular(SecurityTransferOperation securityTransferOperationTmp){

		String messages = "";
		
		if(isParticipant){
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getSourceParticipant()) &&
			   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getTargetParticipant()) &&
				Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
		}

		if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			messages = PropertiesUtilities.getMessage(propertiesConstantsAllActions("annular"),new Object[]{listOperationNumber});
			alert(messages);
			return false;
		}
	
		return true;
	}
	
	/**
	 * Valid confirm.
	 *
	 * @param securityTransferOperationTmp the security transfer operation tmp
	 * @return true, if successful
	 */
	public boolean validConfirm(SecurityTransferOperation securityTransferOperationTmp){

		String messages = "";
		
		if(isParticipant){
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getSourceParticipant()) &&
				Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getSourceParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
			
			if(Validations.validateIsNotNull(securityTransferOperationTmp.getTargetParticipant()) &&
				Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk())){
				if(securityTransferOperationTmp.getTargetParticipant().getIdParticipantPk().equals(participantLogin)){
					
					if( securityTransferOperationTmp.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
						messages = PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[]{listOperationNumber});
						alert(messages);
						return false;
					}
				}
			}
		}

		if(!securityTransferOperationTmp.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())){
			messages = PropertiesUtilities.getMessage(propertiesConstantsAllActions("confirm"),new Object[]{listOperationNumber});
			alert(messages);
			return false;
		}
	
		return true;
	}
	
	/**
	 * Execute search action.
	 *
	 * @param action the action
	 * @param securityTransferOperationTmp the security transfer operation tmp
	 * @return the string
	 */
	public String executeSearchAction(String action, SecurityTransferOperation securityTransferOperationTmp){
		
		if(action.equals(STR_APPROVE.toString())){
			if(!validApprove(securityTransferOperationTmp)){
				return null;
			}
		}else if(action.equals(STR_REVIEW.toString())){
			if(!validReview(securityTransferOperationTmp)){
				return null;
			}
		}else if(action.equals(STR_ANNULAR.toString())){
			if(!validAnnular(securityTransferOperationTmp)){
				return null;
			}
		}else if(action.equals(STR_REJECT.toString())){
			if(!validReject(securityTransferOperationTmp)){
				return null;
			}
		}else if(action.equals(STR_CONFIRM.toString())){
			if(!validConfirm(securityTransferOperationTmp)){
				return null;
			}
		}
			
		securityTransferOperationTmp = arrSecurityTransferOperation[0];
		
		if(!action.equals(STR_REVIEW.toString())){
			if( !( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) &&
				   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ) 
			){
				securityTransferOperationTmp.setSourceHolderAccount(null);
			}
			if( !( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) &&
				   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk())) ){
				securityTransferOperationTmp.setTargetHolderAccount(null);
			}
		}else{
			if( !( Validations.validateIsNotNull(securityTransferOperationTmp.getSourceHolderAccount()) &&
					   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getSourceHolderAccount().getIdHolderAccountPk()) ) 
			){
				securityTransferOperationTmp.setSourceHolderAccount(new HolderAccount());
			}
			if( !( Validations.validateIsNotNull(securityTransferOperationTmp.getTargetHolderAccount()) &&
				   Validations.validateIsNotNullAndPositive(securityTransferOperationTmp.getTargetHolderAccount().getIdHolderAccountPk())) 
			){
				securityTransferOperationTmp.setTargetHolderAccount(new HolderAccount());
			}
		}
		
		String redirect = postConstructorView(securityTransferOperationTmp,action);
		enabledButtonsWithState(securityTransferOperationTmp.getState(),action);
		
		return redirect;
	}
	
	/**
	 * Search holder origin balances.
	 */
	@LoggerAuditWeb
	public void searchHolderOriginBalances(){
		
		try {
			
			if( securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				if( !(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) && 
					Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));
					return;
				}
				
			}else{	//origen - destino | mismo participante
				if( securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
					if( !(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) && 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())) ){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_SOURCE));
						return;
					} 
				}
				
				if( securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode()) ){
					if( !(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()) && 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));
						return;
					}
				}
			}
			
			HolderAccountBalance holderAccountBalance = new HolderAccountBalance();
			HolderAccountBalancePK id = new HolderAccountBalancePK();
			id.setIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
			id.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			if(Validations.validateIsNotNullAndNotEmpty(securityTransferOperation.getSecurities().getIdSecurityCodePk())){
				id.setIdSecurityCodePk(securityTransferOperation.getSecurities().getIdSecurityCodePk());
			}
			holderAccountBalance.setId(id);
			Security securityFilter=new Security();
			securityFilter.setSecurityClass(securityTransferOperation.getSecurities().getSecurityClass());
			//securityFilter.setStateSecurity( SecurityStateType.REGISTERED.getCode());
			lstHolderAccountBalance = transferAvailableFacade.getListHolderAccountBalance(holderAccountBalance, checkOnliAvailable, securityFilter, userInfo);
			lstHolderAccountBalance = lstHolderAccountBalance.stream().filter(balance -> balance.getSecurity()
					.getStateSecurity().equals(SecurityStateType.REGISTERED.getCode())
					|| balance.getSecurity().getStateSecurity().equals(SecurityStateType.GUARDA_EXCLUSIVE.getCode()))
					.collect(Collectors.toList());
			
			dataModelHolderAccountBalance = new GenericDataModel<HolderAccountBalance>(lstHolderAccountBalance);

			if( lstHolderAccountBalance.size() <= 0 ){	
				registersFound=false;
			}else{
				registersFound=true;
			}
			
		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams()) );
		} 
	}
	
	/**
	 * Return to search.
	 *
	 * @return the string
	 */
	public String returnToSearch(){
		validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.BACK_TO_SCREEN);
		
	   if( Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk())
		|| Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
		|| Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())
		|| Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()) ){

		   String strHeader = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
		   registersFound=true;
		   confirmAlertAction = "returnToSearch";
		   question(validationMessage,strHeader);
		   return "";
		}else{
			registersFound=true;
			securityTransferOperation = new SecurityTransferOperation();
			return "searchTransferAvailableView";
		}
			
	}
	
	/**
	 * Back view with out validation.
	 *
	 * @return the string
	 */
	public String backViewWithOutValidation(){
		enabledButtons();
		cleanFiltersSearch();
		securityEvents();
		return "searchTransferAvailableView";
	}
	
	/**
	 * Clean filters search.
	 */
	public void cleanFiltersSearch(){
		searchSecurityTransferOperation.setParticipant(new Participant());
		searchSecurityTransferOperation.setState(null);
		sourceHolder = new Holder();
		searchSecurityTransferOperation.setHolderAccount(new HolderAccount());
		searchSecurityTransferOperation.setSecurities(new Security());
		registersFound=true;
	}
	
	/**
	 * Alert.
	 *
	 * @param message the message
	 */
	public void alert(String message){
		String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/**
	 * Alter reject cancel.
	 *
	 * @param validationMessage the validation message
	 */
	public void alterRejectCancel(String validationMessage){
		showMessageOnDialog(validationMessage,""); 
		loadRejectorCancelCombos();
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/**
	 * Alert.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void alert(String header,String message){
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/**
	 * Question.
	 *
	 * @param message the message
	 */
	public void question(String message){
		String header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING);
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	/**
	 * Question.
	 *
	 * @param message the message
	 * @param header the header
	 */
	public void question(String message,String header){
		showMessageOnDialog(header,message); 
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		JSFUtilities.updateComponent("opnlDialogs");
	}
	
	//APROBADO
	/**
	 * Search solicitud.
	 */
	@LoggerAuditWeb
	public void searchSolicitud(){
		reviewAll = false;
		try {
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					&& Validations.validateIsNullOrNotPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk())){
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
				return;
			}
			
			searchSecurityTransferOperation.setMapSecurityTransferState(mapStateSecurityTransferOperation);
			searchSecurityTransferOperation.setHolder(sourceHolder);
			lstTransferSecurityAvailable = transferAvailableFacade.searchTransferSecurityAvailableNativo(searchSecurityTransferOperation,userInfo.getUserAccountSession());
			List<SecurityTransferOperation> lstTmpSecTransfOpe = new ArrayList<SecurityTransferOperation>();
			lstTmpSecTransfOpe.addAll(lstTransferSecurityAvailable);
			
			if (Validations.validateIsNotNull(lstTransferSecurityAvailable) && lstTransferSecurityAvailable.size()>0){
				
				for (int i = 0; i < lstTransferSecurityAvailable.size(); i++) {					
					if(	lstTransferSecurityAvailable.get(i).getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) &&
					  ( lstTransferSecurityAvailable.get(i).getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ||
						lstTransferSecurityAvailable.get(i).getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ) ){
						lstTransferSecurityAvailable.get(i).setCheckForReview(false);
						lstTransferSecurityAvailable.get(i).setEnabledForReview(false);
					}else{
						lstTransferSecurityAvailable.get(i).setCheckForReview(true);
						lstTransferSecurityAvailable.get(i).setEnabledForReview(true);
						
						if(!lstTransferSecurityAvailable.get(i).getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
							lstTransferSecurityAvailable.get(i).setStyleClassButton(styleBtnActive);
						}else{
							lstTransferSecurityAvailable.get(i).setStyleClassButton(styleBtnPending);
						}						
					}					
				}
				
				lstTransferSecurityAvailable = lstTmpSecTransfOpe;
				
				if (Validations.validateIsNotNull(lstTransferSecurityAvailable) && lstTransferSecurityAvailable.size()>0){

					dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
					registersFound=true;
					settingLastSearch(searchSecurityTransferOperation);
					enabledButtons();
					return;
					
				}else{

					SecurityTransferOperation securityTransferOperationTmp = inicializeSecurityTransferOperation();
					settingLastSearch(securityTransferOperationTmp);
					
					lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
					dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
					registersFound=false;
					
					return;
				}
				
			}else{
				SecurityTransferOperation securityTransferOperationTmp = inicializeSecurityTransferOperation();				
				settingLastSearch(securityTransferOperationTmp);
				
				lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
				dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
				registersFound=false;
				
				return;
			}
		} catch (ServiceException ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}
	
	//APROBADO
	/**
	 * Search with operation number.
	 */
	@LoggerAuditWeb
	public void searchWithOperationNumber(){
		
		try {
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					&& Validations.validateIsNullOrNotPositive(searchSecurityTransferOperation.getParticipant().getIdParticipantPk())){
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
				return;
			}
			
			if( !(Validations.validateIsNotNull(searchSecurityTransferOperation.getCustodyOperation()) &&  
			      Validations.validateIsNotNullAndPositive(searchSecurityTransferOperation.getCustodyOperation().getOperationNumber())) ){
				String propertieConstant = PropertiesConstants.LBL_ERROR_TRANSFER_SOLICITUD_REQUIRED;
				alert(PropertiesUtilities.getMessage(propertieConstant));
			}
			
			
			if(isParticipant){
				searchSecurityTransferOperation.getParticipant().setIdParticipantPk(participantLogin);
			}
			searchSecurityTransferOperation.setMapSecurityTransferState(mapStateSecurityTransferOperation);
			SecurityTransferOperation securityTransferSearch = transferAvailableFacade.SolicitudWithAccount(searchSecurityTransferOperation,userInfo.getUserAccountSession());
			
			if(Validations.validateIsNotNullAndPositive(securityTransferSearch.getState())){	//SI TRAJO REGISTROS

				lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
				
				if( securityTransferSearch.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) &&
				  ( securityTransferSearch.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ||
					securityTransferSearch.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ) ){
					securityTransferSearch.setCheckForReview(false);
					securityTransferSearch.setEnabledForReview(false);
				}else{
					securityTransferSearch.setCheckForReview(true);
					securityTransferSearch.setEnabledForReview(true);
					if(!securityTransferSearch.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
						securityTransferSearch.setStyleClassButton(styleBtnActive);
					}else{
						securityTransferSearch.setStyleClassButton(styleBtnPending);
					}
				}
				
				lstTransferSecurityAvailable.add(securityTransferSearch);
				
//				if(isParticipant){
//
//					if( securityTransferSearch.getSourceParticipant().getIdParticipantPk().equals(participantLogin) 
//						&& (securityTransferSearch.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()) 
//						||  securityTransferSearch.getState().equals(TransferSecuritiesStateType.ANULADO.getCode()))
//					){
//						if(securityTransferSearch.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
//							lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
//							dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
//							registersFound=false;
//							return;
//						}
//					}
//					
//					if( securityTransferSearch.getTargetParticipant().getIdParticipantPk().equals(participantLogin)
//						&& (securityTransferSearch.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()) 
//						||  securityTransferSearch.getState().equals(TransferSecuritiesStateType.ANULADO.getCode()))
//					){
//						if(securityTransferSearch.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
//							lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
//							dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
//							registersFound=false;
//							return;
//						}
//					}
//					
//					searchSecurityTransferOperation.setParticipant(new Participant());
//					
//				}
				
				dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
				registersFound=true;
				settingLastSearch(searchSecurityTransferOperation);
				enabledButtons();
				
				return;
				
			}else{
				SecurityTransferOperation securityTransferOperationTmp = inicializeSecurityTransferOperation();				
				settingLastSearch(securityTransferOperationTmp);
				
				lstTransferSecurityAvailable = new ArrayList<SecurityTransferOperation>();
				dataModelTransferSecurityAvailable = new GenericDataModel<SecurityTransferOperation>(lstTransferSecurityAvailable);
				registersFound=false;
				return;
				
			}
					
		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(),e.getParams())	);
		}
	}
	
	
	/**
	 * Sets the boolean transfer type.
	 */
	public void setBooleanTransferType(){
		
		if(Validations.validateIsNotNullAndPositive(securityTransferOperation.getTransferType())){
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ){
				flagTargetSource = true;
			}else{
				flagTargetSource = false;
			}
		}
	}
	
	//APROBADO
	/**
	 * Enabled buttons.
	 */
	public void enabledButtons(){
		
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(Validations.validateIsNotNullAndPositive(securityTransferOperation.getState())){
			
			if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
	
				flagApprovedSto = true;flasgAnnuledSto = true;
				flagRevisedSto = false;flagRejectSto = false;flagConfirmSto = false;
			}else if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())){
	
				flagRevisedSto = true;flagRejectSto = true;
				flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
			}else if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.ANULADO.getCode())){
				
				flagRevisedSto = false;flagRejectSto = false;flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
			}else if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())){
				
				flagConfirmSto = true;flagRejectSto = true;
				flagRevisedSto = false;flagApprovedSto = false;flasgAnnuledSto = false;
			}else if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.RECHAZADO.getCode())){
				
				flagRevisedSto = false;flagRejectSto = false;flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
			}else if(securityTransferOperation.getState().equals(TransferSecuritiesStateType.CONFIRMADO.getCode())){
				
				flagRevisedSto = false;flagRejectSto = false;flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
			}
		}
		
		if(userPrivilege.getUserAcctions().isApprove()){
			privilegeComponent.setBtnApproveView(true);
		}
		
		if(userPrivilege.getUserAcctions().isAnnular()){
			privilegeComponent.setBtnAnnularView(true);
		}
		
		if(userPrivilege.getUserAcctions().isReview()){
			privilegeComponent.setBtnReview(true);
		}
		 
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(true);
		}
		
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(true);
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	//APROBADO
	/**
	 * Enabled buttons with state.
	 *
	 * @param state the state
	 * @param action the action
	 */
	public void enabledButtonsWithState(Integer state, String action){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		
		if(state.equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
			flagApprovedSto = true;flasgAnnuledSto = true;
			flagRevisedSto = false;flagRejectSto = false;flagConfirmSto = false;
			if(userPrivilege.getUserAcctions().isApprove() && action.equals(STR_APPROVE.toString())){
				privilegeComponent.setBtnApproveView(true);
				flasgAnnuledSto = false;
			}
			
			if(userPrivilege.getUserAcctions().isAnnular() && action.equals(STR_ANNULAR.toString())){
				privilegeComponent.setBtnAnnularView(true);
				flagApprovedSto = true;
			}
		}else if(state.equals(TransferSecuritiesStateType.APROBADO.getCode())){
			flagRevisedSto = true;flagRejectSto = true;
			flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
			if(userPrivilege.getUserAcctions().isReview() && action.equals(STR_REVIEW.toString())){
				privilegeComponent.setBtnReview(true);
				flagRejectSto = false;
			}
			 
			if(userPrivilege.getUserAcctions().isReject() && action.equals(STR_REJECT.toString())){
				privilegeComponent.setBtnRejectView(true);
				flagRevisedSto = false;
			}
		}else if(state.equals(TransferSecuritiesStateType.ANULADO.getCode())){
			flagRevisedSto = false;flagRejectSto = false;flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
		}else if(state.equals(TransferSecuritiesStateType.REVISADO.getCode())){
			flagConfirmSto = true;flagRejectSto = true;
			flagRevisedSto = false;flagApprovedSto = false;flasgAnnuledSto = false;
			if(userPrivilege.getUserAcctions().isReject() && action.equals(STR_REJECT.toString())){
				privilegeComponent.setBtnRejectView(true);
				flagConfirmSto = false;
			}
			
			if(userPrivilege.getUserAcctions().isConfirm() && action.equals(STR_CONFIRM.toString())){
				privilegeComponent.setBtnConfirmView(true);
				flagRejectSto = false;
			}
		}else if(state.equals(TransferSecuritiesStateType.RECHAZADO.getCode())){
			flagRevisedSto = false;flagRejectSto = false;flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
		}else if(state.equals(TransferSecuritiesStateType.CONFIRMADO.getCode())){
			flagRevisedSto = false;flagRejectSto = false;flagApprovedSto = false;flasgAnnuledSto = false;flagConfirmSto = false;
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	//APROBADO
	/**
	 * Disabled buttons.
	 */
	public void disabledButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if(userPrivilege.getUserAcctions().isApprove()){
			privilegeComponent.setBtnApproveView(false);
		}
		
		if(userPrivilege.getUserAcctions().isAnnular()){
			privilegeComponent.setBtnAnnularView(false);
		}
		
		if(userPrivilege.getUserAcctions().isReview()){
			privilegeComponent.setBtnReview(false);
		}
		 
		if(userPrivilege.getUserAcctions().isReject()){
			privilegeComponent.setBtnRejectView(false);
		}
		
		if(userPrivilege.getUserAcctions().isConfirm()){
			privilegeComponent.setBtnConfirmView(false);
		}
		
		userPrivilege.setPrivilegeComponent(privilegeComponent);
		flagApprovedSto = false;flasgAnnuledSto = false;
		flagRevisedSto = false;flagRejectSto = false;flagConfirmSto = false;
	}
	
	//APROBADO
	/**
	 * Onchange initial date.
	 */
	public void onchangeInitialDate(){
		Date initialDate = searchSecurityTransferOperation.getCustodyOperation().getOperationInit();
		Date endDate = 	searchSecurityTransferOperation.getCustodyOperation().getOperationEnd();
		minTime = CommonsUtilities.addDate(endDate, -maxDaysOfCustodyRequest);
		String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
		if(evaluateDates(initialDate,endDate)){
			alert(PropertiesUtilities.getMessage(propertieConstant));
		}
	}
	
	//APROBADO
	/**
	 * Onchange end date.
	 */
	public void onchangeEndDate(){
		Date initialDate = searchSecurityTransferOperation.getCustodyOperation().getOperationInit();
		Date endDate = 	searchSecurityTransferOperation.getCustodyOperation().getOperationEnd();
		minTime = CommonsUtilities.addDate(endDate, -maxDaysOfCustodyRequest);
		
		if(evaluateDates(initialDate,endDate)){
			String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			return;
		}
		if(evaluateDates(endDate,CommonsUtilities.currentDateTime())){
			String propertieConstant = PropertiesConstants.END_DATE_GREATER_THAN_SYSTEM_DATE;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			return;
		}
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}

	/**
	 * Evaluate dates.
	 *
	 * @param firstDate the first date
	 * @param secondDate the second date
	 * @return the boolean
	 */
	public Boolean evaluateDates(Date firstDate, Date secondDate) {
		Boolean greaterDate = Boolean.FALSE;
		if (firstDate.after(secondDate)) {
			greaterDate = Boolean.TRUE;
		}
		return greaterDate;
	}
	
	/**
	 * *************************Get And Set****************************.
	 *
	 * @return the security transfer operation
	 */
	
	public SecurityTransferOperation getSecurityTransferOperation() {
		return securityTransferOperation;
	}

	/**
	 * Sets the security transfer operation.
	 *
	 * @param securityTransferOperation the new security transfer operation
	 */
	public void setSecurityTransferOperation(
			SecurityTransferOperation securityTransferOperation) {
		this.securityTransferOperation = securityTransferOperation;
	}

	/**
	 * Gets the lst transfer type.
	 *
	 * @return the lst transfer type
	 */
	public List<ParameterTable> getLstTransferType() {
		return lstTransferType;
	}

	/**
	 * Sets the lst transfer type.
	 *
	 * @param lstTransferType the new lst transfer type
	 */
	public void setLstTransferType(List<ParameterTable> lstTransferType) {
		this.lstTransferType = lstTransferType;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the data model holder account balance.
	 *
	 * @return the data model holder account balance
	 */
	public GenericDataModel<HolderAccountBalance> getDataModelHolderAccountBalance() {
		return dataModelHolderAccountBalance;
	}

	/**
	 * Sets the data model holder account balance.
	 *
	 * @param dataModelHolderAccountBalance the new data model holder account balance
	 */
	public void setDataModelHolderAccountBalance(
			GenericDataModel<HolderAccountBalance> dataModelHolderAccountBalance) {
		this.dataModelHolderAccountBalance = dataModelHolderAccountBalance;
	}

	/**
	 * Gets the arr holder account balance.
	 *
	 * @return the arr holder account balance
	 */
	public HolderAccountBalance[] getArrHolderAccountBalance() {
		return arrHolderAccountBalance;
	}

	/**
	 * Sets the arr holder account balance.
	 *
	 * @param arrHolderAccountBalance the new arr holder account balance
	 */
	public void setArrHolderAccountBalance(
			HolderAccountBalance[] arrHolderAccountBalance) {
		this.arrHolderAccountBalance = arrHolderAccountBalance;
	}

	/**
	 * Gets the validation message.
	 *
	 * @return the validation message
	 */
	public String getValidationMessage() {
		return validationMessage;
	}

	/**
	 * Sets the validation message.
	 *
	 * @param validationMessage the new validation message
	 */
	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
 * Gets the lst holder account balance.
 *
 * @return the lst holder account balance
 */
public List<HolderAccountBalance> getLstHolderAccountBalance() {
		return lstHolderAccountBalance;
	}

	/**
	 * Sets the lst holder account balance.
	 *
	 * @param lstHolderAccountBalance the new lst holder account balance
	 */
	public void setLstHolderAccountBalance(
			List<HolderAccountBalance> lstHolderAccountBalance) {
		this.lstHolderAccountBalance = lstHolderAccountBalance;
	}

	/**
	 * Gets the holder account balance.
	 *
	 * @return the holder account balance
	 */
	public HolderAccountBalance getHolderAccountBalance() {
		return holderAccountBalance;
	}

	/**
	 * Sets the holder account balance.
	 *
	 * @param holderAccountBalance the new holder account balance
	 */
	public void setHolderAccountBalance(HolderAccountBalance holderAccountBalance) {
		this.holderAccountBalance = holderAccountBalance;
	}
	
	/**
	 * Gets the data model transfer security available.
	 *
	 * @return the data model transfer security available
	 */
	public GenericDataModel<SecurityTransferOperation> getDataModelTransferSecurityAvailable() {
		return dataModelTransferSecurityAvailable;
	}

	/**
	 * Sets the data model transfer security available.
	 *
	 * @param dataModelTransferSecurityAvailable the new data model transfer security available
	 */
	public void setDataModelTransferSecurityAvailable(
			GenericDataModel<SecurityTransferOperation> dataModelTransferSecurityAvailable) {
		this.dataModelTransferSecurityAvailable = dataModelTransferSecurityAvailable;
	}

	/**
	 * Gets the arr security transfer operation.
	 *
	 * @return the arr security transfer operation
	 */
	public SecurityTransferOperation[] getArrSecurityTransferOperation() {
		return arrSecurityTransferOperation;
	}

	/**
	 * Sets the arr security transfer operation.
	 *
	 * @param arrSecurityTransferOperation the new arr security transfer operation
	 */
	public void setArrSecurityTransferOperation(
			SecurityTransferOperation[] arrSecurityTransferOperation) {
		this.arrSecurityTransferOperation = arrSecurityTransferOperation;
	}
	
	/**
	 * Gets the lst transfer state.
	 *
	 * @return the lst transfer state
	 */
	public List<ParameterTable> getLstTransferState() {
		return lstTransferState;
	}

	/**
	 * Sets the lst transfer state.
	 *
	 * @param lstTransferState the new lst transfer state
	 */
	public void setLstTransferState(List<ParameterTable> lstTransferState) {
		this.lstTransferState = lstTransferState;
	}

	/**
	 * Gets the lst transfer actions.
	 *
	 * @return the lst transfer actions
	 */
	public List<ParameterTable> getLstTransferActions() {
		return lstTransferActions;
	}

	/**
	 * Sets the lst transfer actions.
	 *
	 * @param lstTransferActions the new lst transfer actions
	 */
	public void setLstTransferActions(List<ParameterTable> lstTransferActions) {
		this.lstTransferActions = lstTransferActions;
	}

	/**
	 * Gets the lst transfer security available.
	 *
	 * @return the lst transfer security available
	 */
	public List<SecurityTransferOperation> getLstTransferSecurityAvailable() {
		return lstTransferSecurityAvailable;
	}

	/**
	 * Sets the lst transfer security available.
	 *
	 * @param lstTransferSecurityAvailable the new lst transfer security available
	 */
	public void setLstTransferSecurityAvailable(
			List<SecurityTransferOperation> lstTransferSecurityAvailable) {
		this.lstTransferSecurityAvailable = lstTransferSecurityAvailable;
	}

	/**
	 * Checks if is flag revised sto.
	 *
	 * @return true, if is flag revised sto
	 */
	public boolean isFlagRevisedSto() {
		return flagRevisedSto;
	}

	/**
	 * Sets the flag revised sto.
	 *
	 * @param flagRevisedSto the new flag revised sto
	 */
	public void setFlagRevisedSto(boolean flagRevisedSto) {
		this.flagRevisedSto = flagRevisedSto;
	}

	/**
	 * Checks if is flag reject sto.
	 *
	 * @return true, if is flag reject sto
	 */
	public boolean isFlagRejectSto() {
		return flagRejectSto;
	}

	/**
	 * Sets the flag reject sto.
	 *
	 * @param flagRejectSto the new flag reject sto
	 */
	public void setFlagRejectSto(boolean flagRejectSto) {
		this.flagRejectSto = flagRejectSto;
	}

	/**
	 * Checks if is flag approved sto.
	 *
	 * @return true, if is flag approved sto
	 */
	public boolean isFlagApprovedSto() {
		return flagApprovedSto;
	}

	/**
	 * Sets the flag approved sto.
	 *
	 * @param flagApprovedSto the new flag approved sto
	 */
	public void setFlagApprovedSto(boolean flagApprovedSto) {
		this.flagApprovedSto = flagApprovedSto;
	}

	/**
	 * Checks if is flasg annuled sto.
	 *
	 * @return true, if is flasg annuled sto
	 */
	public boolean isFlasgAnnuledSto() {
		return flasgAnnuledSto;
	}

	/**
	 * Sets the flasg annuled sto.
	 *
	 * @param flasgAnnuledSto the new flasg annuled sto
	 */
	public void setFlasgAnnuledSto(boolean flasgAnnuledSto) {
		this.flasgAnnuledSto = flasgAnnuledSto;
	}

	/**
	 * Checks if is flag confirm sto.
	 *
	 * @return true, if is flag confirm sto
	 */
	public boolean isFlagConfirmSto() {
		return flagConfirmSto;
	}

	/**
	 * Sets the flag confirm sto.
	 *
	 * @param flagConfirmSto the new flag confirm sto
	 */
	public void setFlagConfirmSto(boolean flagConfirmSto) {
		this.flagConfirmSto = flagConfirmSto;
	}

	/**
	 * Gets the security transfer operation tmp.
	 *
	 * @return the security transfer operation tmp
	 */
	public SecurityTransferOperation getSecurityTransferOperationTmp() {
		return securityTransferOperationTmp;
	}

	/**
	 * Sets the security transfer operation tmp.
	 *
	 * @param securityTransferOperationTmp the new security transfer operation tmp
	 */
	public void setSecurityTransferOperationTmp(
			SecurityTransferOperation securityTransferOperationTmp) {
		this.securityTransferOperationTmp = securityTransferOperationTmp;
	}

	/**
	 * Checks if is flag register.
	 *
	 * @return true, if is flag register
	 */
	public boolean isFlagRegister() {
		return flagRegister;
	}

	/**
	 * Sets the flag register.
	 *
	 * @param flagRegister the new flag register
	 */
	public void setFlagRegister(boolean flagRegister) {
		this.flagRegister = flagRegister;
	}

	/**
	 * Checks if is flag search.
	 *
	 * @return true, if is flag search
	 */
	public boolean isFlagSearch() {
		return flagSearch;
	}

	/**
	 * Sets the flag search.
	 *
	 * @param flagSearch the new flag search
	 */
	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	/**
	 * Checks if is flag search participant.
	 *
	 * @return true, if is flag search participant
	 */
	public boolean isFlagSearchParticipant() {
		return flagSearchParticipant;
	}
	
	/**
	 * Sets the flag search participant.
	 *
	 * @param flagSearchParticipant the new flag search participant
	 */
	public void setFlagSearchParticipant(boolean flagSearchParticipant) {
		this.flagSearchParticipant = flagSearchParticipant;
	}

	/**
	 * Checks if is flag register origin participant.
	 *
	 * @return true, if is flag register origin participant
	 */
	public boolean isFlagRegisterOriginParticipant() {
		return flagRegisterOriginParticipant;
	}

	/**
	 * Sets the flag register origin participant.
	 *
	 * @param flagRegisterOriginParticipant the new flag register origin participant
	 */
	public void setFlagRegisterOriginParticipant(
			boolean flagRegisterOriginParticipant) {
		this.flagRegisterOriginParticipant = flagRegisterOriginParticipant;
	}

	/**
	 * Checks if is flag register target participant.
	 *
	 * @return true, if is flag register target participant
	 */
	public boolean isFlagRegisterTargetParticipant() {
		return flagRegisterTargetParticipant;
	}

	/**
	 * Sets the flag register target participant.
	 *
	 * @param flagRegisterTargetParticipant the new flag register target participant
	 */
	public void setFlagRegisterTargetParticipant(
			boolean flagRegisterTargetParticipant) {
		this.flagRegisterTargetParticipant = flagRegisterTargetParticipant;
	}
	
	/**
	 * Gets the now time.
	 *
	 * @return the now time
	 */
	public Date getNowTime() {
		return nowTime;
	}

	/**
	 * Sets the now time.
	 *
	 * @param nowTime the new now time
	 */
	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}

	/**
	 * Gets the registers found.
	 *
	 * @return the registers found
	 */
	public Boolean getRegistersFound() {
		return registersFound;
	}

	/**
	 * Sets the registers found.
	 *
	 * @param registersFound the new registers found
	 */
	public void setRegistersFound(Boolean registersFound) {
		this.registersFound = registersFound;
	}

	/**
	 * Gets the radio selected.
	 *
	 * @return the radio selected
	 */
	public Integer getRadioSelected() {
		return radioSelected;
	}

	/**
	 * Sets the radio selected.
	 *
	 * @param radioSelected the new radio selected
	 */
	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

	/**
	 * Gets the lst instrument type.
	 *
	 * @return the lst instrument type
	 */
	public List<ParameterTable> getLstInstrumentType() {
		return lstInstrumentType;
	}

	/**
	 * Sets the lst instrument type.
	 *
	 * @param lstInstrumentType the new lst instrument type
	 */
	public void setLstInstrumentType(List<ParameterTable> lstInstrumentType) {
		this.lstInstrumentType = lstInstrumentType;
	}

	/**
	 * Checks if is check onli available.
	 *
	 * @return true, if is check onli available
	 */
	public boolean isCheckOnliAvailable() {
		return checkOnliAvailable;
	}

	/**
	 * Sets the check onli available.
	 *
	 * @param checkOnliAvailable the new check onli available
	 */
	public void setCheckOnliAvailable(boolean checkOnliAvailable) {
		this.checkOnliAvailable = checkOnliAvailable;
	}

	/**
	 * Gets the min time.
	 *
	 * @return the min time
	 */
	public Date getMinTime() {
		return minTime;
	}

	/**
	 * Sets the min time.
	 *
	 * @param minTime the new min time
	 */
	public void setMinTime(Date minTime) {
		this.minTime = minTime;
	}

	/**
	 * Gets the str document number.
	 *
	 * @return the str document number
	 */
	public String getStrDocumentNumber() {
		return strDocumentNumber;
	}

	/**
	 * Sets the str document number.
	 *
	 * @param strDocumentNumber the new str document number
	 */
	public void setStrDocumentNumber(String strDocumentNumber) {
		this.strDocumentNumber = strDocumentNumber;
	}

	/**
	 * Gets the str sate description.
	 *
	 * @return the str sate description
	 */
	public String getStrSateDescription() {
		return strSateDescription;
	}

	/**
	 * Sets the str sate description.
	 *
	 * @param strSateDescription the new str sate description
	 */
	public void setStrSateDescription(String strSateDescription) {
		this.strSateDescription = strSateDescription;
	}
	
	/**
	 * Gets the participant login.
	 *
	 * @return the participant login
	 */
	public Long getParticipantLogin() {
		return participantLogin;
	}

	/**
	 * Gets the checks if is participant.
	 *
	 * @return the checks if is participant
	 */
	public boolean getIsParticipant() {
		return isParticipant;
	}

	/**
	 * Gets the checks if is depositary.
	 *
	 * @return the checks if is depositary
	 */
	public boolean getIsDepositary() {
		return isDepositary;
	}

	/**
	 * Gets the reject motive list.
	 *
	 * @return the reject motive list
	 */
	public List<ParameterTable> getRejectMotiveList() {
		return rejectMotiveList;
	}

	/**
	 * Sets the reject motive list.
	 *
	 * @param rejectMotiveList the new reject motive list
	 */
	public void setRejectMotiveList(List<ParameterTable> rejectMotiveList) {
		this.rejectMotiveList = rejectMotiveList;
	}

	/**
	 * Gets the other reject motive id.
	 *
	 * @return the other reject motive id
	 */
	public Integer getOtherRejectMotiveId() {
		return otherRejectMotiveId;
	}

	/**
	 * Sets the other reject motive id.
	 *
	 * @param otherRejectMotiveId the new other reject motive id
	 */
	public void setOtherRejectMotiveId(Integer otherRejectMotiveId) {
		this.otherRejectMotiveId = otherRejectMotiveId;
	}

	/**
	 * Gets the int reject motive.
	 *
	 * @return the int reject motive
	 */
	public Integer getIntRejectMotive() {
		return intRejectMotive;
	}

	/**
	 * Sets the int reject motive.
	 *
	 * @param intRejectMotive the new int reject motive
	 */
	public void setIntRejectMotive(Integer intRejectMotive) {
		this.intRejectMotive = intRejectMotive;
	}

	/**
	 * Gets the str reject motive.
	 *
	 * @return the str reject motive
	 */
	public String getStrRejectMotive() {
		return strRejectMotive;
	}

	/**
	 * Sets the str reject motive.
	 *
	 * @param strRejectMotive the new str reject motive
	 */
	public void setStrRejectMotive(String strRejectMotive) {
		this.strRejectMotive = strRejectMotive;
	}

	/**
	 * Checks if is flag target source.
	 *
	 * @return true, if is flag target source
	 */
	public boolean isFlagTargetSource() {
		return flagTargetSource;
	}

	/**
	 * Sets the flag target source.
	 *
	 * @param flagTargetSource the new flag target source
	 */
	public void setFlagTargetSource(boolean flagTargetSource) {
		this.flagTargetSource = flagTargetSource;
	}

	/**
	 * Gets the search security transfer operation.
	 *
	 * @return the search security transfer operation
	 */
	public SecurityTransferOperation getSearchSecurityTransferOperation() {
		return searchSecurityTransferOperation;
	}

	/**
	 * Sets the search security transfer operation.
	 *
	 * @param serchSecurityTransferOperation the new search security transfer operation
	 */
	public void setSearchSecurityTransferOperation(
			SecurityTransferOperation serchSecurityTransferOperation) {
		this.searchSecurityTransferOperation = serchSecurityTransferOperation;
	}

	/**
	 * Checks if is flag same participant.
	 *
	 * @return true, if is flag same participant
	 */
	public boolean isFlagSameParticipant() {
		return flagSameParticipant;
	}

	/**
	 * Sets the flag same participant.
	 *
	 * @param flagSameParticipant the new flag same participant
	 */
	public void setFlagSameParticipant(boolean flagSameParticipant) {
		this.flagSameParticipant = flagSameParticipant;
	}

	/**
	 * Checks if is flag source target.
	 *
	 * @return true, if is flag source target
	 */
	public boolean isFlagSourceTarget() {
		return flagSourceTarget;
	}

	/**
	 * Sets the flag source target.
	 *
	 * @param flagSourceTarget the new flag source target
	 */
	public void setFlagSourceTarget(boolean flagSourceTarget) {
		this.flagSourceTarget = flagSourceTarget;
	}

	/**
	 * Checks if is flag review target.
	 *
	 * @return true, if is flag review target
	 */
	public boolean isFlagReviewTarget() {
		return flagReviewTarget;
	}

	/**
	 * Sets the flag review target.
	 *
	 * @param flagReviewTarget the new flag review target
	 */
	public void setFlagReviewTarget(boolean flagReviewTarget) {
		this.flagReviewTarget = flagReviewTarget;
	}

	/**
	 * Checks if is flag review source.
	 *
	 * @return true, if is flag review source
	 */
	public boolean isFlagReviewSource() {
		return flagReviewSource;
	}

	/**
	 * Sets the flag review source.
	 *
	 * @param flagReviewSource the new flag review source
	 */
	public void setFlagReviewSource(boolean flagReviewSource) {
		this.flagReviewSource = flagReviewSource;
	}

	/**
	 * Gets the holder market fact balance.
	 *
	 * @return the holder market fact balance
	 */
	public MarketFactBalanceHelpTO getHolderMarketFactBalance() {
		return holderMarketFactBalance;
	}

	/**
	 * Sets the holder market fact balance.
	 *
	 * @param holderMarketFactBalance the new holder market fact balance
	 */
	public void setHolderMarketFactBalance(MarketFactBalanceHelpTO holderMarketFactBalance) {
		this.holderMarketFactBalance = holderMarketFactBalance;
	}

	/**
	 * Gets the source holder.
	 *
	 * @return the source holder
	 */
	public Holder getSourceHolder() {
		return sourceHolder;
	}

	/**
	 * Sets the source holder.
	 *
	 * @param sourceHolder the new source holder
	 */
	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}

	/**
	 * Gets the target holder.
	 *
	 * @return the target holder
	 */
	public Holder getTargetHolder() {
		return targetHolder;
	}

	/**
	 * Sets the target holder.
	 *
	 * @param targetHolder the new target holder
	 */
	public void setTargetHolder(Holder targetHolder) {
		this.targetHolder = targetHolder;
	}

	/**
	 * Gets the source account to.
	 *
	 * @return the source account to
	 */
	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	/**
	 * Sets the source account to.
	 *
	 * @param sourceAccountTo the new source account to
	 */
	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}

	/**
	 * Gets the target account to.
	 *
	 * @return the target account to
	 */
	public HolderAccountHelperResultTO getTargetAccountTo() {
		return targetAccountTo;
	}

	/**
	 * Sets the target account to.
	 *
	 * @param targetAccountTo the new target account to
	 */
	public void setTargetAccountTo(HolderAccountHelperResultTO targetAccountTo) {
		this.targetAccountTo = targetAccountTo;
	}

	/**
	 * Gets the lst source account to.
	 *
	 * @return the lst source account to
	 */
	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	/**
	 * Sets the lst source account to.
	 *
	 * @param lstSourceAccountTo the new lst source account to
	 */
	public void setLstSourceAccountTo(
			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	/**
	 * Gets the lst target account to.
	 *
	 * @return the lst target account to
	 */
	public List<HolderAccountHelperResultTO> getLstTargetAccountTo() {
		return lstTargetAccountTo;
	}

	/**
	 * Sets the lst target account to.
	 *
	 * @param lstTargetAccountTo the new lst target account to
	 */
	public void setLstTargetAccountTo(
			List<HolderAccountHelperResultTO> lstTargetAccountTo) {
		this.lstTargetAccountTo = lstTargetAccountTo;
	}

	/**
	 * Gets the view error message.
	 *
	 * @return the view error message
	 */
	public String getViewErrorMessage() {
		return viewErrorMessage;
	}

	/**
	 * Sets the view error message.
	 *
	 * @param viewErrorMessage the new view error message
	 */
	public void setViewErrorMessage(String viewErrorMessage) {
		this.viewErrorMessage = viewErrorMessage;
	}

	/**
	 * Gets the int hidden motive.
	 *
	 * @return the int hidden motive
	 */
	public Integer getIntHiddenMotive() {
		return intHiddenMotive;
	}

	/**
	 * Sets the int hidden motive.
	 *
	 * @param intHiddenMotive the new int hidden motive
	 */
	public void setIntHiddenMotive(Integer intHiddenMotive) {
		this.intHiddenMotive = intHiddenMotive;
	}

	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	/**
	 * Gets the help market fact balance view.
	 *
	 * @return the help market fact balance view
	 */
	public MarketFactBalanceHelpTO getHelpMarketFactBalanceView() {
		return helpMarketFactBalanceView;
	}

	/**
	 * Sets the help market fact balance view.
	 *
	 * @param helpMarketFactBalanceView the new help market fact balance view
	 */
	public void setHelpMarketFactBalanceView(
			MarketFactBalanceHelpTO helpMarketFactBalanceView) {
		this.helpMarketFactBalanceView = helpMarketFactBalanceView;
	}

	/**
	 * Gets the map security instrument type.
	 *
	 * @return the map security instrument type
	 */
	public Map<Integer, String> getMapSecurityInstrumentType() {
		return mapSecurityInstrumentType;
	}

	/**
	 * Sets the map security instrument type.
	 *
	 * @param mapSecurityInstrumentType the map security instrument type
	 */
	public void setMapSecurityInstrumentType(
			Map<Integer, String> mapSecurityInstrumentType) {
		this.mapSecurityInstrumentType = mapSecurityInstrumentType;
	}

	/**
	 * Gets the lst security type.
	 *
	 * @return the lst security type
	 */
	public List<ParameterTable> getLstSecurityType() {
		return lstSecurityType;
	}

	/**
	 * Sets the lst security type.
	 *
	 * @param lstSecurityType the new lst security type
	 */
	public void setLstSecurityType(List<ParameterTable> lstSecurityType) {
		this.lstSecurityType = lstSecurityType;
	}

	/**
	 * Gets the map security type.
	 *
	 * @return the map security type
	 */
	public Map<Integer, String> getMapSecurityType() {
		return mapSecurityType;
	}

	/**
	 * Sets the map security type.
	 *
	 * @param mapSecurityType the map security type
	 */
	public void setMapSecurityType(Map<Integer, String> mapSecurityType) {
		this.mapSecurityType = mapSecurityType;
	}

	/**
	 * Gets the map lst source account to.
	 *
	 * @return the map lst source account to
	 */
	public Map<Long, List<HolderAccountHelperResultTO>> getMapLstSourceAccountTo() {
		return mapLstSourceAccountTo;
	}

	/**
	 * Sets the map lst source account to.
	 *
	 * @param mapLstSourceAccountTo the map lst source account to
	 */
	public void setMapLstSourceAccountTo(
			Map<Long, List<HolderAccountHelperResultTO>> mapLstSourceAccountTo) {
		this.mapLstSourceAccountTo = mapLstSourceAccountTo;
	}

	/**
	 * Gets the map lst target account to.
	 *
	 * @return the map lst target account to
	 */
	public Map<Long, List<HolderAccountHelperResultTO>> getMapLstTargetAccountTo() {
		return mapLstTargetAccountTo;
	}

	/**
	 * Sets the map lst target account to.
	 *
	 * @param mapLstTargetAccountTo the map lst target account to
	 */
	public void setMapLstTargetAccountTo(
			Map<Long, List<HolderAccountHelperResultTO>> mapLstTargetAccountTo) {
		this.mapLstTargetAccountTo = mapLstTargetAccountTo;
	}
	
	/**
	 * Lst target account to search.
	 *
	 * @param OperationNumber the operation number
	 * @return the list
	 */
	public List<HolderAccountHelperResultTO> lstTargetAccountToSearch(Long OperationNumber){
		return mapLstTargetAccountTo.get(OperationNumber);
	}
	

	/**
	 * Lst source account to search.
	 *
	 * @param OperationNumber the operation number
	 * @return the list
	 */
	public List<HolderAccountHelperResultTO> lstSourceAccountToSearch(Long OperationNumber){
		return mapLstSourceAccountTo.get(OperationNumber);
	}

	/**
	 * Disabled cbo source.
	 *
	 * @param OperationNumber the operation number
	 * @return the boolean
	 */
	public Boolean disabledCboSource(Long OperationNumber){
		List<HolderAccountHelperResultTO> lstResult =  mapLstSourceAccountTo.get(OperationNumber);
		if(lstResult != null && lstResult.size()>0){
			if(lstResult.size()==1){
				return true;
			}
			return false;
		}else{
			return false;
		}
	}
	
	/**
	 * Disabled cbo target.
	 *
	 * @param OperationNumber the operation number
	 * @return the boolean
	 */
	public Boolean disabledCboTarget(Long OperationNumber){
		List<HolderAccountHelperResultTO> lstResult =  mapLstTargetAccountTo.get(OperationNumber);
		if(lstResult != null && lstResult.size()>0){
			if(lstResult.size()==1){
				return true;
			}
			return false;
		}else{
			return false;
		}
	}
	
	/**
	 * Checks if is flag account origin visible.
	 *
	 * @return true, if is flag account origin visible
	 */
	public boolean isFlagAccountOriginVisible() {
		return flagAccountOriginVisible;
	}

	/**
	 * Sets the flag account origin visible.
	 *
	 * @param flagAccountOriginVisible the new flag account origin visible
	 */
	public void setFlagAccountOriginVisible(boolean flagAccountOriginVisible) {
		this.flagAccountOriginVisible = flagAccountOriginVisible;
	}

	/**
	 * Checks if is flag account target visible.
	 *
	 * @return true, if is flag account target visible
	 */
	public boolean isFlagAccountTargetVisible() {
		return flagAccountTargetVisible;
	}

	/**
	 * Sets the flag account target visible.
	 *
	 * @param flagAccountTargetVisible the new flag account target visible
	 */
	public void setFlagAccountTargetVisible(boolean flagAccountTargetVisible) {
		this.flagAccountTargetVisible = flagAccountTargetVisible;
	}

	/**
	 * Checks if is flag target origin.
	 *
	 * @return true, if is flag target origin
	 */
	public boolean isFlagTargetOrigin() {
		return flagTargetOrigin;
	}

	/**
	 * Sets the flag target origin.
	 *
	 * @param flagTargetOrigin the new flag target origin
	 */
	public void setFlagTargetOrigin(boolean flagTargetOrigin) {
		this.flagTargetOrigin = flagTargetOrigin;
	}

	/**
	 * Checks if is flag view market fact.
	 *
	 * @return true, if is flag view market fact
	 */
	public boolean isFlagViewMarketFact() {
		return flagViewMarketFact;
	}

	/**
	 * Sets the flag view market fact.
	 *
	 * @param flagViewMarketFact the new flag view market fact
	 */
	public void setFlagViewMarketFact(boolean flagViewMarketFact) {
		this.flagViewMarketFact = flagViewMarketFact;
	}

	/**
	 * Gets the lst target participant.
	 *
	 * @return the lst target participant
	 */
	public List<Participant> getLstTargetParticipant() {
		return lstTargetParticipant;
	}

	/**
	 * Sets the lst target participant.
	 *
	 * @param lstTargetParticipant the new lst target participant
	 */
	public void setLstTargetParticipant(List<Participant> lstTargetParticipant) {
		this.lstTargetParticipant = lstTargetParticipant;
	}

	/**
	 * Gets the list operation number.
	 *
	 * @return the listOperationNumber
	 */
	public String getListOperationNumber() {
		return listOperationNumber;
	}

	/**
	 * Sets the list operation number.
	 *
	 * @param listOperationNumber the listOperationNumber to set
	 */
	public void setListOperationNumber(String listOperationNumber) {
		this.listOperationNumber = listOperationNumber;
	}

	/**
	 * Checks if is flag participante investor.
	 *
	 * @return true, if is flag participante investor
	 */
	public boolean isFlagParticipanteInvestor() {
		return flagParticipanteInvestor;
	}

	/**
	 * Sets the flag participante investor.
	 *
	 * @param flagParticipanteInvestor the new flag participante investor
	 */
	public void setFlagParticipanteInvestor(boolean flagParticipanteInvestor) {
		this.flagParticipanteInvestor = flagParticipanteInvestor;
	}

	/**
	 * Checks if is review all.
	 *
	 * @return true, if is review all
	 */
	public boolean isReviewAll() {
		return reviewAll;
	}

	/**
	 * Sets the review all.
	 *
	 * @param reviewAll the new review all
	 */
	public void setReviewAll(boolean reviewAll) {
		this.reviewAll = reviewAll;
	}
}
