package com.pradera.custody.tranfersecurities.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.tranfersecurities.to.SecurityTransferOperationTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferMarketfact;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransferAvailableServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class TransferAvailableServiceBean extends CrudDaoServiceBean{

	/** The transfer available. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailable;
	
	/**
	 * Gets the list holder account balance.
	 *
	 * @param filter the filter
	 * @param chechOnliAvailable the chech onli available
	 * @param securityFilter the security filter
	 * @return the list holder account balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getListHolderAccountBalance(HolderAccountBalance filter, boolean chechOnliAvailable, Security securityFilter, UserInfo userInfo) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" Select HAB.totalBalance, " +
					   " HAB.availableBalance, " +
					   " SE.idSecurityCodePk, " +
					   " SE.description, " +
					   " SE.idIsinCode, " +
					   " SE.securityClass, " +
					   " (Select count(*) from HolderMarketFactBalance HMB where HMB.availableBalance > 0 and HMB.indActive = 1 and HMB.security.idSecurityCodePk = HAB.id.idSecurityCodePk and " +
					   " HMB.holderAccount.idHolderAccountPk = HAB.id.idHolderAccountPk and HMB.participant.idParticipantPk = HAB.id.idParticipantPk) "
					   + " , SE.notTraded " +
					     " , SE.stateSecurity " + //8
					   " From HolderAccountBalance HAB join HAB.security SE join HAB.participant part");
		sbQuery.append(" Where HAB.id.idSecurityCodePk = SE.idSecurityCodePk ");
		sbQuery.append(" and HAB.availableBalance > 0 ");
		
		if(chechOnliAvailable){
			sbQuery.append(" and HAB.availableBalance > 0 ");
		}
		if(Validations.validateIsNotNullAndPositive(securityFilter.getSecurityClass())){
			sbQuery.append(" and SE.securityClass = :SecurityClassPrm");
			parameters.put("SecurityClassPrm",securityFilter.getSecurityClass());
		}
		if(Validations.validateIsNotNullAndPositive(securityFilter.getStateSecurity())){
			sbQuery.append(" and SE.id.stateSecurity = :securityStatePrm ");
			parameters.put("securityStatePrm",securityFilter.getStateSecurity());
		}
		if( Validations.validateIsNotNull(filter.getId().getIdHolderAccountPk()) ){
			sbQuery.append(" and HAB.id.idHolderAccountPk = :holderAccountPkPrm ");
			parameters.put("holderAccountPkPrm", filter.getId().getIdHolderAccountPk());
		}
		if( Validations.validateIsNotNull(filter.getId().getIdParticipantPk()) ){
			sbQuery.append(" and HAB.id.idParticipantPk = :participantPkPrm ");
			parameters.put("participantPkPrm", filter.getId().getIdParticipantPk());
		}
		if( Validations.validateIsNotNull(filter.getId().getIdSecurityCodePk()) ){
			sbQuery.append(" and HAB.id.idSecurityCodePk = :idSecurityCodePkPrm ");
			parameters.put("idSecurityCodePkPrm", filter.getId().getIdSecurityCodePk());
		}
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
			if(Validations.validateIsNullOrNotPositive(securityFilter.getSecurityClass())){
				sbQuery.append(" and SE.securityClass in (1976,420) ");
			}
			sbQuery.append(" and SE.issuer.idIssuerPk = :idIssuerPkPrm ");
			parameters.put("idIssuerPkPrm", userInfo.getUserAccountSession().getIssuerCode());
		}
		
		if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
			sbQuery.append(" AND EXISTS ( " );
			sbQuery.append(" 	SELECT 1 " );
			
			sbQuery.append(" 	FROM HolderAccountBalance HAB_SUB " );
			sbQuery.append(" 		inner JOIN HAB_SUB.participant PAR " );
			sbQuery.append(" 		inner JOIN HAB_SUB.holderAccount HA " );
			sbQuery.append(" 		inner JOIN HAB_SUB.security S " );
			sbQuery.append(" 		inner JOIN HA.holderAccountDetails HAD " );
			sbQuery.append(" 		inner JOIN HAD.holder H " );
			
			sbQuery.append(" 	WHERE 1=1 ");
			sbQuery.append(" 		AND S.idSecurityCodePk = SE.idSecurityCodePk");
			sbQuery.append(" 		AND part.documentType = H.documentType");
			sbQuery.append(" 		AND part.documentNumber = H.documentNumber");
			
			sbQuery.append(" 		AND S.stateSecurity = :stateSecurity");
			sbQuery.append(" 		AND S.instrumentType = :instrumentType");
			sbQuery.append(" ) " );
			
			parameters.put("stateSecurity", SecurityStateType.REGISTERED.getCode());
			parameters.put("instrumentType", InstrumentType.FIXED_INCOME.getCode());
		}
		
		sbQuery.append(" 	order by SE.idSecurityCodePk asc" );
			
		List<HolderAccountBalance> lstHolderAccountBalance = new ArrayList<HolderAccountBalance>();
		HolderAccountBalance holderAccountBalances = null;
		HolderAccountBalancePK id = null;
		Security security = null;
		List<Object[]> lstObject = findListByQueryString(sbQuery.toString(), parameters);
		
		for (Object obj[] : lstObject) {
			holderAccountBalances = new HolderAccountBalance();
			id = new HolderAccountBalancePK();
			security = new Security();
			
			holderAccountBalances.setTotalBalance((BigDecimal)obj[0]);
			holderAccountBalances.setAvailableBalance((BigDecimal)obj[1]);
			
			if(Validations.validateIsNotNull(filter.getTransferAmmount())){
				if( Validations.validateIsNotNullAndPositive(filter.getTransferAmmount().intValue()) )
				holderAccountBalances.setTransferAmmount(filter.getTransferAmmount());
			}
			security.setSecurityClass((Integer)obj[5]);
			security.setNotTraded((Integer)obj[7]);
			security.setIdIsinCode((String)obj[4]);
			security.setDescription((String)obj[3]);
			security.setIdSecurityCodePk((String)obj[2]);
			security.setStateSecurity((Integer)obj[8]);
			id.setIdSecurityCodePk(security.getIdSecurityCodePk());
			holderAccountBalances.setSecurity(security);
			if( Validations.validateIsNotNull(filter.getId().getIdHolderAccountPk()) )
				id.setIdHolderAccountPk(filter.getId().getIdHolderAccountPk());
			if( Validations.validateIsNotNull(filter.getId().getIdParticipantPk()) )
				id.setIdParticipantPk(filter.getId().getIdParticipantPk());
			holderAccountBalances.setId(id);
			holderAccountBalances.setDisabled(true);
			holderAccountBalances.setMarketCount(new Long(obj[6].toString()));
			lstHolderAccountBalance.add(holderAccountBalances);
		}
		
		
		return lstHolderAccountBalance;
	}
	
	/**
	 * Gets the list market fact balance in transfer operation.
	 *
	 * @param idSecurityTransferOperationPk the id security transfer operation pk
	 * @param todate the todate
	 * @return the list market fact balance in transfer operation
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactDetailHelpTO> getListMarketFactBalanceInTransferOperation(Long idSecurityTransferOperationPk, boolean todate) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select STM.marketDate, " + 	//0
				   	   " STM.marketRate, "+				//1
					   " STM.marketPrice, "+			//2
					   " STM.quantityOperation ");		//3
					  if(todate){
		sbQuery.append(", HMB.totalBalance, "+			//4
					   " HMB.availableBalance, "+		//5
					   " HMB.marketPrice ");		//6
					  }
		sbQuery.append(" From  SecurityTransferMarketfact STM JOIN STM.securityTransferOperation STO "); 
					 if(todate){
		sbQuery.append(",  	   HolderMarketFactBalance HMB ");
					 }
		sbQuery.append(" Where STO.idTransferOperationPk = :idTransferOperationPkPrm ");
					 if(todate){
		sbQuery.append(" and HMB.holderAccount.idHolderAccountPk = STO.sourceHolderAccount.idHolderAccountPk ");
		sbQuery.append(" and HMB.participant.idParticipantPk = STO.sourceParticipant.idParticipantPk ");
		sbQuery.append(" and HMB.security.idSecurityCodePk = STO.securities.idSecurityCodePk ");
		sbQuery.append(" and HMB.marketDate = STM.marketDate ");
		sbQuery.append(" and nvl(HMB.marketRate,1) = decode( nvl(HMB.marketRate,1), nvl(STM.marketRate,1), nvl(STM.marketRate,1), nvl(HMB.marketRate,1) ) ");
		sbQuery.append(" and nvl(HMB.marketPrice,1) = decode( nvl(HMB.marketPrice,1), nvl(STM.marketPrice,1), nvl(STM.marketPrice,1), nvl(HMB.marketPrice,1) ) ");
					 }
		Query query = em.createQuery(sbQuery.toString());

		query.setParameter("idTransferOperationPkPrm", idSecurityTransferOperationPk);
		

		List<MarketFactDetailHelpTO> lstMarketFactDetailHelpTO = new ArrayList<MarketFactDetailHelpTO>();
		MarketFactDetailHelpTO marketFactDetailHelpTO = null;
		for (int i = 0; i < query.getResultList().size(); i++) {
			
			Object obj[] = (Object[])query.getResultList().get(i);
			
			marketFactDetailHelpTO = new MarketFactDetailHelpTO();
			
			if(Validations.validateIsNotNull(obj[0])){
				marketFactDetailHelpTO.setMarketDate((Date)obj[0]);
			}
			
			if(Validations.validateIsNotNull(obj[1])){
				marketFactDetailHelpTO.setMarketRate(new BigDecimal((obj[1]).toString()));
			}
			
			if(Validations.validateIsNotNull(obj[2])){
				marketFactDetailHelpTO.setMarketPrice(new BigDecimal((obj[2]).toString()));
			}

			if(Validations.validateIsNotNull(obj[3])){
				marketFactDetailHelpTO.setEnteredBalance(new BigDecimal((obj[3]).toString()));
			}

			if(todate){
				if(Validations.validateIsNotNull(obj[4])){
					marketFactDetailHelpTO.setTotalBalance(new BigDecimal((obj[4]).toString()));
				}
				
				if(Validations.validateIsNotNull(obj[5])){
					marketFactDetailHelpTO.setAvailableBalance(new BigDecimal((obj[5]).toString()));
				}
				if(Validations.validateIsNotNull(obj[6])){
					marketFactDetailHelpTO.setMarketPrice(new BigDecimal((obj[6]).toString()));
				}
			}
			marketFactDetailHelpTO.setTodate(todate);
			
			lstMarketFactDetailHelpTO.add(marketFactDetailHelpTO);
		}
		return lstMarketFactDetailHelpTO;
	}
	
	/**
	 * Gets the list market fact balance in transfer operation.
	 *
	 * @param hab the hab
	 * @return the list market fact balance in transfer operation
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactDetailHelpTO> getListMarketFactBalanceInTransferOperation(HolderAccountBalance hab) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select STM.marketDate, " + 	//0
				   	   " STM.marketRate, "+				//1
					   " STM.marketPrice, "+			//2
					   " STM.totalBalance, "+			//3
					   " STM.availableBalance ");		//4					
		sbQuery.append(" From  HolderMarketFactBalance STM  "); 		
		sbQuery.append(" Where STM.holderAccount.idHolderAccountPk = :idHolderAccountPk ");
		sbQuery.append(" and STM.participant.idParticipantPk = :idParticipantPk ");
		sbQuery.append(" and STM.security.idSecurityCodePk = :idSecurityCodePk ");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idHolderAccountPk", hab.getId().getIdHolderAccountPk());
		query.setParameter("idParticipantPk",  hab.getId().getIdParticipantPk());
		query.setParameter("idSecurityCodePk",  hab.getId().getIdSecurityCodePk());

		List<MarketFactDetailHelpTO> lstMarketFactDetailHelpTO = new ArrayList<MarketFactDetailHelpTO>();
		MarketFactDetailHelpTO marketFactDetailHelpTO = null;
		for (int i = 0; i < query.getResultList().size(); i++) {
			
			Object obj[] = (Object[])query.getResultList().get(i);
			
			marketFactDetailHelpTO = new MarketFactDetailHelpTO();
			
			if(Validations.validateIsNotNull(obj[0])){
				marketFactDetailHelpTO.setMarketDate((Date)obj[0]);
			}
			
			if(Validations.validateIsNotNull(obj[1])){
				marketFactDetailHelpTO.setMarketRate(new BigDecimal((obj[1]).toString()));
			}
			
			if(Validations.validateIsNotNull(obj[2])){
				marketFactDetailHelpTO.setMarketPrice(new BigDecimal((obj[2]).toString()));
			}

			if(Validations.validateIsNotNull(obj[3])){
				marketFactDetailHelpTO.setTotalBalance(new BigDecimal((obj[3]).toString()));
			}
			if(Validations.validateIsNotNull(obj[4])){
				marketFactDetailHelpTO.setAvailableBalance(new BigDecimal((obj[4]).toString()));
			}
			marketFactDetailHelpTO.setEnteredBalance(hab.getTransferAmmount());
			
			lstMarketFactDetailHelpTO.add(marketFactDetailHelpTO);
		}
		return lstMarketFactDetailHelpTO;
	}
	
	/**
	 * Gets the exist security for holder.
	 *
	 * @param filter the filter
	 * @return the exist security for holder
	 * @throws ServiceException the service exception
	 */
	public Integer getExistSecurityForHolder(HolderAccountBalance filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select count(HAB.totalBalance) " + 
					   " From HolderAccountBalance HAB ");
		sbQuery.append(" Where HAB.id.idSecurityCodePk = :idSecurityCodePkPrm ");
		sbQuery.append(" and HAB.id.idHolderAccountPk = :holderAccountPkPrm ");
		sbQuery.append(" and HAB.id.idParticipantPk = :participantPkPrm ");
				
		Query query = em.createQuery(sbQuery.toString()); 

		query.setParameter("holderAccountPkPrm", filter.getId().getIdHolderAccountPk());
		query.setParameter("participantPkPrm", filter.getId().getIdParticipantPk());
		query.setParameter("idSecurityCodePkPrm", filter.getId().getIdSecurityCodePk());
	
		return ((Long)query.getSingleResult()).intValue() ;
			
	}
	
	
	/**
	 * Gets the security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security getSecurity(String idSecurityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select SE " +
					   " From Security SE ");
		sbQuery.append(" Where 1=1 ");

		if( Validations.validateIsNotNull(idSecurityCodePk) )
			sbQuery.append(" and SE.idSecurityCodePk = :idSecurityCodePk ");

		Query query = em.createQuery(sbQuery.toString()); 
		
		if(Validations.validateIsNotNull(idSecurityCodePk))
    		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
		return (Security)query.getSingleResult();
	}
	
	/**
	 * Gets the coupon count with security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the coupon count with security
	 * @throws ServiceException the service exception
	 */
	public Integer getCouponCountWithSecurity(String idSecurityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" select count(distinct(pic.coupon_number)) "+
						" from security se "+ 
						" inner join "+ 
						" interest_payment_schedule ips "+ 
						" on ips.id_security_code_fk=se.id_security_code_pk "+ 
						" inner join program_interest_coupon pic "+
						" on ips.id_int_payment_schedule_pk = pic.id_int_payment_schedule_fk "+
						" where se.id_security_code_pk = :securityCodePkPrm ");
		Query query = em.createNativeQuery(sbQuery.toString()); 
		
		query.setParameter("securityCodePkPrm", idSecurityCodePk);
		
		if(query.getSingleResult() != null){
			return ((BigDecimal)query.getSingleResult()).intValue();
		}else{
			return 0;
		}
		
	}
	
	/**
	 * Valid security with instrument type.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param securityClass the security class
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long validSecurityWithSecurityClass(String idSecurityCodePk, Integer securityClass) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select nvl(count(SE.stateSecurity),0) From Security SE  ");
		sbQuery.append(" Where 1=1 ");
		if(Validations.validateIsNotNullAndPositive(securityClass) )
		sbQuery.append(" and SE.securityClass = :securityClassPrm ");
		sbQuery.append(" and SE.idSecurityCodePk = :securityCodePkPrm ");

		Query query = em.createQuery(sbQuery.toString()); 
		
		query.setParameter("securityCodePkPrm", idSecurityCodePk);
		if(Validations.validateIsNotNullAndPositive(securityClass) )
		query.setParameter("securityClassPrm", securityClass);
		
		return (Long)query.getSingleResult();
	}

	
	/**
	 * Find security transfer marketfact.
	 *
	 * @param idSecTansferMarketfactPk the id sec tansfer marketfact pk
	 * @param idTransferOperationPk the id transfer operation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityTransferMarketfact> findSecurityTransferMarketfact(Long idSecTansferMarketfactPk, Long idTransferOperationPk) 
																		  throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select stm From SecurityTransferMarketfact stm  ");
		sbQuery.append(" Where 1=1 ");
		
		if( Validations.validateIsNotNullAndPositive(idSecTansferMarketfactPk) ){
			sbQuery.append(" and stm.IdSecTansferMarketfactPk = :idSecTansferMarketfactPkPrm ");
		}
		if( Validations.validateIsNotNullAndPositive(idTransferOperationPk) ){
			sbQuery.append(" and stm.securityTransferOperation.idTransferOperationPk = :idTransferOperationPkPrm ");
		}
		
		TypedQuery<SecurityTransferMarketfact> query = em.createQuery(sbQuery.toString(),SecurityTransferMarketfact.class); 

		if( Validations.validateIsNotNullAndPositive(idSecTansferMarketfactPk) ){
			query.setParameter("idSecTansferMarketfactPkPrm", idSecTansferMarketfactPk);
		}
		if( Validations.validateIsNotNullAndPositive(idTransferOperationPk) ){
			query.setParameter("idTransferOperationPkPrm", idTransferOperationPk);
		}
		
		return query.getResultList();
	}
	
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Date getDate(){
	
		int intDays = 7;
		//CommonsUtilities.addDate(CommonsUtilities.currentDate(),-(intDays+1))
			
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT count(1) FROM holiday "+
						" where "+
						" to_char(date_holiday,'d') != 1 and to_char(date_holiday,'d') != 7 "+
						" and date_holiday <= :endDate "+ 
						" and date_holiday >  :initDate");
		Query query = em.createNativeQuery(sbQuery.toString()); 
		query.setParameter("endDate",CommonsUtilities.currentDate() ,TemporalType.DATE);
		query.setParameter("initDate",CommonsUtilities.addDate(CommonsUtilities.currentDate(),-intDays) ,TemporalType.DATE);
		
		BigDecimal daysBd = (BigDecimal)query.getSingleResult();
		intDays += Integer.parseInt(daysBd.toString());
	
		return CommonsUtilities.addDate(CommonsUtilities.currentDate(),-intDays);
	} 
	
	/**
	 * Weekends days.
	 *
	 * @param date the date
	 * @param days the days
	 * @return the date
	 */
	public Date weekendsDays(Date date,Integer days){
		//
		GregorianCalendar calendarInicio = new GregorianCalendar();
		calendarInicio.setTime(date);
		
		GregorianCalendar calendarFin = new GregorianCalendar();
		Date dateEnd = CommonsUtilities.addDate(date, days);
		calendarFin.setTime(dateEnd);
		int i=0;
		do{
			if(calendarInicio.DAY_OF_WEEK==1 || calendarInicio.DAY_OF_WEEK==6){
				i++;
			}
		}while(!calendarInicio.equals(calendarFin));
		
		return CommonsUtilities.addDate(dateEnd, i);
	}
	

	/**
	 * Gets the account pk with cuis.
	 *
	 * @param lstCui the lst cui
	 * @param ParticipantPk the participant pk
	 * @param holder the holder
	 * @return the account pk with cuis
	 * @throws ServiceException the service exception
	 */
	public List<Long> getAccountPkWithCuis(List<Long> lstCui, Long ParticipantPk, HolderAccount holder) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select HA.idHolderAccountPk " +
					   " From HolderAccountDetail HAD INNER JOIN HAD.holderAccount HA INNER JOIN HA.participant PA INNER JOIN HAD.holder HO ");
		sbQuery.append(" Where ");
		sbQuery.append(" PA.idParticipantPk = :participantPkPrm and ");
		if(Validations.validateIsNotNull(holder) && Validations.validateIsNotNullAndPositive(holder.getAccountType())){
			sbQuery.append(" HA.accountType = :accountTypePrm and ");
		}
		sbQuery.append(" HO.idHolderPk in (:lstCuis) ");
		sbQuery.append(" GROUP BY HA.idHolderAccountPk HAVING COUNT(HA.idHolderAccountPk) = :sizePrm ");
		Query query = em.createQuery(sbQuery.toString()); 
		query.setParameter("lstCuis",lstCui);
		query.setParameter("participantPkPrm",ParticipantPk);
		if(Validations.validateIsNotNull(holder) && Validations.validateIsNotNullAndPositive(holder.getAccountType())){
			query.setParameter("accountTypePrm",holder.getAccountType());
		}
		query.setParameter("sizePrm",new Long(lstCui.size()));
		
		List<Long> lstLongHolderAccountPk = new ArrayList<Long>();
		List<Object> lstObject = query.getResultList();

		if(lstObject.size()>0){
			for (int i = 0; i < lstObject.size(); i++) {
				Long obj = null;
				if(lstObject.get(i) != null){
					obj = (Long)lstObject.get(i);
					lstLongHolderAccountPk.add(obj);
				}
			}
		}
		
		return lstLongHolderAccountPk;
		
	}
	
	/**
	 * Gets the cuis with account.
	 *
	 * @param holderAccount the holder account
	 * @return the cuis with account
	 * @throws ServiceException the service exception
	 */
	public List<Long> getCuisWithAccount(HolderAccount holderAccount) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select HAD.holder.idHolderPk " +
					   " From HolderAccountDetail HAD ");
		sbQuery.append(" Where ");
		sbQuery.append(" HAD.holderAccount.idHolderAccountPk = :idHolderAccountPkPrm ");
		Query query = em.createQuery(sbQuery.toString()); 
		query.setParameter("idHolderAccountPkPrm",holderAccount.getIdHolderAccountPk());
		
		List<Long> lstLongCuis = new ArrayList<Long>();
		List<Object> lstObject = query.getResultList();
		
		if(lstObject.size()>0){
			for (int i = 0; i < lstObject.size(); i++) {
				Long obj = null;
				if(lstObject.get(i) != null){
					obj = (Long)lstObject.get(i);
					lstLongCuis.add(obj);
				}
			}
		}
		
		return lstLongCuis;
		
	}

	/**
	 * Buscar las solicitudes pendientes 
	 * mayores a la cantidad de dias establecidos 
	 * en la tabla de parametros.
	 *
	 * @param securityTransferOperationTO the security transfer operation to
	 * @return the list
	 */
	
	public List<SecurityTransferOperation> searchTransferOperationsByDays(SecurityTransferOperationTO  securityTransferOperationTO){

		List<SecurityTransferOperation> lstSecurityTransferOperation = new ArrayList<SecurityTransferOperation>();
		/*Query para obtener la cantidad de dias de la tabla PARAMETER_TABLE*/
		
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append(" select " +
						" sto.idTransferOperationPk, " +//0
						" sto.transferType, " +//1
						" sto.sourceParticipant.idParticipantPk, "+//2
						" sto.targetParticipant.idParticipantPk, "+//3
						" sto.sourceHolderAccount.idHolderAccountPk, "+//4
						" sto.targetHolderAccount.idHolderAccountPk, "+//5
						" sto.quantityOperation, " +//6
						" sto.securities.idSecurityCodePk, " +//7
						" sto.state, " +//8
						" co.operationType, " +//9
						" co.operationNumber, "+//10
						" co.operationDate "+//11
						" from SecurityTransferOperation sto Join sto.custodyOperation co " +								
						" where 1=1 ");
		sbQuery.append(" and trunc(co.registryDate) = TRUNC(:finalDatePrm) ");
		sbQuery.append(" and co.state in :statesPrm ");
		sbQuery.append(" and sto.indBlockTransfer = :indBlockPrm ");
		sbQuery.append(" Order By co.operationNumber ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("finalDatePrm", securityTransferOperationTO.getOperationEndDate());
		query.setParameter("statesPrm", securityTransferOperationTO.getStates());
		query.setParameter("indBlockPrm", securityTransferOperationTO.getIndBlock());

		/* settear los parametros */
		SecurityTransferOperation securityTransferOperation = null;
		CustodyOperation custodyOperation = null;
		Participant participant = null;
		Security security = null;
		//HolderAccount holderAccount = null;

		for (int i = 0; i < query.getResultList().size(); i++) {
			
			Object obj[] = (Object[])query.getResultList().get(i);
			securityTransferOperation = new SecurityTransferOperation();
			
			if(Validations.validateIsNotNull(obj[0])){
				securityTransferOperation.setIdTransferOperationPk(new Long((obj[0]).toString()));
			}
			
			if(Validations.validateIsNotNull(obj[1])){
				securityTransferOperation.setTransferType(Integer.parseInt((obj[1]).toString()));
			}
			
			participant = new Participant();
			
			if(Validations.validateIsNotNull(obj[2])){
				participant.setIdParticipantPk(new Long((obj[2]).toString()));
			}

			if(Validations.validateIsNotNull(obj[2])){
				Participant srcParticipant = new Participant();
				srcParticipant.setIdParticipantPk(new Long((obj[2]).toString()));
				securityTransferOperation.setSourceParticipant(srcParticipant);
			}
			
			if(Validations.validateIsNotNull(obj[3])){
				Participant tgtParticipant = new Participant();
				tgtParticipant.setIdParticipantPk(new Long((obj[3]).toString()));
				securityTransferOperation.setTargetParticipant(tgtParticipant);
			}
			
			if(Validations.validateIsNotNull(obj[4])){
				HolderAccount srcHolderAccount = new HolderAccount();
				srcHolderAccount.setIdHolderAccountPk(new Long((obj[4]).toString()));
				securityTransferOperation.setSourceHolderAccount(srcHolderAccount);
			}
			
			if(Validations.validateIsNotNull(obj[5])){
				HolderAccount tgtHolderAccount = new HolderAccount();
				tgtHolderAccount.setIdHolderAccountPk(new Long((obj[5]).toString()));
				securityTransferOperation.setTargetHolderAccount(tgtHolderAccount);
			}
	
			if(Validations.validateIsNotNull(obj[6])){
				securityTransferOperation.setQuantityOperation((BigDecimal)obj[6]);
			}
			security = new Security();
			
			if(Validations.validateIsNotNull(obj[7])){
				security.setIdIsinCode((String)obj[7]);
			}
			
			securityTransferOperation.setSecurities(security);
			security = new Security();

			if(Validations.validateIsNotNull(obj[8])){
				securityTransferOperation.setState(Integer.parseInt((obj[8]).toString()));
			}
			
			custodyOperation = new CustodyOperation();
			
			custodyOperation.setIdCustodyOperationPk(securityTransferOperation.getIdTransferOperationPk());
			
			if(Validations.validateIsNotNull(obj[9])){
				custodyOperation.setOperationType(new Long((obj[9]).toString()));
			}

			if(Validations.validateIsNotNull(obj[10])){
				custodyOperation.setOperationNumber(new Long((obj[10]).toString()));
			}
			
			if(Validations.validateIsNotNull(obj[11])){
				custodyOperation.setOperationDate(new Date(((Timestamp)obj[11]).getTime()));
			}

			securityTransferOperation.setCustodyOperation(custodyOperation);
			
			lstSecurityTransferOperation.add(securityTransferOperation);
		}
		
		return lstSecurityTransferOperation;
	}
	
	
	/**
	 * Validate request state.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @param stateNew the state new
	 * @throws ServiceException the service exception
	 */
	public void validateRequestState(
			SecurityTransferOperation securityTransferOperation, Integer stateNew) throws ServiceException {
		
		//Ejecuto el batch para los cambios de estado
		if(stateNew.equals(TransferSecuritiesStateType.APROBADO.getCode()) ||
		   stateNew.equals(TransferSecuritiesStateType.CONFIRMADO.getCode()) ||
		   stateNew.equals(TransferSecuritiesStateType.REGISTRADO.getCode()) ||
		   stateNew.equals(TransferSecuritiesStateType.ANULADO.getCode()) ||
		   stateNew.equals(TransferSecuritiesStateType.REVISADO.getCode()) ||
		   stateNew.equals(TransferSecuritiesStateType.RECHAZADO.getCode())
		){
			Integer stateSecurityTransferOperation = securityTransferOperation.getState();
			
			if(stateNew.equals(TransferSecuritiesStateType.APROBADO.getCode()) || stateNew.equals(TransferSecuritiesStateType.ANULADO.getCode())) {
				if(!stateSecurityTransferOperation.equals(TransferSecuritiesStateType.REGISTRADO.getCode())){
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				}
			} else if(stateNew.equals(TransferSecuritiesStateType.REVISADO.getCode())) {
				if(!stateSecurityTransferOperation.equals(TransferSecuritiesStateType.APROBADO.getCode())){
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				}
			} else if(stateNew.equals(TransferSecuritiesStateType.CONFIRMADO.getCode())) {
				if(!stateSecurityTransferOperation.equals(TransferSecuritiesStateType.REVISADO.getCode())){
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				}
			} else if(stateNew.equals(TransferSecuritiesStateType.RECHAZADO.getCode())) {
				if(!(stateSecurityTransferOperation.equals(TransferSecuritiesStateType.APROBADO.getCode())
						|| stateSecurityTransferOperation.equals(TransferSecuritiesStateType.REVISADO.getCode()))){
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				}
			}
			
		}
	}

	/**
	 * Validate request objects.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public void validateRequestObjects(SecurityTransferOperation securityTransferOperation) throws ServiceException {
			
			Map<String,Object> param = new HashMap<String,Object>();
			
			//Validamos cuenta origen
			if(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount())
			&& Validations.validateIsNotNullAndPositive((securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()))){
				param.put("idHolderAccountPkParam", securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
				HolderAccount srcHolderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
				if(!srcHolderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK_PARAM,
							new Object[] {srcHolderAccount.getAccountNumber().toString()});
				}
				
				//detalle de cuentas origen
			/*	for(HolderAccountDetail holderAccountDetail: securityTransferOperation.getSourceHolderAccount().getHolderAccountDetails()){
					param = new HashMap<String,Object>();
					param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
					Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
					if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_BLOCK_PARAM,
								new Object[] {holder.getIdHolderPk().toString()});
					}
				}*/
			}
			
			param = new HashMap<String,Object>();
			
			//Validamos cuenta destino
			if(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) 
			&& Validations.validateIsNotNullAndPositive((securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
				param.put("idHolderAccountPkParam", securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk());
				HolderAccount tgtHolderAccount = findObjectByNamedQuery(HolderAccount.HOLDER_ACCOUNT_STATE, param);
				if(!tgtHolderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())){
					throw new ServiceException(ErrorServiceType.HOLDER_ACCOUNT_BLOCK_PARAM,
							new Object[] {tgtHolderAccount.getAccountNumber().toString()});
				}
				
			/*	for(HolderAccountDetail holderAccountDetail: securityTransferOperation.getTargetHolderAccount().getHolderAccountDetails()){
					param = new HashMap<String,Object>();
					param.put("idHolderPkParam", holderAccountDetail.getHolder().getIdHolderPk());
					Holder holder = findObjectByNamedQuery(Holder.HOLDER_STATE, param);
					if(!holder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
						throw new ServiceException(ErrorServiceType.HOLDER_BLOCK_PARAM,
								new Object[] {holder.getIdHolderPk().toString()});
					}
				}*/
			}
			
			param = new HashMap<String,Object>();
			param.put("idParticipantPkParam", securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			Participant srcParticipant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
			if(!srcParticipant.getState().equals(ParticipantStateType.REGISTERED.getCode()) && 
					!srcParticipant.getState().equals(ParticipantStateType.BLOCKED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK_PARAM,
						new Object[]{srcParticipant.getDisplayCodeMnemonic()});
			}
			
			param.put("idParticipantPkParam", securityTransferOperation.getTargetParticipant().getIdParticipantPk());
			Participant tgtParticipant = findObjectByNamedQuery(Participant.PARTICIPANT_STATE, param);
			if(!tgtParticipant.getState().equals(ParticipantStateType.REGISTERED.getCode()) && 
					!tgtParticipant.getState().equals(ParticipantStateType.BLOCKED.getCode())){
				throw new ServiceException(ErrorServiceType.PARTICIPANT_BLOCK_PARAM,
						tgtParticipant.getDisplayCodeMnemonic());
			}

	}

	/**
	 * Reblock in transfer avalaible.
	 *
	 * @param idTransferAvailablePk the id transfer available pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Long> reblockInTransferAvalaible(Long idTransferAvailablePk) throws ServiceException{
		List<Integer> blockStateNotIn = new ArrayList<Integer>();
		blockStateNotIn.add(AffectationStateType.ANNULLED.getCode());
		blockStateNotIn.add(AffectationStateType.UNBLOCKED.getCode());
		
		Integer blockLevel = LevelAffectationType.REBLOCK.getCode();
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select BoCo.operationNumber From BlockOperation bo INNER JOIN bo.custodyOperation BoCo, "
					 		+ " SecurityTransferOperation sto INNER JOIN sto.custodyOperation StoCo  ");
		sbQuery.append(" Where  ");
		sbQuery.append("     sto.securities.idSecurityCodePk = bo.securities.idSecurityCodePk ");
		sbQuery.append(" and sto.sourceHolderAccount.idHolderAccountPk = bo.holderAccount.idHolderAccountPk ");
		sbQuery.append(" and sto.sourceParticipant.idParticipantPk = bo.participant.idParticipantPk ");
		sbQuery.append(" and TRUNC(StoCo.registryDate) <= TRUNC(BoCo.registryDate) ");
		sbQuery.append(" and StoCo.idCustodyOperationPk = :idTransferAvailablePkPrm ");
		sbQuery.append(" and bo.blockState not in (:blockStateNotInPrm) ");
		sbQuery.append(" and bo.blockLevel = :blockLevelPrm ");

		TypedQuery<Long> query = em.createQuery(sbQuery.toString(),Long.class); 
		
		query.setParameter("idTransferAvailablePkPrm", idTransferAvailablePk);
		query.setParameter("blockStateNotInPrm", blockStateNotIn);
		query.setParameter("blockLevelPrm", blockLevel);
		
		return query.getResultList();
		
	}
}
