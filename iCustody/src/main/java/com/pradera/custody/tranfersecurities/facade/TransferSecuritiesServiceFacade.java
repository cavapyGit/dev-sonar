/*
 * 
 */
package com.pradera.custody.tranfersecurities.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.service.SecuritiesQueryServiceBean;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.core.framework.notification.service.NotificationServiceBean;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.affectation.to.AffectationTransferTO;
import com.pradera.custody.affectation.to.MarketFactTO;
import com.pradera.custody.reversals.service.RequestReversalsServiceBean;
import com.pradera.custody.reversals.to.BlockOpeForReversalsTO;
import com.pradera.custody.reversals.to.RegisterReversalTO;
import com.pradera.custody.reversals.to.RequestReversalsTO;
import com.pradera.custody.reversals.to.ReversalsMarkFactTO;
import com.pradera.custody.tranfersecurities.accounts.service.BlockSercuiritiesServiceBean;
import com.pradera.custody.tranfersecurities.accounts.service.CustodyOperationServiceBean;
import com.pradera.custody.tranfersecurities.accounts.service.HolderAccountServiceBean;
import com.pradera.custody.tranfersecurities.service.TransferAvailableServiceBean;
import com.pradera.custody.tranfersecurities.to.SecurityBlockTransferTO;
import com.pradera.custody.tranfersecurities.to.SecurityTransferOperationTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.component.BlockMarketFactOperation;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.reversals.type.MotiveReverlsasType;
import com.pradera.model.custody.reversals.type.RequestReversalsStateType;
import com.pradera.model.custody.transfersecurities.BlockOperationTransfer;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferMarketfact;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.SecuritiesTransferMotiveType;
import com.pradera.model.custody.type.SecuritiesTransferType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.operations.RequestCorrelativeType;
// TODO: Auto-generated Javadoc
//import com.pradera.model.component.type.OperationTypeType;

//TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransferSecuritiesServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class TransferSecuritiesServiceFacade {

	/** The notification service bean. */
	@EJB 
	NotificationServiceBean notificationServiceBean;
	
	/** The block sercuirities service bean. */
	@EJB
	BlockSercuiritiesServiceBean blockSercuiritiesServiceBean;
	
	/** The transfer available service bean. */
	@EJB
	TransferAvailableServiceBean transferAvailableServiceBean;
	
	/** The holder account service bean. */
	@EJB
	HolderAccountServiceBean holderAccountServiceBean;
	
	/** The custody operation service bean. */
	@EJB
	CustodyOperationServiceBean custodyOperationServiceBean;
	
    /** The accounts component service. */
    @Inject
    Instance<AccountsComponentService> accountsComponentService;

	
	/** The securities query service bean. */
	@EJB
	SecuritiesQueryServiceBean securitiesQueryServiceBean;
	
	/** The affectations service bean. */
	@EJB
	AffectationsServiceBean affectationsServiceBean;
	
	/** The request affectation service bean. */
	@EJB
	RequestAffectationServiceBean requestAffectationServiceBean;
	
	/** The request reversals service bean. */
	@EJB
	RequestReversalsServiceBean requestReversalsServiceBean;
	
	/** The crud dao service bean. */
	@EJB
	CrudDaoServiceBean crudDaoServiceBean;

	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/**
	 * Gets the list holder account balance.
	 *
	 * @param filter the filter
	 * @param chechOnliAvailable the chech onli available
	 * @param securityFilter the security filter
	 * @return the list holder account balance
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalance> getListHolderAccountBalance(HolderAccountBalance filter, boolean chechOnliAvailable, Security securityFilter, UserInfo userInfo) throws ServiceException{
		return transferAvailableServiceBean.getListHolderAccountBalance(filter, chechOnliAvailable, securityFilter, userInfo);
	}

	
	/**
	 * Gets the exist security for holder.
	 *
	 * @param filter the filter
	 * @return the exist security for holder
	 * @throws ServiceException the service exception
	 */
	public Integer getExistSecurityForHolder(HolderAccountBalance filter) throws ServiceException{
		return transferAvailableServiceBean.getExistSecurityForHolder(filter);
	}

	/**
	 * Gets the list market fact balance in transfer operation.
	 *
	 * @param idSecurityTransferOperationPk the id security transfer operation pk
	 * @param todate the todate
	 * @return the list market fact balance in transfer operation
	 * @throws ServiceException the service exception
	 */
	/*Obtiene los hechos del mercado a partir de una solicitud, ojo, 1 solicitud de traspaso, es para 1 solo valor*/
	public List<MarketFactDetailHelpTO> getListMarketFactBalanceInTransferOperation(Long idSecurityTransferOperationPk, boolean todate) throws ServiceException{
		return transferAvailableServiceBean.getListMarketFactBalanceInTransferOperation(idSecurityTransferOperationPk,todate);
	}
	
	/**
	 * Gets the list market fact balance in transfer operation.
	 *
	 * @param hab the hab
	 * @return the list market fact balance in transfer operation
	 * @throws ServiceException the service exception
	 */
	public List<MarketFactDetailHelpTO> getListMarketFactBalanceInTransferOperation(HolderAccountBalance hab) throws ServiceException{
		return transferAvailableServiceBean.getListMarketFactBalanceInTransferOperation(hab);
	}
	
	/**
	 * Gets the accounts with cuis and participant.
	 *
	 * @param holderAccount the holder account
	 * @param participantPk the participant pk
	 * @return the accounts with cuis and participant
	 * @throws ServiceException the service exception
	 */
	public List<Long> getAccountsWithCuisAndParticipant(HolderAccount holderAccount, Long participantPk) throws ServiceException{
		List<Long> lstCuisWithAccount = new ArrayList<Long>();
		List<Long> lstAccountsWithCuis = new ArrayList<Long>();
		
		lstCuisWithAccount = getCuisWithAccount(holderAccount);
		if(lstCuisWithAccount.size()>0){
			lstAccountsWithCuis = transferAvailableServiceBean.getAccountPkWithCuis(lstCuisWithAccount,participantPk,holderAccount);
		}
		
		return lstAccountsWithCuis;
	}
	
	/**
	 * Gets the cuis with account.
	 *
	 * @param holderAccount the holder account
	 * @return the cuis with account
	 * @throws ServiceException the service exception
	 */
	public List<Long> getCuisWithAccount(HolderAccount holderAccount) throws ServiceException{
		return transferAvailableServiceBean.getCuisWithAccount(holderAccount);
	}
	
	/**
	 * Gets the security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the security
	 * @throws ServiceException the service exception
	 */
	public Security getSecurity(String idSecurityCodePk) throws ServiceException{
		return transferAvailableServiceBean.getSecurity(idSecurityCodePk);
	}

	/**
	 * Gets the coupon count with security.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @return the coupon count with security
	 * @throws ServiceException the service exception
	 */
	public Integer getCouponCountWithSecurity(String idSecurityCodePk) throws ServiceException{
		return transferAvailableServiceBean.getCouponCountWithSecurity(idSecurityCodePk);
	}
	
	/**
	 * Valid security with instrument type.
	 *
	 * @param idSecurityCodePk the id security code pk
	 * @param securityClass the security class
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer validSecurityWithSecurityClass(String idSecurityCodePk,Integer securityClass) throws ServiceException{
		return transferAvailableServiceBean.validSecurityWithSecurityClass(idSecurityCodePk, securityClass).intValue();
	}
	

	/**
	 * Find security transfer marketfact.
	 *
	 * @param idSecTansferMarketfactPk the id sec tansfer marketfact pk
	 * @param idTransferOperationPk the id transfer operation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityTransferMarketfact> findSecurityTransferMarketfact(Long idSecTansferMarketfactPk, Long idTransferOperationPk)  throws ServiceException{
		return transferAvailableServiceBean.findSecurityTransferMarketfact(idSecTansferMarketfactPk, idTransferOperationPk);
	}
	
	/**
	 * Gets the custody operation.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation(Long idCustodyOperation){
		return transferAvailableServiceBean.find(CustodyOperation.class, idCustodyOperation);
	}
	
	/**
	 * Gets the lis holder account service bean.
	 *
	 * @param filter the filter
	 * @return the lis holder account service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getLisHolderAccountServiceBean(HolderAccount filter) throws ServiceException{
		return holderAccountServiceBean.getLisHolderAccountServiceBean(filter);
	}
	
	/**
	 * Gets the holder account active service bean.
	 *
	 * @param holderAccount the holder account
	 * @param participant the participant
	 * @return the holder account active service bean
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccountActiveServiceBean(HolderAccount holderAccount,Participant participant) throws ServiceException{
		holderAccount.setParticipant(participant);
		holderAccount.setStateAccount(HolderAccountStatusType.ACTIVE.getCode());
		if(Validations.validateIsNull(participant) || Validations.validateIsNull(holderAccount.getAccountNumber())){	//account and participant 
			return holderAccount;
		}else{
			holderAccount = (getLisHolderAccountServiceBean(holderAccount).size()>0)?getLisHolderAccountServiceBean(holderAccount).get(0):new HolderAccount();
		}
		return holderAccount;
	}
	
	/**
	 * Gets the lis holder account detail service bean.
	 *
	 * @param filter the filter
	 * @return the lis holder account detail service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountDetail> getLisHolderAccountDetailServiceBean(HolderAccountDetail filter) throws ServiceException{
		return holderAccountServiceBean.getLisHolderAccountDetailServiceBean(filter);
	}
	
	/**
	 * Reblock in transfer avalaible.
	 *
	 * @param idTransferAvailablePk the id transfer available pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	/*De existir un rebloque, este devuelve el nro de la operacion de rebloqueo*/
	public List<Long> reblockInTransferAvalaible(Long idTransferAvailablePk) throws ServiceException{
		return transferAvailableServiceBean.reblockInTransferAvalaible(idTransferAvailablePk);
	}
	
	/**
	 * Send message.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	/*public void sendMessage(String userName,
			BusinessProcess businessProcess,
			InstitutionType institution,
			Object institutionCode,
			UserAccountResponsabilityType responsability){
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		//notificationServiceBean.fireMessageNotification(loggerUser,userName,businessProcess,institution,institutionCode,responsability);
	}*/
	
	
	/**
	 * Equals rnt with holder account.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean equalsRntWithHolderAccount(SecurityTransferOperation securityTransferOperation) throws ServiceException{
		Long sourceIdHolderAccountPk;
		if( Validations.validateIsNotNull(securityTransferOperation) &&
			Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) &&
			Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())){
			sourceIdHolderAccountPk=securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk();
		}else{
			return false;
		}

		Long targetIdHolderAccountPk;
		if(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) &&
		   Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())){
			targetIdHolderAccountPk=securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk();
		}else{
			return false;
		}
		
		return holderAccountServiceBean.equalsRntWithHolderAccount(sourceIdHolderAccountPk, targetIdHolderAccountPk);
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REGISTER)
	/*public String registrySecurityTransferServiceBeanAvailable(SecurityTransferOperation 
			objSecurityTransferOperation, Long businessProcessId, Long operationType,
			List<SecurityBlockTransferTO> lstSecurityBlockTransferTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		return registrySecurityTransferServiceBean(objSecurityTransferOperation,  businessProcessId,  operationType,lstSecurityBlockTransferTO);
	}*/
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REGISTER)
	/**
	 * Registry list security transfer service bean available.
	 *
	 * @param ListobjSecurityTransferOperation the listobj security transfer operation
	 * @param businessProcessId the business process id
	 * @param operationType the operation type
	 * @param lstSecurityBlockTransferTO the lst security block transfer to
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String registryListSecurityTransferServiceBeanAvailable(List<SecurityTransferOperation> ListobjSecurityTransferOperation, Long businessProcessId, Long operationType,
																   List<SecurityBlockTransferTO> lstSecurityBlockTransferTO, boolean flagIndMarketFact/*,
																   Map<Integer, List<SecurityTransferMarketfact>> mapSecurityTransferMarketfact*/) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		String pk = "";
		String msgReturn ="";
		List<String> lstPk= new ArrayList<String>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		for(SecurityTransferOperation ojSecTransf : ListobjSecurityTransferOperation){
			params.put("idSecurityCodePkParam", ojSecTransf.getSecurities().getIdSecurityCodePk());
			Security securityState = crudDaoServiceBean.findObjectByNamedQuery(Security.SECURITY_STATE, params);
			if(!(securityState.isRegisteredState() || securityState.isGuardaExclusive())){
				throw new ServiceException(ErrorServiceType.SECURITY_CODE_BLOCK,new Object[]{securityState.getIdSecurityCodePk()});
			}
		}
		for(SecurityTransferOperation securityTO : ListobjSecurityTransferOperation){
			SecurityTransferOperation secTo = new SecurityTransferOperation();
			secTo = securityTO;
			pk = registrySecurityTransferServiceBean(secTo,  businessProcessId,  operationType,lstSecurityBlockTransferTO, flagIndMarketFact/*, mapSecurityTransferMarketfact*/);
			lstPk.add(pk);
		}
        
        for (int i = 0; i < lstPk.size(); i++) {	
        	msgReturn += (i > 0) ? " ,"+lstPk.get(i): " "+lstPk.get(i);
		}
        return msgReturn;
	}

	//@ProcessAuditLogger(process=BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REGISTER)
	/**
	 * Registry security transfer service bean block.
	 *
	 * @param objSecurityTransferOperation the obj security transfer operation
	 * @param businessProcessId the business process id
	 * @param operationType the operation type
	 * @param lstSecurityBlockTransferTO the lst security block transfer to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String registrySecurityTransferServiceBeanBlock(SecurityTransferOperation 
			objSecurityTransferOperation, Long businessProcessId, Long operationType,
			List<SecurityBlockTransferTO> lstSecurityBlockTransferTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
		return registrySecurityTransferServiceBean(objSecurityTransferOperation,  businessProcessId,  operationType,lstSecurityBlockTransferTO,false/*,null*/);
	}
	
	
	/**
	 * Validate objects transfer.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public void validateObjectsTransfer(SecurityTransferOperation securityTransferOperation) throws ServiceException {
		SecurityTransferOperation stoValidations = new SecurityTransferOperation();
		
		if( Validations.validateIsNotNull(securityTransferOperation.getSourceParticipant()) &&
			Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk()) ){
				Participant sourceParticipant = new Participant();
				sourceParticipant.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
				stoValidations.setSourceParticipant(sourceParticipant);
		}
		
		if( Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) &&
			Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()) ){
			HolderAccount holderAccountSource = new HolderAccount();
			holderAccountSource.setIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
			stoValidations.setSourceHolderAccount(holderAccountSource);
		}

		if( Validations.validateIsNotNull(securityTransferOperation.getTargetParticipant()) &&
			Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk()) ){
			Participant participantTarget = new Participant();
			participantTarget.setIdParticipantPk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
			stoValidations.setTargetParticipant(participantTarget);
		}

		if( Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) &&
			Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()) ){
			HolderAccount holderAccountTarget = new HolderAccount();
			holderAccountTarget.setIdHolderAccountPk(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk());
			stoValidations.setTargetHolderAccount(holderAccountTarget);
		}
		
		transferAvailableServiceBean.validateRequestObjects(stoValidations);
	}
	
	/**
	 * Registry security transfer service bean.
	 *
	 * @param objSecurityTransferOperation the obj security transfer operation
	 * @param businessProcessId the business process id
	 * @param operationType the operation type
	 * @param lstSecurityBlockTransferTO the lst security block transfer to
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String registrySecurityTransferServiceBean(SecurityTransferOperation objSecurityTransferOperation, Long businessProcessId, Long operationType,
													  List<SecurityBlockTransferTO> lstSecurityBlockTransferTO, boolean flagIndMarketFact) throws ServiceException{

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
        
        //LISTA DE HECHOS DEL MERCADO
        List<HolderMarketFactBalance> lstHolderMarketfactBalance = null;
        if( Validations.validateIsNotNull(objSecurityTransferOperation.getLstHolderMarketfactBalance()) && 
        	objSecurityTransferOperation.getLstHolderMarketfactBalance().size()>0 ){
        	lstHolderMarketfactBalance = objSecurityTransferOperation.getLstHolderMarketfactBalance();
        }
        
		SecurityTransferOperation securityTransferOperation = new SecurityTransferOperation();
		securityTransferOperation.setSourceParticipant(objSecurityTransferOperation.getSourceParticipant());
		securityTransferOperation.setSourceHolderAccount(objSecurityTransferOperation.getSourceHolderAccount());
		securityTransferOperation.setTargetParticipant(objSecurityTransferOperation.getTargetParticipant());
		securityTransferOperation.setTargetHolderAccount(objSecurityTransferOperation.getTargetHolderAccount());
		securityTransferOperation.setQuantityOperation(objSecurityTransferOperation.getQuantityOperation());
		securityTransferOperation.setTransferType(objSecurityTransferOperation.getTransferType());
		securityTransferOperation.setSecurities(objSecurityTransferOperation.getSecurities());
		securityTransferOperation.setIndBlockTransfer(objSecurityTransferOperation.getIndBlockTransfer());
		
		
		// validamos objetos: titulares, cuentas, participante.
		validateObjectsTransfer(securityTransferOperation);
		
		if(Validations.validateIsNotNull(objSecurityTransferOperation.getSourceHolderAccount())){
			affectationsServiceBean.validateCurrentReblocks(securityTransferOperation.getSourceParticipant().getIdParticipantPk(),
					securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk(),
					securityTransferOperation.getSecurities().getIdSecurityCodePk(),
					objSecurityTransferOperation.getQuantityOperation());
		}
		if(Validations.validateIsNotNull(objSecurityTransferOperation.getTargetHolderAccount())){
			affectationsServiceBean.validateCurrentReblocks(securityTransferOperation.getTargetParticipant().getIdParticipantPk(),
					securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk(),
					securityTransferOperation.getSecurities().getIdSecurityCodePk(),
					objSecurityTransferOperation.getQuantityOperation());
		}
	    
		CustodyOperation objCustodyOperation = new CustodyOperation();

		objCustodyOperation.setOperationDate(CommonsUtilities.currentDate());
		objCustodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
		objCustodyOperation.setIndRectification(ComponentConstant.ZERO);
		objCustodyOperation.setOperationType(operationType);
		
		//correlativo
		Long operationNumber = crudDaoServiceBean.getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_OPERATION_TRANSFER_SECURITIES_AVAILABLE);
		objCustodyOperation.setOperationNumber(operationNumber);
		
		// mismo participante, directamente a revisado.
		if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
			objCustodyOperation.setApprovalDate(CommonsUtilities.currentDateTime());
			objCustodyOperation.setApprovalUser(loggerUser.getUserName());
			objCustodyOperation.setReviewDate(CommonsUtilities.currentDateTime());
			objCustodyOperation.setReviewUser(loggerUser.getUserName());
			objCustodyOperation.setRegistryUser(loggerUser.getUserName());
			objCustodyOperation.setState(TransferSecuritiesStateType.REVISADO.getCode());
			securityTransferOperation.setState(TransferSecuritiesStateType.REVISADO.getCode());
		}else{
			objCustodyOperation.setRegistryUser(loggerUser.getUserName());
			objCustodyOperation.setState(TransferSecuritiesStateType.REGISTRADO.getCode());
			securityTransferOperation.setState(TransferSecuritiesStateType.REGISTRADO.getCode());
		}
		securityTransferOperation.setTransferType(securityTransferOperation.getTransferType());
		objCustodyOperation.setAudit(loggerUser);
		securityTransferOperation.setAudit(loggerUser);
		securityTransferOperation.setCustodyOperation(objCustodyOperation);
		securityTransferOperation.setCorrelativeOperation(objSecurityTransferOperation.getCorrelativeOperation());
		transferAvailableServiceBean.create(securityTransferOperation);
		
		Security security = objSecurityTransferOperation.getSecurities();
		List<MarketFactAccountTO> lstMarketFactAccountTO = null;
		if(!Validations.validateIsNotNullAndPositive(security.getInstrumentType())){
			security = transferAvailableServiceBean.getSecurity(objSecurityTransferOperation.getSecurities().getIdSecurityCodePk());
		}
		
		//SI USA LOS HECHOS DEL MERCADO. GRABAR TAMBIEN EN LA TABLA SecurityTransferMarketfact
		if( flagIndMarketFact && !securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) && 
			Validations.validateIsNotNull(lstHolderMarketfactBalance) && lstHolderMarketfactBalance.size()>0){
			
			lstMarketFactAccountTO = new ArrayList<MarketFactAccountTO>();
			
			for(HolderMarketFactBalance holderMarketFactBalance :lstHolderMarketfactBalance){
				
				//DATOS PARA LOS HECHOS DEL MERCADO
				MarketFactAccountTO objMarketFact = new MarketFactAccountTO();
				objMarketFact.setMarketDate(holderMarketFactBalance.getMarketDate());
				objMarketFact.setQuantity(holderMarketFactBalance.getTransferAmmount());
				//if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
					objMarketFact.setMarketRate(holderMarketFactBalance.getMarketRate());	
				//}else{
					objMarketFact.setMarketPrice(holderMarketFactBalance.getMarketPrice());	
				//}
				lstMarketFactAccountTO.add(objMarketFact);
				
				
				//DATOS PARA CREAR MI SecurityTransferMarketfact
				SecurityTransferMarketfact securityTransferMarketfact = new SecurityTransferMarketfact();
				securityTransferMarketfact.setMarketDate(holderMarketFactBalance.getMarketDate());
				securityTransferMarketfact.setQuantityOperation(holderMarketFactBalance.getTransferAmmount());
				//if(security.getInstrumentType().equals(InstrumentType.FIXED_INCOME.getCode())){
					securityTransferMarketfact.setMarketRate(holderMarketFactBalance.getMarketRate());	
				//}else{
					securityTransferMarketfact.setMarketPrice(holderMarketFactBalance.getMarketPrice());	
				//}
				securityTransferMarketfact.setSecurityTransferOperation(securityTransferOperation);
				securityTransferMarketfact.setAudit(loggerUser);
				transferAvailableServiceBean.create(securityTransferMarketfact);
			}
			
		}


		if(businessProcessId.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REGISTER.getCode())){
			
			if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) || 
			   objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				executeComponent(securityTransferOperation, businessProcessId, null,ComponentConstant.SOURCE,null,flagIndMarketFact,lstMarketFactAccountTO);
			}
		}
		
		// si es bloqueados, guardamos la lista de bloqueos, no se guarda aca los hechos del mercado
		if(businessProcessId.equals(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REGISTER.getCode())){
			SecurityBlockTransferTO objSecurityBlockTransferTO=null;
			for(int i=0;i<lstSecurityBlockTransferTO.size();i++){
				
				objSecurityBlockTransferTO = (SecurityBlockTransferTO)lstSecurityBlockTransferTO.get(i);
				Boolean existTransfer = existSecurityTransferBlock(objSecurityBlockTransferTO.getBlockType(),
						securityTransferOperation.getSecurities().getIdSecurityCodePk(),objSecurityBlockTransferTO.getDocumentNumber());
				if(existTransfer){
					throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
				}
				
				//we verify if the block operation exists at any custody request
				requestAffectationServiceBean.verifyBlockOperationAtOtherRequests(lstSecurityBlockTransferTO.get(i).getIdBlockOperationPk(), 
						lstSecurityBlockTransferTO.get(i).getDocumentNumber().toString());
				
				BlockOperation blockOperation = new BlockOperation();
				
				blockOperation.setIdBlockOperationPk(lstSecurityBlockTransferTO.get(i).getIdBlockOperationPk());
				
				BlockOperationTransfer blockOperationTransfer = new BlockOperationTransfer();
				blockOperationTransfer.setSecurityTransferOperation(securityTransferOperation);
				blockOperationTransfer.setBlockOperation(blockOperation);
				blockOperationTransfer.setOriginalBlockBalance(lstSecurityBlockTransferTO.get(i).getActualBlockBalance());
				
				blockOperationTransfer = transferAvailableServiceBean.create(blockOperationTransfer);
			}
		}
		
		//Retorno el pk
		if(Validations.validateIsNotNullAndPositive(securityTransferOperation.getIdTransferOperationPk()))
			return objCustodyOperation.getOperationNumber().toString();
		else
			return "";
	}
	
	/**
	 * Execute confirm block.
	 *
	 * @param securityBlockTo the security block to
	 * @param lstNewSecurityBlockTransferTO the lst new security block transfer to
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws Exception the exception
	 */
	public String executeConfirmBlock(SecurityBlockTransferTO securityBlockTo,
			List<SecurityBlockTransferTO> lstNewSecurityBlockTransferTO,
			SecurityTransferOperation tmpTransferOperation, boolean flagIndMarketFact) throws Exception {

		String result = "";	
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		
		tmpTransferOperation = transferAvailableServiceBean.find(SecurityTransferOperation.class, tmpTransferOperation.getIdTransferOperationPk());	
		
		// ejecutamos el cambio de estado, incluyendo validaciones, etc etc.
		result = executeChangeState(securityBlockTo.getStateOld(), securityBlockTo.getStateNew(),
				securityBlockTo.getBussinesProcess(), tmpTransferOperation,true,null);
		
		/***********************  EJECUTAR CUANDO ESTE FUNCIONANDO EL COMPONENTE DE MOVIMIENTOS  *************************************/
		// ejecutamos el desbloqueo de valores en origen, traspaso de balance, y bloqueo de valores en destino
		executeConfirmActions(tmpTransferOperation, securityBlockTo, lstNewSecurityBlockTransferTO, loggerUser, flagIndMarketFact);
		//ver si hay q comentar algo o no
		/*********************** END  *************************************/
		return result;
	}
	
	//TRANSFER BLOCK
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE)
	/**
	 * Execute change state aprove block.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String executeChangeStateAproveBlock(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		return executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact,null) ;
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL)
	/**
	 * Execute change state cancel block.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String executeChangeStateCancelBlock(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation, boolean flagIndMarketFact) throws ServiceException{

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
		return executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation, flagIndMarketFact,null) ;
	}

	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW)
	/**
	 * Execute change state review block.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String executeChangeStateReviewBlock(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
		return executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact,null) ;
	}

	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT)
	/**
	 * Execute change state reject block.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String executeChangeStateRejectBlock(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation, boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		return executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation, flagIndMarketFact, null) ;
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM)
	/**
	 * Execute change state confirm block.
	 *
	 * @param securityBlockTo the security block to
	 * @param lstNewSecurityBlockTransferTO the lst new security block transfer to
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @return the string
	 * @throws Exception the exception
	 */
	public String executeChangeStateConfirmBlock(SecurityBlockTransferTO securityBlockTo,
			List<SecurityBlockTransferTO> lstNewSecurityBlockTransferTO,
			SecurityTransferOperation tmpTransferOperation, boolean flagIndMarketFact) throws Exception{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		return executeConfirmBlock( securityBlockTo, lstNewSecurityBlockTransferTO, tmpTransferOperation, flagIndMarketFact);
	}
	

	//TRANSFER AVAILABLE
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE)
	/**
	 * Execute change state aprove available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateAproveAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation, flagIndMarketFact,null) ;
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE)
	/**
	 * Execute change state all aprove available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateAllAproveAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation[] tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		for (int i = 0; i < tmpTransferOperation.length; i++) {
			executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation[i],flagIndMarketFact,null) ;
		}
		
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL)
	/**
	 * Execute change state cancel available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateCancelAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation, boolean flagIndMarketFact) throws ServiceException{

		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
		executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact,null) ;
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL)
		/**
	 * Execute change state all cancel available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateAllCancelAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
				   SecurityTransferOperation[] tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{

			LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeCancel());
			for (int i = 0; i < tmpTransferOperation.length; i++) {
				executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation[i],flagIndMarketFact,null) ;
			}
		}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW)
	/**
	 * Execute change state review available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateReviewAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation,
			   boolean flagIndMarketFact/*,Map<Integer, List<SecurityTransferMarketfact>> mapSecurityTransferMarketfact*/) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
		
		/*Security security = tmpTransferOperation.getSecurities();
		if(Validations.validateIsNotNullAndPositive(security.getInstrumentType())){
			security = transferAvailableServiceBean.getSecurity(tmpTransferOperation.getSecurities().getIdSecurityCodePk());
		}*/
		
		//SI ES UNA REVISION EN UNA SOLICITUD DESTINO ORIGEN
		//SI USA LOS HECHOS DEL MERCADO. GRABAR TAMBIEN EN LA TABLA SecurityTransferMarketfact
		List<MarketFactAccountTO> lstMarketFactAccountTO = new ArrayList<MarketFactAccountTO>();
		List<SecurityTransferMarketfact> lstSecurityTransferMarketfact = new ArrayList<>();
		
		if(flagIndMarketFact && tmpTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
		
			List<HolderMarketFactBalance> lstHolderMarketFactBalance = tmpTransferOperation.getLstHolderMarketfactBalance();
			
			for(HolderMarketFactBalance obj : lstHolderMarketFactBalance){
				
				//voy a grabar mi SecurityTransferMarketfact, ya que es una revision destino origen
				SecurityTransferMarketfact securityTransferMarketfact = new SecurityTransferMarketfact();
				securityTransferMarketfact.setMarketDate(obj.getMarketDate());
				securityTransferMarketfact.setMarketPrice(obj.getMarketPrice());
				securityTransferMarketfact.setMarketRate(obj.getMarketRate());
				securityTransferMarketfact.setQuantityOperation(obj.getTransferAmmount());
				securityTransferMarketfact.setSecurityTransferOperation(tmpTransferOperation);
				securityTransferMarketfact.setAudit(loggerUser);
				lstSecurityTransferMarketfact.add(securityTransferMarketfact);
				
				//LLEVO MI LISTA AL OTRO LADO YA LLENA
				MarketFactAccountTO objMarketFact = new MarketFactAccountTO();
				objMarketFact.setMarketDate(securityTransferMarketfact.getMarketDate());
				objMarketFact.setQuantity(securityTransferMarketfact.getQuantityOperation());
				objMarketFact.setMarketRate(securityTransferMarketfact.getMarketRate());	
				objMarketFact.setMarketPrice(securityTransferMarketfact.getMarketPrice());	
				
				lstMarketFactAccountTO.add(objMarketFact);
			}
		}

		tmpTransferOperation.setLstsecurityTransferMarketfact(lstSecurityTransferMarketfact); 
		//cambio el estado	y muevo los saldos, en el caso de destino origen, envio mi lista lstMarketFactAccountTO ya llena	
		executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact, lstMarketFactAccountTO) ;
		
	}

	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW)
	/**
	 * Execute change state all review available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param arrTmpTransferOperation the arr tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateAllReviewAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation[] arrTmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
		for (SecurityTransferOperation tmpTransferOperation: arrTmpTransferOperation) {
			
			//SI ES UNA REVISION EN UNA SOLICITUD DESTINO ORIGEN
			//SI USA LOS HECHOS DEL MERCADO. GRABAR TAMBIEN EN LA TABLA SecurityTransferMarketfact
			List<MarketFactAccountTO> lstMarketFactAccountTO = new ArrayList<MarketFactAccountTO>();
			List<SecurityTransferMarketfact> lstSecurityTransferMarketfact = new ArrayList<>();
			
			if(flagIndMarketFact && tmpTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
			
				List<HolderMarketFactBalance> lstHolderMarketFactBalance = tmpTransferOperation.getLstHolderMarketfactBalance();
				
				for(HolderMarketFactBalance obj : lstHolderMarketFactBalance){
					
					//voy a grabar mi SecurityTransferMarketfact, ya que es una revision destino origen
					SecurityTransferMarketfact securityTransferMarketfact = new SecurityTransferMarketfact();
					securityTransferMarketfact.setMarketDate(obj.getMarketDate());
					securityTransferMarketfact.setMarketPrice(obj.getMarketPrice());
					securityTransferMarketfact.setMarketRate(obj.getMarketRate());
					securityTransferMarketfact.setQuantityOperation(obj.getTransferAmmount());
					securityTransferMarketfact.setSecurityTransferOperation(tmpTransferOperation);
					securityTransferMarketfact.setAudit(loggerUser);
					lstSecurityTransferMarketfact.add(securityTransferMarketfact);
					
					//LLEVO MI LISTA AL OTRO LADO YA LLENA
					MarketFactAccountTO objMarketFact = new MarketFactAccountTO();
					objMarketFact.setMarketDate(securityTransferMarketfact.getMarketDate());
					objMarketFact.setQuantity(securityTransferMarketfact.getQuantityOperation());
					objMarketFact.setMarketRate(securityTransferMarketfact.getMarketRate());	
					objMarketFact.setMarketPrice(securityTransferMarketfact.getMarketPrice());	
					
					lstMarketFactAccountTO.add(objMarketFact);
				}
			}
			tmpTransferOperation.setLstsecurityTransferMarketfact(lstSecurityTransferMarketfact); 
			executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact,lstMarketFactAccountTO) ;
		}
	}
		
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT)
	/**
	 * Execute change state reject available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateRejectAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact,null) ;
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT)
	/**
	 * Execute change state rejec all available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateRejecAllAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation[] tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		for (int i = 0; i < tmpTransferOperation.length; i++) {
			executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation[i],flagIndMarketFact,null) ;
		}
	}

	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM)
	/**
	 * Execute change state confirm available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateConfirmAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation tmpTransferOperation, boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation,flagIndMarketFact,null) ;
	}
		
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM)
	/**
	 * Execute change state all confirm available.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeChangeStateAllConfirmAvailable(Integer stateOld,Integer stateNew,Long businessProcess,
			   SecurityTransferOperation[] tmpTransferOperation,boolean flagIndMarketFact) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		for (int i = 0; i < tmpTransferOperation.length; i++) {
			executeChangeState( stateOld, stateNew, businessProcess, tmpTransferOperation[i],flagIndMarketFact,null) ;
		}
	}
	
	
	/**
	 * Execute change state.
	 *
	 * @param stateOld the state old
	 * @param stateNew the state new
	 * @param businessProcess the business process
	 * @param tmpTransferOperation the tmp transfer operation
	 * @param flagIndMarketFact the flag ind market fact
	 * @param lstMarketFactAccountTO the lst market fact account to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String executeChangeState(Integer stateOld,Integer stateNew,Long businessProcess,
								   SecurityTransferOperation tmpTransferOperation,
								   boolean flagIndMarketFact,List<MarketFactAccountTO> lstMarketFactAccountTO) throws ServiceException{
						
		SecurityTransferOperation objSecurityTransferOperation = 
				transferAvailableServiceBean.find(SecurityTransferOperation.class, tmpTransferOperation.getIdTransferOperationPk());	
		
		// validacion de objetos cuando es diferente a anular y rechazar 
		if(!stateNew.equals(TransferSecuritiesStateType.RECHAZADO.getCode()) && !stateNew.equals(TransferSecuritiesStateType.ANULADO.getCode())){
			validateObjectsTransfer(tmpTransferOperation);
		}
		
		//validamos estado al momento de la confirmacion
		transferAvailableServiceBean.validateRequestState(objSecurityTransferOperation,stateNew);	
	
		/*** DATOS DE AUDITORIA ***/
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    
		if(stateNew.equals(TransferSecuritiesStateType.APROBADO.getCode())){
			
			objSecurityTransferOperation.getCustodyOperation().setApprovalDate(CommonsUtilities.currentDateTime());
			objSecurityTransferOperation.getCustodyOperation().setApprovalUser(loggerUser.getUserName());
			
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		}else if(stateNew.equals(TransferSecuritiesStateType.REVISADO.getCode())){			
			
			objSecurityTransferOperation.getCustodyOperation().setReviewDate(CommonsUtilities.currentDateTime());
			objSecurityTransferOperation.getCustodyOperation().setReviewUser(loggerUser.getUserName());
			
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReview());
			
		}else if(stateNew.equals(TransferSecuritiesStateType.ANULADO.getCode()) || stateNew.equals(TransferSecuritiesStateType.RECHAZADO.getCode())){
			
			objSecurityTransferOperation.getCustodyOperation().setRejectDate(CommonsUtilities.currentDateTime());
			objSecurityTransferOperation.getCustodyOperation().setRejectUser(loggerUser.getUserName());
			objSecurityTransferOperation.getCustodyOperation().setRejectMotive(tmpTransferOperation.getCustodyOperation().getRejectMotive());
			objSecurityTransferOperation.getCustodyOperation().setRejectMotiveOther(tmpTransferOperation.getCustodyOperation().getRejectMotiveOther());
			
			if(stateNew.equals(TransferSecuritiesStateType.ANULADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
			}
			if(stateNew.equals(TransferSecuritiesStateType.RECHAZADO.getCode())){
				loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
			}
			
		}else if(stateNew.equals(TransferSecuritiesStateType.CONFIRMADO.getCode())){
						
			objSecurityTransferOperation.getCustodyOperation().setConfirmDate(CommonsUtilities.currentDateTime());
			objSecurityTransferOperation.getCustodyOperation().setConfirmUser(loggerUser.getUserName());
			loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		}
		
		/**** DATOS PARA LA ACTUALIZACION DE LA SOLICITUD DE TRASPASOS ****/

		//ASIGNAMOS LOS NUEVOS ESTADOS
		objSecurityTransferOperation.setState(stateNew);
		objSecurityTransferOperation.getCustodyOperation().setState(stateNew);
		
		//SI ES UNA REVISION ASIGNAMOS LAS CUENTAS CONTRAPARTE
		if(stateNew.equals(TransferSecuritiesStateType.REVISADO.getCode())){
			//solo para la REVISION ORIGEN DESTINO Y DESTINO ORIGEN
			if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				objSecurityTransferOperation.setSourceHolderAccount(tmpTransferOperation.getSourceHolderAccount());
			}else if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
				objSecurityTransferOperation.setTargetHolderAccount(tmpTransferOperation.getTargetHolderAccount());
			}
		}

		//HECHOS DEL MERCADO
		List<SecurityTransferMarketfact> lstSecurityTransferMarketfact = new ArrayList<SecurityTransferMarketfact>();
		lstSecurityTransferMarketfact = tmpTransferOperation.getLstsecurityTransferMarketfact();
		
		//ACTUALIZAMOS LOS TRASPASOS
		transferAvailableServiceBean.updateWithOutFlush(objSecurityTransferOperation);
		
		if( businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode()) && 
			objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
			for(SecurityTransferMarketfact stoMkf: lstSecurityTransferMarketfact){
				stoMkf.setSecurityTransferOperation(objSecurityTransferOperation);
				transferAvailableServiceBean.create(stoMkf);
			}
		}
		
		/**** PASAMOS A EJECUTAR LOS MOVIMIENTOS ****/
		
		//OBTENGO LOS HECHOS DEL MERCADOS YA REGISTRADOS
		if(! (businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode()) && 
			  objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())) ){
			lstSecurityTransferMarketfact = findSecurityTransferMarketfact(null,tmpTransferOperation.getIdTransferOperationPk());
		}
		
		
		//NO INMOVILIZA SALDOS EN LA APROBACION APROVACION
		if( businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_APPROVE.getCode())){ //aprobacion
			return "";
		}
		
		//INMOVILIZO SALDOS EN DESTINO ORIGEN AL REVISAR
		if( businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REVIEW.getCode())){ //revision
			if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				
				//lstMarketFactAccountTO <- YA LLEGA LLENA DE DATOS, QUE SE OBTUVO DESDE EL HELP DE HECHOS DEL MERCADO
				executeComponent(objSecurityTransferOperation, businessProcess,null,ComponentConstant.SOURCE,null,flagIndMarketFact,lstMarketFactAccountTO);
			}else{
				//EN LOS CASOS ORIGEN DESTINO Y MISMO PARTICIPANTE, LOS SALDOS YA FUERON AFECTADOS EN INSTANCIAS ANTERIORES
			}
		}
		
		//REVIERTO SALDOS EN CASO SE ORIGEN DESTINO O MISMO PARTICIPANTE
		if( businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode()) || 
			businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL.getCode())){ 	//rechazo o anulacion
			
			if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode()) ||
			   objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){

				lstMarketFactAccountTO= getLstMarketFactAccountTO(lstSecurityTransferMarketfact);

				executeComponent(objSecurityTransferOperation, businessProcess,null,ComponentConstant.SOURCE,null,
								 flagIndMarketFact,lstMarketFactAccountTO);
			}
		}
		
		//REVIERTO SALDOS EN CASO SEA UNA RECHAZO DE UNA SOLICITUD QUE YA FUE REVISADA, EN EL DESTINO ORIGEN
		if( businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode()) && 
			stateOld.equals(TransferSecuritiesStateType.REVISADO.getCode())	){ 
			if(objSecurityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				
				lstMarketFactAccountTO= getLstMarketFactAccountTO(lstSecurityTransferMarketfact);
				
				executeComponent(objSecurityTransferOperation, businessProcess,null,ComponentConstant.SOURCE,null,
								 flagIndMarketFact,lstMarketFactAccountTO);
			}else{
				//EN OTROS CASOS NO SE A INMOVILIZADO SALDOS EN UNA SOLICITUD DE DESTINO ORIGEN
			}
		}
		
		//MUEVO LOS SALDOS A LA CUENTA DESTINO
		if( businessProcess.equals(BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CONFIRM.getCode())){ //confirmacion
			
			lstMarketFactAccountTO= getLstMarketFactAccountTO(lstSecurityTransferMarketfact);
			
			executeComponent(objSecurityTransferOperation, businessProcess,null,ComponentConstant.SOURCE,ComponentConstant.TARGET,
							 flagIndMarketFact,lstMarketFactAccountTO);
		}
				
		//Retorno el pk
		if(Validations.validateIsNotNullAndPositive(objSecurityTransferOperation.getIdTransferOperationPk()))
			return objSecurityTransferOperation.getCustodyOperation().getOperationNumber().toString();
		else
			return "";
	}
	
	/**
	 * Gets the lst market fact account to.
	 *
	 * @param lstSecurityTransferMarketfact the lst security transfer marketfact
	 * @return the lst market fact account to
	 */
	//SETTEO LOS HECHOS DEL MERCADO
	public List<MarketFactAccountTO> getLstMarketFactAccountTO(List<SecurityTransferMarketfact> lstSecurityTransferMarketfact){
		MarketFactAccountTO marketfactTO =null;
		List<MarketFactAccountTO> lstMarketFactAccountTO= new ArrayList<MarketFactAccountTO>();
		
		for(SecurityTransferMarketfact obj: lstSecurityTransferMarketfact){
			marketfactTO = new MarketFactAccountTO();
			marketfactTO.setMarketDate(obj.getMarketDate());
			marketfactTO.setQuantity(obj.getQuantityOperation());
			
			if( Validations.validateIsNotNull(obj.getMarketPrice()) && Validations.validateIsNotNull(obj.getMarketPrice())){
				marketfactTO.setMarketPrice(obj.getMarketPrice());
			}
			
			if( Validations.validateIsNotNull(obj.getMarketRate()) && Validations.validateIsNotNull(obj.getMarketRate()) ){
				marketfactTO.setMarketRate(obj.getMarketRate());
			}
			lstMarketFactAccountTO.add(marketfactTO);
			
		}
		return lstMarketFactAccountTO;
	}

	/**
	 * Execute component.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @param businessProcess the business process
	 * @param quantity the quantity
	 * @param source the source
	 * @param target the target
	 * @param indMarketFact the ind market fact
	 * @param lstMarketFactAccountTO the lst market fact account to
	 * @throws ServiceException the service exception
	 */
	public void executeComponent(SecurityTransferOperation securityTransferOperation, 
			Long businessProcess,BigDecimal quantity, Long source, Long target, 
			boolean indMarketFact, List<MarketFactAccountTO> lstMarketFactAccountTO) throws ServiceException{
		
		SecurityTransferOperation securityObject = securityTransferOperation;
		CustodyOperation custodyOperation = securityObject.getCustodyOperation();
		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
		HolderAccountBalanceTO holderAccountBalanceTO = null;
		
		List<HolderAccountBalanceTO> lstAccountsSource = null;
		if(source!=null && source.equals(ComponentConstant.SOURCE)){
			
			
			lstAccountsSource = new ArrayList<HolderAccountBalanceTO>();
			holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(securityObject.getSourceHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(securityObject.getSourceParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(securityObject.getSecurities().getIdSecurityCodePk());
			
			//envio la lista
			holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);
			
			if(quantity !=null){
				holderAccountBalanceTO.setStockQuantity(quantity);
			}else{
				holderAccountBalanceTO.setStockQuantity(securityObject.getQuantityOperation());
			}
			holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);
			lstAccountsSource.add(holderAccountBalanceTO);
		}
		
		List<HolderAccountBalanceTO> lstAccountsTarget = null;
		if(target!=null && target.equals(ComponentConstant.TARGET)){
			lstAccountsTarget = new ArrayList<HolderAccountBalanceTO>();
			holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(securityObject.getTargetHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(securityObject.getTargetParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(securityObject.getSecurities().getIdSecurityCodePk());
			if(quantity !=null){
				holderAccountBalanceTO.setStockQuantity(quantity);
			}else{
				holderAccountBalanceTO.setStockQuantity(securityObject.getQuantityOperation());
			}			
			
			//envio la lista
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for (MarketFactAccountTO marketFactAccountTO: lstMarketFactAccountTO) {
				MarketFactAccountTO targetMarketFactAccountTO= null;
				try {
					targetMarketFactAccountTO= marketFactAccountTO.clone();
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
				holderAccountBalanceTO.getLstMarketFactAccounts().add(targetMarketFactAccountTO);
			}
			//holderAccountBalanceTO.setLstMarketFactAccounts(lstMarketFactAccountTO);
			lstAccountsTarget.add(holderAccountBalanceTO);
		}

		objAccountComponent.setLstSourceAccounts(lstAccountsSource);
		objAccountComponent.setLstTargetAccounts(lstAccountsTarget);
		objAccountComponent.setIdBusinessProcess(businessProcess);
		objAccountComponent.setIdCustodyOperation(custodyOperation.getIdCustodyOperationPk());
		objAccountComponent.setIdOperationType(custodyOperation.getOperationType());
		//Se deshabilita indMarketFact
		//objAccountComponent.setIndMarketFact(1);
		
		accountsComponentService.get().executeAccountsComponent(objAccountComponent);
		
	}	
	
	
	/**
	 * Execute confirm actions.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @param securityBlockTo the security block to
	 * @param lstSecurityBlockTransferTO the lst security block transfer to
	 * @param loggerUser the logger user
	 * @param flagIndMarketFact the flag ind market fact
	 * @throws ServiceException the service exception
	 */
	public void executeConfirmActions(SecurityTransferOperation securityTransferOperation, SecurityBlockTransferTO securityBlockTo,
			List<SecurityBlockTransferTO> lstSecurityBlockTransferTO, LoggerUser loggerUser, boolean flagIndMarketFact) throws ServiceException{
		
		//cuentas origen y destino
		HolderAccount srcAccount = transferAvailableServiceBean.find(HolderAccount.class,securityBlockTo.getSourceIdHolderAccountPk());
		HolderAccount tgtAccount = transferAvailableServiceBean.find(HolderAccount.class, securityBlockTo.getTargetIdHolderAccountPk());
		
		//componente de traspasos
		Map<Long, List<MarketFactAccountTO>> mapMarketFactAccountTOWithOperation = new HashMap<Long, List<MarketFactAccountTO>>();
		
		//componente afectacion
		Map<Long, ArrayList<MarketFactTO>> mapMarketAffectationWithOperation = new HashMap<Long, ArrayList<MarketFactTO>>();

		//lista para ejecutar los movimientos por documento de bloqueo
		//List<Long> lstBlockOperationsPk= new ArrayList<Long>();
		
		Map<Long, Integer> mapBlockOperationsPk = new HashMap<Long, Integer>();
		
		
		/*** AGRUPO POR DOCUMENTOS DE BLOQUEO ***/
		Map<Long,List<SecurityBlockTransferTO>> mapByRequest = groupBlocksByRequest(lstSecurityBlockTransferTO);
		for (Entry<Long, List<SecurityBlockTransferTO>> entry : mapByRequest.entrySet()) {
			
			//DATOS INICIALES PARA LA REVERSION

			BigDecimal totalUnblock = new BigDecimal(0);
			ParameterTable motive = new ParameterTable();
	        motive.setParameterTablePk(MotiveReverlsasType.EJECUTION.getCode());
	        
	        BlockRequest blockRequest = new BlockRequest();
			blockRequest.setIdBlockRequestPk(entry.getKey());
			blockRequest = getBlockRequest(blockRequest);
	        
			RegisterReversalTO regReversalTO = new RegisterReversalTO();
	    	regReversalTO.setRegistryUser(loggerUser.getUserName()); 
	    	regReversalTO.setDocumentNumber(blockRequest.getBlockNumber());
	    	regReversalTO.setParamTableMotiveUnblock(motive);
	    	regReversalTO.setIdState(RequestReversalsStateType.APPROVED.getCode());
	    	regReversalTO.setLstBlockOpeForReversals(new ArrayList<BlockOpeForReversalsTO>());
	    	
	    	
	    	//RECORRO TODOS LOS DOCUMENTOS DE BLOQUEO
	    	
	    	for(SecurityBlockTransferTO trans : entry.getValue()){
	    		
	    		//seteo mi documento de bloqueo
	    		//if(trans.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
	    		mapBlockOperationsPk.put(trans.getIdBlockOperationPk(), trans.getBlockLevel());
	    		//}
	    		//hechos del mercado por operacion de bloqueo
	    		List<BlockMarketFactOperation> lstBlockMarketFactOperation = lstBlockMarketFactOperation(null, trans.getIdBlockOperationPk());
	    		//componente de reversion
	    		List<ReversalsMarkFactTO> reversalsMarkFactList = new ArrayList<ReversalsMarkFactTO>();
				//componente de traspasos
				List<MarketFactAccountTO> lstMarketFactAccount = new ArrayList<MarketFactAccountTO>();
				//componente de afectacion
		    	ArrayList<MarketFactTO> lstMarketFact = new ArrayList<MarketFactTO>();
				
		    	
	    		//recorro los hechos del mercado por 
	    		for(BlockMarketFactOperation obj: lstBlockMarketFactOperation){
	    			
	    			//DESAFECTACION
	    			ReversalsMarkFactTO reversalsMarkFact = new ReversalsMarkFactTO();	
	    			reversalsMarkFact.setBlockAmount(obj.getActualBlockBalance());
	    			reversalsMarkFact.setMarketDate(obj.getMarketDate());
	    			reversalsMarkFact.setMarketPrice(obj.getMarketPrice());
	    			reversalsMarkFact.setMarketRate(obj.getMarketRate());
	    			reversalsMarkFactList.add(reversalsMarkFact);
	    			
					//TRASPASOS 
	    			MarketFactAccountTO transferMarketFact = new MarketFactAccountTO();
	    			transferMarketFact.setMarketDate(obj.getMarketDate());
	    			transferMarketFact.setMarketPrice(obj.getMarketPrice());
	    			transferMarketFact.setMarketRate(obj.getMarketRate());
	    			transferMarketFact.setQuantity(obj.getActualBlockBalance());
	    			lstMarketFactAccount.add(transferMarketFact);
	    			
	    			//AFECTACION
					MarketFactTO objMarketFactAff = new MarketFactTO();
					objMarketFactAff.setMarketDate(obj.getMarketDate());
					objMarketFactAff.setMarketPrice(obj.getMarketPrice());
					objMarketFactAff.setMarketRate(obj.getMarketRate());
					objMarketFactAff.setQuantity(obj.getActualBlockBalance());
					lstMarketFact.add(objMarketFactAff);
					
	    		}
	    		
	    		//************ DATOS PARA LA REVERSION ***********
	    		totalUnblock = totalUnblock.add(trans.getActualBlockBalance());
	    		
	    		BlockOpeForReversalsTO blockOpeForReversal = new BlockOpeForReversalsTO();
	    		blockOpeForReversal.setIdBlockOperationPk(trans.getIdBlockOperationPk());
	    		blockOpeForReversal.setAmountUnblock(trans.getActualBlockBalance());
	    		blockOpeForReversal.setReversalsMarkFactList(reversalsMarkFactList);
	    		
	    		regReversalTO.setIdHolderPk(srcAccount.getRepresentativeHolder().getIdHolderPk());
	    		regReversalTO.getLstBlockOpeForReversals().add(blockOpeForReversal);

	    		
	    		//mapa afectacion
		    	mapMarketAffectationWithOperation.put(trans.getIdBlockOperationPk(), lstMarketFact);
		    	//mapa traspasos
		    	mapMarketFactAccountTOWithOperation.put(trans.getIdBlockOperationPk(), lstMarketFactAccount);
	    	}
	    	
	    	/*** EJECUTO LA DESAFECTACION, llena con todos los datos ****/
	    	
	    	regReversalTO.setTotalUnblock(totalUnblock);
	    	regReversalTO.setConfirmationUser(loggerUser.getUserName());
	        Long unblockRequestPk = requestReversalsServiceBean.saveReversalRequest(regReversalTO);
	    	
	        RequestReversalsTO requestReversalsTO = new RequestReversalsTO();
	        requestReversalsTO.setIdUnblockRequestPk(unblockRequestPk);
	        requestReversalsTO.setBusinessProcessTypeId(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode());
	        
	        //EJECUTO LA CONFIRMACION PARA LA DESAFECTACION
	        requestReversalsServiceBean.confirmReversalRequest(requestReversalsTO, loggerUser, Boolean.FALSE);
		}
	
		/****** RECORRO LOS DOCUMENTOS DE BLOQUEO **********/
		
		//for (Long blockOperationId: lstBlockOperationsPk) {
		for (Entry<Long, Integer> entry : mapBlockOperationsPk.entrySet()) {
		
		/** 2. TRASPASO LOS SALDOS A LA CUENTA DESTINO */
		
		if(entry.getValue().equals(LevelAffectationType.BLOCK.getCode())){
			/*** DATOS PARA EL TRASPASO *****/
			BigDecimal bigTotal = new BigDecimal(0);
			
			List<MarketFactAccountTO> lstMarketFactAccountTO = mapMarketFactAccountTOWithOperation.get(entry.getKey());
			for(MarketFactAccountTO securityTransferMarketfact :lstMarketFactAccountTO){
				bigTotal = bigTotal.add(securityTransferMarketfact.getQuantity());
			}
			
			//modificar mi saldo a traspasar
			if(!lstMarketFactAccountTO.isEmpty()) {
				securityTransferOperation.setQuantityOperation(bigTotal); 				
			} else if(Validations.validateIsNotNull(securityTransferOperation.getQuantityOperation())){
				bigTotal = securityTransferOperation.getQuantityOperation();
			}
			transferAvailableServiceBean.updateWithOutFlush(securityTransferOperation);
			
			//GRABAR LOS SUBSALDOS EN LA TABLA DE TRASPASOS DE VALORES BLOQUEADOS
			for(MarketFactAccountTO securityTransferMarketfact :lstMarketFactAccountTO){
				SecurityTransferMarketfact objMarketFact = new SecurityTransferMarketfact();
				objMarketFact.setMarketDate(securityTransferMarketfact.getMarketDate());
				objMarketFact.setQuantityOperation(securityTransferMarketfact.getQuantity());
				objMarketFact.setMarketRate(securityTransferMarketfact.getMarketRate());	
				objMarketFact.setMarketPrice(securityTransferMarketfact.getMarketPrice());	
				objMarketFact.setSecurityTransferOperation(securityTransferOperation);
				objMarketFact.setAudit(loggerUser);
				transferAvailableServiceBean.create(objMarketFact);
			}

			/**** EJECUTO EL TRASPASO DE SALDOS POR DOCUMENTO DE BLOQUEO ****/
			executeComponent(securityTransferOperation, 
							 BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode(), 
							 bigTotal, ComponentConstant.SOURCE, ComponentConstant.TARGET, Boolean.FALSE,lstMarketFactAccountTO);
		}

		/** 3. BLOQUEO LOS SALDOS EN LA CUENTA DESTINO */
			
			/*** DATOS PARA LA AFECTACION*****/
			//Genero el bloqueo en la cuenta destino
			List<AffectationTransferTO> transfers = new ArrayList<AffectationTransferTO>();
			
			for(SecurityBlockTransferTO value: lstSecurityBlockTransferTO){
				if(entry.getKey().equals(value.getIdBlockOperationPk())){
					AffectationTransferTO at = new AffectationTransferTO();
					at.setIdHolderPk(tgtAccount.getRepresentativeHolder().getIdHolderPk());
					at.setIdHolderAccountPk(tgtAccount.getIdHolderAccountPk());
					at.setMarketFactAffectations(mapMarketAffectationWithOperation.get(value.getIdBlockOperationPk()));
					at.setIdParticipantPk(securityBlockTo.getTargetIdParticipantPk());
					at.setIdSecurityCodePk(value.getIdSecurityCodePk());
					at.setIdBlockOperationPk(value.getIdBlockOperationPk());
					at.setQuantity(value.getActualBlockBalance());
					for(BlockOperationTransfer objBlockOperationTransfer:securityTransferOperation.getBlockOperationTransfer()){
						if(objBlockOperationTransfer.getBlockOperation().getIdBlockOperationPk()
								.equals(value.getIdBlockOperationPk())){
							at.setBlockOperationTransfer(objBlockOperationTransfer);
							break;
						}
					}
					
					transfers.add(at);
				}
			}

			/****** EJECUTO EL COMPONENTE DE AFECTACION ******/
			affectationsServiceBean.transferBlockOperations(transfers,BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode(), 
															loggerUser.getUserName(),ComponentConstant.ZERO);
			
		}

	}
	
	/**
	 * Lst block market fact operation.
	 *
	 * @param idBlockMarketfactPk the id block marketfact pk
	 * @param idBlockOperationPk the id block operation pk
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BlockMarketFactOperation> lstBlockMarketFactOperation(Long idBlockMarketfactPk, Long idBlockOperationPk) throws ServiceException{
		List<BlockMarketFactOperation> lstBlockMarketFactOperation = affectationsServiceBean.lstBlockMarketFactOperation(idBlockMarketfactPk, idBlockOperationPk);
		return lstBlockMarketFactOperation;
	}
	
	/**
	 * Group blocks by request.
	 *
	 * @param lstSecurityBlockTransferTO the lst security block transfer to
	 * @return the map
	 */
	public Map<Long,List<SecurityBlockTransferTO>> groupBlocksByRequest(List<SecurityBlockTransferTO> lstSecurityBlockTransferTO){
		
		Map<Long,List<SecurityBlockTransferTO>> mapByRequest = new HashMap<Long,List<SecurityBlockTransferTO>>();
		
		for(SecurityBlockTransferTO to : lstSecurityBlockTransferTO){
			List<SecurityBlockTransferTO> currentBlockList  = mapByRequest.get(to.getIdBlockRequest());
			if(currentBlockList == null){
				currentBlockList = new ArrayList<SecurityBlockTransferTO>();
			}
			currentBlockList.add(to);
			mapByRequest.put(to.getIdBlockRequest(), currentBlockList);
		}
		
		return mapByRequest;
	}
	
	/**
	 * Gets the complement holder account.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @return the complement holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getComplementHolderAccount(SecurityTransferOperation securityTransferOperation) throws ServiceException{
		return custodyOperationServiceBean.getComplementHolderAccount(securityTransferOperation);
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_QUERY)
	/**
	 * Solicitud with account.
	 *
	 * @param securityTo the security to
	 * @param objUserAccountSession the obj user account session
	 * @return the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public SecurityTransferOperation SolicitudWithAccount(SecurityTransferOperation securityTo,UserAccountSession objUserAccountSession) throws ServiceException{
		List<SecurityTransferOperation> lstSecurityTO = new ArrayList<SecurityTransferOperation>();
		securityTo.getCustodyOperation().setOperationInit(null);
		securityTo.getCustodyOperation().setOperationEnd(null);
		lstSecurityTO = custodyOperationServiceBean.searchTransferSecurityAvailableNativo(securityTo,objUserAccountSession);
		
		if(lstSecurityTO.size()>0){
			return lstSecurityTO.get(0);
		}else{
			return new SecurityTransferOperation();
		}
	}
	
	/**
	 * Gets the holder account.
	 *
	 * @param holderAccount the holder account
	 * @return the holder account
	 * @throws ServiceException the service exception
	 */
	public HolderAccount getHolderAccount(HolderAccount holderAccount) throws ServiceException{
		return custodyOperationServiceBean.getHolderAccount(holderAccount);
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_QUERY)
	/**
	 * Search transfer security available nativo.
	 *
	 * @param securityTo the security to
	 * @param objUserAccountSession the obj user account session
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityTransferOperation> searchTransferSecurityAvailableNativo(SecurityTransferOperation securityTo,UserAccountSession objUserAccountSession) throws ServiceException{
		return  custodyOperationServiceBean.searchTransferSecurityAvailableNativo(securityTo,objUserAccountSession);
	}
	
	/**
	 * Gets the dates custody operation for pk.
	 *
	 * @param custodyOperation the custody operation
	 * @return the dates custody operation for pk
	 * @throws ServiceException the service exception
	 */
	public CustodyOperation getDatesCustodyOperationForPk(CustodyOperation custodyOperation) throws ServiceException{
		return custodyOperationServiceBean.getDatesCustodyOperationForPk(custodyOperation);
	}
	
	
	/**
	 * Checks if is valid the transfer ammount.
	 *
	 * @param holderAccountBalance the holder account balance
	 * @return the string
	 */
	public String isValidTheTransferAmmount(HolderAccountBalance holderAccountBalance){
		if(Validations.validateIsNotNull(holderAccountBalance.getTransferAmmount())){

			if( holderAccountBalance.getTransferAmmount().intValue() > 0 ){	/** if the transfer ammount if the ammount valid */
				
				if(holderAccountBalance.getTransferAmmount().intValue() > holderAccountBalance.getAvailableBalance().intValue()){	/** if available balance is less to transfer ammount, transfer ammount 
																																		the initialized to 0 beacuse the value if not valid*/
					return PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
							 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
							 PropertiesConstants.AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT);
				}
			}else if( holderAccountBalance.getTransferAmmount().intValue() <= 0 ){	/** if the transfer ammount if negative */
				return PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT);
			}
		}else{
			return PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.AVAILABLEBALANCE_MINOR_TRANSFERAMOUNT);
		}
		
		return "";
	}

	/**
	 * Gets the lis holder account service bean.
	 *
	 * @param security the security
	 * @return the lis holder account service bean
	 * @throws ServiceException the service exception
	 */
	public List<Security> getLisHolderAccountServiceBean(Security security) throws ServiceException {
		return securitiesQueryServiceBean.getLisSecurityServiceBean(security);
	}
	
	//@ProcessAuditLogger(process=BusinessProcessType.BLOCK_SECUTITY_TRANSFER_QUERY)
	/**
	 * Search solicitud security block transfer to.
	 *
	 * @param filter the filter
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityBlockTransferTO> searchSolicitudSecurityBlockTransferTo(SecurityBlockTransferTO filter) throws ServiceException{
		return blockSercuiritiesServiceBean.searchSolicitudSecurityBlockTransferTo(filter);
	}
	
	/**
	 * Search security block transfer to.
	 *
	 * @param filters the filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityBlockTransferTO> searchSecurityBlockTransferTo(SecurityBlockTransferTO filters) throws ServiceException{
		return affectationsServiceBean.searchSecurityBlockTransferTo(filters);
	}
	

	/**
	 * Valid block amount in transfer available.
	 *
	 * @param idTransferBlockPk the id transfer block pk
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer validBlockAmountInTransferAvailable(Long idTransferBlockPk) throws ServiceException{
		return affectationsServiceBean.validBlockAmountInTransferAvailable(idTransferBlockPk);
	}
	
	/**
	 * Search security block transfer to width document number.
	 *
	 * @param filters the filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityBlockTransferTO> searchSecurityBlockTransferToWidthDocumentNumber(List<SecurityBlockTransferTO> filters) throws ServiceException{
		return affectationsServiceBean.searchSecurityBlockTransferToWidthDocumentNumber(filters);
	}

	/**
	 * Search document numbers with operation.
	 *
	 * @param filters the filters
	 * @param onlyread the onlyread
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<SecurityBlockTransferTO> searchDocumentNumbersWithOperation(SecurityBlockTransferTO filters, boolean onlyread) throws ServiceException{
		return blockSercuiritiesServiceBean.searchDocumentNumbersWithOperation(filters, onlyread);
	}
	
	/**
	 * Gets the block request.
	 *
	 * @param filters the filters
	 * @return the block request
	 * @throws ServiceException the service exception
	 */
	public BlockRequest getBlockRequest(BlockRequest filters) throws ServiceException{
		return affectationsServiceBean.getBlockRequest(filters);
	}
	

	/**
	 * Exist security transfer block.
	 *
	 * @param blockType the block type
	 * @param securityCode the security code
	 * @param OperationNumber the operation number
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean existSecurityTransferBlock(Integer blockType, String securityCode, Long OperationNumber) throws ServiceException{
		if(custodyOperationServiceBean.existSecurityTransferBlock(blockType,securityCode,OperationNumber)>0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Cancela o Anula las solicitudes
	 * Dependiendo el estado en que se encuentre,
	 * Finalmente mueve los saldos.
	 *
	 * @param finalDate the final date
	 * @param indBlock the ind block
	 * @throws ServiceException the service exception
	 */
	
	public void automaticTransferCancelations(Date finalDate, Integer indBlock) throws ServiceException{		
		SecurityTransferOperationTO securityTransferOperationTO = new SecurityTransferOperationTO();
		securityTransferOperationTO.setOperationEndDate(finalDate);
		securityTransferOperationTO.setIndBlock(indBlock);
		List<Integer> states = new ArrayList<Integer>();
		states.add(TransferSecuritiesStateType.REGISTRADO.getCode());
		states.add(TransferSecuritiesStateType.APROBADO.getCode());
		states.add(TransferSecuritiesStateType.REVISADO.getCode());
		securityTransferOperationTO.setStates(states);		
		List<SecurityTransferOperation> lstSecurityTransferOperation = 
				transferAvailableServiceBean.searchTransferOperationsByDays(securityTransferOperationTO);		
		Integer newState = null;
		Long businessProcess= null;
		for (SecurityTransferOperation objTransferOperation : lstSecurityTransferOperation){
			objTransferOperation.getCustodyOperation().setRejectMotive(SecuritiesTransferMotiveType.EXCEEDED_TIME_ALLOWED.getCode());									
			if(indBlock.equals(ComponentConstant.ONE)){
				if (objTransferOperation.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
					newState = TransferSecuritiesStateType.ANULADO.getCode();
					businessProcess = BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CANCEL.getCode();
					executeChangeStateCancelBlock(objTransferOperation.getState(), newState, businessProcess, objTransferOperation, false);
				}else if (objTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) || 
						objTransferOperation.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())) {
					newState = TransferSecuritiesStateType.RECHAZADO.getCode();
					businessProcess = BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REJECT.getCode();
					executeChangeStateRejectBlock(objTransferOperation.getState(), newState, businessProcess, objTransferOperation, false);
				}								
			}else if(indBlock.equals(ComponentConstant.ZERO)){
				if (objTransferOperation.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
					newState = TransferSecuritiesStateType.ANULADO.getCode();
					businessProcess = BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_CANCEL.getCode();
					executeChangeStateCancelAvailable(objTransferOperation.getState(), newState, businessProcess, objTransferOperation, false);
				}else if (objTransferOperation.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) || 
						objTransferOperation.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())) {
					newState = TransferSecuritiesStateType.RECHAZADO.getCode();
					businessProcess = BusinessProcessType.AVAILABLE_SECUTITY_TRANSFER_REJECT.getCode();
					executeChangeStateRejectAvailable(objTransferOperation.getState(), newState, businessProcess, objTransferOperation, false);
				}	
			}
		}		
	}
	
	/**
	 * Gets the security transfer operation.
	 *
	 * @param idSecurityTransferOperation the id security transfer operation
	 * @return the security transfer operation
	 * @throws ServiceException the service exception
	 */
	public SecurityTransferOperation getSecurityTransferOperation(Long idSecurityTransferOperation) throws ServiceException{
		return custodyOperationServiceBean.getSecurityTransferOperation(idSecurityTransferOperation);
	}
	
	public Date getExpirationDateInfo(String securityCode) throws ServiceException{
		return custodyOperationServiceBean.getExpirationDateInfo(securityCode).getExpirationDate();
	}
}
