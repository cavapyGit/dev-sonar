package com.pradera.custody.tranfersecurities.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.custody.tranfersecurities.to.SecurityBlockTransferTO;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SecurityBlockTransferDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class SecurityBlockTransferDataModel extends ListDataModel<SecurityBlockTransferTO> implements SelectableDataModel<SecurityBlockTransferTO>,Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4619801468926703772L;

	/**
	 * Instantiates a new security block transfer data model.
	 */
	public SecurityBlockTransferDataModel() {
			
	}
	
	/**
	 * Instantiates a new security block transfer data model.
	 *
	 * @param lstSecurityBlockTransferTO the lst security block transfer to
	 */
	public SecurityBlockTransferDataModel(List<SecurityBlockTransferTO> lstSecurityBlockTransferTO) {
		super(lstSecurityBlockTransferTO);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public SecurityBlockTransferTO getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<SecurityBlockTransferTO> ListSecurityBlockTransferTO = (List<SecurityBlockTransferTO>)getWrappedData();
		for(SecurityBlockTransferTO securityBlockTransferTO : ListSecurityBlockTransferTO){
			if(securityBlockTransferTO.getIdBlockOperationPk().toString().equals(rowKey))
				return securityBlockTransferTO;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(SecurityBlockTransferTO holderAccountBalance) {//Return Object
		// TODO Auto-generated method stub
		return holderAccountBalance.getIdBlockOperationPk();
	}

}
