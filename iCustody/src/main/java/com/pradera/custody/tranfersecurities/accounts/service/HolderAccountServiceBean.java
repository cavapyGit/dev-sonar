package com.pradera.custody.tranfersecurities.accounts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.SecuritySearchTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountMovement;
import com.pradera.model.component.HolderMarketFactBalance;
import com.pradera.model.component.MovementType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationMarketFact;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.externalinterface.InterfaceTransaction;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.negotiation.type.MechanismOperationStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class HolderAccountServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
public class HolderAccountServiceBean extends CrudDaoServiceBean{

	/**
	 * Gets the lis holder account service bean.
	 *
	 * @param filter the filter
	 * @return the lis holder account service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccount> getLisHolderAccountServiceBean(HolderAccount filter) throws ServiceException{
		List<HolderAccount> list = new ArrayList<HolderAccount>();
		StringBuilder sbQuery = new StringBuilder();
	

		sbQuery.append(" Select ha From HolderAccount ha join fetch ha.holderAccountDetails had " +
					   " join fetch had.holder ho join fetch ha.participant pa");
		
		sbQuery.append(" Where 1 = 1 ");
		if(Validations.validateIsNotNullAndPositive(filter.getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :idHolderAccountPkPrm ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountNumber())){
			sbQuery.append(" and ha.accountNumber = :accountNumberPrm ");
		}
		if(Validations.validateIsNotNull(filter.getParticipant())){
			if(Validations.validateIsNotNullAndPositive(filter.getParticipant().getIdParticipantPk()))
				sbQuery.append(" and pa.idParticipantPk = :participantPkPrm ");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getStateAccount())){
			sbQuery.append(" and ha.stateAccount = :statePrm ");
		}else{
			//If not have parameter state, the state is distinct to BLOQUED AND CLOSED
		}
		Query query = em.createQuery(sbQuery.toString()); 

		if(Validations.validateIsNotNullAndPositive(filter.getIdHolderAccountPk())){
    		query.setParameter("idHolderAccountPkPrm", filter.getIdHolderAccountPk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountNumber())){
    		query.setParameter("accountNumberPrm", filter.getAccountNumber());
    	}
		if(Validations.validateIsNotNull(filter.getParticipant())){
			if(Validations.validateIsNotNullAndPositive(filter.getParticipant().getIdParticipantPk()))
    		query.setParameter("participantPkPrm", filter.getParticipant().getIdParticipantPk());
    	}
		if(Validations.validateIsNotNullAndPositive(filter.getStateAccount())){
    		query.setParameter("statePrm", filter.getStateAccount());
    	}
		
		list = (List<HolderAccount>)query.getResultList();
		if(Validations.validateIsNull(list)){
			 list = new ArrayList<HolderAccount>();
		}
		return list;
	}
	
	/**
	 * Equals rnt with holder account.
	 *
	 * @param sourceIdHolderAccountPk the source id holder account pk
	 * @param targetIdHolderAccountPk the target id holder account pk
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean equalsRntWithHolderAccount(Long sourceIdHolderAccountPk,Long targetIdHolderAccountPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		List<Long> lstObj = null;
		List<Long> lstSourceRnts = new ArrayList<Long>();
		List<Long> lstTargetRnts = new ArrayList<Long>();
		
		sbQuery.append(" Select ho.idHolderPk from HolderAccount ha " +
				"join ha.holderAccountDetails had " +
				"join had.holder ho ");
		sbQuery.append(" Where ha.idHolderAccountPk = :sourceIdHolderAccountPk ");
		Query query = em.createQuery(sbQuery.toString()); 
    	query.setParameter("sourceIdHolderAccountPk", sourceIdHolderAccountPk);
    	lstObj = query.getResultList();
    	if(lstObj.size()>0){
    		for (int i = 0; i < lstObj.size(); i++) {
    			lstSourceRnts.add((Long)lstObj.get(i));
			}
    	}
    	
    	lstObj = null;
    	sbQuery = new StringBuilder();
    	sbQuery.append(" Select ho.idHolderPk from HolderAccount ha " +
    			"join ha.holderAccountDetails had " +
    			"join had.holder ho ");
		sbQuery.append(" Where ha.idHolderAccountPk = :targetIdHolderAccountPk ");
		sbQuery.append(" and ho.stateHolder = :state ");
		query = em.createQuery(sbQuery.toString());
    	query.setParameter("targetIdHolderAccountPk", targetIdHolderAccountPk);
    	query.setParameter("state", HolderStateType.REGISTERED.getCode());
    	lstObj = query.getResultList();
    	if(lstObj.size()>0){
    		for (int i = 0; i < lstObj.size(); i++) {
    			lstTargetRnts.add((Long)lstObj.get(i));
			}
    	}
    	
    	boolean flagValidRnt=false;
    	
    	if(lstSourceRnts.size()==lstTargetRnts.size()){
    	
	    	for (int i = 0; i < lstSourceRnts.size(); i++) {
	    		for (int j = 0; j < lstTargetRnts.size(); j++) {
	    			if( lstSourceRnts.get(i).equals(lstTargetRnts.get(j)) ){
	    				flagValidRnt = true;
	    			}
				}
	    		
	    		if(!flagValidRnt){
	    			return false;
	    		}else{
	    			flagValidRnt = false;
	    		}
			}
    	}else{
    		return false;
    	}
    	
			return true;
	}
	
	
	/**
	 * Gets the lis holder account detail service bean.
	 *
	 * @param filter the filter
	 * @return the lis holder account detail service bean
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountDetail> getLisHolderAccountDetailServiceBean(HolderAccountDetail filter) throws ServiceException{
		List<HolderAccountDetail> list = new ArrayList<HolderAccountDetail>();
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select had From HolderAccountDetail had join fetch had.holder ho");
		sbQuery.append(" join fetch had.holderAccount ha join fetch ha.participant pa");
		sbQuery.append(" Where 1 = 1 ");
		if(Validations.validateIsNotNullAndPositive(filter.getIdHolderAccountDetailPk())){
			sbQuery.append(" and had.idHolderAccountDetailPk = :holderAccountDetailPkPrm ");
		}
		if(Validations.validateIsNotNull(filter.getHolderAccount()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getIdHolderAccountPk())){
			sbQuery.append(" and ha.idHolderAccountPk = :HolderAccountPrm ");
		}
		if(Validations.validateIsNotNull(filter.getHolderAccount().getParticipant()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getParticipant().getIdParticipantPk()) ){
			sbQuery.append(" and pa.idParticipantPk = :idParticipantPk ");
		}
		if(Validations.validateIsNotNull(filter.getHolder()) && Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk()) ){
			sbQuery.append(" and ho.idHolderPk = :idHolderPk ");
		}
		if(Validations.validateIsNotNull(filter.getHolderAccount()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getStateAccount())){
			sbQuery.append(" and ha.stateAccount = :stateAccount ");
		}
		
		Query query = em.createQuery(sbQuery.toString()); 
		
		if(Validations.validateIsNotNullAndPositive(filter.getIdHolderAccountDetailPk())){
    		query.setParameter("holderAccountDetailPkPrm", filter.getIdHolderAccountDetailPk());
    	}
		if(Validations.validateIsNotNull(filter.getHolderAccount()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getIdHolderAccountPk())){
    		query.setParameter("HolderAccountPrm", filter.getHolderAccount().getIdHolderAccountPk());
    	}
		if(Validations.validateIsNotNull(filter.getHolderAccount().getParticipant()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getParticipant().getIdParticipantPk()) ){
			query.setParameter("idParticipantPk", filter.getHolderAccount().getParticipant().getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(filter.getHolder()) && Validations.validateIsNotNullAndPositive(filter.getHolder().getIdHolderPk()) ){
				query.setParameter("idHolderPk", filter.getHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNull(filter.getHolderAccount()) && Validations.validateIsNotNullAndPositive(filter.getHolderAccount().getStateAccount())){
			query.setParameter("stateAccount", filter.getHolderAccount().getStateAccount());
		}
		
		list = query.getResultList();
		if(Validations.validateIsNull(list)){
			 list = new ArrayList<HolderAccountDetail>();
		}
		return list;
	}
	
	
	/**
	 * Count available balances service bean.
	 *
	 * @param filters the filters
	 * @return the integer
	 * @throws ServiceException the service exception
	 */
	public Integer countAvailableBalancesServiceBean(Map<String,Object> filters) throws ServiceException{

		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append(" select count(hab.id.idSecurityCodePk) as balCount from HolderAccountBalance hab ");
		stringBuilderSql.append(" inner join hab.participant p ");
		stringBuilderSql.append(" inner join hab.holderAccount ha ");
		stringBuilderSql.append(" Where 1 = 1 ");
		stringBuilderSql.append(" and hab.totalBalance > 0 ");
		stringBuilderSql.append(" and hab.availableBalance > 0 ");
		
		if(filters.get("idParticipantPk")!=null){
			stringBuilderSql.append("and hab.id.idParticipantPk = :idParticipantPk ");
		}
		if(filters.get("idHolderAccountPk")!=null){
			stringBuilderSql.append("and hab.id.idHolderAccountPk = :idHolderAccountPk ");
		}
  
		Query query = em.createQuery(stringBuilderSql.toString());
		
		if(filters.get("idParticipantPk")!=null){
			query.setParameter("idParticipantPk", filters.get("idParticipantPk"));
		}
		if(filters.get("idHolderAccountPk")!=null){
			query.setParameter("idHolderAccountPk", filters.get("idHolderAccountPk"));
		}
		
		int count = ((Long) query.getSingleResult()).intValue();
		
		return count;
	}
	
	
	/**
	 * List holder account balances service bean.
	 *
	 * @param filters the filters
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Object[]> listHolderAccountBalancesServiceBean(Map<String,Object> filters) throws ServiceException{
		List<Object[]> holderAccountBalancesList = new ArrayList<Object[]>();
		StringBuilder stringBuilderSql = new StringBuilder();
		
		stringBuilderSql.append(" SELECT hab, s.idSecurityCodePk, s.description, s.securityClass , s.cfiCode , s.idIsinCode, ");
		stringBuilderSql.append("  ( ");
		stringBuilderSql.append("  select nvl(sum(hao.stockQuantity),0) from ");
		stringBuilderSql.append("  HolderAccountOperation hao inner join hao.mechanismOperation mo ");
		stringBuilderSql.append("  where hao.role = :idRole ");
		stringBuilderSql.append("  and hao.operationPart = :idOperationPart ");
		stringBuilderSql.append("  and hao.stockReference is null ");				
		stringBuilderSql.append("  and mo.operationState in :operationStates ");
		stringBuilderSql.append("  and mo.securities.idSecurityCodePk =  hab.id.idSecurityCodePk ");
		stringBuilderSql.append("  and hao.holderAccount.idHolderAccountPk = ha.id.idHolderAccountPk ");
		stringBuilderSql.append(" ), s.stateSecurity ");
		stringBuilderSql.append(" FROM HolderAccountBalance hab ");
		stringBuilderSql.append(" inner join fetch hab.participant p ");
		stringBuilderSql.append(" inner join hab.security s ");
		stringBuilderSql.append(" inner join fetch hab.holderAccount ha ");
		stringBuilderSql.append(" Where hab.totalBalance > 0 ");
		if(filters.get("idParticipantPk")!=null){
			stringBuilderSql.append("and hab.id.idParticipantPk = :idParticipant ");
		}
		if(filters.get("idHolderAccountPk")!=null){
			stringBuilderSql.append("and hab.id.idHolderAccountPk = :idHolderAccount ");
		}
		if(filters.get("idIsinCodePk")!=null){
			stringBuilderSql.append("and hab.id.idSecurityCodePk in :idSecurityCode ");
		}
		if(filters.get("securityClass") != null) {
			stringBuilderSql.append("and s.securityClass = :securityClass ");
		}
		if(filters.get("numberSecuritiesShow") != null && !filters.get("numberSecuritiesShow").equals("0")) {
			stringBuilderSql.append("and rownum <= :numberSecuritiesShow ");
		}
  
		Query query = em.createQuery(stringBuilderSql.toString());
		if(filters.get("idParticipantPk")!=null){
			query.setParameter("idParticipant", filters.get("idParticipantPk"));
		}
		if(filters.get("idHolderAccountPk")!=null){
			query.setParameter("idHolderAccount", filters.get("idHolderAccountPk"));
		}
		if(filters.get("idIsinCodePk")!=null){
			query.setParameter("idSecurityCode", filters.get("idSecurityCodePk"));
		}
		if(filters.get("securityClass") != null) {
			query.setParameter("securityClass", filters.get("securityClass"));
		}
		if(filters.get("numberSecuritiesShow") != null && !filters.get("numberSecuritiesShow").equals("0")) {
			query.setParameter("numberSecuritiesShow", Long.parseLong((String) filters.get("numberSecuritiesShow")));
		}
		
		query.setParameter("idRole", GeneralConstants.TWO_VALUE_INTEGER.intValue());
		query.setParameter("idOperationPart", GeneralConstants.ONE_VALUE_INTEGER.intValue());
		List<Integer> states = new ArrayList<Integer>();
		states.add(MechanismOperationStateType.REGISTERED_STATE.getCode());
		states.add(MechanismOperationStateType.ASSIGNED_STATE.getCode());
		query.setParameter("operationStates", states );
		
		holderAccountBalancesList = (List<Object[]>)query.getResultList();
		return holderAccountBalancesList;
	}
	
	/**
	 * Gets the exist balance for account.
	 *
	 * @param filter the filter
	 * @return the exist balance for account
	 * @throws ServiceException the service exception
	 */
	public Integer getExistBalanceForAccount(HolderAccountBalance filter) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select count(HAB.availableBalance) " + 
					   " From HolderAccountBalance HAB ");
		sbQuery.append(" Where HAB.id.idHolderAccountPk = :holderAccountPkPrm ");
		if(Validations.validateIsNotNullAndNotEmpty(filter.getId().getIdParticipantPk())){
			sbQuery.append(" and HAB.id.idParticipantPk = :participantPkPrm ");
		}		
		sbQuery.append(" and HAB.availableBalance >0 ");		
		Query query = em.createQuery(sbQuery.toString()); 
		query.setParameter("holderAccountPkPrm", filter.getId().getIdHolderAccountPk());
		if(Validations.validateIsNotNullAndNotEmpty(filter.getId().getIdParticipantPk())){
			query.setParameter("participantPkPrm", filter.getId().getIdParticipantPk());
		}			
		return ((Long)query.getSingleResult()).intValue() ;
			
	}
	
	/**
	 * Gets the holderAccount by account number
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param idParticipant the id participant
	 * @param accountNumber the account number
	 * @param accountType the account type
	 * @return the id holder account
	 */
	public HolderAccount getIdHolderAccountByAccountNumber(Long idParticipant, Integer accountNumber){
		StringBuilder jpqlQuery = new StringBuilder();
		jpqlQuery.append(" select ha from HolderAccount ha ");
		jpqlQuery.append(" where ha.participant.idParticipantPk= :idParticipant ");
		jpqlQuery.append(" and ha.accountNumber = :accountNumber ");
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("idParticipant", idParticipant);
		query.setParameter("accountNumber", accountNumber);
		try {
			return (HolderAccount) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets holder marketfact balance
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param idParticipant the id participant
	 * @param accountNumber the account number
	 * @param accountType the account type
	 * @return the id holder account
	 */
	public HolderMarketFactBalance getHolderMarketFactBalance(HolderAccountBalance objHolAccBalance,AccountAnnotationMarketFact objAccAnnMarketFact){
		StringBuilder jpqlQuery = new StringBuilder();
		jpqlQuery.append(" select habal ");
		jpqlQuery.append(" from HolderMarketFactBalance habal ");
		jpqlQuery.append(" where 1=1 ");
		jpqlQuery.append(" and habal.holderAccount.idHolderAccountPk = :parIdHolderAccount ");
		jpqlQuery.append(" and habal.participant.idParticipantPk = :parIdParticipant   ");	
		jpqlQuery.append(" and habal.security.idSecurityCodePk = :parIdSecurityCode  ");
		jpqlQuery.append(" and trunc(habal.marketDate) = trunc(:parMarketDate)  ");
		jpqlQuery.append(" and habal.marketPrice = :parMarketPrice  ");
		jpqlQuery.append(" and habal.marketRate = :parMarketRate  ");
		
		Query query = em.createQuery(jpqlQuery.toString());
		query.setParameter("parIdHolderAccount", objHolAccBalance.getHolderAccount().getIdHolderAccountPk());
		query.setParameter("parIdParticipant", objHolAccBalance.getParticipant().getIdParticipantPk());
		query.setParameter("parIdSecurityCode",objHolAccBalance.getSecurity().getIdSecurityCodePk());
		query.setParameter("parMarketDate", objAccAnnMarketFact.getMarketDate());
		query.setParameter("parMarketPrice", objAccAnnMarketFact.getMarketPrice());
		query.setParameter("parMarketRate",objAccAnnMarketFact.getMarketRate());
		try {
			return (HolderMarketFactBalance) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Gets the account annotation operation.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param strIdSecurityCode the str id security code
	 * @param lngIdParticipant the lng id participant
	 * @param lngIdHolderAccount the lng id holder account
	 * @return the holder account balance by retirement
	 */
	public AccountAnnotationOperation getAccountAnnotationOperation(String idSecudityCode,Long OperationNumber){
		AccountAnnotationOperation objAccountAnnotationOperation = new AccountAnnotationOperation();				
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT AAO ");
			stringBuilder.append(" FROM AccountAnnotationOperation AAO  ");
			stringBuilder.append(" WHERE 1=1 ");
			stringBuilder.append(" and AAO.security.idSecurityCodePk = :parIdSecurityCode ");
			stringBuilder.append(" and AAO.operationNumber = :parOperationNumber ");
			TypedQuery<AccountAnnotationOperation> typedQuery= em.createQuery(stringBuilder.toString(), AccountAnnotationOperation.class);
			typedQuery.setParameter("parIdSecurityCode", idSecudityCode);
			typedQuery.setParameter("parOperationNumber", OperationNumber);
			objAccountAnnotationOperation = (AccountAnnotationOperation) typedQuery.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return objAccountAnnotationOperation;
	}

	/**
	 * Gets the holder account marketfact.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param objAccountAnnotationOperation
	 * @return
	 */
	public AccountAnnotationMarketFact getHolderAccountMarketFact(AccountAnnotationOperation objAccountAnnotationOperation){
		AccountAnnotationMarketFact objAccountAnnotationMarketFact = new AccountAnnotationMarketFact();				
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" SELECT AAMF ");
			stringBuilder.append(" FROM AccountAnnotationMarketFact AAMF ");
			stringBuilder.append(" WHERE 1=1 ");
			stringBuilder.append(" and AAMF.accountAnnotationOperation.idAnnotationOperationPk = :parIdAnnotationOperation ");
			TypedQuery<AccountAnnotationMarketFact> typedQuery= em.createQuery(stringBuilder.toString(), AccountAnnotationMarketFact.class);
			typedQuery.setParameter("parIdAnnotationOperation", objAccountAnnotationOperation.getIdAnnotationOperationPk());
			objAccountAnnotationMarketFact = (AccountAnnotationMarketFact) typedQuery.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return objAccountAnnotationMarketFact;
	}
	
	/**
	 * Gets the list holder account balance by idHolderAccount.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param idParticipant the id participant
	 * @param idSecudityCode the id secudity code
	 * @param isReversal the is reversal
	 * @return the list holder account balance
	 */
	public List<HolderAccountBalance> getListHolderAccountBalanceByIdHolderAccount(Long idParticipant, String idSecudityCode, Long idHolderAccount, boolean isReversal) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" SELECT HAB ");
		stringBuilder.append(" FROM HolderAccountBalance HAB ");
		stringBuilder.append(" WHERE 1=1 ");
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			stringBuilder.append(" and HAB.participant.idParticipantPk = :idParticipant ");
		}
		stringBuilder.append(" and HAB.security.idSecurityCodePk = :idSecudityCode ");
		stringBuilder.append(" and HAB.holderAccount.idHolderAccountPk = :idHolderAccount ");
		if(!isReversal){
			stringBuilder.append(" and HAB.totalBalance > 0 ");
		}		
		TypedQuery<HolderAccountBalance> typedQuery= em.createQuery(stringBuilder.toString(), HolderAccountBalance.class);
		if(Validations.validateIsNotNullAndNotEmpty(idParticipant)){
			typedQuery.setParameter("idParticipant", idParticipant);
		}		
		typedQuery.setParameter("idSecudityCode", idSecudityCode);		
		typedQuery.setParameter("idHolderAccount", idHolderAccount);		
		return typedQuery.getResultList();
	}
	
	/**
	 * The method is used to get list validate HolderAccountMovement not placement.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param idParticipant Long
	 * @param idHolderAccount Long
	 * @param idSecurityCode String
	 * @param lstIdCustodyOperation the lst id custody operation
	 * @return the validate holder account movement
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<HolderAccountMovement> getValidateHolderAccountMovementNotPlacement(Long idParticipant, Long idHolderAccount, 
			String idSecurityCode, List<Long> lstIdCustodyOperation) throws ServiceException{
		
		List<Long> lstIdMovementType = new ArrayList<Long>();	
		//Adicionando ingreso de acv por desmaterializacion por compra directa bcb
		lstIdMovementType.add(new Long(300716));
		//Adicionado ingreso de acv por desmaterializacion por compra tesoro directo tgn
		lstIdMovementType.add(new Long(300717));
		//Adicionando salida por reversion de valores
		lstIdMovementType.add(new Long(200066));
		
		List<HolderAccountMovement> lstHolderAccountMovement = new ArrayList<HolderAccountMovement>();		
		StringBuilder jpqlQuery = new StringBuilder();		
		jpqlQuery.append(" 	SELECT ham FROM HolderAccountMovement ham												");
		jpqlQuery.append("	WHERE ham.holderAccountBalance.participant.idParticipantPk = :parIdParticipant			");
		jpqlQuery.append(" 	AND ham.holderAccountBalance.holderAccount.idHolderAccountPk = :parIdHolderAccount		");
		jpqlQuery.append(" 	AND ham.movementType.idMovementTypePk not in (:parlstIdMovementType)");
		jpqlQuery.append(" 	AND ham.holderAccountBalance.security.idSecurityCodePk = :parIdSecurityCode 			");
		jpqlQuery.append(" 	AND (ham.custodyOperation.idCustodyOperationPk not in (:parlstIdCustodyOperation)		");
		jpqlQuery.append(" 	OR ham.custodyOperation.idCustodyOperationPk is null )									");
		Query queryJpql = em.createQuery(jpqlQuery.toString());		
		queryJpql.setParameter("parIdParticipant", idParticipant);
		queryJpql.setParameter("parIdHolderAccount", idHolderAccount);
		queryJpql.setParameter("parIdSecurityCode", idSecurityCode);
		queryJpql.setParameter("parlstIdCustodyOperation", lstIdCustodyOperation);
		queryJpql.setParameter("parlstIdMovementType", lstIdMovementType);
		try{
			lstHolderAccountMovement = queryJpql.getResultList();			
		}catch(NoResultException ex){
			return null;
		}		
		return lstHolderAccountMovement;
	}
	
	/**
	 * Find securities by description for bbx and btx.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RRC
	 * @param securitySearchTO the security search to
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Security> findSecuritiesByDescriptionForBbxBtx(SecuritySearchTO securitySearchTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT 	se");
		sbQuery.append(" FROM Security se");
		sbQuery.append(" WHERE se.description like :description");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("description", GeneralConstants.PERCENTAGE_STRING + 
											securitySearchTO.getDescription() + 
											GeneralConstants.PERCENTAGE_STRING);
		List<Security> lstResult = (List<Security>)query.getResultList();
		return lstResult;
	}
	
	/**
	 * Gets the list interface transaction by obligation number.
	 * Issue 1239 Desarrollo de Servicios Web BCB Venta Directa RCO
	 * @param idParticipant the id participant
	 * @param obligationNumber the obligation number
	 * @param interfaceCode the interface code
	 * @return the list interface transaction
	 */
	public List<InterfaceTransaction> getListInterfaceTransactionByObligationNumber(Long idParticipant, String obligationNumber, String interfaceCode) {
		StringBuffer stringBuffer= new StringBuffer();
		stringBuffer.append(" SELECT IT ");
		stringBuffer.append(" FROM InterfaceTransaction IT ");
		stringBuffer.append(" WHERE IT.participant.idParticipantPk = :idParticipant ");
		stringBuffer.append(" and IT.obligationNumber = :obligationNumber ");
		stringBuffer.append(" and IT.interfaceProcess.idExternalInterfaceFk.interfaceName = :interfaceCode ");
		stringBuffer.append(" and IT.transactionState = :transactionState ");
		
		TypedQuery<InterfaceTransaction> typedQuery= em.createQuery(stringBuffer.toString(), InterfaceTransaction.class);
		typedQuery.setParameter("idParticipant", idParticipant);
		typedQuery.setParameter("obligationNumber", obligationNumber);
		typedQuery.setParameter("interfaceCode", interfaceCode);
		typedQuery.setParameter("transactionState", BooleanType.YES.getCode());
		
		return typedQuery.getResultList();
	}
}
