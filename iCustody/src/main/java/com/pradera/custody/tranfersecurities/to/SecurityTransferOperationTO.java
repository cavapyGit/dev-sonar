package com.pradera.custody.tranfersecurities.to;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.pradera.model.component.OperationTypeCascade;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SecurityTransferOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class SecurityTransferOperationTO implements Serializable {
	
	
	/** The id isin code pk. */
	private String idIsinCodePk;			//SecurityTransferOperation (reference Security)
	
	/** The id security code pk. */
	private String idSecurityCodePk;		//SecurityTransferOperation (reference Security)
	
	/** The Account number. */
	private Integer AccountNumber;	//HolderAccount
	
	/** The source account number. */
	private Integer sourceAccountNumber;	//HolderAccount
	
	/** The target account number. */
	private Integer targetAccountNumber;	//HolderAccount
	
	/** The source id participant pk. */
	private Long sourceIdParticipantPk;		//SecurityTransferOperation	(reference Participant)
	
	/** The target id participant pk. */
	private Long targetIdParticipantPk;		//SecurityTransferOperation	(reference Participant)
	
	/** The id participant pk. */
	private Long idParticipantPk;		//SecurityTransferOperation	(reference Participant)
	
	/** The transfer type. */
	private Integer transferType;			//SecurityTransferOperation
	
	/** The operation init date. */
	private Date operationInitDate;			//CustodyOperation
	
	/** The operation end date. */
	private Date operationEndDate;			//CustodyOperation
	
	/** The operation number. */
	private Long operationNumber;			//CustodyOperation
	
	/** The operation type. */
	private Long operationType;				//CustodyOperation
	
	/** The state. */
	private Integer state;						//SecurityTransferOperation
	
	/** The action. */
	private Integer action;							//Action
	
	/** The balance type. */
	private Integer balanceType;
	
	/** The ind block. */
	private Integer indBlock;
	
	//opcionales
	/** The id custody operation pk. */
	private Long idCustodyOperationPk;		//CustodyOperation	= pk_SecurityTransferOperation 
	
	/** The source id holder account pk. */
	private Long sourceIdHolderAccountPk;	//SecurityTransferOperation	(reference HolderAccount)
	
	/** The target id holder account pk. */
	private Long targetIdHolderAccountPk;	//SecurityTransferOperation	(reference HolderAccount)
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;	//SecurityTransferOperation	(reference HolderAccount)
	
	/** The states. */
	private List<Integer> states;
	
	/** The list operation type pk. */
	private List<OperationTypeCascade> listOperationTypePk;
	
	/** The operation date. */
	private Date operationDate;
	
	/** The map transfer state. */
	private Map<String, Integer> mapTransferState = new HashMap<String, Integer>();
	
	/**
	 * Gets the states.
	 *
	 * @return the states
	 */
	public List<Integer> getStates() {
		return states;
	}
	
	/**
	 * Sets the states.
	 *
	 * @param states the states to set
	 */
	public void setStates(List<Integer> states) {
		this.states = states;
	}
	
	/**
	 * Gets the balance type.
	 *
	 * @return the balance type
	 */
	public Integer getBalanceType() {
		return balanceType;
	}
	
	/**
	 * Sets the balance type.
	 *
	 * @param balanceType the new balance type
	 */
	public void setBalanceType(Integer balanceType) {
		this.balanceType = balanceType;
	}
	
	/**
	 * Gets the id isin code pk.
	 *
	 * @return the id isin code pk
	 */
	public String getIdIsinCodePk() {
		return idIsinCodePk;
	}
	
	/**
	 * Sets the id isin code pk.
	 *
	 * @param idIsinCodePk the new id isin code pk
	 */
	public void setIdIsinCodePk(String idIsinCodePk) {
		this.idIsinCodePk = idIsinCodePk;
	}
	
	/**
	 * Gets the source account number.
	 *
	 * @return the source account number
	 */
	public Integer getSourceAccountNumber() {
		return sourceAccountNumber;
	}
	
	/**
	 * Sets the source account number.
	 *
	 * @param sourceAccountNumber the new source account number
	 */
	public void setSourceAccountNumber(Integer sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}
	
	/**
	 * Gets the target account number.
	 *
	 * @return the target account number
	 */
	public Integer getTargetAccountNumber() {
		return targetAccountNumber;
	}
	
	/**
	 * Sets the target account number.
	 *
	 * @param targetAccountNumber the new target account number
	 */
	public void setTargetAccountNumber(Integer targetAccountNumber) {
		this.targetAccountNumber = targetAccountNumber;
	}
	
	/**
	 * Gets the source id participant pk.
	 *
	 * @return the source id participant pk
	 */
	public Long getSourceIdParticipantPk() {
		return sourceIdParticipantPk;
	}
	
	/**
	 * Sets the source id participant pk.
	 *
	 * @param sourceIdParticipantPk the new source id participant pk
	 */
	public void setSourceIdParticipantPk(Long sourceIdParticipantPk) {
		this.sourceIdParticipantPk = sourceIdParticipantPk;
	}
	
	/**
	 * Gets the target id participant pk.
	 *
	 * @return the target id participant pk
	 */
	public Long getTargetIdParticipantPk() {
		return targetIdParticipantPk;
	}
	
	/**
	 * Sets the target id participant pk.
	 *
	 * @param targetIdParticipantPk the new target id participant pk
	 */
	public void setTargetIdParticipantPk(Long targetIdParticipantPk) {
		this.targetIdParticipantPk = targetIdParticipantPk;
	}
	
	/**
	 * Gets the transfer type.
	 *
	 * @return the transfer type
	 */
	public Integer getTransferType() {
		return transferType;
	}
	
	/**
	 * Sets the transfer type.
	 *
	 * @param transferType the new transfer type
	 */
	public void setTransferType(Integer transferType) {
		this.transferType = transferType;
	}
	
	/**
	 * Gets the operation init date.
	 *
	 * @return the operation init date
	 */
	public Date getOperationInitDate() {
		return operationInitDate;
	}
	
	/**
	 * Sets the operation init date.
	 *
	 * @param operationInitDate the new operation init date
	 */
	public void setOperationInitDate(Date operationInitDate) {
		this.operationInitDate = operationInitDate;
	}
	
	/**
	 * Gets the operation end date.
	 *
	 * @return the operation end date
	 */
	public Date getOperationEndDate() {
		return operationEndDate;
	}
	
	/**
	 * Sets the operation end date.
	 *
	 * @param operationEndDate the new operation end date
	 */
	public void setOperationEndDate(Date operationEndDate) {
		this.operationEndDate = operationEndDate;
	}
	
	/**
	 * Gets the operation number.
	 *
	 * @return the operation number
	 */
	public Long getOperationNumber() {
		return operationNumber;
	}
	
	/**
	 * Sets the operation number.
	 *
	 * @param operationNumber the new operation number
	 */
	public void setOperationNumber(Long operationNumber) {
		this.operationNumber = operationNumber;
	}
	
	/**
	 * Gets the operation type.
	 *
	 * @return the operation type
	 */
	public Long getOperationType() {
		return operationType;
	}
	
	/**
	 * Sets the operation type.
	 *
	 * @param operationType the new operation type
	 */
	public void setOperationType(Long operationType) {
		this.operationType = operationType;
	}
	
	/**
	 * Gets the id custody operation pk.
	 *
	 * @return the id custody operation pk
	 */
	public Long getIdCustodyOperationPk() {
		return idCustodyOperationPk;
	}
	
	/**
	 * Sets the id custody operation pk.
	 *
	 * @param idCustodyOperationPk the new id custody operation pk
	 */
	public void setIdCustodyOperationPk(Long idCustodyOperationPk) {
		this.idCustodyOperationPk = idCustodyOperationPk;
	}
	
	/**
	 * Gets the source id holder account pk.
	 *
	 * @return the source id holder account pk
	 */
	public Long getSourceIdHolderAccountPk() {
		return sourceIdHolderAccountPk;
	}
	
	/**
	 * Sets the source id holder account pk.
	 *
	 * @param sourceIdHolderAccountPk the new source id holder account pk
	 */
	public void setSourceIdHolderAccountPk(Long sourceIdHolderAccountPk) {
		this.sourceIdHolderAccountPk = sourceIdHolderAccountPk;
	}
	
	/**
	 * Gets the target id holder account pk.
	 *
	 * @return the target id holder account pk
	 */
	public Long getTargetIdHolderAccountPk() {
		return targetIdHolderAccountPk;
	}
	
	/**
	 * Sets the target id holder account pk.
	 *
	 * @param targetIdHolderAccountPk the new target id holder account pk
	 */
	public void setTargetIdHolderAccountPk(Long targetIdHolderAccountPk) {
		this.targetIdHolderAccountPk = targetIdHolderAccountPk;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public Integer getAction() {
		return action;
	}
	
	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(Integer action) {
		this.action = action;
	}
	
	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return AccountNumber;
	}
	
	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		AccountNumber = accountNumber;
	}
	
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	
	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	
	/**
	 * Gets the ind block.
	 *
	 * @return the indBlock
	 */
	public Integer getIndBlock() {
		return indBlock;
	}
	
	/**
	 * Sets the ind block.
	 *
	 * @param indBlock the indBlock to set
	 */
	public void setIndBlock(Integer indBlock) {
		this.indBlock = indBlock;
	}
	
	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	
	/**
	 * Gets the id security code pk.
	 *
	 * @return the id security code pk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	
	/**
	 * Sets the id security code pk.
	 *
	 * @param idSecurityCodePk the new id security code pk
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * Gets the list operation type pk.
	 *
	 * @return the list operation type pk
	 */
	public List<OperationTypeCascade> getListOperationTypePk() {
		return listOperationTypePk;
	}

	/**
	 * Sets the list operation type pk.
	 *
	 * @param listOperationTypePk the new list operation type pk
	 */
	public void setListOperationTypePk(
			List<OperationTypeCascade> listOperationTypePk) {
		this.listOperationTypePk = listOperationTypePk;
	}

	/**
	 * Gets the map transfer state.
	 *
	 * @return the map transfer state
	 */
	public Map<String, Integer> getMapTransferState() {
		return mapTransferState;
	}

	/**
	 * Sets the map transfer state.
	 *
	 * @param mapTransferState the map transfer state
	 */
	public void setMapTransferState(Map<String, Integer> mapTransferState) {
		this.mapTransferState = mapTransferState;
	}

	/**
	 * Gets the operation date.
	 *
	 * @return the operation date
	 */
	public Date getOperationDate() {
		return operationDate;
	}

	/**
	 * Sets the operation date.
	 *
	 * @param operationDate the new operation date
	 */
	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	
}
