package com.pradera.custody.tranfersecurities.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransferSecurityDataModel.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
public class TransferSecurityDataModel extends ListDataModel<SecurityTransferOperation> implements SelectableDataModel<SecurityTransferOperation>,Serializable{

	/**
	 * Instantiates a new transfer security data model.
	 */
	public TransferSecurityDataModel() {
			
	}
	
	/**
	 * Instantiates a new transfer security data model.
	 *
	 * @param ListSecurityTransferOperation the list security transfer operation
	 */
	public TransferSecurityDataModel(List<SecurityTransferOperation> ListSecurityTransferOperation) {
		super(ListSecurityTransferOperation);
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowData(java.lang.String)
	 */
	@Override
	public SecurityTransferOperation getRowData(String rowKey) {
		// TODO Auto-generated method stub
		List<SecurityTransferOperation> ListSecurityTransferOperation = (List<SecurityTransferOperation>)getWrappedData();
		for(SecurityTransferOperation SecurityTransferOperation : ListSecurityTransferOperation){
			if(SecurityTransferOperation.getCustodyOperation().getIdCustodyOperationPk().toString().equals(rowKey))
				return SecurityTransferOperation;
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.primefaces.model.SelectableDataModel#getRowKey(java.lang.Object)
	 */
	@Override
	public Object getRowKey(SecurityTransferOperation securityTransferOperation) {//Return Object
		// TODO Auto-generated method stub
		return securityTransferOperation.getCustodyOperation().getIdCustodyOperationPk();
	}

}
