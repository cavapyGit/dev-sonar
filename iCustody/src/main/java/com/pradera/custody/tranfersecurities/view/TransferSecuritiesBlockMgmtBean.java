package com.pradera.custody.tranfersecurities.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.core.component.helperui.to.SecuritiesSearcherTO;
import com.pradera.core.component.helperui.view.SecuritiesPartHolAccount;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.affectation.facade.RequestAffectationServiceFacade;
import com.pradera.custody.reversals.facade.RequestReversalsServiceFacade;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.custody.tranfersecurities.to.SecurityBlockTransferTO;
import com.pradera.custody.tranfersecurities.to.SecurityTransferOperationTO;
import com.pradera.custody.transfersecurities.participantunification.facade.ParticipantUnificationServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.HolderAccountBalancePK;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.transfersecurities.SecurityTransferOperation;
import com.pradera.model.custody.type.SecuritiesTransferType;
import com.pradera.model.custody.type.TransferSecuritiesStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class TransferSecuritiesBlockMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class TransferSecuritiesBlockMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant log. */
	/*@Inject 
	PraderaLogger log;*/
	/** The general parameters facade. */

	@Inject
	SecuritiesPartHolAccount securitiesHelperAccBean;

	/** The user info. */
	@Inject
	UserInfo userInfo;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;

	/** The securities part hol account. */
	@Inject
	SecuritiesPartHolAccount securitiesPartHolAccount;

	/** The max days of custody request. */
	@Inject
	@Configurable
	Integer maxDaysOfCustodyRequest;
	/** The general pameters facade. */
	
	@EJB
	ParticipantUnificationServiceFacade unificationServiceFacade;

	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade generalParametersFacade;

	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;

	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;

	/** The transfer securities facade. */
	@EJB
	TransferSecuritiesServiceFacade transferSecuritiesFacade;
	
	/** The request reversals service facade. */
	@EJB
	RequestReversalsServiceFacade requestReversalsServiceFacade;

	/** The request affectation service facade. */
	@EJB
	RequestAffectationServiceFacade requestAffectationServiceFacade;

	/** The transfer available facade. */
	@EJB
	TransferSecuritiesServiceFacade transferAvailableFacade;

	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;

	/** The security to. */
	private SecurityTransferOperationTO securityTO = new SecurityTransferOperationTO();
	/* END TRANSFER SECURITIES BLOCK */
	/** The participant origin. */
	private Participant participantOrigin = new Participant();
	
	/** The participant target. */
	private Participant participantTarget = new Participant();
	
	/** The participant. */
	private Participant participant = new Participant();

	/** The security origin. */
	private Security securityOrigin = new Security();
	
	/** The security. */
	private Security security = new Security();
	
	/** The holder account origin. */
	private HolderAccount holderAccountOrigin = new HolderAccount();
	
	/** The holder account target. */
	private HolderAccount holderAccountTarget = new HolderAccount();
	
	/** The holder account. */
	private HolderAccount holderAccount = new HolderAccount();
	/* INIT SEARCH */
	/** The lst transfer type. */
	private List<ParameterTable> lstTransferType = new ArrayList<ParameterTable>();
	
	/** The lst participant. */
	private List<Participant> lstParticipant = new ArrayList<Participant>();
	
	
	/** The lst target participant. */
	private List<Participant> lstTargetParticipant;
	
	/** The lst transfer state. */
	private List<ParameterTable> lstTransferState = new ArrayList<ParameterTable>();
	
	/** The map state security transfer operation. */
	private Map<Integer, String> mapStateSecurityTransferOperation = new HashMap<Integer,String>();
	
	/** The lst block type. */
	private List<ParameterTable> lstBlockType = new ArrayList<ParameterTable>();
	
	/** The lst transfer actions. */
	private List<ParameterTable> lstTransferActions = new ArrayList<ParameterTable>();
	/* END SEARCH */
	/** The lst security block transfer to. */
	private List<SecurityBlockTransferTO> lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
	
	/** The lst security block transfer to view. */
	private List<SecurityBlockTransferTO> lstSecurityBlockTransferTOView = new ArrayList<SecurityBlockTransferTO>();
	
	/** The lst new security block transfer to. */
	private List<SecurityBlockTransferTO> lstNewSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
	
	/** The lst action security block transfer to. */
	private List<SecurityBlockTransferTO> lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
	
	/** The arr security block transfer. */
	private SecurityBlockTransferTO[] arrSecurityBlockTransfer;
	
	/** The security block transfertmp. */
	private SecurityBlockTransferTO securityBlockTransfertmp;
	
	/** The data model security block transfer. */
	private SecurityBlockTransferDataModel dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel();
	
	/** The data model security block transfer view. */
	private SecurityBlockTransferDataModel dataModelSecurityBlockTransferView = new SecurityBlockTransferDataModel();
	
	/** The security view transfer to. */
	private SecurityBlockTransferTO securityViewTransferTo = new SecurityBlockTransferTO();
	
	/** The now time. */
	private Date nowTime = new Date();
	
	/** The min time. */
	private Date minTime = new Date();
	
	/** The radio selected. */
	private Integer radioSelected = 1;

	/** The validation message. */
	private String validationMessage = "";
	
	/** The str transfer type. */
	private String strTransferType = "";
	
	/** The flag search enabled. */
	private boolean flagSearchEnabled = true;
	
	/** The flag participante investor. */
	private boolean flagParticipanteInvestor = false;
	
	/** The flag account origin visible. */
	private boolean flagAccountOriginVisible = false;
	
	/** The flag account target visible. */
	private boolean flagAccountTargetVisible = false;

	/** The reject motive list. */
	private List<ParameterTable> rejectMotiveList;
	
	/** The other reject motive id. */
	private Integer otherRejectMotiveId;
	
	/** The int reject motive. */
	private Integer intRejectMotive;
	
	/** The str reject motive. */
	private String strRejectMotive;

	/* flag actions */
	/** The flag revised sto. */
	boolean flagRevisedSto = false;
	
	/** The flag reject sto. */
	boolean flagRejectSto = false;
	
	/** The flag approved sto. */
	boolean flagApprovedSto = false;
	
	/** The flasg annuled sto. */
	boolean flasgAnnuledSto = false;
	
	/** The flag confirm sto. */
	boolean flagConfirmSto = false;
	
	/** The flag register. */
	boolean flagRegister = false;
	
	/** The flag search. */
	boolean flagSearch = false;

	/** The registers found. */
	private Boolean registersFound = true;

	/** The str logger user. */
	private String strLoggerUser = "NONE";
	
	/** The is participant. */
	private boolean isParticipant = false;
	
	/** The is depositary. */
	private boolean isDepositary = false;
	
	/** The str document number. */
	private String strDocumentNumber;
	
	/** The str sate description. */
	private String strSateDescription;

	/** ** REFACTOR *******. */
	private List<ParameterTable> lstInstrumentType = new ArrayList<ParameterTable>();

	/** The lst security class. */
	private List<ParameterTable> lstSecurityClass = new ArrayList<ParameterTable>();
	
	/** The participant login. */
	private Long participantLogin = new Long(0);
	
	/** The Defaultparticipant. */
	private Participant Defaultparticipant = new Participant();

	/** The flag source target. */
	boolean flagSourceTarget = true;
	
	/** The flag same participant. */
	private boolean flagSameParticipant = false;
	
	/** The confirm alert action. */
	private String confirmAlertAction = "";

	/** The security transfer operation. */
	private SecurityTransferOperation securityTransferOperation = new SecurityTransferOperation();
	
	/** The custody operation. */
	private CustodyOperation custodyOperation = new CustodyOperation();
	
	/** The block operation. */
	private BlockOperation blockOperation = new BlockOperation();
	
	/** The block request. */
	private BlockRequest blockRequest = new BlockRequest();
	
	/** The flag register origin participant. */
	private boolean flagRegisterOriginParticipant = false;
	
	/** The flag register target participant. */
	private boolean flagRegisterTargetParticipant = false;
	
	/** The flag ind market fact. */
	boolean flagIndMarketFact = false;
	
	/** The int hidden motive. */
	private Integer intHiddenMotive = 0;
	
	/** The int other reject motive. */
	private Integer intOtherRejectMotive = 1718;
	
	/** helpers *. */
	private Holder sourceHolder = new Holder();
	
	/** The target holder. */
	private Holder targetHolder = new Holder();

	/** The source account to. */
	private HolderAccountHelperResultTO sourceAccountTo = new HolderAccountHelperResultTO();
	
	/** The target account to. */
	private HolderAccountHelperResultTO targetAccountTo = new HolderAccountHelperResultTO();
	
	/** The lst source account to. */
	private List<HolderAccountHelperResultTO> lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	/** The lst target account to. */
	private List<HolderAccountHelperResultTO> lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
	
	/** The map state holder account. */
	Map<Integer, String> mapStateHolderAccount = new HashMap<Integer,String>();
	
	/** The lst holder account state. */
	private List<ParameterTable> lstHolderAccountState = new ArrayList<ParameterTable>();
	
	/** The view error message. */
	private String viewErrorMessage="";
	
	/** The Constant STR_APPROVE. */
	private static final String STR_APPROVE = "approve";
	
	/** The Constant STR_REJECT. */
	private static final String STR_REJECT = "reject";
	
	/** The Constant STR_REVIEW. */
	private static final String STR_REVIEW = "review";
	
	/** The Constant STR_ANNULAR. */
	private static final String STR_ANNULAR = "annular";
	
	/** The Constant STR_CONFIRM. */
	private static final String STR_CONFIRM = "confirm";
	
	/** The security class. */
	private Integer securityClass;
	
	/**
	 * Gets the security class.
	 *
	 * @return the security class
	 */
	public Integer getSecurityClass() {
		return securityClass;
	}

	/**
	 * Sets the security class.
	 *
	 * @param securityClass the new security class
	 */
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}

	/**
	 * Post Construct search.xhtml
	 *
	 * @param action the action
	 * @return the string
	 */
	
	public String propertiesConstantsAllActions(String action){
		if(action.equals("approve")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_APROVE;
		}else if(action.equals("annular")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_ANULL;
		}else if(action.equals("review")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_REVIEW;
		}else if(action.equals("reject")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_REJECT;
		}else if(action.equals("confirm")){
			return PropertiesConstants.ERROR_TRANSFER_STATE_FOR_CONFIRM;
		}
		
		return PropertiesConstants.NO_ACTION_VALID;
	}

	private List<String> validateIsValidUserPartOne(SecurityBlockTransferTO securityBlockTransfertmp) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult = null;	
		OperationUserTO operationUserTO = new OperationUserTO();
		SecurityTransferOperation securityTransferOperationTmp = null;;
		try {
			securityTransferOperationTmp = transferSecuritiesFacade.getSecurityTransferOperation(securityBlockTransfertmp.getIdTransferOperationPk());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		if(securityTransferOperationTmp != null){
			operationUserTO.setOperNumber(securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString());
			operationUserTO.setUserName(securityTransferOperationTmp.getCustodyOperation().getRegistryUser());
			lstOperationUserParam.add(operationUserTO);
			lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		}		
		return lstOperationUserResult;
	}
	private List<String> validateIsValidUserPartTwo(SecurityBlockTransferTO securityBlockTransfertmp) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult = null;	
		OperationUserTO operationUserTO = new OperationUserTO();
		SecurityTransferOperation securityTransferOperationTmp = null;;
		try {
			securityTransferOperationTmp = transferSecuritiesFacade.getSecurityTransferOperation(securityBlockTransfertmp.getIdTransferOperationPk());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		if(securityTransferOperationTmp != null){
			operationUserTO.setOperNumber(securityTransferOperationTmp.getCustodyOperation().getOperationNumber().toString());
			operationUserTO.setUserName(securityTransferOperationTmp.getCustodyOperation().getReviewUser());
			lstOperationUserParam.add(operationUserTO);
			lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		}		
		return lstOperationUserResult;
	}
	/**
	 * Execute all actions.
	 *
	 * @param action the action
	 * @return the string
	 */
	public String executeAllActions(String action) {
		  
		if( !(Validations.validateIsNotNull(securityBlockTransfertmp) && 
			  Validations.validateIsNotNull(securityBlockTransfertmp.getOperationNumber())) ){

    		String headerMessage=PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
    		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_NOSELECTDATA);
    		alert(headerMessage,bodyMessage);
    		return "";
		}
		
		if(action.equals(STR_APPROVE.toString()) || action.equals(STR_ANNULAR.toString()) ){
			List<String> lstOperationUser = validateIsValidUserPartOne(securityBlockTransfertmp);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String act = GeneralConstants.EMPTY_STRING;
				  if(action.equals(STR_ANNULAR.toString())){
					  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ANNUL;
				  }else  if(action.equals(STR_APPROVE.toString())){
					  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_APPROVE;
				  }
				  String bodyMessage = PropertiesUtilities.getMessage(act,
							new Object[]{StringUtils.join(lstOperationUser,",")});
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),bodyMessage);
					return "";
			  }
		}else if(action.equals(STR_CONFIRM.toString()) || action.equals(STR_REJECT.toString()) ){
			List<String> lstOperationUser = validateIsValidUserPartTwo(securityBlockTransfertmp);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String act = GeneralConstants.EMPTY_STRING;
				  if(action.equals(STR_CONFIRM.toString())){
					  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_CONFIRM;
				  }else  if(action.equals(STR_REJECT.toString())){
					  act = PropertiesConstants.ERROR_MESSAGE_INVALID_USER_REJECT;
				  }
				  
				  String bodyMessage = PropertiesUtilities.getMessage(act,
							new Object[]{StringUtils.join(lstOperationUser,",")});
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),bodyMessage);
					return "";
			  }
		}		
		Long operationNumber = securityBlockTransfertmp.getOperationNumber();
		String messages = PropertiesUtilities.getMessage(propertiesConstantsAllActions(action),new Object[] { operationNumber.toString() });

//		if (securityBlockTransfertmp == null) {
//			alert(PropertiesUtilities.getMessage(PropertiesConstants.LBL_WARNING_ACTION_MESSAGE));
//			return null;
//		}

		if (!(isParticipant || isDepositary)) {
			alert(PropertiesUtilities.getMessage(PropertiesConstants.NO_ACTION_VALID,new Object[] { operationNumber.toString() }));
		}

		if (action.equals("approve")) {

			if (!securityBlockTransfertmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
				alert(messages);
				return null;
			}

			if (isParticipant) {

				if (Validations.validateIsNotNull(securityBlockTransfertmp.getTargetIdParticipantPk())) {
					if (securityBlockTransfertmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())
					&& securityBlockTransfertmp.getTargetIdParticipantPk().equals(participantLogin)) {
						alert(messages);
						return null;
					}
				}
			}

		} else if (action.equals("reject")) {

			if (!(securityBlockTransfertmp.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()) 
			   || securityBlockTransfertmp.getState().equals(TransferSecuritiesStateType.REVISADO.getCode()))) {
				alert(messages);
				return null;
			}

			if (isParticipant) {

				if (Validations.validateIsNotNull(securityBlockTransfertmp.getTargetIdParticipantPk())) {
					if (securityBlockTransfertmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())
					 && securityBlockTransfertmp.getTargetIdParticipantPk().equals(participantLogin)) {
						alert(messages);
						return null;
					}
				}
			}

		} else if (action.equals("review")) {

			if (!(securityBlockTransfertmp.getState().equals(TransferSecuritiesStateType.APROBADO.getCode()))) {
				alert(messages);
				return null;
			}

			if (isParticipant) {

				if (Validations.validateIsNotNull(securityBlockTransfertmp.getSourceIdParticipantPk())) {
					if (securityBlockTransfertmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())
					 && securityBlockTransfertmp.getSourceIdParticipantPk().equals(participantLogin)) {
						alert(messages);
						return null;
					}
				}
			}

		} else if (action.equals("annular")) {

			if (!(securityBlockTransfertmp.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()))) {
				alert(messages);
				return null;
			}

			if (isParticipant) {

				if (Validations.validateIsNotNull(securityBlockTransfertmp.getSourceIdParticipantPk()) && securityBlockTransfertmp.getSourceIdParticipantPk().equals(participantLogin)) {
				}
			}

		} else if (action.equals("confirm")) {

			if (!securityBlockTransfertmp.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())) {
				alert(messages);
				return null;
			}

			if (isParticipant) {

				if (Validations.validateIsNotNull(securityBlockTransfertmp.getSourceIdParticipantPk())) {
					if (securityBlockTransfertmp.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())
					 && securityBlockTransfertmp.getSourceIdParticipantPk().equals(participantLogin)) {
						alert(messages);
						return null;
					}
				}
			}

		}

		boolean adtionalValidations = false;
		if (action.equals("annular") || action.equals("reject")) {
			adtionalValidations = true;
		}
		String redirect = postConstructorView(securityBlockTransfertmp,adtionalValidations,action);

		if (Validations.validateIsNotNull(redirect)) {
			if (!Validations.validateIsNotNullAndPositive(securityTO.getState())) {
				if (Validations.validateIsNotNullAndPositive(securityBlockTransfertmp.getState())) {
					securityTO.setState(securityBlockTransfertmp.getState());
				}
			}
			enabledButtonsWithState(securityBlockTransfertmp.getState(), action);
		}

		return redirect;
	}

	/**
	 * Setting holder origin.
	 */
	public void settingHolderOrigin() {

		if (Validations.validateIsNullOrNotPositive(holderAccountOrigin.getAccountNumber())) {
			if (Validations.validateIsNotNull(securityOrigin.getIdSecurityCodePk())) {
				securityOrigin = new Security();
				securitiesHelperAccBean.setDescription(null);
				securitiesHelperAccBean.setMnemonic(null);
			}
		}

		if (Validations.validateIsNotNullAndPositive(holderAccountOrigin.getAccountNumber())) {

			if (!holderAccountOrigin.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
				String description = holderAccountOrigin.getHolderDescription();
				Integer account = holderAccountOrigin.getAccountNumber();
				String stateDescription = (holderAccountOrigin.getStateAccount().equals(HolderAccountStatusType.BLOCK.getCode())) ? HolderAccountStatusType.BLOCK.getValue(): HolderAccountStatusType.CLOSED.getValue();
				holderAccountOrigin = new HolderAccount();
				
				String messages = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ERROR_TRANSFER_ACCOUNT_IN_STATES,
																 new Object[]{account.toString(),
																			  description.toString(),
																			  stateDescription.toString()});
				alert(messages);
				
				return;
			}

			for (HolderAccountDetail holderAccountDetail : holderAccountOrigin.getHolderAccountDetails()) {
				if (!holderAccountDetail.getHolder().getStateHolder().equals(HolderStateType.REGISTERED.getCode())) {
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
					holderAccountOrigin = new HolderAccount();
					return;
				}
			}

			if (holderAccountOrigin.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
				lstNewSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
				lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
				lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
				arrSecurityBlockTransfer = null;
				dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel();
				securityOrigin = new Security();
				SecuritiesSearcherTO secSearcherTO = new SecuritiesSearcherTO();
				secSearcherTO.setHolderAccount(holderAccountOrigin);
				secSearcherTO.setParticipantCode(participantOrigin.getIdParticipantPk());
				securitiesPartHolAccount.setSecuritiesSearcher(secSearcherTO);
			}
		}

	}

	/**
	 * Valid the Transfer type.
	 *
	 * @return the string
	 */

	public String searchWithOperationNumber() {
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
				&& Validations.validateIsNullOrNotPositive(participantOrigin.getIdParticipantPk())){
			validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
			return "";
		}
		
		if (!Validations.validateIsNotNullAndPositive(custodyOperation.getOperationNumber())) {
			custodyOperation = new CustodyOperation();
			return null;
		}

		SecurityBlockTransferTO secTransfOpe = new SecurityBlockTransferTO();

		try {
			if (isParticipant) {
				secTransfOpe.setSourceIdParticipantPk(participantLogin);
			}

			securityTO.setOperationNumber(custodyOperation.getOperationNumber());
			secTransfOpe.setOperationNumber(securityTO.getOperationNumber());
			
			lstSecurityBlockTransferTO = transferSecuritiesFacade.searchSolicitudSecurityBlockTransferTo(secTransfOpe);
			if (lstSecurityBlockTransferTO.size() > 0) {
				secTransfOpe = lstSecurityBlockTransferTO.get(0);
				enabledButtons();
				registersFound = true;
			} else {
				disabledButtons();
				secTransfOpe = new SecurityBlockTransferTO();
				lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
				registersFound = false;
				custodyOperation.setOperationNumber(null);
				securityTO.setOperationNumber(null);

				alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_REQUEST_NUMBER_NOT_EXIST));
				return null;
			}

			secTransfOpe.setStateDescription(mapStateSecurityTransferOperation.get(secTransfOpe.getState()));

			securityTO.setState(secTransfOpe.getState());

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (isParticipant) {

			if (Validations.validateIsNotNull(secTransfOpe.getTransferType())) {
				if (secTransfOpe.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())) {
					if (secTransfOpe.getTargetIdParticipantPk().equals(participantLogin)
					&& (secTransfOpe.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode()) || 
						secTransfOpe.getState().equals(TransferSecuritiesStateType.ANULADO.getCode()))) {

						disabledButtons();
						secTransfOpe = new SecurityBlockTransferTO();
						lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
						registersFound = false;
						custodyOperation.setOperationNumber(null);
						securityTO.setOperationNumber(null);
						alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_REQUEST_NUMBER_NOT_EXIST));
						return null;

					}
				}
			}

		}

		/**/
		dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel(lstSecurityBlockTransferTO);
		return null;
	}

	/**
	 * Enabled buttons.
	 */
	public void enabledButtons() {

		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (Validations.validateIsNotNullAndPositive(securityTO.getState())) {

			if (securityTO.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {

				flagApprovedSto = true;
				flasgAnnuledSto = true;
				flagRevisedSto = false;
				flagRejectSto = false;
				flagConfirmSto = false;
			} else if (securityTO.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())) {

				flagRevisedSto = true;
				flagRejectSto = true;
				flagApprovedSto = false;
				flasgAnnuledSto = false;
				flagConfirmSto = false;
			} else if (securityTO.getState().equals(TransferSecuritiesStateType.ANULADO.getCode())) {

				flagRevisedSto = false;
				flagRejectSto = false;
				flagApprovedSto = false;
				flasgAnnuledSto = false;
				flagConfirmSto = false;
			} else if (securityTO.getState().equals(TransferSecuritiesStateType.REVISADO.getCode())) {

				flagConfirmSto = true;
				flagRejectSto = true;
				flagRevisedSto = false;
				flagApprovedSto = false;
				flasgAnnuledSto = false;
			} else if (securityTO.getState().equals(TransferSecuritiesStateType.RECHAZADO.getCode())) {

				flagRevisedSto = false;
				flagRejectSto = false;
				flagApprovedSto = false;
				flasgAnnuledSto = false;
				flagConfirmSto = false;
			} else if (securityTO.getState().equals(TransferSecuritiesStateType.CONFIRMADO.getCode())) {

				flagRevisedSto = false;
				flagRejectSto = false;
				flagApprovedSto = false;
				flasgAnnuledSto = false;
				flagConfirmSto = false;
			}
		}

		if (userPrivilege.getUserAcctions().isApprove()) {
			privilegeComponent.setBtnApproveView(true);
		}

		if (userPrivilege.getUserAcctions().isAnnular()) {
			privilegeComponent.setBtnAnnularView(true);
		}

		if (userPrivilege.getUserAcctions().isReview()) {
			privilegeComponent.setBtnReview(true);
		}

		if (userPrivilege.getUserAcctions().isReject()) {
			privilegeComponent.setBtnRejectView(true);
		}

		if (userPrivilege.getUserAcctions().isConfirm()) {
			privilegeComponent.setBtnConfirmView(true);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);

	}

	/**
	 * Enabled buttons with state.
	 *
	 * @param state the state
	 * @param action the action
	 */
	public void enabledButtonsWithState(Integer state, String action) {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (state.equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
			flagApprovedSto = true;
			flasgAnnuledSto = true;
			flagRevisedSto = false;
			flagRejectSto = false;
			flagConfirmSto = false;
			if (userPrivilege.getUserAcctions().isApprove() && action.equals("approve")) {
				privilegeComponent.setBtnApproveView(true);
				flasgAnnuledSto = false;
			}

			if (userPrivilege.getUserAcctions().isAnnular() && action.equals("annular")) {
				privilegeComponent.setBtnAnnularView(true);
				flagApprovedSto = true;
			}
		} else if (state.equals(TransferSecuritiesStateType.APROBADO.getCode())) {
			flagRevisedSto = true;
			flagRejectSto = true;
			flagApprovedSto = false;
			flasgAnnuledSto = false;
			flagConfirmSto = false;
			if (userPrivilege.getUserAcctions().isReview() && action.equals("review")) {
				privilegeComponent.setBtnReview(true);
				flagRejectSto = false;
			}

			if (userPrivilege.getUserAcctions().isReject() && action.equals("reject")) {
				privilegeComponent.setBtnRejectView(true);
				flagRevisedSto = false;
			}
		} else if (state.equals(TransferSecuritiesStateType.ANULADO.getCode())) {
			flagRevisedSto = false;
			flagRejectSto = false;
			flagApprovedSto = false;
			flasgAnnuledSto = false;
			flagConfirmSto = false;
		} else if (state.equals(TransferSecuritiesStateType.REVISADO.getCode())) {
			flagConfirmSto = true;
			flagRejectSto = true;
			flagRevisedSto = false;
			flagApprovedSto = false;
			flasgAnnuledSto = false;
			if (userPrivilege.getUserAcctions().isReject() && action.equals("reject")) {
				privilegeComponent.setBtnRejectView(true);
				flagConfirmSto = false;
			}

			if (userPrivilege.getUserAcctions().isConfirm() && action.equals("confirm")) {
				privilegeComponent.setBtnConfirmView(true);
				flagRejectSto = false;
			}
		} else if (state.equals(TransferSecuritiesStateType.RECHAZADO.getCode())) {
			flagRevisedSto = false;
			flagRejectSto = false;
			flagApprovedSto = false;
			flasgAnnuledSto = false;
			flagConfirmSto = false;
		} else if (state.equals(TransferSecuritiesStateType.CONFIRMADO.getCode())) {
			flagRevisedSto = false;
			flagRejectSto = false;
			flagApprovedSto = false;
			flasgAnnuledSto = false;
			flagConfirmSto = false;
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}

	/**
	 * Search solicitud.
	 */
	public void searchSolicitud() {

		try {

			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
					&& Validations.validateIsNullOrNotPositive(participantOrigin.getIdParticipantPk())){
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
						 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
						 PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
				return;
			}
			
			SecurityBlockTransferTO securityBlockTransferTO = new SecurityBlockTransferTO();

			if (Validations.validateIsNotNullAndPositive(blockRequest.getBlockType()))
				securityBlockTransferTO.setBlockType(blockRequest.getBlockType());

			if (Validations.validateIsNotNullAndPositive(securityTransferOperation.getTransferType()))
				securityBlockTransferTO.setTransferType(securityTransferOperation.getTransferType());

			if (Validations.validateIsNotNullAndPositive(custodyOperation.getOperationNumber()))
				securityBlockTransferTO.setOperationNumber(custodyOperation.getOperationNumber());

			if (isParticipant) {
				securityBlockTransferTO.setSourceIdParticipantPk(participantLogin);
			} else {
				if (Validations.validateIsNotNullAndPositive(participantOrigin.getIdParticipantPk()))
					securityBlockTransferTO.setSourceIdParticipantPk(participantOrigin.getIdParticipantPk());
			}

			if (Validations.validateIsNotNullAndPositive(holderAccount.getAccountNumber()))
				securityBlockTransferTO.setSourceAccountNumber(holderAccount.getAccountNumber());

			if (Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk()))
				securityBlockTransferTO.setSourceIdHolderAccountPk(holderAccount.getIdHolderAccountPk());

			if (Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodePk()))
				securityBlockTransferTO.setIdSecurityCodePk(security.getIdSecurityCodePk());

			if (Validations.validateIsNotNullAndNotEmpty(securityClass))
				securityBlockTransferTO.setSecurityClass(securityClass);
			
			if (Validations.validateIsNotNullAndNotEmpty(securityTO.getState()))
				securityBlockTransferTO.setState(securityTO.getState());

			if (Validations.validateIsNotNullAndNotEmpty(securityTO.getOperationInitDate()))
				securityBlockTransferTO.setOperationInitDate(securityTO.getOperationInitDate());

			if (Validations.validateIsNotNullAndNotEmpty(securityTO.getOperationEndDate()))
				securityBlockTransferTO.setOperationEndDate(securityTO.getOperationEndDate());

			if (Validations.validateIsNotNullAndNotEmpty(securityTO.getOperationEndDate()))
				securityBlockTransferTO.setOperationEndDate(securityTO.getOperationEndDate());
			
			if (Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()))
				securityBlockTransferTO.setHolder(sourceHolder);
			
			// Buscar

			lstSecurityBlockTransferTO = transferSecuritiesFacade.searchSolicitudSecurityBlockTransferTo(securityBlockTransferTO);
			List<SecurityBlockTransferTO> lstTmpSecTransfOpe = new ArrayList<SecurityBlockTransferTO>();
			lstTmpSecTransfOpe.addAll(lstSecurityBlockTransferTO);

			if (isParticipant) {
				for (int i = 0; i < lstSecurityBlockTransferTO.size(); i++) {
					if (lstSecurityBlockTransferTO.get(i).getTargetIdParticipantPk().equals(participantLogin)
					&& lstSecurityBlockTransferTO.get(i).getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())
					|| lstSecurityBlockTransferTO.get(i).getState().equals(TransferSecuritiesStateType.ANULADO.getCode())) {

						if (lstSecurityBlockTransferTO.get(i).getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())) {

							lstTmpSecTransfOpe.remove(lstSecurityBlockTransferTO.get(i));
						}

					}
				}
			}

			lstSecurityBlockTransferTO = lstTmpSecTransfOpe;

			dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel(lstSecurityBlockTransferTO);

			if (lstSecurityBlockTransferTO.size() > 0) {
				registersFound = true;
				enabledButtons();
			} else {
				registersFound = false;
				disabledButtons();
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Search transfer block width document numbers.
	 *
	 * @param filters the filters
	 * @return the string
	 */
	public String searchTransferBlockWidthDocumentNumbers(
			SecurityBlockTransferTO filters) {
		try {
			String message = "";
			List<SecurityBlockTransferTO> lstFilter = transferSecuritiesFacade.searchDocumentNumbersWithOperation(filters, false);

			if (lstFilter.size() >= 1) {
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_BLOCKS_DOCUMENTS);
			} else {
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_BLOCK_DOCUMENT);
			}

			int j = 0;

			for (int i = 0; i < lstFilter.size(); i++) {

				if (!lstFilter.get(i).getState().equals(AffectationStateType.BLOCKED.getCode())) {
					message += (i > 0) ? " ,"
							+ lstFilter.get(i).getDocumentNumber() : " "
							+ lstFilter.get(i).getDocumentNumber();
					j++;
				}
			}

			if (lstFilter.size() > 1) {
				message += " "+PropertiesUtilities.getMessage(PropertiesConstants.MSG_STATE_INVALID);
				
			} else {
				message += " "+PropertiesUtilities.getMessage(PropertiesConstants.MSG_STATE_INVALID);
			}

			if (j > 0) {
				return message;		//agregar a propertie con todo lo anterior ***
			}
			/**/
			lstNewSecurityBlockTransferTO = transferSecuritiesFacade.searchSecurityBlockTransferToWidthDocumentNumber(lstFilter);
			dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel(lstNewSecurityBlockTransferTO);

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "viewTransferBlock";
	}

	/**
	 * Alert return to search.
	 *
	 * @return the string
	 */
	public String alertReturnToSearch() {
		if (Validations.validateIsNotNullAndPositive(participantOrigin.getIdParticipantPk())
				|| Validations.validateIsNotNullAndPositive(participantTarget.getIdParticipantPk())
				|| Validations.validateIsNotNullAndPositive(holderAccountOrigin.getIdHolderAccountPk())
				|| Validations.validateIsNotNullAndPositive(holderAccountTarget.getIdHolderAccountPk())) {

			registersFound = true;
			confirmAlertAction = "searchTransferBlock";

			String strHeader = PropertiesUtilities.getMessage(
							GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES,
							FacesContext.getCurrentInstance().getViewRoot().getLocale(),
							GeneralConstants.LBL_HEADER_ALERT_CONFIRM);

			question(PropertiesUtilities.getMessage(PropertiesConstants.MSG_BACK_VALIDATION),strHeader);//agregar a propertie ***
			return "";
		} else {
			return returnToSearch();
		}

	}

	/**
	 * Return to search.
	 *
	 * @return the string
	 */
	public String returnToSearch() {
		resetFields();
		enabledButtons();
		registersFound = true;
		securityEvents();
		return "searchTransferBlock";
	}

	/**
	 * Back view with out validation.
	 *
	 * @return the string
	 */
	public String backViewWithOutValidation(){
		enabledButtons();
		securityEvents();
		return "searchTransferBlock";
	}
	
	/**
	 * Return only to search.
	 *
	 * @return the string
	 */
	public String returnOnlyToSearch() {
		returnToSearch();
		return "onlySearchTransferBlock";
	}

	/**
	 * Alertclear fields.
	 */
	public void AlertclearFields() {
		String message = PropertiesUtilities
				.getMessage(PropertiesConstants.LBL_MSG_CLEAR_SCREEN);

		validationMessage = PropertiesUtilities.getMessage(
				GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
	}

	/**
	 * Clear fields.
	 */
	public void clearFields() {
		resetFields();
	}

	/**
	 * Reset fields.
	 */
	public void resetFields() {
		/* END TRANSFER SECURITIES BLOCK */
		participantOrigin = new Participant();
		participantTarget = new Participant();
		participant = new Participant();

		securityOrigin = new Security();
		securitiesHelperAccBean.setDescription(null);
		securitiesHelperAccBean.setMnemonic(null);
		security = new Security();
		securityClass=null;
		holderAccountOrigin = new HolderAccount();
		holderAccountTarget = new HolderAccount();
		holderAccount = new HolderAccount();
		custodyOperation = new CustodyOperation();
		// securityTransferOperation = new SecurityTransferOperation();
		/* END SEARCH */
		lstNewSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		arrSecurityBlockTransfer = null;
		dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel();
		securityViewTransferTo = new SecurityBlockTransferTO();
		securityTransferOperation = inicializeSecurityTransferOperation();
		validationMessage = "";
		strTransferType = "";
		flagSourceTarget = true;
		flagSearchEnabled = true;
		securityBlockTransfertmp = new SecurityBlockTransferTO();
		securityTransferOperation = new SecurityTransferOperation();
		/* flag actions */
		flagRevisedSto = false;
		flagRejectSto = false;
		flagApprovedSto = false;
		flasgAnnuledSto = false;
		flagConfirmSto = false;
		blockRequest = new BlockRequest();
		dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel();
		securityTO = new SecurityTransferOperationTO();
		securityTO.setOperationInitDate(CommonsUtilities.currentDate());
		securityTO.setOperationEndDate(CommonsUtilities.currentDate());
		registersFound = true;
		minTime = CommonsUtilities.addDate(CommonsUtilities.currentDate(),
				-maxDaysOfCustodyRequest);
		securityEvents();
		
		//helpers
		sourceHolder = new Holder();
		targetHolder = new Holder();

		sourceAccountTo = new HolderAccountHelperResultTO();
		targetAccountTo = new HolderAccountHelperResultTO();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		
	}

	/**
	 * Selected radio.
	 *
	 * @param e the e
	 */
	public void selectedRadio(SelectEvent e) {
		SecurityBlockTransferTO securityBlockTransfer = (SecurityBlockTransferTO) e.getObject();
		securityTO.setState(securityBlockTransfer.getState());
		lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		lstActionSecurityBlockTransferTO.add(securityBlockTransfer);
	}

	/**
	 * Execute approve.
	 */
	@LoggerAuditWeb
	public void executeApprove() {
		try {
			
			String strPkTransferBlockFacade = "";
			
			SecurityBlockTransferTO securityBlockTo = securityViewTransferTo;
			securityBlockTo.setStateOld(TransferSecuritiesStateType.REGISTRADO.getCode());
			securityBlockTo.setStateNew(TransferSecuritiesStateType.APROBADO.getCode());

			if (!(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) 
			   && Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()))) {
				securityTransferOperation.setTargetHolderAccount(null);
			}

			strPkTransferBlockFacade = transferAvailableFacade.executeChangeStateAproveBlock(
											TransferSecuritiesStateType.REGISTRADO.getCode(),
											TransferSecuritiesStateType.APROBADO.getCode(),
											BusinessProcessType.BLOCK_SECUTITY_TRANSFER_APPROVE.getCode(), securityTransferOperation,flagIndMarketFact);
			if(!strPkTransferBlockFacade.equals("")){
				
				confirmAlertAction = "searchTransferBlock";

				String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
				String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_APPROVE_VB,new Object[] { strDocumentNumber.toString() });

				alert(header, message);
				
				/*** INICIO NOTIFICACIONES ***/
				  BusinessProcess businessProcess = new BusinessProcess();
				  businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_APPROVE.getCode()); 
				  Object[] parameters = new Object[1];
				  parameters[0] = strDocumentNumber.toString();
				  if(isDepositary){
					  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, parameters); 
				  }else if(isParticipant){
				  
				  if(SecuritiesTransferType.DESTINO_ORIGEN.getCode().equals(securityTransferOperation.getTransferType())){
				  
					  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
							  									 securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters); 
				  }else{
				  
					  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
							  									 securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters); 
					  } 
				  }
				/*** FIN NOTIFICACIONES ***/
				  cleanAfterActions();

			}			
		} catch (ServiceException e) {
			confirmAlertAction = "searchTransferBlock";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
	}

	/**
	 * Execute anulled.
	 */
	@LoggerAuditWeb
	public void executeAnulled() {

		try {
			String strPkTransferBlockFacade = "";
			
			if (!(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) 
			   && Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()))) {
				securityTransferOperation.setTargetHolderAccount(null);
			}
			securityTransferOperation.setCustodyOperation(custodyOperation);
			strPkTransferBlockFacade = transferAvailableFacade.executeChangeStateCancelBlock(TransferSecuritiesStateType.REGISTRADO.getCode(),
																	TransferSecuritiesStateType.ANULADO.getCode(),
																	BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CANCEL.getCode(), 
																	securityTransferOperation,
																	flagIndMarketFact);
			if(!strPkTransferBlockFacade.equals("")){
				//clearFields();
				initScreenNew();
				searchSolicitud();
				cleanAfterActions();
				confirmAlertAction = "searchTransferBlock";

				String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
				String message = PropertiesUtilities.getMessage(
																PropertiesConstants.TRANSFER_SUCCES_ANNULER_VB,
																new Object[] { strDocumentNumber.toString() });
				alert(header, message);
				
				/*** INICIO NOTIFICACIONES ***/
				BusinessProcess businessProcess = new BusinessProcess();
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CANCEL.getCode()); 
				Object[] parameters = new Object[1];
				parameters[0] = strDocumentNumber.toString();
				if(isDepositary){
				  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters); 
				} else if(isParticipant){			  
					if(SecuritiesTransferType.DESTINO_ORIGEN.getCode().equals(securityTransferOperation.getTransferType())){			  
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
						  									 securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters); 
					} else {			  
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
						  									 securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters); 
					} 
				}
				/*** FIN NOTIFICACIONES ***/
				
			}			
		} catch (ServiceException e) {
			//clearFields();
			initScreenNew();
			searchSolicitud();
			confirmAlertAction = "searchTransferBlock";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
														PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
		
	}

	/**
	 * Execute reject.
	 */
	@LoggerAuditWeb
	public void executeReject() {

		try {
			String strPkTransferBlockFacade = "";
			if (!(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) 
			   && Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()))) {
				securityTransferOperation.setTargetHolderAccount(null);
			}

			Integer stateOld = (securityViewTransferTo.getState().equals(TransferSecuritiesStateType.APROBADO.getCode())) ? TransferSecuritiesStateType.APROBADO.getCode() : TransferSecuritiesStateType.REVISADO.getCode();
			securityTransferOperation.setCustodyOperation(custodyOperation);
			strPkTransferBlockFacade = transferAvailableFacade.executeChangeStateRejectBlock(stateOld,
																TransferSecuritiesStateType.RECHAZADO.getCode(),
																BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REJECT.getCode(), 
																securityTransferOperation,
																flagIndMarketFact);

			if(!strPkTransferBlockFacade.equals("")){
				/** CAMBIA EL ESTADO AL BLOCK OPERATION A BLOQUEADO **/
				//clearFields();
				// removeAlert();
				confirmAlertAction = "searchTransferBlock";

				String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
				String message = PropertiesUtilities.getMessage(
																PropertiesConstants.TRANSFER_SUCCES_REJECT_VB,
																new Object[] { strDocumentNumber.toString() });
				alert(header, message);

				  /*** INICIO NOTIFICACIONES ***/
				  BusinessProcess businessProcess = new BusinessProcess();
				  businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REJECT.getCode()); 
				  Object[] parameters = new Object[1];
				  parameters[0] = strDocumentNumber.toString();
				  if(isDepositary){
					  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, parameters); 
				  }else if(isParticipant){
				  
					  if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
					  
						  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
							  									 	securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters); 
					  }else{
					  
						  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
							  											securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters); 
					  } 
				  }
				 /*** FIN NOTIFICACIONES ***/
				  initScreenNew();
					searchSolicitud();
					cleanAfterActions();
			}			  
		} catch (ServiceException e) {
			//clearFields();
			initScreenNew();
			searchSolicitud();
			confirmAlertAction = "searchTransferBlock";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									  PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
		
	}

	// ExecuteRevise
	/**
	 * Execute revise.
	 */
	@LoggerAuditWeb
	public void executeRevise() {

		try {
			String strPkTransferBlockFacade = "";
			
			if (!transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation)) {
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				holderAccountTarget = new HolderAccount();
				String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ORGIN_RNT_ACCOUNT_NO_RELATION);
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),message);
				return;
			}

			securityViewTransferTo.setTargetIdHolderAccountPk(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk());

			strPkTransferBlockFacade = transferAvailableFacade.executeChangeStateReviewBlock(TransferSecuritiesStateType.APROBADO.getCode(),
																 TransferSecuritiesStateType.REVISADO.getCode(),
																 BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REVIEW.getCode(), 
																 securityTransferOperation,
																 flagIndMarketFact);
			if(!strPkTransferBlockFacade.equals("")){
				
				confirmAlertAction = "searchTransferBlock";

				String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
				String message = PropertiesUtilities.getMessage(
						PropertiesConstants.TRANSFER_SUCCES_REVIEW_VB,
						new Object[] { strDocumentNumber.toString() });
				alert(header, message);


				/*** JH NOTIFICACIONES ***/
				  BusinessProcess businessProcess = new BusinessProcess();
				  businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REVIEW.getCode()); 
				  Object[] parameters = new Object[1];
					parameters[0] =  strDocumentNumber.toString() ;
				  if(isDepositary){
					  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, parameters); 
				  }else if(isParticipant){
					  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, participantLogin, parameters); 
				  }
				/*** end JH NOTIFICACIONES ***/
				//clearFields();
					initScreenNew();
					searchSolicitud();
					cleanAfterActions();
			}			  
		} catch (ServiceException e) {
			//clearFields();
			initScreenNew();
			searchSolicitud();
			confirmAlertAction = "searchTransferBlock";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
	}

	/**
	 * Execute confirm.
	 */
	@LoggerAuditWeb
	public void executeConfirm() {

		try {
			String strPkTransferBlockFacade = "";
			
			SecurityBlockTransferTO securityBlockTo = securityViewTransferTo;
			securityBlockTo.setStateOld(TransferSecuritiesStateType.REVISADO.getCode());
			securityBlockTo.setStateNew(TransferSecuritiesStateType.CONFIRMADO.getCode());
			securityBlockTo.setBussinesProcess(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode());
			strPkTransferBlockFacade = transferAvailableFacade.executeChangeStateConfirmBlock(securityBlockTo, lstNewSecurityBlockTransferTO,securityTransferOperation,flagIndMarketFact);
			if(!strPkTransferBlockFacade.equals("")){
				//clearFields();
				confirmAlertAction = "searchTransferBlock";
				String header = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
				String message = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_SUCCES_CONFIRM_B_VB,new Object[]{strDocumentNumber});
				alert(header, message);


				/*** JH NOTIFICACIONES ***/
				  BusinessProcess businessProcess = new BusinessProcess();
				  businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_CONFIRM.getCode()); 
				  Object[] parameters = new Object[3];
				  parameters[1] = securityTransferOperation.getTargetHolderAccount().getAccountNumber().toString();
				  parameters[2] = securityTransferOperation.getSecurities().getIdSecurityCodePk();
				  if(isDepositary){
						parameters[0]=securityTransferOperation.getTargetParticipant().getDisplayCodeMnemonic()+GeneralConstants.HYPHEN+
								securityTransferOperation.getTargetParticipant().getDescription();
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																   businessProcess, 
																   null, parameters);
					}else if(isParticipant){

						if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
						
							parameters[0]=securityTransferOperation.getTargetParticipant().getDisplayCodeMnemonic()+GeneralConstants.HYPHEN+
									securityTransferOperation.getTargetParticipant().getDescription();
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																   businessProcess, 
																   securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parameters);
						
								
						}else{
							parameters[0]=securityTransferOperation.getSourceParticipant().getDisplayCodeMnemonic()+GeneralConstants.HYPHEN+
									securityTransferOperation.getSourceParticipant().getDescription();
							notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
																	   businessProcess, 
																	   securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parameters);
						}
					}
				/*** JH NOTIFICACIONES ***/
				initScreenNew();
				searchSolicitud();
				cleanAfterActions();

			}
					} catch (Exception e) {
			//clearFields();
			initScreenNew();
			searchSolicitud();
			confirmAlertAction = "searchTransferBlock";
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(((ServiceException) e).getErrorService().getMessage(), ((ServiceException) e).getParams()));
		}
	}

	/**
	 * Execute confirm actions.
	 */
	@LoggerAuditWeb
	public void executeConfirmActions() {

		if (confirmAlertAction.equals("executeApprove")) {
			confirmAlertAction = "";
			executeApprove();
		} else if (confirmAlertAction.equals("executeRevise")) {
			confirmAlertAction = "";
			executeRevise();
		} else if (confirmAlertAction.equals("executeConfirm")) {
			confirmAlertAction = "";
			executeConfirm();
		}

	}
	
	/**
	 * Clean after actions.
	 */
	public void cleanAfterActions(){
		postConstructorSearch();
		securityTO= new SecurityTransferOperationTO();
		blockRequest=new BlockRequest();
		sourceHolder=new Holder();
		searchSolicitud();
	}

	/**
	 * Execute opetion.
	 *
	 * @param action the action
	 */
	public void executeOpetion(String action) {

		confirmAlertAction = action;
		String message = "";
		String header="";
		
		/** agregar validacion de documento de bloqueo***/
		try {
			
			/*Integer intValidBlockAmount = transferAvailableFacade.validBlockAmountInTransferAvailable(securityViewTransferTo.getIdTransferOperationPk());
			if(!(intValidBlockAmount!= null && intValidBlockAmount.intValue() > 0) ){

				alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_AMOUNTS_INVALID_CHANGES));
				return;
			}*/
			SecurityBlockTransferTO objSecurityBlockTransferTO= new SecurityBlockTransferTO();
			objSecurityBlockTransferTO.setOperationNumber(securityViewTransferTo.getOperationNumber());
			List<SecurityBlockTransferTO> lstFilter = transferSecuritiesFacade.searchDocumentNumbersWithOperation(objSecurityBlockTransferTO,true);
			BigDecimal bigMonto = new BigDecimal(0);

			for (int i = 0; i < lstFilter.size(); i++) {
				bigMonto = bigMonto.add(lstFilter.get(i).getActualBlockBalance());
			}

			if (bigMonto.compareTo(securityViewTransferTo.getActualBlockBalance()) == 1) {
				alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_AMOUNTS_INVALID_CHANGES));
				return;
			}
			/******/
			
			if (confirmAlertAction.equals("executeApprove")) {
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_APPROVE);
				header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_APPROVE);
			} else if (confirmAlertAction.equals("executeRevise")) {
				if(! (Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) && 
				      Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_TARGET));
				}
				
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_REVIEW);
				header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REVIEW);
			} else if (confirmAlertAction.equals("executeConfirm")) {
				message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONFIRM);
				header=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM);
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Object[] parametros = {message,securityViewTransferTo.getOperationNumber().toString()};
		
		validationMessage=PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
				 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				 PropertiesConstants.TRANSFER_QUESTION_UNIQUE_SAVE_VB,parametros);
		
		question(validationMessage,header);//agregar a propertie ***

	}

	/**
	 * Question execute.
	 *
	 * @param message the message
	 */
	public void questionExecute(String message) {
		validationMessage = PropertiesUtilities.getMessage(
				GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, FacesContext.getCurrentInstance().getViewRoot().getLocale(),
				GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.showComponent("formPopUp:confirmAlertExecution");
	}

	/**
	 * Call anulled or reject.
	 */
	@LoggerAuditWeb
	public void callAnulledOrReject() {
		strRejectMotive="";
		intRejectMotive=null;
		alertAnulledOrReject();
	}

	/**
	 * Alert anulled or reject.
	 */
	@LoggerAuditWeb
	public void alertAnulledOrReject() {
		validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_MOTIVE_OF);
		
		if (securityViewTransferTo.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
			validationMessage += PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ANULL);
		} else {
			validationMessage += PropertiesUtilities.getMessage(PropertiesConstants.ALERT_REJECT);
		}

		alterRejectCancel(validationMessage);
	}

	/**
	 * Sets the reject or anulled properties.
	 */
	@LoggerAuditWeb
	public void setRejectOrAnulledProperties() {
		executeAction();
		JSFUtilities.resetViewRoot();
		JSFUtilities.hideGeneralDialogues();  
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').hide()");
		custodyOperation.setRejectDate(CommonsUtilities.currentDate());
		custodyOperation.setRejectMotive(intRejectMotive);
		if (intRejectMotive.equals(otherRejectMotiveId)) {
			custodyOperation.setRejectMotiveOther(strRejectMotive);
		}
		custodyOperation.setRejectUser(userInfo.getUserAccountSession().getUserName());
		if (securityViewTransferTo.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_QUESTION_ANULLED_OPERATION,
				    		  new Object[]{securityViewTransferTo.getOperationNumber().toString()}) );
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REJECT)
				      , PropertiesUtilities.getMessage(PropertiesConstants.MSG_QUESTION_REJECTED_OPERATION,
				    		  new Object[]{securityViewTransferTo.getOperationNumber().toString()}) );
		}
		confirmAlertAction = "executeAnulledOrReject";
		JSFUtilities.executeJavascriptFunction("PF('dialogwConfirm').show()");

	}

	/**
	 * Close dialog reject motive.
	 */
	public void closeDialogRejectMotive(){
		JSFUtilities.resetComponent(":formPopUp:alterReject");
	}
	
	/**
	 * Close reject or anulled properties.
	 */
	public void closeRejectOrAnulledProperties() {
		intRejectMotive = null;
		strRejectMotive = null;
	}
	
	/**
	 * Execute anulled or reject.
	 */
	@LoggerAuditWeb
	public void executeAnulledOrReject() {
		if (securityViewTransferTo.getState().equals(TransferSecuritiesStateType.REGISTRADO.getCode())) {
			executeAnulled();
		} else {
			executeReject();
		}
		strRejectMotive = "";
		intRejectMotive = -1;
	}

	/**
	 * Clear target participant.
	 */
	public void clearTargetParticipant(){
		securityTransferOperation.setTargetParticipant(new Participant());
		securityTransferOperation.setTargetHolderAccount(new HolderAccount());

		if(Validations.validateIsNotNull(securityTransferOperation) && Validations.validateIsNotNull(securityTransferOperation.getTransferType())){
			if(securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
				securityTransferOperation.setSecurities(new Security());
			}
		}
		
		targetHolder = new Holder();
		lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		targetAccountTo = new HolderAccountHelperResultTO();
	}
	
	/**
	 * Clear source participant.
	 */
	public void clearSourceParticipant(){
		securityTransferOperation.setSourceParticipant(new Participant());
		securityTransferOperation.setSourceHolderAccount(new HolderAccount());

		if(securityTransferOperation.getTransferType()!=null &&
				!securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()) ){
			securityTransferOperation.setSecurities(new Security());
		}
		
		sourceHolder = new Holder();
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		sourceAccountTo = new HolderAccountHelperResultTO();
	}
	
	/**
	 * Valid transfer type.
	 */
	public void validTransferType() {
		if (Validations.validateIsNotNullAndPositive(securityTransferOperation.getTransferType())) {
			
			if (securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())) {
				flagSameParticipant = false;
				flagSourceTarget = true;
				clearTargetParticipant();

				if (isParticipant) {
					flagRegisterOriginParticipant = true;
					flagRegisterTargetParticipant = false;
					securityTransferOperation.setSourceParticipant(Defaultparticipant);
				}
				
			} else if (securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())) {
				flagSameParticipant = true;
				flagSourceTarget = false;
				
				clearTargetParticipant();
				if (isParticipant) {
					securityTransferOperation.setSourceParticipant(Defaultparticipant);
					flagRegisterOriginParticipant = true;
					flagRegisterTargetParticipant = false;
				}
				
				if(Validations.validateIsNotNull(securityTransferOperation.getSourceParticipant()) && 
				   Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk()) ){
					securityTransferOperation.setTargetParticipant(securityTransferOperation.getSourceParticipant());
					
					if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()) ){
						targetHolder.setIdHolderPk(sourceHolder.getIdHolderPk());
						targetHolder.setDescriptionHolder(sourceHolder.getDescriptionHolder());
						targetHolder.setFirstLastName(sourceHolder.getFirstLastName());
						targetHolder.setName(sourceHolder.getName());
						targetHolder.setFullName(sourceHolder.getFullName());
						targetHolder.setSecondLastName(sourceHolder.getSecondLastName());
						targetHolder.setHolderType(sourceHolder.getHolderType());

						HolderTO holderTO = new HolderTO();
						holderTO.setHolderId(sourceHolder.getIdHolderPk());
						holderTO.setFlagAllHolders(true);
						holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
						List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
						try {
							lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						lstTargetAccountTo = lstAccountTo;

						if(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) && 
							Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())	){
						
							for(HolderAccountHelperResultTO obj:lstTargetAccountTo){
								if(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().equals(obj.getAccountPk())){
									obj.setDisabled(true);
								}else{
									obj.setDisabled(false);
								}
							}
							
						}
						
					}
							
				}
			}
			dataTableRegistry();
		}

	}

	/**
	 * Valid block type.
	 */
	public void validBlockType() {
		lstNewSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		lstSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		arrSecurityBlockTransfer = null;
		dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel();
	}

	/**
	 * Radio select event.
	 */
	public void radioSelectEvent() {
		if (radioSelected.equals(1)) {
			custodyOperation.setOperationNumber(null);
		} else if (radioSelected.equals(2)) {
			securityTransferOperation.setTransferType(null);
			participantOrigin = new Participant();
			if (userInfo.getUserAccountSession().isParticipantInstitucion() || 
					userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
					userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) {
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					Long participantDpf = getIssuerDpfInstitution(userInfo);					
					if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
						participantOrigin.setIdParticipantPk(participantDpf);
					}
				}else if( Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode()) ){
					participantOrigin.setIdParticipantPk(userInfo.getUserAccountSession().getParticipantCode());					
				}
				
			}
			securityTO = new SecurityTransferOperationTO();
			holderAccount = new HolderAccount();
			blockRequest = new BlockRequest();
			security = new Security();
			securityTO.setOperationInitDate(CommonsUtilities.currentDate());
			securityTO.setOperationEndDate(CommonsUtilities.currentDate());
		}
		lstSecurityBlockTransferTO = null;
		dataModelSecurityBlockTransfer = null;
	}

	/**
	 * Disabled buttons.
	 */
	public void disabledButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();

		if (userPrivilege.getUserAcctions().isApprove()) {
			privilegeComponent.setBtnApproveView(false);
		}

		if (userPrivilege.getUserAcctions().isAnnular()) {
			privilegeComponent.setBtnAnnularView(false);
		}

		if (userPrivilege.getUserAcctions().isReview()) {
			privilegeComponent.setBtnReview(false);
		}

		if (userPrivilege.getUserAcctions().isReject()) {
			privilegeComponent.setBtnRejectView(false);
		}

		if (userPrivilege.getUserAcctions().isConfirm()) {
			privilegeComponent.setBtnConfirmView(false);
		}

		userPrivilege.setPrivilegeComponent(privilegeComponent);
		flagApprovedSto = false;
		flasgAnnuledSto = false;
		flagRevisedSto = false;
		flagRejectSto = false;
		flagConfirmSto = false;
	}

	/**
	 * Evaluate dates.
	 *
	 * @param firstDate the first date
	 * @param secondDate the second date
	 * @return the boolean
	 */
	public Boolean evaluateDates(Date firstDate, Date secondDate) {
		Boolean greaterDate = Boolean.FALSE;
		if (firstDate.after(secondDate)) {
			greaterDate = Boolean.TRUE;
		}
		return greaterDate;
	}

	/**
	 * Setting unique contrapart account.
	 *
	 * @param cleanData the new ting unique contrapart account
	 */
	public void settingUniqueContrapartAccount(boolean cleanData) {

		try {
			
			HolderAccount holderAccount = new HolderAccount();
			if(!Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getAccountNumber())){
				if(cleanData){
					securityTransferOperation.setTargetHolderAccount(holderAccount);
				}
				return;
			}
			holderAccount.setParticipant(securityTransferOperation.getTargetParticipant());
			holderAccount.setAccountNumber(securityTransferOperation.getTargetHolderAccount().getAccountNumber());
			holderAccount = getHolderAccountAndDetails(holderAccount);
			
			if( !(Validations.validateIsNotNull(holderAccount) &&
				  Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())) ){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ERROR_HOLDER_ACCOUNT_NOT_EXIST));
				if(cleanData){
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
					targetHolder = new Holder();
					lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				}
				targetAccountTo = new HolderAccountHelperResultTO();
				return;
			}else{
					securityTransferOperation.setTargetHolderAccount(holderAccount);
			}
			
			if(!transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation)){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
				if(cleanData){
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
					targetHolder = new Holder();
					lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				}
				targetAccountTo = new HolderAccountHelperResultTO();
				return;
			}
				
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	/**
	 * Post constructor view.
	 *
	 * @param securityTransferTo the security transfer to
	 * @param onlyread the onlyread
	 * @param action the action
	 * @return the string
	 */
	public String postConstructorView(SecurityBlockTransferTO securityTransferTo, boolean onlyread,String action) {

		try {
			intHiddenMotive = 0;
			disabledButtons();
			
			lstTargetAccountTo = null;
			targetAccountTo = null;

			if (Validations.validateIsNotNullAndPositive(securityTransferTo.getOperationNumber())) {
				strDocumentNumber = securityTransferTo.getOperationNumber().toString();
			}
			strSateDescription = securityTransferTo.getStateDescription();
			blockRequest.setBlockType(securityTransferTo.getBlockType());

			Participant participantOrigin = new Participant();
			participantOrigin.setIdParticipantPk(securityTransferTo.getSourceIdParticipantPk());
			participantOrigin.setDescription(securityTransferTo.getSourceParticipantDescription());
			participantOrigin.setDisplayCodeMnemonic(securityTransferTo.getSourceParticipantMnemonic()+GeneralConstants.HYPHEN+
					securityTransferTo.getSourceIdParticipantPk());

			Security security = new Security();
			security.setIdSecurityCodePk(securityTransferTo.getIdSecurityCodePk());
			security = helperComponentFacade.findSecurity(security);

			Participant participantTarget = new Participant();
			participantTarget.setIdParticipantPk(securityTransferTo.getTargetIdParticipantPk());
			participantTarget.setDescription(securityTransferTo.getTargetParticipantDescription());
			participantTarget.setDisplayCodeMnemonic(securityTransferTo.getTargetParticipantMnemonic()+GeneralConstants.HYPHEN+
					securityTransferTo.getTargetIdParticipantPk());

			HolderAccount srcHolderAccount = new HolderAccount();
			srcHolderAccount.setIdHolderAccountPk(securityTransferTo.getSourceIdHolderAccountPk());
			List<HolderAccount> lstHolderAccount = transferAvailableFacade.getLisHolderAccountServiceBean(srcHolderAccount);
			if (lstHolderAccount.size() > 0) {
				holderAccountOrigin = lstHolderAccount.get(0);
			}

			if (Validations.validateIsNotNullAndPositive(securityTransferTo.getTargetIdHolderAccountPk())) {
				HolderAccount tgtHolderAccount = new HolderAccount();
				tgtHolderAccount.setIdHolderAccountPk(securityTransferTo.getTargetIdHolderAccountPk());
				lstHolderAccount = transferAvailableFacade.getLisHolderAccountServiceBean(tgtHolderAccount);

				if (lstHolderAccount.size() > 0) {
					holderAccountTarget = lstHolderAccount.get(0);
				}
				securityTransferOperation.setTargetHolderAccount(holderAccountTarget);
			} else {
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
			}

			securityTransferOperation.setIdTransferOperationPk(securityTransferTo.getIdTransferOperationPk());
			securityTransferOperation.setTransferType(securityTransferTo.getTransferType());
			securityTransferOperation.setSecurities(security);
			securityTransferOperation.setSourceParticipant(participantOrigin);
			securityTransferOperation.setTargetParticipant(participantTarget);
			securityTransferOperation.setSourceHolderAccount(holderAccountOrigin);
			securityTransferOperation.setCustodyOperation(transferAvailableFacade.getCustodyOperation(securityTransferTo.getIdTransferOperationPk()));
			securityTransferOperation.getCustodyOperation().setRejectMotiveDesc(securityTransferTo.getRejectMotiveDesc());
			
			if(securityTransferTo.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
				flagSameParticipant = true;
				flagSourceTarget = false;
			}else{
				flagSameParticipant = false;
				flagSourceTarget = true;
			}
			
			if( Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) &&
				Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())  ){
				
				HolderAccount holderAccount = getHolderAccountAndDetails(securityTransferOperation.getSourceHolderAccount());
				securityTransferOperation.setSourceHolderAccount(holderAccount);
				
				HolderTO holderTO = new HolderTO();
				holderTO.setParticipantFk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
				holderTO.setHolderAccountId(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				lstSourceAccountTo = lstAccountTo;
			}
			
			if( Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) &&
				Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()) ){
				
				HolderAccount holderAccount = getHolderAccountAndDetails(securityTransferOperation.getTargetHolderAccount());
				securityTransferOperation.setTargetHolderAccount(holderAccount);
				HolderTO holderTO = new HolderTO();
				holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
				holderTO.setHolderAccountId(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk());
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				lstTargetAccountTo = lstAccountTo;
			}
			
			if (Validations.validateIsNull(this.getOtherRejectMotiveId())) {
				this.setOtherRejectMotiveId(intOtherRejectMotive);
			}

			if (Validations.validateIsNull(this.getRejectMotiveList())) {
				// Setting Reject Motive List
				rejectMotiveList = new ArrayList<ParameterTable>();
				ParameterTableTO paramTable = new ParameterTableTO();
				paramTable.setMasterTableFk(MasterTableType.MASTER_TABLE_REJECT_MOTIVE_TRANSFER_SECURITIES_AVAILABLE.getCode());
				paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
				for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
					rejectMotiveList.add(param);
				}
				this.setRejectMotiveList(rejectMotiveList);
			}

			securityViewTransferTo = securityTransferTo;
			String message = "";
			List<SecurityBlockTransferTO> lstFilter = transferSecuritiesFacade.searchDocumentNumbersWithOperation(securityTransferTo,onlyread);

			if (!onlyread) {
				if (lstFilter.size() > 1) {
					message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_IN_REQUEST) +securityTransferTo.getOperationNumber().toString()+" "+
							  PropertiesUtilities.getMessage(PropertiesConstants.MSG_BLOCKS_DOCUMENTS);
				} else {
					message = PropertiesUtilities.getMessage(PropertiesConstants.MSG_IN_REQUEST) +securityTransferTo.getOperationNumber().toString()+" "+
							  PropertiesUtilities.getMessage(PropertiesConstants.MSG_BLOCK_DOCUMENT);
				}

				int j = 0;
				BigDecimal bigMonto = new BigDecimal(0);

				for (int i = 0; i < lstFilter.size(); i++) {

					if (!lstFilter.get(i).getState().equals(AffectationStateType.BLOCKED.getCode())) {
						message += (j > 0) ? " ,"
								+ lstFilter.get(i).getDocumentNumber() : " "
								+ lstFilter.get(i).getDocumentNumber();
						j++;
					}
					bigMonto = bigMonto.add(lstFilter.get(i).getActualBlockBalance());
				}

				if (lstFilter.size() > 1) {
					message += " "+PropertiesUtilities.getMessage(PropertiesConstants.MSG_STATE_INVALID);		//agregar a propertie ***
				} else {
					message += " "+PropertiesUtilities.getMessage(PropertiesConstants.MSG_STATE_INVALID);		//agregar a propertie ***
				}

				if (j > 0) {
					alert(message);
					return null;
				}

				if (bigMonto.compareTo(securityTransferTo.getActualBlockBalance()) == 1) {
					
					alert(PropertiesUtilities.getMessage(PropertiesConstants.MSG_AMOUNTS_INVALID_CHANGES));//agregar a propertie ***
					return null;
				}
			}
			
			viewErrorMessage = "";
			if( Validations.validateIsNotNull(action) && action.equals("review") ){
				
				List<Long> lstAccounts = transferAvailableFacade.getAccountsWithCuisAndParticipant(securityTransferOperation.getSourceHolderAccount(), 
				securityTransferOperation.getTargetParticipant().getIdParticipantPk());

				HolderTO holderTO = new HolderTO();
				holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
				holderTO.setLstHolderAccountId(lstAccounts);
				List<HolderAccountHelperResultTO> lstAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				if(lstAccounts!=null && lstAccounts.size()>0){
					lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				}
				lstTargetAccountTo = lstAccountTo;
				if(lstTargetAccountTo.size()<=0){
				viewErrorMessage=PropertiesUtilities.getMessage(PropertiesConstants.MSG_CONTRAPART_NOT_EXIST_ACCOUNT);
				}

				if(lstTargetAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
					targetAccountTo=lstTargetAccountTo.get(0);
					settingTargetViewAccount();
				}
			}
		
			lstNewSecurityBlockTransferTO = transferSecuritiesFacade.searchSecurityBlockTransferToWidthDocumentNumber(lstFilter);
			dataModelSecurityBlockTransferView = new SecurityBlockTransferDataModel(lstNewSecurityBlockTransferTO);

			/*visibilidad de cuentas*/
			if(isDepositary){
				flagAccountOriginVisible = true;
				flagAccountTargetVisible = true;
			}else if(isParticipant){
				if(participantLogin.equals(securityTransferTo.getSourceIdParticipantPk())){	//participante origen
					if(securityTransferTo.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())){
						flagAccountOriginVisible = true;
						flagAccountTargetVisible = false;	
					}else if(securityTransferTo.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())){
						flagAccountOriginVisible = true;
						flagAccountTargetVisible = true;
					}
				}else{	//participante destino
					
						flagAccountTargetVisible = true;
						flagAccountOriginVisible = false;
				}
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "viewTransferBlock";
	}

	/**
	 * Onchange initial date.
	 */
	public void onchangeInitialDate() {
		Date initialDate = securityTO.getOperationInitDate();
		Date endDate = securityTO.getOperationEndDate();
		minTime = CommonsUtilities.addDate(endDate, -maxDaysOfCustodyRequest);
		String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
		if (evaluateDates(initialDate, endDate)) {
			alert(PropertiesUtilities.getMessage(propertieConstant));
		}
	}

	/**
	 * Onchange end date.
	 */
	public void onchangeEndDate() {
		Date initialDate = securityTO.getOperationInitDate();
		Date endDate = securityTO.getOperationEndDate();
		minTime = CommonsUtilities.addDate(endDate, -maxDaysOfCustodyRequest);

		if (evaluateDates(initialDate, endDate)) {
			String propertieConstant = PropertiesConstants.INI_DATE_GREATER_THAN_END_DATE;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			return;
		}
		if (evaluateDates(endDate, CommonsUtilities.currentDateTime())) {
			String propertieConstant = PropertiesConstants.END_DATE_GREATER_THAN_SYSTEM_DATE;
			alert(PropertiesUtilities.getMessage(propertieConstant));
			return;
		}
	}

	/**
	 * Check transfer block.
	 *
	 * @param e the e
	 */
	public void checkTransferBlock(SelectEvent e) {
		SecurityBlockTransferTO securityBlockTransfer = (SecurityBlockTransferTO) e
				.getObject();

		lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		for (int i = 0; i < arrSecurityBlockTransfer.length; i++) {
			lstActionSecurityBlockTransferTO.add(arrSecurityBlockTransfer[i]);
		}
	}

	/**
	 * Uncheck transfer block.
	 *
	 * @param e the e
	 */
	public void uncheckTransferBlock(UnselectEvent e) {
		SecurityBlockTransferTO securityBlockTransfer = (SecurityBlockTransferTO) e.getObject();

		lstActionSecurityBlockTransferTO = new ArrayList<SecurityBlockTransferTO>();
		for (int i = 0; i < arrSecurityBlockTransfer.length; i++) {
			lstActionSecurityBlockTransferTO.add(arrSecurityBlockTransfer[i]);
		}
	}

	/**
	 * Valid targetparticipant.
	 */
	public void validTargetparticipant() {

		Participant tmpParticipant = securityTransferOperation.getTargetParticipant();
		clearTargetParticipant();
		securityTransferOperation.setTargetParticipant(tmpParticipant);
		
		if (Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk())
		 && Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
		 && !securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())
		 && securityTransferOperation.getSourceParticipant().getIdParticipantPk().equals(securityTransferOperation.getTargetParticipant().getIdParticipantPk())) {
		
			String headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.EQUALS_PARTICIPANT);

			if (flagSourceTarget) {
				confirmAlertAction = "clearTargetParticipant";
			}

			alert(headerMessage, bodyMessage);
		}
		dataTableRegistry();

	}

	/**
	 * Save transfer block.
	 */
	@LoggerAuditWeb
	public void saveTransferBlock() {
		try {
			ArrayList<String> lstPkTransferBlockFacade = new ArrayList<String>();
			String strPkTransferBlockFacade = "";

			BigDecimal monto = new BigDecimal(0);
			if (lstActionSecurityBlockTransferTO.size() > 0) {
				for (int i = 0; i < lstActionSecurityBlockTransferTO.size(); i++) {

					/* seteo los datos capturados del formulario */
					lstActionSecurityBlockTransferTO.get(i).setBlockType(blockRequest.getBlockType());
					lstActionSecurityBlockTransferTO.get(i).setTransferType(securityTransferOperation.getTransferType());
					lstActionSecurityBlockTransferTO.get(i).setOperationType((ParameterOperationType.SECURITIES_TRANSFER_AVAILABLE_SOURCE_TARGET.getCode()));

					if (Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount())
					&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())) {
						lstActionSecurityBlockTransferTO.get(i).setSourceIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
					}

					if (Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount())
					&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) {
						lstActionSecurityBlockTransferTO.get(i).setTargetIdHolderAccountPk(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk());
					}

					if (Validations.validateIsNotNull(securityTransferOperation.getSourceParticipant())
					&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk())) {
						lstActionSecurityBlockTransferTO.get(i).setSourceIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
					}

					if (Validations.validateIsNotNull(securityTransferOperation.getTargetParticipant())
					&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk())) {
						lstActionSecurityBlockTransferTO.get(i).setTargetIdParticipantPk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
					}

					//if (lstActionSecurityBlockTransferTO.get(i).getBlockLeveDesc().equals(LevelAffectationType.BLOCK.getValue())) {
						monto = monto.add(lstActionSecurityBlockTransferTO.get(i).getActualBlockBalance());
					//}

				}

				if (!(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) 
				&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()))) {
					securityTransferOperation.setSourceHolderAccount(null);
				}

				if (!(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount()) 
				&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()))) {
					securityTransferOperation.setTargetHolderAccount(null);
				}

				securityTransferOperation.setSecurities(new Security(lstActionSecurityBlockTransferTO.get(0).getIdSecurityCodePk()));
				securityTransferOperation.setIndBlockTransfer(ComponentConstant.ONE); // 1 para bloqueados
				securityTransferOperation.setQuantityOperation(monto);

				Long operationType = ParameterOperationType.SECURITIES_TRANSFER_AVAILABLE_SOURCE_TARGET.getCode();
				Long businessProcessId = BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REGISTER.getCode();

				strPkTransferBlockFacade = transferAvailableFacade.registrySecurityTransferServiceBeanBlock(securityTransferOperation, businessProcessId,
																											operationType, lstActionSecurityBlockTransferTO);

				if (!strPkTransferBlockFacade.equals("")) {
					lstPkTransferBlockFacade.add(strPkTransferBlockFacade);
				}

				if (lstPkTransferBlockFacade.size() > 0) {

					
					String solicitudes = "";
					for (int i = 0; i < lstPkTransferBlockFacade.size(); i++) {
						solicitudes += (i > 0) ? " ,"
								+ lstPkTransferBlockFacade.get(i) : " "
								+ lstPkTransferBlockFacade.get(i);
					}
					
					validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.REGISTER_COMPLETE_TRANSFER_SECURITY_BLOCK, 
																		new Object[]{solicitudes});
		
					confirmAlertAction = "backTosearchTransferBlock";
					String headerMessage=PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
					alert(headerMessage, validationMessage);


					/*** ENVIO NOTIFICACIONES ***/
					  BusinessProcess businessProcess = new BusinessProcess();
					  businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_SECUTITY_TRANSFER_REGISTER.getCode());
					  Object[] parametersNot = new Object[1];
					  parametersNot[0] = solicitudes;
					  if(isDepositary){
						  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(),businessProcess, null, parametersNot); 
					  } else if(isParticipant){						  
						  if(SecuritiesTransferType.DESTINO_ORIGEN.getCode().equals(securityTransferOperation.getTransferType())){
							  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
									  									 securityTransferOperation.getTargetParticipant().getIdParticipantPk(), parametersNot);
						  } else {
							  notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,
									  									 securityTransferOperation.getSourceParticipant().getIdParticipantPk(), parametersNot);
						  }
					  }
				   /*** FIN NOTIFICACIONES ***/
					  cleanAfterActions();
				}
			} else {
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,FacesContext.getCurrentInstance().getViewRoot()
										.getLocale(),PropertiesConstants.NOT_REGISTER_TRANSFER_SECURITY_AVAILABLE);
				alert(validationMessage);
			}

		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									  PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
	}

	/**
	 * Search transfer block.
	 */
	public void searchTransferBlock() {
		try {
			
			if( !securityTransferOperation.getTransferType().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode())){
				//origen - destino | mismo participante
				if( !(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) && 
					Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())) ){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED));
					return;
				} 
				
				if( securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode()) ){
					if( !(Validations.validateIsNotNull(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk()) && 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())) ){
						alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED));
						return;
					}
				}
			}
			
			if(!Validations.validateIsNotNull(securityTransferOperation.getSecurities().getIdSecurityCodePk())) {
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_INSERT_SECURITY));
				return;
			}
			
			SecurityBlockTransferTO filters = new SecurityBlockTransferTO();
			filters.setSourceIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().longValue());
			filters.setSourceIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			filters.setBlockType(blockRequest.getBlockType());
			filters.setIdSecurityCodePk(securityTransferOperation.getSecurities().getIdSecurityCodePk());
			
			lstNewSecurityBlockTransferTO = transferSecuritiesFacade.searchSecurityBlockTransferTo(filters);
			dataModelSecurityBlockTransfer = new SecurityBlockTransferDataModel(lstNewSecurityBlockTransferTO);

			if (lstNewSecurityBlockTransferTO.size() > 0) {
				registersFound = true;
			} else {
				registersFound = false;
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Save transfer block alert.
	 */
	@LoggerAuditWeb
	public void saveTransferBlockAlert() {

		try {

			if (securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())) {

				if (!transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation)) {
					if (securityTransferOperation.getTransferType().equals(SecuritiesTransferType.ORIGEN_DESTINO.getCode())) {
						securityTransferOperation.setSourceHolderAccount(new HolderAccount());
					} else {
						securityTransferOperation.setTargetHolderAccount(new HolderAccount());
					}
					String message = PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ORGIN_RNT_ACCOUNT_NO_RELATION);
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),message);//agregar a propertie ***
					return;
				}
			}

			if (lstActionSecurityBlockTransferTO.size() > 0) {
				String messageError=null;
				Boolean existTransfer = false;
				int cont=0;
				SecurityBlockTransferTO objSecurityBlockTransferTO=null;
				for (int i = 0; i < lstActionSecurityBlockTransferTO.size(); i++){
					objSecurityBlockTransferTO = (SecurityBlockTransferTO)lstActionSecurityBlockTransferTO.get(i);
					existTransfer = transferSecuritiesFacade.existSecurityTransferBlock(blockRequest.getBlockType(),
							securityTransferOperation.getSecurities().getIdSecurityCodePk(),objSecurityBlockTransferTO.getDocumentNumber());
					if(existTransfer){
						if(cont==0){
							messageError=objSecurityBlockTransferTO.getDocumentNumber().toString();
							cont++;
						}else{
							messageError=messageError + GeneralConstants.STR_COMMA_WITHOUT_SPACE + objSecurityBlockTransferTO.getDocumentNumber().toString();
							cont++;
						}
					}						
				}				

				if (cont==1) {					
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						  PropertiesUtilities.getMessage(PropertiesConstants.MSG_QUESTION_EXIST_SOLICITUD_TRANSFER_BLOCK,
														 new Object[]{messageError}));
					return;
				}else if(cont>1){
					alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
							  PropertiesUtilities.getMessage(PropertiesConstants.MSG_QUESTION_EXIST_SOLICITUD_TRANSFER_BLOCK_ALL,
															 new Object[]{messageError}));
					return;
				}

				confirmAlertAction = "saveTransferBlock";
				question(PropertiesUtilities.getMessage(PropertiesConstants.MSG_QUESTION_REGISTRY_TRANSFER_BLOCK),
						PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER));//agregar a propertie ***
			} else {
				validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.TRANSFER_NOT_SELECT_BLOCK_DOCUMENT);//agregar a propertie ***
				alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Setting target view account.
	 */
	public void settingTargetViewAccount(){
		HolderAccount holderAccount = new HolderAccount();
		holderAccount.setIdHolderAccountPk(targetAccountTo.getAccountPk());
		holderAccount.setAccountNumber(targetAccountTo.getAccountNumber());
		securityTransferOperation.setTargetHolderAccount(holderAccount);
		settingUniqueContrapartAccount(false);
	}
	
	/**
	 * Valid holder account.
	 */
	public void validHolderAccount() {

		if (Validations.validateIsNotNull(getParticipantOrigin()) && 
			Validations.validateIsNotNullAndPositive(getParticipantOrigin().getIdParticipantPk()) &&
			Validations.validateIsNotNull(sourceHolder) && 
			Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk()) &&
			Validations.validateIsNotNull(holderAccount) && 
			Validations.validateIsNotNullAndPositive(holderAccount.getIdHolderAccountPk())
		) {

			HolderAccount varholderAccount = getHolderAccountAndDetails(holderAccount);

			if(Validations.validateIsNotNull(varholderAccount) && Validations.validateIsNotNull(varholderAccount.getIdHolderAccountPk())){
				if (!varholderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
					holderAccount = new HolderAccount();
					return;
				}
			}
			else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_EXISTED));
				holderAccount = new HolderAccount();
				return;
			}

			holderAccount = varholderAccount;

		}
	}

	/**
	 * Apply standar owa.
	 */
	public void applyStandarOwa() {

		
		if (lstSecurityBlockTransferTO != null && lstSecurityBlockTransferTO.size() > 0) {
			lstSecurityBlockTransferTO = null;
			dataModelSecurityBlockTransfer = null;
		}
		if(Validations.validateIsNotNull(securityTransferOperation.getSecurities())
				&& Validations.validateIsNotNullAndPositive(securityTransferOperation.getSecurities().getSecurityClass())){
			securityTransferOperation.setSecurityClass(securityTransferOperation.getSecurities().getSecurityClass());
		}
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNull(sourceHolder.getIdHolderPk())
				&& Validations.validateIsNotNull(participantOrigin)
				&& Validations.validateIsNotNull(participantOrigin.getIdParticipantPk())){
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				flagParticipanteInvestor = true;
			}else{
				flagParticipanteInvestor = false;
			}
			if(!helperComponentFacade.findRelationHolderParticipant(sourceHolder.getIdHolderPk(), participantOrigin.getIdParticipantPk(),flagParticipanteInvestor)){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.not.relation.participant"));
				JSFUtilities.showSimpleValidationDialog();
				sourceHolder=new Holder();
				return;
			}
		}

	}

	/**
	 * Valid securitie.
	 */
	public void validSecuritie() {
		try {

			if(Validations.validateIsNullOrNotPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_SOURCE));
				securityTransferOperation.setSecurities(new Security());
				return;
			}
			
			if (Validations.validateIsNotNull(securityTransferOperation.getSecurities()) &&
				Validations.validateIsNotNull(securityTransferOperation.getSecurities().getIdSecurityCodePk())) {

				if(!(securityTransferOperation.getSecurities().isRegisteredState() || securityTransferOperation.getSecurities().isGuardaExclusive())){
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_STATE_INVALID));
					securityTransferOperation.setSecurities(new Security());
					return;
				}
				//verifico si el valor le pertenece al participante y cuenta	
				HolderAccountBalance filter = new HolderAccountBalance();
				HolderAccountBalancePK id = new HolderAccountBalancePK();
				id.setIdHolderAccountPk(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk());
				id.setIdParticipantPk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
				id.setIdSecurityCodePk(securityTransferOperation.getSecurities().getIdSecurityCodePk());
				filter.setId(id);
				
				if(transferAvailableFacade.getExistSecurityForHolder(filter) <= 0){
					securityTransferOperation.setSecurities(new Security());
					alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_SECURITY_NO_RELATION_PARTICIPANT_AND_ACCOUNT));
					return;
				}
				
				// validate if it is correct instrument type
				Integer secCount = transferAvailableFacade.validSecurityWithSecurityClass(securityTransferOperation.getSecurities().getIdSecurityCodePk(),
									null);
				if (secCount == null || secCount <= 0) {
					securityTransferOperation.setSecurities(new Security());
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_SECURITY_INVALID_INSTRUMENT_TYPE));
				}
			}
			dataTableRegistry();
		} catch (ServiceException e) {
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
				  PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
		}
	}

	/**
	 * Valid source participant.
	 */
	public void validSourceParticipant() {
		
		Participant tmpParticipant = securityTransferOperation.getSourceParticipant();
		clearSourceParticipant();
		securityTransferOperation.setSourceParticipant(tmpParticipant);
		if (flagSourceTarget || flagSameParticipant) {
			clearTargetParticipant();
			
			if (flagSameParticipant) {
				clearTargetParticipant();
				securityTransferOperation.setTargetParticipant(tmpParticipant);
			}
		}

		if (Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceParticipant().getIdParticipantPk())
		 && Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetParticipant().getIdParticipantPk())
		 && !securityTransferOperation.getTransferType().equals(SecuritiesTransferType.MISMO_PARTICIPANTE.getCode())
		 && securityTransferOperation.getSourceParticipant().getIdParticipantPk().equals(securityTransferOperation.getTargetParticipant().getIdParticipantPk())) {

			if (flagSourceTarget) {
				confirmAlertAction = "clearTargetParticipant";
			}
			String headerMessage = PropertiesUtilities.getGenericMessage(PropertiesConstants.LBL_HEADER_ALERT_WARNING);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.EQUALS_PARTICIPANT);
			alert(headerMessage, bodyMessage);
		}
		dataTableRegistry();
	}

	/**
	 * Confirm alert actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmAlertActions() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (tmpAction.equals("saveTransferBlock")) {
			saveTransferBlock();
		}
		if (tmpAction.equals("executeApprove")) {
			confirmAlertAction = "";
			executeApprove();
		} else if (tmpAction.equals("executeRevise")) {
			confirmAlertAction = "";
			executeRevise();
		} else if (tmpAction.equals("executeConfirm")) {
			confirmAlertAction = "";
			executeConfirm();
		} else if (tmpAction.equals("executeAnulledOrReject")) {
			confirmAlertAction = "";
			executeAnulledOrReject();
		}

		return "";
	}
	
	/**
	 * Change motive handler.
	 */
	public void changeMotiveHandler(){
		JSFUtilities.resetViewRoot();
	}
	/**
	 * Confirm actions.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String confirmActions() {

		String tmpAction = "";
		tmpAction = confirmAlertAction;
		confirmAlertAction = "";

		if (tmpAction.equals("backTosearchTransferBlock")) {
			//clearFields();
			securityTransferOperation = new SecurityTransferOperation();
			securityTransferOperation.setTransferType(null);
			return "searchTransferBlock";
		} else if (tmpAction.equals("searchTransferBlock")) {
			//clearFields();
			return "searchTransferBlock";
		}

		return null;
	}

	/**
	 * Valid target holder account.
	 */
	public void validTargetHolderAccount() {

		try {
				
			if(!(Validations.validateIsNotNull(securityTransferOperation.getSourceHolderAccount()) && 
			   Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk())) ){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_NO_ACCOUNT_SELECTED_SOURCE));					
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				targetAccountTo = new HolderAccountHelperResultTO();
				return;
			}
		
			if(Validations.validateIsNotNull(targetAccountTo) && Validations.validateIsNotNullAndPositive(targetAccountTo.getAccountNumber()) ){
				
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(targetAccountTo.getAccountPk());
				holderAccount = getHolderAccountAndDetails(holderAccount);
	
				if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_ACCOUNT_NOT_REGISTERED));
					securityTransferOperation.setTargetHolderAccount(new HolderAccount());
					targetAccountTo = new HolderAccountHelperResultTO();
					return;
				}
				securityTransferOperation.setTargetHolderAccount(holderAccount);
	
				if(flagSameParticipant){
					if( 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()) && 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())
					){
						if( !transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation) ){
							securityTransferOperation.setTargetHolderAccount(new HolderAccount());
							targetAccountTo = new HolderAccountHelperResultTO();
							alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
							return;
						}
					}
				}
				
			}else{
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				targetAccountTo = new HolderAccountHelperResultTO();
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Gets the holder account and details.
	 *
	 * @param holderAccount the holder account
	 * @return the holder account and details
	 */
	public HolderAccount getHolderAccountAndDetails(HolderAccount holderAccount) {
		try {
			holderAccount = transferAvailableFacade.getHolderAccount(holderAccount);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return holderAccount;
	}

	/**
	 * Load holder account state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadHolderAccountState() throws ServiceException{
		lstHolderAccountState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstHolderAccountState.add(param);
			mapStateHolderAccount.put(param.getParameterTablePk(), param.getDescription());
		}
	}
	
	/**
	 * Gets the map holder account state.
	 *
	 * @param code the code
	 * @return the map holder account state
	 */
	public String getMapHolderAccountState(Integer code){
		return mapStateHolderAccount.get(code);
	}
	
	/**
	 * Valid target holder.
	 */
	public void validTargetHolder(){
		
		if(Validations.validateIsNotNull(targetHolder) && Validations.validateIsNotNullAndPositive(targetHolder.getIdHolderPk())){
			if(!targetHolder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
				targetAccountTo = new HolderAccountHelperResultTO();
				lstTargetAccountTo = null; 
				return;
			}
			
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(targetHolder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
			
			try {
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
				lstTargetAccountTo = lstAccountTo;
				if(lstTargetAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER)
				{
					targetAccountTo=lstTargetAccountTo.get(0);
				}
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		}
	}

	/**
	 * Sol number valid.
	 */
	public void solNumberValid(){
		if(Validations.validateIsNotNull(custodyOperation) && Validations.validateIsNotNull(custodyOperation.getOperationNumber())){
			if(custodyOperation.getOperationNumber().equals(0L)){
				custodyOperation.setOperationNumber(null);
			}
		}	 
	}
	
	/**
	 * Valid source holder.
	 */
	public void validSourceHolder(){
		if(Validations.validateIsNotNull(sourceHolder) && Validations.validateIsNotNullAndPositive(sourceHolder.getIdHolderPk())){
			if(!sourceHolder.getStateHolder().equals(HolderStateType.REGISTERED.getCode())){
				alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
				sourceHolder = new Holder();
				return;
			}
			
			HolderTO holderTO = new HolderTO();
			holderTO.setHolderId(sourceHolder.getIdHolderPk());
			holderTO.setFlagAllHolders(true);
			holderTO.setParticipantFk(securityTransferOperation.getSourceParticipant().getIdParticipantPk());
			
			try {
				List<HolderAccountHelperResultTO> lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
			
			if(lstAccountTo.size()>0){
				lstSourceAccountTo = lstAccountTo;
				
				if(flagSameParticipant){
					targetHolder.setIdHolderPk(sourceHolder.getIdHolderPk());
					targetHolder.setDescriptionHolder(sourceHolder.getDescriptionHolder());
					targetHolder.setFirstLastName(sourceHolder.getFirstLastName());
					targetHolder.setName(sourceHolder.getName());
					targetHolder.setFullName(sourceHolder.getFullName());
					targetHolder.setSecondLastName(sourceHolder.getSecondLastName());
					targetHolder.setHolderType(sourceHolder.getHolderType());

					holderTO = new HolderTO();
					holderTO.setHolderId(sourceHolder.getIdHolderPk());
					holderTO.setFlagAllHolders(true);
					holderTO.setParticipantFk(securityTransferOperation.getTargetParticipant().getIdParticipantPk());
					lstAccountTo = helperComponentFacade.searchAccountByFilter(holderTO);
					lstTargetAccountTo = lstAccountTo;
				}
				if(lstAccountTo.size()==GeneralConstants.ONE_VALUE_INTEGER){
					sourceAccountTo=lstAccountTo.get(0);
					validSourceHolderAccount();
				}
				
			}else{
				sourceHolder = new Holder();
				lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				sourceAccountTo = new HolderAccountHelperResultTO();
				String mensaje=PropertiesUtilities.getMessage(PropertiesConstants.COMMONS_LABEL_PARTICIPANT_CUI_INCORRECT);
				alert(mensaje);
			}
				
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
		}else{
			
			dataTableRegistry();
			securityTransferOperation.setSourceHolderAccount(new HolderAccount());
			lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
			sourceAccountTo = new HolderAccountHelperResultTO();
			
			securityTransferOperation.setSecurities(new Security());
			
			if(flagSameParticipant){
				securityTransferOperation.setTargetHolderAccount(new HolderAccount());
				lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
				targetAccountTo = new HolderAccountHelperResultTO();
			}

		}
	}
	
	/**
	 * Valid source holder account.
	 */
	public void validSourceHolderAccount() {
		try {
			
			if(Validations.validateIsNotNull(sourceAccountTo) && Validations.validateIsNotNullAndPositive(sourceAccountTo.getAccountNumber()) ){
				HolderAccount holderAccount = new HolderAccount();
				holderAccount.setIdHolderAccountPk(sourceAccountTo.getAccountPk());
				holderAccount = getHolderAccountAndDetails(holderAccount);
				
				dataTableRegistry();
				securityTransferOperation.setSecurities(new Security());
	
				if (!holderAccount.getStateAccount().equals(HolderAccountStatusType.ACTIVE.getCode())) {
					alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_NOT_REGISTERED));
					securityTransferOperation.setSourceHolderAccount(new HolderAccount());
					sourceAccountTo = new HolderAccountHelperResultTO();
					
					securityTransferOperation.setSourceHolderAccount(new HolderAccount());
					if(flagSameParticipant){
						securityTransferOperation.setTargetHolderAccount(new HolderAccount());
						lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
						targetAccountTo = new HolderAccountHelperResultTO();
						for(HolderAccountHelperResultTO obj:lstTargetAccountTo){
							obj.setDisabled(false);
						}
					}
					return;
				}
				securityTransferOperation.setSourceHolderAccount(holderAccount);
	
				if(flagSameParticipant){
					boolean disableAnyRegistry=false;
					for(HolderAccountHelperResultTO obj:lstTargetAccountTo){
						if(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk().equals(obj.getAccountPk())){
							obj.setDisabled(true);
							disableAnyRegistry=true;
						}else{
							obj.setDisabled(false);
						}
					}
					if(disableAnyRegistry){
						return;
					}
					
					if( 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getSourceHolderAccount().getIdHolderAccountPk()) && 
						Validations.validateIsNotNullAndPositive(securityTransferOperation.getTargetHolderAccount().getIdHolderAccountPk())
					){	
						
						if( !transferAvailableFacade.equalsRntWithHolderAccount(securityTransferOperation) ){
							securityTransferOperation.setTargetHolderAccount(new HolderAccount());
							targetAccountTo = new HolderAccountHelperResultTO();
							alert(PropertiesUtilities.getMessage(PropertiesConstants.TDV_MSG_HOLDER_EQUALS_RNT));
							return;
						}
					}
				}
				
				dataTableRegistry();
	
			
			}else{
				alert(PropertiesUtilities.getMessage(PropertiesConstants.ALERT_ACCOUNT_NOT_SELECTED));
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Listener modify motives.
	 */
	public void listenerModifyMotives(){
		if(Validations.validateIsNotNullAndPositive(intRejectMotive)){
			intHiddenMotive++;
		}
		if(Validations.validateIsNotNullAndNotEmpty(strRejectMotive)){
			intHiddenMotive++;	
		}
	}

	/**
	 * Data table registry.
	 */
	public void dataTableRegistry() {
		lstNewSecurityBlockTransferTO = null;
		dataModelSecurityBlockTransfer = null;
		arrSecurityBlockTransfer = null;
	}

	/**
	 * Load participant.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadParticipant() throws ServiceException {
		lstParticipant = new ArrayList<Participant>();
		Participant filter = new Participant();
		List<Integer> states = new ArrayList<Integer>();
		states.add(ParticipantStateType.REGISTERED.getCode());
		filter.setLstParticipantStates(states);
		lstTargetParticipant=accountsFacade.getLisParticipantServiceBean(filter);
		if (userInfo.getUserAccountSession().isDepositaryInstitution()){
			states.add(ParticipantStateType.BLOCKED.getCode());
			states.add(ParticipantStateType.UNIFIED.getCode());
		}
		filter.setLstParticipantStates(states);
		lstParticipant = accountsFacade.getLisParticipantServiceBean(filter);
	}

	/**
	 * Load instrument type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadInstrumentType() throws ServiceException {
		lstInstrumentType = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.INSTRUMENT_TYPE.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		for (ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)) {
			lstInstrumentType.add(param);
		}
	}

	/**
	 * Load security class.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadSecurityClass() throws ServiceException{
		lstSecurityClass = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		paramTable.setState(GeneralConstants.ONE_VALUE_INTEGER);
		paramTable.setOrderByText1(1);
		for(ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)){
			lstSecurityClass.add(param);
		}
		billParametersTableMap(lstSecurityClass, true);
	}
	
	/**
	 * Load transfer type.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTransferType() throws ServiceException {
		lstTransferType = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.TRANSFER_TYPE.getCode());
		paramTable.setState(1);
		for (ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)) {
			if (!param.getParameterTablePk().equals(SecuritiesTransferType.DESTINO_ORIGEN.getCode()))
				lstTransferType.add(param);
		}
	}

	/**
	 * Load transfer state.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTransferState() throws ServiceException {
		lstTransferState = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.SECURITY_TRANSFER_STATE.getCode());
		lstTransferState = generalParametersFacade.getComboParameterTable(paramTable);
		for (ParameterTable parameterTable : lstTransferState) {
			mapStateSecurityTransferOperation.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
	}

	/**
	 * Load transfer actions.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadTransferActions() throws ServiceException {
		lstTransferActions = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.TRANSFER_ACTIONS.getCode());
		lstTransferActions = generalParametersFacade.getComboParameterTable(paramTable);
	}

	/**
	 * Load block types.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadBlockTypes() throws ServiceException {
		lstBlockType = new ArrayList<ParameterTable>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setMasterTableFk(MasterTableType.AFFECTATION_TYPE.getCode());
		for (ParameterTable param : generalParametersFacade.getComboParameterTable(paramTable)) {
			// if(!param.getParameterTablePk().equals(AffectationType.RESERVE.getCode()))
			lstBlockType.add(param);
		}
	}

	/**
	 * Load combos.
	 */
	public void loadCombos() {
		try {

			if(lstHolderAccountState.size() <=0){
				loadHolderAccountState();
			}
			
			if (lstTransferType.size() <= 0) {
				loadTransferType();
			}

			if (lstParticipant.size() <= 0) {
				if(isParticipant) {
					
					loadParticipant();
					if(Validations.validateIsNotNull(Defaultparticipant) && 
					   Validations.validateIsNotNullAndPositive(Defaultparticipant.getIdParticipantPk())){
						boolean flagInList = false;
						for(Participant obj: lstParticipant){
							if(obj.getIdParticipantPk().equals(Defaultparticipant.getIdParticipantPk())){
								flagInList = true;
								break;
							}
						}
						if(!flagInList){
							lstParticipant.add(Defaultparticipant);
						}
					}
					
				} else {
					loadParticipant();
				}
			}

			if (lstTransferState.size() <= 0) {
				loadTransferState();
			}

			if (lstTransferActions.size() <= 0) {
				loadTransferActions();
			}

			if (lstInstrumentType.size() <= 0) {
				loadInstrumentType();
			}
			
			if (lstSecurityClass.size() <= 0) {
				loadSecurityClass();
			}
			
			if (lstBlockType.size() <= 0) {
				loadBlockTypes();
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Post constructor search.
	 */
	@PostConstruct
	public void postConstructorSearch() {
		
		radioSelected = 1;
		minTime = CommonsUtilities.addDate(CommonsUtilities.currentDate(), -maxDaysOfCustodyRequest);
		securityTO.setOperationInitDate(CommonsUtilities.currentDate());
		securityTO.setOperationEndDate(CommonsUtilities.currentDate());

		securityTransferOperation = inicializeSecurityTransferOperation();
		minTime = CommonsUtilities.addDate(CommonsUtilities.currentDate(), -maxDaysOfCustodyRequest);
		securityEvents();
		loadCombos();

	}

	/**
	 * Post constructor new.
	 *
	 * @return the string
	 */
	public String postConstructorNew() {
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
				&& Validations.validateIsNullOrNotPositive(participantOrigin.getIdParticipantPk())){
			validationMessage = PropertiesUtilities.getMessage(PropertiesConstants.PROPERTYFILE,
					 FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					 PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
			alert(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),validationMessage);
			return "";
		}
		
		JSFUtilities.setValidViewComponentAndChildrens("frmCustody");
		JSFUtilities.setValidViewComponent("frmCustody:idCuiTargetHolder:idCuiTargetHolder");
		JSFUtilities.setValidViewComponent("frmCustody:idCuiSourceHolder:idCuiSourceHolder");
		JSFUtilities.setValidViewComponent("frmCustody:idSourceSecurity:idSourceSecurity");
		
		initScreenNew();
		return "newTransferBlock";
	}
	
	/**
	 * Clean registry.
	 */
	public void cleanRegistry() {
		initScreenNew();
		JSFUtilities.resetViewRoot();
	}

	/**
	 * Inits the screen new.
	 */
	public void initScreenNew() {
		flagSameParticipant = false;
		flagSourceTarget = true;
		registersFound = true;
		loadCombos();
		securityEvents();
		securityTransferOperation = inicializeSecurityTransferOperation();
		securityTransferOperation.setTransferType(SecuritiesTransferType.ORIGEN_DESTINO.getCode());
		blockRequest = new BlockRequest();
		//se agrego por los helpers
		lstSourceAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		lstTargetAccountTo = new ArrayList<HolderAccountHelperResultTO>();
		sourceAccountTo = new HolderAccountHelperResultTO();
		targetAccountTo = new HolderAccountHelperResultTO();
		sourceHolder = new Holder();
		targetHolder = new Holder();
		
		lstNewSecurityBlockTransferTO = null;
		dataModelSecurityBlockTransfer = null;
		arrSecurityBlockTransfer = null;
		
		if(isParticipant){
			securityTransferOperation.setSourceParticipant(Defaultparticipant);
		}
	}

	/**
	 * Security events.
	 */
	public void securityEvents() {
		
		try {

			if (userInfo.getUserAccountSession().isParticipantInstitucion()||
					userInfo.getUserAccountSession().isIssuerDpfInstitucion() ||
					userInfo.getUserAccountSession().isParticipantInvestorInstitucion()) {
				isParticipant = true;
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){					
					Long participantDpf = getIssuerDpfInstitution(userInfo);									
					if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
						participantLogin = participantDpf;
					}
				}else if (Validations.validateIsNotNullAndPositive(userInfo.getUserAccountSession().getParticipantCode())) {
					participantLogin = userInfo.getUserAccountSession().getParticipantCode();
				}
				if(participantLogin !=null &&  participantLogin>0){
					if (!Validations.validateIsNotNullAndPositive(Defaultparticipant.getIdParticipantPk())) {
						Defaultparticipant = helperComponentFacade.findParticipantsByCode(participantLogin);
					}
					participantOrigin = Defaultparticipant;
				}				
			} else if (userInfo.getUserAccountSession().isDepositaryInstitution()) {
				isDepositary = true;
			}

			if (userPrivilege.getUserAcctions().isRegister()) {
				flagRegister = true;
			}

			if (userPrivilege.getUserAcctions().isSearch()) {
				flagSearch = true;
			}

		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Inicialize security transfer operation.
	 *
	 * @param securityTransferOperation the security transfer operation
	 * @return the security transfer operation
	 */
	public SecurityTransferOperation inicializeSecurityTransferOperation() {
		securityTransferOperation = new SecurityTransferOperation();
		CustodyOperation custodyOperation = new CustodyOperation();
		Participant participant = new Participant();
		Participant sourceParticipant = new Participant();
		Participant targetParticipant = new Participant();
		Security security = new Security();
		HolderAccount holderAccount = new HolderAccount();
		HolderAccount sourceHolderAccount = new HolderAccount();
		HolderAccount targetHolderAccount = new HolderAccount();
		securityTransferOperation.setCustodyOperation(custodyOperation);
		securityTransferOperation.setHolderAccount(holderAccount);
		securityTransferOperation.setSourceHolderAccount(sourceHolderAccount);
		securityTransferOperation.setTargetHolderAccount(targetHolderAccount);
		securityTransferOperation.setSecurities(security);
		securityTransferOperation.setParticipant(participant);
		securityTransferOperation.setSourceParticipant(sourceParticipant);
		securityTransferOperation.setTargetParticipant(targetParticipant);
		securityTransferOperation.getCustodyOperation().setOperationInit(CommonsUtilities.currentDate());
		securityTransferOperation.getCustodyOperation().setOperationEnd(CommonsUtilities.currentDate());
		return securityTransferOperation;
	}

	/**
	 * Alert.
	 *
	 * @param message the message
	 */
	public void alert(String message) {
		String validationMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, 
							FacesContext.getCurrentInstance().getViewRoot().getLocale(),
							GeneralConstants.LBL_HEADER_ALERT_WARNING);

		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	/**
	 * Alter reject cancel.
	 *
	 * @param validationMessage the validation message
	 */
	public void alterRejectCancel(String validationMessage) {
		showMessageOnDialog(validationMessage, "");
		JSFUtilities.executeJavascriptFunction("PF('alterRejectWidget').show();");
	}

	/**
	 * Alert.
	 *
	 * @param header the header
	 * @param message the message
	 */
	public void alert(String header, String message) {
		showMessageOnDialog(header, message);
		JSFUtilities.executeJavascriptFunction("PF('alterValidationWidget').show();");
	}

	/**
	 * Question.
	 *
	 * @param message the message
	 */
	public void question(String message) {
		validationMessage = PropertiesUtilities.getMessage(GeneralConstants.PROPERTY_FILE_GENERIC_MESSAGES, 
														   FacesContext.getCurrentInstance().getViewRoot().getLocale(),
														   GeneralConstants.LBL_HEADER_ALERT_WARNING);
		showMessageOnDialog(validationMessage, message);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}

	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable){
		billParametersTableMap(lstParameterTable,false);
	}
	
	/**
	 * Bill parameters table map.
	 *
	 * @param lstParameterTable the lst parameter table
	 * @param isObj the is obj
	 */
	public void billParametersTableMap(List<ParameterTable> lstParameterTable, boolean isObj){
		if(super.getParametersTableMap()==null){
			super.setParametersTableMap(new HashMap<Integer,Object>());
		}
		for(ParameterTable pTable : lstParameterTable){
			if(isObj){
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable);
			}else{
				super.getParametersTableMap().put(pTable.getParameterTablePk(), pTable.getParameterName());
			}
		}
	}
	
	/**
	 * Question.
	 *
	 * @param message the message
	 * @param header the header
	 */
	public void question(String message, String header) {
		showMessageOnDialog(header, message);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}

	/**
	 * ************ GET AND SET *************************.
	 *
	 * @return the security transfer operation
	 */

	public SecurityTransferOperation getSecurityTransferOperation() {
		return securityTransferOperation;
	}

	/**
	 * Sets the security transfer operation.
	 *
	 * @param securityTransferOperation the new security transfer operation
	 */
	public void setSecurityTransferOperation(SecurityTransferOperation securityTransferOperation) {
		this.securityTransferOperation = securityTransferOperation;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custody operation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the new custody operation
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the block operation.
	 *
	 * @return the block operation
	 */
	public BlockOperation getBlockOperation() {
		return blockOperation;
	}

	/**
	 * Sets the block operation.
	 *
	 * @param blockOperation the new block operation
	 */
	public void setBlockOperation(BlockOperation blockOperation) {
		this.blockOperation = blockOperation;
	}

	/**
	 * Gets the security to.
	 *
	 * @return the security to
	 */
	public SecurityTransferOperationTO getSecurityTO() {
		return securityTO;
	}

	/**
	 * Sets the security to.
	 *
	 * @param securityTO the new security to
	 */
	public void setSecurityTO(SecurityTransferOperationTO securityTO) {
		this.securityTO = securityTO;
	}

	/**
	 * Gets the participant origin.
	 *
	 * @return the participant origin
	 */
	public Participant getParticipantOrigin() {
		return participantOrigin;
	}

	/**
	 * Sets the participant origin.
	 *
	 * @param participantOrigin the new participant origin
	 */
	public void setParticipantOrigin(Participant participantOrigin) {
		this.participantOrigin = participantOrigin;
	}

	/**
	 * Gets the participant target.
	 *
	 * @return the participant target
	 */
	public Participant getParticipantTarget() {
		return participantTarget;
	}

	/**
	 * Sets the participant target.
	 *
	 * @param participantTarget the new participant target
	 */
	public void setParticipantTarget(Participant participantTarget) {
		this.participantTarget = participantTarget;
	}

	/**
	 * Gets the participant.
	 *
	 * @return the participant
	 */
	public Participant getParticipant() {
		return participant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param participant the new participant
	 */
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}

	/**
	 * Gets the security origin.
	 *
	 * @return the security origin
	 */
	public Security getSecurityOrigin() {
		return securityOrigin;
	}

	/**
	 * Sets the security origin.
	 *
	 * @param securityOrigin the new security origin
	 */
	public void setSecurityOrigin(Security securityOrigin) {
		this.securityOrigin = securityOrigin;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the new security
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the holder account origin.
	 *
	 * @return the holder account origin
	 */
	public HolderAccount getHolderAccountOrigin() {
		return holderAccountOrigin;
	}

	/**
	 * Sets the holder account origin.
	 *
	 * @param holderAccountOrigin the new holder account origin
	 */
	public void setHolderAccountOrigin(HolderAccount holderAccountOrigin) {
		this.holderAccountOrigin = holderAccountOrigin;
	}

	/**
	 * Gets the holder account target.
	 *
	 * @return the holder account target
	 */
	public HolderAccount getHolderAccountTarget() {
		return holderAccountTarget;
	}

	/**
	 * Sets the holder account target.
	 *
	 * @param holderAccountTarget the new holder account target
	 */
	public void setHolderAccountTarget(HolderAccount holderAccountTarget) {
		this.holderAccountTarget = holderAccountTarget;
	}

	/**
	 * Gets the holder account.
	 *
	 * @return the holder account
	 */
	public HolderAccount getHolderAccount() {
		return holderAccount;
	}

	/**
	 * Sets the holder account.
	 *
	 * @param holderAccount the new holder account
	 */
	public void setHolderAccount(HolderAccount holderAccount) {
		this.holderAccount = holderAccount;
	}

	/**
	 * Gets the lst transfer type.
	 *
	 * @return the lst transfer type
	 */
	public List<ParameterTable> getLstTransferType() {
		return lstTransferType;
	}

	/**
	 * Sets the lst transfer type.
	 *
	 * @param lstTransferType the new lst transfer type
	 */
	public void setLstTransferType(List<ParameterTable> lstTransferType) {
		this.lstTransferType = lstTransferType;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Gets the lst transfer state.
	 *
	 * @return the lst transfer state
	 */
	public List<ParameterTable> getLstTransferState() {
		return lstTransferState;
	}

	/**
	 * Sets the lst transfer state.
	 *
	 * @param lstTransferState the new lst transfer state
	 */
	public void setLstTransferState(List<ParameterTable> lstTransferState) {
		this.lstTransferState = lstTransferState;
	}

	/**
	 * Gets the lst transfer actions.
	 *
	 * @return the lst transfer actions
	 */
	public List<ParameterTable> getLstTransferActions() {
		return lstTransferActions;
	}

	/**
	 * Sets the lst transfer actions.
	 *
	 * @param lstTransferActions the new lst transfer actions
	 */
	public void setLstTransferActions(List<ParameterTable> lstTransferActions) {
		this.lstTransferActions = lstTransferActions;
	}

	/**
	 * Gets the validation message.
	 *
	 * @return the validation message
	 */
	public String getValidationMessage() {
		return validationMessage;
	}

	/**
	 * Sets the validation message.
	 *
	 * @param validationMessage the new validation message
	 */
	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}

	/**
	 * Gets the str transfer type.
	 *
	 * @return the str transfer type
	 */
	public String getStrTransferType() {
		return strTransferType;
	}

	/**
	 * Sets the str transfer type.
	 *
	 * @param strTransferType the new str transfer type
	 */
	public void setStrTransferType(String strTransferType) {
		this.strTransferType = strTransferType;
	}

	/**
	 * Checks if is flag search enabled.
	 *
	 * @return true, if is flag search enabled
	 */
	public boolean isFlagSearchEnabled() {
		return flagSearchEnabled;
	}

	/**
	 * Sets the flag search enabled.
	 *
	 * @param flagSearchEnabled the new flag search enabled
	 */
	public void setFlagSearchEnabled(boolean flagSearchEnabled) {
		this.flagSearchEnabled = flagSearchEnabled;
	}

	/**
	 * Gets the lst security block transfer to.
	 *
	 * @return the lst security block transfer to
	 */
	public List<SecurityBlockTransferTO> getLstSecurityBlockTransferTO() {
		return lstSecurityBlockTransferTO;
	}

	/**
	 * Sets the lst security block transfer to.
	 *
	 * @param lstSecurityBlockTransferTO the new lst security block transfer to
	 */
	public void setLstSecurityBlockTransferTO(
			List<SecurityBlockTransferTO> lstSecurityBlockTransferTO) {
		this.lstSecurityBlockTransferTO = lstSecurityBlockTransferTO;
	}

	/**
	 * Checks if is flag revised sto.
	 *
	 * @return true, if is flag revised sto
	 */
	public boolean isFlagRevisedSto() {
		return flagRevisedSto;
	}

	/**
	 * Sets the flag revised sto.
	 *
	 * @param flagRevisedSto the new flag revised sto
	 */
	public void setFlagRevisedSto(boolean flagRevisedSto) {
		this.flagRevisedSto = flagRevisedSto;
	}

	/**
	 * Checks if is flag reject sto.
	 *
	 * @return true, if is flag reject sto
	 */
	public boolean isFlagRejectSto() {
		return flagRejectSto;
	}

	/**
	 * Sets the flag reject sto.
	 *
	 * @param flagRejectSto the new flag reject sto
	 */
	public void setFlagRejectSto(boolean flagRejectSto) {
		this.flagRejectSto = flagRejectSto;
	}

	/**
	 * Checks if is flag approved sto.
	 *
	 * @return true, if is flag approved sto
	 */
	public boolean isFlagApprovedSto() {
		return flagApprovedSto;
	}

	/**
	 * Sets the flag approved sto.
	 *
	 * @param flagApprovedSto the new flag approved sto
	 */
	public void setFlagApprovedSto(boolean flagApprovedSto) {
		this.flagApprovedSto = flagApprovedSto;
	}

	/**
	 * Checks if is flasg annuled sto.
	 *
	 * @return true, if is flasg annuled sto
	 */
	public boolean isFlasgAnnuledSto() {
		return flasgAnnuledSto;
	}

	/**
	 * Sets the flasg annuled sto.
	 *
	 * @param flasgAnnuledSto the new flasg annuled sto
	 */
	public void setFlasgAnnuledSto(boolean flasgAnnuledSto) {
		this.flasgAnnuledSto = flasgAnnuledSto;
	}

	/**
	 * Checks if is flag confirm sto.
	 *
	 * @return true, if is flag confirm sto
	 */
	public boolean isFlagConfirmSto() {
		return flagConfirmSto;
	}

	/**
	 * Sets the flag confirm sto.
	 *
	 * @param flagConfirmSto the new flag confirm sto
	 */
	public void setFlagConfirmSto(boolean flagConfirmSto) {
		this.flagConfirmSto = flagConfirmSto;
	}

	/**
	 * Gets the lst block type.
	 *
	 * @return the lst block type
	 */
	public List<ParameterTable> getLstBlockType() {
		return lstBlockType;
	}

	/**
	 * Sets the lst block type.
	 *
	 * @param lstBlockType the new lst block type
	 */
	public void setLstBlockType(List<ParameterTable> lstBlockType) {
		this.lstBlockType = lstBlockType;
	}

	/**
	 * Gets the block request.
	 *
	 * @return the block request
	 */
	public BlockRequest getBlockRequest() {
		return blockRequest;
	}

	/**
	 * Sets the block request.
	 *
	 * @param blockRequest the new block request
	 */
	public void setBlockRequest(BlockRequest blockRequest) {
		this.blockRequest = blockRequest;
	}

	/**
	 * Gets the data model security block transfer.
	 *
	 * @return the data model security block transfer
	 */
	public SecurityBlockTransferDataModel getDataModelSecurityBlockTransfer() {
		return dataModelSecurityBlockTransfer;
	}

	/**
	 * Sets the data model security block transfer.
	 *
	 * @param dataModelSecurityBlockTransfer the new data model security block transfer
	 */
	public void setDataModelSecurityBlockTransfer(
			SecurityBlockTransferDataModel dataModelSecurityBlockTransfer) {
		this.dataModelSecurityBlockTransfer = dataModelSecurityBlockTransfer;
	}

	/**
	 * Gets the arr security block transfer.
	 *
	 * @return the arr security block transfer
	 */
	public SecurityBlockTransferTO[] getArrSecurityBlockTransfer() {
		return arrSecurityBlockTransfer;
	}

	/**
	 * Sets the arr security block transfer.
	 *
	 * @param arrSecurityBlockTransfer the new arr security block transfer
	 */
	public void setArrSecurityBlockTransfer(
			SecurityBlockTransferTO[] arrSecurityBlockTransfer) {
		this.arrSecurityBlockTransfer = arrSecurityBlockTransfer;
	}

	/**
	 * Gets the lst action security block transfer to.
	 *
	 * @return the lst action security block transfer to
	 */
	public List<SecurityBlockTransferTO> getLstActionSecurityBlockTransferTO() {
		return lstActionSecurityBlockTransferTO;
	}

	/**
	 * Gets the security view transfer to.
	 *
	 * @return the security view transfer to
	 */
	public SecurityBlockTransferTO getSecurityViewTransferTo() {
		return securityViewTransferTo;
	}

	/**
	 * Sets the security view transfer to.
	 *
	 * @param securityViewTransferTo the new security view transfer to
	 */
	public void setSecurityViewTransferTo(
			SecurityBlockTransferTO securityViewTransferTo) {
		this.securityViewTransferTo = securityViewTransferTo;
	}

	/**
	 * Sets the lst action security block transfer to.
	 *
	 * @param lstActionSecurityBlockTransferTO the new lst action security block transfer to
	 */
	public void setLstActionSecurityBlockTransferTO(
			List<SecurityBlockTransferTO> lstActionSecurityBlockTransferTO) {
		this.lstActionSecurityBlockTransferTO = lstActionSecurityBlockTransferTO;
	}

	/**
	 * Gets the security block transfertmp.
	 *
	 * @return the security block transfertmp
	 */
	public SecurityBlockTransferTO getSecurityBlockTransfertmp() {
		return securityBlockTransfertmp;
	}

	/**
	 * Sets the security block transfertmp.
	 *
	 * @param securityBlockTransfertmp the new security block transfertmp
	 */
	public void setSecurityBlockTransfertmp(
			SecurityBlockTransferTO securityBlockTransfertmp) {
		this.securityBlockTransfertmp = securityBlockTransfertmp;
	}

	/**
	 * Gets the lst new security block transfer to.
	 *
	 * @return the lst new security block transfer to
	 */
	public List<SecurityBlockTransferTO> getLstNewSecurityBlockTransferTO() {
		return lstNewSecurityBlockTransferTO;
	}

	/**
	 * Sets the lst new security block transfer to.
	 *
	 * @param lstNewSecurityBlockTransferTO the new lst new security block transfer to
	 */
	public void setLstNewSecurityBlockTransferTO(
			List<SecurityBlockTransferTO> lstNewSecurityBlockTransferTO) {
		this.lstNewSecurityBlockTransferTO = lstNewSecurityBlockTransferTO;
	}

	/**
	 * Gets the now time.
	 *
	 * @return the now time
	 */
	public Date getNowTime() {
		return nowTime;
	}

	/**
	 * Sets the now time.
	 *
	 * @param nowTime the new now time
	 */
	public void setNowTime(Date nowTime) {
		this.nowTime = nowTime;
	}

	/**
	 * Gets the min time.
	 *
	 * @return the min time
	 */
	public Date getMinTime() {
		return minTime;
	}

	/**
	 * Sets the min time.
	 *
	 * @param minTime the new min time
	 */
	public void setMinTime(Date minTime) {
		this.minTime = minTime;
	}

	/**
	 * Gets the radio selected.
	 *
	 * @return the radio selected
	 */
	public Integer getRadioSelected() {
		return radioSelected;
	}

	/**
	 * Sets the radio selected.
	 *
	 * @param radioSelected the new radio selected
	 */
	public void setRadioSelected(Integer radioSelected) {
		this.radioSelected = radioSelected;
	}

	/**
	 * Gets the registers found.
	 *
	 * @return the registers found
	 */
	public Boolean getRegistersFound() {
		return registersFound;
	}

	/**
	 * Sets the registers found.
	 *
	 * @param registersFound the new registers found
	 */
	public void setRegistersFound(Boolean registersFound) {
		this.registersFound = registersFound;
	}

	/**
	 * Gets the str logger user.
	 *
	 * @return the str logger user
	 */
	public String getStrLoggerUser() {
		return strLoggerUser;
	}

	/**
	 * Sets the str logger user.
	 *
	 * @param strLoggerUser the new str logger user
	 */
	public void setStrLoggerUser(String strLoggerUser) {
		this.strLoggerUser = strLoggerUser;
	}

	/**
	 * Sets the participant login.
	 *
	 * @param participantLogin the new participant login
	 */
	public void setParticipantLogin(Long participantLogin) {
		this.participantLogin = participantLogin;
	}

	/**
	 * Gets the participant login.
	 *
	 * @return the participant login
	 */
	public Long getParticipantLogin() {
		return participantLogin;
	}

	/**
	 * Gets the checks if is participant.
	 *
	 * @return the checks if is participant
	 */
	public boolean getIsParticipant() {
		return isParticipant;
	}

	/**
	 * Gets the checks if is depositary.
	 *
	 * @return the checks if is depositary
	 */
	public boolean getIsDepositary() {
		return isDepositary;
	}

	/**
	 * Gets the str document number.
	 *
	 * @return the str document number
	 */
	public String getStrDocumentNumber() {
		return strDocumentNumber;
	}

	/**
	 * Sets the str document number.
	 *
	 * @param strDocumentNumber the new str document number
	 */
	public void setStrDocumentNumber(String strDocumentNumber) {
		this.strDocumentNumber = strDocumentNumber;
	}

	/**
	 * Gets the str sate description.
	 *
	 * @return the str sate description
	 */
	public String getStrSateDescription() {
		return strSateDescription;
	}

	/**
	 * Sets the str sate description.
	 *
	 * @param strSateDescription the new str sate description
	 */
	public void setStrSateDescription(String strSateDescription) {
		this.strSateDescription = strSateDescription;
	}

	/**
	 * Checks if is flag register.
	 *
	 * @return true, if is flag register
	 */
	public boolean isFlagRegister() {
		return flagRegister;
	}

	/**
	 * Sets the flag register.
	 *
	 * @param flagRegister the new flag register
	 */
	public void setFlagRegister(boolean flagRegister) {
		this.flagRegister = flagRegister;
	}

	/**
	 * Checks if is flag search.
	 *
	 * @return true, if is flag search
	 */
	public boolean isFlagSearch() {
		return flagSearch;
	}

	/**
	 * Sets the flag search.
	 *
	 * @param flagSearch the new flag search
	 */
	public void setFlagSearch(boolean flagSearch) {
		this.flagSearch = flagSearch;
	}

	/**
	 * Gets the confirm alert action.
	 *
	 * @return the confirm alert action
	 */
	public String getconfirmAlertAction() {
		return confirmAlertAction;
	}

	/**
	 * Sets the confirm alert action.
	 *
	 * @param confirmAlertAction the new confirm alert action
	 */
	public void setconfirmAlertAction(String confirmAlertAction) {
		this.confirmAlertAction = confirmAlertAction;
	}

	/**
	 * Gets the reject motive list.
	 *
	 * @return the reject motive list
	 */
	public List<ParameterTable> getRejectMotiveList() {
		return rejectMotiveList;
	}

	/**
	 * Sets the reject motive list.
	 *
	 * @param rejectMotiveList the new reject motive list
	 */
	public void setRejectMotiveList(List<ParameterTable> rejectMotiveList) {
		this.rejectMotiveList = rejectMotiveList;
	}

	/**
	 * Gets the other reject motive id.
	 *
	 * @return the other reject motive id
	 */
	public Integer getOtherRejectMotiveId() {
		return otherRejectMotiveId;
	}

	/**
	 * Sets the other reject motive id.
	 *
	 * @param otherRejectMotiveId the new other reject motive id
	 */
	public void setOtherRejectMotiveId(Integer otherRejectMotiveId) {
		this.otherRejectMotiveId = otherRejectMotiveId;
	}

	/**
	 * Gets the str reject motive.
	 *
	 * @return the str reject motive
	 */
	public String getStrRejectMotive() {
		return strRejectMotive;
	}

	/**
	 * Sets the str reject motive.
	 *
	 * @param strRejectMotive the new str reject motive
	 */
	public void setStrRejectMotive(String strRejectMotive) {
		this.strRejectMotive = strRejectMotive;
	}

	/**
	 * Gets the int reject motive.
	 *
	 * @return the int reject motive
	 */
	public Integer getIntRejectMotive() {
		return intRejectMotive;
	}

	/**
	 * Sets the int reject motive.
	 *
	 * @param intRejectMotive the new int reject motive
	 */
	public void setIntRejectMotive(Integer intRejectMotive) {
		this.intRejectMotive = intRejectMotive;
	}

	/**
	 * Checks if is flag source target.
	 *
	 * @return true, if is flag source target
	 */
	public boolean isFlagSourceTarget() {
		return flagSourceTarget;
	}

	/**
	 * Sets the flag source target.
	 *
	 * @param flagSourceTarget the new flag source target
	 */
	public void setFlagSourceTarget(boolean flagSourceTarget) {
		this.flagSourceTarget = flagSourceTarget;
	}

	/**
	 * Gets the lst instrument type.
	 *
	 * @return the lst instrument type
	 */
	public List<ParameterTable> getLstInstrumentType() {
		return lstInstrumentType;
	}

	/**
	 * Sets the lst instrument type.
	 *
	 * @param lstInstrumentType the new lst instrument type
	 */
	public void setLstInstrumentType(List<ParameterTable> lstInstrumentType) {
		this.lstInstrumentType = lstInstrumentType;
	}

	/**
	 * Checks if is depositary.
	 *
	 * @return true, if is depositary
	 */
	public boolean isDepositary() {
		return isDepositary;
	}

	/**
	 * Sets the depositary.
	 *
	 * @param isDepositary the new depositary
	 */
	public void setDepositary(boolean isDepositary) {
		this.isDepositary = isDepositary;
	}

	/**
	 * Checks if is flag same participant.
	 *
	 * @return true, if is flag same participant
	 */
	public boolean isFlagSameParticipant() {
		return flagSameParticipant;
	}

	/**
	 * Sets the flag same participant.
	 *
	 * @param flagSameParticipant the new flag same participant
	 */
	public void setFlagSameParticipant(boolean flagSameParticipant) {
		this.flagSameParticipant = flagSameParticipant;
	}

	/**
	 * Checks if is flag register origin participant.
	 *
	 * @return true, if is flag register origin participant
	 */
	public boolean isFlagRegisterOriginParticipant() {
		return flagRegisterOriginParticipant;
	}

	/**
	 * Sets the flag register origin participant.
	 *
	 * @param flagRegisterOriginParticipant the new flag register origin participant
	 */
	public void setFlagRegisterOriginParticipant(
			boolean flagRegisterOriginParticipant) {
		this.flagRegisterOriginParticipant = flagRegisterOriginParticipant;
	}

	/**
	 * Checks if is flag register target participant.
	 *
	 * @return true, if is flag register target participant
	 */
	public boolean isFlagRegisterTargetParticipant() {
		return flagRegisterTargetParticipant;
	}

	/**
	 * Sets the flag register target participant.
	 *
	 * @param flagRegisterTargetParticipant the new flag register target participant
	 */
	public void setFlagRegisterTargetParticipant(
			boolean flagRegisterTargetParticipant) {
		this.flagRegisterTargetParticipant = flagRegisterTargetParticipant;
	}

	/**
	 * Sets the participant.
	 *
	 * @param isParticipant the new participant
	 */
	public void setParticipant(boolean isParticipant) {
		this.isParticipant = isParticipant;
	}

	/**
	 * Gets the source holder.
	 *
	 * @return the source holder
	 */
	public Holder getSourceHolder() {
		return sourceHolder;
	}

	/**
	 * Sets the source holder.
	 *
	 * @param sourceHolder the new source holder
	 */
	public void setSourceHolder(Holder sourceHolder) {
		this.sourceHolder = sourceHolder;
	}

	/**
	 * Gets the target holder.
	 *
	 * @return the target holder
	 */
	public Holder getTargetHolder() {
		return targetHolder;
	}

	/**
	 * Sets the target holder.
	 *
	 * @param targetHolder the new target holder
	 */
	public void setTargetHolder(Holder targetHolder) {
		this.targetHolder = targetHolder;
	}

	/**
	 * Gets the source account to.
	 *
	 * @return the source account to
	 */
	public HolderAccountHelperResultTO getSourceAccountTo() {
		return sourceAccountTo;
	}

	/**
	 * Sets the source account to.
	 *
	 * @param sourceAccountTo the new source account to
	 */
	public void setSourceAccountTo(HolderAccountHelperResultTO sourceAccountTo) {
		this.sourceAccountTo = sourceAccountTo;
	}

	/**
	 * Gets the target account to.
	 *
	 * @return the target account to
	 */
	public HolderAccountHelperResultTO getTargetAccountTo() {
		return targetAccountTo;
	}

	/**
	 * Sets the target account to.
	 *
	 * @param targetAccountTo the new target account to
	 */
	public void setTargetAccountTo(HolderAccountHelperResultTO targetAccountTo) {
		this.targetAccountTo = targetAccountTo;
	}

	/**
	 * Gets the lst source account to.
	 *
	 * @return the lst source account to
	 */
	public List<HolderAccountHelperResultTO> getLstSourceAccountTo() {
		return lstSourceAccountTo;
	}

	/**
	 * Sets the lst source account to.
	 *
	 * @param lstSourceAccountTo the new lst source account to
	 */
	public void setLstSourceAccountTo(
			List<HolderAccountHelperResultTO> lstSourceAccountTo) {
		this.lstSourceAccountTo = lstSourceAccountTo;
	}

	/**
	 * Gets the lst target account to.
	 *
	 * @return the lst target account to
	 */
	public List<HolderAccountHelperResultTO> getLstTargetAccountTo() {
		return lstTargetAccountTo;
	}

	/**
	 * Sets the lst target account to.
	 *
	 * @param lstTargetAccountTo the new lst target account to
	 */
	public void setLstTargetAccountTo(
			List<HolderAccountHelperResultTO> lstTargetAccountTo) {
		this.lstTargetAccountTo = lstTargetAccountTo;
	}

	/**
	 * Gets the view error message.
	 *
	 * @return the view error message
	 */
	public String getViewErrorMessage() {
		return viewErrorMessage;
	}

	/**
	 * Sets the view error message.
	 *
	 * @param viewErrorMessage the new view error message
	 */
	public void setViewErrorMessage(String viewErrorMessage) {
		this.viewErrorMessage = viewErrorMessage;
	}

	/**
	 * Gets the lst security block transfer to view.
	 *
	 * @return the lst security block transfer to view
	 */
	public List<SecurityBlockTransferTO> getLstSecurityBlockTransferTOView() {
		return lstSecurityBlockTransferTOView;
	}

	/**
	 * Sets the lst security block transfer to view.
	 *
	 * @param lstSecurityBlockTransferTOView the new lst security block transfer to view
	 */
	public void setLstSecurityBlockTransferTOView(
			List<SecurityBlockTransferTO> lstSecurityBlockTransferTOView) {
		this.lstSecurityBlockTransferTOView = lstSecurityBlockTransferTOView;
	}

	/**
	 * Gets the data model security block transfer view.
	 *
	 * @return the data model security block transfer view
	 */
	public SecurityBlockTransferDataModel getDataModelSecurityBlockTransferView() {
		return dataModelSecurityBlockTransferView;
	}

	/**
	 * Sets the data model security block transfer view.
	 *
	 * @param dataModelSecurityBlockTransferView the new data model security block transfer view
	 */
	public void setDataModelSecurityBlockTransferView(
			SecurityBlockTransferDataModel dataModelSecurityBlockTransferView) {
		this.dataModelSecurityBlockTransferView = dataModelSecurityBlockTransferView;
	}

	/**
	 * Gets the int hidden motive.
	 *
	 * @return the int hidden motive
	 */
	public Integer getIntHiddenMotive() {
		return intHiddenMotive;
	}

	/**
	 * Sets the int hidden motive.
	 *
	 * @param intHiddenMotive the new int hidden motive
	 */
	public void setIntHiddenMotive(Integer intHiddenMotive) {
		this.intHiddenMotive = intHiddenMotive;
	}

	/**
	 * Gets the lst security class.
	 *
	 * @return the lst security class
	 */
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}

	/**
	 * Sets the lst security class.
	 *
	 * @param lstSecurityClass the new lst security class
	 */
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}

	/**
	 * Checks if is flag account origin visible.
	 *
	 * @return true, if is flag account origin visible
	 */
	public boolean isFlagAccountOriginVisible() {
		return flagAccountOriginVisible;
	}

	/**
	 * Sets the flag account origin visible.
	 *
	 * @param flagAccountOriginVisible the new flag account origin visible
	 */
	public void setFlagAccountOriginVisible(boolean flagAccountOriginVisible) {
		this.flagAccountOriginVisible = flagAccountOriginVisible;
	}

	/**
	 * Checks if is flag account target visible.
	 *
	 * @return true, if is flag account target visible
	 */
	public boolean isFlagAccountTargetVisible() {
		return flagAccountTargetVisible;
	}

	/**
	 * Sets the flag account target visible.
	 *
	 * @param flagAccountTargetVisible the new flag account target visible
	 */
	public void setFlagAccountTargetVisible(boolean flagAccountTargetVisible) {
		this.flagAccountTargetVisible = flagAccountTargetVisible;
	}

	/**
	 * Gets the lst target participant.
	 *
	 * @return the lst target participant
	 */
	public List<Participant> getLstTargetParticipant() {
		return lstTargetParticipant;
	}

	/**
	 * Sets the lst target participant.
	 *
	 * @param lstTargetParticipant the new lst target participant
	 */
	public void setLstTargetParticipant(List<Participant> lstTargetParticipant) {
		this.lstTargetParticipant = lstTargetParticipant;
	}

	/**
	 * Checks if is flag participante investor.
	 *
	 * @return true, if is flag participante investor
	 */
	public boolean isFlagParticipanteInvestor() {
		return flagParticipanteInvestor;
	}

	/**
	 * Sets the flag participante investor.
	 *
	 * @param flagParticipanteInvestor the new flag participante investor
	 */
	public void setFlagParticipanteInvestor(boolean flagParticipanteInvestor) {
		this.flagParticipanteInvestor = flagParticipanteInvestor;
	}
	
}
