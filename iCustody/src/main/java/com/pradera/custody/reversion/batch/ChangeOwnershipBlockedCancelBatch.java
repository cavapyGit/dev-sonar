package com.pradera.custody.reversion.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.custody.ownershipexchange.facade.ChangeOwnershipServiceFacade;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class OfacMassiveRegisterProcess.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , Mar 21, 2013
 */
@BatchProcess(name = "ChangeOwnershipBlockedCancelBatch")
@RequestScoped
public class ChangeOwnershipBlockedCancelBatch implements JobExecution,
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;
	
	/** The change ownership service facade. */
	@EJB
	ChangeOwnershipServiceFacade changeOwnershipServiceFacade;

	/** The idepositary setup. */
	@Inject
	@DepositarySetup
	IdepositarySetup idepositarySetup;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com
	 * .pradera.model.process.ProcessLogger)
	 */
	@Override
	@Transactional
	public void startJob(ProcessLogger processLogger) {
		log.info(getClass().getName() + " being executed.");
		try {
			Date currentDate = CommonsUtilities.currentDate();
			changeOwnershipServiceFacade.automaticChangeOwnershipBlockedCancelations(currentDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
