package com.pradera.custody.reversion.batch;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class ReversalCustodyBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@BatchProcess(name = "ReversalCustodyBatch")
@RequestScoped
public class ReversalCustodyBatch implements JobExecution, Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The affectation cancel batch. */
	@Inject
	@BatchProcess(name = "AffectationCancelBatch")
	AffectationCancelBatch affectationCancelBatch;

	/** The change ownership availables cancel batch. */
	@Inject
	@BatchProcess(name = "ChangeOwnershipAvailablesCancelBatch")
	ChangeOwnershipAvailablesCancelBatch changeOwnershipAvailablesCancelBatch;

	/** The change ownership blocked cancel batch. */
	@Inject
	@BatchProcess(name = "ChangeOwnershipBlockedCancelBatch")
	ChangeOwnershipBlockedCancelBatch changeOwnershipBlockedCancelBatch;

	/** The detachment coupons cancel batch. */
	@Inject
	@BatchProcess(name = "DetachmentCouponsCancelBatch")
	DetachmentCouponsCancelBatch detachmentCouponsCancelBatch;

	/** The securities accreditation cancel batch. */
	@Inject
	@BatchProcess(name = "SecuritiesAccreditationCancelBatch")
	SecuritiesAccreditationCancelBatch securitiesAccreditationCancelBatch;

	/** The securities dematerialisation cancel batch. */
	@Inject
	@BatchProcess(name = "SecuritiesDematerialisationCancelBatch")
	SecuritiesDematerialisationCancelBatch securitiesDematerialisationCancelBatch;

	/** The securities loanable cancel batch. */
	@Inject
	@BatchProcess(name = "SecuritiesLoanableCancelBatch")
	SecuritiesLoanableCancelBatch securitiesLoanableCancelBatch;

	/** The securities reteirement cancel batch. */
	@Inject
	@BatchProcess(name = "SecuritiesReteirementCancelBatch")
	SecuritiesReteirementCancelBatch securitiesReteirementCancelBatch;

	/** The securities transfer availables cancel batch. */
	@Inject
	@BatchProcess(name = "SecuritiesTransferAvailablesCancelBatch")
	SecuritiesTransferAvailablesCancelBatch securitiesTransferAvailablesCancelBatch;

	/** The securities transfer blocked cancel batch. */
	@Inject
	@BatchProcess(name = "SecuritiesTransferBlockedCancelBatch")
	SecuritiesTransferBlockedCancelBatch securitiesTransferBlockedCancelBatch;
	
	/** The certificate deposit batch. */
	@Inject
	@BatchProcess(name = "CertificateDepositBatch")
	CertificateDepositBatch certificateDepositBatch;
	
	/** The request reversals batch. */
	@Inject
	@BatchProcess(name = "RequestReversalsBatch")
	RequestReversalsBatch requestReversalsBatch;
	
	/** The reteirement deposite batch. */
	@Inject
	@BatchProcess(name = "ReteirementDepositeBatch")
	ReteirementDepositeBatch reteirementDepositeBatch;
	
	
	/** The accreditation certificate batch. */
	@Inject
	@BatchProcess(name = "AccreditationCertificateReversalBatch")
	AccreditationCertificateReversalBatch accreditationCertificateReversalBatch;


	@Inject
	@BatchProcess(name = "DepositPhysicalCertificateCancelBatch")
	DepositPhysicalCertificateCancelBatch depositPhysicalCertificateCancelBatch;

	
	@Inject
	@BatchProcess(name = "RetirementPhysicalCertificateCancelBatch")
	RetirementPhysicalCertificateCancelBatch retirementPhysicalCertificateCancelBatch;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info(getClass().getName() + " being executed.");
		try {
			affectationCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		try {
			changeOwnershipAvailablesCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		try {
			changeOwnershipBlockedCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		try {
			detachmentCouponsCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		try {
			securitiesAccreditationCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		/*try {
			securitiesDematerialisationCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}*/
		try {
			securitiesLoanableCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		/*try {
			securitiesReteirementCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}*/
		try {
			securitiesTransferAvailablesCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		try {
			securitiesTransferBlockedCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		/*try {
			certificateDepositBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		*/
		try {
			requestReversalsBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		/*try {
			reteirementDepositeBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}*/
		try {
			accreditationCertificateReversalBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. "
					+ ex.getMessage());
		}
		try {
			depositPhysicalCertificateCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. depositPhysicalCertificateCancelBatch"
					+ ex.getMessage());
		}
		try {
			retirementPhysicalCertificateCancelBatch.startJob(processLogger);
		} catch (Exception ex) {
			log.info("Error Inesperado al ejecutar el Batch. retirementPhysicalCertificateCancelBatch"
					+ ex.getMessage());
		}
		
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
