package com.pradera.custody.reversion.batch;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.core.framework.extension.transaction.Transactional;
import com.pradera.custody.retirementoperation.facade.RetirementOperationServiceFacade;
import com.pradera.custody.tranfersecurities.facade.TransferSecuritiesServiceFacade;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.process.ProcessLogger;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary. Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class SecuritiesTransferAvailablesCancelBatch.
 * 
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@BatchProcess(name = "RetirementPhysicalCertificateCancelBatch")
@RequestScoped
public class RetirementPhysicalCertificateCancelBatch implements JobExecution,
		Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The log. */
	@Inject
	PraderaLogger log;

	/** The transfer securities service facade. */
	@EJB
	RetirementOperationServiceFacade retirementOperationServiceFacade;
	
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com
	 * .pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		log.info(getClass().getName() + " being executed.");
		try {
			Integer dayOfRevert = 2;
			Integer PARAMETER_REVERT_RETIREMENT = 2895;
			List<ParameterTable> lstParametersDays = generalPametersFacade.getListParameterTableServiceBean(MasterTableType.DAYS_OF_REVERT_DEP_RET_PHYSICAL.getCode());
			if(lstParametersDays!=null && lstParametersDays.size()>0) {
				for(ParameterTable parameter: lstParametersDays) {
					if(parameter.getParameterTablePk().equals(PARAMETER_REVERT_RETIREMENT)) {
						if(parameter.getShortInteger() != null) {
							dayOfRevert = parameter.getShortInteger();
							break;
						};
					}
				}
			}

			Date revertDate = CommonsUtilities.removeDaysHabilsToDate(CommonsUtilities.currentDate(), dayOfRevert );
			log.info(getClass().getName() + " execute => revertDate:"+revertDate);
			retirementOperationServiceFacade.automaticRetirementCancelations(revertDate);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			log.error(getClass().getName() + " execute => exception:"+e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#
	 * getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#
	 * getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification
	 * ()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
