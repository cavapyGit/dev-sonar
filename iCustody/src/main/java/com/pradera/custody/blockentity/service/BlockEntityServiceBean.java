package com.pradera.custody.blockentity.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.ValidationMode;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.blockentity.to.BlockEntitySearchType;
import com.pradera.custody.blockentity.to.BlockEntityTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.BlockEntityStateType;


// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockEntityServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BlockEntityServiceBean extends CrudDaoServiceBean {
	 
	 ///:::::::::::::::::::::::: REFACTORING CODE :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	 /**
 	 * Find block entity by document.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
 	public List<BlockEntity> findBlockEntityByDocument(BlockEntity blockEntity) throws ServiceException{
		 try{
			 StringBuilder stringBuilderSql = new StringBuilder();	
			 stringBuilderSql.append("SELECT h.idBlockEntityPk, h.documentType, h.documentNumber, h.personType,  ");//0,1,2,3
			 stringBuilderSql.append("h.firstLastName, h.secondLastName, h.name, h.fullName, 	");//4,5,6,7
			 stringBuilderSql.append("ho.idHolderPk, ho.fullName 	");//8,9
			 stringBuilderSql.append("FROM BlockEntity h 	");
			 stringBuilderSql.append("Left Join h.holder ho	");
			 stringBuilderSql.append("WHERE 1=1 "); 
			 if(blockEntity.getDocumentType()!=null){
				 stringBuilderSql.append(" And h.documentType = :documentType");
			 }
			 if(blockEntity.getDocumentNumber()!=null){
				 stringBuilderSql.append(" And h.documentNumber = :documentNumber");
			 }
		     if(blockEntity.getDocumentSource()!=null){
		    	 stringBuilderSql.append(" And h.documentSource = :documentSource");
		     }
		     if(blockEntity.getHolder()!=null && blockEntity.getHolder().getIdHolderPk()!=null){
		    	 stringBuilderSql.append(" And ho.idHolderPk = :idHolderPk");
		     }
			 Query query = em.createQuery(stringBuilderSql.toString());
			 if(blockEntity.getDocumentType()!=null){
				 query.setParameter("documentType", blockEntity.getDocumentType());	
			 }
			 if(blockEntity.getDocumentNumber()!=null){
				 query.setParameter("documentNumber",blockEntity.getDocumentNumber());
			 }
			 if(blockEntity.getDocumentSource()!=null){
				 query.setParameter("documentSource",blockEntity.getDocumentSource());
			 }
			 if(blockEntity.getHolder()!=null && blockEntity.getHolder().getIdHolderPk()!=null){
				 query.setParameter("idHolderPk", blockEntity.getHolder().getIdHolderPk());    
			 }
			 
			 List<BlockEntity> listobJBlockEntity=null;
			 if(query.getResultList()!=null && query.getResultList().size()>0){
				 listobJBlockEntity =  populateBlockEntity((List<Object[]>)query.getResultList());
			 }
			 
			 return listobJBlockEntity;
		
		 }catch(NoResultException e){
			 return null;
		 }
	}
	 
	/**
	 * Populate block entity.
	 *
	 * @param listData the list data
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	public List<BlockEntity> populateBlockEntity(List<Object[]> listData) throws ServiceException{
		List<BlockEntity> listBlockEntity = new ArrayList<BlockEntity>();
		for(Object[] objectData :listData){
				BlockEntity blockEntity = new BlockEntity();
				blockEntity.setIdBlockEntityPk((Long) objectData[0]);
				blockEntity.setDocumentType((Integer) objectData[1]);
				blockEntity.setDocumentNumber((String) objectData[2]);
				blockEntity.setPersonType((Integer) objectData[3]);
				blockEntity.setFirstLastName((String) objectData[4]);
				blockEntity.setSecondLastName((String) objectData[5]);
				blockEntity.setName((String) objectData[6]);
				blockEntity.setFullName((String) objectData[7]);
				
				if(objectData[8]!=null){
					Holder holder = new Holder();
					holder.setIdHolderPk((Long) objectData[8]);
					holder.setFullName((String) objectData[9]);
					blockEntity.setHolder(holder);
				}
				
				listBlockEntity.add(blockEntity);
		
		}
		return listBlockEntity;
	}
	
	 /**
 	 * Find holder from block entity.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the holder
 	 * @throws ServiceException the service exception
 	 */
 	public List<Holder> findHolderServiceBean(BlockEntity blockEntity) throws ServiceException{
		 try{
			 StringBuilder stringBuilderSql = new StringBuilder();
			 	stringBuilderSql.append(" Select h From Holder h 	");
			 	stringBuilderSql.append(" Where 1=1		");
			 
			 if(blockEntity.getHolder().getIdHolderPk()!=null){
				 stringBuilderSql.append(" And h.idHolderPk = :idHolderPk ");
			 }
			 if(blockEntity.getPersonType()!=null){
				 stringBuilderSql.append(" And h.holderType = :personType ");
			 }
			 if(blockEntity.getDocumentType()!=null && blockEntity.getDocumentNumber()!=null){
				 stringBuilderSql.append(" And (	(h.documentType = :documentType and h.documentNumber  = :documentNumber) Or		");
				 stringBuilderSql.append(" 	(h.secondDocumentType = :documentType and h.secondDocumentNumber = :documentNumber))");
			 }			 
			 if(blockEntity.getNationality()!=null){
				 stringBuilderSql.append(" And ( h.nationality = :nationality Or h.secondNationality = :nationality )	");
			 }
			 if(blockEntity.getDocumentSource()!=null){
				 stringBuilderSql.append(" And h.documentSource = :documentSource");
			 }
			 Query query = em.createQuery(stringBuilderSql.toString());
			 
			 if(blockEntity.getHolder().getIdHolderPk()!=null){
				 query.setParameter("idHolderPk",blockEntity.getHolder().getIdHolderPk());
			 }
			 if(blockEntity.getPersonType()!=null){
				 query.setParameter("personType",blockEntity.getPersonType());
			 }
			 if(blockEntity.getNationality()!=null){
				 query.setParameter("nationality",blockEntity.getNationality());
			 }
			 if(blockEntity.getDocumentType()!=null && blockEntity.getDocumentNumber()!=null){
				 query.setParameter("documentType",blockEntity.getDocumentType());
				 query.setParameter("documentNumber",blockEntity.getDocumentNumber());			 
			 }
			 if(blockEntity.getDocumentSource()!=null){
				 query.setParameter("documentSource",blockEntity.getDocumentSource());
			 }		 
			 return (List<Holder>)query.getResultList();
			
		 }catch(NoResultException e){
			 return null;
		 }catch(NonUniqueResultException e){
			 throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		 }
		 	
	 }
	 
	 /**
 	 * Find institution information service.
 	 *
 	 * @param documentType the document type
 	 * @param documentNumber the document number
 	 * @return the institution information
 	 * @throws ServiceException the service exception
 	 */
 	public InstitutionInformation findInstitutionInformationService(Integer documentType, String documentNumber) throws ServiceException {
		try{
			Query query = em.createNamedQuery(InstitutionInformation.INSTITUTION_INFORMATION_BY_ID);
			query.setParameter("docTyp", documentType);
			query.setParameter("docNum", documentNumber);
			return (InstitutionInformation) query.getSingleResult();
		}catch(NoResultException ex){
		   return null;
		}catch(NonUniqueResultException ex){
		   throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
		}
	 }
	 
	 /**
 	 * method to transform data From ArrayObject[] to blockEntity.
 	 *
 	 * @param objectData the object data
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
	 public List<BlockEntity> populateBlockEntityFromArrayObject(List<Object[]> objectData) throws ServiceException{
		 List<BlockEntity> blockEntityList = new ArrayList<BlockEntity>();
		 for(Object[] objectRowData: objectData){
			 BlockEntity blockEntity = new BlockEntity();
			 blockEntity.setIdBlockEntityPk((Long) objectRowData[0]);
			 blockEntity.setDocumentNumber((String) objectRowData[1]);
			 blockEntity.setFullName((String) objectRowData[2]);
			 blockEntity.setRegistryDate((Date) objectRowData[3]);
			 blockEntity.setDocumentType((Integer) objectRowData[4]);
			 blockEntity.setDescriptionTypeDocument((String) objectRowData[5]);
			 blockEntity.setStateBlockEntity((Integer) objectRowData[6]);
			 blockEntity.setStateDescription((String) objectRowData[7]);
			 blockEntity.setRegistryUser((String) objectRowData[8]);
			 blockEntityList.add(blockEntity);
		 }
		 return blockEntityList;
	 }
	 

	 /**
 	 * Find block entity from to.
 	 *
 	 * @param blockEntTO the block ent to
 	 * @return the list
 	 * @throws ServiceException the service exception
 	 */
 	@SuppressWarnings("unchecked")
	 public List<BlockEntity> findBlockEntityFromTO(BlockEntityTO blockEntTO) throws ServiceException{
		 StringBuilder querySql = new StringBuilder();
		 Map<String,Object> parameters = new HashMap<String, Object>();
		 
		 querySql.append("Select b.idBlockEntityPk, b.documentNumber, b.fullName,  b.registryDate,  ");//0,1,2,3
		 querySql.append("		 b.documentType, (Select p.parameterName From ParameterTable p Where p.parameterTablePk = b.documentType), ");//4,5
		 querySql.append("		 b.stateBlockEntity,  (Select p.parameterName From ParameterTable p Where p.parameterTablePk = b.stateBlockEntity), b.registryUser		");//6,7,8
		 querySql.append("From BlockEntity b  ");
		 querySql.append("Where  1 = 1	");

		 
		 //switch request type
		 BlockEntitySearchType blockEntityReqType = Validations.validateIsNotNull(BlockEntitySearchType.get(blockEntTO.getRequestSearchType())) ? BlockEntitySearchType.get(blockEntTO.getRequestSearchType()) : BlockEntitySearchType.BLOCK_ENTITY;
		 switch(blockEntityReqType){
		 	case BLOCK_ENTITY:
		 		if(blockEntTO.getBlockCode()!=null){
		 			querySql.append(" and b.idBlockEntityPk = :idBlockEntityPk");
					parameters.put("idBlockEntityPk", blockEntTO.getBlockCode());
				 };break;
			
		 	case HOLDER_COD:
		 		if(blockEntTO.getHolderCode()!=null){
					 querySql.append(" and b.holder.idHolderPk = :idHolderPk"); 
					 parameters.put("idHolderPk", blockEntTO.getHolderCode());
				 };break;
				 
		 	case DATE:
		 		if(blockEntTO.getInitialDate()!=null && blockEntTO.getEndDate()!=null){
		 			querySql.append(" and b.registryDate between TRUNC(:dateIni) and TRUNC(:dateEnd) ");
		 			parameters.put("dateIni", blockEntTO.getInitialDate());
		 			parameters.put("dateEnd", blockEntTO.getEndDate());
				};break;
				 
		 	case PERSON_TYPE:
		 		if(blockEntTO.getPersonType()!=null && blockEntTO.getPersonType().equals(PersonType.NATURAL.getCode())){			   
					 querySql.append(" and b.personType = :personType");
					 parameters.put("personType", blockEntTO.getPersonType());
					 
					 if(blockEntTO.getNames()!=null && !blockEntTO.getNames().isEmpty()){
						 querySql.append(" and b.name like :name");
						 parameters.put("name", blockEntTO.getNames());
					 }
					 if(blockEntTO.getFirstLastName()!=null && !blockEntTO.getFirstLastName().isEmpty()){
						 querySql.append(" and b.firstLastName like :firstLastName");
						 parameters.put("firstLastName", blockEntTO.getFirstLastName());
					 }
					 if(blockEntTO.getSecondLastName()!=null && !blockEntTO.getSecondLastName().isEmpty()){
						 querySql.append(" and b.secondLastName like :secondLastName");
						 parameters.put("secondLastName", blockEntTO.getSecondLastName());
					 }
					 if(blockEntTO.getDocumentType()!=null){
						 querySql.append(" and b.documentType = :documentType ");
						 parameters.put("documentType", blockEntTO.getDocumentType());
					 }
					 if(blockEntTO.getDocumentNumber()!=null && !blockEntTO.getDocumentNumber().isEmpty()){
						 querySql.append(" and b.documentNumber = :documentNumber");
						 parameters.put("documentNumber", blockEntTO.getDocumentNumber());
					 }
					 if(blockEntTO.getDocumentSource()!=null){
						 querySql.append(" and b.documentSource = :documentSource");
						 parameters.put("documentSource", blockEntTO.getDocumentSource());
					 }
				 }
				 if(blockEntTO.getPersonType()!=null && blockEntTO.getPersonType().equals(PersonType.JURIDIC.getCode())){			   
					 querySql.append(" and b.personType = :personType");
					 parameters.put("personType", PersonType.JURIDIC.getCode());
					 
					 if(blockEntTO.getBusinessName()!=null && !blockEntTO.getBusinessName().isEmpty()){
						 querySql.append(" and b.fullName like :fullName");
						 parameters.put("fullName", blockEntTO.getBusinessName());
					 }
					 if(blockEntTO.getDocumentType()!=null){
						 querySql.append(" and b.documentType = :documentType ");
						 parameters.put("documentType", blockEntTO.getDocumentType());
					 }
					 if(blockEntTO.getDocumentNumber()!=null && !blockEntTO.getDocumentNumber().isEmpty()){
						 querySql.append(" and b.documentNumber = :documentNumber");
					     parameters.put("documentNumber", blockEntTO.getDocumentNumber());
					 }
					  
				 };break;
		}		
		querySql.append(" ORDER BY b.idBlockEntityPk DESC");
		 
		try{
			List<Object[]> objectData = findListByQueryString(querySql.toString(), parameters);
			return populateBlockEntityFromArrayObject(objectData);
		}catch(NoResultException ex){
			return new ArrayList<BlockEntity>();
		}
	 }
	 
	 /**
 	 * method to register object on database.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public BlockEntity registerBlockEntityService(BlockEntity blockEntity) throws ServiceException{
		 if(blockEntity.getHolder()!=null && blockEntity.getHolder().getIdHolderPk()==null){
			 blockEntity.setHolder(null);
		 }
		 if(blockEntity.getPersonType()!=null && blockEntity.getPersonType().equals(PersonType.NATURAL.getCode())){
			 String firstName = blockEntity.getFirstLastName();
			 String secondName = blockEntity.getSecondLastName();
			 String name = blockEntity.getName();
			 
			 StringBuilder fullName = new StringBuilder();
			 if(firstName!=null && !firstName.isEmpty()){
				 fullName.append(firstName).append(" ");
			 }
			 if(secondName!=null && !secondName.isEmpty()){
				 fullName.append(secondName).append(" ");;
			 }
			 if(name!=null && !name.isEmpty()){
				 fullName.append(name);
			 }
			 
			 blockEntity.setFullName(fullName.toString().trim());
		 }
		 create(blockEntity);
		 return blockEntity;
	 }
	 
	 /**
 	 * method to desactive blockEntity.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public BlockEntity desactiveBlockEntity(BlockEntity blockEntity) throws ServiceException{
		 BlockEntity blockEntityToUpd = em.find(BlockEntity.class, blockEntity.getIdBlockEntityPk());
		 
		 if(blockEntityToUpd.getStateBlockEntity().equals(BlockEntityStateType.ACTIVATED.getCode())){
			 blockEntityToUpd.setStateBlockEntity(BlockEntityStateType.OFF.getCode());
			 
			 //updating object on database
			 update(blockEntityToUpd);
			 
			 //return object saved
			 return blockEntityToUpd;
		 }else{
			 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		 }
	 }
	 
	 /**
 	 * Active block entity.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
 	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public BlockEntity activeBlockEntity(BlockEntity blockEntity) throws ServiceException{
		 BlockEntity blockEntityToUpd = em.find(BlockEntity.class, blockEntity.getIdBlockEntityPk());
		 
		 if(blockEntityToUpd.getStateBlockEntity().equals(BlockEntityStateType.OFF.getCode())){
			 blockEntityToUpd.setStateBlockEntity(BlockEntityStateType.ACTIVATED.getCode());
			 
			 //updating object on database
			 update(blockEntityToUpd);
			 
			 //return object saved
			 return blockEntityToUpd;
		 }else{
			 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		 }
	 }
	 
	 /**
 	 * method to modify object on database.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
	 @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	 public BlockEntity modifyBlockEntityService(BlockEntity blockEntity) throws ServiceException{
		 BlockEntity blocEntityToUpd = em.find(BlockEntity.class, blockEntity.getIdBlockEntityPk());
		 
		 if(blocEntityToUpd.getLastModifyDate().equals(blockEntity.getLastModifyDate())){
			 blocEntityToUpd.setHomeAddress(blockEntity.getHomeAddress());
			 blocEntityToUpd.setIndNotification(blockEntity.getIndNotification());
			 
			 if(Validations.validateIsNotNull(blockEntity.getIndNotification()) && blockEntity.getIndNotification().equals(BooleanType.YES.getCode())){
				 if(blockEntity.getEmail()!=null){
					 blocEntityToUpd.setEmail(blockEntity.getEmail());
				 }else{//if indicator is active & not exist email, it's inconsistente of data
					 throw new ServiceException(ErrorServiceType.INCONSISTENCY_DATA);
				 }
			 }else{
				 blocEntityToUpd.setEmail(null);
			 }
			 blocEntityToUpd.setComments(blockEntity.getComments());
			 
			 //updating object to database
			 update(blocEntityToUpd);
			 
			 //return the object
			 return blocEntityToUpd;
			 
		 
		 }else{
			 throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS);
		 }
	 
	 }
	 
	 /**
 	 * method to get BlockEntity from ID.
 	 *
 	 * @param blockEntityID the block entity id
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
	 public BlockEntity findBlockEntityByIdService(Long blockEntityID) throws ServiceException{
		 String querySql= "Select be From BlockEntity be Left Outer Join Fetch be.holder Where be.idBlockEntityPk = :blockEntityID";
		 Query query = em.createQuery(querySql);
		 query.setParameter("blockEntityID", blockEntityID);
		 return (BlockEntity) query.getSingleResult();
	 }
}