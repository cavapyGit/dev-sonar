package com.pradera.custody.blockentity.to;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.pradera.commons.utils.PropertiesUtilities;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockEntityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/01/2014
 */
public enum BlockEntitySearchType {
	
	/** The block entity. */
	BLOCK_ENTITY(1,""),
	
	/** The holder cod. */
	HOLDER_COD(2,""),
	
	/** The person type. */
	PERSON_TYPE(3,""),
	
	/** The date. */
	DATE(4,"");		
	
	/** The code. */
	private Integer code;
	
	/** The value. */
	private String value;
	

	/** The Constant list. */
	public static final List<BlockEntitySearchType> list = new ArrayList<BlockEntitySearchType>();
	
	/** The Constant lookup. */
	public static final Map<Integer, BlockEntitySearchType> lookup = new HashMap<Integer, BlockEntitySearchType>();

	static {
		for (BlockEntitySearchType s : EnumSet.allOf(BlockEntitySearchType.class)) {
			list.add(s);
			lookup.put(s.getCode(), s);
		}
	}

	/**
	 * Instantiates a new block entity search type.
	 *
	 * @param code the code
	 * @param value the value
	 */
	private BlockEntitySearchType(Integer code, String value) {
		this.code = code;
		this.value = value;			
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets the description.
	 *
	 * @param locale the locale
	 * @return the description
	 */
	public String getDescription(Locale locale) {
		return PropertiesUtilities.getMessage(locale, this.getValue());
	}
	
	/**
	 * Gets the.
	 *
	 * @param code the code
	 * @return the block entity search type
	 */
	public static BlockEntitySearchType get(Integer code) {
		return lookup.get(code);
	}
}