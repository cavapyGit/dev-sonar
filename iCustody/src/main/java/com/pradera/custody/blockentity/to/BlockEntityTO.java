package com.pradera.custody.blockentity.to;

import java.io.Serializable;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockEntityTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/01/2014
 */
public class BlockEntityTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The rnt code. */
	private Long holderCode;
	
	/** The request search type. */
	private Integer requestSearchType;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The end date. */
	private Date endDate;
	
	/** The block code. */
	private Long blockCode;
	
	/** The person type. */
	private Integer personType;
	
	/** The document number. */
	private String documentNumber;
	
	/** The document type. */
	private Integer documentType;
	
	/** The names. */
	private String names;
	
	/** The first last name. */
	private String firstLastName;
	
	/** The second last name. */
	private String secondLastName;
	
	/** The business name. */
	private String businessName;
	
	/** The ind notification. */
	private Integer indNotification;
	
	/** The document source. */
	private Integer documentSource;
	
	/**
	 * Instantiates a new block entity to.
	 */
	public BlockEntityTO() {
		super();
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initial date
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the new initial date
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Sets the end date.
	 *
	 * @param endDate the new end date
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the block code.
	 *
	 * @return the block code
	 */
	public Long getBlockCode() {
		return blockCode;
	}

	/**
	 * Sets the block code.
	 *
	 * @param blockCode the new block code
	 */
	public void setBlockCode(Long blockCode) {
		this.blockCode = blockCode;
	}

	/**
	 * Gets the person type.
	 *
	 * @return the person type
	 */
	public Integer getPersonType() {
		return personType;
	}

	/**
	 * Sets the person type.
	 *
	 * @param personType the new person type
	 */
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}

	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}

	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	/**
	 * Gets the names.
	 *
	 * @return the names
	 */
	public String getNames() {
		return names;
	}

	/**
	 * Sets the names.
	 *
	 * @param names the new names
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
	 * Gets the business name.
	 *
	 * @return the business name
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Sets the business name.
	 *
	 * @param businessName the new business name
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * Gets the ind notification.
	 *
	 * @return the ind notification
	 */
	public Integer getIndNotification() {
		return indNotification;
	}

	/**
	 * Sets the ind notification.
	 *
	 * @param indNotification the new ind notification
	 */
	public void setIndNotification(Integer indNotification) {
		this.indNotification = indNotification;
	}

	/**
	 * Gets the holder code.
	 *
	 * @return the holder code
	 */
	public Long getHolderCode() {
		return holderCode;
	}

	/**
	 * Sets the holder code.
	 *
	 * @param holderCode the new holder code
	 */
	public void setHolderCode(Long holderCode) {
		this.holderCode = holderCode;
	}

	/**
	 * Gets the request search type.
	 *
	 * @return the request search type
	 */
	public Integer getRequestSearchType() {
		return requestSearchType;
	}

	/**
	 * Sets the request search type.
	 *
	 * @param requestSearchType the new request search type
	 */
	public void setRequestSearchType(Integer requestSearchType) {
		this.requestSearchType = requestSearchType;
	}

	/**
	 * Gets the document source.
	 *
	 * @return the document source
	 */
	public Integer getDocumentSource() {
		return documentSource;
	}

	/**
	 * Sets the document source.
	 *
	 * @param documentSource the new document source
	 */
	public void setDocumentSource(Integer documentSource) {
		this.documentSource = documentSource;
	}
	
}
