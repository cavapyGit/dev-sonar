package com.pradera.custody.blockentity.facade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.blockentity.service.BlockEntityServiceBean;
import com.pradera.custody.blockentity.to.BlockEntityTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.InstitutionInformation;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.BlockRequest;
import com.pradera.model.custody.affectation.type.AffectationStateType;
import com.pradera.model.custody.affectation.type.LevelAffectationType;
import com.pradera.model.custody.blockentity.BlockEntity;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockEntityServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04-abr-2014
 */
@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BlockEntityServiceFacade {
	
	 /** The block entity service bean. */
 	@EJB
	 BlockEntityServiceBean blockEntityServiceBean;
	 
	 /** The crud dao service bean. */
 	@EJB
	 CrudDaoServiceBean crudDaoServiceBean;
	 
	 /** The transaction registry. */
 	@Resource TransactionSynchronizationRegistry transactionRegistry;
	 
	 /**
 	 * Validate affectation on block entity.
 	 *
 	 * @param blockEntity the block entity
 	 * @return true, if successful
 	 * @throws ServiceException the service exception
 	 */
 	public boolean validateAffectationOnBlockEntity(BlockEntity blockEntity)throws ServiceException{
		 BlockEntity obBlockEntity = blockEntityServiceBean.findBlockEntityByIdService(blockEntity.getIdBlockEntityPk());
		 
		 if(obBlockEntity!=null){
			 for(BlockRequest blockRequest: obBlockEntity.getBlockRequest()){
				 if(blockRequest.getBlockEntity()!=null){
				   for(BlockOperation blockOperation: blockRequest.getBlockOperation()){
					   if(blockOperation.getBlockLevel()!=null && blockOperation.getBlockLevel().equals(LevelAffectationType.BLOCK.getCode())){
						   if(blockOperation.getBlockState().equals(AffectationStateType.BLOCKED.getCode())){
							   throw new ServiceException(ErrorServiceType.BLOCKENTITY_DEACTIVE_AFFEC);
						   }
					   }
					   else if(blockOperation.getBlockLevel()!=null && blockOperation.getBlockLevel().equals(LevelAffectationType.REBLOCK.getCode())){
						   if(blockOperation.getBlockState().equals(AffectationStateType.REGISTRED.getCode())){
							   throw new ServiceException(ErrorServiceType.BLOCKENTITY_DEACTIVE_AFFEC);
						   }
					   }
				   }
			   }
			 }
		 }
		 return true;
	 }

	
	//::::::::::::::::::::::::::::::: REFACTORING CODE ::::::::::::::::::::::::::::::::::::::
	/** The parameter service bean. */
	@EJB ParameterServiceBean parameterServiceBean;
	
	
	/**
	 * Populate from bock entity.
	 *
	 * @param blockEntity the block entity
	 * @param depositaryCountry the depositary country
	 * @return the person to
	 * @throws ServiceException the service exception
	 */
	public PersonTO populateFromBockEntity(BlockEntity blockEntity, Integer depositaryCountry) throws ServiceException{
		PersonTO personTO = new PersonTO();
		personTO.setPersonType(blockEntity.getPersonType());
		personTO.setFlagHolderIndicator(Boolean.TRUE);
		
		if(blockEntity.getIndMinor()!=null && blockEntity.getIndMinor().equals(BooleanType.YES.getCode())){
			personTO.setFlagMinorIndicator(Boolean.TRUE);
		}

		if(blockEntity.getIndDisabled()!=null && blockEntity.getIndDisabled().equals(BooleanType.YES.getCode())){
			personTO.setFlagInabilityIndicator(Boolean.TRUE);
		}
		
		if(blockEntity.getNationality()!=null && blockEntity.getNationality().equals(depositaryCountry)){	
			if(blockEntity.getIndResidence()!=null && blockEntity.getIndResidence().equals(BooleanType.YES.getCode())){
				personTO.setFlagNationalResident(Boolean.TRUE);				
			}
			else{
				personTO.setFlagNationalNotResident(Boolean.TRUE);
			}
			
		}else if(blockEntity.getNationality()!=null && !blockEntity.getNationality().equals(depositaryCountry)){
			if(blockEntity.getIndResidence()!=null ){
				if(blockEntity.getIndResidence().equals(BooleanType.YES.getCode())){
					personTO.setFlagForeignResident(Boolean.TRUE);
				}else{
					personTO.setFlagForeignNotResident(Boolean.TRUE);
				}
			}
		}
		return personTO;
	}
	
	/**
	 * Populate from block entity to.
	 *
	 * @param blockEntityTO the block entity to
	 * @return the person to
	 * @throws ServiceException the service exception
	 */
	public PersonTO populateFromBlockEntityTO(BlockEntityTO blockEntityTO )throws ServiceException{
		PersonTO personTO = new PersonTO();
		personTO.setPersonType(blockEntityTO.getPersonType());
		personTO.setFlagHolderIndicator(Boolean.TRUE);
		personTO.setFlagInabilityIndicator(Boolean.TRUE);
		if(personTO.getPersonType().equals(PersonType.NATURAL.getCode())){
			personTO.setFlagNaturalDocuments(Boolean.TRUE);
		}else if(personTO.getPersonType().equals(PersonType.JURIDIC.getCode())){
			personTO.setFlagJuridicDocuments(Boolean.TRUE);
		}
		
		return personTO;
	}
	
	 
	 /**
 	 * Find block entity by document.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the block entity
 	 * @throws ServiceException the service exception
 	 */
 	public List<BlockEntity> findBlockEntityByDocument(BlockEntity blockEntity)throws ServiceException{
		return blockEntityServiceBean.findBlockEntityByDocument(blockEntity);
	 }
	 
	 /**
 	 * Validate exist block entity.
 	 *
 	 * @param blockEntity the block entity
 	 * @throws ServiceException the service exception
 	 */
 	public void validateExistBlockEntity(BlockEntity blockEntity) throws ServiceException{
		 List<BlockEntity> listBlockEntity = blockEntityServiceBean.findBlockEntityByDocument(blockEntity);
		 String documentName=null;
		 String documentNumb=null;
		 
		 if(listBlockEntity==null){
			 BlockEntity auxBlockEntity = new BlockEntity();
			 auxBlockEntity.setDocumentType(blockEntity.getDocumentType());
			 auxBlockEntity.setDocumentNumber(blockEntity.getDocumentNumber());
			 auxBlockEntity.setDocumentSource(null);
			 if(blockEntity.getHolder()!=null){
				 auxBlockEntity.setHolder(blockEntity.getHolder());
			 }
			 
			 listBlockEntity = blockEntityServiceBean.findBlockEntityByDocument(auxBlockEntity);
		 }
		 //validate bockEntity
		 if(listBlockEntity!=null){
			 String message=GeneralConstants.EMPTY_STRING;
			 String slave = GeneralConstants.BLANK_SPACE;
			 for(BlockEntity ObjBlockEntity : listBlockEntity){
					//Add parameter to handle exception
					documentName = parameterServiceBean.findParameterTableDescription(ObjBlockEntity.getDocumentType());
					documentNumb = ObjBlockEntity.getDocumentNumber();
					
					String below = ObjBlockEntity.getDescriptionPerson();
					Long blockEntityId = ObjBlockEntity.getIdBlockEntityPk();
					 
					 if(ObjBlockEntity.getHolder()!=null){
						 String cuiCode = ObjBlockEntity.getHolder().getIdHolderPk().toString();
					
						slave = slave + PropertiesUtilities.getExceptionMessage(
								ErrorServiceType.BLOCKENTITY_EXIST_HOLDER.getMessage(),
									new Object[]{below,cuiCode,blockEntityId});
					 }else{
						 
						 slave = slave + PropertiesUtilities.getExceptionMessage(
									ErrorServiceType.BLOCKENTITY_EXIST.getMessage(),
										new Object[]{below,blockEntityId});
					}					 
					 	
			 }
			  Object[] bodyData = new Object[3];
			  
			  bodyData[0]= documentName;
			  bodyData[1]= documentNumb;
			  message = message.concat(GeneralConstants.BLANK_SPACE).concat(slave).trim();
			  bodyData[2]= message;
			  
			  message = PropertiesUtilities.getExceptionMessage(
						ErrorServiceType.BLOCKENTITY_EXIST_HEADLER.getMessage(),
						bodyData);
			  
			  throw new ServiceException(ErrorServiceType.BLOCKENTITY_EXIST,message);
				
		 }
	 }
	 
	 /**
 	 * Find holder from block entity.
 	 *
 	 * @param blockEntity the block entity
 	 * @return the holder
 	 * @throws ServiceException the service exception
 	 */
 	public List<Holder> findHolderServiceFacade(BlockEntity blockEntity)throws ServiceException{
		 return blockEntityServiceBean.findHolderServiceBean(blockEntity);
	 }
	
	/**
	 * Update status block entity service facade.
	 *
	 * @param blockEntity the block entity
	 * @return the boolean
	 * @throws ServiceException the service exception
	 */
	public Boolean updateStatusBlockEntityServiceFacade(BlockEntity blockEntity) throws ServiceException{
		
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	    loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify()); 
		Boolean flag = null;
		String strQuery = " UPDATE BlockEntity " +
						  " SET lastModifyApp= :modifyApp, " +
						  " lastModifyDate= :modifyDate, " +
						  " lastModifyIp= :modifyIp, " +
						  " lastModifyUser= :modifyUser, " +
						  " stateBlockEntity = :state " +
						  " WHERE idBlockEntityPk = :idBlockEntityPk";
		Map<String,Object> mapParams = new HashMap<String, Object>();
		mapParams.put("modifyApp", loggerUser.getIdPrivilegeOfSystem());
		mapParams.put("modifyDate", loggerUser.getAuditTime());
		mapParams.put("modifyIp", loggerUser.getIpAddress());
		mapParams.put("modifyUser", loggerUser.getUserName());
		mapParams.put("state", blockEntity.getStateBlockEntity());
		mapParams.put("idBlockEntityPk", blockEntity.getIdBlockEntityPk());
		crudDaoServiceBean.updateByQuery(strQuery,mapParams);
		flag = true;		 
		return flag;
	}
	
	/**
	 * *
	 * method to validate InstitutionInformation.
	 *
	 * @param documentType the document type
	 * @param documentNumber the document number
	 * @return the institution information
	 * @throws ServiceException the service exception
	 */
	public InstitutionInformation validateInstitutionInformation(Integer documentType, String documentNumber) throws ServiceException {
		InstitutionInformation instInfo = blockEntityServiceBean.findInstitutionInformationService(documentType, documentNumber);
		if(instInfo == null){
			throw new ServiceException(ErrorServiceType.BLOCKENTITY_INSTINFO_NOTEXIST,new Object[]{documentNumber.toString()});
		}
 		return instInfo; 
 	}
	
	/**
	 * Register block entity object.
	 *
	 * @param blockEntity the block entity
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process = BusinessProcessType.BLOCK_ENTITY_REGISTER)
	public BlockEntity registerBlockEntityObject(BlockEntity blockEntity)throws ServiceException{
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		 blockEntityServiceBean.registerBlockEntityService(blockEntity);
		 return blockEntity;
	}
	
	/**
	 * Modify block entity object.
	 *
	 * @param blockEntity the block entity
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process = BusinessProcessType.BLOCK_ENTITY_DATA_MODIFICATION)
	public BlockEntity modifyBlockEntityObject(BlockEntity blockEntity)throws ServiceException{
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeModify()); 
		 blockEntityServiceBean.modifyBlockEntityService(blockEntity);
		 return blockEntity;
	 }
	
	/**
	 * Desactive block entity object.
	 *
	 * @param blockEntity the block entity
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process = BusinessProcessType.BLOCK_ENTITY_DISABLE)
	public BlockEntity desactiveBlockEntityObject(BlockEntity blockEntity) throws ServiceException{
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeDeactivate()); 
	     //validate if exist affectation on block Entity
	     validateAffectationOnBlockEntity(blockEntity);
	     
		 blockEntityServiceBean.desactiveBlockEntity(blockEntity);
		 return blockEntity;
	}
	
	/**
	 * Active block entity object.
	 *
	 * @param blockEntity the block entity
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process = BusinessProcessType.BLOCK_ENTITY_ACTIVE)
	public BlockEntity activeBlockEntityObject(BlockEntity blockEntity) throws ServiceException{
		 LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
	     loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeActivate()); 
		 blockEntityServiceBean.activeBlockEntity(blockEntity);
		 return blockEntity;
	}
	
	/**
	 * Gets the list block entity service facade.
	 *
	 * @param blockEntityFilter the block entity filter
	 * @return the list block entity service facade
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.BLOCK_ENTITY_QUERY)
	public List<BlockEntity> getListBlockEntityServiceFacade(BlockEntityTO blockEntityFilter) throws ServiceException{
		 List<BlockEntity> list = blockEntityServiceBean.findBlockEntityFromTO(blockEntityFilter);
		 return list;
	 }
	

	/**
	 * method to get BlockEntity by ID.
	 *
	 * @param idBlockEntityPk the id block entity pk
	 * @return the block entity
	 * @throws ServiceException the service exception
	 */
	public BlockEntity findBlockEntity(Long idBlockEntityPk) throws ServiceException{
		return blockEntityServiceBean.findBlockEntityByIdService(idBlockEntityPk);
	}	 
}