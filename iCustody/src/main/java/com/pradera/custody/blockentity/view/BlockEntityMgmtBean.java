package com.pradera.custody.blockentity.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.OperationUserTO;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.GeographicLocationTO;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.utils.DocumentTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.blockentity.facade.BlockEntityServiceFacade;
import com.pradera.custody.blockentity.to.BlockEntitySearchType;
import com.pradera.custody.blockentity.to.BlockEntityTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.DocumentType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.BlockEntityStateType;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.GeographicLocationStateType;
import com.pradera.model.generalparameter.type.GeographicLocationType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.process.BusinessProcess;


// TODO: Auto-generated Javadoc

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * 
 * The Class BlockEntityMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/01/2014
 */
@DepositaryWebBean
@LoggerCreateBean
public class BlockEntityMgmtBean extends GenericBaseBean implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	///:::::::::::::::::::::: REFACTORING CODE ::::::::::::::::::::::::::::::::::::::::
	/** The logger. */
	@Inject
	private transient PraderaLogger logger;
	
	/** The list person type. */
	private List<ParameterTable> listPersonType;

	/** The list document type. */
	private List<ParameterTable> listDocumentType;
	
	/** The list geographic location. */
	private List<GeographicLocation> listGeographicLocation;
	
	/** The list emission source. */
    private List<ParameterTable> listDocumentSource;
	
	/** The document validator. */
	@Inject
	private DocumentValidator documentValidator;
	
	//Objects to show in the view
	/** The block entity. */
	private BlockEntity blockEntity;
	
	/** The block entity filter. */
	private BlockEntityTO blockEntityTO;

	/** The block entity result, to show on DataTable. */
	private GenericDataModel<BlockEntity> blockEntityResult;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The user privilege. */
	@Inject private UserPrivilege userPrivilege;
	
	/** The general pameters facade. */
	@EJB
	private GeneralParametersFacade generalPametersFacade;
	
	/** The accounts facade. */
	@EJB
	private AccountsFacade accountsFacade;
	
	/** The block entity service facade. */
	@EJB
	private BlockEntityServiceFacade blockEntityServiceFacade; 
	
	/** The parameter service bean. */
	@EJB ParameterServiceBean parameterServiceBean;
	
	/** The country residence. */
	@Inject @Configurable private Integer countryResidence;
	
	/** The department issuance. */
	@Inject @Configurable Integer departmentIssuance;
	
	/** The block entity selected. */
	private BlockEntity blockEntitySelected;
	
	/** The document to. */
	private DocumentTO documentTO;
	
	/** The option selected one radio. */
	private Integer optionSelectedOneRadio;
	
	/** The disabled cui code. */
	private boolean disabledCuiCode;
	
	/** The flag participant. */
	private boolean flagParticipant;
	
	/** The id participant. */
	private Long idParticipant;
	
	/** The holder. */
	private Holder holder;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try{
			findPersonTypeList();
			findGeographicLocations();
			loadBlockEntityTO();
			loadBlockEntityPrivileges();
			loadDocumentEmissionSource();
			documentTO = new DocumentTO();
			holder = new Holder();
			flagParticipant = false;
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || 
					userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				flagParticipant = true;
				idParticipant = null;
				if(userInfo.getUserAccountSession().isParticipantInstitucion()){
					idParticipant = userInfo.getUserAccountSession().getParticipantCode();
				}else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					Long participantDpf = getIssuerDpfInstitution(userInfo);										
					if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
						idParticipant = participantDpf;
					}
				}
			}
		}catch(ServiceException ex){
			ex.printStackTrace();
		}
	}
	
	/**
	 * *
	 * method to load privileges on page.
	 */
	private void loadBlockEntityPrivileges() {
		// TODO Auto-generated method stub
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnModifyView(Boolean.TRUE);
		privilegeComponent.setBtnDeactivate(Boolean.TRUE);
		privilegeComponent.setBtnActivate(Boolean.TRUE);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * Load document emission source.
	 */
	public void loadDocumentEmissionSource() {

		try {
			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(MasterTableType.DOCUMENT_SOURCE.getCode());			
			listDocumentSource = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);

		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}

	}
	
	/**
	 * Event document type.
	 */
	public void eventDocumentType(){	
		if(getViewOperationType().equals(ViewOperationsType.REGISTER.getCode())){
			blockEntity.setDocumentNumber(null);
			blockEntity.setDocumentSource(null);			
		}
		documentTO = documentValidator.loadMaxLenghtType(blockEntity.getDocumentType());			
		boolean isNumeric = documentTO.isNumericTypeDocument();
		Integer maxlenght = documentTO.getMaxLenghtDocumentNumber();
		documentTO = documentValidator.validateEmissionRules(blockEntity.getDocumentType());
		documentTO.setNumericTypeDocument(isNumeric);
		documentTO.setMaxLenghtDocumentNumber(maxlenght);		
		blockEntity.setDocumentSource(null);
	}
	
	
	/**
	 * Listener document type.
	 */
	public void listenerDocumentType(){
		blockEntityTO.setDocumentNumber(null);
		blockEntityTO.setDocumentSource(null);
	}
	

	/**
	 * Gets the block entity search type list.
	 *
	 * @return the block entity search type list
	 */
	public List<BlockEntitySearchType> getBlockEntitySearchTypeList(){
		return BlockEntitySearchType.list;
	}
	
	/**
	 * Load block entity to.
	 */
	private void loadBlockEntityTO() {
		// TODO Auto-generated method stub
		blockEntityTO = new BlockEntityTO();
		blockEntityTO.setRequestSearchType(BlockEntitySearchType.DATE.getCode());
		blockEntityTO.setInitialDate(CommonsUtilities.currentDate());
		blockEntityTO.setEndDate(CommonsUtilities.currentDate());
	}

	/**
	 * Gets the lst person type.
	 *
	 * @return the lst person type
	 * @throws ServiceException the service exception
	 */
	public void findPersonTypeList()  throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
		parameterTableTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		listPersonType = generalPametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}
	
	/**
	 * Gets the lst document type.
	 *
	 * @return the lst document type
	 * @throws ServiceException the service exception
	 */
	public void findDocumentTypeList()throws ServiceException{
		listDocumentType = documentValidator.getDocumentTypeResult(blockEntityServiceFacade.populateFromBockEntity(blockEntity, countryResidence));
		
	}
	
	/**
	 * Gets the lst geographic location.
	 *
	 * @return the lst geographic location
	 * @throws ServiceException the service exception
	 */
	public void findGeographicLocations() throws ServiceException{
		GeographicLocationTO geographicLocationTO = new GeographicLocationTO();
		geographicLocationTO.setGeographicLocationType(GeographicLocationType.COUNTRY.getCode());
		geographicLocationTO.setState(GeographicLocationStateType.REGISTERED.getCode());
		listGeographicLocation = generalPametersFacade.getListGeographicLocationServiceFacade(geographicLocationTO);
	}

	/**
	 * Load registration page.
	 *
	 * @return the string
	 */
	public String loadRegistrationPage(){
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
				&& Validations.validateIsNullOrNotPositive(idParticipant)){
			
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			return GeneralConstants.EMPTY_STRING;
		}
		
		JSFUtilities.resetViewRoot();
		setViewOperationType(ViewOperationsType.REGISTER.getCode());
		blockEntity = new BlockEntity();
		blockEntity.setHolder(new Holder());
		blockEntity.setIndNotification(BooleanType.NO.getCode());
		blockEntity.setIndResidence(BooleanType.NO.getCode());
		blockEntity.setIndMinor(BooleanType.NO.getCode());
		optionSelectedOneRadio = Integer.valueOf(1);
		disabledCuiCode = false;
		holder = new Holder();
		return "goToRegisterBlockEntity";
	}

	/**
	 * Validate nationality resident.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateNationalityResident() throws ServiceException{
		if(blockEntity.getNationality()!=null){
			
			if(blockEntity.getPersonType()!=null && blockEntity.getPersonType().equals(PersonType.JURIDIC.getCode())){
					blockEntity.setIndResidence(BooleanType.NO.getCode());
			}
			
			findDocumentTypeList();
		}
	}
	
	/**
	 * *
	 * method to validate personType on search page.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validatePersonType() throws ServiceException{
		if(blockEntityTO.getPersonType()!=null){
			blockEntityTO.setNames(null);
			blockEntityTO.setFirstLastName(null);
			blockEntityTO.setSecondLastName(null);
			blockEntityTO.setBusinessName(null);
			listDocumentType = documentValidator.getDocumentTypeResult(blockEntityServiceFacade.populateFromBlockEntityTO(blockEntityTO));
		}
	}
	
	/**
	 * Search entity block.
	 */
	@LoggerAuditWeb
	public void searchEntityBlock() {
		
		if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() 
				&& Validations.validateIsNullOrNotPositive(idParticipant)){
			
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERRROR_MESSAGE_ISSUER_DPF_DO_NOT_HAVE_PARTICIPANT);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		setViewOperationType(ViewOperationsType.CONSULT.getCode());		
		try {
			List<BlockEntity> blockEntityResultList = null;
			BlockEntityTO parameters = blockEntityTO;
			blockEntityResultList = blockEntityServiceFacade.getListBlockEntityServiceFacade(parameters);
			
			if(blockEntityResultList!=null && !blockEntityResultList.isEmpty()){
				blockEntityResult = new GenericDataModel<BlockEntity>(blockEntityResultList);
			}else{
				this.blockEntityResult = new GenericDataModel<BlockEntity>();
			}
		} catch (ServiceException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Listener format document number.
	 *
	 * @param idComponentDocumentNumber the id component document number
	 */
	public void listenerFormatDocumentNumber(String idComponentDocumentNumber){
		try{
			Boolean formatDocIsCorrect = documentValidator.validateFormatDocumentNumber(blockEntityTO.getDocumentType(), blockEntityTO.getDocumentNumber(), idComponentDocumentNumber);
			if(formatDocIsCorrect!=null){
				if(formatDocIsCorrect.equals(Boolean.FALSE))
					blockEntityTO.setDocumentNumber(null);				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	/**
	 * Validate format document number.
	 */
	public void validateFormatDocumentNumber() {
		//getting info about document
		
		try{
			if(blockEntity.getDocumentNumber()!=null){
			Boolean formatDocIsCorrect = documentValidator.validateFormatDocumentNumber(blockEntity.getDocumentType(), blockEntity.getDocumentNumber(), null);
			if(formatDocIsCorrect!=null){
				if(formatDocIsCorrect.equals(Boolean.FALSE)){
					blockEntity.setDocumentNumber(null);
					blockEntity.setFullName(null);
				}else if(formatDocIsCorrect.equals(Boolean.TRUE)){
					//validate if exist block entity 
					blockEntityServiceFacade.validateExistBlockEntity(blockEntity);
					
					//getting holder from blockEntity about document type & document Number
					List<Holder> listHolder = blockEntityServiceFacade.findHolderServiceFacade(blockEntity);
					if(listHolder!=null && listHolder.size()>0){
						String documentName = null;
						String documentNumber = null;
						String message=GeneralConstants.EMPTY_STRING;
						String slave = GeneralConstants.BLANK_SPACE; 
						
						for(Holder holder : listHolder){
							documentName = parameterServiceBean.findParameterTableDescription(holder.getDocumentType());
							documentNumber = holder.getDocumentNumber();
							
							slave = slave + PropertiesUtilities.getExceptionMessage(
										ErrorServiceType.EXIST_HOLDER.getMessage(),
											new Object[]{holder.getFullName().toUpperCase(),holder.getIdHolderPk().toString()});
						}
						
						  Object[] bodyData = new Object[3];
						  
						  bodyData[0]= documentName;
						  bodyData[1]= documentNumber;
						  message = message.concat(GeneralConstants.BLANK_SPACE).concat(slave).trim();
						  bodyData[2]= message;
						  
						  message = PropertiesUtilities.getExceptionMessage(
									ErrorServiceType.EXIST_HOLDER_HEADLER.getMessage(),
									bodyData);
						  
					    String header = PropertiesUtilities.getMessage("header.message.alert");
						JSFUtilities.showMessageOnDialog(header,message);
					    JSFUtilities.executeJavascriptFunction("PF('dlgBlockEntityExistW').show()");
						
						blockEntity.setHolder(new Holder());
						blockEntity.setDocumentNumber(null);
						blockEntity.setFullName(null);
					    return;
					}
				}
			}
			}
		}catch(ServiceException ex){
			//getting data for dialog message validation
			if(ex.getErrorService().equals(ErrorServiceType.BLOCKENTITY_EXIST)){
			String header = PropertiesUtilities.getMessage("header.message.alert");
			JSFUtilities.showMessageOnDialog(header,ex.getMessage());
		    JSFUtilities.executeJavascriptFunction("PF('dlgBlockEntityExistW').show()");
			
			blockEntity.setHolder(new Holder());
			blockEntity.setDocumentNumber(null);
			blockEntity.setFullName(null);
			}
		}
	}
	
	/**
	 * Show holder information.
	 */
	public void showHolderInformation(){
		try{
		BlockEntity blockEntity = new BlockEntity();
		blockEntity.setPersonType(holder.getHolderType());
		blockEntity.setNationality(holder.getNationality());
		blockEntity.setIndResidence(holder.getIndResidence());
		blockEntity.setIndMinor(holder.getIndMinor());
		blockEntity.setIndDisabled(holder.getIndDisabled());
		
		listDocumentType = documentValidator.getDocumentTypeResult(blockEntityServiceFacade.populateFromBockEntity(blockEntity,countryResidence));
		
		String documentType = getDocumentTypeDescription(holder.getDocumentType());
		String documentNumber = holder.getDocumentNumber();		
		String CUI = holder.getIdHolderPk().toString();		
		
		//variables for message dialog
		String messageDlg = PropertiesUtilities.getMessage("blockentity.validation.holder",new Object[]{CUI,documentType,documentNumber});
		String headerDlg = PropertiesUtilities.getMessage("header.message.alert");
		JSFUtilities.showMessageOnDialog(headerDlg,messageDlg);
		JSFUtilities.executeJavascriptFunction("PF('dlgLoadHolderW').show()");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Load holder information.
	 */
	public void loadHolderInformation(){
		if(optionSelectedOneRadio.equals(Integer.valueOf(1))){
			blockEntity.setHolder(holder);
		    blockEntity.setFirstLastName(holder.getFirstLastName());
			blockEntity.setSecondLastName(holder.getSecondLastName());
			blockEntity.setNationality(holder.getNationality());
			blockEntity.setName(holder.getName());
			blockEntity.setFullName(holder.getFullName());
			blockEntity.setIndMinor(holder.getIndMinor());
			blockEntity.setIndResidence(holder.getIndResidence());
			blockEntity.setIndDisabled(holder.getIndDisabled());
			blockEntity.setPersonType(holder.getHolderType());
			blockEntity.setDocumentNumber(holder.getDocumentNumber());
			blockEntity.setDocumentType(holder.getDocumentType());
			blockEntity.setDocumentSource(holder.getDocumentSource());
		}
	}
	
	/**
	 * Listener validate cui.
	 */
	public void listenerValidateCUI(){
		try{			
			
		if(blockEntity.getHolder()!=null && blockEntity.getHolder().getIdHolderPk()!=null){
		
		Holder holderAux = blockEntity.getHolder();
		
		blockEntity = new BlockEntity();
		blockEntity.setHolder(holderAux);
		
		blockEntityServiceFacade.validateExistBlockEntity(blockEntity);
		
		//getting holder
		List<Holder> listHolder = blockEntityServiceFacade.findHolderServiceFacade(blockEntity);
		if(listHolder!=null && listHolder.size()==1){
			holder = (listHolder.get(0));
			showHolderInformation();
		}
	   }
	  }
		catch(ServiceException ex){
			//getting data for dialog message validation
			if(ex.getErrorService().equals(ErrorServiceType.BLOCKENTITY_EXIST)){
			String header = PropertiesUtilities.getMessage("header.message.alert");
			JSFUtilities.showMessageOnDialog(header,ex.getMessage());
		    JSFUtilities.executeJavascriptFunction("PF('dlgBlockEntityExistW').show()");
			
			blockEntity.setHolder(new Holder());
			
			}
		}
	}
	
	/**
	 * Clean holder info dialog.
	 */
	public void cleanHolderInfoDialog(){
		holder = new Holder();
		blockEntity.setHolder(new Holder());
		blockEntity.setDocumentNumber(null);
	}
	
	/**
	 * method to clean BlockEntityTO, from view.
	 */
	
	public void changeRadioSelect(){
		JSFUtilities.resetViewRoot();
		blockEntityTO.setInitialDate(CommonsUtilities.currentDate());
		blockEntityTO.setEndDate(CommonsUtilities.currentDate());
		blockEntityTO.setBlockCode(null);
		blockEntityTO.setHolderCode(null);
		blockEntityTO.setPersonType(null);
		blockEntityTO.setDocumentType(null);
		blockEntityTO.setDocumentNumber(null);
		blockEntityTO.setFirstLastName(null);
		blockEntityTO.setSecondLastName(null);
		blockEntityTO.setNames(null);
		blockEntityTO.setBusinessName(null);
		cleanBlockEntityResult();//call clean blockEntityResult to delete data of dataTable
	}
	
	/**
	 * Clean block entity to.
	 */
	public void cleanBlockEntityTO(){
		JSFUtilities.resetViewRoot();
		blockEntityTO.setInitialDate(CommonsUtilities.currentDate());
		blockEntityTO.setEndDate(CommonsUtilities.currentDate());
		blockEntityTO.setBlockCode(null);
		blockEntityTO.setHolderCode(null);
		blockEntityTO.setPersonType(null);
		blockEntityTO.setDocumentType(null);
		blockEntityTO.setDocumentNumber(null);
		blockEntityTO.setFirstLastName(null);
		blockEntityTO.setSecondLastName(null);
		blockEntityTO.setNames(null);
		blockEntityTO.setBusinessName(null);
		blockEntityTO.setRequestSearchType(BlockEntitySearchType.DATE.getCode());
		cleanBlockEntityResult();//call clean blockEntityResult to delete data of dataTable
	}
	
	/**
	 * Clean block entity with out reset view root.
	 */
	public void cleanBlockEntityWithOutResetViewRoot(){
		blockEntityTO.setInitialDate(CommonsUtilities.currentDate());
		blockEntityTO.setEndDate(CommonsUtilities.currentDate());
		blockEntityTO.setBlockCode(null);
		blockEntityTO.setHolderCode(null);
		blockEntityTO.setPersonType(null);
		blockEntityTO.setDocumentType(null);
		blockEntityTO.setDocumentNumber(null);
		blockEntityTO.setFirstLastName(null);
		blockEntityTO.setSecondLastName(null);
		blockEntityTO.setNames(null);
		blockEntityTO.setBusinessName(null);
		blockEntityTO.setRequestSearchType(BlockEntitySearchType.DATE.getCode());
		cleanBlockEntityResult();//call clean blockEntityResult to delete data of dataTable
	}
	
	/**
	 * *
	 * method to clean BlockEntity Result.
	 */
	public void cleanBlockEntityResult(){
		blockEntityResult = null;
	}
	
	/**
	 * Gets the document type description.
	 *
	 * @param documentType the document type
	 * @return the document type description
	 */
	public String getDocumentTypeDescription(Integer documentType){
		for(ParameterTable parameterTable: listDocumentType){
			if(parameterTable.getParameterTablePk().equals(documentType)){
				return parameterTable.getParameterName();
			}
		}
		return StringUtils.EMPTY;
	}
	
	/**
	 * Validate type person.
	 *
	 * @throws ServiceException the service exception
	 */
	public void validateTypePerson()throws ServiceException{
		blockEntity.setHolder(new Holder());
		holder = new Holder();
		blockEntity.setName(null);
		blockEntity.setFirstLastName(null);
		blockEntity.setSecondLastName(null);
		blockEntity.setFullName(null);
		blockEntity.setNationality(null);
		blockEntity.setIndResidence(null);
		blockEntity.setEmail(null);
		blockEntity.setComments(null);
		blockEntity.setHomeAddress(null);
		blockEntity.setIndNotification(BooleanType.NO.getCode());
		blockEntity.setIndMinor(BooleanType.NO.getCode());
		blockEntity.setIndResidence(BooleanType.NO.getCode());
		blockEntity.setIndDisabled(BooleanType.NO.getCode());
		blockEntity.setDocumentType(null);
		blockEntity.setDocumentNumber(null);
		if(blockEntity.getPersonType()!=null && blockEntity.getPersonType().equals(PersonType.JURIDIC.getCode())){
			blockEntity.setNationality(countryResidence);
			blockEntity.setIndResidence(BooleanType.YES.getCode());
			findDocumentTypeList();
			for(ParameterTable parameter : listDocumentType){
				if(parameter.getParameterTablePk().equals(DocumentType.RUC.getCode())){
					blockEntity.setDocumentType(parameter.getParameterTablePk());
					break;
				}
			}
			eventDocumentType();
			
		}
		else{
			blockEntity.setNationality(countryResidence);
			blockEntity.setIndResidence(BooleanType.YES.getCode());
			findDocumentTypeList();
			for(ParameterTable parameter : listDocumentType){
				if(parameter.getParameterTablePk().equals(DocumentType.CI.getCode())){
					blockEntity.setDocumentType(parameter.getParameterTablePk());
					break;
				}
			}
			eventDocumentType();
			loadDocumentEmissionSource();
			blockEntity.setDocumentSource(departmentIssuance);
			
		}
		
	}
	

	/**
	 * Before save all.
	 */
	public void beforeSaveBlockEntity(){
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER);
		
		String messagePropertiesConstanst = null ;
		switch(ViewOperationsType.get(getViewOperationType())){
			case REGISTER: messagePropertiesConstanst = PropertiesConstants.ADM_BLOCK_ENTITY_REGISTRY_CONFIRM_MESSAGE;break;
			case MODIFY: messagePropertiesConstanst = PropertiesConstants.ADM_BLOCK_ENTITY_MODIFY_CONFIRM_MESSAGE;break;
			case ACTIVATE: messagePropertiesConstanst = PropertiesConstants.ADM_BLOCK_ENTITY_ACTIVE_CONFIRM_MESSAGE;break;
			case DEACTIVATE: messagePropertiesConstanst = PropertiesConstants.ADM_BLOCK_ENTITY_DESACTIVE_CONFIRM_MESSAGE;break;
			default: break;
		}
		String bodyMessage = PropertiesUtilities.getMessage(messagePropertiesConstanst);
		showMessageOnDialog(headerMessage,bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('cnfwAdmBlockEntity').show()");	
	}
	
	/**
	 * Clean email.
	 */
	public void cleanEmail(){
		BlockEntity blockEntityChange = blockEntity;
		blockEntityChange.setEmail(null);
	}
	
	/**
	 * Save all.
	 */
	@LoggerAuditWeb
	public void saveBlockEntityObject(){
		try{
			switch(ViewOperationsType.get(getViewOperationType())){
				case REGISTER: 	registerBlockEntityObject();
					cleanBlockEntityWithOutResetViewRoot();break;
							   	
				case MODIFY: 	modifyBlockEntityObject();break;
				
				case ACTIVATE: 	activeBlockEntityObject();
							   	searchEntityBlock();break;
							   	
				case DEACTIVATE:desactiveBlockEntityObject();
								searchEntityBlock();break;
				default:
					break;
			}
		}
		catch (ServiceException ex) {
			String message = PropertiesUtilities.getExceptionMessage(ex.getMessage(),ex.getParams());
			String header = PropertiesUtilities.getMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER);
			JSFUtilities.showMessageOnDialog(header, message);
			JSFUtilities.showSimpleValidationDialog();
			
			//clean blockEntity && holder
			blockEntity.setHolder(new Holder());
			blockEntity.setDocumentNumber(null);
			blockEntity.setFullName(null);
		}
	}

	/**
	 * Register block entity object.
	 *
	 * @throws ServiceException the service exception
	 */
	public void registerBlockEntityObject() throws ServiceException{
		BlockEntity blockEntity = this.blockEntity;
		blockEntity.setRegistryDate(CommonsUtilities.currentDate());
		blockEntity.setStateBlockEntity(BlockEntityStateType.ACTIVATED.getCode());
		blockEntity.setRegistryUser(userInfo.getUserAccountSession().getUserName());		
	    
		//call transactional method to save
		blockEntityServiceFacade.registerBlockEntityObject(blockEntity);
		
		//show message on screen
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_REGISTRY_BLOCKENTITY_OK,new Object[]{blockEntity.getIdBlockEntityPk().toString()});
		JSFUtilities.showMessageOnDialog(headerMessage, bodyMessage);
		//JSFUtilities.showSimpleEndTransactionDialog("searchBockEntity");
		JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
		
		//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_ENTITY_REGISTRATION.getCode());
		Object[] parameters = { blockEntity.getIdBlockEntityPk().toString() };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
		
	}
	
	/**
	 * Modify block entity object.
	 *
	 * @throws ServiceException the service exception
	 */
	public void modifyBlockEntityObject() throws ServiceException{
		BlockEntity blockEntityObject = blockEntity;
		
		//call transtaccional method to save
		blockEntityServiceFacade.modifyBlockEntityObject(blockEntityObject);
		
		//show message on screen
		Long blockEntityId = blockEntityObject.getIdBlockEntityPk();
		String documentType = getDocumentTypeDescription(blockEntityObject.getDocumentType());
		String documentNumber = blockEntityObject.getDocumentNumber();
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_MODIFY_BLOCKENTITY_OK,new Object[]{blockEntityId,documentType,documentNumber});
		showMessageOnDialog(headerMessage, bodyMessage);

		//show dialog on front End
		//JSFUtilities.showSimpleEndTransactionDialog("searchBockEntity");
		JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
		
		//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_ENTITY_DATA_MODIFICATION.getCode());
		Object[] parameters = { blockEntityId.toString(),
				documentType,
				documentNumber};
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	
	/**
	 * Desactive block entity object.
	 *
	 * @throws ServiceException the service exception
	 */
	public void desactiveBlockEntityObject() throws ServiceException {
		BlockEntity blockEntityObject = blockEntity;
		
		//call transtaccional method to save
		blockEntityServiceFacade.desactiveBlockEntityObject(blockEntityObject);
		
		//show message on screen
		Long blockEntityId = blockEntityObject.getIdBlockEntityPk();
		String fullName = blockEntityObject.getFullName();
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_DESACTIVE_BLOCKENTITY_OK,new Object[]{blockEntityId,fullName});
		showMessageOnDialog(headerMessage, bodyMessage);
		
		//show dialog on front End
		//JSFUtilities.showSimpleEndTransactionDialog("searchBockEntity");
		JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
		
		//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_ENTITY_DISABLE.getCode());
		Object[] parameters = { blockEntityId.toString(), fullName };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	
	/**
	 * Active block entity object.
	 *
	 * @throws ServiceException the service exception
	 */
	public void activeBlockEntityObject() throws ServiceException {
		BlockEntity blockEntityObject = blockEntity;
		
		//call transtaccional method to save
		blockEntityServiceFacade.activeBlockEntityObject(blockEntityObject);
		
		//show message on screen
		Long blockEntityId = blockEntityObject.getIdBlockEntityPk();
		String fullName = blockEntityObject.getFullName();
		
		String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS);
		String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.LBL_ACTIVATE_BLOCKENTITY_OK,new Object[]{blockEntityId,fullName});
		showMessageOnDialog(headerMessage, bodyMessage);
		
		//show dialog on front End
		//JSFUtilities.showSimpleEndTransactionDialog("searchBockEntity");
		JSFUtilities.executeJavascriptFunction("PF('endTrasaction').show()");
		
		//SIEMPRE ENVIAMOS AL USUARIO QUE REGISTRA LA OPERACION Y A LOS SUPERVISORES
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.BLOCK_ENTITY_ACTIVE.getCode());
		Object[] parameters = { blockEntityId.toString(), fullName };
		notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, parameters);
	}
	
	/**
	 * Loading block information.
	 *
	 * @param event the event
	 */
	public void loadBlockEntityObject(ActionEvent event){	
		if(event!=null){		
			setViewOperationType(ViewOperationsType.DETAIL.getCode());	
			BlockEntity blockEntitySelected = (BlockEntity) event.getComponent().getAttributes().get("blockInformation");
			
			try{
				//go to database get BlockEntity Object
				blockEntity = blockEntityServiceFacade.findBlockEntity(blockEntitySelected.getIdBlockEntityPk());
				listDocumentType = documentValidator.getDocumentTypeResult(blockEntityServiceFacade.populateFromBockEntity(blockEntity,countryResidence));
				
			}catch(ServiceException ex){
				logger.info("BLOCK ENTITI NOT FINDED");
			}
		}
	}
	

	/**
	 * Validate is valid user.
	 *
	 * @param blockEntitySelected the block entity selected
	 * @return the list
	 */
	private List<String> validateIsValidUser(BlockEntity blockEntitySelected) {	
		List<OperationUserTO> lstOperationUserParam = new ArrayList<OperationUserTO>();
		List<String> lstOperationUserResult;	
		OperationUserTO operationUserTO = new OperationUserTO();  
		operationUserTO.setOperNumber(blockEntitySelected.getIdBlockEntityPk().toString());
		operationUserTO.setUserName(blockEntitySelected.getRegistryUser());
		lstOperationUserParam.add(operationUserTO);
		lstOperationUserResult = getIsSubsidiary(userInfo,lstOperationUserParam);
		return lstOperationUserResult;
	}
	
	/**
	 * Before on modify.
	 *
	 * @return the string
	 */
	public String beforeModifyBlockEntity(){	
		if(blockEntitySelected==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
		}else if(!blockEntitySelected.getStateBlockEntity().equals(BlockEntityStateType.ACTIVATED.getCode())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_DESACTIVE_INVALID);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
				
		}else{			
			List<String> lstOperationUser = validateIsValidUser(blockEntitySelected);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_MODIFY,new Object[]{StringUtils.join(lstOperationUser,",")});
					showMessageOnDialog(headerMessage, bodyMessage);
					JSFUtilities.showSimpleValidationDialog();
					return "";
			  }
			setViewOperationType(ViewOperationsType.MODIFY.getCode());
			try {
				blockEntity = blockEntityServiceFacade.findBlockEntity(blockEntitySelected.getIdBlockEntityPk());
				listDocumentType = documentValidator.getDocumentTypeResult(blockEntityServiceFacade.populateFromBockEntity(blockEntity,countryResidence));
			
				return "goToRegisterBlockEntity";
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}	
		return null;
	}
	
	/**
	 * Before desactive block entity.
	 *
	 * @return the string
	 */
	public String beforeDesactiveBlockEntity(){
		if(blockEntitySelected==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
		}
		else if(!blockEntitySelected.getStateBlockEntity().equals(BlockEntityStateType.ACTIVATED.getCode())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_DESACTIVE_INVALID);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
		}else {
			List<String> lstOperationUser = validateIsValidUser(blockEntitySelected);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_DESACTIVE,new Object[]{StringUtils.join(lstOperationUser,",")});
					showMessageOnDialog(headerMessage, bodyMessage);
					JSFUtilities.showSimpleValidationDialog();
					return "";
			  }
			setViewOperationType(ViewOperationsType.DEACTIVATE.getCode());
			try {
				blockEntity = blockEntityServiceFacade.findBlockEntity(blockEntitySelected.getIdBlockEntityPk());
				return "goToRegisterBlockEntity";
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Before active block entity.
	 *
	 * @return the string
	 */
	public String beforeActiveBlockEntity(){
		if(blockEntitySelected==null){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
			
		}else if(!blockEntitySelected.getStateBlockEntity().equals(BlockEntityStateType.OFF.getCode())){
			String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.WARNING_OPERATION_ACTIVE_INVALID);
			showMessageOnDialog(headerMessage, bodyMessage);
			JSFUtilities.showSimpleValidationDialog();
		}else {
			List<String> lstOperationUser = validateIsValidUser(blockEntitySelected);
			  if(Validations.validateListIsNotNullAndNotEmpty(lstOperationUser)){
				  String headerMessage = PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT);
					String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.ERROR_MESSAGE_INVALID_USER_ACTIVE,new Object[]{StringUtils.join(lstOperationUser,",")});
					showMessageOnDialog(headerMessage, bodyMessage);
					JSFUtilities.showSimpleValidationDialog();
					return "";
			  }
			setViewOperationType(ViewOperationsType.ACTIVATE.getCode());
			try {
				blockEntity = blockEntityServiceFacade.findBlockEntity(blockEntitySelected.getIdBlockEntityPk());
				return "goToRegisterBlockEntity";
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * Validate option selected.
	 */
	public void validateOptionSelected(){
	 try{
		    blockEntity = new BlockEntity();
			blockEntity.setHolder(new Holder());
			blockEntity.setIndNotification(BooleanType.NO.getCode());
			blockEntity.setIndResidence(BooleanType.NO.getCode());
			blockEntity.setIndMinor(BooleanType.NO.getCode());
			holder = new Holder();
			documentTO.setIndicatorDocumentSource(false);
		if(optionSelectedOneRadio.equals(Integer.valueOf(0))){
			disabledCuiCode = true;
			blockEntity.setIndResidence(BooleanType.YES.getCode());
			blockEntity.setNationality(countryResidence);
			findDocumentTypeList();
		}
		else{
			disabledCuiCode = false;
		}
	 }
	 catch(Exception e){
		 e.printStackTrace();
	 }
	}
	
	/**
	 * Event document source.
	 */
	public void eventDocumentSource(){
		blockEntity.setDocumentNumber(null);
	}

	/**
	 * Gets the list person type.
	 *
	 * @return the list person type
	 */
	public List<ParameterTable> getListPersonType() {
		return listPersonType;
	}

	/**
	 * Sets the list person type.
	 *
	 * @param listPersonType the new list person type
	 */
	public void setListPersonType(List<ParameterTable> listPersonType) {
		this.listPersonType = listPersonType;
	}

	/**
	 * Gets the list document type.
	 *
	 * @return the list document type
	 */
	public List<ParameterTable> getListDocumentType() {
		return listDocumentType;
	}

	/**
	 * Sets the list document type.
	 *
	 * @param listDocumentType the new list document type
	 */
	public void setListDocumentType(List<ParameterTable> listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	/**
	 * Gets the list geographic location.
	 *
	 * @return the list geographic location
	 */
	public List<GeographicLocation> getListGeographicLocation() {
		return listGeographicLocation;
	}

	/**
	 * Sets the list geographic location.
	 *
	 * @param listGeographicLocation the new list geographic location
	 */
	public void setListGeographicLocation(
			List<GeographicLocation> listGeographicLocation) {
		this.listGeographicLocation = listGeographicLocation;
	}

	/**
	 * Gets the block entity.
	 *
	 * @return the block entity
	 */
	public BlockEntity getBlockEntity() {
		return blockEntity;
	}

	/**
	 * Sets the block entity.
	 *
	 * @param blockEntity the new block entity
	 */
	public void setBlockEntity(BlockEntity blockEntity) {
		this.blockEntity = blockEntity;
	}

	/**
	 * Gets the block entity to.
	 *
	 * @return the block entity to
	 */
	public BlockEntityTO getBlockEntityTO() {
		return blockEntityTO;
	}

	/**
	 * Sets the block entity to.
	 *
	 * @param blockEntityTO the new block entity to
	 */
	public void setBlockEntityTO(BlockEntityTO blockEntityTO) {
		this.blockEntityTO = blockEntityTO;
	}

	/**
	 * Gets the block entity result.
	 *
	 * @return the block entity result
	 */
	public GenericDataModel<BlockEntity> getBlockEntityResult() {
		return blockEntityResult;
	}

	/**
	 * Sets the block entity result.
	 *
	 * @param blockEntityResult the new block entity result
	 */
	public void setBlockEntityResult(GenericDataModel<BlockEntity> blockEntityResult) {
		this.blockEntityResult = blockEntityResult;
	}

	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Gets the block entity selected.
	 *
	 * @return the block entity selected
	 */
	public BlockEntity getBlockEntitySelected() {
		return blockEntitySelected;
	}

	/**
	 * Sets the block entity selected.
	 *
	 * @param blockEntitySelected the new block entity selected
	 */
	public void setBlockEntitySelected(BlockEntity blockEntitySelected) {
		this.blockEntitySelected = blockEntitySelected;
	}
	
	/**
	 * Gets the list boolean.
	 *
	 * @return the list boolean
	 */
	public List<BooleanType> getListBoolean(){
		return BooleanType.list;
	}

	/**
	 * Gets the country residence.
	 *
	 * @return the country residence
	 */
	public Integer getCountryResidence() {
		return countryResidence;
	}

	/**
	 * Sets the country residence.
	 *
	 * @param countryResidence the new country residence
	 */
	public void setCountryResidence(Integer countryResidence) {
		this.countryResidence = countryResidence;
	}

	/**
	 * Gets the list document source.
	 *
	 * @return the list document source
	 */
	public List<ParameterTable> getListDocumentSource() {
		return listDocumentSource;
	}

	/**
	 * Sets the list document source.
	 *
	 * @param listDocumentSource the new list document source
	 */
	public void setListDocumentSource(List<ParameterTable> listDocumentSource) {
		this.listDocumentSource = listDocumentSource;
	}

	/**
	 * Gets the document to.
	 *
	 * @return the document to
	 */
	public DocumentTO getDocumentTO() {
		return documentTO;
	}

	/**
	 * Sets the document to.
	 *
	 * @param documentTO the new document to
	 */
	public void setDocumentTO(DocumentTO documentTO) {
		this.documentTO = documentTO;
	}

	/**
	 * Gets the option selected one radio.
	 *
	 * @return the option selected one radio
	 */
	public Integer getOptionSelectedOneRadio() {
		return optionSelectedOneRadio;
	}

	/**
	 * Sets the option selected one radio.
	 *
	 * @param optionSelectedOneRadio the new option selected one radio
	 */
	public void setOptionSelectedOneRadio(Integer optionSelectedOneRadio) {
		this.optionSelectedOneRadio = optionSelectedOneRadio;
	}

	/**
	 * Checks if is disabled cui code.
	 *
	 * @return true, if is disabled cui code
	 */
	public boolean isDisabledCuiCode() {
		return disabledCuiCode;
	}

	/**
	 * Sets the disabled cui code.
	 *
	 * @param disabledCuiCode the new disabled cui code
	 */
	public void setDisabledCuiCode(boolean disabledCuiCode) {
		this.disabledCuiCode = disabledCuiCode;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the new holder
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the id participant.
	 *
	 * @return the id participant
	 */
	public Long getIdParticipant() {
		return idParticipant;
	}

	/**
	 * Sets the id participant.
	 *
	 * @param idParticipant the new id participant
	 */
	public void setIdParticipant(Long idParticipant) {
		this.idParticipant = idParticipant;
	}

	/**
	 * Checks if is flag participant.
	 *
	 * @return true, if is flag participant
	 */
	public boolean isFlagParticipant() {
		return flagParticipant;
	}

	/**
	 * Sets the flag participant.
	 *
	 * @param flagParticipant the new flag participant
	 */
	public void setFlagParticipant(boolean flagParticipant) {
		this.flagParticipant = flagParticipant;
	}
	
	
	
	
}