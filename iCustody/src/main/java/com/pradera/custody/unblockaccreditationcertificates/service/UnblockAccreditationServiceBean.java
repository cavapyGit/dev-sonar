package com.pradera.custody.unblockaccreditationcertificates.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.ParticipantServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.custody.accreditationcertificates.service.AccreditationCertificatesServiceBean;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.accreditationcertificates.to.DateTypeTO;
import com.pradera.custody.unblockaccreditationcertificates.to.UnblockAccreditationOperationTO;
import com.pradera.custody.webclient.CustodyServiceConsumer;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.service.AccountsComponentService;
import com.pradera.integration.component.business.to.AccountsComponentTO;
import com.pradera.integration.component.business.to.HolderAccountBalanceTO;
import com.pradera.integration.component.business.to.MarketFactAccountTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountDetail;
import com.pradera.model.component.type.ParameterOperationType;
import com.pradera.model.custody.accreditationcertificates.AccreditationDetail;
import com.pradera.model.custody.accreditationcertificates.AccreditationMarketFact;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.accreditationcertificates.UnblockAccreditationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.CertificationType;
import com.pradera.model.custody.type.UnblockAccreditationOperationDateType;
import com.pradera.model.custody.type.UnblockAccreditationOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.InstrumentType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.negotiation.HolderAccountOperation;
import com.pradera.model.operations.RequestCorrelativeType;

// TODO: Auto-generated Javadoc
/**
 * The Class UnblockAccreditationServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 04/04/2014
 */
@Stateless
public class UnblockAccreditationServiceBean extends CrudDaoServiceBean {
	
	/** The rejected state type. */
	private Integer rejectedStateType	 = UnblockAccreditationOperationStateType.REJECTED.getCode();
	
	/** The confirmed state type. */
	private Integer confirmedStateType	 = UnblockAccreditationOperationStateType.CONFIRMED.getCode();
	
	/** The approved state type. */
	private Integer approvedStateType 	 = UnblockAccreditationOperationStateType.APPROVED.getCode();
	
	/** The annulled state type. */
	private Integer annulledStateType	 = UnblockAccreditationOperationStateType.ANNULLED .getCode();
	
	/** The log. */
	@Inject
	transient PraderaLogger log;
	
	/** The bean service. */
	@Inject
	private AccreditationCertificatesServiceBean accreditationCertificatesServiceBean;
	
	/** The executor component service bean. */
	@Inject
    Instance<AccountsComponentService> accountsComponentService;
	
	/** The custody service consumer. */
	@EJB
	private CustodyServiceConsumer custodyServiceConsumer;
	
	/** The participant service bean. */
	@EJB
	private ParticipantServiceBean participantServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;

	
	/**
	 * Save.
	 *
	 * @param listAccreditationSelectedTO the list accreditation selected to
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String Save(List<AccreditationOperationTO> listAccreditationSelectedTO, LoggerUser loggerUser) throws ServiceException{
		
		List<UnblockAccreditationOperation> list = new ArrayList<>();
		List<Long> unblockNumbers = new ArrayList<>();
		String unblockNumbersMessage = null;
		UnblockAccreditationOperation unblockAccreditationOperation = null;
		CustodyOperation custodyOperation = null;
		AccreditationOperation accreditationOperation = null;
		Long operationNumber = null;
		
		for (AccreditationOperationTO accreditationOperationTO : listAccreditationSelectedTO) {
			
			//validation if the accreditation certificate request is in use
			Object[] params = new Object[1];
			UnblockAccreditationOperationTO toTemp = new UnblockAccreditationOperationTO();
			AccreditationOperation toAccreditationTemp = new AccreditationOperation();
			List<Integer> listState = new ArrayList<>();
			
			toAccreditationTemp.setIdAccreditationOperationPk(accreditationOperationTO.getIdAccreditationOperationPk());
			toTemp.setAccreditationOperation(toAccreditationTemp);
			listState.add(UnblockAccreditationOperationStateType.ANNULLED.getCode());
			listState.add(UnblockAccreditationOperationStateType.REJECTED.getCode());
			toTemp.setListState(listState);
			
			List<UnblockAccreditationOperationTO> listTemp = searchUnblockAccreditationOperation(toTemp);
			params[0] = accreditationOperationTO.getCustodyOperation().getOperationNumber();
			
			if(!listTemp.isEmpty() && listTemp.size() >= GeneralConstants.ONE_VALUE_INTEGER){
				throw new ServiceException(ErrorServiceType.UNBLOCK_ACCREDITATION_SAVE_CONCURRENCY,params);
			}
			
			unblockAccreditationOperation = new UnblockAccreditationOperation();
			custodyOperation = new CustodyOperation();
			accreditationOperation = new AccreditationOperation();
			
			operationNumber = getNextCorrelativeOperations(RequestCorrelativeType.REQUEST_UNBLOCK_OPERATION_ACCREDITATION_CERTIFICATE);
			
			accreditationOperation = find(AccreditationOperation.class, accreditationOperationTO.getIdAccreditationOperationPk());
			
			unblockAccreditationOperation.setAccreditationOperation(accreditationOperation);
			unblockAccreditationOperation.setIndAutomatic(GeneralConstants.ZERO_VALUE_INTEGER);
			unblockAccreditationOperation.setExpirationDate(CommonsUtilities.currentDate());
			
			custodyOperation.setOperationType(ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode());
			custodyOperation.setOperationDate(CommonsUtilities.currentDateTime());
			custodyOperation.setOperationNumber(operationNumber);
			custodyOperation.setRegistryDate(CommonsUtilities.currentDateTime());
			custodyOperation.setRegistryUser(loggerUser.getUserName());
			custodyOperation.setState(UnblockAccreditationOperationStateType.REGISTERED.getCode());
			
			unblockAccreditationOperation.setCustodyOperation(custodyOperation);
			
			list.add(unblockAccreditationOperation);
		}
		//save the unblock accreditation operation
		this.saveAll(list);
		
		//add the id unblock accreditation operation to list for message
		for (UnblockAccreditationOperation o : list) {
			unblockNumbers.add(o.getCustodyOperation().getOperationNumber());
		}
		
		unblockNumbersMessage = unblockNumbersToMessage(unblockNumbers);
		
		return unblockNumbersMessage;
	}
	
	/**
	 * Unblock numbers to message.
	 *
	 * @param unblockNumber the unblock number
	 * @return the string
	 */
	public String unblockNumbersToMessage(List<Long> unblockNumber){
		String certs = StringUtils.EMPTY;
		if(unblockNumber != null && unblockNumber.size() ==1)
			return unblockNumber.get(0).toString();
		if(unblockNumber != null && unblockNumber.size() > 0){
			for(Long crt : unblockNumber){
				if(StringUtils.equalsIgnoreCase(StringUtils.EMPTY, certs)){
					certs = crt.toString();
				}else{
					certs = certs.concat(GeneralConstants.STR_COMMA.concat(crt.toString()));
				}
			}
		}
		return certs;
	}
	
	/**
	 * Approve unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String ApproveUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO,
			LoggerUser loggerUser) throws ServiceException{
		return ChangeStateToUnblockAccreditationRequest(listUnblockAccreditationSelectedTO, approvedStateType, loggerUser);
	}

	/**
	 * Annul unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String AnnulUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO,
			LoggerUser loggerUser) throws ServiceException{
		return ChangeStateToUnblockAccreditationRequest(listUnblockAccreditationSelectedTO, annulledStateType, loggerUser);
	}

	/**
	 * Reject unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String RejectUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO,
			LoggerUser loggerUser) throws ServiceException{
		return ChangeStateToUnblockAccreditationRequest(listUnblockAccreditationSelectedTO, rejectedStateType, loggerUser);
	}
	
	/**
	 * Confirm unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String ConfirmUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO,
			LoggerUser loggerUser) throws ServiceException{
		String unblockNumbersMessage = ChangeStateToUnblockAccreditationRequest(listUnblockAccreditationSelectedTO, confirmedStateType, loggerUser);
		for (UnblockAccreditationOperationTO to : listUnblockAccreditationSelectedTO) {
			AccreditationOperation accreditationOperationToUnblock = find(AccreditationOperation.class, to.getAccreditationOperation().getIdAccreditationOperationPk());
			UnblockAccreditation(accreditationOperationToUnblock, loggerUser);
		}
		
		return unblockNumbersMessage;
	}
	
	/**
	 * Change state to unblock accreditation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @param state the state
	 * @param loggerUser the logger user
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String ChangeStateToUnblockAccreditationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO,
			Integer state, LoggerUser loggerUser) throws ServiceException{
		
		List<Long> unblockNumbers = new ArrayList<>();
		String unblockNumbersMessage = null;
		
		for (UnblockAccreditationOperationTO to : listUnblockAccreditationSelectedTO) {
			
			UnblockAccreditationOperation unblockAccreditationOperation = find(UnblockAccreditationOperation.class, to.getIdUnblockAccreditationPk());
			CustodyOperation custodyOperation = unblockAccreditationOperation.getCustodyOperation();
			custodyOperation.setState(state);
			
			if(annulledStateType.equals(state)){
				custodyOperation.setAnnulationUser(loggerUser.getUserName());
				custodyOperation.setAnnulationDate(CommonsUtilities.currentDateTime());
			}else if(approvedStateType.equals(state)){
				custodyOperation.setApprovalUser(loggerUser.getUserName());
				custodyOperation.setApprovalDate(CommonsUtilities.currentDateTime());
			}else if(rejectedStateType.equals(state)){
				custodyOperation.setRejectUser(loggerUser.getUserName());
				custodyOperation.setRejectDate(CommonsUtilities.currentDateTime());
			}else if(confirmedStateType.equals(state)){
				custodyOperation.setConfirmUser(loggerUser.getUserName());
				custodyOperation.setConfirmDate(CommonsUtilities.currentDateTime());
				unblockAccreditationOperation.setExpirationDate(CommonsUtilities.currentDateTime());
			}
			this.update(unblockAccreditationOperation);
			unblockNumbers.add(to.getCustodyOperation().getOperationNumber());
		}
		
		unblockNumbersMessage = unblockNumbersToMessage(unblockNumbers);
		return unblockNumbersMessage;
	}

	/**
	 * Un block accreditation.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
//	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK)	
	public void UnblockAccreditation(AccreditationOperation accreditationOperationToUnblock, LoggerUser loggerUser) throws ServiceException{
        
		Long idAccreditationOperationPk = accreditationOperationToUnblock.getCustodyOperation().getIdCustodyOperationPk();
		
		List<HolderAccountBalanceTO> accountBalanceMixedTOs = this.getAccountBalanceTOsFromMixed(idAccreditationOperationPk);
		
		if(accreditationOperationToUnblock.getCertificationType().equals(CertificationType.AVAILABLE_BALANCE.getCode())){
			this.executeAccountsComponent(idAccreditationOperationPk, BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK.getCode(),
									  	  ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode());
			
		}else if(accreditationOperationToUnblock.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())){
			List<BlockOperation> blockOperations = accreditationCertificatesServiceBean.searchBlockOperations(idAccreditationOperationPk);
			for(BlockOperation objBlockOperation : blockOperations){
				objBlockOperation.setIndAccreditation(BooleanType.NO.getCode());
				accreditationCertificatesServiceBean.updateBlockOperation(objBlockOperation);
			}
		}else{
			if(accountBalanceMixedTOs.size() >0){
				accountsComponentService.get().executeAccountsComponent(this.getAccountsComponentTO(idAccreditationOperationPk,
						BusinessProcessType.ACCREDITATION_CERTIFICATE_MANUAL_UNBLOCK.getCode(), accountBalanceMixedTOs,
						   	ParameterOperationType.ACCREDITATION_CERTIFICATE_UNBLOCK.getCode()));
			}
			List<BlockOperation> blockOperations = accreditationCertificatesServiceBean.searchBlockOperations(idAccreditationOperationPk);
			if(blockOperations != null){
				for(BlockOperation objBlockOperation : blockOperations){
					objBlockOperation.setIndAccreditation(BooleanType.NO.getCode());
					accreditationCertificatesServiceBean.updateBlockOperation(objBlockOperation);
				}
			}
			List<HolderAccountOperation> lstHAO = accreditationCertificatesServiceBean.searchHolderAccountOperationToUpdate(accreditationOperationToUnblock,loggerUser);
			Integer state= accreditationOperationToUnblock.getAccreditationState();
			if(lstHAO != null){
				accreditationCertificatesServiceBean.updateReportedOperation(lstHAO, state);
			}
		}
		
		/*** UPDATING STATUS TO UNBLOCKED ***/
		accreditationOperationToUnblock.setAccreditationState(AccreditationOperationStateType.UNBLOCKED.getCode());
		accreditationOperationToUnblock.setRealExpirationDate(CommonsUtilities.currentDateTime());
		this.accreditationCertificatesServiceBean.update(accreditationOperationToUnblock);
		
		/*
		 * CALLING THE CLIENT WEB SERVICE
		 */
		if(!accreditationOperationToUnblock.getCertificationType().equals(CertificationType.BLOCKED_BALANCE.getCode())
				&& Validations.validateListIsNotNullAndNotEmpty(accountBalanceMixedTOs)){
			
			for(AccreditationDetail detail : accreditationCertificatesServiceBean.searchAccreditationDetailByAccreditationOperation(accreditationOperationToUnblock.getIdAccreditationOperationPk())){
				if(detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPA.getCode()) ||
						detail.getSecurity().getSecurityClass().equals(SecurityClassType.DPF.getCode())){
					try {
						custodyServiceConsumer.sendUnblockAccreditationOperationWebClient(accreditationOperationToUnblock, loggerUser);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * Find accreditation operation.
	 *
	 * @param idUnblockAccreditationPk the id unblock accreditation pk
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public UnblockAccreditationOperation findUnblockAccreditationOperation(Long idUnblockAccreditationPk) throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select UAO " );
		
		sbQuery.append(" FROM UnblockAccreditationOperation UAO " );
		sbQuery.append(" 	inner JOIN fetch UAO.custodyOperation UCO " );
		sbQuery.append(" 	inner JOIN fetch UAO.accreditationOperation AO " );
		
		sbQuery.append(" where UAO.idUnblockAccreditationPk = :idUnblockAccreditationPk ");
		Map<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("idUnblockAccreditationPk", idUnblockAccreditationPk);
		return (UnblockAccreditationOperation) findObjectByQueryString(sbQuery.toString(), parameters);
	}

	/**
	 * Find accreditation operation.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public AccreditationOperation findAccreditationOperation(Long accreditationOperationId)throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();	
		sbQuery.append("Select ao " );
		sbQuery.append("FROM AccreditationOperation AO " );
		sbQuery.append(" inner JOIN fetch AO.holder H " );
		sbQuery.append(" left JOIN fetch AO.participantPetitioner PP " );
		sbQuery.append(" inner JOIN fetch AO.custodyOperation co " );
		sbQuery.append(" left JOIN fetch AO.listAccreditationDeliveryInformation lad " );
		sbQuery.append("where AO.idAccreditationOperationPk = :accreditationOperationId ");
		Map<String,Object> parameters = new HashMap<String, Object>();
		parameters.put("accreditationOperationId", accreditationOperationId);
		
		return (AccreditationOperation) findObjectByQueryString(sbQuery.toString(), parameters);		
	}
	
	/**
	 * Search unblock accreditation operation.
	 *
	 * @param to the to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UnblockAccreditationOperationTO> searchUnblockAccreditationOperation(UnblockAccreditationOperationTO to) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		List<UnblockAccreditationOperationTO> listUnblockAccreditationOperationTO = new ArrayList<>();
		
		sbQuery.append(" SELECT " );
		sbQuery.append("  UAO.idUnblockAccreditationPk, " ); 	//0
		sbQuery.append("  UAO.expirationDate, " );				//1
		sbQuery.append("  UCO.operationNumber, " );				//2
		sbQuery.append("  UCO.registryDate, " );				//3
		sbQuery.append("  UCO.state, " );						//4
		sbQuery.append("  AO.idAccreditationOperationPk, " );	//5
		sbQuery.append("  CO.operationNumber, " );				//6
		sbQuery.append("  HA.idHolderAccountPk, " );			//7
		sbQuery.append("  HA.accountNumber, " );				//8
		sbQuery.append("  S.idSecurityCodePk " );				//9
		
		sbQuery.append(" FROM UnblockAccreditationOperation UAO " );
		sbQuery.append(" 	inner JOIN UAO.custodyOperation UCO " );
		sbQuery.append(" 	inner JOIN UAO.accreditationOperation AO " );
		sbQuery.append("  	Inner Join AO.accreditationDetails AD ");
		sbQuery.append(" 	inner JOIN AO.custodyOperation CO " );
		sbQuery.append(" 	left  JOIN AO.participantPetitioner P " );
		sbQuery.append("  	inner JOIN AD.holderAccount HA " );
		sbQuery.append(" 	inner JOIN AO.holder H " );
		sbQuery.append(" 	inner JOIN AD.security S " );
		
		sbQuery.append(" WHERE 1=1 ");
		
		if(Validations.validateIsNotNull(to.getIdUnblockAccreditationPk())){
			sbQuery.append(" AND UAO.idUnblockAccreditationPk =:idUnblockAccreditationPk");
			mapParam.put("idUnblockAccreditationPk", to.getIdUnblockAccreditationPk());
		}
		if(Validations.validateIsNotNull(to.getCustodyOperation())
				&& Validations.validateIsNotNull(to.getCustodyOperation().getOperationNumber())){
			sbQuery.append(" AND UCO.operationNumber =:operationNumberUAO");
			mapParam.put("operationNumberUAO", to.getCustodyOperation().getOperationNumber());
		}
		if(Validations.validateIsNotNull(to.getAccreditationOperation())
				&& Validations.validateIsNotNull(to.getAccreditationOperation().getIdAccreditationOperationPk())){
			sbQuery.append(" AND AO.idAccreditationOperationPk =:idAccreditationOperationPk");
			mapParam.put("idAccreditationOperationPk", to.getAccreditationOperation().getIdAccreditationOperationPk());
		}
		if(Validations.validateIsNotNull(to.getAccreditationOperation())
				&& Validations.validateIsNotNull(to.getAccreditationOperation().getCustodyOperation())
				&& Validations.validateIsNotNull(to.getAccreditationOperation().getCustodyOperation().getOperationNumber())){
			sbQuery.append(" AND CO.operationNumber =:operationNumberAO");
			mapParam.put("operationNumberAO", to.getAccreditationOperation().getCustodyOperation().getOperationNumber());
		}
		if(Validations.validateIsNotNull(to.getAccreditationOperation())
				&& Validations.validateIsNotNull(to.getAccreditationOperation().getCertificateNumber())){
			sbQuery.append(" AND AO.certificateNumber =:certificateNumber");
			mapParam.put("certificateNumber", to.getAccreditationOperation().getCertificateNumber());
		}
		if(!to.isUserIssuerDPF() && Validations.validateIsNotNull(to.getParticipantPetitioner())
				&& Validations.validateIsNotNull(to.getParticipantPetitioner().getIdParticipantPk())){
			sbQuery.append(" AND P.idParticipantPk =:idParticipantPk");
			mapParam.put("idParticipantPk", to.getParticipantPetitioner().getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(to.getIssuer())
				&& Validations.validateIsNotNull(to.getIssuer().getIdIssuerPk())){
			sbQuery.append(" AND S.issuer.idIssuerPk =:idIssuerPk");
			mapParam.put("idIssuerPk", to.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNull(to.getHolder())
				&& Validations.validateIsNotNull(to.getHolder().getIdHolderPk())){
			sbQuery.append(" AND H.idHolderPk =:idHolderPk");
			mapParam.put("idHolderPk", to.getHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNull(to.getSecurity())
				&& Validations.validateIsNotNull(to.getSecurity().getSecurityClass())){
			sbQuery.append(" AND S.securityClass =:securityClass");
			mapParam.put("securityClass", to.getSecurity().getSecurityClass());
		}
		if(Validations.validateIsNotNull(to.getSecurity())
				&& Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk())){
			sbQuery.append(" AND S.idSecurityCodePk =:idSecurityCodePk");
			mapParam.put("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(to.getInitialDate())){
			sbQuery.append(" AND trunc(UAO.expirationDate) >=:initialDate ");
			mapParam.put("initialDate", to.getInitialDate());
		}
		if(Validations.validateIsNotNull(to.getFinalDate())){
			sbQuery.append(" AND trunc(UAO.expirationDate) <=:finalDate ");
			mapParam.put("finalDate", to.getFinalDate());
		}
		if(Validations.validateIsNotNull(to.getCustodyOperation())
				&& Validations.validateIsNotNull(to.getCustodyOperation().getState())){
			sbQuery.append(" AND UCO.state =:state");
			mapParam.put("state", to.getCustodyOperation().getState());
		}
		if(Validations.validateIsNotNullAndNotEmpty(to.getListState())){
			sbQuery.append(" AND UCO.state not in (:states)");
			mapParam.put("states", to.getListState());
		}
		if(Validations.validateIsNotNull(to.getIndAutomatic())){
			sbQuery.append("AND UAO.indAutomatic =:indAutomatic");
			mapParam.put("indAutomatic", to.getIndAutomatic());
		}
		sbQuery.append(" ORDER BY UCO.operationNumber DESC ");
		
		Query query = em.createQuery(sbQuery.toString());
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        
        List<Object> objectsList = ((List<Object>)query.getResultList());
        
        for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			UnblockAccreditationOperationTO toResult = new UnblockAccreditationOperationTO();
			toResult.setCustodyOperation(new CustodyOperation());
			toResult.setAccreditationOperation(new AccreditationOperation());
			CustodyOperation custodyOperationAO = new CustodyOperation();
			toResult.setSecurity(new Security());
			
			toResult.setIdUnblockAccreditationPk((Long)objects[0]);
			toResult.setExpirationDate((Date)objects[1]);
			toResult.getCustodyOperation().setOperationNumber((Long)objects[2]);
			toResult.getCustodyOperation().setRegistryDate((Date)objects[3]);
			toResult.getCustodyOperation().setState((Integer)objects[4]);
			
			toResult.getAccreditationOperation().setIdAccreditationOperationPk((Long)objects[5]);
			custodyOperationAO.setOperationNumber((Long)objects[6]);
			toResult.getAccreditationOperation().setCustodyOperation(custodyOperationAO);
			
			toResult.setIdHolderAccountPk((Long)objects[7]);
			toResult.setAccountNumber((Integer)objects[8]);
			//Get Cui - Description list
			toResult.setHolderDescription(searchHolderDescriptionByHolderAccount(toResult.getIdHolderAccountPk()));
			toResult.getSecurity().setIdSecurityCodePk((String)objects[9]);
			
			listUnblockAccreditationOperationTO.add(toResult);
		}
        
		return listUnblockAccreditationOperationTO;
	}
	
	/**
	 * Search accreditation operation.
	 *
	 * @param to the to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<AccreditationOperationTO> searchAccreditationOperation(AccreditationOperationTO to) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		List<AccreditationOperationTO> listAccreditationOperationTO = new ArrayList<>();

		sbQuery.append(" SELECT " );
		sbQuery.append("  AO.idAccreditationOperationPk, " ); 	//0
		sbQuery.append("  AO.registerDate, " );					//1
		sbQuery.append("  AO.expirationDate, " );				//2
		sbQuery.append("  AO.accreditationState, " );			//3
		sbQuery.append("  AO.petitionerType, " );				//4
		sbQuery.append("  CO.operationNumber, " );				//5
		sbQuery.append("  HA.idHolderAccountPk, " );			//6
		sbQuery.append("  HA.accountNumber, " );				//7
		sbQuery.append("  S.idSecurityCodePk " );				//8
		
		sbQuery.append(" FROM AccreditationOperation AO ");
		sbQuery.append("  Inner Join AO.accreditationDetails AD ");
		sbQuery.append("  Inner Join AO.custodyOperation CO ");
		sbQuery.append("  left  JOIN AO.participantPetitioner P " );
		sbQuery.append("  inner JOIN AD.holderAccount HA " );
		sbQuery.append("  inner JOIN AO.holder H " );
		sbQuery.append("  inner JOIN AD.security S " );
		sbQuery.append(" WHERE 1=1");
		
		if(Validations.validateIsNotNull(to.getIdAccreditationOperationPk())){
			sbQuery.append(" AND AO.idAccreditationOperationPk =:idAccreditationOperationPk");
			mapParam.put("idAccreditationOperationPk", to.getIdAccreditationOperationPk());
		}
		if(Validations.validateIsNotNull(to.getCustodyOperation())
				&& Validations.validateIsNotNull(to.getCustodyOperation().getOperationNumber())){
			sbQuery.append(" AND CO.operationNumber =:operationNumberAO");
			mapParam.put("operationNumberAO", to.getCustodyOperation().getOperationNumber());
		}
		if(Validations.validateIsNotNull(to.getCertificateNumber())){
			sbQuery.append(" AND AO.certificateNumber =:certificateNumber");
			mapParam.put("certificateNumber", to.getCertificateNumber());
		}
		if(Validations.validateIsNotNull(to.getPetitionerType())){
			sbQuery.append(" AND AO.petitionerType =:petitionerType");
			mapParam.put("petitionerType", to.getPetitionerType());
		}
		if(!to.isUserIssuerDPF() && Validations.validateIsNotNull(to.getParticipantPetitioner())
				&& Validations.validateIsNotNull(to.getParticipantPetitioner().getIdParticipantPk())){
			sbQuery.append(" AND P.idParticipantPk =:idParticipantPk");
			mapParam.put("idParticipantPk", to.getParticipantPetitioner().getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(to.getIdBlockEntity())){
			sbQuery.append(" AND EXISTS( ");
			sbQuery.append(" Select 1 ");
			sbQuery.append(" FROM BlockOperationCertification BOC ");
			sbQuery.append(" where BOC.accreditationDetail.accreditationOperation.idAccreditationOperationPk = AO.idAccreditationOperationPk ");
			sbQuery.append(" 	and BOC.blockOperation.blockRequest.blockEntity.idBlockEntityPk =:idBlockEntityPk ) ");
			mapParam.put("idBlockEntityPk", to.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNull(to.getIssuer())
				&& Validations.validateIsNotNull(to.getIssuer().getIdIssuerPk())){
			sbQuery.append(" AND S.issuer.idIssuerPk =:idIssuerPk");
			mapParam.put("idIssuerPk", to.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNull(to.getHolder())
				&& Validations.validateIsNotNull(to.getHolder().getIdHolderPk())){
			sbQuery.append(" AND H.idHolderPk =:idHolderPk");
			mapParam.put("idHolderPk", to.getHolder().getIdHolderPk());
		}
		if(Validations.validateIsNotNull(to.getSecurity())
				&& Validations.validateIsNotNull(to.getSecurity().getSecurityClass())){
			sbQuery.append(" AND S.securityClass =:securityClass");
			mapParam.put("securityClass", to.getSecurity().getSecurityClass());
		}
		if(Validations.validateIsNotNull(to.getSecurity())
				&& Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk())){
			sbQuery.append(" AND S.idSecurityCodePk =:idSecurityCodePk");
			mapParam.put("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(to.getDateTypeTO()) && to.getDateTypeTO().getCode().intValue()==UnblockAccreditationOperationDateType.REGISTRY_DATE.getCode()){
			if(Validations.validateIsNotNull(to.getInitialDate())){
				sbQuery.append(" AND trunc(AO.registerDate) >=:initialDate ");
				mapParam.put("initialDate", to.getInitialDate());
			}
			if(Validations.validateIsNotNull(to.getFinalDate())){
				sbQuery.append(" AND trunc(AO.registerDate) <=:finalDate ");
				mapParam.put("finalDate", to.getFinalDate());
			}
		}
		if(Validations.validateIsNotNull(to.getDateTypeTO()) && to.getDateTypeTO().getCode().intValue()==UnblockAccreditationOperationDateType.EXPIRATION_DATE.getCode()){
			if(Validations.validateIsNotNull(to.getInitialDate())){
				sbQuery.append(" AND trunc(AO.expirationDate) >=:initialDate ");
				mapParam.put("initialDate", to.getInitialDate());
			}
			if(Validations.validateIsNotNull(to.getFinalDate())){
				sbQuery.append(" AND trunc(AO.expirationDate) <=:finalDate ");
				mapParam.put("finalDate", to.getFinalDate());
			}
		}
		if(Validations.validateIsNotNull(to.getAccreditationState())){
			sbQuery.append(" AND AO.accreditationState =:accreditationState");
			mapParam.put("accreditationState", to.getAccreditationState());
		}
		
		if(to.getIndOperationType().equals(BooleanType.YES.getCode())){
			
			List<Integer> states = new ArrayList<>();
			states.add(UnblockAccreditationOperationStateType.REGISTERED.getCode());
			states.add(UnblockAccreditationOperationStateType.APPROVED.getCode());
			
			sbQuery.append(" AND CO.operationNumber NOT IN ( " );
			sbQuery.append(" 	SELECT CO_SQ.operationNumber " );
			
			sbQuery.append(" 	FROM UnblockAccreditationOperation UAO_SQ " );
			sbQuery.append(" 		inner JOIN UAO_SQ.custodyOperation UCO_SQ " );
			sbQuery.append(" 		inner JOIN UAO_SQ.accreditationOperation AO_SQ " );
			sbQuery.append(" 		inner JOIN AO_SQ.custodyOperation CO_SQ " );
			
			sbQuery.append(" 	WHERE 1=1 ");
			sbQuery.append(" 		AND UCO_SQ.state in (:states)");
			mapParam.put("states", states);
			
			sbQuery.append(" ) " );
		}
		
		if(to.isUserParticipantInvestor()){
			
			sbQuery.append(" AND EXISTS ( " );
			sbQuery.append(" 	SELECT 1 " );
			
			sbQuery.append(" 	FROM HolderAccountBalance HAB " );
			sbQuery.append(" 		inner JOIN HAB.participant PAR " );
			sbQuery.append(" 		inner JOIN HAB.holderAccount HA " );
			sbQuery.append(" 		inner JOIN HAB.security SEC " );
			sbQuery.append(" 		inner JOIN HA.holderAccountDetails HAD " );
			sbQuery.append(" 		inner JOIN HAD.holder H " );
			
			sbQuery.append(" 	WHERE 1=1 ");
			sbQuery.append(" 		AND SEC.idSecurityCodePk = S.idSecurityCodePk");
			sbQuery.append(" 		AND PAR.documentType = H.documentType");
			sbQuery.append(" 		AND PAR.documentNumber = H.documentNumber");
			
			sbQuery.append(" 		AND SEC.stateSecurity = :stateSecurity");
			sbQuery.append(" 		AND SEC.instrumentType = :instrumentType");
			sbQuery.append(" ) " );
			
			mapParam.put("stateSecurity", SecurityStateType.REGISTERED.getCode());
			mapParam.put("instrumentType", InstrumentType.FIXED_INCOME.getCode());
		}
		
		if(to.isUserIssuerDPF()){
			
			List<Integer> lstSecurityClass = new ArrayList<>();
			lstSecurityClass.add(SecurityClassType.DPF.getCode());
			lstSecurityClass.add(SecurityClassType.DPA.getCode());
			
			sbQuery.append(" AND S.securityClass in (:lstSecurityClass)");
			mapParam.put("lstSecurityClass", lstSecurityClass);
		}
		
		sbQuery.append(" ORDER BY CO.operationNumber DESC ");
		
		Query query = em.createQuery(sbQuery.toString());
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        
        List<Object> objectsList = ((List<Object>)query.getResultList());
        
        for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			AccreditationOperationTO toResult = new AccreditationOperationTO();
			toResult.setCustodyOperation(new CustodyOperation());
			toResult.setSecurity(new Security());
			
			toResult.setIdAccreditationOperationPk((Long)objects[0]);
			toResult.setRegisterDate((Date)objects[1]);
			toResult.setExpirationDate((Date)objects[2]);
			toResult.setAccreditationState((Integer)objects[3]);
			toResult.setPetitionerType((Integer)objects[4]);
			toResult.getCustodyOperation().setOperationNumber((Long)objects[5]);
			
			toResult.setIdHolderAccountPk((Long)objects[6]);
			toResult.setAccountNumber((Integer)objects[7]);
			//Get Cui - Description list
			toResult.setHolderDescription(searchHolderDescriptionByHolderAccount(toResult.getIdHolderAccountPk()));
			toResult.getSecurity().setIdSecurityCodePk((String)objects[8]);
			
			listAccreditationOperationTO.add(toResult);
		}
        
		return listAccreditationOperationTO;
	}
	
	/**
	 * Search holder description by holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public String searchHolderDescriptionByHolderAccount(Long idHolderAccountPk) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		List<HolderAccountDetail> listHolderAccountDetail = new ArrayList<>();
		String holderDescription = null;

		sbQuery.append(" SELECT H " );
		
		sbQuery.append("  FROM HolderAccountDetail HAD ");
		sbQuery.append("  	inner JOIN HAD.holder H " );
		sbQuery.append(" 	WHERE 1=1");
		
		if(Validations.validateIsNotNull(idHolderAccountPk)){
			sbQuery.append(" AND HAD.holderAccount.idHolderAccountPk =:idHolderAccountPk");
			mapParam.put("idHolderAccountPk", idHolderAccountPk);
		}
		
		Query query = em.createQuery(sbQuery.toString());
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        
        List<Holder> holders = (List<Holder>) query.getResultList();
        
        for (Holder holder : holders) {
        	HolderAccountDetail holderAccountDetail = new HolderAccountDetail();
        	holderAccountDetail.setHolder(holder);
        	listHolderAccountDetail.add(holderAccountDetail);
		}
        
        HolderAccount holderAccount = new HolderAccount();
    	holderAccount.setHolderAccountDetails(listHolderAccountDetail);
    	holderDescription = holderAccount.getAccountRntAndDescription();
    	
		return holderDescription;
	}

	/**
	 * Gets the holder account balance t os.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> getAccountBalanceTOsFromMixed(Long accreditationOperationId) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();
		List<AccreditationDetail> accreditationDetailList = accreditationCertificatesServiceBean.searchAccreditationDetailByOperation(accreditationOperationId);
		log.info("size accreditationDetailList: " + accreditationDetailList.size());
		
		for(AccreditationDetail acreditationDetail : accreditationDetailList){
			log.info("acreditationDetail.getAvailableBalance(): acreditationDetail.getAvailableBalance()");
			
			if(acreditationDetail.getAvailableBalance().compareTo(BigDecimal.ZERO) > 0){
				HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
				holderAccountBalanceTO.setIdHolderAccount(acreditationDetail.getHolderAccount().getIdHolderAccountPk());
				holderAccountBalanceTO.setIdParticipant(acreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk());
				holderAccountBalanceTO.setIdSecurityCode(acreditationDetail.getSecurity().getIdSecurityCodePk());
				holderAccountBalanceTO.setStockQuantity(acreditationDetail.getAvailableBalance());		
				holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
				
				for(AccreditationMarketFact accreditationMarketFact : acreditationDetail.getAccreditationMarketFactList()){
					MarketFactAccountTO mark = new MarketFactAccountTO();
					mark.setMarketDate(accreditationMarketFact.getMarketDate());
					mark.setMarketRate(accreditationMarketFact.getMarketRate());
					mark.setMarketPrice(accreditationMarketFact.getMarketPrice());
					mark.setQuantity(accreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mark);
				}			
				accountBalanceTOs.add(holderAccountBalanceTO);
			}			
		}				
		return accountBalanceTOs;
	}

	/**
	 * Execute accounts component.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @param businessProcessType the business process type
	 * @param parameterOperationType the parameter operation type
	 * @throws ServiceException the service exception
	 */
	public void executeAccountsComponent(Long accreditationOperationId, Long businessProcessType, Long parameterOperationType) throws ServiceException{
		List<HolderAccountBalanceTO> accountBalanceTOs = getHolderAccountBalanceTOs(accreditationOperationId);
		
		accountsComponentService.get().executeAccountsComponent(getAccountsComponentTO(accreditationOperationId,
																							   businessProcessType, 
																							   	 accountBalanceTOs,
																							parameterOperationType));
	}

	/**
	 * Gets the holder account balance t os.
	 *
	 * @param accreditationOperationId the accreditation operation id
	 * @return the holder account balance t os
	 * @throws ServiceException the service exception
	 */
	public List<HolderAccountBalanceTO> getHolderAccountBalanceTOs(Long accreditationOperationId) throws ServiceException{

		List<AccreditationDetail> accreditationDetailList = accreditationCertificatesServiceBean.searchAccreditationDetailByOperation(accreditationOperationId);	
		
		if(Validations.validateListIsNullOrEmpty(accreditationDetailList)){
			throw new ServiceException(ErrorServiceType.BALANCE_WITHOUT_MARKETFACT);
		}
		
		List<HolderAccountBalanceTO> accountBalanceTOs = new ArrayList<HolderAccountBalanceTO>();		
		for(AccreditationDetail acreditationDetail : accreditationDetailList){
			HolderAccountBalanceTO holderAccountBalanceTO = new HolderAccountBalanceTO();
			holderAccountBalanceTO.setIdHolderAccount(acreditationDetail.getHolderAccount().getIdHolderAccountPk());
			holderAccountBalanceTO.setIdParticipant(acreditationDetail.getHolderAccount().getParticipant().getIdParticipantPk());
			holderAccountBalanceTO.setIdSecurityCode(acreditationDetail.getSecurity().getIdSecurityCodePk());
			holderAccountBalanceTO.setStockQuantity(acreditationDetail.getTotalBalance());		
			holderAccountBalanceTO.setLstMarketFactAccounts(new ArrayList<MarketFactAccountTO>());
			for(AccreditationMarketFact accreditationMarketFact : acreditationDetail.getAccreditationMarketFactList()){
				if(accreditationMarketFact.getIndActive().equals(BooleanType.YES.getCode())){
					MarketFactAccountTO mark = new MarketFactAccountTO();
					mark.setMarketDate(accreditationMarketFact.getMarketDate());
					mark.setMarketRate(accreditationMarketFact.getMarketRate());
					mark.setMarketPrice(accreditationMarketFact.getMarketPrice());
					mark.setQuantity(accreditationMarketFact.getQuantityToAccredit());
					holderAccountBalanceTO.getLstMarketFactAccounts().add(mark);
				}
			}			
			accountBalanceTOs.add(holderAccountBalanceTO);
		}				
		return accountBalanceTOs;
	}

	/**
	 * Gets the accounts component to.
	 *
	 * @param idCustodyOperation the id custody operation
	 * @param businessProcessType the business process type
	 * @param accountBalanceTOs the account balance t os
	 * @param operationType the operation type
	 * @return the accounts component to
	 */
	public AccountsComponentTO getAccountsComponentTO(Long idCustodyOperation, Long businessProcessType, List<HolderAccountBalanceTO> accountBalanceTOs, Long operationType){

		AccountsComponentTO objAccountComponent = new AccountsComponentTO();
		objAccountComponent.setIdBusinessProcess(businessProcessType);
		objAccountComponent.setIdCustodyOperation(idCustodyOperation);
		objAccountComponent.setLstSourceAccounts(accountBalanceTOs);
		objAccountComponent.setIdOperationType(operationType);
		objAccountComponent.setIndMarketFact(1);
		return objAccountComponent;
	}

	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipant(Long participantCode) throws ServiceException{
		return find(Participant.class, participantCode);
	}

	/**
	 * Gets the list participant.
	 *
	 * @param participant the participant
	 * @return the list participant
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getListParticipant(Participant participant) throws ServiceException{
		return participantServiceBean.getLisParticipantServiceBean(participant);
	}
	
	/**
	 * Gets the issuer.
	 *
	 * @param issuerCode the issuer code
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuer(String issuerCode) throws ServiceException{
		return find(Issuer.class, issuerCode);
	}
	
	/**
	 * Gets the list parameter.
	 *
	 * @param filter the filter
	 * @return the list parameter
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameter(ParameterTableTO filter) throws ServiceException{
		return parameterServiceBean.getListParameterTableServiceBean(filter);
	}
	
	/**
	 * Gets the date type.
	 *
	 * @return the date type
	 * @throws ServiceException the service exception
	 */
	public List<DateTypeTO> getDateType() throws ServiceException {
		
		List<DateTypeTO> listDateType = new ArrayList<>();
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Select 1, 'FECHA DE REGISTRO' " );
		sbQuery.append(" FROM DUAL D1  " );
		sbQuery.append(" UNION  " );
		sbQuery.append(" Select 2, 'FECHA DE DESBLOQUEO' " );
		sbQuery.append(" FROM DUAL D2  " );
		
		Query query = em.createNativeQuery(sbQuery.toString());
		List<Object> listObject = (List<Object>)query.getResultList();
		
		for(int i = 0; i < listObject.size();++i){
			Object[] objects = (Object[]) listObject.get(i);
			DateTypeTO dateTypeTO = new DateTypeTO();
			dateTypeTO.setCode(new Long(objects[0].toString()));
			dateTypeTO.setDescription((String) objects[1]);
			listDateType.add(dateTypeTO);
		}
		
		return listDateType;
	}
	
	/**
	 * Search unblock accreditation operation.
	 * @param lstParamState 
	 * @param date 
	 * @param to the to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@SuppressWarnings("unchecked")
	public List<UnblockAccreditationOperationTO> getListRequestAccreditationByRevert(Date date, List<Integer> lstParamState) throws ServiceException{
		Map<String,Object> mapParam = new HashMap<String,Object>();
		StringBuilder sbQuery = new StringBuilder();
		List<UnblockAccreditationOperationTO> listUnblockAccreditationOperationTO = new ArrayList<>();
		
		sbQuery.append(" SELECT " );
		sbQuery.append("  UAO.idUnblockAccreditationPk, " ); 	//0
		sbQuery.append("  UAO.expirationDate, " );				//1
		sbQuery.append("  UCO.operationNumber, " );				//2
		sbQuery.append("  UCO.registryDate, " );				//3
		sbQuery.append("  UCO.state, " );						//4
		sbQuery.append("  AO.idAccreditationOperationPk, " );	//5
		sbQuery.append("  CO.operationNumber, " );				//6
		sbQuery.append("  HA.idHolderAccountPk, " );			//7
		sbQuery.append("  HA.accountNumber, " );				//8
		sbQuery.append("  S.idSecurityCodePk " );				//9
		
		sbQuery.append(" FROM UnblockAccreditationOperation UAO " );
		sbQuery.append(" 	inner JOIN UAO.custodyOperation UCO " );
		sbQuery.append(" 	inner JOIN UAO.accreditationOperation AO " );
		sbQuery.append("  	Inner Join AO.accreditationDetails AD ");
		sbQuery.append(" 	inner JOIN AO.custodyOperation CO " );
		sbQuery.append(" 	left  JOIN AO.participantPetitioner P " );
		sbQuery.append("  	inner JOIN AD.holderAccount HA " );
		sbQuery.append(" 	inner JOIN AO.holder H " );
		sbQuery.append(" 	inner JOIN AD.security S " );
		
		sbQuery.append(" WHERE 1=1 ");
		
		sbQuery.append(" AND UCO.state not in (:state)");
		mapParam.put("state", lstParamState);
		
		sbQuery.append(" ORDER BY UCO.operationNumber DESC ");
		
		Query query = em.createQuery(sbQuery.toString());
		@SuppressWarnings("rawtypes")
		Iterator it= mapParam.entrySet().iterator();
        while(it.hasNext()){
        	@SuppressWarnings("unchecked")
			Entry<String,Object> entry = (Entry<String,Object>)it.next();
        	query.setParameter(entry.getKey(), entry.getValue());
        }
        
        List<Object> objectsList = ((List<Object>)query.getResultList());
        
        for (int i = 0; i < objectsList.size(); ++i) {
			Object[] objects = (Object[]) objectsList.get(i);
			UnblockAccreditationOperationTO toResult = new UnblockAccreditationOperationTO();
			toResult.setCustodyOperation(new CustodyOperation());
			toResult.setAccreditationOperation(new AccreditationOperation());
			CustodyOperation custodyOperationAO = new CustodyOperation();
			toResult.setSecurity(new Security());
			
			toResult.setIdUnblockAccreditationPk((Long)objects[0]);
			toResult.setExpirationDate((Date)objects[1]);
			toResult.getCustodyOperation().setOperationNumber((Long)objects[2]);
			toResult.getCustodyOperation().setRegistryDate((Date)objects[3]);
			toResult.getCustodyOperation().setState((Integer)objects[4]);
			
			toResult.getAccreditationOperation().setIdAccreditationOperationPk((Long)objects[5]);
			custodyOperationAO.setOperationNumber((Long)objects[6]);
			toResult.getAccreditationOperation().setCustodyOperation(custodyOperationAO);
			
			toResult.setIdHolderAccountPk((Long)objects[7]);
			toResult.setAccountNumber((Integer)objects[8]);
			//Get Cui - Description list
			toResult.setHolderDescription(searchHolderDescriptionByHolderAccount(toResult.getIdHolderAccountPk()));
			toResult.getSecurity().setIdSecurityCodePk((String)objects[9]);
			
			listUnblockAccreditationOperationTO.add(toResult);
		}
        
		return listUnblockAccreditationOperationTO;
	}
	
	/**
	 * To revert request unblockAccreditationOperation.
	 *
	 * @param unblockAccreditationOperationTO
	 * @return 
	 * @throws ServiceException the service exception
	 */
	public void AnnulRejectUnblockAccreditationCertificate(UnblockAccreditationOperationTO unblockAccreditationOperationTO, LoggerUser loggerUser) throws ServiceException{
		UnblockAccreditationOperation unblockAccreditationOperation = find(UnblockAccreditationOperation.class, unblockAccreditationOperationTO.getIdUnblockAccreditationPk());
		
		CustodyOperation custodyOperation = unblockAccreditationOperation.getCustodyOperation();
		custodyOperation.setState(unblockAccreditationOperationTO.getCustodyOperation().getState());
		if(annulledStateType.equals(custodyOperation.getState())){
			custodyOperation.setAnnulationUser(loggerUser.getUserName());
			custodyOperation.setAnnulationDate(CommonsUtilities.currentDateTime());
		}else if(rejectedStateType.equals(custodyOperation.getState())){
			custodyOperation.setRejectUser(loggerUser.getUserName());
			custodyOperation.setRejectDate(CommonsUtilities.currentDateTime());
		}
		update(unblockAccreditationOperation);
	}
}
