package com.pradera.custody.unblockaccreditationcertificates.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.audit.ProcessAuditLogger;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.accreditationcertificates.to.DateTypeTO;
import com.pradera.custody.unblockaccreditationcertificates.service.UnblockAccreditationServiceBean;
import com.pradera.custody.unblockaccreditationcertificates.to.UnblockAccreditationOperationTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.UnblockAccreditationOperation;
import com.pradera.model.custody.type.UnblockAccreditationOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class UnblockAccreditationServiceFacade.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Feb 14, 2013
 */
@Stateless
@Interceptors(ContextHolderInterceptor.class) 
public class UnblockAccreditationServiceFacade{

	/** The bean service. */
	@Inject
	private UnblockAccreditationServiceBean unblockAccreditationServiceBean;
	
	/** The transaction registry. */
	@Resource
    TransactionSynchronizationRegistry transactionRegistry;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Register new Available Balance request.
	 *
	 * @param listAccreditationSelectedTO the list accreditation selected to
	 * @return String
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_REGISTER)
	public String Save(List<AccreditationOperationTO> listAccreditationSelectedTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeRegister());
		return unblockAccreditationServiceBean.Save(listAccreditationSelectedTO, loggerUser);
	}

	/**
	 * Find accreditation operation.
	 *
	 * @param idUnblockAccreditationPk the accreditation operation id
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public UnblockAccreditationOperation findUnblockAccreditationOperation(Long idUnblockAccreditationPk) throws ServiceException{
		return unblockAccreditationServiceBean.findUnblockAccreditationOperation(idUnblockAccreditationPk);
	}
	
	/**
	 * Search unblock accreditation operation.
	 *
	 * @param to the to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_QUERY)
	public List<UnblockAccreditationOperationTO> searchUnblockAccreditationOperation(UnblockAccreditationOperationTO to) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return unblockAccreditationServiceBean.searchUnblockAccreditationOperation(to);
	}
	
	/**
	 * Search accreditation operation.
	 *
	 * @param to the to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.ACCREDITATION_CERTIFICATE_QUERY)
	public List<AccreditationOperationTO> searchAccreditationOperation(AccreditationOperationTO to) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
        loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		return unblockAccreditationServiceBean.searchAccreditationOperation(to);
	}
	
	/**
	 * Approve unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_APPROVE)
	public String ApproveUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeApprove());
		return unblockAccreditationServiceBean.ApproveUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO,loggerUser);
	}
	
	/**
	 * Annul unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_ANNUL)
	public String AnnulUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
		return unblockAccreditationServiceBean.AnnulUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO,loggerUser);
	}

	/**
	 * Confirm unblok accreditation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	/* Confirm accreditation status.
	 *
	 * @param accreditationOperationToChange, accreditationState the accreditation operation to change
	 * @param accreditationState the accreditation state
	 * @throws Exception the exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_CONFIRM)
	public String ConfirmUnblokAccreditationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeConfirm());
		return unblockAccreditationServiceBean.ConfirmUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO,loggerUser);
	}
	
	/**
	 * Reject unblock accreditation operation request.
	 *
	 * @param listUnblockAccreditationSelectedTO the list unblock accreditation selected to
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_REJECT)
	public String RejectUnblockAccreditationOperationRequest(List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
		return unblockAccreditationServiceBean.RejectUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO,loggerUser);
	}
	
	/**
	 * Automatic unblock accreditation .
	 *
	 * @param finalDate the final date
	 * @throws ServiceException the service exception
	 */
	public void automaticAccreditationCertificateReversal(Date finalDate) throws ServiceException{		
		List <Integer> lstParamState = new ArrayList<>();
		lstParamState.add(UnblockAccreditationOperationStateType.CONFIRMED.getCode());
		lstParamState.add(UnblockAccreditationOperationStateType.ANNULLED.getCode());
		lstParamState.add(UnblockAccreditationOperationStateType.REJECTED.getCode());
		List<UnblockAccreditationOperationTO> lstResult = unblockAccreditationServiceBean.getListRequestAccreditationByRevert(finalDate, lstParamState);
		for (UnblockAccreditationOperationTO unblockAccreditationOperationTO : lstResult) {			
			if (unblockAccreditationOperationTO.getCustodyOperation().getState().equals(UnblockAccreditationOperationStateType.REGISTERED.getCode())) {	
				unblockAccreditationOperationTO.getCustodyOperation().setState(UnblockAccreditationOperationStateType.ANNULLED.getCode());
				AnnulUnblockAccreditationCertificate(unblockAccreditationOperationTO);
			}else{
				if (unblockAccreditationOperationTO.getCustodyOperation().getState().equals(UnblockAccreditationOperationStateType.APPROVED.getCode())) {
					unblockAccreditationOperationTO.getCustodyOperation().setState(UnblockAccreditationOperationStateType.REJECTED.getCode());
					RejectedUnblockAccreditationCertificate(unblockAccreditationOperationTO);
				} 
			}
		}	
	}
	
	/**
	 * Annul request UnblockAccreditationOperation.
	 *
	 * @param unblockAccreditationOperationTO the request unblockAccreditationOperation
	 * @return 
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_ANNUL)
	public void AnnulUnblockAccreditationCertificate(UnblockAccreditationOperationTO unblockAccreditationOperationTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeAnnular());
        unblockAccreditationServiceBean.AnnulRejectUnblockAccreditationCertificate(unblockAccreditationOperationTO,loggerUser);
	}
	
	/**
	 * Reject request UnblockAccreditationOperation.
	 *
	 * @param unblockAccreditationOperationTO the request unblockAccreditationOperation
	 * @return 
	 * @throws ServiceException the service exception
	 */
	@ProcessAuditLogger(process=BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_REJECT)
	public void RejectedUnblockAccreditationCertificate(UnblockAccreditationOperationTO unblockAccreditationOperationTO)throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeReject());
        unblockAccreditationServiceBean.AnnulRejectUnblockAccreditationCertificate(unblockAccreditationOperationTO,loggerUser);
	}
	
	/**
	 * Find accreditation operation.
	 *
	 * @param idAccreditationOperationPk the id accreditation operation pk
	 * @return the accreditation operation
	 * @throws ServiceException the service exception
	 */
	public AccreditationOperation findAccreditationOperation(Long idAccreditationOperationPk) throws ServiceException {
		return unblockAccreditationServiceBean.findAccreditationOperation(idAccreditationOperationPk);
	}
	
	/**
	 * Gets the participant.
	 *
	 * @param participantCode the participant code
	 * @return the participant
	 * @throws ServiceException the service exception
	 */
	public Participant getParticipant(Long participantCode) throws ServiceException{
		return unblockAccreditationServiceBean.getParticipant(participantCode);
	}

	/**
	 * Gets the issuer.
	 *
	 * @param issuerCode the issuer code
	 * @return the issuer
	 * @throws ServiceException the service exception
	 */
	public Issuer getIssuer(String issuerCode) throws ServiceException{
		return unblockAccreditationServiceBean.getIssuer(issuerCode);
	}
	
	/**
	 * Gets the list participant.
	 *
	 * @param participant the participant
	 * @return the list participant
	 * @throws ServiceException the service exception
	 */
	public List<Participant> getListParticipant(Participant participant) throws ServiceException{
		return unblockAccreditationServiceBean.getListParticipant(participant);
	}
	
	/**
	 * Gets the list parameter.
	 *
	 * @param filter the filter
	 * @return the list parameter
	 * @throws ServiceException the service exception
	 */
	public List<ParameterTable> getListParameter(ParameterTableTO filter) throws ServiceException{
		return unblockAccreditationServiceBean.getListParameter(filter);
	}
	
	/**
	 * Gets the date type.
	 *
	 * @return the date type
	 * @throws ServiceException the service exception
	 */
	public List<DateTypeTO> getDateType() throws ServiceException {
		return unblockAccreditationServiceBean.getDateType();
	}
	
	/**
	 * Unblock numbers to message.
	 *
	 * @param unblockNumber the unblock number
	 * @return the string
	 */
	public String unblockNumbersToMessage(List<Long> unblockNumber){
		return unblockAccreditationServiceBean.unblockNumbersToMessage(unblockNumber);
	}
	
}
