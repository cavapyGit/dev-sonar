package com.pradera.custody.unblockaccreditationcertificates.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ViewOperationsType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.accreditationcertificates.to.AccreditationOperationTO;
import com.pradera.custody.accreditationcertificates.to.DateTypeTO;
import com.pradera.custody.unblockaccreditationcertificates.facade.UnblockAccreditationServiceFacade;
import com.pradera.custody.unblockaccreditationcertificates.to.UnblockAccreditationOperationTO;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.UnblockAccreditationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.custody.type.AccreditationOperationStateType;
import com.pradera.model.custody.type.PetitionerType;
import com.pradera.model.custody.type.UnblockAccreditationOperationStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UnblockAccreditationMgmBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class UnblockAccreditationMgmBean extends GenericBaseBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The user privilege. */
	@Inject
	UserPrivilege userPrivilege;
	
	/** The user info. */
	@Inject
	UserInfo userInfo;

	/** The general pameters facade. */
	@Inject
	GeneralParametersFacade generalPametersFacade;
	
	/** The unblock accreditation service facade. */
	@EJB
	private UnblockAccreditationServiceFacade unblockAccreditationServiceFacade;
	
	/** The notification service facade. */
	@EJB
	NotificationServiceFacade notificationServiceFacade;
	
	/** The unblock accreditation operation to. */
	private UnblockAccreditationOperationTO unblockAccreditationOperationTO;
	
	/** The unblock accreditation operation. */
	private UnblockAccreditationOperation unblockAccreditationOperation;
	
	/** The accreditation operation to. */
	private AccreditationOperationTO accreditationOperationTO;
	
	/** The list unblock accreditation data model. */
	private GenericDataModel<UnblockAccreditationOperationTO> listUnblockAccreditationDataModel;
	
	/** The list unblock accreditation selected to. */
	private List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO;
	
	/** The list accreditation data model. */
	private GenericDataModel<AccreditationOperationTO> listAccreditationDataModel;
	
	/** The list accreditation selected to. */
	private List<AccreditationOperationTO> listAccreditationSelectedTO;
	
	/** The list accreditation data model detail. */
	private GenericDataModel<AccreditationOperationTO> listAccreditationDataModelDetail;

	/** The bl participant. */
	private boolean blParticipant;
	
	/** The bl issuer. */
	private boolean blIssuer;
	
	/** The creditor petitioner. */
	private Integer creditorPetitioner = PetitionerType.CREDITOR.getCode();
	
	/** The map security class parameters. */
	private HashMap<Integer, String> mapSecurityClassParameters = new HashMap<Integer,String>();
	
	/** The map petitioner type parameters. */
	private HashMap<Integer, String> mapPetitionerTypeParameters = new HashMap<Integer,String>();
	
	/** The Constant REGISTER_VIEW_MAPPING. */
	private static final String REGISTER_VIEW_MAPPING = "registerUnblockAccreditation";
	
	/** The Constant DETAIL_VIEW_MAPPING. */
	private static final String DETAIL_VIEW_MAPPING = "viewUnblockAccreditation";
	
	/** The Constant DETAIL_VIEW_ACCREDITATION_REG_MAPPING. */
	private static final String DETAIL_VIEW_ACCREDITATION_REG_MAPPING = "viewAccreditationDetailFromRegister";
	
	/** The Constant DETAIL_VIEW_ACCREDITATION_DET_MAPPING. */
	private static final String DETAIL_VIEW_ACCREDITATION_DET_MAPPING = "viewAccreditationDetailFromDetail";
//	private static final String SEARCH_VIEW_MAPPING = "searchUnblockAccreditation";
	
	/**
 * Inits the.
 */
@PostConstruct
	public void init(){
		showPrivilegeButtons();
		try {
			super.setParametersTableMap(new HashMap<Integer,Object>());
			unblockAccreditationOperation = new UnblockAccreditationOperation();
			cleanFilters();
			cleanResults();
			loadListParticipantSearch();
			getUserParticipantSearch();
			getUserIssuerSearch();
			loadParameters();
			loadListSecurityClassSearch();
			loadListUnblockAccreditationState();
			
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the user participant search.
	 *
	 * @return the user participant search
	 */
	public void getUserParticipantSearch(){
		try {
			Participant participant = new Participant();
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				participant = unblockAccreditationServiceFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				blParticipant = Boolean.TRUE;
				
			}else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && Validations.validateIsNotNull(userInfo.getUserAccountSession().getPartIssuerCode())){
				participant = unblockAccreditationServiceFacade.getParticipant(userInfo.getUserAccountSession().getPartIssuerCode());
				blParticipant = Boolean.TRUE;
			}
			unblockAccreditationOperationTO.setParticipantPetitioner(participant);
		
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Gets the user issuer search.
	 *
	 * @return the user issuer search
	 */
	public void getUserIssuerSearch(){
		try {
			if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				blIssuer = true;
				Issuer issuer = unblockAccreditationServiceFacade.getIssuer(userInfo.getUserAccountSession().getIssuerCode());
				unblockAccreditationOperationTO.setIssuer(issuer);
			}
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Gets the user participant register.
	 *
	 * @return the user participant register
	 */
	public void getUserParticipantRegister(){
		try {
			Participant participant = new Participant();
			
			if(userInfo.getUserAccountSession().isParticipantInstitucion() || userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				participant = unblockAccreditationServiceFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
				accreditationOperationTO.setPetitionerType(PetitionerType.PARTICIPANT.getCode());
				blParticipant = Boolean.TRUE;
				
			}else if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && Validations.validateIsNotNull(userInfo.getUserAccountSession().getPartIssuerCode())){
				participant = unblockAccreditationServiceFacade.getParticipant(userInfo.getUserAccountSession().getPartIssuerCode());
				blParticipant = Boolean.TRUE;
			}
			accreditationOperationTO.setParticipantPetitioner(participant);
			
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Gets the user issuer register.
	 *
	 * @return the user issuer register
	 */
	public void getUserIssuerRegister(){
		try {
			if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				blIssuer = true;
				Issuer issuer = unblockAccreditationServiceFacade.getIssuer(userInfo.getUserAccountSession().getIssuerCode());
				accreditationOperationTO.setIssuer(issuer);
				accreditationOperationTO.setPetitionerType(PetitionerType.ISSUER.getCode());
			}
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Load list participant search.
	 */
	public void loadListParticipantSearch(){
		try {
			Participant participant = new Participant();
			participant.setState(ParticipantStateType.REGISTERED.getCode());
			List<Participant> listParticipant = unblockAccreditationServiceFacade.getListParticipant(participant);
			unblockAccreditationOperationTO.setListParticipantPetitioner(listParticipant);
			
			
			
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Load list participant register.
	 */
	public void loadListParticipantRegister(){
		try {
			Participant participant = new Participant();
			participant.setState(ParticipantStateType.REGISTERED.getCode());
			List<Participant> listParticipant = unblockAccreditationServiceFacade.getListParticipant(participant);
			accreditationOperationTO.setListParticipantPetitioner(listParticipant);
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Load list petitioner type.
	 */
	public void loadListPetitionerType(){
		try {
			Integer masterTable = MasterTableType.PETITIONER_TYPE.getCode();
			ParameterTableTO to = new ParameterTableTO();
			to.setMasterTableFk(masterTable);
			accreditationOperationTO.setListPetitionerType(unblockAccreditationServiceFacade.getListParameter(to));
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Load list date type.
	 */
	public void loadListDateType(){
		try {
			List<DateTypeTO> list = unblockAccreditationServiceFacade.getDateType();
			accreditationOperationTO.setListDateType(list);
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Load list unblock accreditation state.
	 */
	public void loadListUnblockAccreditationState(){
		try {
			Integer masterTable = MasterTableType.UNBLOCK_ACCREDITATION_CERTIFICATE_STATE.getCode();
			ParameterTableTO to = new ParameterTableTO();
			to.setMasterTableFk(masterTable);
			unblockAccreditationOperationTO.setListUnblockAccreditationState(unblockAccreditationServiceFacade.getListParameter(to));
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}

	/**
	 * Load list security class search.
	 */
	public void loadListSecurityClassSearch(){
		try {
			Integer masterTable = MasterTableType.SECURITIES_CLASS.getCode();
			ParameterTableTO to = new ParameterTableTO();
			List<Integer> lstParameterTablePk = new ArrayList<>();
			
			to.setMasterTableFk(masterTable);
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				lstParameterTablePk.add(SecurityClassType.DPA.getCode());
				lstParameterTablePk.add(SecurityClassType.DPF.getCode());
				to.setLstParameterTablePk(lstParameterTablePk);
			}
			
			unblockAccreditationOperationTO.setListSecurityClass(unblockAccreditationServiceFacade.getListParameter(to));
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Load list security class register.
	 */
	public void loadListSecurityClassRegister(){
		try {
			Integer masterTable = MasterTableType.SECURITIES_CLASS.getCode();
			ParameterTableTO to = new ParameterTableTO();
			List<Integer> lstParameterTablePk = new ArrayList<>();
			
			to.setMasterTableFk(masterTable);
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				lstParameterTablePk.add(SecurityClassType.DPA.getCode());
				lstParameterTablePk.add(SecurityClassType.DPF.getCode());
				to.setLstParameterTablePk(lstParameterTablePk);
			}
			
			accreditationOperationTO.setListSecurityClass(unblockAccreditationServiceFacade.getListParameter(to));
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
	}
	
	/**
	 * Change participant search.
	 */
	public void changeParticipantSearch(){
		unblockAccreditationOperationTO.setHolder(new Holder());
		cleanResults();
	}

	/**
	 * Clean.
	 */
	public void clean(){
		try {
			unblockAccreditationOperation = new UnblockAccreditationOperation();
			cleanFilters();
			cleanResults();
			loadListParticipantSearch();
			getUserParticipantSearch();
			getUserIssuerSearch();
			loadParameters();
			loadListSecurityClassSearch();
			loadListUnblockAccreditationState();
			
		} catch (ServiceException e) {
			e.printStackTrace();
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Clean results.
	 */
	public void cleanResults(){
		listUnblockAccreditationSelectedTO = null;
		listUnblockAccreditationDataModel = null;
	}
	
	/**
	 * Clean filters.
	 */
	public void cleanFilters(){
		unblockAccreditationOperationTO = new UnblockAccreditationOperationTO();
		unblockAccreditationOperationTO.setInitialDate(CommonsUtilities.currentDate());
		unblockAccreditationOperationTO.setFinalDate(CommonsUtilities.currentDate());
		unblockAccreditationOperationTO.setCustodyOperation(new CustodyOperation());
		unblockAccreditationOperationTO.setAccreditationOperation(new AccreditationOperation());
		unblockAccreditationOperationTO.getAccreditationOperation().setCustodyOperation(new CustodyOperation());
		unblockAccreditationOperationTO.setParticipantPetitioner(new Participant());
		unblockAccreditationOperationTO.setHolder(new Holder());
		unblockAccreditationOperationTO.setIssuer(new Issuer());
		unblockAccreditationOperationTO.setSecurity(new Security());
	}
	
	/**
	 * Change participant register.
	 */
	public void changeParticipantRegister(){
		accreditationOperationTO.setHolder(new Holder());
		cleanResultsRegister();
	}

	/**
	 * Clean results register.
	 */
	public void cleanResultsRegister(){
		listAccreditationSelectedTO = null;
		listAccreditationDataModel = null;
	}
	
	/**
	 * Clean filters register.
	 */
	public void cleanFiltersRegister(){
		accreditationOperationTO = new AccreditationOperationTO();
		accreditationOperationTO.setInitialDate(CommonsUtilities.currentDate());
		accreditationOperationTO.setFinalDate(CommonsUtilities.currentDate());
		accreditationOperationTO.setCustodyOperation(new CustodyOperation());
		accreditationOperationTO.setPetitionerType(null);
		accreditationOperationTO.setParticipantPetitioner(new Participant());
		accreditationOperationTO.setHolder(new Holder());
		accreditationOperationTO.setIssuer(new Issuer());
		accreditationOperationTO.setSecurity(new Security());
		accreditationOperationTO.setDateTypeTO(new DateTypeTO());
	}

	/**
	 * On change petitioner for search.
	 */
	public void onChangePetitionerForSearch(){
		accreditationOperationTO.setParticipantPetitioner(new Participant());
		accreditationOperationTO.setHolder(new Holder());
		accreditationOperationTO.setIssuer(new Issuer());
		cleanResultsRegister();
	}
	
	/**
	 * Search request unblock accreditation.
	 */
	@LoggerAuditWeb
	public void SearchRequestUnblockAccreditation() {
		try {
			
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				unblockAccreditationOperationTO.setUserParticipantInvestor(Boolean.TRUE);				
			}
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				unblockAccreditationOperationTO.setUserIssuerDPF(Boolean.TRUE);
			}
			
			List<UnblockAccreditationOperationTO> listTO = unblockAccreditationServiceFacade.searchUnblockAccreditationOperation(unblockAccreditationOperationTO);
			listUnblockAccreditationDataModel = new GenericDataModel<>(listTO);
			listUnblockAccreditationSelectedTO = null;
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Search request accreditation.
	 */
	@LoggerAuditWeb
	public void SearchRequestAccreditation() {
		try {
			
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){
				accreditationOperationTO.setUserParticipantInvestor(Boolean.TRUE);
			}
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
				accreditationOperationTO.setUserIssuerDPF(Boolean.TRUE);
			}
			
			accreditationOperationTO.setIndOperationType(BooleanType.YES.getCode());
			accreditationOperationTO.setAccreditationState(AccreditationOperationStateType.CONFIRMED.getCode());
			List<AccreditationOperationTO> listTO = unblockAccreditationServiceFacade.searchAccreditationOperation(accreditationOperationTO);
			listAccreditationDataModel = new GenericDataModel<AccreditationOperationTO>(listTO);
			listAccreditationSelectedTO = null;
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	/**
	 * Search request accreditation by unblock request.
	 */
	@LoggerAuditWeb
	public void SearchRequestAccreditationByUnblockRequest() {
		try {
			AccreditationOperationTO to = new AccreditationOperationTO();
			if(unblockAccreditationOperation != null
					&& unblockAccreditationOperation.getAccreditationOperation() != null
					&& unblockAccreditationOperation.getAccreditationOperation().getIdAccreditationOperationPk() != null){	
				to.setIdAccreditationOperationPk(unblockAccreditationOperation.getAccreditationOperation().getIdAccreditationOperationPk());
			}
			
			to.setIndOperationType(BooleanType.NO.getCode());
			List<AccreditationOperationTO> listTO = unblockAccreditationServiceFacade.searchAccreditationOperation(to);
			listAccreditationDataModelDetail = new GenericDataModel<AccreditationOperationTO>(listTO);
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}

	/**
	 * Validate search issuer.
	 */
	public void validateSearchIssuer() {
		if ( !(Validations.validateIsNotNull(unblockAccreditationOperationTO.getIssuer()) 
				&& Validations.validateIsNotNull(unblockAccreditationOperationTO.getIssuer().getIdIssuerPk())) ) {
			return;
		}
		String issuerSearchPK = unblockAccreditationOperationTO.getIssuer().getIdIssuerPk();
		Issuer registerIssuer = null;
		try {
			registerIssuer = unblockAccreditationServiceFacade.getIssuer(issuerSearchPK);
		} catch (ServiceException se) {
			excepcion.fire(new ExceptionToCatchEvent(se));
		}
		if (registerIssuer != null && IssuerStateType.BLOCKED.getCode().equals(registerIssuer.getStateIssuer())) {
			String bodyMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ISSUER_BLOCKED);
			String headerMessage = PropertiesUtilities.getMessage(PropertiesConstants.MSG_ERROR_TITLE);
			alert(headerMessage, bodyMessage);
			unblockAccreditationOperationTO.setIssuer(new Issuer());
		}
		cleanFilters();
		cleanResults();
	}
	
	/**
	 * Alert.
	 *
	 * @param headerMessage the header message
	 * @param bodyMessage the body message
	 */
	public void alert(String headerMessage, String bodyMessage) {
		showMessageOnDialog(headerMessage, bodyMessage);
		JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	}
	
	/**
	 * Load parameters.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParameters() throws ServiceException{
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setMasterTableFk(MasterTableType.UNBLOCK_ACCREDITATION_CERTIFICATE_STATE.getCode());
		
		List<ParameterTable> list = generalPametersFacade.getListParameterTableServiceBean(parameterFilter);
		for (ParameterTable parameterTable : list){
			mapSecurityClassParameters.put(parameterTable.getParameterTablePk(), parameterTable.getDescription());
		}
	}
	
	/**
	 * Load parameters register.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadParametersRegister() throws ServiceException{
		ParameterTableTO parameterFilter = new ParameterTableTO();
		parameterFilter.setMasterTableFk(MasterTableType.PETITIONER_TYPE.getCode());
		
		List<ParameterTable> list = generalPametersFacade.getListParameterTableServiceBean(parameterFilter);
		for (ParameterTable parameterTable : list){
			mapPetitionerTypeParameters.put(parameterTable.getParameterTablePk(), parameterTable.getParameterName());
		}
		
	}
	
	/**
	 * Show privilege buttons.
	 */
	private void showPrivilegeButtons(){
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnApproveView(true);
		privilegeComponent.setBtnAnnularView(true);
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	/**
	 * New request.
	 *
	 * @return the string
	 */
	public String newRequest(){
		try {
			unblockAccreditationOperation = new UnblockAccreditationOperation();
			cleanFiltersRegister();
			cleanResultsRegister();
			getUserParticipantRegister();
			getUserIssuerRegister();
			loadListPetitionerType();
			loadListParticipantRegister();
			loadParametersRegister();
			loadListSecurityClassRegister();
			loadListDateType();
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		
		return REGISTER_VIEW_MAPPING;
	}
	
	/**
	 * Before save.
	 */
	public void beforeSave() {
		
		if (Validations.validateListIsNullOrEmpty(listAccreditationSelectedTO)) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
						PropertiesUtilities.getMessage(PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REGISTER_ASK));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
	}
	
	/**
	 * Save request.
	 */
	@LoggerAuditWeb
	public void saveRequest() {
		try {
			String unblockNumbersMessage = null;
			Object[] param= new Object[1];
			
			//save the unblock accreditation operation
			unblockNumbersMessage = unblockAccreditationServiceFacade.Save(listAccreditationSelectedTO);
			param[0] = unblockNumbersMessage;
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
								PropertiesUtilities.getMessage(PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REGISTER_SUCCESS,param));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			
			// INICIO ENVIO DE NOTIFICACIONES
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_REGISTER.getCode());
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, param);
			// FIN ENVIO DE NOTIFICACIONES
			
		} catch (ServiceException e) {
			if(e.getMessage()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
									PropertiesUtilities.getExceptionMessage(e.getMessage(), e.getParams()));
				JSFUtilities.showSimpleValidationDialog();
			}else{
				excepcion.fire(new ExceptionToCatchEvent(e));
			}
		}
	}
	
	/**
	 * Clean registration.
	 */
	public void cleanRegistration() {
		JSFUtilities.resetViewRoot();
		newRequest();
	}
	
	/**
	 * Load unblock accreditation operation.
	 *
	 * @param idUnblockAccreditationPk the id unblock accreditation pk
	 * @return the string
	 */
	@LoggerAuditWeb
	public String loadUnblockAccreditationOperation(Long idUnblockAccreditationPk) {
		try {
			unblockAccreditationOperation = unblockAccreditationServiceFacade.findUnblockAccreditationOperation(idUnblockAccreditationPk);
			//Cargar el detalle de la solicitud de accreditation del titular
			SearchRequestAccreditationByUnblockRequest();
			
			setViewOperationType(ViewOperationsType.DETAIL.getCode());
			loadParameters();
			loadParametersRegister();
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
		return DETAIL_VIEW_MAPPING;
	}

	/**
	 * Load accreditation operation.
	 *
	 * @param to the to
	 * @return the string
	 */
	public String loadAccreditationOperation(UnblockAccreditationOperationTO to) {
//		try {
//			unblockAccreditationOperation = unblockAccreditationServiceFacade.findAccreditationOperation(to.getIdUnblockAccreditationPk());
//			//Cargar el detalle de la solicitud de accreditation del titular
//			
//			setViewOperationType(ViewOperationsType.DETAIL.getCode());
//		} catch (Exception e) {
//			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//								, PropertiesUtilities.getExceptionMessage(e.getMessage()));
//			JSFUtilities.showSimpleValidationDialog();
//		}
		return DETAIL_VIEW_ACCREDITATION_REG_MAPPING;
	}
	
	/**
	 * Validate approve.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateApprove() {
		return validateAction(ViewOperationsType.APPROVE.getCode(), PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_APPROVE_ASK);
	}
	
	/**
	 * Validate annul.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateAnnul() {
		return validateAction(ViewOperationsType.ANULATE.getCode(), PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_ANNUL_ASK);
	}

	/**
	 * Validate reject.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateReject() {
		return validateAction(ViewOperationsType.REJECT.getCode(), PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REJECT_ASK);
	}
	
	/**
	 * Validate confirm.
	 *
	 * @return the string
	 */
	@LoggerAuditWeb
	public String validateConfirm() {
		return validateAction(ViewOperationsType.CONFIRM.getCode(), PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_CONFIRM_ASK);
	}
	
	/**
	 * Validate action.
	 *
	 * @param action the action
	 * @param strMessage the str message
	 * @return the string
	 */
	public String validateAction(Integer action, String strMessage) {
		if (Validations.validateListIsNotNullAndNotEmpty(listUnblockAccreditationSelectedTO)) {
			
			String strUnblockNumbers = GeneralConstants.EMPTY_STRING;
			Integer state = null;
			List<Long> unblockNumbersCorrectState = new ArrayList<>();
			List<Long> unblockNumbersIncorrectState = new ArrayList<>();
			Boolean isIncorrectState = Boolean.FALSE;
			String message = null;
			Object[] param = new Object[1];
			
			if(ViewOperationsType.APPROVE.getCode() == action || ViewOperationsType.ANULATE.getCode() == action){
				state = UnblockAccreditationOperationStateType.REGISTERED.getCode();
				message = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_REGISTERED;
			}else if(ViewOperationsType.CONFIRM.getCode() == action || ViewOperationsType.REJECT.getCode() == action){
				state = UnblockAccreditationOperationStateType.APPROVED.getCode();
				message = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REQUEST_NO_APPROVED;
			}
			
			for(UnblockAccreditationOperationTO to: listUnblockAccreditationSelectedTO) {
				if (!state.equals(to.getCustodyOperation().getState())) {
					isIncorrectState = Boolean.TRUE;
					unblockNumbersIncorrectState.add(to.getCustodyOperation().getOperationNumber());
				}else {
					unblockNumbersCorrectState.add(to.getCustodyOperation().getOperationNumber());					
				}
			}
			
			//Validate if state is correct to continue operation
			if(isIncorrectState){
				//get list of request number with incorrect state for message
				strUnblockNumbers = unblockAccreditationServiceFacade.unblockNumbersToMessage(unblockNumbersIncorrectState);
				param[0] = strUnblockNumbers;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
									PropertiesUtilities.getMessage(message, param));
				JSFUtilities.showSimpleValidationDialog();
				return null;
			}
			
			if (listUnblockAccreditationSelectedTO.size() == GeneralConstants.ONE_VALUE_INTEGER.intValue()) {
				loadUnblockAccreditationOperation(listUnblockAccreditationSelectedTO.get(0).getIdUnblockAccreditationPk());
				setViewOperationType(action);
				return DETAIL_VIEW_MAPPING;
			} else {
				//get list of request number with correct state for message
				strUnblockNumbers = unblockAccreditationServiceFacade.unblockNumbersToMessage(unblockNumbersCorrectState);
				param[0] = strUnblockNumbers;
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_CONFIRM), 
									PropertiesUtilities.getMessage(strMessage, param));
				JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
			}
			setViewOperationType(action);
		} else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
								PropertiesUtilities.getMessage(PropertiesConstants.ERROR_RECORD_NOT_SELECTED));
			JSFUtilities.showSimpleValidationDialog();
		}
		return null;
	}
	
	/**
	 * Before action.
	 */
	public void beforeAction(){
		String headerMessage = null;
		String bodyMessage = null;
		if(isViewOperationApprove()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_APPROVE;
			bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_APPROVE_ASK;
		}else if(isViewOperationAnnul()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_ANNUL;
			bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_ANNUL_ASK;
		}else if(isViewOperationReject()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_REJECT;
			bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REJECT_ASK;
		}else if(isViewOperationConfirm()){
			headerMessage = GeneralConstants.LBL_HEADER_ALERT_CONFIRM;
			bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_CONFIRM_ASK;
		}
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(headerMessage), PropertiesUtilities.getMessage(
				bodyMessage,
				new Object[]{
						unblockAccreditationOperation.getCustodyOperation().getOperationNumber().toString()
						}));
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show()");
	}
	
	/**
	 * Do action.
	 */
	@LoggerAuditWeb
	public void doAction(){
		try {
			String bodyMessage= null;
			Object[] param = new Object[1];
			BusinessProcess businessProcess = new BusinessProcess();
			
			if(isViewOperationAnnul()){
				param[0] = unblockAccreditationServiceFacade.AnnulUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO);
				bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_ANNUL_SUCCESS;
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_ANNUL.getCode());
			} else if(isViewOperationApprove()){
				param[0] = unblockAccreditationServiceFacade.ApproveUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO);
				bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_APPROVE_SUCCESS;
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_APPROVE.getCode());
			} else if(isViewOperationReject()){
				param[0] = unblockAccreditationServiceFacade.RejectUnblockAccreditationOperationRequest(listUnblockAccreditationSelectedTO);
				bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_REJECT_SUCCESS;
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_REJECT.getCode());
			} else if(isViewOperationConfirm()){
				param[0] = unblockAccreditationServiceFacade.ConfirmUnblokAccreditationRequest(listUnblockAccreditationSelectedTO);
				bodyMessage = PropertiesConstants.MSG_UNBLOCK_ACCREDITATION_CONFIRM_SUCCESS;
				businessProcess.setIdBusinessProcessPk(BusinessProcessType.UNBLOCK_ACCREDITATION_CERTIFICATE_CONFIRM.getCode());
			}
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
																	PropertiesUtilities.getMessage(bodyMessage, param));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			
			// INICIO ENVIO DE NOTIFICACIONES
			notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess, null, param);
			// FIN ENVIO DE NOTIFICACIONES
			
			SearchRequestUnblockAccreditation();
			unblockAccreditationOperation= null;
			
//		} catch (Exception e) {
//			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
//												, PropertiesUtilities.getExceptionMessage(e.getMessage()));
//			JSFUtilities.showSimpleValidationDialog();
//		}
		
		} catch (ServiceException e) {
			if(e.getErrorService()!=null){
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
									PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage()));
				JSFUtilities.showSimpleValidationDialog();
			}
		}
	}
	
	
	/**
	 * Gets the unblock accreditation operation to.
	 *
	 * @return the unblock accreditation operation to
	 */
	public UnblockAccreditationOperationTO getUnblockAccreditationOperationTO() {
		return unblockAccreditationOperationTO;
	}

	/**
	 * Sets the unblock accreditation operation to.
	 *
	 * @param unblockAccreditationOperationTO the new unblock accreditation operation to
	 */
	public void setUnblockAccreditationOperationTO(
			UnblockAccreditationOperationTO unblockAccreditationOperationTO) {
		this.unblockAccreditationOperationTO = unblockAccreditationOperationTO;
	}
	
	/**
	 * Gets the user info.
	 *
	 * @return the user info
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * Sets the user info.
	 *
	 * @param userInfo the new user info
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Checks if is bl participant.
	 *
	 * @return true, if is bl participant
	 */
	public boolean isBlParticipant() {
		return blParticipant;
	}

	/**
	 * Sets the bl participant.
	 *
	 * @param blParticipant the new bl participant
	 */
	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}

	/**
	 * Checks if is bl issuer.
	 *
	 * @return true, if is bl issuer
	 */
	public boolean isBlIssuer() {
		return blIssuer;
	}

	/**
	 * Sets the bl issuer.
	 *
	 * @param blIssuer the new bl issuer
	 */
	public void setBlIssuer(boolean blIssuer) {
		this.blIssuer = blIssuer;
	}

	/**
	 * Gets the unblock accreditation operation.
	 *
	 * @return the unblock accreditation operation
	 */
	public UnblockAccreditationOperation getUnblockAccreditationOperation() {
		return unblockAccreditationOperation;
	}

	/**
	 * Sets the unblock accreditation operation.
	 *
	 * @param unblockAccreditationOperation the new unblock accreditation operation
	 */
	public void setUnblockAccreditationOperation(
			UnblockAccreditationOperation unblockAccreditationOperation) {
		this.unblockAccreditationOperation = unblockAccreditationOperation;
	}

	/**
	 * Gets the list unblock accreditation data model.
	 *
	 * @return the list unblock accreditation data model
	 */
	public GenericDataModel<UnblockAccreditationOperationTO> getListUnblockAccreditationDataModel() {
		return listUnblockAccreditationDataModel;
	}

	/**
	 * Sets the list unblock accreditation data model.
	 *
	 * @param listUnblockAccreditationDataModel the new list unblock accreditation data model
	 */
	public void setListUnblockAccreditationDataModel(
			GenericDataModel<UnblockAccreditationOperationTO> listUnblockAccreditationDataModel) {
		this.listUnblockAccreditationDataModel = listUnblockAccreditationDataModel;
	}

	/**
	 * Gets the list unblock accreditation selected to.
	 *
	 * @return the list unblock accreditation selected to
	 */
	public List<UnblockAccreditationOperationTO> getListUnblockAccreditationSelectedTO() {
		return listUnblockAccreditationSelectedTO;
	}

	/**
	 * Sets the list unblock accreditation selected to.
	 *
	 * @param listUnblockAccreditationSelectedTO the new list unblock accreditation selected to
	 */
	public void setListUnblockAccreditationSelectedTO(
			List<UnblockAccreditationOperationTO> listUnblockAccreditationSelectedTO) {
		this.listUnblockAccreditationSelectedTO = listUnblockAccreditationSelectedTO;
	}

	/**
	 * Gets the unblock accreditation service facade.
	 *
	 * @return the unblock accreditation service facade
	 */
	public UnblockAccreditationServiceFacade getUnblockAccreditationServiceFacade() {
		return unblockAccreditationServiceFacade;
	}

	/**
	 * Sets the unblock accreditation service facade.
	 *
	 * @param unblockAccreditationServiceFacade the new unblock accreditation service facade
	 */
	public void setUnblockAccreditationServiceFacade(
			UnblockAccreditationServiceFacade unblockAccreditationServiceFacade) {
		this.unblockAccreditationServiceFacade = unblockAccreditationServiceFacade;
	}

	/**
	 * Gets the accreditation operation to.
	 *
	 * @return the accreditation operation to
	 */
	public AccreditationOperationTO getAccreditationOperationTO() {
		return accreditationOperationTO;
	}

	/**
	 * Sets the accreditation operation to.
	 *
	 * @param accreditationOperationTO the new accreditation operation to
	 */
	public void setAccreditationOperationTO(
			AccreditationOperationTO accreditationOperationTO) {
		this.accreditationOperationTO = accreditationOperationTO;
	}

	/**
	 * Gets the list accreditation data model.
	 *
	 * @return the list accreditation data model
	 */
	public GenericDataModel<AccreditationOperationTO> getListAccreditationDataModel() {
		return listAccreditationDataModel;
	}

	/**
	 * Sets the list accreditation data model.
	 *
	 * @param listAccreditationDataModel the new list accreditation data model
	 */
	public void setListAccreditationDataModel(
			GenericDataModel<AccreditationOperationTO> listAccreditationDataModel) {
		this.listAccreditationDataModel = listAccreditationDataModel;
	}

	/**
	 * Gets the list accreditation selected to.
	 *
	 * @return the list accreditation selected to
	 */
	public List<AccreditationOperationTO> getListAccreditationSelectedTO() {
		return listAccreditationSelectedTO;
	}

	/**
	 * Sets the list accreditation selected to.
	 *
	 * @param listAccreditationSelectedTO the new list accreditation selected to
	 */
	public void setListAccreditationSelectedTO(
			List<AccreditationOperationTO> listAccreditationSelectedTO) {
		this.listAccreditationSelectedTO = listAccreditationSelectedTO;
	}
	
	/**
	 * Gets the map security class parameters.
	 *
	 * @return the map security class parameters
	 */
	public HashMap<Integer, String> getMapSecurityClassParameters() {
		return mapSecurityClassParameters;
	}

	/**
	 * Sets the map security class parameters.
	 *
	 * @param mapSecurityClassParameters the map security class parameters
	 */
	public void setMapSecurityClassParameters(
			HashMap<Integer, String> mapSecurityClassParameters) {
		this.mapSecurityClassParameters = mapSecurityClassParameters;
	}

	/**
	 * Gets the map petitioner type parameters.
	 *
	 * @return the map petitioner type parameters
	 */
	public HashMap<Integer, String> getMapPetitionerTypeParameters() {
		return mapPetitionerTypeParameters;
	}

	/**
	 * Sets the map petitioner type parameters.
	 *
	 * @param mapPetitionerTypeParameters the map petitioner type parameters
	 */
	public void setMapPetitionerTypeParameters(
			HashMap<Integer, String> mapPetitionerTypeParameters) {
		this.mapPetitionerTypeParameters = mapPetitionerTypeParameters;
	}

	/**
	 * Gets the creditor petitioner.
	 *
	 * @return the creditor petitioner
	 */
	public Integer getCreditorPetitioner() {
		return creditorPetitioner;
	}

	/**
	 * Sets the creditor petitioner.
	 *
	 * @param creditorPetitioner the new creditor petitioner
	 */
	public void setCreditorPetitioner(Integer creditorPetitioner) {
		this.creditorPetitioner = creditorPetitioner;
	}

	/**
	 * Gets the list accreditation data model detail.
	 *
	 * @return the list accreditation data model detail
	 */
	public GenericDataModel<AccreditationOperationTO> getListAccreditationDataModelDetail() {
		return listAccreditationDataModelDetail;
	}

	/**
	 * Sets the list accreditation data model detail.
	 *
	 * @param listAccreditationDataModelDetail the new list accreditation data model detail
	 */
	public void setListAccreditationDataModelDetail(
			GenericDataModel<AccreditationOperationTO> listAccreditationDataModelDetail) {
		this.listAccreditationDataModelDetail = listAccreditationDataModelDetail;
	}
	
}
