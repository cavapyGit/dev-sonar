package com.pradera.custody.unblockaccreditationcertificates.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.transfersecurities.CustodyOperation;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class UnblockAccreditationOperationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
public class UnblockAccreditationOperationTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id unblock accreditation pk. */
	private Long idUnblockAccreditationPk;
	
	/** The custody operation. */
	private CustodyOperation custodyOperation;

	/** The accreditation operation. */
	private AccreditationOperation accreditationOperation;
	
    /** The expiration date. */
	private Date expirationDate;
    
	/** The last modify app. */
	private Integer lastModifyApp;

    /** The last modify date. */
	private Date lastModifyDate;

	/** The last modify ip. */
	private String lastModifyIp;

	/** The last modify user. */
	private String lastModifyUser;

    /** The indicator automatic. */
    private Integer indAutomatic;
    
    /** The is selected. */
	private Boolean isAutomatic;

	/** The participant petitioner. */
	private Participant participantPetitioner;
	
	/** The list participant petitioner. */
	private List<Participant> listParticipantPetitioner;
	
	/** The list security class. */
	private List<ParameterTable> listSecurityClass;
	
	/** The list unblock accreditation state. */
	private List<ParameterTable> listUnblockAccreditationState;
	
	/** The holder. */
	private Holder holder;
	
	/** The holder description. */
	private String holderDescription;
	
	/** The security. */
	private Security security;
	
	/** The issuer. */
	private Issuer issuer;
	
	/** The initial date. */
	private Date initialDate;
	
	/** The final date. */
	private Date finalDate;
	
	/** The id holder account pk. */
	private Long idHolderAccountPk;
	
	/** The account number. */
	private Integer accountNumber;
	
	/** The list state. */
	private List<Integer> listState;
	
	/** The user participant investor. */
	private boolean userParticipantInvestor;
	
	/** The user issuer dpf. */
	private boolean userIssuerDPF;
	
	/**
	 * Instantiates a new unblock accreditation operation to.
	 */
	public UnblockAccreditationOperationTO() {}

	/**
	 * Gets the id unblock accreditation pk.
	 *
	 * @return the idUnblockAccreditationPk
	 */
	public Long getIdUnblockAccreditationPk() {
		return idUnblockAccreditationPk;
	}

	/**
	 * Sets the id unblock accreditation pk.
	 *
	 * @param idUnblockAccreditationPk the idUnblockAccreditationPk to set
	 */
	public void setIdUnblockAccreditationPk(Long idUnblockAccreditationPk) {
		this.idUnblockAccreditationPk = idUnblockAccreditationPk;
	}

	/**
	 * Gets the custody operation.
	 *
	 * @return the custodyOperation
	 */
	public CustodyOperation getCustodyOperation() {
		return custodyOperation;
	}

	/**
	 * Sets the custody operation.
	 *
	 * @param custodyOperation the custodyOperation to set
	 */
	public void setCustodyOperation(CustodyOperation custodyOperation) {
		this.custodyOperation = custodyOperation;
	}

	/**
	 * Gets the accreditation operation.
	 *
	 * @return the accreditationOperation
	 */
	public AccreditationOperation getAccreditationOperation() {
		return accreditationOperation;
	}

	/**
	 * Sets the accreditation operation.
	 *
	 * @param accreditationOperation the accreditationOperation to set
	 */
	public void setAccreditationOperation(
			AccreditationOperation accreditationOperation) {
		this.accreditationOperation = accreditationOperation;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the last modify app.
	 *
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}

	/**
	 * Sets the last modify app.
	 *
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}

	/**
	 * Gets the last modify date.
	 *
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}

	/**
	 * Sets the last modify date.
	 *
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}

	/**
	 * Gets the last modify ip.
	 *
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}

	/**
	 * Sets the last modify ip.
	 *
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}

	/**
	 * Gets the last modify user.
	 *
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}

	/**
	 * Sets the last modify user.
	 *
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	/**
	 * Gets the ind automatic.
	 *
	 * @return the indAutomatic
	 */
	public Integer getIndAutomatic() {
		return indAutomatic;
	}

	/**
	 * Sets the ind automatic.
	 *
	 * @param indAutomatic the indAutomatic to set
	 */
	public void setIndAutomatic(Integer indAutomatic) {
		this.indAutomatic = indAutomatic;
	}

	/**
	 * Gets the checks if is automatic.
	 *
	 * @return the isAutomatic
	 */
	public Boolean getIsAutomatic() {
		return isAutomatic;
	}

	/**
	 * Sets the checks if is automatic.
	 *
	 * @param isAutomatic the isAutomatic to set
	 */
	public void setIsAutomatic(Boolean isAutomatic) {
		this.isAutomatic = isAutomatic;
	}

	/**
	 * Gets the participant petitioner.
	 *
	 * @return the participantPetitioner
	 */
	public Participant getParticipantPetitioner() {
		return participantPetitioner;
	}

	/**
	 * Sets the participant petitioner.
	 *
	 * @param participantPetitioner the participantPetitioner to set
	 */
	public void setParticipantPetitioner(Participant participantPetitioner) {
		this.participantPetitioner = participantPetitioner;
	}

	/**
	 * Gets the security.
	 *
	 * @return the security
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * Sets the security.
	 *
	 * @param security the security to set
	 */
	public void setSecurity(Security security) {
		this.security = security;
	}

	/**
	 * Gets the holder.
	 *
	 * @return the holder
	 */
	public Holder getHolder() {
		return holder;
	}

	/**
	 * Sets the holder.
	 *
	 * @param holder the holder to set
	 */
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

	/**
	 * Gets the issuer.
	 *
	 * @return the issuer
	 */
	public Issuer getIssuer() {
		return issuer;
	}

	/**
	 * Sets the issuer.
	 *
	 * @param issuer the issuer to set
	 */
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}

	/**
	 * Gets the list participant petitioner.
	 *
	 * @return the listParticipantPetitioner
	 */
	public List<Participant> getListParticipantPetitioner() {
		return listParticipantPetitioner;
	}

	/**
	 * Sets the list participant petitioner.
	 *
	 * @param listParticipantPetitioner the listParticipantPetitioner to set
	 */
	public void setListParticipantPetitioner(
			List<Participant> listParticipantPetitioner) {
		this.listParticipantPetitioner = listParticipantPetitioner;
	}

	/**
	 * Gets the initial date.
	 *
	 * @return the initialDate
	 */
	public Date getInitialDate() {
		return initialDate;
	}

	/**
	 * Sets the initial date.
	 *
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	/**
	 * Gets the final date.
	 *
	 * @return the finalDate
	 */
	public Date getFinalDate() {
		return finalDate;
	}

	/**
	 * Sets the final date.
	 *
	 * @param finalDate the finalDate to set
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	/**
	 * Gets the list security class.
	 *
	 * @return the list security class
	 */
	public List<ParameterTable> getListSecurityClass() {
		return listSecurityClass;
	}

	/**
	 * Sets the list security class.
	 *
	 * @param listSecurityClass the new list security class
	 */
	public void setListSecurityClass(List<ParameterTable> listSecurityClass) {
		this.listSecurityClass = listSecurityClass;
	}

	/**
	 * Gets the list unblock accreditation state.
	 *
	 * @return the list unblock accreditation state
	 */
	public List<ParameterTable> getListUnblockAccreditationState() {
		return listUnblockAccreditationState;
	}

	/**
	 * Sets the list unblock accreditation state.
	 *
	 * @param listUnblockAccreditationState the new list unblock accreditation state
	 */
	public void setListUnblockAccreditationState(
			List<ParameterTable> listUnblockAccreditationState) {
		this.listUnblockAccreditationState = listUnblockAccreditationState;
	}

	/**
	 * Gets the id holder account pk.
	 *
	 * @return the id holder account pk
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * Sets the id holder account pk.
	 *
	 * @param idHolderAccountPk the new id holder account pk
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Integer getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the holder description.
	 *
	 * @return the holder description
	 */
	public String getHolderDescription() {
		return holderDescription;
	}

	/**
	 * Sets the holder description.
	 *
	 * @param holderDescription the new holder description
	 */
	public void setHolderDescription(String holderDescription) {
		this.holderDescription = holderDescription;
	}

	/**
	 * Gets the list state.
	 *
	 * @return the list state
	 */
	public List<Integer> getListState() {
		return listState;
	}

	/**
	 * Sets the list state.
	 *
	 * @param listState the new list state
	 */
	public void setListState(List<Integer> listState) {
		this.listState = listState;
	}

	/**
	 * @return the userParticipantInvestor
	 */
	public boolean isUserParticipantInvestor() {
		return userParticipantInvestor;
	}

	/**
	 * @param userParticipantInvestor the userParticipantInvestor to set
	 */
	public void setUserParticipantInvestor(boolean userParticipantInvestor) {
		this.userParticipantInvestor = userParticipantInvestor;
	}

	/**
	 * @return the userIssuerDPF
	 */
	public boolean isUserIssuerDPF() {
		return userIssuerDPF;
	}

	/**
	 * @param userIssuerDPF the userIssuerDPF to set
	 */
	public void setUserIssuerDPF(boolean userIssuerDPF) {
		this.userIssuerDPF = userIssuerDPF;
	}
		
}
