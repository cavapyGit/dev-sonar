package com.pradera.custody.receivecertificates.servicebean;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.receivecertificates.view.SearchReceiveCertificateTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;

@Stateless
public class ReceiveCertificateServiceBean extends CrudDaoServiceBean{

	public List<PayrollHeader> searchPayrollHeaders(SearchReceiveCertificateTO searchReceiveCertificateTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph FROM PayrollHeader ph");
		sbQuery.append("	LEFT JOIN ph.participant");
		sbQuery.append("	LEFT JOIN ph.issuer");
		sbQuery.append("	WHERE ph.payrollType = :payrollType");
		sbQuery.append("	AND ph.indApprove = :indApprove");			
		sbQuery.append("	AND TRUNC(ph.registerDate) >= :initialDate");
		sbQuery.append("	AND TRUNC(ph.registerDate) <= :endDate");
		if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getIdParticipantPk()))
			sbQuery.append("	AND ph.participant.idParticipantPk = :idParticipantPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getIssuer().getIdIssuerPk()))
			sbQuery.append("	AND ph.issuer.idIssuerPk = :idIssuerPk");
		if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getState()))
			sbQuery.append("	AND ph.payrollState = :payrollState");
		/*if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getBranchOffice()))
			sbQuery.append("	AND ph.branchOffice = :branchOffice");*/
		sbQuery.append("	ORDER BY ph.idPayrollHeaderPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", searchReceiveCertificateTO.getPayRollType());		
		query.setParameter("initialDate", searchReceiveCertificateTO.getInitialDate());		
		query.setParameter("endDate", searchReceiveCertificateTO.getEndDate());
		query.setParameter("indApprove", 1);
		if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchReceiveCertificateTO.getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getIssuer().getIdIssuerPk()))
			query.setParameter("idIssuerPk", searchReceiveCertificateTO.getIssuer().getIdIssuerPk());
		if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getState()))
			query.setParameter("payrollState", searchReceiveCertificateTO.getState());
		/*if(Validations.validateIsNotNullAndNotEmpty(searchReceiveCertificateTO.getBranchOffice()))
			query.setParameter("branchOffice", searchReceiveCertificateTO.getBranchOffice());*/
		return (List<PayrollHeader>)query.getResultList();
	}

	public PayrollHeader getPayrollHeader(Long idPayrollHeaderPk) throws ServiceException{
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ph FROM PayrollHeader ph");
			sbQuery.append("	LEFT JOIN ph.participant");
			sbQuery.append("	LEFT JOIN ph.issuer");
			sbQuery.append("	WHERE ph.idPayrollHeaderPk = :idPayrollHeaderPk");			
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);	
			return (PayrollHeader) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}		
	}

	public List<PhysicalCertificate> getPhysicalCertificatesRelated(Long idRetirementOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT	rc.physicalCertificate");
		sbQuery.append("	FROM RetirementCertificate rc");
		sbQuery.append("	WHERE rc.retirementOperation.idRetirementOperationPk = :idRetirementOperationPk");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idRetirementOperationPk", idRetirementOperationPk);	
		return query.getResultList();
	}
	


	public PayrollDetailTO getPayrollDetailInCorporative(Long idPayrollDetailPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" SELECT pd.IND_CUPON ,pc.ID_PROGRAM_INTEREST_FK, pc.ID_SECURITY_CODE_FK ");
		sbQuery.append(" FROM PAYROLL_DETAIL pd ");
		sbQuery.append(" INNER JOIN RETIREMENT_DETAIL rd ON rd.ID_RETIREMENT_DETAIL_PK = pd.ID_RETIREMENT_DETAIL_FK ");
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = rd.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append(" WHERE pd.ID_PAYROLL_DETAIL_PK = :idPayrollDetailPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idPayrollDetailPk", idPayrollDetailPk);

		List<Object[]> list = query.getResultList();
		
		PayrollDetailTO payrollDetail = new PayrollDetailTO();
		for(Object[] temp:list){

			if(Validations.validateIsNotNullAndNotEmpty(temp[0])){
				payrollDetail.setIndCupon(Integer.parseInt(temp[0].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[1])){
				payrollDetail.setIdProgramInterestPk(Long.valueOf(temp[1].toString()));
			}
			if(Validations.validateIsNotNullAndNotEmpty(temp[2])){
				payrollDetail.setIdSecurityCodePk(temp[2].toString());
			}
		}
		CorporativeOperation corporativeOp = null;
		if(payrollDetail.getIndCupon() !=null && payrollDetail.getIndCupon().equals(1)) {

			if(payrollDetail.getIdProgramInterestPk()!=null) {
				corporativeOp = getCorporativeOperationCupon(payrollDetail.getIdProgramInterestPk());
			}
		}else {
			
			if(payrollDetail.getIdSecurityCodePk()!=null) {
				corporativeOp = getCorporativeOperationCapital(payrollDetail.getIdSecurityCodePk());
			}
		}
		
		if(corporativeOp!=null && corporativeOp.getIndPayed()!=null) {
			payrollDetail.setIndCorporativeOperationPaid(corporativeOp.getIndPayed());
		}else {
			payrollDetail.setIndCorporativeOperationPaid(0);
		}
		
		return payrollDetail;
	}
	
	public CorporativeOperation getCorporativeOperationCupon(Long idProgramInterestPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT	co ");
		sbQuery.append("	FROM CorporativeOperation co ");
		sbQuery.append("	WHERE co.programInterestCoupon.idProgramInterestPk = :idProgramInterestPk ");
		sbQuery.append("	AND co.state = :state ");
		TypedQuery<CorporativeOperation> query = em.createQuery(sbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idProgramInterestPk", idProgramInterestPk);	
		query.setParameter("state", CorporateProcessStateType.DEFINITIVE.getCode());
		return query.getSingleResult();
	}
	
	public CorporativeOperation getCorporativeOperationCapital(String idSecurityCodePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT	co ");
		sbQuery.append("	FROM CorporativeOperation co ");
		sbQuery.append("	WHERE co.securities.idSecurityCodePk = :idSecurityCodePk ");
		sbQuery.append("	AND co.programAmortizationCoupon.idProgramAmortizationPk IS NOT NULL ");
		sbQuery.append("	AND co.state = :state ");
		TypedQuery<CorporativeOperation> query = em.createQuery(sbQuery.toString(),CorporativeOperation.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		query.setParameter("state", CorporateProcessStateType.DEFINITIVE.getCode());
		return query.getSingleResult();
	}
	
}
