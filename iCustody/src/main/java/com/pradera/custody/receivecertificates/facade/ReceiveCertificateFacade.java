package com.pradera.custody.receivecertificates.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.custody.payroll.servicebean.GeneratePayrollServiceBean;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.receivecertificates.servicebean.ReceiveCertificateServiceBean;
import com.pradera.custody.receivecertificates.view.SearchReceiveCertificateTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.custody.type.DematerializationStateType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class ReceiveCertificateFacade {
	
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;
	@EJB
	ReceiveCertificateServiceBean receiveCertificateServiceBean;
	@Inject
	GeneratePayrollServiceBean generatePayrollServiceBean;
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	public List<PayrollHeader> searchPayrollHeaders(SearchReceiveCertificateTO searchReceiveCertificateTO) throws ServiceException{
		return receiveCertificateServiceBean.searchPayrollHeaders(searchReceiveCertificateTO);
	}

	public PayrollHeader getPayrollHeader(Long idPayrollHeaderPk) throws ServiceException{
		return receiveCertificateServiceBean.getPayrollHeader(idPayrollHeaderPk);
	}
	
	public Boolean isPayrollCorporative(Long idPayrollHeaderPk, Integer payrollType) throws ServiceException{
		if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType)){
			return generatePayrollServiceBean.isRetirementPayrollDetailCorporative(idPayrollHeaderPk);
		}
		return true;
	}

	public List<PayrollDetailTO> getPayrollDetail(Long idPayrollHeaderPk, Integer payrollType) throws ServiceException{
		List<Object[]> lstResult = null;
		List<PayrollDetailTO> lstPayrollDetailTO = new ArrayList<>();
		
		/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailCorporative(idPayrollHeaderPk);
		}else*/ 
		if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailAccountAnnotation(idPayrollHeaderPk);
		}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailRemoveSecurities(idPayrollHeaderPk);
		}/*else if(PayrollType.CERTIFICATE_OPERATION.getCode().equals(payrollType)) {
			lstResult = generatePayrollServiceBean.getPayrollDetailCertificateOperation(idPayrollHeaderPk);
		}*/
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			for(Object[] objResult:lstResult){
				PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
				if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(payrollType))
					payrollDetailTO.setIdCorporativeOperationPk(new Long(objResult[0].toString()));
				else{
					payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[0].toString()));
					payrollDetailTO.setRegistryDate((Date) objResult[1]);
					payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNullAndNotEmpty(objResult[2])?objResult[2].toString():null);
					
					if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollType)
							|| PayrollType.CERTIFICATE_OPERATION.getCode().equals(payrollType)){



						if (objResult[19] != null) {
							List<AccountAnnotationOperation> annotationOperations =
									generatePayrollServiceBean.lstAccountAnnotationOperation(new Long(objResult[19].toString()));
							if (annotationOperations != null && !annotationOperations.isEmpty()) {
								payrollDetailTO.setCertificateFrom(annotationOperations.get(0).getCertificateFrom());
								payrollDetailTO.setCertificateTo(annotationOperations.get(0).getCertificateTo());
							}
						}
						
						Integer retirementDetailQuantity = (objResult[20]!=null)? new Integer(objResult[20].toString()) : 0 ;
						payrollDetailTO.setFromToCupons("");
						
						Integer motiveRetirement = new Integer(objResult[21].toString());
						payrollDetailTO.setMotiveRetirement(motiveRetirement);
						
						Integer indCupon = new Integer(objResult[7].toString());
						payrollDetailTO.setBlIsCupon((indCupon==1));
						
						
						if( RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode().equals(motiveRetirement)  ) {
							//si no tiene el from es RF, si lo tiene es un RV
							/*if( payrollDetailTO.getCertificateFrom()!=null && payrollDetailTO.getCertificateTo()!=null ) {
								retirementDetailQuantity = (payrollDetailTO.getCertificateTo()-payrollDetailTO.getCertificateFrom())+1;
							}else {
								retirementDetailQuantity = 1;	
							}*/
							
							/*payrollDetailTO.setTotalStock(0);
							payrollDetailTO.setCuponQuantity(1);*/
							payrollDetailTO.setCuponQuantityReport(0);
							if( payrollDetailTO.isBlIsCupon() ) {
								payrollDetailTO.setTotalStockReport(1);
								payrollDetailTO.setTotalStock(0);
								payrollDetailTO.setTotalBalance(BigDecimal.ZERO);
							}else {
								payrollDetailTO.setTotalStockReport(1);
								payrollDetailTO.setTotalStock(1);
								payrollDetailTO.setTotalBalance(BigDecimal.ONE);
							}
							
							//para acciones siempre es 0, y en los otros casos, como son retiro individuales tambien es 0
							retirementDetailQuantity = 0;
							payrollDetailTO.setDetailCuponQuantity(retirementDetailQuantity);
							payrollDetailTO.setCuponQuantity(1);
							/*
							payrollDetailTO.setDetailCuponQuantity(0);
							payrollDetailTO.setCuponQuantity(retirementDetailQuantity);
							*/
						}else {
							Integer cuponQuantityDb = new Integer(retirementDetailQuantity);
							
							//le resto 1 porque no se toma en cuenta el capital, solo cupones
							retirementDetailQuantity = retirementDetailQuantity-1;
							
							//para acciones siempre es 0
							if( payrollDetailTO.getCertificateFrom()!=null && payrollDetailTO.getCertificateTo()!=null ) {
								retirementDetailQuantity = 0;//(payrollDetailTO.getCertificateTo()-payrollDetailTO.getCertificateFrom())+1
							}
							
							payrollDetailTO.setDetailCuponQuantity(retirementDetailQuantity);
							payrollDetailTO.setCuponQuantity(cuponQuantityDb);
							
							if( payrollDetailTO.isBlIsCupon() ) {
								payrollDetailTO.setTotalStockReport(1);
								payrollDetailTO.setTotalStock(0);
								payrollDetailTO.setTotalBalance(BigDecimal.ZERO);
								payrollDetailTO.setCuponQuantityReport(0);
							}else {
								payrollDetailTO.setTotalStockReport(1);
								payrollDetailTO.setTotalStock(1);
								payrollDetailTO.setTotalBalance(BigDecimal.ONE);
								payrollDetailTO.setCuponQuantityReport(0);
							}
						}
						//payrollDetailTO.setQuantity((BigDecimal) objResult[3]);
						
						payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[4].toString()));
						payrollDetailTO.setState(new Integer(objResult[5].toString()));
						payrollDetailTO.setOperationType(new Integer(objResult[6].toString()));

						if(objResult[16] != null) {
							payrollDetailTO.setMnemonicIssuer((String)objResult[16]);
						}

						if(objResult[17] != null) {
							payrollDetailTO.setExpirationDate(((Date)objResult[17]));
						}
						
						payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[8].toString()):null);
						if(objResult[9]!=null) {
							payrollDetailTO.setSerialCode(objResult[9].toString());
						}
						payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[10]);
						payrollDetailTO.setCurrencyDesc( CurrencyType.get(((Integer)objResult[11])).getCodeIso() );
						
						//comentado para que no esten todos seleccionados por defecto
						if(PayrollDetailStateType.ANALYZED.getCode().equals(payrollDetailTO.getState())) {
							payrollDetailTO.setBlSelected(false);
						}

						Integer indReenter = (objResult[12]!=null)? new Integer(objResult[12].toString()) : 0 ;
						Date lastReenterDate = (objResult[13]!=null)? (Date)objResult[13] : null;
						Long idRetirementOperation = (objResult[14]!=null)? new Long(objResult[14].toString()) : 0L ;
						Long idPayrollDetail = (objResult[15]!=null)? new Long(objResult[15].toString()) : 0L ;

						
						
						RetirementOperation retirementOperationAAO = generatePayrollServiceBean.getRetirementOperation(idRetirementOperation);
						
						AccountAnnotationOperation aao = retirementOperationAAO.getAccountAnnotationOperation();
						if(retirementOperationAAO !=null && aao!=null && aao.getCertificateFrom()!=null && aao.getCertificateTo()!=null) {

							payrollDetailTO.setIndIsRV(true);
							payrollDetailTO.setFromToCupons(aao.getCertificateFrom()+" - "+aao.getCertificateTo());	
							Integer totalStockWithSerializable = Integer.valueOf(aao.getCertificateTo()) - Integer.valueOf(aao.getCertificateFrom()) + 1; 
							payrollDetailTO.setTotalStock(totalStockWithSerializable);
							payrollDetailTO.setTotalStockReport(totalStockWithSerializable);
							payrollDetailTO.setTotalBalance(new BigDecimal(totalStockWithSerializable));
							payrollDetailTO.setCuponQuantityReport(totalStockWithSerializable);
							
						}else {
							payrollDetailTO.setIndIsRV(false);
							
							if( !RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode().equals(motiveRetirement)  ) {								
								String fromToCupons = "";
								Integer cuponQuantityReport = 0;
								List<Object[]> lstCupons = generatePayrollServiceBean.getCuponsInRetirementDetail(idRetirementOperation);
								if( lstCupons!=null && lstCupons.size()>0 ) {
									
									List<String> lstMinMaxCupons = new ArrayList<String>();
									for(Object[] objCupon:lstCupons){
										lstMinMaxCupons.add(objCupon[0].toString());
										cuponQuantityReport++;
									}
									
									if(lstMinMaxCupons.size() == 1) {
										fromToCupons = lstMinMaxCupons.get(0);
									}else if(lstMinMaxCupons.size() > 1) {
										fromToCupons = lstMinMaxCupons.get(0) + " - " + lstMinMaxCupons.get(lstMinMaxCupons.size()-1);
									}
	
									payrollDetailTO.setCuponQuantityReport(cuponQuantityReport);
									payrollDetailTO.setFromToCupons(fromToCupons);	
								}
							}else {

								payrollDetailTO.setCuponQuantityReport(0);
								payrollDetailTO.setFromToCupons("");	
							}
							
						}
						
						if (objResult[18] != null) {
							payrollDetailTO.setNominalValue((BigDecimal) objResult[18]);
						}

						RetirementOperation retirementOperation = generatePayrollServiceBean.getRetirementOperation(idRetirementOperation);
						
						//Long idAccountAnnotation = (objResult[14]!=null)? new Long(objResult[14].toString()) : 0L ;
						
						if(retirementOperation!=null) {
							AccountAnnotationOperation accountAnnotationOperation = retirementOperation.getAccountAnnotationOperation();
							
							if(accountAnnotationOperation!=null) {
								Integer annotationMotive = accountAnnotationOperation.getMotive();
								payrollDetailTO.setAnnotationMotive(annotationMotive);
								
								if(DematerializationCertificateMotiveType.get(annotationMotive) !=null) {
									payrollDetailTO.setAnnotationMotiveDescription(DematerializationCertificateMotiveType.get(annotationMotive).getValue());
								}else {
									payrollDetailTO.setAnnotationMotiveDescription("");
								}
							}
						}
						/*Integer annotationMotive = (objResult[14]!=null)? new Integer(objResult[14].toString()) : 0 ;
						payrollDetailTO.setAnnotationMotive(annotationMotive);
						if(DematerializationCertificateMotiveType.get(payrollType) !=null) {
							payrollDetailTO.setAnnotationMotiveDescription(DematerializationCertificateMotiveType.get(payrollType).getValue());
						}else {
							payrollDetailTO.setAnnotationMotiveDescription("");
						}*/
						
						payrollDetailTO.setIndReenter(indReenter);
						payrollDetailTO.setLastReenterDate(lastReenterDate);
						
					}else if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollType)){
						HolderAccountTO temp = new HolderAccountTO();
						temp.setIdHolderAccountPk(new Long(objResult[3].toString()));
						payrollDetailTO.setQuantity((BigDecimal) objResult[4]);
						payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
						payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[5].toString()));
						payrollDetailTO.setState(new Integer(objResult[6].toString()));
						payrollDetailTO.setIndSerializable(new Integer(objResult[7].toString()));

						Integer indCupon = new Integer(objResult[8].toString());
						payrollDetailTO.setBlIsCupon((indCupon==1));
						

						if(indCupon.equals(0)) {
							payrollDetailTO.setTotalStock(1);
						}
						payrollDetailTO.setCuponQuantity(1);
					
						
						payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[9].toString()):null);
						if(objResult[10]!=null) {
							payrollDetailTO.setSerialCode(objResult[10].toString());
						}
						if(objResult[11] != null) {
							payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[11]);
						}

						if(objResult[13] != null) {
							payrollDetailTO.setMnemonicIssuer((String)objResult[13]);
						}

						if(objResult[14] != null) {
							payrollDetailTO.setExpirationDate(((Date)objResult[14]));
						}
						payrollDetailTO.setCurrencyDesc( CurrencyType.get(((Integer)objResult[15])).getCodeIso() );
						

						if(objResult[16] != null) {
							Integer annotationMotive = new Integer(objResult[16].toString());
							payrollDetailTO.setAnnotationMotive(annotationMotive);
							
							if(DematerializationCertificateMotiveType.get(annotationMotive) !=null) {
								payrollDetailTO.setAnnotationMotiveDescription(DematerializationCertificateMotiveType.get(annotationMotive).getValue());
							}else {
								payrollDetailTO.setAnnotationMotiveDescription("");
							}
						}
						
						if (objResult[17] != null) {
							Integer securityClass = new Integer(objResult[17].toString());
							payrollDetailTO.setSecurityClassDesc(SecurityClassType.get(securityClass).getValue());
						}
						
						//comentado para que no esten todos seleccionados por defecto
						if(PayrollDetailStateType.GENERATED.getCode().equals(payrollDetailTO.getState())) {
							payrollDetailTO.setBlSelected(false);
						}
					}		
								
				}
				lstPayrollDetailTO.add(payrollDetailTO);
			}
		}
		return lstPayrollDetailTO;
	}
	
	public PayrollDetailTO getPayrollDetailInCorporative(Long idPayrollDetailPk) throws ServiceException{
		return receiveCertificateServiceBean.getPayrollDetailInCorporative(idPayrollDetailPk);	
	}
	
	public PayrollDetail findPayrollDetail(Long idPayrollDetailPk){
		return receiveCertificateServiceBean.find(PayrollDetail.class, idPayrollDetailPk);
		
	}	
	public void revertToAnalized(PayrollDetailTO payrollDetailTO) {
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		PayrollDetail payrollDetail = receiveCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
		payrollDetail.setPayrollState(PayrollDetailStateType.ANALYZED.getCode());
		payrollDetail.setIndReenter(1);
		payrollDetail.setLastReenterDate(CommonsUtilities.currentDate());
		
		//PayrollHeader payrollHeader = receiveCertificateServiceBean.find(PayrollHeader.class, payrollDetailTO.getIdPayrollHeaderPk());
		//payrollHeader.setPayrollState(PayrollHeaderStateType.PROCESSED.getCode());
	}

	public List<PayrollDetailTO> receivePayroll(List<PayrollDetailTO> lstPayrollDetailTO, PayrollHeader payrollHeader) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		boolean blProcess = false;
		List<PayrollDetailTO> lstPayrollDetailReturn = new ArrayList<PayrollDetailTO>();
		
		for(PayrollDetailTO payrollDetailTO:lstPayrollDetailTO){
			if(payrollDetailTO.isBlSelected()){
				PayrollDetail payrollDetail = receiveCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
				if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
					if(!PayrollDetailStateType.GENERATED.getCode().equals(payrollDetail.getPayrollState()))
						throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
					payrollDetail.setPayrollState(PayrollDetailStateType.RECEIVED.getCode());
					lstPayrollDetailReturn.add(payrollDetailTO);
					blProcess = true;
				}else{
					if(!PayrollDetailStateType.ANALYZED.getCode().equals(payrollDetail.getPayrollState())) {
						throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
					}
					payrollDetail.setPayrollState(PayrollDetailStateType.DELIVERED.getCode());
					lstPayrollDetailReturn.add(payrollDetailTO);
					blProcess = true;
					if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
						List<PhysicalCertificate> lstPhysicalCertificates = receiveCertificateServiceBean.getPhysicalCertificatesRelated(payrollDetail.getRetirementOperation().getIdRetirementOperationPk());
						if(Validations.validateListIsNotNullAndNotEmpty(lstPhysicalCertificates)) {
							for(PhysicalCertificate physicalCertificate:lstPhysicalCertificates) {					
								physicalCertificate.setSituation(PayrollDetailStateType.DELIVERED.getCode());
							}
						}
					}else{
						
					}
				}
			}/*else{
				if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
					PayrollDetail payrollDetail = receiveCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
					payrollDetail.setPayrollState(PayrollDetailStateType.ANNULATE.getCode());
					AccountAnnotationOperation accountAnnotationOperation = receiveCertificateServiceBean.find(AccountAnnotationOperation.class, payrollDetailTO.getIdCustodyOperationPk());
					accountAnnotationOperation.setState(DematerializationStateType.REIGSTERED.getCode());
					accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REIGSTERED.getCode());
					accountAnnotationOperation.getCustodyOperation().setApprovalDate(null);
					accountAnnotationOperation.getCustodyOperation().setApprovalUser(null);
				}
			}*/
		}
		if(blProcess){
			if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType()))
				payrollHeader.setPayrollState(PayrollHeaderStateType.PROCESSED.getCode());
			else
				payrollHeader.setPayrollState(PayrollHeaderStateType.FINISHED.getCode());
			receiveCertificateServiceBean.update(payrollHeader);
		}
		
		return lstPayrollDetailReturn;
	}
	

	public List<PayrollDetail> annulatePayroll(List<PayrollDetailTO> lstPayrollDetailTO, PayrollHeader payrollHeader, Integer motive, String requestOtherMotiveReject) throws ServiceException{
		List<PayrollDetail> lstPayrollDetail = new ArrayList<PayrollDetail>();
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		boolean blProcess = false;
		Integer quantityDetailAnulate = 0;
		for(PayrollDetailTO payrollDetailTO:lstPayrollDetailTO){

			PayrollDetail payrollDetail = receiveCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
			if(payrollDetailTO.isBlSelected()){
				if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
					if(!PayrollDetailStateType.GENERATED.getCode().equals(payrollDetail.getPayrollState())) {
						throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
					}
					payrollDetail.setPayrollState(PayrollDetailStateType.REJECT.getCode());
					payrollDetail.setRejectMotive(motive);
					payrollDetail.setRejectMotiveOther(requestOtherMotiveReject);
					lstPayrollDetail.add(payrollDetail);
					AccountAnnotationOperation accountAnnotationOperation = receiveCertificateServiceBean.find(AccountAnnotationOperation.class, payrollDetailTO.getIdCustodyOperationPk());
					accountAnnotationOperation.setState(DematerializationStateType.REIGSTERED.getCode());
					accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REIGSTERED.getCode());
					accountAnnotationOperation.getCustodyOperation().setApprovalDate(null);
					accountAnnotationOperation.getCustodyOperation().setApprovalUser(null);
					blProcess = true;
				}
				
			}
			if(payrollDetail.getPayrollState().equals(PayrollDetailStateType.REJECT.getCode()) || payrollDetailTO.isBlSelected() ) {
				quantityDetailAnulate++;
			}
			/*else{
				if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())){
					PayrollDetail payrollDetail = receiveCertificateServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
					payrollDetail.setPayrollState(PayrollDetailStateType.ANNULATE.getCode());
					AccountAnnotationOperation accountAnnotationOperation = receiveCertificateServiceBean.find(AccountAnnotationOperation.class, payrollDetailTO.getIdCustodyOperationPk());
					accountAnnotationOperation.setState(DematerializationStateType.REIGSTERED.getCode());
					accountAnnotationOperation.getCustodyOperation().setState(DematerializationStateType.REIGSTERED.getCode());
					accountAnnotationOperation.getCustodyOperation().setApprovalDate(null);
					accountAnnotationOperation.getCustodyOperation().setApprovalUser(null);
				}
			}*/
		}
		if(blProcess){
			/*if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
				payrollHeader.setPayrollState(PayrollHeaderStateType.ANNULATE.getCode());
			}else {
				payrollHeader.setPayrollState(PayrollHeaderStateType.ANNULATE.getCode());
			}*/
			if(quantityDetailAnulate >= lstPayrollDetailTO.size()) {
				payrollHeader.setPayrollState(PayrollHeaderStateType.REJECT.getCode());
				payrollHeader.setRejectMotive(motive);
				receiveCertificateServiceBean.update(payrollHeader);
			}
		}
		return lstPayrollDetail;
	}
	
	public boolean generateAcuse(PayrollHeader payrollHeader) {
		try {
			receiveCertificateServiceBean.update(payrollHeader);
			return true;
		}catch (ServiceException se) {
			se.printStackTrace();
			return false;
			// TODO: handle exception
		}
	}

}
