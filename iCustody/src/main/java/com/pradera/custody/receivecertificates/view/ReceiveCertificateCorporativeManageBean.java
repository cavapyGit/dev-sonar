package com.pradera.custody.receivecertificates.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;

//import org.jboss.solder.exception.control.ExceptionToCatch;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.payroll.view.SummaryPayrollTO;
import com.pradera.custody.receivecertificates.facade.ReceiveCertificateFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;

@DepositaryWebBean
@LoggerCreateBean
public class ReceiveCertificateCorporativeManageBean extends GenericBaseBean {

	private static final long serialVersionUID = 1L;
	private Map<Integer, String> mpStates, mpPayrollTypes, mpDetailStates, mpOperationTypes, mpRejectMotives;
	private List<ParameterTable> lstStates, lstPayrollTypes, lstBranchOffices;
	private SearchReceiveCertificateTO searchReceiveCertificateTO;
	private SummaryPayrollTO summaryPayrollTO;
	private PayrollHeader payrollHeader;
	private List<PayrollDetailTO> lstPayrollDetailTO;
	private List<Participant> lstParticipants;
	private final static String VIEW_MAPPING = "viewCertificateReceptionCorporative";
	private final static String SEARCH_MAPPING = "searchCertificateReceptionCorporative";
	private Integer requestMotive;
	private List<ParameterTable> listMotives;
	@Inject
	UserPrivilege userPrivilege;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@Inject
	AccountsFacade accountsFacade;
	@Inject
	ReceiveCertificateFacade receiveCertificateFacade;

	@PostConstruct
	public void init() {
		try {
			showPrivilegeButtons();
			fillParameters();
			searchReceiveCertificateTO = new SearchReceiveCertificateTO();
			searchReceiveCertificateTO.setInitialDate(getCurrentSystemDate());
			searchReceiveCertificateTO.setEndDate(getCurrentSystemDate());
			searchReceiveCertificateTO.setIssuer(new Issuer());
			searchReceiveCertificateTO.setBranchOffice(2735);// oficina asuncion
			searchReceiveCertificateTO.setPayRollType(PayrollType.REMOVE_SECURITIES_OPERATION.getCode());
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			lstParticipants = accountsFacade.getLisParticipantServiceBean(filterPar);
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	@LoggerAuditWeb
	public void search() {
		try {
			searchReceiveCertificateTO.setLstPayrollHeader(null);
			searchReceiveCertificateTO.setBlResults(false);
			List<PayrollHeader> lstResult = receiveCertificateFacade.searchPayrollHeaders(searchReceiveCertificateTO);
			List<PayrollHeader> lstResulCorporative = new ArrayList<PayrollHeader>();
			if (Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
				
				for (PayrollHeader payrollHeader : lstResult) {
					Boolean flagPayrollCorporativeInRetirement = receiveCertificateFacade.isPayrollCorporative(payrollHeader.getIdPayrollHeaderPk(), searchReceiveCertificateTO.getPayRollType());
					if(flagPayrollCorporativeInRetirement) {
						payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
						payrollHeader.setPayrollTypeDesc(mpPayrollTypes.get(payrollHeader.getPayrollType()));
						lstResulCorporative.add(payrollHeader);
					}
				}
				searchReceiveCertificateTO.setLstPayrollHeader(new GenericDataModel<>(lstResulCorporative));
				searchReceiveCertificateTO.setBlResults(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	@LoggerAuditWeb
	public void beforeAnnulation() {
		boolean blSelected = false;
		getLstRejectMotivesType();
		for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
			if (payrollDetailTO.isBlSelected()) {
				blSelected = true;
				break;
			}
		}
		if (!blSelected) {
			boolean blConfirm = false;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2701) {// ANALYZED
						blConfirm = true;
						break;
					}
				}
			} else {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2715) {// PayrollDetailStateType.GENERATED.getCode()
						blConfirm = true;
						break;
					}
				}
			}
			if (blConfirm) {
				if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron entregados.");
					JSFUtilities.showSimpleValidationDialog();
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron recepcionados.");
					JSFUtilities.showSimpleValidationDialog();
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe seleccionar por lo menos un registro.");
				JSFUtilities.showSimpleValidationDialog();
			}
		} else {
			String bodyText = GeneralConstants.EMPTY_STRING;
			/*
			 * if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.
			 * getPayrollType())) bodyText =
			 * "¿Está seguro que desea entregar los siguientes registros de la planilla?";
			 * else
			 */
			bodyText = "¿Está seguro que desea anular los siguientes registros de la planilla?";
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
		}

	}

	public void getLstRejectMotivesType() {

		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(1248);
			listMotives = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/*
	 * generatePayrollFacade.annulPayroll(payrollHeader.getIdPayrollHeaderPk(),
	 * lstPayrollDetailTO);
	 * showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.
	 * LBL_HEADER_ALERT_SUCCESS) , "Se anulo la planilla número " +
	 * payrollHeader.getIdPayrollHeaderPk() +".");
	 * JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	 */

	public void annulation() {
		try {

			receiveCertificateFacade.annulatePayroll(lstPayrollDetailTO, payrollHeader, requestMotive, null);
			String bodyText = GeneralConstants.EMPTY_STRING;
			/*
			 * if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.
			 * getPayrollType())) bodyText = "Se entregaron " +
			 * summaryPayrollTO.getYesRecords() + " registro(s) de la planilla con N° " +
			 * payrollHeader.getIdPayrollHeaderPk().toString() + "."; else
			 */
			bodyText = "Se anularon " + summaryPayrollTO.getYesRecords() + " registro(s) de la planilla con N° "
					+ payrollHeader.getIdPayrollHeaderPk().toString() + ".";

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			search();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	public String viewDetail(PayrollHeader payrollHeader) {
		try {
			this.payrollHeader = receiveCertificateFacade.getPayrollHeader(payrollHeader.getIdPayrollHeaderPk());
			this.payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
			if(payrollHeader.getRejectMotive() != null && payrollHeader.getPayrollState() == 2835) {
				this.payrollHeader.setRejectMotiveDesc(mpRejectMotives.get(payrollHeader.getRejectMotive()));
			}	
			lstPayrollDetailTO = receiveCertificateFacade.getPayrollDetail(payrollHeader.getIdPayrollHeaderPk(),
					payrollHeader.getPayrollType());
			summaryPayrollTO = new SummaryPayrollTO();
			if (PayrollHeaderStateType.PROCESSED.getCode().equals(this.payrollHeader.getPayrollState())) {
				for (PayrollDetailTO temp : lstPayrollDetailTO) {
					temp.setStateDesc(mpDetailStates.get(temp.getState()));
					temp.setOperationTypeDesc(mpOperationTypes.get(temp.getOperationType()));

					if(temp.getQuantity() == null ) {
						temp.setQuantity(BigDecimal.ZERO);
					}
					
					if (PayrollDetailStateType.ANNULATE.getCode().equals(temp.getState())) {
						
						if(summaryPayrollTO.getNoStock() == null ) {
							summaryPayrollTO.setNoStock(BigDecimal.ZERO);
						}
						if(summaryPayrollTO.getNoRecords() == null ) {
							summaryPayrollTO.setNoRecords(BigDecimal.ZERO);
						}
						
						summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(temp.getQuantity()));
						summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.ONE));
						
					} else {
						
						if(summaryPayrollTO.getYesStock() == null ) {
							summaryPayrollTO.setYesStock(BigDecimal.ZERO);
						}
						if(summaryPayrollTO.getYesRecords() == null ) {
							summaryPayrollTO.setYesRecords(BigDecimal.ZERO);
						}
						
						summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(temp.getQuantity()));
						summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(BigDecimal.ONE));
						
					}

					if(summaryPayrollTO.getTotalStock() == null ) {
						summaryPayrollTO.setTotalStock(BigDecimal.ZERO);
					}
					
					if(summaryPayrollTO.getTotalRecords() == null ) {
						summaryPayrollTO.setTotalRecords(BigDecimal.ZERO);
					}
					
					summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalStock().add(temp.getQuantity()));
					summaryPayrollTO.setTotalRecords(summaryPayrollTO.getTotalRecords().add(BigDecimal.ONE));
				}
			} else {
				for (PayrollDetailTO temp : lstPayrollDetailTO) {
					
					temp.setStateDesc(mpDetailStates.get(temp.getState()));
					if (temp.isBlSelected()) {

						if(summaryPayrollTO.getTotalStock() == null ) {
							summaryPayrollTO.setTotalStock(BigDecimal.ZERO);
						}
						
						if(summaryPayrollTO.getTotalRecords() == null ) {
							summaryPayrollTO.setTotalRecords(BigDecimal.ZERO);
						}
						summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalStock().add(temp.getQuantity()));
						summaryPayrollTO.setTotalRecords(summaryPayrollTO.getTotalRecords().add(BigDecimal.ONE));
					}
					
				}
				summaryPayrollTO.setYesStock(summaryPayrollTO.getTotalStock());
				summaryPayrollTO.setYesRecords(summaryPayrollTO.getTotalRecords());
			}
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}

	@LoggerAuditWeb
	public void revertToAnalized(PayrollDetailTO temp) {

		try {
			PayrollDetailTO payrollDetailTOBd = receiveCertificateFacade.getPayrollDetailInCorporative(temp.getIdPayrollDetailPk());
			
			if(payrollDetailTOBd.getIndCorporativeOperationPaid().equals(1)) {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"El proceso corporativo ya se encuentra consignado. Verifique");
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			receiveCertificateFacade.revertToAnalized(temp);
			viewDetail(this.payrollHeader);
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateSummary(PayrollDetailTO temp) {

		for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
			if (payrollDetailTO.getIdCustodyOperationPk().equals(temp.getIdCustodyOperationPk())) {
				payrollDetailTO.setBlSelected(temp.isBlSelected());
			}
		}
		updateRecordSelected();
	}

	public void updateRecordSelected() {

		BigDecimal yesRecords = BigDecimal.ZERO;
		BigDecimal noRecords = BigDecimal.ZERO;

		summaryPayrollTO = new SummaryPayrollTO();
		summaryPayrollTO.setTotalRecords(new BigDecimal(lstPayrollDetailTO.size()));
		summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalRecords());

		for (PayrollDetailTO detail : lstPayrollDetailTO) {
			if (detail.isBlSelected()) {
				yesRecords = yesRecords.add(BigDecimal.ONE);
			} else {
				noRecords = noRecords.add(BigDecimal.ONE);
			}
		}
		summaryPayrollTO.setYesRecords(yesRecords);
		summaryPayrollTO.setNoRecords(noRecords);

	}

	public void beforeReceive() {
		boolean blSelected = false;
		for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
			if (payrollDetailTO.isBlSelected()) {
				blSelected = true;
				break;
			}
		}
		if (!blSelected) {
			boolean blConfirm = false;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2701) {// ANALYZED
						blConfirm = true;
						break;
					}
				}
			} else {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2715) {// PayrollDetailStateType.GENERATED.getCode()
						blConfirm = true;
						break;
					}
				}
			}
			if (blConfirm) {
				if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron entregados.");
					JSFUtilities.showSimpleValidationDialog();
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron recepcionados.");
					JSFUtilities.showSimpleValidationDialog();
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe seleccionar por lo menos un registro.");
				JSFUtilities.showSimpleValidationDialog();
			}
		} else {
			String bodyText = GeneralConstants.EMPTY_STRING;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType()))
				bodyText = "¿Está seguro que desea entregar los siguientes registros de la planilla?";
			else
				bodyText = "¿Está seguro que desea recepcionar los siguientes registros de la planilla?";
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
	}

	@LoggerAuditWeb
	public void receive() {
		try {
			receiveCertificateFacade.receivePayroll(lstPayrollDetailTO, payrollHeader);
			String bodyText = GeneralConstants.EMPTY_STRING;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType()))
				bodyText = "Se entregaron " + summaryPayrollTO.getYesRecords() + " registro(s) de la planilla con N° "
						+ payrollHeader.getIdPayrollHeaderPk().toString() + ".";
			else
				bodyText = "Se recepcionaron " + summaryPayrollTO.getYesRecords()
						+ " registro(s) de la planilla con N° " + payrollHeader.getIdPayrollHeaderPk().toString() + ".";

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			search();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	public String back() {
		return SEARCH_MAPPING;
	}

	public void resetSearch() {
		searchReceiveCertificateTO.setIdParticipantPk(null);
		searchReceiveCertificateTO.setIssuer(new Issuer());
		cleanResult();
	}

	public void cleanResult() {
		searchReceiveCertificateTO.setLstPayrollHeader(null);
	}

	public void clean() {
		JSFUtilities.resetViewRoot();
		searchReceiveCertificateTO = new SearchReceiveCertificateTO();
		searchReceiveCertificateTO.setInitialDate(getCurrentSystemDate());
		searchReceiveCertificateTO.setEndDate(getCurrentSystemDate());
		searchReceiveCertificateTO.setIssuer(new Issuer());
	}

	private void fillParameters() throws ServiceException {
		mpStates = new HashMap<>();
		mpDetailStates = new HashMap<>();
		mpPayrollTypes = new HashMap<>();
		mpOperationTypes = new HashMap<>();
		mpRejectMotives = new HashMap<>();
		lstStates = new ArrayList<>();
		lstPayrollTypes = new ArrayList<>();
		lstBranchOffices = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_HEADER_STATE.getCode());
		List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			if (ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())) {
				lstStates.add(paramTab);
			}
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			if (ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()) && paramTab.getParameterTablePk().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode()) ) {
				lstPayrollTypes.add(paramTab);
			}
			mpPayrollTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_DETAIL_STATE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			mpDetailStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			if (ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())) {
				lstBranchOffices.add(paramTab);
			}
		}
		paramTabTO.setMasterTableFk(MasterTableType.CERTIFICATE_OPERATIONS.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp)
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		paramTabTO.setMasterTableFk(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp)
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		paramTabTO.setMasterTableFk(MasterTableType.MOTIVES_INMOVILIZATION_REJECT.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp)
			mpRejectMotives.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	}

	private void showPrivilegeButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnCertifyView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	public void acceptRejectMotive() {		
    	
    	if(Validations.validateIsNullOrNotPositive(requestMotive)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Debe seleccionar un motivo de rechazo");
			//JSFUtilities.updateComponent("outPnlDialogs");
			JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
		
		String bodyText = GeneralConstants.EMPTY_STRING;

		bodyText = "¿Está seguro que desea anular los siguientes registros de la planilla raaaaaaaw?";
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
				bodyText);;
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogAnnulation').show()");
	}

	public Map<Integer, String> getMpStates() {
		return mpStates;
	}

	public void setMpStates(Map<Integer, String> mpStates) {
		this.mpStates = mpStates;
	}

	public Map<Integer, String> getMpPayrollTypes() {
		return mpPayrollTypes;
	}

	public void setMpPayrollTypes(Map<Integer, String> mpPayrollTypes) {
		this.mpPayrollTypes = mpPayrollTypes;
	}

	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	public List<ParameterTable> getLstPayrollTypes() {
		return lstPayrollTypes;
	}

	public void setLstPayrollTypes(List<ParameterTable> lstPayrollTypes) {
		this.lstPayrollTypes = lstPayrollTypes;
	}

	public SearchReceiveCertificateTO getSearchReceiveCertificateTO() {
		return searchReceiveCertificateTO;
	}

	public void setSearchReceiveCertificateTO(SearchReceiveCertificateTO searchReceiveCertificateTO) {
		this.searchReceiveCertificateTO = searchReceiveCertificateTO;
	}

	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}

	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}

	public List<PayrollDetailTO> getLstPayrollDetailTO() {
		return lstPayrollDetailTO;
	}

	public void setLstPayrollDetailTO(List<PayrollDetailTO> lstPayrollDetailTO) {
		this.lstPayrollDetailTO = lstPayrollDetailTO;
	}

	public SummaryPayrollTO getSummaryPayrollTO() {
		return summaryPayrollTO;
	}

	public void setSummaryPayrollTO(SummaryPayrollTO summaryPayrollTO) {
		this.summaryPayrollTO = summaryPayrollTO;
	}

	public Map<Integer, String> getMpDetailStates() {
		return mpDetailStates;
	}

	public void setMpDetailStates(Map<Integer, String> mpDetailStates) {
		this.mpDetailStates = mpDetailStates;
	}

	public List<ParameterTable> getLstBranchOffices() {
		return lstBranchOffices;
	}

	public void setLstBranchOffices(List<ParameterTable> lstBranchOffices) {
		this.lstBranchOffices = lstBranchOffices;
	}

	public Map<Integer, String> getMpOperationTypes() {
		return mpOperationTypes;
	}

	public void setMpOperationTypes(Map<Integer, String> mpOperationTypes) {
		this.mpOperationTypes = mpOperationTypes;
	}

	public Integer getRequestMotive() {
		return requestMotive;
	}

	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	public List<ParameterTable> getListMotives() {
		return listMotives;
	}

	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}

}
