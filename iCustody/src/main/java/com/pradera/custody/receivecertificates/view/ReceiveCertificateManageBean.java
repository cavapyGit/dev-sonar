package com.pradera.custody.receivecertificates.view;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

//import org.jboss.solder.exception.control.ExceptionToCatch;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.PrivilegeComponent;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.sessionuser.UserPrivilege;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.payroll.view.SummaryPayrollTO;
import com.pradera.custody.receivecertificates.facade.ReceiveCertificateFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.process.BusinessProcess;

@DepositaryWebBean
@LoggerCreateBean
public class ReceiveCertificateManageBean extends GenericBaseBean {

	private static final long serialVersionUID = 1L;
	private Map<Integer, String> mpStates, mpPayrollTypes, mpDetailStates, mpOperationTypes, mpRejectMotives;
	private List<ParameterTable> lstStates, lstPayrollTypes, lstBranchOffices;
	private SearchReceiveCertificateTO searchReceiveCertificateTO;
	private SummaryPayrollTO summaryPayrollTO;
	private PayrollHeader payrollHeader;
	private List<PayrollDetailTO> lstPayrollDetailTO;
	private List<Participant> lstParticipants;
	private final static String VIEW_MAPPING = "viewCertificateReception";
	private final static String SEARCH_MAPPING = "searchCertificateReception";
	private Integer requestMotive;
	private String requestOtherMotiveReject;
	private List<ParameterTable> listMotives;
	@Inject
	UserPrivilege userPrivilege;
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@Inject
	AccountsFacade accountsFacade;
	@Inject
	ReceiveCertificateFacade receiveCertificateFacade;

	@EJB
	NotificationServiceFacade notificationServiceFacade; 

	@Inject
	private UserInfo userInfo;
	
	private boolean isParticipant = false;
	private boolean isDepositary = false;
	
	@PostConstruct
	public void init() {
		try {
			if(userInfo.getUserAccountSession().isParticipantInstitucion() ){
				isParticipant = true;
			}else {
				isDepositary = true;
			}
			
			showPrivilegeButtons();
			fillParameters();
			searchReceiveCertificateTO = new SearchReceiveCertificateTO();
			searchReceiveCertificateTO.setInitialDate(getCurrentSystemDate());
			searchReceiveCertificateTO.setEndDate(getCurrentSystemDate());
			searchReceiveCertificateTO.setIssuer(new Issuer());
			searchReceiveCertificateTO.setBranchOffice(2735);// oficina asuncion
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			lstParticipants = accountsFacade.getLisParticipantServiceBean(filterPar);
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	@LoggerAuditWeb
	public void search() {
		try {
			searchReceiveCertificateTO.setLstPayrollHeader(null);
			searchReceiveCertificateTO.setBlResults(false);
			List<PayrollHeader> lstResult = receiveCertificateFacade.searchPayrollHeaders(searchReceiveCertificateTO);
			if (Validations.validateListIsNotNullAndNotEmpty(lstResult)) {
				for (PayrollHeader payrollHeader : lstResult) {
					payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
					payrollHeader.setPayrollTypeDesc(mpPayrollTypes.get(payrollHeader.getPayrollType()));
				}
				searchReceiveCertificateTO.setLstPayrollHeader(new GenericDataModel<>(lstResult));
				searchReceiveCertificateTO.setBlResults(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	@LoggerAuditWeb
	public void beforeAnnulation() {
		boolean blSelected = false;
		getLstRejectMotivesType();
		for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
			if (payrollDetailTO.isBlSelected()) {
				blSelected = true;
				break;
			}
		}
		if (!blSelected) {
			boolean blConfirm = false;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2701) {// ANALYZED
						blConfirm = true;
						break;
					}
				}
			} else {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2715) {// PayrollDetailStateType.GENERATED.getCode()
						blConfirm = true;
						break;
					}
				}
			}
			if (blConfirm) {
				if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron entregados.");
					JSFUtilities.showSimpleValidationDialog();
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron recepcionados.");
					JSFUtilities.showSimpleValidationDialog();
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe seleccionar por lo menos un registro.");
				JSFUtilities.showSimpleValidationDialog();
			}
		} else {
			String bodyText = GeneralConstants.EMPTY_STRING;
			/*
			 * if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.
			 * getPayrollType())) bodyText =
			 * "¿Está seguro que desea entregar los siguientes registros de la planilla?";
			 * else
			 */
			bodyText = "¿Está seguro que desea anular los siguientes registros de la planilla?";
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('dlgMotive').show();");
		}

	}

	public void getLstRejectMotivesType() {

		try {

			ParameterTableTO parameterTableTO = new ParameterTableTO();
			parameterTableTO.setState(ParameterTableStateType.REGISTERED.getCode());
			parameterTableTO.setMasterTableFk(1248);
			parameterTableTO.setOrderbyParameterTableCd(1);
			listMotives = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		} catch (Exception ex) {
			excepcion.fire(new ExceptionToCatchEvent(ex));
		}
	}

	/*
	 * generatePayrollFacade.annulPayroll(payrollHeader.getIdPayrollHeaderPk(),
	 * lstPayrollDetailTO);
	 * showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.
	 * LBL_HEADER_ALERT_SUCCESS) , "Se anulo la planilla número " +
	 * payrollHeader.getIdPayrollHeaderPk() +".");
	 * JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
	 */
	
	public String payrollDetailStr(List<PayrollDetail> lstPayrollDetail) {
		String strPayrollDetail = "";
		for (PayrollDetail payrollDetail : lstPayrollDetail) {
			strPayrollDetail += ", "+payrollDetail.getIdPayrollDetailPk();
		}
		
		return strPayrollDetail.substring(1,strPayrollDetail.length());
	}
	
	public String motiveStr(Integer requestMotivo) {
		String strMotive = "";
		for (ParameterTable parameter: listMotives) {
			if( parameter.getParameterTablePk().equals(requestMotivo) ) {
				strMotive = parameter.getParameterName();
				break;
			}
		}
		
		return strMotive;
	}
	
	public void annulation() {
		try {

			List<PayrollDetail> lstPayrollDetail = receiveCertificateFacade.annulatePayroll(lstPayrollDetailTO, payrollHeader, requestMotive, requestOtherMotiveReject);
			

			
			try {
			/*** JH NOTIFICACIONES ***/
			
			BusinessProcess businessProcess = new BusinessProcess();
			businessProcess.setIdBusinessProcessPk(BusinessProcessType.UTILITIES.getCode());//PHYSICAL_CERTIFICATE_REJECT_PAYROLL_DETAIL

			Object[] parameters = new Object[2];
			parameters[0] = payrollDetailStr(lstPayrollDetail);
			parameters[1] = motiveStr(requestMotive);
			/*
			notificationServiceFacade.sendParticipantNotificationEmail(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, 
					   payrollHeader.getParticipant().getIdParticipantPk(), 
					   parameters);
			*/
				notificationServiceFacade.sendNotificationEmailParticipantNoConfig(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, 
					   "RECHAZO DE TITULO",
					   "La solicitud Nro %s ha sido rechazada por motivo %s ",
					   payrollHeader.getParticipant().getIdParticipantPk(), 
					   parameters);
				
			}catch (Exception e) {
				System.out.println("*** no se pudo enviar la notificacion:: "+e.getMessage());
			}
			/*
			notificationServiceFacade.sendNotificationEmailProfileNoConfig(userInfo.getUserAccountSession().getUserName(), 
					   businessProcess, 
					   "mensaje de prueba perfil",
					   "este mensaje tiene 2 %s , %s parametros",
					   324, 
					   parameters);
			*/
			//payrollHeader.getParticipant().getIdParticipantPk()
			/*** JH NOTIFICACIONES ***/
			
			String bodyText = GeneralConstants.EMPTY_STRING;
			/*
			 * if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.
			 * getPayrollType())) bodyText = "Se entregaron " +
			 * summaryPayrollTO.getYesRecords() + " registro(s) de la planilla con N° " +
			 * payrollHeader.getIdPayrollHeaderPk().toString() + "."; else
			 */
			//" + summaryPayrollTO.getYesRecords() + "
			bodyText = "Se rechazo registro(s) de la planilla con N "
					+ payrollHeader.getIdPayrollHeaderPk().toString() + ".";

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			search();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	public String viewDetail(PayrollHeader payrollHeader) {
		try {
			this.payrollHeader = receiveCertificateFacade.getPayrollHeader(payrollHeader.getIdPayrollHeaderPk());
			this.payrollHeader.setStateDesc(mpStates.get(payrollHeader.getPayrollState()));
			if(payrollHeader.getRejectMotive() != null && payrollHeader.getPayrollState() == 2835) {
				this.payrollHeader.setRejectMotiveDesc(mpRejectMotives.get(payrollHeader.getRejectMotive()));
			}	
			lstPayrollDetailTO = receiveCertificateFacade.getPayrollDetail(payrollHeader.getIdPayrollHeaderPk(),
					payrollHeader.getPayrollType());
			
			if( PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType()) ) {
				for(PayrollDetailTO det: lstPayrollDetailTO) {
					det.setBlSelected(true);
				}
			}
			
			summaryPayrollTO = new SummaryPayrollTO();
			if (PayrollHeaderStateType.PROCESSED.getCode().equals(this.payrollHeader.getPayrollState())) {
				for (PayrollDetailTO temp : lstPayrollDetailTO) {
					temp.setStateDesc(mpDetailStates.get(temp.getState()));
					temp.setOperationTypeDesc(mpOperationTypes.get(temp.getOperationType()));
					/*if (PayrollDetailStateType.ANNULATE.getCode().equals(temp.getState())) {
						summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(temp.getQuantity()));
						summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.ONE));
					} else {
						summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(temp.getQuantity()));
						summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(BigDecimal.ONE));
					}
					summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalStock().add(temp.getQuantity()));
					summaryPayrollTO.setTotalRecords(summaryPayrollTO.getTotalRecords().add(BigDecimal.ONE));*/
				}
			} else {
				for (PayrollDetailTO temp : lstPayrollDetailTO) {
					temp.setStateDesc(mpDetailStates.get(temp.getState()));
					temp.setOperationTypeDesc(mpOperationTypes.get(temp.getOperationType()));
				}
				/*
				for (PayrollDetailTO temp : lstPayrollDetailTO) {
					temp.setStateDesc(mpDetailStates.get(temp.getState()));
					if (temp.isBlSelected()) {
						summaryPayrollTO.setTotalStock(summaryPayrollTO.getTotalStock().add(temp.getQuantity()));
						summaryPayrollTO.setTotalRecords(summaryPayrollTO.getTotalRecords().add(BigDecimal.ONE));
					}
				}
				summaryPayrollTO.setYesStock(summaryPayrollTO.getTotalStock());
				summaryPayrollTO.setYesRecords(summaryPayrollTO.getTotalRecords());*/
			}
			updateRecordSelected();
			return VIEW_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}

	public void updateSummary(PayrollDetailTO temp) {
		/*
		 * if(temp.isBlSelected()){
		 * summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(
		 * BigDecimal.ONE));
		 * summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().subtract(
		 * BigDecimal.ONE));
		 * summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(temp.
		 * getQuantity()));
		 * summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().subtract(temp.
		 * getQuantity())); }else{
		 * summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().subtract(
		 * BigDecimal.ONE));
		 * summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.
		 * ONE));
		 * summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().subtract(temp.
		 * getQuantity()));
		 * summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(temp.
		 * getQuantity())); }
		 */

		for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
			if (payrollDetailTO.getIdCustodyOperationPk().equals(temp.getIdCustodyOperationPk())) {
				payrollDetailTO.setBlSelected(temp.isBlSelected());
				/*
				 * if(payrollDetailTO.isBlSelected()){
				 * summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().add(
				 * BigDecimal.ONE));
				 * summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().subtract(
				 * BigDecimal.ONE));
				 * summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().add(
				 * payrollDetailTO.getQuantity()));
				 * summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().subtract(
				 * payrollDetailTO.getQuantity())); }else{
				 * summaryPayrollTO.setYesRecords(summaryPayrollTO.getYesRecords().subtract(
				 * BigDecimal.ONE));
				 * summaryPayrollTO.setNoRecords(summaryPayrollTO.getNoRecords().add(BigDecimal.
				 * ONE)); summaryPayrollTO.setYesStock(summaryPayrollTO.getYesStock().subtract(
				 * payrollDetailTO.getQuantity()));
				 * summaryPayrollTO.setNoStock(summaryPayrollTO.getNoStock().add(payrollDetailTO
				 * .getQuantity())); }
				 */
			}
		}
		updateRecordSelected();
	}

	public void updateRecordSelected() {

		BigDecimal yesRecords = BigDecimal.ZERO;
		BigDecimal noRecords = BigDecimal.ZERO;

		Integer totalRecords = 0;
		Integer totalQuantity = 0;
		Integer totalStock = 0;
		for (PayrollDetailTO detail : lstPayrollDetailTO) {
			Integer cuponQuantity = (detail.getCuponQuantity()==null)? 0 : detail.getCuponQuantity();
			Integer currentStock = (detail.getTotalStock()==null)? 0 : detail.getTotalStock();
			if (detail.isBlSelected()) {
				//yesRecords = yesRecords.add(cuponQuantity);
				totalRecords++;
				totalQuantity = totalQuantity + cuponQuantity;
				totalStock = totalStock + currentStock;
			} else {
				//noRecords = noRecords.add(cuponQuantity);
			}
		}

		summaryPayrollTO = new SummaryPayrollTO();

		summaryPayrollTO.setTotalRecords( new BigDecimal(totalQuantity) );
		/*summaryPayrollTO.setYesRecords(yesRecords);
		summaryPayrollTO.setNoRecords(noRecords);*/
		

		summaryPayrollTO.setTotalStock( new BigDecimal(totalStock) );
		/*summaryPayrollTO.setYesStock(yesRecords);
		summaryPayrollTO.setNoStock(noRecords);*/

		
	}

	public void beforeReceive() {
		boolean blSelected = false;
		for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
			if (payrollDetailTO.isBlSelected()) {
				blSelected = true;
				break;
			}
		}
		if (!blSelected) {
			boolean blConfirm = false;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2701) {// ANALYZED
						blConfirm = true;
						break;
					}
				}
			} else {
				for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
					if (payrollDetailTO.getState() != 2715) {// PayrollDetailStateType.GENERATED.getCode()
						blConfirm = true;
						break;
					}
				}
			}
			if (blConfirm) {
				if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron entregados.");
					JSFUtilities.showSimpleValidationDialog();
				} else {
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
							"Los títulos ya fueron recepcionados.");
					JSFUtilities.showSimpleValidationDialog();
				}
			} else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"Debe seleccionar por lo menos un registro.");
				JSFUtilities.showSimpleValidationDialog();
			}
		} else {
			String bodyText = GeneralConstants.EMPTY_STRING;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType()))
				bodyText = "¿Está seguro que desea entregar los siguientes registros de la planilla?";
			else
				bodyText = "¿Está seguro que desea recepcionar los siguientes registros de la planilla?";
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}
	}

	@LoggerAuditWeb
	public void receive() {
		try {
			List<PayrollDetailTO> lstPayrollDetailExecute = receiveCertificateFacade.receivePayroll(lstPayrollDetailTO, payrollHeader);
			String bodyText = GeneralConstants.EMPTY_STRING;
			if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
				bodyText = "Se entregaron " + summaryPayrollTO.getTotalRecords() + " registro(s) de la planilla con N° "
						+ payrollHeader.getIdPayrollHeaderPk().toString() + ".";
			}else {
				bodyText = "Se recepcionaron " + summaryPayrollTO.getTotalRecords()
						+ " registro(s) de la planilla con N° " + payrollHeader.getIdPayrollHeaderPk().toString() + ".";
			}
			
			try {

				if (PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(payrollHeader.getPayrollType())) {
					//if( !( payrollHeader.getIndDelivery()!=null && payrollHeader.getIndDelivery().equals(1) ) 
					/*&& ( payrollHeader.getRetirementMotive()!=null && !payrollHeader.getRetirementMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) )*/ //en este caso el retiro por corporativo
					//) 
					{
						
						/*** JH NOTIFICACIONES ***/
//						Object[] parameters = new Object[lstPayrollDetailExecute.size()];
//						for(int i=0; i< lstPayrollDetailExecute.size(); i++) {
//							parameters[i] = lstPayrollDetailExecute.get(i).getIdPayrollHeaderPk();
//						}
						Object[] parameters = new Object[1];
						parameters[0] = payrollHeader.getIdPayrollHeaderPk();
						
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_RETIREMENT_DELIVERY.getCode());
						
						if(isDepositary){
							notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							businessProcess, 
							payrollHeader.getParticipant().getIdParticipantPk(), parameters);
						}else if(isParticipant){
							notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), 
							businessProcess, 
							payrollHeader.getParticipant().getIdParticipantPk(), parameters);
						}
						/*** end JH NOTIFICACIONES ***/
						
						/*
						BusinessProcess businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.UTILITIES.getCode());//PHYSICAL_CERTIFICATE_REJECT_PAYROLL_DETAIL
		
						Object[] parameters = new Object[1];
						parameters[0] = payrollHeader.getIdPayrollHeaderPk();
						
						notificationServiceFacade.sendNotificationEmailParticipantNoConfig(userInfo.getUserAccountSession().getUserName(), 
								   businessProcess, 
								   "RETIRO DE TITULOS",
								   "Los titulos de la solicitud %s han sido retirados",
								   payrollHeader.getParticipant().getIdParticipantPk(), 
								   parameters);
						*/
						
					}
				}
				
			}catch (Exception e) {
				System.out.println("*** no se pudo enviar la notificacion:: "+e.getMessage());
			}
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
					bodyText);
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			search();
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
					PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			// excepcion.fire(new ExceptionToCatch(e));
		}
	}

	public String back() {
		return SEARCH_MAPPING;
	}

	public void resetSearch() {
		searchReceiveCertificateTO.setIdParticipantPk(null);
		searchReceiveCertificateTO.setIssuer(new Issuer());
		cleanResult();
	}

	public void cleanResult() {
		searchReceiveCertificateTO.setLstPayrollHeader(null);
	}

	public void clean() {
		JSFUtilities.resetViewRoot();
		searchReceiveCertificateTO = new SearchReceiveCertificateTO();
		searchReceiveCertificateTO.setInitialDate(getCurrentSystemDate());
		searchReceiveCertificateTO.setEndDate(getCurrentSystemDate());
		searchReceiveCertificateTO.setIssuer(new Issuer());
	}

	private void fillParameters() throws ServiceException {
		mpStates = new HashMap<>();
		mpDetailStates = new HashMap<>();
		mpPayrollTypes = new HashMap<>();
		mpOperationTypes = new HashMap<>();
		mpRejectMotives = new HashMap<>();
		lstStates = new ArrayList<>();
		lstPayrollTypes = new ArrayList<>();
		lstBranchOffices = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_HEADER_STATE.getCode());
		List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			if (ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())) {
				lstStates.add(paramTab);
			}
			mpStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			if (ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())) {
				lstPayrollTypes.add(paramTab);
			}
			mpPayrollTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_DETAIL_STATE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			mpDetailStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp) {
			if (ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())) {
				lstBranchOffices.add(paramTab);
			}
		}
		paramTabTO.setMasterTableFk(MasterTableType.CERTIFICATE_OPERATIONS.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp)
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		paramTabTO.setMasterTableFk(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp)
			mpOperationTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		paramTabTO.setMasterTableFk(MasterTableType.MOTIVES_INMOVILIZATION_REJECT.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for (ParameterTable paramTab : lstTemp)
			mpRejectMotives.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
	}

	private void showPrivilegeButtons() {
		PrivilegeComponent privilegeComponent = new PrivilegeComponent();
		privilegeComponent.setBtnConfirmView(true);
		privilegeComponent.setBtnRejectView(true);
		privilegeComponent.setBtnCertifyView(true);
		userPrivilege.setPrivilegeComponent(privilegeComponent);
	}
	
	public void acceptRejectMotive() {		
    	
    	if(Validations.validateIsNullOrNotPositive(requestMotive)){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Debe seleccionar un motivo de rechazo");
			//JSFUtilities.updateComponent("outPnlDialogs");
			JSFUtilities.showSimpleValidationDialog();
    		return;
    	}
		
		String bodyText = GeneralConstants.EMPTY_STRING;

		bodyText = "¿Está seguro que desea rechazar los siguientes registros de la planilla ?";
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING),
				bodyText);
		JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialogAnnulation').show()");
	}
	
	public void saveFormat() {
		try {
			String pdfFileName = "planilla.pdf";
			Document document = 
					( payrollHeader.getPayrollType().equals(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode()) )
							? generatePDFReception(pdfFileName, lstPayrollDetailTO)
							: generatePDFDeliver(pdfFileName, lstPayrollDetailTO);
			if (payrollHeader.getPayrollType().equals(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode())) {
				payrollHeader.setReceptionAcuseFile(getBytesFromFile(new File(pdfFileName)));
				payrollHeader.setIndGenerateReceptionAcuse(BooleanType.YES.getCode());
			} else if (payrollHeader.getPayrollType().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode())) {
				payrollHeader.setSendAcuseFile(getBytesFromFile(new File(pdfFileName)));
				payrollHeader.setIndGenerateSendAcuse(BooleanType.YES.getCode());
			}
			if (receiveCertificateFacade.generateAcuse(payrollHeader)) {
				String bodyText = "Acuse generado correctamente.";
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS),
						bodyText);
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			} else {
				String bodyText = "Error al generar el acuse.";
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR),
						bodyText);
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void downloadFormat() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		String pdfFileName= "planilla.pdf";
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition",
        "inline=filename=" + pdfFileName);
        Document document;
        byte file[] = null;
		try {
			if (payrollHeader.getPayrollType().equals(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode())) {
				if (payrollHeader.getIndGenerateReceptionAcuse().equals(BooleanType.NO.getCode())) {
					document = generatePDFReception(pdfFileName, lstPayrollDetailTO);
					response.getOutputStream().write(getBytesFromFile(new File(pdfFileName)));
				} else {
					file = payrollHeader.getReceptionAcuseFile();
					response.getOutputStream().write(file);
				}
			} else {
				if (payrollHeader.getIndGenerateSendAcuse().equals(BooleanType.NO.getCode())) {
					document = generatePDFDeliver(pdfFileName, lstPayrollDetailTO);
					response.getOutputStream().write(getBytesFromFile(new File(pdfFileName)));
				} else {
					file = payrollHeader.getSendAcuseFile();
					response.getOutputStream().write(file);
				}
			}
            try {
                response.getOutputStream().flush();
                response.getOutputStream().close();
                context.responseComplete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public com.lowagie.text.Document generatePDFReception(String pdfFileName, List<PayrollDetailTO> dataTable) throws IOException, DocumentException, URISyntaxException, ClassNotFoundException {
		
		Calendar calendario = new GregorianCalendar();
		
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(pdfFileName));
        
        document.setPageSize(PageSize.LEGAL); 
        document.setMargins(40f, 40f, 80f, 80f);
  	  
  	    FacesContext context = FacesContext.getCurrentInstance(); 
        ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
  	    Image logo = Image.getInstance(servletContext.getRealPath("") + "/WEB-INF/classes/" + "logo.jpg"); 
  	    logo.setAlignment(Image.MIDDLE);
  	    logo.scaleAbsoluteHeight(15); logo.scaleAbsoluteWidth(15);
  	    logo.scalePercent(15);
  	  
  	    Chunk chunk = new Chunk(logo, -185, -10); HeaderFooter header = new
  	    HeaderFooter(new Phrase(chunk), false);
  	    header.setAlignment(Element.ALIGN_CENTER);
  	    header.setBorder(Rectangle.NO_BORDER); 
  	    document.setHeader(header);
        
        Font fontTittle= new Font();
        fontTittle.setSize(15);

        Font fontTableHeader= new Font();
        fontTableHeader.setSize(8.5f);
        fontTableHeader.setColor(Color.WHITE);

        Font fontTableBody= new Font();
        fontTableBody.setSize(8.5f);
        
        Font fontBodyText= new Font(Font.TIMES_ROMAN);
        fontBodyText.setSize(11);
        
        document.open();
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        
        Paragraph dateLocation = new Paragraph("Asunción, " + calendario.get(Calendar.DAY_OF_MONTH)+" de " + getMonthDesc(calendario.get(Calendar.MONTH)+1) + " del "+CommonsUtilities.currentYear(), fontBodyText);
        dateLocation.setAlignment(Element.ALIGN_RIGHT);
        document.add(dateLocation);
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("Sres. " + payrollHeader.getParticipant().getDescription() + ":", fontBodyText));
		document.add(new Paragraph("   "));
		document.add(new Paragraph("Conforme planilla nro. " + payrollHeader.getIdPayrollHeaderPk() +", confirmamos el estado de los siguientes títulos presentados a la recepción de CAVAPY", fontBodyText));
		document.add(new Paragraph("   "));

        
        com.lowagie.text.pdf.PdfPTable table = new com.lowagie.text.pdf.PdfPTable(10);
        table.setWidthPercentage(110);
        float f [];
        f = new float[10];
        f[0] = 60; f[1] = 70; f[2] = 50; f[3] = 40; f[4] = 55; f[5] = 60; f[6] = 60; f[7] = 70; f[8] = 90; f[9] = 80;
        table.setTotalWidth(f);
        
        com.lowagie.text.pdf.PdfPCell columnHeader;
        com.lowagie.text.pdf.PdfPCell columnBody;
        
        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("N° SOLICITUD", fontTableHeader)); 
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("CLASE DE VALOR", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("EMISOR", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("SERIE", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("MONEDA", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("CANTIDAD", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);
        
        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("VALOR NOMINAL", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("FECHA VTO", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("MOTIVO INGRESO", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);

        columnHeader = new com.lowagie.text.pdf.PdfPCell(new Paragraph ("ESTADO", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        table.addCell( columnHeader);
        
        for(int i=0; i<dataTable.size(); i++) {

        	columnBody = new com.lowagie.text.pdf.PdfPCell(new Paragraph (dataTable.get(i).getIdCustodyOperationPk().toString(), fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(columnBody); //1
            
            columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( dataTable.get(i).getSecurityClassDesc(), fontTableBody ));
            columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody); //2
            
            columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( dataTable.get(i).getMnemonicIssuer(), fontTableBody ));
            columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody); //2
        	
            columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( dataTable.get(i).getSerialCode(), fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody); //3
        	
        	columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( (dataTable.get(i).getCurrencyDesc()), fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody); //5
            
            columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getQuantity()), fontTableBody ));
            columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody); //2

        	columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getSecurityNominalValue()), fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody); //7

            if (dataTable.get(i).getExpirationDate() != null) {

    			columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph (CommonsUtilities.convertDatetoString(dataTable.get(i).getExpirationDate()),fontTableBody ));
    			columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
    			table.addCell( columnBody); //7
            } else {

    			columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ("N/A" ,fontTableBody ));
    			columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
    			table.addCell( columnBody); //7
            }

			String motiveDesc = "";			
			if (dataTable.get(i).getAnnotationMotiveDescription().contains("INGRESO POR")) {
				motiveDesc = dataTable.get(i).getAnnotationMotiveDescription().substring("INGRESO POR ".length(),
						dataTable.get(i).getAnnotationMotiveDescription().length());
			}else {
				motiveDesc = dataTable.get(i).getAnnotationMotiveDescription();
			}
			
			columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( motiveDesc, fontTableBody ));
			columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell( columnBody); //9

			columnBody = new com.lowagie.text.pdf.PdfPCell( new Paragraph ( dataTable.get(i).getStateDesc(), fontTableBody ));
			columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell( columnBody); //9
        }

        document.add(table);
		
        document.add(new Paragraph("   "));
        
        Paragraph footer = new Paragraph("Estimado Depositante, favor tener en cuenta que los títulos-valores físicos recibidos pasarán por un proceso de verificación y análisis, y de no cumplir con las formalidades establecidas serán rechazados, de acuerdo al Reglamento Interno.", fontBodyText);
        footer.setAlignment(Element.ALIGN_LEFT);
        
        document.add(footer);
        
        document.close();
        
        return document;
	}
	
	private String getMonthDesc(int mes) {
		String mesDesc = "";
		switch(mes) {
		case 1:
			mesDesc = "enero";
			break;
		case 2:
			mesDesc = "febrero";
			break;
		case 3:
			mesDesc = "marzo";
			break;
		case 4:
			mesDesc = "abril";
			break;
		case 5:
			mesDesc = "mayo";
			break;
		case 6:
			mesDesc = "junio";
			break;
		case 7:
			mesDesc = "julio";
			break;
		case 8:
			mesDesc = "agosto";
			break;
		case 9:
			mesDesc = "setiembre";
			break;
		case 10:
			mesDesc = "octubre";
			break;
		case 11:
			mesDesc = "noviembre";
			break;
		case 12:
			mesDesc = "diciembre";
			break;
		}
		return mesDesc;
	}

	public com.lowagie.text.Document generatePDFDeliver(String pdfFileName, List<PayrollDetailTO> dataTable) throws DocumentException, MalformedURLException, IOException {

		Calendar calendario = new GregorianCalendar();
		
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(pdfFileName));
        
        document.setPageSize(PageSize.LEGAL); 
        document.setMargins(40f, 40f, 80f, 80f);
  	  
  	    FacesContext context = FacesContext.getCurrentInstance(); 
        ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
  	    Image logo = Image.getInstance(servletContext.getRealPath("") + "/WEB-INF/classes/" + "logo.jpg"); 
  	    logo.setAlignment(Image.MIDDLE);
  	    logo.scaleAbsoluteHeight(15); 
  	    logo.scaleAbsoluteWidth(15);
  	    logo.scalePercent(15);
  	    
  	    Chunk chunk = new Chunk(logo, -185, -10); HeaderFooter header = new HeaderFooter(new Phrase(chunk), false);
  	  	header.setAlignment(Element.ALIGN_CENTER);
  	  	header.setBorder(Rectangle.NO_BORDER); 
  	  	document.setHeader(header);
        
        document.open();
        
        Font fontTittle= new Font(Font.TIMES_ROMAN);
        fontTittle.setSize(15);

        Font fontTableHeader= new Font();
        fontTableHeader.setSize(9);
        fontTableHeader.setColor(Color.WHITE);
        
        Font fontTableBody= new Font();
        fontTableBody.setSize(9);
        
        Font fontBodyText= new Font(Font.TIMES_ROMAN);
        fontBodyText.setSize(11);
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        
        Paragraph dateLocation = new Paragraph("Asunción, " + calendario.get(Calendar.DAY_OF_MONTH)+" de " + getMonthDesc(calendario.get(Calendar.MONTH)+1) + " del "+CommonsUtilities.currentYear(), fontBodyText);
        dateLocation.setAlignment(Element.ALIGN_RIGHT);
        document.add(dateLocation);
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));

        document.add(new Paragraph("Señores", fontBodyText));
        document.add(new Paragraph("CAVAPY S.A.", fontBodyText));
        document.add(new Paragraph("Presente", fontBodyText));
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));

        document.add(new Paragraph("Yo, _____________________________________________________ identificado con documento de identidad N° ", fontBodyText));
        document.add(new Paragraph("_____________________, en representación del Depositante " + payrollHeader.getParticipant().getDescription() + " declaro ", fontBodyText));
        document.add(new Paragraph("haber recibido los siguientes títulos-valores físicos detallados más abajo:", fontBodyText));
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        
        
        PdfPTable table = new PdfPTable(8);
        table.setWidthPercentage(100);
        
        PdfPCell columnHeader;
        PdfPCell columnBody;
        
        columnHeader = new PdfPCell(new Paragraph ("N° Solicitud", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);

        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Código de valor", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);

        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Emisor", fontTableHeader));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);


        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Cantidad", fontTableHeader ));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);


        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Valor Nominal", fontTableHeader ));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        
        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Fecha de Vencimiento", fontTableHeader ));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);

        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Cantidad de Cupones", fontTableHeader ));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);

        table.addCell( columnHeader);
        columnHeader = new PdfPCell(new Paragraph ("Desde - Hasta", fontTableHeader ));
        columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
        columnHeader.setBackgroundColor(Color.GRAY);
        
        table.addCell( columnHeader);
        
        for(int i=0; i<dataTable.size(); i++) {

        	columnBody = new PdfPCell(new Paragraph (dataTable.get(i).getIdCustodyOperationPk().toString(), fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody);
        	
        	columnBody = new PdfPCell( new Paragraph ( dataTable.get(i).getIdSecurityCodePk() ,fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody);
        	
        	columnBody = new PdfPCell( new Paragraph ( dataTable.get(i).getMnemonicIssuer(), fontTableBody ));
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody);

        	columnBody = new PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getTotalStockReport()) , fontTableBody ));//CuponQuantity()
        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell( columnBody);
            

            if (dataTable.get(i).getIndIsRV() != null && dataTable.get(i).getIndIsRV()) {

            	columnBody = new PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getNominalValue()), fontTableBody ));
            	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell( columnBody);
                
            }else {
                
                if (dataTable.get(i).getCuponQuantity() == 0) {
                	columnBody = new PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getNominalValue()), fontTableBody ));
                	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell( columnBody);
                }else {
                	columnBody = new PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getSecurityNominalValue()), fontTableBody ));
                	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell( columnBody);
                }
            }

            if (dataTable.get(i).getExpirationDate() != null) {

            	columnBody = new PdfPCell( new Paragraph (CommonsUtilities.convertDatetoString(dataTable.get(i).getExpirationDate()),fontTableBody ));
            	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell( columnBody);
            } else {

            	columnBody = new PdfPCell( new Paragraph ("N/A", fontTableBody ));
            	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell( columnBody);
            }        	
            

            if (dataTable.get(i).getIndIsRV()) {
            	
	        	columnBody = new PdfPCell( new Paragraph ( "N/A", fontTableBody ));
	        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
	            table.addCell( columnBody);
            }else {
            	
            	columnBody = new PdfPCell( new Paragraph ( formatNumberPy(dataTable.get(i).getCuponQuantityReport()), fontTableBody ));
            	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell( columnBody);
            }
            
            if (dataTable.get(i).getIndIsRV()) {
            	
        		columnBody = new PdfPCell( new Paragraph ( dataTable.get(i).getFromToCupons(), fontTableBody ) );
            	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell( columnBody);
                
            } else {
            	
                if (dataTable.get(i).getMotiveRetirement().equals(RetirementOperationMotiveType.REVERSION.getCode())) {
                	columnBody = new PdfPCell( new Paragraph ( dataTable.get(i).getFromToCupons(), fontTableBody ));
    	        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
    	            table.addCell( columnBody);
                }else {
                	columnBody = new PdfPCell( new Paragraph ( "N/A", fontTableBody ));
    	        	columnBody.setHorizontalAlignment(Element.ALIGN_CENTER);
    	            table.addCell( columnBody);
                }
            }

            
            
            
        }
        
        document.add(table);
		
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        
        Paragraph footer = new Paragraph("Atentamente,");
        footer.setAlignment(Element.ALIGN_LEFT);
        document.add(footer);
        
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        document.add(new Paragraph("   "));
        
        Paragraph firm = new Paragraph("________________________");
        firm.setAlignment(Element.ALIGN_CENTER);
        document.add(firm);
        
        document.close();
        
        return document;
	}
	
	
	private String formatNumber(BigDecimal securityNominalValue) {
		NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
		return nf.format(securityNominalValue.doubleValue());
	}
	
	private String formatNumberPy(BigDecimal securityNominalValue) {
		NumberFormat nf = NumberFormat.getInstance(new Locale("es", "ES"));
		return nf.format(securityNominalValue);
	}
	
	private String formatNumberPy(int securityNominalValue) {
		NumberFormat nf = NumberFormat.getInstance(new Locale("es", "ES"));
		return nf.format(securityNominalValue);
	}

	public static byte[] getBytesFromFile(File file) throws IOException {
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            throw new IOException("File is too large!");
        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        InputStream is = new FileInputStream(file);
        try {
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
        } finally {
            is.close();
        }
        if (offset < bytes.length) {
            throw new IOException("No se pudo completar la generacion del archivo");
        }
        return bytes;
    }

	public Map<Integer, String> getMpStates() {
		return mpStates;
	}

	public void setMpStates(Map<Integer, String> mpStates) {
		this.mpStates = mpStates;
	}

	public Map<Integer, String> getMpPayrollTypes() {
		return mpPayrollTypes;
	}

	public void setMpPayrollTypes(Map<Integer, String> mpPayrollTypes) {
		this.mpPayrollTypes = mpPayrollTypes;
	}

	public List<ParameterTable> getLstStates() {
		return lstStates;
	}

	public void setLstStates(List<ParameterTable> lstStates) {
		this.lstStates = lstStates;
	}

	public List<ParameterTable> getLstPayrollTypes() {
		return lstPayrollTypes;
	}

	public void setLstPayrollTypes(List<ParameterTable> lstPayrollTypes) {
		this.lstPayrollTypes = lstPayrollTypes;
	}

	public SearchReceiveCertificateTO getSearchReceiveCertificateTO() {
		return searchReceiveCertificateTO;
	}

	public void setSearchReceiveCertificateTO(SearchReceiveCertificateTO searchReceiveCertificateTO) {
		this.searchReceiveCertificateTO = searchReceiveCertificateTO;
	}

	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}

	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}

	public PayrollHeader getPayrollHeader() {
		return payrollHeader;
	}

	public void setPayrollHeader(PayrollHeader payrollHeader) {
		this.payrollHeader = payrollHeader;
	}

	public List<PayrollDetailTO> getLstPayrollDetailTO() {
		return lstPayrollDetailTO;
	}

	public void setLstPayrollDetailTO(List<PayrollDetailTO> lstPayrollDetailTO) {
		this.lstPayrollDetailTO = lstPayrollDetailTO;
	}

	public SummaryPayrollTO getSummaryPayrollTO() {
		return summaryPayrollTO;
	}

	public void setSummaryPayrollTO(SummaryPayrollTO summaryPayrollTO) {
		this.summaryPayrollTO = summaryPayrollTO;
	}

	public Map<Integer, String> getMpDetailStates() {
		return mpDetailStates;
	}

	public void setMpDetailStates(Map<Integer, String> mpDetailStates) {
		this.mpDetailStates = mpDetailStates;
	}

	public List<ParameterTable> getLstBranchOffices() {
		return lstBranchOffices;
	}

	public void setLstBranchOffices(List<ParameterTable> lstBranchOffices) {
		this.lstBranchOffices = lstBranchOffices;
	}

	public Map<Integer, String> getMpOperationTypes() {
		return mpOperationTypes;
	}

	public void setMpOperationTypes(Map<Integer, String> mpOperationTypes) {
		this.mpOperationTypes = mpOperationTypes;
	}

	public Integer getRequestMotive() {
		return requestMotive;
	}

	public void setRequestMotive(Integer requestMotive) {
		this.requestMotive = requestMotive;
	}

	public List<ParameterTable> getListMotives() {
		return listMotives;
	}

	public void setListMotives(List<ParameterTable> listMotives) {
		this.listMotives = listMotives;
	}

	public String getRequestOtherMotiveReject() {
		return requestOtherMotiveReject;
	}

	public void setRequestOtherMotiveReject(String requestOtherMotiveReject) {
		this.requestOtherMotiveReject = requestOtherMotiveReject;
	}

}
