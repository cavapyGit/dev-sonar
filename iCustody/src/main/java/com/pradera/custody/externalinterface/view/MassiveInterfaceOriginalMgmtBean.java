package com.pradera.custody.externalinterface.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveInterfaceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MassiveInterfaceOriginalMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The upload file types. */
	private String fUploadFileTypes = "xml";
	
	/** The upload file types pattern. */
	private Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.xml";

	/** The process files data model. */
	private GenericDataModel<ProcessFileTO> processFilesDataModel;
	
	/** The errors data model. */
	private GenericDataModel<RecordValidationType> errorsDataModel;
	
	/** The process file filter. */
	private ProcessFileTO processFileFilter;  // for searches
	
	/** The process file. */
	private ProcessFileTO processFile; // bean sent to Registration Batch 
	
	/** The tmp process file. */
	private ProcessFileTO tmpProcessFile; // temporal bean on upload
	
	/** The back action. */
	private String backAction;
	
	/** The title label. */
	private String titleLabel;
	
	/** The massive interface facade. */
	@EJB
	private GeneralParametersFacade massiveInterfaceFacade;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFile = null;
	}
	
	/**
	 * Load interface params.
	 *
	 * @param interfaceName the interface name
	 * @param backAction the back action
	 */
	public void loadInterfaceParams(String interfaceName, String backAction){
		if(interfaceName!=null){
			
			processFileFilter.setInterfaceName(interfaceName);
			
			this.tmpProcessFile = massiveInterfaceFacade.getExternalInterfaceInformation(interfaceName);
			
			if(ComponentConstant.INTERFACE_EARLY_PAYMENT.equals(interfaceName)){
				this.titleLabel = PropertiesUtilities.getGenericMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_MASSIVE_LABEL);
			}else if(ComponentConstant.INTERFACE_ACCOUNT_ENTRY.equals(interfaceName) || ComponentConstant.INTERFACE_ACCOUNT_ENTRY_MASSIVE.equals(interfaceName)){
				this.titleLabel = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_MASSIVE_LABEL);
			}
			
		}
		this.backAction = backAction;
		
	}

	/**
	 * On change process date.
	 */
	public void onChangeProcessDate() {
		processFilesDataModel = null;
		processFile = null;
	}
	
	/**
	 * Clean all.
	 */
	public void cleanAll() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveMCN");
		String interfaceName = processFileFilter.getInterfaceName();
		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFileFilter.setInterfaceName(interfaceName);
		
		processFile = null;
		processFilesDataModel = null;
	}

	/**
	 * Search processed files.
	 */
	public void searchProcessedFiles() {
		List<ProcessFileTO> mcnProcessFiles = massiveInterfaceFacade.getProcessedFilesInformation(processFileFilter.getProcessDate(),processFileFilter.getInterfaceName());
		processFilesDataModel = new GenericDataModel<ProcessFileTO>(mcnProcessFiles);
	}

	/**
	 * File upload handler.
	 *
	 * @param event the event
	 */
	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateFile(event.getFile(), null, null,fUploadFileTypes);
			if (fDisplayName != null) {
				tmpProcessFile.setFileName(fDisplayName);
				tmpProcessFile.setProcessFile(event.getFile().getContents());
				tmpProcessFile.setTempProcessFile(File.createTempFile("tempMcnfile", ".tmp"));
				FileUtils.writeByteArrayToFile(tmpProcessFile.getTempProcessFile(), tmpProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}
	
	/**
	 * Gets the file stream content.
	 *
	 * @param mcnFile the mcn file
	 * @return the file stream content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getFileStreamContent(ProcessFileTO mcnFile) throws IOException{
		if(mcnFile!=null){
			
			InputStream inputStream = new ByteArrayInputStream(mcnFile.getProcessFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream,null, mcnFile.getFileName());
			inputStream.close();

			return streamedContentFile;
		}
		return null;
	}

	/**
	 * Removefile handler.
	 *
	 * @param actionEvent the action event
	 */
	public void removefileHandler(ActionEvent actionEvent) {
		tmpProcessFile.setProcessFile(null);
		tmpProcessFile.setFileName(null);
		if(tmpProcessFile.getTempProcessFile()!=null){
			tmpProcessFile.getTempProcessFile().delete();
			tmpProcessFile.setTempProcessFile(null);
		}
	}
	
	/**
	 * Gets the processed file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the processed file content
	 */
	public StreamedContent getProcessedFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = massiveInterfaceFacade.getInterfaceUploadedFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE));
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	/**
	 * Gets the response file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the response file content
	 */
	public StreamedContent getResponseFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = massiveInterfaceFacade.getInterfaceResponseFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE));
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	

	/**
	 * Validate interface file.
	 *
	 * @throws BeanIOConfigurationException the bean io configuration exception
	 * @throws IllegalArgumentException the illegal argument exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServiceException the service exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void validateInterfaceFile() 
			throws BeanIOConfigurationException, IllegalArgumentException, IOException, ServiceException, ParserConfigurationException {
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveMCN");
		int countValidationErrors = 0;
				
		if(Validations.validateIsNull(tmpProcessFile.getTempProcessFile())){
    		JSFUtilities.addContextMessage("frmMassiveMCN:fuplMcnFile", FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL), 
    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL));
    		
			countValidationErrors++;
    	}
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		
		CommonsUtilities.validateFileOperation(tmpProcessFile);
		 
		ValidationProcessTO mcnValidationTO = tmpProcessFile.getValidationProcessTO();
		
		errorsDataModel = new GenericDataModel<RecordValidationType>(mcnValidationTO==null?new ArrayList<RecordValidationType>():mcnValidationTO.getLstValidations());
		processFile = tmpProcessFile;
	}
	
	/**
	 * Before process interface file.
	 */
	public void beforeProcessInterfaceFile(){	
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_UPLOAD_CONFIRM));
		JSFUtilities.executeJavascriptFunction("PF('cnfwProcessMcnFile').show();");
	}
	
	/**
	 * Process interface file.
	 */
	@LoggerAuditWeb
	public void processInterfaceFile(){	
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(processFile.getIdBusinessProcess());
		
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_FILE, processFile.getTempProcessFile().getAbsolutePath());
			details.put(GeneralConstants.PARAMETER_STREAM, processFile.getStreamFileDir());
			details.put(GeneralConstants.PARAMETER_STREAM_RESPONSE, processFile.getStreamResponseFileDir());
			details.put(GeneralConstants.PARAMETER_LOCALE, "es"); //JSFUtilities.getCurrentLocale().toString());
			details.put(GeneralConstants.PARAMETER_FILE_NAME, processFile.getFileName());
			details.put(GeneralConstants.PARAMETER_ROOT_TAG, processFile.getRootTag());
			
			batchProcessServiceFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_UPLOAD_SUCCESS));
		JSFUtilities.showSimpleValidationDialog();
		cleanAll();
	}
	
	/**
	 * Verify on back action.
	 *
	 * @return the string
	 */
	public String verifyOnBackAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("PF('transferOwnershipBackCnfWidget').show();");
			return null;
		}else{
			return "searchMCNOperations";
		}
	}

	/**
	 * Verify on clean action.
	 */
	public void verifyOnCleanAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("PF('transferOwnershipClearCnfWidget').show();");
		}else{
			cleanAll();
		}
	}
	
	
	/**
	 * Checks if is modified form.
	 *
	 * @return true, if is modified form
	 */
	public boolean isModifiedForm(){
		if(Validations.validateIsNotNullAndPositive(processFileFilter.getIdNegotiationMechanismPk()) ||
				Validations.validateIsNotNullAndPositive(processFile.getIdNegotiationMechanismPk()) ||
				Validations.validateIsNotNull(processFile.getTempProcessFile())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Gets the process files data model.
	 *
	 * @return the mcnProcessFilesDataModel
	 */
	public GenericDataModel<ProcessFileTO> getProcessFilesDataModel() {
		return processFilesDataModel;
	}

	/**
	 * Sets the process files data model.
	 *
	 * @param processFilesDataModel the new process files data model
	 */
	public void setProcessFilesDataModel(
			GenericDataModel<ProcessFileTO> processFilesDataModel) {
		this.processFilesDataModel = processFilesDataModel;
	}

	/**
	 * Gets the process file filter.
	 *
	 * @return the mcnProcessFileFilter
	 */
	public ProcessFileTO getProcessFileFilter() {
		return processFileFilter;
	}

	/**
	 * Sets the process file filter.
	 *
	 * @param mcnProcessFileFilter            the mcnProcessFileFilter to set
	 */
	public void setProcessFileFilter(ProcessFileTO mcnProcessFileFilter) {
		this.processFileFilter = mcnProcessFileFilter;
	}

	/**
	 * Gets the process file.
	 *
	 * @return the mcnProcessFile
	 */
	public ProcessFileTO getProcessFile() {
		return processFile;
	}

	/**
	 * Sets the process file.
	 *
	 * @param mcnProcessFile            the mcnProcessFile to set
	 */
	public void setProcessFile(ProcessFileTO mcnProcessFile) {
		this.processFile = mcnProcessFile;
	}

	/**
	 * Gets the errors data model.
	 *
	 * @return the errorsDataModel
	 */
	public GenericDataModel<RecordValidationType> getErrorsDataModel() {
		return errorsDataModel;
	}

	/**
	 * Sets the errors data model.
	 *
	 * @param errorsDataModel            the errorsDataModel to set
	 */
	public void setErrorsDataModel(GenericDataModel<RecordValidationType> errorsDataModel) {
		this.errorsDataModel = errorsDataModel;
	}
	
	/**
	 * Gets the f upload file types.
	 *
	 * @return the fUploadFileTypes
	 */
	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	/**
	 * Sets the f upload file types.
	 *
	 * @param fUploadFileTypes the fUploadFileTypes to set
	 */
	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	/**
	 * Gets the f upload file types display.
	 *
	 * @return the fUploadFileTypesDisplay
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/**
	 * Sets the f upload file types display.
	 *
	 * @param fUploadFileTypesDisplay the fUploadFileTypesDisplay to set
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}
	
	/**
	 * Gets the f upload file types pattern.
	 *
	 * @return the fUploadFileTypesPattern
	 */
	public Pattern getfUploadFileTypesPattern() {
		return fUploadFileTypesPattern;
	}

	/**
	 * Sets the f upload file types pattern.
	 *
	 * @param fUploadFileTypesPattern the fUploadFileTypesPattern to set
	 */
	public void setfUploadFileTypesPattern(Pattern fUploadFileTypesPattern) {
		this.fUploadFileTypesPattern = fUploadFileTypesPattern;
	}
	
	/**
	 * Gets the tmp process file.
	 *
	 * @return the tmpMcnProcessFile
	 */
	public ProcessFileTO getTmpProcessFile() {
		return tmpProcessFile;
	}

	/**
	 * Sets the tmp process file.
	 *
	 * @param tmpMcnProcessFile the tmpMcnProcessFile to set
	 */
	public void setTmpProcessFile(ProcessFileTO tmpMcnProcessFile) {
		this.tmpProcessFile = tmpMcnProcessFile;
	}

	/**
	 * Back action.
	 *
	 * @return the string
	 */
	public String backAction() {
		return backAction;
	}

	/**
	 * Gets the title label.
	 *
	 * @return the title label
	 */
	public String getTitleLabel() {
		return titleLabel;
	}

	/**
	 * Sets the title label.
	 *
	 * @param titleLabel the new title label
	 */
	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

}
