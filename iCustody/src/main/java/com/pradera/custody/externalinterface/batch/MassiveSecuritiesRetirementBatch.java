package com.pradera.custody.externalinterface.batch;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;

import com.pradera.commons.configuration.DepositarySetup;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.framework.batchprocess.BatchProcess;
import com.pradera.core.framework.batchprocess.service.JobExecution;
import com.pradera.custody.retirementoperation.facade.RetirementOperationServiceFacade;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.model.component.IdepositarySetup;
import com.pradera.model.process.ProcessLogger;
import com.pradera.model.process.ProcessLoggerDetail;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveSecuritiesRetirementBatch.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@BatchProcess(name="MassiveSecuritiesRetirementBatch")
@RequestScoped
public class MassiveSecuritiesRetirementBatch implements Serializable,JobExecution {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/** The idepositary setup. */
	@Inject @DepositarySetup
	IdepositarySetup idepositarySetup;
	
	/** The retirement operation service facade. */
	@EJB
	RetirementOperationServiceFacade retirementOperationServiceFacade;

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#startJob(com.pradera.model.process.ProcessLogger)
	 */
	@Override
	public void startJob(ProcessLogger processLogger) {
		
		System.out.println("************************************* BATCH SERVICE WAS CALLED ******************************************************");
		
		String mappingFile=null;
		String mappingResponseFile=null;
		String filePath=null;
		String locale=null;
		String fileName=null;
		String rootTag=null;
		
		for(ProcessLoggerDetail detail : processLogger.getProcessLoggerDetails()){
			if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE)){
				filePath = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM)){
				mappingFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_STREAM_RESPONSE)){
				mappingResponseFile = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_LOCALE)){
				locale = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_FILE_NAME)){
				fileName = detail.getParameterValue();
			}else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_ROOT_TAG)){
				rootTag = detail.getParameterValue();
			}
//			else if(detail.getParameterName().equals(GeneralConstants.PARAMETER_PROCESS_TYPE)){
//				processType = Integer.valueOf(detail.getParameterValue());
//			}
		}
		
		try {
			
			ProcessFileTO processFileTO = new ProcessFileTO();
			processFileTO.setStreamFileDir(mappingFile);
			processFileTO.setStreamResponseFileDir(mappingResponseFile);
			processFileTO.setTempProcessFile(new File(filePath));
			processFileTO.setProcessFile(FileUtils.readFileToByteArray(processFileTO.getTempProcessFile()));
			processFileTO.setLocale(new Locale(locale));
			processFileTO.setFileName(fileName);
			processFileTO.setIndAutomaticProcess(GeneralConstants.TWO_VALUE_INTEGER);
			processFileTO.setInterfaceName(ComponentConstant.INTERFACE_EARLY_PAYMENT);
			processFileTO.setRootTag(rootTag);			
			CommonsUtilities.validateFileOperationStruct(processFileTO);			
			retirementOperationServiceFacade.saveMassiveRetirementOperation(processFileTO);
				
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getParametersNotification()
	 */
	@Override
	public Object[] getParametersNotification() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#getDestinationInstitutions()
	 */
	@Override
	public List<Object> getDestinationInstitutions() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.pradera.core.framework.batchprocess.service.JobExecution#sendNotification()
	 */
	@Override
	public boolean sendNotification() {
		// TODO Auto-generated method stub
		return true;
	}

}
