package com.pradera.custody.externalinterface.view;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.exception.control.event.ExceptionToCatchEvent;
import org.beanio.BeanIOConfigurationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.GenericPropertiesConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.core.framework.notification.facade.NotificationServiceFacade;
import com.pradera.custody.dematerializationcertificate.facade.DematerializationCertificateFacade;
import com.pradera.custody.dematerializationcertificate.to.RegisterAccountAnnotationTO;
import com.pradera.custody.physicalcertificate.facade.CertificateDepositeServiceFacade;
import com.pradera.custody.utils.view.PropertiesConstants;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationCupon;
import com.pradera.integration.common.validation.to.ValidationAccountAnnotationOperation;
import com.pradera.integration.common.validation.to.ValidationProcessTO;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.HolderAccountBank;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStateBankType;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountStatusType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationCupon;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.funds.InstitutionBankAccount;
import com.pradera.model.funds.type.InstitutionBankAccountsType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.CurrencyType;
import com.pradera.model.generalparameter.type.PeriodType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.type.CalendarDayType;
import com.pradera.model.issuancesecuritie.type.InterestPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestPeriodicityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.process.BusinessProcess;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MassiveInterfaceMgmtBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@DepositaryWebBean
@LoggerCreateBean
public class MassiveInterfaceMgmtBean extends GenericBaseBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The upload file types. */
	private String fUploadFileTypes = "xls|xlsx";
	
	/** The upload file types pattern. */
	private Pattern fUploadFileTypesPattern=Pattern.compile("([^\\s]+(\\.(?i)("+fUploadFileTypes+"))$)");
	
	/** The upload file types display. */
	private String fUploadFileTypesDisplay="*.xml";

	/** The process files data model. */
	private GenericDataModel<ProcessFileTO> processFilesDataModel;
	
	/** The errors data model. */
	private GenericDataModel<RecordValidationType> errorsDataModel;
	
	/** The process file filter. */
	private ProcessFileTO processFileFilter;  // for searches
	
	/** The process file. */
	private ProcessFileTO processFile; // bean sent to Registration Batch 
	
	/** The tmp process file. */
	private ProcessFileTO tmpProcessFile; // temporal bean on upload
	
	/** The back action. */
	private String backAction;
	
	/** The title label. */
	private String titleLabel;
	
	/** The massive interface facade. */
	@EJB
	private GeneralParametersFacade massiveInterfaceFacade;
	
	/** The batch process service facade. */
	@EJB
    private BatchProcessServiceFacade batchProcessServiceFacade;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	@Inject
	DematerializationCertificateFacade dematerializationCertificateFacade;

	@EJB
	private CertificateDepositeServiceFacade certificateDepositeServiceFacade;
	
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	@Inject @Configurable 
	Integer countryResidence;	
	
	@Inject @Configurable 
	Integer departmentDefault;
	
	@Inject @Configurable 
	Integer provinceDefault;
	
	@Inject @Configurable 
	Integer districtDefault;
	
	private boolean blParticipant;
	
	private Long idParticipantPk;
	
	private List<Participant> lstParticipant;
	
	private Participant participantSelected;

	private Integer excelTypeId;
	
	@Inject
	AccountsFacade accountsFacade;

	@EJB
	NotificationServiceFacade notificationServiceFacade; 
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFile = null;
		

		lstRegisterAccountAnnotationTO = null;
		errorsQuantity = 0;
		sucessQuantity = 0;
		
		tmpProcessFile = null;
		
		try {
			loadParticipantDefault();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void loadParticipantDefault() throws ServiceException {
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()){
			
			blParticipant = true;
			participantSelected = dematerializationCertificateFacade.getParticipant(userInfo.getUserAccountSession().getParticipantCode());
			idParticipantPk = userInfo.getUserAccountSession().getParticipantCode();
			lstParticipant = new ArrayList<Participant>();
			lstParticipant.add(participantSelected);
			
		}else {
			
			blParticipant = false;
			Participant filterPar = new Participant();
			filterPar.setState(ParticipantStateType.REGISTERED.getCode());
			lstParticipant = accountsFacade.getLisParticipantServiceBean(filterPar);
			
		}
		
	}
	
	public Participant getParticipantWithPk(Long idParticipantePk) {
		for(Participant participant: lstParticipant) {
			if(participant.getIdParticipantPk().equals(idParticipantePk)) {
				participantSelected = participant;
				break;
			}
		}
		
		return participantSelected;
	}
	
	/**
	 * Load interface params.
	 *
	 * @param interfaceName the interface name
	 * @param backAction the back action
	 */
	public void loadInterfaceParams(String interfaceName, String backAction){
		if(interfaceName!=null){
			
			processFileFilter.setInterfaceName(interfaceName);
			
			this.tmpProcessFile = massiveInterfaceFacade.getExternalInterfaceInformation(interfaceName);
			
			if(ComponentConstant.INTERFACE_EARLY_PAYMENT.equals(interfaceName)){
				this.titleLabel = PropertiesUtilities.getGenericMessage(PropertiesConstants.MSG_RETIREMENT_SECURITIES_MASSIVE_LABEL);
			}else if(ComponentConstant.INTERFACE_ACCOUNT_ENTRY.equals(interfaceName) || ComponentConstant.INTERFACE_ACCOUNT_ENTRY_MASSIVE.equals(interfaceName)){
				this.titleLabel = PropertiesUtilities.getMessage(PropertiesConstants.ACCOUNT_ANNOTATION_MASSIVE_LABEL);
			}
			
		}
		this.backAction = backAction;

		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFile = null;
		

		lstRegisterAccountAnnotationTO = null;
		errorsQuantity = 0;
		sucessQuantity = 0;
		
		tmpProcessFile = null;
	}

	/**
	 * On change process date.
	 */
	public void onChangeProcessDate() {
		processFilesDataModel = null;
		processFile = null;
	}
	
	/**
	 * Clean all.
	 */
	public void cleanAll() {
		JSFUtilities.hideGeneralDialogues();
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveInterface");
		String interfaceName = processFileFilter.getInterfaceName();
		processFileFilter = new ProcessFileTO();
		processFileFilter.setProcessDate(CommonsUtilities.currentDate());
		processFileFilter.setInterfaceName(interfaceName);
		
		processFile = null;
		processFilesDataModel = null;
		

		lstRegisterAccountAnnotationTO = null;
		errorsQuantity = 0;
		sucessQuantity = 0;
		
		tmpProcessFile = null;
	}

	/**
	 * Search processed files.
	 */
	public void searchProcessedFiles() {
		List<ProcessFileTO> mcnProcessFiles = massiveInterfaceFacade.getProcessedFilesInformation(processFileFilter.getProcessDate(),processFileFilter.getInterfaceName());
		processFilesDataModel = new GenericDataModel<ProcessFileTO>(mcnProcessFiles);
	}
	
	/**
	 * Gets the file stream content.
	 *
	 * @param mcnFile the mcn file
	 * @return the file stream content
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public StreamedContent getFileStreamContent(ProcessFileTO mcnFile) throws IOException{
		if(mcnFile!=null){
			
			InputStream inputStream = new ByteArrayInputStream(mcnFile.getProcessFile());
			StreamedContent streamedContentFile = new DefaultStreamedContent(inputStream,null, mcnFile.getFileName());
			inputStream.close();

			return streamedContentFile;
		}
		return null;
	}

	/**
	 * Removefile handler.
	 *
	 * @param actionEvent the action event
	 */
	public void removefileHandler(ActionEvent actionEvent) {
		tmpProcessFile.setProcessFile(null);
		tmpProcessFile.setFileName(null);
		if(tmpProcessFile.getTempProcessFile()!=null){
			tmpProcessFile.getTempProcessFile().delete();
			tmpProcessFile.setTempProcessFile(null);
		}
	}
	
	/**
	 * Gets the processed file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the processed file content
	 */
	public StreamedContent getProcessedFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = massiveInterfaceFacade.getInterfaceUploadedFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE));
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	
	/**
	 * Gets the response file content.
	 *
	 * @param mcnfile the mcnfile
	 * @return the response file content
	 */
	public StreamedContent getResponseFileContent(ProcessFileTO mcnfile) {
		try {
			
			byte[] file = massiveInterfaceFacade.getInterfaceResponseFileContent(mcnfile.getIdProcessFilePk());
			
			return getStreamedContentFromFile(file, null,mcnfile.getFileName());
		} catch (Exception e) {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), 
				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_ERROR_DOWNLOADING_FILE));
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
		}
		return null;
	}
	

	public void fileUploadHandler(FileUploadEvent event) {
		try {
			String fDisplayName = fUploadValidateXlsFile(event.getFile(), null, null,null);//fUploadFileTypes
			if (fDisplayName != null) {
				tmpProcessFile = new ProcessFileTO();
				tmpProcessFile.setFileName(fDisplayName);
				tmpProcessFile.setProcessFile(event.getFile().getContents());
				tmpProcessFile.setProcessInputStream(event.getFile().getInputstream());
				//tmpProcessFile.setTempProcessFile(File.createTempFile("tempMcnfile", ".tmp"));
				//FileUtils.writeByteArrayToFile(tmpProcessFile.getTempProcessFile(), tmpProcessFile.getProcessFile());
			}
		} catch (Exception e) {
			excepcion.fire(new ExceptionToCatchEvent(e));
		}
	}

	private  List<ValidationAccountAnnotationOperation>  lstAccountAnnotationOperation;
	private List<RegisterAccountAnnotationTO> lstRegisterAccountAnnotationTO;
	private Integer errorsQuantity = 0;
	private Integer sucessQuantity = 0;
	private Integer totalRows = 0;

	@EJB
	private GeneralParametersFacade generalParameterFacade;
	
	public void validateInterfaceFile() 
			throws BeanIOConfigurationException, IllegalArgumentException, IOException, ServiceException, ParserConfigurationException {
		JSFUtilities.setValidViewComponentAndChildrens(":frmMassiveInterface");
		int countValidationErrors = 0;
				
		if(tmpProcessFile == null || Validations.validateIsNullOrEmpty(tmpProcessFile.getProcessInputStream())){
    		JSFUtilities.addContextMessage("frmMassiveInterface:fuplMcnFile", FacesMessage.SEVERITY_ERROR,
    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL), 
    				PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_FILE_IS_NULL));
    		
			countValidationErrors++;
    	}
		
		if(countValidationErrors > 0) {
    		showMessageOnDialog(null,null,PropertiesConstants.MESSAGE_DATA_REQUIRED,null);
    		JSFUtilities.showComponent("cnfMsgRequiredValidation");
    		return;
    	}
		
		try {
			lstRegisterAccountAnnotationTO = null;
			errorsQuantity = 0;
			sucessQuantity = 0;
			
			List<ValidationAccountAnnotationOperation>  lstOperations = null;
			if( excelTypeId== 0) {
				lstOperations = massiveInterfaceFacade.setXLSXToEntityAccountAnnotationCDA(tmpProcessFile.getProcessInputStream(), idParticipantPk);
				totalRows = lstOperations.size();
			}else if(excelTypeId== 1) {
				lstOperations = massiveInterfaceFacade.setXLSXToEntityAccountAnnotationPagares(tmpProcessFile.getProcessInputStream(), idParticipantPk);
				totalRows = lstOperations.size();
			}else if(excelTypeId== 2) {
				lstOperations = massiveInterfaceFacade.setXLSXToEntityAccountAnnotationAcciones(tmpProcessFile.getProcessInputStream(), idParticipantPk);
				totalRows = lstOperations.size();
			}else {
				lstOperations = massiveInterfaceFacade.setXLSXToEntityAccountAnnotationGarantias(tmpProcessFile.getProcessInputStream(), idParticipantPk);
				totalRows = lstOperations.size();
			}
			
			lstAccountAnnotationOperation = lstOperations;
			
			if( excelTypeId== 0) {
				lstRegisterAccountAnnotationTO = setListAccountAnnotationOperationEntityCDA(lstOperations);
			}else if(excelTypeId== 1) {
				lstRegisterAccountAnnotationTO = setListAccountAnnotationOperationEntityPagares(lstOperations);
			}else if(excelTypeId== 2) {
				lstRegisterAccountAnnotationTO = setListAccountAnnotationOperationEntityAcciones(lstOperations);
			}else {//2
				lstRegisterAccountAnnotationTO = setListAccountAnnotationOperationEntityGarantias(lstOperations);
			}
			for(RegisterAccountAnnotationTO registerAccountAnnotationTO: lstRegisterAccountAnnotationTO) {
				List<AccountAnnotationCupon> cupones = registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons();
				
				Integer periodicity = registerAccountAnnotationTO.getAccountAnnotationOperation().getPeriodicity();
				Date paymentDate = null;
				Integer cantidadDiasARestar = 0;
				Integer cantidadDiasAPagar = 0;
				Date begingDateCalc = null;
				//BigDecimal nroDecupones = BigDecimal.ZERO;
				BigDecimal interestRate = BigDecimal.ZERO;
				/*
				if(cupones!=null && cupones.size()>0) {
					nroDecupones = new BigDecimal(cupones.get(cupones.size()-1).getCuponNumber());
					if( anotacion.getAccountAnnotationOperation().getSecurityInterestRate() != null ) {
						interestRate = anotacion.getAccountAnnotationOperation().getSecurityInterestRate().divide(nroDecupones);
					}
				}*/

				Date lastExpirationDate = null;
				if(cupones!=null && cupones.size()>0) {
					for(int i=0; i < cupones.size(); i++) {
						cantidadDiasARestar =  CommonsUtilities.daysDefaultWithFixedPeridiocity(periodicity);
						paymentDate = generalParameterFacade.workingDateCalculateServiceFacade( cupones.get(i).getExpirationDate(), GeneralConstants.ONE_VALUE_INTEGER); 
						begingDateCalc = CommonsUtilities.addDaysToDate(cupones.get(i).getExpirationDate(), cantidadDiasARestar*-1);
						cupones.get(i).setPaymentDate(paymentDate);
	
						cupones.get(i).setRegistryDate(cupones.get(i).getExpirationDate());
						
						if( cupones.get(i).getCuponNumber() == 1 ){
							cupones.get(i).setBeginingDate(registerAccountAnnotationTO.getAccountAnnotationOperation().getExpeditionDate());
						}else {
							/* if (anotacion.getAccountAnnotationOperation().getExpeditionDate() != null &&
								 anotacion.getAccountAnnotationOperation().getExpeditionDate().after(begingDateCalc)
							 ) {
								 cupones.get(i).setBeginingDate(anotacion.getAccountAnnotationOperation().getExpeditionDate());
							 }else {*/
							if(lastExpirationDate == null) {
								cupones.get(i).setBeginingDate( CommonsUtilities.addDaysToDate(cupones.get(i).getExpirationDate(),cantidadDiasARestar*-1) );	//begingDateCalc
							}else {
								cupones.get(i).setBeginingDate(lastExpirationDate);	//begingDateCalc	
							}
							// }
						}
						cantidadDiasAPagar = CommonsUtilities.getDaysBetween(cupones.get(i).getBeginingDate(), cupones.get(i).getExpirationDate());
						cupones.get(i).setPaymentDays(cantidadDiasAPagar);
						
	
						BigDecimal rate=registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate().divide(BigDecimal.valueOf(100),MathContext.DECIMAL128);
						BigDecimal factor=null;
						factor=rate.divide( new BigDecimal(CalendarDayType._365.getIntegerValue()),MathContext.DECIMAL128 );
						factor=factor.multiply( BigDecimal.valueOf(cantidadDiasAPagar) );
						factor=factor.multiply(GeneralConstants.HUNDRED_VALUE_BIGDECIMAL);
						interestRate = factor;
						
						if(interestRate.compareTo(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate()) ==1 ) {
							cupones.get(i).setInterestRate(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityInterestRate());
						}else {
							cupones.get(i).setInterestRate(interestRate);	
						}
						
						lastExpirationDate = cupones.get(i).getExpirationDate();
					}
				}
				
				if(excelTypeId != 2) {
					
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons()!=null) {
						//regla de cupones mayor a los 3 dias siguientes
						List<AccountAnnotationCupon> lstCuponsAvailables = new ArrayList<AccountAnnotationCupon>();

						Integer diasHabiles = 3;
						
						ParameterTableTO filter = new ParameterTableTO();
						if(Validations.validateIsNotNullAndNotEmpty(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive())) {
							if(registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
								filter.setParameterTablePk(2911);
							else
								filter.setParameterTablePk(2912);
						}else
							filter.setParameterTablePk(2912);
						List<ParameterTable> param = parameterServiceBean.getListParameterTableServiceBean(filter);
						if(param!=null && param.size()>0) {
							diasHabiles = param.get(0).getShortInteger();
						}
						
						Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles);
						for(AccountAnnotationCupon cupon: registerAccountAnnotationTO.getAccountAnnotationOperation().getAccountAnnotationCupons()) {
							if( cupon.getExpirationDate().after(maxAvailableDate) ) {
								lstCuponsAvailables.add(cupon);
							}else {
								
							}
						}
						registerAccountAnnotationTO.getAccountAnnotationOperation().setAccountAnnotationCupons(lstCuponsAvailables);
	
						if( !(lstCuponsAvailables!=null && lstCuponsAvailables.size()>0) ) {
							registerAccountAnnotationTO.getAccountAnnotationOperation().setMsgMassiveErrors(registerAccountAnnotationTO.getAccountAnnotationOperation().getMsgMassiveErrors()+", No hay Cupones que coincidan con los datos del Capital");
							errorsQuantity += 1;
							
							if(sucessQuantity>1) {
								sucessQuantity -= 1;
							}
						}
					}else {
						registerAccountAnnotationTO.getAccountAnnotationOperation().setMsgMassiveErrors(registerAccountAnnotationTO.getAccountAnnotationOperation().getMsgMassiveErrors()+ ", No hay Cupones que coincidan con los datos del Capital");
						errorsQuantity += 1;
						
						if(sucessQuantity>1) {
							sucessQuantity -= 1;
						}
					}
					
				}
				
				if(excelTypeId== 2) {
					//POR REGLA SE DEJARA PASAR LOS REGISTROS REPETIDOS PARA ACCIONES, Y CAVAPY TOMARA LA DESICION DE QUE HARA, SI RECHAZA ESTE NUEVO INGRESO DE ACCIONES, O SI REVIERTE LA ACCION DE LAS MISMAS CARACTERISTICAS YA INGRESADA
					/*
					if(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurity() != null ) {
						
						AccountAnnotationOperation accountAnnotationOperation = certificateDepositeServiceFacade.findAccountAnnotationOperationAcciones(registerAccountAnnotationTO.getAccountAnnotationOperation());
						if(accountAnnotationOperation != null) {
							registerAccountAnnotationTO.getAccountAnnotationOperation().setMsgMassiveErrors("Ya se encuentra registrado una accion con las mismas caracteristicas, Verifique por favor");
							errorsQuantity += 1;
						}else {
							List<AccountAnnotationOperation> lstAccountAnnotationOperation = certificateDepositeServiceFacade.lstAccountAnnotationOperationAccionesAllFromTo(registerAccountAnnotationTO.getAccountAnnotationOperation());
							if(lstAccountAnnotationOperation != null) {
								for(AccountAnnotationOperation current: lstAccountAnnotationOperation) {
									
									if(registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() !=null 
									&& registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() != null) {
									
										if( ( current.getCertificateFrom() <= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() 
											&&  current.getCertificateTo() >= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateFrom() ) 
											|| 	
											( current.getCertificateFrom() <= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() 
											&&  current.getCertificateTo() >= registerAccountAnnotationTO.getAccountAnnotationOperation().getCertificateTo() ) 
										) {
											
											registerAccountAnnotationTO.getAccountAnnotationOperation().setMsgMassiveErrors(", se encontraron acciones dentro del desde-hasta de otro titulo, Verifique por favor");
											errorsQuantity += 1;
										}
									}
								}
							}
						}
					}*/
					
				}else {

					AccountAnnotationOperation accountAnnotationOperation = certificateDepositeServiceFacade.findAccountAnnotationOperation(registerAccountAnnotationTO.getAccountAnnotationOperation());
					if(accountAnnotationOperation != null) {
						String messageError = (registerAccountAnnotationTO.getAccountAnnotationOperation().getMsgMassiveErrors()!=null)?registerAccountAnnotationTO.getAccountAnnotationOperation().getMsgMassiveErrors():"";
						registerAccountAnnotationTO.getAccountAnnotationOperation().setMsgMassiveErrors(messageError+", Ya se encuentra registrado un titulo con las mismas caracteristicas, Verifique por favor");
						errorsQuantity += 1;
						
						if(sucessQuantity>1) {
							sucessQuantity -= 1;
						}
					}
					//else 
					//{
						/*
						if(!registerAccountAnnotationTO.getAccountAnnotationOperation().getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode())) {
							Boolean blTypeCurrency = BooleanType.NO.getBooleanValue();
							List<HolderAccountBank> lsHolderAccountBank = certificateDepositeServiceFacade.findHolderAccountBank(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk(), HolderAccountStateBankType.REGISTERED.getCode());
							
							if(Validations.validateIsNotNull(lsHolderAccountBank)) {
								for (HolderAccountBank holderAccountBank : lsHolderAccountBank) {
									if (holderAccountBank.getHolderAccount().getIndPdd() == null || holderAccountBank.getHolderAccount().getIndPdd() == 0) {
										if(holderAccountBank.getCurrency().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityCurrency())) {
											blTypeCurrency = BooleanType.YES.getBooleanValue();
										}
									}else {
										List<InstitutionBankAccount> lsInstitutionBankAccount = certificateDepositeServiceFacade.findInstitutionBankAccount(registerAccountAnnotationTO.getAccountAnnotationOperation().getHolderAccount().getIdHolderAccountPk(), InstitutionBankAccountsType.STATE.getCode());
										if (Validations.validateIsNotNull(lsInstitutionBankAccount)) {
											for (InstitutionBankAccount institutionBankAccount : lsInstitutionBankAccount) {
												if(institutionBankAccount.getCurrency().equals(registerAccountAnnotationTO.getAccountAnnotationOperation().getSecurityCurrency())) {
													blTypeCurrency = BooleanType.YES.getBooleanValue();
												}
											}
										}
									}
								}
								
							}
							
							if (!blTypeCurrency) {
								String messageError = (registerAccountAnnotationTO.getAccountAnnotationOperation().getMsgMassiveErrors()!=null)?registerAccountAnnotationTO.getAccountAnnotationOperation().getMsgMassiveErrors():"";
								registerAccountAnnotationTO.getAccountAnnotationOperation().setMsgMassiveErrors(messageError+", No posee cuenta bancaria registrada en la moneda de la operación");
							}
						}*/
						
					//}
					
				}
				
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//CommonsUtilities.convertAccountAnnotationOperationXLStoXML(tmpProcessFile);
		 
		ValidationProcessTO mcnValidationTO = tmpProcessFile.getValidationProcessTO();
		
		errorsDataModel = new GenericDataModel<RecordValidationType>(mcnValidationTO==null?new ArrayList<RecordValidationType>():mcnValidationTO.getLstValidations());
		processFile = tmpProcessFile;
	}
	
	
	public List<RegisterAccountAnnotationTO> setListAccountAnnotationOperationEntityGarantias(List<ValidationAccountAnnotationOperation>  lstOperations){
		
		List<RegisterAccountAnnotationTO>  lstAccountAnnotationOperation = null;
		if(lstOperations!=null) {
			RegisterAccountAnnotationTO accountAnnotationOperationTO = null;
			AccountAnnotationOperation accountAnnotationOperation = null;
			Participant participant = null;
			HolderAccount holderAccount = null;
			Issuer issuer = null;
			String messagesError = "";
			lstAccountAnnotationOperation = new ArrayList<RegisterAccountAnnotationTO>();
			
			for (ValidationAccountAnnotationOperation validationOperation: lstOperations) {
				
				validationOperation.setSecurityInterestType("FIJO");
				
				messagesError = "";

				accountAnnotationOperationTO = new RegisterAccountAnnotationTO();
				accountAnnotationOperation = new AccountAnnotationOperation();
				
				accountAnnotationOperationTO.setSecurityClass(SecurityClassType.FCT.getCode());
				accountAnnotationOperationTO.setDepartment(departmentDefault);
				accountAnnotationOperationTO.setProvince(provinceDefault);
				accountAnnotationOperationTO.setMunicipality(districtDefault);
				accountAnnotationOperationTO.setBlLien(false);

				accountAnnotationOperation.setSecurityClass(SecurityClassType.FCT.getCode());
				accountAnnotationOperation.setSecurityClassDescription(SecurityClassType.FCT.getValue());
				accountAnnotationOperation.setBranchOffice(2735);
				accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
				accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
				accountAnnotationOperation.setTotalBalance(BigDecimal.ONE);
				accountAnnotationOperation.setSecurityInterestType(InterestType.FIXED.getCode());
				accountAnnotationOperation.setSecurityInterestTypeDesc(InterestType.FIXED.getValue());
				accountAnnotationOperation.setIndSerializable(BooleanType.NO.getCode());
				accountAnnotationOperation.setSecurity(new Security());
				accountAnnotationOperation.getSecurity().setSecurityClass(accountAnnotationOperation.getSecurityClass());
				accountAnnotationOperation.setIndDematerialization(0);
				accountAnnotationOperation.setIndImmobilization(1);
				accountAnnotationOperation.setReferencePartCode(validationOperation.getObservation());
				
				/*try {
					if(validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() != null) {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getValue());
					}else if( (validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() == null) ||  
							  (validationOperation.getAmountRate() == null && validationOperation.getParticipantSeller() != null)
							 ){
						messagesError = messagesError+", Existe errores en los campos de Ingreso OTC";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Error en el ingreso por OTC";
				}*/
				
				try {
					if(!validationOperation.getMotive().equals("")) {
						if(validationOperation.getMotive().equals("INMOVILIZACION")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA ADMINISTRADA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_MANAGED.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA EXCLUSIVA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getValue());
						}
					}else {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
					}
				} catch (Exception e) {
					messagesError = messagesError+",motivo";
				}
				
				try {
					participant = certificateDepositeServiceFacade.findParticipant(new BigDecimal(validationOperation.getParticipant()).longValue());
					accountAnnotationOperationTO.setIdParticipantPk(participant.getIdParticipantPk());
					accountAnnotationOperationTO.setParticipant(participant);
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Participante";
				}

				try {
					Integer holderAccountNumber = (new BigDecimal(validationOperation.getHolderAccountNumber())).intValue();
					holderAccount = certificateDepositeServiceFacade.getHoldetAccount(holderAccountNumber, participant.getIdParticipantPk());
					accountAnnotationOperation.setHolderAccount(holderAccount);
					accountAnnotationOperation.getHolderAccount().setParticipant(participant);
					
					if ( !HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()) ) {
						messagesError = messagesError+", La Cuenta Titular no esta Activa";
					}
			
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Cuenta Holder";
				}
				
				try {
					issuer = certificateDepositeServiceFacade.findIssuerWithNemonic(validationOperation.getMnemonicIssuer());
					if(issuer == null) {
						messagesError = messagesError+", Emisor";
					}
					accountAnnotationOperation.getSecurity().setIssuer(issuer);
					accountAnnotationOperation.setIssuer(issuer);
				} catch (Exception e) {
					issuer = null;
					messagesError = messagesError+", Emisor";
				}
				
				try {
					if(!validationOperation.getCertificateNumber().equals("")) {

						Integer validNumber = Integer.valueOf(validationOperation.getCertificateNumber());
						if( validNumber > 0 ) {
							accountAnnotationOperation.setCertificateNumber(validationOperation.getCertificateNumber());
						}else {
							messagesError = messagesError+", Certificado ";
						}
					}
					
				} catch (NumberFormatException e) {
					messagesError = messagesError+", Certificado (Debe ser numerico)";
				} catch (Exception e) {
					messagesError = messagesError+", Certificado";
				}
				
				try {
					if(!validationOperation.getSerialNumber().equals("")) {
						accountAnnotationOperation.setSerialNumber(validationOperation.getSerialNumber().toUpperCase());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Serie";
				}
				
				try {
					if(!validationOperation.getSecurityNominalValue().equals("")) {
						BigDecimal nominalValue = new BigDecimal(validationOperation.getSecurityNominalValue());
						accountAnnotationOperation.setSecurityNominalValue(nominalValue); //();
						
						BigDecimal shareCapital = nominalValue.multiply(accountAnnotationOperation.getTotalBalance());
						accountAnnotationOperationTO.setShareCapital(shareCapital);

						accountAnnotationOperation.getSecurity().setCashNominal(nominalValue.intValue());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Valor Nominal";
				}
				
				try {
					if(!validationOperation.getExpeditionDate().equals("")) {
						accountAnnotationOperation.setExpeditionDate(CommonsUtilities.convertStringtoDate(validationOperation.getExpeditionDate()));
						Calendar expeditionDate = Calendar.getInstance();
						expeditionDate.setTime(accountAnnotationOperation.getExpeditionDate());
						
						/*if( CommonsUtilities.isWeekend(expeditionDate) ) {
							messagesError = messagesError+", Fecha Emision no puede ser fin de semana";
						}*/
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Emision";
				}

				try {
					if(!validationOperation.getSecurityExpirationDate().equals("")) {
						accountAnnotationOperation.setSecurityExpirationDate(CommonsUtilities.convertStringtoDate(validationOperation.getSecurityExpirationDate()));

						Integer diasHabiles = 3;
						
						ParameterTableTO filter = new ParameterTableTO();
						if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation.getMotive())) {
							if(accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
								filter.setParameterTablePk(2911);
							else
								filter.setParameterTablePk(2912);
						}else
							filter.setParameterTablePk(2912);
						List<ParameterTable> param = parameterServiceBean.getListParameterTableServiceBean(filter);
						if(param!=null && param.size()>0) {
							diasHabiles = param.get(0).getShortInteger();
						}
						
						Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles); 
						if( maxAvailableDate.after(accountAnnotationOperation.getSecurityExpirationDate()) ) {
							messagesError = messagesError+", No puede registrarse titulos cuya fecha vencimiento este dentro de los "+diasHabiles+" dias siguientes ";
						}
						/*
						Calendar expirationDate = Calendar.getInstance();
						expirationDate.setTime(accountAnnotationOperation.getSecurityExpirationDate());
						
						if( CommonsUtilities.isWeekend(expirationDate) ) {
							messagesError = messagesError+", Fecha Vencimiento no puede ser fin de semana";
						}*/
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Vencimiento";
				}

				try {
					if(!validationOperation.getSecurityCurrency().equals("")) {
						if(CurrencyType.PYG.getCodeIso().equalsIgnoreCase(validationOperation.getSecurityCurrency()) || ("GS").equalsIgnoreCase(validationOperation.getSecurityCurrency()) ) {
							accountAnnotationOperation.setSecurityCurrency(CurrencyType.PYG.getCode());
							accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.PYG.getValue());
						}else if(CurrencyType.USD.getCodeIso().equalsIgnoreCase(validationOperation.getSecurityCurrency())) {
							accountAnnotationOperation.setSecurityCurrency(CurrencyType.USD.getCode());
							accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.USD.getValue());
						} 
						accountAnnotationOperation.getSecurity().setCurrency(accountAnnotationOperation.getSecurityCurrency());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Moneda";
				}

				try {
					if(!validationOperation.getSecurityInterestRate().equals("")) {
						BigDecimal interestRate = new BigDecimal(validationOperation.getSecurityInterestRate());
						accountAnnotationOperation.setSecurityInterestRate(interestRate); 
						accountAnnotationOperation.getSecurity().setInterestRate(interestRate);
					}
				} catch (Exception e) {
					messagesError = messagesError+", Tasa Interes Anual";
				}

				try {
					if(!validationOperation.getPeriodicity().equals("")) {
						
						if(validationOperation.getPeriodicity().endsWith(InterestPaymentModalityType.AT_MATURITY.getValue())) {
							accountAnnotationOperation.setPeriodicity(InterestPaymentModalityType.AT_MATURITY.getCode());
							accountAnnotationOperation.setPeriodicityDesc(InterestPaymentModalityType.AT_MATURITY.getValue());
						}else {
							Integer peridicityType = InterestPeriodicityType.getWitgDescription(validationOperation.getPeriodicity()).getCode();
							accountAnnotationOperation.setPeriodicity(peridicityType);
							accountAnnotationOperation.setPeriodicityDesc(validationOperation.getPeriodicity());
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Peridiocidad";
				}

				try {
					if(!validationOperation.getIndElectronicCupon().equals("")) {
						if(validationOperation.getIndElectronicCupon().equals("SI") || validationOperation.getIndElectronicCupon().equals("S")  || validationOperation.getIndElectronicCupon().equals("1")  ) {
							accountAnnotationOperation.setIndElectronicCupon(1);
							accountAnnotationOperation.setIndElectronicCuponDesc("SI");
						}else{
							accountAnnotationOperation.setIndElectronicCupon(0);
							accountAnnotationOperation.setIndElectronicCuponDesc("NO");
						}
					}else {
						accountAnnotationOperation.setIndElectronicCupon(0);
						accountAnnotationOperation.setIndElectronicCuponDesc("NO");
					}
				} catch (Exception e) {
					messagesError = messagesError+",Cupon Electronico";
				}

				accountAnnotationOperation.setIndTaxExoneration(0);
				accountAnnotationOperation.setIndTaxExonerationDesc("NO");

				try {
					if(validationOperation.getParticipantSeller() != null) {
						Participant participantSell = certificateDepositeServiceFacade.findParticipant(validationOperation.getParticipantSeller());
						accountAnnotationOperationTO.setParticipantSeller(participantSell);
						if(participantSell != null) {
							HolderAccount holderAccountSell = certificateDepositeServiceFacade.getHoldetAccount(1, participantSell.getIdParticipantPk());
							if(holderAccountSell!=null) {
								accountAnnotationOperationTO.setHolderAccountSeller(holderAccountSell);
								accountAnnotationOperation.setHolderAccountSeller(holderAccountSell);
							}else {
								messagesError = messagesError+", Participante Vendedor no tiene un titular con cuenta 1 configurado. ";
							}
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Participante Vendedor";
				}
				

				try {
					if(validationOperation.getAmountRate() != null) {
						BigDecimal amountRate = new BigDecimal(validationOperation.getAmountRate());
						accountAnnotationOperationTO.setAmountRate(amountRate);
						accountAnnotationOperation.setAmountRate(amountRate);
						

						BigDecimal nominalValue = accountAnnotationOperation.getSecurityNominalValue();
						BigDecimal rate = accountAnnotationOperation.getAmountRate();
						accountAnnotationOperation.setCashPrice( rate.multiply(nominalValue).divide(BigDecimal.valueOf(100d)) );
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Precio sucio";
				}
				
				
				setAccountAnnotationCupon(accountAnnotationOperation, validationOperation.getValidationAccountAnnotationCupons());
				messagesError = messagesError+accountAnnotationOperation.getMsgMassiveErrors();

				if(messagesError!=null && !messagesError.equals("")) {
					if( !messagesError.equals("") ) {
						messagesError = "Hubo Problemas en el titulo " + messagesError;
					}
					errorsQuantity += 1;
				}else {
					sucessQuantity += 1;
				}
				
				accountAnnotationOperation.setMsgMassiveErrors(messagesError);
				accountAnnotationOperationTO.setAccountAnnotationOperation(accountAnnotationOperation);
				lstAccountAnnotationOperation.add(accountAnnotationOperationTO);

			}
			
		}
		
		return lstAccountAnnotationOperation;
	}

	public List<RegisterAccountAnnotationTO> setListAccountAnnotationOperationEntityPagares(List<ValidationAccountAnnotationOperation>  lstOperations){
		
		List<RegisterAccountAnnotationTO>  lstAccountAnnotationOperation = null;
		if(lstOperations!=null) {
			RegisterAccountAnnotationTO accountAnnotationOperationTO = null;
			AccountAnnotationOperation accountAnnotationOperation = null;
			Participant participant = null;
			HolderAccount holderAccount = null;
			Issuer issuer = null;
			String messagesError = "";
			lstAccountAnnotationOperation = new ArrayList<RegisterAccountAnnotationTO>();
			
			for (ValidationAccountAnnotationOperation validationOperation: lstOperations) {
				
				validationOperation.setSecurityInterestType("FIJO");
				
				messagesError = "";

				accountAnnotationOperationTO = new RegisterAccountAnnotationTO();
				accountAnnotationOperation = new AccountAnnotationOperation();
				
				accountAnnotationOperationTO.setSecurityClass(SecurityClassType.PGS.getCode());
				accountAnnotationOperationTO.setDepartment(departmentDefault);
				accountAnnotationOperationTO.setProvince(provinceDefault);
				accountAnnotationOperationTO.setMunicipality(districtDefault);
				accountAnnotationOperationTO.setBlLien(false);

				accountAnnotationOperation.setSecurityClass(SecurityClassType.PGS.getCode());
				accountAnnotationOperation.setSecurityClassDescription(SecurityClassType.PGS.getValue());
				accountAnnotationOperation.setBranchOffice(2735);
				accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
				accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
				accountAnnotationOperation.setTotalBalance(BigDecimal.ONE);
				accountAnnotationOperation.setSecurityInterestType(InterestType.FIXED.getCode());
				accountAnnotationOperation.setSecurityInterestTypeDesc(InterestType.FIXED.getValue());
				accountAnnotationOperation.setIndSerializable(BooleanType.NO.getCode());
				accountAnnotationOperation.setSecurity(new Security());
				accountAnnotationOperation.getSecurity().setSecurityClass(accountAnnotationOperation.getSecurityClass());
				accountAnnotationOperation.setIndDematerialization(0);
				accountAnnotationOperation.setIndImmobilization(1);
				accountAnnotationOperation.setReferencePartCode(validationOperation.getObservation());

				try {
					if(!validationOperation.getMotive().equals("")) {
						if(validationOperation.getMotive().equals("INMOVILIZACION")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA ADMINISTRADA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_MANAGED.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA EXCLUSIVA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getValue());
						}
					}else {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
					}
				} catch (Exception e) {
					messagesError = messagesError+",motivo";
				}
				
				try {
					if(validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() != null) {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getValue());
					}else if( (validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() == null) ||  
							  (validationOperation.getAmountRate() == null && validationOperation.getParticipantSeller() != null)
							 ){
						messagesError = messagesError+", Existe errores en los campos de Ingreso OTC";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Error en el ingreso por OTC";
				}
				
				try {
					participant = certificateDepositeServiceFacade.findParticipant(new BigDecimal(validationOperation.getParticipant()).longValue());
					accountAnnotationOperationTO.setIdParticipantPk(participant.getIdParticipantPk());
					accountAnnotationOperationTO.setParticipant(participant);
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Participante";
				}

				try {
					Integer holderAccountNumber = (new BigDecimal(validationOperation.getHolderAccountNumber())).intValue();
					holderAccount = certificateDepositeServiceFacade.getHoldetAccount(holderAccountNumber, participant.getIdParticipantPk());
					accountAnnotationOperation.setHolderAccount(holderAccount);
					accountAnnotationOperation.getHolderAccount().setParticipant(participant);
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Cuenta Holder";
				}
				

				if(validationOperation.getHolderAccountNumberBenefi()!=null) {
					try {
						Integer holderAccountNumber = (new BigDecimal(validationOperation.getHolderAccountNumberBenefi())).intValue();
						holderAccount = certificateDepositeServiceFacade.getHoldetAccount(holderAccountNumber, participant.getIdParticipantPk());
						accountAnnotationOperation.setHolderAccountBenef(holderAccount);
						accountAnnotationOperation.getHolderAccountBenef().setParticipant(participant);
					} catch (Exception e) {
						participant = null;
						messagesError = messagesError+", Cuenta Beneficiario";
					}
				}
				

				if(validationOperation.getHolderAccountNumberDebtor()!=null) {
					try {
						Integer holderAccountNumber = (new BigDecimal(validationOperation.getHolderAccountNumberDebtor())).intValue();
						holderAccount = certificateDepositeServiceFacade.getHoldetAccount(holderAccountNumber, participant.getIdParticipantPk());
						accountAnnotationOperation.setHolderAccountDebtor(holderAccount);
						accountAnnotationOperation.getHolderAccountDebtor().setParticipant(participant);
					} catch (Exception e) {
						participant = null;
						messagesError = messagesError+", Cuenta Deudor";
					}
				}
				
				
				try {
					issuer = certificateDepositeServiceFacade.findIssuerWithNemonic(validationOperation.getMnemonicIssuer());
					if(issuer == null) {
						messagesError = messagesError+", Emisor";
					}
					accountAnnotationOperation.getSecurity().setIssuer(issuer);
					accountAnnotationOperation.setIssuer(issuer);
				} catch (Exception e) {
					issuer = null;
					messagesError = messagesError+", Emisor";
				}
				
				try {
					if(!validationOperation.getCertificateNumber().equals("")) {

						Integer validNumber = Integer.valueOf(validationOperation.getCertificateNumber());
						if( validNumber > 0 ) {
							accountAnnotationOperation.setCertificateNumber(validationOperation.getCertificateNumber());
						}else {
							messagesError = messagesError+", Certificado ";
						}
					}
					
				} catch (NumberFormatException e) {
					messagesError = messagesError+", Certificado (Debe ser numerico)";
				} catch (Exception e) {
					messagesError = messagesError+", Certificado";
				}
				
				try {
					if(!validationOperation.getSerialNumber().equals("")) {
						accountAnnotationOperation.setSerialNumber(validationOperation.getSerialNumber().toUpperCase());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Serie";
				}
				
				try {
					if(!validationOperation.getSecurityNominalValue().equals("")) {
						BigDecimal nominalValue = new BigDecimal(validationOperation.getSecurityNominalValue());
						accountAnnotationOperation.setSecurityNominalValue(nominalValue); //();
						
						BigDecimal shareCapital = nominalValue.multiply(accountAnnotationOperation.getTotalBalance());
						accountAnnotationOperationTO.setShareCapital(shareCapital);

						accountAnnotationOperation.getSecurity().setCashNominal(nominalValue.intValue());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Valor Nominal";
				}
				
				try {
					if(!validationOperation.getExpeditionDate().equals("")) {
						accountAnnotationOperation.setExpeditionDate(CommonsUtilities.convertStringtoDate(validationOperation.getExpeditionDate()));
						/*Calendar expeditionDate = Calendar.getInstance();
						expeditionDate.setTime(accountAnnotationOperation.getExpeditionDate());
						
						if( CommonsUtilities.isWeekend(expeditionDate) ) {
							messagesError = messagesError+", Fecha Emision no puede ser fin de semana";
						}*/
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Emision";
				}

				try {
					if(!validationOperation.getSecurityExpirationDate().equals("")) {
						accountAnnotationOperation.setSecurityExpirationDate(CommonsUtilities.convertStringtoDate(validationOperation.getSecurityExpirationDate())); 
						
						Integer diasHabiles = 3;
						
						ParameterTableTO filter = new ParameterTableTO();
						if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation.getMotive())) {
							if(accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
								filter.setParameterTablePk(2911);
							else
								filter.setParameterTablePk(2912);
						}else
							filter.setParameterTablePk(2912);
						List<ParameterTable> param = parameterServiceBean.getListParameterTableServiceBean(filter);
						if(param!=null && param.size()>0) {
							diasHabiles = param.get(0).getShortInteger();
						}
						
						Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles); 
						if( maxAvailableDate.after(accountAnnotationOperation.getSecurityExpirationDate()) ) {
							messagesError = messagesError+", No puede registrarse titulos cuya fecha vencimiento este dentro de los "+diasHabiles+" dias siguientes ";
						}
						/*
						Calendar expirationDate = Calendar.getInstance();
						expirationDate.setTime(accountAnnotationOperation.getSecurityExpirationDate());
						
						if( CommonsUtilities.isWeekend(expirationDate) ) {
							messagesError = messagesError+", Fecha Vencimiento no puede ser fin de semana";
						}*/
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Vencimiento";
				}

				try {
					if(!validationOperation.getSecurityCurrency().equals("")) {
						if(CurrencyType.PYG.getCodeIso().equalsIgnoreCase(validationOperation.getSecurityCurrency()) || ("GS").equalsIgnoreCase(validationOperation.getSecurityCurrency()) ) {
							accountAnnotationOperation.setSecurityCurrency(CurrencyType.PYG.getCode());
							accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.PYG.getValue());
						}else if(CurrencyType.USD.getCodeIso().equalsIgnoreCase(validationOperation.getSecurityCurrency())) {
							accountAnnotationOperation.setSecurityCurrency(CurrencyType.USD.getCode());
							accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.USD.getValue());
						} 
						accountAnnotationOperation.getSecurity().setCurrency(accountAnnotationOperation.getSecurityCurrency());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Moneda";
				}

				try {
					if(!validationOperation.getSecurityInterestRate().equals("")) {
						BigDecimal interestRate = new BigDecimal(validationOperation.getSecurityInterestRate());
						accountAnnotationOperation.setSecurityInterestRate(interestRate); 
						accountAnnotationOperation.getSecurity().setInterestRate(interestRate);
					}
				} catch (Exception e) {
					messagesError = messagesError+", Tasa Interes Anual";
				}

				try {
					if(!validationOperation.getPeriodicity().equals("")) {
						
						if(validationOperation.getPeriodicity().endsWith(InterestPaymentModalityType.AT_MATURITY.getValue())) {
							accountAnnotationOperation.setPeriodicity(InterestPaymentModalityType.AT_MATURITY.getCode());
							accountAnnotationOperation.setPeriodicityDesc(InterestPaymentModalityType.AT_MATURITY.getValue());
						}else {
							Integer peridicityType = PeriodType.getWitgDescription(validationOperation.getPeriodicity()).getCode();
							accountAnnotationOperation.setPeriodicity(peridicityType);
							accountAnnotationOperation.setPeriodicityDesc(validationOperation.getPeriodicity());
						}
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Peridiocidad";
				}

				try {
					if(!validationOperation.getTaxExoneration().equals("")) {
						if(validationOperation.getTaxExoneration().equals("SI") || validationOperation.getTaxExoneration().equals("S")  || validationOperation.getTaxExoneration().equals("1")  ) {
							accountAnnotationOperation.setIndTaxExoneration(1);
							accountAnnotationOperation.setIndTaxExonerationDesc("SI");
						}else{
							accountAnnotationOperation.setIndTaxExoneration(0);
							accountAnnotationOperation.setIndTaxExonerationDesc("NO");
						}
					}else {
						accountAnnotationOperation.setIndTaxExoneration(0);
						accountAnnotationOperation.setIndTaxExonerationDesc("NO");
					}
				} catch (Exception e) {
					messagesError = messagesError+",Exoneracion de impuesto";
				}

				accountAnnotationOperation.setIndElectronicCupon(0);
				accountAnnotationOperation.setIndElectronicCuponDesc("NO");
				
				try {
					if(validationOperation.getParticipantSeller() != null) {
						Participant participantSell = certificateDepositeServiceFacade.findParticipant(validationOperation.getParticipantSeller());
						accountAnnotationOperationTO.setParticipantSeller(participantSell);
						if(participantSell != null) {
							HolderAccount holderAccountSell = certificateDepositeServiceFacade.getHoldetAccount(1, participantSell.getIdParticipantPk());
							if(holderAccountSell!=null) {
								accountAnnotationOperationTO.setHolderAccountSeller(holderAccountSell);
								accountAnnotationOperation.setHolderAccountSeller(holderAccountSell);
							}else {
								messagesError = messagesError+", Participante Vendedor no tiene un titular con cuenta 1 configurado. ";
							}
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Participante Vendedor";
				}
				

				try {
					if(validationOperation.getAmountRate() != null) {
						BigDecimal amountRate = new BigDecimal(validationOperation.getAmountRate());
						accountAnnotationOperationTO.setAmountRate(amountRate);
						accountAnnotationOperation.setAmountRate(amountRate);
						

						BigDecimal nominalValue = accountAnnotationOperation.getSecurityNominalValue();
						BigDecimal rate = accountAnnotationOperation.getAmountRate();
						accountAnnotationOperation.setCashPrice( rate.multiply(nominalValue).divide(BigDecimal.valueOf(100d)) );
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Precio sucio";
				}
				
				setAccountAnnotationCupon(accountAnnotationOperation, validationOperation.getValidationAccountAnnotationCupons());
				messagesError = messagesError+accountAnnotationOperation.getMsgMassiveErrors();
				
				if(messagesError!=null && !messagesError.equals("")) {
					if( !messagesError.equals("") ) {
						messagesError = "Hubo Problemas en el titulo " + messagesError;
					}
					errorsQuantity += 1;
				}else {
					sucessQuantity += 1;
				}
				
				accountAnnotationOperation.setMsgMassiveErrors(messagesError);
				
				accountAnnotationOperationTO.setAccountAnnotationOperation(accountAnnotationOperation);
				lstAccountAnnotationOperation.add(accountAnnotationOperationTO);

			}
			
		}
		
		return lstAccountAnnotationOperation;
	}

	public List<RegisterAccountAnnotationTO> setListAccountAnnotationOperationEntityCDA(List<ValidationAccountAnnotationOperation>  lstOperations){
		
		List<RegisterAccountAnnotationTO>  lstAccountAnnotationOperation = null;
		if(lstOperations!=null) {
			RegisterAccountAnnotationTO accountAnnotationOperationTO = null;
			AccountAnnotationOperation accountAnnotationOperation = null;
			Participant participant = null;
			HolderAccount holderAccount = null;
			Issuer issuer = null;
			String messagesError = "";
			lstAccountAnnotationOperation = new ArrayList<RegisterAccountAnnotationTO>();
			
			for (ValidationAccountAnnotationOperation validationOperation: lstOperations) {
				
				validationOperation.setSecurityInterestType("FIJO");
				
				messagesError = "";

				accountAnnotationOperationTO = new RegisterAccountAnnotationTO();
				accountAnnotationOperation = new AccountAnnotationOperation();
				
				accountAnnotationOperationTO.setSecurityClass(SecurityClassType.DPF.getCode());
				accountAnnotationOperationTO.setDepartment(departmentDefault);
				accountAnnotationOperationTO.setProvince(provinceDefault);
				accountAnnotationOperationTO.setMunicipality(districtDefault);
				accountAnnotationOperationTO.setBlLien(false);

				accountAnnotationOperation.setSecurityClass(SecurityClassType.DPF.getCode());
				accountAnnotationOperation.setSecurityClassDescription(SecurityClassType.DPF.getValue());
				accountAnnotationOperation.setBranchOffice(2735);
				accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
				accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
				accountAnnotationOperation.setTotalBalance(BigDecimal.ONE);
				accountAnnotationOperation.setSecurityInterestType(InterestType.FIXED.getCode());
				accountAnnotationOperation.setSecurityInterestTypeDesc(InterestType.FIXED.getValue());
				accountAnnotationOperation.setIndSerializable(BooleanType.NO.getCode());
				accountAnnotationOperation.setSecurity(new Security());
				accountAnnotationOperation.getSecurity().setSecurityClass(accountAnnotationOperation.getSecurityClass());
				accountAnnotationOperation.setIndDematerialization(0);
				accountAnnotationOperation.setIndImmobilization(1);
				accountAnnotationOperation.setReferencePartCode(validationOperation.getObservation());

				
				try {
					if(validationOperation.getMotive()!= null && !validationOperation.getMotive().equals("")) {
						if(validationOperation.getMotive().equals("INMOVILIZACION")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA ADMINISTRADA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_MANAGED.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA EXCLUSIVA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getValue());
						}
					}else {
//						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
//						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
						messagesError = messagesError+", Motivo";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Motivo";
				}
				
				
				try {
					if(validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() != null) {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getValue());
					}else if( (validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() == null) ||  
							  (validationOperation.getAmountRate() == null && validationOperation.getParticipantSeller() != null)
							 ){
						messagesError = messagesError+", Existe errores en los campos de Ingreso OTC";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Error en el ingreso por OTC";
				}
				
				try {
					participant = certificateDepositeServiceFacade.findParticipant(new BigDecimal(validationOperation.getParticipant()).longValue());
					accountAnnotationOperationTO.setIdParticipantPk(participant.getIdParticipantPk());
					accountAnnotationOperationTO.setParticipant(participant);
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Participante";
				}

				try {
					Integer holderAccountNumber = (new BigDecimal(validationOperation.getHolderAccountNumber())).intValue();
					holderAccount = certificateDepositeServiceFacade.getHoldetAccount(holderAccountNumber, participant.getIdParticipantPk());
					accountAnnotationOperation.setHolderAccount(holderAccount);
					accountAnnotationOperation.getHolderAccount().setParticipant(participant);
					
					if ( !HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()) ) {
						messagesError = messagesError+", La Cuenta Titular no esta Activa";
					}
			
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Cuenta Holder";
				}
				
				try {
					issuer = certificateDepositeServiceFacade.findIssuerWithNemonic(validationOperation.getMnemonicIssuer());
					if(issuer == null) {
						messagesError = messagesError+", Emisor";
					}
					accountAnnotationOperation.getSecurity().setIssuer(issuer);
					accountAnnotationOperation.setIssuer(issuer);
				} catch (Exception e) {
					issuer = null;
					messagesError = messagesError+", Emisor";
				}
				
				try {
					if(!validationOperation.getCertificateNumber().equals("")) {

						Integer validNumber = Integer.valueOf(validationOperation.getCertificateNumber());
						if( validNumber > 0 ) {
							accountAnnotationOperation.setCertificateNumber(validationOperation.getCertificateNumber());
						}else {
							messagesError = messagesError+", Certificado ";
						}
					}
					
				} catch (NumberFormatException e) {
					messagesError = messagesError+", Certificado (Debe ser numerico)";
				} catch (Exception e) {
					messagesError = messagesError+", Certificado";
				}
				
				try {
					if(!validationOperation.getSerialNumber().equals("")) {
						accountAnnotationOperation.setSerialNumber(validationOperation.getSerialNumber().toUpperCase());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Serie";
				}
				
				try {
					if(!validationOperation.getSecurityNominalValue().equals("")) {
						BigDecimal nominalValue = new BigDecimal(validationOperation.getSecurityNominalValue());
						accountAnnotationOperation.setSecurityNominalValue(nominalValue); //();
						
						BigDecimal shareCapital = nominalValue.multiply(accountAnnotationOperation.getTotalBalance());
						accountAnnotationOperationTO.setShareCapital(shareCapital);

						accountAnnotationOperation.getSecurity().setCashNominal(nominalValue.intValue());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Valor Nominal";
				}
				
				try {
					if(!validationOperation.getExpeditionDate().equals("")) {
						accountAnnotationOperation.setExpeditionDate(CommonsUtilities.convertStringtoDate(validationOperation.getExpeditionDate()));
						/*Calendar expeditionDate = Calendar.getInstance();
						expeditionDate.setTime(accountAnnotationOperation.getExpeditionDate());
						
						if( CommonsUtilities.isWeekend(expeditionDate) ) {
							messagesError = messagesError+", Fecha Emision no puede ser fin de semana";
						}*/
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Emision";
				}

				try {
					if(!validationOperation.getSecurityExpirationDate().equals("")) {
						accountAnnotationOperation.setSecurityExpirationDate(CommonsUtilities.convertStringtoDate(validationOperation.getSecurityExpirationDate()));

						Integer diasHabiles = 3;
						
						ParameterTableTO filter = new ParameterTableTO();
						if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation.getMotive())) {
							if(accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
								filter.setParameterTablePk(2911);
							else
								filter.setParameterTablePk(2912);
						}else
							filter.setParameterTablePk(2912);
						List<ParameterTable> param = parameterServiceBean.getListParameterTableServiceBean(filter);
						if(param!=null && param.size()>0) {
							diasHabiles = param.get(0).getShortInteger();
						}
						
						Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles); 
						if( maxAvailableDate.after(accountAnnotationOperation.getSecurityExpirationDate()) ) {
							messagesError = messagesError+", No puede registrarse titulos cuya fecha vencimiento este dentro de los "+diasHabiles+" dias siguientes ";
						}
						/*
						Calendar expirationDate = Calendar.getInstance();
						expirationDate.setTime(accountAnnotationOperation.getSecurityExpirationDate());
						
						if( CommonsUtilities.isWeekend(expirationDate) ) {
							messagesError = messagesError+", Fecha Vencimiento no puede ser fin de semana";
						}*/
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Vencimiento";
				}

				try {
					if(!validationOperation.getSecurityCurrency().equals("")) {
						if(CurrencyType.PYG.getCodeIso().equalsIgnoreCase(validationOperation.getSecurityCurrency()) || ("GS").equalsIgnoreCase(validationOperation.getSecurityCurrency()) ) {
							accountAnnotationOperation.setSecurityCurrency(CurrencyType.PYG.getCode());
							accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.PYG.getValue());
						}else if(CurrencyType.USD.getCodeIso().equalsIgnoreCase(validationOperation.getSecurityCurrency())) {
							accountAnnotationOperation.setSecurityCurrency(CurrencyType.USD.getCode());
							accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.USD.getValue());
						} 
						accountAnnotationOperation.getSecurity().setCurrency(accountAnnotationOperation.getSecurityCurrency());
					}
				} catch (Exception e) {
					messagesError = messagesError+", Moneda";
				}

				try {
					if(!validationOperation.getSecurityInterestRate().equals("")) {
						BigDecimal interestRate = new BigDecimal(validationOperation.getSecurityInterestRate());
						accountAnnotationOperation.setSecurityInterestRate(interestRate); 
						accountAnnotationOperation.getSecurity().setInterestRate(interestRate);
					}
				} catch (Exception e) {
					messagesError = messagesError+", Tasa Interes Anual";
				}

				try {
					if(validationOperation.getPeriodicity()!= null && !validationOperation.getPeriodicity().equals("")) {
						
						if(validationOperation.getPeriodicity().endsWith(InterestPaymentModalityType.AT_MATURITY.getValue())) {
							accountAnnotationOperation.setPeriodicity(InterestPaymentModalityType.AT_MATURITY.getCode());
							accountAnnotationOperation.setPeriodicityDesc(InterestPaymentModalityType.AT_MATURITY.getValue());
						}else {
							Integer peridicityType = InterestPeriodicityType.getWitgDescription(validationOperation.getPeriodicity()).getCode();
							accountAnnotationOperation.setPeriodicity(peridicityType);
							accountAnnotationOperation.setPeriodicityDesc(validationOperation.getPeriodicity());
						}
					}else {
						messagesError = messagesError+", Peridiocidad";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Peridiocidad";
				}

				try {
					if(validationOperation.getIndElectronicCupon()!= null && !validationOperation.getIndElectronicCupon().equals("")) {
						if(validationOperation.getIndElectronicCupon().equals("SI") || validationOperation.getIndElectronicCupon().equals("S")  || validationOperation.getIndElectronicCupon().equals("1")  ) {
							accountAnnotationOperation.setIndElectronicCupon(1);
							accountAnnotationOperation.setIndElectronicCuponDesc("SI");
						}else{
							accountAnnotationOperation.setIndElectronicCupon(0);
							accountAnnotationOperation.setIndElectronicCuponDesc("NO");
						}
					}else {
//						accountAnnotationOperation.setIndElectronicCupon(0);
//						accountAnnotationOperation.setIndElectronicCuponDesc("NO");
						messagesError = messagesError+", Cupon Electronico";
					}
				} catch (Exception e) {
					messagesError = messagesError+",Cupon Electronico";
				}

				accountAnnotationOperation.setIndTaxExoneration(1);
				accountAnnotationOperation.setIndTaxExonerationDesc("SI");

				try {
					if(validationOperation.getParticipantSeller() != null) {
						Participant participantSell = certificateDepositeServiceFacade.findParticipant(validationOperation.getParticipantSeller());
						accountAnnotationOperationTO.setParticipantSeller(participantSell);
						if(participantSell != null) {
							HolderAccount holderAccountSell = certificateDepositeServiceFacade.getHoldetAccount(1, participantSell.getIdParticipantPk());
							if(holderAccountSell!=null) {
								accountAnnotationOperationTO.setHolderAccountSeller(holderAccountSell);
								accountAnnotationOperation.setHolderAccountSeller(holderAccountSell);
							}else {
								messagesError = messagesError+", Participante Vendedor no tiene un titular con cuenta 1 configurado. ";
							}
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Participante Vendedor";
				}
				

				try {
					if(validationOperation.getAmountRate() != null) {
						BigDecimal amountRate = new BigDecimal(validationOperation.getAmountRate());
						accountAnnotationOperationTO.setAmountRate(amountRate);
						accountAnnotationOperation.setAmountRate(amountRate);
						

						BigDecimal nominalValue = accountAnnotationOperation.getSecurityNominalValue();
						BigDecimal rate = accountAnnotationOperation.getAmountRate();
						accountAnnotationOperation.setCashPrice( rate.multiply(nominalValue).divide(BigDecimal.valueOf(100d)) );
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Precio sucio";
				}
				
				
				if(lstAccountAnnotationOperation != null && lstAccountAnnotationOperation.size() >0 ) {
					for(RegisterAccountAnnotationTO current : lstAccountAnnotationOperation) {
						
						if( current.getAccountAnnotationOperation().getCertificateNumber()!=null && accountAnnotationOperation.getCertificateNumber() != null &&
							accountAnnotationOperation.getCertificateNumber().equals(current.getAccountAnnotationOperation().getCertificateNumber()) &&
							
							current.getAccountAnnotationOperation().getSerialNumber()!=null && accountAnnotationOperation.getSerialNumber()!=null && 
							accountAnnotationOperation.getSerialNumber().equals(current.getAccountAnnotationOperation().getSerialNumber()) &&
							
							current.getAccountAnnotationOperation().getSecurityCurrency()!=null && accountAnnotationOperation.getSecurityCurrency()!=null && 
							accountAnnotationOperation.getSecurityCurrency().equals(current.getAccountAnnotationOperation().getSecurityCurrency()) &&

							current.getAccountAnnotationOperation().getIssuer()!=null && accountAnnotationOperation.getIssuer()!=null && 
									current.getAccountAnnotationOperation().getIssuer().getIdIssuerPk()!=null && accountAnnotationOperation.getIssuer().getIdIssuerPk()!=null && 
							accountAnnotationOperation.getIssuer().getIdIssuerPk().equals(current.getAccountAnnotationOperation().getIssuer().getIdIssuerPk())
							
						){
							messagesError = messagesError+", Existe mas de un titulo con las mismas caracteristicas dentro del archivo. ";
							//errorsQuantity += 1;
							break;
						}
						
					}
				}
				
				if(validationOperation.getValidationAccountAnnotationCupons() != null && validationOperation.getValidationAccountAnnotationCupons().size()>0) {
					setAccountAnnotationCupon(accountAnnotationOperation, validationOperation.getValidationAccountAnnotationCupons());
					//messagesError = messagesError+accountAnnotationOperation.getMsgMassiveErrors();
					if( accountAnnotationOperation.getAccountAnnotationCupons()!=null && accountAnnotationOperation.getAccountAnnotationCupons().size()>0 ) {
						for(AccountAnnotationCupon accountAnnotationCupon:accountAnnotationOperation.getAccountAnnotationCupons()) {
							messagesError = messagesError + accountAnnotationCupon.getMessagesError();
						}
					}
				}else {
					//messagesError = messagesError+accountAnnotationOperation.getMsgMassiveErrors();
					messagesError = messagesError+", no hay Cupones que coincidan con los datos del Capital";
				}


				if(messagesError!=null && !messagesError.equals("")) {
					if( !messagesError.equals("") ) {
						messagesError = "Hubo Problemas en el titulo " + messagesError;
					}
					errorsQuantity += 1;
				}else {
					sucessQuantity += 1;
				}
				
				accountAnnotationOperation.setMsgMassiveErrors(messagesError);
				
				
				//--------------------------------------------------------------------------------------------------------------------
				if(!accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode())) {
					Boolean blTypeCurrency = BooleanType.NO.getBooleanValue();
					
					/*USANDO EL MISMO QUE EL REGISTRO MANUAL*/
					if(accountAnnotationOperation.getHolderAccount().getIndPdd()== null || accountAnnotationOperation.getHolderAccount().getIndPdd()== 0) {
						List<HolderAccountBank> lstHolderAccount = certificateDepositeServiceFacade.findHolderAccountBank(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk(), HolderAccountStateBankType.REGISTERED.getCode());
						if(Validations.validateIsNotNull(lstHolderAccount)) {
							for (HolderAccountBank holderAccountBank : lstHolderAccount) {
								if (holderAccountBank.getCurrency().equals(accountAnnotationOperation.getSecurityCurrency())) {
									blTypeCurrency = BooleanType.YES.getBooleanValue();
								}
							}
						}
					}else {
						List<InstitutionBankAccount> lsInstitutionBankAccount = certificateDepositeServiceFacade.findInstitutionBankAccount(participant.getIdParticipantPk(), InstitutionBankAccountsType.STATE.getCode());
						if(Validations.validateIsNotNull(lsInstitutionBankAccount)) {
							for (InstitutionBankAccount institutionBank : lsInstitutionBankAccount) {
								if (institutionBank.getCurrency().equals(accountAnnotationOperation.getSecurityCurrency())) {
									blTypeCurrency = BooleanType.YES.getBooleanValue();
								}
							}
						}
					}
					
					/*LA VALIDACION YA EXISTENTE DEL REGISTRO MASIVO*/
					/*
					List<HolderAccountBank> lsHolderAccountBank = (accountAnnotationOperation.getHolderAccount()!=null)? certificateDepositeServiceFacade.findHolderAccountBank(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk(), HolderAccountStateBankType.REGISTERED.getCode()) : null;
					if(Validations.validateIsNotNull(lsHolderAccountBank)) {
						for (HolderAccountBank holderAccountBank : lsHolderAccountBank) {
							if (holderAccountBank.getHolderAccount().getIndPdd() == null || holderAccountBank.getHolderAccount().getIndPdd() == 0) {
								if(holderAccountBank.getCurrency().equals(accountAnnotationOperation.getSecurityCurrency())) {
									blTypeCurrency = BooleanType.YES.getBooleanValue();
								}
							}else {
								List<InstitutionBankAccount> lsInstitutionBankAccount = certificateDepositeServiceFacade.findInstitutionBankAccount(accountAnnotationOperation.getHolderAccount().getIdHolderAccountPk(), InstitutionBankAccountsType.STATE.getCode());
								if (Validations.validateIsNotNull(lsInstitutionBankAccount)) {
									for (InstitutionBankAccount institutionBankAccount : lsInstitutionBankAccount) {
										if(institutionBankAccount.getCurrency().equals(accountAnnotationOperation.getSecurityCurrency())) {
											blTypeCurrency = BooleanType.YES.getBooleanValue();
										}
									}
								}
							}
						}
					}
					*/
					
					
					if (!blTypeCurrency) {
						String messageError = (accountAnnotationOperation.getMsgMassiveErrors()!=null)?accountAnnotationOperation.getMsgMassiveErrors():"";
						accountAnnotationOperation.setMsgMassiveErrors(messageError+", No posee cuenta bancaria registrada en la moneda de la operación");
					}
				}
				//--------------------------------------------------------------------------------------------------------------------
				
				accountAnnotationOperationTO.setAccountAnnotationOperation(accountAnnotationOperation);
				lstAccountAnnotationOperation.add(accountAnnotationOperationTO);

			}
			
		}
		
		return lstAccountAnnotationOperation;
	}

	public List<RegisterAccountAnnotationTO> setListAccountAnnotationOperationEntityAcciones(List<ValidationAccountAnnotationOperation>  lstOperations) throws ServiceException{
		
		List<RegisterAccountAnnotationTO>  lstAccountAnnotationOperation = null;
		if(lstOperations!=null) {
			RegisterAccountAnnotationTO accountAnnotationOperationTO = null;
			AccountAnnotationOperation accountAnnotationOperation = null;
			Participant participant = null;
			HolderAccount holderAccount = null;
			Issuer issuer = null;
			String messagesError = "";
			Security security = null;
			lstAccountAnnotationOperation = new ArrayList<RegisterAccountAnnotationTO>();
			
			for (ValidationAccountAnnotationOperation validationOperation: lstOperations) {
				
				validationOperation.setSecurityInterestType("FIJO");
				
				messagesError = "";

				accountAnnotationOperationTO = new RegisterAccountAnnotationTO();
				accountAnnotationOperation = new AccountAnnotationOperation();
				
				accountAnnotationOperationTO.setSecurityClass(SecurityClassType.DPF.getCode());
				accountAnnotationOperationTO.setDepartment(departmentDefault);
				accountAnnotationOperationTO.setProvince(provinceDefault);
				accountAnnotationOperationTO.setMunicipality(districtDefault);
				accountAnnotationOperationTO.setBlLien(false);
				List<SecuritySerialRange> lstSecuritySerialRange = null;

				//
				try {
					
					security = certificateDepositeServiceFacade.findSecurity(validationOperation.getIdSecurityCodePk());
					
					if(security == null) {
						messagesError = messagesError+", Codigo Valor no existe";
					}else {
						
						if( security.getIndAuthorized().equals(0) ) {
							messagesError = messagesError+", Codigo Valor no esta autorizado";
						}else {
						
							if( !(SecurityStateType.REGISTERED.getCode().equals(security.getStateSecurity()) || 
									SecurityStateType.GUARDA_EXCLUSIVE.getCode().equals(security.getStateSecurity()) || 
									SecurityStateType.GUARDA_MIXTA.getCode().equals(security.getStateSecurity()) )
							) {
								messagesError = messagesError+", Codigo Valor no esta habilitado";
							}else {
								
								accountAnnotationOperation.setSecurity(security);
								accountAnnotationOperation.setIssuer(security.getIssuer());
								
								accountAnnotationOperation.setSecurityClass(security.getSecurityClass());
								if( SecurityClassType.get(security.getSecurityClass()) !=null ) {
									accountAnnotationOperation.setSecurityClassDescription(SecurityClassType.get(security.getSecurityClass()).getValue());
								}
								
								accountAnnotationOperation.setSecurityInterestType(security.getInterestType());
								if( InterestType.get(security.getInterestType()) !=null ) {
									accountAnnotationOperation.setSecurityInterestTypeDesc(InterestType.get(security.getInterestType()).getValue());
								}
			
								accountAnnotationOperation.setSecurityNominalValue(security.getInitialNominalValue());
								accountAnnotationOperation.setSecurityExpirationDate(security.getExpirationDate());
								accountAnnotationOperation.setExpeditionDate(security.getIssuanceDate());
			
								accountAnnotationOperation.setSecurityCurrency(security.getCurrency());
								accountAnnotationOperation.setSecurityCurrencyDesc(CurrencyType.get(security.getCurrency()).getValue());

								lstSecuritySerialRange = dematerializationCertificateFacade.findSecuritySerialRange(security.getIdSecurityCodePk());
								
							}
							
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Codigo Valor no existe";
				}
				
				accountAnnotationOperation.setBranchOffice(2735);
				accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
				accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
				accountAnnotationOperation.setIndSerializable(BooleanType.YES.getCode());
				accountAnnotationOperation.setIndDematerialization(0);
				accountAnnotationOperation.setIndImmobilization(1);
				
				//accountAnnotationOperation.setIndElectronicCupon(1);
				//accountAnnotationOperation.setIndElectronicCuponDesc("SI");
				

				//accountAnnotationOperation.setPeriodicity(InterestPaymentModalityType.AT_MATURITY.getCode());
				//accountAnnotationOperation.setPeriodicityDesc(InterestPaymentModalityType.AT_MATURITY.getValue());

				accountAnnotationOperation.setReferencePartCode(validationOperation.getObservation());

				

				
				try {
					if(!validationOperation.getSerialNumber().equals("")) {
						
						List<SecuritySerialRange> lstRangeInSerie = new ArrayList<SecuritySerialRange>();
						if(lstSecuritySerialRange!=null) {
							for(SecuritySerialRange current: lstSecuritySerialRange) {
								if(current.getSerialRange().equals(validationOperation.getSerialNumber())) {
									lstRangeInSerie.add(current);
								}
							}
						}

						if(lstRangeInSerie.size()>0) {
							accountAnnotationOperation.setLstSecuritySerialRange(lstRangeInSerie);
							accountAnnotationOperation.setSerialNumber(validationOperation.getSerialNumber().toUpperCase());
						}else {
							messagesError = messagesError+", la Serie, no coincide con la informacion del valor";
						}
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Serie";
				}
				
				
				if(accountAnnotationOperation.getLstSecuritySerialRange() == null) {
					messagesError = messagesError+", El valor no tiene registrado una Inversion minima";
				}else {

					if(validationOperation.getCertificateFrom()!=null && !validationOperation.getCertificateFrom().equals("")) {
						Integer certificateFrom = new Integer(validationOperation.getCertificateFrom());
						
						for (SecuritySerialRange current : accountAnnotationOperation.getLstSecuritySerialRange()) { 

							if(	current.getNumberShareInitial() <= certificateFrom  && 
								current.getNumberShareFinal() >= certificateFrom 	
							) {
								accountAnnotationOperation.setCertificateFrom(certificateFrom);
							}
						}
						
						if(accountAnnotationOperation.getCertificateFrom() == null){
							messagesError = messagesError+", la cantidad  desde, no coincide dentro del valor Inversion minima/maxima";
						}
						
					}else {
						messagesError = messagesError+", Error en el ingreso del campo desde";
					}
				}
				

				if(accountAnnotationOperation.getLstSecuritySerialRange() == null) {
					messagesError = messagesError+", El valor no tiene registrado una Inversion maxima";
				}else {

					if(validationOperation.getCertificateTo()!=null && !validationOperation.getCertificateTo().equals("")) {
						
						Integer certificateTo = new Integer(validationOperation.getCertificateTo());
						

						for (SecuritySerialRange current : accountAnnotationOperation.getLstSecuritySerialRange()) { 
							if(	current.getNumberShareInitial() <= certificateTo && 
								current.getNumberShareFinal() >= certificateTo 	
							) {
								accountAnnotationOperation.setCertificateTo(certificateTo);
	
								Integer difToFrom = (accountAnnotationOperation.getCertificateTo()-accountAnnotationOperation.getCertificateFrom())+1;
								BigDecimal totalBalance = new BigDecimal(difToFrom);
								accountAnnotationOperation.setTotalBalance(totalBalance);
								
								BigDecimal shareCapital = security.getInitialNominalValue().multiply(accountAnnotationOperation.getTotalBalance());
								accountAnnotationOperationTO.setShareCapital(shareCapital);
							} 
						}
						

						if(accountAnnotationOperation.getCertificateTo() == null){
							messagesError = messagesError+", la cantidad  hasta, no coincide dentro del valor Inversion minima/maxima";
						}
						
						//accountAnnotationOperation.setCertificateTo(new Integer(validationOperation.getCertificateTo()));
					}else {
						messagesError = messagesError+", Error en el ingreso del campo hasta";
					}
				}
				

				if(accountAnnotationOperation.getLstSecuritySerialRange() != null) {

					if(accountAnnotationOperation.getCertificateFrom() != null && accountAnnotationOperation.getCertificateTo() != null){
						Boolean isRangeValid = false;
						for (SecuritySerialRange current : accountAnnotationOperation.getLstSecuritySerialRange()) { 
							
							Integer certificateFrom = new Integer(validationOperation.getCertificateFrom());
							Integer certificateTo = new Integer(validationOperation.getCertificateTo());
							if(	current.getNumberShareInitial() <= certificateFrom  && 
								current.getNumberShareFinal() >= certificateFrom &&
								current.getNumberShareInitial() <= certificateTo && 
								current.getNumberShareFinal() >= certificateTo 	
							) {
								isRangeValid = true;
								break;
							}
							
						}
						
						if(!isRangeValid) {
							messagesError = messagesError+", el rango ingresado no coincide dentro del valor Inversion minima/maxima";
						}
					}
				}

				if(validationOperation.getTotalBalance() !=null && !validationOperation.getTotalBalance().equals("")) {
					Integer excelTotalBalance = new Integer(validationOperation.getTotalBalance());
					Integer difTotalBalance = (accountAnnotationOperation.getTotalBalance()!=null)?accountAnnotationOperation.getTotalBalance().intValue():0;
					if(excelTotalBalance > difTotalBalance || excelTotalBalance < difTotalBalance) {
						accountAnnotationOperation.setTotalBalance(BigDecimal.valueOf(excelTotalBalance));
						messagesError = messagesError+", La cantidad ingresada, no es la diferencia de los campos desde y hasta";
					}
					
				}else {
					messagesError = messagesError+", Error en el ingreso del campo Cantidad";
				}
				
				
				try {
					if(validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() != null) {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION_OTC.getValue());
					}else if( (validationOperation.getAmountRate() != null && validationOperation.getParticipantSeller() == null) ||  
							  (validationOperation.getAmountRate() == null && validationOperation.getParticipantSeller() != null)
							 ){
						messagesError = messagesError+", Existe errores en los campos de Ingreso OTC";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Error en el ingreso por OTC";
				}
				
				try {
					participant = certificateDepositeServiceFacade.findParticipant(new BigDecimal(validationOperation.getParticipant()).longValue());
					accountAnnotationOperationTO.setIdParticipantPk(participant.getIdParticipantPk());
					accountAnnotationOperationTO.setParticipant(participant);
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Participante";
				}

				try {
					Integer holderAccountNumber = (new BigDecimal(validationOperation.getHolderAccountNumber())).intValue();
					holderAccount = certificateDepositeServiceFacade.getHoldetAccount(holderAccountNumber, participant.getIdParticipantPk());
					accountAnnotationOperation.setHolderAccount(holderAccount);
					accountAnnotationOperation.getHolderAccount().setParticipant(participant);
					
					if ( !HolderAccountStatusType.ACTIVE.getCode().equals(holderAccount.getStateAccount()) ) {
						messagesError = messagesError+", La Cuenta Titular no esta Activa";
					}
			
				} catch (Exception e) {
					participant = null;
					messagesError = messagesError+", Cuenta Holder";
				}
				
				try {
					if(!validationOperation.getCertificateNumber().equals("")) {
						
						if(	security !=null && security.getSubClass() != null && security.getSubClass().equals(validationOperation.getCertificateNumber()) ) {
							accountAnnotationOperation.setCertificateNumber(validationOperation.getCertificateNumber().toUpperCase());
						}else {
							messagesError = messagesError+", la Subclase, no coincide con la informacion del valor";
						}
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Subclase";
				}
				
				if( security!=null && security.getStateSecurity() !=null && 
					(security.getStateSecurity().equals(SecurityStateType.GUARDA_EXCLUSIVE.getCode()) || 
					 security.getStateSecurity().equals(SecurityStateType.GUARDA_MIXTA.getCode())) 
				) {
					accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
					accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getValue());
				}else {
					accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
					accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
				}

				/*
				try {
					if(!validationOperation.getSecurityInterestRate().equals("")) {
						BigDecimal interestRate = new BigDecimal(validationOperation.getSecurityInterestRate());
						accountAnnotationOperation.setSecurityInterestRate(interestRate); 
						accountAnnotationOperation.getSecurity().setInterestRate(interestRate);
					}
				} catch (Exception e) {
					messagesError = messagesError+", Tasa Interes Anual";
				}
				*/
				
				/*
				try {
					if(!validationOperation.getMotive().equals("")) {
						if(validationOperation.getMotive().equals("INMOVILIZACION")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA ADMINISTRADA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_MANAGED.getValue());
						}else if(validationOperation.getMotive().equals("GUARDA EXCLUSIVA")) {
							accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode());
							accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getValue());
						}
					}else {
						accountAnnotationOperation.setMotive(DematerializationCertificateMotiveType.DEMATERIALIZATION.getCode());
						accountAnnotationOperation.setMotiveDescription(DematerializationCertificateMotiveType.DEMATERIALIZATION.getValue());
					}
					
				} catch (Exception e) {
					messagesError = messagesError+", motivo";
				}
				*/
				//cboTaxExempt
				if(security.getIndTaxExempt()!= null && security.getIndTaxExempt().equals(1)) {
					accountAnnotationOperation.setIndTaxExoneration(1);
					accountAnnotationOperation.setIndTaxExonerationDesc("SI");
				}else {
					accountAnnotationOperation.setIndTaxExoneration(0);
					accountAnnotationOperation.setIndTaxExonerationDesc("NO");
				}

				try {
					if(validationOperation.getParticipantSeller() != null) {
						Participant participantSell = certificateDepositeServiceFacade.findParticipant(validationOperation.getParticipantSeller());
						accountAnnotationOperationTO.setParticipantSeller(participantSell);
						if(participantSell != null) {
							HolderAccount holderAccountSell = certificateDepositeServiceFacade.getHoldetAccount(1, participantSell.getIdParticipantPk());
							if(holderAccountSell!=null) {
								accountAnnotationOperationTO.setHolderAccountSeller(holderAccountSell);
								accountAnnotationOperation.setHolderAccountSeller(holderAccountSell);
							}else {
								messagesError = messagesError+", Participante Vendedor no tiene un titular con cuenta 1 configurado. ";
							}
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Participante Vendedor";
				}
				

				try {
					if(validationOperation.getAmountRate() != null) {
						BigDecimal amountRate = new BigDecimal(validationOperation.getAmountRate());
						accountAnnotationOperationTO.setAmountRate(amountRate);
						accountAnnotationOperation.setAmountRate(amountRate);
						

						BigDecimal nominalValue = accountAnnotationOperation.getSecurityNominalValue();
						BigDecimal rate = accountAnnotationOperation.getAmountRate();
						accountAnnotationOperation.setCashPrice( rate.multiply(nominalValue).divide(BigDecimal.valueOf(100d)) );
						
					}
				} catch (Exception e) {
					messagesError = messagesError+", Precio sucio";
				}

				if(messagesError!=null && !messagesError.equals("")) {
					messagesError = ", Hubo Problemas en el titulo " + messagesError;
					errorsQuantity += 1;
				}else {
					sucessQuantity += 1;
				}
				
				accountAnnotationOperation.setMsgMassiveErrors(messagesError);
				
				accountAnnotationOperationTO.setAccountAnnotationOperation(accountAnnotationOperation);
				lstAccountAnnotationOperation.add(accountAnnotationOperationTO);

			}
			
		}
		
		return lstAccountAnnotationOperation;
	}
	
	public void setAccountAnnotationCupon(AccountAnnotationOperation accountAnnotationOperation, List<ValidationAccountAnnotationCupon> lstValidationAccountAnnotationCupon) {
		if( lstValidationAccountAnnotationCupon!=null && lstValidationAccountAnnotationCupon.size()>0 ) {
			List<AccountAnnotationCupon> lstAccountAnnotationCupon = new ArrayList<AccountAnnotationCupon>();
			AccountAnnotationCupon accountAnnotationCupon = null;
			String messagesError = "";
			Integer nroCupon = 0;
			Integer nroCuponRepeat = 0;
			
			for(ValidationAccountAnnotationCupon validationCupon: lstValidationAccountAnnotationCupon) {
				accountAnnotationCupon = new AccountAnnotationCupon();
				messagesError = "";
				nroCupon = 0;
				nroCuponRepeat = 0;
				
				try {
					if( validationCupon.getNroCupon()!=null && !validationCupon.getNroCupon().equals("") ) {
						if(Validations.validateListIsNotNullAndNotEmpty(lstAccountAnnotationCupon)) {
							for(AccountAnnotationCupon accountAnnotationCuponSave:lstAccountAnnotationCupon) {
								if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationCuponSave.getCuponNumber())) {
									if(accountAnnotationCuponSave.getCuponNumber().equals(Integer.valueOf(validationCupon.getNroCupon()))) {
										nroCuponRepeat = nroCuponRepeat+1;
									}
								}
							}
						}
						if(nroCuponRepeat<1) {
							nroCupon = Integer.valueOf(validationCupon.getNroCupon());
							accountAnnotationCupon.setCuponNumber(nroCupon);
						}else {
							messagesError = messagesError+", Nro Cupon "+validationCupon.getNroCupon()+" Repetido";
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Nro Cupon";
				}
				

				try {
					if( validationCupon.getExpirationDate() !=null && !validationCupon.getExpirationDate().equals("") ) {
						accountAnnotationCupon.setExpirationDate(CommonsUtilities.convertStringtoDate(validationCupon.getExpirationDate()));
						/*
						Calendar expirationDater = Calendar.getInstance();
						expirationDater.setTime(accountAnnotationCupon.getExpirationDate());
						
						if( CommonsUtilities.isWeekend(expirationDater) ) {
							messagesError = messagesError+", Fecha de Vencimiento del cupon no puede ser fin de semana";
						}*/
						Integer diasHabiles = 3;
						
						ParameterTableTO filter = new ParameterTableTO();
						if(Validations.validateIsNotNullAndNotEmpty(accountAnnotationOperation.getMotive())) {
							if(accountAnnotationOperation.getMotive().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()))
								filter.setParameterTablePk(2911);
							else
								filter.setParameterTablePk(2912);
						}else
							filter.setParameterTablePk(2912);
						List<ParameterTable> param = parameterServiceBean.getListParameterTableServiceBean(filter);
						if(param!=null && param.size()>0) {
							diasHabiles = param.get(0).getShortInteger();
						}
						
						Date maxAvailableDate = CommonsUtilities.addDaysHabilsToDate(CommonsUtilities.currentDate(), diasHabiles);
						if( maxAvailableDate.after(accountAnnotationCupon.getExpirationDate()) || maxAvailableDate.equals(accountAnnotationCupon.getExpirationDate()) ) {
							messagesError = messagesError+", No puede registrarse un cupon cuya fecha vencimiento este dentro de los "+diasHabiles+" dias siguientes";
						}
						
						if(accountAnnotationCupon.getExpirationDate().after(accountAnnotationOperation.getSecurityExpirationDate()) ) {
							messagesError = messagesError+", Fecha de Vencimiento del cupon No puede ser mayor a la fecha de Vencimiento del Titulo";
						}
						
						if( accountAnnotationOperation.getExpeditionDate().after(accountAnnotationCupon.getExpirationDate()) ) {
							messagesError = messagesError+", Fecha de Vencimiento del cupon "+accountAnnotationCupon.getCuponNumber()+" No puede ser menor a la fecha de Emision del Titulo";
						}
						
					}else {
						messagesError = messagesError+", Fecha de Vencimiento del cupon ";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Fecha Vencimiento del cupon "+accountAnnotationCupon.getCuponNumber();
				}
				

				try {
					if( validationCupon.getCuponAmount() !=null && !validationCupon.getCuponAmount().equals("") ) {
						accountAnnotationCupon.setCuponAmount(new BigDecimal(validationCupon.getCuponAmount()));
					}else {
						messagesError = messagesError+", Monto Cupon";
					}
				} catch (Exception e) {
					messagesError = messagesError+", Monto Cupon";
				}
				
				Issuer issuer = null;
				try {
					issuer = certificateDepositeServiceFacade.findIssuerWithNemonic(validationCupon.getMnemonicIssuer() );
					if(issuer == null) {
						messagesError = messagesError+", Emisor del cupon ";
					}
					accountAnnotationCupon.setIssuer(issuer);
				} catch (Exception e) {
					issuer = null;
					messagesError = messagesError+", Emisor del cupon ";
				}

				try {
					if(!validationCupon.getSecurityCurrency().equals("")) {
						if(CurrencyType.PYG.getCodeIso().equalsIgnoreCase(validationCupon.getSecurityCurrency()) || ("GS").equalsIgnoreCase(validationCupon.getSecurityCurrency()) ) {
							accountAnnotationCupon.setSecurityCurrency(CurrencyType.PYG.getCode());
						}else if(CurrencyType.USD.getCodeIso().equalsIgnoreCase(validationCupon.getSecurityCurrency())) {
							accountAnnotationCupon.setSecurityCurrency(CurrencyType.USD.getCode());
						}
					}
				} catch (Exception e) {
					messagesError = messagesError+", Moneda del cupon ";
				}
				
				if(messagesError!=null && !messagesError.equals("")) {
					messagesError = "\n. Hubo Problemas en los siguientes cupones" + messagesError;
				}


				try {
					Integer paymentDays=null;
					if(true){
						paymentDays=CommonsUtilities.getDaysBetween(accountAnnotationCupon.getBeginingDate(),accountAnnotationCupon.getPaymentDate());
					}
					accountAnnotationCupon.setPaymentDays(paymentDays);
				} catch (Exception e) {
					messagesError = messagesError+", Cantidad de dias";
				}
				
				accountAnnotationCupon.setStateCupon(true);

				accountAnnotationCupon.setInterestFactor( accountAnnotationOperation.getSecurityInterestRate() );
				accountAnnotationCupon.setCertificateNumber(accountAnnotationOperation.getCertificateNumber()+"-"+accountAnnotationCupon.getCuponNumber());
				accountAnnotationCupon.setSerialNumber(accountAnnotationOperation.getSerialNumber().toUpperCase());
				
				accountAnnotationCupon.setMessagesError(messagesError);
				accountAnnotationOperation.setMsgMassiveErrors(messagesError);//accountAnnotationOperation.getMsgMassiveErrors()+
				
				
				lstAccountAnnotationCupon.add(accountAnnotationCupon);
				/*private String expirationDate;//Fecha Vencimiento
				private String paymentDate;//Fecha Pago
				private String interestRate;//tasa
				private String cuponAmount;//monto Cupon*/
				
			}
			if( !(lstAccountAnnotationCupon!=null && lstAccountAnnotationCupon.size()>0) ) {
				accountAnnotationCupon.setMessagesError(accountAnnotationCupon.getMessagesError()+" No tiene cupones Validos");	
			}
			accountAnnotationOperation.setAccountAnnotationCupons(lstAccountAnnotationCupon);
		}
		
	}
	
	/**
	 * Before process interface file.
	 */
	public void beforeProcessInterfaceFile(){	
		if(errorsQuantity > 0) {

			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING), 
					"Existen Errores por solucionar");
			JSFUtilities.showSimpleValidationDialog();
		}else {
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
					PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_UPLOAD_CONFIRM));
			JSFUtilities.executeJavascriptFunction("PF('cnfwProcessMcnFile').show();");	
		}
	}
	
	/**
	 * Process interface file.
	 */

	
	@LoggerAuditWeb
	public void processInterfaceFile(){	
		
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(processFile.getIdBusinessProcess());
		
		try {
			
			/*Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PARAMETER_FILE, processFile.getTempProcessFile().getAbsolutePath());
			details.put(GeneralConstants.PARAMETER_STREAM, processFile.getStreamFileDir());
			details.put(GeneralConstants.PARAMETER_STREAM_RESPONSE, processFile.getStreamResponseFileDir());
			details.put(GeneralConstants.PARAMETER_LOCALE, "es"); //JSFUtilities.getCurrentLocale().toString());
			details.put(GeneralConstants.PARAMETER_FILE_NAME, processFile.getFileName());
			details.put(GeneralConstants.PARAMETER_ROOT_TAG, processFile.getRootTag());
			
			batchProcessServiceFacade.registerBatch(
					userInfo.getUserAccountSession().getUserName(),businessProcess,details);
			*/
			
			if(lstRegisterAccountAnnotationTO!=null && lstRegisterAccountAnnotationTO.size()>0) {
				try {
					
					List<AccountAnnotationOperation> listReturn = dematerializationCertificateFacade.registerListAccountAnnotationOperation(lstRegisterAccountAnnotationTO);
					
					if(listReturn!=null) {
						String operacionesRegistradas = "";
						for (AccountAnnotationOperation accountAnnotationOperation : listReturn) {
							operacionesRegistradas = operacionesRegistradas + accountAnnotationOperation.getCustodyOperation().getOperationNumber()+ ",";
						}

					    businessProcess = new BusinessProcess();
						businessProcess.setIdBusinessProcessPk(BusinessProcessType.PHYSICAL_CERTIFICATE_DEPOSIT_REGISTER.getCode());
						Object[] parameters = { operacionesRegistradas };
						
						notificationServiceFacade.sendNotification(userInfo.getUserAccountSession().getUserName(), businessProcess,lstRegisterAccountAnnotationTO.get(0).getIdParticipantPk() ,parameters);
						
						
					}
					
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
							PropertiesUtilities.getGenericMessage(GenericPropertiesConstants.EXTERNAL_INTERFACE_UPLOAD_SUCCESS));
					JSFUtilities.showSimpleValidationDialog();
					
				} catch (ServiceException e) {
					e.printStackTrace();
					showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
							, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
					JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				}
			}else {

				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS), 
						"Ocurrio un error al registrar los CDAs");
				JSFUtilities.showSimpleValidationDialog();
			}
			cleanAll();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Verify on back action.
	 *
	 * @return the string
	 */
	public String verifyOnBackAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("PF('transferOwnershipBackCnfWidget').show();");
			return null;
		}else{
			return "searchMCNOperations";
		}
	}

	/**
	 * Verify on clean action.
	 */
	public void verifyOnCleanAction(){
		if(isModifiedForm()){
			JSFUtilities.putViewMap("headerMessageView", PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_WARNING));
			JSFUtilities.executeJavascriptFunction("PF('transferOwnershipClearCnfWidget').show();");
		}else{
			cleanAll();
		}
	}
	
	
	/**
	 * Checks if is modified form.
	 *
	 * @return true, if is modified form
	 */
	public boolean isModifiedForm(){
		if(Validations.validateIsNotNullAndPositive(processFileFilter.getIdNegotiationMechanismPk()) ||
				Validations.validateIsNotNullAndPositive(processFile.getIdNegotiationMechanismPk()) ||
				Validations.validateIsNotNull(processFile.getTempProcessFile())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Gets the process files data model.
	 *
	 * @return the mcnProcessFilesDataModel
	 */
	public GenericDataModel<ProcessFileTO> getProcessFilesDataModel() {
		return processFilesDataModel;
	}

	/**
	 * Sets the process files data model.
	 *
	 * @param processFilesDataModel the new process files data model
	 */
	public void setProcessFilesDataModel(
			GenericDataModel<ProcessFileTO> processFilesDataModel) {
		this.processFilesDataModel = processFilesDataModel;
	}

	/**
	 * Gets the process file filter.
	 *
	 * @return the mcnProcessFileFilter
	 */
	public ProcessFileTO getProcessFileFilter() {
		return processFileFilter;
	}

	/**
	 * Sets the process file filter.
	 *
	 * @param mcnProcessFileFilter            the mcnProcessFileFilter to set
	 */
	public void setProcessFileFilter(ProcessFileTO mcnProcessFileFilter) {
		this.processFileFilter = mcnProcessFileFilter;
	}

	/**
	 * Gets the process file.
	 *
	 * @return the mcnProcessFile
	 */
	public ProcessFileTO getProcessFile() {
		return processFile;
	}

	/**
	 * Sets the process file.
	 *
	 * @param mcnProcessFile            the mcnProcessFile to set
	 */
	public void setProcessFile(ProcessFileTO mcnProcessFile) {
		this.processFile = mcnProcessFile;
	}

	/**
	 * Gets the errors data model.
	 *
	 * @return the errorsDataModel
	 */
	public GenericDataModel<RecordValidationType> getErrorsDataModel() {
		return errorsDataModel;
	}

	/**
	 * Sets the errors data model.
	 *
	 * @param errorsDataModel            the errorsDataModel to set
	 */
	public void setErrorsDataModel(GenericDataModel<RecordValidationType> errorsDataModel) {
		this.errorsDataModel = errorsDataModel;
	}
	
	/**
	 * Gets the f upload file types.
	 *
	 * @return the fUploadFileTypes
	 */
	public String getfUploadFileTypes() {
		return fUploadFileTypes;
	}

	/**
	 * Sets the f upload file types.
	 *
	 * @param fUploadFileTypes the fUploadFileTypes to set
	 */
	public void setfUploadFileTypes(String fUploadFileTypes) {
		this.fUploadFileTypes = fUploadFileTypes;
	}

	/**
	 * Gets the f upload file types display.
	 *
	 * @return the fUploadFileTypesDisplay
	 */
	public String getfUploadFileTypesDisplay() {
		return fUploadFileTypesDisplay;
	}

	/**
	 * Sets the f upload file types display.
	 *
	 * @param fUploadFileTypesDisplay the fUploadFileTypesDisplay to set
	 */
	public void setfUploadFileTypesDisplay(String fUploadFileTypesDisplay) {
		this.fUploadFileTypesDisplay = fUploadFileTypesDisplay;
	}
	
	/**
	 * Gets the f upload file types pattern.
	 *
	 * @return the fUploadFileTypesPattern
	 */
	public Pattern getfUploadFileTypesPattern() {
		return fUploadFileTypesPattern;
	}

	/**
	 * Sets the f upload file types pattern.
	 *
	 * @param fUploadFileTypesPattern the fUploadFileTypesPattern to set
	 */
	public void setfUploadFileTypesPattern(Pattern fUploadFileTypesPattern) {
		this.fUploadFileTypesPattern = fUploadFileTypesPattern;
	}
	
	/**
	 * Gets the tmp process file.
	 *
	 * @return the tmpMcnProcessFile
	 */
	public ProcessFileTO getTmpProcessFile() {
		return tmpProcessFile;
	}

	/**
	 * Sets the tmp process file.
	 *
	 * @param tmpMcnProcessFile the tmpMcnProcessFile to set
	 */
	public void setTmpProcessFile(ProcessFileTO tmpMcnProcessFile) {
		this.tmpProcessFile = tmpMcnProcessFile;
	}

	/**
	 * Back action.
	 *
	 * @return the string
	 */
	public String backAction() {
		return backAction;
	}

	/**
	 * Gets the title label.
	 *
	 * @return the title label
	 */
	public String getTitleLabel() {
		return titleLabel;
	}

	/**
	 * Sets the title label.
	 *
	 * @param titleLabel the new title label
	 */
	public void setTitleLabel(String titleLabel) {
		this.titleLabel = titleLabel;
	}

	public List<ValidationAccountAnnotationOperation> getLstAccountAnnotationOperation() {
		return lstAccountAnnotationOperation;
	}

	public void setLstAccountAnnotationOperation(List<ValidationAccountAnnotationOperation> lstAccountAnnotationOperation) {
		this.lstAccountAnnotationOperation = lstAccountAnnotationOperation;
	}

	public List<RegisterAccountAnnotationTO> getLstRegisterAccountAnnotationTO() {
		return lstRegisterAccountAnnotationTO;
	}

	public void setLstRegisterAccountAnnotationTO(List<RegisterAccountAnnotationTO> lstRegisterAccountAnnotationTO) {
		this.lstRegisterAccountAnnotationTO = lstRegisterAccountAnnotationTO;
	}

	public Integer getErrorsQuantity() {
		return errorsQuantity;
	}

	public void setErrorsQuantity(Integer errorsQuantity) {
		this.errorsQuantity = errorsQuantity;
	}

	public Integer getSucessQuantity() {
		return sucessQuantity;
	}

	public void setSucessQuantity(Integer sucessQuantity) {
		this.sucessQuantity = sucessQuantity;
	}

	public boolean isBlParticipant() {
		return blParticipant;
	}

	public void setBlParticipant(boolean blParticipant) {
		this.blParticipant = blParticipant;
	}

	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	public Integer getExcelTypeId() {
		return excelTypeId;
	}

	public void setExcelTypeId(Integer excelTypeId) {
		this.excelTypeId = excelTypeId;
	}
	
}
