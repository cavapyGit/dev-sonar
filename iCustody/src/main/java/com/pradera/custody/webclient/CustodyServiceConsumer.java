package com.pradera.custody.webclient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.custody.accreditationcertificates.service.AccreditationCertificatesServiceBean;
import com.pradera.custody.affectation.service.AffectationsServiceBean;
import com.pradera.custody.affectation.service.RequestAffectationServiceBean;
import com.pradera.custody.dematerializationcertificate.service.DematerializationCertificateServiceBean;
import com.pradera.custody.ownershipexchange.service.ChangeOwnershipServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.common.validation.to.ProcessFileTO;
import com.pradera.integration.common.validation.to.RecordValidationType;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.component.business.service.InterfaceComponentService;
import com.pradera.integration.component.custody.to.AffectationRegisterTO;
import com.pradera.integration.component.custody.to.SecondaryAccountEntryRegisterTO;
import com.pradera.integration.component.custody.to.StockUnblockRegisterTO;
import com.pradera.integration.component.settlements.to.SecuritiesTransferRegisterTO;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.custody.accreditationcertificates.AccreditationOperation;
import com.pradera.model.custody.accreditationcertificates.BlockOperation;
import com.pradera.model.custody.blockoperation.UnblockOperation;
import com.pradera.model.custody.changeownership.ChangeOwnershipDetail;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionStateType;
import com.pradera.model.generalparameter.externalinterface.type.ExternalInterfaceReceptionType;

import bo.com.edv.www.ServiciosClienteEDVProxy;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class CustodyServiceConsumer.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 20/08/2015
 */
@Stateless
public class CustodyServiceConsumer implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	
	/** The interface component service bean. */
	@Inject
    private Instance<InterfaceComponentService> interfaceComponentServiceBean;
	
	/** The parameter service bean. */
	@EJB
	private ParameterServiceBean parameterServiceBean;
	
	/** The affectations service bean. */
	@EJB
	private AffectationsServiceBean affectationsServiceBean;
	
	/** The accreditation certificates service bean. */
	@EJB
	private AccreditationCertificatesServiceBean accreditationCertificatesServiceBean;
	
	/** The change ownership service bean. */
	@EJB
	private ChangeOwnershipServiceBean changeOwnershipServiceBean;
	
	/** The dematerialization certificate service bean. */
	@EJB
	private DematerializationCertificateServiceBean dematerializationCertificateServiceBean;
	
	/** The request affectation service bean. */
	@EJB
	private RequestAffectationServiceBean requestAffectationServiceBean;
	
	/** The server configuration. */		
	@Inject
	@InjectableResource(location = "ApplicationConfiguration.properties")
	Properties applicationConfiguration;
	
	/** The str services consumer. */
	String strServicesConsumer = GeneralConstants.EMPTY_STRING;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){
		strServicesConsumer = applicationConfiguration.getProperty("eif.banks.web.services.consumer");
	}
	/**
	 * Send block operation web client.
	 *
	 * @param blockOperation the block operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void sendBlockOperationWebClient(BlockOperation blockOperation, LoggerUser loggerUser) throws ServiceException {						
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumer);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		AffectationRegisterTO affectationRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_STOCK_BLOCKING_DPF, 
																			null, ComponentConstant.BLQ_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= affectationsServiceBean.generateAffectationRegisterTO(blockOperation, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					affectationRegisterTO = (AffectationRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse= serviciosClienteEDVProxy.enviarBloqueo(xmlUpload.toString(), affectationRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
					log.info(":::::::: ACTUALIZAMOS INDICADOR ENVIADO POR WEBSERVICE ::::::::");
					requestAffectationServiceBean.updateIndWebService(blockOperation.getCustodyOperation());
				}	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send unblock operation web client.
	 *
	 * @param unblockOperation the unblock operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void sendUnblockOperationWebClient(UnblockOperation unblockOperation, LoggerUser loggerUser) throws ServiceException {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumer);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StockUnblockRegisterTO stockUnblockRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_STOCK_UNBLOCKING_DPF, 
																			null, ComponentConstant.DBQ_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= affectationsServiceBean.generateStockUnblockRegisterTO(unblockOperation, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					stockUnblockRegisterTO = (StockUnblockRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse= serviciosClienteEDVProxy.enviarDesbloqueo(xmlUpload.toString(), stockUnblockRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
					log.info(":::::::: ACTUALIZAMOS INDICADOR ENVIADO POR WEBSERVICE ::::::::");
					requestAffectationServiceBean.updateIndWebService(unblockOperation.getCustodyOperation());
				}	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send block accreditation operation web client.
	 *
	 * @param accreditationOperation the accreditation operation
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void sendBlockAccreditationOperationWebClient(AccreditationOperation accreditationOperation, LoggerUser loggerUser) throws ServiceException {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumer);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		AffectationRegisterTO affectationRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_STOCK_BLOCKING_DPF, 
																			null, ComponentConstant.BLQ_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= accreditationCertificatesServiceBean.generateAccreditationOperationRegisterTO(accreditationOperation, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					affectationRegisterTO = (AffectationRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse= serviciosClienteEDVProxy.enviarBloqueo(xmlUpload.toString(), affectationRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
					log.info(":::::::: ACTUALIZAMOS INDICADOR ENVIADO POR WEBSERVICE ::::::::");
					requestAffectationServiceBean.updateIndWebService(accreditationOperation.getCustodyOperation());
				}	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send unblock accreditation operation web client.
	 *
	 * @param accreditationOperationToUnblock the accreditation operation to unblock
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	@Asynchronous
	public void sendUnblockAccreditationOperationWebClient(AccreditationOperation accreditationOperationToUnblock, LoggerUser loggerUser) throws ServiceException  {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumer);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		StockUnblockRegisterTO stockUnblockRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_STOCK_UNBLOCKING_DPF, 
																			null, ComponentConstant.BLQ_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= accreditationCertificatesServiceBean.generateUnblockAccreditationOperationRegisterTO(accreditationOperationToUnblock, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					stockUnblockRegisterTO = (StockUnblockRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse = serviciosClienteEDVProxy.enviarDesbloqueo(xmlUpload.toString(), stockUnblockRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
					log.info(":::::::: ACTUALIZAMOS INDICADOR ENVIADO POR WEBSERVICE ::::::::");
					requestAffectationServiceBean.updateIndWebService(accreditationOperationToUnblock.getCustodyOperation());
				}	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				ServiceException se= (ServiceException) ex;
				throw se;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send secondary acv operation web client.
	 *
	 * @param accountAnnotationOperation the account annotation operation
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	@Asynchronous
	public void sendSecondaryACVOperationWebClient(AccountAnnotationOperation accountAnnotationOperation, LoggerUser loggerUser) throws Exception {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumer);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SecondaryAccountEntryRegisterTO secondaryAccountEntryRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_SECONDARY_ACCOUNT_ENTRY_DPF, 
																			null, ComponentConstant.ACS_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= dematerializationCertificateServiceBean.generateSecondaryAccountEntryRegisterTO(
																					accountAnnotationOperation, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					secondaryAccountEntryRegisterTO = (SecondaryAccountEntryRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse= serviciosClienteEDVProxy.enviarAnotacionCuentaVoluntaria(xmlUpload.toString(), secondaryAccountEntryRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
				}	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				throw ex;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
	
	/**
	 * Send securities transfer operation web client.
	 *
	 * @param changeOwnershipDetail the change ownership detail
	 * @param loggerUser the logger user
	 * @throws Exception the exception
	 */
	@Asynchronous
	public void sendSecuritiesTransferOperationWebClient(ChangeOwnershipDetail changeOwnershipDetail, LoggerUser loggerUser) throws Exception {
		ServiciosClienteEDVProxy serviciosClienteEDVProxy= new ServiciosClienteEDVProxy(strServicesConsumer);
		List<RecordValidationType> lstRecordValidationTypes= new ArrayList<RecordValidationType>();
		Integer processState = ExternalInterfaceReceptionStateType.CORRECT.getCode();
		SecuritiesTransferRegisterTO securitiesTransferRegisterTO= null;
		ProcessFileTO objProcessFileTO = null;
		StringBuffer xmlUpload= new StringBuffer();
		try {
			objProcessFileTO= parameterServiceBean.validateInterfaceFile(ComponentConstant.INTERFACE_SECURITIES_TRANSFER_DPF, 
																			null, ComponentConstant.TRV_DPF_NAME, Boolean.FALSE);
			if (Validations.validateIsNotNull(objProcessFileTO)) {
				Long idInterfaceProcess= interfaceComponentServiceBean.get().saveExternalInterfaceReceptionTx(objProcessFileTO, 
																		ExternalInterfaceReceptionType.WEBSERVICE.getCode(), loggerUser);
				log.info(":::::::: GENERACION DEL OBJETO TO CON LA INFORMACION A ENVIAR ::::::::");
				lstRecordValidationTypes= changeOwnershipServiceBean.generateSecuritiesTransferRegisterTO(changeOwnershipDetail, idInterfaceProcess, loggerUser);
				
				if (Validations.validateListIsNotNullAndNotEmpty(lstRecordValidationTypes)) {
					securitiesTransferRegisterTO = (SecuritiesTransferRegisterTO) lstRecordValidationTypes.get(0).getOperationRef();
					log.info(":::::::: CREACION DEL ARCHIVO XML ::::::::");
					xmlUpload.append(interfaceComponentServiceBean.get().saveExternalInterfaceUploadFileTx(objProcessFileTO.getIdProcessFilePk(),
																			lstRecordValidationTypes, objProcessFileTO.getStreamFileDir(), 
																			ComponentConstant.DPF_TAG, ComponentConstant.OPERATIONS_RECORD_TAG, 
																			processState, loggerUser));
					log.info(":::::::: INICIO ENVIO DE MENSAJE AL BUS ::::::::");
					String xmlResponse= serviciosClienteEDVProxy.enviarTransferencia(xmlUpload.toString(), securitiesTransferRegisterTO.getEntityNemonic());
					log.info(":::::::: VERIFICACION DE MENSAJE DE RESPUESTA DEL BUS ::::::::");
					//parameterServiceBean.verifyResponseFile(xmlResponse, objProcessFileTO);
					interfaceComponentServiceBean.get().updateInterfaceProcessResponseFile(objProcessFileTO.getIdProcessFilePk(), 
																							processState, xmlResponse.getBytes(), loggerUser);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof com.pradera.integration.exception.ServiceException) {
				throw ex;
			}
			throw new com.pradera.integration.exception.ServiceException(ErrorServiceType.WEB_SERVICE_ERROR_PROCESS);
		}
	}
}
