package com.pradera.custody.vaultcertificates.servicebean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.payroll.view.PhysicalCertificateTO;
import com.pradera.custody.payroll.view.VaultSummary;
import com.pradera.custody.vaultcertificates.view.SearchCertificateVaultTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Participant;
import com.pradera.model.component.HolderAccountBalance;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.corporatives.type.PayrollStateType;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.PhysicalCertificateStateType;
import com.pradera.model.custody.type.SituationType;

@Stateless
public class CertificateVaultServiceBean extends CrudDaoServiceBean{

	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	
	public List<Object[]> getPayrollDetailCorporative(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		// TODO Auto-generated method stub
		return null;
	}

	public List<Object[]> getPayrollDetailAccountAnnotation(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk,");//0
		sbQuery.append("	pd.accountAnnotationOperation.custodyOperation.registryDate,");//1
		sbQuery.append("	pd.payrollHeader.idPayrollHeaderPk,");//2
		sbQuery.append("	pd.accountAnnotationOperation.security.idSecurityCodePk,");//3
		sbQuery.append("	pd.accountAnnotationOperation.holderAccount.idHolderAccountPk,");//4
		sbQuery.append("	pd.payrollQuantity,");//5
		sbQuery.append("	pd.idPayrollDetailPk,");//6
		sbQuery.append("	pd.payrollState,");//7		
		sbQuery.append("	pd.accountAnnotationOperation.indSerializable, ");//8
		sbQuery.append("	0, ");//9 - pd.branchOffice
		sbQuery.append("	pd.accountAnnotationOperation.physicalCertificate.idPhysicalCertificatePk, ");//10
		sbQuery.append("	pd.indCupon, ");//11
		sbQuery.append("	pd.cuponNumber, ");//12
		sbQuery.append("	pd.serialCode, ");//13
		sbQuery.append("	pd.currentVault, ");//14
		sbQuery.append("	(CASE WHEN pd.indCupon = 1 THEN ");
		sbQuery.append("		( SELECT aac.cuponAmount FROM AccountAnnotationCupon aac WHERE aac.accountAnnotationOperation.idAnnotationOperationPk = pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk and aac.cuponNumber = pd.cuponNumber ) ");//15
		sbQuery.append("	ELSE ");
		sbQuery.append("		pd.accountAnnotationOperation.securityNominalValue ");
		sbQuery.append("	END ) as securityNominalValue, ");
		sbQuery.append("	pd.accountAnnotationOperation.holderAccount.participant.mnemonic, ");//16
		sbQuery.append("	pd.accountAnnotationOperation.idAnnotationOperationPk ");//17
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE 1 = 1 ");//pd.payrollHeader.branchOffice = :branchOffice
		sbQuery.append("	AND pd.payrollHeader.payrollType = :payrollType");
		sbQuery.append("	AND pd.payrollHeader.registerDate >= :initialDate ");
		sbQuery.append("	AND pd.payrollHeader.registerDate <= :finalDate ");
		if(searchCertificateVaultTO.getPayrollDetailState()!=null) {
			sbQuery.append("	AND pd.payrollState = :state");
		}else {
			sbQuery.append("	AND pd.payrollState in (:lstState)");
		}
		
		if(searchCertificateVaultTO.getEntryVault()!=null) {
			sbQuery.append("	AND pd.currentVault = :currentVault");
		}else {
			sbQuery.append("	AND pd.currentVault is null ");
		}
		if( Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer()) && 
				Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer().getIdIssuerPk())) {
			sbQuery.append("	AND pd.accountAnnotationOperation.issuer.idIssuerPk = :idIssuerPk");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getPayRollNumber())) {
			sbQuery.append("	AND pd.payrollHeader.idPayrollHeaderPk = :payRollNumber");
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIdParticipantPk()))
			sbQuery.append("	AND pd.payrollHeader.participant.idParticipantPk = :idParticipantPk");
		sbQuery.append("	ORDER BY pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk DESC, pd.idPayrollDetailPk ASC ");
		
		Query query = em.createQuery(sbQuery.toString());
		/*query.setParameter("branchOffice", searchCertificateVaultTO.getBranchOffice());*/
		query.setParameter("payrollType", searchCertificateVaultTO.getPayRollType());
		if(searchCertificateVaultTO.getPayrollDetailState()!=null) {
			query.setParameter("state", searchCertificateVaultTO.getPayrollDetailState());
		}else {
			List<Integer> lstValidStates = new ArrayList<Integer>();
			lstValidStates.add(PayrollDetailStateType.RECEIVED.getCode());
			lstValidStates.add(PayrollDetailStateType.MICROFILMACION.getCode());
			lstValidStates.add(PayrollDetailStateType.ANALISIS1.getCode());
			lstValidStates.add(PayrollDetailStateType.ANALISIS2.getCode());
			lstValidStates.add(PayrollDetailStateType.ANALYZED.getCode());
			lstValidStates.add(PayrollDetailStateType.VAULT.getCode());
			query.setParameter("lstState", lstValidStates );
		}
		if(searchCertificateVaultTO.getEntryVault()!=null) {
			query.setParameter("currentVault", searchCertificateVaultTO.getEntryVault());
		}
		if( Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer()) && 
			Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer().getIdIssuerPk())) {
			query.setParameter("idIssuerPk", searchCertificateVaultTO.getIssuer().getIdIssuerPk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getPayRollNumber())) {
			query.setParameter("payRollNumber", searchCertificateVaultTO.getPayRollNumber());
		}
		query.setParameter("initialDate", searchCertificateVaultTO.getInitialDate());
		query.setParameter("finalDate", searchCertificateVaultTO.getFinalDate());
		
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchCertificateVaultTO.getIdParticipantPk());
		return query.getResultList();
	}

	public List<Object[]> getPayrollDetailRemoveSecurities(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		/*
		sbQuery.append("	SELECT pd.retirementOperation.custodyOperation.idCustodyOperationPk, ");//0
		sbQuery.append("	pd.retirementOperation.custodyOperation.registryDate, ");//1
		sbQuery.append("	pd.payrollHeader.idPayrollHeaderPk, ");//2
		sbQuery.append("	pd.retirementOperation.security.idSecurityCodePk, ");//3
		sbQuery.append("	pd.payrollQuantity, ");//4
		sbQuery.append("	pd.idPayrollDetailPk, ");//5
		sbQuery.append("	pd.payrollState, ");//6
		sbQuery.append("	0, ");//7 - pd.branchOffice
		sbQuery.append("	pd.currentVault ");//8
		sbQuery.append("	FROM PayrollDetail pd");
		sbQuery.append("	WHERE 1 = 1 ");//pd.payrollHeader.branchOffice = :branchOffice
		sbQuery.append("	AND pd.payrollHeader.payrollType = :payrollType");
		sbQuery.append("	AND pd.payrollState IN (:stateGenerated)");
		sbQuery.append("	AND pd.indCupon = :indCupon");
		*/
		sbQuery.append("	SELECT pd.retirementOperation.custodyOperation.idCustodyOperationPk,");//0
		sbQuery.append("	pd.retirementOperation.custodyOperation.registryDate,");//1
		sbQuery.append("	pd.payrollHeader.idPayrollHeaderPk,");//2
		sbQuery.append("	pd.retirementOperation.security.idSecurityCodePk,");//3
		sbQuery.append("	pd.retirementOperation.holderAccount.idHolderAccountPk,");//4	pd.accountAnnotationOperation.holderAccount.idHolderAccountPk
		sbQuery.append("	pd.payrollQuantity,");//5
		sbQuery.append("	pd.idPayrollDetailPk,");//6
		sbQuery.append("	pd.payrollState,");//7		
		sbQuery.append("	0, ");//8	- pd.accountAnnotationOperation.indSerializable
		sbQuery.append("	0, ");//9 - pd.branchOffice
		sbQuery.append("	pd.retirementOperation.physicalCertificate.idPhysicalCertificatePk, ");//10	-	titulo del cupon
		sbQuery.append("	pd.indCupon, ");//11
		sbQuery.append("	pd.cuponNumber, ");//12
		sbQuery.append("	pd.serialCode, ");//13
		sbQuery.append("	pd.currentVault, ");//14
		sbQuery.append("	pd.retirementOperation.physicalCertificate.nominalValue, ");//15
		sbQuery.append("	pd.retirementOperation.participant.mnemonic, ");//16
		sbQuery.append("	(SELECT aao.idAnnotationOperationPk FROM AccountAnnotationOperation aao WHERE aao.physicalCertificate.idPhysicalCertificatePk = pd.retirementOperation.physicalCertificate.idPhysicalCertificatePk ) as idAnnotationOperationPk, ");//17
		sbQuery.append("	pd.retirementOperation.motive, ");//18
		sbQuery.append("	pd.retirementOperation.security.issuer.mnemonic ");//19
		sbQuery.append("	FROM PayrollDetail pd ");
		sbQuery.append("	WHERE 1 = 1  ");
		sbQuery.append("	AND pd.payrollHeader.payrollType = :payrollType");
		sbQuery.append("	AND pd.payrollHeader.registerDate >= :initialDate ");
		sbQuery.append("	AND pd.payrollHeader.registerDate <= :finalDate ");
		sbQuery.append("	AND pd.payrollHeader.indApprove = :indApprove ");
		sbQuery.append("	AND pd.payrollState IN (:stateGenerated) ");
		if(searchCertificateVaultTO.getMotive()!=null) {
			sbQuery.append("	AND pd.retirementOperation.motive = :motive ");
		}
		//sbQuery.append("	AND pd.indCupon = :indCupon");
		
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIdParticipantPk())) {
			sbQuery.append("	AND pd.payrollHeader.participant.idParticipantPk = :idParticipantPk");
		}

		if( Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer()) && 
				Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer().getIdIssuerPk())) {
			sbQuery.append("	AND pd.retirementOperation.security.issuer.idIssuerPk = :idIssuerPk");
		}
		sbQuery.append("	ORDER BY pd.retirementOperation.custodyOperation.idCustodyOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		//query.setParameter("branchOffice", searchCertificateVaultTO.getBranchOffice());
		query.setParameter("payrollType", searchCertificateVaultTO.getPayRollType());
		query.setParameter("stateGenerated", PayrollDetailStateType.GENERATED.getCode());
		if(searchCertificateVaultTO.getMotive()!=null) {
			query.setParameter("motive", searchCertificateVaultTO.getMotive());
		}
		//query.setParameter("indCupon", 0);
		
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIdParticipantPk())) {
			query.setParameter("idParticipantPk", searchCertificateVaultTO.getIdParticipantPk());
		}

		if( Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer()) && 
			Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIssuer().getIdIssuerPk())) {
			query.setParameter("idIssuerPk", searchCertificateVaultTO.getIssuer().getIdIssuerPk());
		}

		query.setParameter("initialDate", searchCertificateVaultTO.getInitialDate());
		query.setParameter("finalDate", searchCertificateVaultTO.getFinalDate());
		query.setParameter("indApprove", 1);
		
		return query.getResultList();
	}

public List<VaultSummary> getVaultSummaryAll(List<String> lstBoveda) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT  ");
		sbQuery.append(" subt.ARMARIO , subt.NIVEL , subt.CAJA , subt.FOLIO , subt.INDICE_CAJA , ");	//0 1 2 3 4
		sbQuery.append(" subt.SERIE,  ");	//5
		sbQuery.append(" subt.MNEMONIC, ");	//6
		sbQuery.append(" TO_CHAR(subt.REGISTRY_DATE,'DD/MM/YYYY') AS REGISTRY_DATE, ");	//7
		sbQuery.append(" TO_CHAR(subt.EXPIRATION_DATE,'DD/MM/YYYY') AS EXPIRATION_DATE, ");	//8
		sbQuery.append(" subt.ID_PARTICIPANT_FK, ");	//9
		sbQuery.append(" subt.ID_HOLDER_ACCOUNT_FK, ");  //10
		sbQuery.append(" subt.SECURITY_NOMINAL_VALUE, ");	//11
		sbQuery.append(" subt.COUPON_NUMBER, ");		//12
		sbQuery.append(" (SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK = subt.SECURITY_CURRENCY) AS CURRENCY, ");	//13
		sbQuery.append(" subt.CERTIFICATE_QUANTITY AS QUANTITY, "); 	//14
		sbQuery.append(" (CASE WHEN subt.CERTIFICATE_FROM IS NOT NULL AND subt.CERTIFICATE_TO IS NOT NULL  ");
		sbQuery.append(" THEN TO_CHAR(subt.CERTIFICATE_FROM) || ' - ' || TO_CHAR(subt.CERTIFICATE_TO) ELSE NULL END) AS CERTIFICATE_FROM_TO ");	//15
		sbQuery.append(" FROM  ");
		sbQuery.append(" ( ");
		sbQuery.append(" SELECT  ");
		sbQuery.append(" cb.ARMARIO , cb.NIVEL , cb.CAJA , cb.FOLIO , cb.INDICE_CAJA , ");
		sbQuery.append(" pc.SERIAL_NUMBER || pc.CERTIFICATE_NUMBER || nvl2(TO_CHAR(cb.COUPON_NUMBER), '-', '') || cb.COUPON_NUMBER AS SERIE,  ");
		sbQuery.append(" i.MNEMONIC, ");
		sbQuery.append(" cb.REGISTRY_DATE, ");
		sbQuery.append(" pc.EXPIRATION_DATE, ");
		sbQuery.append(" ha2.ID_PARTICIPANT_FK, ");
		sbQuery.append(" aao.ID_HOLDER_ACCOUNT_FK, ");
		sbQuery.append(" aao.SECURITY_NOMINAL_VALUE, ");
		sbQuery.append(" cb.COUPON_NUMBER, ");
		sbQuery.append(" aao.SECURITY_CURRENCY, ");
		sbQuery.append(" pc.CERTIFICATE_QUANTITY, ");
		sbQuery.append(" aao.CERTIFICATE_FROM, ");
		sbQuery.append(" aao.CERTIFICATE_TO ");
		sbQuery.append(" FROM ACCOUNT_ANNOTATION_OPERATION aao  ");
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK  ");
		sbQuery.append(" INNER JOIN CONTROL_BOVEDA cb ON cb.ID_SECURITY_CODE_FK = aao.ID_SECURITY_CODE_FK AND cb.ID_PHYSICAL_CERTIFICATE_FK = aao.ID_PHYSICAL_CERTIFICATE_FK ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN ISSUER i ON aao.ID_ISSUER_FK = i.ID_ISSUER_PK ");
		sbQuery.append(" WHERE  ");
		sbQuery.append(" aao.ID_SECURITY_CODE_FK IS NOT NULL AND aao.STATE_ANNOTATION = 858 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null  ");
		sbQuery.append(" UNION  ");
		sbQuery.append(" SELECT  ");
		sbQuery.append(" cb.ARMARIO , cb.NIVEL , cb.CAJA , cb.FOLIO , cb.INDICE_CAJA , ");
		sbQuery.append(" pc.SERIAL_NUMBER || pc.CERTIFICATE_NUMBER || nvl2(TO_CHAR(cb.COUPON_NUMBER), '-', '') || cb.COUPON_NUMBER AS SERIE,  ");
		sbQuery.append(" i.MNEMONIC, ");
		sbQuery.append(" cb.REGISTRY_DATE, ");
		sbQuery.append(" pc.EXPIRATION_DATE, ");
		sbQuery.append(" ha2.ID_PARTICIPANT_FK, ");
		sbQuery.append(" aao.ID_HOLDER_ACCOUNT_FK, ");
		sbQuery.append(" aac.CUPON_AMOUNT, ");
		sbQuery.append(" cb.COUPON_NUMBER, ");
		sbQuery.append(" aao.SECURITY_CURRENCY, ");
		sbQuery.append(" pc.CERTIFICATE_QUANTITY, ");
		sbQuery.append(" aao.CERTIFICATE_FROM, ");
		sbQuery.append(" aao.CERTIFICATE_TO ");
		sbQuery.append(" FROM ACCOUNT_ANNOTATION_OPERATION aao  ");
		sbQuery.append(" INNER JOIN ACCOUNT_ANNOTATION_CUPON aac ON aac.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK ");
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = aao.ID_PHYSICAL_CERTIFICATE_FK  ");
		sbQuery.append(" INNER JOIN CONTROL_BOVEDA cb ON cb.ID_SECURITY_CODE_FK = aao.ID_SECURITY_CODE_FK AND cb.COUPON_NUMBER = aac.COUPON_NUMBER  ");
		sbQuery.append(" INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK ");
		sbQuery.append(" INNER JOIN ISSUER i ON aao.ID_ISSUER_FK = i.ID_ISSUER_PK ");
		sbQuery.append(" WHERE  ");
		sbQuery.append(" aao.ID_SECURITY_CODE_FK IS NOT NULL AND aao.STATE_ANNOTATION = 858   ");
		sbQuery.append(" AND aao.IND_ELECTRONIC_CUPON = 0 AND aao.IND_SERIALIZABLE = 0 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null ");
		sbQuery.append(" ) subt  ");
		sbQuery.append(" WHERE 1 = 1 ");
		if(lstBoveda!= null && lstBoveda.size() >0) {
			sbQuery.append(" AND (subt.ARMARIO ||' - '|| subt.NIVEL ||' - '|| subt.CAJA ||' - '|| subt.FOLIO ||' - '|| subt.INDICE_CAJA) in (:boveda) ");
		}
		sbQuery.append(" ORDER BY subt.ARMARIO , subt.NIVEL , subt.CAJA , subt.FOLIO , subt.INDICE_CAJA ASC ");

		
		Query query = em.createNativeQuery(sbQuery.toString());

		if(lstBoveda!= null && lstBoveda.size() >0) {
			query.setParameter("boveda", lstBoveda);
		}
		
		List<Object[]> lstResult = query.getResultList();
		List<VaultSummary> listVaultSummary = new ArrayList<VaultSummary>();
		
        for (Object[] objResult:lstResult) {
        	VaultSummary vaultSummary = new VaultSummary();
        	vaultSummary.setArmario(objResult[0].toString());
        	vaultSummary.setNivel(objResult[1].toString());
        	vaultSummary.setCaja(objResult[2].toString());
        	vaultSummary.setFolio(objResult[3].toString());
        	vaultSummary.setIndiceCaja(objResult[4].toString());
        	vaultSummary.setSerie(objResult[5].toString());
        	vaultSummary.setEmisor(objResult[6].toString());
        	if(objResult[7]!=null) {
        		vaultSummary.setFechaIngreso(objResult[7].toString());
        	}
        	if(objResult[8]!=null) {
        		vaultSummary.setFechaVencimiento(objResult[8].toString());
        	}
    		Long idParticipantPk = Long.valueOf(objResult[9].toString());
    		vaultSummary.setSecurityNominalValue(objResult[11].toString());
    		vaultSummary.setCurrency(objResult[13].toString());
    		vaultSummary.setQuantity(Long.valueOf(objResult[14].toString()));
    		if(objResult[15]!=null) {
    			vaultSummary.setCertificateFromTo(objResult[15].toString());
        	}else {
        		vaultSummary.setCertificateFromTo("");
        	}
    		
        	Participant participant = find(Participant.class, idParticipantPk);
        	if(participant!=null) {
        		vaultSummary.setDepositante(participant.getDescription());
        	}
    		/*HolderAccountBalance balance = getUniqueHolderAccountBalance(vaultSummary.getIdSecurityCodePk());
    		if(balance!=null) {
    			vaultSummary.setDepositante(balance.getParticipant().getDescription());
    		}*/
    		
        	listVaultSummary.add(vaultSummary);
        }
		
		return listVaultSummary;
		
	}

	public List<VaultSummary> getVaultSummary(List<String> lstIdSecurityCodePk) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(" SELECT ");
		sbQuery.append(" 	CB.ARMARIO, ");//0
		sbQuery.append(" 	CB.NIVEL, ");//1
		sbQuery.append(" 	CB.CAJA, ");//2
		sbQuery.append(" 	CB.FOLIO, ");//3
		sbQuery.append(" 	CB.INDICE_CAJA, ");//4
		sbQuery.append(" 	PC.SERIAL_NUMBER || PC.CERTIFICATE_NUMBER || nvl2(TO_CHAR(CB.COUPON_NUMBER), '-', '') || CB.COUPON_NUMBER AS SERIE , ");//5
		sbQuery.append(" 	I.BUSINESS_NAME AS EMISOR, ");//6
		sbQuery.append(" 	TO_CHAR(CB.REGISTRY_DATE,'DD/MM/YYYY') AS INGRESO, ");//7
		sbQuery.append(" 	TO_CHAR(PC.EXPIRATION_DATE,'DD/MM/YYYY') AS EXPIRACION, ");//8
		sbQuery.append(" 	CB.ID_SECURITY_CODE_FK, ");//9
		sbQuery.append(" 	CB.ID_PHYSICAL_CERTIFICATE_FK ");//10
		sbQuery.append(" FROM CONTROL_BOVEDA CB ");
		sbQuery.append(" INNER JOIN PHYSICAL_CERTIFICATE PC  ");
		sbQuery.append(" 	ON PC.ID_PHYSICAL_CERTIFICATE_PK = CB.ID_PHYSICAL_CERTIFICATE_FK  ");
		sbQuery.append(" INNER JOIN ISSUER I ");
		sbQuery.append(" 	ON I.ID_ISSUER_PK = PC.ID_ISSUER_FK ");
		sbQuery.append(" WHERE 1 = 1 ");
		if(lstIdSecurityCodePk!= null && lstIdSecurityCodePk.size() >0) {
			sbQuery.append(" AND CB.ID_SECURITY_CODE_FK in (:idSecurityCodePk) ");
		}else {
			sbQuery.append(" AND CB.ID_SECURITY_CODE_FK IS NOT NULL ");
		}
		sbQuery.append(" ORDER BY CB.ARMARIO, CB.NIVEL, CB.CAJA , CB.FOLIO ,CB.INDICE_CAJA ");
		
		Query query = em.createNativeQuery(sbQuery.toString());

		if(lstIdSecurityCodePk!= null && lstIdSecurityCodePk.size() >0) {
			query.setParameter("idSecurityCodePk", lstIdSecurityCodePk);
		}
		
		List<Object[]> lstResult = query.getResultList();
		List<VaultSummary> listVaultSummary = new ArrayList<VaultSummary>();
		
        for (Object[] objResult:lstResult) {
        	VaultSummary vaultSummary = new VaultSummary();
        	vaultSummary.setArmario(objResult[0].toString());
        	vaultSummary.setNivel(objResult[1].toString());
        	vaultSummary.setCaja(objResult[2].toString());
        	vaultSummary.setFolio(objResult[3].toString());
        	vaultSummary.setIndiceCaja(objResult[4].toString());
        	vaultSummary.setSerie(objResult[5].toString());
        	vaultSummary.setEmisor(objResult[6].toString());
        	if(objResult[7]!=null) {
        		vaultSummary.setFechaIngreso(objResult[7].toString());
        	}
        	if(objResult[8]!=null) {
        		vaultSummary.setFechaVencimiento(objResult[8].toString());
        	}
    		vaultSummary.setIdSecurityCodePk(objResult[9].toString());

        	if(objResult[10]!=null) {
        		vaultSummary.setIdPhysicalCertificatePk(new Long(objResult[10].toString())); //FechaVencimiento(objResult[8].toString());
        	}
    		
        	Participant participant = getParticipantWithAnnotationCertificate(vaultSummary.getIdPhysicalCertificatePk());
        	if(participant!=null) {
        		vaultSummary.setDepositante(participant.getDescription());
        	}
    		/*HolderAccountBalance balance = getUniqueHolderAccountBalance(vaultSummary.getIdSecurityCodePk());
    		if(balance!=null) {
    			vaultSummary.setDepositante(balance.getParticipant().getDescription());
    		}*/
    		
        	listVaultSummary.add(vaultSummary);
        }
		
		return listVaultSummary;
		
	}
	
	public HolderAccountBalance getUniqueHolderAccountBalance(String idSecurityCodePk) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hab ");
		sbQuery.append("	FROM HolderAccountBalance hab ");
		sbQuery.append("	JOIN FETCH hab.participant p ");
		sbQuery.append("	WHERE 1 = 1 ");
		sbQuery.append("	AND hab.totalBalance > 0 ");
		sbQuery.append("	AND hab.security.idSecurityCodePk = :idSecurityCodePk");
		
		TypedQuery<HolderAccountBalance> query = em.createQuery(sbQuery.toString(), HolderAccountBalance.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
		List<HolderAccountBalance> lstHolderAccountBalance = query.getResultList();
		if(lstHolderAccountBalance!= null && lstHolderAccountBalance.size() >0) {
			return lstHolderAccountBalance.get(0);
		}
		
		return null;
	}
	
	public Participant getParticipantWithAnnotationCertificate(Long idPhysicalCertificatePk) {

		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT aao ");
		sbQuery.append("	FROM AccountAnnotationOperation aao ");
		sbQuery.append("	JOIN FETCH aao.physicalCertificate pc ");
		sbQuery.append("	JOIN FETCH aao.holderAccount ha ");
		sbQuery.append("	JOIN FETCH ha.participant p ");
		sbQuery.append("	WHERE 1 = 1 ");
		sbQuery.append("	AND pc.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
		TypedQuery<AccountAnnotationOperation> query = em.createQuery(sbQuery.toString(), AccountAnnotationOperation.class);
		query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
		/*
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT hab ");
		sbQuery.append("	FROM HolderAccountBalance hab ");
		sbQuery.append("	JOIN FETCH hab.participant p ");
		sbQuery.append("	WHERE 1 = 1 ");
		sbQuery.append("	AND hab.totalBalance > 0 ");
		sbQuery.append("	AND hab.security.idSecurityCodePk = :idSecurityCodePk");
		
		TypedQuery<HolderAccountBalance> query = em.createQuery(sbQuery.toString(), HolderAccountBalance.class);
		query.setParameter("idSecurityCodePk", idSecurityCodePk);
		
		List<HolderAccountBalance> lstHolderAccountBalance = query.getResultList();
		if(lstHolderAccountBalance!= null && lstHolderAccountBalance.size() >0) {
			return lstHolderAccountBalance.get(0);
		}
		*/

		List<AccountAnnotationOperation> lstAccountAnnotationOperation = query.getResultList();
		if(lstAccountAnnotationOperation!= null && lstAccountAnnotationOperation.size() >0) {
			return lstAccountAnnotationOperation.get(0).getHolderAccount().getParticipant();
		}
		/*
		List<Participant> lstParticipant = query.getResultList();
		if(lstParticipant!= null && lstParticipant.size() >0) {
			return lstParticipant.get(0);
		}
		*/
		return null;
	}
	
	public PhysicalCertificate getPhysicalCertificate(Long idPhysicalCertificatePk) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pc ");
		sbQuery.append("	FROM PhysicalCertificate pc ");
		sbQuery.append("	WHERE pc.idPhysicalCertificatePk = :idPhysicalCertificatePk ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPhysicalCertificatePk", idPhysicalCertificatePk);
		
		return (PhysicalCertificate) query.getSingleResult();
		
	}
	

	public List<PhysicalCertificate> getPhysicalCertificates(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pc");
		sbQuery.append("	FROM PhysicalCertificate pc");
		sbQuery.append("	WHERE 1 = 1 ");//pc.branchOffice = :branchOffice
		sbQuery.append("	AND pc.situation = :vault");
		sbQuery.append("	AND pc.state = :registered");
		sbQuery.append("	AND pc.securities.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	ORDER BY pc.idPhysicalCertificatePk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		//query.setParameter("branchOffice", searchCertificateVaultTO.getBranchOffice());
		query.setParameter("idSecurityCodePk", searchCertificateVaultTO.getPayrollDetailTOSelected().getIdSecurityCodePk());
		query.setParameter("vault", PayrollDetailStateType.VAULT.getCode());//SituationType.CUSTODY_VAULT.getCode()
		query.setParameter("registered", PhysicalCertificateStateType.CONFIRMED.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchCertificateVaultTO.getIdParticipantPk());
		return query.getResultList();
	}

	public List<PhysicalCertificate> getPhysicalCertificatesWithoutCupons(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pc");
		sbQuery.append("	FROM PhysicalCertificate pc");
		sbQuery.append("	WHERE pc.branchOffice = :branchOffice");
		sbQuery.append("	AND pc.situation = :vault");
		sbQuery.append("	AND pc.state = :registered");
		sbQuery.append("	AND pc.securities.idSecurityCodePk = :idSecurityCodePk");
		sbQuery.append("	AND pc.PhysicalCertificateReference is null ");
		sbQuery.append("	ORDER BY pc.idPhysicalCertificatePk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("branchOffice", searchCertificateVaultTO.getBranchOffice());
		query.setParameter("idSecurityCodePk", searchCertificateVaultTO.getPayrollDetailTOSelected().getIdSecurityCodePk());
		query.setParameter("vault", PayrollDetailStateType.VAULT.getCode());
		query.setParameter("registered", PhysicalCertificateStateType.CONFIRMED.getCode());
		if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getIdParticipantPk()))
			query.setParameter("idParticipantPk", searchCertificateVaultTO.getIdParticipantPk());
		return query.getResultList();
	}
	
	//
	public List<PayrollDetailTO> searchPayrollDetailCorporative(SearchCertificateVaultTO to) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("	SELECT ");
		sbQuery.append("	  ISS.mnemonic, ");//0
		sbQuery.append("	  PH.branchOffice, ");//1
		sbQuery.append("	  PD.vaultLocation, ");//2
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//3
		sbQuery.append("  	  CO.idCorporativeOperationPk, ");//4
		sbQuery.append("	  '''', ");//5
		sbQuery.append("	  CURRENT_DATE, ");//6
		sbQuery.append("	  CURRENT_DATE, ");//7
		sbQuery.append("	  CO.securities.idSecurityCodePk, ");//8
		sbQuery.append("	  '''' ");//9
		
		sbQuery.append("	FROM PayrollHeader PH ");
		sbQuery.append("	  INNER JOIN PH.lstPayrollDetail PD ");
		sbQuery.append("	  INNER JOIN PH.issuer ISS ");
		sbQuery.append("	  INNER JOIN PD.corporativeOperation CO ");

		sbQuery.append("	WHERE PH.payrollType = :payrollType");
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			sbQuery.append("  AND PH.branchOffice = :branchOffice");
		if(Validations.validateIsNotNull(to.getIdIssuerPk()))
			sbQuery.append("  AND ISS.idIssuerPk = :idIssuerPk");
		if(Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("  AND co.securities.idSecurityCodePk = :idSecurityCodePk");
		
		sbQuery.append("	ORDER BY CO.idCorporativeOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", to.getPayRollType());
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			query.setParameter("branchOffice", to.getBranchOffice());
		if(Validations.validateIsNotNull(to.getIdIssuerPk()))
			query.setParameter("idIssuerPk", to.getIdIssuerPk());
		if(Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		
		List<Object[]> lstResult = query.getResultList();
		List<PayrollDetailTO> listPayrollDetailTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
        	payrollDetailTO.setMnemonicParticipant(objResult[0].toString());
        	payrollDetailTO.setBranchOffice(new Integer(objResult[1].toString()));
        	payrollDetailTO.setVaultLocation(Validations.validateIsNotNull(objResult[2]) ? new Integer(objResult[2].toString()) : null);
        	payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[3].toString()));
        	payrollDetailTO.setIdCorporativeOperationPk(new Long(objResult[4].toString()));
        	
        	payrollDetailTO.setEntryDate((Date) objResult[6]);
        	payrollDetailTO.setExitDate((Date) objResult[7]);
        	payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[8]) ? objResult[8].toString() : null);
        	listPayrollDetailTO.add(payrollDetailTO);
        }
		
		return listPayrollDetailTO;
	}
	public List<PayrollDetailTO> searchPayrollDetailRemoveSecuritiesLocation(SearchCertificateVaultTO to) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("	SELECT ");
		sbQuery.append("	  PA.mnemonic, ");//0
		sbQuery.append("	  PC.branchOffice, ");//1
		sbQuery.append("	  PC.vaultLocation, ");//2
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//3
		sbQuery.append("  	  CU.idCustodyOperationPk, ");//4
		sbQuery.append("	  PC.certificateNumber, ");//5
		sbQuery.append("	  CURRENT_DATE, ");//6
		sbQuery.append("	  PC.securities.idSecurityCodePk, ");//7
		sbQuery.append("	  PC.situation, ");//8
		sbQuery.append("	  HA.idHolderAccountPk, ");//9
		sbQuery.append("	  PC.securities.issuer.mnemonic, ");//10
		sbQuery.append("	  PD.serialCode, ");//11
		sbQuery.append("	  PC.expirationDate, ");//12
		sbQuery.append("	  PC.nominalAmount, ");//13
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//14
		sbQuery.append("	  RO.motive ");//1
		sbQuery.append("	FROM PayrollDetail PD ");
		sbQuery.append("	  INNER JOIN PD.payrollHeader PH ");
		sbQuery.append("	  INNER JOIN PD.retirementOperation RO ");
		sbQuery.append("	  INNER JOIN RO.custodyOperation CU ");
		//sbQuery.append("	  INNER JOIN RO.retirementDetails RD ");
		sbQuery.append("	  INNER JOIN RO.holderAccount HA ");
		sbQuery.append("	  INNER JOIN RO.participant PA ");
		//sbQuery.append("	  ,RetirementCertificate RC ");
		sbQuery.append("	  INNER JOIN RO.physicalCertificate PC ");
		
		sbQuery.append("	WHERE 1 = 1 ");//RC.retirementOperation.idRetirementOperationPk = RO.idRetirementOperationPk
		sbQuery.append("	  AND PH.payrollType = :payrollType ");
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			sbQuery.append("  AND PH.branchOffice = :branchOffice ");
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			sbQuery.append("  AND PA.idParticipantPk = :idParticpantPk ");
		if(Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("  AND PC.securities.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateIsNotNull(to.getSituation()))
			sbQuery.append("  AND PC.situation = :situation");
		if(Validations.validateIsNotNull(to.getInitialDate()))
			sbQuery.append("  AND PC.registerDate >= :initialRegisterDate");
		if(Validations.validateIsNotNull(to.getFinalDate()))
			sbQuery.append("  AND PC.registerDate <= :finalRegisterDate");
		
		sbQuery.append("	ORDER BY CU.idCustodyOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", to.getPayRollType());
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			query.setParameter("branchOffice", to.getBranchOffice());
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			query.setParameter("idParticpantPk", to.getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNull(to.getSituation()))
			query.setParameter("situation", to.getSituation());
		if(Validations.validateIsNotNull(to.getInitialDate()))
			query.setParameter("initialRegisterDate", to.getInitialDate());
		if(Validations.validateIsNotNull(to.getFinalDate()))
			query.setParameter("finalRegisterDate", to.getFinalDate());
		
		List<Object[]> lstResult = query.getResultList();
		List<PayrollDetailTO> listPayrollDetailTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
        	payrollDetailTO.setMnemonicParticipant(objResult[0].toString());
        	payrollDetailTO.setBranchOffice(Validations.validateIsNotNull(objResult[1]) ? new Integer(objResult[1].toString()) : null);
        	payrollDetailTO.setVaultLocation(Validations.validateIsNotNull(objResult[2]) ? new Integer(objResult[2].toString()) : null);
        	payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[3].toString()));
        	payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[4].toString()));
        	payrollDetailTO.setCertificateNumber(objResult[5].toString());
        	payrollDetailTO.setEntryDate((Date) objResult[6]);
        	payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[7]) ? objResult[7].toString() : null);
        	payrollDetailTO.setSituation(new Integer(objResult[8].toString()));
        	HolderAccountTO temp = new HolderAccountTO();
			temp.setIdHolderAccountPk(new Long(objResult[9].toString()));
			payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));		
			payrollDetailTO.setMnemonicIssuer(Validations.validateIsNotNull(objResult[10]) ? objResult[10].toString() : "");
			payrollDetailTO.setSerialCode(Validations.validateIsNotNull(objResult[11]) ? objResult[11].toString() : "");

			payrollDetailTO.setExpirationDate((Date) objResult[12]);
			payrollDetailTO.setSecurityNominalValue((BigDecimal) objResult[13]);
			payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[14].toString()));
        	payrollDetailTO.setMotive(Validations.validateIsNotNull(objResult[15]) ? new Integer(objResult[15].toString()) : null);
			payrollDetailTO.setPayRollType(PayrollType.REMOVE_SECURITIES_OPERATION.getCode());
        	listPayrollDetailTO.add(payrollDetailTO);
        }
		
		return listPayrollDetailTO;
	}
	
	public List<PayrollDetailTO> searchHolding(SearchCertificateVaultTO to) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT  ");
		sbQuery.append("	( CASE WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NULL  THEN 'ELECTRONICO'  ");
		sbQuery.append("		 WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NOT NULL THEN pt.PARAMETER_NAME END ) AS MOTIVO , ");							//0
		sbQuery.append("	p.DESCRIPTION ,  ");																										//1
		sbQuery.append("	i.BUSINESS_NAME , ");																										//2
		sbQuery.append("	s.ID_SECURITY_CODE_PK, ");																									//3
		sbQuery.append("	( CASE WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NULL  THEN s.SECURITY_SERIAL  ");
		sbQuery.append("		 WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NOT NULL THEN  ");
		sbQuery.append("		 	(CASE WHEN pc.IND_CUPON = 0 THEN pc.SERIAL_NUMBER  || pc.CERTIFICATE_NUMBER ");
		sbQuery.append("		 	      WHEN pc.IND_CUPON = 1 THEN  pc.SERIAL_NUMBER  || pc.CERTIFICATE_NUMBER || '-' || pc.CUPON_NUMBER END ) ");
		sbQuery.append("		 END ) AS CERTIFICADO , ");																								//4
		sbQuery.append("	( CASE WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NULL  THEN s.INITIAL_NOMINAL_VALUE ");
		sbQuery.append("		 WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NOT NULL THEN pc.NOMINAL_VALUE END ) AS NOMINAL_VALUE , ");						//5
		sbQuery.append("	( CASE WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NULL  THEN s.EXPIRATION_DATE ");
		sbQuery.append("		 WHEN pc.ID_PHYSICAL_CERTIFICATE_PK IS NOT NULL THEN pc.EXPIRATION_DATE END ) AS EXPIRATION_DATE , ");					//6
		sbQuery.append("	HAB.TOTAL_BALANCE , ");																										//7
		sbQuery.append("	HAB.AVAILABLE_BALANCE, ");																									//8
		sbQuery.append("	HAB.TRANSIT_BALANCE , ");																									//9
		sbQuery.append("	hab.ID_PARTICIPANT_PK,  ");
		sbQuery.append("	pc.ID_PHYSICAL_CERTIFICATE_PK ,  ");
		sbQuery.append("	pc.IND_CUPON,  ");
		sbQuery.append("	pc.MOTIVE  ");
		sbQuery.append("	FROM SECURITY s  ");
		sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE hab ON hab.ID_SECURITY_CODE_PK = s.ID_SECURITY_CODE_PK  ");
		sbQuery.append("	INNER JOIN PARTICIPANT p ON p.ID_PARTICIPANT_PK = hab.ID_PARTICIPANT_PK  ");
		sbQuery.append("	INNER JOIN ISSUER i ON i.ID_ISSUER_PK = s.ID_ISSUER_FK  ");
		sbQuery.append("	LEFT JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_SECURITY_CODE_FK = s.ID_SECURITY_CODE_PK  AND pc.STATE IN (601) AND pc.IND_CUPON = 0  ");
		sbQuery.append("	LEFT JOIN PARAMETER_TABLE pt ON pt.PARAMETER_TABLE_PK = pc.MOTIVE  ");
		sbQuery.append("	WHERE s.STATE_SECURITY NOT IN (2033,2848) AND hab.TOTAL_BALANCE > 0 ");

		if(Validations.validateIsNotNull(to.getIdParticipantPk())) {
			sbQuery.append("  AND p.ID_PARTICIPANT_PK = :getIdParticipantPk");
		}
		if(Validations.validateIsNotNull(to.getIdIssuerPk())) {
			sbQuery.append("  AND i.ID_ISSUER_PK = :getIdIssuerPk");
		}
		if(Validations.validateIsNotNull(to.getIdSecurityCodePk())) {
			sbQuery.append("  AND s.ID_SECURITY_CODE_PK = :getIdSecurityCodePk");
		}
		
		sbQuery.append("	ORDER BY s.REGISTRY_DATE  ");

		 		
		Query query = em.createNativeQuery(sbQuery.toString());
		

		if(Validations.validateIsNotNull(to.getIdParticipantPk()) ) {
			query.setParameter("getIdParticipantPk", to.getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(to.getIdIssuerPk()) ) {
			query.setParameter("getIdIssuerPk", to.getIdIssuerPk());
		}
		if(Validations.validateIsNotNull(to.getIdSecurityCodePk()) ) {
			query.setParameter("getIdSecurityCodePk", to.getIdSecurityCodePk());
		}
		
		List<Object[]> lstResult = query.getResultList();
		List<PayrollDetailTO> listPayrollDetailTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
        	if(objResult[0]!=null) {
        		payrollDetailTO.setAnnotationMotiveDescription(objResult[0].toString());
        	}
        	if(objResult[1]!=null) {
        		payrollDetailTO.setMnemonicParticipant(objResult[1].toString());
        	}
        	if(objResult[2]!=null) {
        		payrollDetailTO.setIssuerDesc(objResult[2].toString());
        	}
        	if(objResult[3]!=null) {
        		payrollDetailTO.setIdSecurityCodePk(objResult[3].toString());
        	}
        	if(objResult[4]!=null) {
        		payrollDetailTO.setSerialCode(objResult[4].toString());
        	}
        	if(objResult[5]!=null) {
        		payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[5]);
        	}
        	if(objResult[6]!=null) {
        		payrollDetailTO.setExpirationDate((Date)objResult[6]);
        	}
        	if(objResult[7]!=null) {
        		payrollDetailTO.setTotalBalance((BigDecimal)objResult[7]);
        	}
        	if(objResult[8]!=null) {
        		payrollDetailTO.setAvailableBalance((BigDecimal)objResult[8]);
        	}
        	if(objResult[9]!=null) {
        		payrollDetailTO.setTransitBalance((BigDecimal)objResult[9]);
        	}
        	
        	listPayrollDetailTO.add(payrollDetailTO);
        }
		
		return listPayrollDetailTO;
	}
	
	public List<PhysicalCertificateTO> searchPhysicalCerticate(SearchCertificateVaultTO to) throws ServiceException{
		
		StringBuilder sbQuery = new StringBuilder();
		if(to.getFinalDate().before(CommonsUtilities.currentDate())){

			sbQuery.append("	SELECT  "); 
			sbQuery.append("    subt.fecha_ingreso, ");
			
			sbQuery.append("    (SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK  												");
			sbQuery.append("    	= (SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK)) AS motivo_ingreso,  	");
			
			sbQuery.append("	(SELECT LISTAGG(had.ID_HOLDER_FK, '-') WITHIN GROUP (ORDER BY had.ID_HOLDER_FK) 											");
			sbQuery.append("		FROM HOLDER_ACCOUNT_DETAIL had WHERE had.ID_HOLDER_ACCOUNT_FK =subt.ID_HOLDER_ACCOUNT_PK) AS RUT, 							");
			
			sbQuery.append("	(SELECT ha.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT ha WHERE ha.ID_HOLDER_ACCOUNT_PK = subt.ID_HOLDER_ACCOUNT_PK) AS CUENTA, 		");
			sbQuery.append("	FN_GET_HOLDER_DESC(subt.ID_HOLDER_ACCOUNT_PK, '-',0) AS NOBRE_CLIENTE, 														");
			sbQuery.append("	(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK =subt.security_class) AS tipo_valor, 			");
			sbQuery.append("	subt.ID_SECURITY_CODE_PK AS codigo_valor, 																					"); 
			
			sbQuery.append("	((SELECT aao2.SERIAL_NUMBER FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK) ");
			sbQuery.append("		||(SELECT aao2.CERTIFICATE_NUMBER FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK)||	");
			sbQuery.append("		CASE WHEN subt.numero_cupon IS NOT NULL THEN '-'||subt.numero_cupon END) AS serie, 											");
			
			sbQuery.append("	(CASE WHEN subt.cupon_electronico = 1 THEN 'SI' WHEN (subt.cupon_electronico = 0 AND subt.numero_cupon IS NULL) THEN NULL 	"); 
			sbQuery.append("		ELSE 'NO' END) AS cupon_electronico, 																						");
			
			sbQuery.append("	(SELECT i.MNEMONIC FROM ISSUER i WHERE i.ID_ISSUER_PK=subt.ID_ISSUER_FK)  AS emisor, 										");
			sbQuery.append("	(SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK=subt.security_currency) AS moneda, 				");
			
			sbQuery.append("	(CASE WHEN (SELECT aac.CUPON_AMOUNT FROM ACCOUNT_ANNOTATION_CUPON aac WHERE aac.ID_ANNOTATION_OPERATION_FK = subt.ID_ANNOTATION_OPERATION_PK 	");
			sbQuery.append("		AND subt.numero_cupon = aac.COUPON_NUMBER) IS NOT NULL 																	");
			sbQuery.append("		THEN (SELECT aac.CUPON_AMOUNT FROM ACCOUNT_ANNOTATION_CUPON aac WHERE aac.ID_ANNOTATION_OPERATION_FK = subt.ID_ANNOTATION_OPERATION_PK 	");
			sbQuery.append("		AND subt.numero_cupon = aac.COUPON_NUMBER) 																				");
			sbQuery.append("		ELSE (SELECT aao2.SECURITY_NOMINAL_VALUE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK) END) AS valor_nominal,	");
			
			sbQuery.append("	subt.EXPIRATION_DATE AS fecha_vencimiento, 																					");
			sbQuery.append("    CASE WHEN subt.quantity IS NULL THEN 1 ELSE subt.quantity END AS CANTIDAD, 													");
			sbQuery.append("	subt.CERTIFICATE_FROM AS desde , subt.CERTIFICATE_TO AS hasta, 																");
			sbQuery.append("	(SELECT aao3.REFERENCE_PART_CODE FROM ACCOUNT_ANNOTATION_OPERATION aao3 WHERE aao3.ID_ANNOTATION_OPERATION_PK=subt.ID_ANNOTATION_OPERATION_PK) AS observacion,	");
			sbQuery.append("	subt.cupon_electronico as electronico, 																						");
			sbQuery.append("	subt.numero_cupon as cuponNumber,																							");
			sbQuery.append("	subt.ID_ANNOTATION_OPERATION_PK as idAnnotationOperation																	");
			sbQuery.append("	FROM 																														");
			sbQuery.append("	(																															");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso ,																							");  
			sbQuery.append("	hab.ID_HOLDER_ACCOUNT_PK,																									");
			sbQuery.append("	s.security_class,																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK, 																										");
			sbQuery.append("	s.ID_ISSUER_FK, 																											");
			sbQuery.append("	s.currency AS security_currency, 																							");
			sbQuery.append("	pc.EXPIRATION_DATE,																											");
			sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
			sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, 																					");
			sbQuery.append("	NULL AS numero_cupon, 0 AS cupon_electronico, 																				");
			sbQuery.append("	aao.ID_ANNOTATION_OPERATION_PK, 																							");
			sbQuery.append("	hab.ID_PARTICIPANT_PK , 																									");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= aao.ID_ANNOTATION_OPERATION_PK) AS motive,																				");
			sbQuery.append("	cb.CUT_DATE AS BOVEDA_CUT_DATE																								");
			sbQuery.append("	FROM CONTROL_BOVEDA_HISTORY cb  																							");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK				");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE_HISTORY hab ON cb.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK							");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON hab.ID_HOLDER_ACCOUNT_PK = ha2.ID_HOLDER_ACCOUNT_PK 										");
			sbQuery.append("	INNER JOIN SECURITY s ON aao.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK 													");
			sbQuery.append("	WHERE 																														");
			sbQuery.append("	pc.ID_SECURITY_CODE_FK IS NOT NULL AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null 											");
			sbQuery.append("	AND hab.TOTAL_BALANCE > 0																									");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 124																									");
			sbQuery.append("	AND TRUNC(hab.CUT_DATE) = :finalRegisterDate																				");
			sbQuery.append("	UNION 																														");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso , 																						"); 
			sbQuery.append("	hab2.ID_HOLDER_ACCOUNT_PK, 																									");
			sbQuery.append("	s.security_class, 																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK,  																									");
			sbQuery.append("	s.ID_ISSUER_FK, 																											");
			sbQuery.append("	s.currency, 																												");
			sbQuery.append("	pc.EXPIRATION_DATE,																											");
			sbQuery.append("	(1) AS quantity, 																											");
			sbQuery.append("	NULL CERTIFICATE_FROM, NULL CERTIFICATE_TO,																					");
			sbQuery.append("	cb.COUPON_NUMBER, (SELECT aao2.IND_ELECTRONIC_CUPON FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK) AS IND_ELECTRONIC_CUPON,	");
			sbQuery.append("	(SELECT aao.ID_ANNOTATION_OPERATION_PK FROM ACCOUNT_ANNOTATION_OPERATION aao WHERE ID_PHYSICAL_CERTIFICATE_FK 				");
			sbQuery.append("	= (SELECT cb2.ID_PHYSICAL_CERTIFICATE_FK FROM CONTROL_BOVEDA cb2 															");
			sbQuery.append("	WHERE cb2.ID_SECURITY_CODE_FK = cb.ID_SECURITY_CODE_FK AND cb2.COUPON_NUMBER IS NULL)) AS ID_ANNOTATION_OPERATION_PK,		");
			sbQuery.append("	hab2.ID_PARTICIPANT_PK, 																									");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= (SELECT aao.ID_ANNOTATION_OPERATION_PK FROM ACCOUNT_ANNOTATION_OPERATION aao WHERE ID_PHYSICAL_CERTIFICATE_FK				");
			sbQuery.append("	= (SELECT cb2.ID_PHYSICAL_CERTIFICATE_FK FROM CONTROL_BOVEDA cb2 															");
			sbQuery.append("	WHERE cb2.ID_SECURITY_CODE_FK = cb.ID_SECURITY_CODE_FK AND cb2.COUPON_NUMBER IS NULL))) AS motive,							");
			sbQuery.append("	cb.CUT_DATE 																												");
			sbQuery.append("	FROM CONTROL_BOVEDA_HISTORY cb  																							");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE_HISTORY hab2 ON cb.ID_SECURITY_CODE_FK = hab2.ID_SECURITY_CODE_PK							");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON hab2.ID_HOLDER_ACCOUNT_PK = ha2.ID_HOLDER_ACCOUNT_PK 										");
			sbQuery.append("	INNER JOIN SECURITY s ON cb.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK 														");
			sbQuery.append("	WHERE  																														");
			sbQuery.append("	cb.ID_SECURITY_CODE_FK IS NOT NULL 								  															");
			sbQuery.append("	AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null 																				");
			sbQuery.append("	AND cb.COUPON_NUMBER IS NOT NULL 																							");
			sbQuery.append("	AND hab2.TOTAL_BALANCE > 0																									");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 124																									");
			sbQuery.append("	AND TRUNC(hab2.CUT_DATE) = :finalRegisterDate																				");
			sbQuery.append("	UNION  																														");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso ,																	 						");  
			sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK , 																									");
			sbQuery.append("	s.security_class, 																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK,																										");
			sbQuery.append("	s.ID_ISSUER_FK, 																											");
			sbQuery.append("	s.currency, 																												");
			sbQuery.append("	pc.EXPIRATION_DATE, 																										");
			sbQuery.append("	(1) AS quantity, 																											");
			sbQuery.append("	NULL CERTIFICATE_FROM, NULL CERTIFICATE_TO,																					");
			sbQuery.append("	aac.COUPON_NUMBER, aao.IND_ELECTRONIC_CUPON,  																				");
			sbQuery.append("	aao.ID_ANNOTATION_OPERATION_PK, 																							");
			sbQuery.append("	hab3.ID_PARTICIPANT_PK, 																									");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= aao.ID_ANNOTATION_OPERATION_PK) AS motive,																				");
			sbQuery.append("	cb.CUT_DATE  																												");
			sbQuery.append("	FROM CONTROL_BOVEDA_HISTORY cb  																							");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK				");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_CUPON aac ON aac.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK  				");
			sbQuery.append("	INNER JOIN INTEREST_PAYMENT_SCHEDULE ips ON ips.ID_SECURITY_CODE_FK = cb.ID_SECURITY_CODE_FK  								");
			sbQuery.append("	INNER JOIN PROGRAM_INTEREST_COUPON pic ON ips.ID_INT_PAYMENT_SCHEDULE_PK = pic.ID_INT_PAYMENT_SCHEDULE_FK  					");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE_HISTORY hab3 ON cb.ID_SECURITY_CODE_FK = hab3.ID_SECURITY_CODE_PK							");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON hab3.ID_HOLDER_ACCOUNT_PK = ha2.ID_HOLDER_ACCOUNT_PK  										");
			sbQuery.append("	INNER JOIN SECURITY s ON aao.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK  													");
			sbQuery.append("	WHERE 																														");
			sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL 								  														");
			sbQuery.append("	AND aao.IND_ELECTRONIC_CUPON = 1 AND aao.IND_SERIALIZABLE = 0 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null  				");
			sbQuery.append("	AND hab3.TOTAL_BALANCE > 0																									");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 124																									");
			sbQuery.append("	AND pic.STATE_PROGRAM_INTEREST = 1350																						");
			sbQuery.append("	AND TRUNC(hab3.CUT_DATE) = 	:finalRegisterDate																				");
			sbQuery.append("	UNION																														");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso, 																							");
			sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK ,																									");
			sbQuery.append("	s.security_class,																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK,																										");
			sbQuery.append("	s.ID_ISSUER_FK,																												");
			sbQuery.append("	s.currency,																													");
			sbQuery.append("	pc.EXPIRATION_DATE,																											");
			sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
			sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, 																					");
			sbQuery.append("	NULL AS numero_cupon, 0 AS cupon_electronico, 																				");
			sbQuery.append("	aao.ID_ANNOTATION_OPERATION_PK,																								");
			sbQuery.append("	ha2.ID_PARTICIPANT_FK, 																										");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= aao.ID_ANNOTATION_OPERATION_PK) AS motive,																				");
			sbQuery.append("	cb.CUT_DATE  																												");
			sbQuery.append("	FROM CONTROL_BOVEDA_HISTORY cb  																							");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK				");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK										");
			sbQuery.append("	INNER JOIN SECURITY s ON aao.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK														");
			sbQuery.append("	WHERE																														");
			sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT NULL											");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 399																									");
			sbQuery.append("	) subt 																														");
			sbQuery.append("	WHERE 1 = 1 																												");
			
			if(Validations.validateIsNotNull(to.getIdHolderAccountPk()) ) {
				sbQuery.append("  AND subt.ID_HOLDER_ACCOUNT_PK = :getIdHolderAccountPk ");
			}
			if(Validations.validateIsNotNull(to.getIdParticipantPk())) {
				sbQuery.append("  AND subt.ID_PARTICIPANT_PK = :getIdParticipantPk");
			}
			if(Validations.validateIsNotNull(to.getIdIssuerPk())) {
				sbQuery.append("  AND subt.ID_ISSUER_FK = :getIdIssuerPk");
			}
			if(Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk())) {
				sbQuery.append("  AND subt.ID_SECURITY_CODE_PK = :getIdSecurityCodePk");
			}
			if(Validations.validateIsNotNull(to.getFinalDate())) {
				sbQuery.append("  AND TRUNC(subt.BOVEDA_CUT_DATE) = :finalRegisterDate");
				sbQuery.append("  AND TRUNC(subt.fecha_ingreso) <= :finalRegisterDate");
			}
			if(Validations.validateIsNotNull(to.getMotive())) {
				sbQuery.append("  AND subt.motive = :motive");
			}
			if(Validations.validateIsNotNull(to.getSecurityCurrency())) {
				sbQuery.append("  AND subt.security_currency = :securityCurrency");
			}
			if(Validations.validateIsNotNull(to.getSecurityClass())) {
				sbQuery.append("  AND subt.security_class = :securityClass");
			}
			
			sbQuery.append("	ORDER BY subt.ID_ANNOTATION_OPERATION_PK DESC, subt.numero_cupon ASC NULLS FIRST  ");
			
		}else {
			sbQuery.append("	SELECT  "); 
			sbQuery.append("    subt.fecha_ingreso, ");
			
			sbQuery.append("    (SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK  												");
			sbQuery.append("    	= (SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK)) AS motivo_ingreso,  	");
			
			sbQuery.append("	(SELECT LISTAGG(had.ID_HOLDER_FK, '-') WITHIN GROUP (ORDER BY had.ID_HOLDER_FK) 											");
			sbQuery.append("		FROM HOLDER_ACCOUNT_DETAIL had WHERE had.ID_HOLDER_ACCOUNT_FK =subt.ID_HOLDER_ACCOUNT_PK) AS RUT, 							");
			
			sbQuery.append("	(SELECT ha.ACCOUNT_NUMBER FROM HOLDER_ACCOUNT ha WHERE ha.ID_HOLDER_ACCOUNT_PK = subt.ID_HOLDER_ACCOUNT_PK) AS CUENTA, 		");
			sbQuery.append("	FN_GET_HOLDER_DESC(subt.ID_HOLDER_ACCOUNT_PK, '-',0) AS NOBRE_CLIENTE, 														");
			sbQuery.append("	(SELECT pt.PARAMETER_NAME FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK =subt.security_class) AS tipo_valor, 			");
			sbQuery.append("	subt.ID_SECURITY_CODE_PK AS codigo_valor, 																					"); 
			
			sbQuery.append("	((SELECT aao2.SERIAL_NUMBER FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK) ");
			sbQuery.append("		||(SELECT aao2.CERTIFICATE_NUMBER FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK)||	");
			sbQuery.append("		CASE WHEN subt.numero_cupon IS NOT NULL THEN '-'||subt.numero_cupon END) AS serie, 											");
			
			sbQuery.append("	(CASE WHEN subt.cupon_electronico = 1 THEN 'SI' WHEN (subt.cupon_electronico = 0 AND subt.numero_cupon IS NULL) THEN NULL 	"); 
			sbQuery.append("		ELSE 'NO' END) AS cupon_electronico, 																						");
			
			sbQuery.append("	(SELECT i.MNEMONIC FROM ISSUER i WHERE i.ID_ISSUER_PK=subt.ID_ISSUER_FK)  AS emisor, 										");
			sbQuery.append("	(SELECT pt.DESCRIPTION FROM PARAMETER_TABLE pt WHERE pt.PARAMETER_TABLE_PK=subt.security_currency) AS moneda, 				");
			
			sbQuery.append("	(CASE WHEN (SELECT aac.CUPON_AMOUNT FROM ACCOUNT_ANNOTATION_CUPON aac WHERE aac.ID_ANNOTATION_OPERATION_FK = subt.ID_ANNOTATION_OPERATION_PK 	");
			sbQuery.append("		AND subt.numero_cupon = aac.COUPON_NUMBER) IS NOT NULL 																	");
			sbQuery.append("		THEN (SELECT aac.CUPON_AMOUNT FROM ACCOUNT_ANNOTATION_CUPON aac WHERE aac.ID_ANNOTATION_OPERATION_FK = subt.ID_ANNOTATION_OPERATION_PK 	");
			sbQuery.append("		AND subt.numero_cupon = aac.COUPON_NUMBER) 																				");
			sbQuery.append("		ELSE (SELECT aao2.SECURITY_NOMINAL_VALUE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK = subt.ID_ANNOTATION_OPERATION_PK) END) AS valor_nominal,	");
			
			sbQuery.append("	subt.EXPIRATION_DATE AS fecha_vencimiento, 																					");
			sbQuery.append("    CASE WHEN subt.quantity IS NULL THEN 1 ELSE subt.quantity END AS CANTIDAD, 													");
			sbQuery.append("	subt.CERTIFICATE_FROM AS desde , subt.CERTIFICATE_TO AS hasta, 																");
			sbQuery.append("	(SELECT aao3.REFERENCE_PART_CODE FROM ACCOUNT_ANNOTATION_OPERATION aao3 WHERE aao3.ID_ANNOTATION_OPERATION_PK=subt.ID_ANNOTATION_OPERATION_PK) AS observacion,	");
			sbQuery.append("	subt.cupon_electronico as electronico, 																						");
			sbQuery.append("	subt.numero_cupon as cuponNumber,																							");
			sbQuery.append("	subt.ID_ANNOTATION_OPERATION_PK as idAnnotationOperation																	");
			sbQuery.append("	FROM 																														");
			sbQuery.append("	(																															");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso ,																							");  
			sbQuery.append("	hab.ID_HOLDER_ACCOUNT_PK,																									");
			sbQuery.append("	s.security_class,																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK, 																										");
			sbQuery.append("	s.ID_ISSUER_FK, 																											");
			sbQuery.append("	s.currency AS security_currency, 																							");
			sbQuery.append("	pc.EXPIRATION_DATE,																											");
			sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
			sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, 																					");
			sbQuery.append("	NULL AS numero_cupon, 0 AS cupon_electronico, 																				");
			sbQuery.append("	aao.ID_ANNOTATION_OPERATION_PK, 																							");
			sbQuery.append("	hab.ID_PARTICIPANT_PK , 																									");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= aao.ID_ANNOTATION_OPERATION_PK) AS motive																					");
			sbQuery.append("	FROM CONTROL_BOVEDA cb  																									");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK				");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE hab ON cb.ID_SECURITY_CODE_FK = hab.ID_SECURITY_CODE_PK									");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON hab.ID_HOLDER_ACCOUNT_PK = ha2.ID_HOLDER_ACCOUNT_PK 										");
			sbQuery.append("	INNER JOIN SECURITY s ON aao.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK 													");
			sbQuery.append("	WHERE 																														");
			sbQuery.append("	pc.ID_SECURITY_CODE_FK IS NOT NULL AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null 											");
			sbQuery.append("	AND hab.TOTAL_BALANCE > 0																									");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 124																									");
			sbQuery.append("	UNION 																														");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso , 																						"); 
			sbQuery.append("	hab2.ID_HOLDER_ACCOUNT_PK, 																									");
			sbQuery.append("	s.security_class, 																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK,  																									");
			sbQuery.append("	s.ID_ISSUER_FK, 																											");
			sbQuery.append("	s.currency, 																												");
			sbQuery.append("	pc.EXPIRATION_DATE,																											");
			sbQuery.append("	(1) AS quantity, 																											");
			sbQuery.append("	NULL CERTIFICATE_FROM, NULL CERTIFICATE_TO,																					");
			sbQuery.append("	cb.COUPON_NUMBER, (SELECT aao2.IND_ELECTRONIC_CUPON FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK) AS IND_ELECTRONIC_CUPON,	");
			sbQuery.append("	(SELECT aao.ID_ANNOTATION_OPERATION_PK FROM ACCOUNT_ANNOTATION_OPERATION aao WHERE ID_PHYSICAL_CERTIFICATE_FK 				");
			sbQuery.append("	= (SELECT cb2.ID_PHYSICAL_CERTIFICATE_FK FROM CONTROL_BOVEDA cb2 															");
			sbQuery.append("	WHERE cb2.ID_SECURITY_CODE_FK = cb.ID_SECURITY_CODE_FK AND cb2.COUPON_NUMBER IS NULL)) AS ID_ANNOTATION_OPERATION_PK,		");
			sbQuery.append("	hab2.ID_PARTICIPANT_PK, 																									");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= (SELECT aao.ID_ANNOTATION_OPERATION_PK FROM ACCOUNT_ANNOTATION_OPERATION aao WHERE ID_PHYSICAL_CERTIFICATE_FK				");
			sbQuery.append("	= (SELECT cb2.ID_PHYSICAL_CERTIFICATE_FK FROM CONTROL_BOVEDA cb2 															");
			sbQuery.append("	WHERE cb2.ID_SECURITY_CODE_FK = cb.ID_SECURITY_CODE_FK AND cb2.COUPON_NUMBER IS NULL))) AS motive							");
			sbQuery.append("	FROM CONTROL_BOVEDA cb  																									");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE hab2 ON cb.ID_SECURITY_CODE_FK = hab2.ID_SECURITY_CODE_PK									");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON hab2.ID_HOLDER_ACCOUNT_PK = ha2.ID_HOLDER_ACCOUNT_PK 										");
			sbQuery.append("	INNER JOIN SECURITY s ON cb.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK 														");
			sbQuery.append("	WHERE  																														");
			sbQuery.append("	cb.ID_SECURITY_CODE_FK IS NOT NULL 								  															");
			sbQuery.append("	AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null 																				");
			sbQuery.append("	AND cb.COUPON_NUMBER IS NOT NULL 																							");
			sbQuery.append("	AND hab2.TOTAL_BALANCE > 0																									");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 124																									");
			sbQuery.append("	UNION  																														");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso ,																	 						");  
			sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK , 																									");
			sbQuery.append("	s.security_class, 																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK,																										");
			sbQuery.append("	s.ID_ISSUER_FK, 																											");
			sbQuery.append("	s.currency, 																												");
			sbQuery.append("	pc.EXPIRATION_DATE, 																										");
			sbQuery.append("	(1) AS quantity, 																											");
			sbQuery.append("	NULL CERTIFICATE_FROM, NULL CERTIFICATE_TO,																					");
			sbQuery.append("	aac.COUPON_NUMBER, aao.IND_ELECTRONIC_CUPON,  																				");
			sbQuery.append("	aao.ID_ANNOTATION_OPERATION_PK, 																							");
			sbQuery.append("	hab3.ID_PARTICIPANT_PK, 																									");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= aao.ID_ANNOTATION_OPERATION_PK) AS motive																					");
			sbQuery.append("	FROM CONTROL_BOVEDA cb  																									");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK				");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_CUPON aac ON aac.ID_ANNOTATION_OPERATION_FK = aao.ID_ANNOTATION_OPERATION_PK  				");
			sbQuery.append("	INNER JOIN INTEREST_PAYMENT_SCHEDULE ips ON ips.ID_SECURITY_CODE_FK = cb.ID_SECURITY_CODE_FK  								");
			sbQuery.append("	INNER JOIN PROGRAM_INTEREST_COUPON pic ON ips.ID_INT_PAYMENT_SCHEDULE_PK = pic.ID_INT_PAYMENT_SCHEDULE_FK  					");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT_BALANCE hab3 ON cb.ID_SECURITY_CODE_FK = hab3.ID_SECURITY_CODE_PK									");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON hab3.ID_HOLDER_ACCOUNT_PK = ha2.ID_HOLDER_ACCOUNT_PK  										");
			sbQuery.append("	INNER JOIN SECURITY s ON aao.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK  													");
			sbQuery.append("	WHERE 																														");
			sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL 								  														");
			sbQuery.append("	AND aao.IND_ELECTRONIC_CUPON = 1 AND aao.IND_SERIALIZABLE = 0 AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT null  				");
			sbQuery.append("	AND hab3.TOTAL_BALANCE > 0																									");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 124																									");
			sbQuery.append("	AND pic.STATE_PROGRAM_INTEREST = 1350																						");
			sbQuery.append("	UNION																														");
			sbQuery.append("	SELECT																														");
			sbQuery.append("	cb.REGISTRY_DATE AS fecha_ingreso, 																							");
			sbQuery.append("	aao.ID_HOLDER_ACCOUNT_FK ,																									");
			sbQuery.append("	s.security_class,																											");
			sbQuery.append("	s.ID_SECURITY_CODE_PK,																										");
			sbQuery.append("	s.ID_ISSUER_FK,																												");
			sbQuery.append("	s.currency,																													");
			sbQuery.append("	pc.EXPIRATION_DATE,																											");
			sbQuery.append("	(aao.CERTIFICATE_TO - aao.CERTIFICATE_FROM + 1) AS quantity, 																");
			sbQuery.append("	aao.CERTIFICATE_FROM, aao.CERTIFICATE_TO, 																					");
			sbQuery.append("	NULL AS numero_cupon, 0 AS cupon_electronico, 																				");
			sbQuery.append("	aao.ID_ANNOTATION_OPERATION_PK,																								");
			sbQuery.append("	ha2.ID_PARTICIPANT_FK, 																										");
			sbQuery.append("	(SELECT aao2.MOTIVE FROM ACCOUNT_ANNOTATION_OPERATION aao2 WHERE aao2.ID_ANNOTATION_OPERATION_PK							");
			sbQuery.append("	= aao.ID_ANNOTATION_OPERATION_PK) AS motive																					");
			sbQuery.append("	FROM CONTROL_BOVEDA cb  																									");
			sbQuery.append("	INNER JOIN PHYSICAL_CERTIFICATE pc ON pc.ID_PHYSICAL_CERTIFICATE_PK = cb.ID_PHYSICAL_CERTIFICATE_FK 						");
			sbQuery.append("	INNER JOIN ACCOUNT_ANNOTATION_OPERATION aao ON aao.ID_PHYSICAL_CERTIFICATE_FK = pc.ID_PHYSICAL_CERTIFICATE_PK				");
			sbQuery.append("	INNER JOIN HOLDER_ACCOUNT ha2 ON aao.ID_HOLDER_ACCOUNT_FK = ha2.ID_HOLDER_ACCOUNT_PK										");
			sbQuery.append("	INNER JOIN SECURITY s ON aao.ID_SECURITY_CODE_FK= s.ID_SECURITY_CODE_PK														");
			sbQuery.append("	WHERE																														");
			sbQuery.append("	aao.ID_SECURITY_CODE_FK IS NOT NULL AND cb.ID_PHYSICAL_CERTIFICATE_FK IS NOT NULL											");
			sbQuery.append("	AND s.INSTRUMENT_TYPE = 399																									");
			sbQuery.append("	) subt 																														");
			sbQuery.append("	WHERE 1 = 1 																												");
		
		if(Validations.validateIsNotNull(to.getIdHolderAccountPk()) ) {
			sbQuery.append("  AND subt.ID_HOLDER_ACCOUNT_PK = :getIdHolderAccountPk ");
		}
		if(Validations.validateIsNotNull(to.getIdParticipantPk())) {
			sbQuery.append("  AND subt.ID_PARTICIPANT_PK = :getIdParticipantPk");
		}
		if(Validations.validateIsNotNull(to.getIdIssuerPk())) {
			sbQuery.append("  AND subt.ID_ISSUER_FK = :getIdIssuerPk");
		}
		if(Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk())) {
			sbQuery.append("  AND subt.ID_SECURITY_CODE_PK = :getIdSecurityCodePk");
		}
		if(Validations.validateIsNotNull(to.getFinalDate())) {
			sbQuery.append("  AND TRUNC(subt.fecha_ingreso) <= :finalRegisterDate");
		}
		if(Validations.validateIsNotNull(to.getMotive())) {
			sbQuery.append("  AND subt.motive = :motive");
		}
		if(Validations.validateIsNotNull(to.getSecurityCurrency())) {
			sbQuery.append("  AND subt.security_currency = :securityCurrency");
		}
		if(Validations.validateIsNotNull(to.getSecurityClass())) {
			sbQuery.append("  AND subt.security_class = :securityClass");
		}
		
		sbQuery.append("	ORDER BY subt.ID_ANNOTATION_OPERATION_PK DESC, subt.numero_cupon ASC NULLS FIRST  ");
		
		}

		 		
		Query query = em.createNativeQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(to.getIdHolderAccountPk()) ) {
			query.setParameter("getIdHolderAccountPk", to.getIdHolderAccountPk());
		}
		if(Validations.validateIsNotNull(to.getIdParticipantPk()) ) {
			query.setParameter("getIdParticipantPk", to.getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(to.getIdIssuerPk()) ) {
			query.setParameter("getIdIssuerPk", to.getIdIssuerPk());
		}
		if(Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()) ) {
			query.setParameter("getIdSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNull(to.getFinalDate())) {
			query.setParameter("finalRegisterDate", to.getFinalDate());
		}
		if(Validations.validateIsNotNull(to.getMotive())) {
			query.setParameter("motive", to.getMotive());
		}
		if(Validations.validateIsNotNull(to.getSecurityCurrency())) {
			query.setParameter("securityCurrency", to.getSecurityCurrency());
		}
		if(Validations.validateIsNotNull(to.getSecurityClass())) {
			query.setParameter("securityClass", to.getSecurityClass());
		}
		
		List<Object[]> lstResult = query.getResultList();
		List<PhysicalCertificateTO> listPhysicalCertificateTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PhysicalCertificateTO physicalCertificateTO = new PhysicalCertificateTO();
        	if(objResult[0]!=null) {
        		physicalCertificateTO.setEntryDate((Date)objResult[0]);
        	}
        	if(objResult[1]!=null) {
        		physicalCertificateTO.setMotiveDescription(Validations.validateIsNotNull(objResult[1]) ? objResult[1].toString() : "");
        	}
        	if(objResult[2]!=null) {
        		physicalCertificateTO.setRut(Validations.validateIsNotNull(objResult[2]) ? objResult[2].toString() : "");
        	}
        	if(objResult[3]!=null) {
        		physicalCertificateTO.setAccountNumber(new Integer(objResult[3].toString()));
        	}
        	if(objResult[4]!=null) {
        		physicalCertificateTO.setName(Validations.validateIsNotNull(objResult[4]) ? objResult[4].toString() : "");
        	}
        	if(objResult[5]!=null) {
        		physicalCertificateTO.setSecurityClassDescription(Validations.validateIsNotNull(objResult[5]) ? objResult[5].toString() : "");
        	}
        	if(objResult[6]!=null) {
        		physicalCertificateTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[6]) ? objResult[6].toString() : "");
        	}
        	if(objResult[7]!=null) {
        		physicalCertificateTO.setSerialCode(Validations.validateIsNotNull(objResult[7]) ? objResult[7].toString() : "");
        	}
        	if(objResult[8]!=null) {
        		physicalCertificateTO.setElectronic(Validations.validateIsNotNull(objResult[8]) ? objResult[8].toString() : "");
        	}
        	if(objResult[9]!=null) {
        		physicalCertificateTO.setIssuer(Validations.validateIsNotNull(objResult[9]) ? objResult[9].toString() : "");
        	}
        	if(objResult[10]!=null) {
        		physicalCertificateTO.setCurrencyDescription(Validations.validateIsNotNull(objResult[10]) ? objResult[10].toString() : "");
        	}
        	if(objResult[11]!=null) {
        		physicalCertificateTO.setSecurityNominalValue((BigDecimal)objResult[11]);
        	}
        	if(objResult[12]!=null) {
        		physicalCertificateTO.setExpirationDate((Date)objResult[12]);
        	}
        	if(objResult[13]!=null) {
        		physicalCertificateTO.setQuantity(new Integer(objResult[13].toString()));
        	}
        	if(objResult[14]!=null) {
        		physicalCertificateTO.setCertificateFrom(new Integer(objResult[14].toString()));
        	}
        	if(objResult[15]!=null) {
        		physicalCertificateTO.setCertificateTo(new Integer(objResult[15].toString()));
        	}
        	if(objResult[16]!=null) {
        		physicalCertificateTO.setComments(Validations.validateIsNotNull(objResult[16]) ? objResult[16].toString() : "");
        	}
        	if(objResult[17]!=null) {
        		physicalCertificateTO.setIndElectronicCupon(new Integer(objResult[17].toString()));
        	}
        	if(objResult[18]!=null) {
        		physicalCertificateTO.setCuponNumber(new Integer(objResult[18].toString()));
        	}
        	if(objResult[19]!=null) {
        		physicalCertificateTO.setIdAnnotationOperation(new Long(objResult[19].toString()));
        	}
        	
        	listPhysicalCertificateTO.add(physicalCertificateTO);
        }
		
		return listPhysicalCertificateTO;
	}
	
	public PayrollDetail searchPhysicalCerticateFile(PhysicalCertificateTO physicalCertificate) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd ");
		sbQuery.append("	FROM PayrollDetail pd ");
		sbQuery.append("	WHERE pd.accountAnnotationOperation.custodyOperation.idCustodyOperationPk = :idAnnotationOperation ");
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificate.getCuponNumber())) {
			sbQuery.append("	AND pd.cuponNumber = :cuponNumber ");
		}else {
			sbQuery.append("	AND pd.cuponNumber is null ");
		}
		
		TypedQuery<PayrollDetail> query = em.createQuery(sbQuery.toString(),PayrollDetail.class);
		query.setParameter("idAnnotationOperation", physicalCertificate.getIdAnnotationOperation());
		if(Validations.validateIsNotNullAndNotEmpty(physicalCertificate.getCuponNumber())) {
			query.setParameter("cuponNumber", physicalCertificate.getCuponNumber());
		}
		sbQuery.append(" ORDER BY pd.idPayrollDetailPk DESC ");
		
		try {
			List<PayrollDetail> lstPayrollDetail = query.getResultList();
			if(lstPayrollDetail!=null && lstPayrollDetail.size()>0) {
				return lstPayrollDetail.get(0);
			}
			return null;
		}catch (NoResultException e) {
			return null;
		}
	}
	
	
	public List<PayrollDetailTO> searchPayrollDetailAccountAnnotationLocation(SearchCertificateVaultTO to) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("	SELECT ");
		sbQuery.append("	  PA.mnemonic, ");//0
		sbQuery.append("	  (select PC.branchOffice from PhysicalCertificate PC where PC.idPhysicalCertificatePk = AAO.physicalCertificate.idPhysicalCertificatePk ) as branchOffice, ");//1
		sbQuery.append("	  (select PC.vaultLocation from PhysicalCertificate PC where PC.idPhysicalCertificatePk = AAO.physicalCertificate.idPhysicalCertificatePk ) as vaultLocation, ");//2
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//3
		sbQuery.append("  	  CU.idCustodyOperationPk, ");//4
		sbQuery.append("	  AAO.certificateNumber, ");//5
		sbQuery.append("	  CURRENT_DATE, ");//6
		sbQuery.append("	  AAO.security.idSecurityCodePk, ");//7
		sbQuery.append("	  PD.payrollState as situation, ");//8 --(select PC.situation from PhysicalCertificate PC where PC.idPhysicalCertificatePk = AAO.physicalCertificate.idPhysicalCertificatePk )
		sbQuery.append("	  HA.idHolderAccountPk, ");//9
		sbQuery.append("	  AAO.issuer.mnemonic, ");//10
		sbQuery.append("	  PD.serialCode, ");//11
		sbQuery.append("	  ( CASE "
							+ "	WHEN PD.indCupon = 0 THEN AAO.securityExpirationDate "
							+ " WHEN PD.indCupon = 1 THEN ( SELECT aac.expirationDate FROM AccountAnnotationCupon aac WHERE aac.cuponNumber = PD.cuponNumber AND aac.accountAnnotationOperation.idAnnotationOperationPk = AAO.idAnnotationOperationPk ) "
							+ " END ) as securityExpirationDate, ");//12
		sbQuery.append("	  ( CASE "
							+ "	WHEN PD.indCupon = 0 THEN AAO.securityNominalValue "
							+ " WHEN PD.indCupon = 1 THEN ( SELECT aac.cuponAmount FROM AccountAnnotationCupon aac WHERE aac.cuponNumber = PD.cuponNumber AND aac.accountAnnotationOperation.idAnnotationOperationPk = AAO.idAnnotationOperationPk ) "
							+ " END ) as securityNominalValue, ");//13
		//sbQuery.append("	  AAO.securityExpirationDate, ");//12
		//sbQuery.append("	  AAO.securityNominalValue, ");//13
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//14
		sbQuery.append("	  PC.idPhysicalCertificatePk, ");//15
		sbQuery.append("	  PD.cuponNumber, ");//16
		sbQuery.append("	  AAO.motive ");//17
		sbQuery.append("	FROM PayrollDetail PD ");
		sbQuery.append("	  INNER JOIN PD.payrollHeader PH ");
		sbQuery.append("	  INNER JOIN PD.accountAnnotationOperation AAO ");
		sbQuery.append("	  INNER JOIN AAO.custodyOperation CU ");
		sbQuery.append("	  INNER JOIN AAO.holderAccount HA ");
		sbQuery.append("	  INNER JOIN HA.participant PA ");
		sbQuery.append("	  LEFT JOIN AAO.physicalCertificate PC ");

		sbQuery.append("	WHERE PH.payrollType = :payrollType ");
		sbQuery.append("  AND PD.payrollState != :payrollState ");

		if(Validations.validateIsNotNull(to.getIdHolderAccountPk()) ) {
			sbQuery.append("  AND HA.idHolderAccountPk = :idHolderAccountPk ");
		}
		if(Validations.validateIsNotNull(to.getIdParticipantPk()) ) {
			sbQuery.append("  AND PA.idParticipantPk = :idParticipantPk ");
		}
		if(Validations.validateIsNotNull(to.getIdIssuerPk()) ) {
			sbQuery.append("  AND AAO.idIssuerPk = :idIssuerPk ");
		}
		
		
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			sbQuery.append("  AND PH.branchOffice = :branchOffice ");
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			sbQuery.append("  AND PA.idParticipantPk = :idParticpantPk ");
		if( Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("  AND AAO.security.idSecurityCodePk = :idSecurityCodePk");

		if(Validations.validateIsNotNull(to.getInitialDate()))
			sbQuery.append("  AND PH.registerDate >= :initialRegisterDate");
		if(Validations.validateIsNotNull(to.getFinalDate()))
			sbQuery.append("  AND PH.registerDate <= :finalRegisterDate");
		
		sbQuery.append("	ORDER BY CU.idCustodyOperationPk DESC, PD.idPayrollDetailPk ASC  ");
		
		 		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", to.getPayRollType());
		query.setParameter("payrollState", 2702);//no son anulados
		

		if(Validations.validateIsNotNull(to.getIdHolderAccountPk()) ) {
			query.setParameter("idHolderAccountPk", to.getIdHolderAccountPk());
		}
		if(Validations.validateIsNotNull(to.getIdParticipantPk()) ) {
			query.setParameter("idParticipantPk", to.getIdParticipantPk());
		}
		if(Validations.validateIsNotNull(to.getIdIssuerPk()) ) {
			query.setParameter("idIssuerPk", to.getIdIssuerPk());
		}
		
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			query.setParameter("branchOffice", to.getBranchOffice());
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			query.setParameter("idParticpantPk", to.getIdParticipantPk());
		if( Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNull(to.getSituation()))
			query.setParameter("situation", to.getSituation());
		if(Validations.validateIsNotNull(to.getInitialDate()))
			query.setParameter("initialRegisterDate", to.getInitialDate());
		if(Validations.validateIsNotNull(to.getFinalDate()))
			query.setParameter("finalRegisterDate", to.getFinalDate());
		
		List<Object[]> lstResult = query.getResultList();
		List<PayrollDetailTO> listPayrollDetailTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
        	payrollDetailTO.setMnemonicParticipant(objResult[0].toString());
        	payrollDetailTO.setBranchOffice(Validations.validateIsNotNull(objResult[1]) ? new Integer(objResult[1].toString()) : null);
        	payrollDetailTO.setVaultLocation(Validations.validateIsNotNull(objResult[2]) ? new Integer(objResult[2].toString()) : null);
        	payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[3].toString()));
        	payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[4].toString()));
        	payrollDetailTO.setCertificateNumber(objResult[5].toString());
        	payrollDetailTO.setEntryDate(CommonsUtilities.currentDate());//(Date) objResult[6]
        	
        	payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[7]) ? objResult[7].toString() : null);
        	payrollDetailTO.setSituation(Validations.validateIsNotNull(objResult[8]) ? new Integer(objResult[8].toString()) : null);
        	
        	HolderAccountTO temp = new HolderAccountTO();
			temp.setIdHolderAccountPk(new Long(objResult[9].toString()));
			payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
			payrollDetailTO.setMnemonicIssuer(Validations.validateIsNotNull(objResult[10]) ? objResult[10].toString() : "");
			payrollDetailTO.setSerialCode(Validations.validateIsNotNull(objResult[11]) ? objResult[11].toString() : "");
			payrollDetailTO.setExpirationDate((Date) objResult[12]);
			payrollDetailTO.setSecurityNominalValue((BigDecimal) objResult[13]);
			payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[14].toString()));
			if(objResult[15]!=null) {
				payrollDetailTO.setIdPhysicalCertificatePk(new Long(objResult[15].toString()));
			}
        	if(objResult[16]!=null) {
        		payrollDetailTO.setCuponNumber(new Integer(objResult[16].toString()));
        	}
        	if(objResult[17]!=null) {
        		payrollDetailTO.setMotive(new Integer(objResult[17].toString()));
        	}
			
			if(payrollDetailTO.getIdSecurityCodePk()!=null) {
				VaultControlComponent vaultControl = null;
				if(payrollDetailTO.getExpirationDate() != null) {
					vaultControl = getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getCuponNumber());
				}else {
					vaultControl = getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getIdPhysicalCertificatePk());
				}
				String ruta= "";
				if(vaultControl!=null) {
					ruta = vaultControl.getArmario()+" - "+vaultControl.getNivel()+" - "+vaultControl.getCaja()+" - "+vaultControl.getFolio()+" - "+vaultControl.getIndiceCaja();
				}
				payrollDetailTO.setBovedaPath(ruta);
			}
			payrollDetailTO.setPayRollType(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode());
			
        	listPayrollDetailTO.add(payrollDetailTO);
        }
		
		return listPayrollDetailTO;
	}
	
	public List<PayrollDetailTO> searchPayrollDetailAccountAnnotation(SearchCertificateVaultTO to) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("	SELECT ");
		sbQuery.append("	  PA.mnemonic, ");//0
		sbQuery.append("	  PC.branchOffice, ");//1
		sbQuery.append("	  PC.vaultLocation, ");//2
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//3
		sbQuery.append("  	  CU.idCustodyOperationPk, ");//4
		sbQuery.append("	  PC.certificateNumber, ");//5
		sbQuery.append("	  PC.registerDate, ");//6
		sbQuery.append("	  PC.securities.idSecurityCodePk, ");//7
		sbQuery.append("	  PC.situation, ");//8
		sbQuery.append("	  HA.idHolderAccountPk, ");//9
		sbQuery.append("	  PC.securities.issuer.mnemonic, ");//10
		sbQuery.append("	  PD.serialCode, ");//11
		
		//sbQuery.append("	  PC.expirationDate, ");//12
		sbQuery.append("	(CASE WHEN PD.indCupon = 1 THEN ");
		sbQuery.append("		( SELECT aac.expirationDate FROM AccountAnnotationCupon aac WHERE aac.accountAnnotationOperation.idAnnotationOperationPk = PD.accountAnnotationOperation.custodyOperation.idCustodyOperationPk and aac.cuponNumber = PD.cuponNumber ) ");//15
		sbQuery.append("	ELSE ");
		sbQuery.append("		PC.expirationDate ");
		sbQuery.append("	END ) as expirationDate, ");//12
		
		
		sbQuery.append("	(CASE WHEN PD.indCupon = 1 THEN ");
		sbQuery.append("		( SELECT aac.cuponAmount FROM AccountAnnotationCupon aac WHERE aac.accountAnnotationOperation.idAnnotationOperationPk = PD.accountAnnotationOperation.custodyOperation.idCustodyOperationPk and aac.cuponNumber = PD.cuponNumber ) ");//15
		sbQuery.append("	ELSE ");
		sbQuery.append("		PD.accountAnnotationOperation.securityNominalValue ");
		sbQuery.append("	END ) as securityNominalValue, ");//13
		sbQuery.append("	  PC.idPhysicalCertificatePk, ");//14
		sbQuery.append("	  PD.cuponNumber ");//15
		sbQuery.append("	FROM PayrollDetail PD ");
		sbQuery.append("	  INNER JOIN PD.payrollHeader PH ");
		sbQuery.append("	  INNER JOIN PD.accountAnnotationOperation AAO ");
		sbQuery.append("	  INNER JOIN AAO.physicalCertificate PC ");
		sbQuery.append("	  INNER JOIN AAO.custodyOperation CU ");
		sbQuery.append("	  INNER JOIN AAO.holderAccount HA ");
		sbQuery.append("	  INNER JOIN HA.participant PA ");

		sbQuery.append("	WHERE PH.payrollType = :payrollType ");
		sbQuery.append("  AND PD.payrollState not in (:payrollState) ");
		
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			sbQuery.append("  AND PH.branchOffice = :branchOffice ");
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			sbQuery.append("  AND PA.idParticipantPk = :idParticpantPk ");
		if( Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("  AND PC.securities.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateIsNotNull(to.getSituation()))
			sbQuery.append("  AND PC.situation = :situation");
		if(Validations.validateIsNotNull(to.getInitialDate()))
			sbQuery.append("  AND PC.registerDate >= :initialRegisterDate");
		if(Validations.validateIsNotNull(to.getFinalDate()))
			sbQuery.append("  AND PC.registerDate <= :finalRegisterDate");
		
		sbQuery.append("	ORDER BY CU.idCustodyOperationPk DESC, pd.idPayrollDetailPk ASC  ");
		
		 		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", to.getPayRollType());
		
		List<Integer> notStateIn = new ArrayList<Integer>();
		notStateIn.add(PayrollDetailStateType.ANNULATE.getCode());
		notStateIn.add(PayrollDetailStateType.REJECT.getCode());
		query.setParameter("payrollState", notStateIn);//2702
		
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			query.setParameter("branchOffice", to.getBranchOffice());
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			query.setParameter("idParticpantPk", to.getIdParticipantPk());
		if( Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNull(to.getSituation()))
			query.setParameter("situation", to.getSituation());
		if(Validations.validateIsNotNull(to.getInitialDate()))
			query.setParameter("initialRegisterDate", to.getInitialDate());
		if(Validations.validateIsNotNull(to.getFinalDate()))
			query.setParameter("finalRegisterDate", to.getFinalDate());
		
		List<Object[]> lstResult = query.getResultList();
		List<PayrollDetailTO> listPayrollDetailTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
        	payrollDetailTO.setMnemonicParticipant(objResult[0].toString());
        	payrollDetailTO.setBranchOffice(Validations.validateIsNotNull(objResult[1]) ? new Integer(objResult[1].toString()) : null);
        	payrollDetailTO.setVaultLocation(Validations.validateIsNotNull(objResult[2]) ? new Integer(objResult[2].toString()) : null);
        	payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[3].toString()));
        	payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[4].toString()));
        	payrollDetailTO.setCertificateNumber(objResult[5].toString());
        	payrollDetailTO.setEntryDate((Date) objResult[6]);
        	payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[7]) ? objResult[7].toString() : null);
        	payrollDetailTO.setSituation(new Integer(objResult[8].toString()));
        	HolderAccountTO temp = new HolderAccountTO();
			temp.setIdHolderAccountPk(new Long(objResult[9].toString()));
			payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));
			payrollDetailTO.setMnemonicIssuer(Validations.validateIsNotNull(objResult[10]) ? objResult[10].toString() : "");
			payrollDetailTO.setSerialCode(Validations.validateIsNotNull(objResult[11]) ? objResult[11].toString() : "");
			payrollDetailTO.setExpirationDate((Date) objResult[12]);
			payrollDetailTO.setSecurityNominalValue((BigDecimal) objResult[13]);
        	payrollDetailTO.setIdPhysicalCertificatePk(new Long(objResult[14].toString()));
        	if(objResult[15]!=null) {
        		payrollDetailTO.setCuponNumber(new Integer(objResult[15].toString()));
        	}
        	
        	
        	VaultControlComponent vaultControl = null;
        	if(payrollDetailTO.getCuponNumber()==null) {
        		vaultControl = getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getIdPhysicalCertificatePk());
        	}else {
        		vaultControl = getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getCuponNumber());
        	}
			String ruta= "";
			if(vaultControl!=null) {
				ruta = vaultControl.getArmario()+" - "+vaultControl.getNivel()+" - "+vaultControl.getCaja()+" - "+vaultControl.getFolio()+" - "+vaultControl.getIndiceCaja();
			}
			payrollDetailTO.setBovedaPath(ruta);
			
        	listPayrollDetailTO.add(payrollDetailTO);
        }
		
		return listPayrollDetailTO;
	}
	
	public List<VaultControlComponent> getVaultControlComponentWithSecurityCode(String idSecurityCodeFk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT v ");
		sbQuery.append("	FROM VaultControlComponent v ");
		sbQuery.append("	WHERE v.idSecurityCodeFk = :idSecurityCodeFk ");
		
		TypedQuery<VaultControlComponent> query = em.createQuery(sbQuery.toString(),VaultControlComponent.class);
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		
		try {
			return query.getResultList();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public VaultControlComponent getVaultControlComponentWithSecurityCodeAndCertificate(String idSecurityCodeFk, Long idPhysicalCertificateFk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT v ");
		sbQuery.append("	FROM VaultControlComponent v ");
		sbQuery.append("	WHERE v.idSecurityCodeFk = :idSecurityCodeFk ");
		sbQuery.append("	AND v.idPhysicalCertificateFk = :idPhysicalCertificateFk ");
		
		TypedQuery<VaultControlComponent> query = em.createQuery(sbQuery.toString(),VaultControlComponent.class);
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		query.setParameter("idPhysicalCertificateFk", idPhysicalCertificateFk);
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public VaultControlComponent getVaultControlComponentWithSecurityCode(String idSecurityCodeFk, Integer couponNumber) throws ServiceException{
		Integer indCupon = (couponNumber == null)?0:1;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT v ");
		sbQuery.append("	FROM VaultControlComponent v ");
		sbQuery.append("	WHERE v.idSecurityCodeFk = :idSecurityCodeFk ");
		sbQuery.append("	AND v.indCoupon = :indCoupon ");
		
		if(couponNumber!=null) {
			sbQuery.append("	AND v.couponNumber = :couponNumber ");
		}
		TypedQuery<VaultControlComponent> query = em.createQuery(sbQuery.toString(),VaultControlComponent.class);
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		query.setParameter("indCoupon", indCupon);
		if(couponNumber!=null) {
			query.setParameter("couponNumber", couponNumber);
		}
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public VaultControlComponent getVaultControlComponentWithSecurityCode(String idSecurityCodeFk, Long idPhysicalCertificatePk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT v ");
		sbQuery.append("	FROM VaultControlComponent v ");
		sbQuery.append("	WHERE v.idSecurityCodeFk = :idSecurityCodeFk AND v.idPhysicalCertificateFk = :idPhysicalCertificateFk ");
		
		TypedQuery<VaultControlComponent> query = em.createQuery(sbQuery.toString(),VaultControlComponent.class);
		query.setParameter("idSecurityCodeFk", idSecurityCodeFk);
		query.setParameter("idPhysicalCertificateFk", idPhysicalCertificatePk);
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public List<PayrollDetailTO> searchPayrollDetailRemoveSecurities(SearchCertificateVaultTO to) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append("	SELECT DISTINCT ");
		sbQuery.append("	  PA.mnemonic, ");//0
		sbQuery.append("	  PC.branchOffice, ");//1
		sbQuery.append("	  PC.vaultLocation, ");//2
		sbQuery.append("	  PH.idPayrollHeaderPk, ");//3
		sbQuery.append("  	  CU.idCustodyOperationPk, ");//4
		sbQuery.append("	  PC.certificateNumber, ");//5
		sbQuery.append("	  CURRENT_DATE, ");//6
		sbQuery.append("	  PC.securities.idSecurityCodePk, ");//7
		sbQuery.append("	  PC.situation, ");//8
		sbQuery.append("	  HA.idHolderAccountPk, ");//9
		sbQuery.append("	  PC.securities.issuer.mnemonic, ");//10
		sbQuery.append("	  PD.serialCode||nvl2(TO_CHAR(PC.cuponNumber), '-', '')||PC.cuponNumber ");//11
		

		sbQuery.append("	FROM PayrollDetail PD ");
		sbQuery.append("	  INNER JOIN PD.payrollHeader PH ");
		sbQuery.append("	  INNER JOIN PD.retirementOperation RO ");
		sbQuery.append("	  INNER JOIN RO.custodyOperation CU ");
		sbQuery.append("	  LEFT JOIN RO.retirementDetails RD ");
		sbQuery.append("	  INNER JOIN ro.holderAccount HA ");
		sbQuery.append("	  INNER JOIN HA.participant PA ");
		
		sbQuery.append("	  ,RetirementCertificate RC ");
		sbQuery.append("	    INNER JOIN RD.physicalCertificate PC ");
		
		sbQuery.append("	WHERE RC.retirementOperation.idRetirementOperationPk = RO.idRetirementOperationPk ");
		sbQuery.append("	  AND PH.payrollType = :payrollType ");
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			sbQuery.append("  AND PH.branchOffice = :branchOffice ");
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			sbQuery.append("  AND PA.idParticipantPk = :idParticpantPk ");
		if(Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			sbQuery.append("  AND PC.securities.idSecurityCodePk = :idSecurityCodePk");
		if(Validations.validateIsNotNull(to.getSituation()))
			sbQuery.append("  AND PC.situation = :situation");
		if(Validations.validateIsNotNull(to.getInitialDate()))
			sbQuery.append("  AND TRUNC(RC.lastModifyDate) >= :initialRegisterDate");
		if(Validations.validateIsNotNull(to.getFinalDate()))
			sbQuery.append("  AND TRUNC(RC.lastModifyDate) <= :finalRegisterDate");
		
		sbQuery.append("	ORDER BY CU.idCustodyOperationPk DESC");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("payrollType", to.getPayRollType());
		if(Validations.validateIsNotNull(to.getBranchOffice()))
			query.setParameter("branchOffice", to.getBranchOffice());
		if(Validations.validateIsNotNull(to.getIdParticipantPk()))
			query.setParameter("idParticpantPk", to.getIdParticipantPk());
		if(Validations.validateIsNotNullAndNotEmpty(to.getSecurity()) && Validations.validateIsNotNull(to.getSecurity().getIdSecurityCodePk()))
			query.setParameter("idSecurityCodePk", to.getSecurity().getIdSecurityCodePk());
		if(Validations.validateIsNotNull(to.getSituation()))
			query.setParameter("situation", to.getSituation());
		if(Validations.validateIsNotNull(to.getInitialDate()))
			query.setParameter("initialRegisterDate", to.getInitialDate());
		if(Validations.validateIsNotNull(to.getFinalDate()))
			query.setParameter("finalRegisterDate", to.getFinalDate());
		
		List<Object[]> lstResult = query.getResultList();
		List<PayrollDetailTO> listPayrollDetailTO = new ArrayList<>();
		
        for (Object[] objResult:lstResult) {
        	PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
        	payrollDetailTO.setMnemonicParticipant(objResult[0].toString());
        	payrollDetailTO.setBranchOffice(Validations.validateIsNotNull(objResult[1]) ? new Integer(objResult[1].toString()) : null);
        	payrollDetailTO.setVaultLocation(Validations.validateIsNotNull(objResult[2]) ? new Integer(objResult[2].toString()) : null);
        	payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[3].toString()));
        	payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[4].toString()));
        	payrollDetailTO.setCertificateNumber(objResult[5].toString());
        	payrollDetailTO.setEntryDate((Date) objResult[6]);
        	payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNull(objResult[7]) ? objResult[7].toString() : null);
        	payrollDetailTO.setSituation(new Integer(objResult[8].toString()));
        	HolderAccountTO temp = new HolderAccountTO();
			temp.setIdHolderAccountPk(new Long(objResult[9].toString()));
			payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));		
			payrollDetailTO.setMnemonicIssuer(Validations.validateIsNotNull(objResult[10]) ? objResult[10].toString() : "");
			payrollDetailTO.setSerialCode(Validations.validateIsNotNull(objResult[11]) ? objResult[11].toString() : "");
			payrollDetailTO.setPayRollType(PayrollType.REMOVE_SECURITIES_OPERATION.getCode());
        	listPayrollDetailTO.add(payrollDetailTO);
        }
		
		return listPayrollDetailTO;
	}

	public List<PayrollHeader> getPayrollHeaderAndDetailByPk(List<Long> lstIdPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph ");
		sbQuery.append("	FROM PayrollHeader ph JOIN FETCH ph.lstPayrollDetail ");
		sbQuery.append("	WHERE ph.idPayrollHeaderPk in (:idPayrollHeaderPk)  ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idPayrollHeaderPk", lstIdPayrollHeaderPk);
		return query.getResultList();
	}

	public PayrollHeader getSinglePayrollHeaderAndDetailByPk(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph ");
		sbQuery.append("	FROM PayrollHeader ph JOIN FETCH ph.lstPayrollDetail ");
		sbQuery.append("	WHERE ph.idPayrollHeaderPk = :idPayrollHeaderPk  ");
		
		TypedQuery<PayrollHeader> query = em.createQuery(sbQuery.toString(),PayrollHeader.class);
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}

	public PayrollHeader getSinglePayrollHeaderByPk(Long idPayrollHeaderPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ph ");
		sbQuery.append("	FROM PayrollHeader ph ");
		sbQuery.append("	WHERE ph.idPayrollHeaderPk = :idPayrollHeaderPk  ");
		
		TypedQuery<PayrollHeader> query = em.createQuery(sbQuery.toString(),PayrollHeader.class);
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public PayrollDetail getSinglePayrollDetailByRetirementDetailAndPayrollHeaderPk(Long idPayrollHeaderPk, Long idRetirementDetailPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT pd ");
		sbQuery.append("	FROM PayrollDetail pd ");
		sbQuery.append("	WHERE pd.payrollHeader.idPayrollHeaderPk = :idPayrollHeaderPk and pd.retirementDetail.idRetirementDetailPk =:idRetirementDetailPk  ");
		
		TypedQuery<PayrollDetail> query = em.createQuery(sbQuery.toString(),PayrollDetail.class);
		query.setParameter("idPayrollHeaderPk", idPayrollHeaderPk);
		query.setParameter("idRetirementDetailPk", idRetirementDetailPk);
		
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
	
	public PhysicalCertificate getSinglePhysicalCertificateByRetirementDetailPk(Long idRetirementDetailPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT rd ");
		sbQuery.append("	FROM RetirementDetail rd ");
		sbQuery.append("	JOIN FETCH rd.physicalCertificate pc ");
		sbQuery.append("	WHERE rd.idRetirementDetailPk =:idRetirementDetailPk  ");
		
		TypedQuery<RetirementDetail> query = em.createQuery(sbQuery.toString(),RetirementDetail.class);
		query.setParameter("idRetirementDetailPk", idRetirementDetailPk);
		
		try {
			return query.getSingleResult().getPhysicalCertificate();
		}catch (NoResultException e) {
			return null;
		}
	}

	public RetirementOperation getRetirementOperationAndDetail(Long idRetirementOperationPk) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT ro ");
		sbQuery.append("	FROM RetirementOperation ro ");
		sbQuery.append("	JOIN FETCH rO.retirementDetails rd ");
		sbQuery.append("	WHERE ro.idRetirementOperationPk = :idRetirementOperationPk  ");
		
		TypedQuery<RetirementOperation> query = em.createQuery(sbQuery.toString(),RetirementOperation.class);
		query.setParameter("idRetirementOperationPk", idRetirementOperationPk);
		
		try {
			return query.getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}
}
