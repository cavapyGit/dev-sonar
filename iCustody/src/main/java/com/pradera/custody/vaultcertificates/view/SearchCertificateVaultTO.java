package com.pradera.custody.vaultcertificates.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.physical.PhysicalCertificate;

public class SearchCertificateVaultTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer payRollType;
	private Integer branchOffice;
	private Integer payrollDetailState;
	private Long idParticipantPk;
	private Issuer issuer;
	private String idIssuerPk;
	private Holder holder;
	private Security security;
	private String idSecurityCodePk;
	private GenericDataModel<PayrollDetailTO> lstResult;
	private List<PhysicalCertificate> lstPhysicalCertificates;
	private boolean blNoResult;
	private Date initialDate;
	private Date finalDate;
	private PayrollDetailTO payrollDetailTOSelected;
	private Integer situation;
	private List<PayrollDetailTO> lstRelation;
	private Integer entryVault;
	private Long payRollNumber;
	private Integer motive;
	private Long idHolderAccountPk;
	private Integer securityCurrency;
	private Integer securityClass;
	
	public Integer getPayRollType() {
		return payRollType;
	}
	public void setPayRollType(Integer payRollType) {
		this.payRollType = payRollType;
	}
	public Integer getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(Integer branchOffice) {
		this.branchOffice = branchOffice;
	}
	public Integer getPayrollDetailState() {
		return payrollDetailState;
	}
	public void setPayrollDetailState(Integer payrollDetailState) {
		this.payrollDetailState = payrollDetailState;
	}
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	public String getIdIssuerPk() {
		return idIssuerPk;
	}
	public void setIdIssuerPk(String idIssuerPk) {
		this.idIssuerPk = idIssuerPk;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public GenericDataModel<PayrollDetailTO> getLstResult() {
		return lstResult;
	}
	public void setLstResult(GenericDataModel<PayrollDetailTO> lstResult) {
		this.lstResult = lstResult;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}	
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}
	public Security getSecurity() {
		return security;
	}
	public void setSecurity(Security security) {
		this.security = security;
	}
	public Holder getHolder() {
		return holder;
	}
	public void setHolder(Holder holder) {
		this.holder = holder;
	}
	public List<PhysicalCertificate> getLstPhysicalCertificates() {
		return lstPhysicalCertificates;
	}
	public void setLstPhysicalCertificates(
			List<PhysicalCertificate> lstPhysicalCertificates) {
		this.lstPhysicalCertificates = lstPhysicalCertificates;
	}
	public PayrollDetailTO getPayrollDetailTOSelected() {
		return payrollDetailTOSelected;
	}
	public void setPayrollDetailTOSelected(PayrollDetailTO payrollDetailTOSelected) {
		this.payrollDetailTOSelected = payrollDetailTOSelected;
	}
	public Integer getSituation() {
		return situation;
	}
	public void setSituation(Integer situation) {
		this.situation = situation;
	}
	public List<PayrollDetailTO> getLstRelation() {
		return lstRelation;
	}
	public void setLstRelation(List<PayrollDetailTO> lstRelation) {
		this.lstRelation = lstRelation;
	}
	public Integer getEntryVault() {
		return entryVault;
	}
	public void setEntryVault(Integer entryVault) {
		this.entryVault = entryVault;
	}
	public Long getPayRollNumber() {
		return payRollNumber;
	}
	public void setPayRollNumber(Long payRollNumber) {
		this.payRollNumber = payRollNumber;
	}
	public Integer getMotive() {
		return motive;
	}
	public void setMotive(Integer motive) {
		this.motive = motive;
	}
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}
	public Integer getSecurityCurrency() {
		return securityCurrency;
	}
	public void setSecurityCurrency(Integer securityCurrency) {
		this.securityCurrency = securityCurrency;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	
}
