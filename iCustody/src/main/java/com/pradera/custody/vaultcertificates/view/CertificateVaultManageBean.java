package com.pradera.custody.vaultcertificates.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.deltaspike.core.api.resourceloader.InjectableResource;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.util.Constants;

//import org.jboss.solder.exception.control.ExceptionToCatch;

import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.interceptor.LoggerCreateBean;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.type.ModuleWarType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.DepositaryWebBean;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.payroll.view.VaultSummary;
import com.pradera.custody.vaultcertificates.facade.CertificateVaultFacade;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.component.business.ComponentConstant;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.type.VaultType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.process.BusinessProcess;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

import net.sf.jasperreports.engine.export.oasis.CellStyle;

@DepositaryWebBean
@LoggerCreateBean
public class CertificateVaultManageBean extends GenericBaseBean {
	
	private static final long serialVersionUID = 1L;
	private Map<Integer, String> mpPayrollTypes, mpDetailStates, mpVaults, mpPayrollTypesSearch, mpBranchOfficesSearch, mpVaultsSearch, mpSituationSearch;
	private List<ParameterTable> lstDetailStates, lstPayrollTypes, lstBranchOffices, lstVaults,
		lstPayrollTypesSearch, lstBranchOfficesSearch, lstVaultsSearch, lstSituationSearch;
	private List<Participant> lstParticipants, lstParticipantsSearch;
	private List<Issuer> lstIssuer;
	private SearchCertificateVaultTO searchCertificateVaultTO, filterCertificateVaultTO;
	private static final String ENTRY_MAPPING = "entryCertificateVault";
	private static final String EXIT_MAPPING = "exitCertificateVault";
	private static final String SEARCH_MAPPING = "searchCertificateVault";	
	@Inject
	GeneralParametersFacade generalParametersFacade;
	@Inject
	AccountsFacade accountsFacade;
	@Inject
	CertificateVaultFacade certificateVaultFacade;

	@Inject
	private UserInfo userInfo;

	@EJB
	private BatchProcessServiceFacade batchProcessServiceFacade;

	private List<ParameterTable> lstMotives = new ArrayList<ParameterTable>();
	
	@PostConstruct
	public void init(){
		try {		
			cleanSearch();
			fillParametersSearch();
			fillParticipantsSearch();
			fillIssuerSearch();
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void fillIssuerSearch() {
		Issuer filterPar = new Issuer();
		filterPar.setStateIssuer(IssuerStateType.REGISTERED.getCode());
		try {
			lstIssuer = accountsFacade.getLisIssuerServiceBean(filterPar);
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void fillParticipantsSearch(){
		Participant filterPar = new Participant();
		filterPar.setState(ParticipantStateType.REGISTERED.getCode());
		try {
			lstParticipantsSearch = accountsFacade.getLisParticipantServiceBean(filterPar);
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public String entry(){
		try {
			initAction(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode(), null);//PayrollDetailStateType.ANALYZED.getCode()
			return ENTRY_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}
	
	public String exit(){
		try {
			initAction(PayrollType.REMOVE_SECURITIES_OPERATION.getCode(), PayrollDetailStateType.VAULT.getCode());
			return EXIT_MAPPING;
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
		return null;
	}
	
	public void vaultCavapy() {
		
		List<PayrollDetailTO> lstPayrollDetailTO = searchCertificateVaultTO.getLstResult().getDataList();
		if(searchCertificateVaultTO.getLstResult()!=null) {
			for(PayrollDetailTO current : lstPayrollDetailTO) {
				if( current.getIdVaultPk().equals(VaultType.VAULT_CAVAPY.getCode()) ) {
					current.setBlSelected(true);
				}
			}
			searchCertificateVaultTO.setLstResult(new GenericDataModel<>(lstPayrollDetailTO));
		}
		
	}
	
	public void retirementVaultCavapy() {
		
		List<PayrollDetailTO> lstPayrollDetailTO = searchCertificateVaultTO.getLstResult().getDataList();
		if(searchCertificateVaultTO.getLstResult()!=null) {
			for(PayrollDetailTO current : lstPayrollDetailTO) {
				//if( current.getIdVaultPk().equals(VaultType.VAULT_CAVAPY.getCode()) ) {
					current.setBlSelected(true);
				//}
			}
			searchCertificateVaultTO.setLstResult(new GenericDataModel<>(lstPayrollDetailTO));
		}
	}
	
	public void initAction(Integer payrollType, Integer payrollDetailState) throws ServiceException{
		fillParameters(payrollType);
		searchCertificateVaultTO = new SearchCertificateVaultTO();
		searchCertificateVaultTO.setIssuer(new Issuer());
		searchCertificateVaultTO.setPayrollDetailState(payrollDetailState);
		searchCertificateVaultTO.setBranchOffice(2735);
		searchCertificateVaultTO.setInitialDate(CommonsUtilities.currentDate());
		searchCertificateVaultTO.setFinalDate(CommonsUtilities.currentDate());
		changeBranchOffice();
		Participant filterPar = new Participant();
		filterPar.setState(ParticipantStateType.REGISTERED.getCode());
		lstParticipants = accountsFacade.getLisParticipantServiceBean(filterPar);
	}
	
	private void fillParametersSearch() throws ServiceException{
		mpPayrollTypesSearch = new HashMap<>();
		mpBranchOfficesSearch = new HashMap<>();
		mpVaultsSearch = new HashMap<>();
		mpSituationSearch = new HashMap<>();
		lstPayrollTypesSearch = new ArrayList<>();
		lstBranchOfficesSearch = new ArrayList<>();
		lstVaultsSearch = new ArrayList<>();
		lstSituationSearch = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		lstPayrollTypesSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstPayrollTypesSearch){
			mpPayrollTypesSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		lstBranchOfficesSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstBranchOfficesSearch){
			mpBranchOfficesSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		paramTabTO.setMasterTableFk(MasterTableType.VAULTS_FOR_BRANCH.getCode());
		lstVaultsSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstVaultsSearch){
			mpVaultsSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		paramTabTO.setMasterTableFk(MasterTableType.RETIREMENT_MOTIVE_PARAMETER.getCode());
		lstVaultsSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstVaultsSearch){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()) &&
					ComponentConstant.ONE.equals(paramTab.getIndicator4())){
				lstMotives.add(paramTab);
			}
			mpVaultsSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		List<Integer> listIdSituation = new ArrayList<>();
		listIdSituation.add(PayrollDetailStateType.VAULT.getCode());
		listIdSituation.add(PayrollDetailStateType.TRANSIT.getCode());
		listIdSituation.add(PayrollDetailStateType.DELIVERED.getCode());
		paramTabTO.setLstParameterTablePk(listIdSituation);
		paramTabTO.setMasterTableFk(null);
		lstSituationSearch = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstSituationSearch){
			mpSituationSearch.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
	}
	
	public String back(){
		return SEARCH_MAPPING;
	}
	
	public void searchCertificateVault(){
		try {
			filterCertificateVaultTO.setBlNoResult(true);
			filterCertificateVaultTO.setLstResult(null);
			List<PayrollDetailTO> lstResult = certificateVaultFacade.searchPayrollDetail(filterCertificateVaultTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				filterCertificateVaultTO.setBlNoResult(false);
				filterCertificateVaultTO.setLstResult(new GenericDataModel<>(lstResult));
				for(PayrollDetailTO payrollDetailTO:filterCertificateVaultTO.getLstResult()){
					payrollDetailTO.setBranchOfficeDesc(mpBranchOfficesSearch.get(payrollDetailTO.getBranchOffice()));
					payrollDetailTO.setVaultLocationDesc(mpVaultsSearch.get(payrollDetailTO.getVaultLocation()));
					payrollDetailTO.setSituationDesc(mpSituationSearch.get(payrollDetailTO.getSituation()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void relateCertificates(){
		boolean blSelected = false;
		BigDecimal totalCertificates = BigDecimal.ZERO;
		for(PhysicalCertificate physicalCert:searchCertificateVaultTO.getLstPhysicalCertificates())
			if(physicalCert.isBlSelected()){
				totalCertificates = totalCertificates.add(physicalCert.getCertificateQuantity());
				blSelected = true;				
			}
		if(blSelected){
			//
			
			//if(searchCertificateVaultTO.getPayrollDetailTOSelected().getQuantity().compareTo(totalCertificates) == 0){
				PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
				payrollDetailTO.setIdCustodyOperationPk(searchCertificateVaultTO.getPayrollDetailTOSelected().getIdCustodyOperationPk());
				payrollDetailTO.setIdPayrollDetailPk(searchCertificateVaultTO.getPayrollDetailTOSelected().getIdPayrollDetailPk());
				payrollDetailTO.setIdPayrollHeaderPk(searchCertificateVaultTO.getPayrollDetailTOSelected().getIdPayrollHeaderPk());
				payrollDetailTO.setQuantity(searchCertificateVaultTO.getPayrollDetailTOSelected().getQuantity());
				payrollDetailTO.setIdSecurityCodePk(searchCertificateVaultTO.getPayrollDetailTOSelected().getIdSecurityCodePk());				
				StringBuilder sbCertificates = new StringBuilder();
				StringBuilder sbCertificatesIds = new StringBuilder();
				
				for(PhysicalCertificate physicalCert:searchCertificateVaultTO.getLstPhysicalCertificates())
					if(physicalCert.isBlSelected()){
						sbCertificatesIds.append(physicalCert.getIdPhysicalCertificatePk().toString().concat(GeneralConstants.HYPHEN));
						sbCertificates.append(physicalCert.getCertificateFull());
						sbCertificates.append(GeneralConstants.STR_COMMA).append(GeneralConstants.BLANK_SPACE);
					}
				payrollDetailTO.setCertificatesRelated(sbCertificatesIds.toString());
				payrollDetailTO.setComments(sbCertificates.substring(0, sbCertificates.length()-3).toString());		
				
				
				
				if(Validations.validateListIsNullOrEmpty(searchCertificateVaultTO.getLstRelation())){
					List<PayrollDetailTO> lstTemp = new ArrayList<>();
					lstTemp.add(payrollDetailTO);
					searchCertificateVaultTO.setLstRelation(lstTemp);					
				}else
					searchCertificateVaultTO.getLstRelation().add(payrollDetailTO);
				searchCertificateVaultTO.getLstResult().getDataList().remove(searchCertificateVaultTO.getPayrollDetailTOSelected());
				searchCertificateVaultTO.setPayrollDetailTOSelected(null);
				searchCertificateVaultTO.setLstPhysicalCertificates(null);
			/*}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"No se puede relacionar los certificados ya que las cantidades con la solicitud no son exactas.");
				JSFUtilities.showSimpleValidationDialog();	
			}*/
		
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Debe seleccionar por lo menos un certificado.");
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	public void searchEntry(){
		try {
			searchCertificateVaultTO.setBlNoResult(true);
			searchCertificateVaultTO.setLstResult(null);
			/*
			searchCertificateVaultTO.setPayrollDetailState();
			*/
			List<PayrollDetailTO> lstResult = certificateVaultFacade.getPayrollForVault(searchCertificateVaultTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchCertificateVaultTO.setBlNoResult(false);
				searchCertificateVaultTO.setLstResult(new GenericDataModel<>(lstResult));
				for(PayrollDetailTO payrollDetailTO:searchCertificateVaultTO.getLstResult()){
					payrollDetailTO.setStateDesc(mpDetailStates.get(payrollDetailTO.getState()));
					if(payrollDetailTO.getIdVaultPk() != null) {
						
						if(payrollDetailTO.getIdVaultPk().equals(0)) {
							if(payrollDetailTO.getState().equals(PayrollDetailStateType.RECEIVED.getCode()) ||
								payrollDetailTO.getState().equals(PayrollDetailStateType.MICROFILMACION.getCode()) ||
								payrollDetailTO.getState().equals(PayrollDetailStateType.ANALISIS1.getCode()) ||
								payrollDetailTO.getState().equals(PayrollDetailStateType.ANALISIS2.getCode()) /*||
								payrollDetailTO.getState().equals(PayrollDetailStateType.ANALYZED.getCode())*/ 
								) {
								payrollDetailTO.setIdVaultPk(VaultType.PROVISORY.getCode());
							}else {
								payrollDetailTO.setIdVaultPk(VaultType.VAULT_CAVAPY.getCode());
							}
						}
						//payrollDetailTO.setVaultName(mpVaults.get(payrollDetailTO.getIdVaultPk()));
					}else {
						if(payrollDetailTO.getState().equals(PayrollDetailStateType.RECEIVED.getCode()) ||
							payrollDetailTO.getState().equals(PayrollDetailStateType.MICROFILMACION.getCode()) ||
							payrollDetailTO.getState().equals(PayrollDetailStateType.ANALISIS1.getCode()) ||
							payrollDetailTO.getState().equals(PayrollDetailStateType.ANALISIS2.getCode()) /*||
							payrollDetailTO.getState().equals(PayrollDetailStateType.ANALYZED.getCode()) */
							) {
							payrollDetailTO.setIdVaultPk(VaultType.PROVISORY.getCode());
						}else {
							payrollDetailTO.setIdVaultPk(VaultType.VAULT_CAVAPY.getCode());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void searchCertificates(){
		try {			
			searchCertificateVaultTO.setLstPhysicalCertificates(certificateVaultFacade.getPhysicalCertificates(searchCertificateVaultTO));
			if(Validations.validateListIsNotNullAndNotEmpty(searchCertificateVaultTO.getLstPhysicalCertificates())){				
				for(PhysicalCertificate physicalCertificate:searchCertificateVaultTO.getLstPhysicalCertificates())
					physicalCertificate.setVaultLocationDesc(mpVaults.get(physicalCertificate.getVaultLocation()));
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"No se encontraron certificados para el valor en la solicitud.");
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void searchCertificatesWithoutCupons(){
		try {			
			searchCertificateVaultTO.setLstPhysicalCertificates(certificateVaultFacade.getPhysicalCertificatesRetirementWithoutCupons(searchCertificateVaultTO));
			if(Validations.validateListIsNotNullAndNotEmpty(searchCertificateVaultTO.getLstPhysicalCertificates())){				
				for(PhysicalCertificate physicalCertificate:searchCertificateVaultTO.getLstPhysicalCertificates())
					physicalCertificate.setVaultLocationDesc(mpVaults.get(physicalCertificate.getVaultLocation()));
			}else{
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
						"No se encontraron certificados para el valor en la solicitud.");
				JSFUtilities.showSimpleValidationDialog();
			}
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void searchExit(){
		try {
			searchCertificateVaultTO.setLstPhysicalCertificates(null);
			searchCertificateVaultTO.setPayrollDetailTOSelected(null);
			searchCertificateVaultTO.setLstRelation(null);
			searchCertificateVaultTO.setBlNoResult(true);			
			searchCertificateVaultTO.setLstResult(null);
			searchCertificateVaultTO.setPayRollType(PayrollType.REMOVE_SECURITIES_OPERATION.getCode());
			List<PayrollDetailTO> lstResult = certificateVaultFacade.getPayrollForVault(searchCertificateVaultTO);
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				searchCertificateVaultTO.setBlNoResult(false);
				searchCertificateVaultTO.setLstResult(new GenericDataModel<>(lstResult));
				for(PayrollDetailTO payrollDetailTO:searchCertificateVaultTO.getLstResult()){
					payrollDetailTO.setStateDesc(mpDetailStates.get(payrollDetailTO.getState()));
					payrollDetailTO.setVaultName(mpVaults.get(payrollDetailTO.getIdVaultPk()));
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void changeBranchOffice(){
		try {
			cleanResults();
			lstVaults = new ArrayList<>();
			mpVaults = new HashMap<>();
			if(Validations.validateIsNotNullAndNotEmpty(searchCertificateVaultTO.getBranchOffice())){				
				ParameterTableTO paramTabTO = new ParameterTableTO();
				paramTabTO.setMasterTableFk(MasterTableType.VAULTS_FOR_BRANCH.getCode());
				paramTabTO.setIdRelatedParameterFk(searchCertificateVaultTO.getBranchOffice());
				List<ParameterTable> lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
				for(ParameterTable paramTab:lstTemp){
					if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState()))
						lstVaults.add(paramTab);
					mpVaults.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
				}
			}	
		} catch (Exception e) {
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}		
	}
	
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		filterCertificateVaultTO = new SearchCertificateVaultTO();
		filterCertificateVaultTO.setInitialDate(getCurrentSystemDate());
		filterCertificateVaultTO.setFinalDate(getCurrentSystemDate());
		filterCertificateVaultTO.setIssuer(new Issuer());
		filterCertificateVaultTO.setHolder(new Holder());
		filterCertificateVaultTO.setSecurity(new Security());
	}
	
	public void clean(){
		JSFUtilities.resetViewRoot();
		searchCertificateVaultTO = new SearchCertificateVaultTO();
		searchCertificateVaultTO.setIssuer(new Issuer());
	}
	
	public void cleanResultsSearch(){
		filterCertificateVaultTO.setBlNoResult(false);
		filterCertificateVaultTO.setLstResult(null);
	}
	
	public void cleanResults(){
		searchCertificateVaultTO.setBlNoResult(false);
		searchCertificateVaultTO.setLstResult(null);
		searchCertificateVaultTO.setLstPhysicalCertificates(null);
		searchCertificateVaultTO.setLstRelation(null);
	}
	
	public void beforeEntry(){
		boolean blSelected = false;
		for(PayrollDetailTO payrollDetailTO:searchCertificateVaultTO.getLstResult()){
			if(payrollDetailTO.isBlSelected()){
				blSelected = true;
				break;
			}
		}
		if(blSelected){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					"¿Está seguro que desea ingresar los siguientes títulos a la Bóveda asignada?");			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"Debe seleccionar por lo menos un registro.");
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	public void selectedAllBoveda(PayrollDetailTO currentSelected) {
		List<PayrollDetailTO> lstPayrollDetailTO = searchCertificateVaultTO.getLstResult().getDataList();
		if(searchCertificateVaultTO.getLstResult()!=null) {
			for(PayrollDetailTO current : lstPayrollDetailTO) {
				if( currentSelected.getIdCustodyOperationPk().equals(current.getIdCustodyOperationPk()) ) {
					current.setIdVaultPk(currentSelected.getIdVaultPk());
				}
			}
			searchCertificateVaultTO.setLstResult(new GenericDataModel<>(lstPayrollDetailTO));
		}
	}
	
	public void selectedAllCupon() {
		List<Long> allCustodySelecteds = new ArrayList<Long>();
		List<PayrollDetailTO> lstPayrollDetailTO = searchCertificateVaultTO.getLstResult().getDataList();
		List<PayrollDetailTO> lstPayrollDetailTONew = new ArrayList<PayrollDetailTO>();
		Integer currentVault = 0;
		if(searchCertificateVaultTO.getLstResult()!=null) {
			
			for(PayrollDetailTO current : lstPayrollDetailTO) {
				if( current.isBlSelected() && !current.isBlIsCupon() ) {
					allCustodySelecteds.add(current.getIdCustodyOperationPk());
					currentVault = current.getIdVaultPk();
				}
			}
			for(PayrollDetailTO current : lstPayrollDetailTO) {
				current.setBlSelected(false);
			}
			for(PayrollDetailTO current : lstPayrollDetailTO) {
				for(Long custodyPk : allCustodySelecteds) {
					if( custodyPk.equals(current.getIdCustodyOperationPk()) ) {
						current.setBlSelected(true);
						if(currentVault == null || (currentVault != null && currentVault == 0)) {
							current.setIdVaultPk(VaultType.VAULT_CAVAPY.getCode());
						}else {
							current.setIdVaultPk(currentVault);
						}
					}
				}
				lstPayrollDetailTONew.add(current);
			}
			
			searchCertificateVaultTO.setLstResult(new GenericDataModel<>(lstPayrollDetailTONew));
			
		}
	}

	@LoggerAuditWeb
	public void vaultConsolidation() throws IOException, ServiceException {
		/*
		List<String> lstBoveda = null;
		
		if( searchCertificateVaultTO!=null && searchCertificateVaultTO.getLstResult()!=null) {
			lstBoveda = new ArrayList<String>();
			for( PayrollDetailTO payrollDetailTO: searchCertificateVaultTO.getLstResult().getDataList() ){
				if(payrollDetailTO.isBlSelected()){
					if(payrollDetailTO.getPositionInVault()!=null && !payrollDetailTO.getPositionInVault().equals("")) {
						
						String[] arrayPosition = payrollDetailTO.getPositionInVault().split("<br/>");
						for(int i=0; i<arrayPosition.length; i++) {
							lstBoveda.add(arrayPosition[i].trim());
						}
					}
				}
			}
		}
		*/

		List<VaultSummary>  arrayDataVault = certificateVaultFacade.getVaultSummaryAll(null);
		
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "Resumen de Boveda");
	    
        String[] headers = new String[]{
                "ARMARIO",
                "NIVEL",
                "CAJA",
                "FOLIO",
                "INDICE CAJA",
                "SERIE",
                "EMISOR",
                "VALOR NOMINAL",
                "MONEDA",
                "CANTIDAD",
                "DESDE HASTA",
                "INGRESO",
                "VENCIMIENTO",
                "DEPOSITANTE"
        };

        XSSFRow headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; ++i) {
            String header = headers[i];
            XSSFCell cell = headerRow.createCell(i);
            cell.setCellValue(header);
        }
        
        for (int i = 0; i < arrayDataVault.size(); ++i) {
            XSSFRow dataRow = sheet.createRow(i + 1);
            
        	VaultSummary vaultSummary = arrayDataVault.get(i);
            dataRow.createCell(0).setCellValue(vaultSummary.getArmario());
            dataRow.createCell(1).setCellValue(vaultSummary.getNivel());
            dataRow.createCell(2).setCellValue(vaultSummary.getCaja());
            dataRow.createCell(3).setCellValue(vaultSummary.getFolio());
            dataRow.createCell(4).setCellValue(vaultSummary.getIndiceCaja());
            dataRow.createCell(5).setCellValue(vaultSummary.getSerie());
            dataRow.createCell(6).setCellValue(vaultSummary.getEmisor());
            dataRow.createCell(7).setCellValue(vaultSummary.getSecurityNominalValue());
            dataRow.createCell(8).setCellValue(vaultSummary.getCurrency());
            dataRow.createCell(9).setCellValue(vaultSummary.getQuantity());
            dataRow.createCell(10).setCellValue(vaultSummary.getCertificateFromTo());
            dataRow.createCell(11).setCellValue(vaultSummary.getFechaIngreso());
            dataRow.createCell(12).setCellValue(vaultSummary.getFechaVencimiento());
            dataRow.createCell(13).setCellValue(vaultSummary.getDepositante());
        }
            FacesContext context = FacesContext.getCurrentInstance();

            context.getExternalContext().setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            context.getExternalContext().setResponseHeader("Expires", "0");
            context.getExternalContext().setResponseHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            context.getExternalContext().setResponseHeader("Pragma", "public");
            context.getExternalContext().setResponseHeader("Content-disposition", "attachment;filename=reporteBoveda.xlsx");
            context.getExternalContext().addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());

            OutputStream out = context.getExternalContext().getResponseOutputStream();
            workbook.write(out);
            context.getExternalContext().responseFlushBuffer();
            
            try {
                context.responseComplete();
            } catch (/*IO*/Exception e) {
                e.printStackTrace();
            }
            

	}
	
	
	@LoggerAuditWeb
	public void vaultConsolidationRetirement() throws IOException, ServiceException {
		List<String> lstBoveda = null;
		
		if( searchCertificateVaultTO!=null && searchCertificateVaultTO.getLstResult()!=null) {
			lstBoveda = new ArrayList<String>();
			for( PayrollDetailTO payrollDetailTO: searchCertificateVaultTO.getLstResult().getDataList() ){
				if(payrollDetailTO.getPositionInVault()!=null && !payrollDetailTO.getPositionInVault().equals("")) {
					
					String[] arrayPosition = payrollDetailTO.getPositionInVault().split("<br/>");
					for(int i=0; i<arrayPosition.length; i++) {
						lstBoveda.add(arrayPosition[i].trim());
					}
					
				}
			}
		}

		List<VaultSummary>  arrayDataVault = certificateVaultFacade.getVaultSummaryAll(lstBoveda);
		
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "Resumen de Boveda");
	    
        String[] headers = new String[]{
                "ARMARIO",
                "NIVEL",
                "CAJA",
                "FOLIO",
                "INDICE CAJA",
                "SERIE",
                "EMISOR",
                "VALOR NOMINAL",
                "MONEDA",
                "CANTIDAD",
                "DESDE HASTA",
                "INGRESO",
                "VENCIMIENTO",
                "DEPOSITANTE"
        };

        XSSFRow headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; ++i) {
            String header = headers[i];
            XSSFCell cell = headerRow.createCell(i);
            cell.setCellValue(header);
        }
        
        for (int i = 0; i < arrayDataVault.size(); ++i) {
            XSSFRow dataRow = sheet.createRow(i + 1);
            
        	VaultSummary vaultSummary = arrayDataVault.get(i);
            dataRow.createCell(0).setCellValue(vaultSummary.getArmario());
            dataRow.createCell(1).setCellValue(vaultSummary.getNivel());
            dataRow.createCell(2).setCellValue(vaultSummary.getCaja());
            dataRow.createCell(3).setCellValue(vaultSummary.getFolio());
            dataRow.createCell(4).setCellValue(vaultSummary.getIndiceCaja());
            dataRow.createCell(5).setCellValue(vaultSummary.getSerie());
            dataRow.createCell(6).setCellValue(vaultSummary.getEmisor());
            dataRow.createCell(7).setCellValue(vaultSummary.getSecurityNominalValue());
            dataRow.createCell(8).setCellValue(vaultSummary.getCurrency());
            dataRow.createCell(9).setCellValue(vaultSummary.getQuantity());
            dataRow.createCell(10).setCellValue(vaultSummary.getCertificateFromTo());
            dataRow.createCell(11).setCellValue(vaultSummary.getFechaIngreso());
            dataRow.createCell(12).setCellValue(vaultSummary.getFechaVencimiento());
            dataRow.createCell(13).setCellValue(vaultSummary.getDepositante());
        }
    		
            
            FacesContext context = FacesContext.getCurrentInstance();

            context.getExternalContext().setResponseContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            context.getExternalContext().setResponseHeader("Expires", "0");
            context.getExternalContext().setResponseHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            context.getExternalContext().setResponseHeader("Pragma", "public");
            context.getExternalContext().setResponseHeader("Content-disposition", "attachment;filename=reporteBoveda.xlsx");
            context.getExternalContext().addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());

            OutputStream out = context.getExternalContext().getResponseOutputStream();
            workbook.write(out);
            context.getExternalContext().responseFlushBuffer();
            
            try {
                context.responseComplete();
            } catch (/*IO*/Exception e) {
                e.printStackTrace();
            }
            

	}

	
	@LoggerAuditWeb
	public void entryVault(){
		try {
			String listPayRollDetailPk = certificateVaultFacade.entryToVault(searchCertificateVaultTO.getLstResult().getDataList());
			
			if( !listPayRollDetailPk.equals("")) {
				
				//certificateVaultFacade.updatePayrollHeadState(searchCertificateVaultTO.getLstResult().getDataList());
				executeBatchCreateIssuanceAndSecurityWithEntryToVault(listPayRollDetailPk);
			}else {
				showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
						, "Hubo un problema al ingresaron los titulos a la Bóveda.");
				JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
				return;
			}
			
			
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, "Se ingresaron los siguientes titulos a la Bóveda.");
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {			
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	

	@Inject
	@InjectableResource(location = "ServerConfiguration.properties")
	private Properties serverConfiguration;
	
	@LoggerAuditWeb
	public void executeBatchCreateIssuanceAndSecurityWithEntryToVault(String listPayRollDetailPk ) throws KeyManagementException, UniformInterfaceException, ClientHandlerException, NoSuchAlgorithmException {
		
    	StringBuilder urlResource = new StringBuilder();
        urlResource.append(serverPath).append("/").append(ModuleWarType.SECURITIES.getValue()).
        append("/resources/IssuanceSecurityResource/createIssuanceAndSecurityWithEntryToVault").
        append("/").append(userInfo.getUserAccountSession().getUserName()).append("/").append(userInfo.getUserAccountSession().getIpAddress()).append("/").append(listPayRollDetailPk);
        createClientJerser().resource(urlResource.toString()).type("application/json").get(ClientResponse.class);
        
		/*
		BusinessProcess businessProcess = new BusinessProcess();
		businessProcess.setIdBusinessProcessPk(BusinessProcessType.CREATE_ISSUANCE_SECURITY_WITH_VALUT_ENTRY.getCode());
		try {
			Map<String, Object> details = new HashMap<String, Object>();
			details.put(GeneralConstants.PAYROLL_DETAIL_PK, listPayRollDetailPk);
			details.put(GeneralConstants.USER_NAME, userInfo.getUserAccountSession().getUserName());
			batchProcessServiceFacade.registerBatch(userInfo.getUserAccountSession().getUserName(),businessProcess,details);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		*/
	}
	
	private Client createClientJerser() throws NoSuchAlgorithmException,KeyManagementException{
		return Client.create();
	}
	
	public void beforeExit(){

		searchCertificateVaultTO.setLstRelation(null);
		if(searchCertificateVaultTO.getLstResult() != null && searchCertificateVaultTO.getLstResult().getSize()>0) {
			if(searchCertificateVaultTO.getLstRelation() == null) {
				searchCertificateVaultTO.setLstRelation(new ArrayList<PayrollDetailTO>());
			}
			for(PayrollDetailTO current: searchCertificateVaultTO.getLstResult()) {
				if(current.isBlSelected()) {
					current.setCertificatesRelated(""+current.getIdPhysicalCertificatePk());
					searchCertificateVaultTO.getLstRelation().add(current);
				}
			}
		}
		
		/*searchCertificateVaultTO.setLstRelation(null);
		//Boolean flagSelectedAny = false;
		for(PayrollDetailTO current: searchCertificateVaultTO.getLstResult().getDataList()) {
			if(current.isBlSelected()) {
				if(searchCertificateVaultTO.getLstRelation() == null) {
					searchCertificateVaultTO.setLstRelation(new ArrayList<PayrollDetailTO>());
				}
				searchCertificateVaultTO.getLstRelation().add(current);
			}
		}*/
		//relateCertificates();
		//flagSelectedAny;
		if(searchCertificateVaultTO.getLstRelation() != null && searchCertificateVaultTO.getLstRelation().size()>0){
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_REGISTER), 
					"¿Está seguro que desea retirar los siguientes titulos a la Bóveda?");			
			JSFUtilities.executeJavascriptFunction("PF('cnfWAskDialog').show();");
		}else{
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT),
					"No se a seleccionado titulos para ser retirados.");
			JSFUtilities.showSimpleValidationDialog();
		}
	}
	
	@LoggerAuditWeb
	public void exitVault(){
		try {
			
			certificateVaultFacade.exitToVault(searchCertificateVaultTO.getLstRelation(),searchCertificateVaultTO.getMotive());
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_SUCCESS)
					, "Se retiraron los siguientes titulos a la Bóveda.");
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (ServiceException e) {
			e.printStackTrace();
			showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR)
					, PropertiesUtilities.getExceptionMessage(e.getErrorService().getMessage(), e.getParams()));
			JSFUtilities.executeJavascriptFunction("PF('cnfWEndDialog').show();");
		} catch (Exception e) {			
			e.printStackTrace();
			//excepcion.fire(new ExceptionToCatch(e));
		}
	}
	
	public void changeParticipant(){
		//filterCertificateVaultTO.setHolder(new Holder());
		cleanResultsSearch();
	}
	
	private void fillParameters(Integer payRollType) throws ServiceException{
		mpDetailStates = new HashMap<>();
		mpPayrollTypes = new HashMap<>();
		lstDetailStates = new ArrayList<>();
		lstPayrollTypes = new ArrayList<>();
		lstBranchOffices = new ArrayList<>();
		ParameterTableTO paramTabTO = new ParameterTableTO();
		
		List<ParameterTable> lstTemp = null;
		/***************/
		/*paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_DETAIL_STATE.getCode());
		 = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstDetailStates.add(paramTab);
			}
			mpDetailStates.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}*/
		
		//List<ParameterTable> lstTempPayrollDetail = new ArrayList<ParameterTable>();
		
		ParameterTable paramTable = new ParameterTable();
		paramTable.setParameterTablePk(PayrollDetailStateType.RECEIVED.getCode());
		paramTable.setParameterName(PayrollDetailStateType.RECEIVED.getValue());
		paramTable.setDescription(PayrollDetailStateType.RECEIVED.getValue());
		lstDetailStates.add(paramTable);
		mpDetailStates.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		
		paramTable = new ParameterTable();
		paramTable.setParameterTablePk(PayrollDetailStateType.MICROFILMACION.getCode());
		paramTable.setParameterName(PayrollDetailStateType.MICROFILMACION.getValue());
		paramTable.setDescription(PayrollDetailStateType.MICROFILMACION.getValue());
		lstDetailStates.add(paramTable);
		mpDetailStates.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		
		paramTable = new ParameterTable();
		paramTable.setParameterTablePk(PayrollDetailStateType.ANALISIS1.getCode());
		paramTable.setParameterName(PayrollDetailStateType.ANALISIS1.getValue());
		paramTable.setDescription(PayrollDetailStateType.ANALISIS1.getValue());
		lstDetailStates.add(paramTable);
		mpDetailStates.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		
		paramTable = new ParameterTable();
		paramTable.setParameterTablePk(PayrollDetailStateType.ANALISIS2.getCode());
		paramTable.setParameterName(PayrollDetailStateType.ANALISIS2.getValue());
		paramTable.setDescription(PayrollDetailStateType.ANALISIS2.getValue());
		lstDetailStates.add(paramTable);
		mpDetailStates.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		
		paramTable = new ParameterTable();
		paramTable.setParameterTablePk(PayrollDetailStateType.ANALYZED.getCode());
		paramTable.setParameterName(PayrollDetailStateType.ANALYZED.getValue());
		paramTable.setDescription(PayrollDetailStateType.ANALYZED.getValue());
		lstDetailStates.add(paramTable);
		mpDetailStates.put(paramTable.getParameterTablePk(), paramTable.getParameterName());
		
		paramTable = new ParameterTable();
		paramTable.setParameterTablePk(PayrollDetailStateType.VAULT.getCode());
		paramTable.setParameterName(PayrollDetailStateType.VAULT.getValue());
		paramTable.setDescription(PayrollDetailStateType.VAULT.getValue());
		lstDetailStates.add(paramTable);
		mpDetailStates.put(paramTable.getParameterTablePk(), paramTable.getParameterName());

		/***/
		
		
		paramTabTO.setMasterTableFk(MasterTableType.PAYROLL_TYPE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())
					&& paramTab.getParameterTablePk().equals(payRollType)){
				lstPayrollTypes.add(paramTab);
			}
			mpPayrollTypes.put(paramTab.getParameterTablePk(), paramTab.getParameterName());
		}
		
		paramTabTO.setMasterTableFk(MasterTableType.BRANCH_OFFICE.getCode());
		lstTemp = generalParametersFacade.getListParameterTableServiceBean(paramTabTO);
		for(ParameterTable paramTab:lstTemp){
			if(ParameterTableStateType.REGISTERED.getCode().equals(paramTab.getParameterState())){
				lstBranchOffices.add(paramTab);
			}
		}		
	}
	
	public Map<Integer, String> getMpPayrollTypes() {
		return mpPayrollTypes;
	}
	public void setMpPayrollTypes(Map<Integer, String> mpPayrollTypes) {
		this.mpPayrollTypes = mpPayrollTypes;
	}
	public Map<Integer, String> getMpDetailStates() {
		return mpDetailStates;
	}
	public void setMpDetailStates(Map<Integer, String> mpDetailStates) {
		this.mpDetailStates = mpDetailStates;
	}	
	public List<ParameterTable> getLstPayrollTypes() {
		return lstPayrollTypes;
	}
	public void setLstPayrollTypes(List<ParameterTable> lstPayrollTypes) {
		this.lstPayrollTypes = lstPayrollTypes;
	}
	public List<ParameterTable> getLstBranchOffices() {
		return lstBranchOffices;
	}
	public void setLstBranchOffices(List<ParameterTable> lstBranchOffices) {
		this.lstBranchOffices = lstBranchOffices;
	}
	public List<Participant> getLstParticipants() {
		return lstParticipants;
	}
	public void setLstParticipants(List<Participant> lstParticipants) {
		this.lstParticipants = lstParticipants;
	}
	public SearchCertificateVaultTO getSearchCertificateVaultTO() {
		return searchCertificateVaultTO;
	}
	public void setSearchCertificateVaultTO(
			SearchCertificateVaultTO searchCertificateVaultTO) {
		this.searchCertificateVaultTO = searchCertificateVaultTO;
	}
	public List<ParameterTable> getLstDetailStates() {
		return lstDetailStates;
	}
	public void setLstDetailStates(List<ParameterTable> lstDetailStates) {
		this.lstDetailStates = lstDetailStates;
	}
	public List<ParameterTable> getLstVaults() {
		return lstVaults;
	}
	public void setLstVaults(List<ParameterTable> lstVaults) {
		this.lstVaults = lstVaults;
	}
	public Map<Integer, String> getMpVaults() {
		return mpVaults;
	}
	public void setMpVaults(Map<Integer, String> mpVaults) {
		this.mpVaults = mpVaults;
	}

	public List<ParameterTable> getLstPayrollTypesSearch() {
		return lstPayrollTypesSearch;
	}

	public void setLstPayrollTypesSearch(List<ParameterTable> lstPayrollTypesSearch) {
		this.lstPayrollTypesSearch = lstPayrollTypesSearch;
	}

	public List<ParameterTable> getLstBranchOfficesSearch() {
		return lstBranchOfficesSearch;
	}

	public void setLstBranchOfficesSearch(
			List<ParameterTable> lstBranchOfficesSearch) {
		this.lstBranchOfficesSearch = lstBranchOfficesSearch;
	}

	public Map<Integer, String> getMpPayrollTypesSearch() {
		return mpPayrollTypesSearch;
	}

	public void setMpPayrollTypesSearch(Map<Integer, String> mpPayrollTypesSearch) {
		this.mpPayrollTypesSearch = mpPayrollTypesSearch;
	}

	public Map<Integer, String> getMpBranchOfficesSearch() {
		return mpBranchOfficesSearch;
	}

	public void setMpBranchOfficesSearch(Map<Integer, String> mpBranchOfficesSearch) {
		this.mpBranchOfficesSearch = mpBranchOfficesSearch;
	}

	public List<Participant> getLstParticipantsSearch() {
		return lstParticipantsSearch;
	}

	public void setLstParticipantsSearch(List<Participant> lstParticipantsSearch) {
		this.lstParticipantsSearch = lstParticipantsSearch;
	}

	public SearchCertificateVaultTO getFilterCertificateVaultTO() {
		return filterCertificateVaultTO;
	}

	public void setFilterCertificateVaultTO(
			SearchCertificateVaultTO filterCertificateVaultTO) {
		this.filterCertificateVaultTO = filterCertificateVaultTO;
	}

	public List<ParameterTable> getLstVaultsSearch() {
		return lstVaultsSearch;
	}

	public void setLstVaultsSearch(List<ParameterTable> lstVaultsSearch) {
		this.lstVaultsSearch = lstVaultsSearch;
	}

	public Map<Integer, String> getMpVaultsSearch() {
		return mpVaultsSearch;
	}

	public void setMpVaultsSearch(Map<Integer, String> mpVaultsSearch) {
		this.mpVaultsSearch = mpVaultsSearch;
	}

	public Map<Integer, String> getMpSituationSearch() {
		return mpSituationSearch;
	}

	public void setMpSituationSearch(Map<Integer, String> mpSituationSearch) {
		this.mpSituationSearch = mpSituationSearch;
	}

	public List<ParameterTable> getLstSituationSearch() {
		return lstSituationSearch;
	}

	public void setLstSituationSearch(List<ParameterTable> lstSituationSearch) {
		this.lstSituationSearch = lstSituationSearch;
	}

	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}

	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	public List<ParameterTable> getLstMotives() {
		return lstMotives;
	}

	public void setLstMotives(List<ParameterTable> lstMotives) {
		this.lstMotives = lstMotives;
	}

}
