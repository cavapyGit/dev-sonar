package com.pradera.custody.vaultcertificates.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.TransactionSynchronizationRegistry;

import org.jboss.ejb3.annotation.TransactionTimeout;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.contextholder.interceptor.LoggerAuditWeb;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.type.BusinessProcessType;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.accounts.service.HolderAccountComponentServiceBean;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.framework.batchprocess.facade.BatchProcessServiceFacade;
import com.pradera.custody.payroll.view.PayrollDetailTO;
import com.pradera.custody.payroll.view.PhysicalCertificateTO;
import com.pradera.custody.payroll.view.VaultSummary;
import com.pradera.custody.receivecertificates.servicebean.ReceiveCertificateServiceBean;
import com.pradera.custody.vaultcertificates.servicebean.CertificateVaultServiceBean;
import com.pradera.custody.vaultcertificates.view.SearchCertificateVaultTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.component.VaultControlComponent;
import com.pradera.model.custody.payroll.PayrollDetail;
import com.pradera.model.custody.payroll.PayrollDetailStateType;
import com.pradera.model.custody.payroll.PayrollHeader;
import com.pradera.model.custody.payroll.PayrollHeaderStateType;
import com.pradera.model.custody.payroll.PayrollType;
import com.pradera.model.custody.payroll.RetirementCertificate;
import com.pradera.model.custody.physical.PhysicalCertificate;
import com.pradera.model.custody.securitiesretirement.RetirementDetail;
import com.pradera.model.custody.securitiesretirement.RetirementOperation;
import com.pradera.model.custody.type.MotiveReteirementDepositeType;
import com.pradera.model.custody.type.RetirementOperationMotiveType;
import com.pradera.model.custody.type.SituationType;
import com.pradera.model.custody.type.VaultType;
import com.pradera.model.custody.vaultmovements.VaultMovements;
import com.pradera.model.process.BusinessProcess;

@Stateless
@Interceptors(ContextHolderInterceptor.class)
public class CertificateVaultFacade {
	
	@EJB
	CertificateVaultServiceBean certificateVaultServiceBean;
	@Inject
	ReceiveCertificateServiceBean receiveCertificateServiceBean;
	@EJB
	HolderAccountComponentServiceBean holderAccountComponentServiceBean;
	@EJB
	BatchProcessServiceFacade batchProcessServiceFacade;
	
	@Resource
	TransactionSynchronizationRegistry transactionRegistry;

	public List<PayrollDetailTO> getPayrollForVault(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		List<Object[]> lstTemp = null;		
		List<PayrollDetailTO> lstResult = new ArrayList<>();
		/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(searchCertificateVaultTO.getPayRollType())) {
			lstTemp = certificateVaultServiceBean.getPayrollDetailCorporative(searchCertificateVaultTO);
		}else 
		*/	
		if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(searchCertificateVaultTO.getPayRollType())) {
			lstTemp = certificateVaultServiceBean.getPayrollDetailAccountAnnotation(searchCertificateVaultTO);
		}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(searchCertificateVaultTO.getPayRollType())) {
			lstTemp = certificateVaultServiceBean.getPayrollDetailRemoveSecurities(searchCertificateVaultTO);
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstTemp)){
			for(Object[] objResult:lstTemp){
				PayrollDetailTO payrollDetailTO = new PayrollDetailTO();
				/*if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(searchCertificateVaultTO.getPayRollType())) {
					payrollDetailTO.setIdCorporativeOperationPk(new Long(objResult[0].toString()));
				}else{*/
					if( PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(searchCertificateVaultTO.getPayRollType()) ){

						HolderAccountTO temp = new HolderAccountTO();
						temp.setIdHolderAccountPk(new Long(objResult[4].toString()));
						//getVaultControlComponentWithSecurityCodeAndCertificate
						payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[0].toString()));
						payrollDetailTO.setRegistryDate((Date) objResult[1]);
						payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[2].toString()));
						payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNullAndNotEmpty(objResult[3])?objResult[3].toString():null);
						payrollDetailTO.setQuantity((BigDecimal) objResult[5]);
						payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[6].toString()));
						payrollDetailTO.setState(new Integer(objResult[7].toString()));
						payrollDetailTO.setStateDesc(PayrollDetailStateType.get(payrollDetailTO.getState()).getValue());
						payrollDetailTO.setIndSerializable(new Integer(objResult[8].toString()));
						payrollDetailTO.setBranchOffice(new Integer(objResult[9].toString()));
						payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));	
						
						if(objResult[10]!=null){
							payrollDetailTO.setIdPhysicalCertificatePk(new Long(objResult[10].toString()));
						}
						if(objResult[13]!=null) {
							payrollDetailTO.setSerialCode(objResult[13].toString());
						}
						if(objResult[15] != null) {
							payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[15]);
						}
						
						Integer indCupon = new Integer(objResult[11].toString());
						
						if(searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) || 
							searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode()) ) {
							payrollDetailTO.setBlIsCupon(false);
						}else {
							payrollDetailTO.setBlIsCupon((indCupon==1));	
						}
						
						payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[12].toString()):null);
						
						
						String strPositionInVault = "";
						if(payrollDetailTO.getIdSecurityCodePk()!=null && !payrollDetailTO.getIdSecurityCodePk().equals("")) {
							VaultControlComponent vaultControlComponent = null;
							if(payrollDetailTO.getCuponNumber() == null) {
								vaultControlComponent = certificateVaultServiceBean.getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getIdPhysicalCertificatePk());
							}else {
								vaultControlComponent = certificateVaultServiceBean.getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getCuponNumber());
							}
							if(vaultControlComponent!=null) {
								strPositionInVault = vaultControlComponent.getArmario()+" - "+vaultControlComponent.getNivel()+" - "+vaultControlComponent.getCaja()+" - "+vaultControlComponent.getFolio()+" - "+vaultControlComponent.getIndiceCaja();
							}
						}
						payrollDetailTO.setPositionInVault(strPositionInVault);
						
						if(objResult[14]!=null) {
							payrollDetailTO.setIdVaultPk(new Integer(objResult[14].toString()));
						}else {
							payrollDetailTO.setIdVaultPk(0);
						}

						if(objResult[17] != null) {
							payrollDetailTO.setIdAnnotationOperationPk(new Long(objResult[17].toString()));
						}
						
						if(objResult[16] != null) {
							payrollDetailTO.setParticipantNemonic((String)objResult[16]);
						}
						
					}else if( PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(searchCertificateVaultTO.getPayRollType()) ) {

						payrollDetailTO.setIdCustodyOperationPk(new Long(objResult[0].toString()));
						payrollDetailTO.setRegistryDate((Date) objResult[1]);
						payrollDetailTO.setIdPayrollHeaderPk(new Long(objResult[2].toString()));
						payrollDetailTO.setIdSecurityCodePk(Validations.validateIsNotNullAndNotEmpty(objResult[3])?objResult[3].toString():null);
						
						HolderAccountTO temp = new HolderAccountTO();
						temp.setIdHolderAccountPk(new Long(objResult[4].toString()));
						payrollDetailTO.setHolderAccount(holderAccountComponentServiceBean.getHolderAccountComponentServiceBean(temp));	
						payrollDetailTO.setQuantity((BigDecimal) objResult[5]);
						payrollDetailTO.setIdPayrollDetailPk(new Long(objResult[6].toString()));
						payrollDetailTO.setState(new Integer(objResult[7].toString()));
						payrollDetailTO.setBranchOffice(new Integer(objResult[9].toString()));
						if(objResult[10]!=null){
							payrollDetailTO.setIdPhysicalCertificatePk(new Long(objResult[10].toString()));
						}

						PhysicalCertificate physicalCertificate = certificateVaultServiceBean.getPhysicalCertificate(payrollDetailTO.getIdPhysicalCertificatePk());
						payrollDetailTO.setIndSerializable( (physicalCertificate.getExpirationDate()== null)?1:0 );
						
						if(objResult[13]!=null) {
							payrollDetailTO.setSerialCode(objResult[13].toString());
						}
						if(objResult[15] != null) {
							payrollDetailTO.setSecurityNominalValue((BigDecimal)objResult[15]);
						}
						if(objResult[19] != null) {
							payrollDetailTO.setMnemonicIssuer(objResult[19].toString());
						}

						
						Integer indCupon = new Integer(objResult[11].toString());
						
						if(searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode()) || 
							//searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ||
							searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_GUARD_EXCLUSIVE.getCode())) {
							payrollDetailTO.setBlIsCupon(false);
							
						}else {
							payrollDetailTO.setBlIsCupon((indCupon==1));	
						}
						
						payrollDetailTO.setCuponNumber((indCupon==1)?new Integer(objResult[12].toString()):null);
						
						String strPositionInVault = "";
						
						if( searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.REVERSION.getCode()) || 
							searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.REVERSION_RV.getCode()) ||
							searchCertificateVaultTO.getMotive()!=null && searchCertificateVaultTO.getMotive().equals(RetirementOperationMotiveType.RETIREMENT_OTC.getCode())
						) {
							RetirementOperation retirementOperation = certificateVaultServiceBean.getRetirementOperationAndDetail(payrollDetailTO.getIdCustodyOperationPk());
							payrollDetailTO.setCuponQuantity(retirementOperation.getRetirementDetails().size());	
							payrollDetailTO.setLstRetirementDetailVault(retirementOperation.getRetirementDetails());
							System.out.println("************* payrollDetailTO.getLstRetirementDetailVault():: "+payrollDetailTO.getLstRetirementDetailVault().size()); 
							
							List<VaultControlComponent> lstVaultControlComponent = null;
							if(payrollDetailTO.getIndSerializable().equals(0)) {
								lstVaultControlComponent = certificateVaultServiceBean.getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk());
							}else {
								lstVaultControlComponent = new ArrayList<VaultControlComponent>();
								VaultControlComponent currentVault = certificateVaultServiceBean.getVaultControlComponentWithSecurityCodeAndCertificate(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getIdPhysicalCertificatePk()); 
								if(currentVault!=null) {
									lstVaultControlComponent.add(currentVault);
								}
							}
							
							if(lstVaultControlComponent!=null && lstVaultControlComponent.size()>0) {
								for(VaultControlComponent vaultControlComponent : lstVaultControlComponent) {
									strPositionInVault = strPositionInVault+vaultControlComponent.getArmario()+" - "+vaultControlComponent.getNivel()+" - "+vaultControlComponent.getCaja()+" - "+vaultControlComponent.getFolio()+" - "+vaultControlComponent.getIndiceCaja() + " <br/>";
								}
							}
							
							
						}else {
							payrollDetailTO.setCuponQuantity(1);
							VaultControlComponent vaultControlComponent = null;
							
							if(payrollDetailTO.getCuponNumber() == null) {
								vaultControlComponent = certificateVaultServiceBean.getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getIdPhysicalCertificatePk() );
							}else {
								vaultControlComponent = certificateVaultServiceBean.getVaultControlComponentWithSecurityCode(payrollDetailTO.getIdSecurityCodePk(), payrollDetailTO.getCuponNumber() );//
							}
							
							if(vaultControlComponent!=null) {
								strPositionInVault = vaultControlComponent.getArmario()+" - "+vaultControlComponent.getNivel()+" - "+vaultControlComponent.getCaja()+" - "+vaultControlComponent.getFolio()+" - "+vaultControlComponent.getIndiceCaja();
							}
							
						}
						
						if(objResult[14]!=null) {
							payrollDetailTO.setIdVaultPk(new Integer(objResult[14].toString()));
						}else {
							payrollDetailTO.setIdVaultPk(0);
						}
						
						payrollDetailTO.setPositionInVault(strPositionInVault);
						
						if(objResult[16] != null) {
							payrollDetailTO.setParticipantNemonic((String)objResult[16]);
						}

						if(objResult[17] != null) {
							payrollDetailTO.setIdAnnotationOperationPk(new Long(objResult[17].toString()));
						}

						if(objResult[18] != null) {
							payrollDetailTO.setMotive(Integer.valueOf(objResult[18].toString()));
						}
						
					}							
				//}			
				lstResult.add(payrollDetailTO);
			}
		}
		return lstResult;
	}
	
	public List<PhysicalCertificate> getPhysicalCertificates(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		Map<String, String> mpCertificatesRelated = new HashMap<>();
		if(Validations.validateListIsNotNullAndNotEmpty(searchCertificateVaultTO.getLstRelation())){
			for(PayrollDetailTO payrollDetailTO:searchCertificateVaultTO.getLstRelation()){
				if(payrollDetailTO.getIdSecurityCodePk().equals(searchCertificateVaultTO.getPayrollDetailTOSelected().getIdSecurityCodePk())){
					String[] certificates = payrollDetailTO.getComments().split(GeneralConstants.STR_COMMA.concat(GeneralConstants.BLANK_SPACE)); 
					for(String certificateNumber:certificates)
						mpCertificatesRelated.put(certificateNumber, certificateNumber);					
				}					
			}
		}
		List<PhysicalCertificate> lstTemp = certificateVaultServiceBean.getPhysicalCertificates(searchCertificateVaultTO);
		List<PhysicalCertificate> lstResult = new ArrayList<>();
		if(!mpCertificatesRelated.isEmpty()){
			for(PhysicalCertificate physicalCertificate:lstTemp){
				if(Validations.validateIsNull(mpCertificatesRelated.get(physicalCertificate.getCertificateNumber().toString())))
					lstResult.add(physicalCertificate);
			}
			return lstResult;
		}
		return lstTemp;
				
	}
	
	public List<PhysicalCertificate> getPhysicalCertificatesRetirementWithoutCupons(SearchCertificateVaultTO searchCertificateVaultTO) throws ServiceException{
		Map<String, String> mpCertificatesRelated = new HashMap<>();
		if(Validations.validateListIsNotNullAndNotEmpty(searchCertificateVaultTO.getLstRelation())){
			for(PayrollDetailTO payrollDetailTO:searchCertificateVaultTO.getLstRelation()){
				if(payrollDetailTO.getIdSecurityCodePk().equals(searchCertificateVaultTO.getPayrollDetailTOSelected().getIdSecurityCodePk())){
					String[] certificates = payrollDetailTO.getComments().split(GeneralConstants.STR_COMMA.concat(GeneralConstants.BLANK_SPACE)); 
					for(String certificateNumber:certificates)
						mpCertificatesRelated.put(certificateNumber, certificateNumber);					
				}					
			}
		}
		List<PhysicalCertificate> lstTemp = certificateVaultServiceBean.getPhysicalCertificates(searchCertificateVaultTO);
		List<PhysicalCertificate> lstResult = new ArrayList<>();
		if(!mpCertificatesRelated.isEmpty()){
			for(PhysicalCertificate physicalCertificate:lstTemp){
				if(Validations.validateIsNull(mpCertificatesRelated.get(physicalCertificate.getCertificateNumber().toString()))) {
					physicalCertificate.setBlSelected(true);
					lstResult.add(physicalCertificate);
				}
			}
			return lstResult;
		}
		
		if(searchCertificateVaultTO.getPayRollType().equals(PayrollType.REMOVE_SECURITIES_OPERATION.getCode())) {
			for(PhysicalCertificate tmp: lstTemp) {
				tmp.setBlSelected(true);
			}
		}
		
		return lstTemp;
	}

	public List<VaultSummary> getVaultSummary(List<String> lstIdSecurityCodePk) throws ServiceException{
		return certificateVaultServiceBean.getVaultSummary(lstIdSecurityCodePk);
	}
	
	public List<VaultSummary> getVaultSummaryAll(List<String> lstBoveda) throws ServiceException{
		return certificateVaultServiceBean.getVaultSummaryAll(lstBoveda);
	}
	
		
	
	@LoggerAuditWeb
	@TransactionTimeout(unit=TimeUnit.MINUTES, value=30)
	public String entryToVault(List<PayrollDetailTO> lstPayrollDetailTO) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		String listPayRollDetailPk = "";
		for(PayrollDetailTO payrollDetailTO:lstPayrollDetailTO){
			if(payrollDetailTO.isBlSelected()){
				PayrollDetail payrollDetail = certificateVaultServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
				
				if(payrollDetailTO.getIdVaultPk().equals(VaultType.VAULT_CAVAPY.getCode())) {
					if(! (PayrollDetailStateType.ANALYZED.getCode().equals(payrollDetail.getPayrollState())) ) // || PayrollDetailStateType.VAULT.getCode().equals(payrollDetail.getPayrollState())
						throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
				}
				
				VaultMovements vaultMovements = new VaultMovements();
				if(payrollDetailTO.getIdPhysicalCertificatePk()!=null){
					PhysicalCertificate physicalCertificate = certificateVaultServiceBean.find(PhysicalCertificate.class, payrollDetailTO.getIdPhysicalCertificatePk());				
					physicalCertificate.setVaultLocation(payrollDetailTO.getIdVaultPk());
					physicalCertificate.setBranchOffice(1);//payrollDetailTO.getBranchOffice()
					physicalCertificate.setSituation(PayrollDetailStateType.VAULT.getCode());
					vaultMovements.setPhysicalCertificate(physicalCertificate);
				}				
				
				vaultMovements.setVault(payrollDetailTO.getIdVaultPk());
				vaultMovements.setMovementQuantity(payrollDetailTO.getQuantity());
				vaultMovements.setBranchOffice(payrollDetailTO.getIdVaultPk());//payrollDetail.getBranchOffice()
				vaultMovements.setMovementType(1);		//1 INGRESO // 2 RETIRO		
				vaultMovements.setMovementDate(CommonsUtilities.currentDate());
				certificateVaultServiceBean.create(vaultMovements);


				//si no es boveda de cavapy no cambio el estado de la planilla
				if(payrollDetailTO.getIdVaultPk().equals(VaultType.VAULT_CAVAPY.getCode())) {
					payrollDetail.setPayrollState(PayrollDetailStateType.VAULT.getCode());
					if(payrollDetail.getIndSecurityCreated() == null) {
						payrollDetail.setIndSecurityCreated(0);
					}
				}
				payrollDetail.setCurrentVault(payrollDetailTO.getIdVaultPk());
				certificateVaultServiceBean.update(payrollDetail);

				PayrollHeader payrollHeader = certificateVaultServiceBean.getSinglePayrollHeaderAndDetailByPk(payrollDetailTO.getIdPayrollHeaderPk());
				Boolean allPayrollDetailInVault = true;
				for(PayrollDetail pd : payrollHeader.getLstPayrollDetail()) {
					
					if(!(pd.getPayrollState().equals(PayrollDetailStateType.REJECT.getCode()) || pd.getPayrollState().equals(PayrollDetailStateType.ANNULATE.getCode())) ) {
						if( !( pd.getCurrentVault()!=null && pd.getCurrentVault().equals(VaultType.VAULT_CAVAPY.getCode()) ) 
						) {
							allPayrollDetailInVault = false;
						}
					}
					
				}
				
				if(allPayrollDetailInVault) {
					payrollHeader.setPayrollState(PayrollHeaderStateType.FINISHED.getCode());
					certificateVaultServiceBean.update(payrollHeader);
				}
				/*
				if(payrollDetailTO.getIdVaultPk().equals(VaultType.VAULT_CAVAPY.getCode())) {
					payrollHeader.setPayrollState(PayrollHeaderStateType.FINISHED.getCode());
				}*/
				
				if(listPayRollDetailPk.equals("")) {
					listPayRollDetailPk = ""+payrollDetail.getIdPayrollDetailPk();	
				}else {
					listPayRollDetailPk = listPayRollDetailPk+","+payrollDetail.getIdPayrollDetailPk();
				}
				
			}			
		}
		
		//lstPayrollDetailTO que entraron a boveda

		return listPayRollDetailPk;
	}

	public void updatePayrollHeadState(List<PayrollDetailTO> lstPayrollDetailTO) throws ServiceException{
		if(lstPayrollDetailTO!=null) {
			List<Long> lstAllPayrollHeadPk = new ArrayList<Long>();
			List<Long> lstFilterPayrollHeadPk = new ArrayList<Long>();
			for (PayrollDetailTO payrollDetailTO : lstPayrollDetailTO) {
				if(payrollDetailTO.isBlSelected()){
					lstAllPayrollHeadPk.add(payrollDetailTO.getIdPayrollHeaderPk());
				}
			}
			
			if(lstAllPayrollHeadPk!=null) {
				lstFilterPayrollHeadPk = lstAllPayrollHeadPk.stream().distinct().collect(Collectors.toList());
				List<PayrollHeader> lstPayrollHeaders = certificateVaultServiceBean.getPayrollHeaderAndDetailByPk(lstFilterPayrollHeadPk);
				
				Boolean changeState;
				for (PayrollHeader payrollHeader : lstPayrollHeaders) {
					changeState= true;
					for(PayrollDetail detail : payrollHeader.getLstPayrollDetail()) {

						if(!(detail.getPayrollState().equals(PayrollDetailStateType.REJECT.getCode()) || detail.getPayrollState().equals(PayrollDetailStateType.ANNULATE.getCode())) ) {
							if( !(detail.getCurrentVault() != null && detail.getCurrentVault().equals(VaultType.VAULT_CAVAPY.getCode())) ) {
								changeState = false;
								break;
							}
						}
					}
					if(changeState) {
						payrollHeader.setPayrollState(PayrollHeaderStateType.FINISHED.getCode());
					}
				}
			}
			
		}
	}
	
	public void exitToVaultOld(List<PayrollDetailTO> lstRelated) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		List< PayrollHeader> lstPayrollHeader = new ArrayList<>();
		for(PayrollDetailTO payrollDetailTO:lstRelated){
			PayrollDetail payrollDetail = certificateVaultServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
			if(!lstPayrollHeader.contains(payrollDetail.getPayrollHeader().getIdPayrollHeaderPk())
					&& !PayrollHeaderStateType.PROCESSED.getCode().equals(payrollDetail.getPayrollHeader().getPayrollState()))
				lstPayrollHeader.add(payrollDetail.getPayrollHeader());
			if(!PayrollDetailStateType.GENERATED.getCode().equals(payrollDetail.getPayrollState()))
				throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
			payrollDetail.setPayrollState(PayrollDetailStateType.TRANSIT.getCode());
			RetirementOperation retirementOperation = new RetirementOperation();
			retirementOperation.setIdRetirementOperationPk(payrollDetailTO.getIdCustodyOperationPk());
			String[] certificates = payrollDetailTO.getCertificatesRelated().split(GeneralConstants.HYPHEN);
			for(String idPhysicalCertificatePk:certificates){
				PhysicalCertificate physicalCertificate = certificateVaultServiceBean.find(PhysicalCertificate.class, new Long(idPhysicalCertificatePk));
				if(!PayrollDetailStateType.VAULT.getCode().equals(physicalCertificate.getSituation()))
					throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
				physicalCertificate.setSituation(PayrollDetailStateType.TRANSIT.getCode());
				RetirementCertificate retirementCertificate = new RetirementCertificate();			
				retirementCertificate.setRetirementOperation(retirementOperation);
				retirementCertificate.setPhysicalCertificate(physicalCertificate);
				certificateVaultServiceBean.create(retirementCertificate);
				VaultMovements vaultMovements = new VaultMovements();
				vaultMovements.setPhysicalCertificate(physicalCertificate);				
				vaultMovements.setVault(physicalCertificate.getVaultLocation());
				vaultMovements.setMovementQuantity(physicalCertificate.getCertificateQuantity());
				vaultMovements.setBranchOffice(physicalCertificate.getBranchOffice());
				vaultMovements.setMovementType(2);
				vaultMovements.setMovementDate(CommonsUtilities.currentDate());
				certificateVaultServiceBean.create(vaultMovements);
			}
		}
		for(PayrollHeader payrolllHeader:lstPayrollHeader)
			payrolllHeader.setPayrollState(PayrollHeaderStateType.PROCESSED.getCode());
	}
	
	public void exitToVault(List<PayrollDetailTO> lstRelated, Integer motive) throws ServiceException{
		LoggerUser loggerUser = (LoggerUser)transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		loggerUser.setIdPrivilegeOfSystem(loggerUser.getUserAction().getIdPrivilegeSearch());
		
		for(PayrollDetailTO payrollDetailTO:lstRelated){

			PayrollHeader payrolllHeader = certificateVaultServiceBean.getSinglePayrollHeaderByPk(payrollDetailTO.getIdPayrollHeaderPk());		
			PayrollDetail payrollDetail = certificateVaultServiceBean.find(PayrollDetail.class, payrollDetailTO.getIdPayrollDetailPk());
			//for(PayrollDetail payrollDetail: payrolllHeader.getLstPayrollDetail()) {
			if(!PayrollDetailStateType.GENERATED.getCode().equals(payrollDetail.getPayrollState())) {
				throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
			}
			if(motive.equals(RetirementOperationMotiveType.RETIREMENT_EXPIRATION_CUPON.getCode())) {
				payrollDetail.setPayrollState(PayrollDetailStateType.ANALYZED.getCode());
			}else {
				payrollDetail.setPayrollState(PayrollDetailStateType.TRANSIT.getCode());
			}
			certificateVaultServiceBean.update(payrollDetail);
			

			
			RetirementOperation retirementOperation = new RetirementOperation();
			retirementOperation.setIdRetirementOperationPk(payrollDetailTO.getIdCustodyOperationPk());
			
			if(payrollDetailTO.getLstRetirementDetailVault()!=null) {
				for( RetirementDetail retirementDetailInVault: payrollDetailTO.getLstRetirementDetailVault()) {
					PayrollDetail payrollDetailWRT = certificateVaultServiceBean.getSinglePayrollDetailByRetirementDetailAndPayrollHeaderPk(payrollDetailTO.getIdPayrollHeaderPk(), retirementDetailInVault.getIdRetirementDetailPk());	
					if(payrollDetailWRT != null && !payrollDetailWRT.getIdPayrollDetailPk().equals(payrollDetail.getIdPayrollDetailPk())) {
						payrollDetailWRT.setPayrollState(payrollDetail.getPayrollState());
						certificateVaultServiceBean.update(payrollDetailWRT);
						
						
						/**/
						PhysicalCertificate physicalCertificate = certificateVaultServiceBean.getSinglePhysicalCertificateByRetirementDetailPk(retirementDetailInVault.getIdRetirementDetailPk());
						physicalCertificate.setSituation(PayrollDetailStateType.TRANSIT.getCode());
						RetirementCertificate retirementCertificate = new RetirementCertificate();			
						retirementCertificate.setRetirementOperation(retirementOperation);
						retirementCertificate.setPhysicalCertificate(physicalCertificate);
						certificateVaultServiceBean.create(retirementCertificate);
						
					}
				}
			}
			
			//}
			
			payrolllHeader.setPayrollState(PayrollHeaderStateType.PROCESSED.getCode());
			certificateVaultServiceBean.update(payrolllHeader);
		
			String[] certificates = payrollDetailTO.getCertificatesRelated().split(GeneralConstants.HYPHEN);
			
			
			for(String idPhysicalCertificatePk:certificates){
				PhysicalCertificate physicalCertificate = certificateVaultServiceBean.find(PhysicalCertificate.class, new Long(idPhysicalCertificatePk));
				
				//if( !( SituationType.CUSTODY_VAULT.getCode().equals(physicalCertificate.getSituation()) || SituationType.DEPOSITARY_VAULT.getCode().equals(physicalCertificate.getSituation()) ) ) {
				if( !( PayrollDetailStateType.VAULT.getCode().equals(physicalCertificate.getSituation()) /*|| PayrollDetailStateType.VAULT.getCode().equals(physicalCertificate.getSituation())*/ ) ) {	
					throw new ServiceException(ErrorServiceType.ACCOUNT_ANNOTATION_OPERATION_MODIFIED_STATE);
				}
				
				physicalCertificate.setSituation(PayrollDetailStateType.TRANSIT.getCode());
				RetirementCertificate retirementCertificate = new RetirementCertificate();			
				retirementCertificate.setRetirementOperation(retirementOperation);
				retirementCertificate.setPhysicalCertificate(physicalCertificate);
				certificateVaultServiceBean.create(retirementCertificate);
				
				VaultMovements vaultMovements = new VaultMovements();
				vaultMovements.setPhysicalCertificate(physicalCertificate);				
				vaultMovements.setVault(physicalCertificate.getVaultLocation());
				vaultMovements.setMovementQuantity(physicalCertificate.getCertificateQuantity());
				vaultMovements.setBranchOffice(physicalCertificate.getBranchOffice());
				vaultMovements.setMovementType(2);
				vaultMovements.setMovementDate(CommonsUtilities.currentDate());
				certificateVaultServiceBean.create(vaultMovements);
			}
		}
	}

	public List<PayrollDetailTO> searchPayrollDetail(SearchCertificateVaultTO to) throws ServiceException{
		
		List<PayrollDetailTO> list = new ArrayList<>();
		if(PayrollType.CORPORATIVE_OPERATION.getCode().equals(to.getPayRollType()))
			list = certificateVaultServiceBean.searchPayrollDetailCorporative(to);
		else if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(to.getPayRollType()))
			list = certificateVaultServiceBean.searchPayrollDetailAccountAnnotation(to);
		else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(to.getPayRollType()))
			list = certificateVaultServiceBean.searchPayrollDetailRemoveSecurities(to);
		
		return list;
	}

	public List<PayrollDetailTO> searchHolding(SearchCertificateVaultTO to) throws ServiceException{
		
		return certificateVaultServiceBean.searchHolding(to);
	}
	
	public List<PhysicalCertificateTO> searchPhysicalCerticate(SearchCertificateVaultTO to) throws ServiceException{
		
		return certificateVaultServiceBean.searchPhysicalCerticate(to);
	}
	
	public PayrollDetail searchPhysicalCerticateFile(PhysicalCertificateTO to) throws ServiceException{
		
		return certificateVaultServiceBean.searchPhysicalCerticateFile(to);
	}

	public List<PayrollDetailTO> searchPayrollDetailLocation(SearchCertificateVaultTO to) throws ServiceException{
		
		List<PayrollDetailTO> list = new ArrayList<>();
		if(PayrollType.ACCOUNT_ANNOTATION_OPERATION.getCode().equals(to.getPayRollType())) {
			list = certificateVaultServiceBean.searchPayrollDetailAccountAnnotationLocation(to);
		}else if(PayrollType.REMOVE_SECURITIES_OPERATION.getCode().equals(to.getPayRollType())) {
			list = certificateVaultServiceBean.searchPayrollDetailRemoveSecuritiesLocation(to);
		}else {
			List<PayrollDetailTO> listAnnotacion = certificateVaultServiceBean.searchPayrollDetailAccountAnnotationLocation(to);
			if(listAnnotacion!=null && listAnnotacion.size()>0) {
				list.addAll(listAnnotacion);
			}
			/*List<PayrollDetailTO> listRetirement = certificateVaultServiceBean.searchPayrollDetailRemoveSecurities(to);  
			if(listRetirement!=null && listRetirement.size()>0) {
				list.addAll(listRetirement);
			}*/
		}
		
		return list;
	}
	
}
