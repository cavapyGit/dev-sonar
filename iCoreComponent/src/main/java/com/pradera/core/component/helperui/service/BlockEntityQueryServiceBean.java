package com.pradera.core.component.helperui.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.BlockEntitySearchTO;
import com.pradera.core.component.helperui.to.BlockEntityTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.custody.blockentity.BlockEntity;
import com.pradera.model.custody.type.BlockEntityStateType;

@Stateless
@Performance
public class BlockEntityQueryServiceBean extends CrudDaoServiceBean{

	
	@SuppressWarnings("unchecked")
	public List<Object[]> findBlockEntityByFilter(BlockEntityTO blockEntityTO) {
		String strQuery = "SELECT 	documentNumber," +			//0
							"		(SELECT indicator1 from ParameterTable where BE.documentType = parameterTablePk)," +	//1
							"		idBlockEntityPk," +			//2
							"		name," +					//3
							"		(SELECT parameterName from ParameterTable where BE.stateBlockEntity = parameterTablePk)," +	//4
							"		holder.idHolderPk," +		//5
							"		fullName," +				//6
							"		firstLastName," +			//7
							"		secondLastName" +			//8
							" FROM BlockEntity BE" +
							" WHERE stateBlockEntity = :state";
		
		if(PersonType.NATURAL.getCode().equals(blockEntityTO.getPersonType())){
				strQuery += "	AND personType =  :personType";
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getName()))
				strQuery += "	AND name like  :name";
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getFirstLastName()))
				strQuery += "	AND firstLastName like :firstLastName";
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getSecondLastName()))
				strQuery += "	AND seconLastName like :secondLastName";
		}else if(PersonType.JURIDIC.getCode().equals(blockEntityTO.getPersonType())){
				strQuery += "	AND personType =  :personType";
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getFullName()))
				strQuery += "	AND fullName like :fullName";
		}
			
		if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getIdHolderPk()))
			strQuery += "	AND holder.idHolderPk = :idHolderPk";
		
		Query query = em.createQuery(strQuery);		
		query.setParameter("state", BlockEntityStateType.ACTIVATED.getCode());
		
		if(PersonType.NATURAL.getCode().equals(blockEntityTO.getPersonType())){
			query.setParameter("personType",blockEntityTO.getPersonType());
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getName()))
				query.setParameter("name", GeneralConstants.PERCENTAGE_STRING + blockEntityTO.getName() + GeneralConstants.PERCENTAGE_STRING);
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getFirstLastName()))
				query.setParameter("firstLastName", GeneralConstants.PERCENTAGE_STRING + blockEntityTO.getFirstLastName() + GeneralConstants.PERCENTAGE_STRING);
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getSecondLastName()))
				query.setParameter("secondLastName", GeneralConstants.PERCENTAGE_STRING + blockEntityTO.getSecondLastName() + GeneralConstants.PERCENTAGE_STRING);
		}else if(PersonType.JURIDIC.getCode().equals(blockEntityTO.getPersonType())){
			query.setParameter("personType",blockEntityTO.getPersonType());
			if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getFullName()))
				query.setParameter("fullName", GeneralConstants.PERCENTAGE_STRING + blockEntityTO.getFullName() + GeneralConstants.PERCENTAGE_STRING);
		}		
		
		if(Validations.validateIsNotNullAndNotEmpty(blockEntityTO.getIdHolderPk()))
			query.setParameter("idHolderPk", blockEntityTO.getIdHolderPk());	
		List<Object[]> lstRList = query.getResultList();
		return lstRList;
	}
	
	public List<BlockEntity> findBlockEntityByState(BlockEntityTO blockEntityTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" SELECT be FROM BlockEntity be ");
		sbQuery.append(" where 1 = 1");
		
		if(Validations.validateIsNotNull(blockEntityTO.getState())){
			sbQuery.append(" and be.stateBlockEntity=:state");			
		}
		
		Query query = em.createQuery(sbQuery.toString());
		
		if(Validations.validateIsNotNull(blockEntityTO.getState())){
			query.setParameter("state", blockEntityTO.getState());
		}
		
		List<BlockEntity> blockEntityList = (List<BlockEntity>) query.getResultList();
		return blockEntityList;
		
 	}

	@SuppressWarnings("unchecked")
	public List<BlockEntity> findBlockEntities(BlockEntitySearchTO blockEntitySearchTO) {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("	SELECT be.idBlockEntityPk,");
		sbQuery.append("			be.name,");
		sbQuery.append("			be.firstLastName,");
		sbQuery.append("			be.secondLastName,");
		sbQuery.append("			be.fullName,");
		sbQuery.append("			be.documentType,");
		sbQuery.append("			be.documentNumber,");
		sbQuery.append("			be.holder.idHolderPk,");
		sbQuery.append("			be.stateBlockEntity,");
		sbQuery.append("			be.personType");
		sbQuery.append("	FROM BlockEntity be");
		if(GeneralConstants.ZERO_VALUE_INTEGER.equals(blockEntitySearchTO.getSearchType())){
			sbQuery.append("	WHERE be.personType = :personType");
			if(PersonType.NATURAL.getCode().equals(blockEntitySearchTO.getPersonType())){
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getName()))
					sbQuery.append("	AND be.name like :name");
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getFirstLastName()))
					sbQuery.append("	AND be.firstLastName like :firstLastName");
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getSecondLastName()))
					sbQuery.append("	AND be.secondLastName like :secondLastName");
			}else if(PersonType.JURIDIC.getCode().equals(blockEntitySearchTO.getPersonType())){
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getFullName())){
					sbQuery.append("	AND be.fullName like :fullName");
				}
			}
		}else{
			sbQuery.append("	WHERE be.holder.idHolderPk = :idHolderPk");
		}
		sbQuery.append("	ORDER BY be.idBlockEntityPk DESC");
		Query query = em.createQuery(sbQuery.toString());
		if(GeneralConstants.ZERO_VALUE_INTEGER.equals(blockEntitySearchTO.getSearchType())){
			query.setParameter("personType",blockEntitySearchTO.getPersonType());
			if(PersonType.NATURAL.getCode().equals(blockEntitySearchTO.getPersonType())){				
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getName()))
					query.setParameter("name", GeneralConstants.PERCENTAGE_STRING +
												blockEntitySearchTO.getName() +
												GeneralConstants.PERCENTAGE_STRING);
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getFirstLastName()))
					query.setParameter("firstLastName", GeneralConstants.PERCENTAGE_STRING +
												blockEntitySearchTO.getFirstLastName() +
												GeneralConstants.PERCENTAGE_STRING);
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getSecondLastName()))
					query.setParameter("secondLastName", GeneralConstants.PERCENTAGE_STRING +
												blockEntitySearchTO.getSecondLastName() +
												GeneralConstants.PERCENTAGE_STRING);
			}else if(PersonType.JURIDIC.getCode().equals(blockEntitySearchTO.getPersonType())){
				if(Validations.validateIsNotNullAndNotEmpty(blockEntitySearchTO.getFullName())){
					query.setParameter("fullName", GeneralConstants.PERCENTAGE_STRING +	blockEntitySearchTO.getFullName() + GeneralConstants.PERCENTAGE_STRING);
				}
			}
		}else{
			query.setParameter("idHolderPk", blockEntitySearchTO.getIdHolderPk());
		}
		List<Object[]> lstResult = (List<Object[]>)query.getResultList();
		List<BlockEntity> lstBlockEntity = null;
		if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
			lstBlockEntity = new ArrayList<BlockEntity>();
			for(Object[] object:lstResult){
				BlockEntity blockEntity = new BlockEntity();
				blockEntity.setIdBlockEntityPk((Long)object[0]);
				blockEntity.setName(Validations.validateIsNotNullAndNotEmpty(object[1])?object[1].toString():null);
				blockEntity.setFirstLastName(Validations.validateIsNotNullAndNotEmpty(object[2])?object[2].toString():null);
				blockEntity.setSecondLastName(Validations.validateIsNotNullAndNotEmpty(object[3])?object[3].toString():null);
				blockEntity.setFullName(Validations.validateIsNotNullAndNotEmpty(object[4])?object[4].toString():null);
				blockEntity.setDocumentType((Integer)object[5]);
				blockEntity.setDocumentNumber(object[6].toString());
				if(Validations.validateIsNotNullAndNotEmpty(object[7]))
					blockEntity.setHolder(new Holder((Long)object[7]));
				else
					blockEntity.setHolder(new Holder());
				blockEntity.setStateBlockEntity((Integer)object[8]);
				blockEntity.setPersonType((Integer)object[9]);
				lstBlockEntity.add(blockEntity);
			}
		}
		return lstBlockEntity;
	}
	
	

}
