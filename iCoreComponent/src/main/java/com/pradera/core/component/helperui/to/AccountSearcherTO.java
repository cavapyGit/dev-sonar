package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.accounts.holderaccounts.HolderAccount;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountSearcherTO.
 * Transfer Object to accounts
 * @author PraderaTechnologies.
 * @version 1.0 , 04/02/2013
 */
public class AccountSearcherTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5919347311970971440L;
	
	/** The participant code. */
	private Long participantCode;
	
	//TMP
	/** The Cui Code */
	private Long idHolderPk;
	
	/** The document type. */
	private Integer documentType;
	
	/** The document number. */
	private String documentNumber;
	
	/** The name. */
	private String name;
	
	/** The firts last name. */
	private String firtsLastName;
	
	/** The second last name. */
	private String secondLastName;
	
	/** The company name. */
	private String companyName;
	
	/** The selected. */
	private AccountTO selected;
	
	/** The list accounts. */
	private List<AccountTO> listAccounts;
	
	//TMP
	/** Object HolderAccount Entity*/
	private HolderAccount holderAccountEntity;
	

	/**
	 * Instantiates a new account searcher to.
	 */
	public AccountSearcherTO() {
		// TODO Auto-generated constructor stub
	}




	/**
	 * Gets the participant code.
	 *
	 * @return the participant code
	 */
	public Long getParticipantCode() {
		return participantCode;
	}




	/**
	 * Sets the participant code.
	 *
	 * @param participantCode the new participant code
	 */
	public void setParticipantCode(Long participantCode) {
		this.participantCode = participantCode;
	}




	/**
	 * Gets the document type.
	 *
	 * @return the document type
	 */
	public Integer getDocumentType() {
		return documentType;
	}




	/**
	 * Sets the document type.
	 *
	 * @param documentType the new document type
	 */
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}




	/**
	 * Gets the document number.
	 *
	 * @return the document number
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}




	/**
	 * Sets the document number.
	 *
	 * @param documentNumber the new document number
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}




	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}




	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}




	/**
	 * Gets the firts last name.
	 *
	 * @return the firts last name
	 */
	public String getFirtsLastName() {
		return firtsLastName;
	}




	/**
	 * Sets the firts last name.
	 *
	 * @param firtsLastName the new firts last name
	 */
	public void setFirtsLastName(String firtsLastName) {
		this.firtsLastName = firtsLastName;
	}




	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return secondLastName;
	}




	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}




	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}




	/**
	 * Sets the company name.
	 *
	 * @param companyName the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}




	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public AccountTO getSelected() {
		return selected;
	}




	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(AccountTO selected) {
		this.selected = selected;
	}




	/**
	 * Gets the list accounts.
	 *
	 * @return the list accounts
	 */
	public List<AccountTO> getListAccounts() {
		return listAccounts;
	}

	/**
	 * Sets the list accounts.
	 *
	 * @param listAccounts the new list accounts
	 */
	public void setListAccounts(List<AccountTO> listAccounts) {
		this.listAccounts = listAccounts;
	}




	public Long getIdHolderPk() {
		return idHolderPk;
	}




	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	public HolderAccount getHolderAccountEntity() {
		return holderAccountEntity;
	}

	public void setHolderAccountEntity(HolderAccount holderAccountEntity) {
		this.holderAccountEntity = holderAccountEntity;
	}

	
}
