package com.pradera.core.component.helperui.extension.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.accounts.to.HolderAccountTO;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.AccountHelperInputTO;
import com.pradera.core.component.helperui.to.HolderAccountHelperResultTO;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.report.ReportFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class AccountExtensionHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Sep 24, 2013
 */
@ViewDepositaryBean
public class AccountExtensionHelperBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1700161330330682221L;

	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The User Info. */
	@Inject
	private transient UserInfo userInfo;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade 		helperComponentFacade;
	
	/** The general parameters facade. */
	@EJB
	GeneralParametersFacade     generalParametersFacade;
	
	/** The accounts facade. */
	@EJB
	AccountsFacade accountsFacade;
	
	/** The result list. */
	private List<HolderAccountHelperResultTO> resultList;
	
	/** The account helper input to. */
	private AccountHelperInputTO accountHelperInputTO;
	
	/** The list participants. */
	private List<SelectItem> listParticipants;
	
	/** The doc type list. */
	private List<ParameterTable> docTypeList;
	
	/** The account status list. */
	private List<ParameterTable> accountStatusList, lstAccountType;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The list empty. */
	private boolean listEmpty = false;
	
	/** The disabled participant. */
	private boolean disabledParticipant = false;
	
	private boolean renderedParticipant = false;
	
	/** The search filter. */
	private Integer searchFilter;

	private boolean disabledHolder = false;
	
	private Long idReport;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		accountHelperInputTO = new AccountHelperInputTO();
		//PARTICIPANTS
		listParticipants = new ArrayList<SelectItem>();
		searchFilter = GeneralConstants.NEGATIVE_ONE_INTEGER;
		fillComponents();
//		verifyUserType();
	}
	
	//FILL COMBOS
	/**
	 * Fill components.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillComponents() throws ServiceException{
		ParameterTableTO parameterTableTO = new ParameterTableTO();
		
		//DOCUMENT TYPES
		parameterTableTO.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		docTypeList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		//ACCOUNTS STATES
		parameterTableTO.setMasterTableFk(MasterTableType.HOLDER_ACCOUNT_STATUS.getCode());
		accountStatusList = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
		
		//ACCOUNT TYPES
		parameterTableTO.setMasterTableFk(MasterTableType.ACCOUNTS_TYPE.getCode());
		lstAccountType = generalParametersFacade.getListParameterTableServiceBean(parameterTableTO);
	}

	/**
	 * Fill participants.
	 *
	 * @throws ServiceException the service exception
	 */
	public void fillParticipants() throws ServiceException{
		Participant participant = new Participant();
		//PARTICIPANTS
		listParticipants = new ArrayList<SelectItem>();
		
		for(Participant part : helperComponentFacade.getLisParticipantServiceFacade(participant)) {
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDisplayDescriptionCode()));
		}
	}
	
	/**
	 * Verify user type.
	 *
	 * @throws ServiceException the service exception
	 */
	public void verifyUserType(Long idReport) throws ServiceException{
		//RESTARTING PARTICIPANT LIST
		listParticipants = new ArrayList<SelectItem>();
		ValueExpression valueExpressionPartCode = null;
		ValueExpression veHolderId = null;
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		idParticipantPk=null;
		accountHelperInputTO.setIdHolderPk(null);
		accountHelperInputTO.setLstHolderAccountPk(null);
		accountHelperInputTO.setUserIssuer(false);
		accountHelperInputTO.setUserIssuerDpf(false);
		renderedParticipant = true;
		//GETTING holderId VALUE
		veHolderId = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.idHolder}", Long.class);
		
		if(Validations.validateIsNotNullAndPositive((Long)veHolderId.getValue(elContext))){
			//SETTING GLOBAL HOLDER
			accountHelperInputTO.setIdHolderPk((Long)veHolderId.getValue(elContext));
		}
		
		if(userInfo.getUserAccountSession().isParticipantInstitucion()
				|| userInfo.getUserAccountSession().isParticipantInvestorInstitucion()){ //IF USER TYPE IS PARTICIPANT OR PARTICIPANT INVESTOR
			
			Participant part = new Participant();

			//DISABLED PARTICIPANT COMBO
			disabledParticipant = true;
			
			//GETTING THE PARTICIPANT BY CODE
			part = helperComponentFacade.findParticipantsByCode(userInfo.getUserAccountSession().getParticipantCode());
			
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion() && 
					Validations.validateIsNull(accountHelperInputTO.getIdHolderPk())){
				HolderAccountTO holderAccountTO = new HolderAccountTO();
				holderAccountTO.setParticipantTO(part.getIdParticipantPk());
				List<Long> lstHolderPk = new ArrayList<Long>();
				HolderSearchTO holderSearchTO = new HolderSearchTO();
				holderSearchTO.setDocumentType(part.getDocumentType());
				holderSearchTO.setDocumentNumber(part.getDocumentNumber());
				holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
				List<Holder> lstHolder = helperComponentFacade.findHolders(holderSearchTO);
				if(lstHolder!=null && lstHolder.size()>0){
					for(Holder objHolder : lstHolder){
						lstHolderPk.add(objHolder.getIdHolderPk());
					}
				}
				holderAccountTO.setLstHolderPk(lstHolderPk);
				List<HolderAccount> lstHolderAccount = null;
				if(holderAccountTO.getLstHolderPk()!=null && holderAccountTO.getLstHolderPk().size()>0){
					 lstHolderAccount = accountsFacade.getHolderAccountListComponentServiceFacade(holderAccountTO);
				}						
				List<Long> lstHolderAccountPk = new ArrayList<Long>();
				if(lstHolderAccount!=null && lstHolderAccount.size()>0){
					for(HolderAccount objHolderAccount : lstHolderAccount){
						lstHolderAccountPk.add(objHolderAccount.getIdHolderAccountPk());
					}
				}
				accountHelperInputTO.setLstHolderAccountPk(lstHolderAccountPk);
			}
			
			//SETTING GLOBAL PARTICIPANT
			idParticipantPk = part.getIdParticipantPk();
			
			//ADDING PARTICIPANT TO COMBO
			listParticipants.add(new SelectItem(part.getIdParticipantPk(), part.getDisplayDescriptionCode()));
			
			//SETTING FILTER TO SEARCH
			//accountHelperInputTO.setIdParticipantPK(idParticipantPk);
		}else{
			
			if(IsInstitutionTypeIssuerOrIssuerDPF() && 
					Validations.validateIsNull(accountHelperInputTO.getIdHolderPk())){
				accountHelperInputTO.setIssuerFk(userInfo.getUserAccountSession().getIssuerCode());		
				if(userInfo.getUserAccountSession().isIssuerInstitucion()){
					accountHelperInputTO.setUserIssuer(true);
				}
				if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()){
					accountHelperInputTO.setUserIssuerDpf(true);
				}
			}
			if(IsInstitutionTypeIssuerOrIssuerDPF()){
				renderedParticipant = false;
			}
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					&& userInfo.getUserAccountSession().getPartIssuerCode() != null
					&& generalParametersFacade.isReportInPartIssuerGroup(idReport)){
				idParticipantPk = userInfo.getUserAccountSession().getPartIssuerCode();
			}else{
				//GETTING partCode VALUE
				valueExpressionPartCode = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
				
				if(Validations.validateIsNotNullAndPositive((Long)valueExpressionPartCode.getValue(elContext))){
					//SETTING GLOBAL PARTICIPANT
					idParticipantPk = (Long)valueExpressionPartCode.getValue(elContext);
				}
			}
		}
		
		//FILL ALL PARTICIPANTS
		fillParticipants();
		
		if(idParticipantPk!=null || IsInstitutionTypeIssuerOrIssuerDPF()){
			disabledParticipant = true;
		}else{
			disabledParticipant = false;
			idParticipantPk=null;
		}
		if(Validations.validateIsNotNullAndPositive(accountHelperInputTO.getIdHolderPk())){
			disabledHolder = true;
		}else{
			disabledHolder = false;
			accountHelperInputTO.setIdHolderPk(null);
		}
	}

	public Boolean IsInstitutionTypeIssuerOrIssuerDPF(){
		Boolean condition = Boolean.FALSE;
		if(userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion())
			condition = Boolean.TRUE;
		return condition;
	}
	
	/**
	 * Open dialog.
	 *
	 * @throws ServiceException the service exception
	 */
	public void openDialog(Long idReport) throws ServiceException{
//		try {
//			FacesContext context = FacesContext.getCurrentInstance();
//			ELContext elContext = context.getELContext();
//			ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.idHolder}", Long.class);
//			Long cui = (Long) valueExpression.getValue(elContext);
//			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blHolder}", Boolean.class);
//			Boolean blHolder = (Boolean) valueExpression.getValue(elContext);
//			
//			if(Validations.validateIsNullOrEmpty(cui) && blHolder){
////				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
////												PropertiesUtilities.getGenericMessage("account.helper.alert.holder"));
////				JSFUtilities.showSimpleValidationDialog();
////				return;
//			}	
//			
//			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.partCode}", Long.class);
//			Long participant = (Long) valueExpression.getValue(elContext);
//			if(Validations.validateIsNullOrEmpty(participant) || GeneralConstants.ZERO_VALUE_LONG.equals(participant)){
////				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
////												PropertiesUtilities.getGenericMessage("account.helper.alert.participant"));
////				JSFUtilities.showSimpleValidationDialog();
////				return;
//			}				
//			accountHelperInputTO = new AccountHelperInputTO();
//			Holder h = null;
//			if(cui.intValue()>0){
//				accountHelperInputTO.setIdHolderPk(cui);
//				h = helperComponentFacade.findHolderByCode(cui);
//				accountHelperInputTO.setFullName(h.getFullName());
//			}
//			Participant participantFilter = helperComponentFacade.findParticipant(participant);
//			accountHelperInputTO.setIdParticipantPK(participantFilter.getIdParticipantPk());
////			RequestContext.getCurrentInstance().execute("dlgAccount#{cc.attrs.id}.show();");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		this.idReport = idReport;
		verifyUserType(this.idReport);
	}
	
	/**
	 * Search account by filter.
	 *
	 * @throws ServiceException the service exception
	 */
	public void searchAccountByFilter() throws ServiceException{
//		verifyUserType();
		
		//SETTING VALUE OF COMPONENT(PARTICIPANT PK)
		accountHelperInputTO.setIdParticipantPK(idParticipantPk);
		
		//VALIDATE IF ANY FIELD IS NULL
		if(Validations.validateIsNullOrNotPositive(accountHelperInputTO.getIdParticipantPK())
				&& !IsInstitutionTypeIssuerOrIssuerDPF()){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											 PropertiesUtilities.getGenericMessage("account.helper.alert.participant"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else{	
			resultList = helperComponentFacade.searchAccountExtensionByFilter(accountHelperInputTO);

			if(resultList.size() > GeneralConstants.ZERO_VALUE_INTEGER){
				listEmpty = false;
			}else{
				listEmpty = true;
			}
		}
		
		/*if(Validations.validateIsNullOrNotPositive(accountHelperInputTO.getIdHolderPk())){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
											 PropertiesUtilities.getGenericMessage("account.helper.alert.holder"));
			JSFUtilities.showSimpleValidationDialog();
			return;
		}else{	
			resultList = helperComponentFacade.searchAccountExtensionByFilter(accountHelperInputTO);

			if(resultList.size() > GeneralConstants.ZERO_VALUE_INTEGER){
				listEmpty = false;
			}else{
				listEmpty = true;
			}
		}*/
	}
	
	//clearing by option selected
		/**
	 * Clear by option.
	 */
	public void clearByOption(){
			switch(searchFilter){
				case 1:
					accountHelperInputTO.setDocTypeHolder(null);
					accountHelperInputTO.setDocNumberHolder(null);
					accountHelperInputTO.setName(null);
					accountHelperInputTO.setFirstLastName(null);
					accountHelperInputTO.setSecondLastName(null);
					accountHelperInputTO.setFullName(null);
					break;
				case 2:
					accountHelperInputTO.setIdHolderPk(null);
					accountHelperInputTO.setName(null);
					accountHelperInputTO.setFirstLastName(null);
					accountHelperInputTO.setSecondLastName(null);
					accountHelperInputTO.setFullName(null);
					break;
				case 3:
					accountHelperInputTO.setIdHolderPk(null);
					accountHelperInputTO.setDocTypeHolder(null);
					accountHelperInputTO.setDocNumberHolder(null);
					accountHelperInputTO.setFullName(null);
					break;
				case 4:
					accountHelperInputTO.setIdHolderPk(null);
					accountHelperInputTO.setDocTypeHolder(null);
					accountHelperInputTO.setDocNumberHolder(null);
					accountHelperInputTO.setName(null);
					accountHelperInputTO.setFirstLastName(null);
					accountHelperInputTO.setSecondLastName(null);
					break;
			}
		}	
		
		/**
	 * Assign account from helper.
	 *
	 * @param accountHelperFilter the account helper filter
	 * @throws ServiceException the service exception
	 */
	public void assignAccountFromHelper(HolderAccountHelperResultTO accountHelperFilter) throws ServiceException{
			FacesContext context = FacesContext.getCurrentInstance();
			ELContext elContext  = context.getELContext();
			
			//GETTING HOLDER OBJECT FROM HOLDER HELPER
			ValueExpression valueExpression = context.getApplication().getExpressionFactory().
											createValueExpression(elContext,"#{cc.attrs.account}", Object.class);
			//SET VALUE TO LOADREPORT(ACCOUNT OBJECT)
			valueExpression.setValue(elContext, accountHelperFilter);
			
			//update report filter
			ValueExpression valueExpressionReport = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.report}", ReportFilter.class);
			ReportFilter reportFilter =(ReportFilter)valueExpressionReport.getValue(elContext);
			if(reportFilter!=null) {
//				reportFilter.setValue(accountHelperFilter);
				HolderAccount ha = new HolderAccount();
				Participant p = new Participant();
				List<HolderAccount> lstHA= new ArrayList<HolderAccount>();
				ha.setAlternateCode(accountHelperFilter.getAlternateCode());
				ha.setSelected(true);
				ha.setAccountNumber(accountHelperFilter.getAccountNumber());
				ha.setStateAccountDescription(accountHelperFilter.getAccountStatusDescription());
			    //ha.setAccountType(accountHelperFilter.getAccountType());
				ha.setIdHolderAccountPk(accountHelperFilter.getAccountPk());
				p.setIdParticipantPk(accountHelperFilter.getParticipant().getIdParticipantPk());
				p.setMnemonic(accountHelperFilter.getParticipant().getMnemonic());
				p.setDescription(accountHelperFilter.getParticipant().getDescription());
				ha.setParticipant(p);
				if(IsInstitutionTypeIssuerOrIssuerDPF()){
					ha.setSelected(true);
				}
				lstHA.add(ha);
				reportFilter.setHolderAccounts(lstHA);
			}
			//update caller
			ValueExpression valueExpressionName = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.update}", String.class);
			String componentsId = (String)valueExpressionName.getValue(elContext);
			if(StringUtils.isNotBlank(componentsId)) {
				RequestContext.getCurrentInstance().update(CommonsUtilities.getListOfComponentsForUpdate(componentsId));
			}
			clear();
		}	
		
	//CLEAR BUTTON
	/**
	 * Clear.
	 */
	public void clear() throws ServiceException{
		resultList = null;
		searchFilter = GeneralConstants.NEGATIVE_ONE_INTEGER;
		listEmpty = false;
		idParticipantPk = null;
		accountHelperInputTO.setIdHolderPk(null);
		accountHelperInputTO.setIdParticipantPK(null);
		accountHelperInputTO.setAccountStatus(null);
		accountHelperInputTO.setAccountType(null);
		verifyUserType(this.idReport);
	}
	
	//GETTERS AND SETTERS
	/**
	 * Gets the helper component facade.
	 *
	 * @return the helper component facade
	 */
	public HelperComponentFacade getHelperComponentFacade() {
		return helperComponentFacade;
	}

	/**
	 * Sets the helper component facade.
	 *
	 * @param helperComponentFacade the new helper component facade
	 */
	public void setHelperComponentFacade(HelperComponentFacade helperComponentFacade) {
		this.helperComponentFacade = helperComponentFacade;
	}

	/**
	 * Gets the general parameters facade.
	 *
	 * @return the general parameters facade
	 */
	public GeneralParametersFacade getGeneralParametersFacade() {
		return generalParametersFacade;
	}

	/**
	 * Sets the general parameters facade.
	 *
	 * @param generalParametersFacade the new general parameters facade
	 */
	public void setGeneralParametersFacade(
			GeneralParametersFacade generalParametersFacade) {
		this.generalParametersFacade = generalParametersFacade;
	}

	/**
	 * Gets the result list.
	 *
	 * @return the result list
	 */
	public List<HolderAccountHelperResultTO> getResultList() {
		return resultList;
	}

	/**
	 * Sets the result list.
	 *
	 * @param resultList the new result list
	 */
	public void setResultList(List<HolderAccountHelperResultTO> resultList) {
		this.resultList = resultList;
	}

	/**
	 * Gets the account helper input to.
	 *
	 * @return the account helper input to
	 */
	public AccountHelperInputTO getAccountHelperInputTO() {
		return accountHelperInputTO;
	}

	/**
	 * Sets the account helper input to.
	 *
	 * @param accountHelperInputTO the new account helper input to
	 */
	public void setAccountHelperInputTO(AccountHelperInputTO accountHelperInputTO) {
		this.accountHelperInputTO = accountHelperInputTO;
	}

	/**
	 * Gets the list participants.
	 *
	 * @return the list participants
	 */
	public List<SelectItem> getListParticipants() {
		return listParticipants;
	}

	/**
	 * Sets the list participants.
	 *
	 * @param listParticipants the new list participants
	 */
	public void setListParticipants(List<SelectItem> listParticipants) {
		this.listParticipants = listParticipants;
	}

	/**
	 * Gets the doc type list.
	 *
	 * @return the doc type list
	 */
	public List<ParameterTable> getDocTypeList() {
		return docTypeList;
	}

	/**
	 * Sets the doc type list.
	 *
	 * @param docTypeList the new doc type list
	 */
	public void setDocTypeList(List<ParameterTable> docTypeList) {
		this.docTypeList = docTypeList;
	}

	/**
	 * Gets the account status list.
	 *
	 * @return the account status list
	 */
	public List<ParameterTable> getAccountStatusList() {
		return accountStatusList;
	}

	/**
	 * Sets the account status list.
	 *
	 * @param accountStatusList the new account status list
	 */
	public void setAccountStatusList(List<ParameterTable> accountStatusList) {
		this.accountStatusList = accountStatusList;
	}

	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}

	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}

	/**
	 * Checks if is list empty.
	 *
	 * @return true, if is list empty
	 */
	public boolean isListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * Checks if is disabled participant.
	 *
	 * @return true, if is disabled participant
	 */
	public boolean isDisabledParticipant() {
		return disabledParticipant;
	}

	/**
	 * Sets the disabled participant.
	 *
	 * @param disabledParticipant the new disabled participant
	 */
	public void setDisabledParticipant(boolean disabledParticipant) {
		this.disabledParticipant = disabledParticipant;
	}

	/**
	 * Gets the search filter.
	 *
	 * @return the search filter
	 */
	public Integer getSearchFilter() {
		return searchFilter;
	}

	/**
	 * Sets the search filter.
	 *
	 * @param searchFilter the new search filter
	 */
	public void setSearchFilter(Integer searchFilter) {
		this.searchFilter = searchFilter;
	}

	public List<ParameterTable> getLstAccountType() {
		return lstAccountType;
	}

	public void setLstAccountType(List<ParameterTable> lstAccountType) {
		this.lstAccountType = lstAccountType;
	}

	public boolean isDisabledHolder() {
		return disabledHolder;
	}

	public void setDisabledHolder(boolean disabledHolder) {
		this.disabledHolder = disabledHolder;
	}

	public boolean isRenderedParticipant() {
		return renderedParticipant;
	}

	public void setRenderedParticipant(boolean renderedParticipant) {
		this.renderedParticipant = renderedParticipant;
	}

	public Long getIdReport() {
		return idReport;
	}

	public void setIdReport(Long idReport) {
		this.idReport = idReport;
	}

	/**
	 * @return the maxResultsQueryHelper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * @param maxResultsQueryHelper the maxResultsQueryHelper to set
	 */
	public void setMaxResultsQueryHelper(Integer maxResultsQueryHelper) {
		this.maxResultsQueryHelper = maxResultsQueryHelper;
	}
	
	
	
}
