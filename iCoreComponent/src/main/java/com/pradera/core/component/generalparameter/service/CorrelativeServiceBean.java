package com.pradera.core.component.generalparameter.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.Correlative;
import com.pradera.model.generalparameter.type.CorrelativeType;


@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CorrelativeServiceBean extends CrudDaoServiceBean{
	
	@Inject
	private PraderaLogger log;
	

	
	public Correlative getCorrelativeServiceBean(Correlative correlativeFilter) throws ServiceException{
		CriteriaBuilder criteriaBuilder=em.getCriteriaBuilder();
		CriteriaQuery<Correlative> criteriaQuery=criteriaBuilder.createQuery(Correlative.class);
		Root<Correlative> correlativeRoot=criteriaQuery.from(Correlative.class);
		
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		criteriaQuery.select(correlativeRoot);
		
		criteriaParameters.add(criteriaBuilder.equal(correlativeRoot.<String>get("correlativeCd"), correlativeFilter.getCorrelativeCd()));
		
		if(correlativeFilter.getParticipant()!=null && null != correlativeFilter.getParticipant().getIdParticipantPk()){
			criteriaParameters.add(criteriaBuilder.equal(correlativeRoot.get("participant").get("idParticipantPk"), correlativeFilter.getParticipant().getIdParticipantPk()));
		}
		if(correlativeFilter.getIssuer()!=null && StringUtils.isNotEmpty(correlativeFilter.getIssuer().getIdIssuerPk())){
			criteriaParameters.add( criteriaBuilder.equal( correlativeRoot.get("issuer").get("idIssuerPk") , correlativeFilter.getIssuer().getIdIssuerPk()) );
		}
		if(null != correlativeFilter.getCorrelativeDate()){
		//	criteriaParameters.add( criteriaBuilder.equal(correlativeRoot.get("correlativeDate"), correlativeFilter.getCorrelativeDate()) );
			Calendar cal=Calendar.getInstance();
			cal.setTime(correlativeFilter.getCorrelativeDate());
			if(CorrelativeType.ISSUER_CODE_COR.getValue().equals(correlativeFilter.getCorrelativeCd())){
				criteriaParameters.add( criteriaBuilder.equal( criteriaBuilder.function("year", Integer.class, correlativeRoot.get("correlativeDate")) , cal.get(Calendar.YEAR)) );
			}
		}
		
		criteriaQuery.where(criteriaBuilder.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()]))); 
		Correlative correlativeResult=null;
		try {
			TypedQuery<Correlative> correlativeQuery = em.createQuery(criteriaQuery);
			correlativeResult = correlativeQuery.getSingleResult();
		} catch(NonUniqueResultException nuex){
			correlativeResult =null;
			log.info("Issuer have more codes same!");
		} catch(NoResultException nex){
			correlativeResult =null;
			log.info("Issuer not found");
		}
		return correlativeResult;
	}

}
