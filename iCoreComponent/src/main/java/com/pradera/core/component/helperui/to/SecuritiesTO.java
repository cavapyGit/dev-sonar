package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class SecurityTO.
 * Transfer Object to Secutiry Entity using in helper
 * @author PraderaTechnologies.
 * @version 1.0 , 18/01/2013
 */
public class SecuritiesTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1229032786660948360L;
	
	/** The security code. */
	private String securitiesCode;
	
	/** The security code only . */
	private String securitiesCodeOnly;
	
	private String securityClass;
	
	/** The serie. */
	private String serie;
	
	/** The code cfi. */
	private String codeCFI;
	
	/** The nmemonic. */
	private String mnemonic;
	
	/** The description. */
	private String description;
	
	/** The state desc. */
	private String stateDesc;
	
	/** The state. */
	private Integer state;
	
	/** The InstrumentType. */
	private Integer instrumentType;

	private String instrumentTypeDesc;
	
	/** Added by Martin Zarate Rafael 
	 * 
	 *  for Results of securities-acc helper
	 * */
	private BigDecimal totalBalance;
	
	private BigDecimal availableBalance;
	
	private String isinCode;
	private String securityType;
	
	private Integer issuanceForm;
	
	/**
	 * Instantiates a new security to.
	 */
	public SecuritiesTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the securities code.
	 *
	 * @return the securities code
	 */
	public String getSecuritiesCode() {
		return securitiesCode;
	}

	/**
	 * Sets the securities code.
	 *
	 * @param securitiesCode the new securities code
	 */
	public void setSecuritiesCode(String securitiesCode) {
		this.securitiesCode = securitiesCode;
	}

	/**
	 * Gets the serie.
	 *
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}

	/**
	 * Sets the serie.
	 *
	 * @param serie the new serie
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}

	/**
	 * Gets the code cfi.
	 *
	 * @return the code cfi
	 */
	public String getCodeCFI() {
		return codeCFI;
	}

	/**
	 * Sets the code cfi.
	 *
	 * @param codeCFI the new code cfi
	 */
	public void setCodeCFI(String codeCFI) {
		this.codeCFI = codeCFI;
	}
	
	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	public Integer getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getInstrumentTypeDesc() {
		return instrumentTypeDesc;
	}

	public void setInstrumentTypeDesc(String instrumentTypeDesc) {
		this.instrumentTypeDesc = instrumentTypeDesc;
	}

	public String getSecuritiesCodeOnly() {
		return securitiesCodeOnly;
	}

	public void setSecuritiesCodeOnly(String securitiesCodeOnly) {
		this.securitiesCodeOnly = securitiesCodeOnly;
	}

	public String getSecurityClass() {
		return securityClass;
	}

	public void setSecurityClass(String securityClass) {
		this.securityClass = securityClass;
	}

	public String getIsinCode() {
		return isinCode;
	}

	public void setIsinCode(String isinCode) {
		this.isinCode = isinCode;
	}

	public String getSecurityType() {
		return securityType;
	}

	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}

	public Integer getIssuanceForm() {
		return issuanceForm;
	}

	public void setIssuanceForm(Integer issuanceForm) {
		this.issuanceForm = issuanceForm;
	}
	
	
}
