package com.pradera.core.component.helperui.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.accounting.account.to.AccountingParameterTo;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounting.AccountingParameter;


@Stateless
public class ParameterAccountingQueryServiceBean extends CrudDaoServiceBean {
	
	@Inject
	private PraderaLogger log;
	
	@Inject
	@Configurable
	private Integer maxResultsQuery;
		
	public List<Object[]>  findParameterAccountingServiceBean(AccountingParameterTo accountingParameterTo){
		
		StringBuilder sb = new StringBuilder();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sb.append("select ap.idAccountingParameterPk ,  ");  // 0 
		sb.append(" ap.parameterType,  ");//1
		sb.append(" (select p.parameterName from ParameterTable p where p.parameterTablePk = ap.parameterType) , ");//2
		sb.append(" ap.nrCorrelative,  ap.parameterName, ap.description, ");//3 //4 //5 
		sb.append(" ap.status,  ");//6
		sb.append(" (select p.parameterName from ParameterTable p where p.parameterTablePk = ap.status)  ");//7
		sb.append(" from  AccountingParameter ap");
		sb.append(" where 1 = 1 ");
		
		if (Validations.validateIsNotNullAndPositive(accountingParameterTo.getParameterType())){
			sb.append(" and ap.parameterType like :parameterType ");
			parameters.put("parameterType", "%"+accountingParameterTo.getParameterType()+"%");
		}
		if (Validations.validateIsNotNullAndNotEmpty(accountingParameterTo.getParameterName())){
			sb.append(" and ap.parameterName like :shortName ");
			parameters.put("shortName", "%"+accountingParameterTo.getParameterName()+"%");
		}
		if (Validations.validateIsNotNullAndPositive(accountingParameterTo.getIdAccountingParameterPk())){
			sb.append(" and ap.idAccountingParameterPk = :idAccountingParameterPk ");
			parameters.put("idAccountingParameterPk", accountingParameterTo.getIdAccountingParameterPk());
		}
		
		
		sb.append(" order by ap.parameterName ");
		
		return findListByQueryString(sb.toString(),parameters);
		
	}
		
	public AccountingParameter findAccountingParameteByCode(Long idAccountingParameterPk){
		AccountingParameter accountingParameter=null;
		
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" select ap from AccountingParameter ap where ap.idAccountingParameterPk="+idAccountingParameterPk);
			Query query = em.createQuery(sb.toString());
			accountingParameter = (AccountingParameter)query.getSingleResult();
		}catch(NoResultException nex) {
			log.error("No results found for  code " + idAccountingParameterPk);
			accountingParameter = null;
		}
		
		return accountingParameter;
	}
}
