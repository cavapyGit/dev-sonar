package com.pradera.core.component.generalparameter.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.generalparameter.GeographicLocation;
import com.pradera.model.generalparameter.Holiday;
import com.pradera.model.generalparameter.type.HolidayStateType;
import com.pradera.model.negotiation.constant.NegotiationConstant;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolidayQueryServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01/10/2015
 */
@Stateless
public class HolidayQueryServiceBean extends CrudDaoServiceBean{
	
	
	
	/**
	 * Gets the list holiday date service bean.
	 *
	 * @param filter the filter
	 * @return the list holiday date service bean
	 */
	public List<Date> getListHolidayDateServiceBean(Holiday filter) {
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("Select H.holidayDate From Holiday H ");
		sbQuery.append(" Where H.state = :statePrm ");		

		if(Validations.validateIsNotNull(filter.getHolidayDate())){
			sbQuery.append(" And H.holidayDate = :holidayDate ");
		}
		
		if(Validations.validateIsNotNull(filter.getGeographicLocation()) && 
				Validations.validateIsNotNullAndPositive(filter.getGeographicLocation().getIdGeographicLocationPk())){
			sbQuery.append(" And H.geographicLocation.idGeographicLocationPk = :countryPrm");
		}
				
    	TypedQuery<Date> query = em.createQuery(sbQuery.toString(),Date.class);  

        query.setParameter("statePrm", filter.getState());
        
        if(Validations.validateIsNotNull(filter.getHolidayDate())){
        	query.setParameter("holidayDate", filter.getHolidayDate());
		}
        
        if(Validations.validateIsNotNull(filter.getGeographicLocation()) && 
				Validations.validateIsNotNullAndPositive(filter.getGeographicLocation().getIdGeographicLocationPk())){
        	query.setParameter("countryPrm", filter.getGeographicLocation().getIdGeographicLocationPk());
		}
		
		return (List<Date>) query.getResultList();
	}
	
	/**
	 * Validates if the entered date is holiday or is sat or sun.
	 *
	 * @param date the date
	 * @param valSatSun the val sat sun
	 * @return true, if is non working day service bean
	 */
	public boolean isNonWorkingDayServiceBean(Date date, boolean valSatSun) {
		if(valSatSun && !CommonsUtilities.isUtilDate(date)){
			return true;
		}
		Holiday holidayFilter = new Holiday();
		holidayFilter.setHolidayDate(date);
		holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
		List<Date> res = getListHolidayDateServiceBean(holidayFilter);
		if(res.size()>0){
			return true;
		}
		return false;
	}
	
	/**
	 * Calculates a date adding or subtracting days without considering Sat, Sun and Holidays.
	 *
	 * @param initialDate Initial Date
	 * @param days Days to add or substract to Initial Date
	 * @param addOrSub Add action (1) or subtract action (0)
	 * @param calendar 		  1: Calendar Days (final day considering weekdays-holidays), 
	 * 		  2: Working Days (final day without considering weekdays-holidays), 
	 * 		  3: Working Days Passby Weekdays (if final day ends in weekdays-holidays, return next working day)
	 * @return the calculated final Date
	 */
	public Date getCalculateDate(Date initialDate, Integer days, Integer addOrSub, Integer calendar){
		
		if(calendar==null){
			// if null, defaults to working days
			calendar = NegotiationConstant.WORKING_DAYS;
		}
		
		Date finalDate = null;
		
		if(Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(days) &&
				(addOrSub.intValue() == 0 || addOrSub.intValue() == 1 )){
			StringBuilder sql = new StringBuilder("select FN_CALCULATE_DATE(?,?,?,?,?) from dual");
			Query query = em.createNativeQuery(sql.toString());
			query.setParameter(1, initialDate);
			query.setParameter(2, days);
			query.setParameter(3, addOrSub);
			query.setParameter(4, HolidayStateType.REGISTERED.getCode());
			query.setParameter(5, calendar);
			finalDate = (Date) query.getSingleResult();
		}
		
		return finalDate;
	}
	
	/**
	 * Gets the last no working day between dates.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 * @return the last no working day between dates
	 */
	public Date getLastWorkingDayBetweenDates(Date initialDate, Date finalDate){
		Date evaluateDate = finalDate;
		do{
			Calendar date = Calendar.getInstance();
			date.setTime(evaluateDate);
			if(!CommonsUtilities.isWeekend(date) && !isNonWorkingDayServiceBean(evaluateDate, Boolean.FALSE)){
				return evaluateDate;
			}
			evaluateDate = CommonsUtilities.addDaysToDate(evaluateDate, -1);
		}while(initialDate.before(finalDate));
		return null;
	}
	
	/**
	 * Gets the next holidate date.
	 *
	 * @param initialDate the initial date
	 * @return the next holidate date
	 */
	public Date getNextHolidateDate(Date initialDate){
		Date evaluateDate = CommonsUtilities.addDaysToDate(initialDate, +1);
		while(!isNonWorkingDayServiceBean(evaluateDate, Boolean.TRUE)){
			evaluateDate = CommonsUtilities.addDaysToDate(evaluateDate, +1);
		}
		return evaluateDate;
	}

	/**
	 * Gets the non working days between dates.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 * @param country the country
	 * @return the non working days between dates
	 */
	public Integer getNonWorkingDaysBetweenDates(Date initialDate,
			Date finalDate, Integer country) {
		return getDaysBetweenDates(initialDate, finalDate, country, true);
	}
	
	/**
	 * Gets the days between dates.
	 *
	 * @param initialDate the initial date
	 * @param finalDate the final date
	 * @param country the country
	 * @param nonWorkingDays the non working days
	 * @return the days between dates
	 */
	public Integer getDaysBetweenDates(Date initialDate,
			Date finalDate, Integer country, boolean nonWorkingDays) {
		
	   Holiday holidayFilter = new Holiday();
	   holidayFilter.setState(HolidayStateType.REGISTERED.getCode());
	   GeographicLocation countryFilter = new GeographicLocation();
	   countryFilter.setIdGeographicLocationPk(country);
	   holidayFilter.setGeographicLocation(countryFilter);
	   String allHolidays = CommonsUtilities.convertListDateToString(getListHolidayDateServiceBean(holidayFilter));

	   Calendar dateInstance = Calendar.getInstance();
	   dateInstance.setTime(initialDate);
	   Integer daysBetween = CommonsUtilities.getDaysBetween(initialDate, finalDate);
	   int tmpDays = daysBetween.intValue();
	   int nwDays = 0;
	   while (tmpDays > 0) {
			dateInstance.add(Calendar.DATE, 1);
			if (CommonsUtilities.isWeekend(dateInstance)) {
				nwDays++;
			} else {
				String date = CommonsUtilities.convertDatetoString(dateInstance.getTime());
				if(allHolidays.indexOf(date) >= 0){
					nwDays++;
				}
			}
			tmpDays--;
	  }
	  if(nonWorkingDays){
		  return nwDays;
	  }else{
		  return daysBetween - nwDays;
	  }
	}
	
	/**
	 * Checks if is holiday date service bean.
	 *
	 * @param date the date
	 * @return true, if is holiday date service bean
	 * @throws ServiceException the service exception
	 */
	public boolean isHolidayDateServiceBean(Date date) throws ServiceException{
		StringBuilder sbQuery=new StringBuilder();
		sbQuery.append("Select count(H) From Holiday H where H.holidayDate=:holyDate and H.state=:statePrm");
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("holyDate", date);
        query.setParameter("statePrm", HolidayStateType.REGISTERED.getCode());
        return Integer.parseInt(  query.getSingleResult().toString()  )>0;
	}
	
	
	/**
	 * Gets the calculate date service bean.
	 *
	 * @param initialDate the initial date
	 * @param days the days
	 * @param addOrSub the add or sub
	 * @param calendar the calendar
	 * @return the calculate date service bean
	 */
	public Date getCalculateDateServiceBean(Date initialDate, Integer days, Integer addOrSub, Integer calendar) {
		if(calendar == null){
			// if null, defaults to working days
			calendar = NegotiationConstant.WORKING_DAYS;
		}		
		Integer counter = 1;
		Date newDate = null;		
		Integer dayNumber = null;		
		Date dateHoliday = null;		
		Integer newDays = null;
		
		if(Validations.validateIsNotNull(initialDate) && Validations.validateIsNotNull(days) &&
				(addOrSub.intValue() == 0 || addOrSub.intValue() == 1 )) {
			newDate = initialDate;
			newDays = days;
			
			 while (counter.intValue() <= newDays.intValue()) {
				if(addOrSub.intValue() == 0) {
					newDate = CommonsUtilities.addDate(newDate, -1);
				} else if (addOrSub.intValue() == 1) {
					newDate = CommonsUtilities.addDate(newDate, 1);
				}				
				dayNumber = CommonsUtilities.getDayOfWeek(newDate);				
				if(calendar.intValue() == 2){
					counter = counter + 1;
					continue;
				}				
				dateHoliday = getDateHolidayServiceBean(newDate, HolidayStateType.REGISTERED.getCode());				
				if(calendar.intValue() == 1) {
					if(dayNumber.intValue() >= 2 &&  dayNumber.intValue() <= 6 && dateHoliday == null) {
						counter = counter + 1;
					}
				} else if (calendar.intValue() == 3) {
					counter = counter + 1;
					if((dayNumber.intValue() < 2 || dayNumber.intValue() > 6 || dateHoliday != null) && counter.intValue() == (newDays.intValue() + 1)) {
						newDays = newDays + 1;
					}
				}
			}	
		}		
		if(newDate != null){
			newDate = CommonsUtilities.formatDate(newDate);
		}
		return newDate;
	}
	
	/**
	 * Gets the date holiday service bean.
	 *
	 * @param newDate the new date
	 * @return the date holiday service bean
	 */
	public Date getDateHolidayServiceBean(Date newDate, Integer state) {
		StringBuffer sbQuery = new StringBuffer();
		Map<String, Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select distinct holidayDate from Holiday h ");		
		sbQuery.append(" where to_char(h.holidayDate, 'd') != 1 ");
		sbQuery.append(" and to_char(h.holidayDate, 'd') != 7 ");
		sbQuery.append(" and h.state = :parSate  ");
		sbQuery.append(" and TRUNC(h.holidayDate) = :parNewDate  ");
		parameters.put("parNewDate", newDate);
		parameters.put("parSate", state);
		Date dateHoliday = null;
		try {
			dateHoliday = (Date) findObjectByQueryString(sbQuery.toString(), parameters);
		} catch (NoResultException e) {
			return null;
		}
		return dateHoliday;
	}
	/**
 	 * Restar dias habiles
 	 * */
 	public Date subtractBusinessDays(Date objDate, int daysSubt){
 		boolean sw = true;
 		Date dateCalculate = null;
 		do {
 			try {
 				DateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
 				Calendar calCurrentDate = Calendar.getInstance();
 				calCurrentDate.setTime(objDate);
 				calCurrentDate.add(Calendar.DATE,-daysSubt);
 				String strCurrentDate = dfDate.format(calCurrentDate.getTime());
 				dateCalculate = dfDate.parse(strCurrentDate);
 				if (isNonWorkingDayServiceBean(dateCalculate,CommonsUtilities.isWeekend(calCurrentDate))) {
 					sw = true;
 					daysSubt++;
 				}else{
 					sw=false;
 				}
 			 }
 			 catch (ParseException e) {
 				e.printStackTrace();
 			}
 		} while (sw);
 		return dateCalculate;
 	}	
 	/** method to obtain the next working day from the date sent as parameter
     * 
      * @param dateParameter
     * @return */
     public Date getNextWorkingDayServiceBean(Date dateParameter) {
	        boolean checkingNextDay;
	        do {
	            checkingNextDay = false;
	            dateParameter = CommonsUtilities.addDate(dateParameter, 1);
	            Calendar cal = Calendar.getInstance();
	            cal.setTime(dateParameter);
	
	        if (isNonWorkingDayServiceBean(dateParameter, CommonsUtilities.isWeekend(cal))) 
	            checkingNextDay = true;
	        } while (checkingNextDay);
	        return dateParameter;
     }
}
