package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AccountingAccountTo implements Serializable, Comparable<AccountingAccountTo> {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	private Long 	idAccountingAccountPk;
	private Long 	idAccountingAccountFk;
	private String  accountCode;
	private String  description;
	private Integer accountType; 
	private Integer natureType;
	private Integer currency;
	private Integer instrumentType;
	private Integer indAccumulation;
	private Integer indBalanceControl; 
	private Integer	indSubAccount;
	private Integer indDimension;
	private Integer adjustmentType;
	private Integer auxiliaryType;
	private Integer status;
	private Integer indEntityAccount;
	private Integer entityAccount;
	private Integer accountingType;
	private Date    registerDate;
	private String  registerUser;
	private Date    updateDate;
	private String  updateUser;
	private Date    cancelDate;
	private String  cancelUser;
	private Integer lastModifyApp;
	private Date    lastModifyDate;
	private String  lastModifyIp;
	private String  lastModifyUser;
	private Integer branch;
	private Integer indExpirationDate;
	private String 	descriptionTypeAccount;
	private String 	descriptionCurrency;
	private String 	descriptionBranchAccount;
	private String	descriptionTypeOfAdjustment;
	private String 	descriptionTypeAuxiliary;
	private String 	descriptionStateAccount;
	private String 	descriptionAccountingType;
	private String 	descriptionAuxiliaryType;
	private String 	descriptionDynamicType;
	private String 	descriptionEntityAccount;
	private String  fatherCode;

	private BigDecimal amount;
	
	private BigDecimal amountAssets;
	private BigDecimal amountDebit;
	/**Indicador si valor viene de BD**/
	private Integer 	valueDB;
	public AccountingAccountTo(){
		
	}




	/**
	 * @return the idAccountingAccountPk
	 */
	public Long getIdAccountingAccountPk() {
		return idAccountingAccountPk;
	}




	/**
	 * @param idAccountingAccountPk the idAccountingAccountPk to set
	 */
	public void setIdAccountingAccountPk(Long idAccountingAccountPk) {
		this.idAccountingAccountPk = idAccountingAccountPk;
	}




	/**
	 * @return the idAccountingAccountFk
	 */
	public Long getIdAccountingAccountFk() {
		return idAccountingAccountFk;
	}




	/**
	 * @param idAccountingAccountFk the idAccountingAccountFk to set
	 */
	public void setIdAccountingAccountFk(Long idAccountingAccountFk) {
		this.idAccountingAccountFk = idAccountingAccountFk;
	}




	/**
	 * @return the accountCode
	 */
	public String getAccountCode() {
		return accountCode;
	}




	/**
	 * @param accountCode the accountCode to set
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}




	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}




	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}




	/**
	 * @return the accountType
	 */
	public Integer getAccountType() {
		return accountType;
	}




	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}




	/**
	 * @return the natureType
	 */
	public Integer getNatureType() {
		return natureType;
	}




	/**
	 * @param natureType the natureType to set
	 */
	public void setNatureType(Integer natureType) {
		this.natureType = natureType;
	}




	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}




	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}




	/**
	 * @return the indAccumulation
	 */
	public Integer getIndAccumulation() {
		return indAccumulation;
	}




	/**
	 * @param indAccumulation the indAccumulation to set
	 */
	public void setIndAccumulation(Integer indAccumulation) {
		this.indAccumulation = indAccumulation;
	}




	/**
	 * @return the indBalanceControl
	 */
	public Integer getIndBalanceControl() {
		return indBalanceControl;
	}




	/**
	 * @param indBalanceControl the indBalanceControl to set
	 */
	public void setIndBalanceControl(Integer indBalanceControl) {
		this.indBalanceControl = indBalanceControl;
	}


 



	public Integer getIndSubAccount() {
		return indSubAccount;
	}




	public void setIndSubAccount(Integer indSubAccount) {
		this.indSubAccount = indSubAccount;
	}




	/**
	 * @return the indDimension
	 */
	public Integer getIndDimension() {
		return indDimension;
	}




	/**
	 * @param indDimension the indDimension to set
	 */
	public void setIndDimension(Integer indDimension) {
		this.indDimension = indDimension;
	}




	/**
	 * @return the adjustmentType
	 */
	public Integer getAdjustmentType() {
		return adjustmentType;
	}




	/**
	 * @param adjustmentType the adjustmentType to set
	 */
	public void setAdjustmentType(Integer adjustmentType) {
		this.adjustmentType = adjustmentType;
	}




	/**
	 * @return the auxiliaryType
	 */
	public Integer getAuxiliaryType() {
		return auxiliaryType;
	}




	/**
	 * @param auxiliaryType the auxiliaryType to set
	 */
	public void setAuxiliaryType(Integer auxiliaryType) {
		this.auxiliaryType = auxiliaryType;
	}




	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}




	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}




	/**
	 * @return the accountingType
	 */
	public Integer getAccountingType() {
		return accountingType;
	}




	/**
	 * @param accountingType the accountingType to set
	 */
	public void setAccountingType(Integer accountingType) {
		this.accountingType = accountingType;
	}




	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}




	/**
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}




	/**
	 * @return the registerUser
	 */
	public String getRegisterUser() {
		return registerUser;
	}




	/**
	 * @param registerUser the registerUser to set
	 */
	public void setRegisterUser(String registerUser) {
		this.registerUser = registerUser;
	}




	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}




	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}




	/**
	 * @return the updateUser
	 */
	public String getUpdateUser() {
		return updateUser;
	}




	/**
	 * @param updateUser the updateUser to set
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}




	/**
	 * @return the cancelDate
	 */
	public Date getCancelDate() {
		return cancelDate;
	}




	/**
	 * @param cancelDate the cancelDate to set
	 */
	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}




	/**
	 * @return the cancelUser
	 */
	public String getCancelUser() {
		return cancelUser;
	}




	/**
	 * @param cancelUser the cancelUser to set
	 */
	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}




	/**
	 * @return the lastModifyApp
	 */
	public Integer getLastModifyApp() {
		return lastModifyApp;
	}




	/**
	 * @param lastModifyApp the lastModifyApp to set
	 */
	public void setLastModifyApp(Integer lastModifyApp) {
		this.lastModifyApp = lastModifyApp;
	}




	/**
	 * @return the lastModifyDate
	 */
	public Date getLastModifyDate() {
		return lastModifyDate;
	}




	/**
	 * @param lastModifyDate the lastModifyDate to set
	 */
	public void setLastModifyDate(Date lastModifyDate) {
		this.lastModifyDate = lastModifyDate;
	}




	/**
	 * @return the lastModifyIp
	 */
	public String getLastModifyIp() {
		return lastModifyIp;
	}




	/**
	 * @param lastModifyIp the lastModifyIp to set
	 */
	public void setLastModifyIp(String lastModifyIp) {
		this.lastModifyIp = lastModifyIp;
	}




	/**
	 * @return the lastModifyUser
	 */
	public String getLastModifyUser() {
		return lastModifyUser;
	}




	/**
	 * @param lastModifyUser the lastModifyUser to set
	 */
	public void setLastModifyUser(String lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}




	/**
	 * @return the branch
	 */
	public Integer getBranch() {
		return branch;
	}




	/**
	 * @param branch the branch to set
	 */
	public void setBranch(Integer branch) {
		this.branch = branch;
	}




	/**
	 * @return the indExpirationDate
	 */
	public Integer getIndExpirationDate() {
		return indExpirationDate;
	}




	/**
	 * @param indExpirationDate the indExpirationDate to set
	 */
	public void setIndExpirationDate(Integer indExpirationDate) {
		this.indExpirationDate = indExpirationDate;
	}




	/**
	 * @return the descriptionTypeAccount
	 */
	public String getDescriptionTypeAccount() {
		return descriptionTypeAccount;
	}




	/**
	 * @param descriptionTypeAccount the descriptionTypeAccount to set
	 */
	public void setDescriptionTypeAccount(String descriptionTypeAccount) {
		this.descriptionTypeAccount = descriptionTypeAccount;
	}




	/**
	 * @return the descriptionCurrency
	 */
	public String getDescriptionCurrency() {
		return descriptionCurrency;
	}




	/**
	 * @param descriptionCurrency the descriptionCurrency to set
	 */
	public void setDescriptionCurrency(String descriptionCurrency) {
		this.descriptionCurrency = descriptionCurrency;
	}




	/**
	 * @return the descriptionBranchAccount
	 */
	public String getDescriptionBranchAccount() {
		return descriptionBranchAccount;
	}




	/**
	 * @param descriptionBranchAccount the descriptionBranchAccount to set
	 */
	public void setDescriptionBranchAccount(String descriptionBranchAccount) {
		this.descriptionBranchAccount = descriptionBranchAccount;
	}




	/**
	 * @return the descriptionTypeOfAdjustment
	 */
	public String getDescriptionTypeOfAdjustment() {
		return descriptionTypeOfAdjustment;
	}




	/**
	 * @param descriptionTypeOfAdjustment the descriptionTypeOfAdjustment to set
	 */
	public void setDescriptionTypeOfAdjustment(String descriptionTypeOfAdjustment) {
		this.descriptionTypeOfAdjustment = descriptionTypeOfAdjustment;
	}




	/**
	 * @return the descriptionTypeAuxiliary
	 */
	public String getDescriptionTypeAuxiliary() {
		return descriptionTypeAuxiliary;
	}




	/**
	 * @param descriptionTypeAuxiliary the descriptionTypeAuxiliary to set
	 */
	public void setDescriptionTypeAuxiliary(String descriptionTypeAuxiliary) {
		this.descriptionTypeAuxiliary = descriptionTypeAuxiliary;
	}




	/**
	 * @return the descriptionStateAccount
	 */
	public String getDescriptionStateAccount() {
		return descriptionStateAccount;
	}




	/**
	 * @param descriptionStateAccount the descriptionStateAccount to set
	 */
	public void setDescriptionStateAccount(String descriptionStateAccount) {
		this.descriptionStateAccount = descriptionStateAccount;
	}




	/**
	 * @return the descriptionAccountingType
	 */
	public String getDescriptionAccountingType() {
		return descriptionAccountingType;
	}




	/**
	 * @param descriptionAccountingType the descriptionAccountingType to set
	 */
	public void setDescriptionAccountingType(String descriptionAccountingType) {
		this.descriptionAccountingType = descriptionAccountingType;
	}




	public String getFatherCode() {
		return fatherCode;
	}




	public void setFatherCode(String fatherCode) {
		this.fatherCode = fatherCode;
	}




	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}




	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
 


	/**
	 * @return the descriptionAuxiliaryType
	 */
	public String getDescriptionAuxiliaryType() {
		return descriptionAuxiliaryType;
	}




	/**
	 * @param descriptionAuxiliaryType the descriptionAuxiliaryType to set
	 */
	public void setDescriptionAuxiliaryType(String descriptionAuxiliaryType) {
		this.descriptionAuxiliaryType = descriptionAuxiliaryType;
	}




	/**
	 * @return the amountAssets
	 */
	public BigDecimal getAmountAssets() {
		return amountAssets;
	}




	/**
	 * @param amountAssets the amountAssets to set
	 */
	public void setAmountAssets(BigDecimal amountAssets) {
		this.amountAssets = amountAssets;
	}




	/**
	 * @return the amountDebit
	 */
	public BigDecimal getAmountDebit() {
		return amountDebit;
	}




	/**
	 * @param amountDebit the amountDebit to set
	 */
	public void setAmountDebit(BigDecimal amountDebit) {
		this.amountDebit = amountDebit;
	}




	/**
	 * @return the descriptionDynamicType
	 */
	public String getDescriptionDynamicType() {
		return descriptionDynamicType;
	}




	/**
	 * @param descriptionDynamicType the descriptionDynamicType to set
	 */
	public void setDescriptionDynamicType(String descriptionDynamicType) {
		this.descriptionDynamicType = descriptionDynamicType;
	}



 




	/**
	 * @return the valueDB
	 */
	public Integer getValueDB() {
		return valueDB;
	}




	/**
	 * @param valueDB the valueDB to set
	 */
	public void setValueDB(Integer valueDB) {
		this.valueDB = valueDB;
	}




	/**
	 * @return the indEntityAccount
	 */
	public Integer getIndEntityAccount() {
		return indEntityAccount;
	}




	/**
	 * @param indEntityAccount the indEntityAccount to set
	 */
	public void setIndEntityAccount(Integer indEntityAccount) {
		this.indEntityAccount = indEntityAccount;
	}




	/**
	 * @return the entityAccount
	 */
	public Integer getEntityAccount() {
		return entityAccount;
	}




	/**
	 * @param entityAccount the entityAccount to set
	 */
	public void setEntityAccount(Integer entityAccount) {
		this.entityAccount = entityAccount;
	}




	/**
	 * @return the descriptionEntityAccount
	 */
	public String getDescriptionEntityAccount() {
		return descriptionEntityAccount;
	}


	/**
	 * @param descriptionEntityAccount the descriptionEntityAccount to set
	 */
	public void setDescriptionEntityAccount(String descriptionEntityAccount) {
		this.descriptionEntityAccount = descriptionEntityAccount;
	}


	/**
	 * @return the instrumentType
	 */
	public Integer getInstrumentType() {
		return instrumentType;
	}




	/**
	 * @param instrumentType the instrumentType to set
	 */
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}




	public int compareTo(AccountingAccountTo o) {
		AccountingAccountTo accountingAccountTo= (AccountingAccountTo)o;
		String accountCode=accountingAccountTo.getAccountCode();
		String accountThis=this.getAccountCode();
		
		return accountThis.compareTo(accountCode);
	}





	
	
}
