package com.pradera.core.component.helperui.view; 

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;

import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.ui.UICompositeEvent;
import com.pradera.commons.view.ui.UICompositeType;
import com.pradera.core.component.helperui.service.MarketFactBalanceServiceBean;
import com.pradera.core.component.helperui.to.MarketFactBalanceHelpTO;
import com.pradera.core.component.helperui.to.MarketFactDetailHelpTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.exception.ServiceException;



/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class MarketFactBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 01-sep-2015
 */
@ApplicationScoped
@Named
public class MarketFactBean implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5036009796727734519L;
	
	/** The general parameter facade. */
	@EJB 
	private MarketFactBalanceServiceBean marketServiceBean;
	
	/** The market fact balance. */
	private MarketFactBalanceHelpTO marketFactBalance;
	
	/** The key data. */
	public static Map<String,MarketFactBalanceHelpTO> keyData = new HashMap<String, MarketFactBalanceHelpTO>();
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	private boolean checkAllItems,tranferAllItems;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		checkAllItems = false;
		tranferAllItems = false;
	}
	
	/**
	 * Receive event component.
	 *
	 * @param marketFactBalanceTO the market fact balance to
	 */
	public void receiveEventComponent(@Observes @UICompositeEvent(UICompositeType.MARKETFACT_BALANCE) Object marketFactBalanceTO) {
		MarketFactBalanceHelpTO marketFactBalance = null;
		if(marketFactBalanceTO instanceof MarketFactBalanceHelpTO){
			marketFactBalance = (MarketFactBalanceHelpTO)marketFactBalanceTO;
		}else{
			marketFactBalance = new MarketFactBalanceHelpTO();
		}
		 
		try {
			if(marketFactBalance.getSecurityCodePk()!=null){
				MarketFactBalanceHelpTO marketBalanceFinded=null;
				
				/*ATRIBUTO PARA MANEJAR SI HECHOS DE MERCADO SON LLENADAS ANTES DE USAR EL COMPONENTE*/
				if(marketFactBalance.getIndHandleDetails()!=null && marketFactBalance.getIndHandleDetails().equals(BooleanType.YES.getCode())){
					marketBalanceFinded = marketFactBalance;
				}else{
					marketBalanceFinded = marketServiceBean.findBalanceWithMarketFact(marketFactBalance);
				}
				/*si fuese un solo hecho de mercado*/
				if(marketBalanceFinded.getMarketFacBalances().size()==1)
					checkAllItems = marketBalanceFinded.getMarketFacBalances().get(0).getIsSelected();
				else{
					List<MarketFactDetailHelpTO> marketFacBalances = marketBalanceFinded.getMarketFacBalances();
					for (MarketFactDetailHelpTO marketFactDetailHelpTO : marketFacBalances) {
						marketFactDetailHelpTO.setIsSelected(false);
						checkAllItems = false;
					}
				}
				tranferAllItems = false;
				keyData.put(userInfo.getUserAccountSession().getUserName().concat(marketFactBalance.getUiComponentName()), marketBalanceFinded);
				showDialog(marketFactBalance.getUiComponentName());
			}
		} catch (ServiceException e) {
			switch(e.getErrorService()){
				case INCONSISTENCY_DATA: case BALANCE_WITHOUT_MARKETFACT: case MARKETFACT_UI_ERROR:
					JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
													 PropertiesUtilities.getExceptionMessage(e.getMessage()));
					JSFUtilities.showSimpleValidationDialog();break;
			default:
				break;
			}
		}
	}
	
	/**
	 * Validate amount entered.
	 *
	 * @param uiComponentName the ui component name
	 * @param marketDet the market det
	 * @param availabLabel the availab label
	 * @param blockLabel the block label
	 * @param totalLabel the total label
	 * @param loanableOperation the loanable operation
	 */
	public boolean validateAmountEntered(String uiComponentName, MarketFactDetailHelpTO marketDet, String availabLabel, String blockLabel, String totalLabel,
																								String loanableOperation){
		BigDecimal balanceEntered = marketDet.getEnteredBalance();
		Boolean existsError = Boolean.FALSE;
		String msgs = "";
		
		if(balanceEntered!=null && !balanceEntered.equals(BigDecimal.ZERO)){
			if(marketDet.getMarketFactBalanceTO().getIndBlockBalance()!=null){
				if(balanceEntered.compareTo(marketDet.getQuantityBlockDocument())>0){
					msgs = PropertiesUtilities.getValidationMessage("error.greater.market.fact", new Object[]{blockLabel});
					existsError = Boolean.TRUE;
				}
			}else if(marketDet.getMarketFactBalanceTO().getIndTotalBalance()!=null){
				if(balanceEntered.compareTo(marketDet.getTotalBalance())>0){
					msgs = PropertiesUtilities.getValidationMessage("error.greater.market.fact", new Object[]{totalLabel});
					existsError = Boolean.TRUE;
				}
			}else if(marketDet.getMarketFactBalanceTO().getIndLonableBalance()!=null){
				if(balanceEntered.compareTo(marketDet.getQuantityLoanableOperation())>0){
					msgs = PropertiesUtilities.getValidationMessage("error.greater.market.fact", new Object[]{loanableOperation});
					existsError = Boolean.TRUE;
				}
			}else{
				if(balanceEntered.compareTo(marketDet.getAvailableBalance())>0){
					msgs = PropertiesUtilities.getValidationMessage("error.greater.market.fact", new Object[]{availabLabel});
					existsError = Boolean.TRUE;
				}
			}
		}
		
		if(existsError){
			JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT_ERROR), msgs);
			JSFUtilities.showSimpleValidationDialog();
			marketDet.setEnteredBalance(null);
		}
		recalculateAmountEntered(uiComponentName);
		return existsError;
	}
	
	/**
	 * Recalculate amount entered.
	 *
	 * @param uiComponentName the ui component name
	 */
	public void recalculateAmountEntered(String uiComponentName){
		BigDecimal quantityCalculated = BigDecimal.ZERO;
		
		MarketFactBalanceHelpTO marketFactBalance = getMarketFat(uiComponentName);
		for(MarketFactDetailHelpTO marketDetail: marketFactBalance.getMarketFacBalances()){
			if(marketDetail.getIsSelected()) 
				if(marketDetail.getEnteredBalance()!=null 
					&& !marketDetail.getEnteredBalance().equals(BigDecimal.ZERO))
					quantityCalculated = quantityCalculated.add(marketDetail.getEnteredBalance());
				else
					marketDetail.setEnteredBalance(null);
			else
				marketDetail.setEnteredBalance(null);
		}
		marketFactBalance.setBalanceResult(quantityCalculated);
	}
	
	public void startTransferAllitems(String uiComponentName){
		List<MarketFactDetailHelpTO> marketFacBalances = getMarketFat(uiComponentName).getMarketFacBalances();
		for (MarketFactDetailHelpTO marketDetail : marketFacBalances) {
			if(marketDetail.getIsSelected()){
				/*Transferimos los montos*/
				marketDetail.setEnteredBalance(marketDetail.getAvailableBalance());
				/*Luego verificamos los montos esten correctos*/
				boolean error =  validateAmountEntered(uiComponentName,marketDetail
					,marketDetail.getAvailableBalance().toString()
					,marketDetail.getQuantityBlockDocument().toString()
					,marketDetail.getTotalBalance().toString()
					,marketDetail.getQuantityLoanableOperation().toString());
				if (error) {
					tranferAllItems = false;
					//marketDetail.setIsDisabled(false);
					return;
				}//else{
					//marketDetail.setIsDisabled(true);
				//}
			}
		}
	}
	public void addAllItems(String uiComponentName){
		List<MarketFactDetailHelpTO> marketFacBalances = getMarketFat(uiComponentName).getMarketFacBalances();
		
		if(isCheckAllItems()){
			/*Marcamos todos los HM con saldo disponible mayor a cero*/
			for(MarketFactDetailHelpTO marketDetail: marketFacBalances){
				if(!marketDetail.getAvailableBalance().equals(BigDecimal.ZERO)
				   && marketDetail.getAvailableBalance()!=null) {
					marketDetail.setIsSelected(true);
				}else{
					marketDetail.setIsSelected(false);
				}
				//marketDetail.setIsDisabled(false);
			}
		}else{
			for(MarketFactDetailHelpTO marketDetail: marketFacBalances){
				marketDetail.setEnteredBalance(null);
				marketDetail.setIsSelected(false);
				tranferAllItems=false;
			}
			marketFactBalance = new MarketFactBalanceHelpTO();
			marketFactBalance.setBalanceResult(BigDecimal.ZERO);
		}
	}
	/**
	 * Gets the market fat.
	 *
	 * @param uiComponentName the ui component name
	 * @return the market fat
	 */
	public MarketFactBalanceHelpTO getMarketFat(String uiComponentName){
		return keyData.get(userInfo.getUserAccountSession().getUserName().concat(uiComponentName));
	}
	
	/**
	 * Return marke balancet object.
	 *
	 * @param uiComponent the ui component
	 * @return the market fact balance help to
	 */
	public MarketFactBalanceHelpTO returnMarkeBalancetObject(String uiComponent){
		MarketFactBalanceHelpTO objectFromKey = getMarketFat(uiComponent);
		return marketServiceBean.populateMarketWithData(objectFromKey);
	}
	
	/**
	 * Show dialog.
	 *
	 * @param uiComponentName the ui component name
	 */
	private void showDialog(String uiComponentName){
		StringBuilder jsExpresion = new StringBuilder();
		jsExpresion.append("PF('dialogW");
		jsExpresion.append(uiComponentName);
		jsExpresion.append("').show()");
		JSFUtilities.executeJavascriptFunction(jsExpresion.toString());
	}
	
	/**
	 * Close dialog.
	 *
	 * @param uiComponentName the ui component name
	 */
	public void closeDialog(String uiComponentName){
		keyData.remove(userInfo.getUserAccountSession().getUserName().concat(uiComponentName));
		StringBuilder jsExpresion = new StringBuilder();
		jsExpresion.append("PF('dialogW");
		jsExpresion.append(uiComponentName);
		jsExpresion.append("').hide()");
		JSFUtilities.executeJavascriptFunction(jsExpresion.toString());
	}


	/**
	 * Gets the market fact balance.
	 *
	 * @return the market fact balance
	 */
	public MarketFactBalanceHelpTO getMarketFactBalance() {
		return marketFactBalance;
	}

	/**
	 * Sets the market fact balance.
	 *
	 * @param marketFactBalance the new market fact balance
	 */
	public void setMarketFactBalance(MarketFactBalanceHelpTO marketFactBalance) {
		this.marketFactBalance = marketFactBalance;
	}

	public boolean isCheckAllItems() {
		return checkAllItems;
	}

	public void setCheckAllItems(boolean checkAllItems) {
		this.checkAllItems = checkAllItems;
	}

	public boolean isTranferAllItems() {
		return tranferAllItems;
	}

	public void setTranferAllItems(boolean tranferAllItems) {
		this.tranferAllItems = tranferAllItems;
	}
}