package com.pradera.core.component.generalparameter.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.model.generalparameter.type.MasterTableType;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class ParameterTableTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
public class ParameterTableTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The parameter table pk*/
	private Integer parameterTablePk;
	
	/** The parameter table pk list*/
	private List<Integer> lstParameterTablePk;
	
	private List<Integer> lstParameterTablePkNotIn;
	
	private List<Integer> lstMasterTableFk;
	
	/** The longInteger */
	private Long longInteger;
	
	private Integer shortInteger;

	/** The parameter table cd*/
	private String parameterTableCd;
	
	/** The master table fk. */
	private Integer masterTableFk;
	
	/** The state. */
	private Integer state;
	
	/** The element_subclasification1. */
	private Integer elementSubclasification1;
	
	/** The element_subclasification2. */
	private Integer elementSubclasification2;
	
	/** The id related parameter fk. */
	private Integer idRelatedParameterFk;
	
	/** The indicator1 */
	private String indicator1;
	
	/** The indicator2 */
	private String indicator2;
	
	/** The indicator4 */
	private Integer indicator4;
	
	/** The indicator4 */
	private Integer indicator5;
	
	/** The indicator4 */
	private Integer indicator6;
	
	/** The text2 */
	private String text1;
	
	/** The text2 */
	private String text2;
	
	/** The orderby parameter table cd. */
	private Integer orderbyParameterTableCd;
	
	private Integer orderbyParameterName;

	private Integer orderbyParameterDescription;
	
	private Integer orderByText1;
	
	public List<Integer> getLstMasterTableFk() {
		return lstMasterTableFk;
	}

	public void setLstMasterTableFk(List<Integer> lstMasterTableFk) {
		this.lstMasterTableFk = lstMasterTableFk;
	}

	/**
	 * Instantiates a new parameter table to.
	 */
	public ParameterTableTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ParameterTableTO(Integer parameterTablePk){
		this.parameterTablePk= parameterTablePk; 
	}
	
	/**
	 * Gets the master table fk.
	 *
	 * @return the master table fk
	 */
	public Integer getMasterTableFk() {
		return masterTableFk;
	}
	
	/**
	 * Sets the master table fk.
	 *
	 * @param masterTableFk the new master table fk
	 */
	public void setMasterTableFk(Integer masterTableFk) {
		this.masterTableFk = masterTableFk;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * Gets the id related parameter fk.
	 *
	 * @return the id related parameter fk
	 */
	public Integer getIdRelatedParameterFk() {
		return idRelatedParameterFk;
	}

	/**
	 * Sets the id related parameter fk.
	 *
	 * @param idRelatedParameterFk the new id related parameter fk
	 */
	public void setIdRelatedParameterFk(Integer idRelatedParameterFk) {
		this.idRelatedParameterFk = idRelatedParameterFk;
	}

	public Integer getParameterTablePk() {
		return parameterTablePk;
	}

	public void setParameterTablePk(Integer parameterTablePk) {
		this.parameterTablePk = parameterTablePk;
	}

	public String getParameterTableCd() {
		return parameterTableCd;
	}

	public void setParameterTableCd(String parameterTableCd) {
		this.parameterTableCd = parameterTableCd;
	}
	
	public String getIndicator1() {
		return indicator1;
	}

	public void setIndicator1(String indicator1) {
		this.indicator1 = indicator1;
	}

	public String getIndicator2() {
		return indicator2;
	}

	public void setIndicator2(String indicator2) {
		this.indicator2 = indicator2;
	}
	
	public List<Integer> getLstParameterTablePk() {
		return lstParameterTablePk;
	}

	public void setLstParameterTablePk(List<Integer> lstParameterTablePk) {
		this.lstParameterTablePk = lstParameterTablePk;
	}
	
	public Integer getElementSubclasification1() {
		return elementSubclasification1;
	}

	public void setElementSubclasification1(Integer elementSubclasification1) {
		this.elementSubclasification1 = elementSubclasification1;
	}

	
	public Integer getElementSubclasification2() {
		return elementSubclasification2;
	}

	public void setElementSubclasification2(Integer elementSubclasification2) {
		this.elementSubclasification2 = elementSubclasification2;
	}

	/**
	 * Sets the parater table pk from master.
	 * this method is useful when call parameter's table facade is called a lot's times, with this only write one code line
	 * @param masterTableType the master table type
	 * @return the parameter table to
	 */
	public ParameterTableTO setParaterTablePkFromMaster(MasterTableType masterTableType){
		this.setMasterTableFk(masterTableType.getCode());
		return this;
	}

	/**
	 * Gets the orderby parameter table cd.
	 *
	 * @return the orderby parameter table cd
	 */
	public Integer getOrderbyParameterTableCd() {
		return orderbyParameterTableCd;
	}

	/**
	 * Sets the orderby parameter table cd.
	 *
	 * @param orderbyParameterTableCd the new orderby parameter table cd
	 */
	public void setOrderbyParameterTableCd(Integer orderbyParameterTableCd) {
		this.orderbyParameterTableCd = orderbyParameterTableCd;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((parameterTablePk == null) ? 0 : parameterTablePk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParameterTableTO other = (ParameterTableTO) obj;
		if (parameterTablePk == null) {
			if (other.parameterTablePk != null)
				return false;
		} else if (!parameterTablePk.equals(other.parameterTablePk))
			return false;
		return true;
	}

	/**
	 * @return the indicator4
	 */
	public Integer getIndicator4() {
		return indicator4;
	}

	/**
	 * @param indicator4 the indicator4 to set
	 */
	public void setIndicator4(Integer indicator4) {
		this.indicator4 = indicator4;
	}

	/**
	 * @return the longInteger
	 */
	public Long getLongInteger() {
		return longInteger;
	}

	/**
	 * @param longInteger the longInteger to set
	 */
	public void setLongInteger(Long longInteger) {
		this.longInteger = longInteger;
	}

	/**
	 * get Short Integer
	 * @return shortInteger
	 */
	public Integer getShortInteger() {
		return shortInteger;
	}

	/**
	 * set Short Integer
	 * @param shortInteger the shortInteger to set
	 */
	public void setShortInteger(Integer shortInteger) {
		this.shortInteger = shortInteger;
	}

	/**
	 * @return the text2
	 */
	public String getText2() {
		return text2;
	}

	/**
	 * @param text2 the text2 to set
	 */
	public void setText2(String text2) {
		this.text2 = text2;
	}

	public List<Integer> getLstParameterTablePkNotIn() {
		return lstParameterTablePkNotIn;
	}

	public void setLstParameterTablePkNotIn(List<Integer> lstParameterTablePkNotIn) {
		this.lstParameterTablePkNotIn = lstParameterTablePkNotIn;
	}

	public Integer getIndicator5() {
		return indicator5;
	}

	public void setIndicator5(Integer indicator5) {
		this.indicator5 = indicator5;
	}

	public Integer getIndicator6() {
		return indicator6;
	}

	public void setIndicator6(Integer indicator6) {
		this.indicator6 = indicator6;
	}

	public Integer getOrderbyParameterName() {
		return orderbyParameterName;
	}

	public void setOrderbyParameterName(Integer orderbyParameterName) {
		this.orderbyParameterName = orderbyParameterName;
	}

	public Integer getOrderbyParameterDescription() {
		return orderbyParameterDescription;
	}

	public void setOrderbyParameterDescription(Integer orderbyParameterDescription) {
		this.orderbyParameterDescription = orderbyParameterDescription;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public Integer getOrderByText1() {
		return orderByText1;
	}

	public void setOrderByText1(Integer orderByText1) {
		this.orderByText1 = orderByText1;
	}
	
	
		
}