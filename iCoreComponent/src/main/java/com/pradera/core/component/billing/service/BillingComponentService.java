package com.pradera.core.component.billing.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.pradera.commons.contextholder.interceptor.ContextHolderInterceptor;
import com.pradera.commons.interceptores.Performance;
import com.pradera.commons.sessionuser.UserAccountSession;
import com.pradera.core.component.billing.to.BillingServiceTo;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.billing.BillingService;
import com.pradera.model.billing.type.RateStateType;
import com.pradera.model.billing.type.ServiceStateType;

@Stateless
@Performance
@Interceptors(ContextHolderInterceptor.class)
public class BillingComponentService extends CrudDaoServiceBean {

	/**
	 * Find Billing Service For Helper
	 * @param billingServiceTo
	 * @return
	 */
	public List<BillingService> findBillingService(BillingServiceTo billingServiceTo) {
		List<BillingService> listBillingService = null;

		if (Validations.validateIsNotNullAndNotEmpty(billingServiceTo)) {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("Select distinct billingService From BillingService billingService left outer join fetch billingService.serviceRates srs ");
			sbQuery.append(" Where  billingService.baseCollection = :baseCollection ");
			sbQuery.append(" and billingService.serviceState=" + ServiceStateType.SERVICE_STATUS_ACTIVE_PK.getCode());

			if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected())
					&& Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())) {

				sbQuery.append(" and  :effectiveDate  between trunc(srs.initialEffectiveDate) and trunc(srs.endEffectiveDate)");
				sbQuery.append(" and not srs.rateState in ("
						+ ServiceStateType.SERVICE_STATUS_CANCELLED_PK.getCode() + ","
						+ ServiceStateType.SERVICE_STATUS_LOCK_PK.getCode() + ")");

			} else {
				sbQuery.append(" and srs.rateState=" + RateStateType.ACTIVED.getCode());
			}

			Query query = em.createQuery(sbQuery.toString());

			query.setParameter("baseCollection",
					billingServiceTo.getBaseCollection());

			if (Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected())
					&& Validations.validateIsNotNull(billingServiceTo.getServiceRateToSelected().getLastEffectiveDate())) {
				query.setParameter("effectiveDate", billingServiceTo.getServiceRateToSelected().getLastEffectiveDate(),
						TemporalType.DATE);
			}

			try {
				listBillingService = query.getResultList();
			} catch (NoResultException nrex) {
				nrex.printStackTrace();
			}

		}

		return listBillingService;
	}
	
	/**
	 * Gets the list user by business process.
	 *
	 * @param businessProcess the business process
	 * @return the list user by business process
	 */
	public List<UserAccountSession> getListUserByBusinessProcess(Long businessProcess){
		List<UserAccountSession> listUserAccountSession=null;
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("    SELECT ");
		sbQuery.append("    	NVL(dn.id_user_account,' '),  ");		
		sbQuery.append("    	NVL(dn.email,' '),  ");
		sbQuery.append("    	NVL(dn.mobile_number,' '),   ");
		sbQuery.append("    	NVL(dn.full_name,' ') ");
		sbQuery.append("    FROM  ");
		sbQuery.append("    	business_process bp ");
		sbQuery.append("    inner join process_notification pn on pn.id_business_process_fk=bp.id_business_process_pk ");
		sbQuery.append("    inner join destination_notification dn on dn.id_process_notification_fk=pn.id_process_notification_pk ");
		sbQuery.append("    WHERE ");
		sbQuery.append("    	bp.id_business_process_pk=:idBusinessProcessPk ");
		Query query = em.createNativeQuery(sbQuery.toString());
		sbQuery.append("    ORDER BY id_business_process_pk  ");
		if(Validations.validateIsNotNullAndPositive(businessProcess)){
			query.setParameter("idBusinessProcessPk", businessProcess);
		}
		List<Object[]>  objectList = query.getResultList();
		listUserAccountSession=new ArrayList<UserAccountSession>();
		for(int i=0;i<objectList.size();++i){ 
	    	Object[] sResults = (Object[])objectList.get(i); 
			UserAccountSession userAccountSession=new UserAccountSession();
			userAccountSession.setUserName(sResults[0].toString());
			userAccountSession.setEmail(sResults[1].toString());
			userAccountSession.setPhoneNumber(sResults[2].toString());
			userAccountSession.setFullName(sResults[3].toString());
			listUserAccountSession.add(userAccountSession);
		}
		
		return listUserAccountSession;
	}

}
