package com.pradera.core.component.operation.to;

import java.io.Serializable;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project Pradera.
 * Copyright 2013.</li>
 * </ul>
 * 
 * The Class MechanismModalityTO.
 * this class is useful to handle queries to mechanism_modality By one nogitation_mechanism
 * for example if anyone want to get All otc's negotiation modality
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 17/04/2013
 */
public class MechanismModalityTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6509733311059672538L;

	/** The id negotiation mechanism pk. */
	private Long idNegotiationMechanismPK;
	
	/** The id negotiation modality state. */
	private Integer negotiationModalityState;
	
	/** The id participant pk. */
	private Long idParticipantPk;
	
	/** The id negotiation modality pk list. */
	private List<Long> idNegotiationModalityPkList;
	
	/** The participant mechanism role. */
	private Integer participantMechanismRole;
	
	/**
	 * Gets the id negotiation mechanism pk.
	 *
	 * @return the id negotiation mechanism pk
	 */
	public Long getIdNegotiationMechanismPK() {
		return idNegotiationMechanismPK;
	}
	/**
	 * Sets the id negotiation mechanism pk.
	 *
	 * @param idNegotiationMechanismPK the new id negotiation mechanism pk
	 */
	public void setIdNegotiationMechanismPK(Long idNegotiationMechanismPK) {
		this.idNegotiationMechanismPK = idNegotiationMechanismPK;
	}
	/**
	 * Gets the id negotiation modality state.
	 *
	 * @return the id negotiation modality state
	 */
	public Integer getNegotiationModalityState() {
		return negotiationModalityState;
	}
	/**
	 * Sets the id negotiation modality state.
	 *
	 * @param idNegotiationModalityState the new id negotiation modality state
	 */
	public void setNegotiationModalityState(Integer idNegotiationModalityState) {
		this.negotiationModalityState = idNegotiationModalityState;
	}
	/**
	 * Gets the id participant pk.
	 *
	 * @return the id participant pk
	 */
	public Long getIdParticipantPk() {
		return idParticipantPk;
	}
	/**
	 * Sets the id participant pk.
	 *
	 * @param idParticipantPk the new id participant pk
	 */
	public void setIdParticipantPk(Long idParticipantPk) {
		this.idParticipantPk = idParticipantPk;
	}
	/**
	 * Gets the id negotiation modality pk list.
	 *
	 * @return the id negotiation modality pk list
	 */
	public List<Long> getIdNegotiationModalityPkList() {
		return idNegotiationModalityPkList;
	}
	/**
	 * Sets the id negotiation modality pk list.
	 *
	 * @param idNegotiationModalityPkList the new id negotiation modality pk list
	 */
	public void setIdNegotiationModalityPkList(List<Long> idNegotiationModalityPkList) {
		this.idNegotiationModalityPkList = idNegotiationModalityPkList;
	}
	public MechanismModalityTO() {
		super();
	}
	public Integer getParticipantMechanismRole() {
		return participantMechanismRole;
	}
	public void setParticipantMechanismRole(Integer participantMechanismRole) {
		this.participantMechanismRole = participantMechanismRole;
	}
}