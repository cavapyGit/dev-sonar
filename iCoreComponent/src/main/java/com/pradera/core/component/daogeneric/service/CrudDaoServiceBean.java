package com.pradera.core.component.daogeneric.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionSynchronizationRegistry;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.contextholder.type.RegistryContextHolderType;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.core.framework.entity.producer.DepositaryDataBase;
import com.pradera.core.framework.entity.producer.DepositaryDataBaseBBV;
import com.pradera.core.framework.entity.service.LoaderEntityServiceBean;
import com.pradera.integration.audit.Auditable;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.operations.RequestCorrelativeType;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class CrudDaoServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 08/05/2013
 */
@Singleton
public class CrudDaoServiceBean {
	
	/** The em. */
	@Inject @DepositaryDataBase
	protected EntityManager em;
	
//	@Inject @DepositaryDataBaseBBV
//	protected EntityManager embbv;
	
	/** The loader entity service. */
	@EJB
	LoaderEntityServiceBean loaderEntityService;
	
	/** The transaction registry. */
	@Resource
	private TransactionSynchronizationRegistry transactionRegistry;
	
	@Inject
	PraderaLogger log;
	
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/**
	 * Creates the entity.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 */
	public <T> T create(T t) {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return create(t, object);
	}
	
	public <T> T createWithFlush(T t) {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		T x = create(t, object);
		this.em.flush();
		return x;
	}
	
	/**
	 * Creates the entity.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 */
	public <T> T create(T t, Object object) {
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
			}
		}
		this.em.persist(t);
		return t;
	}
	/**
	 * Merge for Batch
	 * @param t : Entity
	 * @param batchSize : Size to Batch
	 * @return
	 * @throws ServiceException
	 */
	public <T> T mergeForBatch(List<T> t, int batchSize) throws ServiceException {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		try {
		
			for (int i = 0; i < t.size(); i++) {
			    T entity = t.get(i);
			    if (object instanceof LoggerUser) {
					if (entity instanceof Auditable) {
						LoggerUser loggerUser = (LoggerUser)object;
						loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
						Auditable auditable = (Auditable)entity;
						setAuditFields(auditable,loggerUser);
					}
				}
			    em.merge(entity);
			    if(i % batchSize == 0) {
			        em.flush();
			        em.clear();
			    }
			}
		
		em.flush();
		em.clear();
		
		
		} catch(OptimisticLockException oex) {		
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS, oex);
		}

		
		return (T) t;
	}
	
	/**
	 * Persist for Batch
	 * @param t : Entity
	 * @param batchSize : Size to Batch
	 * @return
	 * @throws ServiceException
	 */
	public <T> T persistForBatch(List<T> t, int batchSize) throws ServiceException {
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		
		try {
		
			for (int i = 0; i < t.size(); i++) {
			    T entity = t.get(i);
			    if (object instanceof LoggerUser) {
					if (entity instanceof Auditable) {
						LoggerUser loggerUser = (LoggerUser)object;
						loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
						Auditable auditable = (Auditable)entity;
						setAuditFields(auditable,loggerUser);
					}
				}
			    em.persist(entity);
			    if(i % batchSize == 0) {
			        em.flush();
			        em.clear();
			    }
			}
		
		em.flush();
		em.clear();
		
		
		} catch(OptimisticLockException oex) {
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS, oex);
		}

		
		return (T) t;
	}
	
	/**
	 * Sets the audit fields.
	 *
	 * @param auditable the auditable
	 * @param loggerUser the logger user
	 */
	private void setAuditFields(Auditable auditable,LoggerUser loggerUser) {
		if(auditable != null) {
			auditable.setAudit(loggerUser);
			Map<String,List<? extends Auditable>> listAudit = auditable.getListForAudit();
			if (listAudit != null) {
				//if have list for audit
				for(List<? extends Auditable> auditableList :listAudit.values()) {
					if (auditableList != null && loaderEntityService.isLoadedList(auditableList)){
						//if list is not null
						for (Auditable childrenAudit : auditableList) {
							setAuditFields(childrenAudit,loggerUser);
						}
					}
				}
			}
		}
	}

	/**
	 * Find.
	 *
	 * @param <T> the generic type
	 * @param type the type
	 * @param id the id
	 * @return the t
	 */
	public <T> T find(Class<T> type, Object id) {
		return (T) this.em.find(type, id);
	}

	/**
	 * Delete.
	 *
	 * @param type the type
	 * @param id the id
	 */
	public void delete(Class<?> type, Object id) {
		Object ref = this.em.getReference(type, id);
		this.em.remove(ref);
	}
	
	
	/**
	 * Detach.
	 *
	 * @param id the id
	 */
	public void detach(Object id) {
		this.em.detach(id);
	}

	/**
	 * Update.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 * @throws ServiceException the service exception
	 */
	public <T> T update(T t) throws ServiceException{
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return update(t, object);
	}
	
	public <T> T updateWithOutFlush(T t)throws ServiceException{
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);
		return  updateWithOutFlush(t, object);
	}
	

	/**
	 * Update.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the t
	 * @throws ServiceException the service exception
	 */
	public <T> T update(T t, Object object) throws ServiceException{
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				//update with real time
				loggerUser.setAuditTime(CommonsUtilities.currentDateTime());
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
			}
		}
		T mergeEntity = null;
		try {
			mergeEntity = this.em.merge(t);
			this.em.flush();
		} catch(OptimisticLockException oex) {		
			throw new ServiceException(ErrorServiceType.CONCURRENCY_ERROR_PROCESS, oex);
		}
				
		return mergeEntity;
	}
	
	public <T> T updateWithOutFlush(T t, Object object) throws ServiceException{
		if (object instanceof LoggerUser) {
			if (t instanceof Auditable) {
				LoggerUser loggerUser = (LoggerUser)object;
				Auditable auditable = (Auditable)t;
				setAuditFields(auditable,loggerUser);
			}
		}
		T mergeEntity = null;
		
		mergeEntity = this.em.merge(t);
		
		return mergeEntity;
	}
	
	/**
	 * Find with named query.
	 *
	 * @param namedQueryName the named query name
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}
	
	/**
	 * Find with named query.
	 *
	 * @param namedQueryName the named query name
	 * @param parameters the parameters
	 * @return the object
	 */
	@SuppressWarnings("unchecked")
	public <T> T findObjectByNamedQuery(String namedQueryName,Map<String, Object> parameters) {
		Query query = this.em.createNamedQuery(namedQueryName);
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return (T)query.getSingleResult();
	}

	/**
	 * Find with named query.
	 *
	 * @param namedQueryName the named query name
	 * @param parameters the parameters
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		return findWithNamedQuery(namedQueryName, parameters, 0);
	}

	/**
	 * Find by native query.
	 *
	 * @param <T> the generic type
	 * @param sql the sql
	 * @param type the type
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> findByNativeQuery(String sql, Class<T> type) {
		return this.em.createNativeQuery(sql, type).getResultList();
	}
	
	/**
	 * find list bv native query.
	 *
	 * @param strSqlquery String
	 * @param parameters Map<String, Object>
	 * @return List
	 */
	public List findByNativeQuery(String strSqlquery,Map<String, Object> parameters) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNativeQuery(strSqlquery);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}

	/**
	 * Find with named query.
	 *
	 * @param namedQueryName the named query name
	 * @param parameters the parameters
	 * @param resultLimit the result limit
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<Object> findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0)
			query.setMaxResults(resultLimit);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return (List<Object>) query.getResultList();
	}

	/**
	 * Find.
	 *
	 * @param <T> the generic type
	 * @param id the id
	 * @param type the type
	 * @return the t
	 */
	public <T> T find(Object id, Class<T> type) {
		return (T) this.em.find(type, id);
	}
	

	/**
	 * Method for returning the list of objects 
	 * @param sql String
	 * @return the list 
	 */
	public List findListByQueryString(String sql) {
		return this.em.createQuery(sql).getResultList();
	}

	/**
	 * Used to get List of Objects.
	 *
	 * @param sql String
	 * @param parameters Map<String,Object>
	 * @return the list
	 * List
	 */
	public List findListByQueryString(String sql,Map<String, Object> parameters) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(sql);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
    }
	
	public <T> List<T> findListByQueryStringMaxResults(String sql,Map<String, Object> parameters) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(sql);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		query.setMaxResults(maxResultsQueryHelper);
		return query.getResultList();
    }

	/**
	 * Used to get Unique Object.
	 *
	 * @param sql String
	 * @param parameters Map<String,Object>
	 * @return Object
	 */
	public Object findObjectByQueryString(String sql,Map<String, Object> parameters) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(sql);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Object object = null;
		try{
			object = query.getSingleResult();
		}catch(NoResultException nre){
			log.error("object not found for query ");
		}
		// do not catch! , it must be thrown because is an inconsistency
		//catch(NonUniqueResultException nue){
		//	log.error("multiple object found for query (inconsistency)");
		//}
		return object;
    }
	
	/**
	 * Find entity by named query.
	 *
	 * @param <T> the generic type
	 * @param classEntity the class entity
	 * @param nameQuery the name query
	 * @param parameters the parameters
	 * @return the t
	 */
	public <T> T findEntityByNamedQuery(Class<T> classEntity,String nameQuery,Map<String, Object> parameters){
		T entity = null;
		TypedQuery<T> query = em.createNamedQuery(nameQuery, classEntity);
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		try{
			entity = query.getSingleResult();
		} catch(NoResultException  nrex){
			// Object not found 
		}
		return entity;
	}
	
	/**
	 * Save all.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 * @return the list
	 */
	public <T> List<T> saveAll(List<T> list, Object object){
		if(list != null){
			for (T t : list) {
				create(t,object);
			}
		}
		return list;
	}
	
	/**
	 * Save all.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 * @return the list
	 */
	public <T> List<T> saveAll(List<T> list){
		Object object = transactionRegistry.getResource(RegistryContextHolderType.LOGGER_USER);		
		return saveAll(list, object);
	}
	
	/**
	 * Update list.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 * @return the list
	 */
	public <T> List<T> updateList(List<T> list){
		if(list != null){
			for (T t : list) {
				em.merge(t);
			}
		}
		return list;
	}
	
	/**
	 * Update by query.
	 *
	 * @param sql the sql
	 * @param parameters the parameters
	 * @return the object
	 */
	public Object updateByQuery(String sql,Map<String, Object> parameters){
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createQuery(sql);
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.executeUpdate();
	}
	
	/**
	 * Gets the next correlative request operations.
	 *
	 * @param requestOperation the request operation
	 * @return the next correlative operations
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Long getNextCorrelativeOperations(RequestCorrelativeType requestOperation){
		Object value=null;
		Long nextVal=null;
		try{
		String nameSequence = requestOperation.getValue();
		Query query = em.createNativeQuery("select "+nameSequence+".nextval from dual");
		value = query.getSingleResult();	
			if(value!=null){
				nextVal = Long.valueOf(value.toString());
			}
		}
		catch(Exception e){
	    	boolean foundException=false;
	    	while (e.getCause() != null){
	    		 if(e.getCause().getMessage().toUpperCase().contains("NEXTVAL EXCEEDS MAXVALUE")){
	    			 foundException = true; 
	    			 break;			    			 
	    		 }
	    		 else{
	    			 e = (Exception) e.getCause();
	    		 }
	    	}
	    	if(foundException){
	    		nextVal = new Long(-1);
	    	}
			
		}
		return nextVal;
	}
	
	
	/**
	 * Gets the next correlative request operations.
	 *
	 * @param requestOperation the request operation
	 * @return the next correlative operations
	 */
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Long getNextCorrelativeOperations(String nameSequence){
		Object value=null;
		Long nextVal=null;
		try{		
		Query query = em.createNativeQuery("select "+nameSequence+".nextval from dual");
		value = query.getSingleResult();	
			if(value!=null){
				nextVal = Long.valueOf(value.toString());
			}
		}
		catch(Exception e){
	    	boolean foundException=false;
	    	while (e.getCause() != null){
	    		 if(e.getCause().getMessage().toUpperCase().contains("NEXTVAL EXCEEDS MAXVALUE")){
	    			 foundException = true; 
	    			 break;			    			 
	    		 }
	    		 else{
	    			 e = (Exception) e.getCause();
	    		 }
	    	}
	    	if(foundException){
	    		nextVal = new Long(-1);
	    	}
			
		}
		return nextVal;
	}
	
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long getCurrValSequence(RequestCorrelativeType requestOperation){
		Object value=null;
		Long nextVal=null;
		String nameSequence = requestOperation.getValue();
		Query query = em.createNativeQuery("select "+nameSequence+".currval from dual");
		value = query.getSingleResult();	
		if(value!=null){
			nextVal = Long.valueOf(value.toString());
		}
		return nextVal;
	}
	
	/**
	 * The Flush Transaction
	 */
	public void flushTransaction(){
		this.em.flush();
	}
	
	/**
	 * The Clear Transaction
	 */
	public void clearTransaction(){
		this.em.clear();
	}
	
	
}
