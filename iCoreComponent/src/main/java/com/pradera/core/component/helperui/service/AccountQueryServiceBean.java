package com.pradera.core.component.helperui.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.helperui.to.AccountSearcherTO;
import com.pradera.core.component.helperui.to.HolderTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.model.accounts.holderaccounts.HolderAccount;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountGroupType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;

@Stateless
public class AccountQueryServiceBean extends CrudDaoServiceBean {

	@Inject
	private PraderaLogger log;
	
	@Inject
	@Configurable
	private Integer maxResultsQuery;
	
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	public AccountQueryServiceBean() {
		// TODO Auto-generated constructor stub
	}
	
	//SEARCH ACCOUNT BY ACCOUNT NUMBER
	public HolderAccount getAccountByNumber(AccountSearcherTO accountSearcher) {
		HolderAccount holderAccount = null;
		StringBuilder sb = new StringBuilder();
		sb.append(" select ha from HolderAccount ha " +
				 " join fetch ha.participant p " +
				 " join fetch ha.holderAccountDetails had " +
				 " join fetch had.holder "+
				 " where 1=1 ");
		
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getSelected().getAccountNumber())){
			sb.append(" and ha.accountNumber= :accountNumber ");
		}
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getParticipantCode())){
			sb.append(" and p.idParticipantPk= :partCode");
		}
		
		TypedQuery<HolderAccount> typedQuery =  em.createQuery(sb.toString(),HolderAccount.class);
		
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getSelected().getAccountNumber())){
			typedQuery.setParameter("accountNumber", accountSearcher.getSelected().getAccountNumber());
		}
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getParticipantCode())){
			typedQuery.setParameter("partCode", accountSearcher.getParticipantCode());
		}
		
		try{
			holderAccount = typedQuery.getSingleResult();
		}catch(NoResultException nex) {
			log.info("Account not found");
			holderAccount = null;
		}
		
		return holderAccount;
	}
	
	//SEARCH ACCOUNT BY CUI and/or Participant TMP
	public List<HolderAccount> getAccountByCuiandParticipant(AccountSearcherTO accountSearcher) {
		List<HolderAccount> lstHolderAccounts = null;
		StringBuilder sb = new StringBuilder();
		sb.append(" select ha from HolderAccount ha " +
				 " join fetch ha.participant p " +
				 " join fetch ha.holderAccountDetails had " +
				 " join fetch had.holder ho "+
				 " where 1=1 ");
		
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getIdHolderPk())){
			sb.append(" and ho.idHolderPk= :idHolderPk");
		}
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getParticipantCode())){
			sb.append(" and p.idParticipantPk= :partCode");
		}
		
		TypedQuery<HolderAccount> typedQuery =  em.createQuery(sb.toString(),HolderAccount.class);
		
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getIdHolderPk())){
			typedQuery.setParameter("idHolderPk", accountSearcher.getIdHolderPk());
		}
		if(Validations.validateIsNotNullAndPositive(accountSearcher.getParticipantCode())){
			typedQuery.setParameter("partCode", accountSearcher.getParticipantCode());
		}
		
		try{
			lstHolderAccounts = typedQuery.getResultList();
		}catch(NoResultException nex) {
			log.info("Account not found");
			lstHolderAccounts = null;
		}
		
		return lstHolderAccounts;
	}

	public List<Object[]> searchHolderAccountByFilter(HolderTO filter){
		StringBuilder sb = new StringBuilder();
		
		sb.append(	" SELECT 													" +
					"		HA.ACCOUNT_NUMBER,									" +
					"		HA.ALTERNATE_CODE,									" +
					"		HO.NAME,											" +
					"		HO.FIRST_LAST_NAME,									" +
					"		HO.SECOND_LAST_NAME,								" +
					"		HO.FULL_NAME,										" +
					"		HA.STATE_ACCOUNT,									" +
					"		HA.ID_HOLDER_ACCOUNT_PK,							" +
					"		HA.ACCOUNT_GROUP,									" +
					"		HO.HOLDER_TYPE,										" +
					"		HO.ID_HOLDER_PK,									" +
					"		P.ID_PARTICIPANT_PK,								" +
					"		P.MNEMONIC,											" +
					"		P.DESCRIPTION,										" +
					"		HA.ACCOUNT_TYPE									    " +
					" FROM														" +
					"	    HOLDER_ACCOUNT HA,									" +
					"	    HOLDER_ACCOUNT_DETAIL HAD,							" +
					"	    HOLDER	HO,											" +
					"	    PARTICIPANT	P										" +
					" WHERE														" +
					"		HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK	" +
					" AND														" +
					"		HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK					" +
					" AND														" +
					"		HA.ID_PARTICIPANT_FK = P.ID_PARTICIPANT_PK					" );
		if(Validations.validateIsNotNullAndPositive(filter.getHolderAccountId())){
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK= :holderAccountId");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getAccountNumber())){
			sb.append(" AND HA.ACCOUNT_NUMBER= :holderAccount");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getPartIdPk())){
			sb.append(" AND HA.ID_PARTICIPANT_FK= :idParticipantPk");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getHolderId()) && filter.getFlagAllHolders()){
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK in (select had2.id_holder_account_fk from holder_account_detail had2 where had2.id_holder_fk = :idHolderPk ) ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getHolderId()) && !filter.getFlagAllHolders()){
			sb.append(" AND HO.ID_HOLDER_PK= :idHolderPk");
		}
		
		if(Validations.validateIsNotNull(filter.getLstHolderAccountId()) && filter.getLstHolderAccountId().size()>0){
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK in ( :lstHolderAccountId )");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			sb.append(" AND HO.DOCUMENT_TYPE= :docType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			sb.append(" AND HO.DOCUMENT_NUMBER LIKE :docNumber");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
			sb.append(" AND HO.NAME LIKE :holderName ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
			sb.append(" AND HO.FIRST_LAST_NAME LIKE :holderFirstLastName ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
			sb.append(" AND HO.SECOND_LAST_NAME LIKE :holderSecondLastName ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			sb.append(" AND HO.FULL_NAME LIKE :holderFullName");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountState())){
			sb.append(" AND HA.STATE_ACCOUNT= :accountState");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			sb.append(" AND HA.ACCOUNT_TYPE= :accountType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerFk()) && filter.isUserIssuer()){
			sb.append(" AND (EXISTS(SELECT 1 FROM HOLDER_ACCOUNT_BALANCE HAB , HOLDER_ACCOUNT HA,HOLDER_ACCOUNT_DETAIL HAD,SECURITY SE ");
			sb.append("WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
			sb.append("and HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
			sb.append("and HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
			sb.append("and SE.ID_ISSUER_FK = :issuer ");
			sb.append("and HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK ");
			sb.append("and HAB.TOTAL_BALANCE>0)) ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerFk()) && filter.isUserIssuerDpf()){
			sb.append(" AND (EXISTS(SELECT 1 FROM HOLDER_ACCOUNT_BALANCE HAB , HOLDER_ACCOUNT HA,HOLDER_ACCOUNT_DETAIL HAD,SECURITY SE ");
			sb.append("WHERE HAB.ID_HOLDER_ACCOUNT_PK = HA.ID_HOLDER_ACCOUNT_PK ");
			sb.append("and HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK ");
			sb.append("and HAB.ID_SECURITY_CODE_PK = SE.ID_SECURITY_CODE_PK ");
			sb.append("and SE.ID_ISSUER_FK = :issuer ");
			sb.append("and SE.SECURITY_CLASS in (:lstSecurityClass) ");
			sb.append("and HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK ");
			sb.append("and HAB.TOTAL_BALANCE>0)) ");
		}
		sb.append(" ORDER BY HA.ACCOUNT_NUMBER ");
			
		Query query = em.createNativeQuery(sb.toString());
		
		if(Validations.validateIsNotNullAndPositive(filter.getHolderAccountId())){
			query.setParameter("holderAccountId", filter.getHolderAccountId());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountNumber())){
			query.setParameter("holderAccount", filter.getAccountNumber());
		}
		if(Validations.validateIsNotNull(filter.getLstHolderAccountId()) && filter.getLstHolderAccountId().size()>0){
			query.setParameter("lstHolderAccountId",filter.getLstHolderAccountId());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getPartIdPk())){
			query.setParameter("idParticipantPk", filter.getPartIdPk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getHolderId())){
			query.setParameter("idHolderPk", filter.getHolderId());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			query.setParameter("docType",filter.getDocumentType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			query.setParameter("docNumber", "%"+filter.getDocumentNumber()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
			query.setParameter("holderName", "%"+filter.getName()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
			query.setParameter("holderFirstLastName", "%"+filter.getFirstLastName()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
			query.setParameter("holderSecondLastName", "%"+filter.getSecondLastName()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			query.setParameter("holderFullName", "%"+filter.getFullName()+"%");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountState())){
			query.setParameter("accountState", filter.getAccountState());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountType())){
			query.setParameter("accountType", filter.getAccountType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerFk()) && filter.isUserIssuer()){
			query.setParameter("issuer", filter.getIssuerFk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getIssuerFk()) && filter.isUserIssuerDpf()){
			query.setParameter("issuer", filter.getIssuerFk());
			List<Integer> lstSecurityClass = new ArrayList<Integer>();
			lstSecurityClass.add(SecurityClassType.DPA.getCode());
			lstSecurityClass.add(SecurityClassType.DPF.getCode());
			query.setParameter("lstSecurityClass", lstSecurityClass);
		}
		query.setMaxResults(maxResultsQueryHelper);
		
		return query.getResultList();
	}
	
	//SEARCH ACCOUNT BY FILTERS
	public List<Object[]> searchAccountByFilter(HolderTO filter){
		StringBuilder sb = new StringBuilder();
		
		sb.append(	" SELECT 													" +
					"		HA.ACCOUNT_NUMBER,									" +
					"		HA.ALTERNATE_CODE,									" +
					"		HO.NAME,											" +
					"		HO.FIRST_LAST_NAME,									" +
					"		HO.SECOND_LAST_NAME,								" +
					"		HO.FULL_NAME,										" +
					"		HA.STATE_ACCOUNT,									" +
					"		HA.ID_HOLDER_ACCOUNT_PK,							" +
					"		HA.ACCOUNT_GROUP,									" +
					"		HO.HOLDER_TYPE,										" +
					"		HO.ID_HOLDER_PK,									" +
					"		HA.ACCOUNT_TYPE,									" +
					"		HA.ID_PARTICIPANT_FK,								" +
					"		HA.RELATED_NAME										" +
					" FROM														" +
					"	    HOLDER_ACCOUNT HA,									" +
					"	    HOLDER_ACCOUNT_DETAIL HAD,							" +
					"	    HOLDER	HO											" +
					" WHERE														" +
					"		HA.ID_HOLDER_ACCOUNT_PK = HAD.ID_HOLDER_ACCOUNT_FK	" +
					" AND														" +
					"		HAD.ID_HOLDER_FK = HO.ID_HOLDER_PK					" + 
					" AND	HA.ACCOUNT_GROUP in ( :lstInvestorGroup ) 			");
		if(Validations.validateIsNotNullAndPositive(filter.getHolderAccountId())){
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK= :holderAccountId");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getParticipantFk())){
			sb.append(" AND HA.ID_PARTICIPANT_FK= :idParticipantPk");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getHolderId()) && filter.getFlagAllHolders()){
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK in (select had2.id_holder_account_fk from holder_account_detail had2 where had2.id_holder_fk = :idHolderPk ) ");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getHolderId()) && !filter.getFlagAllHolders()){
			sb.append(" AND HO.ID_HOLDER_PK= :idHolderPk");
		}
		
		if(Validations.validateIsNotNull(filter.getLstHolderAccountId()) && filter.getLstHolderAccountId().size()>0){
			sb.append(" AND HA.ID_HOLDER_ACCOUNT_PK in ( :lstHolderAccountId )");
		}
		
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			sb.append(" AND HO.DOCUMENT_TYPE= :docType");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			sb.append(" AND HO.DOCUMENT_NUMBER LIKE :docNumber");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
			sb.append(" AND HO.NAME LIKE :holderName ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
			sb.append(" AND HO.FIRST_LAST_NAME LIKE :holderFirstLastName ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
			sb.append(" AND HO.SECOND_LAST_NAME LIKE :holderSecondLastName ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			sb.append(" AND HO.FULL_NAME LIKE :holderFullName");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountState())){
			sb.append(" AND HA.STATE_ACCOUNT= :accountState");
		}
		
		sb.append(" ORDER BY HA.ACCOUNT_NUMBER ");
			
		Query query = em.createNativeQuery(sb.toString());
		List<Integer> lstInvestorGroup= new ArrayList<Integer>();
		if(filter.getFlagOnlyInvestors()) {
			lstInvestorGroup.add(HolderAccountGroupType.INVERSTOR.getCode());
		}else {
			lstInvestorGroup.add(HolderAccountGroupType.ISSUER.getCode());
			lstInvestorGroup.add(HolderAccountGroupType.INVERSTOR.getCode());
		}	
		query.setParameter("lstInvestorGroup", lstInvestorGroup);
		
		if(Validations.validateIsNotNullAndPositive(filter.getHolderAccountId())){
			query.setParameter("holderAccountId", filter.getHolderAccountId());
		}
		if(Validations.validateIsNotNull(filter.getLstHolderAccountId()) && filter.getLstHolderAccountId().size()>0){
			query.setParameter("lstHolderAccountId",filter.getLstHolderAccountId());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getParticipantFk())){
			query.setParameter("idParticipantPk", filter.getParticipantFk());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getHolderId())){
			query.setParameter("idHolderPk", filter.getHolderId());
		}
		if(Validations.validateIsNotNullAndPositive(filter.getDocumentType())){
			query.setParameter("docType",filter.getDocumentType());
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getDocumentNumber())){
			query.setParameter("docNumber", "%"+filter.getDocumentNumber()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getName())){
			query.setParameter("holderName", "%"+filter.getName()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFirstLastName())){
			query.setParameter("holderFirstLastName", "%"+filter.getFirstLastName()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getSecondLastName())){
			query.setParameter("holderSecondLastName", "%"+filter.getSecondLastName()+"%");
		}
		if(Validations.validateIsNotNullAndNotEmpty(filter.getFullName())){
			query.setParameter("holderFullName", "%"+filter.getFullName()+"%");
		}
		if(Validations.validateIsNotNullAndPositive(filter.getAccountState())){
			query.setParameter("accountState", filter.getAccountState());
		}
		
		query.setMaxResults(maxResultsQuery);
		
		return query.getResultList();
	}
	
	/**
	 * Find holder account.
	 *
	 * @param idHolderAccountPk the id holder account pk
	 * @return the holder account
	 */
	public HolderAccount findHolderAccount(Long idHolderAccountPk) {
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ha");
			sbQuery.append("	FROM HolderAccount ha");
			sbQuery.append("	INNER JOIN FETCH ha.holderAccountDetails had");
			sbQuery.append("	INNER JOIN FETCH had.holder ho");
			sbQuery.append("	WHERE ha.idHolderAccountPk = :idHolderAccountPk");
			Query query = em.createQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			return (HolderAccount)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * Get Description to HolderAccount 
	 * @param idHolderAccountPk
	 * @return
	 */
	public String getHolderAccountDetailDescription(Long idHolderAccountPk){
		HolderAccount holderAccount=findHolderAccount(idHolderAccountPk);
		String holderAccountDescription="";
		if(Validations.validateIsNotNull(holderAccount)){
			holderAccountDescription=holderAccount.getHolderAccountDetailDescription();
		}
		
		return holderAccountDescription;
	}
	
	/**
	 * Get Full Name To Holder By idHolderAccountPk
	 * @param idHolderAccountPk 
	 * @return Full Name Holder
	 */
	public String getFullNameHolderAccount(Long idHolderAccountPk){
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("	SELECT ");
			sbQuery.append("	listagg(ho.full_name,' <br/> ') within group (order by ho.full_name) ");
			sbQuery.append("	 from holder_account ha inner join holder_account_detail had");
			sbQuery.append("	on had.id_holder_account_fk=ha.id_holder_account_pk");
			sbQuery.append("	inner join holder ho ");
			sbQuery.append("	on ho.id_holder_pk=had.id_holder_fk ");
			sbQuery.append("	where ha.id_holder_account_pk= :idHolderAccountPk");
			sbQuery.append("	order by ha.id_holder_account_pk ");
			
			Query query = em.createNativeQuery(sbQuery.toString());
			query.setParameter("idHolderAccountPk", idHolderAccountPk);
			return (String)query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
