package com.pradera.core.component.business.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.pradera.model.accounts.Holder;
import com.pradera.model.issuancesecuritie.Issuer;

public class ExpirationSecurityTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idHolderAccountPk;
	
	private Long accountNumber;
	
	private Long idHolderPk;
	
	private String idSecurityCodePk;
	
	private Date issuanceDate;
	
	private Date expirationDate;
	
	private String balance;
	
	private BigDecimal quantity;
	
	private BigDecimal finalSecurity;
	
	private BigDecimal totalAmount;
	
	private Integer indicator;
	
	private Long idCorporativeProcessResult;
	
	private String holder;
	
	private String holderName;
	
	private String description;
	
	private String currencyDescription;
	
	private Integer currency;
	
	private Integer indicatorType;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the idHolderAccount
	 */
	public Long getIdHolderAccountPk() {
		return idHolderAccountPk;
	}

	/**
	 * @param idHolderAccount the idHolderAccount to set
	 */
	public void setIdHolderAccountPk(Long idHolderAccountPk) {
		this.idHolderAccountPk = idHolderAccountPk;
	}

	/**
	 * @return the idHolderPk
	 */
	public Long getIdHolderPk() {
		return idHolderPk;
	}

	/**
	 * @param idHolderPk the idHolderPk to set
	 */
	public void setIdHolderPk(Long idHolderPk) {
		this.idHolderPk = idHolderPk;
	}

	/**
	 * @return the idSecurityCodePk
	 */
	public String getIdSecurityCodePk() {
		return idSecurityCodePk;
	}

	/**
	 * @param idSecurityCodePk the idSecurityCodePk to set
	 */
	public void setIdSecurityCodePk(String idSecurityCodePk) {
		this.idSecurityCodePk = idSecurityCodePk;
	}

	/**
	 * @return the issuanceDate
	 */
	public Date getIssuanceDate() {
		return issuanceDate;
	}

	/**
	 * @param issuanceDate the issuanceDate to set
	 */
	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	/**
	 * @return the expirationDate
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate the expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the finalSecurity
	 */
	public BigDecimal getFinalSecurity() {
		return finalSecurity;
	}

	/**
	 * @param finalSecurity the finalSecurity to set
	 */
	public void setFinalSecurity(BigDecimal finalSecurity) {
		this.finalSecurity = finalSecurity;
	}

	/**
	 * @return the totalAmount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the indicator
	 */
	public Integer getIndicator() {
		return indicator;
	}

	/**
	 * @param indicator the indicator to set
	 */
	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	/**
	 * @return the idCorporativeProcessResult
	 */
	public Long getIdCorporativeProcessResult() {
		return idCorporativeProcessResult;
	}

	/**
	 * @param idCorporativeProcessResult the idCorporativeProcessResult to set
	 */
	public void setIdCorporativeProcessResult(Long idCorporativeProcessResult) {
		this.idCorporativeProcessResult = idCorporativeProcessResult;
	}

	/**
	 * @return the accountNumber
	 */
	public Long getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the holder
	 */
	public String getHolder() {
		return holder;
	}

	/**
	 * @param holder the holder to set
	 */
	public void setHolder(String holder) {
		this.holder = holder;
	}

	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return the currencyDescription
	 */
	public String getCurrencyDescription() {
		return currencyDescription;
	}

	/**
	 * @param currencyDescription the currencyDescription to set
	 */
	public void setCurrencyDescription(String currencyDescription) {
		this.currencyDescription = currencyDescription;
	}

	/**
	 * @return the currency
	 */
	public Integer getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	/**
	 * @return the indicatorType
	 */
	public Integer getIndicatorType() {
		return indicatorType;
	}

	/**
	 * @param indicatorType the indicatorType to set
	 */
	public void setIndicatorType(Integer indicatorType) {
		this.indicatorType = indicatorType;
	}
	
	

}
