package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;


/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class HolderTO.
 * Tranfers Object to Holder
 * @author PraderaTechnologies.
 * @version 1.0 , 04/02/2013
 */
public class HolderTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4223401164944092592L;

	private List<Long> lstHolderAccountId;
	
	/** The holder id. */
	private Long HolderAccountId;
	
	/** The holder id. */
	private Long holderId;

	/** The holder id. */
	private Boolean flagAllHolders = false;
	
	/** The holder id. */
	private Boolean flagOnlyInvestors = true;

	/** The full name. */
	private String fullName;
	
	private String relatedName;
	
	public String getRelatedName() {
		return relatedName;
	}

	public void setRelatedName(String relatedName) {
		this.relatedName = relatedName;
	}

	/** The name. */
	private String name;
	
	/** The first last name. */
	private String firstLastName;
	
	/** The second last name. */
	private String secondLastName;
	
	private Integer documentType;
	
	private String documentTypeDesc;
	
	private String documentNumber;

	private Long partIdPk;
	
	private Integer accountState;
	
	private Integer legalResidenceCountry;
	private Integer nationality;
	private Integer legalRepresentativeCountry;
	private Long participantFk;
	private String issuerFk;
	private Long blockEntityFk;
	private Integer indParticipant;
	private boolean blNatural;
	private boolean blJuridic;
	private Integer state;
	private Integer accountNumber;
	private Integer accountType;
	private boolean userIssuer;
	private boolean userIssuerDpf;
	
	private String totalBalance;
	
	private String availableBalance;
	
	public Integer getIndParticipant() {
		return indParticipant;
	}

	public void setIndParticipant(Integer indParticipant) {
		this.indParticipant = indParticipant;
	}

	public Integer getLegalResidenceCountry() {
		return legalResidenceCountry;
	}

	public void setLegalResidenceCountry(Integer legalResidenceCountry) {
		this.legalResidenceCountry = legalResidenceCountry;
	}

	public Integer getNationality() {
		return nationality;
	}

	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	public Integer getLegalRepresentativeCountry() {
		return legalRepresentativeCountry;
	}

	public void setLegalRepresentativeCountry(Integer legalRepresentativeCountry) {
		this.legalRepresentativeCountry = legalRepresentativeCountry;
	}

	public Long getParticipantFk() {
		return participantFk;
	}

	public void setParticipantFk(Long participantFk) {
		this.participantFk = participantFk;
	}

	public String getIssuerFk() {
		return issuerFk;
	}

	public void setIssuerFk(String issuerFk) {
		this.issuerFk = issuerFk;
	}

	public Long getBlockEntityFk() {
		return blockEntityFk;
	}

	public void setBlockEntityFk(Long blockEntityFk) {
		this.blockEntityFk = blockEntityFk;
	}

	private Integer personType;
	public Integer getPersonType() {
		return personType;
	}

	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	/**
	 * Instantiates a new holder to.
	 */
	public HolderTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Gets the holder id.
	 *
	 * @return the holder id
	 */
	public Long getHolderId() {
		return holderId;
	}

	/**
	 * Sets the holder id.
	 *
	 * @param holderId the new holder id
	 */
	public void setHolderId(Long holderId) {
		this.holderId = holderId;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * Sets the full name.
	 *
	 * @param fullName the new full name
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the first last name.
	 *
	 * @return the first last name
	 */
	public String getFirstLastName() {
		return firstLastName;
	}

	/**
	 * Sets the first last name.
	 *
	 * @param firstLastName the new first last name
	 */
	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	/**
	 * Gets the second last name.
	 *
	 * @return the second last name
	 */
	public String getSecondLastName() {
		return secondLastName;
	}

	/**
	 * Sets the second last name.
	 *
	 * @param secondLastName the new second last name
	 */
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public Integer getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentTypeDesc() {
		return documentTypeDesc;
	}

	public void setDocumentTypeDesc(String documentTypeDesc) {
		this.documentTypeDesc = documentTypeDesc;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public Long getPartIdPk() {
		return partIdPk;
	}
	public void setPartIdPk(Long partIdPk) {
		this.partIdPk = partIdPk;
	}

	public Integer getAccountState() {
		return accountState;
	}

	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}

	public Long getHolderAccountId() {
		return HolderAccountId;
	}

	public void setHolderAccountId(Long holderAccountId) {
		HolderAccountId = holderAccountId;
	}

	public List<Long> getLstHolderAccountId() {
		return lstHolderAccountId;
	}

	public void setLstHolderAccountId(List<Long> lstHolderAccountId) {
		this.lstHolderAccountId = lstHolderAccountId;
	}

	public Boolean getFlagAllHolders() {
		return flagAllHolders;
	}

	public void setFlagAllHolders(Boolean flagAllHolders) {
		this.flagAllHolders = flagAllHolders;
	}

	public boolean isBlNatural() {
		return blNatural;
	}

	public void setBlNatural(boolean blNatural) {
		this.blNatural = blNatural;
	}

	public boolean isBlJuridic() {
		return blJuridic;
	}

	public void setBlJuridic(boolean blJuridic) {
		this.blJuridic = blJuridic;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public boolean isUserIssuer() {
		return userIssuer;
	}

	public void setUserIssuer(boolean userIssuer) {
		this.userIssuer = userIssuer;
	}

	public boolean isUserIssuerDpf() {
		return userIssuerDpf;
	}

	public void setUserIssuerDpf(boolean userIssuerDpf) {
		this.userIssuerDpf = userIssuerDpf;
	}

	public String getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(String totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	public Boolean getFlagOnlyInvestors() {
		return flagOnlyInvestors;
	}

	public void setFlagOnlyInvestors(Boolean flagOnlyInvestors) {
		this.flagOnlyInvestors = flagOnlyInvestors;
	}

}
