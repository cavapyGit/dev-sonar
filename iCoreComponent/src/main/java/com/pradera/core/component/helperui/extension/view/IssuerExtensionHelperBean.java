package com.pradera.core.component.helperui.extension.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.ViewDepositaryBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.IssuerSearcherTO;
import com.pradera.core.component.helperui.to.IssuerTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.type.EconomicSectorType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.type.IssuerStateType;
import com.pradera.model.report.ReportFilter;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class IssuerExtensionHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , Sep 24, 2013
 */
@ViewDepositaryBean
public class IssuerExtensionHelperBean implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5483140460510498933L;
	
	/** The log. */
	@Inject
	private transient PraderaLogger log;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The parameters facade. */
	@EJB
	GeneralParametersFacade parametersFacade;
	
	/** The issuer searcher. */
	private IssuerSearcherTO issuerSearcher;
	
	/** The selected. */
	private IssuerTO selected = new IssuerTO();
	
	/** The list issuers. */
	private List<IssuerTO> listIssuers;
	
	/** The searched. */
	private boolean searched;
	
	/** The input filter. */
	private boolean inputFilter;
	//Combos
	
	/** The searched by code. */
	private boolean searchedByCode;
	
	/** The list empty. */
	private Boolean listEmpty;
	
	/** The list states. */
	private List<SelectItem> listStates;
	
	/** The document type list. */
	private List<SelectItem> documentTypeList;
	
	/** The economic sector list. */
	private List<SelectItem> economicSectorList;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/**
	 * Inits the.
	 *
	 * @throws ServiceException the service exception
	 */
	@PostConstruct
	public void init() throws ServiceException {
		log.info("Bean created");
		listEmpty = false;
		loadFilters();
	}
	
	/**
	 * Load filters.
	 *
	 * @throws ServiceException the service exception
	 */
	private void loadFilters() throws ServiceException {
		issuerSearcher = new IssuerSearcherTO();
		listStates 		   = new ArrayList<SelectItem>();
		documentTypeList   = new ArrayList<SelectItem>();
		economicSectorList = new ArrayList<SelectItem>();
		ParameterTableTO paramTable = new ParameterTableTO();
		paramTable.setState(ParameterTableStateType.REGISTERED.getCode());

		//LOAD ISSUER STATES
		paramTable.setMasterTableFk(MasterTableType.ISSUER_STATE.getCode());
		List<Integer> lstParameterTablePkNotIn = new ArrayList<Integer>();
		lstParameterTablePkNotIn.add(IssuerStateType.PENDING.getCode());
		paramTable.setLstParameterTablePkNotIn(lstParameterTablePkNotIn);
		for(ParameterTable param : parametersFacade.getListParameterTableServiceBean(paramTable)) {
			listStates.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
		}
		
		//LOAD ISSUER DOCUMENT TYPES
		paramTable.setMasterTableFk(MasterTableType.TYPE_DOCUMENT_IDENTITY_PERSON.getCode());
		for(ParameterTable param : parametersFacade.getListParameterTableServiceBean(paramTable)) {
			documentTypeList.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
		}
		
		//LOAD ECONOMIC SECTORS
		paramTable.setMasterTableFk(MasterTableType.ECONOMIC_SECTOR.getCode());
		for(ParameterTable param : parametersFacade.getListParameterTableServiceBean(paramTable)) {
			economicSectorList.add(new SelectItem(param.getParameterTablePk(), param.getParameterName()));
		}
	}

	/**
	 * Select issuer from helper.
	 *
	 * @return the string
	 */
	public String selectIssuerFromHelper() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (selected != null) {
			//set client isin code
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory()
			    .createValueExpression(elContext, "#{cc.attrs.issuer}", Object.class);
			//new Value
			valueExpression.setValue(elContext, selected);
			//update report filter
			ValueExpression valueExpressionReport = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.report}", ReportFilter.class);
			ReportFilter reportFilter =(ReportFilter)valueExpressionReport.getValue(elContext);
			if(reportFilter!=null) {
				reportFilter.setValue(selected);
			}
			//update caller
			ValueExpression valueExpressionName = context.getApplication().getExpressionFactory()
				    .createValueExpression(elContext, "#{cc.attrs.update}", String.class);
			String componentsId = (String)valueExpressionName.getValue(elContext);
			if(StringUtils.isNotBlank(componentsId)) {
				RequestContext.getCurrentInstance().update(CommonsUtilities.getListOfComponentsForUpdate(componentsId));
			}
			clean(null);
		} else {
			//no se encontro el Emisor
			log.error("No encontro el Emisor ");
		}
		return null;
	}
	
	/**
	 * Search issue by helper.
	 *
	 * @param event the event
	 */
	public void searchIssueByHelper(ActionEvent event) {
		searched = true;
		try {
			if(Validations.validateIsNullOrEmpty(issuerSearcher.getState())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												 PropertiesUtilities.getGenericMessage("issuer.helper.alert.state"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(Validations.validateIsNullOrEmpty(issuerSearcher.getEconomicSector())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT), 
												 PropertiesUtilities.getGenericMessage("issuer.helper.alert.economicsector"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			
			
			listIssuers = helperComponentFacade.findIssuerByHelper(issuerSearcher);
			
			if(!(listIssuers.size()>0)){
				listEmpty = true;
			}
		} catch (Exception ex) {
			// TODO: handle exception
		}
	}
	
	/**
	 * Checks if is input filters.
	 *
	 * @param filter the filter
	 * @return true, if is input filters
	 */
	private boolean isInputFilters(IssuerSearcherTO filter) {
		boolean check = false;
		if (StringUtils.isNotBlank(filter.getIssuerCode())
				|| StringUtils.isNotBlank(filter.getIssuerDesc())
				|| Validations.validateIsNotNullAndPositive(filter.getDocumentType())
				|| StringUtils.isNotBlank(filter.getDocumentNumber())
				|| StringUtils.isNotBlank(filter.getMnemonic())
				|| Validations.validateIsNotNullAndPositive(filter.getState())
				|| Validations.validateIsNotNullAndPositive(filter.getEconomicSector())){
			check = true;
		}
		return check;
	}
	
	/**
	 * Clean.
	 *
	 * @param event the event
	 */
	public void clean(ActionEvent event) {
		issuerSearcher = new IssuerSearcherTO();
		issuerSearcher.setState(IssuerStateType.REGISTERED.getCode());
		issuerSearcher.setEconomicSector(EconomicSectorType.FINANCIAL_PUBLIC_SECTOR.getCode());
		searched       = false;
		listEmpty	   = false;
		listIssuers    = null;
		selected = null;
	}
	
	/**
	 * Exit.
	 *
	 * @param event the event
	 */
	public void exit(ActionEvent event) {
		clean(event);
	}
	
	

	/**
	 * Instantiates a new issuer helper bean.
	 */
	public IssuerExtensionHelperBean(){}
	/**
	 * Gets the issuer searcher.
	 *
	 * @return the issuer searcher
	 */
	public IssuerSearcherTO getIssuerSearcher() {
		return issuerSearcher;
	}

	/**
	 * Sets the issuer searcher.
	 *
	 * @param issuerSearcher the new issuer searcher
	 */
	public void setIssuerSearcher(IssuerSearcherTO issuerSearcher) {
		this.issuerSearcher = issuerSearcher;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public IssuerTO getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected the new selected
	 */
	public void setSelected(IssuerTO selected) {
		this.selected = selected;
	}

	/**
	 * Gets the list issuers.
	 *
	 * @return the list issuers
	 */
	public List<IssuerTO> getListIssuers() {
		return listIssuers;
	}

	/**
	 * Sets the list issuers.
	 *
	 * @param lisIssuers the new list issuers
	 */
	public void setListIssuers(List<IssuerTO> lisIssuers) {
		this.listIssuers = lisIssuers;
	}

	
	/**
	 * Gets the list states.
	 *
	 * @return the list states
	 */
	public List<SelectItem> getListStates() {
		return listStates;
	}



	/**
	 * Sets the list states.
	 *
	 * @param listStates the new list states
	 */
	public void setListStates(List<SelectItem> listStates) {
		this.listStates = listStates;
	}

	/**
	 * Gets the document type list.
	 *
	 * @return the document type list
	 */
	public List<SelectItem> getDocumentTypeList() {
		return documentTypeList;
	}

	/**
	 * Sets the document type list.
	 *
	 * @param documentTypeList the new document type list
	 */
	public void setDocumentTypeList(List<SelectItem> documentTypeList) {
		this.documentTypeList = documentTypeList;
	}

	/**
	 * Checks if is searched.
	 *
	 * @return true, if is searched
	 */
	public boolean isSearched() {
		return searched;
	}
	
	/**
	 * Checks if is input filter.
	 *
	 * @return true, if is input filter
	 */
	public boolean isInputFilter() {
		return inputFilter;
	}

	/**
	 * Sets the input filter.
	 *
	 * @param inputFilter the new input filter
	 */
	public void setInputFilter(boolean inputFilter) {
		this.inputFilter = inputFilter;
	}

	/**
	 * Sets the searched.
	 *
	 * @param searched the new searched
	 */
	public void setSearched(boolean searched) {
		this.searched = searched;
	}

	/**
	 * Gets the economic sector list.
	 *
	 * @return the economic sector list
	 */
	public List<SelectItem> getEconomicSectorList() {
		return economicSectorList;
	}

	/**
	 * Sets the economic sector list.
	 *
	 * @param economicSectorList the new economic sector list
	 */
	public void setEconomicSectorList(List<SelectItem> economicSectorList) {
		this.economicSectorList = economicSectorList;
	}

	/**
	 * Checks if is searched by code.
	 *
	 * @return true, if is searched by code
	 */
	public boolean isSearchedByCode() {
		return searchedByCode;
	}

	/**
	 * Sets the searched by code.
	 *
	 * @param searchedByCode the new searched by code
	 */
	public void setSearchedByCode(boolean searchedByCode) {
		this.searchedByCode = searchedByCode;
	}

	/**
	 * Gets the list empty.
	 *
	 * @return the list empty
	 */
	public Boolean getListEmpty() {
		return listEmpty;
	}

	/**
	 * Sets the list empty.
	 *
	 * @param listEmpty the new list empty
	 */
	public void setListEmpty(Boolean listEmpty) {
		this.listEmpty = listEmpty;
	}

	/**
	 * @return the maxResultsQueryHelper
	 */
	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * @param maxResultsQueryHelper the maxResultsQueryHelper to set
	 */
	public void setMaxResultsQueryHelper(Integer maxResultsQueryHelper) {
		this.maxResultsQueryHelper = maxResultsQueryHelper;
	}
	
	
}
