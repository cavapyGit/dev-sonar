package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author USUARIO
 *
 */
public class SecurityFinancialDataHelpTO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String securityCodePk;
	
	private String uiComponentName;

	public String getSecurityCodePk() {
		return securityCodePk;
	}

	public String getUiComponentName() {
		return uiComponentName;
	}

	public void setUiComponentName(String uiComponentName) {
		this.uiComponentName = uiComponentName;
	}

	public void setSecurityCodePk(String securityCodePk) {
		this.securityCodePk = securityCodePk;
	}
}