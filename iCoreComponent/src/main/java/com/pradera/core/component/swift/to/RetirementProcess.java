package com.pradera.core.component.swift.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class RetirementProcess implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long fundOperationReturnPK;
	private String originalTRNDevolutionOperation;
	private Long fundOperationType;
	private Integer devolutionError;
	private Long mechanismOperationPK;
	private Date settlementDate;
	private Long mechanism;
	private Long modalityGroup;
	private Long benefitPaymentAllocationPK;
	private Long settlementProcessPK;
	private Integer currency;
	private Long participantBuyerPK;
	private Long participantPK;
	private String issuerPK;
	private Long bankPK;
	private Long centralBankPk;
	private BigDecimal amount;
	private Long holder;
	private String corporativeProcessDescription;


	public Long getMechanismOperationPK() {
		return mechanismOperationPK;
	}
	public void setMechanismOperationPK(Long mechanismOperationPK) {
		this.mechanismOperationPK = mechanismOperationPK;
	}
	public Long getBenefitPaymentAllocationPK() {
		return benefitPaymentAllocationPK;
	}
	public void setBenefitPaymentAllocationPK(Long benefitPaymentAllocationPK) {
		this.benefitPaymentAllocationPK = benefitPaymentAllocationPK;
	}
	public Long getSettlementProcessPK() {
		return settlementProcessPK;
	}
	public void setSettlementProcessPK(Long settlementProcessPK) {
		this.settlementProcessPK = settlementProcessPK;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public Long getParticipantBuyerPK() {
		return participantBuyerPK;
	}
	public void setParticipantBuyerPK(Long participantBuyerPK) {
		this.participantBuyerPK = participantBuyerPK;
	}
	public Long getParticipantPK() {
		return participantPK;
	}
	public void setParticipantPK(Long participantPK) {
		this.participantPK = participantPK;
	}
	public String getIssuerPK() {
		return issuerPK;
	}
	public void setIssuerPK(String issuerPK) {
		this.issuerPK = issuerPK;
	}
	public Long getBankPK() {
		return bankPK;
	}
	public void setBankPK(Long bankPK) {
		this.bankPK = bankPK;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getOriginalTRNDevolutionOperation() {
		return originalTRNDevolutionOperation;
	}
	public void setOriginalTRNDevolutionOperation(
			String originalTRNDevolutionOperation) {
		this.originalTRNDevolutionOperation = originalTRNDevolutionOperation;
	}
	public Integer getDevolutionError() {
		return devolutionError;
	}
	public void setDevolutionError(Integer devolutionError) {
		this.devolutionError = devolutionError;
	}
	public Long getFundOperationType() {
		return fundOperationType;
	}
	public void setFundOperationType(Long fundOperationType) {
		this.fundOperationType = fundOperationType;
	}
	public Long getCentralBankPk() {
		return centralBankPk;
	}
	public void setCentralBankPk(Long centralBankPk) {
		this.centralBankPk = centralBankPk;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public Long getMechanism() {
		return mechanism;
	}
	public void setMechanism(Long mechanism) {
		this.mechanism = mechanism;
	}
	public Long getModalityGroup() {
		return modalityGroup;
	}
	public void setModalityGroup(Long modalityGroup) {
		this.modalityGroup = modalityGroup;
	}
	public String getCorporativeProcessDescription() {
		return corporativeProcessDescription;
	}
	public void setCorporativeProcessDescription(
			String corporativeProcessDescription) {
		this.corporativeProcessDescription = corporativeProcessDescription;
	}
	public Long getHolder() {
		return holder;
	}
	public void setHolder(Long holder) {
		this.holder = holder;
	}
	public Long getFundOperationReturnPK() {
		return fundOperationReturnPK;
	}
	public void setFundOperationReturnPK(Long fundOperationReturnPK) {
		this.fundOperationReturnPK = fundOperationReturnPK;
	}
}