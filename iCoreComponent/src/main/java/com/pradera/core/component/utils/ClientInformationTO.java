package com.pradera.core.component.utils;

import java.io.Serializable;

public class ClientInformationTO implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private boolean viewMarriedLastName;
	private boolean viewCivilStatus;
	private boolean viewMainActivity;
	private boolean viewJobSourceBusinessName;
	private boolean viewJobDateAdmission;
	private boolean viewAppointment;
	private boolean viewJobAddress;
	private boolean viewOfficePhone;
	private boolean viewTotalIncome;
	private boolean viewPerRefFullName;
	private boolean viewPerRefLandLine;
	private boolean viewPerRefCellPhone;
	private boolean viewComerRefBusinessName;
	private boolean viewComerRefLandLine;
	private boolean viewEntityPublicAppointment;
	private boolean viewNitNaturalPerson;
	
	private boolean viewFundCompanyEnrollment;
	private boolean viewConstitutionDate;
	private boolean viewContactPhone1;
	private boolean viewContactPhone2;
	private boolean viewContactWebPage;
	private boolean viewAuthorityLegalNumber;
	
	
	public boolean isViewMarriedLastName() {
		return viewMarriedLastName;
	}
	public void setViewMarriedLastName(boolean viewMarriedLastName) {
		this.viewMarriedLastName = viewMarriedLastName;
	}
	public boolean isViewCivilStatus() {
		return viewCivilStatus;
	}
	public void setViewCivilStatus(boolean viewCivilStatus) {
		this.viewCivilStatus = viewCivilStatus;
	}
	public boolean isViewMainActivity() {
		return viewMainActivity;
	}
	public void setViewMainActivity(boolean viewMainActivity) {
		this.viewMainActivity = viewMainActivity;
	}
	public boolean isViewJobSourceBusinessName() {
		return viewJobSourceBusinessName;
	}
	public void setViewJobSourceBusinessName(boolean viewJobSourceBusinessName) {
		this.viewJobSourceBusinessName = viewJobSourceBusinessName;
	}
	public boolean isViewJobDateAdmission() {
		return viewJobDateAdmission;
	}
	public void setViewJobDateAdmission(boolean viewJobDateAdmission) {
		this.viewJobDateAdmission = viewJobDateAdmission;
	}
	public boolean isViewAppointment() {
		return viewAppointment;
	}
	public void setViewAppointment(boolean viewAppointment) {
		this.viewAppointment = viewAppointment;
	}
	public boolean isViewJobAddress() {
		return viewJobAddress;
	}
	public void setViewJobAddress(boolean viewJobAddress) {
		this.viewJobAddress = viewJobAddress;
	}
	public boolean isViewOfficePhone() {
		return viewOfficePhone;
	}
	public void setViewOfficePhone(boolean viewOfficePhone) {
		this.viewOfficePhone = viewOfficePhone;
	}
	public boolean isViewTotalIncome() {
		return viewTotalIncome;
	}
	public void setViewTotalIncome(boolean viewTotalIncome) {
		this.viewTotalIncome = viewTotalIncome;
	}
	public boolean isViewPerRefFullName() {
		return viewPerRefFullName;
	}
	public void setViewPerRefFullName(boolean viewPerRefFullName) {
		this.viewPerRefFullName = viewPerRefFullName;
	}
	public boolean isViewPerRefLandLine() {
		return viewPerRefLandLine;
	}
	public void setViewPerRefLandLine(boolean viewPerRefLandLine) {
		this.viewPerRefLandLine = viewPerRefLandLine;
	}
	public boolean isViewPerRefCellPhone() {
		return viewPerRefCellPhone;
	}
	public void setViewPerRefCellPhone(boolean viewPerRefCellPhone) {
		this.viewPerRefCellPhone = viewPerRefCellPhone;
	}
	public boolean isViewComerRefBusinessName() {
		return viewComerRefBusinessName;
	}
	public void setViewComerRefBusinessName(boolean viewComerRefBusinessName) {
		this.viewComerRefBusinessName = viewComerRefBusinessName;
	}
	public boolean isViewComerRefLandLine() {
		return viewComerRefLandLine;
	}
	public void setViewComerRefLandLine(boolean viewComerRefLandLine) {
		this.viewComerRefLandLine = viewComerRefLandLine;
	}
	
	public boolean isViewEntityPublicAppointment() {
		return viewEntityPublicAppointment;
	}
	public void setViewEntityPublicAppointment(boolean viewEntityPublicAppointment) {
		this.viewEntityPublicAppointment = viewEntityPublicAppointment;
	}
	public boolean isViewFundCompanyEnrollment() {
		return viewFundCompanyEnrollment;
	}
	public void setViewFundCompanyEnrollment(boolean viewFundCompanyEnrollment) {
		this.viewFundCompanyEnrollment = viewFundCompanyEnrollment;
	}
	public boolean isViewConstitutionDate() {
		return viewConstitutionDate;
	}
	public void setViewConstitutionDate(boolean viewConstitutionDate) {
		this.viewConstitutionDate = viewConstitutionDate;
	}
	public boolean isViewContactPhone1() {
		return viewContactPhone1;
	}
	public void setViewContactPhone1(boolean viewContactPhone1) {
		this.viewContactPhone1 = viewContactPhone1;
	}
	public boolean isViewContactPhone2() {
		return viewContactPhone2;
	}
	public void setViewContactPhone2(boolean viewContactPhone2) {
		this.viewContactPhone2 = viewContactPhone2;
	}
	public boolean isViewContactWebPage() {
		return viewContactWebPage;
	}
	public void setViewContactWebPage(boolean viewContactWebPage) {
		this.viewContactWebPage = viewContactWebPage;
	}
	
	public boolean isViewAuthorityLegalNumber() {
		return viewAuthorityLegalNumber;
	}
	public void setViewAuthorityLegalNumber(boolean viewAuthorityLegalNumber) {
		this.viewAuthorityLegalNumber = viewAuthorityLegalNumber;
	}
	public boolean isViewNitNaturalPerson() {
		return viewNitNaturalPerson;
	}
	public void setViewNitNaturalPerson(boolean viewNitNaturalPerson) {
		this.viewNitNaturalPerson = viewNitNaturalPerson;
	}
	
	
	
	
}
