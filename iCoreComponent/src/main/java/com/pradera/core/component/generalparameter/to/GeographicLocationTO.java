package com.pradera.core.component.generalparameter.to;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2013.</li>
 * </ul>
 * 
 * The Class GeographicLocationTO.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 25/01/2013
 */
public class GeographicLocationTO implements Serializable{
	
	/** The geographic location type. */
	private Integer geographicLocationType;
	
	/** The state. */
	private Integer state;
	
	/** The id location reference fk. */
	private Integer idLocationReferenceFk;
	
	/** The country name. */
	private String countryName;
	
	/** The id geographic location pk. */
	private Integer idGeographicLocationPk;
	
	/** The indicator Local Country. */
	private Integer indLocalCountry;
	
	/** The indicator Local Country. */
	private String geographicLocationCode;
	
	/**
	 * Gets the country name.
	 *
	 * @return the country name
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * Sets the country name.
	 *
	 * @param countryName the new country name
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(Integer state) {
		this.state = state;
	}
	
	/**
	 * Gets the geographic location type.
	 *
	 * @return the geographic location type
	 */
	public Integer getGeographicLocationType() {
		return geographicLocationType;
	}
	
	/**
	 * Sets the geographic location type.
	 *
	 * @param geographicLocationType the new geographic location type
	 */
	public void setGeographicLocationType(Integer geographicLocationType) {
		this.geographicLocationType = geographicLocationType;
	}
	
	/**
	 * Gets the id location reference fk.
	 *
	 * @return the id location reference fk
	 */
	public Integer getIdLocationReferenceFk() {
		return idLocationReferenceFk;
	}
	
	/**
	 * Sets the id location reference fk.
	 *
	 * @param idLocationReferenceFk the new id location reference fk
	 */
	public void setIdLocationReferenceFk(Integer idLocationReferenceFk) {
		this.idLocationReferenceFk = idLocationReferenceFk;
	}

	/**
	 * Gets the id geographic location pk.
	 *
	 * @return the id geographic location pk
	 */
	public Integer getIdGeographicLocationPk() {
		return idGeographicLocationPk;
	}

	/**
	 * Sets the id geographic location pk.
	 *
	 * @param idGeographicLocationPk the new id geographic location pk
	 */
	public void setIdGeographicLocationPk(Integer idGeographicLocationPk) {
		this.idGeographicLocationPk = idGeographicLocationPk;
	}
	/**
	 * Get Indicator Local Country
	 * @return Indicator Local Country
	 */
	public Integer getIndLocalCountry() {
		return indLocalCountry;
	}

	/**
	 * Set Indicator Local Country
	 * @param indLocalCountry Indicator Local Country
	 */
	public void setIndLocalCountry(Integer indLocalCountry) {
		this.indLocalCountry = indLocalCountry;
	}

	public String getGeographicLocationCode() {
		return geographicLocationCode;
	}

	public void setGeographicLocationCode(String geographicLocationCode) {
		this.geographicLocationCode = geographicLocationCode;
	}
	
}
