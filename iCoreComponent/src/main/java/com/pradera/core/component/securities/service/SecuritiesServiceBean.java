package com.pradera.core.component.securities.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.logging.PraderaLogger;
import com.pradera.commons.utils.CommonsUtilities;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.core.component.business.to.SecurityTO;
import com.pradera.core.component.corporateevents.service.CorporateEventComponentServiceBean;
import com.pradera.core.component.daogeneric.service.CrudDaoServiceBean;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.service.ParameterServiceBean;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.integration.common.type.BooleanType;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.contextholder.LoggerUser;
import com.pradera.integration.exception.ServiceException;
import com.pradera.integration.exception.type.ErrorServiceType;
import com.pradera.model.accounts.InternationalDepository;
import com.pradera.model.accounts.Participant;
import com.pradera.model.corporatives.CorporativeEventType;
import com.pradera.model.corporatives.CorporativeOperation;
import com.pradera.model.corporatives.type.AccordType;
import com.pradera.model.corporatives.type.CorporateProcessStateType;
import com.pradera.model.corporatives.type.DeliveryPlacementType;
import com.pradera.model.corporatives.type.ImportanceEventType;
import com.pradera.model.custody.securitiesannotation.AccountAnnotationOperation;
import com.pradera.model.custody.type.DematerializationCertificateMotiveType;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.AmoPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.AmortizationPaymentSchedule;
import com.pradera.model.issuancesecuritie.CfiCodeGenerator;
import com.pradera.model.issuancesecuritie.IntPaymentScheduleReqHi;
import com.pradera.model.issuancesecuritie.InterestPaymentSchedule;
import com.pradera.model.issuancesecuritie.ProgramAmoCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramAmortizationCoupon;
import com.pradera.model.issuancesecuritie.ProgramIntCouponReqHi;
import com.pradera.model.issuancesecuritie.ProgramInterestCoupon;
import com.pradera.model.issuancesecuritie.Security;
import com.pradera.model.issuancesecuritie.SecurityClassSetup;
import com.pradera.model.issuancesecuritie.SecurityForeignDepository;
import com.pradera.model.issuancesecuritie.SecurityForeignDepositoryPK;
import com.pradera.model.issuancesecuritie.SecurityHistoryBalance;
import com.pradera.model.issuancesecuritie.SecurityNegotiationMechanism;
import com.pradera.model.issuancesecuritie.SecuritySerialRange;
import com.pradera.model.issuancesecuritie.SecuritySplitCoupon;
import com.pradera.model.issuancesecuritie.type.CapitalPaymentModalityType;
import com.pradera.model.issuancesecuritie.type.InterestType;
import com.pradera.model.issuancesecuritie.type.PaymentScheduleReqRegisterType;
import com.pradera.model.issuancesecuritie.type.ProgramScheduleStateType;
import com.pradera.model.issuancesecuritie.type.SecurityClassType;
import com.pradera.model.issuancesecuritie.type.SecurityForeignDepositoryStateType;
import com.pradera.model.issuancesecuritie.type.SecurityInversionistStateType;
import com.pradera.model.issuancesecuritie.type.SecurityStateType;
import com.pradera.model.issuancesecuritie.type.SecurityType;
import com.pradera.model.negotiation.MechanismModality;
import com.pradera.model.negotiation.MechanismModalityPK;
import com.pradera.model.negotiation.NegotiationMechanism;
import com.pradera.model.negotiation.NegotiationModality;

// TODO: Auto-generated Javadoc
/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2014.</li>
 * </ul>
 * The Class SecuritiesServiceBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 28/03/2014
 */
@Stateless
public class SecuritiesServiceBean extends CrudDaoServiceBean {

	/** The corporate event component service bean. */
	@EJB
	private CorporateEventComponentServiceBean corporateEventComponentServiceBean;

	@EJB
	ParameterServiceBean parameterServiceBean;
	
	@Inject 
	@Configurable 
	String idIssuerBC;
	
	@Inject 
	@Configurable 
	String idIssuerTGN;
	
	@Inject 
	@Configurable 
	String mnemonicIssuerTGN;
	
	@Inject 
	@Configurable 
	String mnemonicIssuerBC;
	
	/** The general parameters facade. */
	@Inject
	private GeneralParametersFacade generalParametersFacade;
	
	/** The log. */
	@Inject
	private PraderaLogger log;
	
	/**
	 * Security isin generate.
	 *
	 * @param security the security
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String securityIsinGenerate(Security security) throws ServiceException {
		StringBuilder strbIsin = new StringBuilder();
		String correlativeByIssuer = getSecuritiesCorrelativeByIssuerServiceBean(security
				.getIssuer().getIdIssuerPk());
		
		strbIsin.append(security.getIssuer().getIdIssuerPk().substring(0, 2)); // country (2)
																			
		strbIsin.append(security.getIssuer().getMnemonic().substring(0, 3)); // issuer mnemonic (3)
		
		strbIsin.append(correlativeByIssuer); // (6)
		
		strbIsin.append(checkDigitGenerate(strbIsin.toString())); //Check Digit (1)
		return strbIsin.toString();
	}
	
	public String securityBcbCodeGenerate(Security security) throws ServiceException{
		if(security.getIssuer().getMnemonic().equals(mnemonicIssuerBC) || 
				security.getIssuer().getMnemonic().equals(mnemonicIssuerTGN)){
			StringBuilder strbSecurityBcbCode=new StringBuilder();
			ParameterTable pTable =(ParameterTable) parameterServiceBean.getParameterDetail(security.getSecurityClass());
			if(Validations.validateIsNotNullAndNotEmpty( pTable.getText3() )){
				strbSecurityBcbCode.append(GeneralConstants.BRACKET_OPEN);
				strbSecurityBcbCode.append( pTable.getText3() );
				strbSecurityBcbCode.append(GeneralConstants.BRACKET_CLOSE);
				strbSecurityBcbCode.append( security.getIdSecurityCodeOnly());
				strbSecurityBcbCode.append("-000");
				return strbSecurityBcbCode.toString();
			}	
		}
		return null;
	}

	/**
	 * Alternative code generate.
	 *
	 * @param security the security
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String alternativeCodeGenerate(Security security)
			throws ServiceException {
		StringBuilder sbAlternateCode = new StringBuilder();
		DateFormat df = new SimpleDateFormat(GeneralConstants.DATE_FORMAT_PATTERN);
		String date = df.format(security.getIssuanceDate());
		if (security.isFixedInstrumentType()) {

			sbAlternateCode.append(security.getIssuer().getMnemonic());
			sbAlternateCode.append(security.getMnemonic()!=null?security.getMnemonic():"");
			sbAlternateCode.append(GeneralConstants.UNDERLINE_SEPARATOR);
			sbAlternateCode.append(date.replaceAll("/", ""));
			sbAlternateCode.append(GeneralConstants.UNDERLINE_SEPARATOR);
			// dias al vencimiento
			Calendar calIssuanceDate = Calendar.getInstance();
			calIssuanceDate.setTime(security.getIssuanceDate());
			Calendar calExpirationDate = Calendar.getInstance();
			calExpirationDate.setTime(security.getExpirationDate());

			DateTime start = new DateTime().withDate(
					calIssuanceDate.get(Calendar.YEAR),
					calIssuanceDate.get(Calendar.MONTH) + 1,
					calIssuanceDate.get(Calendar.DAY_OF_MONTH));
			DateTime end = new DateTime().withDate(
					calExpirationDate.get(Calendar.YEAR),
					calExpirationDate.get(Calendar.MONTH) + 1,
					calExpirationDate.get(Calendar.DAY_OF_MONTH));
			Period p = new Period(start, end, PeriodType.days());
			Integer days = p.getDays();
			sbAlternateCode.append(days.toString());

			sbAlternateCode.append(GeneralConstants.UNDERLINE_SEPARATOR);
			sbAlternateCode.append((int) security.getInitialNominalValue()
					.doubleValue() / 1000);
		} else if (security.isVariableInstrumentType()) {
			sbAlternateCode.append(security.getIssuer().getMnemonic());
			sbAlternateCode.append(security.getMnemonic());
			sbAlternateCode.append(GeneralConstants.UNDERLINE_SEPARATOR);
			sbAlternateCode.append(date);
			sbAlternateCode.append(GeneralConstants.UNDERLINE_SEPARATOR);
			sbAlternateCode.append((int) security.getInitialNominalValue()
					.doubleValue() / 1000);
		}
		return sbAlternateCode.toString();
	}
	
	/**
	 * Gets the cfi code generator service bean.
	 *
	 * @param cfiCodeGen the cfi code gen
	 * @return the cfi code generator service bean
	 * @throws ServiceException the service exception
	 */
	public String getCfiCodeGeneratorServiceBean(CfiCodeGenerator cfiCodeGen) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		Map<String,Object> parameters = new HashMap<String, Object>();
		sbQuery.append(" select cfiCode from CfiCodeGenerator cfiCode " );
		sbQuery.append("where 1 = 1 ");
		sbQuery.append("and cfiCode.securityClass = :securityClass");
		parameters.put("securityClass",cfiCodeGen.getSecurityClass());
		if(cfiCodeGen.getSecurityTerm()!=null){
			sbQuery.append(" and cfiCode.securityTerm = :securityTerm");
			parameters.put("securityTerm",cfiCodeGen.getSecurityTerm());
		}else{
			sbQuery.append(" and cfiCode.securityTerm is null");
		}
		if(cfiCodeGen.getInterestType()!=null){
			sbQuery.append(" and cfiCode.interestType = :interestType ");
			parameters.put("interestType",cfiCodeGen.getInterestType());
		}else{
			sbQuery.append(" and cfiCode.interestType is null");
		}
		if(cfiCodeGen.getInstrumentType()!=null){
			sbQuery.append(" and cfiCode.instrumentType = :instrumentType ");
			parameters.put("instrumentType",cfiCodeGen.getInstrumentType());
		}else{
			sbQuery.append(" and cfiCode.instrumentType is null");
		}
		if(cfiCodeGen.getIndEarlyRedemption()!=null){
			sbQuery.append(" and cfiCode.indEarlyRedemption = :indEarlyRedemption ");
			parameters.put("indEarlyRedemption",cfiCodeGen.getIndEarlyRedemption());
		}else{
			sbQuery.append(" and cfiCode.indEarlyRedemption is null");
		}
		if(cfiCodeGen.getCapitalPaymentModality()!=null){
			sbQuery.append(" and cfiCode.capitalPaymentModality = :capitalPaymentModality ");
			parameters.put("capitalPaymentModality",cfiCodeGen.getCapitalPaymentModality());
		}else{
			sbQuery.append(" and cfiCode.capitalPaymentModality is null");
		}
//		System.out.println("****************************************************");
//		System.out.println("CLASE DE VALOR      : "+cfiCodeGen.getSecurityClass());
//		System.out.println("TERMINO DE VALOR    : "+cfiCodeGen.getSecurityTerm());
//		System.out.println("TIPO DE INTERES     : "+cfiCodeGen.getInterestType());
//		System.out.println("TIPO DE INSTRUMENTO : "+cfiCodeGen.getInstrumentType());
//		System.out.println("IND REDENCION ANT   : "+cfiCodeGen.getIndEarlyRedemption());
//		System.out.println("FORMA DE PAGO       : "+cfiCodeGen.getCapitalPaymentModality());
//		System.out.println("****************************************************");
		sbQuery.append(" Order by Id_Cfi_Code_Pk desc");
		List<CfiCodeGenerator> codeGeneratorResultList = null;
		CfiCodeGenerator codeGeneratorResult = new CfiCodeGenerator();
		try {
			codeGeneratorResultList =  findListByQueryString(sbQuery.toString(), parameters);
			if(codeGeneratorResultList.size()>0)
				codeGeneratorResult = codeGeneratorResultList.get(0);
			else
				return null;
			//else 
			//	throw new ServiceException(ErrorServiceType.SECURITY_CFI_CODE_NO_GENERATE);
		}  catch (NoResultException ex) {
			throw new ServiceException(ErrorServiceType.SECURITY_CFI_CODE_NO_GENERATE);
		}
		StringBuilder strBuilder=new StringBuilder();
		strBuilder.append( codeGeneratorResult.getIdCategoryFk() );
		strBuilder.append( codeGeneratorResult.getIdGroupFk() );
		strBuilder.append( codeGeneratorResult.getIdAttribute_1_fk());
		strBuilder.append( codeGeneratorResult.getIdAttribute_2_fk());
		strBuilder.append( codeGeneratorResult.getIdAttribute_3_fk());
		strBuilder.append( codeGeneratorResult.getIdAttribute_4_fk());
		
		return strBuilder.toString();
	}
	/**
	 * Obtiene el nombre el detalle de un valor en forma corta.
	 * @author hcoarite
	 * @param Security,IsIssuance
	 * @return retorna el SHORT CFI CODE
	 * @throws ServiceException the service exception
	 */
	public String getShortCfiCode(Security security) throws ServiceException{
		
		try {
			//Map<String,Object> parameters=new HashMap<String, Object>();
			//parameters.put("idIssuerPkParam", security.getIssuer().getIdIssuerPk());
			
			//ParameterTable pTable=parameterServiceBean.getParameterTableById(security.getSecurityClass() );
			String issuerMnemonic=security.getIssuer().getMnemonic();
			
			StringBuilder shortCfiCode = new StringBuilder();
			shortCfiCode.append(issuerMnemonic);
			shortCfiCode.append(GeneralConstants.SLASH);
			/*ACE, ACC*/
			if(isActions(security.getSecurityClass())){
			   shortCfiCode.append(getDescSecurityClass(security.getSecurityClass()));//Nemonico de clases de valor 
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   //ACE : CON VOTO Y ORDINARIO
			   if(security.getSecurityClass().equals(SecurityClassType.ACE.getCode())){
				   /*Todos con derecho a voto*/
				   shortCfiCode.append("V");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE); 
				   //S : ORDINARIO
				   shortCfiCode.append("S");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }
			   
			   if(security.getSecurityClass().equals(SecurityClassType.ACC.getCode())){
				   /* TODOS CON DERCHO A VOTO y CLASE DE VALORACION*/
				   shortCfiCode.append("V");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE); 
				   //S : ORDINARIAS  
				   shortCfiCode.append("S");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }
			   if(security.getSecurityClass().equals(SecurityClassType.ACC_RF.getCode())){
				   /* TODOS CON DERCHO A VOTO y CLASE DE VALORACION*/
				   shortCfiCode.append("N");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE); 
				   //P: PREFERENTES
				   shortCfiCode.append("P");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }
			   if(security.getSecurityClass().equals(SecurityClassType.ANR.getCode())){
				   /* TODOS CON DERCHO A VOTO y CLASE DE VALORACION*/
				   shortCfiCode.append("V");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE); 
				   //S: 
				   shortCfiCode.append("S");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }
			  /*Clave de valor*/
			  shortCfiCode.append(security.getIdSecurityCodeOnly());//Clave de valor
			  shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			  /*Valor nominal*/
			  shortCfiCode.append(getNominalValue(security.getInitialNominalValue()));
			  return shortCfiCode.toString();
			}
			/*CFC*/
			if(isParticipationFees(security.getSecurityClass())){
			  shortCfiCode.append(getDescSecurityClass(security.getSecurityClass()));//Nemonico de clases de valor 
			  shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			  shortCfiCode.append(security.getIdSecurityCodeOnly());//Clave de valor
		      shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			  shortCfiCode.append(getNominalValue(security.getInitialNominalValue()));
			  return shortCfiCode.toString();
			}
			/*Letras del tesoro*/
			if(isLettersCouponsCertificates(security.getSecurityClass())){
			   shortCfiCode.append(getNominalValue(security.getInitialNominalValue()));
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Columna descripsion*/
			   shortCfiCode.append(getDescSecurityClass(security.getSecurityClass()));//desc LTS,.... 
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Fecha de Vencimiento*/
			   String date= CommonsUtilities.convertDateToString(security.getExpirationDate(),CommonsUtilities.DATE_PATTERN_BILLING);
			   shortCfiCode.append(date);
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Clase G : Redension anticipada, F : Redimible a vencimiento*/
			   if(!security.getSecurityClass().equals(SecurityClassType.CDS.getCode())){
				   String classRedemtion = security.getIndEarlyRedemption().equals(new Integer("1"))?"G":"F";
				   shortCfiCode.append(classRedemtion);
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }else{
				   shortCfiCode.append("F");
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }
			   /*Restricciones*/
			   if(!security.getSecurityClass().equals(SecurityClassType.CDS.getCode())){
				   if(security.getSecurityClass().equals(SecurityClassType.LBS.getCode()))
					   shortCfiCode.append(getInterestTypeChar(security.getInterestType()));
				   else
					   shortCfiCode.append("Z"); 
			   }else{
				   shortCfiCode.append(getInterestTypeChar(security.getInterestType())); 
			   }
			   return shortCfiCode.toString();
			}
			/*Bonos y otras series */
			if(isBondsOthersSeries(security.getSecurityClass())){
			   shortCfiCode.append(security.getInterestRate().setScale(2, RoundingMode.HALF_UP).toString());//Tasa
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Descripsion*/
			   shortCfiCode.append(getDescSecurityClass(security.getSecurityClass()));//Clave de valor
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Fecha de Vencimiento*/
			   String date= CommonsUtilities.convertDateToString(security.getExpirationDate(),CommonsUtilities.DATE_PATTERN_BILLING);
			   shortCfiCode.append(date);
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Redension anticipada*/
			   String classRedemtion = security.getIndEarlyRedemption().equals(new Integer("1"))?"G":"F";
			   shortCfiCode.append(classRedemtion);
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Serie*/
			   if(Validations.validateIsNotNullAndNotEmpty(security.getSecuritySerial())){
				   shortCfiCode.append(security.getSecuritySerial());
				   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   }
			   /*Restricciones*/
			   shortCfiCode.append(getInterestTypeChar(security.getInterestType()));
			   return shortCfiCode.toString();  
			}
			if(isOthers(security.getSecurityClass())){
			   /*Tasa*/
			   shortCfiCode.append(security.getInterestRate().setScale(2, RoundingMode.HALF_UP).toString());//Tasa
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Descripsion*/
			   shortCfiCode.append(getDescSecurityClass(security.getSecurityClass()));//Clave de valor
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Fecha de Vencimiento*/
			   String date= CommonsUtilities.convertDateToString(security.getExpirationDate(),CommonsUtilities.DATE_PATTERN_BILLING);
			   shortCfiCode.append(date);
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   
			   String classRedemtion = security.getIndEarlyRedemption().equals(new Integer("1"))?"G":"F";
			   shortCfiCode.append(classRedemtion);
			   shortCfiCode.append(GeneralConstants.BLANK_SPACE);
			   /*Restricciones*/
			   if(security.getSecurityClass().equals(SecurityClassType.BMS.getCode())||
			     security.getSecurityClass().equals(SecurityClassType.BCP.getCode())||
				 security.getSecurityClass().equals(SecurityClassType.BBC.getCode())||
				 security.getSecurityClass().equals(SecurityClassType.BTS.getCode()))
				  	shortCfiCode.append(getInterestTypeChar(security.getInterestType()));
			    else
			    	shortCfiCode.append("F");
			   return shortCfiCode.toString();
			}
		}
		catch (Exception ex) {
			throw new ServiceException(ErrorServiceType.SECURITY_SHORT_CFI_CODE_NO_GENERATE,"No se pudo Generar FSIN");
		} 	
		return null;
	}
	private String getDescSecurityClass(Integer securityClass){
		ParameterTable parameterTable= find(ParameterTable.class,securityClass);
		return parameterTable.getText1();
	}
	//*Acciones*/
	private boolean isActions(Integer securityClass){
		if(securityClass.equals(SecurityClassType.ACE.getCode())||
		   securityClass.equals(SecurityClassType.ACC.getCode())||
		   securityClass.equals(SecurityClassType.ACC_RF.getCode())||
		   securityClass.equals(SecurityClassType.ANR.getCode()))
			return true;
		else
			return false;
	}
	/*Cuotas de participacion*/
	private boolean isParticipationFees(Integer securityClass){
		if(securityClass.equals(SecurityClassType.CFC.getCode()))
			return true;
		else
			return false;
	}
	/*Letras, cupones y certificados de deposito*/
	private boolean isLettersCouponsCertificates(Integer securityClass){
		if(securityClass.equals(SecurityClassType.LTS.getCode())||
		   securityClass.equals(SecurityClassType.LBS.getCode())||
		   securityClass.equals(SecurityClassType.LRS.getCode())||
		   securityClass.equals(SecurityClassType.CUP.getCode())||
		   securityClass.equals(SecurityClassType.CDS.getCode()))
			return true;
		else
			return false;
	}
	/*Bonos y otros series*/
	private boolean isBondsOthersSeries(Integer securityClass){
		if(securityClass.equals(SecurityClassType.BLP.getCode())||
		   securityClass.equals(SecurityClassType.BBB.getCode())||
		   securityClass.equals(SecurityClassType.VTD.getCode())||
	       securityClass.equals(SecurityClassType.PGB.getCode())||
		   securityClass.equals(SecurityClassType.BPB.getCode())||
		   securityClass.equals(SecurityClassType.BMS.getCode())||
		   securityClass.equals(SecurityClassType.BCP.getCode())||
		   securityClass.equals(SecurityClassType.BBC.getCode()))
			return true;
		else
			return false;
	}
	/*Otros*/
	private boolean isOthers (Integer securityClass) {
		if(securityClass.equals(SecurityClassType.BRS.getCode())||
		   securityClass.equals(SecurityClassType.BTS.getCode())||
		   securityClass.equals(SecurityClassType.BBS.getCode())){
		   //securityClass.equals(SecurityClassType.BLP.getCode())*/){
			return true;
		}
		else
			return false;
	}
	private String getInterestTypeChar(Integer interestType){
		 if(interestType.equals(InterestType.FIXED.getCode()))
			   return "F"; 
		   if(interestType.equals(InterestType.CERO.getCode()))
			   return "Z";
		   if(interestType.equals(InterestType.VARIABLE.getCode()))
			   return "V";
		 return "X";
	}
	private BigDecimal getNominalValue(BigDecimal initialNominalValue){
		return initialNominalValue.setScale(2,RoundingMode.HALF_UP);
	}
	
	
	/**
	 * Gets the securities correlative by issuer service bean.
	 *
	 * @param idIssuerPkPrm the id issuer pk prm
	 * @return the securities correlative by issuer service bean
	 * @throws ServiceException the service exception
	 */
	public String getSecuritiesCorrelativeByIssuerServiceBean(
			String idIssuerPkPrm) throws ServiceException {
		int count = getSecuritiesCountServiceBean(idIssuerPkPrm);
		count++;
		
		String correlativeFormat=String.format("%06d", count);
		return correlativeFormat;
	}

	/**
	 * Gets the securities count service bean.
	 *
	 * @param idIssuerPkPrm the id issuer pk prm
	 * @return the securities count service bean
	 * @throws ServiceException the service exception
	 */
	public int getSecuritiesCountServiceBean(String idIssuerPkPrm)
			throws ServiceException {
		
	//	StringBuilder strSql=new StringBuilder("Select max(right(s.idIsinCode,6)) from Security s where s.issuer.idIssuerPk=:idIssuerPrm");
		StringBuilder strSql=new StringBuilder("SELECT MAX(SUBSTR(ID_ISIN_CODE,LENGTH(ID_ISIN_CODE)-6,6)) FROM SECURITY WHERE ID_ISSUER_FK=:idIssuerPrm");
		Query query = em.createNativeQuery(strSql.toString());  
		
		query.setParameter("idIssuerPrm", idIssuerPkPrm);
		Integer result=null;
		try {
			result=Integer.parseInt( query.getSingleResult().toString() );
		} catch (Exception e) {
			e.printStackTrace();
			result=GeneralConstants.ZERO_VALUE_INTEGER;
		}
		
		
		return result.intValue();
		/*
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Security> securityRoot = cq.from(Security.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();
		Expression<Long> count = cb.count(securityRoot);
		cq.select(count);

		criteriaParameters.add(cb.equal(securityRoot.<String> get("issuer")
				.get("idIssuerPk"), idIssuerPkPrm));

		if (criteriaParameters.size() == 0) {
			throw new RuntimeException("no criteria");
		} else if (criteriaParameters.size() == 1) {
			cq.where(criteriaParameters.get(0));
		} else {
			cq.where(cb.and(criteriaParameters
					.toArray(new Predicate[criteriaParameters.size()])));
		}

		Long countCriteria = em.createQuery(cq).getSingleResult();
		return countCriteria.intValue();
		*/
	}
	
	public int getIsinMax(String idSecurityCode, Integer formatCorrelative)throws ServiceException {
		StringBuilder strSql=new StringBuilder();
		if(GeneralConstants.TWO_VALUE_INTEGER.equals(formatCorrelative))
			strSql.append("SELECT MAX(SUBSTR(id_security_code_pk,LENGTH(id_security_code_pk)-1,2)) FROM SECURITY WHERE id_security_code_pk like :idSecurityCodePk");
		else 
			strSql.append("SELECT MAX(SUBSTR(id_security_code_pk,LENGTH(id_security_code_pk)-2,3)) FROM SECURITY WHERE id_security_code_pk like :idSecurityCodePk");
		
		Query query = em.createNativeQuery(strSql.toString());  
		
		query.setParameter("idSecurityCodePk", idSecurityCode + GeneralConstants.PERCENTAGE_CHAR);
		Integer result=null;
		try {
			result=Integer.parseInt( query.getSingleResult().toString() );
		} catch (Exception e) {
			e.printStackTrace();
			result=GeneralConstants.ZERO_VALUE_INTEGER;
		}
		
		return result.intValue();
	}

	/**
	 * Check digit generate.
	 *
	 * @param strbIsinCode the strb isin code
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String checkDigitGenerate(String strbIsinCode)
			throws ServiceException {
		StringBuilder isinCode = new StringBuilder(strbIsinCode);
		BigDecimal isinCodeDecimal = null;
		BigDecimal sum = new BigDecimal(BigInteger.ZERO);
		BigDecimal factor = new BigDecimal(2);
		BigDecimal checkDigit = new BigDecimal(BigInteger.ZERO);

		for (int i = 0; i < isinCode.length(); i++) {
			char character = isinCode.charAt(i);
			if (Character.isLetter(character)) {
				int asiiCode = (int) character;
				asiiCode = asiiCode - 55;
				isinCode.replace(i, i + 1, String.valueOf(asiiCode));
			}
		}
		isinCodeDecimal = new BigDecimal(isinCode.toString());

		while (isinCodeDecimal.compareTo(BigDecimal.ZERO) != 0) {
			BigDecimal currentDigit = isinCodeDecimal.remainder(BigDecimal.TEN);
			isinCodeDecimal = isinCodeDecimal.divide(BigDecimal.TEN,
					BigDecimal.ROUND_FLOOR);
			if (currentDigit.multiply(factor).compareTo(BigDecimal.TEN) == -1) {
				sum = sum.add(currentDigit.multiply(factor));
			} else {
				BigDecimal multiplyCurrentDigit = currentDigit.multiply(factor);
				while (multiplyCurrentDigit.compareTo(BigDecimal.ZERO) != 0) {
					BigDecimal individualDigit = multiplyCurrentDigit
							.remainder(BigDecimal.TEN);
					multiplyCurrentDigit = multiplyCurrentDigit.divide(
							BigDecimal.TEN, BigDecimal.ROUND_FLOOR);
					sum = sum.add(individualDigit);
				}
			}
			if (factor.compareTo(BigDecimal.valueOf(2)) == 0)
				factor = new BigDecimal(1);
			else
				factor = new BigDecimal(2);
		}
		checkDigit = sum.remainder(BigDecimal.TEN);
		if (checkDigit.compareTo(BigDecimal.ZERO) == 0) {
			return "0";
		}
		checkDigit = BigDecimal.TEN.subtract(checkDigit);
		return checkDigit.toString();
	}

	/**
	 * Method to get the list of interest coupons pending to be paid.
	 *
	 * @param idIsinCode the id isin code
	 * @return List<ProgramInterestCoupon>
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramInterestCoupon> getListProgramInterestCoupon(
			String idIsinCode) {
		List<ProgramInterestCoupon> lstProgramInterestCoupon = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT PIC FROM ProgramInterestCoupon PIC ");
		stringBuffer
				.append(" WHERE PIC.interestPaymentSchedule.security.idIsinCodePk= :idIsinCode ");
		stringBuffer
				.append(" and PIC.stateProgramInterest = :stateProgramInterest ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", idIsinCode);
		query.setParameter("stateProgramInterest",
				ProgramScheduleStateType.PENDING.getCode());
		lstProgramInterestCoupon = query.getResultList();
		return lstProgramInterestCoupon;
	}

	/**
	 * Gets the list program amortization coupon.
	 *
	 * @param idIsinCode the id isin code
	 * @return the list program amortization coupon
	 */
	@SuppressWarnings("unchecked")
	public List<ProgramAmortizationCoupon> getListProgramAmortizationCoupon(
			String idIsinCode) {
		List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT PAC FROM ProgramAmortizationCoupon PAC ");
		stringBuffer
				.append(" WHERE PAC.interestPaymentSchedule.security.idIsinCodePk= :idIsinCode ");
		stringBuffer
				.append(" and PAC.stateProgramInterest = :stateProgramInterest ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", idIsinCode);
		query.setParameter("stateProgramInterest",
				ProgramScheduleStateType.PENDING.getCode());
		lstProgramAmortizationCoupon = query.getResultList();
		return lstProgramAmortizationCoupon;
	}

	/**
	 * Gets the program interest coupon.
	 *
	 * @param idProgramInterestCoupon the id program interest coupon
	 * @return the program interest coupon
	 */
	public ProgramInterestCoupon getProgramInterestCoupon(
			Long idProgramInterestCoupon) {
		ProgramInterestCoupon objProgramInterestCoupon = null;
		try {
			objProgramInterestCoupon = this.find(ProgramInterestCoupon.class,
					idProgramInterestCoupon);
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objProgramInterestCoupon;
	}

	/**
	 * Gets the securities by id.
	 *
	 * @param idIsinCode the id isin code
	 * @return the securities by id
	 */
	public Security getSecuritiesById(String idIsinCode) {
		Security objSecurity = null;
		try {
			objSecurity = this.find(Security.class, idIsinCode);
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return objSecurity;
	}

	/**
	 * Gets the list security negotiation mechanism.
	 *
	 * @param idIsinCode the id isin code
	 * @return the list security negotiation mechanism
	 */
	public List<SecurityNegotiationMechanism> getListSecurityNegotiationMechanism(
			String idIsinCode) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSecurityCodePkPrm", idIsinCode);
		parameters.put("statePrm", BooleanType.YES.getCode());
		@SuppressWarnings("unchecked")
		List<SecurityNegotiationMechanism> lstSecurityNegotiationMechanisms = (List<SecurityNegotiationMechanism>) findWithNamedQuery(
				SecurityNegotiationMechanism.SEC_NEGOTIATION_MECHANISM_BY_PARAMS,
				parameters);
		return lstSecurityNegotiationMechanisms;
	}

	/**
	 * Gets the list security foreign depositories.
	 *
	 * @param idIsinCode the id isin code
	 * @return the list security foreign depositories
	 */
	public List<SecurityForeignDepository> getListSecurityForeignDepositories(
			String idIsinCode) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idSecurityCodePrm", idIsinCode);
		parameters.put("statePrm",
				SecurityForeignDepositoryStateType.REGISTERED.getCode());
		@SuppressWarnings("unchecked")
		List<SecurityForeignDepository> lstSecurityForeignDepositories = (List<SecurityForeignDepository>) findWithNamedQuery(
				SecurityForeignDepository.SEARCH_BY_SECURITY_AND_STATE,
				parameters);
		return lstSecurityForeignDepositories;
	}

	/**
	 * Update split securities description.
	 *
	 * @param idIsinCode the id isin code
	 * @param strDescription the str description
	 */
	public void updateSplitSecuritiesDescription(String idIsinCode,String strDescription) {
		Security security = em.find(Security.class, idIsinCode);
		security.setDescription(strDescription);
		
		try {
			update(security);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
//		
//		StringBuffer stringBuffer = new StringBuffer();
//		stringBuffer
//				.append(" UPDATE Security set description = :strDescription ");
//		stringBuffer.append(" WHERE idIsinCodePk = :idIsinCode ");
//		Query query = em.createQuery(stringBuffer.toString());
//		query.setParameter("strDescription", strDescription);
//		query.setParameter("idIsinCode", idIsinCode);
//		query.executeUpdate();
	}

	/**
	 * Update split parent securities.
	 *
	 * @param idIsinCode the id isin code
	 * @param loggerUser the logger user
	 */
	public void updateSplitParentSecurities(String idIsinCode,
			LoggerUser loggerUser) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer
				.append(" UPDATE Security set indHasSplitSecurities = :indHasSplitSecurities ");
		stringBuffer.append(" , lastModifyApp = :lastModifyApp ");
		stringBuffer.append(" , lastModifyDate = :lastModifyDate ");
		stringBuffer.append(" , lastModifyIp = :lastModifyIp ");
		stringBuffer.append(" , lastModifyUser = :lastModifyUser ");
		stringBuffer.append(" WHERE idIsinCodePk = :idIsinCode ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", idIsinCode);
		query.setParameter("indHasSplitSecurities", BooleanType.YES.getCode());
		query.setParameter("lastModifyApp", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyDate", loggerUser.getAuditTime());
		query.setParameter("lastModifyIp", loggerUser.getIpAddress());
		query.setParameter("lastModifyUser", loggerUser.getUserName());
		query.executeUpdate();
	}

	/**
	 * Gets the list security interest coupons.
	 *
	 * @param idIsinCode the id isin code
	 * @return the list security interest coupons
	 */
	@SuppressWarnings("unchecked")
	public List<SecuritySplitCoupon> getListSecurityInterestCoupons(
			String idIsinCode) {
		List<SecuritySplitCoupon> lstSecurityInterestCoupons = null;
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(" SELECT SIC FROM SecuritySplitCoupon SIC ");
		stringBuffer
				.append(" WHERE SIC.securities.idIsinCodePk = :idIsinCode ");
		stringBuffer
				.append(" and SIC.programInterestCoupon.stateProgramInterest = :stateProgramInterest ");

		Query query = em.createQuery(stringBuffer.toString());
		query.setParameter("idIsinCode", idIsinCode);
		query.setParameter("stateProgramInterest",
				ProgramScheduleStateType.PENDING.getCode());
		lstSecurityInterestCoupons = query.getResultList();
		return lstSecurityInterestCoupons;
	}

	/**
	 * Creates the amortization corporative operation.
	 *
	 * @param objSecurities the obj securities
	 * @param loggerUser the logger user
	 */
	public void createAmortizationCorporativeOperation(Security objSecurities,
			LoggerUser loggerUser) {
		for (ProgramAmortizationCoupon programAmortizationCoupon : objSecurities
				.getAmortizationPaymentSchedule()
				.getProgramAmortizationCoupons()) {
			CorporativeOperation corporativeOperation = new CorporativeOperation();
			// corporativeOperation.setAudit(loggerUser);
			corporativeOperation
					.setProgramAmortizationCoupon(programAmortizationCoupon);
			
			CorporativeEventType corporativeEveType=new CorporativeEventType();
			corporativeEveType.setIdCorporativeEventTypePk(new Long(ImportanceEventType.CAPITAL_AMORTIZATION.getCode().toString()));
			corporativeOperation.setCorporativeEventType(corporativeEveType);
			
			corporativeOperation.setIssuer(objSecurities.getIssuer());
			corporativeOperation.setSecurities(objSecurities);
			corporativeOperation.setAccordType(AccordType.AMORTIZATION_PAYMENT
					.getCode());
			corporativeOperation.setAccordDate(objSecurities.getRegistryDate());
			corporativeOperation.setState(CorporateProcessStateType.REGISTERED
					.getCode());
			corporativeOperation.setRegistryUser(loggerUser.getUserName());

			corporativeOperation.setRegistryDate(programAmortizationCoupon
					.getRegistryDate());
			corporativeOperation.setCutoffDate(programAmortizationCoupon
					.getCutoffDate());
			corporativeOperation.setDeliveryDate(programAmortizationCoupon
					.getPaymentDate());
			corporativeOperation.setCreationDate(programAmortizationCoupon
					.getCorporativeDate());

			corporativeOperation.setCurrency(programAmortizationCoupon
					.getCurrency());
			corporativeOperation
					.setDeliveryPlacement(DeliveryPlacementType.DEPOSITARY
							.getCode());
			corporativeOperation.setDeliveryFactor(programAmortizationCoupon
					.getAmortizationFactor());
			corporativeOperation.setIndRound(BooleanType.NO.getCode());

			if (objSecurities.getIndTaxExempt().equals(
					BooleanType.YES.getCode()))
				corporativeOperation.setIndTax(BooleanType.NO.getCode());
			if (objSecurities.getIndTaxExempt()
					.equals(BooleanType.NO.getCode()))
				corporativeOperation.setIndTax(BooleanType.YES.getCode());

			corporativeOperation.setIndRemanent(BooleanType.NO.getCode());
			corporativeOperation.setIndExcludedCommissions(BooleanType.NO
					.getCode());
			corporativeOperation.setOldShareCapital(objSecurities
					.getShareCapital());
			corporativeOperation.setNewShareCapital(objSecurities
					.getShareCapital().multiply(
							BigDecimal.valueOf(100).subtract(
									programAmortizationCoupon
											.getAmortizationFactor())));
			corporativeOperation.setVariationAmount(objSecurities
					.getShareCapital().subtract(
							corporativeOperation.getNewShareCapital()));
			corporativeOperation.setOldNominalValue(objSecurities
					.getCurrentNominalValue());
			corporativeOperation.setIndPayed(BooleanType.NO.getCode());
			// IND_REMANENT
			this.create(corporativeOperation);
		}
	}
	
	/**
	 * Generate cfi code.
	 *
	 * @param security the security
	 * @return the string
	 * @throws ServiceException the service exception
	 */
	public String generateCFICode(Security security) {
		
		CfiCodeGenerator cfiCodeGenerator=new CfiCodeGenerator();
		cfiCodeGenerator.setSecurityClass(security.getSecurityClass());
		cfiCodeGenerator.setSecurityTerm(security.getSecurityTerm());
		cfiCodeGenerator.setInterestType(security.getInterestType());
		cfiCodeGenerator.setInstrumentType(security.getInstrumentType() );
		//cfiCodeGenerator.setEconomicSector(security.getIssuer().getEconomicSector()  );
		cfiCodeGenerator.setIndEarlyRedemption(security.getIndEarlyRedemption() ); 
		cfiCodeGenerator.setCapitalPaymentModality(security.getCapitalPaymentModality());
		String codeGeneratorResult = null;
	   try {
		    return  getCfiCodeGeneratorServiceBean(cfiCodeGenerator);
		} catch (ServiceException e) {
			return codeGeneratorResult;
	   }
	}

	/**
	 * Registry security service bean.
	 *
	 * @param security the security
	 * @param loggerUser the logger user
	 * @param lstDtbInternationalDepository the lst dtb international depository
	 * @param lstDtbNegotiationModalities the lst dtb negotiation modalities
	 * @throws ServiceException the service exception
	 */
	public void registrySecurityServiceBean(Security security,
											LoggerUser loggerUser,
											List<InternationalDepository> lstDtbInternationalDepository,
											List<NegotiationModality> lstDtbNegotiationModalities,
											Integer indAutomatic,
											AccountAnnotationOperation accountAnnotationOperation)
											throws ServiceException{
		security.setFsinCode(getShortCfiCode(security));
		if(security.isPrepaid()){
			String idSecudityOriginCode=security.getSecurityOrigin().getIdSecurityCodePk();
			Security sec=new Security();
			sec.setIdSecurityCodePk(idSecudityOriginCode);
			security.setSecurityOrigin(sec);
			security.setSecurityHistoryBalances(null);
			security.setSecurityHistoryStates(null);
		}
		security.setSecurityHistoryStates(null); 
		security.setPlacedAmount(BigDecimal.ZERO);
		security.setPlacedBalance(BigDecimal.ZERO);
		
		if (!security.isForeignSecuritySource() && !security.isDpfDpaSecurityClass()) {
			String idIsinCode = null;			
			/*Publico*/
			if(security.getIssuance().getSecurityType().equals(SecurityType.PUBLIC.getCode())) {
				if(security.isPrepaid()) {//es Q			
					security.setIdIsinCode(null);	
				} 
				else {
					if(SecurityClassType.AOP.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.BBX.getCode().equals(security.getSecurityClass()) 
							|| SecurityClassType.BCA.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.BOP.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.BTX.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.DPA.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.DPF.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.PGE.getCode().equals(security.getSecurityClass())
							|| SecurityClassType.PGS.getCode().equals(security.getSecurityClass())){
						security.setIdIsinCode(null);
					}
					else{
						/*idIsinCode = securityIsinGenerate(security);
						security.setIdIsinCode(idIsinCode);
						security.setCfiCode(generateCFICode(security));
						log.info("Se genero CFI code y ISIN Code");*/
					}
			    }
			} 
			/*Privado*/
			if(security.getIssuance().getSecurityType().equals(SecurityType.PRIVATE.getCode())) {
				security.setIdIsinCode(null);				
				/*//no verifica Q
				if(security.isPrepaid()) {//es Q			
					security.setIdIsinCode(null);	
					
				} else {
					idIsinCode = securityIsinGenerate(security);
					security.setIdIsinCode(idIsinCode);
					
				}*/
			}		    
		}	
		
		String idBcbCode=securityBcbCodeGenerate(security);	
		security.setIdSecurityBcbCode( idBcbCode );
//		String alternativeCode = alternativeCodeGenerate(security);
		
		
		if(Validations.validateIsNullOrEmpty(security.getCfiCode()) && security.getBolCfiCode()) {				
			if(security.getIssuance().getSecurityType().equals(SecurityType.PUBLIC.getCode())) {
				if(security.isPrepaid()) {//es Q
					security.setCfiCode(null);
					
				} else {
					if(SecurityClassType.AOP.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.BBX.getCode().equals(security.getSecurityClass()) 
					|| SecurityClassType.BCA.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.BOP.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.BTX.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.DPA.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.DPF.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.PGE.getCode().equals(security.getSecurityClass())
					|| SecurityClassType.PGS.getCode().equals(security.getSecurityClass())){
						security.setCfiCode(null);
					}
					else{
						/*String cfiCode=generateCFICode(security);
						security.setCfiCode(cfiCode);	
						security.setIdIsinCode(securityIsinGenerate(security));
						log.info("Se genero CFI code y ISIN Code");*/
					}	
				}
			}
			
			if(security.getIssuance().getSecurityType().equals(SecurityType.PRIVATE.getCode()))  {
				security.setCfiCode(null);
				/*//no verifica Q
				if(security.isPrepaid()) {//es Q
					security.setCfiCode(null);
				} else {
					String cfiCode=generateCFICode(security);
					security.setCfiCode(cfiCode);					
				}*/
			}
		}
		
		ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(security.getSecurityClass());
		StringBuilder strbSecCode=new StringBuilder();
		strbSecCode.append(pTableSecClass.getText1());
		//strbSecCode.append("-");//para que no salga el guien en la creacion del valor
		strbSecCode.append( security.getIdSecurityCodeOnly() );
		
		security.setIdSecurityCodePk(strbSecCode.toString());
		
		if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.PGS.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.CHQ.getCode().equals(security.getSecurityClass()))  {

			if(Validations.validateIsNotNullAndNotEmpty(security.getIdSecurityCodeOnly()) && 
					Validations.validateIsNotNull(accountAnnotationOperation)) {
				security.setIdSecurityCodeOnly(generateIsinCodePk(security, accountAnnotationOperation));
				security.setIdSecurityCodePk(security.getIdSecurityCodeOnly());
				security.setIdIsinCode(security.getIdSecurityCodePk());
				security.setAlternativeCode(security.getIdSecurityCodePk());
				if(security.getDescription() == null) {
					security.setDescription(security.getIdSecurityCodePk());
				}
//			security.setMnemonic(security.getIdSecurityCodePk());
			}
		} else if (
				SecurityClassType.ACC_RF.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.ACC.getCode().equals(security.getSecurityClass()) 
				|| SecurityClassType.ACE.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.CFC.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.ACC_OVM.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.ACC_APE.getCode().equals(security.getSecurityClass())) {
			if(Validations.validateIsNullOrEmpty(security.getIdSecurityCodeOnly())){
				security.setIdSecurityCodeOnly(generateIsinCodePk(security, accountAnnotationOperation));
				security.setIdSecurityCodePk(security.getIdSecurityCodeOnly());
				security.setIdIsinCode(security.getIdSecurityCodePk());
				security.setAlternativeCode(security.getIdSecurityCodePk());
				if(security.getDescription() == null) {
					security.setDescription(security.getIdSecurityCodePk());
				}
			}
		}
		
		SecurityForeignDepository secForeignDepository = null;
		SecurityNegotiationMechanism securityNegotiationMechanism = null;

		security.setCorporativeProcessDays(Integer.valueOf(0));
		security.setIndHasSplitSecurities(BooleanType.NO.getCode());
		security.setIndHolderDetail(BooleanType.NO.getCode());
		
//		security.setAlternativeCode(alternativeCode);
		security.setStateSecurity(SecurityStateType.REGISTERED.getCode());

		security.setRegistryUser(loggerUser.getUserName());
		security.setRegistryDate(loggerUser.getAuditTime());

		security.getSecurityInvestor().setSecurity(security);
		security.getSecurityInvestor().setSecurityInvestorState(
				SecurityInversionistStateType.REGISTERED.getCode());

		if(Validations.validateListIsNotNullAndNotEmpty(lstDtbInternationalDepository)){
			security.setSecurityForeignDepositories(new ArrayList<SecurityForeignDepository>());
			for (InternationalDepository depository : lstDtbInternationalDepository) {
				if (depository.isSelected()) {
					secForeignDepository = new SecurityForeignDepository();
					secForeignDepository.setId(new SecurityForeignDepositoryPK());
					secForeignDepository.getId().setIdSecurityCodePk(
							security.getIdSecurityCodePk());
					secForeignDepository.getId().setIdInternationalDepositoryPk(
							depository.getIdInternationalDepositoryPk());
//					secForeignDepository.setRegistryDate(CommonsUtilities
//							.currentDate());
					secForeignDepository
							.setSecurityForeignState(SecurityForeignDepositoryStateType.REGISTERED
									.getCode());

					security.getSecurityForeignDepositories().add(
							secForeignDepository);
				}
			}
		}
		
		if(Validations.validateListIsNotNullAndNotEmpty(lstDtbNegotiationModalities)){
			security.setSecurityNegotiationMechanisms(new ArrayList<SecurityNegotiationMechanism>());
			for (NegotiationModality negotiationModality : lstDtbNegotiationModalities) {
				for (NegotiationMechanism negotiationMechanism : negotiationModality
						.getNegotiationMechanisms()) {
					if (negotiationMechanism.isSelected()) {
						securityNegotiationMechanism = new SecurityNegotiationMechanism();
						securityNegotiationMechanism.setMechanismModality(new MechanismModality(
								new MechanismModalityPK(negotiationMechanism.getIdNegotiationMechanismPk(), negotiationModality.getIdNegotiationModalityPk())));
						securityNegotiationMechanism.setSecurity(security);
						securityNegotiationMechanism.setSecurityNegotationState(BooleanType.YES.getCode());						
						security.getSecurityNegotiationMechanisms().add(securityNegotiationMechanism);
					}
				}
			}
		}
		
		if (security.isVariableInstrumentTypeAcc()) {
			security.setAmortizationPaymentSchedule(null);
			security.setInterestPaymentSchedule(null);
		}
		if(security.isCeroInterestType()){
			security.setInterestPaymentSchedule(null);
		}
		
		if(security.getAmortizationPaymentSchedule()!=null){
			security.getAmortizationPaymentSchedule().setRegistryDays(1);
		}
		String idIssuancePk=security.getIssuance().getIdIssuanceCodePk();
		
		//Add new 		
		if(loggerUser != null){
			log.info("registrySecurityServiceBean ");
			log.info("WSERROR Privilege: " + loggerUser.getIdPrivilegeOfSystem());
			log.info("WSERROR user : " + loggerUser.getUserName());
			log.info("WSERROR address : " + loggerUser.getIpAddress());
		}
		security.setAudit(loggerUser);		
		security.setValuatorProcessClass(null);
		
		/* cambiando la logica original:
		 * solo ingresa en estos casos inmovilizacion */
//		if(	SecurityClassType.CUP.getCode().equals(security.getSecurityClass()) || 
//			SecurityClassType.DPF.getCode().equals(security.getSecurityClass())
//		){ 
//
//			if( !(security.getMotiveAnnotation()!= null && (security.getMotiveAnnotation().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()) || 
//				  security.getMotiveAnnotation().equals(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode()))
//			)){
//				String ccnCode = "CCN"+security.getIdSecurityCodePk().substring(3);
//				security.setIdSecurityCodePk(ccnCode);
//			}
//			
//			//Formateo de nujevo codigo PK redmine 225
////			security.setIdIsinCode(generateIsinCodePk(security));
////			security.setIdSecurityCodePk(generateIsinCodePk(security));
//			
//			security.setIdIsinCode(security.getIdSecurityCodePk());
//			security.setAlternativeCode(security.getIdSecurityCodePk());
//			security.setDescription(security.getIdSecurityCodePk());
//			security.setMnemonic(security.getIdSecurityCodePk());
//		}
		
		if(security.getMotiveAnnotation()!= null && security.getMotiveAnnotation().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode())) {
			if( security.getStateExpirate() ) {
				security.setStateSecurity(SecurityStateType.GUARDA_EXCLUSIVE_EXPIRATION.getCode());
			}else {
				security.setStateSecurity(SecurityStateType.GUARDA_EXCLUSIVE.getCode());
			}
		}
		
		List<SecuritySerialRange> lstSecuritySerialRange = null;
		if(security.getSecuritySerialRange()!=null) {
			lstSecuritySerialRange = security.getSecuritySerialRange();
			security.setSecuritySerialRange(null);
		}
		if(security.getIndIsManaged()==null) {
			security.setIndIsManaged(0);
		}
		if(Validations.validateIsNotNullAndNotEmpty(indAutomatic)) {
			if(indAutomatic.equals(GeneralConstants.ONE_VALUE_INTEGER)) {
				security.setIdSecurityCodeOnly(security.getIdSecurityCodePk());
			}else {
				security.setIdSecurityCodePk(security.getIdSecurityCodeOnly());
				security.setIdIsinCode(security.getIdSecurityCodePk());
				security.setAlternativeCode(security.getIdSecurityCodePk());
			}
		}
	
		this.create(security);
		
		if(lstSecuritySerialRange!=null) {
			for (SecuritySerialRange securitySerialRange : lstSecuritySerialRange) {
				securitySerialRange.setSecurity(security);
				this.create(securitySerialRange);
			}
		}
		
		if(security.getModifyIssuance() && (security.isMixedIssuanceForm() || security.isPhysicalIssuanceForm()) && !security.isPrepaid()){
			BigDecimal circulationAmount=security.getIssuance().getCirculationAmount();
			circulationAmount=circulationAmount.add( security.getCirculationAmount() );
			
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append(" Update Issuance i ");
			sbQuery.append(" Set i.circulationAmount=:circulationAmountPrm, ");
			sbQuery.append(" i.lastModifyDate=:lastModifyDatePrm, ");
			sbQuery.append(" i.lastModifyUser=:lastModifyUserPrm, ");
			sbQuery.append(" i.lastModifyIp=:lastModifyIpPrm, ");
			sbQuery.append(" i.lastModifyApp=:lastModifyAppPrm ");
			sbQuery.append(" where i.idIssuanceCodePk=:idIssuanceCodePrm ");
			
			Query query = em.createQuery(sbQuery.toString());
			
			query.setParameter("circulationAmountPrm",circulationAmount);
			query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
			query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
			query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
			query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
			query.setParameter("idIssuanceCodePrm",idIssuancePk);
		    query.executeUpdate();
		}
		CorporativeEventType interestPaymentCorp;
		CorporativeEventType capitalAmortizacionCorp;
 
		interestPaymentCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																						ImportanceEventType.INTEREST_PAYMENT.getCode(),
																						security.getInstrumentType(),
																						com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
		capitalAmortizacionCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																							ImportanceEventType.CAPITAL_AMORTIZATION.getCode(),
																							security.getInstrumentType(),
																							com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
		
		// Corporative Operations
		CorporativeOperation corporativeOperation = null;
		if (security.isFixedInstrumentTypeAcc()) {
			if(!security.isCeroInterestType()){
				for (ProgramInterestCoupon programInterestCoupon : security.getInterestPaymentSchedule().getProgramInterestCoupons()) {
					corporativeOperation = new CorporativeOperation();
					corporativeOperation.setDataFromProgramIntCoupon(programInterestCoupon, interestPaymentCorp, security, loggerUser);
					corporativeOperation.setCreationDate(CommonsUtilities.currentDate());
					if (corporativeOperation.getIndTax().equals(BooleanType.YES.getCode())) {
						BigDecimal value = this.findTaxFactor();
						corporativeOperation.setTaxFactor(value);
					} else if (!corporativeOperation.getIndTax().equals(BooleanType.YES.getCode())) {
						corporativeOperation.setTaxFactor(null);
					}
					this.create(corporativeOperation);
				}
			}
			BigDecimal lastNewNominalValue=null;
			
			for (int i = 0 ; i<security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size();i++) {
				ProgramAmortizationCoupon programAmortizationCoupon = (ProgramAmortizationCoupon)security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(i);
				if(CapitalPaymentModalityType.PARTIAL.getCode().equals(security.getCapitalPaymentModality())){
					if(i == (security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size()-1)){
						programAmortizationCoupon.setUltimateAmortization(BooleanType.YES.getBooleanValue());
					}else{
						programAmortizationCoupon.setUltimateAmortization(BooleanType.NO.getBooleanValue());
					}					
				}
				corporativeOperation = new CorporativeOperation();
				corporativeOperation.setDataFromProgramAmortCoupon(programAmortizationCoupon,capitalAmortizacionCorp, security, loggerUser);
				corporativeOperation.setCreationDate(CommonsUtilities.currentDate());
				lastNewNominalValue = corporativeOperation.calculateAmortizationNominalValues(security.getInitialNominalValue(),security.isAtMaturityAmortPaymentModality(), lastNewNominalValue);
				if (corporativeOperation.getIndTax().equals(BooleanType.YES.getCode())) {
					BigDecimal value = this.findTaxFactor();
					corporativeOperation.setTaxFactor(value);
				} else if (!corporativeOperation.getIndTax().equals(BooleanType.YES.getCode())) {
					corporativeOperation.setTaxFactor(null);
				}
				this.create(corporativeOperation);
			}
		}
		/**
		 * Create Security History Balance
		 */
		SecurityHistoryBalance securityHistoryBalance =new SecurityHistoryBalance();
		securityHistoryBalance.setSecurity(security);
		securityHistoryBalance.setUpdateBalanceDate(CommonsUtilities.currentDate());
		securityHistoryBalance.setNominalValue(security.getCurrentNominalValue());
		securityHistoryBalance.setPlacedAmount(BigDecimal.ZERO);
		securityHistoryBalance.setPlacedBalance(BigDecimal.ZERO);
		securityHistoryBalance.setShareCapital(BigDecimal.ZERO);
		securityHistoryBalance.setShareBalance(BigDecimal.ZERO);
		securityHistoryBalance.setDesmaterializedBalance(BigDecimal.ZERO);
		
		this.create(securityHistoryBalance);

	}
	
	public String generateIsinCodePk(Security security, AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		String isin= "";
		if(SecurityClassType.DPF.getCode().equals(security.getSecurityClass())) {
			if(!(security.getMotiveAnnotation()!= null && (security.getMotiveAnnotation().equals(DematerializationCertificateMotiveType.GUARDA_EXCLUSIVE.getCode()) || 
					  security.getMotiveAnnotation().equals(DematerializationCertificateMotiveType.GUARDA_MANAGED.getCode()))
				)){
					isin =  generateIsinCDA(security, accountAnnotationOperation);
					//isin = "CCN"+security.getIdSecurityCodePk().substring(3);
				}else {
					isin =  generateIsinCDA(security, accountAnnotationOperation);
				}
		}else if(SecurityClassType.PGS.getCode().equals(security.getSecurityClass())) {
			isin = generateIsinPAG(security, accountAnnotationOperation);
		}else if(SecurityClassType.CHQ.getCode().equals(security.getSecurityClass())) {
			isin = generateIsinCHQ(security, accountAnnotationOperation);
		}else if(SecurityClassType.ACC_RF.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.ACC.getCode().equals(security.getSecurityClass()) 
				|| SecurityClassType.ACE.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.CFC.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.ACC_OVM.getCode().equals(security.getSecurityClass())
				|| SecurityClassType.ACC_APE.getCode().equals(security.getSecurityClass())) {
			isin = generateIsinACC(security);
		}
		
		return isin;
	}

	private void generateGeneralInfo(Security security, StringBuilder strbIsin) throws ServiceException {
		strbIsin.append(codeClassSecurity(security.getSecurityClass()));  			// clase de valor 3
		strbIsin.append(security.getIssuer().getMnemonic().substring(0, 3));		//mnemonico 3
		strbIsin.append(getIndicator1(security.getCurrency()));					//Moneda 1
		strbIsin.append(getTextThreeParameter(security.getIssuanceForm()));					//T EMISION 1
	}
	
	private String generateIsinCDA(Security security, AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		StringBuilder strbIsin = new StringBuilder();
		generateGeneralInfo(security, strbIsin);
		
		StringBuilder sbIdentSecurity = new StringBuilder();
		if(Validations.validateIsNotNull(accountAnnotationOperation) && 
				Validations.validateIsNotNull(accountAnnotationOperation.getCertificateNumber()) &&
				Validations.validateIsNotNull(accountAnnotationOperation.getSerialNumber())) {
			sbIdentSecurity.append(accountAnnotationOperation.getSerialNumber());
			String certificateNumber = String.format("%4s", accountAnnotationOperation.getCertificateNumber()).replace(' ', '0');			
			sbIdentSecurity.append(certificateNumber);
		}
		
		strbIsin.append(String.format("%11s", sbIdentSecurity.toString()).replace(' ', '0'));
		/*if(security.getSecuritySerial()!=null) {
			String securitySerialAppend = "";
			Integer cantidadMaxima = security.getSecuritySerial().length();
			if( cantidadMaxima >= 6 ) {
				securitySerialAppend = security.getSecuritySerial().substring(0, 6);
			}else {
				securitySerialAppend = security.getSecuritySerial().substring(0, cantidadMaxima);
			}
			strbIsin.append(securitySerialAppend);
		}*/
		return strbIsin.toString();
	}
	
	private String generateIsinACC(Security security) throws ServiceException{
		StringBuilder strbIsin = new StringBuilder();
		strbIsin.append("ACC");
		strbIsin.append(security.getIssuer().getMnemonic().substring(0, 3));		//mnemonico 3
		strbIsin.append(codeClassSecurity(security.getSecurityClass()));  			// clase de valor 3
		strbIsin.append(security.getSubClass()!= null?security.getSubClass().substring(0,1):""); 					//sub class
		strbIsin.append(getTextThreeParameter(security.getIssuanceForm()));			//Tipo de Emision
		strbIsin.append(getCorrelativeSecurity(strbIsin.toString(),GeneralConstants.TWO_VALUE_INTEGER));	
		return strbIsin.toString();
	}

	private String generateIsinPAG(Security security, AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		StringBuilder strbIsin = new StringBuilder();
		generateGeneralInfo(security, strbIsin);
		
		StringBuilder sbIdentSecurity = new StringBuilder();
		if(Validations.validateIsNotNull(accountAnnotationOperation) && 
				Validations.validateIsNotNull(accountAnnotationOperation.getCertificateNumber()) &&
				Validations.validateIsNotNull(accountAnnotationOperation.getSerialNumber())) {
			String serialNumber = String.format("%4s", accountAnnotationOperation.getSerialNumber()).replace(' ', '0');
			sbIdentSecurity.append(serialNumber);
			sbIdentSecurity.append(accountAnnotationOperation.getCertificateNumber());			
		}
		
		strbIsin.append(String.format("%11s", sbIdentSecurity.toString()).replace(' ', '0'));
		
		return strbIsin.toString();
	}
	
	private String generateIsinCHQ(Security security, AccountAnnotationOperation accountAnnotationOperation) throws ServiceException{
		StringBuilder strbIsin = new StringBuilder();
		generateGeneralInfo(security, strbIsin);
		
		StringBuilder sbIdentSecurity = new StringBuilder();
		if(Validations.validateIsNotNull(accountAnnotationOperation) && 
				Validations.validateIsNotNull(accountAnnotationOperation.getCertificateNumber()) &&
				Validations.validateIsNotNull(accountAnnotationOperation.getSerialNumber())) {
			String serialNumber = String.format("%4s", accountAnnotationOperation.getSerialNumber()).replace(' ', '0');
			sbIdentSecurity.append(serialNumber);
			sbIdentSecurity.append(accountAnnotationOperation.getCertificateNumber());			
		}
		
		strbIsin.append(String.format("%11s", sbIdentSecurity.toString()).replace(' ', '0'));
		
		return strbIsin.toString();
	}
	
	private String getCorrelativeSecurity(String idSecurityCode,Integer formatCorrelative) throws ServiceException {
		int count = getIsinMax(idSecurityCode,formatCorrelative);
		count++;
		String correlativeFormat;
		if(GeneralConstants.TWO_VALUE_INTEGER.equals(formatCorrelative))
			correlativeFormat=String.format("%02d", count);
		else 
			correlativeFormat=String.format("%03d", count);
		return correlativeFormat;
	}
	
	private String codeClassSecurity(Integer securityClass) throws ServiceException {
		ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(securityClass);
		return  pTableSecClass.getText1();
	}
	
	private String getTextThreeParameter(Integer securityClass) throws ServiceException {
		ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(securityClass);
		return  pTableSecClass.getText3();
	}
	
	private String getIndicator1(Integer securityClass) throws ServiceException {
		ParameterTable pTableSecClass=parameterServiceBean.getParameterTableById(securityClass);
		return  pTableSecClass.getIndicator1();
	}
	/**
	 * Find tax factor.
	 *
	 * @return the big decimal
	 */
	public BigDecimal findTaxFactor(){
		BigDecimal tax = BigDecimal.ZERO;		
		ParameterTableTO filter = new ParameterTableTO();
		filter.setState(ParameterTableStateType.REGISTERED.getCode());
		filter.setParameterTablePk(MasterTableType.PARAMETER_TABLE_IVA_TAX.getCode());
		List<ParameterTable> lstParameterTable= this.getParameters(filter);
		if(lstParameterTable!=null && lstParameterTable.size()>0){
			ParameterTable obj = (ParameterTable)lstParameterTable.get(0);
			if(obj.getIndicator1()!=null && 
					Validations.validateIsNotEmpty(obj.getIndicator1())){
				tax = new BigDecimal(obj.getIndicator1().toString());
			}
		}	
		return tax;
	}
	/**
	 * Gets the parameters.
	 * 
	 * @param filter
	 *            the filter
	 * @return the parameters
	 */
	private List<ParameterTable> getParameters(ParameterTableTO filter) {		
			try {
				return generalParametersFacade.getListParameterTableServiceBean(filter);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}
	/**
	 * Update security service bean.
	 *
	 * @param security the security
	 * @param loggerUser the logger user
	 * @param lstDtbInternationalDepository the lst dtb international depository
	 * @param lstDtbNegotiationModalities the lst dtb negotiation modalities
	 * @param isReGenerated the is re generated
	 * @throws ServiceException the service exception
	 */
	public void updateSecurityServiceBean(Security security,
			LoggerUser loggerUser,
			List<InternationalDepository> lstDtbInternationalDepository,
			List<NegotiationModality> lstDtbNegotiationModalities,
			boolean isReGenerated) throws ServiceException {
		
		
		if(StringUtils.isEmpty(security.getCfiCode()) && security.getBolCfiCode()){
			String cfiCode=generateCFICode(security);
			security.setCfiCode(cfiCode);
		}
		
		if (isReGenerated) {
			createHistoryToPaymentSchedules(security, loggerUser);
			deleteCorporativeByParentServiceBean(security.getInterestPaymentSchedule(), CorporateProcessStateType.REGISTERED.getCode());
			deleteProgramInterestCouponByParentServiceBean(
					security.getInterestPaymentSchedule(),
					ProgramScheduleStateType.PENDING.getCode());
			deleteProgramAmortizationCouponByParentServiceBean(
					security.getAmortizationPaymentSchedule(),
					ProgramScheduleStateType.PENDING.getCode());
		}
		// SecurityInvestor securityInversionist=null;
		SecurityForeignDepository securityForeignDepository = null;
		SecurityNegotiationMechanism secNegotiationMechanism = null;

		// disable international depository unchecked
		for (SecurityForeignDepository secForeignDep : security
				.getSecurityForeignDepositories()) {
			if (!isCheckedInterMechanism(secForeignDep.getId()
					.getIdInternationalDepositoryPk(),
					lstDtbInternationalDepository)) {
				secForeignDep
						.setSecurityForeignState(SecurityForeignDepositoryStateType.BLOCKED
								.getCode());
			}
		}

		// register new international depository
		for (InternationalDepository interDepository : lstDtbInternationalDepository) {
			if (interDepository.isSelected()) {
				if (!isAssignedInterMechanism(security,
						interDepository.getIdInternationalDepositoryPk())) {
					securityForeignDepository = new SecurityForeignDepository();

					securityForeignDepository
							.setId(new SecurityForeignDepositoryPK());
					securityForeignDepository.getId().setIdSecurityCodePk(
							security.getIdSecurityCodePk());
					securityForeignDepository.getId()
							.setIdInternationalDepositoryPk(
									interDepository
											.getIdInternationalDepositoryPk());

					securityForeignDepository
							.setSecurityForeignState(SecurityForeignDepositoryStateType.REGISTERED
									.getCode());
					securityForeignDepository.setSecurity(security);

					security.getSecurityForeignDepositories().add(
							securityForeignDepository);
				}
			}
		}

		// disable Security Negotiation Mechanism unchecked
		for (SecurityNegotiationMechanism secNegMechanism : security
				.getSecurityNegotiationMechanisms()) {
			if (!isCheckedSecNegotiationMechanism(secNegMechanism,
					lstDtbNegotiationModalities)) {
				secNegMechanism.setSecurityNegotationState(BooleanType.NO
						.getCode());
			}
		}

		// register new negotiation mechanism
		StringBuilder sbIdConverted = new StringBuilder();
		for (NegotiationModality negotiationModality : lstDtbNegotiationModalities) {
			for (NegotiationMechanism negotiationMechanism : negotiationModality
					.getNegotiationMechanisms()) {
				if (negotiationMechanism.isSelected()) {
					sbIdConverted.delete(0, sbIdConverted.length());
					sbIdConverted
							.append(negotiationMechanism
									.getIdNegotiationMechanismPk())
							.append("-")
							.append(negotiationModality
									.getIdNegotiationModalityPk());
					if (!isAssignedSecNegotiationMechanism(security,
							sbIdConverted.toString())) {
						secNegotiationMechanism = new SecurityNegotiationMechanism();
						secNegotiationMechanism.setSecurity(security);
						secNegotiationMechanism
								.setSecurityNegotationState(BooleanType.YES
										.getCode());
						secNegotiationMechanism
								.setMechanismModality(new MechanismModality(
										new MechanismModalityPK(
												negotiationMechanism
														.getIdNegotiationMechanismPk(),
												negotiationModality
														.getIdNegotiationModalityPk())));

						security.getSecurityNegotiationMechanisms().add(
								secNegotiationMechanism);
					}
				}
			}
		}
		
		if (security.isVariableInstrumentTypeAcc() || (security.isFixedInstrumentType() && security.isPhysicalIssuanceForm())) {
			security.setAmortizationPaymentSchedule(null);
			security.setInterestPaymentSchedule(null);
		}
		if(security.isCeroInterestType()){
			security.setInterestPaymentSchedule(null);
		}
		
		/**
		 * Issuer 2433
		 * Update Issuance
		 */
		if((security.isMixedIssuanceForm() || security.isPhysicalIssuanceForm()) && !security.isPrepaid()){
			updateIssuanceBySecurity(security,loggerUser);
		}
		
		if(Validations.validateIsNotNull(security.getValuatorProcessClass()) && Validations.validateIsNull(security.getValuatorProcessClass().getIdValuatorClassPk())) {
			security.setValuatorProcessClass(null);
		}
		
		security = update(security);

		if (isReGenerated) {
			// Generate the new corporate events
			createIntPaymentScheduleCorpEvents(security, loggerUser);
			createAmoPaymentScheduleCorpEvents(security, loggerUser);

		}
	}

	/**
	 * Creates the history to payment schedules.
	 *
	 * @param security the security
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	private void createHistoryToPaymentSchedules(Security security,
			LoggerUser loggerUser) throws ServiceException {
		List<ProgramInterestCoupon> lstProgramInterestCoupon = findProgramInterestCouponsServiceBean(security
				.getInterestPaymentSchedule().getIdIntPaymentSchedulePk());
		List<ProgramAmortizationCoupon> lstProgramAmortizationCoupon = findProgramAmortizationCouponsServiceBean(security
				.getAmortizationPaymentSchedule().getIdAmoPaymentSchedulePk());

		// create a history for the payment schedule
		// interest
		IntPaymentScheduleReqHi intPayScheReqHiToHistory = new IntPaymentScheduleReqHi();
		intPayScheReqHiToHistory.setDataFromIntPaymentSchedule(
				security.getInterestPaymentSchedule(), loggerUser,
				PaymentScheduleReqRegisterType.HISTORY.getCode());
		intPayScheReqHiToHistory.setSecurity(security);
		intPayScheReqHiToHistory
				.setProgramIntCouponReqHis(new ArrayList<ProgramIntCouponReqHi>());

		ProgramIntCouponReqHi programIntCouponReqHi = null;

		for (ProgramInterestCoupon programInterestCoupon : lstProgramInterestCoupon) {
			programIntCouponReqHi = new ProgramIntCouponReqHi();
			programIntCouponReqHi.setIntPaymentScheduleReqHi(intPayScheReqHiToHistory);
			programIntCouponReqHi.setDataFromProgramInterestCoupon(programInterestCoupon, loggerUser);

			intPayScheReqHiToHistory.getProgramIntCouponReqHis().add(
					programIntCouponReqHi);

			CorporativeOperation cOperation = new CorporativeOperation();
			cOperation.setProgramInterestCoupon(programInterestCoupon);
			cOperation.setState(CorporateProcessStateType.ANNULLED.getCode());
			corporateEventComponentServiceBean
					.updateCorporativeOperationServiceBean(cOperation,
							loggerUser);
		}
		this.create(intPayScheReqHiToHistory);

		// amortization
		AmoPaymentScheduleReqHi amoPaymentScheduleReqHi = new AmoPaymentScheduleReqHi();
		amoPaymentScheduleReqHi.setDataFromAmoPaymentSchedule(
				security.getAmortizationPaymentSchedule(), loggerUser,
				PaymentScheduleReqRegisterType.HISTORY.getCode());
		amoPaymentScheduleReqHi.setSecurity(security);
		amoPaymentScheduleReqHi
				.setProgramAmoCouponReqHis(new ArrayList<ProgramAmoCouponReqHi>());

		ProgramAmoCouponReqHi programAmoCouponReqHi = null;

		for (ProgramAmortizationCoupon programAmortizationCoupon : lstProgramAmortizationCoupon) {
			programAmoCouponReqHi = new ProgramAmoCouponReqHi();
			programAmoCouponReqHi
					.setAmoPaymentScheduleReqHi(amoPaymentScheduleReqHi);
			programAmoCouponReqHi.setDataFromProgramAmortizationCoupon(
					programAmortizationCoupon, loggerUser);

			amoPaymentScheduleReqHi.getProgramAmoCouponReqHis().add(
					programAmoCouponReqHi);

			CorporativeOperation cOperation = new CorporativeOperation();
			cOperation.setProgramAmortizationCoupon(programAmortizationCoupon);
			cOperation.setState(CorporateProcessStateType.ANNULLED.getCode());
			corporateEventComponentServiceBean
					.updateCorporativeOperationServiceBean(cOperation,
							loggerUser);
		}
		this.create(amoPaymentScheduleReqHi);
	}

	/**
	 * Creates the int payment schedule corp events.
	 *
	 * @param security the security
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void createIntPaymentScheduleCorpEvents(Security security,LoggerUser loggerUser) throws ServiceException {
		CorporativeOperation corporativeOperation = null;
		
		CorporativeEventType interestPaymentCorp;
		interestPaymentCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																						ImportanceEventType.INTEREST_PAYMENT.getCode(),
																						security.getInstrumentType(),
																						com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));	
		
		for (ProgramInterestCoupon programInterestCouponToCO : security.getInterestPaymentSchedule().getProgramInterestCoupons()) {
			if (programInterestCouponToCO.getStateProgramInterest().equals(ProgramScheduleStateType.PENDING.getCode())) {
				corporativeOperation = new CorporativeOperation();
				corporativeOperation.setAudit(loggerUser);
				corporativeOperation.setDataFromProgramIntCoupon(programInterestCouponToCO,interestPaymentCorp, security, loggerUser);
				corporateEventComponentServiceBean.saveCoprorativeOperation(corporativeOperation);
			}
		}
	}

	/**
	 * Creates the amo payment schedule corp events.
	 *
	 * @param security the security
	 * @param loggerUser the logger user
	 * @throws ServiceException the service exception
	 */
	public void createAmoPaymentScheduleCorpEvents(Security security,
			LoggerUser loggerUser) throws ServiceException {
		CorporativeOperation corporativeOperation = null;
		CorporativeEventType capitalAmortizacionCorp;
		capitalAmortizacionCorp=corporateEventComponentServiceBean.findCorporativeEventType(new CorporativeEventType(
																							ImportanceEventType.CAPITAL_AMORTIZATION.getCode()
																							,security.getInstrumentType(),
																							com.pradera.model.corporatives.type.CorporativeEventType.RIGHTS_TO_DELIVER.getCode()));
		for (int i = 0 ; i<security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size();i++) {
			ProgramAmortizationCoupon programAmortizationCouponToCO = (ProgramAmortizationCoupon)security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().get(i);
			if(CapitalPaymentModalityType.PARTIAL.getCode().equals(security.getCapitalPaymentModality())){
				if(i == (security.getAmortizationPaymentSchedule().getProgramAmortizationCoupons().size()-1)){
					programAmortizationCouponToCO.setUltimateAmortization(BooleanType.YES.getBooleanValue());
				}else{
					programAmortizationCouponToCO.setUltimateAmortization(BooleanType.NO.getBooleanValue());
				}					
			}
			BigDecimal lastNewNominalValue=null;
			if (programAmortizationCouponToCO.getStateProgramAmortization().equals(ProgramScheduleStateType.PENDING.getCode())) {
				corporativeOperation = new CorporativeOperation();
				corporativeOperation.setAudit(loggerUser);
				corporativeOperation.setDataFromProgramAmortCoupon(programAmortizationCouponToCO,capitalAmortizacionCorp, security, loggerUser);				
				lastNewNominalValue = corporativeOperation.calculateAmortizationNominalValues(security.getInitialNominalValue(),security.isAtMaturityAmortPaymentModality(), lastNewNominalValue);
				corporateEventComponentServiceBean.saveCoprorativeOperation(corporativeOperation);
			}
		}
	}

	/**
	 * Checks if is checked inter mechanism.
	 *
	 * @param idInterDepository the id inter depository
	 * @param lstDtbInternationalDepository the lst dtb international depository
	 * @return true, if is checked inter mechanism
	 */
	private boolean isCheckedInterMechanism(Long idInterDepository,
			List<InternationalDepository> lstDtbInternationalDepository) {
		for (InternationalDepository interDepository : lstDtbInternationalDepository) {
			if (interDepository.isSelected()
					&& (interDepository.getIdInternationalDepositoryPk()
							.equals(idInterDepository))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if is assigned inter mechanism.
	 *
	 * @param security the security
	 * @param idInterDepository the id inter depository
	 * @return true, if is assigned inter mechanism
	 */
	private boolean isAssignedInterMechanism(Security security,
			Long idInterDepository) {
		for (SecurityForeignDepository secForDep : security
				.getSecurityForeignDepositories()) {
			if (secForDep.getId().getIdInternationalDepositoryPk()
					.equals(idInterDepository)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if is checked sec negotiation mechanism.
	 *
	 * @param secNegotiationMechanism the sec negotiation mechanism
	 * @param lstDtbNegotiationModalities the lst dtb negotiation modalities
	 * @return true, if is checked sec negotiation mechanism
	 */
	public boolean isCheckedSecNegotiationMechanism(
			SecurityNegotiationMechanism secNegotiationMechanism,
			List<NegotiationModality> lstDtbNegotiationModalities) {
		for (NegotiationModality negotiationModality : lstDtbNegotiationModalities) {
			if (negotiationModality.getIdNegotiationModalityPk().equals(
					secNegotiationMechanism.getMechanismModality().getId()
							.getIdNegotiationModalityPk())) {
				for (NegotiationMechanism negotiationMechanism : negotiationModality
						.getNegotiationMechanisms()) {
					if (negotiationMechanism.getIdNegotiationMechanismPk()
							.equals(secNegotiationMechanism
									.getMechanismModality().getId()
									.getIdNegotiationMechanismPk())
							&& negotiationMechanism.isSelected()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Checks if is assigned sec negotiation mechanism.
	 *
	 * @param security the security
	 * @param idConverted the id converted
	 * @return true, if is assigned sec negotiation mechanism
	 */
	public boolean isAssignedSecNegotiationMechanism(Security security,
			String idConverted) {
		for (SecurityNegotiationMechanism secNegMechanism : security
				.getSecurityNegotiationMechanisms()) {
			if (secNegMechanism.getMechanismModality().getIdConverted()
					.equals(idConverted)) {
				return true;
			}
		}
		return false;
	}

	/* PaymentCronogramServiceBean in PraderaSecurity -- START */
	/**
	 * Find program interest coupons service bean.
	 *
	 * @param idIntPayCronPkPrm the id int pay cron pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProgramInterestCoupon> findProgramInterestCouponsServiceBean(
			Long idIntPayCronPkPrm) throws ServiceException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProgramInterestCoupon> cq = cb
				.createQuery(ProgramInterestCoupon.class);
		Root<ProgramInterestCoupon> progIntCouponRoot = cq
				.from(ProgramInterestCoupon.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();

		Join<ProgramInterestCoupon, CorporativeOperation> corporativeOperationJoin = (Join) progIntCouponRoot
				.fetch("corporativeOperation", JoinType.LEFT);
		// Join<ProgramInterestCoupon, CorporativeOperation>
		// corporativeOperationJoin =
		// progIntCouponRoot.join("corporativeOperation", JoinType.LEFT);

		cq.orderBy(cb.asc(progIntCouponRoot.get("couponNumber")));

		cq.select(progIntCouponRoot);

		criteriaParameters.add(cb.equal(
				progIntCouponRoot.get("interestPaymentSchedule").<Long> get(
						"idIntPaymentSchedulePk"), idIntPayCronPkPrm));

		criteriaParameters.add(cb.equal(corporativeOperationJoin.get("state"),
				CorporateProcessStateType.REGISTERED.getCode()));

		if (criteriaParameters.size() == 0) {
			throw new RuntimeException("no criteria");
		} else if (criteriaParameters.size() == 1) {
			cq.where(criteriaParameters.get(0));
		} else {
			cq.where(cb.and(criteriaParameters
					.toArray(new Predicate[criteriaParameters.size()])));
		}

		TypedQuery<ProgramInterestCoupon> proIntCoupon = em.createQuery(cq);
		return proIntCoupon.getResultList();
	}

	/**
	 * Find program amortization coupons service bean.
	 *
	 * @param idAmortCronPkPrm the id amort cron pk prm
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<ProgramAmortizationCoupon> findProgramAmortizationCouponsServiceBean(
			Long idAmortCronPkPrm) throws ServiceException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProgramAmortizationCoupon> cq = cb
				.createQuery(ProgramAmortizationCoupon.class);
		Root<ProgramAmortizationCoupon> progAmoCouponRoot = cq
				.from(ProgramAmortizationCoupon.class);
		List<Predicate> criteriaParameters = new ArrayList<Predicate>();

		// progAmoCouponRoot.fetch("corporativeOperation",JoinType.LEFT);
		Join<ProgramAmortizationCoupon, CorporativeOperation> corporativeOperationJoin = (Join) progAmoCouponRoot
				.fetch("corporativeOperation", JoinType.LEFT);

		cq.orderBy(cb.asc(progAmoCouponRoot.get("couponNumber")));

		cq.select(progAmoCouponRoot);

		criteriaParameters
				.add(cb.equal(
						progAmoCouponRoot.get("amortizationPaymentSchedule")
								.<Long> get("idAmoPaymentSchedulePk"),
						idAmortCronPkPrm));

		criteriaParameters.add(cb.equal(corporativeOperationJoin.get("state"),
				CorporateProcessStateType.REGISTERED.getCode()));

		if (criteriaParameters.size() == 0) {
			throw new RuntimeException("no criteria");
		} else if (criteriaParameters.size() == 1) {
			cq.where(criteriaParameters.get(0));
		} else {
			cq.where(cb.and(criteriaParameters
					.toArray(new Predicate[criteriaParameters.size()])));
		}

		TypedQuery<ProgramAmortizationCoupon> progAmortCoupon = em
				.createQuery(cq);

		return progAmortCoupon.getResultList();
	}

	/**
	 * Delete program interest coupon by parent service bean.
	 *
	 * @param intPaySchedule the int pay schedule
	 * @param programState the program state
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramInterestCouponByParentServiceBean(
			InterestPaymentSchedule intPaySchedule, Integer programState){
		
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete from PROGRAM_INTEREST_COUPON where ID_INT_PAYMENT_SCHEDULE_FK=:idIntPaymentPrm");
		if (programState != null) {
			sbQuery.append(" and STATE_PROGRAM_INTEREST=:programState");
		}
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idIntPaymentPrm",
				intPaySchedule.getIdIntPaymentSchedulePk());
		if (programState != null) {
			query.setParameter("programState", programState);
		}
		query.executeUpdate();
	}

	public void deleteCorporativeByParentServiceBean(InterestPaymentSchedule intPaySchedule, Integer corporativeState){
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete from CORPORATIVE_OPERATION where ID_PROGRAM_INTEREST_FK in ");
		sbQuery.append("( SELECT p.ID_PROGRAM_INTEREST_PK FROM PROGRAM_INTEREST_COUPON p where p.ID_INT_PAYMENT_SCHEDULE_FK=:idIntPaymentPrm)");
		if (corporativeState != null) {
			sbQuery.append(" and state=:corporativeState");
		}
		Query query = em.createNativeQuery(sbQuery.toString());
		query.setParameter("idIntPaymentPrm",
				intPaySchedule.getIdIntPaymentSchedulePk());
		if (corporativeState != null) {
			query.setParameter("corporativeState", corporativeState);
		}
		query.executeUpdate();
	}
	
	/**
	 * Delete program amortization coupon by parent service bean.
	 *
	 * @param amoPaySchedule the amo pay schedule
	 * @param programState the program state
	 * @throws ServiceException the service exception
	 */
	public void deleteProgramAmortizationCouponByParentServiceBean(
			AmortizationPaymentSchedule amoPaySchedule, Integer programState)
			throws ServiceException {
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Delete from ProgramAmortizationCoupon p where p.amortizationPaymentSchedule.idAmoPaymentSchedulePk=:idAmoPaymentPrm");
		if (programState != null) {
			sbQuery.append(" and p.stateProgramAmortization=:programState");
		}
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("idAmoPaymentPrm",
				amoPaySchedule.getIdAmoPaymentSchedulePk());
		if (programState != null) {
			query.setParameter("programState", programState);
		}
		query.executeUpdate();
	}
	/* PaymentCronogramServiceBean in PraderaSecurity -- END */

	/**
	 * Find securities service bean.
	 *
	 * @param securityTO the security to
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Security> findSecuritiesServiceBean(SecurityTO securityTO) throws ServiceException{
		CriteriaBuilder cb=em.getCriteriaBuilder();
		CriteriaQuery<Security> cq=cb.createQuery(Security.class);
		Root<Security> securityRoot=cq.from( Security.class );
		
		List<Predicate> criteriaParameters=new ArrayList<Predicate>();
		cq.select(securityRoot);
		
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuerCodePk()  )){
			criteriaParameters.add( cb.equal(securityRoot.<String>get("issuer").get("idIssuerPk"), securityTO.getIdIssuerCodePk()) );
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIssuanceCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("issuance").get("idIssuanceCodePk"), securityTO.getIdIssuanceCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdIsinCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("idIsinCodePk"), securityTO.getIdIsinCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getIdCfiCodePk() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("cfiCode"), securityTO.getIdCfiCodePk() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getAlternativeCode() )){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("alternativeCode"), securityTO.getAlternativeCode() ));
		}
		if(Validations.validateIsNotNullAndNotEmpty( securityTO.getDescriptionSecurity() )){
			criteriaParameters.add(cb.like(securityRoot.<String>get("description"),
																new StringBuilder()
																.append(GeneralConstants.PERCENTAGE_CHAR)
																.append(securityTO.getDescriptionSecurity())
																.append(GeneralConstants.PERCENTAGE_CHAR).toString())    );
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getSecurityState())){
			criteriaParameters.add(cb.equal(securityRoot.<String>get("stateSecurity"), securityTO.getSecurityState() ));
		}
		
		if(!criteriaParameters.isEmpty()){
			if (criteriaParameters.size() == 1) {
		        cq.where(criteriaParameters.get(0));
		    } else {
		        cq.where(cb.and(criteriaParameters.toArray(new Predicate[criteriaParameters.size()])));
		    }
		}
		
		TypedQuery<Security> security=em.createQuery(cq);
		return (List<Security>) security.getResultList();
	}
	
	/**
	 * Find security negotiation participant.
	 *
	 * @param idIsinCode the id isin code
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<Participant> findSecurityNegotiationParticipant(SecurityTO securityTO) throws ServiceException{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Select distinct h from HolderAccountBalance h ");
		//where h.security.idSecurityCodePk=:idSecurityCodePrm
		sbQuery.append(" inner join h.security s  ");
		sbQuery.append(" inner join s.issuance i  ");
		sbQuery.append(" where 1=1  ");
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdSecurityCodePk())){
			sbQuery.append(" and s.idSecurityCodePk=:idSecurityCodePrm ");
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuanceCodePk())){
			sbQuery.append(" and i.idIssuanceCodePk=:idIssuanceCodePrm ");
		}
		Query query = em.createQuery(sbQuery.toString());
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdSecurityCodePk())){
			query.setParameter("idSecurityCodePrm",securityTO.getIdSecurityCodePk());
		}
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuanceCodePk())){
			query.setParameter("idIssuanceCodePrm",securityTO.getIdIssuanceCodePk());
		}
		List<Participant> lstPart=null;

			lstPart = (List<Participant>)query.getResultList();

		return lstPart;
	}
	
	public Integer countSecurityByIssuance(SecurityTO securityTO){
		 StringBuilder stringBuilderSql = new StringBuilder();
		 Map<String, Object> parameters = new HashMap<String, Object>();
			
		 stringBuilderSql.append("SELECT Count(sec) FROM Security sec ");
		 stringBuilderSql.append(" Where 1=1 ");
		 
		if(Validations.validateIsNotNullAndNotEmpty(securityTO.getIdIssuanceCodePk())){
			stringBuilderSql.append(" And sec.issuance.idIssuanceCodePk = :idIssuanceCodePrm");
			parameters.put("idIssuanceCodePrm", securityTO.getIdIssuanceCodePk());
		}
		if(Validations.validateIsNotNullAndPositive(securityTO.getStateSecurity())){
			stringBuilderSql.append(" And sec.stateSecurity = :securityStatePrm");
			parameters.put("securityStatePrm", securityTO.getStateSecurity());
		}
		Query query = this.em.createQuery(stringBuilderSql.toString());
		for (Entry<String, Object> entry : parameters.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Integer result=GeneralConstants.ZERO_VALUE_INTEGER;
		result=Integer.parseInt( query.getSingleResult().toString() );
		return result;
	}
	
	public List<ParameterTable> getListSecuritiesClassSetup(Map<String,Object> map) {
		List<ParameterTable> lstSecuritiesClassPrm=null;
		List<SecurityClassSetup> lstSecuritiesClassSetup=null;
		List<ParameterTable> lstEconomicActivityPrmAux=new ArrayList<ParameterTable>();
		
		ParameterTableTO filterParameterTable = new ParameterTableTO();
		filterParameterTable.setState(ParameterTableStateType.REGISTERED.getCode());
		filterParameterTable.setMasterTableFk(MasterTableType.SECURITIES_CLASS.getCode());
		filterParameterTable.setOrderbyParameterTableCd(BooleanType.YES.getCode());
	//	filterParameterTable.setShortInteger( Integer.parseInt( map.get("instrumentType").toString() ) );
	
		lstSecuritiesClassPrm= parameterServiceBean.getListParameterTableServiceBean(filterParameterTable);
	//	map.remove("instrumentType");
		lstSecuritiesClassSetup=(List<SecurityClassSetup>)findWithNamedQuery(SecurityClassSetup.FIND_BY_SECTOR_AND_OFFER_TYPE,map);
		
		for(SecurityClassSetup securityClassSetup : lstSecuritiesClassSetup){
			for(ParameterTable pTable : lstSecuritiesClassPrm){
				if(securityClassSetup.getSecurityClass().compareTo( pTable.getParameterTablePk() )==0){
					lstEconomicActivityPrmAux.add(pTable);
				}
			}
		}
		return lstEconomicActivityPrmAux;
	}
	
	/**
	 * Update Issuance By Security
	 * @param security
	 */
	public void updateIssuanceBySecurity(Security security, LoggerUser loggerUser){
		
		Security securityOld=find(Security.class, security.getIdSecurityCodePk());
		em.detach(securityOld);
		BigDecimal circulationAmount=security.getCirculationAmount();
		BigDecimal circulationAmountOld=securityOld.getCirculationAmount();
		String idIssuancePk=security.getIssuance().getIdIssuanceCodePk();
		
		BigDecimal factorSubstract=CommonsUtilities.subtractTwoDecimal(circulationAmount, circulationAmountOld, GeneralConstants.ROUNDING_DIGIT_NUMBER);
		
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" Update Issuance i ");
		sbQuery.append(" Set i.circulationAmount=i.circulationAmount+:circulationAmountPrm, ");
		sbQuery.append(" i.lastModifyDate=:lastModifyDatePrm, ");
		sbQuery.append(" i.lastModifyUser=:lastModifyUserPrm, ");
		sbQuery.append(" i.lastModifyIp=:lastModifyIpPrm, ");
		sbQuery.append(" i.lastModifyApp=:lastModifyAppPrm ");
		sbQuery.append(" where i.idIssuanceCodePk=:idIssuanceCodePrm ");
		
		Query query = em.createQuery(sbQuery.toString());
		
		query.setParameter("circulationAmountPrm",factorSubstract);
		query.setParameter("lastModifyDatePrm",loggerUser.getAuditTime());
		query.setParameter("lastModifyUserPrm",loggerUser.getUserName());
		query.setParameter("lastModifyIpPrm",loggerUser.getIpAddress());
		query.setParameter("lastModifyAppPrm",loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("idIssuanceCodePrm",idIssuancePk);
	    query.executeUpdate();
	}
	
	/**
	 * Update State Security 
	 * @param security
	 * @param loggerUser
	 */
	public void updateStateSecurity(Security security, LoggerUser loggerUser) {
		StringBuilder sbQuery = new StringBuilder();
		
		sbQuery.append(" Update Security SE Set ");
		sbQuery.append(" SE.stateSecurity = :stateSecurityPrm, ");
		sbQuery.append(" SE.lastModifyUser = :lastModifyUserPrm, ");
		sbQuery.append(" SE.lastModifyDate = :lastModifyDatePrm, ");
		sbQuery.append(" SE.lastModifyApp = :lastModifyAppPrm, ");
		sbQuery.append(" SE.lastModifyIp = :lastModifyIpPrm ");
		sbQuery.append(" Where SE.idSecurityCodePk = :idSecurityCodePk ");
		
		Query query = em.createQuery(sbQuery.toString());
		query.setParameter("stateSecurityPrm", security.getStateSecurity());
		query.setParameter("lastModifyUserPrm", loggerUser.getUserName());
		query.setParameter("lastModifyDatePrm", loggerUser.getAuditTime());
		query.setParameter("lastModifyAppPrm", loggerUser.getIdPrivilegeOfSystem());
		query.setParameter("lastModifyIpPrm", loggerUser.getIpAddress());
		query.setParameter("idSecurityCodePk", security.getIdSecurityCodePk());
				
		query.executeUpdate();
	}
	
	public Integer getNewInterestType(Security security){
		StringBuilder builder = new StringBuilder();
		builder.append(" select count(Pic.Id_Program_Interest_Pk) as COUNT_VIG ");
		builder.append(" from Interest_Payment_Schedule IPS inner join SECURITY sec on sec.ID_SECURITY_CODE_PK = ips.ID_SECURITY_CODE_FK");
		builder.append(" inner join Program_Interest_Coupon pic on Ips.Id_Int_Payment_Schedule_Pk = Pic.Id_Int_Payment_Schedule_Fk ");
		builder.append(" where 1 = 1 ");
		builder.append(" and Pic.State_Program_Interest = 1350 ");
		builder.append(" and sec.ID_SECURITY_CODE_PK = '"+security.getIdSecurityCodePk()+"'");
		Query query = em.createNativeQuery(builder.toString());
		BigDecimal count = (BigDecimal) query.getSingleResult();
		if(count.intValue()==0)
			return InterestType.CERO.getCode();
		else
			return security.getInterestType();
	}
}
