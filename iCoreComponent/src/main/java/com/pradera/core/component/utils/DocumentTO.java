package com.pradera.core.component.utils;

import java.io.Serializable;

public class DocumentTO implements Serializable{ 
	
	private static final long serialVersionUID = 1L;
	
	private boolean numericTypeDocument;
	private Integer maxLenghtDocumentNumber;
	private Integer documentType;
	private String  documentNumber;
	private String  componentDocumentNumber;
	private boolean indicatorDocumentIssuanceDate;
	private boolean indicatorAlphanumericWithDash;
	private boolean indicatorDocumentSource;
	
	public boolean isNumericTypeDocument() {
		return numericTypeDocument;
	}
	public void setNumericTypeDocument(boolean numericTypeDocument) {
		this.numericTypeDocument = numericTypeDocument;
	}
	public Integer getMaxLenghtDocumentNumber() {
		return maxLenghtDocumentNumber;
	}
	public void setMaxLenghtDocumentNumber(Integer maxLenghtDocumentNumber) {
		this.maxLenghtDocumentNumber = maxLenghtDocumentNumber;
	}
	public Integer getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getComponentDocumentNumber() {
		return componentDocumentNumber;
	}
	public void setComponentDocumentNumber(String componentDocumentNumber) {
		this.componentDocumentNumber = componentDocumentNumber;
	}
	public boolean isIndicatorDocumentIssuanceDate() {
		return indicatorDocumentIssuanceDate;
	}
	public void setIndicatorDocumentIssuanceDate(
			boolean indicatorDocumentIssuanceDate) {
		this.indicatorDocumentIssuanceDate = indicatorDocumentIssuanceDate;
	}
	public boolean isIndicatorAlphanumericWithDash() {
		return indicatorAlphanumericWithDash;
	}
	public void setIndicatorAlphanumericWithDash(
			boolean indicatorAlphanumericWithDash) {
		this.indicatorAlphanumericWithDash = indicatorAlphanumericWithDash;
	}
	public boolean isIndicatorDocumentSource() {
		return indicatorDocumentSource;
	}
	public void setIndicatorDocumentSource(
			boolean indicatorDocumentSource) {
		this.indicatorDocumentSource = indicatorDocumentSource;
	}
	
	
	
	

}
