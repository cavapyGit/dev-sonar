package com.pradera.core.component.helperui.to;

import java.io.Serializable;
import java.util.List;

import com.pradera.commons.utils.GenericDataModel;
import com.pradera.model.generalparameter.ParameterTable;
import com.pradera.model.issuancesecuritie.Issuance;
import com.pradera.model.issuancesecuritie.Issuer;

public class IssuanceSearchTO implements Serializable {

	private static final long serialVersionUID = -1229032786660948360L;
	private Issuer issuer;
	private List<ParameterTable> lstInstrumentType, lstSecurityType, lstSecurityClass, lstState;
	private Integer instrumentType, securityType, securityClass, state;
	private String description;
	private boolean blNoResult;
	private GenericDataModel<Issuance> lstIssuance;
	
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	public List<ParameterTable> getLstInstrumentType() {
		return lstInstrumentType;
	}
	public void setLstInstrumentType(List<ParameterTable> lstInstrumentType) {
		this.lstInstrumentType = lstInstrumentType;
	}
	public List<ParameterTable> getLstSecurityType() {
		return lstSecurityType;
	}
	public void setLstSecurityType(List<ParameterTable> lstSecurityType) {
		this.lstSecurityType = lstSecurityType;
	}
	public List<ParameterTable> getLstSecurityClass() {
		return lstSecurityClass;
	}
	public void setLstSecurityClass(List<ParameterTable> lstSecurityClass) {
		this.lstSecurityClass = lstSecurityClass;
	}
	public List<ParameterTable> getLstState() {
		return lstState;
	}
	public void setLstState(List<ParameterTable> lstState) {
		this.lstState = lstState;
	}
	public Integer getInstrumentType() {
		return instrumentType;
	}
	public void setInstrumentType(Integer instrumentType) {
		this.instrumentType = instrumentType;
	}
	public Integer getSecurityType() {
		return securityType;
	}
	public void setSecurityType(Integer securityType) {
		this.securityType = securityType;
	}
	public Integer getSecurityClass() {
		return securityClass;
	}
	public void setSecurityClass(Integer securityClass) {
		this.securityClass = securityClass;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isBlNoResult() {
		return blNoResult;
	}
	public void setBlNoResult(boolean blNoResult) {
		this.blNoResult = blNoResult;
	}
	public GenericDataModel<Issuance> getLstIssuance() {
		return lstIssuance;
	}
	public void setLstIssuance(GenericDataModel<Issuance> lstIssuance) {
		this.lstIssuance = lstIssuance;
	}
}
