package com.pradera.core.component.helperui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import com.pradera.commons.configuration.Configurable;
import com.pradera.commons.sessionuser.UserInfo;
import com.pradera.commons.utils.GeneralConstants;
import com.pradera.commons.utils.GenericDataModel;
import com.pradera.commons.utils.JSFUtilities;
import com.pradera.commons.utils.PropertiesUtilities;
import com.pradera.commons.view.GenericBaseBean;
import com.pradera.core.component.accounts.facade.AccountsFacade;
import com.pradera.core.component.generalparameter.facade.GeneralParametersFacade;
import com.pradera.core.component.generalparameter.to.ParameterTableTO;
import com.pradera.core.component.helperui.HelperBean;
import com.pradera.core.component.helperui.facade.HelperComponentFacade;
import com.pradera.core.component.helperui.to.HolderSearchTO;
import com.pradera.core.component.helperui.to.IssuerSearchTO;
import com.pradera.core.component.utils.DocumentValidator;
import com.pradera.core.component.utils.PersonTO;
import com.pradera.integration.common.validation.Validations;
import com.pradera.integration.exception.ServiceException;
import com.pradera.model.accounts.Holder;
import com.pradera.model.accounts.Participant;
import com.pradera.model.accounts.holderaccounts.type.HolderAccountType;
import com.pradera.model.accounts.type.HolderStateType;
import com.pradera.model.accounts.type.ParticipantStateType;
import com.pradera.model.accounts.type.PersonType;
import com.pradera.model.generalparameter.type.MasterTableType;
import com.pradera.model.generalparameter.type.ParameterTableStateType;
import com.pradera.model.issuancesecuritie.Issuer;

/**
 * <ul>
 * <li>
 * Project iDepositary.
 * Copyright PraderaTechnologies 2015.</li>
 * </ul>
 * 
 * The Class HolderHelperBean.
 *
 * @author PraderaTechnologies.
 * @version 1.0 , 12-jun-2015
 */
@HelperBean
public class HolderHelperBean extends GenericBaseBean implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The general parameters facade. */
	@Inject
	GeneralParametersFacade generalParametersFacade;
	
	/** The helper component facade. */
	@EJB
	HelperComponentFacade helperComponentFacade;
	
	/** The helper component facade. */
	@EJB
	AccountsFacade accountFacade;
	
	Boolean indIssuerBlock ;
	
	/** The user info. */
	@Inject
	private UserInfo userInfo;
	
	/** The document validator. */
	@Inject
	private DocumentValidator documentValidator;
	
	/** The max results query helper. */
	@Inject @Configurable
	Integer maxResultsQueryHelper;
	
	/** The disabled issuer. */
	private boolean disabledParticipant,disabledIssuer, disablecomPart;
	
	/** The lst participant. */
	private List<Participant> lstParticipant;
	
	/** The lst issuer. */
	private List<Issuer> lstIssuer;
	
	/** The holder search to. */
	private HolderSearchTO holderSearchTO;
	
	
	private boolean isParticipantInv;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		holderSearchTO = new HolderSearchTO();
		holderSearchTO.setParticipant(new Participant());
		disablecomPart=true;
		isParticipantInv=false;
	}
	
	/**
	 * Load help.
	 */
	public void loadHelp(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ELContext elContext = context.getELContext();
			ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.personType}", Integer.class);
			Integer personType = (Integer) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blPersonType}", Boolean.class);
			Boolean blPersonType = (Boolean) valueExpression.getValue(elContext);
			ValueExpression valueExpressionIndicator = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuerBlock}", Boolean.class);
			indIssuerBlock = Boolean.valueOf(valueExpressionIndicator.getValue(elContext).toString());
			if((Validations.validateIsNullOrEmpty(personType) || GeneralConstants.ZERO_VALUE_INTEGER.equals(personType))
					&& blPersonType){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.persontype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuer}", Boolean.class);
			Boolean blIssuer = (Boolean) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
			Issuer issuer = (Issuer) valueExpression.getValue(elContext);
			valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.participant}", Long.class);
			Long participant = (Long) valueExpression.getValue(elContext);
			holderSearchTO = new HolderSearchTO();
			if(personType!=null && HolderAccountType.NATURAL.getCode().equals(personType)
					|| HolderAccountType.SUCCESSION.getCode().equals(personType)){
				holderSearchTO.setPersonType(PersonType.NATURAL.getCode());
				holderSearchTO.setBlPersonType(true);
				PersonTO person = new PersonTO();
				person.setPersonType(holderSearchTO.getPersonType());
				changePersonType();
			}else if(personType!=null && HolderAccountType.JURIDIC.getCode().equals(personType)){
				holderSearchTO.setPersonType(PersonType.JURIDIC.getCode());
				holderSearchTO.setBlPersonType(true);
				PersonTO person = new PersonTO();
				person.setPersonType(holderSearchTO.getPersonType());
				changePersonType();
			}
			else{
				holderSearchTO.setPersonType(PersonType.JURIDIC.getCode());
				// si no es mancomunado, se maneja el disabled desde el formulario. Si es mancomunado no se bloquea el tipo de persona
				if(personType!=null && !HolderAccountType.OWNERSHIP.getCode().equals(personType)){
					holderSearchTO.setBlPersonType(blPersonType);
				}
				
				changePersonType();
			}
			
			disabledIssuer = true;
			disabledParticipant = true;
			
			if(blIssuer){
				disabledParticipant = false;
				holderSearchTO.setBlIssuer(true);
				if(issuer!=null && Validations.validateIsNotNull(issuer.getIdIssuerPk())){					
					holderSearchTO.setIssuer(issuer);
					lstIssuer = new ArrayList<Issuer>();
					lstIssuer.add(holderSearchTO.getIssuer());	
					if(Validations.validateIsNotNullAndPositive(participant)){
						holderSearchTO.setParticipant(helperComponentFacade.findParticipant(participant));
						lstParticipant = new ArrayList<Participant>();
						lstParticipant.add(holderSearchTO.getParticipant());
					}else{
						holderSearchTO.setParticipant(new Participant());
						if(lstParticipant == null || lstParticipant.size()<=1){
							listParticipantsForIssuer(holderSearchTO.getIssuer());
						}						
					}
				}else{
					holderSearchTO.setIssuer(new Issuer());
					if(lstIssuer == null || lstIssuer.size()<=1){
						listIssuers();
					}
					if(!userInfo.getUserAccountSession().isIssuerInstitucion()){
						disabledIssuer = false;
					}
					holderSearchTO.setParticipant(new Participant());
					listParticipants();
				}	
			}else{
				holderSearchTO.setBlIssuer(false);
				if(Validations.validateIsNotNullAndPositive(participant)){
					holderSearchTO.setParticipant(helperComponentFacade.findParticipant(participant));
		
					//fill lstParticipant with one item 
					lstParticipant = new ArrayList<Participant>();
					lstParticipant.add(holderSearchTO.getParticipant());
				} else {
					
					holderSearchTO.setParticipant(new Participant());
					
					if(lstParticipant == null || lstParticipant.size()<=1){
						listParticipants();
					}
					
					//If user is not participant institution, then enable participant combo
					if(!userInfo.getUserAccountSession().isParticipantInstitucion()){
						disabledParticipant = false;
					}
				}
			}
			if((userInfo.getUserAccountSession().isIssuerInstitucion() || userInfo.getUserAccountSession().isIssuerDpfInstitucion())&&  indIssuerBlock){
				disablecomPart = false;
			}
			fillCombos();			
			holderSearchTO.setState(HolderStateType.REGISTERED.getCode());
			loginUserParticipantInv();
			loginUserIssuerDpf(indIssuerBlock);
			RequestContext.getCurrentInstance().execute("PF('dlgHolder').show();");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	/**
	 * set type and numbre document 
	 *  
	 * */
	public void loginUserParticipantInv()  {
		try {
			if(userInfo.getUserAccountSession().isParticipantInvestorInstitucion()
					&& Validations.validateIsNotNull(userInfo.getUserAccountSession().getParticipantCode()))
			{
				Participant participantInv= new Participant();
				participantInv=		helperComponentFacade.findParticipantsByCode(userInfo.getUserAccountSession().getParticipantCode());
				isParticipantInv=true;
				holderSearchTO.setDocumentNumber(participantInv.getDocumentNumber());
				holderSearchTO.setDocumentType(participantInv.getDocumentType());
				holderSearchTO.setPersonType(PersonType.JURIDIC.getCode());
				holderSearchTO.setParticipant(participantInv);
				holderSearchTO.setBlPersonType(true);
				lstParticipant = new ArrayList<Participant>();
				lstParticipant.add(holderSearchTO.getParticipant());
				disabledParticipant = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * set type and numbre document 
	 *  
	 * */
	public void loginUserIssuerDpf(boolean indIssuerBlock){
		try {
			
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion()
					&& Validations.validateIsNotNull(userInfo.getUserAccountSession().getIssuerCode()) && !indIssuerBlock)
			{
				Long participantDpf = getIssuerDpfInstitution(userInfo);												
				if(Validations.validateIsNotNullAndNotEmpty(participantDpf)){
					disablecomPart = true;					
					holderSearchTO.setParticipant(helperComponentFacade.findParticipant(participantDpf));
					lstParticipant.add(holderSearchTO.getParticipant());
					disabledParticipant = true;
				}else{
					lstParticipant= null ;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * List participants.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listParticipants() throws ServiceException{
		Participant filter = new Participant();
		filter.setState(ParticipantStateType.REGISTERED.getCode());
		
		lstParticipant = helperComponentFacade.getLisParticipantServiceFacade(filter);
	}
	
	/**
	 * List issuers.
	 *
	 * @throws ServiceException the service exception
	 */
	private void listIssuers() throws ServiceException{
		IssuerSearchTO filter = new IssuerSearchTO();
		filter.setOrderBy("mnemonic");		
		lstIssuer = helperComponentFacade.findIssuers(filter);
	}
	
	/**
	 * Load list participant.
	 *
	 * @throws ServiceException the service exception
	 */
	public void loadListParticipant()throws ServiceException{
		if(holderSearchTO.getIssuer()!=null && holderSearchTO.getIssuer().getIdIssuerPk()!=null){
			listParticipantsForIssuer(holderSearchTO.getIssuer());
		}else{
			listIssuers();
		}
	}
	
	/**
	 * List participants for issuer.
	 *
	 * @param issuer the issuer
	 * @throws ServiceException the service exception
	 */
	private void listParticipantsForIssuer(Issuer issuer) throws ServiceException{
		Participant filter = new Participant();
		filter.setState(ParticipantStateType.REGISTERED.getCode());		
		filter.setIssuer(issuer);
		lstParticipant = helperComponentFacade.getLisParticipantForIssuerServiceFacade(filter);
	}
	
	/**
	 * Search holders.
	 */
	public void searchHolders(){
		try {
			List<Holder> lstResult = null;
			if(Validations.validateIsNullOrEmpty(holderSearchTO.getState())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.state"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(Validations.validateIsNullOrEmpty(holderSearchTO.getPersonType())){
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.persontype"));
				JSFUtilities.showSimpleValidationDialog();
				return;
			}
			if(userInfo.getUserAccountSession().isIssuerDpfInstitucion() && !disablecomPart && !indIssuerBlock){
				lstResult=null;
				return;
			}
			holderSearchTO.setLstHolder(null);
			holderSearchTO.setBlNoResult(true);	
			
			if(holderSearchTO.isBlIssuer()){
				lstResult = helperComponentFacade.findHoldersForIssuer(holderSearchTO);
			}else{
				lstResult = helperComponentFacade.findHolders(holderSearchTO);
			}			
			if(Validations.validateListIsNotNullAndNotEmpty(lstResult)){
				holderSearchTO.setLstHolder(new GenericDataModel<Holder>(lstResult));
				holderSearchTO.setBlNoResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Change person type.
	 */
	public void changePersonType(){
		holderSearchTO.setBlJuridic(false);
		holderSearchTO.setBlNatural(false);
		holderSearchTO.setName(null);
		holderSearchTO.setFirstLastName(null);
		holderSearchTO.setSecondLastName(null);
		holderSearchTO.setFullName(null);
		PersonTO person = new PersonTO();
		
		if(PersonType.JURIDIC.getCode().equals(holderSearchTO.getPersonType())){
			holderSearchTO.setBlJuridic(true);
			person.setPersonType(holderSearchTO.getPersonType());
			person.setFlagJuridicDocuments(true);
			person.setFlagHolderIndicator(true);
			
		}
		else if(PersonType.NATURAL.getCode().equals(holderSearchTO.getPersonType())){
			holderSearchTO.setBlNatural(true);
			person.setPersonType(holderSearchTO.getPersonType());
			person.setFlagNaturalDocuments(true);
			person.setFlagHolderIndicator(true);
			
		}
		holderSearchTO.setLstDocumentType(documentValidator.getDocumentTypeResult(person));
		holderSearchTO.getLstDocumentType().size();
		
	}
	
	/**
	 * Clean result.
	 */
	public void cleanResult(){
		holderSearchTO.setLstHolder(null);
		holderSearchTO.setBlNoResult(false);
	}
	
	/**
	 * Clean search.
	 */
	public void cleanSearch(){
		JSFUtilities.resetViewRoot();
		cleanResult();
//		if(!holderSearchTO.isBlPersonType()){
//			holderSearchTO.setPersonType(null);
//		}
		if(!isParticipantInv){
			holderSearchTO.setDocumentType(null);
			holderSearchTO.setDocumentNumber(null);
		}
		holderSearchTO.setName(null);
		holderSearchTO.setFirstLastName(null);
		holderSearchTO.setSecondLastName(null);
		holderSearchTO.setFullName(null);
		holderSearchTO.setBlJuridic(false);
		holderSearchTO.setBlNatural(false);
		loadHelp();
	}
	
	/**
	 * Gets the holder.
	 *
	 * @param holder the holder
	 * @return the holder
	 */
	public void getHolder(Holder holder){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holder}", Holder.class);	
		if(holder==null) {
			valueExpression.setValue(elContext, null);
			return;
		}
		Holder holderTemp =helperComponentFacade.findHolder(holder); 
		
		// VALIDACION SOLO PARA OTC Y TRANSFERENCIAS EXTRABUSRSATILES DE DPF
		String viewId = context.getViewRoot().getViewId();
		if(viewId.equals("/pages/otcOperations/otcOperationsDpf/registerOtcDpf.xhtml") ||
				viewId.equals("/pages/otcOperations/otcOperations/registerOtcOperation.xhtml")){
			if(holderTemp != null && holderTemp.getIndCanNegotiate() != null && 
					holderTemp.getIndCanNegotiate().equals(1)){
				valueExpression.setValue(elContext, holderTemp);
			} else {
				//valueExpression.setValue(elContext, new Holder());
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.cannot.negotiate", new Object[]{holder.getIdHolderPk().toString()}));
				JSFUtilities.showSimpleValidationDialog();				
			}
			
		} else {
			valueExpression.setValue(elContext, holderTemp);
		}
		
	}
	
	
	/**
	 * Find holder.
	 */
	public void findHolder(){
		FacesContext context = FacesContext.getCurrentInstance();
		ELContext elContext = context.getELContext();
		
		Holder holderTemp = null;
		
//		userInfo.get
		ValueExpression valueExpressionParticipant = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.participant}", Long.class);
		Long participant = (Long) valueExpressionParticipant.getValue(elContext);
		ValueExpression valueExpressionHolder = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.holder}", Holder.class);
		Holder holder = (Holder) valueExpressionHolder.getValue(elContext);
		ValueExpression valueExpressionIndicator = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blValidation}", Boolean.class);
		Boolean indValidation = Boolean.valueOf(valueExpressionIndicator.getValue(elContext).toString());
		ValueExpression valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.blIssuer}", Boolean.class);
		Boolean blIssuer = (Boolean) valueExpression.getValue(elContext);
		valueExpression = context.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.issuer}", Issuer.class);
		Issuer issuer = (Issuer) valueExpression.getValue(elContext);
		if(Validations.validateIsNotNullAndNotEmpty(holder.getIdHolderPk())){
			if(blIssuer){
				if(Validations.validateIsNotNull(issuer) && 
						Validations.validateIsNotNullAndNotEmpty(issuer.getIdIssuerPk())){
					if(!helperComponentFacade.findRelationHolderIssuerParticipant(holder.getIdHolderPk(), issuer.getIdIssuerPk(),null,isParticipantInv)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getGenericMessage("holder.helper.alert.not.relation.issuer"));
						JSFUtilities.showSimpleValidationDialog();
						valueExpressionHolder.setValue(elContext, new Holder());
						return;
					}else if(Validations.validateIsNotNullAndPositive(participant)){						
						if(!helperComponentFacade.findRelationHolderIssuerParticipant(holder.getIdHolderPk(), issuer.getIdIssuerPk(),participant,isParticipantInv)){
							JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
									, PropertiesUtilities.getGenericMessage("holder.helper.alert.not.relation.issuer.participant"));
							JSFUtilities.showSimpleValidationDialog();
							valueExpressionHolder.setValue(elContext, new Holder());
							return;
						}						
					}
				}else if(Validations.validateIsNotNullAndPositive(participant)){
					if(!helperComponentFacade.findRelationHolderParticipant(holder.getIdHolderPk(), participant,isParticipantInv)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getGenericMessage("holder.helper.alert.not.relation.participant"));
						JSFUtilities.showSimpleValidationDialog();
						valueExpressionHolder.setValue(elContext, new Holder());
						return;
					}
				}
			}else{
				if(indValidation && Validations.validateIsNotNullAndPositive(participant)){
					if(!helperComponentFacade.findRelationHolderParticipant(holder.getIdHolderPk(), participant,isParticipantInv)){
						JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
								, PropertiesUtilities.getGenericMessage("holder.helper.alert.not.relation.participant"));
						JSFUtilities.showSimpleValidationDialog();
						valueExpressionHolder.setValue(elContext, new Holder());
						return;
					}
				}
			}
			
			holderTemp = helperComponentFacade.findHolder(holder);
			if(Validations.validateIsNotNullAndNotEmpty(holderTemp)){
				valueExpressionHolder.setValue(elContext, holderTemp);
			}else{
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.notexist", new Object[]{holder.getIdHolderPk().toString()}));
				JSFUtilities.showSimpleValidationDialog();
				valueExpressionHolder.setValue(elContext, new Holder());
			}
		}else{
			valueExpressionHolder.setValue(elContext, new Holder());
		}	
		
		// VALIDACION SOLO PARA OTC Y TRANSFERENCIAS EXTRABUSRSATILES DE DPF
		String viewId = context.getViewRoot().getViewId();
		if(viewId.equals("/pages/otcOperations/otcOperationsDpf/registerOtcDpf.xhtml") ||
				viewId.equals("/pages/otcOperations/otcOperations/registerOtcOperation.xhtml")){
			if(holderTemp != null && holderTemp.getIndCanNegotiate() != null && 
					holderTemp.getIndCanNegotiate().equals(1)){
				return;
			} else {
				JSFUtilities.showMessageOnDialog(PropertiesUtilities.getGenericMessage(GeneralConstants.LBL_HEADER_ALERT)
						, PropertiesUtilities.getGenericMessage("holder.helper.alert.cannot.negotiate", new Object[]{holder.getIdHolderPk().toString()}));
				JSFUtilities.showSimpleValidationDialog();
				valueExpressionHolder.setValue(elContext, new Holder());
			}
			
		}
	}
	
	/**
	 * Fill combos.
	 *
	 * @throws ServiceException the service exception
	 */
	private void fillCombos() throws ServiceException{
		ParameterTableTO paramTabTO = new ParameterTableTO();
		paramTabTO.setState(ParameterTableStateType.REGISTERED.getCode());
		paramTabTO.setMasterTableFk(MasterTableType.HOLDER_STATE_TYPE.getCode());
		holderSearchTO.setLstState(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
		paramTabTO.setMasterTableFk(MasterTableType.PERSON_TYPE.getCode());
		holderSearchTO.setLstPersonType(generalParametersFacade.getListParameterTableServiceBean(paramTabTO));
	}
	
	/**
	 * Gets the holder search to.
	 *
	 * @return the holder search to
	 */
	public HolderSearchTO getHolderSearchTO() {
		return holderSearchTO;
	}
	
	/**
	 * Sets the holder search to.
	 *
	 * @param holderSearchTO the new holder search to
	 */
	public void setHolderSearchTO(HolderSearchTO holderSearchTO) {
		this.holderSearchTO = holderSearchTO;
	}

	/**
	 * Checks if is disabled participant.
	 *
	 * @return true, if is disabled participant
	 */
	public boolean isDisabledParticipant() {
		return disabledParticipant;
	}

	/**
	 * Sets the disabled participant.
	 *
	 * @param disabledParticipant the new disabled participant
	 */
	public void setDisabledParticipant(boolean disabledParticipant) {
		this.disabledParticipant = disabledParticipant;
	}

	/**
	 * Gets the lst participant.
	 *
	 * @return the lst participant
	 */
	public List<Participant> getLstParticipant() {
		return lstParticipant;
	}

	/**
	 * Sets the lst participant.
	 *
	 * @param lstParticipant the new lst participant
	 */
	public void setLstParticipant(List<Participant> lstParticipant) {
		this.lstParticipant = lstParticipant;
	}

	/**
	 * Checks if is disabled issuer.
	 *
	 * @return true, if is disabled issuer
	 */
	public boolean isDisabledIssuer() {
		return disabledIssuer;
	}

	/**
	 * Sets the disabled issuer.
	 *
	 * @param disabledIssuer the new disabled issuer
	 */
	public void setDisabledIssuer(boolean disabledIssuer) {
		this.disabledIssuer = disabledIssuer;
	}

	/**
	 * Gets the lst issuer.
	 *
	 * @return the lst issuer
	 */
	public List<Issuer> getLstIssuer() {
		return lstIssuer;
	}

	/**
	 * Sets the lst issuer.
	 *
	 * @param lstIssuer the new lst issuer
	 */
	public void setLstIssuer(List<Issuer> lstIssuer) {
		this.lstIssuer = lstIssuer;
	}

	public Integer getMaxResultsQueryHelper() {
		return maxResultsQueryHelper;
	}

	/**
	 * @return the isParticipantInv
	 */
	public boolean isParticipantInv() {
		return isParticipantInv;
	}

	/**
	 * @param isParticipantInv the isParticipantInv to set
	 */
	public void setParticipantInv(boolean isParticipantInv) {
		this.isParticipantInv = isParticipantInv;
	}

	/**
	 * @return the disablecomPart
	 */
	public boolean isDisablecomPart() {
		return disablecomPart;
	}

	/**
	 * @param disablecomPart the disablecomPart to set
	 */
	public void setDisablecomPart(boolean disablecomPart) {
		this.disablecomPart = disablecomPart;
	}

}
